<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBsoSuppliersProductsPayMethodsAccessBso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bso_suppliers_products_pay_methods_access_bso', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('group_id');
            $table->integer('access_bso');
            $table->integer('bso_supplier_id');
            $table->integer('product_id');
            $table->integer('hold_kv_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bso_suppliers_products_pay_methods_access_bso');
    }
}
