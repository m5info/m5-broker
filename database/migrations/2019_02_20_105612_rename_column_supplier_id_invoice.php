<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnSupplierIdInvoice extends Migration
{


    public function up(){
        Schema::table('invoices', function(Blueprint $table){
            $table->renameColumn('supplier_id', 'org_id');
        });
    }


    public function down(){
        Schema::table('invoices', function(Blueprint $table){
            $table->renameColumn('org_id', 'supplier_id');
        });
    }
}
