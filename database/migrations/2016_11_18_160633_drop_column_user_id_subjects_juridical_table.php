<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnUserIdSubjectsJuridicalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects_juridical', function (Blueprint $table) {
            //$table->dropForeign('fk_users_juridical_user1');
            $table->dropColumn('user_id');
        });
    }

    public function down()
    {

    }

}
