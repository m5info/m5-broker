<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrganizationSettings extends Migration{

    public function up(){
        Schema::table('organizations', function(Blueprint $table){
            $table->integer('nds_type_id');
            $table->integer('request_type_id');
            $table->integer('taxation_system_type_id');
        });
    }

    public function down(){
        Schema::table('organizations', function(Blueprint $table){
            $table->dropColumn('nds_type_id');
            $table->dropColumn('request_type_id');
            $table->dropColumn('taxation_system_type_id');
        });
    }
}
