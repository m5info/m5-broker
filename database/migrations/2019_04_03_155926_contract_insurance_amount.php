<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContractInsuranceAmount extends Migration
{

    public function up()
    {
        Schema::table('contracts', function(Blueprint $table){
            $table->addColumn('decimal', 'insurance_amount', ['total' => 11, 'places' => 2])->default(0);
        });
    }


    public function down()
    {
        Schema::table('contracts', function(Blueprint $table){
            $table->dropColumn('insurance_amount');
        });
    }
}
