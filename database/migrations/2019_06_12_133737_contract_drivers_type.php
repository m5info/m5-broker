<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContractDriversType extends Migration{

    public function up(){
        Schema::table('contracts', function(Blueprint $table){
            $table->addColumn('integer', 'drivers_type_id')->nullable()->default(0);
        });
    }


    public function down(){
        Schema::table('contracts', function(Blueprint $table){
            $table->dropColumn('drivers_type_id');
        });
    }
}
