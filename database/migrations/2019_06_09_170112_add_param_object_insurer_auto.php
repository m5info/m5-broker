<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParamObjectInsurerAuto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('object_insurer_auto', function(Blueprint $table){

            $table->integer('purpose_id')->nullable();

            $table->string('body_number')->nullable();
            $table->integer('car_year')->nullable();

            $table->integer('passengers_count')->nullable();
            $table->decimal('weight')->nullable();
            $table->decimal('capacity')->nullable();

            $table->integer('is_trailer')->default(0)->nullable();

            $table->integer('doc_type')->default(0)->nullable();
            $table->string('docserie')->nullable();
            $table->string('docnumber')->nullable();
            $table->date('docdate')->nullable();

            $table->string('dk_number')->nullable();
            $table->date('dk_date')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
