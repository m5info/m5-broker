<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $perm = [
            'title' => 'templates',
            'not_visible' => 0,
            'group_id' => 3,
        ];

        DB::table('permissions')->insert($perm);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('permissions')->where('title', '=', 'templates')->delete();
    }
}
