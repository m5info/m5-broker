<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReaddTemplateCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::delete('delete from template_categories');
        DB::insert("insert into template_categories values 
            (1 , 0, 'system', 'Системные', 1, 0, 1),
            (2 , 1, 'system_acts', 'Акты', 1, 0, 1),
            (3 , 2, 'bso_from_agent', 'Акт приема БСО от агента', 0, 0, 1),
            (4 , 2, 'warehouse_to_agent', 'Акт передачи со склада агенту', 0, 0, 1),
            (5 , 2, 'agent_to_agent', 'Акт передачи от агента-агенту', 0, 0, 1),
            (6 , 2, 'bso_to_sk', 'Акт передачи БСО в СК', 0, 0, 1),
            (7 , 2, 'bso_to_tp', 'Акт передачи БСО на точку продаж', 0, 0, 1),
            (8 , 2, 'bso_to_courier', 'Акт передачи БСО курьеру', 0, 0, 1),
            (9 , 2, 'damaged_bso_from_agent', 'Акт передачи испорченного БСО от агента', 0, 0, 1),
            (10, 2, 'bso_to_region', 'Акт передачи БСО на Регион', 0, 0, 1),
            (11, 2, 'bso_from_tp_to_tp', 'Акт передачи БСО от ТП на ТП', 0, 0, 1),
            (12, 0, 'sk', 'Страховая Компания', 1, 0, 1),
            (13, 12, 'sk_acts', 'Акты', 1, 0, 1),
            (14, 13, 'sk_bso_to_sk', 'Акт передачи БСО в СК', 1, 1, 1),
            (15, 12, 'sk_reports', 'Отчёты', 1, 0, 1),
            (16, 15, 'report_borderau_to_sk', 'Отчёт Бордеро в СК', 1, 1, 1),
            (17, 15, 'report_dvou_to_sk', 'Отчёт ДВОУ в СК', 1, 1, 1),
            (18, 1, 'system_contracts', 'Договоры', 0, 0, 1),
            (19, 18, 'contract_agent', 'Договор агента', 0, 0, 1),
            (20, 13, 'contracts_to_sk', 'Акт передачи договоров в СК', 1, 1, 1),
            (21, 1, 'system_invoices', 'Счета', 0, 0, 1),
            (22, 21, 'invoice', 'Счёт', 0, 0, 0),
            (23, 21, 'invoice_act', 'Акт по счёту', 0, 0, 0),
            (24, 21, 'invoice_reservation', 'Резервный счёт', 0, 0, 1),
            (25, 1, 'cashbox', 'Касса', 0, 0, 1),
            (26, 25, 'income_expense', 'Доп доходы / Расходы', 0, 0, 1),
            (27, 2, 'bso_realized', 'Акт БСО реализованные', 0, 0, 1),
            (28,21, 'act_checkout', 'Акт сдачи в кассу', 0,0, 1),
            (29, 2, 'reserve_act', 'Резервный акт'	, 0, 0,	1)
        ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->up();
    }
}
