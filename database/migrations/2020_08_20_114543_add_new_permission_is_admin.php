<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPermissionIsAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $p = new \App\Models\Users\Permission();
        $p->title = 'is_admin';
        $p->group_id = \App\Models\Users\PermissionGroup::where('title', 'role_owns')->first()->id;
        $p->not_visible = 0;
        $p->sort = 1;
        $p->type = 0;
        $p->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
