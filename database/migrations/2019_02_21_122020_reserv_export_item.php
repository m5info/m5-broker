<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReservExportItem extends Migration
{

    public function up()
    {
        $export_item = new \App\Models\Settings\ExportItem();
        $export_item->title = 'Страница резервирования счёта';
        $export_item->code = 'reservation_invoice';
        $export_item->save();
    }


    public function down()
    {
        \App\Models\Settings\ExportItem::query()->findOrFail(['code', '=', 'reservation_invoice'])->delete();
    }
}
