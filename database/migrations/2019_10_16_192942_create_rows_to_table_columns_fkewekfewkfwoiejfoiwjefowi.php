<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRowsToTableColumnsFkewekfewkfwoiejfoiwjefowi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* 1 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = '№';
        $c->table_key = 'analitics_common';
        $c->column_key = '\'№\'';
        $c->is_as = 1;
        $c->as_key = '\'№\'';
        $c->sorting = 1;
        $c->save();

        /* 2 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Локация';
        $c->table_key = 'analitics_common';
        $c->column_key = 'citys.title';
        $c->is_as = 1;
        $c->as_key = 'city_title';
        $c->sorting = 1;
        $c->save();

        /* 3 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Точка продаж';
        $c->table_key = 'analitics_common';
        $c->column_key = 'points_sale.title';
        $c->is_as = 1;
        $c->as_key = 'point_title';
        $c->sorting = 1;
        $c->save();

        /* 4 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Подразделение';
        $c->table_key = 'analitics_common';
        $c->column_key = 'departments.title';
        $c->is_as = 1;
        $c->as_key = 'departments_title';
        $c->sorting = 1;
        $c->save();

        /* 5 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Продукт';
        $c->table_key = 'analitics_common';
        $c->column_key = 'products.title';
        $c->is_as = 1;
        $c->as_key = 'product_title';
        $c->sorting = 1;
        $c->save();

        /* 6 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'СК';
        $c->table_key = 'analitics_common';
        $c->column_key = 'insurance_companies.title';
        $c->is_as = 1;
        $c->as_key = 'insurance_companies_title';
        $c->sorting = 1;
        $c->save();

        /* 7 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Поток оплаты';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.payment_flow';
        $c->is_as = 1;
        $c->as_key = 'payments_payment_flow';
        $c->sorting = 1;
        $c->save();

        /* 8 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Полис №';
        $c->table_key = 'analitics_common';
        $c->column_key = 'bso_items.bso_title';
        $c->is_as = 1;
        $c->as_key = 'bso_item_bso_title';
        $c->sorting = 1;
        $c->save();

        /* 9 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Акцепт';
        $c->table_key = 'analitics_common';
        $c->column_key = 'contracts.kind_acceptance';
        $c->is_as = 1;
        $c->as_key = 'contracts_kind_accepance';
        $c->sorting = 1;
        $c->save();

        /* 10 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Кассир';
        $c->table_key = 'analitics_common';
        $c->column_key = 'invoice_payment_user.name';
        $c->is_as = 1;
        $c->as_key = 'cashier_name';
        $c->sorting = 1;
        $c->save();

        /* 11 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Дата договора';
        $c->table_key = 'analitics_common';
        $c->column_key = 'contracts.sign_date';
        $c->is_as = 1;
        $c->as_key = 'date_contract';
        $c->sorting = 1;
        $c->save();

        /* 12 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Дата оплаты';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.payment_data';
        $c->is_as = 1;
        $c->as_key = 'payments_payment_data';
        $c->sorting = 1;
        $c->save();

        /* 13 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Дата по кассе';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.invoice_payment_date';
        $c->is_as = 1;
        $c->as_key = 'payments_invoice_payment_date';
        $c->sorting = 1;
        $c->save();

        /* 14 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Дата акцепта';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.accept_date';
        $c->is_as = 1;
        $c->as_key = 'date_acceptance';
        $c->sorting = 1;
        $c->save();

        /* 15 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Страхователь';
        $c->table_key = 'analitics_common';
        $c->column_key = 'insurer.title';
        $c->is_as = 1;
        $c->as_key = 'insurer_title';
        $c->sorting = 1;
        $c->save();

        /* 16 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Организация';
        $c->table_key = 'analitics_common';
        $c->column_key = 'organizations.title';
        $c->is_as = 1;
        $c->as_key = 'organization_title';
        $c->sorting = 1;
        $c->save();

        /* 17 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Условие продажи';
        $c->table_key = 'analitics_common';
        $c->column_key = 'contracts.sales_condition';
        $c->is_as = 1;
        $c->as_key = 'contract_sale_condition';
        $c->sorting = 1;
        $c->save();

        /* 18 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Фин. политика';
        $c->table_key = 'analitics_common';
        $c->column_key = 'financial_policies.title';
        $c->is_as = 1;
        $c->as_key = 'financial_policies_title';
        $c->sorting = 1;
        $c->save();

        /* 19 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Тип оплаты';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.payment_type';
        $c->is_as = 1;
        $c->as_key = 'payment_type';
        $c->sorting = 1;
        $c->save();

        /* 20 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Тип платежа';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.type_id';
        $c->is_as = 1;
        $c->as_key = 'payment_type_id';
        $c->sorting = 1;
        $c->save();

        /* 21 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Личная продажа';
        $c->table_key = 'analitics_common';
        $c->column_key = 'contracts.is_personal_sales';
        $c->is_as = 1;
        $c->as_key = 'contracts_is_personal_sales';
        $c->sorting = 1;
        $c->save();

        /* 22 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Алгоритм рассрочки';
        $c->table_key = 'analitics_common';
        $c->column_key = 'installment_algorithms_list.title';
        $c->is_as = 1;
        $c->as_key = 'installment_algorithms_list_title';
        $c->sorting = 1;
        $c->save();

        /* 23 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Счет №';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.invoice_id';
        $c->is_as = 1;
        $c->as_key = 'payments_invoice_id';
        $c->sorting = 1;
        $c->save();

        /* 24 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Статус оплаты';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.statys_id';
        $c->is_as = 1;
        $c->as_key = 'payment_status';
        $c->sorting = 1;
        $c->save();

        /* 25 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Банк КВ (%)';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.bank_kv';
        $c->is_as = 1;
        $c->as_key = 'payment_bank_kv';
        $c->sorting = 1;
        $c->save();

        /* 26 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Банк КВ (Сумма)';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.bank_kv_total';
        $c->is_as = 1;
        $c->as_key = 'payment_bank_kv_total';
        $c->sorting = 1;
        $c->save();

        /* 27 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Менеджер';
        $c->table_key = 'analitics_common';
        $c->column_key = 'manager.name';
        $c->is_as = 1;
        $c->as_key = 'manager_name';
        $c->sorting = 1;
        $c->save();

        /* 28 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Агент';
        $c->table_key = 'analitics_common';
        $c->column_key = 'agent.name';
        $c->is_as = 1;
        $c->as_key = 'agent_name';
        $c->sorting = 1;
        $c->save();

        /* 29 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Взнос';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.payment_total';
        $c->is_as = 1;
        $c->as_key = 'payment_total';
        $c->sorting = 1;
        $c->save();

        /* 30 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Сумма в кассу';
        $c->table_key = 'analitics_common'; #analitics_common
        $c->column_key = 'IF (payments.payment_flow = 0, payments.invoice_payment_total, 0)';
        $c->is_as = 1;
        $c->as_key = 'invoice_payment_total';
        $c->sorting = 1;
        $c->save();

        /* 31 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Оф. скидка (%)';
        $c->table_key = 'analitics_common';
        $c->column_key = '(payments.official_discount_total * 100 / payments.payment_total)';
        $c->is_as = 1;
        $c->as_key = 'official_discount_total_procent';
        $c->sorting = 1;
        $c->save();

        /* 32 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Оф. скидка (Сумма)';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.official_discount_total';
        $c->is_as = 1;
        $c->as_key = 'official_discount_total';
        $c->sorting = 1;
        $c->save();

        /* 33 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Неоф. скидка (%)';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.informal_discount';
        $c->is_as = 1;
        $c->as_key = 'payment_informal_discount';
        $c->sorting = 1;
        $c->save();

        /* 34 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Неоф. скидка (Сумма)';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.informal_discount_total';
        $c->is_as = 1;
        $c->as_key = 'payment_informal_discount_total';
        $c->sorting = 1;
        $c->save();

        /* 35 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Возн. агента (%)';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.financial_policy_kv_agent';
        $c->is_as = 1;
        $c->as_key = 'payments_financial_policy_kv_agent';
        $c->sorting = 1;
        $c->save();

        /* 36 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Возн. агента (Сумма)';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.financial_policy_kv_agent_total';
        $c->is_as = 1;
        $c->as_key = 'payments_financial_policy_kv_agent_total';
        $c->sorting = 1;
        $c->save();

        /* 37 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Возн. брокера (%)';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.financial_policy_kv_bordereau + payments.financial_policy_kv_dvoy';
        $c->is_as = 1;
        $c->as_key = 'broker_reward_procent';
        $c->sorting = 1;
        $c->save();

        /* 38 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Возн. брокера (Сумма)';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.financial_policy_kv_bordereau_total + financial_policy_kv_dvoy_total';
        $c->is_as = 1;
        $c->as_key = 'broker_reward_sum';
        $c->sorting = 1;
        $c->save();

        /* 39 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Маржа (%)';
        $c->table_key = 'analitics_common';
        $c->column_key = '\'0\'';
        $c->is_as = 1;
        $c->as_key = 'margin_procent';
        $c->sorting = 1;
        $c->save();

        /* 40 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Маржа (Сумма)';
        $c->table_key = 'analitics_common';
        $c->column_key = '\'1\'';
        $c->is_as = 1;
        $c->as_key = 'margin_sum';
        $c->sorting = 1;
        $c->save();

        /* 41 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Рек. 1';
        $c->table_key = 'analitics_common';
        $c->column_key = 'financial_policies.partnership_reward_1';
        $c->is_as = 1;
        $c->as_key = 'f_p_partnership_reward_1';
        $c->sorting = 1;
        $c->save();

        /* 42 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Рек. 2';
        $c->table_key = 'analitics_common';
        $c->column_key = 'financial_policies.partnership_reward_2';
        $c->is_as = 1;
        $c->as_key = 'f_p_partnership_reward_2';
        $c->sorting = 1;
        $c->save();

        /* 43 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Рек. 3';
        $c->table_key = 'analitics_common';
        $c->column_key = 'financial_policies.partnership_reward_3';
        $c->is_as = 1;
        $c->as_key = 'f_p_partnership_reward_3';
        $c->sorting = 1;
        $c->save();

        /* 44 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'КВ ДВОУ (%)';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.financial_policy_kv_dvoy';
        $c->is_as = 1;
        $c->as_key = 'payments_financial_policy_kv_dvoy';
        $c->sorting = 1;
        $c->save();

        /* 45 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'КВ ДВОУ (Сумма)';
        $c->table_key = 'analitics_common';
        $c->column_key = 'payments.financial_policy_kv_dvoy_total';
        $c->is_as = 1;
        $c->as_key = 'payments_financial_policy_kv_dvoy_total';
        $c->sorting = 1;
        $c->save();

        /* 46 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Номер отчета';
        $c->table_key = 'analitics_common';
        $c->column_key = 'reports_orders.id';
        $c->is_as = 1;
        $c->as_key = 'reports_orders_id';
        $c->sorting = 1;
        $c->save();

        /* 47 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Статус отчета';
        $c->table_key = 'analitics_common';
        $c->column_key = 'reports_orders.accept_status';
        $c->is_as = 1;
        $c->as_key = 'reports_orders_accept_status';
        $c->sorting = 1;
        $c->save();

        /* 48 */

        $c = new \App\Models\Account\TableColumn();
        $c->column_name = 'Квитанция №';
        $c->table_key = 'analitics_common';
        $c->column_key = 'IF (payments.bso_receipt_id > 0, payments.bso_receipt, \'\')';
        $c->is_as = 1;
        $c->as_key = 'receipt_number';
        $c->sorting = 1;
        $c->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
