<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewParObjectInsurerAuto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('object_insurer_auto', function (Blueprint $table){
            $table->string('docissued')->nullable();
            $table->string('dk_coments')->nullable();
            $table->string('brand_tire')->nullable();

            $table->integer('ts_category_okp')->nullable();
            $table->integer('brake_system_id')->nullable();

            $table->decimal('mileage', 11,2)->nullable();
            $table->decimal('max_weight', 11,2)->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
