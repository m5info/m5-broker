<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUndergroundColumnToSubjectsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects_fl', function (Blueprint $table) {
            $table->string('underground')->after('delivery_address')->nullable();
        });

        Schema::table('subjects_ul', function (Blueprint $table) {
            $table->string('underground')->after('delivery_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subjects_tables', function (Blueprint $table) {
            //
        });
    }
}
