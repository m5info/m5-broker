<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubpermissionForInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $perm = \App\Models\Users\Permission::where('title', '=', 'invoices_type_view')->get()->first();

        \App\Models\Users\Subpermission::create([
            'permission_id' => $perm->id,
            'title' => 'all',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => $perm->id,
            'title' => 'nalich',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => $perm->id,
            'title' => 'beznalich',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => $perm->id,
            'title' => 'sk',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
