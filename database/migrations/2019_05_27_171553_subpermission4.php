<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Subpermission4 extends Migration{

    public function up(){

        \App\Models\Users\Subpermission::create([
            'permission_id' => 31,
            'title' => 'temp',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => 31,
            'title' => 'in_cashbox',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => 31,
            'title' => 'in_check',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => 31,
            'title' => 'in_correction',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => 31,
            'title' => 'in_pay',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => 31,
            'title' => 'in_confirm',
        ]);

    }


    public function down(){
        \App\Models\Users\Subpermission::query()->where('permission_id', 31)->delete();
    }
}
