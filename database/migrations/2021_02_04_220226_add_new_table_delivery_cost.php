<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewTableDeliveryCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_cost', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->decimal('delivery_price', 11, 2)->nullable()->default(null);
            $table->decimal('weekend_price', 11, 2)->nullable()->default(null);
            $table->integer('is_actual');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
