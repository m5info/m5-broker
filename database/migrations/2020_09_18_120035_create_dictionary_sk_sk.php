<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDictionarySkSk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dictionary_sk_sk', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('insurance_companies_id')->nullable();
            $table->integer('bso_supplier_id')->nullable();
            $table->integer('dictionary_sk_id')->nullable();
            $table->string('dictionary_sk_sk_id')->nullable();
            $table->string('sk_title')->nullable();
            $table->string('kpp')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dictionary_sk_sk');
    }
}
