<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Kefw219912912kd913kd93k92d39d232k extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* 1 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = '№';
        $t->column_key = 'payments.id';
        $t->as_key = 'payments_id';
        $t->save();

        /* 2 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Акт в СК';
        $t->column_key = "IF (payments.acts_sk_id > 0, reports_act.id, 'Не сформирован')";
        $t->as_key = 'reports_act_id';
        $t->save();

        /* 3 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = '№ договора';
        $t->column_key = 'bso_items.bso_title';
        $t->as_key = 'bso_title';
        $t->save();

        /* 4 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Продукт';
        $t->column_key = 'products.title';
        $t->as_key = 'products_title';
        $t->save();

        /* 5 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Страхователь';
        $t->column_key = 'insurer.title';
        $t->as_key = 'insurer';
        $t->save();

        /* 6 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Агент/Менеджер';
        $t->column_key = "IF (contracts.sales_condition = 0, agent.name, manager.name)";
        $t->as_key = 'manager_or_agent_name';
        $t->save();

        /* 7 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Руководитель';
        $t->column_key = "parent_agent.name";
        $t->as_key = 'parent_agent_name';
        $t->save();

        /* 8 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Тип договора';
        $t->column_key = 'contracts.type_id';
        $t->as_key = 'contracts_type_id';
        $t->save();

        /* 9 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Тип платежа';
        $t->column_key = 'payments.payment_type';
        $t->as_key = 'payments_type';
        $t->save();

        /* 10 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Поток оплаты';
        $t->column_key = 'payments.payment_flow';
        $t->as_key = 'payments_flow';
        $t->save();

        /* 11 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Квитанция';
        $t->column_key = 'payments.bso_receipt';
        $t->as_key = 'payments_bso_receipt';
        $t->save();

        /* 12 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Сумма';
        $t->column_key = 'payments.payment_total';
        $t->as_key = 'payments_payment_total';
        $t->save();

        /* 13 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Оф. скидка';
        $t->column_key = 'payments.official_discount_total';
        $t->as_key = 'payments_official_discount_total';
        $t->save();

        /* 14 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Оф. скидка (%)';
        $t->column_key = 'payments.official_discount';
        $t->as_key = 'payments_official_discount';
        $t->save();

        /* 15 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Неоф. скидка';
        $t->column_key = 'payments.informal_discount_total';
        $t->as_key = 'payments_informal_discount_total';
        $t->save();

        /* 16 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Неоф. скидка (%)';
        $t->column_key = 'payments.informal_discount';
        $t->as_key = 'payments_informal_discount';
        $t->save();

        /* 17 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Дата оплаты';
        $t->column_key = 'payments.payment_data';
        $t->as_key = 'payments_payment_data';
        $t->save();

        /* 18 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Статус';
        $t->column_key = 'payments.statys_id';
        $t->as_key = 'payments_statys_id';
        $t->save();

        /* 19 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Финансовая политика';
        $t->column_key = 'financial_policies.title';
        $t->as_key = 'financial_policies_title';
        $t->save();

        /* 20 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'КВ Агента';
        $t->column_key = 'payments.financial_policy_kv_agent_total';
        $t->as_key = 'payments_financial_policy_kv_agent_total';
        $t->save();

        /* 21 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Бордеро КВ (%)';
        $t->column_key = 'payments.financial_policy_kv_bordereau';
        $t->as_key = 'payments_financial_policy_kv_bordereau';
        $t->save();

        /* 22 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Бордеро КВ';
        $t->column_key = 'payments.financial_policy_kv_bordereau_total';
        $t->as_key = 'payments_financial_policy_kv_bordereau_total';
        $t->save();

        /* 23 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Бордеро Отчет';
        $t->column_key = 'reports_bordero.title';
        $t->as_key = 'reports_bordero_title';
        $t->save();

        /* 25 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'ДВОУ КВ (%)';
        $t->column_key = 'payments.financial_policy_kv_dvoy';
        $t->as_key = 'payments_financial_policy_kv_dvoy';
        $t->save();

        /* 26 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'ДВОУ КВ';
        $t->column_key = 'payments.financial_policy_kv_dvoy_total';
        $t->as_key = 'payments_financial_policy_kv_dvoy_total';
        $t->save();

        /* 27 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'ДВОУ Отчет';
        $t->column_key = 'reports_dvoy.title';
        $t->as_key = 'reports_dvoy_title';
        $t->save();

        /* 28 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Дата заключения договора';
        $t->column_key = 'contracts.sign_date';
        $t->as_key = 'contracts_sign_date';
        $t->save();

        /* 29 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Дата начала договора';
        $t->column_key = 'contracts.begin_date';
        $t->as_key = 'contracts_begin_date';
        $t->save();

        /* 30 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Дата окончания договора';
        $t->column_key = 'contracts.end_date';
        $t->as_key = 'contracts_end_date';
        $t->save();

        /* 31 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Маркер';
        $t->column_key = 'payments.marker_text';
        $t->as_key = 'payments_marker_text';
        $t->save();

        /* 32 */
        $t = new \App\Models\Account\TableColumn();
        $t->is_as = 1;
        $t->sorting = 1;
        $t->is_summary = 0;
        $t->table_key = 'reports_sk';

        $t->column_name = 'Тип акцепта';
        $t->column_key = 'contracts.kind_acceptance';
        $t->as_key = 'contracts_kind_acceptance';
        $t->save();





    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
