<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncomesDocumentsTable extends Migration {

    CONST TABLE_NAME = 'incomes_expenses_documents';

    public function up() {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('file_id');
            $table->integer('income_id');
        });
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->index(['income_id']);
        });
    }

    public function down() {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropIndex(['income_id']);
        });
        Schema::drop(self::TABLE_NAME);
    }

}
