<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TemplateCategoryUnderwriting extends Migration{

    public function up(){

        $template_cat_cashbox = \App\Models\Settings\TemplateCategory::get('cashbox');

        \App\Models\Settings\TemplateCategory::create([
            'parent_id' => $template_cat_cashbox->id,
            'code' => 'bso_to_underwriting',
            'title' => 'Акт передачи БСО в Андеррайтинг',
            'has_choise' => 0,
            'has_supplier' => 0,
            'is_actual' => 1,
        ]);
    }


    public function down(){
        \App\Models\Settings\TemplateCategory::query()->where('code', 'bso_to_underwriting')->delete();
    }
}
