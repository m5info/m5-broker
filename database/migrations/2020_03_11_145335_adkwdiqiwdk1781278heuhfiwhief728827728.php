<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Adkwdiqiwdk1781278heuhfiwhief728827728 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('foundation_payments', function(Blueprint $table){
            $table->decimal('payment_report_total', 11, 2)->nullable();
            $table->decimal('payment_incomes_total', 11, 2)->nullable();
            $table->decimal('payment_expenses_total', 11, 2)->nullable();
            $table->decimal('payment_profit_total', 11, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
