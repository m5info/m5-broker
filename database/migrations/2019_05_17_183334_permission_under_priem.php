<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PermissionUnderPriem extends Migration{

    public function up(){
        $gr_contr = \App\Models\Users\PermissionGroup::query()->where('title', 'contracts')->first();

        \App\Models\Users\Permission::create([
            'title' => 'reception_contracts',
            'group_id' => $gr_contr->id,
            'not_visible' => 0,
            'sort' => 10,
        ]);
    }

    public function down(){
        \App\Models\Users\Permission::query()->where('title', 'reception_contracts')->delete();
    }
}
