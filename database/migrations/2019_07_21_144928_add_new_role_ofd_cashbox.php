<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewRoleOfdCashbox extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\Users\Permission::create([
            'title' => 'ofd_cashbox',
            'group_id' => 8,
            'not_visible' => 0,
            'sort' => 7,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Users\Permission::query()->where('title', 'ofd_cashbox')->delete();
    }
}
