<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewPermGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pg = \App\Models\Users\PermissionGroup::create([
            'title' => 'orders',
            'sort_view' => 3,
            'is_visibility' => 1
        ]);

        $inspections = \App\Models\Users\Permission::create([
            'title' => 'inspections',
            'group_id' => $pg->id,
            'not_visible' => 0,
            'sort' => 1,
        ]);


        $change = \App\Models\Users\Permission::where('title','=', 'contract_orders')->first();
        $change->group_id = $pg->id;
        $change->sort = 2;
        $change->save();

        \App\Models\Users\Subpermission::create([
            'permission_id' => $inspections->id,
            'title' => 'temp_orders',
        ]);

        \App\Models\Users\Subpermission::create([
            'permission_id' => $inspections->id,
            'title' => 'in_work_orders',
        ]);

        \App\Models\Users\Subpermission::create([
            'permission_id' => $inspections->id,
            'title' => 'in_check_orders',
        ]);

        \App\Models\Users\Subpermission::create([
            'permission_id' => $inspections->id,
            'title' => 'agree_orders',
        ]);

        \App\Models\Users\Subpermission::create([
            'permission_id' => $inspections->id,
            'title' => 'archive_orders',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
