<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Kwdk1812j1d81j29d81j2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $cat = \App\Models\Settings\TemplateCategory::query()->where('code', 'income_expense')->first();

        $new = new \App\Models\Settings\TemplateCategory();
        $new->parent_id = $cat->id;
        $new->title = 'Доп доходы / Расходы (Приход)';
        $new->code = 'income_expense_comming';
        $new->has_choise = 0;
        $new->has_supplier = 0;
        $new->is_actual = 1;
        $new->has_org = 0;
        $new->save();

        $new = new \App\Models\Settings\TemplateCategory();
        $new->parent_id = $cat->id;
        $new->code = 'income_expense_spending';
        $new->title = 'Доп доходы / Расходы (Расход)';
        $new->has_choise = 0;
        $new->has_supplier = 0;
        $new->is_actual = 1;
        $new->has_org = 0;
        $new->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
