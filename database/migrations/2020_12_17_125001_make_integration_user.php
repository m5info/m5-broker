<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeIntegrationUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        App\Models\User::create([
            'name' => 'Integration',
            'email' => 'integration@admin.com',
            'password' => bcrypt('integration@admin.com'),
            'subject_type_id' => 1,
            'role_id' => 1,
            'subject_id' => 164,
            'department_id' => 6,
            'filial_id' => 0,
            'settings' => ['menu' => '','menu_section' =>''],
            'signer_fio' => '',
            'region' => '',
            'city_id' => 1,
            'is_work' => 0,
            'notification_news' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
