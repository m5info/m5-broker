<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $perm_group = \App\Models\Users\PermissionGroup::query()->where('title', 'news')->first();

        $news = \App\Models\Users\Permission::query()->where('title', 'add_news')->first();
        $news->title = 'edit_news';
        $news->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
