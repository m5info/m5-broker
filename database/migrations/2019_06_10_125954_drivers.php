<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Drivers extends Migration{

    public function up(){

        Schema::create('drivers', function(Blueprint $table){
            $table->increments('id');
            $table->integer('contract_id')->default(0);
            $table->char('fio')->default('');
            $table->integer('sex')->default(0);
            $table->char('doc_num')->default('');
            $table->date('birth_date');
            $table->date('doc_date');
            $table->date('exp_date');
        });

    }

    public function down(){

        Schema::dropIfExists('drivers');

    }
}
