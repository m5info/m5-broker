<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OperationList extends Migration{

    public function up(){

        $gr = \App\Models\Users\PermissionGroup::query()
            ->where('title', 'cashbox')
            ->first();

        \App\Models\Users\Permission::create([
            'group_id' => $gr->id,
            'title' => 'operation_list',
            'not_visible' => '0',
            'sort' => 6,
        ]);

    }


    public function down(){

        $gr = \App\Models\Users\PermissionGroup::query()->where('title', 'cashbox')->first();

        \App\Models\Users\Permission::query()
            ->where('group_id', $gr->id)
            ->where('title', 'operation_list')
            ->delete();
    }
}
