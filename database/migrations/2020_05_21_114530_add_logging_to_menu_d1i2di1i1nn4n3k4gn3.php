<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoggingToMenuD1i2di1i1nn4n3k4gn3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pg = \App\Models\Users\PermissionGroup::query()->where('title', '=', 'settings')->first();
        $pg->sort_view = 10;
        $pg->save();

        $pg = new \App\Models\Users\PermissionGroup();
        $pg->title = 'logging';
        $pg->sort_view = 9;
        $pg->is_visibility = 1;
        $pg->save();

        $p = new \App\Models\Users\Permission();
        $p->title = 'cud_logs';
        $p->group_id = $pg->id;
        $p->not_visible = 0;
        $p->sort = 0;
        $p->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
