<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPermissionAvinfobot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permission_group = \App\Models\Users\PermissionGroup::where('title', '=', 'directories')->first();

        $new_permission = new \App\Models\Users\Permission();
        $new_permission->title = 'avinfobot';
        $new_permission->group_id = $permission_group->id;
        $new_permission->not_visible = 0;
        $new_permission->sort = 7;
        $new_permission->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
