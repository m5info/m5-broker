<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Akdwoiadkwoidkawk12312di1ioj extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vsk_api_online', function (Blueprint $table){
            $table->decimal('temp_amount', 11, 2)->nullable();
            $table->decimal('agent_kv', 11, 2)->nullable();
        });

        Schema::dropIfExists('api_logs');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
