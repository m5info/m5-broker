<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BsoMenuPerm extends Migration{

    public function up(){
        \App\Models\Users\Permission::create([
            'title' => 'bso_cashbox',
            'group_id' => 8,
            'not_visible' => 0,
            'sort' => 1,
        ]);
    }


    public function down(){
        \App\Models\Users\Permission::query()->where('group_id', 8)->where('title', 'БСО в кассе')->delete();
    }
}
