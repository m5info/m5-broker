<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsOnlineForProducts extends Migration {

    public function up() {
        Schema::table('products', function ($table) {
            $table->tinyInteger('is_online')->nullable();
            $table->string('description')->nullable();
        });
    }

    public function down() {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['is_online']);
            $table->dropColumn(['description']);
        });
    }

}
