<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Files extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('name');
            $table->string('ext');
            $table->string('original_name');
            $table->string('folder');
            $table->string('host');
            $table->timestamps();

        });

        Schema::table('files', function(Blueprint $table){
            //$table->foreign('user_id', 'fk_files_user1')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('files', function (Blueprint $table) {

        });
    }
}
