<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToInspectionOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspection_orders', function (Blueprint $table) {
            $table->integer('status_id')->nullable();
            $table->integer('declined_user_id')->nullable();
        });

        Schema::rename('inspectoin_orders_logs', 'inspection_orders_logs');

        Schema::table('inspection_orders_logs', function (Blueprint $table) {
            $table->string('status_title')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspection_orders', function (Blueprint $table) {
            //
        });
    }
}
