<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubjectsUl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects_ul', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('subject_id')->nullable();
            $table->string('fio')->nullable();
            $table->string('position')->nullable();
            $table->date('birthdate')->nullable();

            $table->string('address_register')->nullable();
            $table->string('address_register_kladr')->nullable();
            $table->string('address_fact')->nullable();
            $table->string('address_fact_kladr')->nullable();

            $table->string('title')->nullable();
            $table->string('inn')->nullable();
            $table->string('kpp')->nullable();
            $table->string('bik')->nullable();
            $table->string('general_manager')->nullable();
            $table->string('ogrn')->nullable();
            $table->string('doc_serie')->nullable();
            $table->string('doc_number')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
