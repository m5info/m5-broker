<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsTosubjectsFlUl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects_fl', function(Blueprint $table){
            $table->string('last_name')->after('fio')->nullable();
            $table->string('second_name')->after('fio')->nullable();
            $table->string('first_name')->after('fio')->nullable();
        });

        Schema::table('subjects_ul', function(Blueprint $table){
            $table->string('last_name')->after('fio')->nullable();
            $table->string('second_name')->after('fio')->nullable();
            $table->string('first_name')->after('fio')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
