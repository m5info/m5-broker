<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add2ColumnToTableColumnsDfeiojwofijweoifjwoeij extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tb = new \App\Models\Account\TableColumn();
        $tb->column_name = "Эквайринг";
        $tb->table_key = "analitics_common";
        $tb->column_key = "payments.acquire_total";
        $tb->is_as = 1;
        $tb->as_key = "payments_acquire_total";
        $tb->sorting = 1;
        $tb->is_summary = 0;
        $tb->save();

        $tb = new \App\Models\Account\TableColumn();
        $tb->column_name = "Эквайринг(%)";
        $tb->table_key = "analitics_common";
        $tb->column_key = "payments.acquire_percent";
        $tb->is_as = 1;
        $tb->as_key = "payments_acquire_percent";
        $tb->sorting = 1;
        $tb->is_summary = 0;
        $tb->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
