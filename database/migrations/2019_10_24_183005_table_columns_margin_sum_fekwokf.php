<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableColumnsMarginSumFekwokf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $column = \App\Models\Account\TableColumn::query()->where('column_name', 'Маржа (Сумма)')->first();
        $column->column_key = '(payments.financial_policy_kv_bordereau_total+payments.financial_policy_kv_dvoy_total)-(IF (contracts.sales_condition = 1, 0, payments.financial_policy_kv_agent_total)+(payments.payment_total/100)*(financial_policies.partnership_reward_1+financial_policies.partnership_reward_2+financial_policies.partnership_reward_3))';
        $column->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
