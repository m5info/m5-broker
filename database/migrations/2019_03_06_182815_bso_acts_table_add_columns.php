<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BsoActsTableAddColumns extends Migration
{

    public function up()
    {
        Schema::table('bso_acts', function(Blueprint $table){
            $table->addColumn('integer', 'bso_supplier_id');
            $table->addColumn('integer', 'report_year');
            $table->addColumn('integer', 'report_month');
            $table->addColumn('date', 'contract_date_start');
            $table->addColumn('date', 'contract_date_end');
        });
    }


    public function down()
    {
        Schema::table('bso_acts', function(Blueprint $table){
            $table->dropColumn('bso_supplier_id');
            $table->dropColumn('report_year');
            $table->dropColumn('report_month');
            $table->dropColumn('contract_date_start');
            $table->dropColumn('contract_date_end');
        });
    }
}
