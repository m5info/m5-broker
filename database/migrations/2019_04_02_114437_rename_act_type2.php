<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameActType2 extends Migration
{
    public function up(){
        \App\Models\BSO\BsoCartType::query()->find(0)->update(['title' => 'Передача от СК на точку продаж']);
    }


    public function down(){
        \App\Models\BSO\BsoCartType::query()->find(0)->update(['title' => 'Передача на точку продаж']);

    }
}
