<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContractToFixedSubpermissions extends Migration{

    public function up(){

        DB::insert("INSERT INTO m5_back.subpermissions (permission_id, title) VALUES (31, 'in_close_correction')");
        DB::insert("INSERT INTO m5_back.subpermissions (permission_id, title) VALUES (33, 'in_close_correction')");
    }

    public function down(){
        DB::delete("DELETE FROM m5_back.subpermissions WHERE permission_id in (31,33) and title='in_close_correction'");
    }
}
