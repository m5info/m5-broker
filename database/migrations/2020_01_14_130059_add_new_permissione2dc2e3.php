<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPermissione2dc2e3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $group = \App\Models\Users\PermissionGroup::where('sort_view','=', 99)->first();

        \App\Models\Users\Permission::create([
            'title' => 'is_pso_ride',
            'group_id' => $group->id,
            'not_visible' => 0,
            'sort' => 12,
        ]);

        \App\Models\Users\Permission::create([
            'title' => 'is_dtp_ride',
            'group_id' => $group->id,
            'not_visible' => 0,
            'sort' => 13,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
