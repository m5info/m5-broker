<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ekjqwikdqqkwd1893dj9283j2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $col = \App\Models\Account\TableColumn::query()->where('column_name', 'Маржа')->first();
        $col->column_key = '(payments.financial_policy_kv_bordereau_total+payments.financial_policy_kv_dvoy_total)-(IF (contracts.sales_condition = 1, 0, payments.financial_policy_kv_agent_total)+(payments.payment_total/100)*( ifnull(financial_policies.partnership_reward_1, 0) + ifnull(financial_policies.partnership_reward_2, 0)+ ifnull(financial_policies.partnership_reward_3, 0)))';
        $col->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
