<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTitleDocCompany extends Migration{

    public function up(){
        \App\Models\Organizations\Organization::findOrFail(1)->update(['title_doc' => 'ООО "ЕВРОГАРАНТ"']);
    }


    public function down(){
        \App\Models\Organizations\Organization::findOrFail(1)->update(['title_doc' => 'ООО "ЕВРОГАРАНТ - СТРАХОВАНИЕ"']);
    }
}
