<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Doakwdopiakwopidkwoijdqoiwdoiqjwd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tb = \App\Models\Account\TableColumn::query()->where('column_key','payments.acquire_percent')->first();
        $tb->column_name = "Эквайринг (%)";
        $tb->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
