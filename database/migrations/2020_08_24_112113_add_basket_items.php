<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBasketItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $p = new \App\Models\Users\Permission();
        $p->title = 'basket_items';
        $p->group_id = \App\Models\Users\PermissionGroup::where('title', 'analitics')->first()->id;
        $p->not_visible = 0;
        $p->sort = 10;
        $p->type = 0;
        $p->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
