<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddparamContractsTerms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts_terms', function(Blueprint $table)
        {

            $table->integer('damage_id')->nullable()->default(0);
            $table->integer('hijacking_id')->nullable()->default(0);
            $table->integer('is_undocumented_settlement_competent_authorities')->nullable()->default(0);
            $table->integer('is_emergency_commissioner_select')->nullable()->default(0);
            $table->integer('is_loss_commodity_value')->nullable()->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
