<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermExpectedPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $perm_group = \App\Models\Users\PermissionGroup::query()->where('title', 'finance')->first();

        \App\Models\Users\Permission::create([
            'title' => 'expected_payments',
            'group_id' => $perm_group->id,
            'not_visible' => 0,
            'sort' => 0
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
