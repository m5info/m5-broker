<?php

use Illuminate\Database\Migrations\Migration;

class PerenosAccept extends Migration{

    public function up(){
        $group = \App\Models\Users\PermissionGroup::query()->where('title', 'analitics')->first();
        \App\Models\Users\Permission::query()->where('title', 'accepted')->update([
            'group_id' => $group->id
        ]);
    }

    public function down(){
        $group = \App\Models\Users\PermissionGroup::query()->where('title', 'contracts')->first();
        \App\Models\Users\Permission::query()->where('title', 'accepted')->update([
            'group_id' => $group->id
        ]);
    }
}
