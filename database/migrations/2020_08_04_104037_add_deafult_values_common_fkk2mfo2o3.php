<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeafultValuesCommonFkk2mfo2o3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function(Blueprint $table){
            $table->decimal('official_discount_total', 11, 2)->default(0)->change();
            $table->decimal('informal_discount_total', 11, 2)->default(0)->change();
            $table->decimal('bank_kv_total', 11, 2)->default(0)->change();
            $table->decimal('payment_total', 11, 2)->default(0)->change();
            $table->decimal('financial_policy_kv_bordereau_total', 11, 2)->default(0)->change();
            $table->decimal('financial_policy_kv_dvoy_total', 11, 2)->default(0)->change();
            $table->decimal('financial_policy_kv_agent_total', 11, 2)->default(0)->change();
            $table->decimal('financial_policy_kv_parent_total', 11, 2)->default(0)->change();
            $table->decimal('invoice_payment_total', 11, 2)->default(0)->change();
            $table->decimal('acquire_total', 11, 2)->default(0)->change();
            $table->decimal('financial_policy_marjing_total', 11, 2)->default(0)->change();
        });

        Schema::table('contracts', function(Blueprint $table){
            $table->decimal('payment_total', 11, 2)->default(0)->change();
        });

        Schema::table('financial_policies', function(Blueprint $table){
            $table->decimal('fix_price_sum', 11, 2)->default(0)->change();
        });

        Schema::table('invoices', function(Blueprint $table){
            $table->decimal('invoice_payment_total', 11, 2)->default(0)->change();
        });

        Schema::table('cashbox_transactions', function(Blueprint $table){
            $table->decimal('total_sum', 11, 2)->default(0)->change();
            $table->decimal('residue', 11, 2)->default(0)->change();
        });

        Schema::table('users_balance', function(Blueprint $table){
            $table->decimal('balance', 11, 2)->default(0)->change();
        });

        Schema::table('users_balance_transactions', function(Blueprint $table){
            $table->decimal('total_sum', 11, 2)->default(0)->change();
            $table->decimal('residue', 11, 2)->default(0)->change();
        });

        DB::statement('update payments set payment_total = 0 where payment_total is null');
        DB::statement('update payments set official_discount_total = 0 where official_discount_total is null');
        DB::statement('update payments set informal_discount_total = 0 where informal_discount_total is null');
        DB::statement('update payments set bank_kv_total = 0 where bank_kv_total is null');
        DB::statement('update payments set financial_policy_kv_bordereau_total = 0 where financial_policy_kv_bordereau_total is null');
        DB::statement('update payments set financial_policy_kv_dvoy_total = 0 where financial_policy_kv_dvoy_total is null');
        DB::statement('update payments set financial_policy_kv_agent_total = 0 where financial_policy_kv_agent_total is null');
        DB::statement('update payments set financial_policy_kv_parent_total = 0 where financial_policy_kv_parent_total is null');
        DB::statement('update payments set invoice_payment_total = 0 where invoice_payment_total is null');
        DB::statement('update payments set acquire_total = 0 where acquire_total is null');
        DB::statement('update payments set financial_policy_marjing_total = 0 where financial_policy_marjing_total is null');

        DB::statement('update contracts set payment_total = 0 where payment_total is null');

        DB::statement('update financial_policies set fix_price_sum = 0 where fix_price_sum is null');

        DB::statement('update invoices set invoice_payment_total = 0 where invoice_payment_total is null');

        DB::statement('update cashbox_transactions set total_sum = 0 where total_sum is null');
        DB::statement('update cashbox_transactions set residue = 0 where residue is null');

        DB::statement('update users_balance set balance = 0 where balance is null');

        DB::statement('update users_balance_transactions set total_sum = 0 where total_sum is null');
        DB::statement('update users_balance_transactions set residue = 0 where residue is null');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
