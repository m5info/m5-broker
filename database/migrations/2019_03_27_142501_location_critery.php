<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LocationCritery extends Migration
{

    public function up()
    {
        Schema::table('bso_locations', function(Blueprint $table){
            $table->addColumn('integer', 'can_be_set_manually')->default(0);
        });

        \App\Models\BSO\BsoLocations::query()->whereIn('id', [0,1,2,4])->update([
             'can_be_set_manually' => 1
        ]);

    }


    public function down()
    {
        Schema::table('bso_locations', function(Blueprint $table){
            $table->dropColumn('can_be_set_manually');
        });
    }
}
