<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DriversFlds extends Migration{

    public function up(){
        Schema::table('drivers', function(Blueprint $table){
            $table->addColumn('float', 'kbm')->default(1);
            $table->addColumn('integer', 'same_as_insurer')->default(0);
        });
    }


    public function down(){
        Schema::table('drivers', function(Blueprint $table){
            $table->dropColumn('kbm');
            $table->dropColumn('same_as_insurer');
        });

    }
}
