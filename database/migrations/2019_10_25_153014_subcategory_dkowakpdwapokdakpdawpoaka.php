<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubcategoryDkowakpdwapokdakpdawpoaka extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $c = \App\Models\Settings\TemplateCategory::query()->where('code', 'user_balance')->first();

        $sub_1 = new \App\Models\Settings\TemplateCategory();
        $sub_1->parent_id = $c->id;
        $sub_1->code = 'user_balance_spending';
        $sub_1->title = 'Балансы | Транзакция | Расход';

        $sub_1->has_choise = 0;
        $sub_1->has_supplier = 0;
        $sub_1->is_actual = 1;
        $sub_1->has_org = 0;

        $sub_1->save();

        $sub_2 = new \App\Models\Settings\TemplateCategory();
        $sub_2->parent_id = $c->id;
        $sub_2->code = 'user_balance_comming';
        $sub_1->title = 'Балансы | Транзакция | Приход';

        $sub_2->has_choise = 0;
        $sub_2->has_supplier = 0;
        $sub_2->is_actual = 1;
        $sub_2->has_org = 0;

        $sub_2->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
