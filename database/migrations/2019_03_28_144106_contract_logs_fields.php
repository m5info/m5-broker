<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContractLogsFields extends Migration
{

    public function up(){
        Schema::table('contracts_logs', function(Blueprint $table){
            $table->addColumn('integer', 'user_id');
        });

    }


    public function down(){
        Schema::table('contracts_logs', function(Blueprint $table){
            $table->dropColumn('user_id');
        });

    }
}
