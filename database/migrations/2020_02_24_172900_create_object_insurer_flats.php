<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectInsurerFlats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('object_insurer_flats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('object_insurer_id')->default(0)->nullable();

            $table->string('address')->nullable();
            $table->string('address_kladr')->nullable();
            $table->string('address_region')->nullable();
            $table->string('address_city')->nullable();
            $table->string('address_city_kladr_id')->nullable();
            $table->string('address_street')->nullable();
            $table->string('address_house')->nullable();
            $table->string('address_block')->nullable();
            $table->string('address_flat')->nullable();

            $table->string('address_latitude')->nullable();
            $table->string('address_longitude')->nullable();

            $table->integer('house_floor')->nullable();
            $table->integer('flat_floor')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('object_insurer_flats');
    }
}
