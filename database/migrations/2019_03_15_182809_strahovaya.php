<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Strahovaya extends Migration
{

    public function up()
    {
        \App\Models\Settings\TemplateType::query()->where('title', 'Страховая')->update([
            'title' => 'Страховая компания'
        ]);

    }


    public function down()
    {
        \App\Models\Settings\TemplateType::query()->where('title', 'Страховая компания')->update([
            'title' => 'Страховая'
        ]);
    }
}
