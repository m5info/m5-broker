<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Users\PermissionGroup;

class AddRoleToPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $model = PermissionGroup::query()->where('title', 'role_owns')->first();
        $model_ = new \App\Models\Users\Permission();
        $model_->title = 'is_manager';
        $model_->group_id = $model->id;
        $model_->not_visible = 0;
        $model_->sort = 6;
        $model_->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
