<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAtol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('atol_check', function (Blueprint $table) {
            $table->string('check_uuid', '255')->default(null);
            $table->text('data_info')->default(null);
        });

        Schema::table('atol_error', function (Blueprint $table) {
            $table->integer('atol_id')->default(null);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
