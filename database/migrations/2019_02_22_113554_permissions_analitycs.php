<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PermissionsAnalitycs extends Migration
{

    public function up()
    {
        $group = new \App\Models\Users\PermissionGroup();
        $group->title = 'analitics';
        $group->sort_view = 6;
        $group->is_visibility = 1;
        $group->save();


        $permission = new \App\Models\Users\Permission();
        $permission->title = 'analitics_common';
        $permission->group_id = $group->id;
        $permission->not_visible = 0;
        $permission->sort = 0;
        $permission->save();

    }


    public function down()
    {
        \App\Models\Users\Permission::query()->where('title', '=', 'analitics_common')->delete();
        \App\Models\Users\PermissionGroup::query()->where('title', '=', 'analitics')->delete();
    }
}
