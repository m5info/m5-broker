<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActToPay extends Migration{

    public function up(){
        DB::insert("INSERT INTO 
            m5_back.template_categories 
            (parent_id, code, title, has_choise, has_supplier, is_actual) 
            VALUES 
            (21, 'act_for_pay_cashless_card_broker', 'Акт на оплату безнал (Карта Брокера)', 0, 0, 1)"
        );
    }


    public function down(){
        DB::delete("delete from m5_back.template_categories where parent_id=21 and code in ('act_for_pay_cashless_card_broker')");

    }
}
