<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVehicleModelsSk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_models_sk', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('insurance_companies_id')->nullable();
            $table->integer('bso_supplier_id')->nullable();
            $table->integer('vehicle_category_id')->nullable();
            $table->integer('vehicle_marks_id')->nullable();
            $table->integer('vehicle_models_id')->nullable();

            $table->string('vehicle_category_sk_id')->nullable();
            $table->string('vehicle_marks_sk_id')->nullable();
            $table->string('vehicle_models_sk_id')->nullable();
            $table->string('sk_title')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
