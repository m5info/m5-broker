<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOnlineCalculationTable extends Migration {

    public function up() {
        Schema::create('online_calculations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contract_id');
            $table->string('sum');
            $table->text('messages');
            $table->text('json');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('online_calculations');
    }

}
