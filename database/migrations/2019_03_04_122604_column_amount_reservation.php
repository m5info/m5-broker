<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ColumnAmountReservation extends Migration
{

    public function up()
    {
        Schema::table('reservations', function(Blueprint $table){
            $table->decimal('amount', 11, 2);
        });
    }

    public function down()
    {
        Schema::table('reservations', function(Blueprint $table){
            $table->dropColumn('amount');
        });
    }
}
