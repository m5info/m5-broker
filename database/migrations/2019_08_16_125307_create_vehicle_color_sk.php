<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleColorSk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_color_sk', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('insurance_companies_id')->nullable();
            $table->integer('bso_supplier_id')->nullable();
            $table->integer('color_id')->nullable();
            $table->string('color_sk_id')->nullable();
            $table->string('sk_title')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_color_sk');
    }
}
