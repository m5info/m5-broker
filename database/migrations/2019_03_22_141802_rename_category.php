<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\Settings\TemplateCategory::query()->find(6)->update(['code' => 'system_bso_to_sk']);
        \App\Models\Settings\TemplateCategory::query()->find(13)->update(['code' => 'sk_bso_to_sk']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Settings\TemplateCategory::query()->find(6)->update(['code' => 'bso_to_sk']);
        \App\Models\Settings\TemplateCategory::query()->find(13)->update(['code' => 'bso_to_sk']);
    }
}
