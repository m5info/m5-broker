<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PayMethod extends Migration{

    public function up(){
        Schema::table('payments', function(Blueprint $table){
            $table->addColumn('integer', 'pay_method')->default(0);
        });
    }

    public function down(){
        Schema::table('payments', function(Blueprint $table){
            $table->dropColumn('pay_method');
        });
    }
}
