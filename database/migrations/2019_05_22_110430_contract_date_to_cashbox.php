<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContractDateToCashbox extends Migration{

    public function up(){
        Schema::table('contracts', function (Blueprint $table) {
            $table->addColumn('datetime','to_cashbox_date')->nullable();
        });
    }

    public function down(){
        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn('to_cashbox_date');
        });
    }
}
