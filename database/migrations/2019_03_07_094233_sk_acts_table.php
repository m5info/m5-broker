<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SkActsTable extends Migration{

    public function up(){

        (new BsoActsTableAddColumns())->down();

        Schema::create('reports_act', function(Blueprint $table){

            $table->increments('id');

            $table->char('title');
            $table->char('signatory_org');
            $table->char('signatory_sk_bso_supplier');

            $table->integer('bso_supplier_id')->default(0);
            $table->integer( 'report_year')->default(0);
            $table->integer( 'report_month')->default(0);

            $table->integer( 'type_id')->default(0); //0 по бсо 1 по договорам
            $table->integer( 'accept_status')->default(0); //0 не акцептован 1 акцептован
            $table->integer( 'is_deleted')->default(0);

            $table->integer( 'accept_user_id')->default(0);
            $table->integer( 'create_user_id')->default(0);

            $table->date( 'accepted_at');
            $table->date('report_date_start');
            $table->date('report_date_end');

            $table->timestamps();

        });

    }


    public function down(){

        (new BsoActsTableAddColumns())->up();
        Schema::dropIfExists('reports_act');
    }
}
