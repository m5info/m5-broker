<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegionSubject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects_fl', function (Blueprint $table) {
            $table->string('address_register_region')->after('doc_info');
            $table->string('address_fact_region')->after('address_register_flat');
        });
        Schema::table('subjects_ul', function (Blueprint $table) {
            $table->string('address_register_region')->after('doc_number');
            $table->string('address_fact_region')->after('address_register_flat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
