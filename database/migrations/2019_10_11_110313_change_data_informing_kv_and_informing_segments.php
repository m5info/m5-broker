<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDataInformingKvAndInformingSegments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('informing_segments', function(Blueprint $table){
            $table->dropColumn('actual_from');

        });

        Schema::table('informing_segments', function(Blueprint $table){
            $table->date('actual_from')->useCurrent()->nullable();
        });

        Schema::table('informing_kv', function(Blueprint $table){
            $table->dropColumn('actual_from');

        });

        Schema::table('informing_kv', function(Blueprint $table){
            $table->date('actual_from')->useCurrent()->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
