<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     *
    'name',
    'phone',
    'need_diagnostics_card',
    'car_brand',
    'car_model',
    'car_region',
    'user_ip',
    'user_source',
    'user_os',
    'user_browser',
    'device_type',
    'device_brand',
    'device_model',
    'object_gosnum',
    'object_data'
     *
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners_leads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->tinyInteger('need_diagnostics_card')->nullable()->default(0);
            $table->string('car_brand')->nullable()->default(null);
            $table->string('car_model')->nullable()->default(null);
            $table->string('car_region')->nullable()->default(null);
            $table->string('user_ip')->nullable()->default(null);
            $table->string('user_source')->nullable()->default(null);
            $table->string('user_os')->nullable()->default(null);
            $table->string('user_browser')->nullable()->default(null);
            $table->string('device_type')->nullable()->default(null);
            $table->string('device_brand')->nullable()->default(null);
            $table->string('device_model')->nullable()->default(null);
            $table->text('object_gosnum')->nullable()->default(null);
            $table->text('object_data')->nullable()->default(null);
            $table->integer('contract_id')->nullable()->default(null);
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners_leads');
    }
}
