<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SegmentRedesign extends Migration{

    public function up(){

        Schema::table('financial_policies_segments', function(Blueprint $table){

            $table->dropColumn('insurer_type_id');
            $table->dropColumn('location_id');
            $table->dropColumn('period');
            $table->dropColumn('contract_type_id');
            $table->dropColumn('vehicle_country_id');
            $table->dropColumn('vehicle_power_from');
            $table->dropColumn('vehicle_power_to');
            $table->dropColumn('vehicle_age');
            $table->dropColumn('has_trailer');
            $table->dropColumn('is_multi_drive');
            $table->dropColumn('drivers_min_age');
            $table->dropColumn('drivers_min_exp');
            $table->dropColumn('owner_age');
            $table->dropColumn('vehicle_category_id');
            $table->dropColumn('period_any');
            $table->dropColumn('contract_type_any');
            $table->dropColumn('vehicle_power_any');
            $table->dropColumn('vehicle_age_any');
            $table->dropColumn('has_trailer_any');
            $table->dropColumn('is_multi_drive_any');
            $table->dropColumn('drivers_age_any');
            $table->dropColumn('owner_age_any');
            $table->dropColumn('drivers_exp_any');
            $table->dropColumn('insurer_type_any');
            $table->dropColumn('vehicle_country_any');
            $table->dropColumn('insurer_location_any');
            $table->dropColumn('kbm');
            $table->dropColumn('kbm_any');
            $table->dropColumn('insurer_kt_any');
            $table->dropColumn('insurer_kt');
            $table->addColumn('integer', 'product_id')->default(0);
            $table->addColumn('text', 'data');

        });

    }


    public function down(){
        Schema::table('financial_policies_segments', function(Blueprint $table){
            $table->addColumn('integer', 'insurer_type_id')->default(0);
            $table->addColumn('integer', 'location_id')->default(0);
            $table->addColumn('integer', 'period')->default(0);
            $table->addColumn('integer', 'contract_type_id')->default(0);
            $table->addColumn('integer', 'vehicle_country_id')->default(0);
            $table->addColumn('decimal', 'vehicle_power_from', ['total' => 11, 'places' => 2])->default(0);
            $table->addColumn('decimal', 'vehicle_power_to', ['total' => 11, 'places' => 2])->default(0);
            $table->addColumn('integer', 'vehicle_age')->default(0);
            $table->addColumn('integer', 'has_trailer')->default(0);
            $table->addColumn('integer', 'is_multi_drive')->default(0);
            $table->addColumn('integer', 'drivers_min_age')->default(0);
            $table->addColumn('integer', 'drivers_min_exp')->default(0);
            $table->addColumn('integer', 'owner_age')->default(0);
            $table->addColumn('integer', 'vehicle_category_id')->default(0);
            $table->addColumn('integer', 'period_any')->default(0);
            $table->addColumn('integer', 'contract_type_any')->default(0);
            $table->addColumn('integer', 'vehicle_power_any')->default(0);
            $table->addColumn('integer', 'vehicle_age_any')->default(0);
            $table->addColumn('integer', 'has_trailer_any')->default(0);
            $table->addColumn('integer', 'is_multi_drive_any')->default(0);
            $table->addColumn('integer', 'drivers_age_any')->default(0);
            $table->addColumn('integer', 'owner_age_any')->default(0);
            $table->addColumn('integer', 'drivers_exp_any')->default(0);
            $table->addColumn('integer', 'insurer_type_any')->default(0);
            $table->addColumn('integer', 'vehicle_country_any')->default(0);
            $table->addColumn('integer', 'insurer_location_any')->default(0);
            $table->addColumn('decimal', 'kbm', ['total' => 11, 'places' => 2])->default(0);
            $table->addColumn('integer', 'kbm_any')->default(0);
            $table->addColumn('integer', 'insurer_kt_any')->default(0);
            $table->addColumn('decimal', 'insurer_kt', ['total' => 11, 'places' => 2])->default(0);
            $table->dropColumn('product_id');
            $table->dropColumn('data');
        });
    }
}
