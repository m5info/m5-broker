<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewSubpermissionForOrderse2vfedve extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $inspections = \App\Models\Users\Permission::where('title','=', 'inspections')->first();


        \App\Models\Users\Subpermission::create([
            'permission_id' => $inspections->id,
            'title' => 'in_distribution',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
