<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dkwpodjqiojdqoijwdioqjwodijq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tb = \App\Models\Account\TableColumn::query()->where('column_name', "Маржа")->first();
        $tb->column_key="IF (contracts.sales_condition = 1, ((CASE WHEN payments.type_id = 0 THEN (payments.financial_policy_kv_bordereau_total+payments.financial_policy_kv_dvoy_total)-(IF (contracts.sales_condition = 1, 0, payments.financial_policy_kv_agent_total)+(payments.payment_total/100)*( ifnull(financial_policies.partnership_reward_1, 0) + ifnull(financial_policies.partnership_reward_2, 0)+ ifnull(financial_policies.partnership_reward_3, 0))) WHEN payments.type_id = 2 THEN CAST('-'+payments.payment_total as DECIMAL(9,2)) ELSE payments.payment_total END) - (payments.informal_discount_total + payments.acquire_total)), (CASE WHEN payments.type_id = 0 THEN (payments.financial_policy_kv_bordereau_total+payments.financial_policy_kv_dvoy_total)-(IF (contracts.sales_condition = 1, 0, payments.financial_policy_kv_agent_total)+(payments.payment_total/100)*( ifnull(financial_policies.partnership_reward_1, 0) + ifnull(financial_policies.partnership_reward_2, 0)+ ifnull(financial_policies.partnership_reward_3, 0))) WHEN payments.type_id = 2 THEN CAST('-'+payments.payment_total as DECIMAL(9,2)) ELSE payments.payment_total END))";
        $tb->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
