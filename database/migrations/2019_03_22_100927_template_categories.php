<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TemplateCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /*(1 , 0, 'system', 'Системные', 1, 0),
                    (2 , 1, 'system_acts', 'Акты', 1, 0),
                        (3 , 2, 'bso_from_agent', 'Акт приема БСО от агента', 0, 0),
                        (4 , 2, 'warehouse_to_agent', 'Акт передачи со склада агенту', 0, 0),
                        (5 , 2, 'agent_to_agent', 'Акт передачи от агента-агенту', 0, 0),
                        (6 , 2, 'bso_to_sk', 'Акт передачи БСО в СК', 0, 0),
                        (7 , 2, 'bso_to_tp', 'Акт передачи БСО на точку продаж', 0, 0),
                        (8 , 2, 'bso_to_courier', 'Акт передачи БСО курьеру', 0, 0),
                        (9 , 2, 'damaged_bso_from_agent', 'Акт передачи испорченного БСО от агента', 0, 0),
                        (10, 2, 'bso_to_region', 'Акт передачи БСО на Регион', 0, 0),
                        (11, 2, 'bso_from_tp_to_tp', 'Акт передачи БСО от ТП на ТП', 0, 0),
                (12, 0, 'sk', 'Страховая Компания', 1, 0),
                    (13, 12, 'sk_acts', 'Акты', 1, 0),
                        (14, 13, 'bso_to_sk', 'Акт передачи БСО в СК', 1, 1),
                    (15, 12, 'sk_reports', 'Отчёты', 1, 0),
                        (16, 15, 'dvou_to_sk', 'Отчет ДВОУ в СК', 1, 1),
                        (17, 15, 'borderau_to_sk', 'Отчет Бордеро в СК', 1, 1)
                        */
        DB::insert("insert into template_categories values 
                    (18, 1, 'system_contracts', 'Договоры', 0, 0),
                    (19, 18, 'contract_agent', 'Договор агента', 0, 0),
                    (20, 13, 'payments_to_sk', 'Акт передачи договоров в СК', 1, 1),
                    (21, 1, 'system_invoices', 'Счета', 0, 0),
                    (22, 21, 'invoice', 'Счёт', 0, 0),
                    (23, 21, 'invoice_act', 'Акт по счёту', 0, 0),
                    (24, 21, 'invoice_reservation', 'Резервный счёт', 0, 0) 
                    
                    ");
    }


    public function down()
    {
        DB::insert("delete from template_categories where id in(18,19, 20, 21, 22, 23, 24)");
    }
}
