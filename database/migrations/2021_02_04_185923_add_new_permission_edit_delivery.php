<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPermissionEditDelivery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pg = \App\Models\Users\PermissionGroup::where('title', '=', 'directories')->first();

        $new_role = new \App\Models\Users\Permission();
        $new_role->title = 'edit_delivery';
        $new_role->group_id = $pg->id;
        $new_role->not_visible = 0;
        $new_role->sort = 6;
        $new_role->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
