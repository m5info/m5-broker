<?php

use App\Models\Partners\IntegrationPartnersTokens;
use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterIntegrationTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('integration_partners_tokens', function(Blueprint $table)
        {
            $table->renameColumn('partner_id', 'user_id');
        });
        $user = User::where('name', 'Integration')->firstOrFail();
        $token = IntegrationPartnersTokens::where('id', 1)->firstOrFail();

        $token->user_id = $user->id;
        $token->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
