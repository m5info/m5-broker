<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Woeifwoi27fiuehwf782hfiueh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vsk_api_online', function (Blueprint $table) {
            $table->string('step')->nullable();
            $table->string('id_correlation')->nullable();
            $table->string('id_message')->nullable();
            $table->string('id_session')->nullable();
            $table->string('id_bp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
