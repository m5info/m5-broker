<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExportItemx extends Migration
{

    public function up()
    {
        $export_item = new \App\Models\Settings\ExportItem();
        $export_item->title = 'Выгрузка счёта в редактировании счёта';
        $export_item->code = 'invoice_export';
        $export_item->save();

        $export_item2 = new \App\Models\Settings\ExportItem();
        $export_item2->title = 'Выгрузка акта в редактировании счёта';
        $export_item2->code = 'invoice_act_export';
        $export_item2->save();
    }


    public function down()
    {
        \App\Models\Settings\ExportItem::query()->findOrFail(['code', '=', 'invoice_export'])->delete();
        \App\Models\Settings\ExportItem::query()->findOrFail(['code', '=', 'invoice_act_export'])->delete();
    }
}
