<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContractChat extends Migration{

    public function up(){

        Schema::create('contract_messages', function(Blueprint $table){

            $table->increments('id');
            $table->timestamps();
            $table->text('message');
            $table->integer('type_id');
            $table->integer('user_id');
            $table->integer('contract_id');
        });


    }


    public function down(){


        Schema::dropIfExists('contract_messages');


    }
}
