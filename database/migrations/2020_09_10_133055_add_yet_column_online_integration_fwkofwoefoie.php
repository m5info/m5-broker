<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddYetColumnOnlineIntegrationFwkofwoefoie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects_fl', function(Blueprint $table){
           $table->string('address_fact_region_full')->default('');
           $table->string('address_register_region_full')->default('');
        });

        Schema::table('subjects_ul', function(Blueprint $table){
            $table->string('address_fact_region_full')->default('');
            $table->string('address_register_region_full')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
