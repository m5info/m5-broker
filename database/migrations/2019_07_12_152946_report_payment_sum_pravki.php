<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReportPaymentSumPravki extends Migration{

    public function up(){
        Schema::dropIfExists('report_payment_sum');
        Schema::create('report_payment_sum', function(Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->integer('type_id')->default(0);
            $table->decimal('amount', 11, 2)->nullable()->default(0.00);
            $table->integer('user_id');
            $table->integer('report_id');
        });
    }


    public function down(){
        Schema::dropIfExists('report_payment_sum');
        Schema::create('report_payment_sum', function(Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->decimal('income', 11, 2)->nullable()->default(0.00);
            $table->decimal('expense', 11, 2)->nullable()->default(0.00);
            $table->integer('user_id');
            $table->integer('report_id');
        });
    }
}
