<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleMapper extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_mapper', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('partner_name')->nullable()->default(null);
            $table->string('local_name')->nullable()->default(null);
            $table->boolean('is_brand')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_mapper');
    }
}
