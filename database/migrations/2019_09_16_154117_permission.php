<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Permission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $perm_group = \App\Models\Users\PermissionGroup::query()->where('title', 'news')->first();

        \App\Models\Users\Permission::create([
            'title' => 'news',
            'group_id' => $perm_group->id,
            'not_visible' => 0,
            'sort' => 0
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
