<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_info', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('user_id')->default(0)->nullable();
            $table->integer('product_id')->default(0)->nullable();
            $table->integer('type_id')->default(0)->nullable();
            $table->integer('sort')->default(0)->nullable();

            $table->string('title')->nullable();
            $table->text('info_text')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_info');
    }
}
