<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewCategoryTemplateKco23m2kfm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tc_f = \App\Models\Settings\TemplateCategory::query()
            ->where('code', '=', 'system_contracts')->first();

        $tc = new \App\Models\Settings\TemplateCategory();
        $tc->parent_id = $tc_f->id;
        $tc->code = 'contracts_act_of_disagreement';
        $tc->title = 'Акт разногласий';
        $tc->has_choise = 0;
        $tc->has_supplier = 0;
        $tc->is_actual = 1;
        $tc->has_org = 0;
        $tc->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
