<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CharToString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drivers', function (Blueprint $table) {
            $table->string('fio')->change();
            $table->string('doc_num')->change();
        });

        Schema::table('expenses', function (Blueprint $table) {
            $table->string('title')->change();
        });

        Schema::table('incomes', function (Blueprint $table) {
            $table->string('title')->change();
        });

        Schema::table('api_settings', function (Blueprint $table) {
            $table->string('dir_name')->change();
            $table->string('battle_server_login')->change();
            $table->string('battle_server_password')->change();
            $table->string('test_server_login')->change();
            $table->string('test_server_password')->change();
        });

        Schema::table('atol_check', function (Blueprint $table) {
            $table->string('check_ext_id')->change();
            $table->string('check_status')->change();
            $table->string('check_product')->change();
            $table->string('check_client_email')->change();
            $table->string('check_client_phone')->change();
            $table->string('stat')->change();
            $table->string('token')->change();
        });

        Schema::table('atol_token', function (Blueprint $table) {
            $table->string('token')->change();
        });

        Schema::table('pay_methods', function (Blueprint $table) {
            $table->string('title')->change();
        });

        Schema::table('reports_act', function (Blueprint $table) {
            $table->string('title')->change();
            $table->string('signatory_org')->change();
        });

        Schema::table('reservations', function (Blueprint $table) {
            $table->string('inn')->change();
            $table->string('kpp')->change();
            $table->string('payer')->change();
        });

        Schema::table('subpermissions', function (Blueprint $table) {
            $table->string('title')->change();
        });

        Schema::table('template_categories', function (Blueprint $table) {
            $table->string('code')->change();
            $table->string('title')->change();
        });

    }


    public function down(){

    }
}
