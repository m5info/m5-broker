<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubpermissionsOrderStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $perm = \App\Models\Users\Permission::where('title', '=', 'order_statuses')->get()->first();

        $perm->type = 1;
        $perm->save();

        \App\Models\Users\Subpermission::create([
            'permission_id' => $perm->id,
            'title' => 'temp',
        ]);

        \App\Models\Users\Subpermission::create([
            'permission_id' => $perm->id,
            'title' => 'in_frame',
        ]);

        \App\Models\Users\Subpermission::create([
            'permission_id' => $perm->id,
            'title' => 'in_print',
        ]);

        \App\Models\Users\Subpermission::create([
            'permission_id' => $perm->id,
            'title' => 'in_delivery',
        ]);

        \App\Models\Users\Subpermission::create([
            'permission_id' => $perm->id,
            'title' => 'in_realized',
        ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
