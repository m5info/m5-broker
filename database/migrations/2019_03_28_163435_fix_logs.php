<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixLogs extends Migration {

    public function up() {
        Schema::table('contracts_logs', function (Blueprint $table) {
            $table->integer('status_id')->nullable()->change();
            $table->integer('contract_id')->nullable()->change();
        });
    }

    public function down() {
        Schema::table('contracts_logs', function (Blueprint $table) {
            $table->integer('status_id')->change();
            $table->integer('contract_id')->change();
        });
    }

}
