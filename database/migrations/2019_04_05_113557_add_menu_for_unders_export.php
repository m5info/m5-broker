<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Users\Permission;

class AddMenuForUndersExport extends Migration {

    CONST NAME = 'contracts_techunder';
    CONST GROUP_ID = 6;
    CONST ID = 65;

    public function up() {
        $permission = new Permission;
        $permission->id = self::ID;
        $permission->title = self::NAME;
        $permission->group_id = 6;
        $permission->not_visible = 0;
        $permission->sort = 0;
        $permission->save();

        DB::statement('INSERT INTO roles_permissions (role_id, permission_id) VALUES (1, ' . self::ID . ');');
    }

    public function down() {
        Permission::where('title', '=', self::NAME)->get()->each->delete();
        DB::statement('DELETE FROM roles_permissions WHERE permission_id = ' . self::ID . ';');
    }

}
