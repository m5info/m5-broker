<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RolesPermissionsGroup extends Migration
{

    public function up()
    {
        $group = \App\Models\Users\PermissionGroup::create([
            'title' => 'role_owns',
            'sort_view' => 99,
            'is_visibility' => 0,
        ]);

        \App\Models\Users\Permission::create([
            'title' => 'is_underwriter',
            'group_id' => $group->id,
            'not_visible' => 0,
            'sort' => 1,
        ]);

        \App\Models\Users\Permission::create([
            'title' => 'is_agent',
            'group_id' => $group->id,
            'not_visible' => 0,
            'sort' => 2,
        ]);

        \App\Models\Users\Permission::create([
            'title' => 'is_cashier',
            'group_id' => $group->id,
            'not_visible' => 0,
            'sort' => 3,
        ]);

    }


    public function down()
    {
        \App\Models\Users\Permission::query()->where('title', 'is_underwriter')->delete();
        \App\Models\Users\Permission::query()->where('title', 'is_agent')->delete();
        \App\Models\Users\Permission::query()->where('title', 'is_cashier')->delete();
        \App\Models\Users\PermissionGroup::query()->where('title', 'role_owns')->delete();
    }
}
