<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnderfillersCalculations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('underfillers_calculations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contract_id');
            $table->integer('product_id');
            $table->text('json_calculate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('underfillers_calculations');
    }
}
