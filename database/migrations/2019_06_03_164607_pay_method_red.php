<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PayMethodRed extends Migration{

    public function up(){

        Schema::create('pay_methods', function(Blueprint $table){
            $table->increments('id');
            $table->char('title');
            $table->integer('is_actual');
        });

        Schema::create('bso_suppliers_products_pay_methods', function(Blueprint $table){
           $table->increments('id');
           $table->integer('bso_supplier_id');
           $table->integer('product_id');
           $table->integer('pay_method_id');
        });


        \App\Models\Finance\PayMethod::create([
            'id' => 1,
            'title' => 'Квитанция',
            'is_actual' => 1,
        ]);

        \App\Models\Finance\PayMethod::create([
            'id' => 2,
            'title' => 'Чек Брокер',
            'is_actual' => 0,
        ]);

        \App\Models\Finance\PayMethod::create([
            'id' => 3,
            'title' => 'Чек СК',
            'is_actual' => 0,
        ]);

        \App\Models\Finance\PayMethod::create([
            'id' => 100,
            'title' => 'Без подтверждения',
            'is_actual' => 1,
        ]);

    }


    public function down(){

        Schema::dropIfExists('pay_methods');
        Schema::dropIfExists('bso_suppliers_products_pay_methods');

    }
}
