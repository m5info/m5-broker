<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDictionarySkK extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $json = $this->getSks();

        $sks = \GuzzleHttp\json_decode($json);

        foreach($sks as $sk){
            $title = $sk->title;
            $inn = $sk->inn ?? '';

            \App\Models\Dictionaries\DictionarySk::create([
                'title' => $title,
                'inn' => $inn
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    public function getSks()
    {
        return '[
  {
    "title": "Акционерное общество \"НАСКО\"",
    "inn": "1657023630"
  },
  {
    "title": "Акционерное общество \"СТРАХОВАЯ КОМПАНИЯ \"ТРАНСНЕФТЬ\"",
    "inn": "7724026253"
  },
  {
    "title": "Акционерное общество \"цюрих\"",
    "inn": "7809016423"
  },
  {
    "title": "Акционерное общество \"ЭНЕРГОГАРАНТ\"",
    "inn": "7710045520"
  },
  {
    "title": "Амурский филиал ООО Страховая компания \"ГЕЛИОС РЕЗЕРВ\"\"",
    "inn": "7705513090"
  },
  {
    "title": "АО \"Страховое общество ЖАСО\"",
    "inn": "7708023079"
  },
  {
    "title": "ВСК",
    "inn": "7710026574"
  },
  {
    "title": "Закрытое акционерное общество \"Защита-Страхование\"",
    "inn": "7704012319"
  },
  {
    "title": "Закрытое акционерное общество \"Московская акционерная страховая компания МАКС\"",
    "inn": "7709031643"
  },
  {
    "title": "Закрытое акционерное общество \"Московская акционерная страховая компания\"",
    "inn": "7709031643"
  },
  {
    "title": "Закрытое акционерное общество \"РЕСО\"",
    "inn": "7708004365"
  },
  {
    "title": "Закрытое акционерное общество \"СОАО \"ВСК\"\"",
    "inn": "7710069828"
  },
  {
    "title": "Закрытое акционерное общество \"УРАЛСИБ\"",
    "inn": "7708020007"
  },
  {
    "title": "ЗАО \"Акционерная страховая компания \"Доверие\"",
    "inn": "7808036145"
  },
  {
    "title": "ЗАО \"АКЦИОНЕРНОЕ СТРАХОВОЕ ОБЩЕСТВО\"",
    "inn": "7744002420"
  },
  {
    "title": "ЗАО \"Железнодорожное Акционерное Страховое Общество -Магистраль\"",
    "inn": "4205001820"
  },
  {
    "title": "ЗАО \"Международная страховая компания \"АЙНИ\"",
    "inn": "3906006430"
  },
  {
    "title": "ЗАО \"Межрегиональное Страховое Соглашение\"",
    "inn": "7709436946"
  },
  {
    "title": "ЗАО \"Поволжский Страховой Альянс\"",
    "inn": "6325028433"
  },
  {
    "title": "ЗАО \"Страховая Акционерная Компания \"Информстрах\"",
    "inn": "7701017213"
  },
  {
    "title": "ЗАО \"Страховая бизнес группа\"",
    "inn": "3666068423"
  },
  {
    "title": "ЗАО \"Страховая группа \"Спасские ворота\"",
    "inn": "7704135977"
  },
  {
    "title": "ЗАО \"Страховая компания \"АСК-Петербург\"",
    "inn": "7825072753"
  },
  {
    "title": "ЗАО \"Страховая компания \"Двадцать первый век\"",
    "inn": "7817021522"
  },
  {
    "title": "ЗАО \"Страховая компания \"Мегарусс-Д\"",
    "inn": "7728115553"
  },
  {
    "title": "ЗАО \"Страховая компания \"Мед-Гарант\"",
    "inn": "5001015792"
  },
  {
    "title": "ЗАО \"Страховая компания \"НИКА\"",
    "inn": "7736050998"
  },
  {
    "title": "ЗАО \"Страховая компания \"Подмосковье\"",
    "inn": "5036011870"
  },
  {
    "title": "ЗАО \"Страховая компания \"РК-гарант\"",
    "inn": "7712056849"
  },
  {
    "title": "ЗАО \"Страховая Компания \"Русские страховые традиции\"",
    "inn": "7710022178"
  },
  {
    "title": "ЗАО \"Страховая компания \"Самара-АСКО\"",
    "inn": "6312013969"
  },
  {
    "title": "ЗАО \"Страховая компания \"СВОД\"",
    "inn": "7810656629"
  },
  {
    "title": "ЗАО \"Страховая компания \"Сибирский спас\"",
    "inn": "5402155821"
  },
  {
    "title": "ЗАО \"Страховая компания \"Чулпан\"",
    "inn": "1644001196"
  },
  {
    "title": "ЗАО \"Страховая Компания AИГ Лайф\"",
    "inn": "7730058711"
  },
  {
    "title": "ЗАО \"Страховая компания правоохранительных органов - УралСиб\"",
    "inn": "7703104976"
  },
  {
    "title": "ЗАО \"Страховое акционерное общество \"ГЕФЕСТ\"",
    "inn": "7713101131"
  },
  {
    "title": "ЗАО \"Страховое акционерное общество \"МЕТРОПОЛИС\"",
    "inn": "7726045389"
  },
  {
    "title": "ЗАО \"Страховое общество \"ЛК-Сити\"",
    "inn": "7729063322"
  },
  {
    "title": "ЗАО \"Страховое Общество \"Прогресс-Нева\"",
    "inn": "7825007514"
  },
  {
    "title": "ЗАО \"Страховое общество правоохранительных органов Республики Татарстан\"",
    "inn": "1654038910"
  },
  {
    "title": "ЗАО \"Транспортно-промышленное страховое общество\"",
    "inn": "7710253619"
  },
  {
    "title": "ЗАО \"Чартис\"",
    "inn": "7710541631"
  },
  {
    "title": "ЗАО «Страховая группа «УралСиб»",
    "inn": "7703032986"
  },
  {
    "title": "ЗАО акционерная страховая компания \"АСТЭК\"",
    "inn": "8903002444"
  },
  {
    "title": "ЗАО страховая компания \"Ариадна\"",
    "inn": "7704012319"
  },
  {
    "title": "ЗАО Страховая Компания \"ЗапСибЖАСО\"",
    "inn": "5407197984"
  },
  {
    "title": "ЗАО Страховая компания \"Инвестиции и Финансы\"",
    "inn": "7718036380"
  },
  {
    "title": "ЗАО Страховая компания \"ФИДЕЛИТИ-РЕЗЕРВ\"",
    "inn": "6150011500"
  },
  {
    "title": "ЗАО Страховое общество \"АСОЛЬ\"",
    "inn": "6320009557"
  },
  {
    "title": "ЗАО Страховое общество \"КОНДА\"",
    "inn": "7816045827"
  },
  {
    "title": "ЗАО Страховое общество \"Медэкспресс\"",
    "inn": "7803025365"
  },
  {
    "title": "ЗАО Страховое общество \"Надежда\"",
    "inn": "2466035034"
  },
  {
    "title": "ЗАО Страховое общество \"СТАНДАРТ-РЕЗЕРВ\"",
    "inn": "7706029830"
  },
  {
    "title": "ЗАСО Страховое общество \"Эрго Русь\"",
    "inn": "7815025049"
  },
  {
    "title": "ЗАСО Эрго Русь",
    "inn": "7815025049"
  },
  {
    "title": "ИНТАЧ СТРАХОВАНИЕ",
    "inn": "6315212497"
  },
  {
    "title": "КИТ Финанс Страхование (Открытое акционерное общество)",
    "inn": "7812016906"
  },
  {
    "title": "ОАО \"Акционерная страховая компания \"Инвестстрах-Агро\" (\"Агрис\")",
    "inn": "7710077804"
  },
  {
    "title": "ОАО \"Акционерное страховое общество \"ЛИДЕР\"",
    "inn": "7702020071"
  },
  {
    "title": "ОАО \"АльфаСтрахование\"",
    "inn": "7713056834"
  },
  {
    "title": "ОАО \"Британское страховое общество\"",
    "inn": "7714034590"
  },
  {
    "title": "ОАО \"Генеральная Страховая Компания\"",
    "inn": "7809000261"
  },
  {
    "title": "ОАО \"КапиталЪ Страхование\"",
    "inn": "7702045615"
  },
  {
    "title": "ОАО \"МЕЖОТРАСЛЕВОЙ СТРАХОВОЙ ЦЕНТР\"",
    "inn": "7733013853"
  },
  {
    "title": "ОАО \"Муниципальная страховая компания \"Страж\" им. С.Живаго\"",
    "inn": "6231037337"
  },
  {
    "title": "ОАО \"Российская государственная страховая компания\"",
    "inn": "7707067683"
  },
  {
    "title": "ОАО \"Российская национальная страховая компания\"",
    "inn": "5407123252"
  },
  {
    "title": "ОАО \"Российское страховое народное общество \"РОСНО\"",
    "inn": "7702073683"
  },
  {
    "title": "ОАО \"Русская Страховая Компания\"",
    "inn": "7710046242"
  },
  {
    "title": "ОАО \"Русское Акционерное Страховое Общество \"РАСО\"",
    "inn": "7717008535"
  },
  {
    "title": "ОАО \"СОГАЗ\"",
    "inn": "7736035485"
  },
  {
    "title": "ОАО \"Страховая акционерная компания \"ЭНЕРГОГАРАНТ\"",
    "inn": "7705041231"
  },
  {
    "title": "ОАО \"Страховая Акционерная Компания \"Энергополис\"",
    "inn": "6315212497"
  },
  {
    "title": "ОАО \"Страховая компания \"ГАРМЕД\"",
    "inn": "7731029625"
  },
  {
    "title": "ОАО \"Страховая компания \"Инкасстрах\"",
    "inn": "6311009276"
  },
  {
    "title": "ОАО \"Страховая компания \"Итиль\"",
    "inn": "1656000493"
  },
  {
    "title": "ОАО \"Страховая компания \"КЛАСС\"",
    "inn": "7812016906"
  },
  {
    "title": "ОАО \"Страховая Компания \"Липецк\"",
    "inn": "4822000473"
  },
  {
    "title": "ОАО \"Страховая компания \"ПАРИ\"",
    "inn": "7704041020"
  },
  {
    "title": "ОАО \"Страховая Компания \"ПОЛИС-ГАРАНТ\"",
    "inn": "7736203789"
  },
  {
    "title": "ОАО \"Страховая компания \"РЕГИОНГАРАНТ\"",
    "inn": "7702005813"
  },
  {
    "title": "ОАО \"Страховая компания \"Самара\"",
    "inn": "6315232133"
  },
  {
    "title": "ОАО \"Страховая компания \"СКМ\"",
    "inn": "7446008779"
  },
  {
    "title": "ОАО \"СТРАХОВАЯ КОМПАНИЯ \"СОЮЗ\"",
    "inn": "6163012444"
  },
  {
    "title": "ОАО \"Страховая компания \"Трансгарант\"",
    "inn": "7706025433"
  },
  {
    "title": "ОАО \"Страховая компания \"ТРАСТ\"",
    "inn": "7447010322"
  },
  {
    "title": "ОАО \"Страховая Компания \"ТЭСТ-ЖАСО\"",
    "inn": "7604036010"
  },
  {
    "title": "ОАО \"Страховая компания \"Урал-Американ Интерконтинентал Лайф Иншуренс Компани\"",
    "inn": "5905013823"
  },
  {
    "title": "ОАО \"Страховая компания \"ЭНИ\"",
    "inn": "6163010542"
  },
  {
    "title": "ОАО \"Страховая фирма \"АСОПО\"",
    "inn": "5410102319"
  },
  {
    "title": "ОАО \"Страховое общество \"Талисман\"",
    "inn": "1655004449"
  },
  {
    "title": "ОАО \"Страховое общество Содружества Независимых Государств\"",
    "inn": "7804002674"
  },
  {
    "title": "ОАО \"Уралсиб\"",
    "inn": "0274062111"
  },
  {
    "title": "ОАО \"Чувашская страховая транспортная компания\"",
    "inn": "2128010172"
  },
  {
    "title": "ОАО \"ЭНЕРГЕТИЧЕСКАЯ СТРАХОВАЯ КОМПАНИЯ\"",
    "inn": "7705000796"
  },
  {
    "title": "ОАО Железнодорожная страховая компания \"ЖАСКО\"",
    "inn": "3525013446"
  },
  {
    "title": "ОАО Страховая группа \"Межрегионгарант\"",
    "inn": "8901010104"
  },
  {
    "title": "ОАО Страховая компания \"БАСК\"",
    "inn": "4202000716"
  },
  {
    "title": "ОАО страховая компания \"Гранит\"",
    "inn": "7702005845"
  },
  {
    "title": "ОАО страховая компания \"Русский мир\"",
    "inn": "7803014532"
  },
  {
    "title": "ОАО страховая компания\" Царица\"",
    "inn": "3444035272"
  },
  {
    "title": "ОАО Страховое общество \"Авиационный Фонд Единый Страховой (АФЕС)\"",
    "inn": "7714016489"
  },
  {
    "title": "ОАО страховое общество \"АСтрО-Волга\"",
    "inn": "6320005464"
  },
  {
    "title": "ОАО Страховое общество \"Жива\"",
    "inn": "7729094271"
  },
  {
    "title": "ОАО Страховое общество \"Защита-Находка\"",
    "inn": "2508038914"
  },
  {
    "title": "ОАО Страховое общество \"Национальная Страховая Группа\"",
    "inn": "5008018432"
  },
  {
    "title": "ОАО Страховое общество \"Регион\"",
    "inn": "4705005100"
  },
  {
    "title": "ОАО Страховое Общество \"Страховая Группа \"Региональный Альянс\"",
    "inn": "3525068276"
  },
  {
    "title": "ОАО Страховое общество \"Экспресс Гарант\"",
    "inn": "6608004240"
  },
  {
    "title": "ОАО Страховое общество \"ЯКОРЬ\"",
    "inn": "7731041830"
  },
  {
    "title": "Общество с ограниченной ответственностью \"\"МАЛАХИТ\"\"",
    "inn": "1655211244"
  },
  {
    "title": "Общество с ограниченной ответственностью \"\"Страховая компания \"ЦЮРИХ\"\"",
    "inn": "7710280644"
  },
  {
    "title": "Общество с ограниченной ответственностью \"альфа страхование\"",
    "inn": "7710045520"
  },
  {
    "title": "Общество с ограниченной ответственностью \"БИН Страхование\"",
    "inn": "7717115093"
  },
  {
    "title": "Общество с ограниченной ответственностью \"ООО \"Регард страхование\"\"",
    "inn": "7715350990"
  },
  {
    "title": "Общество с ограниченной ответственностью \"ЭСКО\"",
    "inn": "7705000796"
  },
  {
    "title": "Общество с ограниченной ответственностью \"ЮЖУРАЛ-АСКО\"",
    "inn": "7710026574"
  },
  {
    "title": "Объединенная страховая компания",
    "inn": "6312013969"
  },
  {
    "title": "ООО \"Генеральный Страховой Альянс\"",
    "inn": "5007009160"
  },
  {
    "title": "ООО \"Городская страховая компания\"",
    "inn": "5404329600"
  },
  {
    "title": "ООО \"Группа Ренессанс Страхование\"",
    "inn": "7724023076"
  },
  {
    "title": "ООО \"Губернская страховая компания Кузбасса\"",
    "inn": "4205002133"
  },
  {
    "title": "ООО \"Евро-Азиатская страховая компания\"",
    "inn": "6663066864"
  },
  {
    "title": "ООО \"Липецкое страховое общество \"Шанс\"",
    "inn": "4825002743"
  },
  {
    "title": "ООО \"НАРОДНАЯ СТРАХОВАЯ КОМПАНИЯ \"РЕКОН\"",
    "inn": "7715017735"
  },
  {
    "title": "ООО \"Национальная страховая группа-\"РОСЭНЕРГО\"",
    "inn": "0411063374"
  },
  {
    "title": "ООО \"Наша Надежда\"",
    "inn": "5257043319"
  },
  {
    "title": "ООО \"Промышленно-страховой альянс\"",
    "inn": "7709326051"
  },
  {
    "title": "ООО \"РОСГОССТРАХ\"",
    "inn": "7707067683"
  },
  {
    "title": "ООО \"Росгосстрах-Аккорд\"",
    "inn": "274089138"
  },
  {
    "title": "ООО \"Росгосстрах-Дальний Восток\"",
    "inn": "2536128461"
  },
  {
    "title": "ООО \"Росгосстрах-Поволжье\"",
    "inn": "5262111797"
  },
  {
    "title": "ООО \"Росгосстрах-Северо-Запад\"",
    "inn": "7813175514"
  },
  {
    "title": "ООО \"Росгосстрах-Сибирь\"",
    "inn": "5406240002"
  },
  {
    "title": "ООО \"Росгосстрах-Татарстан\"",
    "inn": "1655058194"
  },
  {
    "title": "ООО \"Росгосстрах-Урал\"",
    "inn": "7203128922"
  },
  {
    "title": "ООО \"Росгосстрах-Центр\"",
    "inn": "3302021186"
  },
  {
    "title": "ООО \"Росгосстрах-Юг\"",
    "inn": "2310077857"
  },
  {
    "title": "ООО \"Русско-Балтийское страховое общество\"",
    "inn": "7835003406"
  },
  {
    "title": "ООО \"Северо-западная страховая компания\"",
    "inn": "7802128671"
  },
  {
    "title": "ООО \"СК  \"Алроса\"",
    "inn": "7744000454"
  },
  {
    "title": "ООО \"Страховая  компания \"Урал-Рецепт\"",
    "inn": "6608007018"
  },
  {
    "title": "ООО \"Страховая группа \"Адмирал\"",
    "inn": "6167010416"
  },
  {
    "title": "ООО \"Страховая группа \"АСКО\"",
    "inn": "5009047436"
  },
  {
    "title": "ООО \"Страховая группа \"Корона\"",
    "inn": "7729146018"
  },
  {
    "title": "ООО \"Страховая Компания \"АВЕCТ\"",
    "inn": "5405171885"
  },
  {
    "title": "ООО \"Страховая компания \"Айболит\"",
    "inn": "6164008271"
  },
  {
    "title": "ООО \"Страховая компания \"Ангара\"",
    "inn": "3804002162"
  },
  {
    "title": "ООО \"Страховая компания \"Арбат\"",
    "inn": "7744002081"
  },
  {
    "title": "ООО \"Страховая компания \"Генстрахование\"",
    "inn": "7730121233"
  },
  {
    "title": "ООО \"Страховая компания \"ДЖЕНЕРАЛ РЕЗЕРВ\"",
    "inn": "7707243434"
  },
  {
    "title": "ООО \"Страховая компания \"ИННОГАРАНТ\"",
    "inn": "7720247413"
  },
  {
    "title": "ООО \"Страховая компания \"Метротон\"",
    "inn": "7707547591"
  },
  {
    "title": "ООО \"Страховая компания \"Московия\"",
    "inn": "5046005297"
  },
  {
    "title": "ООО \"Страховая компания \"Нефтеполис\"",
    "inn": "7705300359"
  },
  {
    "title": "ООО \"Страховая компания \"ОРАНТА\"",
    "inn": "7709254400"
  },
  {
    "title": "ООО \"СТРАХОВАЯ КОМПАНИЯ \"РАСК\"",
    "inn": "6164203522"
  },
  {
    "title": "ООО \"Страховая Компания \"РОСИНВЕСТ\"",
    "inn": "7707012236"
  },
  {
    "title": "ООО \"Страховая компания \"Север-кооп-полис\"",
    "inn": "7203002091"
  },
  {
    "title": "ООО \"Страховая компания \"Северная казна\"",
    "inn": "6608003165"
  },
  {
    "title": "ООО \"Страховая компания \"СЕРВИСРЕЗЕРВ-КОВРОВ\"",
    "inn": "3317000799"
  },
  {
    "title": "ООО \"Страховая Компания \"Согласие\"",
    "inn": "7706196090"
  },
  {
    "title": "ООО \"Страховая компания \"Универсальный Полис\"",
    "inn": "7744002966"
  },
  {
    "title": "ООО \"Страховая компания \"Уралкооп-Полис\"",
    "inn": "6608007233"
  },
  {
    "title": "ООО \"Страховая компания \"УРАЛРОС\"",
    "inn": "6608007071"
  },
  {
    "title": "ООО \"Страховая компания \"ФИНИСТ-МК\"",
    "inn": "7733022801"
  },
  {
    "title": "ООО \"Страховая компания \"ЮЖУРАЛ-АСКО\"",
    "inn": "7451099235"
  },
  {
    "title": "ООО \"Страховая компания Комфорт Гарант\"",
    "inn": "3907026253"
  },
  {
    "title": "ООО \"Страховая фирма \"Вест-Акрас\"",
    "inn": "3435023707"
  },
  {
    "title": "ООО \"Страховое общество \"Мир коммерческого расчета\"",
    "inn": "5047053617"
  },
  {
    "title": "ООО \"Страховое общество \"ПОДДЕРЖКА-ГАРАНТ\"",
    "inn": "7729103818"
  },
  {
    "title": "ООО \"Страховое общество \"Спортивное страхование\"",
    "inn": "5005030488"
  },
  {
    "title": "ООО \"Страховое общество \"Сургутнефтегаз\"",
    "inn": "8602103061"
  },
  {
    "title": "ООО \"Страховое общество Зенит\"",
    "inn": "7723177852"
  },
  {
    "title": "ООО \"СТРАХОВОЕ ОБЩЕСТВО ТРУБОПРОВОДНОГО ТРАНСПОРТА\"",
    "inn": "7709258588"
  },
  {
    "title": "ООО страховая компания \"Гамма\"",
    "inn": "6608005148"
  },
  {
    "title": "ООО страховая компания \"ДАЛЬАКФЕС\"",
    "inn": "2538010289"
  },
  {
    "title": "ООО Страховая компания \"Национальное качество\"",
    "inn": "7744002701"
  },
  {
    "title": "ООО страховая компания \"Пирамида\"",
    "inn": "2540122529"
  },
  {
    "title": "ООО Страховая компания \"СК \"ПРИРОДА\"",
    "inn": "7706114965"
  },
  {
    "title": "ООО Страховая компания \"ТИРУС\"",
    "inn": "6607001535"
  },
  {
    "title": "ООО Страховая компания \"ЦЮРИХ\"",
    "inn": "7707062854"
  },
  {
    "title": "ООО Страховое общество \"Геополис\"",
    "inn": "7711023801"
  },
  {
    "title": "ОСАО \"Ингосстрах\"",
    "inn": "7705042179"
  },
  {
    "title": "ОСАО \"РЕСО-ГАРАНТИЯ\"",
    "inn": "7710045520"
  },
  {
    "title": "Открытое акционерное общество \"\"Государственная страховая компания\"Югория\"\"",
    "inn": "8601012990"
  },
  {
    "title": "Открытое акционерное общество \"\"Страховая компания \"Прогресс-Гарант\"\"",
    "inn": "7729102596"
  },
  {
    "title": "Открытое акционерное общество \"\"Страховая Компания ЮГОРИЯ\"",
    "inn": "8601023568"
  },
  {
    "title": "Открытое акционерное общество \"аско\"",
    "inn": "7703032986"
  },
  {
    "title": "Открытое акционерное общество \"Боровицкое страховое общество\"",
    "inn": "7714034590"
  },
  {
    "title": "Открытое акционерное общество \"ЖАСО\"",
    "inn": "7704059892"
  },
  {
    "title": "Открытое акционерное общество \"Московская страховая компания\"",
    "inn": "7710026574"
  },
  {
    "title": "Открытое акционерное общество \"ОАО СК \"РОСНО\"\"",
    "inn": "7702073683"
  },
  {
    "title": "Открытое акционерное общество \"РОССТРАХ\"",
    "inn": "5407123252"
  },
  {
    "title": "Открытое акционерное общество \"Ростра\"",
    "inn": "7709810287"
  },
  {
    "title": "Открытое акционерное общество \"Русская страховая транспортная компания\"",
    "inn": "7717013599"
  },
  {
    "title": "Открытое акционерное общество \"Страховая группа МСК\"",
    "inn": "1655006421"
  },
  {
    "title": "Открытое акционерное общество СК \"АЛЬЯНС\"",
    "inn": "7702073683"
  },
  {
    "title": "Открытое страховое акционерное общество \"Россия\"\"",
    "inn": "7702075923"
  },
  {
    "title": "Публичное акционерное общество \"\"Страховая компания \"ГАЙДЕ\"\"",
    "inn": "7809016423"
  },
  {
    "title": "СОГЛАСИЕ",
    "inn": "7717711410"
  },
  {
    "title": "Страховое акционерное общество \"ВСК\"",
    "inn": "7710026574"
  },
  {
    "title": "Страховое открытое акционерное общество \"РСЦ\"",
    "inn": "7730042285"
  },
  {
    "title": "Страховое открытое акционерное общество «ВСК»",
    "inn": "7710026574"
  }
]';
    }
}
