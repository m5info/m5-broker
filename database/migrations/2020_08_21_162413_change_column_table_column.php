<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnTableColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tc = \App\Models\Account\TableColumn::find(29);
        $tc->column_key = 'IF (payments.is_deleted = 0, payments.payment_total, 0)';
        $tc->save();
        $tc2 = \App\Models\Account\TableColumn::find(40);
        $tc2->column_key = 'IF (payments.is_deleted = 0, payments.financial_policy_marjing_total, 0)';
        $tc2->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
