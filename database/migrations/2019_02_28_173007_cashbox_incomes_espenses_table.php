<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CashboxIncomesEspensesTable extends Migration
{

    public function up(){
        Schema::create('incomes_expenses', function(Blueprint $table){
           $table->increments('id');
           $table->integer('category_id');
           $table->integer('status_id');
           $table->integer('user_id');
           $table->dateTime('date');
           $table->decimal('sum', 11, 2)->nullable()->default(null);
           $table->decimal('commission', 11, 2)->nullable()->default(null);
           $table->decimal('total', 11, 2)->nullable()->default(null);
           $table->text('comment');
        });
    }

    public function down(){
        Schema::dropIfExists('incomes_expenses');
    }
}
