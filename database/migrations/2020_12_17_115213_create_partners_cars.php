<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners_cars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vin')->nullable()->default(null);
            $table->string('gosnum')->nullable()->default(null);
            $table->string('brand')->nullable()->default(null);
            $table->string('model')->nullable()->default(null);
            $table->integer('year')->nullable()->default(null);
            $table->float('power', 8, 2)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners_cars');
    }
}
