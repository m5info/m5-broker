<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Settings\ExportItem;

class AddNewExportItem extends Migration {

    public function up() {
        $exportItem = new ExportItem;
        $exportItem->id = 10;
        $exportItem->title = "Выгрузка отчета в СК";
        $exportItem->code = "repots_sk";
        $exportItem->save();
    }

    public function down() {
        ExportItem::where('code', '=', "repots_sk")->get()->each->delete();
    }

}
