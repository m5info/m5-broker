<?php


use Illuminate\Database\Migrations\Migration;

class AnaliticsPermission extends Migration
{

    public function up()
    {
        $pg = \App\Models\Users\PermissionGroup::query()->where('title', 'analitics')->firstOrFail();
        \App\Models\Users\Permission::create([
           'title' => 'analitics_total',
           'group_id' => $pg->id,
           'not_visible' => 0,
           'sort' => 0,
        ]);

    }


    public function down()
    {
        $pg = \App\Models\Users\PermissionGroup::query()->where('title', 'analitics')->firstOrFail();
        \App\Models\Users\Permission::query()->where('group_id', $pg->id)->where('title','analitics_total')->delete();

    }
}
