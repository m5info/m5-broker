<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermisionEditPaidIncomesExpensesJafjsdhkjf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        $new_role = new \App\Models\Users\Permission();
        $new_role->title = 'edit_paid_incomes_expenses';
        $new_role->group_id = 8;
        $new_role->not_visible = 0;
        $new_role->sort = 9;
        $new_role->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
