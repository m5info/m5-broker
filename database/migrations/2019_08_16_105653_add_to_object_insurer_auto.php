<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToObjectInsurerAuto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('object_insurer_auto', function (Blueprint $table) {
            $table->string('modification')->nullable();
            $table->string('body_engine')->nullable();
            $table->string('body_chassis')->nullable();
            $table->integer('type_engine')->nullable();
            $table->integer('kv')->nullable();
            $table->integer('car_year_exp')->nullable();
            $table->integer('doors')->nullable();
            $table->integer('keys')->nullable()->default(2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('object_insurer_auto', function (Blueprint $table) {
            //
        });
    }
}
