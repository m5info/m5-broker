<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspectionOrdersgpeoj3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspection_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->nullable();
            $table->integer('position_type_id')->nullable();
            $table->integer('client_event_type_id')->nullable();
            $table->string('address')->nullable();
            $table->string('geo_lon')->nullable();
            $table->string('geo_lat')->nullable();
            $table->integer('processing_org_id')->nullable();
            $table->integer('accept_org_id')->nullable();
            $table->integer('processing_user_id')->nullable();
            $table->integer('accept_user_id')->nullable();
            $table->decimal('financial_policy_kv_agent_total', 11, 2)->nullable();
            $table->integer('processing_act_id')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
