<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToPartnersCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partners_cars', function (Blueprint $table) {
            $table->integer('pts_region')->nullable()->default(null);
            $table->string('pts_serie')->nullable()->default(null);
            $table->integer('pts_num')->nullable()->default(null);
            $table->string('pts_date')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partners_cars', function (Blueprint $table) {
            //
        });
    }
}
