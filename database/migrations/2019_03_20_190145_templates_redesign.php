<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TemplatesRedesign extends Migration
{

    public function up(){
        Schema::table('templates', function(Blueprint $table){
            $table->renameColumn('export_item_id', 'category_id');
            $table->dropColumn('insurance_id');
            $table->dropColumn('type_id');
        });

        Schema::dropIfExists('export_items');
        Schema::dropIfExists('template_types');

        Schema::create('template_categories', function(Blueprint $table){
            $table->increments('id');
            $table->integer('parent_id');
            $table->char('code');
            $table->char('title');
            $table->integer('has_choise')->default(1);
            $table->integer('has_supplier')->default(0);
        });


        DB::insert(
            "insert into template_categories values 
                (1 , 0, 'system', 'Системные', 1, 0),
                    (2 , 1, 'system_acts', 'Акты', 1, 0),
                        (3 , 2, 'bso_from_agent', 'Акт приема БСО от агента', 0, 0),
                        (4 , 2, 'warehouse_to_agent', 'Акт передачи со склада агенту', 0, 0),
                        (5 , 2, 'agent_to_agent', 'Акт передачи от агента-агенту', 0, 0),
                        (6 , 2, 'bso_to_sk', 'Акт передачи БСО в СК', 0, 0),
                        (7 , 2, 'bso_to_tp', 'Акт передачи БСО на точку продаж', 0, 0),
                        (8 , 2, 'bso_to_courier', 'Акт передачи БСО курьеру', 0, 0),
                        (9 , 2, 'damaged_bso_from_agent', 'Акт передачи испорченного БСО от агента', 0, 0),
                        (10, 2, 'bso_to_region', 'Акт передачи БСО на Регион', 0, 0),
                        (11, 2, 'bso_from_tp_to_tp', 'Акт передачи БСО от ТП на ТП', 0, 0),
                (12, 0, 'sk', 'Страховая Компания', 1, 0),
                    (13, 12, 'sk_acts', 'Акты', 1, 0),
                        (14, 13, 'bso_to_sk', 'Акт передачи БСО в СК', 1, 1),
                    (15, 12, 'sk_reports', 'Отчёты', 1, 0),
                        (16, 15, 'dvou_to_sk', 'Отчет ДВОУ в СК', 1, 1),
                        (17, 15, 'borderau_to_sk', 'Отчет Бордеро в СК', 1, 1)"
        );

    }


    public function down()
    {
        Schema::table('templates', function(Blueprint $table){
            $table->renameColumn('category_id', 'export_item_id');
            $table->addColumn('integer', 'insurance_id');
            $table->addColumn('integer', 'type_id');
        });

        if(!Schema::hasTable('template_types')){
            Schema::create('template_types', function(Blueprint $table){
                $table->increments('id');
                $table->char('title');
            });
            DB::insert("insert into template_types values (1, 'Системный'), (2, 'Страховая')");
        }

        if(!Schema::hasTable('export_items')) {
            Schema::create('export_items', function (Blueprint $table) {
                $table->increments('id');
                $table->char('title');
                $table->char('code');
                $table->integer('type_id');
            });
            DB::insert(
        "insert into export_items values 
                (1,'Реализованные акты БСО','realized_bso','null'),
                (2,'Страница акта приёма передачи БСО','transfer_deed_bso','null'),
                (5,'Страница резервирования счёта','reservation_invoice','null'),
                (8,'Выгрузка счёта в редактировании счёта','invoice_export','null'),
                (9,'Выгрузка акта в редактировании счёта','invoice_act_export','null');"
            );

        }

        Schema::dropIfExists('template_categories');

    }
}
