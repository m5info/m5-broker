<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPermGroupAndPerm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $group = \App\Models\Users\PermissionGroup::where('sort_view','=', 99)->first();

        \App\Models\Users\Permission::create([
            'title' => 'is_partner',
            'group_id' => $group->id,
            'not_visible' => 0,
            'sort' => 10,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
