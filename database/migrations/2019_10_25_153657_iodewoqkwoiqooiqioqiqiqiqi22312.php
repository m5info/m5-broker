<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Iodewoqkwoiqooiqioqiqiqiqi22312 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $c = \App\Models\Settings\TemplateCategory::query()->where('code','user_balance_spending')->first();
        $c->title = 'Балансы (Расход по транзакции))';
        $c->save();

        $c = \App\Models\Settings\TemplateCategory::query()->where('code','user_balance_comming')->first();
        $c->title = 'Балансы (Приход по транзакции)';
        $c->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
