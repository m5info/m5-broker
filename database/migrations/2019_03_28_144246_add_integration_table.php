<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIntegrationTable extends Migration {

    public function up() {
        Schema::create('integrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->integer('active');
            $table->timestamps();
        });


        Schema::create('integrations_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('integration_id');
            $table->string('title');
            $table->string('description');
            $table->string('integration_class');
            $table->integer('active');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('integrations');
        Schema::drop('integrations_versions');
    }

}
