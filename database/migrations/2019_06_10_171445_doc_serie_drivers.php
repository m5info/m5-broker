<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DocSerieDrivers extends Migration{

    public function up(){
        Schema::table('drivers', function(Blueprint $table){
            $table->addColumn('integer','doc_serie')->default(0);
        });
    }


    public function down(){
        Schema::table('drivers', function(Blueprint $table){
            $table->dropColumn('doc_serie');
        });
    }
}
