<?php

use App\Models\Users\Permission;
use App\Models\Users\PermissionGroup;
use Illuminate\Database\Migrations\Migration;

class PernissNew extends Migration{

    public function up(){
        $group = PermissionGroup::query()->where('title', 'finance')->first();
        Permission::create([
            'title' => 'free_invoice_group',
            'group_id' => $group->id,
            'not_visible' => 0,
            'sort' => 3,
        ]);

        Permission::query()->where('title', 'bso_cashbox')->update([
            'title' => 'acts_to_underwriting'
        ]);

    }


    public function down(){
        Permission::query()->where('title', 'free_invoice_group')->delete();
        Permission::query()->where('title', 'acts_to_underwriting')->update([
            'title' => 'bso_cashbox'
        ]);
    }
}
