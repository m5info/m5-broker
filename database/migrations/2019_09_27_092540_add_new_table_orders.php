<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_id')->default(0);
            $table->integer('agent_id')->default(0);
            $table->integer('insurer_id')->default(0);
            $table->integer('object_insurer_auto_id')->default(0);
            $table->integer('is_delivery')->default(0);
            $table->string('address_width')->default('');
            $table->string('address_longitude')->default('');
            $table->string('address_kladr')->default('');
            $table->date('delivery_date')->default('0000-00-00');
            $table->text('products')->default('');
            $table->integer('proccesing_user_id')->default(0);
            $table->integer('printer_user_id')->default(0);
            $table->integer('delivery_user_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
