<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SkApiSettings extends Migration{

    public function up(){

        Schema::create('api_settings', function(Blueprint $table){
            $table->increments('id');
            $table->integer('bso_supplier_id')->default(0);
            $table->integer('is_actual')->default(0);
            $table->integer('work_type')->default(0);
            $table->char('dir_name')->default('');
            $table->text('battle_server_url')->default('');
            $table->char('battle_server_login')->default('');
            $table->char('battle_server_password')->default('');
            $table->text('test_server_url')->default('');
            $table->char('test_server_login')->default('');
            $table->char('test_server_password')->default('');
        });

    }


    public function down(){
        Schema::dropIfExists('api_settings');
    }
}
