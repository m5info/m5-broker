<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewLocationAdd extends Migration
{

    public function up()
    {
        DB::insert("insert into bso_locations (id, title, is_actual) values (?, ?, ?)" ,[14, 'Отвязка квитанции', 1]);
    }


    public function down()
    {
        DB::delete("delete from bso_locations where id = ?", [14]);
    }
}
