<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParamFnsFerma extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fns_ferma', function(Blueprint $table){

            $table->integer('org_id')->default(0);

            $table->decimal('payment_total')->nullable();
            $table->integer('payment_type')->default(0);

            $table->integer('type_id')->default(0);
            $table->integer('payment_method_id')->default(1);

            $table->integer('client_type_id')->default(0);
            $table->string('client_title');
            $table->string('client_inn');
            $table->string('client_doc_serie');
            $table->string('client_doc_number');

            $table->string('client_email');
            $table->string('client_phone');

            $table->string('product_title');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
