<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSystemDeployFeatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $group = App\Models\Users\PermissionGroup::query()->where('title', 'settings')->first();

        $obj = new App\Models\Users\Permission();
        $obj->title = "system_project_features";
        $obj->group_id = $group->id;
        $obj->not_visible = 0;
        $obj->sort = 0;
        $obj->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
