<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Atol extends Migration
{

    public function up(){

        Schema::create('atol_check', function(Blueprint $table){
            $table->increments('id');
            $table->dateTime('check_datetime');
            $table->decimal('check_sum', 11, 2);
            $table->char('check_ext_id');
            $table->char('check_status');
            $table->char('check_product');
            $table->char('check_client_email');
            $table->char('check_client_phone');
            $table->char('stat');
            $table->char('token');
            $table->timestamps();
        });

        Schema::create('atol_error', function(Blueprint $table){
            $table->increments('id');
            $table->text('text');
            $table->timestamps();
        });

        Schema::create('atol_token', function(Blueprint $table){
            $table->increments('id');
            $table->char('token');
            $table->timestamps();
        });
    }


    public function down(){
        Schema::dropIfExists('atol_check');
        Schema::dropIfExists('atol_error');
        Schema::dropIfExists('atol_token');
    }
}
