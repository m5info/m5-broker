<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColsReserv extends Migration
{

    public function up()
    {
        Schema::table('reservations', function(Blueprint $table){
            $table->removeColumn('inn');
            $table->removeColumn('kpp');
            $table->removeColumn('payer');
            $table->removeColumn('comment');
            $table->removeColumn('address');
        });
    }


    public function down()
    {
        Schema::table('reservations', function(Blueprint $table){
            $table->addColumn('char', 'inn');
            $table->addColumn('char', 'kpp');
            $table->addColumn('char', 'payer');
            $table->addColumn('text', 'comment');
            $table->addColumn('text', 'address');
        });

    }
}
