<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewRoleIsUnderfiller extends Migration
{
    public function up()
    {
        $group = \App\Models\Users\PermissionGroup::where('sort_view','=', 99)->first();

        \App\Models\Users\Permission::create([
            'title' => 'is_underfiller',
            'group_id' => $group->id,
            'not_visible' => 0,
            'sort' => 5,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Users\Permission::query()->where('title', 'is_underfiller')->delete();
    }

}
