<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFoundationPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('foundation_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('month');
            $table->integer('year');
            $table->date('accept_date');
            $table->integer('user_id');

            $table->decimal('fact_report_total', 11, 2)->nullable()->default(null);
            $table->decimal('fact_incomes_total', 11, 2)->nullable()->default(null);
            $table->decimal('fact_expenses_total', 11, 2)->nullable()->default(null);
            $table->decimal('fact_profit_total', 11, 2)->nullable()->default(null);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
