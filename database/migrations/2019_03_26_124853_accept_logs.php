<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AcceptLogs extends Migration
{

    public function up(){

        Schema::table('payments', function(Blueprint $table){
            $table->addColumn('integer', 'accept_user_id');
            $table->addColumn('date', 'accept_date');
        });

        Schema::create('accepts', function(Blueprint $table){
            $table->increments('id');
            $table->integer('contract_id');
            $table->integer('payment_id');
            $table->integer('accept_user_id');
            $table->integer('parent_user_id');
            $table->date('accept_date');
        });

        Schema::table('users', function(Blueprint $table){
            $table->addColumn('text', 'settings');
        });


    }


    public function down(){

        Schema::table('payments', function(Blueprint $table){
            $table->removeColumn('accept_user_id');
            $table->removeColumn('accept_date');
        });

        Schema::dropIfExists('accepts');

        Schema::table('users', function(Blueprint $table){
            $table->removeColumn('settings');
        });



    }
}
