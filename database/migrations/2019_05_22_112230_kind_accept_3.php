<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KindAccept3 extends Migration{

    public function up(){
        Schema::table('accepts', function(Blueprint $table){
            $table->integer('kind_acceptance')->default(3)->change();
        });
        Schema::table('contracts', function(Blueprint $table){
            $table->integer('kind_acceptance')->default(3)->change();
        });
    }


    public function down(){
        Schema::table('accepts', function(Blueprint $table){
            $table->integer('kind_acceptance')->default(0)->change();
        });
        Schema::table('contracts', function(Blueprint $table){
            $table->integer('kind_acceptance')->default(0)->change();
        });
    }
}
