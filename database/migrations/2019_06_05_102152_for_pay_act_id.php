<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForPayActId extends Migration{

    public function up(){

        DB::insert("INSERT INTO 
            m5_back.template_categories 
            (parent_id, code, title, has_choise, has_supplier, is_actual) 
            VALUES 
            (21, 'act_for_pay_cash', 'Акт на оплату нал', 0, 0, 1)"
        );

        DB::insert("INSERT INTO 
            m5_back.template_categories 
            (parent_id, code, title, has_choise, has_supplier, is_actual) 
            VALUES 
            (21, 'act_for_pay_cashless', 'Акт на оплату безнал', 0, 0, 1)"
        );
    }


    public function down(){

        DB::delete("delete from m5_back.template_categories where parent_id=21 and code in ('act_for_pay_cash', 'act_for_pay_cashless')");

    }
}
