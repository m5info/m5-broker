<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles_permissions', function (Blueprint $table) {
            $table->integer('role_id')->unsigned();
            $table->integer('permission_id')->unsigned();
            $table->primary(['permission_id','role_id']);
        });

        Schema::table('roles_permissions', function (Blueprint $table) {
            //$table->foreign('role_id', 'fk_roles_permissions_role1')->references('id')->on('roles')->onUpdate('NO ACTION')->onDelete('cascade');
            //$table->foreign('permission_id', 'fk_roles_permissions_permission1')->references('id')->on('permissions')->onUpdate('NO ACTION')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles_permissions');
    }
}
