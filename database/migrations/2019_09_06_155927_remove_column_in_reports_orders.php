<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnInReportsOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_orders', function (Blueprint $table) {
            $table->dropColumn('file_id');
        });

        Schema::create('report_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_id');
            $table->integer('file_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_orders', function (Blueprint $table) {
            //
        });
    }
}
