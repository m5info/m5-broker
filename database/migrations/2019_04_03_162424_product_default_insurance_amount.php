<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductDefaultInsuranceAmount extends Migration
{

    public function up()
    {
        Schema::table('products', function (Blueprint $table){
           $table->addColumn('decimal', 'insurance_amount_default', ['total' => 11, 'places' => 2])->default(0);
        });
    }


    public function down()
    {
        Schema::table('products', function (Blueprint $table){
            $table->dropColumn('insurance_amount_default');
        });
    }
}
