<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Subpermissions extends Migration{

    public function up(){

        Schema::create('subpermissions', function(Blueprint $table){
            $table->integer('permission_id')->default(0);
            $table->integer('view')->default(0);
            $table->integer('edit')->default(0);
            $table->char('title')->default('');
            $table->increments('id');
        });

        Schema::create('roles_subpermissions', function(Blueprint $table){
            $table->integer('subpermission_id');
            $table->integer('role_id');
        });

    }


    public function down(){

        Schema::dropIfExists('subpermissions');
        Schema::dropIfExists('roles_subpermissions');

    }
}
