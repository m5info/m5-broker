<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToTemplaCat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $templ = \App\Models\Settings\TemplateCategory::query()->where('code', 'contract_agent')->first();
        $templ->has_org = 1;
        $templ->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('templa_cat', function (Blueprint $table) {
            //
        });
    }
}
