<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllKindAcceptance extends Migration{

    public function up(){
        \App\Models\Actions\PaymentAccept::query()->update(['kind_acceptance' => 1]);
    }


    public function down(){
        \App\Models\Actions\PaymentAccept::query()->update(['kind_acceptance' => 0]);
    }
}
