<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Settings\TemplateType;

class AddNewTemplatType extends Migration {

    public function up() {
        $templateType = new TemplateType;
        $templateType->id = 2;
        $templateType->title = 'Страховая';
        $templateType->save();
    }

    public function down() {
        TemplateType::where('id', '=', 2)->get()->each->delete();
    }

}
