<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParamUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('work_phone')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->unsignedInteger('organization_id')->nullable();
            $table->unsignedInteger('image_id')->nullable();
            $table->unsignedInteger('small_image_id')->nullable();
            $table->integer('status_user_id')->nullable()->default(0);
            $table->integer('status_security_service')->nullable()->default(0);
        });



        Schema::table('users', function (Blueprint $table) {
            //$table->foreign('organization_id', 'fk_users_organization1')->references('id')->on('organizations')->onDelete('set null');
            //$table->foreign('image_id', 'fk_users_image_1')->references('id')->on('files')->onDelete('set null');
            //$table->foreign('small_image_id', 'fk_users_small_image_1')->references('id')->on('files')->onDelete('set null');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

        });
    }
}
