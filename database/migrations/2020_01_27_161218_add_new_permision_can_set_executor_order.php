<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPermisionCanSetExecutorOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $pg = \App\Models\Users\PermissionGroup::query()->where('title', 'orders')->first();

        $new_role = new \App\Models\Users\Permission();
        $new_role->title = 'can_set_executor';
        $new_role->group_id = $pg->id;
        $new_role->not_visible = 0;
        $new_role->sort = 7;
        $new_role->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
