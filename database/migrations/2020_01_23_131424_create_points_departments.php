<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointsDepartments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points_departments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('organization_id');
            $table->string('title');
            $table->string('address')->nullable();
            $table->string('geo_lat')->nullable();
            $table->string('geo_lng')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points_departments');
    }
}
