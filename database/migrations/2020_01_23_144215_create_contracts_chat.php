<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsChat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts_chat', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('contract_id');
            $table->unsignedInteger('sender_id');
            $table->text('text')->nullable();
            $table->dateTime('date_sent')->nullable();
            $table->dateTime('date_receipt')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('is_player')->default(0);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contracts_chat');
    }
}
