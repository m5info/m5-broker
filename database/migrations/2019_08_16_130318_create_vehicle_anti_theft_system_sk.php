<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleAntiTheftSystemSk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_anti_theft_system_sk', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insurance_companies_id')->nullable();
            $table->integer('bso_supplier_id')->nullable();
            $table->integer('anti_theft_system_id')->nullable();
            $table->string('anti_theft_system_sk_id')->nullable();
            $table->string('sk_title')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_anti_theft_system_sk');
    }
}
