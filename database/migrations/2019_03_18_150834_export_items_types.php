<?php

use App\Models\Settings\ExportItem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExportItemsTypes extends Migration
{

    public function up()
    {
        Schema::table('export_items', function(Blueprint $table){
            $table->dropColumn('allowed_keys');
            $table->addColumn('integer', 'type_id');
        });

        ExportItem::query()->where('code','realized_bso')->update(['type_id' => 1]);
        ExportItem::query()->where('code','transfer_deed_bso')->update(['type_id' => 1]);
        ExportItem::query()->where('code','reservation_invoice')->update(['type_id' => 1]);
        ExportItem::query()->where('code','invoice_export')->update(['type_id' => 1]);
        ExportItem::query()->where('code','invoice_act_export')->update(['type_id' => 1]);
        ExportItem::query()->where('code','repots_sk')->update(['type_id' => 2]);

    }


    public function down()
    {
        Schema::table('export_items', function(Blueprint $table){
            $table->dropColumn('type_id');
            $table->addColumn('text', 'allowed_keys')->default('null');
        });
    }
}
