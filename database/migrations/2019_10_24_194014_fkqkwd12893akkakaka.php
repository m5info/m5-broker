<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fkqkwd12893akkakaka extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $col = new \App\Models\Account\TableColumn();
        $col->column_name = 'КВ Бордеро';
        $col->table_key = 'analitics_common';
        $col->column_key = 'payments.financial_policy_kv_bordereau';
        $col->is_as = 1;
        $col->as_key = 'payments_financial_policy_kv_bordereau';
        $col->sorting = 1;
        $col->is_summary = 0;

        $col->save();

        $col = new \App\Models\Account\TableColumn();
        $col->column_name = 'КВ Бордеро (%)';
        $col->table_key = 'analitics_common';
        $col->column_key = 'payments.financial_policy_kv_bordereau_total';
        $col->is_as = 1;
        $col->as_key = 'payments_financial_policy_kv_bordereau_total';
        $col->sorting = 1;
        $col->is_summary = 0;

        $col->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
