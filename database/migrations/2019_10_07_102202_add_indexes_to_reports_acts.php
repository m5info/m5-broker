<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToReportsActs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_act', function (Blueprint $table) {
            $table->index('bso_supplier_id');
            $table->index('type_id');
            $table->index('created_at');
            $table->index('accept_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_act', function (Blueprint $table)
        {
            $table->dropIndex(['bso_supplier_id']);
            $table->dropIndex(['type_id']);
            $table->dropIndex(['created_at']);
            $table->dropIndex(['accept_status']);
        });
    }
}
