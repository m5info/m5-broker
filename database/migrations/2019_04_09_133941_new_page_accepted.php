<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewPageAccepted extends Migration
{

    public function up(){
        \App\Models\Users\Permission::create([
            'title' => 'accepted',
            'group_id' => 6,
            'not_visible' => 0,
            'sort' => 4,
        ]);
    }


    public function down(){
        \App\Models\Users\Permission::query()->where('group_id', 6)->where('title', 'accepted')->delete();
    }
}
