<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPermission123 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $group = \App\Models\Users\PermissionGroup::where('title', '=', 'role_owns')->get()->first();

        \App\Models\Users\Permission::create([
            'title' => 'login_any_user',
            'group_id' => $group->id,
            'not_visible' => 0,
            'sort' => 15,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
