<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategory extends Migration{

    public function up(){
        DB::insert("insert into template_categories VALUES 
              (25, 1, 'cashbox', 'Касса', 0, 0),
              (26, 25, 'income_expense', 'Доп доходы / Расходы', 0, 0)");
    }


    public function down(){
        DB::delete("delete from template_categories where id in (25,26)");
    }
}
