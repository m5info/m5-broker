<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChengeIdPermInselia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pg = \App\Models\Users\PermissionGroup::where('title', '=', 'orders')->first();

        $perm = \App\Models\Users\Permission::where('title', '=', 'order_statuses')->first();

        $perm->group_id = $pg->id;
        $perm->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
