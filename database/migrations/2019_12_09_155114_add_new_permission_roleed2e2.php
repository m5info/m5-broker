<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPermissionRoleed2e2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $group = \App\Models\Users\PermissionGroup::where('sort_view','=', 99)->first();

        \App\Models\Users\Permission::create([
            'title' => 'is_formalizator',
            'group_id' => $group->id,
            'not_visible' => 0,
            'sort' => 11,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
