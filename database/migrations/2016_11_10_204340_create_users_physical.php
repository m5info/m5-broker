<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersPhysical extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_physical', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('first_name');
            $table->string('second_name');
            $table->string('middle_name');
            $table->string('passport_number');
            $table->string('passport_series');
        });

        Schema::table('users_physical', function (Blueprint $table) {
            //$table->foreign('user_id', 'fk_users_physical_user1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_physical');
    }
}
