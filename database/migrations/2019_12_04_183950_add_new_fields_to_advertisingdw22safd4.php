<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToAdvertisingdw22safd4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_advertising', function (Blueprint $table) {
            $table->float('payment_total')->nullable();
            $table->float('bordereau_total')->nullable();
            $table->float('dvoy_total')->nullable();
            $table->float('amount_total')->nullable();
            $table->float('to_transfer_total')->nullable();
            $table->float('to_return_total')->nullable();
            $table->float('report_pay_outcome')->nullable();
            $table->float('report_pay_need')->nullable();
            $table->float('report_pay_income')->nullable();
            $table->float('debt_sk')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
