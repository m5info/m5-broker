<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBsoFieldUnderActId extends Migration{

    public function up(){
        Schema::table('bso_items', function(Blueprint $table){
            $table->addColumn('integer', 'acts_to_underwriting_id')->default(0);
        });
    }


    public function down(){
        Schema::table('bso_items', function(Blueprint $table){
            $table->dropColumn('acts_to_underwriting_id');
        });
    }
}
