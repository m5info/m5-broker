<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldSubjectsUfFl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects_fl', function(Blueprint $table){
            $table->string('address_register_city_fias_id')->nullable();
            $table->string('address_fact_city_fias_id')->nullable();
        });

        Schema::table('subjects_ul', function(Blueprint $table){
            $table->string('address_register_city_fias_id')->nullable();
            $table->string('address_fact_city_fias_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
