<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title');
            $table->integer('next_act')->nullable()->default(0);
            $table->string('default_purpose_payment')->nullable();
            $table->string('inn')->nullable();
            $table->decimal('limit_year', 11)->nullable()->default(0.00);
            $table->decimal('spent_limit_year', 11)->nullable()->default(0.00);
            $table->integer('is_actual')->nullable()->default(1);
            $table->tinyInteger('org_type_id')->unsigned()->nullable();
            $table->string('title_doc')->nullable();
            $table->string('general_manager')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->text('comment')->nullable();
            $table->integer('status_security_service')->nullable()->default(0);

        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organizations');
    }

}
