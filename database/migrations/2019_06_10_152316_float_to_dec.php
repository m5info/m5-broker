<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FloatToDec extends Migration{

    public function up(){

        Schema::table('drivers', function (Blueprint $table) {
            $table->dropColumn('kbm');
        });

        Schema::table('drivers', function (Blueprint $table) {
            $table->addColumn('decimal', 'kbm', ['total' => 11, 'places' => 2])->default(1);

        });
    }


    public function down(){
        Schema::table('drivers', function (Blueprint $table) {
            $table->dropColumn('kbm');
        });
        Schema::table('drivers', function (Blueprint $table) {
            $table->addColumn('float', 'kbm')->default(1);

        });
    }
}
