<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTablePayments extends Migration
{

    public function up()
    {
        Schema::table('payments', function(Blueprint $table){
            $table->addColumn('integer', 'acts_sk_id')->default(0);
        });
    }

    public function down()
    {
        Schema::table('payments', function(Blueprint $table){
            $table->dropColumn('acts_sk_id');
        });
    }
}
