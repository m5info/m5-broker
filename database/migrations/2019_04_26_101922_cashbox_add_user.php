<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CashboxAddUser extends Migration{

    public function up(){
        Schema::table('cashbox_transactions', function(Blueprint $table){
            $table->addColumn('integer', 'invoice_payment_user_id')->default(0);
        });
    }


    public function down(){
        Schema::table('cashbox_transactions', function(Blueprint $table){
            $table->dropColumn('invoice_payment_user_id');
        });
    }
}
