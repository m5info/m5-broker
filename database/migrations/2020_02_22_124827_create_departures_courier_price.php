<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeparturesCourierPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departures_courier_price', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('is_actual')->default(1)->nullable();
            $table->integer('city_id')->default(0)->nullable();
            $table->string('title')->nullable();
            $table->decimal('price_total', 11, 2)->nullable();
            $table->integer('is_debt')->default(0)->nullable();
            $table->decimal('debt_total', 11, 2)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departures_courier_price');
    }
}
