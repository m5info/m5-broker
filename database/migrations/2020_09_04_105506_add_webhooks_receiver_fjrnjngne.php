<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWebhooksReceiverFjrnjngne extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webhook_receivers', function (Blueprint $table){
           $table->increments('id');
           $table->string('identifier');
           $table->text('body');
           $table->text('headers');
           $table->timestamp('created_at')->default(DB::raw("CURRENT_TIMESTAMP"));
           $table->text('params')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
