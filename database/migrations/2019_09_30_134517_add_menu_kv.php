<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMenuKv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permission_group = \App\Models\Users\PermissionGroup::where('title', 'informing')->first();

        $permission = new \App\Models\Users\Permission();
        $permission->group_id = $permission_group->id;
        $permission->title = 'kv';
        $permission->not_visible = 0;
        $permission->sort = 0;
        $permission->save();

        $permission = new \App\Models\Users\Permission();
        $permission->group_id = $permission_group->id;
        $permission->title = 'edit_kv';
        $permission->not_visible = 0;
        $permission->sort = 0;
        $permission->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
