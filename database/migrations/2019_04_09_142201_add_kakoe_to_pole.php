<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKakoeToPole extends Migration
{

    public function up(){
        Schema::table('accepts', function (Blueprint $table) {
            $table->addColumn('integer', 'kind_acceptance')->default(0);
        });
    }


    public function down(){
        Schema::table('accepts', function (Blueprint $table) {
            $table->removeColumn('kind_acceptance');
        });
    }
}
