<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewSubpermissions extends Migration
{
    public function up(){

        \App\Models\Users\Subpermission::create([
            'permission_id' => 94,
            'title' => 'temp',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => 94,
            'title' => 'in_frame',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => 94,
            'title' => 'in_print',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => 94,
            'title' => 'in_delivery',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => 94,
            'title' => 'in_realized',
        ]);

    }


    public function down(){
        \App\Models\Users\Subpermission::query()->where('permission_id', 94)->delete();
    }
}
