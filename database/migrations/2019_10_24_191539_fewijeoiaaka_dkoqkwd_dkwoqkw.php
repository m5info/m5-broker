<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FewijeoiaakaDkoqkwdDkwoqkw extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $col = \App\Models\Account\TableColumn::query()->where('column_name', 'Банк КВ (Сумма)')->first();
        $col->column_name = 'Банк КВ';
        $col->save();

        $col = \App\Models\Account\TableColumn::query()->where('column_name', 'Оф. скидка (Сумма)')->first();
        $col->column_name = 'Оф. скидка';
        $col->save();

        $col = \App\Models\Account\TableColumn::query()->where('column_name', 'Неоф. скидка (Сумма)')->first();
        $col->column_name = 'Неоф. скидка';
        $col->save();

        $col = \App\Models\Account\TableColumn::query()->where('column_name', 'Возн. агента (Сумма)')->first();
        $col->column_name = 'Возн. агента';
        $col->save();

        $col = \App\Models\Account\TableColumn::query()->where('column_name', 'Возн. брокера (Сумма)')->first();
        $col->column_name = 'Возн. брокера';
        $col->save();

        $col = \App\Models\Account\TableColumn::query()->where('column_name', 'Маржа (Сумма)')->first();
        $col->column_name = 'Маржа';
        $col->save();

        $col = \App\Models\Account\TableColumn::query()->where('column_name', 'КВ ДВОУ (Сумма)')->first();
        $col->column_name = 'КВ ДВОУ';
        $col->save();



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
