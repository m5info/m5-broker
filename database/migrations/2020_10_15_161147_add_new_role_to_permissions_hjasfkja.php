<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewRoleToPermissionsHjasfkja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pg = \App\Models\Users\PermissionGroup::query()->where('title', 'role_owns')->first();

        $new_role = new \App\Models\Users\Permission();
        $new_role->title = 'is_referencer'; //рекомендодатель
        $new_role->group_id = $pg->id;
        $new_role->not_visible = 0;
        $new_role->sort = 11;
        $new_role->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
