<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTemplateCategory extends Migration
{
    public function up(){
        \App\Models\Settings\TemplateCategory::create([
            'parent_id' => 18,
            'code' => 'delivery_act',
            'title' => 'Заявка на выезд',
            'has_choise' => 0,
            'has_supplier' => 0,
            'is_actual' => 1,
        ]);

    }

    public function down(){
        \App\Models\Settings\TemplateCategory::query()->where('code', 'delivery_act')->delete();
    }
}
