<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PermissionExpensesIncomes extends Migration
{

    public function up()
    {
        $group = \App\Models\Users\PermissionGroup::query()->where('title', '=', 'settings')->first();
        $exp = new \App\Models\Users\Permission();
        $exp->title = 'incomes_expenses_categories';
        $exp->group_id = $group->id;
        $exp->not_visible = 0;
        $exp->sort = 0;
        $exp->save();

    }


    public function down()
    {
        $group = \App\Models\Users\PermissionGroup::query()->where('title', '=', 'settings')->first();
        \App\Models\Users\Permission::query()
            ->where('title', '=', 'incomes_expenses_categories')
            ->where('group_id', '=', $group->id)
            ->delete();
    }
}
