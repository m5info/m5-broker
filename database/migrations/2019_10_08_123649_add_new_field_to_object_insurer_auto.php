<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldToObjectInsurerAuto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('object_insurer_auto', function (Blueprint $table) {
            $table->date('dk_date_from')->nullable()->after('dk_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('object_insurer_auto', function (Blueprint $table) {
            $table->dropColumn('dk_date_from');
        });
    }
}
