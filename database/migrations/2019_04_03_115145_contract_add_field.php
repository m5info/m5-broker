<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContractAddField extends Migration
{

    public function up()
    {
        Schema::table('contracts', function(Blueprint $table){
            $table->addColumn('integer', 'type_id')->default(1);
        });
    }


    public function down()
    {
        Schema::table('contracts', function(Blueprint $table){
            $table->dropColumn('type_id');
        });
    }
}
