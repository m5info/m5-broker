<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BsoSuppliersProductsPayMethods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bso_suppliers_products_pay_methods', function(Blueprint $table){
            $table->integer('access_bso')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bso_suppliers_products_pay_methods', function(Blueprint $table){
            $table->dropColumn('access_bso');
        });
    }
}
