<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersJuridical extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_juridical', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->string('ogrn');
            $table->string('inn');
            $table->string('bik');
            $table->string('bank');
            $table->string('rs')->comment('Расчётный счёт');
        });

        Schema::table('users_juridical', function (Blueprint $table) {
            //$table->foreign('user_id', 'fk_users_juridical_user1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_juridical');
    }
}
