<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NullableDriversDates extends Migration{

    public function up(){

        Schema::table('drivers', function(Blueprint $table){
            //$table->date('doc_serie')->nullable();
        });

        Schema::table('drivers', function(Blueprint $table){
            $table->date('birth_date')->nullable()->change();
            $table->date('doc_date')->nullable()->change();
            $table->date('exp_date')->nullable()->change();
            $table->integer('sex')->nullable()->default(0)->change();
            $table->integer('same_as_insurer')->nullable()->default(0)->change();
            $table->decimal('kbm')->nullable()->default(0)->change();
        });

    }


    public function down(){

        Schema::table('drivers', function(Blueprint $table){
            $table->removeColumn('doc_serie');
        });

        Schema::table('drivers', function(Blueprint $table){
            $table->date('birth_date')->nullable(false)->change();
            $table->date('doc_date')->nullable(false)->change();
            $table->date('exp_date')->nullable(false)->change();
            $table->integer('sex')->nullable(false)->change();
            $table->integer('same_as_insurer')->nullable(false)->change();
            $table->decimal('kbm')->nullable(false)->change();
        });

    }
}
