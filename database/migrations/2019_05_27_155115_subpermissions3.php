<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Subpermissions3 extends Migration{

    public function up(){

        Schema::table('roles_subpermissions', function(Blueprint $table){
            $table->addColumn('integer', 'permission_id')->default(0);
        });
    }


    public function down(){
        Schema::table('roles_subpermissions', function(Blueprint $table){
            $table->dropColumn('permission_id');
        });
    }
}
