<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $p1 = \App\Models\Users\Permission::create([
            'title' => 'purpose_of_use',
            'group_id' => 2,
            'not_visible' => 0,
            'sort' => 0,
        ]);



        $p2 = \App\Models\Users\Permission::create([
            'title' => 'categories',
            'group_id' => 2,
            'not_visible' => 0,
            'sort' => 0,
        ]);

        $p3 = \App\Models\Users\Permission::create([
            'title' => 'marks_and_models',
            'group_id' => 2,
            'not_visible' => 0,
            'sort' => 0,
        ]);

        DB::insert('insert into roles_permissions (role_id, permission_id) values (?, ?)', [1, $p1->id]);
        DB::insert('insert into roles_permissions (role_id, permission_id) values (?, ?)', [1, $p2->id]);
        DB::insert('insert into roles_permissions (role_id, permission_id) values (?, ?)', [1, $p3->id]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
