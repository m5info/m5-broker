<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContractsLogs extends Migration {

    public function up() {
        Schema::create('contracts_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contract_id');
            $table->integer('status_id');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('contracts_logs');
    }

}
