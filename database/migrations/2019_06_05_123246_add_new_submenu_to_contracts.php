<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewSubmenuToContracts extends Migration
{
    public function up(){

        $gr = \App\Models\Users\PermissionGroup::query()
            ->where('title', 'contracts')
            ->first();

        \App\Models\Users\Permission::create([
            'group_id' => $gr->id,
            'title' => 'underfillers',
            'not_visible' => '0',
            'sort' => 9,
        ]);

    }


    public function down(){

        $gr = \App\Models\Users\PermissionGroup::query()->where('title', 'contracts')->first();

        \App\Models\Users\Permission::query()
            ->where('group_id', $gr->id)
            ->where('title', 'underfillers')
            ->delete();
    }
}
