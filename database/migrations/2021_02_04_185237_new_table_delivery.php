<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewTableDelivery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->default(null);
            $table->integer('parent_id')->nullable()->default(null);
            $table->date('delivery_date')->nullable()->default(null);
            $table->integer('contract_id')->nullable()->default(null);
            $table->integer('payment_id')->nullable()->default(null);
            $table->decimal('delivery_cost', 11, 2)->nullable()->default(null);
            $table->integer('weekend')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_bac');
    }
}
