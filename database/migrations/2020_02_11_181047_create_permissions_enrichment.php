<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsEnrichment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $perm = \App\Models\Users\PermissionGroup::create([
            'title' => 'enrichment',
            'sort_view' => 10,
            'is_visibility' => 1
        ]);


        \App\Models\Users\Permission::create([
            'title' => 'av100',
            'group_id' => $perm->id,
            'not_visible' => 0,
            'sort' => 0,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
