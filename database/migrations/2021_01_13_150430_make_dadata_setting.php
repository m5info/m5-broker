<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Models\Settings\SettingsSystem;

class MakeDadataSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        SettingsSystem::create([
            'types' => 'dadata',
            'param' => 'token',
            'val' => '5f6f4a246bd9d39b1a60c5058e4aaa359bed9c04',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
