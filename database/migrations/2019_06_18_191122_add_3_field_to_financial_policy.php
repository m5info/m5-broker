<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add3FieldToFinancialPolicy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('financial_policies', function (Blueprint $table) {
            $table->decimal('partnership_reward_1', 11)->nullable()->default(0.00);
            $table->decimal('partnership_reward_2', 11)->nullable()->default(0.00);
            $table->decimal('partnership_reward_3', 11)->nullable()->default(0.00);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('financial_policies', function (Blueprint $table) {
            $table->dropColumn('partnership_reward_1');
            $table->dropColumn('partnership_reward_2');
            $table->dropColumn('partnership_reward_3');
        });
    }

}
