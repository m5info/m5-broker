<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActRealizedCategoryTemplate extends Migration
{

    public function up()
    {
        DB::insert("insert into template_categories values (27, 2, 'bso_realized', 'Акт БСО реализованные', 0, 0)");
    }


    public function down()
    {
        DB::delete('delete from template_categories where id=27');
    }
}
