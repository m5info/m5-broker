<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportOrderBasePayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_orders_base_payments', function (Blueprint $table) {
            $table->increments('id');

            $table->timestamps();
            $table->integer('report_order_id');
            $table->integer('payment_id');

            $table->decimal('payment_total', 11, 2)->nullable();
            $table->decimal('official_discount', 11, 2)->nullable();
            $table->decimal('informal_discount', 11, 2)->nullable();
            $table->decimal('bank_kv', 11, 2)->nullable();

            $table->integer('financial_policy_id')->nullable();
            $table->integer('financial_policy_manually_set')->nullable();


            $table->decimal('financial_policy_kv_bordereau', 11, 2)->nullable();
            $table->decimal('financial_policy_kv_dvoy', 11, 2)->nullable();
            $table->decimal('financial_policy_kv_agent', 11, 2)->nullable();
            $table->decimal('financial_policy_kv_parent', 11, 2)->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_order_base_payment');
    }
}
