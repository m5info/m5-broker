<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $group = \App\Models\Users\PermissionGroup::where('sort_view','=', 99)->first();

        \App\Models\Users\Permission::create([
            'title' => 'temp_contract_visibility_not_full',
            'group_id' => $group->id,
            'not_visible' => 0,
            'sort' => 4,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $perm = \App\Models\Users\Permission::query()->where('title', 'temp_contract_visibility_not_full')->first();
        $perm->delete();
    }
}
