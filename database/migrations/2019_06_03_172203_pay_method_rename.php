<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PayMethodRename extends Migration{

    public function up(){
        Schema::table('payments', function (Blueprint $table){
            $table->renameColumn('pay_method', 'pay_method_id');
        });
    }

    public function down(){
        Schema::table('payments', function (Blueprint $table){
            $table->renameColumn('pay_method_id', 'pay_method');
        });
    }
}
