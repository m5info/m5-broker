<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompanyRazmansovka extends Migration
{

    public function up(){
        \App\Models\User::query()->update([
            'organization_id' => 1,
        ]);
        \App\Models\Organizations\Organization::query()->where('id', 1)->update(['is_delete' => 0]);
    }


    public function down(){
        \App\Models\User::query()->update([
            'organization_id' => 2,
        ]);
        \App\Models\Organizations\Organization::query()->where('id', 1)->update(['is_delete' => 1]);

    }
}
