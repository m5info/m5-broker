<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->integer('check_user_id')->nullable();
        });

        $contracts =\App\Models\Contracts\Contracts::query()
            ->where('check_user_id', '>', 0)
            ->get();

        foreach($contracts as $contract){
            $contract->all_payments()
                ->where('type_id', '=', 0)
                ->update(['check_user_id' => $contract->check_user_id]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
