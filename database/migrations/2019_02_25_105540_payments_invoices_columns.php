<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PaymentsInvoicesColumns extends Migration
{

    public function up()
    {

        Schema::table('payments', function(Blueprint $table){
           $table->integer('org_id')->nullable()->default(null);
           $table->decimal('invoice_payment_total', 11, 2)->nullable()->default(null);
           $table->dateTime('invoice_payment_date')->nullable()->default(null);
        });


        Schema::table('invoices', function(Blueprint $table){
            $table->integer('agent_id')->nullable()->default(null);
            $table->integer('type_invoice_payment_id')->nullable()->default(null);
            $table->integer('invoice_payment_balance_id')->nullable()->default(null);
            $table->integer('invoice_payment_user_id')->nullable()->default(null);
            $table->decimal('invoice_payment_total', 11, 2)->nullable()->default(null);
            $table->dateTime('invoice_payment_date')->nullable()->default(null);
            $table->text('invoice_payment_com');
        });
    }


    public function down()
    {
        Schema::table('payments', function(Blueprint $table){
            $table->dropColumn([
                'org_id',
                'invoice_payment_total',
                'invoice_payment_date',
            ]);
        });

        Schema::table('invoices', function(Blueprint $table){
            $table->dropColumn([
                'agent_id',
                'type_invoice_payment_id',
                'invoice_payment_balance_id',
                'invoice_payment_user_id',
                'invoice_payment_total',
                'invoice_payment_date',
                'invoice_payment_com',
            ]);

        });

    }
}
