<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesContractAndPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts', function(Blueprint $table)
        {
            $table->index('created_at');
            $table->index('updated_at');
            $table->index('statys_id');
            $table->index('user_id');
            $table->index('bso_id');
            $table->index('insurance_companies_id');
            $table->index('bso_supplier_id');
            $table->index('product_id');
            $table->index('agent_id');
            $table->index('is_personal_sales');
            $table->index('manager_id');
            $table->index('parent_agent_id');
            $table->index('sales_condition');
            $table->index('sign_date');
            $table->index('begin_date');
            $table->index('end_date');
            $table->index('underfiller_date');
            $table->index('insurer_id');
            $table->index('object_insurer_id');
            $table->index('payment_total');
            $table->index('financial_policy_id');
            $table->index('financial_policy_kv_bordereau');
            $table->index('financial_policy_kv_dvoy');
            $table->index('financial_policy_kv_agent');
            $table->index('financial_policy_kv_parent');
            $table->index('installment_algorithms_id');
            $table->index('check_user_id');
            $table->index('kind_acceptance');
            $table->index('is_online');
            $table->index('type_id');
            $table->index('underfiller_check');
            $table->index('owner_id');
            $table->index('underfiller_id');
            $table->index('is_epolicy');
            $table->index('beneficiar_id');
        });

        Schema::table('payments', function(Blueprint $table)
        {
            $table->index('statys_id');
            $table->index('type_id');
            $table->index('is_deleted');
            $table->index('invoice_id');
            $table->index('payment_data');
            $table->index('payment_type');
            $table->index('payment_flow');
            $table->index('payment_total');
            $table->index('official_discount');
            $table->index('official_discount_total');
            $table->index('informal_discount');
            $table->index('informal_discount_total');
            $table->index('bank_kv');
            $table->index('bank_kv_total');
            $table->index('financial_policy_id');
            $table->index('financial_policy_manually_set');
            $table->index('financial_policy_kv_bordereau');
            $table->index('financial_policy_kv_bordereau_total');
            $table->index('financial_policy_kv_dvoy');
            $table->index('financial_policy_kv_dvoy_total');
            $table->index('financial_policy_kv_agent');
            $table->index('financial_policy_kv_agent_total');
            $table->index('financial_policy_kv_parent');
            $table->index('financial_policy_kv_parent_total');
            $table->index('bso_not_receipt');
            $table->index('bso_receipt');
            $table->index('bso_receipt_id');
            $table->index('agent_id');
            $table->index('manager_id');
            $table->index('parent_agent_id');
            $table->index('org_id');
            $table->index('invoice_payment_total');
            $table->index('invoice_payment_date');
            $table->index('point_sale_id');
            $table->index('set_balance');
            $table->index('user_id');
            $table->index('order_id');
            $table->index('accept_user_id');
            $table->index('accept_date');
            $table->index('pay_method_id');
            $table->index('financial_policy_marjing');
            $table->index('financial_policy_marjing_total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
