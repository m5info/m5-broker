<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InformingSegments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informing_segments', function(Blueprint $table){

           $table->increments('id');
           $table->integer('insurance_company_id')->nullable();
           $table->integer('bso_supplier_id')->nullable();
           $table->integer('type_insurer')->nullable();
           $table->integer('owner_registration_address')->nullable();
           $table->string('territory_cf')->nullable();
           $table->integer('type_of_contract')->nullable();
           $table->string('period_of_use')->nullable();
           $table->string('kbm')->nullable();
           $table->integer('ts_category')->nullable();
           $table->string('ts_type')->nullable();

           $table->string('ts_power_any')->nullable();
           $table->string('ts_power_from')->nullable();
           $table->string('ts_power_to')->nullable();
           $table->integer('is_trailer')->nullable();
           $table->integer('ts_year')->nullable();

           $table->integer('drivers')->nullable();
           $table->integer('multidrive')->nullable();

           $table->integer('minimal_age_driver')->nullable();
           $table->integer('minimal_exp_driver')->nullable();

           $table->timestamp('created_at');
           $table->timestamp('updated_at');

        });

        Schema::create('informing_segments_group', function(Blueprint $table){

            $table->increments('id');
            $table->integer('group_id');
            $table->integer('is_actual');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->timestamp('actual_from');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informing_segments');
        Schema::dropIfExists('informing_segments_group');
    }
}
