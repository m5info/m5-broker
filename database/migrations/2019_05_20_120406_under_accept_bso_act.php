<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UnderAcceptBsoAct extends Migration{

    public function up(){
        Schema::table('bso_acts', function(Blueprint $table){
            $table->addColumn('integer', 'under_accepted_id')->default(0);
            $table->addColumn('datetime', 'under_accepted_date');
        });
    }


    public function down(){
        Schema::table('bso_acts', function(Blueprint $table){
            $table->dropColumn('under_accepted_id');
            $table->dropColumn( 'under_accepted_date');
        });
    }
}
