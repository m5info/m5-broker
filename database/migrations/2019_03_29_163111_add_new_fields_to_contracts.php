<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToContracts extends Migration {

    public function up() {
        Schema::table('contracts', function (Blueprint $table) {
            $table->string('payment_next_date')->nullable();
            $table->decimal('payment_next_sum', 11, 2)->nullable();
        });
    }

    public function down() {
        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn('payment_next_date');
            $table->dropColumn('payment_next_sum');
        });
    }

}
