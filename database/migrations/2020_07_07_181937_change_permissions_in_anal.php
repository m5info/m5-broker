<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePermissionsInAnal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permission = \App\Models\Users\Permission::where('title', '=', 'analitics_common')->first();

        $old_subs = \App\Models\Users\Subpermission::where('permission_id', '=', $permission->id)->get();

        if($old_subs){
            foreach($old_subs as $old_sub){
                $old_sub->delete();
            }
        }

        for($i = 1; $i < 51; $i++){
            \App\Models\Users\Subpermission::create([
                'permission_id' => $permission->id,
                'title' => 'analitics_common_table_columns_key_'.$i,
            ]);
        }

        \App\Models\Users\Subpermission::create([
            'permission_id' => $permission->id,
            'title' => 'analitics_common_table_columns_key_82',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => $permission->id,
            'title' => 'analitics_common_table_columns_key_83',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => $permission->id,
            'title' => 'analitics_common_table_columns_key_84',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => $permission->id,
            'title' => 'analitics_common_table_columns_key_85',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
