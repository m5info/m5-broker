<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add2ColumnsToReportsFormationF23fi2n3if extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      $tc = new \App\Models\Account\TableColumn();
      $tc->column_name = 'Андеррайтер';
      $tc->table_key = 'reports_sk';
      $tc->column_key = 'underwriter.name';
      $tc->is_as = 1;
      $tc->as_key = 'underwriter_name';
      $tc->sorting = 1;
      $tc->is_summary = '0';
      $tc->save();

      $tc = new \App\Models\Account\TableColumn();
      $tc->column_name = 'СП - Бордеро';
      $tc->table_key = 'reports_sk';
      $tc->column_key = 'payments.payment_total - payments.financial_policy_kv_bordereau_total';
      $tc->is_as = 1;
      $tc->as_key = 'payment_total_minus_bordero';
      $tc->sorting = 1;
      $tc->is_summary = '0';
      $tc->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
