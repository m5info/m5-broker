<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewCategoryUserBalancesDfiejkfiowjeoif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $category = new \App\Models\Settings\TemplateCategory();
        $kassa = \App\Models\Settings\TemplateCategory::query()->where('code', 'cashbox')->first();
        $category->parent_id = $kassa->id;
        $category->code = 'user_balance';
        $category->title = 'Балансы';
        $category->has_choise = 1;
        $category->has_supplier = 0;
        $category->is_actual = 1;
        $category->has_org = 0;
        $category->save();


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
