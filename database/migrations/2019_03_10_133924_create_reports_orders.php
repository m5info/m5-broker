<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_orders', function(Blueprint $table){

            $table->increments('id');

            $table->integer('bso_supplier_id')->default(0);

            $table->integer('type_id')->default(0);
            $table->integer('accept_status')->default(0);

            $table->integer('create_user_id')->nullable()->default(null);

            $table->string('title')->nullable()->default(null);
            $table->string('signatory_org')->nullable()->default(null);
            $table->string('signatory_sk_bso_supplier')->nullable()->default(null);

            $table->integer('report_year')->nullable()->default(0);
            $table->integer('report_month')->nullable()->default(0);
            $table->integer('is_deleted')->nullable()->default(0);

            $table->date('report_date_start');
            $table->date('report_date_end');

            $table->dateTime( 'accepted_at')->nullable()->default(null);
            $table->integer('accept_user_id')->nullable()->default(null);

            $table->decimal('payment_total', 11, 2)->nullable()->default(null);
            $table->decimal('bordereau_total', 11, 2)->nullable()->default(null);
            $table->decimal('dvoy_total', 11, 2)->nullable()->default(null);
            $table->decimal('amount_total', 11, 2)->nullable()->default(null);
            $table->decimal('to_transfer_total', 11, 2)->nullable()->default(null);
            $table->decimal('to_return_total', 11, 2)->nullable()->default(null);

            $table->text('comments');


            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports_orders');
    }
}
