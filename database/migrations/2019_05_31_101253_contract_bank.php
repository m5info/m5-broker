<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContractBank extends Migration{

    public function up(){
        Schema::table('contracts', function(Blueprint $table){
            $table->addColumn('integer', 'bank_id')->default(0);
        });
    }

    public function down(){
        Schema::table('contracts', function(Blueprint $table){
            $table->dropColumn('bank_id');
        });
    }
}
