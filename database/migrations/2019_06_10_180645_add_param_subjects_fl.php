<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParamSubjectsFl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects_fl', function(Blueprint $table){

            $table->string('address_register_city')->nullable();
            $table->string('address_register_city_kladr_id')->nullable();
            $table->string('address_register_street')->nullable();
            $table->string('address_register_house')->nullable();
            $table->string('address_register_block')->nullable();
            $table->string('address_register_flat')->nullable();

            $table->string('address_fact_city')->nullable();
            $table->string('address_fact_city_kladr_id')->nullable();
            $table->string('address_fact_street')->nullable();
            $table->string('address_fact_house')->nullable();
            $table->string('address_fact_block')->nullable();
            $table->string('address_fact_flat')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
