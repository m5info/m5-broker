<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dwqkwdo12dk192jd1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $col = \App\Models\Account\TableColumn::query()->where('column_name', 'КВ Бордеро')->first();
        $col->column_key = 'payments.financial_policy_kv_bordereau_total';
        $col->save();

        $col = \App\Models\Account\TableColumn::query()->where('column_name', 'КВ Бордеро (%)')->first();
        $col->column_key = 'payments.financial_policy_kv_bordereau';
        $col->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
