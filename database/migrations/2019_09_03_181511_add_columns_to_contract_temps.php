<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToContractTemps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts_terms', function (Blueprint $table) {
            $table->tinyInteger('guarantor_repair')->nullable();
            $table->tinyInteger('unguarantor_repair')->nullable();
            $table->tinyInteger('voluntary_liability_insurance')->nullable();
            $table->tinyInteger('passenger_and_driver_accident_insurance')->nullable();
            $table->tinyInteger('permanent_sum_insured')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts_terms', function (Blueprint $table) {
            $table->dropColumn('guarantor_repair');
            $table->dropColumn('unguarantor_repair');
            $table->dropColumn('voluntary_liability_insurance');
            $table->dropColumn('passenger_and_driver_accident_insurance');
            $table->dropColumn('permanent_sum_insured');
        });
    }
}
