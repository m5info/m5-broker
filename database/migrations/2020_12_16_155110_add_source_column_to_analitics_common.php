<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSourceColumnToAnaliticsCommon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\Account\TableColumn::create([
            'column_name' => 'Источник',
            'table_key' => 'analitics_common',
            'column_key' => 'payments.order_source',
            'is_as' => 1,
            'as_key' => 'payments_order_source',
            'sorting' => 1,
            'is_summary' => '0',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analitics_common', function (Blueprint $table) {
            //
        });
    }
}
