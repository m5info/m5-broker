<?php

use App\Models\Users\PermissionGroup;
use App\Models\Users\Permission;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewMenuItemAnalyticsUnderfillers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $group_id = PermissionGroup::where('title', 'analitics')->first()->id;

        $permission = new Permission();
        $permission->title = 'analytics_underfillers';
        $permission->group_id = $group_id;
        $permission->not_visible = 0;
        $permission->sort = 0;
        $permission->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('title', 'analytics_underfillers')->first()->delete();
    }
}
