<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Subpermission2 extends Migration{

    public function up(){

        Schema::table('subpermissions', function(Blueprint $table){
           $table->dropColumn('view');
           $table->dropColumn('edit');

        });


        Schema::table('roles_subpermissions', function(Blueprint $table){
            $table->addColumn('integer', 'view')->default(0);
            $table->addColumn('integer', 'edit')->default(0);
        });

        \App\Models\Users\Subpermission::create([
            'permission_id' => 33,
            'title' => 'temp',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => 33,
            'title' => 'in_cashbox',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => 33,
            'title' => 'in_check',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => 33,
            'title' => 'in_correction',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => 33,
            'title' => 'in_pay',
        ]);
        \App\Models\Users\Subpermission::create([
            'permission_id' => 33,
            'title' => 'in_confirm',
        ]);

    }

    public function down(){

        Schema::table('subpermissions', function(Blueprint $table){
            $table->addColumn('integer', 'view')->default(0);
            $table->addColumn('integer', 'edit')->default(0);
        });

        Schema::table('roles_subpermissions', function(Blueprint $table){
            $table->dropColumn('view');
            $table->dropColumn('edit');
        });

        \App\Models\Users\Subpermission::query()->delete();
    }
}
