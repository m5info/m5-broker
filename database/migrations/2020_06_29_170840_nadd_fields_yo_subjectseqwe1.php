<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NaddFieldsYoSubjectseqwe1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects', function (Blueprint $table) {
            $table->string('add_phone')->nullable();
            $table->string('delivery_address')->nullable();
        });

        Schema::table('subjects_fl', function (Blueprint $table) {
            $table->string('add_phone')->nullable();
            $table->string('delivery_address')->nullable();
        });

        Schema::table('subjects_ul', function (Blueprint $table) {
            $table->string('add_phone')->nullable();
            $table->string('delivery_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
