<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fkweak18jfjere8jqiuhq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('table_columns', function (Blueprint $table) {
            $table->dropIfExists('lvl');
            $table->dropIfExists('colspan');
            $table->dropIfExists('parent_id');
            $table->dropIfExists('is_parent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
