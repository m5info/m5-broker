<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReferralForPay extends Migration{

    public function up(){
       DB::insert("INSERT INTO m5_back.permissions (title, group_id, not_visible, sort) VALUES ('referral_for_pay', 6, 0, 0)");
    }

    public function down(){
        DB::delete("DELETE FROM m5_back.permissions WHERE title='referral_for_pay' and group_id=6");
    }
}
