<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Directories\Products;

class AddSlugForProducts extends Migration {

    public function up() {
        Schema::table('products', function ($table) {
            $table->string('slug')->nullable();
        });

        $products = Products::get();

        foreach ($products as $product) {
            $product->slug = translitClass($product->title);
            $product->save();
        }
    }

    public function down() {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['slug']);
        });
    }

}
