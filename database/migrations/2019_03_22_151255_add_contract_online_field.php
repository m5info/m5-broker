<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContractOnlineField extends Migration {

    public function up() {
        Schema::table('contracts', function ($table) {
            $table->tinyInteger('is_online')->nullable();
        });
    }

    public function down() {
        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn(['is_online']);
        });
    }

}
