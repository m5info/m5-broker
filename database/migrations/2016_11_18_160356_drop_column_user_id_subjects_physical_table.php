<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnUserIdSubjectsPhysicalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects_physical', function (Blueprint $table) {
            //$table->dropForeign('fk_users_physical_user1');
            $table->dropColumn('user_id');
        });
    }


    public function down()
    {

    }
}
