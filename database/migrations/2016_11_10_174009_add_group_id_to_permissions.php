<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGroupIdToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->integer('group_id')->unsigned()->nullable();
        });

        Schema::table('permissions', function (Blueprint $table) {
            //$table->foreign('group_id', 'fk_permissions_group1')->references('id')->on('permissions_groups')->onUpdate('NO ACTION')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //$table->dropForeign('fk_permissions_group1');
            //$table->dropColumn('group_id');
        });
    }
}
