<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CommonLogsF1fu12871827h7d72hd871h28 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cud_logs', function(Blueprint $table){
           $table->increments('id');
           $table->integer('user_id')->nullable()->unsigned();
           $table->integer('role_id')->nullable()->unsigned();
           $table->string('table');
           $table->integer('context_id')->nullable();
           $table->string('event');
           $table->text('old_data')->nullable();
           $table->text('new_data')->nullable();
           $table->timestamp('created')->useCurrent();


           $table->foreign('user_id')->references('id')->on('users');
           $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
