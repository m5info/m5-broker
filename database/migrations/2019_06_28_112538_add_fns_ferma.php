<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFnsFerma extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fns_ferma', function(Blueprint $table){
            $table->increments('id');
            $table->integer('contract_id')->default(0);
            $table->integer('payment_id')->default(0);
            $table->dateTime('create_date');
            $table->string('status');

            $table->text('request_json');
            $table->text('response_json');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
