<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class PaymentAccept extends Migration{

    public function up(){
        Schema::rename('accepts', 'payment_accepts');
    }

    public function down(){
        Schema::rename('payment_accepts', 'accepts');
    }
}
