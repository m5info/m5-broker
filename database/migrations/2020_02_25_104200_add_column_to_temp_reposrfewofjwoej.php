<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToTempReposrfewofjwoej extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('temp_responses_api');

        Schema::create('vsk_api_online', function (Blueprint $table){
           $table->increments('id');
           $table->text('body')->nullable();
           $table->text('headers')->nullable();
           $table->timestamp('created_at');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
