<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLogsOnlineFwnefowefoj extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_calculations_logs', function(Blueprint $table){
           $table->increments('id');
           $table->integer('contract_id');
           $table->integer('online_calculation_id');
           $table->string('action')->default('');
           $table->text('data_send')->default('');
           $table->text('data_response')->default('');
           $table->string('link')->default('');
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
