<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPermissione2vfedve extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $group = \App\Models\Users\PermissionGroup::where('title','=', 'orders')->first();

        \App\Models\Users\Permission::create([
            'title' => 'order_supplier_and_fp',
            'group_id' => $group->id,
            'not_visible' => 0,
            'sort' => 4,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
