<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsSubjectsFlrf43k3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects_fl', function(Blueprint $table){
            $table->string('address_fact_zip')->after('address_fact_kladr');
            $table->string('address_register_zip')->after('address_register_kladr');
            $table->string('address_fact_okato')->after('address_fact_kladr');
            $table->string('address_register_okato')->after('address_register_kladr');
        });

        Schema::table('subjects_ul', function(Blueprint $table){
            $table->string('address_fact_zip')->after('address_fact_kladr');
            $table->string('address_register_zip')->after('address_register_kladr');
            $table->string('address_fact_okato')->after('address_fact_kladr');
            $table->string('address_register_okato')->after('address_register_kladr');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
