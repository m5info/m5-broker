<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class News extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('news', function(Blueprint $table){
           $table->increments('id');
           $table->string('header');
           $table->text('content');
           $table->string('url');
           $table->string('main_pic');
           $table->timestamp('created_at');
           $table->timestamp('updated_at');
            $table->integer('visibility')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }
}
