<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRashodDohod extends Migration{

    public function up()
    {
        Schema::create('incomes_expenses_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->char('title');
            $table->integer('is_actual');
            $table->integer('type');
        });

    }

    public function down()
    {
        Schema::dropIfExists('incomes_expenses_categories');

    }
}
