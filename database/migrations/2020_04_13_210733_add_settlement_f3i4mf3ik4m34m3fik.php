<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSettlementF3i4mf3ik4m34m3fik extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects_fl', function (Blueprint $table){
           $table->string('address_register_settlement')->nullable();
           $table->string('address_fact_settlement')->nullable();

           $table->string('address_register_settlement_kladr_id')->nullable();
           $table->string('address_fact_settlement_kladr_id')->nullable();
        });

        Schema::table('subjects_ul', function (Blueprint $table){
            $table->string('address_register_settlement')->nullable();
            $table->string('address_fact_settlement')->nullable();

            $table->string('address_register_settlement_kladr_id')->nullable();
            $table->string('address_fact_settlement_kladr_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
