<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCategory1 extends Migration
{

    public function up()
    {
        \App\Models\Settings\TemplateCategory::query()->find(20)->update(['code' => 'contracts_to_sk']);
        \App\Models\Settings\TemplateCategory::query()->find(13)->update(['code' => 'sk_acts']);
        \App\Models\Settings\TemplateCategory::query()->find(14)->update(['code' => 'sk_bso_to_sk']);
        \App\Models\Settings\TemplateCategory::query()->find(17)->delete();
        \App\Models\Settings\TemplateCategory::query()->find(16)->update([
            'code' => 'report_to_sk',
            'title' => 'Отчёт в СК',
        ]);
    }


    public function down()
    {
        \App\Models\Settings\TemplateCategory::query()->find(20)->update(['code' => 'payments_to_sk']);
        \App\Models\Settings\TemplateCategory::query()->find(13)->update(['code' => 'sk_bso_to_sk']);
        \App\Models\Settings\TemplateCategory::query()->find(14)->update(['code' => 'bso_to_sk']);
        DB::insert("insert into template_categories values (17, 15,	'borderau_to_sk', 'Отчет Бордеро в СК',	1, 1)");
        \App\Models\Settings\TemplateCategory::query()->find(16)->update([
            'code' => 'dvou_to_sk',
            'title' => 'Отчет ДВОУ в СК',
        ]);
    }
}
