<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnalitycsCommonTableColumnAddContractsTypeIdFkjewifj2fj2ijef extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tb = new \App\Models\Account\TableColumn();
        $tb->column_name = "Тип договора";
        $tb->table_key = "analitics_common";
        $tb->column_key = "contracts.type_id";
        $tb->is_as = 1;
        $tb->as_key = 'contracts_type_id';
        $tb->sorting = 1;
        $tb->is_summary = 0;
        $tb->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
