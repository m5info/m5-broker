<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Users\Permission;

class AddNewMenuItem extends Migration {

    public function up() {
        $permission = new Permission;
        $permission->id = 51;
        $permission->title = 'released_contracts';
        $permission->group_id = 6;
        $permission->not_visible = 0;
        $permission->sort = 0;
        $permission->save();
        
        DB::statement('INSERT INTO roles_permissions (role_id, permission_id) VALUES (1, 51);');
        
    }

    public function down() {
        $permission = Permission::where('title' , '=', 'released_contracts')->get()->each->delete();;
        DB::statement('DELETE FROM roles_permissions WHERE permission_id = 51;');
    }

}
