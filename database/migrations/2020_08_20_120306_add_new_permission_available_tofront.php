<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPermissionAvailableTofront extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $p = new \App\Models\Users\Permission();
        $p->title = 'available_to_front';
        $p->group_id = \App\Models\Users\PermissionGroup::where('title', 'contracts')->first()->id;
        $p->not_visible = 0;
        $p->sort = 19;
        $p->type = 0;
        $p->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
