<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Adawdaw extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $perm_group = \App\Models\Users\Permission::query()->where('title', 'edit_news')->first();
        $settings = \App\Models\Users\PermissionGroup::query()->where('title', 'settings')->get()->first();

        $perm_group->group_id = $settings->id;

        $perm_group->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
