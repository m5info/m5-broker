<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRisksFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts_terms', function (Blueprint $table){

         $table->integer('hijacking_sum')->nullable();
         $table->integer('passenger_and_driver_accident_insurance_sum');
         $table->integer('passenger_and_driver_accident_insurance_seats');
         $table->char('passenger_and_driver_accident_insurance_type');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
