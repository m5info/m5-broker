<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleAutoCodInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_auto_cod_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->dateTime('actual_date')->nullable();
            $table->string('vin')->nullable();
            $table->string('reg_num')->nullable();
            $table->integer('year')->nullable();
            $table->string('title')->nullable();
            $table->longText('info')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_auto_cod_info');
    }
}
