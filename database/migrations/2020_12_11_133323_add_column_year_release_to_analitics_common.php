<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnYearReleaseToAnaliticsCommon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\Account\TableColumn::create([
            'column_name' => 'Год выпуска',
            'table_key' => 'analitics_common',
            'column_key' => 'object_insurer_auto.car_year',
            'is_as' => 1,
            'as_key' => 'object_insurer_auto_car_year',
            'sorting' => 1,
            'is_summary' => '0',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analitics_common', function (Blueprint $table) {
            //
        });
    }
}
