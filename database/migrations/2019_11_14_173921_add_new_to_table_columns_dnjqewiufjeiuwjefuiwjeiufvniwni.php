<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewToTableColumnsDnjqewiufjeiuwjefuiwjeiufvniwni extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tb = new \App\Models\Account\TableColumn();
        $tb->column_name = 'Сумма в СК';
        $tb->table_key = 'analitics_common';
        $tb->column_key = 'IF (payments.payment_flow = 0, (contracts.insurance_amount - (payments.financial_policy_kv_bordereau_total + financial_policy_kv_dvoy_total)), 0)';
        $tb->is_as = 1;
        $tb->as_key = 'contracts_sum_sk';
        $tb->sorting = 1;
        $tb->is_summary = 1;
        $tb->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
