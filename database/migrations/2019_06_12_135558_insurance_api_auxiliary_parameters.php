<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsuranceApiAuxiliaryParameters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_insurance_auxiliary_parameters', function(Blueprint $table){
            $table->increments('id');
            $table->integer('insurance_companies_id')->default(0);
            $table->integer('type_id')->default(0);
            $table->integer('local_id')->default(0);
            $table->string('sk_id')->default('');
            $table->text('auxiliary_parameters')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
