<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentAtolCheckId extends Migration{

    public function up(){
        Schema::table('payments', function(Blueprint $table){
            $table->addColumn('integer', 'atol_check_id')->default(0);
        });
    }

    public function down(){
        Schema::table('payments', function(Blueprint $table){
            $table->dropColumn('atol_check_id');
        });
    }
}
