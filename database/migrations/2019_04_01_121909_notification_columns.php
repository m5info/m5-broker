<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotificationColumns extends Migration
{

    public function up()
    {
        Schema::table('notifications', function(Blueprint $table){
            $table->addColumn('integer','is_read');
        });
    }


    public function down()
    {
        Schema::table('notifications', function(Blueprint $table){
            $table->dropColumn('is_read');
        });
    }
}
