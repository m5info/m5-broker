<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubpermissionColumnTableColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permission = \App\Models\Users\Permission::where('title', '=', 'analitics_common')->first();

        \App\Models\Users\Subpermission::create([
            'permission_id' => $permission->id,
            'title' => 'analitics_common_table_columns_key_89',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
