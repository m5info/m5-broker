<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOrgUsers extends Migration
{

    public function up(){
        \App\Models\User::query()->update([
            'organization_id' => 2,
        ]);
    }


    public function down(){
        \App\Models\User::query()->update([
            'organization_id' => 1,
        ]);
    }
}
