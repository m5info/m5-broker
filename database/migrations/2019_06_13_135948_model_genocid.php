<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModelGenocid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $models = \App\Models\Vehicle\VehicleModels::all()->keyBy('id')->toArray();
        \App\Models\Vehicle\VehicleModels::query()->delete();
        \App\Models\Vehicle\VehicleModels::create_many($models);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $models = \App\Models\Vehicle\VehicleModels::all()->keyBy('id')->toArray();
        \App\Models\Vehicle\VehicleModels::query()->delete();
        \App\Models\Vehicle\VehicleModels::create_many($models);
    }
}
