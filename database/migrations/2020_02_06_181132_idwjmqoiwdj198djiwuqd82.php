<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Idwjmqoiwdj198djiwuqd82 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspection_orders_reports', function(Blueprint $table){
           $table->integer('accept_status')->nullable();
           $table->text('comment')->nullable();
           $table->string('signatory_org')->nullable();
           $table->string('signatory_sk_bso_supplier')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
