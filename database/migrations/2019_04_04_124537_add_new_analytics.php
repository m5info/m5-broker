<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Users\Permission;

class AddNewAnalytics extends Migration {

    public function up() {
        $permission = new Permission;
        $permission->id = 64;
        $permission->title = 'analytics_techunder';
        $permission->group_id = 9;
        $permission->not_visible = 0;
        $permission->sort = 0;
        $permission->save();

        DB::statement('INSERT INTO roles_permissions (role_id, permission_id) VALUES (1, 64);');
    }

    public function down() {
        Permission::where('title', '=', 'analytics_techunder')->get()->each->delete();
        DB::statement('DELETE FROM roles_permissions WHERE permission_id = 64;');
    }

}
