<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermDeparturesCourierPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        \App\Models\Users\Permission::create([
            'title' => 'departures_courier_price',
            'group_id' => 3,
            'not_visible' => 0,
            'sort' => 0,
        ]);

        \App\Models\Users\Permission::create([
            'title' => 'departures_courier_acts',
            'group_id' => 13,
            'not_visible' => 0,
            'sort' => 0,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
