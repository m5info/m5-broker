<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoundationPaymentsBalance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('foundation_payments_balance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('foundation_id')->default(0)->nullable();
            $table->integer('balance_transactions_id')->default(0)->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foundation_payments_balance');
    }
}
