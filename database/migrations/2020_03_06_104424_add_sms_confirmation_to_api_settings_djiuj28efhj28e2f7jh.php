<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSmsConfirmationToApiSettingsDjiuj28efhj28e2f7jh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('api_settings', function (Blueprint $table) {
            $table->string('sms_confirmation')->default(0);
        });

        Schema::table('online_calculations', function (Blueprint $table) {
            $table->string('sms_code')->nullable();
            $table->integer('sms_verify')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
