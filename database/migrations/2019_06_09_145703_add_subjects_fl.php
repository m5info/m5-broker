<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubjectsFl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects_fl', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('subject_id')->nullable();
            $table->string('fio')->nullable();
            $table->integer('sex')->nullable();
            $table->date('birthdate')->nullable();

            $table->string('address_born')->nullable();
            $table->string('address_born_kladr')->nullable();
            $table->string('address_register')->nullable();
            $table->string('address_register_kladr')->nullable();
            $table->string('address_fact')->nullable();
            $table->string('address_fact_kladr')->nullable();

            $table->integer('doc_type')->nullable();
            $table->string('doc_serie')->nullable();
            $table->string('doc_number')->nullable();
            $table->date('doc_date')->nullable();

            $table->string('doc_office')->nullable();
            $table->string('doc_info')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
