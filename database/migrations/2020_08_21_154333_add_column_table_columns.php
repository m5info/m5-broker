<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTableColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $p = new \App\Models\Account\TableColumn();
        $p->column_name = 'Направление РИТ';
        $p->table_key = 'analitics_common';
        $p->column_key = 'payments.order_title';
        $p->is_as = 1;
        $p->as_key = 'payments_order_title';
        $p->sorting = 1;
        $p->is_summary = 0;

        $p->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
