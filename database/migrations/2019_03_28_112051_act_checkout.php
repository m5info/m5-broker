<?php

use App\Models\Settings\TemplateCategory;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActCheckout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* 22	21	invoice	Счёт	0	0
        23	21	invoice_act	Акт по счёту	0	0*/
        Schema::table('template_categories', function(Blueprint $table){
            $table->addColumn('integer', 'is_actual')->default(1);
        });

        TemplateCategory::query()->whereIn('id', [22,23])->update(['is_actual' => 0]);

        DB::insert("insert into template_categories values (28,21, 'act_checkout', 'Акт сдачи в кассу', 0,0, 1)");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_categories', function(Blueprint $table){
            $table->dropColumn('is_actual');
        });
        DB::delete("delete from template_categories where id=28");
    }
}
