<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function(Blueprint $table)
        {
            $table->index('bso_id');
            $table->index('contract_id');
            $table->index('reports_order_id');
            $table->index('reports_dvou_id');
            $table->index('realized_act_id');
            $table->index('acts_sk_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table)
        {
            $table->dropIndex(['bso_id']);
            $table->dropIndex(['contract_id']);
            $table->dropIndex(['reports_order_id']);
            $table->dropIndex(['reports_dvou_id']);
            $table->dropIndex(['realized_act_id']);
            $table->dropIndex(['acts_sk_id']);
        });
    }
}
