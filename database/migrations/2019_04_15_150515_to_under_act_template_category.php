<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ToUnderActTemplateCategory extends Migration
{

    public function up(){
        \App\Models\Settings\TemplateCategory::create([
            'parent_id' => 21,
            'code' => 'act_to_under',
            'title' => 'Акт сдачи в андеррайтинг',
            'has_choise' => 0,
            'has_supplier' => 0,
            'is_actual' => 1,
        ]);

    }

    public function down(){
        \App\Models\Settings\TemplateCategory::query()->where('code', 'act_to_under')->delete();
    }

}
