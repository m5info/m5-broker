<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTerms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts_terms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contract_id');

            $table->string('prolongation_bso')->nullable();
            $table->integer('type_sum_insured_id')->nullable()->default(0);

            $table->integer('franchise_type_id')->nullable()->default(0);
            $table->decimal('franchise', 11, 2)->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts_terms');
    }
}
