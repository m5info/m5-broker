<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVersionFormsValues extends Migration {

    /**
     * type_id - тип, форма для setting или для suppliers
     */
    public function up() {
        Schema::create('integrations_versions_main_form_values', function (Blueprint $table) {
            $table->increments('id');
            $table->string('form_key')->nullable();
            $table->string('value')->nullable();
            $table->integer('version_id');
            $table->timestamps();
        });


        Schema::create('integrations_versions_supplier_form_values', function (Blueprint $table) {
            $table->increments('id');
            $table->string('form_key')->nullable();
            $table->string('value')->nullable();
            $table->integer('version_id');
            
            $table->integer('hold_kv_id');
            $table->integer('insurance_companies_id');
            $table->integer('bso_supplier_id');
            $table->integer('product_id');
            

            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('integrations_versions_main_form_values');
        Schema::drop('integrations_versions_supplier_form_values');
    }

}
