<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentFinancialPolicies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_financial_policies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0)->nullable();
            $table->integer('financial_group_id')->default(0)->nullable();

            $table->date('begin_date')->nullable();
            $table->date('end_date')->nullable();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_financial_policies');
    }
}
