<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InspectionOrdersReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspection_orders_reports', function(Blueprint $table){
            $table->increments('id');
            $table->integer('org_id');
            $table->integer('create_user_id');
            $table->string('title');
            $table->integer('report_year');
            $table->integer('report_month');
            $table->integer('is_deleted');
            $table->date('report_date_start');
            $table->date('report_date_end');
            $table->dateTime('accepted_at');
            $table->integer('accept_user_id');
            $table->decimal('payment_total', 11, 2);
            $table->dateTime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
