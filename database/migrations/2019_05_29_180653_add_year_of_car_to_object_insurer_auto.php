<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddYearOfCarToObjectInsurerAuto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('object_insurer_auto', function (Blueprint $table) {
            $table->integer('year_of_car');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('object_insurer_auto', function (Blueprint $table) {
            $table->dropColumn('year_of_car');
        });
    }
}
