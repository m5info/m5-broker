<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActOrgIdField extends Migration{

    public function up(){
        Schema::table('bso_acts', function(Blueprint $table){
            $table->addColumn('integer', 'act_org_id')->default(0);
        });
    }


    public function down(){
        Schema::table('bso_acts', function(Blueprint $table){
            $table->dropColumn('act_org_id');
        });
    }
}
