<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UnderwriterRate extends Migration{

    public function up(){
        Schema::table('products', function(Blueprint $table){
            $table->addColumn('decimal', 'underwriter_rate', ['total' => 11, 'places' => 2])->default(0);
        });
    }


    public function down(){
        Schema::table('products', function(Blueprint $table){
            $table->dropColumn('underwriter_rate');
        });
    }
}
