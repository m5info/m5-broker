$(function () {
    $(document).on('click', '#work-area-hook', function(){
        var work_area = $("#work-area");
        if(!work_area.hasClass('active')){
            showWorkArea()
        }else{
            hideWorkArea()
        }
    });

    if($('#work-area').length){
        $(document).on('click', 'body', function(ev){
            setTimeout(function(){
                var work_area = $("#work-area");
                if(work_area && work_area[0] && ev.originalEvent && work_area[0].offsetLeft > ev.originalEvent.x && work_area.hasClass('active')){
                    hideWorkArea()
                }
            }, 100);
        });
    }

});

function showWorkArea(){
    initWorkArea();
    var work_area = $('#work-area');
    var work_area_hook = $('#work-area-hook');
    var width = parseInt(work_area.css('width'));
    var hook_right = parseInt(work_area_hook.css('right'));


    work_area.addClass('active');
    //work_area.animate({'right':'0'}, 100);
    work_area.css('right', '0');
    work_area_hook.css('right', (hook_right + width) + 'px');
    //work_area_hook.animate({'right': (hook_right + width) + 'px' }, 100);
}

function hideWorkArea(){
    var work_area = $('#work-area');
    var work_area_hook = $('#work-area-hook');

    var width = parseInt(work_area.css('width'));

    //work_area.animate({'right':'-' + width}, 100);
    work_area.css('right', '-' + width + 'px');
    work_area.removeClass('active');
    work_area_hook.css('right', '-28px');
    //work_area_hook.animate({'right':'-28px'}, 100);

}

function hideWorkAreButton() {
    $('#work-area').hide();
    $('#work-area-hook').hide();
}

function initWorkArea(){
// $('#work-area').show();
// $('#work-area-hook').show();
//     $('#work-area').css('display', 'block');
//     $('#work-area-hook').css('display', 'block');
    document.getElementById("work-area").setAttribute("style", "display:block;");
    document.getElementById("work-area-hook").setAttribute("style", "display:block;");
}

$('#work-area').niceScroll({
    cursorcolor: 'rgba(221,221,221,0.44)',
    cursorwidth: 4,
    cursorborder: 'none'
});
