var selected_guide = null;
var content_guide ={};
var title_guide ={};
var guid_group ={};

$(function () {

    CKFinder.setupCKEditor();

    $.each( guides_counts, function( key, value ) {

        let div = '<a class="guides-group-count" href="/informing/guide/edit/?group='+key+'">'+value+'</a>';
        if(key)
        $(' .list-settings-item.sub-item[href*='+key+']').parent().after(div);
    });

    $('button#addGuideBlock').on('click',function () {
        var button = $(this).parent();

        $.ajax
        ({
            type: "POST",
            //the url where you want to sent the userName and password to
            url: '/informing/guide/edit/add_block',
            dataType: 'json',
            async: false,
            //json object to sent to the authentication url
            data: {'_token':$('[name=_token]').val()},
            success: function (res) {

                console.log(res);

                button.before(res.html);
                $('#guide'+res.id+' h2').attr('contenteditable','true');
                $('#guide'+res.id+' #guide_html'+res.id).attr('contenteditable','true');

                CKEDITOR.inline('guide_html'+res.id);
            }
        });

    });

    /* Работа с сохранением*/

    $('.guide_block').on('click',function () {
        selected_guide = $(this);
        content_guide = $(this).find('.panel-body').html();
        title_guide = $(this).find('h2.block-title').text();
    });

    $(document.body).on('click',function (e){

        var div = $(selected_guide);
        if (div.length != 0)
            var id =  div.data().id != null ? div.data().id : false;
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {

                if(id)
                    updateBlock(id);

            }

    });

    /*Инициализация редактирования*/
    setEdittable();
    $('.guide_block').each(function () {
        let id = $(this).children('.panel-body').attr('id');
        CKEDITOR.inline(id);

    })

});

function setEdittable() {
   $('.guide_block h2.block-title, .guide_block .panel-body').attr('contenteditable','true');
}

function removeGuideBlock(e) {
    var guide = $(e).data().id;
    if(confirm('Удалить блок №'+guide+'?'))
        $.ajax
        ({
            type: "POST",
            //the url where you want to sent the userName and password to
            url: '/informing/guide/edit/delete',
            dataType: 'json',

            //json object to sent to the authentication url
            data: {'_token':$('[name=_token]').val(),'id':guide},
            success: function (res) {
                $('#guide'+guide).remove();
                console.log(res);

            }
        });
}

function updateBlock(id) {
    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: '/informing/guide/edit/'+id,
        dataType: 'json',

        //json object to sent to the authentication url
        data: {
            '_token':$('[name=_token]').val(),
            'id':id,
            'html':$('#guide_html'+id).html(),
            'title':$('#guide'+id+' h2.block-title').text(),
            'group':$('#guide_group'+id).val()
        },
        success: function (res) {
            console.log('save');
            console.log(res);

        },
        done:function (res) {
            console.log('save');
            console.log(res);

        },
    });
}
