﻿var applicationParameters = {};

function ApplicationParameters(application, parameters) {
	this.application = application;
	this.parameters = parameters;
}

applicationParameters["_global_"] = new ApplicationParameters("_global_", {
	"paykiosk" : {
		// общий таймаут, будет использован если не указано детально в параметрах paybox
		"inactivityTimeout" : 5000,
		// отображать предупреждение о невозможности печати чека перед приемом денег
		"printerOfflineWarning" : true,
		// Ошибки принтера не позволяют работать
		"printerRequired" : false,
		// Ошибки диспенсера не позволяют работать
		"dispenserRequired" : false,
		// Разрешить работать при отказе или отсутствии купюроприёмника
		"workWithoutBillAcceptor" : false,
		//использовать звуки
		"playSound" : false,
		//использовать звуки нажатия кнопок
		"playSoundButtons" : true,
		//показ баннера вверху страницы
		"showBanner" : false
	},
	
	"startingPaykiosk" : {
		
		// Устройства, состояние которых отображается при запуске сценария.
		// Имена устройств изменять нельзя,
		// чтобы отключить показ состояние устройства - закомментируйте его имя.
		"devices" : [
			// "doNotShowThis",
			"paymentDevice", 
			"VirtuPOS", 
			"billAcceptor", 
			"Printer"
		],
		
		// время показа сводной таблицы (нахождения в состоянии 'start') [мс]
		"showTime" : 500
	
	},
	
	"card" : {
	
		// время в секундах, в течение которого пользователь должен забрать карту из ридера
		// по истечении этого времени происходит изъятие карты ридером
		"captureTimeout" : 30,
	
		/* Типы карт по RecipientID из bintable
			диапазоны: '100-199'
			одиночные: 9000
			смешанные: ['300-350', 352, '400-420', 430, 500]
		*/
		"cardTypes" : {
			// карты своего банка
			"ours" : ['100-199', 8000],
			// карты чужого банка
			"foreign" : ['200-299', 8001],
			// карты инкассаторов
			"encashment" : 9000	
		}
		
	},
	
	"VirtuPOS" : {
		
		// количество попыток исполнения метода VirtuPOS в случае, если VirtuPOS занят
		"attemptsNumber" : 5,
		// время, через которое будет осуществляться очередная попытка [мс]
		"retryTime" : 2000
		
	}
})

/* Далее следуют системные функции, не изменять. */

function getParameterValue(parameters, parameterPath) {
	var 
		parameterPathArray = parameterPath.split("."),
		parameter = parameters, 
		parameterValue, i, c, e;

	for (i = 0, c = parameterPathArray.length; i < c; i+=1) {
		e = parameterPathArray[i];
		if (parameter.hasOwnProperty(e)) {
			parameter = parameter[e];
			parameterValue = parameter;
		} else {
			return;
		}
	}
	return parameterValue;
}

function getParameter(parameterPath, defaultValue) {

	var parameterValue;
	if (applicationParameters.hasOwnProperty(selectedApplication) && applicationParameters[selectedApplication].parameters)
		parameterValue = getParameterValue(applicationParameters[selectedApplication].parameters, parameterPath);
	
	if (typeof(parameterValue) === "undefined")
		parameterValue = getParameterValue(applicationParameters["_global_"].parameters, parameterPath);

	if (typeof(parameterValue) === "undefined") 
		parameterValue = defaultValue;
	return parameterValue;
	
};

function appendParameters(additionalParameters) {
	if (applicationParameters.hasOwnProperty(activeApplication)) {
		for (var s in additionalParameters) {
			// Добавляем новый параметр только, если он не добавлен ранее для того, чтобы можно было переопределить параметр
			// в начальном файле приложения
			if (!additionalParameters.hasOwnProperty(s) || applicationParameters[activeApplication].parameters.hasOwnProperty(s))
				continue;
			applicationParameters[activeApplication].parameters[s] = additionalParameters[s];
		}
	} else {
		applicationParameters[activeApplication] = new ApplicationParameters(activeApplication, additionalParameters);
	}
}