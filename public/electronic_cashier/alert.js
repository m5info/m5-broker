﻿// =================== Настройки ==========================
var defaultAlertTimeout = 50000;
// В формате используется переменная ${button} равная имени кнопки
//var alertButtonFormat = "<input type=\"button\" value=\"${button}\" onclick=\"executeAlertButtonAction('${button}')\">";
var alertButtonFormat = "<div class=\"alertButton\" onclick=\"executeAlertButtonAction('${button}')\"><img src=\"images\\${locale}\\${button}.png\" border=\"0\"></div>";
// Разделитель между кнопками
var alertButtonSeparator = "&nbsp;";

// Содержимое div для вывода alert. Данный блок должен содержать элемент с идентификатором alertMessageElementID для вывода данных
var alertElementHTML =
'<table border=\"0\">\
<tr valign=middle><td><div id="alertMessageTop"></div>\
<div id="alertMessageDiv"></div>\
<div id="alertMessageBottom"></div></td></tr>\
</table>';
// Имя элемента, который будет создан на основной страницы для хранения отображения
var alertElementID = "alertDiv";
// Имя элемента, в который осуществляется вывод сообщения
var alertMessageElementID = "alertMessageDiv";

// ================ Рабочие переменные ====================
var alertTimeoutId = null;
var alertButtons;
var alertKeyActions = null;
var alertTimeoutAction = null;

var alertLogger = getLogger('alert');

// нижеследующее временно, т.к. сейчас некогда
var hideBanner = false;

/**
Отображает в элемент с идентификатором alertDivID сообщение и набор кнопок, сформированных
в соответствии с параметром alertButtonFormat разделенных alertButtonSeparator.

Если message не определен, то ничего не делает.

Возвращает true, если сообщение было отображено и false, если по каким-то причинам это не удалось

При timeout > 0 выполняет через указанной время (в милисекундах) действие в соответствии
со следующей логикой:
timeoutAction не определен -- прячет сообщение
timeoutAction строка -- выполняет действие в соответствии с одной из кнопок
timeoutAction функция -- выполняет указанную функцию.

Если timeout не указан, то окно закроется через defaultAlertTimeout, если timeout равен 0,
то таймаут применяться не будет
*/
function showAlert(message, buttons, keysMapping, timeoutAction, timeout) {
	if (message == null)
		return false;

	hideBanner = true;
	//loadBanner(false);
		
	hideAlert();
	
	var alertDiv = this.document.getElementById(alertElementID);
	var alertMessageDiv = this.document.getElementById(alertMessageElementID);
	
	if (alertMessageDiv == null) {
		// Нет элемента для отображения сообщения
		alertDiv = this.document.createElement("div");
		alertDiv.id = alertElementID;
		alertDiv.innerHTML = alertElementHTML;
		
		var bodyElement = this.document.getElementsByTagName("body");
		if ((bodyElement == null) || (bodyElement.length < 1)) {
			alertLogger.error("No body element");
			return false;
		} else 
			bodyElement[0].appendChild(alertDiv);
			
		alertMessageDiv = this.document.getElementById(alertMessageElementID);
		if (alertMessageDiv == null) {
			alertLogger.error("No message div");
			return false;
		}
	}

	var modalBG = document.getElementById("modalBG");
	if (!modalBG) {
		modalBG = document.createElement("div");
		modalBG.id="modalBG"
		document.body.appendChild(modalBG);
	}

	var alertButtonsHTML = [];
	
	// Чистим массив обработчиков
	alertButtons = buttons;
	
	// Рисуем кнопки
	if (buttons) {
		for(var button in buttons) {
			if (!buttons.hasOwnProperty(button)) continue;
			alertButtonsHTML.push(alertButtonFormat.replace(/\$\{([^}]+)\}/g,
				function (str, exp) {
					if (exp == "button")
						return button;
					if (exp == "locale")
						return getActiveLocale();
					return "";
				})
			);
//			alertButtonsHTML.push(alertButtonFormat.replace(/\$\{button\}/g, button));
		}
	}
	
	if (keysMapping) {
		alertKeyActions = keysMapping;
		_.addEventListener('key', processAlertKey);
	} else
		alertKeyActions = null;

	alertMessageDiv.innerHTML = message + "<br><br>" + alertButtonsHTML.join(alertButtonSeparator);
	
	if (timeout == null) {
		timeout = getParameter("paykiosk.defaultAlertTimeout", 100000);
	}
	
	// Если таймаут вообще включен
	if (timeout > 0) {
		alertTimeoutAction = null;

		if (timeoutAction != null) {
			if (buttons[timeoutAction] != null)
				alertTimeoutAction = buttons[timeoutAction];
			else
				alertTimeoutAction = timeoutAction;
		}
			
		alertTimeoutId = setTimeout(executeAlertTimeoutAction, timeout);
	}
	
	// Отображаем, если есть кнопки
	alertDiv.style.display = "inline";
	if (modalBG)
		modalBG.style.display = "inline";
	
	return true;
}

/**
Скрывает ранее отображенное сообщение
*/
function hideAlert() {
	var modalBG = document.getElementById("modalBG");
	if (modalBG != null)
		modalBG.style.display = "none";
		
	var alertDiv = document.getElementById(alertElementID);
	
	if (alertDiv != null) {
		// Если что-то показывалось вообще
		alertDiv.style.display = "none";

		// Останавливаем таймаут
		if (alertTimeoutId !== null) {
			clearTimeout(alertTimeoutId);
			alertTimeoutId = null;
		}
		
		if (alertKeyActions != null) {
			_.removeEventListener('key', processAlertKey);
			alertKeyActions = null;
		}
	}
}

function processAlertKey(key) {
	if (alertKeyActions[key])
		executeAlertButtonAction(alertKeyActions[key]);
		
	return false;
}

/**
Обработчик действий для кнопки.
Выполняет указанное для кнопки действие и скрывает окно alert'а
*/
function executeAlertButtonAction(alertButton) {
	// Сначала прячем отображенное сообщение,
	// чтобы новое сообщение из действия не было закрыто
	hideAlert();
	if (alertButtons[alertButton] != null)
		alertButtons[alertButton]();
}

/**
Обработчик таймаута
*/
function executeAlertTimeoutAction() {
	alertTimeoutId = null;
	if (alertTimeoutAction == null) {
		hideAlert();
	} else {
		// Сначала прячем отображенное сообщение,
		// чтобы новое сообщение из действия не было закрыто
		hideAlert();
		alertTimeoutAction();
	}
}
