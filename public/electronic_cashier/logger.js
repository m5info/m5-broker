﻿// ================================ Logger =================================
// Выводит сообщение в консоль для браузеров, которые это поддерживают
function Logger(source) {
	this.formatMessage = function(msg) {
		return '[' + source + '] ' + msg;
	};
}

Logger.prototype = {
	"debug" : function(msg) {
			if (typeof(console) !== "undefined") {
				if (console.debug) console.debug(this.formatMessage(msg));
				else if (console.log) console.log(this.formatMessage(msg));
			}
		},
	
	"info" : function(msg) { if ((typeof(console) !== "undefined") && console.info) console.info(this.formatMessage(msg)); },
	"warn" : function(msg) { if ((typeof(console) !== "undefined") && console.warn) console.warn(this.formatMessage(msg)); },
	"error" : function(msg) { if ((typeof(console) !== "undefined") && console.error) console.error(this.formatMessage(msg)); else alert(this.formatMessage(msg)); }
}

function getLogger(source) {
	return new Logger(source);
}