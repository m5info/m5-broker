﻿_.addEventListeners({
	"load" : function () {
			
		
		_.registerVariables({
			"workerCode" : Framework.Variables("workerCode") ? Framework.Variables("workerCode") : "unknown"
		});
		

		_.registerActions({
			"serviceStackerExecute" : function() { serviceStackerExecute(); },
			"commitStamp" : function() { commitStamp(); },
			"clearStamp" : function() { clearStamp(); },
			"append" : function(value) { append(value); },
			"secondPrint" : function() {
				printBillsRegister(); 
				if (!(Printer && Printer.DeviceReady))
					showAlert('Принтер неисправен', {}, {}, hideAlert, 5000);
			},
			"exit" : function() { _.leave("exit"); }
		});
		
		
		if (!ReceiptPrinter.Offline)
			Framework.AttachEvent(ReceiptPrinter, "OnPrinterEvent", onPrinterEvent);

		
	},
	"run" : function() {
		documentLoaded();
	},
	"unload" : function() {
		return false;
	},
	"inactivityTimer" : function() {
		_["exit"]();
	}
});
