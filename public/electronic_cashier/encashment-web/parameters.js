﻿appendParameters({
	"paykiosk" : {
		"stackerSize" : 1200, // Размер стэкера по умолчанию
		"enterStampNr" : 1, //вводить номер пломбы установленной кассеты (1-да, 0-нет)
		"minBillValue" : 50 //минимальная принимаемая купюра
	}
})