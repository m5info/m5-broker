﻿/**/
var applicationStates = {};

var KBExternal = null;
if (window.external.Framework)
	KBExternal = window.external;
else
	KBExternal = new ActiveXObject("KBFrameworkAlone.KBExternal");
var Framework = KBExternal.Framework;

function ApplicationStates(application, states) {
	this.application = application;
	this.states = states;
	this.statesCache = {};
	
	this.findBehaviour = function(stateType, stateExtension, propertyValue, basePath) {
		if (basePath.indexOf('file:///') != -1)
			basePath = basePath.substr(8); // remove  file:///
			
		// Путь к JS
		var jsPath = basePath+"behaviours/";

		var behaviourJS = "zzz";
		// Название задано в свойстве
		if (propertyValue != null)
			behaviourJS = Framework_IfExistsThen(jsPath +propertyValue, "zzz", getTopWindow());
		// Название по имени+типу, все пути относительно рута
		if (behaviourJS == "zzz" && stateExtension != null) 
			behaviourJS = Framework_IfExistsThen(jsPath +stateType + "-" + stateExtension + ".js", "zzz", getTopWindow());
		if (behaviourJS == "zzz") 
			behaviourJS = Framework_IfExistsThen(jsPath +stateType + ".js", "zzz", getTopWindow());

		if (behaviourJS == "zzz")
			behaviourJS = jsPath +stateType + ".js";

		if (behaviourJS.indexOf('file:///') != -1)
			behaviourJS = behaviourJS.substr(8); // remove  file:///
		return behaviourJS;
	}

	this.findView = function(stateType, stateExtension, propertyValue, basePath) {
		if (basePath.indexOf('file:///') != -1)
			basePath = basePath.substr(8); // remove  file:///

		// Путь к JS
		var jsPath = basePath+"views/";
		var viewJS = "zzz";

		// Название задано в свойстве
		if (propertyValue != null)
			viewJS = Framework_IfExistsThen(jsPath +propertyValue, "zzz", getTopWindow());
		// Название по имени+типу, все пути относительно рута
		if (viewJS == "zzz" && stateExtension != null) 
			viewJS = Framework_IfExistsThen(jsPath +stateType + "-view-" + stateExtension + ".js", "zzz", getTopWindow());
		if (viewJS == "zzz") 
			viewJS = Framework_IfExistsThen(jsPath +stateType + "-view.js", "zzz", getTopWindow());

		if (viewJS == "zzz")
			// viewJS = jsPath+ stateType + "-view.js";
			viewJS = null;

		if (viewJS && viewJS.indexOf('file:///') != -1)
			viewJS = viewJS.substr(8); // remove  file:///

		return viewJS;
	}

	this.findScreen = function (stateType, stateExtension, propertyValue, basePath) {
		if (basePath.indexOf('file:///') != -1)
			basePath = basePath.substr(8); // remove  file:///
			
		basePath += "screens/"
			
		var screenURL = "zzz";
		// Название задано в свойстве
		if (propertyValue != null)
			screenURL = Framework_IfExistsThen(basePath+ propertyValue, "zzz", getTopWindow());
		// Название по имени+типу, все пути относительно рута
		if (screenURL == "zzz" && stateExtension != null) 
			screenURL = Framework_IfExistsThen(basePath +stateType + "-" + stateExtension + ".html", "zzz", getTopWindow());
		if (screenURL == "zzz") 
			screenURL = Framework_IfExistsThen(basePath + stateType + ".html", "zzz", getTopWindow());
		if (screenURL == "zzz")  {
			screenURL = basePath + "_default-view.html";
		}
		
		if (screenURL.indexOf('file:///') != -1)
			screenURL = screenURL.substr(8); // remove  file:///
		return screenURL;
	}
	
	this.findScript = function (stateType, stateExtension, propertyValue, basePath) {
		if (propertyValue == null)
			return null;
			
		if (basePath.indexOf('file:///') != -1)
			basePath = basePath.substr(8); // remove  file:///

		var paykioskPath = basePath.substr(0, basePath.lastIndexOf("/"));
		paykioskPath = paykioskPath.substr(0, paykioskPath.lastIndexOf("/")+1);
		basePath += "scripts/";

		
		var scriptURL = "zzz";
		// Название задано в свойстве всегда
		if (Object.prototype.toString.call(propertyValue) !== "[object Array]")
			propertyValue = [propertyValue];
		
		for (var i = 0; i < propertyValue.length; i++) {
			if (propertyValue[i].indexOf('/') < 0)
				scriptURL = Framework_IfExistsThen(basePath+ propertyValue[i], paykioskPath+propertyValue[i], getTopWindow()); // Еще не искали, путь неизвестен
			else
				scriptURL = propertyValue[i]; // Этот файл уже найден и указан полный путь
			if (scriptURL.indexOf('file:///') != -1)
				scriptURL = scriptURL.substr(8); // remove  file:///
			propertyValue[i] = scriptURL;
		}
		return propertyValue;
	}

	this.getStateProperty = function(stateType, stateExtension, property, propertyValue) {
		var applicationPath = Framework_IfExistsThen(application + "/" + application +".js", "zzz", getTopWindow());
		if (applicationPath == "zzz")
			applicationPath = "";
		else {
			applicationPath = applicationPath.substring(0, applicationPath.lastIndexOf('/')+1);
			if (applicationPath.indexOf('file:///') == 0)
				applicationPath = applicationPath.substr(8); // remove  file:///
		}

		if (stateExtension != null && stateExtension.length == 0)
			stateExtension = null;
		if (property == "behaviour") {
			var behaviourJS = this.findBehaviour(stateType, stateExtension, propertyValue, applicationPath);
			return behaviourJS;
		}
		if (property == "view") {
			var viewJS = this.findView(stateType, stateExtension, propertyValue, applicationPath);
			return viewJS;
		}
		if (property == "screen") {
			var screenURL = this.findScreen(stateType, stateExtension, propertyValue, applicationPath);
			return screenURL;
		}
		if (property == "requires") {
			var scriptURL = this.findScript(stateType, stateExtension, propertyValue, applicationPath);
			return scriptURL;
		}
		if (property == "application") {
			return propertyValue;
		}
		return propertyValue;
	}

	this.constructStateChain = function(stateType, stateExtension) {
		// Значиня для state по умолчанию, ничего не переопределено
		function TargetState() {
			this.behaviour = null;
			this.view = null;
			this.screen = null;
			this.requires = null;
			this.application = null;
			this.inheritanceChain = null;
			this.stateType = null;
		}
		var targetState = null;
		
		var inheritance = new Array();
		var stateName = stateType + '-' + stateExtension;
		if (!states.hasOwnProperty(stateName)) {
			// Нет переопределенного стейта с extension
			stateName = stateType;
		}

		//var b = stateExtension && stateExtension.indexOf("IssueOfCash") >= 0;
		
		// Собираем цепочку наследования
		var stateParent = stateName;
		do {
			if (states[stateParent] == null) {
				stateParent = null;
//				if (b) alert(0);
			}
			else if (typeof(states[stateParent]) === "string") {
				stateParent = states[stateParent];
				// if (b) alert(1);
			}
			else if (states[stateParent].hasOwnProperty("extends")) {
				stateParent = states[stateParent]["extends"];
				// if (b) alert(2);
			}
			else {
				stateParent = null;
				// if (b) alert(3);
			}
			if (stateParent != null) {
				inheritance.unshift(stateParent);
			}
		} while (stateParent != null)
		inheritance.push(stateName);
		// if (b) alert(inheritance);

		// Цепочка готова, начинаем сначала 
		var inheritanceChain = "";
		for (var i = 0; i < inheritance.length; i++) {
			inheritanceChain += inheritance[i]+"->";
			// Корень
			if (targetState == null) {
				targetState = new TargetState();
				for (var p in targetState) {
					var state = states.hasOwnProperty(inheritance[i]) ? states[inheritance[i]] : null;
					if (state != null && state.hasOwnProperty(p)) {
						targetState[p] = this.getStateProperty(inheritance[i], stateExtension, p, state[p])
					} else {
						targetState[p] = this.getStateProperty(inheritance[i], stateExtension, p, null)
					}
				}
				continue;
			}
			for (var p in targetState) {
				// Обрабатываем свойства стейта
				if (states[inheritance[i]].hasOwnProperty(p)) {
					// Значение из цепочки
					targetState[p] = this.getStateProperty(inheritance[i], stateExtension, p, states[inheritance[i]][p]);
				} else {
					//targetState[p] = this.getStateProperty(inheritance[i], stateExtension, p, null);
				}
/*				var state = states.hasOwnProperty(inheritance[i]) ? states[inheritance[i]] : null;
				if (state != null && state.hasOwnProperty(p)) {
					targetState[p] = this.getStateProperty(inheritance[i], stateExtension, p, state[p])
				} else {
					targetState[p] = this.getStateProperty(inheritance[i], stateExtension, p, null)
				}*/
			}
		}

		targetState.stateType = stateType;
		targetState.inheritanceChain = inheritance;

		var stateImpl = stateName;
		for (var p in targetState) {
			stateImpl += "\n" + p + ":" +targetState[p];
		}
		//if (b) alert(stateImpl);
		return this.statesCache[stateName] = targetState;
	}

	this.getState = function(stateType, stateExtension) {
	
		var stateName = stateType + (stateExtension != null ? '-' +  stateExtension : "");
		if (!states.hasOwnProperty(stateName)) {
			// Нет переопределенного стейта с extension
			stateName = stateType;
		}

		if (this.statesCache.hasOwnProperty(stateName)) {
			return this.statesCache[stateName];
		}
		this.constructStateChain(stateType, stateExtension);
		getLogger("States").debug("State " + stateType + "-" + stateExtension + " constructed");
		return this.statesCache[stateName];
	}
};

function appendStates(additionalStates) {
	if (applicationStates.hasOwnProperty(activeApplication)) {
		for (var s in additionalStates) {
			// Добавляем новое состояние только, если оно не добавлено ранее для того, чтобы можно было переопределить состояние
			// в начальном файле приложения
			if (!additionalStates.hasOwnProperty(s) || applicationStates[activeApplication].states.hasOwnProperty(s))
				continue;
			applicationStates[activeApplication].states[s] = additionalStates[s];
		}
	} else {
		applicationStates[activeApplication] = new ApplicationStates(activeApplication, additionalStates);
	}
}

