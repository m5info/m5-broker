﻿var appLogger = getLogger("application");
var applications = {"paybox" : null
		, "encashment" : null 
		, "encashment-web" : null 
		, "service" : null 
		, "external-link" : null 
		};


var Framework = KBExternal.Framework ? KBExternal.Framework : {'Offline' : true};
var VirtuPOS = KBExternal.VirtuPOS ? KBExternal.VirtuPOS : {'Offline' : true};
var PaymentDevice = KBExternal.PaymentDevice ? KBExternal.PaymentDevice : {'Offline' : true};
var FiscalPrinter = KBExternal.FiscalPrinter ? KBExternal.FiscalPrinter : {'Offline' : true};
var ReceiptPrinter = KBExternal.ReceiptPrinter ? KBExternal.ReceiptPrinter : {'Offline' : true};
var FingerScaner = KBExternal.FingerScaner ? KBExternal.FingerScaner : {'Offline' : true};
var BarcodeReader = KBExternal.BarcodeReader ? KBExternal.BarcodeReader : {'Offline' : true};

(function() {
var pathCache = {};
this.Framework_IfExistsThen = function (path, elsePath, context) {
	// var b = path.indexOf("IssueOfCash") >= 0 || elsePath.indexOf("IssueOfCash") >= 0;
// if (b)
	// alert(path+" or "+elsePath);
	if (pathCache.hasOwnProperty(path))
		return pathCache[path];
	if (context.location.href.indexOf('http://') != 0)
		return Framework.IfExistsThen(path, elsePath, context);

	var fullPath;
	if (path.indexOf('http://') == 0)
		fullPath = path;
	else {
		fullPath = context.location.href;
		fullPath = fullPath.substring(0, fullPath.lastIndexOf('/')+1);
		fullPath += path;
	}
	
	var oReq = new XMLHttpRequest();
	oReq.open("HEAD", fullPath, false);
	oReq.send();
	pathCache[path] = oReq.status == 200 ? fullPath : elsePath;
	return pathCache[path];
}
})()


var PaymentGateway = KBExternal.PaymentGateway ? KBExternal.PaymentGateway : {'Offline' : true};
var EWM = KBExternal.EWM ? KBExternal.EWM : {'Offline' : true};

(function () {
	if (!FingerScaner.Offline)
		FingerScaner.ImageFileFolder = "C:\\PayKiosk\\skins\\paybox\\screens\\images";
})()


var activeApplication = null;

var selectedApplication = null;
function getSelectedApplication() {
	return selectedApplication;
}

var applicationDocument = null;
function getApplicationDocument() {
	return applicationDocument;
}

var kioskTop = this;
function getTopWindow() {
	return kioskTop;
}

function updateFrameset(app) {
	// Create frame
	//var frameSet = kioskTop.document.getElementById("kioskFrameset");
 	
	//while(frameSet.hasChildNodes())
	//	frameSet.removeChild(frameSet.childNodes[0]);
	
	//frameSet.rows = "";
	/*
	for (var frameID in applications[app].frames) {
		var frame = kioskTop.document.createElement("frame");
		frame.id = frameID;
		
		if (frameSet.rows.length != 0)
			frameSet.rows += ",";
			
		frameSet.rows += applications[app].frames[frameID];
		frameSet.appendChild(frame);
	}
	*/
}

// Установка текущего приложения
// app -- в какое приложение или ../<outcome> для перезапуска или выхода на уровень выше
// currentState -- из какого состояния
function setActiveApplication(app, currentState) {
	 //alert("Application: " + app + ", currentState: " + currentState);
/*
	var entryState = null, stateExtension = null;

	var idxUpLevel = app.indexOf('../');
	var returnFromApp = idxUpLevel >= 0;
	var restartApp = selectedApplication === null;
	
	var outcome = null;
	
	if (idxUpLevel >= 0) {
		outcome = app.length > 3 ? app.substring(idxUpLevel+3, app.length-idxUpLevel+3) : "complete";
		app = null;
	}
	
	if (app === selectedApplication) {
		appLogger.error("Приложение " + app + " уже активировано");
		return false;
	}
	
	if (!app) {
		var stackItem = Framework.Variables("ApplicationStack").pop();
		
		if (stackItem) {
			app = stackItem.application;
			entryState = stackItem.state;
			stateExtension = stackItem.extension;
		} else {
			app = selectedApplication;
			restartApp = true;
			entryState = null;
			stateExtension = null;
			appLogger.debug("Перезапускаем приложение " + app);
		}
	}
	
	if (!applications.hasOwnProperty(app) || !applications[app]) {
		appLogger.error("Приложение " + app + " не найдено");
		return false;
	}
	
	if (app !== selectedApplication) {
		updateFrameset(app);
		if (applications[app].frames.hasOwnProperty('templates')) {
			//kioskTop.templates.location.href = app + "/screens/_templates.html";
		}
		if (kioskTop.templates)
			kioskTop.document.getElementById('templates').noResize = true;

	}
	
	if (!entryState) {
		entryState = typeof(applications[app].entryPoint) === 'function' ? applications[app].entryPoint() : applications[app].entryPoint;
		if (!entryState) {
			appLogger.error("Невозможно запустить приложение " + app);
			return false;
		}
	}
	applicationDocument = typeof(applications[app].viewDocument) === 'function' ? 
		applications[app].viewDocument() : 
		kioskTop.frames[applications[app].viewDocument];
	
	var stateInstance = applicationStates[app].getState(entryState, stateExtension);

	appLogger.info("Приложение '" + selectedApplication + "' из состояния '" + (currentState ? (currentState.type + (currentState.extension != null ? "-" + currentState.extension : "")) : "не определено") + "' переходит в '" + app + "' начиная с состояния " + entryState);
	
	if (!returnFromApp && !restartApp)
		Framework.Variables("ApplicationStack").push(currentState !== null
			? { "application" : selectedApplication, "state" : currentState.type, "extension" : currentState.extension }
			: { "application" : selectedApplication, "state" : null, "extension": null });

	selectedApplication = app;
	
	//applicationDocument.focus();
	
	if (returnFromApp && !restartApp)
		navigateNextState({"type" : entryState, "extension" : stateExtension}, outcome);
	else {
		//applicationDocument.location.href = stateInstance.screen + "?state="+stateInstance.stateType+(stateExtension != null ? "&extension="+stateExtension : "");
	}
*/
	return true;
}
function getApplicationState(stateType, stateExtension) {
	if (!selectedApplication) {
		appLogger.error("Активное приложение отсутствует. Переход в состояние "+stateType+" невозможен");
		return;
	}
	if (!applicationStates.hasOwnProperty(selectedApplication)) {
		appLogger.error("Приложение "+selectedApplication+" не найдено");
		return;
	}
	return applicationStates[selectedApplication].getState(stateType, stateExtension);
}


function appendApplication(frames, viewDocument, entryPoint, additionalScripts, customCSS) {
	var applicationTemplates = Framework_IfExistsThen(activeApplication + "/screens/_templates.html", "zzz", window);
	if (applicationTemplates !== "zzz") {
		frames['templates'] = 0;
	}

	applications[activeApplication] = {
		"frames" : frames,
		"entryPoint" : entryPoint,
		"viewDocument" : viewDocument,
		"additionalScripts" : additionalScripts,
		"customCSS" : customCSS
	};
}

/* Просто достаем объекты изнутри */
function getApplicationCSS() {
	if (selectedApplication != null)
		return applications[selectedApplication]["customCSS"];
}
function getAdditionalScripts() {
	if (selectedApplication != null)
		return applications[selectedApplication]["additionalScripts"];	
}

/* Точка входа тут. Грузятся все приложения */
(function () {
	window.attachEvent('onload', 
		function() {
			function onInitialLoadComplete() {
			
				activeApplication = null;
				for (var app in applications) {
					if (!applications.hasOwnProperty(app)) {
						appLogger.error("Приложение " + app + " не определено");
						continue;
					}
					
					if (!applications[app]) {
						appLogger.error("Приложение " + app + " не проинициализировано");
						continue;
					}
					
					if (!applications[app].entryPoint) {
						appLogger.error("Не определена точка входа в приложение " + app);
						continue;
					}
					// Enter application start state
					setActiveApplication(app);
					return;
				}
				appLogger.error("Нет доступных приложений для загрузки");
				return;
			}
					
			// Загрузка скриптов
			function appendScript() {
				var scriptFile = scripts.shift();
				if (scriptFile == null) {
					// Все скрипты загружены
					onInitialLoadComplete();
					return;
				}
				var scriptElement = document.createElement('script');
				
				scriptElement.language = 'javascript';
				scriptElement.type = 'text/javascript';
				scriptElement.src = scriptFile;

				appLogger.debug('Загружается скрипт: "' + scriptElement.src + '"');

				if (scriptElement.addEventListener) {
					scriptElement.addEventListener('load', appendScript, false);
					scriptElement.addEventListener('error', appendScript, false);
				} else
					scriptElement.attachEvent('onreadystatechange', function() {
						if (scriptElement.readyState.match(/loaded|complete/)) {
							appendScript();
						}
					});
				
				var scriptPathArray = scriptFile.split("/");
				activeApplication = scriptPathArray[scriptPathArray.length - 2];
				document.getElementsByTagName('head')[0].appendChild(scriptElement);
			}
		
			// Application callstack
			//Framework.Variables("ApplicationStack") = "";
			Framework.Variables("ApplicationStack") = [];

			// Search and load applications
			var scripts = new Array();
			
			// Preload Application, Scenario, States and custom CSS
			for (var app in applications) {
				var applicationEntry = Framework_IfExistsThen(app + "/" + app + ".js", "zzz", window);
				if (applicationEntry == "zzz") {
					appLogger.error("Невозможно загрузить приложение " + app + ". Отсутсвует файл " + app + ".js");
					continue;
				}

				var customStates = Framework_IfExistsThen(app + "/states.js", "zzz", window);
				if (customStates == "zzz") {
					appLogger.error("Невозможно загрузить приложение " + app + ". Отсутсвует файл states.js");
					continue;
				}
				
				var applicationParameters = Framework_IfExistsThen(app + "/parameters.js", "zzz", window);
				
				var applicationScenario = Framework_IfExistsThen(app + "/scenario.js", "zzz", window);
				if (applicationScenario == "zzz") {
					appLogger.error("Невозможно загрузить приложение " + app + ". Отсутсвует файл scenario.js");
					continue;
				}
							
				scripts.push(applicationEntry);
				scripts.push(customStates);
				if (applicationParameters != "zzz")
					scripts.push(applicationParameters);
				scripts.push(applicationScenario);

				// locales
				for (var l = 0; l < supportedLocales.length; l++) {
					var locale = Framework_IfExistsThen(app + "/locale_"+supportedLocales[l]+".js", "zzz", window);
					if (locale != "zzz")
						scripts.push(locale);
				}
			}
			appendScript();
	});
}
)()

