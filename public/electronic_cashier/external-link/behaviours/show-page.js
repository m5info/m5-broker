﻿_.addEventListeners({
	"load" : function() {
		_.registerActions({
			"back" : function() {history.back();},
			"forward" : function() {history.forward();},
			"exit" : function () { 
				hideKeyboard();
				_.leave('exit'); 
			}
		});
		parent.frames["external-link"].location.href = PaymentGateway.SelectedCatalogItem.HREF;
	}
});
