﻿appendNavigationRules({
	// Общие выходы для всех состояний
	"*" : {
		// переход в offline
		"offline" : "offline"
	},

	"show-page" : {
		"exit" : "../complete"
	}
});
