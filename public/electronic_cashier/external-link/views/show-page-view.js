﻿_.addEventListeners({
	"load" : function () {
		ViewButtons.bind({
			'back-page' : 'back', 
			'forward-page' : 'forward', 
			'exit' : 'exit', 
			'keyboard' : isKeyboardPresent() ? function() {
				if (isKeyboardShow()) {
					hideKeyboard();
					document.getElementById('keyboard').className = "";
				} else {
					showKeyboard();
					document.getElementById('keyboard').className = "hide";
				}}
				: null
			});
	}
});