﻿var elka_id = 0; // ID Кассира сделать автризацию но пока не придумал как
var state_scan = 0; // Статус сканера 1 не реогировать на события сканера штрих кода
var agent_id = 0;
var my_cashbox_id = 0;
var my_cashbox_payment_value = 0;
var total_payment_value = 0;
var my_cashbox_total_agent_kv = 0;




function init_elka(){
	var query = getQueryParams(document.location.search);
	elka_id = query.id;
	getOpenMain();
}


function getOpenCollection(user_id){
	state_scan = 1;
	getTemplates('collection', 'elka_id='+elka_id+'&user_id='+user_id);
}



function getPaymentCashBox(cashbox_id){
	state_scan = 1;
	getTemplates('payment_cashbox', 'id='+cashbox_id);
	
	agent_id = $('#agent_id').val();
	my_cashbox_id = $('#my_cashbox_id').val();
	my_cashbox_payment_value = $('#payment_value').val();
	my_cashbox_total_agent_kv = $('#total_agent_kv').val();
	
	button_payment_control('butt_payment', false);
	button_payment_control('butt_balance', false);
	
	openPayment();
	
}

function getPaymentCashBoxBalance(cashbox_id, agent_id){
    state_scan = 1;

    
    var arr = (myPostAjax('m_controllers/payment_cashbox_balance.php', 'cashbox_id='+cashbox_id+'&elka_id='+elka_id+'&agent_id='+agent_id));
	printPaymentCashBoxBalance(arr);
	getOpenMain();
}




function button_payment_control(name, state){//name - id кнопки, state bool вкл или выкл
	//butt_payment = оплатить счет
	//butt_balance = Зачислить на баланс
	if(state == true){
		$("#"+name).show();
	}else{
		$("#"+name).hide();
	}
}


function getOpenCashBox(cashbox_id){
	state_scan = 1;
	getTemplates('cashbox', 'id='+cashbox_id);
}

function getOpenUserBalance(agent){
	state_scan = 1;
	getTemplates('userbalance', 'id='+agent);
	openPayment();
	button_payment_control('butt_balance', false);
	agent_id = agent;
}

function getOpenMain(){
	state_scan = 0;
	//closePayment();
	getTemplates('main');
}

function getTemplates(name, param){ // name - название шаблона param - параметры a=1&b=2
	agent_id = 0;
	my_cashbox_id = 0;
	my_cashbox_payment_value = 0;
	my_cashbox_total_agent_kv = 0;
	total_payment_value = 0;
	alert('m_templates/'+name+', '+param);
	//$('#container_ajax').html(myPostAjax('m_templates/'+name, param));
	
}


function getQueryParams(qs) {
    qs = qs.split('+').join(' ');

    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }

    return params;
}


function get_datetime(){
	dateObj = new Date();
	d = (dateObj.getDate()>9)?dateObj.getDate():'0'+dateObj.getDate();
	m = dateObj.getMonth()+1;
	m = (m>9)?m:'0'+m;
	return d+'.'+m+'.'+dateObj.getFullYear()+' '+dateObj.getHours()+':'+dateObj.getMinutes()+':'+dateObj.getSeconds();
}


