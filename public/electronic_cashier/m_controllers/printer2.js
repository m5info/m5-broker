﻿




function printPaymentCashBox(datas){
	
	var FiscalPrinter = ReceiptPrinter;
	
	var arr = jQuery.parseJSON(datas);
	
	
	FiscalPrinter.Bold = false;
	FiscalPrinter.Underline = false;
	FiscalPrinter.WhiteBlackInversion = false;
	FiscalPrinter.LeftMargin = 2;

    FiscalPrinter.Alignment = 2; // выравнивание по центру
    FiscalPrinter.Writeln("******************************");
    FiscalPrinter.Writeln("*       M5 Back & Cloud      *");
    FiscalPrinter.Writeln("*          Терминал №1       *");
    FiscalPrinter.Writeln("* Москва, пр-т Мира, 16 стр2 *");
    FiscalPrinter.Writeln("* тел. службы помощи клиентам*");
    FiscalPrinter.Writeln("*     +7 (495) 663 91 14     *");
    FiscalPrinter.Writeln("******************************");
    
    
    FiscalPrinter.Bold = false;
    FiscalPrinter.Writeln("--------------------------------");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln("      ОПЛАТА СТРАХОВОЙ ПРЕМИИ   ");
    
    FiscalPrinter.Alignment = 0; // выравнивание по левому краю
    FiscalPrinter.Write("Дата, операции: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(get_datetime());
    FiscalPrinter.Bold = false;

    FiscalPrinter.Write("Номер чека: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.elka_counter);
    FiscalPrinter.Bold = false;

    FiscalPrinter.Write("Тип операции: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln("Оплата счета "+arr.cashbox_id);
    FiscalPrinter.Bold = false;
    
    FiscalPrinter.Write("Номер терминала: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.elka_name);
    FiscalPrinter.Bold = false;
    
    FiscalPrinter.Write("Агент: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.agent_name);
    FiscalPrinter.Bold = false;
    FiscalPrinter.Writeln("--------------------------------");
    
    FiscalPrinter.Write("Внесено: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.total_payment_value);
    FiscalPrinter.Bold = false;
    
    FiscalPrinter.Write("К оплате: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.cashbox_payment_value);
    FiscalPrinter.Bold = false;
    
    FiscalPrinter.Write("Баланс: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.balance_agent);
    FiscalPrinter.Bold = false;
    /*
    FiscalPrinter.Write("Сдача: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.balance_short_change);
    FiscalPrinter.Bold = false;
    */
    
    FiscalPrinter.Writeln("--------------------------------");
    FiscalPrinter.Writeln("--------------------------------");
    
    
    FiscalPrinter.Cut(0);
	
}

function printBalance(datas){
	var FiscalPrinter = ReceiptPrinter;
	
	var arr = jQuery.parseJSON(datas);
	
	
	FiscalPrinter.Bold = false;
	FiscalPrinter.Underline = false;
	FiscalPrinter.WhiteBlackInversion = false;
	FiscalPrinter.LeftMargin = 2;

    FiscalPrinter.Alignment = 2; // выравнивание по центру
    FiscalPrinter.Writeln("******************************");
    FiscalPrinter.Writeln("*       M5 Back & Cloud      *");
    FiscalPrinter.Writeln("*          Терминал №1       *");
    FiscalPrinter.Writeln("* Москва, пр-т Мира, 16 стр2 *");
    FiscalPrinter.Writeln("* тел. службы помощи клиентам*");
    FiscalPrinter.Writeln("*     +7 (495) 663 91 14     *");
    FiscalPrinter.Writeln("******************************");
    
    FiscalPrinter.Bold = false;
    FiscalPrinter.Writeln("--------------------------------");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln("       ЗАЧИСЛЕНИЕ НА БАЛАС     ");
    
    
    FiscalPrinter.Alignment = 0; // выравнивание по левому краю
    FiscalPrinter.Write("Дата, операции: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(get_datetime());
    FiscalPrinter.Bold = false;

    FiscalPrinter.Write("Номер чека: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.elka_counter);
    FiscalPrinter.Bold = false;

    FiscalPrinter.Write("Тип операции: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln("Зачисление на счет");
    FiscalPrinter.Bold = false;
    
    FiscalPrinter.Write("Номер терминала: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.elka_name);
    FiscalPrinter.Bold = false;
    
    FiscalPrinter.Write("Агент: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.agent_name);
    FiscalPrinter.Bold = false;
    FiscalPrinter.Writeln("--------------------------------");
    
    FiscalPrinter.Write("Назначение платежа: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln("Зачисление на баланс");
    
    
    FiscalPrinter.Write("Внесено: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.total_payment_value);
    FiscalPrinter.Bold = false;
    
    FiscalPrinter.Writeln("--------------------------------");
    FiscalPrinter.Writeln("--------------------------------");
    
    
    FiscalPrinter.Cut(0);
	
}




function printCollection(user_id){
	var FiscalPrinter = ReceiptPrinter;
	
	var arr = jQuery.parseJSON(myPostAjax('m_controllers/collection.php', 'get_print=1&elka_id='+elka_id+'&user_id='+user_id));
	
	FiscalPrinter.Bold = false;
	FiscalPrinter.Underline = false;
	FiscalPrinter.WhiteBlackInversion = false;
	FiscalPrinter.LeftMargin = 2;

    FiscalPrinter.Alignment = 2; // выравнивание по центру
    FiscalPrinter.Writeln("******************************");
    FiscalPrinter.Writeln("*       M5 Back & Cloud      *");
    FiscalPrinter.Writeln("*          Терминал №1       *");
    FiscalPrinter.Writeln("* Москва, пр-т Мира, 16 стр2 *");
    FiscalPrinter.Writeln("* тел. службы помощи клиентам*");
    FiscalPrinter.Writeln("*     +7 (495) 663 91 14     *");
    FiscalPrinter.Writeln("******************************");
    
    FiscalPrinter.Alignment = 0; // выравнивание по левому краю
    FiscalPrinter.Write("Дата, Инкассация: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(get_datetime());
    FiscalPrinter.Bold = false;

    FiscalPrinter.Write("Номер терминала: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.elka_name);
    FiscalPrinter.Bold = false;
    
    FiscalPrinter.Write("Инкассатор: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.user_name);
    FiscalPrinter.Bold = false;
    FiscalPrinter.Writeln("--------------------------------");
    
    
    FiscalPrinter.Write("Количество купюр: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.face_count);
    FiscalPrinter.Bold = false;
    
    
    FiscalPrinter.Write("Номинал 10: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.face_count_10+'/'+arr.face_sum_10);
    FiscalPrinter.Bold = false;
    
    
    FiscalPrinter.Write("Номинал 50: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.face_count_50+'/'+arr.face_sum_50);
    FiscalPrinter.Bold = false;
    
    
    FiscalPrinter.Write("Номинал 100: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.face_count_100+'/'+arr.face_sum_100);
    FiscalPrinter.Bold = false;


    FiscalPrinter.Write("Номинал 200: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.face_count_200+'/'+arr.face_sum_200);
    FiscalPrinter.Bold = false;
    
    
    FiscalPrinter.Write("Номинал 500: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.face_count_500+'/'+arr.face_sum_500);
    FiscalPrinter.Bold = false;
    
    
    FiscalPrinter.Write("Номинал 1000: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.face_count_1000+'/'+arr.face_sum_1000);
    FiscalPrinter.Bold = false;


    FiscalPrinter.Write("Номинал 2000: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.face_count_2000+'/'+arr.face_sum_2000);
    FiscalPrinter.Bold = false;
    
    
    FiscalPrinter.Write("Номинал 5000: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.face_count_5000+'/'+arr.face_sum_5000);
    FiscalPrinter.Bold = false;
    
    
    FiscalPrinter.Write("Сумма: ");
    FiscalPrinter.Bold = true;
    FiscalPrinter.Writeln(arr.total_payment_value);
    FiscalPrinter.Bold = false;
    
    FiscalPrinter.Writeln("--------------------------------");
    FiscalPrinter.Writeln("--------------------------------");
    
    FiscalPrinter.Cut(0);
    return true;
}
