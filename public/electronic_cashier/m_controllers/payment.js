﻿var prev_total_summ = 0;


function setPaymentValue(face_value){
	
	myPostAjax('m_controllers/payment_state_logs.php', 'elka_id='+elka_id+'&agent_id='+agent_id+'&cashbox_id='+my_cashbox_id+'&face_value='+face_value);
	
	//my_cashbox_payment_value = 0;
	total_payment_value += face_value;
	
	$('#payment_total_value_view').html(CommaFormatted(total_payment_value));
	
	button_payment_control('butt_back', false);
	
	if(my_cashbox_id>0){
		if(total_payment_value>=my_cashbox_payment_value){
			button_payment_control('butt_balance', false);
			button_payment_control('butt_payment', true);
		}else{
			button_payment_control('butt_balance', true); // Зачислить на баланс если денег не хватает
			button_payment_control('butt_payment', false);
		}
	}else{
		button_payment_control('butt_balance', true); // Зачислить на баланс если денег не хватает
	}
}


function setPaymentCashBoxTest2(){
	//Оплата счета
	var arr = (myPostAjax('m_controllers/payment_cash.php', 'state=1&elka_id='+elka_id+'&agent_id=10&cashbox_id=63207&summ=100&cashbox_payment_value=90&kv_agent=10'));
	
	closePayment();
	printPaymentCashBox(arr);
	getOpenMain();
}

function setPaymentCashBoxTest(){
	//Оплата счета
	var arr = (myPostAjax('m_controllers/payment_cash2.php', 'state=1&elka_id='+elka_id+'&agent_id='+agent_id+'&cashbox_id='+my_cashbox_id+'&summ=100&cashbox_payment_value='+my_cashbox_payment_value+'&kv_agent='+my_cashbox_total_agent_kv));
	
	closePayment();
	printPaymentCashBox(arr);
	getOpenMain();
}

function setPaymentCashBox(){
	//Оплата счета
	var arr = (myPostAjax('m_controllers/payment_cash.php', 'state=1&elka_id='+elka_id+'&agent_id='+agent_id+'&cashbox_id='+my_cashbox_id+'&summ='+total_payment_value+'&cashbox_payment_value='+my_cashbox_payment_value+'&kv_agent='+my_cashbox_total_agent_kv));
	
	closePayment();
	printPaymentCashBox(arr);
	getOpenMain();
}

function setPaymentBalance(){
	// Зачисление на баланс
	var arr = (myPostAjax('m_controllers/payment_cash.php', 'state=0&elka_id='+elka_id+'&agent_id='+agent_id+'&cashbox_id='+my_cashbox_id+'&summ='+total_payment_value));
	
	closePayment();
	printBalance(arr);
	getOpenMain();
}

function setPaymentBalance2(){
	// Зачисление на баланс
	
	//agent_id = $('#agent_id').val();
	
	var arr = (myPostAjax('m_controllers/payment_balance.php', 'state=0&elka_id='+elka_id+'&agent_id='+agent_id+'&summ='+total_payment_value));
	
	closePayment();
	printBalance(arr);
	getOpenMain();
}

function setCollection(user_id){
	// Инкассация
	/*
	if(printCollection(user_id) == true){
		myPostAjax('m_controllers/collection.php', 'elka_id='+elka_id+'&user_id='+user_id);
	}
	*/
    myPostAjax('m_controllers/collection.php', 'elka_id='+elka_id+'&user_id='+user_id);

	getOpenMain();
}


function openPayment(){
	//PaymentDevice
	//PaymentDevice.CancelPayment();
	//PaymentDevice.ChangeSum=0;
	prev_total_summ = 0;
	/*
	if (PaymentDevice.IsReady) {
		
	}else{
		PaymentDevice.AcceptPayment(99999999999999999);
	}
	*/

	//if(PaymentDevice)
		PaymentDevice.AcceptPayment(99999999999999999);
	
}


function PaymentDevice::OnAccumulatedSumChanged() {
	
	var accSum = '';
	if (PaymentDevice.HasAccumulatedSum)
	{
		accSum = (PaymentDevice.AccumulatedSum(PaymentDevice.AccumulatedSum.Count-1).Value / 100)-prev_total_summ;
		prev_total_summ = PaymentDevice.AccumulatedSum(PaymentDevice.AccumulatedSum.Count-1).Value / 100;
	}
	//alert("OK:"+accSum);
	setPaymentValue(accSum);
}



function closePayment(){
	
	for (var i = 0; i < PaymentDevice.AccumulatedSum.Count; ++i){
		PaymentDevice.AccumulatedSum(i).Value = 0;
	}
	
	try {
		PaymentDevice.CancelPayment();
	} catch(e) {
		//ThrowLog("CommitPayment", e);
	}
	
	prev_total_summ = 0;
}
