﻿var ViewButtons = {
	/**
	"Привязывает" действия к кнопкам. В качестве параметра передается ассоциативный массив с ключами по идентификаторам кнопки.
	Значением может быть:
	1. Название действия (просто строка, например 'commit')
	2. Функция (как было ранее)
	3. Массив в начале которого перечислены действия в виде строк, а последним элементом может присутсвовать функция, например 'commit', function() { alert ('commit not possible') }
	4. null (кнопка всегда недоступна)
	Доступность кнопки пересчитывается с учетом _.mayExecute для конкретного действия. В списках срабатывает первое доступное действие.
	Для изменения доступности кнопок достаточно использовать ViewButtons.update()
	
	Пример использования (большинство мест в коде)
	
	В обработчике load
	ViewButtons.bind({'cancelButton' : 'cancel', 'continueButton' : 'commit'});
	
	В обработчике actionsChanged (и в других местах, где изменяется доступность действий)
	ViewButtons.update()
	
	*/
	"bind" : function(buttons) {
		var arrayExecuteBinder = function(actionsSequence, actionsSequenceLength, defaultFunction) {
			return function() {
				for (var i=0, c=actionsSequenceLength; i < c; i++) {
					if (_.mayExecute(actionsSequence[i])) {
						_[actionsSequence[i]]();
						return;
					}
				}
				if (defaultFunction) defaultFunction();
			}
		};
		
		this.active = {};
	
		for (var buttonName in buttons) {
			if (!buttons.hasOwnProperty(buttonName)) continue;
			
			var buttonElement = document.getElementById(buttonName);
			if (!buttonElement) continue;
			
			var action = buttons[buttonName];
			var actionType = typeof action;
			
			if (action) {
				if (Object.prototype.toString.call(action) === "[object Array]") {
					// На кнопку привязано несколько действий. Активно будет первое доступное
					var conditions = [];
					for (var i = 0, c = action.length; i<c; i++) {
						actionType = typeof action[i];
						if (actionType === 'string') {
							conditions.push('_.mayExecute("' + action[i] + '")');
						} else if (actionType === 'function') {
							// если в списке есть просто функция, то кнопка всегда доступна (и после функции дальше мы не смотрим, так как все равно не выполнится
							this.active[buttonName] = true;
							
							// если есть условные действия до функции, то строим сложный обработчик
							if (conditions.length)
								buttonElement.onclick = arrayExecuteBinder(action, i, action[i]);
							else
								// если нет, то это просто вызов функции
								buttonElement.onclick = action[i];
							
							break;
						}
					}
					
					if (!this.active[buttonName]) {
						// безусловной функции в списке действий нет
						switch(conditions.length) {
							case 0:
								// и нет ни одного условного действия
								this.active[buttonName] = false;
								buttonElement.onclick = null;
								break;
							case 1:
								// и есть ровно одно условное действие
								this.active[buttonName] = new Function("return _.mayExecute('" + action[0] + "')");
								buttonElement.onclick = new Function("return _['" + action[0] + "']()");
								break;
							default:
								// и есть больше чем одно условное действие
								this.active[buttonName] = new Function("return " + conditions.join('||'));
								buttonElement.onclick = arrayExecuteBinder(action, i);
						}
					}
				}
				else {
					// На кнопку привязано одной действие (либо условное, либо безусловное)
					if (actionType === 'string') {
						this.active[buttonName] = new Function("return _.mayExecute('" + action + "')");
						buttonElement.onclick = new Function("return _['" + action + "']()");
					} else if (actionType === 'function') {
						this.active[buttonName] = true;
						buttonElement.onclick = action;
					}
				}
			} else {
				// упомянут, но полностью отключен
				this.active[buttonName] = false;
				buttonElement.onclick = null;
				continue;
			}
		}
		
		this.update();
	},
	
	/**
	Обновляет доступность кнопок (включает или выключает)
	*/
	"update" : function() {
		// не изменять ViewButtons на this, т.к. может быть вызвано через fireEvent, который переопределяет this
		var activeButtons = ViewButtons.active;

		for (var buttonName in activeButtons) if (activeButtons.hasOwnProperty(buttonName)) {
			var buttonEnabled = activeButtons[buttonName] && (typeof activeButtons[buttonName] === 'boolean' ? activeButtons[buttonName] : activeButtons[buttonName]());
			var buttonElement = document.getElementById(buttonName);
			
			if (!buttonElement) continue;
			
			var classes = buttonElement.className.split(' ');
			if (buttonEnabled) {
				// Remove -disabled
				for (var i = 0; i < classes.length; i++)
					if (classes[i].indexOf("-disabled") > 0) classes[i] = classes[i].replace("-disabled", "");
				buttonElement.disabled = false;
			} else {
				if (classes[classes.length-1].indexOf("-disabled") < 0) classes[classes.length-1] += "-disabled";
				buttonElement.disabled = true;
			}
				buttonElement.className = classes.join(' ');
		}
	}
};

function bindButtons(buttons) {
	var availableButtons = ['cancelButton', 'continueButton', 'nextField', 'prevField'];
	
	// Временно эмулируем старое поведение (гашение неупомянутых кнопок)
	if (!buttons) buttons = {};
	
	for (var i = 0, c = availableButtons.length; i < c; i++) {
		if (!buttons.hasOwnProperty(availableButtons[i]))
			buttons[availableButtons[i]] = null;
	}
	
	ViewButtons.bind(buttons);
}

function flash(o, c, t) {
	c = c || 4;
	var interval = setInterval( function() {
				if (o.style.visibility == 'visible')
					o.style.visibility = 'hidden';
				else {
					o.style.visibility = 'visible';
					--c;
				}
				if (!c) clearInterval(interval);
			}, t || 200);
}

// в нижеследующих функциях жёсткий html будет переписан на динамический
function showWaitClock() {
	var clockElement = document.getElementById("waitClockDiv");
	if (clockElement == null) {
		clockElement = document.createElement("div");
		clockElement.id = "waitClockDiv";
		clockElement.className = "waitClockDiv";
		clockElement.innerHTML = '<table class="waitClockTable" width="100%" height="100%"><tr valign="middle"><td align="center"><img src="images/clock.png"></td></tr></table>';
		document.body.appendChild(clockElement);
	}
	clockElement.style.visibility = "visible";
}
function hideWaitClock() {
	var clockElement = document.getElementById("waitClockDiv");
	if (clockElement != null)
		clockElement.style.visibility = "hidden";
}

function loadBanner(show) {
	var bannerElement = document.getElementById("banner");
	if (bannerElement != null) 
		bannerElement.innerHTML = getParameter("paykiosk.showBanner", true) && show == true ? '<embed src="images/banner.swf" type="application/x-shockwave-flash" width=100% height=100 onfocus="focus()"/>' : '<table height="100" width="100%" bgcolor="white"><tr><td>&nbsp;</td></tr></table>';
}
// Вывод логотипа с координатами и названием картинки
function showLogo(logoX, logoY, logoName) {
    var parent = document.getElementsByTagName('body')[0];
    var logoElement = document.createElement('div');
			logoElement.style.top = logoX;
			logoElement.style.left = logoY;
			logoElement.style.zIndex = "2010";
			logoElement.style.position = "absolute";
			logoElement.innerHTML = "<img src='images/"+logoName+".png' border=0>";
			parent.appendChild(logoElement);
}