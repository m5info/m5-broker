﻿var clockRedrawIntervalID = null;
var clockCustomHTML = "<embed src='./images/clock.swf' width=100px height=100px wmode='transparent' type='application/x-shockwave-flash'>";
//var clockCustomHTML = null;
function showClock() {
	function getClockHTML() {
		if (clockCustomHTML != null) {
			if (typeof clockCustomHTML == 'function')
				return clockCustomHTML();
			else
				return clockCustomHTML;
		}
		return formatDateTime(new Date(),"HH:MM:SS");
	}
	function formatDateTime(dateTime, mask) {
		var dateTime = new Date(dateTime);
		var dt = {
			'dd' : dateTime.getDate(),
			'm'  : dateTime.getMonth() < 8 ? "0"+(dateTime.getMonth()+1) : (dateTime.getMonth()+1),
			'y'  : dateTime.getYear() < 1000 ? dateTime.getYear()+1900 : dateTime.getYear(),
			'h'  : dateTime.getHours() < 10 ? "0" + dateTime.getHours() : dateTime.getHours(), 
			'mm' : dateTime.getMinutes() < 10 ? "0" + dateTime.getMinutes() : dateTime.getMinutes(),
			'ss' : dateTime.getSeconds() < 10 ? "0" + dateTime.getSeconds() : dateTime.getSeconds()
		}
		if (mask)
			return mask.replace("dd", dt.dd).replace("mm", dt.m).replace("yyyy", dt.y).replace("HH", dt.h).replace("MM", dt.mm).replace("SS", dt.ss);
		else 	
			return dt.dd + '.' + dt.m + '.' + dt.y + ' ' + dt.h + ':' + dt.mm + ':' + dt.ss; 
	}
	
	var clockElement = document.getElementById("timeShow");
	
	if (!clockElement)
	{
		clockElement = document.createElement("div");
		clockElement.id = "timeShow";
		clockElement.className = "time";
		clockElement.innerHTML = getClockHTML();
		document.body.appendChild(clockElement);
	}
	
	clockElement.style.visibility = 'visible';

	if (clockCustomHTML != null && clockElement != null) {
		clockRedrawIntervalID = null;
		clockElement.innerHTML = getClockHTML();
	} else {
		clockRedrawIntervalID = setInterval(function() {
			clockElement.innerHTML = getClockHTML();
		}, 1000);
	}	
}

function hideClock() {
	var clockElement = document.getElementById("timeShow");
	if (clockElement) 	clockElement.style.visibility = 'hidden';//clockElement.style.display = 'none';
	if (clockRedrawIntervalID) {
		clearInterval(clockRedrawIntervalID);
		clockRedrawIntervalID = null;
	}
}

function startClock() {
	if (!getParameter("paykiosk.clockOn", true))
		return;
	showClock();
};
