﻿var messagesLogger = getLogger('messages');

// Возвращает локализованное сообщение
function getMessage(messagePath, messageName, noUndefined) {
	var messagePathArray = messagePath.split(".");
	var message = availableMessages[selectedApplication][activeLocale];
	if (!message)
		return "-= Undefined =-"
	
	if (messageName !== undefined) messagePathArray.push(messageName);
	
	for (var i = 0, c = messagePathArray.length; i < c; i++) {
		var e = messagePathArray[i];
		
		if (message.hasOwnProperty(e)) {
			message = message[e];
		} else {
			if (noUndefined === true) break;
			
			message = message['__undefined__'];
			if (message == null) break;
		}
	}
	
	if (!message) return message;
	
	// Возможность вернуть локализованный объект (например массив строк)
	if (typeof(message) != "string" && !message.hasOwnProperty("replace")) {
		return message;
	}
	
	var that = this;

	return message.replace (
		/(\\?)\$\{([^}]+)\}/g,
		function (str, escaped, exp) {
			if (escaped.length) return str.substring(1);
			else
				try {
					return that.eval(exp);
				} catch(e) {
					//messagesLogger.error('Не удалось вычислить поле "' + str + '": ' + e.message);
					return str;
				}
		}
	);
}

// Возвращает локализованное название числа с учетом склонений
function getNumberName(number, numberNamesPath, numberNamesName) {
	var numberNamesPathArray = numberNamesPath.split('.');
	var numberNames = availableNumbers[selectedApplication][activeLocale];
	
	if (numberNamesName !== undefined) numberNamesPathArray.push(numberNamesName);
	
	for (var i = 0, c = numberNamesPathArray.length; i < c; i++) {
		var e = numberNamesPathArray[i];

		if (numberNames.hasOwnProperty(e))
			numberNames = numberNames[e];
		else
			return "";
	}

	if (!numberNames.join || (numberNames.length === 1))
		// несклоняемое название
		return !numberNames.join ? numberNames : numberNames[0];
	else
		return availableNumbers[selectedApplication][activeLocale].getNumberName(number, numberNames);
}

// Группировка разрядов в целой части числа. По умолчанию на три разряда (# ### ###), 
// как происходит в большинстве стран; пример исключений: Китай, Япония - по 4 числа (### ####).
// Группы равные (так происходит в большинстве стран, однако есть исключения, к примеру, Индия, 
// где: ## ## ## ###, т.е. 3 младших разряда сгруппированы, старшие разряды группируются по 2).
// Неравная группировка не реализована.
// Разделитель групп задаётся в локали.  
function groupDigits(basic, digitSeparationPath, digitSeparationName) {
	if (typeof(basic) !== 'string')
		basic = basic.toString();
	var digitSeparationPathArray = digitSeparationPath.split('.');	
	var numberNames = availableNumbers[selectedApplication][activeLocale];

	if (digitSeparationName !== undefined) digitSeparationPathArray.push(digitSeparationName);

	for (var i = 0, c = digitSeparationPathArray.length; i < c; i++) {
		var e = digitSeparationPathArray[i];
		if (numberNames && numberNames.hasOwnProperty(e))
			numberNames = numberNames[e];
	}
	
	if (!numberNames || !numberNames.hasOwnProperty('quantity') || !numberNames.hasOwnProperty('separator'))
		numberNames = getParameter("paykiosk.digitGrouping", {"quantity" : 3, "separator" : " "});

	var arrn = basic.split('');
	// basic.length % numberNames['quantity'] = количество цифр в старших разрядах
	for (var i = basic.length % numberNames['quantity']; i < arrn.length; i = i + numberNames['quantity'])
		arrn[i] = numberNames['separator'] + arrn[i];
	return arrn.join('');
}

// ================ Рабочие переменные ====================
// Активный набор сообщений
// var messages = null;
// Активный набор для формирования числительных
// var numbers = null;

// Все наборы сообщений разбитые по языкам
var availableMessages = {};
// Все наборы числительных разбитые по языкам
var availableNumbers = {};

// Текущий выборанный язык
var activeLocale = null;

/**
Добавляет набор сообщений для определенного языка
*/
function addLocale(locale, localizedMessages, localizedNumbers) {
	if (!availableMessages.hasOwnProperty(activeApplication))
		availableMessages[activeApplication] = {};
	if (!availableNumbers.hasOwnProperty(activeApplication))
		availableNumbers[activeApplication] = {};
		
	if (!availableMessages[activeApplication].hasOwnProperty(locale)) {
		availableMessages[activeApplication][locale] = localizedMessages;
	} else {
		for (var s in localizedMessages) {
			if (!availableMessages[activeApplication][locale].hasOwnProperty(s))
				availableMessages[activeApplication][locale][s] = localizedMessages[s];
		}
	}
	if (typeof(localizedNumbers) !== 'undefined') {
		if (!availableNumbers[activeApplication].hasOwnProperty(locale)) {
			availableNumbers[activeApplication][locale] = localizedNumbers;
		} else {
			for (var s in localizedNumbers) {
				if (!availableNumbers[activeApplication][locale].hasOwnProperty(s))
					availableNumbers[activeApplication][locale][s] = localizedNumbers[s];
			}
		}
	}
	if ((activeLocale == null) || (activeLocale == locale))  {
		activeLocale = locale;
		// messages = localizedMessages;
		// numbers = localizedNumbers;
	}
}

/**
Устанавливает текущую язык
*/
function setActiveLocale(locale) {
	if (availableMessages[selectedApplication].hasOwnProperty(locale)) {
		// messages = availableMessages[selectedApplication][locale];
		// numbers = availableNumbers[selectedApplication][locale];
		activeLocale = locale;
		return true;
	} else {
		return false;
	}
}

/**
Возвращает текущий язык
*/
function getActiveLocale() {
	return activeLocale;
}

/**
Перечисляет установленные языки
*/
function enumerateLocales(func) {
	for(var locale in availableMessages) func(locale);
}

var supportedLocales = ["ru", "en", "az"];