﻿(function() {

var CardCaptured = false;

_.addEventListeners({
	"load" : function() {
		_.registerEvents(['exiting']);
	
		_.registerActions({
			"commit" : new StateAction(
				function() { return true; },
				function() {_.leave("commit") }),
				
			"cancel" : new StateAction(
				function() { return true; },			
				function() {_.leave("cancel") }),
				
			"exit" : new StateAction(
				function() { return true; },
				function() {
					if (window.external.Framework)
						Framework.CloseApplication();
					else {	
						window.focus();
						parent.opener = window;
						parent.close();	
					}	
				})
		});	
	},
	
	"unload" : function () {
		return true;
	}
});

})();
