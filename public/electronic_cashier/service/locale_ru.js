﻿addLocale(
		"ru",
		{
			"paykiosk" : {
				"noTerminalNumber" : "не задан"
			},

			"operation" : {
				"name" : "Операции",
				"exit" : "Выход из приложения"
			},
			
			"deviceStatus" : {
				"Printer" : {
					"dssUndefined" : "Не определен",
					"dssDisabled_Initialize" : "Отключен",
					"dssIdle_Normal" : "Работает",
					"dssIdle_Suspended" : "Приостановлено",
					"dssIdle_PaperNearEnd" : "Сработал датчик близкого конца бумаги",
					"dssBusy_Printing" : "Идет печать",
					"dssBusy_Holding" : "Удержание чека",
					"dssError_NoDevice" : "Устройство не найдено",
					"dssError_DeviceFailure" : "Сбой в работе устройства",
					"dssError_PaperOut"	: "Нет бумаги или неверно настроены направляющие"	
				},
				
				"billAcceptor" : {
					"dssUndefined" : "Не определен",
					"dssDisabled_PowerUp" : "Отключён, загружается",
					"dssDisabled_Initialize" : "Работает",
					"dssDisabled_Inhibit" : "Запрещен",
					"dssBusy_Accepting" : "Идет прием",
					"dssBusy_RejectingInhibit" : "Запрет возврата купюры",
					"dssBusy_Rejecting" : "Возврат купюры",
					"dssBusy_Escrow" : "Прием купюры",
					"dssBusy_Stacking" : "Укладка купюры",
					"dssBusy_Stacked" : "Уложил купюру",
					"dssBusy_Returning" : "Возвращаю купюру",
					"dssBusy_Returned" : "Возвратил купюру",
					"dssBusy_Paused" : "Пауза",
					"dssBusy_Holding" : "Удержание купюры",
					"dssBusy_dssDispensing" : "Занят, выдача купюр",
					"dssBusy_dssDispensed" : "Занят, купюра выдана",
					"dssBusy_dssDispensedComplete" : "Все купюры выданы",
					"dssBusy_dssUnloading" : "Возврат купюры",
					"dssBusy_dssUnloaded" : "Купюра возвращена",
					"dssBusy_Packed" : "Упакована",
					"dssError_NoDevice" : "Устройство не найдено",
					"dssError_DeviceFailure" : "Сбой в работе устройства",
					"dssError_StackerFull" : "Кассета переполнена!",
					"dssError_ValidatorJammed" : "Замята купюра в приемнике!",
					"dssError_StackerJammed" : "Замята купюра в кассете!",
					"dssError_StackerRemoved" : "Зачем то сняли стекер!",
					"dssIdle_Normal" : "Работает",
					"dssIdle_Cassette" : "Работает",
					"dssIdle_CassetteNormal" : "Работает, кассета в порядке",
					"dssIdle_CassetteNearEnd" : "Кассета почти опустела",
					"dssIdle_CassetteNotPresent" : "Работает, кассета отсутствует",
					"dssIdle_CassetteInvalid" : "Кассета неисправна",
					"dssError_CassetteNotPresent" : "Ошибка, кассета отсутствует",
					"dssIdle_Sensor" : "Работает сенсор",
					"dssIdle_Suspended" : "Приостановлено",
					"dssError_ShutterFailure" : "Шторка сломалась",
					"dssError_PoolFailure" : "Ошибка пула",
					"dssError_PotentiometerFailure" : "Ошибка потенциометра"				
				},
				"VirtuPOS" : {
					"available" : "Доступен",
					"unavailable" : "Недоступен"					
				}
			},
			
			"start" : {
					
				"printerName" : "Чековый принтер",
				"billacceptorName" : "Купюроприёмник",				
				"virtuposName" : "VirtuPOS",
				
				"devName" : "Имя устройства",
				"devState" : "Состояние"				
			
			}			
		});