﻿(function() {

_.addEventListeners({
	"load" : function() {
	
		// Отображаем экран приглашения
		document.getElementById("status").innerHTML = template('statusTemplate', getMessage('operation.name'));
		document.getElementById("data").innerHTML = "<input id='exitButton' type='button' value='" + getMessage('operation.exit') + "'>";
		
		ViewButtons.bind({
			"continueButton" : "commit",
			"cancelButton" : "cancel",
			"exitButton" : "exit"
			
		});	
		
		ViewButtons.update();
	},
	
	"key" : function(key) {
		switch(key) {
			// backspace
			case 8 :
			//esc
			case 27 :
				_['cancel']();
				break;
				
			//return
			case 13 :
				_['commit']();
				break;
		}
		return false;
	}
});
})();