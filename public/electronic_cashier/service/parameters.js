﻿appendParameters({
	"paykiosk" : {
		// Размер стэкера по умолчанию
		"stackerSize" : 1200,
		"encashmentBillacceptor" : false, //Возможность инкассировать купюроприемник
		"encashmentBilldispenser" : false, //Возможность инкассировать диспенсер
		"encashmentCoinacceptor" : false, //Возможность инкассировать монетник
		"encashmentHopper" : false, //Возможность инкассировать хоппер
		"encashmentPayments" : true, //Возможность закрытие журнала платежей
		"encashmentCardCaptured" : false, //Возможность закрытия журнала захваченных карт
		"encashmentZReportPrint" : false // Возможность печати Z-отчёта
	}
})