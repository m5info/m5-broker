﻿(function () {
var vmcounter = 0;
Framework.Variables("paymentResults") = {
	length : 1
};

_.addEventListeners({
	// Обработчик load
	"load" : function() {
		if (!VirtuPOS || VirtuPOS.Offline){
			_.leave("fail");
		return false;	
		}
		
		_.registerVariables({
			"operationCompleted" : false,
			"operationSucceeded" : false
		});
		
		_.registerActions({
			"continue" : new StateAction(
				function() {return _.operationCompleted; },
				function() {
					_.leave('continue');
				})
		});
		
		_.registerEvents(['operationComplete' , 'executingVirtuPOSMethod' , 'tryVirtuPOSMethodAgain']);

		_.addEventListeners({"operationComplete" : function() { 
			_['continue']();
			}
		});

		Framework.AttachEvent(VirtuPOS, "OnP2PCardTransferComplete", onOperationComplete);
		Framework.AttachEvent(VirtuPOS, "OnRereadIC", p2pTransfer);
		
	},
	
	"run" : function() {

		_.addEventListeners({
				"operationComplete" : function() {
				 _.operationCompleted = true;
				}
			  });

			if (!VirtuPOS.IsFallback) {
				VirtuPOS.RereadIC();
			} else {
				p2pTransfer();
			}	 
	}
});

function onOperationComplete(requestCompleteResult) {
	

	_.operationSucceeded = requestCompleteResult;
	
	Framework.Variables("paymentResults")[1] = {
		"recipient" : PaymentGateway.SelectedRecipient,
		"errorCode" : _.operationSucceeded ? 0 : -1,
		"errorText" : ""
	}
	
	/*
	у получателя "перевод с карты на карту" необходимо создать два поля с ID 100 и 101
	в поле 100 в data-entry вводится номер карты получателя
	поле 101 имеет уровень видимости "При печати, в мониторинге"
	ниже номер карты получателя передаётся в локальную переменную и маскируется
	получившееся строковое значение присваивается полю 101
	поле 100 обнуляется и игнорируется при печати
	*/
	var MaskedCardNr = PaymentGateway.SelectedRecipient.Fields.GetFieldByID(100).value;
	PaymentGateway.SelectedRecipient.Fields.GetFieldByID(100).value = "";
	PaymentGateway.SelectedRecipient.Fields.GetFieldByID(101).value = MaskedCardNr.slice(0,4).concat("********",MaskedCardNr.slice(12,16));
	
	_.fireEvent('operationComplete');
	
}

function p2pTransfer() {
	
	vmcounter++;
	_.fireEvent('executingVirtuPOSMethod');
	try {
		VirtuPOS.P2PCardTransfer(PaymentGateway.SelectedRecipient.TransferSum, PaymentGateway.SelectedRecipient.Fields.GetFieldByID(100).value, PaymentGateway.DocumentNumber);  
	} catch(e) {
		if (VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsBusy && vmcounter <= getParameter("VirtuPOS.attemptsNumber", 5)) {
			_.fireEvent('tryVirtuPOSMethodAgain');
			setTimeout(p2pTransfer, getParameter("VirtuPOS.retryTime", 2000));
		} else
			onOperationComplete(false)
	}	
	
}

})();