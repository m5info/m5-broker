﻿
_.addEventListeners({
	"load" : function() {
		_.registerEvents(['cancelComplete','cancelStarted']);
		_.registerVariables({
			"cancelInProgress" : false,
			"cancelResult" : null,
			"errorText" : null

		});
		
		_.addEventListeners({
			"inactivityTimer" : function() {
				cancelPayment();
				return true;
			}
		});
		
		
		_.registerActions({
			"cancel" : function () { _.leave('cancel'); },
			"commit" : function() { cancelPayment(); }
		});

		// подключение к PaymentGW
		Framework.AttachEvent(PaymentGateway, "OnCancelRequestComplete", onCancelComplete);
	}
});

function cancelPayment() {

	_.cancelInProgress = true;
	_.fireEvent("actionsChanged");
	_.fireEvent("cancelStarted");

	PaymentGateway.SelectedRecipient.Fields.GetFieldByID(803).Value = "Dispense";
	
	try {
		PaymentGateway.Cancel();
	} catch(e) {
		hideAlert();
		onCancelComplete(-1, e.description);
	}	

}

function onCancelComplete(requestCompleteResult, errorText) {
	hideAlert();

	_.cancelInProgress = false;

	_.cancelResult = requestCompleteResult;
	_.errorText = errorText == undefined ? getMessage("dataCheck.dataValidationFailed") : errorText;

	// Уведомили View об окончании проверки
	_.fireEvent("actionsChanged");
	_.fireEvent('cancelComplete', requestCompleteResult, errorText);
	
	var Recipient = PaymentGateway.SelectedRecipient;
	_.leave('dispense');
}