﻿_.addEventListeners({
	// Обработчик загрузки текущего состояния
	"load" : function() {
		// AttachEvents
		if (FiscalPrinter) {
			Framework.AttachEvent(FiscalPrinter, "OnFiscalPrinterEvent", onFiscalPrinterEvent);
			Framework.AttachEvent(FiscalPrinter, "OnXReportPrinted", onXReportPrinted);
			Framework.AttachEvent(FiscalPrinter, "OnZReportPrinted", onZReportPrinted);
		}
		if (PaymentDevice) {
			Framework.AttachEvent(PaymentDevice, "OnDeviceStatusChanged", onPaymentDeviceDeviceStatusChanged);
		}
	},
	"inactivityTimer" : function() {
		return false;
	},
	"run" : function() {},
	
	"unload" : function() {
		_.getLogger().info('Выгружаем состояние');
	}
});

/* FiscalPrinter Events*/
function onFiscalPrinterEvent() {
	// if (document.getElementById("fiscalBody").style.visibility == "visible") {
		// activateFiscal();
	// }
}

function onXReportPrinted() {
// 	showAlert("X-отчет напечатан<br>Возьмите квитанцию", {}, {}, hideAlert, 3000);
}

function onZReportPrinted() {
//	showAlert("Z-отчет(ы) напечатан(ы)<br>Возьмите квитанцию", {}, {}, hideAlert, 3000);
}

/* PaymentDevice */
function onPaymentDeviceDeviceStatusChanged() {
/*
	if (document.getElementById("billacceptorBody").style.visibility == "visible") {
		activateBillacceptor();
	}
*/
}
