﻿(function() {
_.addEventListeners({
	"load" : function() {
		// _.registerEvents(['dispenseComplete', 'dispenseNoSum', 'dispenseError']);

		_.registerVariables({
			"changeSum" : 0, //Framework.Variables("changeSum"),
			// необходимо починить errorText
			"errorText" : "",
			// необходимо починить
			"dispenseResult" : 0,
			"unDispensedSum" : 0//Framework.Variables("unDispensedSum")
		});

		_.registerActions({
			"commit" : function() { _.leave("commit") }
		});
	}
});
})()