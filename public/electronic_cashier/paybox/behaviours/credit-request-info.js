﻿(function() {
_.addEventListeners({
	"load" : function() {
			
		_.registerActions({
			"commit" : function() {
					_.leave("commit");
				},
			// Выход в root
			"menu" : new StateAction(
					// Продолжать  можно если
					function ()  {
						return !VirtuPOS.IsCardPresent;
					},
					function () { 
						_.leave("recipients");
					}),
			// Выдать карту		
			"ejectCard" : new StateAction(
				function ()  {
					return VirtuPOS && !VirtuPOS.Offline &&	VirtuPOS.IsCardPresent;
				},
				function() {
					Framework.Variables("EjectReason") = getMessage("ejectReason.cancel");
					_.leave("ejectCard");
				})
		});
	}	

});

})();
