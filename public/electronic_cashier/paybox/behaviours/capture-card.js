(function() {
_.addEventListeners({
	"load" : function() {
		_.registerVariables({
			"operationCompleted" : false,
			"operationSucceeded" : false
		});
	
		_.registerActions({
			"continue" : new StateAction(
				function() {return _.operationCompleted; },
				function() {
					// ����� ��������� ���������� � �������������� leave
					if (_.operationSucceeded) 
						_.leave('success');
					else 
						_.leave('fail');
				})
		});
		
		_.registerEvents({
			"operationComplete" : _['continue']
		});

		/* ������� VirtuPOS */
		Framework.AttachEvent(VirtuPOS, "OnCardCaptured", onCardCaptured);
	},
	"run" : function () {
		if (!VirtuPOS || VirtuPOS.Offline) {
			_.leave("fail");
			return;
		}
		_.addEventListeners({
			"operationComplete" : function() {
				_.operationCompleted = true;
			}
		});
		vpCaptureCard();
	},
	"inactivityTimer" : function() {
		if (_.operationCompleted) {
			_['continue']();
			return false;
		}
	}
});

function vpCaptureCard() {
	try {
		VirtuPOS.CaptureCard();
	} catch(e) {
		onCardCaptured();
	}
}

function onCardCaptured() {
	_.operationSucceeded = true;
	// ���������� ����������� �� View. ���� View �� ��������� �������, �� �������������� �������� commit
	_.fireEvent('operationComplete');
}
})()