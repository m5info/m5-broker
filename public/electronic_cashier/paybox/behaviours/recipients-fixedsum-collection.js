﻿(function() {
_.addEventListeners({
	// Обработчик загрузки текущего состояния
	"load" : function() {
		// Если пришли с получателя, то делаем selectParent
		if (PaymentGateway != null && PaymentGateway.SelectedRecipient != null && PaymentGateway.SelectedCatalogItem != null && !PaymentGateway.SelectedCatalogItem.IsRoot) {
			PaymentGateway.SelectedCatalogItem.SelectParent();
		}

		// Создаем события для взаимодействия с отображением
		_.registerEvents(['updateView', 'unableToPay']);
		
		// Создаем доступные на странице действия с условиями возможности их применения
		_.registerActions({
			"recount" : recount,
			"resetAll" : resetAll,
			"continue" : new StateAction(
				function () {return _.totalSum > 0; },
				finish
			),
			"cancel" : function() { _.leave('cancel'); }
		});
		
		 _.registerVariables({
			"collection" : [],
			"itemCount" : 0,
			"totalSum" : 0,
			"catQuont" : 0
		});		

		for (i = 0, c = PaymentGateway.SelectedCatalogItem.ChildItems; i < c.Count; i++) {
		
		if (c.Item(i).IsLink ||
			!c.Item(i).Recipient ||
			c.Item(i).Recipient.TransferAmountType != 1 ||
			c.Item(i).Recipient.TransferSum <= 0)
			continue;
		
			_.collection[i] = {
				"number" : 0,
				"max" : 5,
				"price" : c.Item(i).Recipient.TransferSum
			};	
			
			_.catQuont++;			
		}
		
		if (PaymentGateway.SelectedCatalogItem == null) {
			if (!_.leave("catalogLost"))
				_.getLogger().error("Потеря каталога не обработана сценарием");
			return;
		}

	},
	
	"inactivityTimer" : function() {
		if (!PaymentGateway.SelectedCatalogItem) {
			_.leave("catalogLost");
		}
		return true;
	}

});

function isRecieptRequired() {
	return (PaymentGateway.SelectedRecipient && PaymentGateway.SelectedRecipient.InCategory("ReceiptRequired"))
			|| PaymentGateway.SelectedCatalogItem.InCategory("ReceiptRequired");
}

function recount(way, idx) {

	if (way == "plus" && _.collection[idx]["number"] < _.collection[idx]["max"]) {
		
		_.collection[idx]["number"]++;
		_.itemCount++;
		_.totalSum += PaymentGateway.SelectedCatalogItem.ChildItems.Item(idx).Recipient.TransferSum;
		
	} else if (way == "minus" && _.collection[idx]["number"] > 0) {
	
		_.collection[idx]["number"]--;
		_.itemCount--;
		_.totalSum -= PaymentGateway.SelectedCatalogItem.ChildItems.Item(idx).Recipient.TransferSum;
		
	}
		
	_.fireEvent('updateView', idx);	
	_.fireEvent('actionsChanged');

}

function resetAll() {
	for (var i=0, c=PaymentGateway.SelectedCatalogItem.ChildItems.Count; i<c; i++) {
		if (!_.collection.hasOwnProperty(i)) 
			continue;
		
		_.collection[i]["number"] = 0;
	}
	_.itemCount = 0;
	_.totalSum = 0;
	
	_.fireEvent('updateView', 'reset');		
	_.fireEvent('actionsChanged');	
}

function finish() {

	if (!(Printer && Printer.DeviceReady) && isRecieptRequired())
		_fireEvent('unableToPay');

	PaymentGateway.RecipientCollection.Clear();
	for (var i=0, cat=PaymentGateway.SelectedCatalogItem.ChildItems; i<cat.Count; i++) {
		if (!_.collection[i]) continue;
		for (var j=0; j<_.collection[i]["number"]; j++) {
			PaymentGateway.RecipientCollection.Add(cat.Item(i).Recipient);
		}
	}
	_.leave('continue');
}

})()