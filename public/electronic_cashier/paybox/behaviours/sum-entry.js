﻿( function() {

_.addEventListeners({
	// Обработчик загрузки текущего состояния
	"load" : function() {
		Framework.Variables("EjectReason") = "";

		// события для взаимодействия с view
		_.registerEvents([
			"sumChanged", "maxLimitReached"
		]);

		// Создаем события для взаимодействия с отображением
		_.registerVariables({
			"maxSum" : 0,
			"minSum" : 0,
			"enteredSum" : 0,
			"enteredSumText" : "0",
			"recipient" : PaymentGateway.SelectedRecipient
		});

		// Создаем доступные на странице действия с условиями возможности их применения
		 _.registerActions({
			"cancel" : function () {
				Framework.Variables("EjectReason") = getMessage('sumEntry.cancelOperation');
				_.leave('cancel'); 
			},
			"commit" : new StateAction(
					// Продолжать  можно если
					function ()  {
						if ((_.enteredSum >= minLimit()) && (_.enteredSum > commissionSum())) {
							//если сдача + принятые больше минимальной суммы платежа и суммы хватает на комиссию
							return true;
						}
						return false;
					},
					function () { 
						doPayment(_.enteredSum);
						_.leave('commit');
					}),
			"appendChar" : appendChar,
			"removeLastChar" : removeLastChar,
			"recipients" : recipients
		});

		if (typeof(getOperationCurrency) !== "undefined") {
			_.recipient.CurrencyCode = getOperationCurrency();
		}
		if (_.recipient.TransferAmountType == 1) {
			_.enteredSum = _.recipient.TransferSum;
			_.enteredSumText = _.enteredSum.toString();
			_.recipient.PaymentSum = _.enteredSum;
			if (!getParameter("sumEntry.confirmFixedSum", true)) {
				_['commit']();
				return false;
			}
		} else {
			_.enteredSum = 0;
			_.enteredSumText = _.enteredSum.toString();
			_.recipient.PaymentSum = 0;		
		}

		calculateCommission();
		
	}
});

function validateSum(newSum) {
	if (newSum > maxLimit(newSum)) {  //пытаемся ввести большую сумму
		_.fireEvent('maxLimitReached');
		_.enteredSum = maxLimit(newSum);
		_.enteredSummText = _.enteredSum.toString();
		_.recipient.PaymentSum = _.enteredSum;
		calculateCommission();
		_.fireEvent('actionsChanged');
		_.fireEvent('sumChanged');
		return false;
	}
	return true;
}

function maxLimit() {
	_.maxSum = Math.min(1000000, getNumber(_.recipient.Fields.GetFieldByID(151).DisplayValue) > 0 ? getNumber(_.recipient.Fields.GetFieldByID(151).DisplayValue) : 1000000);
	return _.maxSum;
}

function minLimit() {
	_.minSum = Math.max(_.recipient.MinLimit, getNumber(_.recipient.Fields.GetFieldByID(130).DisplayValue), getNumber(_.recipient.Fields.GetFieldByID(150).DisplayValue) > 0 ? getNumber(_.recipient.Fields.GetFieldByID(150).DisplayValue) + getNumber(_.recipient.Fields.GetFieldByID(130).DisplayValue) : 0);
	if (_.minSum == getNumber(_.recipient.Fields.GetFieldByID(130).DisplayValue))
		_.minSum ++;
	return _.minSum;
}

function removeLastChar()  {
	var prevText = _.enteredSumText;
	var idxDot = prevText.indexOf('.');
	if (prevText.length > 0)
		prevText = prevText.substring(0, prevText.length - 1);
	if (prevText.length > 0 && prevText.length - 1 == idxDot)
		prevText = prevText.substring(0, prevText.length - 1);
	if (prevText.length == 0)
		prevText = "0";
	_.enteredSumText = prevText;
	_.enteredSum = Number(prevText);
	_.recipient.PaymentSum = _.enteredSum;
	calculateCommission();
	_.fireEvent("sumChanged");
	_.fireEvent("actionsChanged");
}

function appendChar(ch) {

	var idxDot = _.enteredSumText.indexOf(".");
	var digit = ch.charCodeAt(0) - 0x30;

	if ( !((digit >= 0 && digit <= 9) || (idxDot < 0 && (ch == '.' || ch == ',')) ) ){
		return false;
	}
	if (_.enteredSumText.length - idxDot > 2) {
		_.enteredSum = Math.round(_.enteredSum*100)/100;
		_.enteredSumText = ""+_.enteredSum;
	}
	//
	var newSum = _.enteredSumText;
	if (idxDot < 0 || (idxDot >= 0 && _.enteredSumText.length - idxDot <= 2)) {
		newSum += ch;
		if (!validateSum(newSum))
			return false;
	}
	_.enteredSumText = newSum;
	_.enteredSum = Number(_.enteredSumText);
	_.recipient.PaymentSum = _.enteredSum;
	calculateCommission();
	_.fireEvent("sumChanged");
	_.fireEvent("actionsChanged");
	return true;
}

function recipients() {
	PaymentGateway.SelectedCatalogItem.SelectParent();
	_.leave("recipients");
}

// Расчет сумм
function calculateCommission() {
	if (inServerResponseCommission() && _.recipient.PaymentSum > 0 && getNumber(_.recipient.Fields.GetFieldByID(130).DisplayValue) >= 0) {
		// пересчитываем комиссию из поля
		_.recipient.CommissionSum = Math.min(getNumber(_.recipient.Fields.GetFieldByID(130).DisplayValue), _.recipient.PaymentSum);
	}	
	
}

function inServerResponseCommission() {
	if ((_.recipient && _.recipient.InCategory("ServerResponseCommission"))
		|| PaymentGateway.SelectedCatalogItem.InCategory("ServerResponseCommission")) {
		return true;		
	} else {
		return false;
	}
}

function getNumber(n) {
	if (!n.length)
		return 0;
	var re = /\-?\d+\.?\,?/g;
	var nn = n.match(re,"");
	if (nn != null) 
		nn = nn.join("").replace(",", .2.toString().substr(1,1));
	else 
		nn = 0;
	if (nn < 0)
		nn = 0;
	return parseFloat(nn);
}

})();