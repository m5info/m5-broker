﻿
_.addEventListeners({
	"load" : function() {
		_.registerEvents(['cancelComplete','cancelStarted']);
		_.registerVariables({
			"cancelInProgress" : false,
			"cancelResult" : null,
			"errorText" : null

		});
		
		_.addEventListeners({
			"inactivityTimer" : function() {
				cancelPayment();
				return true;
			}
		});
		
		_.registerActions({
			"commit" : new StateAction(
				function() {return !_.cancelInProgress && !_.cancelResult; },
				function() {
					if (!_.leave("commit")) {
						_.getLogger().error("Успешная проверка данных не обработана сценарием");
					}
				}),
		
			"cancel" : new StateAction(
				function() {return !_.cancelInProgress && !_.cancelResult; },
				function(nextHop) {
					cancelPayment(nextHop);
				})
		});

		// подключение к PaymentGW
		Framework.AttachEvent(PaymentGateway, "OnCancelRequestComplete", onCancelComplete);
	}
});

function cancelPayment(nextHope) {

	_.cancelInProgress = true;
	_.fireEvent("actionsChanged");
	_.fireEvent("cancelStarted");

	PaymentGateway.SelectedRecipient.Fields.GetFieldByID(803).Value = nextHope ? nextHope : "";
	
	try {
		PaymentGateway.Cancel();
	} catch(e) {
		hideAlert();
		onCancelComplete(-1, e.description);
	}	

}

function onCancelComplete(requestCompleteResult, errorText) {
	hideAlert();

	_.cancelInProgress = false;

	_.cancelResult = requestCompleteResult;
	_.errorText = errorText == undefined ? getMessage("dataCheck.dataValidationFailed") : errorText;

	// Уведомили View об окончании проверки
	_.fireEvent("actionsChanged");
	_.fireEvent('cancelComplete', requestCompleteResult, errorText);
	
	var Recipient = PaymentGateway.SelectedRecipient;

	switch (requestCompleteResult) {
		case 0 : //удачная отмена
			_["back"]();
			return;
		default:
			break;
	}
	// Сообщаем о неудачной отмене
	showAlert(
		"Ошибка отмены платежа.<br>Обратитесь к администратору зала.",
		{
			"ok" :  function() { _.leave('back') }
		},
		{
			27: "ok"
		},
		"ok"
	);
}