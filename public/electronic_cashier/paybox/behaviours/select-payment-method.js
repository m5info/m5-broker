﻿(function() {
	_.addEventListeners({
		// Обработчик загрузки текущего состояния
		"load" : function() {
			// Основное событие для взаимодействия с view
			_.registerEvents(["paymentMethodSelected"]);

			// Создаем события для взаимодействия с отображением
			_.registerVariables({
				"paymentMethods" : null,
				"selectedPaymentMethod" : -1
			});
			
			// Создаем доступные на странице действия с условиями возможности их применения
			_.registerActions({
				"selectPaymentMethod" : function (idx) {
					Framework.Variables("PaymentMethod") = _.paymentMethods[_.selectedPaymentMethod].state;
					_.selectedPaymentMethod = idx;
					_.fireEvent('actionsChanged');
					_.fireEvent('paymentMethodSelected');
				},
				"continue" : new StateAction(
					function() { return _.selectedPaymentMethod !== -1 },
					function() { _.leave(_.paymentMethods[_.selectedPaymentMethod].state); }
				),
				"cancel" : function () { 
					if (!hasInputFieldsOnSelected()) {
						PaymentGateway.SelectedCatalogItem.SelectParent();
						if (getParameter("recipients.autoSelectRecipent", false) || inSelectSingleItemCategory(PaymentGateway.SelectedCatalogItem)) {
							while (PaymentGateway.SelectedCatalogItem != null && PaymentGateway.SelectedCatalogItem.ChildItems.Count == 1 && !PaymentGateway.SelectedCatalogItem.IsRoot)
								PaymentGateway.SelectedCatalogItem.SelectParent();
						}
					}
					_.leave("cancel");
					return;
				}
			});
			
			_.paymentMethods = getRecipientPaymentMethods();
			
			_.addEventListeners({
				"inactivityTimer" : _["cancel"],
				"paymentMethodSelected" : _["continue"]
			});

		}
	});

})();
