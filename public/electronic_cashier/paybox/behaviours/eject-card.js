﻿var counterInterval;

_.addEventListeners({
	"load" : function() {
		if (!VirtuPOS || VirtuPOS.Offline) {
			_.leave("fail");
			return false;
		}

		_.registerEvents(["captureTick"]);
		
		_.registerVariables({
			"captureTimeoutCount" : getParameter("card.captureTimeout", 30),
			"cardWasntPickedUp" : true
		});
	
		_.registerActions({
			"commit" : new StateAction(
				function() { return !VirtuPOS.IsCardPresent; },
				function() { _.leave("commit") }),
			"capture" : function() { 
       				// Сброс оставшихся сумм
       				for (var i = 0, c = PaymentDevice.AccumulatedSum.Count; i < c; i++) { 
       					var as = PaymentDevice.AccumulatedSum(i);
       					PaymentDevice.ResetAccumulatedSum(as.CurrencyCode);
       				}
				_.leave("capture");
			}
		});
		
		if (!VirtuPOS.IsCardPresent) {
			_.leave("commit");
			return;
		}
		/* События VirtuPOS */
		Framework.AttachEvent(VirtuPOS, "OnCardEjected", onCardEjected);
		Framework.AttachEvent(VirtuPOS, "OnCardPickedUp", onCardPickedUp);
		Framework.AttachEvent(VirtuPOS, "OnCardRemoved", OnCardRemoved);

	},
	"run" : function () {
		Framework.Variables("EjectReason") = "";
		vpEjectCard();
	},
	"inactivityTimer" : function() {
		return false;
	}
});

function onCardEjected() {

	(function(){
		var counterInterval = setInterval(
				function () {
					if (_.captureTimeoutCount <= 0) {
						_["capture"]();
						clearInterval(counterInterval);
						CardCaptured = true;
					}
					else {
						_.fireEvent("captureTick", _.captureTimeoutCount);
						_.captureTimeoutCount = _.captureTimeoutCount-1;
					}
				}
				, 1000);
		_.fireEvent("captureTick", _.captureTimeoutCount);
	})();	
}

function onCardPickedUp() {
	_.cardWasntPickedUp = false;
	_['commit']();
	try {
		VirtuPOS.Clear();
		VirtuPOS.DisableReader();
	} catch(e) {};
}
function vpEjectCard() {
	try {
		VirtuPOS.EjectCard();
		return;
	} catch(e) {
		setTimeout(vpEjectCard, 1000);
	}
}
function OnCardRemoved() {
	if (_.cardWasntPickedUp) {
		_['commit']();
		VirtuPOS.Clear();		
		try {
			VirtuPOS.DisableReader();
		} catch(e) {};	
	}
}