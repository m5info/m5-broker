﻿(function() {

_.addEventListeners({
	// Обработчик load
	"load" : function() {
		_.registerVariables({
			"operationSucceeded" : false, // Флаг успешности завершения операции
			"operationCompleted" : false 
		});
		
		// Действия
		_.registerActions({
			// Действие завершения состояния
			"continue" : new StateAction(
				function() {return true; }, // Доступно только по окончании выполнения
				function() {
					// Здесь обработка результата и соответсвующий leave
					PaymentGateway.SelectedCatalogItem.SelectParent();
					_.leave('continue');
				})
		});
		
		// Событие завершения операции
		_.registerEvents(['operationComplete']);
		// Добавляем обработчик по умолчанию (если view не вернуло явно в обработчике false с намерением попоказывать что-нибудь клиенту и спросить о чем-нибудь)
		_.addEventListeners({"operationComplete" : function() { 
				_['continue']();
			}
		});
		
		Framework.AttachEvent(Printer, "OnPresenterEmptyEvent", onPresenterEmptyEvent);
		if (FiscalPrinter)
			Framework.AttachEvent(FiscalPrinter, "OnPresenterEmptyEvent", onPresenterEmptyEvent);
		if (ReceiptPrinter)
			Framework.AttachEvent(ReceiptPrinter, "OnPresenterEmptyEvent", onPresenterEmptyEvent);
		
	},
	
	"run" : function() {
		// Перед обработкой view устанавливаем результат операции
		// Этот обработчик вызовется до обработчика из view зарегестрированного в load
		_.addEventListeners({
				"operationComplete" : function() {
				 _.operationCompleted = true;
				 _.fireEvent('actionsChanged');
				}
			  });
		
		_.operationSucceeded = printCoupon();
	
		// Отправляем уведомление во View. Если View не обработал событие, то самостоятельно вызываем continue
		_.fireEvent('operationComplete');		
	}
});

// Печать чека
function printCoupon() {
	if (!Printer.Offline && Printer.DeviceReady) {
		if (!ReceiptPrinter.Offline) {
			//печать Нефискального чека
			printNoFiscalCoupon();
			return true;
		}
		return false;
	} else
		return false;
};	

function onPresenterEmptyEvent() {
	_['continue']();
}

})();
