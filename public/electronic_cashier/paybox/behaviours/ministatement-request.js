﻿(function(){

_.addEventListeners({
	"load" : function() {

		_.registerVariables({
			"operationCompleted" : false,
			"operationSucceeded" : false
		});
		
		_.registerEvents(["operationComplete"]);

		_.registerActions({
			// Действие по выходу из состояния с отменой. Может отсутствовать вовсе.
			"continue" : new StateAction(
				function() { return (_.operationCompleted) }, // Доступно только по окончании выполнения
				function() { 
					// Выход на печать
					_.leave(PaymentGateway.SelectedRecipient.Fields.GetFieldByID(998).Value.toLowerCase()); 
				}),
			"cancel" : new StateAction(
				function() { return (_.operationCompleted) }, // Доступно только по окончании выполнения
				function() { 
					// Выход по отмене
					PaymentGateway.SelectedCatalogItem.SelectParent();
					_.leave("cancel"); 
				})
		});
		
		// подключение к PaymentGW
		Framework.AttachEvent(PaymentGateway, "OnCheckNumberRequestComplete", onCheckComplete);
	},
	
	"run" : function() {
		// Перед обработкой view устанавливаем результат операции
		// Этот обработчик вызовется до обработчика из view зарегестрированного в load
		_.addEventListeners({
			"operationComplete" : function() {
			}
		});
		
		doCheck();
	}
});

function doCheck() {
	try {
		PaymentGateway.CheckNumber();
	} catch(e) {
	}	
}

/* Завершена операция */
function onCheckComplete(requestCompleteResult) {
	 _.operationCompleted = true;
	// Успешная проверка в случае rcrSucceeded || rcrNeedNextCheck 
	_.operationSucceeded = requestCompleteResult == 0 || requestCompleteResult == 1;
	// Распечатываем чек
	if (_.operationSucceeded)
		_["continue"]();
	else
	// Отправляем уведомление во View. Если View не обработал событие, то самостоятельно вызываем commit
	_.fireEvent('operationComplete');
}
})()
