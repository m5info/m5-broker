﻿(function () {
var selectResult = null;

_.addEventListeners({
	"load" : function() {
		try {
			PaymentDevice.CancelPayment();
		} catch(e) {};
		/* Инициализация переменных */
		Framework.Variables("cardWrite") = false;
		Framework.Variables("availableBalance") = null;
		if (!(VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsCardPresent)) 
			Framework.Variables("MaskedCardNr") = null;
		Framework.Variables("cardChipInfo") = null;
		
		// Результат выполнения операции Check
		Framework.Variables("checkResult") = null;
		// Результат выполнения операции Pay
		Framework.Variables("paymentResults") = {
			length : 0
		};
		Framework.Variables("incompletePayments") = {};
		// Инкассация и сервис
		Framework.Variables("workerCode") = null;
		
		Framework.Variables("ExchangeToDispense") = null;
		
		// Устанавливаем тип оплаты acceptCashPayment - наличными, acceptCardPayment - по карте
		Framework.Variables("PaymentMethod") = "acceptCashPayment";

		// обнуляем коллекцию 
		PaymentGateway.RecipientCollection.Clear();		
		
		// Устанавливаем текущую локаль
		if (PaymentGateway && PaymentGateway.ActiveLocale == "") {
			var defaultLocale = getParameter("paykiosk.defaultLocale", "ru");

			// Ищем defaultLocale в PGW, если не найдем то первую в PGW
			for (var i=0, c=PaymentGateway.Locales.Count; i<c; i++) {
				if (PaymentGateway.Locales.Item(i).Lang === defaultLocale) {
					PaymentGateway.ActiveLocale = defaultLocale;
					break;
				}
			}
			//TODO: Исправить в PGW пустую локаль возвращаемую из коллекции и убрать for
			if (PaymentGateway.ActiveLocale == "") {
				for (var i=0, c=PaymentGateway.Locales.Count; i<c; i++) {
					if (PaymentGateway.Locales.Item(i).Lang !== "") {
						PaymentGateway.ActiveLocale = PaymentGateway.Locales.Item(i).Lang;
						break;
					}
				}
			}
			setActiveLocale(PaymentGateway.ActiveLocale);
		}
		
		/*dssError_StackerRemoved*/
		if (PaymentDevice.CashDevices.Count == 0  || (PaymentDevice.CashDevices.Count > 0 && PaymentDevice.CashDevices.Item(0).Status.Status == 3 && PaymentDevice.CashDevices.Item(0).Status.StatusEx == 21)) {
			_.leave('billAcceptorFailure');
			return;
		}		
		
		// Флаг прочтения карты
		if (VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsCardPresent) {
			_.leave('ejectCard');
			return;
		}
		
		// Причина возврата карты
		Framework.Variables("EjectReason") = "";
		
		// Сумма накопленная и не использованная
		try {
			if (getParameter("paykiosk.resetChangeSum", false)) {
				for (var i = 0, c = PaymentDevice.AccumulatedSum.Count; i < c; i++) { 
					var as = PaymentDevice.AccumulatedSum(i);
					PaymentDevice.ResetAccumulatedSum(as.CurrencyCode);
				}
			}
		} catch (e) {
		}

		Framework.Variables("unDispensedSum") = 0.0;

		//
		if (!VirtuPOS.Offline) {
			VirtuPOS.Clear();
			try { VirtuPOS.DisableReader(); } catch(e) {};
		}
		
		(function () {
			var waitForReload = function() {
				if (typeof(PaymentGateway) === "undefined" || PaymentGateway == null || PaymentGateway.SelectedCatalogItem == null)
					return false; // PaymentGateway не загрузился, или нет получателя
				// Выбираем root
				PaymentGateway.SelectedCatalogItem.SelectRoot();
				if (PaymentGateway.SelectedCatalogItem == null)
					return false; // Нет получателя
				if (!PaymentDevice.IsReady)
					return false; // PaymentDevice не готов
				clearInterval(timeoutInterval);
				_.leave('rootSelected');
				return true;
			};
			if (waitForReload()) return;
			var timeoutInterval = setInterval(waitForReload, 200);
		})();

		
	}
});
})();