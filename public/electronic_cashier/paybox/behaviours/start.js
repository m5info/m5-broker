﻿(function() {

_.addEventListeners({
	"load" : function() {
	
		_.registerVariables({
			"operationCompleted" : false,
			"devices" : getParameter("startingPaykiosk.devices").join(),
			"devInfo" : {}
		});

		if (_.devices === undefined) {
			_.leave('continue');
			return false;
		}			
		
		_.registerActions({
			"continue" : new StateAction(
				function() {return _.operationCompleted; },
				function() { _.leave('continue'); })
		});
		
		_.registerEvents(['operationComplete']);
		_.addEventListeners({"operationComplete" : function() { 
				setTimeout(_['continue'], getParameter("startingPaykiosk.showTime", 5000));		
			}
		});

	},
	
	"run" : function() {

		_.addEventListeners({
				"operationComplete" : function() {
				 _.operationCompleted = true;
				}
			  });

		if (_.devices.indexOf("Printer") != -1 )
			_.devInfo["Printer"] = Printer.Offline ? 1 : Printer.Status.SubStatus;
		
		for (var i = 0; i < PaymentDevice.CashDevices.Count; i++) {
			var device = PaymentDevice.CashDevices.Item(0);
		// if (_.devices.indexOf("billAcceptor") != -1 )
			// _.devInfo["billAcceptor"] = PaymentDevice.Status.SubStatus;
		}
		
		if (_.devices.indexOf("VirtuPOS") != -1 ) {
			_.devInfo["VirtuPOS"] = (VirtuPOS && !VirtuPOS.Offline) ? 1 : 0;
		}
			
		_.fireEvent('operationComplete');
		
	}
});

})();