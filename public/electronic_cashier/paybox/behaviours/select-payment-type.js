﻿(function() {
_.addEventListeners({
	"load" : function() {
			
		_.registerActions({
			"cancel" : function() {
					_.leave("recipients");
				},
			"selectPaymentType" : function(index) {
					PaymentGateway.SelectedRecipient.SelectedPaymentTypeIndex = index;
					_.leave("paymentTypeSelected");
				},
			// Выход в root
			"menu" : new StateAction(
					// Продолжать  можно если
					function ()  {
						return !VirtuPOS.IsCardPresent;
					},
					function () { 
						_.leave("menu");
					}),
			// Выдать карту		
			"ejectCard" : new StateAction(
				function ()  {
					return VirtuPOS && !VirtuPOS.Offline &&	VirtuPOS.IsCardPresent;
				},
				function() {
					Framework.Variables("EjectReason") = getMessage("ejectReason.cancel");
					_.leave("ejectCard");
				})
		});
	}	

});

})();
