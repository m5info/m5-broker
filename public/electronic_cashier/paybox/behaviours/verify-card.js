﻿( function() {
/**
Состояние действие обеспечивает выполнение проверку уже считанной карты с использованием
операции verify или balance inquiry
*/
var vmcounter = 0;
_.addEventListeners({
	"load" : function() {
		if (!VirtuPOS || VirtuPOS.Offline) {
			_.leave("fail");
			return false;
		}

		/* События VirtuPOS */
		Framework.AttachEvent(VirtuPOS, "OnVerificationComplete", onOperationComplete);
		Framework.AttachEvent(VirtuPOS, "OnBalanceInquiryComplete", onOperationComplete);
		Framework.AttachEvent(VirtuPOS, "OnCardCaptured", onCardCaptured);
		Framework.AttachEvent(VirtuPOS, "OnRereadIC", doVerify);

		if ((VirtuPOS.RecipientID == 100 && !getParameter("verifyCard.cardVerify", false)) ||
			(VirtuPOS.RecipientID != 100 && !getParameter("verifyCard.cardVerifyForeignCards", false))) {
			// Если отключена проверка карт вообще
			_.leave("success");
			return false;
		}
			
		_.registerVariables({
			"operationCompleted" : false,
			"operationSucceeded" : false
		});

		//Если карта не считана, то она точно не может пройти проверку
		if (!VirtuPOS.IsCardPresent) {
			_.leave('cancel');
			return false;
		}
		
		_.registerActions({
			"continue" : new StateAction(
				function() { return _.operationCompleted; },
				function() {
					if (_.operationSucceeded)
						_.leave("success");
					else
						_.leave("fail");
				}),
			"cancel" : new StateAction(
				function() { return _.operationCompleted; },
				function() {
					Framework.Variables("EjectReason") = getMessage("verifyCard.genericError");
					_.leave("cancel");
				})
		});
		
		_.registerEvents(['operationCompleted' , 'executingVirtuPOSMethod' , 'tryVirtuPOSMethodAgain']);
		_.addEventListeners({
				"operationCompleted" : function() {
					// Если view не запретило явно продолжение выполнения операции, то продолжаем в соответствии с результатом
					_['continue']();
				}
		});
	},
	
	"run" : function () {

		// Этот обработчик вызовется до обработчика из view зарегестрированного в load
		_.addEventListeners({
				"operationCompleted" : function() {
					_.operationCompleted = true;
					_.fireEvent('actionsChanged');
				}
		});
		
		if (isCardTypeOf("encashment"))
			_.leave("cancel");
		
		// или проверка карт выполняется только для своих карт
		if (!getParameter("verifyCard.cardVerify", false) ||
			isCardTypeOf("foreign") && !getParameter("verifyCard.cardVerifyForeignCards", false)) {
			// Если отключена проверка карт вообще
			onOperationComplete(true);
			return;
		}

		if (!VirtuPOS.IsFallback && VirtuPOS.IsCardPresent) {
			VirtuPOS.RereadIC();
		} else {
			doVerify();
		}	
			
	}
});

function doVerify() {
	vmcounter++;
	_.fireEvent('executingVirtuPOSMethod');
	try {
			// Проверять баланс и карта своя
		if ((getParameter("verifyCard.cardVerifyBalance", false) ||
			PaymentGateway.SelectedRecipient.InCategory("BalanceQuery")) && isCardTypeOf("ours")) {
			VirtuPOS.QueryBalance();
		}
		else
			VirtuPOS.Verify();
	} catch(e) {
		if (VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsBusy && vmcounter <= getParameter("VirtuPOS.attemptsNumber", 5)) {
			_.fireEvent('tryVirtuPOSMethodAgain');
			setTimeout(doVerify, getParameter("VirtuPOS.retryTime", 2000));
		} else
			onOperationComplete(false)
	}
}

function onOperationComplete(rcr) {
	_.operationSucceeded = (rcr == true) && (VirtuPOS.ResponseCode == 0);
	_.fireEvent('operationCompleted', VirtuPOS.ResponseCode);
}

function onCardCaptured() {
	Framework.Variables("MaskedCardNr") = VirtuPOS.MaskedCardNr;
	_.leave("fail-card-captured");

}

})();
