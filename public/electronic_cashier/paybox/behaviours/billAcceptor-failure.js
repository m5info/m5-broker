﻿_.addEventListeners({
	"load" : function() {
		
		_.registerActions({
			"initialize" : new StateAction(
				function () { return PaymentDevice.IsAcceptReady; },
				function() {
					if (!_.leave("deviceOK")) {
					}
			}),
			"commit" : function() { _.leave("deviceOK"); }
		});

		if (PaymentDevice)
			Framework.AttachEvent(PaymentDevice, "OnCashDeviceStatusChanged", onCashDeviceStatusChanged);
		
	}
});

function onCashDeviceStatusChanged() {
	if (PaymentDevice.IsAcceptReady) {
		_["commit"]();
		return true;
	}
	return false;
}
