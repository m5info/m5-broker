﻿( function() {

_.addEventListeners({
	// Обработчик загрузки текущего состояния
	"load" : function() {
		// события для взаимодействия с view
		_.registerEvents([
			"sumChanged", 
			"maxLimitReached"
		]);

		// Создаем события для взаимодействия с отображением
		_.registerVariables({
			"enteredSum" : new CurrencyAmount(0.0, getOperationCurrency()),
			"enteredSumText" : "0",
			"recipient" : PaymentGateway.SelectedRecipient,
			"amountTarget" : new CurrencyAmount(0.0, getTargetCurrency()),
			"amountDispense" : new CurrencyAmount(0.0, getOperationCurrency()),
			"amountChange" : new CurrencyAmount(0.0, getOperationCurrency()),
			"sourceCurrencyName" : getOperationCurrency(),
			"targetCurrencyName" :getTargetCurrency()
		});
		
		// Создаем доступные на странице действия с условиями возможности их применения
		 _.registerActions({
			"cancel" : function () { _.leave('cancel'); },
			"commit" : new StateAction(
					function ()  {
						if (_.amountTarget.value > 0 && (_.enteredSum.value >= minLimit()) && (_.enteredSum.value > commissionSum())) {
							//если сдача + принятые больше минимальной суммы платежа и суммы хватает на комиссию
							return true;
						}
						return false;
					},
					function () { 
					      Framework.Variables("PrintingFields") = {
						   "amountTarget" : Math.round(_.amountTarget.value),
						   "amountDispense" : Math.round(_.amountDispense.value),
						   "amountChange" : Math.round(_.amountChange.value)
						  };
						_.leave('commit');
					}),
			"appendChar" : appendChar,
			"removeLastChar" : removeLastChar
		});
		_.recipient.CurrencyCode = getOperationCurrency();
		_.enteredSumText = _.enteredSum.value.toString();
		_.recipient.PaymentSum = 0;	
		
		// var calculated = new exchangeCalculator(_.enteredSum.value);
		// _.amountTarget = calculated.targetDispenseAmount;
		// _.amountDispense = calculated.sourceAmountDispense;
		// _.amountChange = calculated.sourceAmountChange;
		// _.recipient.PaymentSum = _.enteredSum.value;
	},
	"run" : function () {
		_.addEventListeners({
			"sumChanged" : function () {
				_.enteredSum = new CurrencyAmount(Number(_.enteredSumText), getOperationCurrency());
				
				var calculated = new exchangeCalculator(_.enteredSum.value);
				_.amountTarget = calculated.targetDispenseAmount;
				_.amountDispense = calculated.sourceAmountDispense;
				_.amountChange = calculated.sourceAmountChange;
				_.recipient.PaymentSum = _.enteredSum.value;

				Framework.Variables("ExchangeToDispense") = [
					_.amountDispense, _.amountTarget
				];
			}
		});
		_.fireEvent('sumChanged');
	}
});

function validateSum(newSum) {
	if (newSum > maxLimit(newSum)) {  //пытаемся ввести большую сумму
		_.fireEvent('maxLimitReached');
		_.enteredSum.value = maxLimit(newSum);
		_.enteredSum.value = Math.round(_.enteredSum.value*100)/100;
		_.enteredSumText = _.enteredSum.value.toString();
		_.recipient.PaymentSum = _.enteredSum.value;
		_.fireEvent('actionsChanged');
		_.fireEvent('sumChanged');
		return false;
	}
	return true;
}

function removeLastChar()  {
	var prevText = _.enteredSumText;
	var idxDot = prevText.indexOf('.');
	if (prevText.length > 0)
		prevText = prevText.substring(0, prevText.length - 1);
	if (prevText.length > 0 && prevText.length - 1 == idxDot)
		prevText = prevText.substring(0, prevText.length - 1);
	if (prevText.length == 0)
		prevText = "0";
	_.enteredSumText = prevText;

	// _.enteredSum = Number(prevText);
	// _.amountTarget = canDispense(getTargetAmount(_.enteredSum), getTargetCurrency());
	// _.amountDispense = canDispense(getTargetAmount(_.enteredSum)-_.amountTarget, getOperationCurrency());
	// _.amountChange = getTargetAmount(_.enteredSum)-_.amountTarget-_.amountDispense;


	// _.recipient.PaymentSum = _.enteredSum;
	_.fireEvent("sumChanged");
	_.fireEvent("actionsChanged");
}

function appendChar(ch) {

	var idxDot = _.enteredSumText.indexOf(".");
	var digit = ch.charCodeAt(0) - 0x30;

	if ( !((digit >= 0 && digit <= 9) || (idxDot < 0 && (ch == '.' || ch == ',')) ) ){
		return false;
	}
	if (_.enteredSumText.length - idxDot > 2) {
		_.enteredSum.value = Math.round(_.enteredSum.value*100)/100;
		_.enteredSumText = ""+_.enteredSum.value;
	}
	//
	var newSum = _.enteredSumText;
	if (idxDot < 0 || (idxDot >= 0 && _.enteredSumText.length - idxDot <= 2)) {
		newSum += ch;
		if (!validateSum(newSum))
			return false;
	}
	_.enteredSumText = newSum;
//alert(Math.round(_.amountChange.value));
	_.fireEvent("sumChanged");
	_.fireEvent("actionsChanged");
	return true;
}

})();