﻿(function() {
_.addEventListeners({
	"load" : function() {
		_.registerVariables({
			"atLeastOneRead" : false,
			"barcode" : "",
			"setBarcodeErrorMessage" : null,
			"validateRecipientErrorMessage" : null,
			"recipientBarcodes" : PaymentGateway.SelectedRecipient.Barcodes,
			"notHandle" : false,
			"canContinue" : false
		});
		
		_.registerActions({
			"continue" : new StateAction(
				function() {return _.canContinue; }, 
				function() {
					onOperationComplete();
				}),
			"commit" : function() {
					if (recipientValidate())
						_.leave('commit');
					else
						_.fireEvent('validateRecipientFailed');
				},
			"correct" : function() { _.leave("correct"); },				
			"cancel" : function() { _.leave("cancel"); },
			"enableReading" : function() {
					_.notHandle = false;
			},	
			"testBarcodesState" : testBarcodesState
		});
		
		_.registerEvents([
				'barcodeComplete',
				'barcodeError',				
				'allFieldsComplete',
				'textChanged',
				'validateRecipientFailed'
			]);
		
		_.addEventListeners({
			"allFieldsComplete" : function() { 
				_.leave('correct');
			},
			"barcodeComplete" : function() {
				_['testBarcodesState']();
			},
			"barcodeError" : function() {
				_.notHandle = false;
			},
			"validateRecipientFailed" : function() { 
				_.leave("cancel");
			}
		});

		Framework.AttachEvent(BarcodeReader, "BarcodeRead", onBarcodeRead);		
		
	},
	
	"run" : function() {
		testBarcodesState();
	}
			  		
});

function testBarcodesState() {

if (_.recipientBarcodes.IsCompleted) {
	onOperationComplete();
	return;
} 

var barcodesState;

if (_.recipientBarcodes.IsDefined) {

	if (!_.atLeastOneRead) {
		if (_.recipientBarcodes.IsRequired) {
			_.canContinue = false;
			barcodesState = "NotDefined";
		} else {
			_.canContinue = true;
			barcodesState = "NotRequired";			
		}
	} else  {
		if (getParameter('barcodeEntry.commitWhenDefined', false)) {
			_.leave('correct');
			return;
		} else {
			_.canContinue = true;
			barcodesState = "Defined";		
		}
	}

} else {

	if (_.recipientBarcodes.IsRequired) {
		_.canContinue = false;
		barcodesState = "NotDefined";			
	} else {
		_.canContinue = true;
		barcodesState = "NotRequired";		
	}	

}

	
_.fireEvent('textChanged', barcodesState);
_.fireEvent('actionsChanged');

_.notHandle = false;
	
}

function onBarcodeRead(barcode) {

	if (_.notHandle)
		return;

	_.barcode = barcode;
	
	var result;
	
	try {	
		result = _.recipientBarcodes.SetBarcode(_.barcode);	
		//TODO: Проверить, что возвращается именно 0, а не undefined
		if (result != 0) {
			_.setBarcodeErrorMessage = getMessage('barcodeEntry.setBarcodeErrors', result);
			_.notHandle = true;
			_.fireEvent('barcodeError');
			return;
		}			
	} catch(e) {
		// Ошибка декодированная в сообщение
		_.setBarcodeErrorMessage = e.message;
		_.notHandle = true;
		_.fireEvent('barcodeError');
		return;		
	}
	
	if (!_.atLeastOneRead)
		_.atLeastOneRead = true;
	
	_.notHandle = true;
	_.fireEvent('barcodeComplete');
	
}

function recipientValidate() {

	try {
		var result = PaymentGateway.SelectedRecipient.Validate();
		//TODO: Проверить, что возвращается именно undefined, а не 0 и не что-нибудь еще
		if (result !== undefined) {
			_.validateRecipientErrorMessage = getMessage('validateRecipientErrors', result);
			return false;
		}
	} catch(e) {
		_.validateRecipientErrorMessage = e.message;
		return false;			
	}
	
	return true;
	
}

function onOperationComplete() {

	_.notHandle = true;

	if (_.recipientBarcodes.IsCompleted) {
		
		var fields = PaymentGateway.SelectedCatalogItem.Fields;
		for (var i = 0, c = fields.Count; i < c; i++) {
			if (fields.Item(i).IsInput && fields.Item(i).Visibility != 4 && !fields.Item(i).IsDefined) {
				_.leave('correct');
				return;
			}
		}		
	
		if (_.recipientBarcodes.IsRequired) 
			_['commit']();
		else		
			_.fireEvent('allFieldsComplete');
			
		return;
		
	}
	
	_.leave('correct');
	
}
})();