﻿(function() {
_.addEventListeners({
	"load" : function() {
	
		// Создаем события для взаимодействия с отображением
		_.registerEvents([
			'unableToPay'
		]);
		
		_.registerActions({
			"selectItem" : function(itemIdx) {

					PaymentGateway.SelectRecipient(itemIdx);
					
					// Если у нас нет работающего принтера, а печатать надо, то выходим
					if (!Printer.DeviceReady && isRecieptRequired()) {
						_.fireEvent('unableToPay');
						PaymentGateway.SelectedCatalogItem.SelectParent();
						return;
					}

					_.leave("recipientSelected");
				},
			"cancel" : function() {
					_.leave("recipients");
				},
			// Выход в root
			"menu" : new StateAction(
					// Продолжать  можно если
					function ()  {
						return !VirtuPOS.IsCardPresent;
					},
					function () { 
						_.leave("initialize");
					}),
			// Выдать карту		
			"ejectCard" : new StateAction(
				function ()  {
					return VirtuPOS && !VirtuPOS.Offline &&	VirtuPOS.IsCardPresent;
				},
				function() {
					Framework.Variables("EjectReason") = getMessage("ejectReason.cancel");
					_.leave("ejectCard");
				})
				
		});
	}	

});

function isRecieptRequired() {
	return (PaymentGateway.SelectedRecipient && PaymentGateway.SelectedRecipient.InCategory("ReceiptRequired"))
			|| PaymentGateway.SelectedCatalogItem.InCategory("ReceiptRequired");
}

})();
