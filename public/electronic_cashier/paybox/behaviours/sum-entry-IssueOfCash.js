﻿( function() {

_.addEventListeners({

	// Обработчик загрузки текущего состояния
	"load" : function() {
		// события для взаимодействия с view
		_.registerEvents([
			"sumChanged", "maxLimitReached"
		]);

		// Создаем события для взаимодействия с отображением
		_.registerVariables({
			"enteredSum" : 0,
			"enteredSumText" : "0",
			"recipient" : PaymentGateway.SelectedRecipient,
			"currency" : PaymentGateway.SelectedRecipient.Fields.GetFieldByID(101).Value
		});

		// Создаем доступные на странице действия с условиями возможности их применения
		 _.registerActions({
			"cancel" : function () { _.leave('cancel'); },
			"commit" : new StateAction(
					// Продолжать  можно если
					function ()  {
						if ((_.enteredSum >= minLimit()) && (_.enteredSum > commissionSum()) && CheckCanDispense()) {
							//если сдача + принятые больше минимальной суммы платежа и суммы хватает на комиссию
							return true;
						}
						return false;
					},
					function () { 
						doPayment(_.enteredSum);
						_.leave('commit');
					}),
			"appendChar" : appendChar,
			"removeLastChar" : removeLastChar
		});

		if (typeof(getOperationCurrency) !== "undefined") {
			_.recipient.CurrencyCode = getOperationCurrency();
		}
		PaymentGateway.SelectedRecipient.CurrencyCode = PaymentGateway.SelectedRecipient.Fields.GetFieldByID(101).Value;
		if (_.recipient.TransferAmountType == 1) {
			_.enteredSum = _.recipient.TransferSum;
			_.enteredSumText = _.enteredSum.toString();
			_.recipient.PaymentSum = _.enteredSum;
			if (!getParameter("sumEntry.confirmFixedSum", true)) {
				_['commit']();
				return false;
			}
		} else {
			_.enteredSum = 0;
			_.enteredSumText = _.enteredSum.toString();
			_.recipient.PaymentSum = 0;		
		}
	}
});
function CheckCanDispense(){
	if (!PaymentGateway.SelectedRecipient.InCategory("IssueOfCash"))
		return true;
	var yesOrNo = 0;
	try {
		yesOrNo = PaymentDevice.CanDispense(_.enteredSum*100, _.currency)/100.0;
	} catch (e) {
		yesOrNo = 0;
	}
	if (yesOrNo == 0 || yesOrNo !=_.enteredSum)
		return false;
	else
		return true;
}
function validateSum(newSum) {
	if (newSum > maxLimit(newSum)) {  //пытаемся ввести большую сумму
		_.fireEvent('maxLimitReached');
		_.enteredSum = maxLimit(newSum);
		_.enteredSummText = _.enteredSum.toString();
		_.recipient.PaymentSum = _.enteredSum;
		_.fireEvent('actionsChanged');
		_.fireEvent('sumChanged');
		return false;
	}
	return true;
}

function removeLastChar()  {
	var prevText = _.enteredSumText;
	var idxDot = prevText.indexOf('.');
	if (prevText.length > 0)
		prevText = prevText.substring(0, prevText.length - 1);
	if (prevText.length > 0 && prevText.length - 1 == idxDot)
		prevText = prevText.substring(0, prevText.length - 1);
	if (prevText.length == 0)
		prevText = "0";
	_.enteredSumText = prevText;
	_.enteredSum = Number(prevText);
	_.recipient.PaymentSum = _.enteredSum;
	_.fireEvent("sumChanged");
	_.fireEvent("actionsChanged");
}

function appendChar(ch) {

	var idxDot = _.enteredSumText.indexOf(".");
	var digit = ch.charCodeAt(0) - 0x30;

	if ( !((digit >= 0 && digit <= 9) || (idxDot < 0 && (ch == '.' || ch == ',')) ) ){
		return false;
	}
	if (_.enteredSumText.length - idxDot > 2) {
		_.enteredSum = Math.round(_.enteredSum*100)/100;
		_.enteredSumText = ""+_.enteredSum;
	}
	//
	var newSum = _.enteredSumText;
	if (idxDot < 0 || (idxDot >= 0 && _.enteredSumText.length - idxDot <= 2)) {
		newSum += ch;
		if (!validateSum(newSum))
			return false;
	}
	_.enteredSumText = newSum;
	_.enteredSum = Number(_.enteredSumText);
	_.recipient.PaymentSum = _.enteredSum;

	_.fireEvent("sumChanged");
	_.fireEvent("actionsChanged");
	return true;
}

})();