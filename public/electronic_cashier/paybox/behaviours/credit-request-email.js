﻿(function() {
_.addEventListeners({
	"load" : function() {
	
		_.registerEvents(["mailSending", "mailSend"]);
		
		_.registerVariables({
			"sendSuccess" : false,
			"getDbtDate" : getDbtDate, 
			"number" : 0, 
			"address" : "" 
		});
			
		_.registerActions({
				"commit" : new StateAction(
					function ()  {
						return true;
					},
					function() {
						Framework.Variables("EjectReason") = getMessage("ejectReason.cancel");
						_.leave("ejectCard");
					})
		});
	}, 
	"run" : function() {
		sendingMail();
	}

});

function sendingMail() {
	_.fireEvent("mailSending");
	_.number = couponProperties.terminalNumber();
	_.address = couponProperties.terminalAddress();
	// Таймаут для отрисовки интерфейса	
	setTimeout(function () { sendMail(getMessage("creditRequestEmail.message"), getMessage("creditRequestEmail.receiver"));}, 500);
}

/*
	Возвращает отформатирвоаннубю дату для электронного письма
*/

function getDbtDate(){
	var d = new Date();
	var curr_date = d.getDate();
	var curr_month = d.getMonth() + 1; 
	if(curr_month<10){curr_month = "0"+curr_month.toString(); }
	var curr_year = d.getFullYear();
	var da = curr_date + "." + curr_month + "." + curr_year;
	return da + " " + new Date().toString().match(/\d{2}:\d{2}:\d{2}/)[0];
}
/*
	Посылает электронное письмо.
	messageText - текст сообщения
	receiver    - получатель
*/
function sendMail(messageText, receiver) {
	if (typeof KBExternal.CDO === "undefined") {
		_.sendSuccess = false;
		_.fireEvent("mailSend");
		return;
	}
	var CDO = KBExternal.CDO;
	CDO.Subject = getMessage("creditRequestEmail.subject");
	CDO.From = getMessage("creditRequestEmail.from");
	CDO.To = receiver;
	CDO.TextBody = messageText;
	CDO.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = getMessage("creditRequestEmail.smtpserver"); //'4212-exedge01';//"192.168.132.173"; //4212-exedge01 
	CDO.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 0;
	CDO.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2; 
	CDO.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25;
	CDO.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = false;
	CDO.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60;
	CDO.Configuration.Fields.Update();
	try {
		CDO.Send();
		_.sendSuccess = true;
	} catch(e) {
		_.sendSuccess = false;
	}
	_.fireEvent("mailSend");
}

})();
