﻿(function() {
_.addEventListeners({
	// Обработчик load
	"load" : function() {
		_.registerVariables({
			"operationCompleted" : false, // Флаг завершения операции
			"operationSucceeded" : false, // Флаг успешности завершения операции
			/* здесь могут быть осталные переменные состояния */
			"errorText" : null,
			"dispenseResult" : null,
			"unDispensedSum" : 0,
			"sumToDispense" : null,
			"dispensingSum" : null,
			"idxDispense" : 0,
			"waitForTake" : null
		});
		
		if (!_.sumToDispense) {
			_.sumToDispense = [];
			for (var i = 0, c = PaymentDevice.AccumulatedSum.Count; i < c; i++) { 
				var as = PaymentDevice.AccumulatedSum(i);
				var canDispenseValue = PaymentDevice.CanDispense(as.Value, as.CurrencyCode);
				_.sumToDispense.push(new CurrencyAmount(canDispenseValue/100, as.CurrencyCode))
			}
		}
		// Событие завершения операции
		// Добавляем обработчик по умолчанию (если view не вернуло явно в обработчике false с намерением попоказывать что-нибудь клиенту и спросить о чем-нибудь)
		_.registerEvents({
			'taken' : _['nextDispense'],
			'dispenseComplete' : _['nextDispense'],
			'dispenseStart' : null,
			'operationComplete' : _['continue']
		});
		
		/*if (!PaymentDevice.DeviceReady) {
			_.leave('fail');
			return false;
		}*/

		// Действия
		_.registerActions({
			// Действие следующая сумма к выдаче
			'nextDispense' : 
				new StateAction(
				function() { return _.waitForTake == null || _.waitForTake.length == 0 },
				function() {
					if (_.idxDispense >= _.sumToDispense.length) {	
							// Отправляем уведомление во View. Если View не обработал событие, то самостоятельно вызываем continue
							_.fireEvent('operationComplete');
						} else {
							dispenseNextAmount();
						}
				}),
			// Действие завершения состояния
			"continue" : new StateAction(
				function() {return _.operationCompleted; }, // Доступно только по окончании выполнения
				function() {
					// Здесь обработка результата и соответсвующий leave
					if (_.operationSucceeded)
						_.leave('success')
					else
						_.leave('fail');
				}),
			"retractNotes" : new StateAction(
					function() { return _.idxDispense > 0 },
					function() { 
						if (_.waitForTake != null && _.waitForTake.length != 0)
							_.leave("retract"); 
						else
							_.leave('success')
					}
				)
		});
		
		// подключение к Dispenser
		Framework.AttachEvent(PaymentDevice, "OnDeviceDispenseComplete", onDeviceDispenseComplete);
		Framework.AttachEvent(PaymentDevice, "OnDispenseComplete", onDispenseComplete);
		//
		Framework.AttachEvent(PaymentDevice, "OnDevicePresentComplete", onDevicePresentComplete);
		Framework.AttachEvent(PaymentDevice, "OnPresentComplete", onPresentComplete);
		//забраны пользователем
		Framework.AttachEvent(PaymentDevice, "OnTaken", onTaken);
		_.addEventListeners({
				"operationComplete" : _['continue'],
				"taken" : _['nextDispense']
		  });
	},
	
	"run" : function() {
		// Перед обработкой view устанавливаем результат операции
		// Этот обработчик вызовется до обработчика из view зарегестрированного в load
		_.addEventListeners({
				"operationComplete" : function() {
				 _.operationCompleted = true;
				 _.fireEvent('actionsChanged');
				}
			  });
		//	Здесь выполняется запуск исполнения операции
		_['nextDispense']();
	},
	"inactivityTimer" : function () {
		if (_.operationCompleted)
			_['continue']();
		if (_.mayExecute('retractNotes')) {
			_['retractNotes']();
			return false;
		}
		_['nextDispense']();
		return false; // не можем завершиться по таймауту
	}
});

function dispenseNextAmount() {
	_.dispensingSum = new CurrencyAmount(_.sumToDispense[_.idxDispense].value, _.sumToDispense[_.idxDispense].currency);
	if (_.dispensingSum.value == 0) {
		_.idxDispense++;
		_['nextDispense']();
		return;
	}
	var c = getCurrency(_.dispensingSum.currency);
	_.fireEvent('dispenseStart');
	
	//alert([c.getMinorAmount(_.dispensingSum.value), c.alphaCode]);
	try {
		PaymentDevice.Dispense(c.getMinorAmount(_.dispensingSum.value), c.alphaCode);
	} catch (e) {
		alert(e);
	}
}
/* 
Завершена операция, 
Аналогичная функция должна быть вызвана по окончании выполнения операции
*/
function onDispenseComplete() {
}

function onPresentComplete() {
	_.operationCompleted = true;
	// Здесь Сохраняем результат для обработки во view или где-то еще
	//_.operationSucceeded = result === 0;
	_.unDispensedSum = _.dispensingSum.value - Math.round(PaymentDevice.LastDispensedSum/100.0);

//	alert(_.unDispensedSum);
	_.operationSucceeded = true; //_.unDispensedSum == 0;
	_.fireEvent('dispenseComplete');
	//
	var c = getCurrency(_.dispensingSum.currency);
	PaymentDevice.UseAccumulatedSum(_.dispensingSum.currency, c.getMinorAmount(_.dispensingSum.value));


	_.idxDispense++;
	// Таймаут для забора денег
	if (_.operationSucceeded && _.waitForTake && _.waitForTake.length != 0)
		_.resetInactivityTimer(getParameter("dispenser.takeTimeout", 15000));
}

function onDeviceDispenseComplete(deviceId, needPresent) {
	if (needPresent) 
		onDeviceDispenseComplete(deviceId);
}
function onDevicePresentComplete(deviceId) {
	if (_.waitForTake == null)
		_.waitForTake = [];

	for (var i = 0; i < _.waitForTake.length; i++) {
		if (_.waitForTake[i] == deviceId) return;
	}

	_.waitForTake.push(deviceId);
}

function onTaken(deviceId) {
	if (_.waitForTake != null) {
		for (var i = 0; i < _.waitForTake.length; i++) {
			if (_.waitForTake[i] == deviceId) {
				_.waitForTake.splice(i, 1);
			}
		}
	}
	if (_.waitForTake == null || _.waitForTake.length == 0)
		_.fireEvent('taken');
}

})();