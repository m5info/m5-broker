﻿_.addEventListeners({
	"load" : function() {
		Framework.AttachEvent(Printer, "OnPrinterEvent", printerDeviceStatus);
		Framework.AttachEvent(Printer, "OnFiscalPrinterEvent", printerDeviceStatus);


		_.registerActions({
			"initialize" : new StateAction(
				function () { return Printer.DeviceReady; },
				function() { _.leave("deviceOK") } ),
			"commit" : function() { _.leave("deviceOK"); }
		});
	},
	"inactivityTimer" : function() {
		return false;
	}
});

function printerDeviceStatus(newStatus, newSubStatus) {
		
	if (Printer.DeviceReady) {
		setTimeout(function() {
			_["initialize"]();
		}, 5000);
		return true;
	}
	return false;

}
