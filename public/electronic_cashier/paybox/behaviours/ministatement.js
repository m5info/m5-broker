﻿(function() {

_.addEventListeners({
	"load" : function() {
		_.registerVariables({
			"operationCompleted" : false, 
			"operationSucceeded" : false,
			"collection" : null
		});
		
		_.registerActions({
			"continue" : new StateAction(
				function() {return _.operationCompleted; },
				function() {
						_.leave('finish');
				}),
			"printBC" : printBC
		});
		
		_.registerEvents(['operationComplete', 'printerSFailure', 'printDone']);
		_.addEventListeners({"operationComplete" : function() { 
				_['continue']();
			}
		});

		Framework.AttachEvent(VirtuPOS, "OnMiniStatementComplete", onOperationComplete);
		Framework.AttachEvent(VirtuPOS, "OnRereadIC", miniStatementQuery);
		
	},
	
	"run" : function() {

		_.addEventListeners({
				"operationComplete" : function() {
				 _.operationCompleted = true;
				 _.fireEvent('actionsChanged');
				}
			  });
			
			if (VirtuPOS && !VirtuPOS.Offline) {
				//VirtuPOS.Clear();
				if (!VirtuPOS.IsFallback) {
					VirtuPOS.RereadIC();
				} else {
					miniStatementQuery();
				}	
			}
			else {
				onOperationComplete();
			}
			  
	}
});

function onOperationComplete(Coll) {
	
	_.operationSucceeded = (VirtuPOS.ResponseCode == 0 || VirtuPOS.ResponseCode == 1) && Coll;
	
	if (_.operationSucceeded)
		_.collection = Coll;
	
	_.fireEvent('operationComplete');
	
}

function printBC() {

	if (!(Printer && Printer.DeviceReady)) {
		_.fireEvent('printerSFailure');
		return;
	}

	printBalanceCoupon(VirtuPOS.AuthCode, VirtuPOS.MaskedCardNr, VirtuPOS.TransactionNumber);
	_.fireEvent('printDone');
}

function miniStatementQuery() {
	
	vmcounter++;
	_.fireEvent('executingVirtuPOSMethod');
	try {
		VirtuPOS.MiniStatement();
	} catch(e) {
		if (VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsBusy && vmcounter <= getParameter("VirtuPOS.attemptsNumber", 5)) {
			_.fireEvent('tryVirtuPOSMethodAgain');
			setTimeout(miniStatementQuery, getParameter("VirtuPOS.retryTime", 2000));
		} else
			onOperationComplete(false)
	}	
	
}

})();