(function(){

_.addEventListeners({
	"load" : function() {
		_.registerVariables({
			"operationCompleted" : false,
			"operationSucceeded" : false
		});

		_.registerActions({
			"continue" : new StateAction(
				function() {return _.operationCompleted; },
				function() {
					// ����� ���������� ����
					for (var i = 0, c = PaymentDevice.AccumulatedSum.Count; i < c; i++) { 
						var as = PaymentDevice.AccumulatedSum(i);
						PaymentDevice.ResetAccumulatedSum(as.CurrencyCode);
					}
					// ����� ��������� ���������� � �������������� leave
					if (_.operationSucceeded) _.leave('success');
					else _.leave('fail');
				})
		});
		
		// ������������ ���������� � ��������� ���������� �� ��������� (���� view �� ������� ���� � ����������� false � ���������� ������������ ���-������ ������� � �������� � ���-������)
		_.registerEvents({
			"operationComplete" : _['continue']
		});
		
		// ����������� � KioskPayments
		Framework.AttachEvent(PaymentDevice, "OnRetractCompleted", onRetractCompleted);
		setTimeout(onRetractCompleted, 5000);
	},
	
	"run" : function() {
		// ����� ���������� view ������������� ��������� ��������
		// ���� ���������� ��������� �� ����������� �� view ������������������� � load
		_.addEventListeners({
				"operationComplete" : function() {
				 _.operationCompleted = true;
				}
		});
		_.resetInactivityTimer(getParameter("dispenser.retractTimeout", 15000));
		PaymentDevice.Retract();
	},
	"inactivityTimer" : function () {
		if (_.operationCompleted)
			_['continue']();
		return false; // �� ����� ����������� �� ��������
	}

});

/* ��������� �������� */
function onRetractCompleted() {
	// �������� �������� � ������ rcrSucceeded || rcrNeedNextCheck 
	_.operationSucceeded = true;
	// ���������� ����������� �� View. ���� View �� ��������� �������, �� �������������� �������� commit
	_.fireEvent('operationComplete');
}
})()
