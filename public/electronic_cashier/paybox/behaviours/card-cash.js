﻿( function() {

Framework.Variables("paymentResults") = {
	length : 0
};
var vmcounter;
_.addEventListeners({
	"load" : function() {
		if (!VirtuPOS || VirtuPOS.Offline) {
			_.leave("fail");
			return false;
		}

		_.registerVariables({
			"operationCompleted" : false, // Флаг завершения операции
			"paymentSucceeded" : false, // Флаг успешности завершения одного платежа
			"activeRecipientIdx" : 0,
			"recipientsArray" : [ ],
			"activeRecipient" : null,
			"deferredPayment" : false,
			"operationSucceeded" : false,
			"cardCaptured" : false
		});	
	
		_.registerActions({
			// Действие завершения состояния
			"continue" : new StateAction(
				function() { return _.operationCompleted && !_.cardCaptured }, // Доступно только по окончании выполнения
				function() { 
					if (_.operationSucceeded)
						_.leave('success');
					else 
						_.leave('fail');
				})
				
		});	
	
		// События для взаимодействия с view
		_.registerEvents(['operationComplete', 'paymentComplete' , 'executingVirtuPOSMethod' , 'tryVirtuPOSMethodAgain']);

		// Добавляем обработчик по умолчанию (если view не вернуло явно в обработчике false с намерением попоказывать что-нибудь клиенту и спросить о чем-нибудь)
		_.addEventListeners({
		
		"paymentComplete" : function() {
			
				if (!nextRecipient()) {
					_.fireEvent('operationComplete');
				} else {
					checkFBandPay();
				}
			
			},
			
		"operationComplete" : function() {
					_['continue']();
			}
			
		});		

		// потестировать коллекцию локально
		if (PaymentGateway.RecipientCollection.Count > 0){
			for (i = 0; i < PaymentGateway.RecipientCollection.Count; i++){
				_.recipientsArray.push(PaymentGateway.RecipientCollection.Item(i));
			}
		} else {
			_.recipientsArray.push(PaymentGateway.SelectedRecipient);
		}
		
		// подключение к VirtuPOS
		Framework.AttachEvent(VirtuPOS, "OnPaymentComplete", onPaymentComplete);
		Framework.AttachEvent(VirtuPOS, "OnPurchaseComplete", onPurchaseComplete);
		Framework.AttachEvent(VirtuPOS, "OnCardCaptured", onCardCaptured);
		Framework.AttachEvent(VirtuPOS, "OnRereadIC", doRecipientPayment);		
		Framework.AttachEvent(VirtuPOS, "OnCashAdvanceComplete", onCashAdvanceComplete);	
	},
	"inactivityTimer" : function () {
		return false;
	},
	
	"run" : function() {
	
		// Перед обработкой view устанавливаем результат операции
		// Этот обработчик вызовется до обработчика из view зарегестрированного в load
		_.addEventListeners({
				"operationComplete" : function() {
				 _.operationCompleted = true;
				 _.fireEvent('actionsChanged');
				 if (getParameter("cardPayment.showResults", false) && _.recipientsArray.length != 1) return false;
				}
			  });

	_.activeRecipient = _.recipientsArray[_.activeRecipientIdx];
	PaymentGateway.SelectedRecipient = _.activeRecipient;
	
	checkFBandPay();
			  
	}
		
});

function checkFBandPay() {
	vmcounter = 0;
	if (!VirtuPOS.IsFallback && VirtuPOS.IsCardPresent) {
		VirtuPOS.RereadIC();
	} else {
		doRecipientPayment();
	}	
}

function nextRecipient(){
	_.activeRecipientIdx += 1;
	if  (_.activeRecipientIdx >= _.recipientsArray.length)
		return false;
	_.activeRecipient = _.recipientsArray[_.activeRecipientIdx];
	PaymentGateway.SelectedRecipient = _.activeRecipient;
	return PaymentGateway.SelectedRecipient;

}
// выбор метода платежа
function doRecipientPayment() {
	Recipient = _.activeRecipient;

	vmcounter++;
	_.fireEvent('executingVirtuPOSMethod');
	
	try {
		if (getParameter("cardPayment.issueOfCash", false)){
			VirtuPOS.CurrencyCode = getCurrency(PaymentGateway.SelectedRecipient.CurrencyCode).numericCode;
			//alert(getCurrency(PaymentGateway.SelectedRecipient.CurrencyCode).numericCode);
			VirtuPOS.CashAdvance(getParameter("cardPayment.disableCardPaymentCommission") ? Recipient.TransferSum : Recipient.PaymentSum, PaymentGateway.DocumentNumber);
		}
		if (getParameter("cardPayment.cardPurchase", false)){
			VirtuPOS.Purchase(getParameter("cardPayment.disableCardPaymentCommission") ? Recipient.TransferSum : Recipient.PaymentSum, PaymentGateway.DocumentNumber);
		}
		else {
			var paramArray = new Array();
			var j=0;
			for (var i=0; i<Recipient.Fields.Count; i++) {
				if (Recipient.Fields.Item(i).ID >= 100 && isDefined(Recipient.Fields.Item(i).Value) && Recipient.Fields.Item(i).IsInput) {
					paramArray[j] = Recipient.Fields.Item(i).Value;
					j++;
				}
			}
		//	if (_.virtuPOSCheck && PaymentGateway.SelectedRecipient.Fields.GetFieldByID(1001)) 
		//		VirtuPOS.CurrencyCode = PaymentGateway.SelectedRecipient.Fields.GetFieldByID(1001).Value;
		//	VirtuPOS.Commission = Recipient.CommissionSum;
		//	VirtuPOS.Payment(getParameter(("cardPayment.disableCardPaymentCommission") || _.virtuPOSCheck) ? Recipient.TransferSum : Recipient.PaymentSum, Recipient.Fields.GetFieldByID("1000").Value, paramArray, PaymentGateway.DocumentNumber);	
		
		}
	} catch (e) {
		if (VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsBusy && vmcounter <= getParameter("VirtuPOS.attemptsNumber", 5)) {
			_.fireEvent('tryVirtuPOSMethodAgain');
			setTimeout(doRecipientPayment, getParameter("VirtuPOS.retryTime", 2000));
		} else
			onPaymentComplete(false);
	}
		
}


function onCashAdvanceComplete(vbSuccess) {
	
	// До тех пор пока не будет отдельно обрабатываться Purchase
	onPaymentComplete(vbSuccess);
}
function onPurchaseComplete(vbSuccess) {
	
	// До тех пор пока не будет отдельно обрабатываться Purchase
	onPaymentComplete(vbSuccess);
}

function onPaymentComplete(vbSuccess) {
	
	_.paymentSucceeded = vbSuccess;
	_.operationSucceeded = vbSuccess;
	var recipientId = _.activeRecipientIdx + 1;
	// сохраняем информацию о платеже
	Framework.Variables("paymentResults").length = recipientId;
	Framework.Variables("paymentResults")[recipientId] = {
		"recipient" : PaymentGateway.SelectedRecipient,
		"errorCode" : VirtuPOS.ResponseCode,
		"errorText" : getMessage("cardPayment", VirtuPOS.ResponseCode)
	}
	_.fireEvent('paymentComplete', VirtuPOS.ResponseCode);	
}

function onCardCaptured() {
	_.cardCaptured = true;
	Framework.Variables("MaskedCardNr") = VirtuPOS.MaskedCardNr;
	_.leave("fail-card-captured");

}

} )();
