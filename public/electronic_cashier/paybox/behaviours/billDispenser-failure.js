﻿_.addEventListeners({
	"load" : function() {

		_.registerActions({
			"initialize" : new StateAction(
				function () { return Dispenser.DeviceReady; },
				function() {
					if (!_.leave("deviceOK")) {
					}
			}),
			"commit" : function() { _.leave("deviceOK"); }
		});
	},
	"billDispenserStatus" : function (newStatus, newSubStatus) {
		
		if (Dispenser.DeviceReady) {
			_["commit"]();
			return true;
		}
		return false;
	}
});
