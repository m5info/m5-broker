﻿( function() {

	_.addEventListeners({
		"load" : function() {
		
			_.registerActions({
				"commit" : function() {
						_.leave("commit");
					}
			});
		},
		
		"run" : function() {
			if (Printer && Printer.DeviceReady)
				printCaptureCoupon();
			VirtuPOS.Clear();
			VirtuPOS.DisableReader();			
		}	
		
	});

})();