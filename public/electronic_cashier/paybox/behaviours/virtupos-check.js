﻿(function(){
_.addEventListeners({
	"load" : function() {

		_.registerVariables({
			"operationCompleted" : false,
			"operationSucceeded" : false,
			"checkResult" : null
		});

		_.registerActions({
			"continue" : new StateAction(
				function() {return _.operationCompleted; },
				function() {
					// Здесь обработка результата и соответсвующий leave
					if (_.operationSucceeded) _.leave('success');
					else _.leave('fail');
				}),
			"commit" : new StateAction(
				function() { return _.operationCompleted && _.operationSucceeded },
				function() {
					_.leave('success');
				}
			),
			// Действие по выходу из состояния с отменой. Может отсутствовать вовсе.
			"cancel" : new StateAction(
				function() { return _.operationCompleted; }, // Доступно только по окончании выполнения
				function() { 
					// Выход по отмене
					_.leave("cancel"); 
				})
		});
		
		// Регистрируем обработчик и добавляем обработчик по умолчанию (если view не вернуло явно в обработчике false с намерением попоказывать что-нибудь клиенту и спросить о чем-нибудь)
		_.registerEvents(['operationComplete', 'binTableNone' , 'executingVirtuPOSMethod' , 'executingMethod' , 'tryVirtuPOSMethodAgain']);
		
		_.addEventListeners({	
		"operationComplete" : function() {
					_['continue']();
			}		
		});			
		
		// подключение к VirtuPOS
		Framework.AttachEvent(VirtuPOS, "OnPaymentComplete", onCheckComplete);
	},
	
	"run" : function() {
		// Перед обработкой view устанавливаем результат операции
		// Этот обработчик вызовется до обработчика из view зарегестрированного в load
		_.addEventListeners({
				"operationComplete" : function() {
				 _.operationCompleted = true;
				}
			  });
		try {
			var paramArray = new Array();
			var j=0;
			var Recipient = PaymentGateway.SelectedRecipient;
			for (var i=0; i<Recipient.Fields.Count; i++) {
				if (Recipient.Fields.Item(i).ID >= 100 && isDefined(Recipient.Fields.Item(i).Value) && Recipient.Fields.Item(i).IsInput) {
					paramArray[j] = Recipient.Fields.Item(i).Value;
					j++;
				}
			}		
			VirtuPOS.Payment(getParameter("virtuposCheck.virtualSum", 501.11), Recipient.Fields.GetFieldByID("1000").Value, paramArray, PaymentGateway.DocumentNumber);	
		} catch (e) {
			onCheckComplete(false);
		}
	}
});

function DoCheck() {
	vmcounter++;
	_.fireEvent('executingVirtuPOSMethod');
	try {
		var paramArray = new Array();
		var j=0;
		var Recipient = PaymentGateway.SelectedRecipient;
		for (var i=0; i<Recipient.Fields.Count; i++) {
			if (Recipient.Fields.Item(i).ID >= 100 && isDefined(Recipient.Fields.Item(i).Value) && Recipient.Fields.Item(i).IsInput) {
				paramArray[j] = Recipient.Fields.Item(i).Value;
				j++;
			}
		}		
		VirtuPOS.Payment(getParameter("virtuposCheck.virtualSum", 501.11), Recipient.Fields.GetFieldByID("1000").Value, paramArray, PaymentGateway.DocumentNumber);	
	} catch (e) {
		if (VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsBusy && vmcounter <= getParameter("VirtuPOS.attemptsNumber", 5)) {
			_.fireEvent('tryVirtuPOSMethodAgain');
			setTimeout(DoCheck, getParameter("VirtuPOS.retryTime", 2000));
		} else	
			onCheckComplete(false);
	}	
}

/* Завершена операция */
function onCheckComplete(requestCompleteResult) {
	_.operationSucceeded = requestCompleteResult == true;
	// Обнуляем, чтобы не возникла ошибка поведения в after-data-check
	Framework.Variables("checkResult") = 0;
	_.checkResult = VirtuPOS.ResponseCode;	
	// Отправляем уведомление во View. Если View не обработал событие, то самостоятельно вызываем commit
	_.fireEvent('operationComplete');
}
})()
