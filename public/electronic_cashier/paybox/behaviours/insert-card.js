﻿(function() {
_.addEventListeners({
	"load" : function() {
		if (!VirtuPOS || VirtuPOS.Offline) {
			_.leave("fail");
			return false;
		}

		/* Вошли с картой в ридере */

		/* События VirtuPOS */
		Framework.AttachEvent(VirtuPOS, "OnCardRead", onCardRead);
		Framework.AttachEvent(VirtuPOS, "OnCardPickedUp", onCardPickedUp);
		Framework.AttachEvent(VirtuPOS, "OnCardRejected", onCardRejected);				
		Framework.AttachEvent(VirtuPOS, "OnBusy", onBusy);
		Framework.AttachEvent(VirtuPOS, "OnIdle", onIdle);
		Framework.AttachEvent(VirtuPOS, "OnCardCaptured", onCardCaptured);		
		Framework.AttachEvent(VirtuPOS, "OnCardInserting", OnCardInserting);		
		Framework.AttachEvent(VirtuPOS, "OnCardRemoved", OnCardRemoved);
		
		_.registerEvents(['cardReject', 'captureTick', 'readerEnabled' , 'startingApplication']);

		_.registerVariables({
			"readerEnabled" : false,
			"cardRejectReason" : 0,
			"readerIdle" : true,
			"CardCaptured" : false
		});
	
		_.registerActions({
			"commit" : new StateAction(
				function() { return VirtuPOS.IsCardPresent; },
				function() {_.leave("commit") }),
			"capture" : function() { 

       				// Сброс оставшихся сумм
       				for (var i = 0, c = PaymentDevice.AccumulatedSum.Count; i < c; i++) { 
       					var as = PaymentDevice.AccumulatedSum(i);
       					PaymentDevice.ResetAccumulatedSum(as.CurrencyCode);
       				}
				_.leave("capture") 
			},
			"cancel" : new StateAction(
				function() { 
					return _.readerIdle;
				},
				function() { 
					if (VirtuPOS.IsCardPresent) {
						_.leave("ejectCard") 
					}
					else {
						disableReader();
						if (PaymentGateway.SelectedRecipient == null)
							PaymentGateway.SelectedCatalogItem.SelectParent();
							_.leave("cancel")
					}
				})
		});	
	},
	"run" : function () {
		if (Framework.Variables("cardWrite")) {
			try {
				VirtuPOS.Clear();
			} catch(e) {};
			Framework.Variables("cardWrite") = false;
		}
		if (VirtuPOS.IsCardPresent || VirtuPOS.Track2Data.length != 0) {
			Framework.Variables("EjectReason") = "";
			_.leave("ejectCard");
			return false;
		}	
		
		if (!VirtuPOS || VirtuPOS.Offline)
			_.leave("cancel");
		enableReader();
		
	},
	"unload" : function () {
		disableReader();
	}
});

function enableReader() {
	if (!_.readerEnabled) {
		try {
			VirtuPOS.EnableReader();
			_.readerEnabled = true;
		} catch(e) {};
		_.fireEvent("readerEnabled");
		_.fireEvent("actionsChanged");
	}
}
function disableReader() {
	if (!VirtuPOS.IsCardPresent && _.readerEnabled) {
		try {
			VirtuPOS.DisableReader();
			_.readerEnabled = false;
		} catch(e) {};		
		_.fireEvent("actionsChanged");
	}	
}

function onCardRead() {
	Framework.Variables("MaskedCardNr") = VirtuPOS.MaskedCardNr;

	ViewButtons.update();
	_.readerIdle = false;
	_.fireEvent("actionsChanged");
	if (isCardTypeOf("encashment"))
		_.leave('encashment');
	else if (PaymentGateway.SelectedCatalogItem.Recipient && 
			(PaymentGateway.SelectedRecipient.InCategory("PeerToPeer") || 
			PaymentGateway.SelectedRecipient.InCategory("MiniStatementQuery") ||
			PaymentGateway.SelectedRecipient.InCategory("BalanceQuery")) && 
			!isCardTypeOf("ours")) {
		vpEjectCard();
	} else {
		if (VirtuPOS && !VirtuPOS.Offline && VirtuPOS.AID && VirtuPOS.ApplicationLabel && VirtuPOS.TVR) {
			Framework.Variables("cardChipInfo")	= {
				"AID" : VirtuPOS.AID,
				"APP" : VirtuPOS.ApplicationLabel,
				"TVR" : VirtuPOS.TVR
			};
		}
		_['commit']();
			
	}
}

function onCardRejected(reason) {
	if (_.CardCaptured) return; // Карточка уже захвачена, второй onCardRejected не ловим
	
	_.cardRejectReason = reason;
	_.readerEnabled = false;	
	_.fireEvent("actionsChanged");
	_.fireEvent("cardReject");

	(function(){
		var captureTimeoutCount = getParameter("card.captureTimeout", 30);
		var counterInterval = setInterval(
				function () {
					if (captureTimeoutCount <= 0) {
						_['capture']();
						clearInterval(counterInterval);
						_.CardCaptured = true;
					}
					else
					_.fireEvent("captureTick", captureTimeoutCount);
					captureTimeoutCount = captureTimeoutCount-1;
				}
				, 1000);
		_.fireEvent("captureTick", captureTimeoutCount);
		_.addEventListeners({
			"readerEnabled" : function() { clearInterval(counterInterval); }
		});
	})();
}

function vpEjectCard() {
	try {
		VirtuPOS.EjectCard();
		onCardRejected(10);
		return;
	} catch(e) {
		setTimeout(vpEjectCard, 1000);
	}
}

function onCardCaptured() {
	_.leave("cardCaptured");
}

function onCardPickedUp() {
		enableReader();
		hideAlert();
	_.fireEvent("actionsChanged");
}
function onBusy() {
	_.readerIdle = false;
	_.fireEvent("actionsChanged");
	_.fireEvent("startingApplication", true);
}
function onIdle() {
	_.readerIdle = true;
	_.fireEvent("actionsChanged");
	_.fireEvent("startingApplication", false);		
}
function OnCardInserting() {
	// событие OnCardInserting может приходить ошибочно
	// определять можем по переменной _.readerEnabled
	// выходим, если событие ошибочное
	if (!_.readerEnabled)
		return;
	_.readerIdle = false;
	_.fireEvent("actionsChanged");
	_.fireEvent("startingApplication", true);		
}
function OnCardRemoved() {
	if (!_.readerEnabled) {
		enableReader();
		hideAlert();
	}
	_.readerIdle = true;
	_.fireEvent("actionsChanged");
	_.fireEvent("startingApplication", false);		
}


})();
