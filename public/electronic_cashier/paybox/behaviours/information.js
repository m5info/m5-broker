﻿_.addEventListeners({
	"load" : function() {
		_.registerActions({
			"cancel" : function() {
				if (!PaymentGateway.SelectedCatalogItem.IsRoot)
					PaymentGateway.SelectedCatalogItem.SelectParent();
				/* Проверяем что не один child*/
				if (getParameter("recipients.autoSelectRecipent", false) || inSelectSingleItemCategory(PaymentGateway.SelectedCatalogItem)) {
					while (PaymentGateway.SelectedCatalogItem != null && PaymentGateway.SelectedCatalogItem.ChildItems.Count == 1 && !PaymentGateway.SelectedCatalogItem.IsRoot)
						PaymentGateway.SelectedCatalogItem.SelectParent();
				}
			
				_.leave('cancel');
			}
		});
	}
});

