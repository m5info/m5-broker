﻿(function() {

var AID;
var APP;
var TVR;

try {
	AID = Framework.Variables("cardChipInfo")["AID"];
	APP = Framework.Variables("cardChipInfo")["APP"];
	TVR = Framework.Variables("cardChipInfo")["TVR"];
} catch(e) {
	AID = null;
	APP = null;
	TVR = null;
};
_.addEventListeners({
	// Обработчик load
	"load" : function() {
		_.registerVariables({
			"operationCompleted" : false, // Флаг завершения операции
			"operationSucceeded" : false, // Флаг успешности завершения операции
			"paymentResult" : null,
			"incompletePayment" : false
		});
		
		var currentReceipt = Framework.Variables("paymentResults").length;
		if (currentReceipt > 0) {
			_.paymentResult = Framework.Variables("paymentResults")[currentReceipt];
			Framework.Variables("paymentResults").length = currentReceipt - 1;
			try {
				_.incompletePayment = Framework.Variables("incompletePayments").hasOwnProperty(_.paymentResult.recipient.Name);			
			} catch(e) {
				_.incompletePayment = false;
			}
		}
		if (!_.paymentResult) {
			_.leave('fail');
			return false;
		}
		
		// Действия
		_.registerActions({
			// Действие завершения состояния
			"continue" : new StateAction(
				function() {return _.operationCompleted; }, // Доступно только по окончании выполнения
				function() {
					// Здесь обработка результата и соответсвующий leave
					if (_.operationSucceeded)
						_.leave('success');
					else
						_.leave('fail');
				})
		});
		
		// Событие завершения операции
		_.registerEvents(['operationComplete']);
		// Добавляем обработчик по умолчанию (если view не вернуло явно в обработчике false с намерением попоказывать что-нибудь клиенту и спросить о чем-нибудь)
		_.addEventListeners({"operationComplete" : function() { 
				_['continue']();
			}
		});
		
		Framework.AttachEvent(Printer, "OnPresenterEmptyEvent", onPresenterEmptyEvent);
		if (FiscalPrinter)
			Framework.AttachEvent(FiscalPrinter, "OnPresenterEmptyEvent", onPresenterEmptyEvent);
		if (ReceiptPrinter)
			Framework.AttachEvent(ReceiptPrinter, "OnPresenterEmptyEvent", onPresenterEmptyEvent);
		
	},
	
	"run" : function() {
		// Перед обработкой view устанавливаем результат операции
		// Этот обработчик вызовется до обработчика из view зарегестрированного в load
		_.addEventListeners({
				"operationComplete" : function() {
				 _.operationCompleted = true;
				 _.fireEvent('actionsChanged');
				}
			  });
		
		// определяем, была ли считана карта;
		var maskedCardNr = null;
		var authCode =  null;
		var transactionNumber = null;
		if (!VirtuPOS.Offline) {
			// try { 
				maskedCardNr = Framework.Variables("MaskedCardNr");
			// } catch(e) {}
			authCode =  VirtuPOS.AuthCode;
			transactionNumber = VirtuPOS.TransactionNumber;
		}
		
		_.operationSucceeded = printCoupon(_.paymentResult.errorCode, _.paymentResult.errorText, _.incompletePayment, authCode, maskedCardNr, transactionNumber);
	
		// Отправляем уведомление во View. Если View не обработал событие, то самостоятельно вызываем continue
		_.fireEvent('operationComplete');		
	}
});

// Печать чека
function printCoupon(requestCompleteResult, errorText, incompletePayment, authCode, cardNr, tNr) {
	var Recipient = PaymentGateway.SelectedRecipient;
	if (!Printer.Offline && Printer.DeviceReady) {
		if (!FiscalPrinter.Offline) {
			//печать фискального чека
			printFiscalCoupon(requestCompleteResult, errorText, incompletePayment, authCode ? authCode : "", cardNr ? cardNr : "", tNr ? tNr : "", AID, APP, TVR);
			return true;
		}
		if (!ReceiptPrinter.Offline) {
			//печать Нефискального чека
			printNoFiscalCoupon(requestCompleteResult, errorText, incompletePayment, authCode ? authCode : "", cardNr ? cardNr : "", tNr ? tNr : "", AID, APP, TVR);
			return true;
		}
		return false;
	} else
		return false;
};	

function onPresenterEmptyEvent() {
	_['continue']();
}

})();
