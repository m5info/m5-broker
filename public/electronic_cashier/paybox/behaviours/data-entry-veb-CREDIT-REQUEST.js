﻿(function() {
// Поля, которые мы будем вводить
//var catalogItemFields = [];
// Индекс текущего поля
var currentFieldIndex = -1;
var previousFieldIndex = [];
var fingerScanInProgress = false;

if (PaymentGateway.SelectedRecipient == null) {
	PaymentGateway.SelectedCatalogItem.Fields.ClearAll();
}

_.addEventListeners({
	// Обработчик загрузки текущего состояния
	"load" : function() {
	
		_.registerVariables({
			"validateErrorMessage" : "",
			"getCurrentField" : getCurrentField,
			"checkNext" : checkNext,
			"previousFieldIndex" : previousFieldIndex,
			"validateCatalogItemErrorMessage" : "",
			"scannedImagePath" : null
		});

		// Создаем доступные на странице действия с условиями возможности их применения
		_.registerActions({
			/*"hiddenBox" : new StateAction(
				function() { return getCurrentField() && getCurrentField().value != "" },
				function() { hiddenBox(getCurrentField().value) }
			),*/
			// Предыдущее поле
			"prevField" : new StateAction(
				function() {
					return true;
					return getCurrentField().Type !== 1 && previousFieldIndex.length != 0
				}, 
				selectPreviousField
				),
			// Следующее поле
			"nextField" : new StateAction(
				function() {
					// _["hiddenBox"]();
					return true;
					return  getCurrentField() && getCurrentField().Type !== 1 && (getCurrentField().IsDefined || !getCurrentField().IsRequired) 
				}, 
				selectNextField),
			// Предыдущее поле для типа string
			"prevFieldString" : new StateAction(
				function() {
					return true;
					return getCurrentField().Type === 1 //&& previousFieldIndex.length != 0
				}, 
				selectPreviousField
				),
			// Следующее поле для типа string 
			"nextFieldString" : new StateAction(
				function() {
					// _["hiddenBox"]();
					return true;
					return  getCurrentField() && getCurrentField().Type === 1 && (getCurrentField().IsDefined || !getCurrentField().IsRequired) 
				}, 
				selectNextField),
			// Возврат в каталог
			"selectParent" : new StateAction(
				function() { 
					return !PaymentGateway.SelectedCatalogItem.IsRoot; }, 
				function() {	
					if (previousFieldIndex.length != 0)
						_["prevField"]();
					else 
						selectParent();
					}	
				),
			// Окончание ввода 
			"commit" : new StateAction(
				function() {
					return false;
					var fields = PaymentGateway.SelectedCatalogItem.Fields;
					for (var i = currentFieldIndex, c = fields.Count; i < c; i++) {
						if (fields.Item(i).IsRequired && !fields.Item(i).IsDefined && fields.Item(i).Visibility != 4 && fields.Item(i).IsInput)
							return false;
					}
					
					return true;
				},
				executeCommit
			),
			"appendFieldChar" : appendFieldChar,
			"removeFieldChar" : removeFieldChar,
			"setEnumValue" : function(value) {

				var field = _.getCurrentField();
				if (field.Type != 3)
					return false;
				field.Value = value;
				_.fireEvent('fieldValueChanged');
				return true;
			},
			"info" : new StateAction (
				function() { return true; },
				function() { 
					_.leave("info");
				}	
			),
			"scanFinger" : new StateAction (
				function() { return !FingerScaner.Offline; },
				function() { 
					fingerScanInProgress = true; 
					FingerScaner.Scan(90000); }
			),
			"cancelScanFinger" : new StateAction (
				function() { return !FingerScaner.Offline; },
				function() { fingerScanInProgress = false; FingerScaner.CancelScan(); }
			),
			// Выход в root
			"menu" : new StateAction(
					// Продолжать  можно если
					function ()  {
						return !VirtuPOS.IsCardPresent;
					},
					function () { 
						_.leave("initialize");
			}),
			// Выдать карту		
			"ejectCard" : new StateAction(
				function ()  {
					return VirtuPOS && !VirtuPOS.Offline &&	VirtuPOS.IsCardPresent;
				},
				function() {
					Framework.Variables("EjectReason") = getMessage("ejectReason.cancel");
					_.leave("ejectCard");
			})
		});
		
		currentFieldIndex = getNextFieldIndex();
		if (currentFieldIndex == -1) {
			_['commit']();
		}

		// ======= Подключение обработчиков событий ========
		// подключение к считывателю штрих-кодов
		// Framework.AttachEvent(BarcodeReader, "BarcodeRead", function() {
			// TODO:  было зачем-то еще и && !Recipient.Barcodes.IsRequired
			// if (Recipient && (Recipient.Barcodes != null)) {
				// _.lastError = 'barcodeAlreadyRead';
				// _.fireEvent('showError');			
			// } else {
				// _.lastError = 'barcodeNotSupported';
				// _.fireEvent('showError');				
			// }
		// });

		if (FingerScaner) {
			Framework.AttachEvent(FingerScaner, "OnScanComplete", function(scanedFinger) {
				var field = _.getCurrentField();
				if (field.Type != 7)
					return;
				field.Value = scanedFinger.TemplateString;
				if (isFieldValid(field)) {
					var fullName = scanedFinger.ImageFile;
					fullName = fullName.substring(fullName.lastIndexOf("\\")+1);
					_.scannedImagePath = _.screensRoot + "/images/" + fullName;
					_.fireEvent('fingerScanComplete');
				} else {
					_['nextField']();
				}
			});
		}
			
		
		// Создаем события для взаимодействия с отображением
		_.registerEvents([
			'fieldChanged', 
			'fieldValueChanged', 
			'showError',
			'fingerScanComplete'
		]);

		//_.fireEvent('actionsChanged');
		
		_.addEventListeners({
			"inactivityTimer" : function() {
				selectParent();
				return true;
			},
			"fingerScanComplete" : function() {
				_['nextField']();
			}
		});
	},
	
	"run" : function() {
	
		_.addEventListeners({
	
			"fieldChanged" : function() {
				if (getCurrentField().Type == 7)
					_["scanFinger"]();
				else if (fingerScanInProgress)
					_["cancelScanFinger"]();
				_.fireEvent('actionsChanged');
			},
			
			"fieldValueChanged" : function() {
				_.fireEvent('actionsChanged');
			}
		})
		_.fireEvent('fieldChanged');
	},
	"unload" : function () {
		_['cancelScanFinger']();
	}
});

//
function getNextFieldIndex() {
	var fields = PaymentGateway.SelectedCatalogItem.Fields;
	for (var i = currentFieldIndex+1, c = fields.Count; i < c; i++) {
		if (fields.Item(i).IsInput && fields.Item(i).Visibility != 4)
			return i;
	}
	return currentFieldIndex;
}

// Действия
function selectParent() {
	PaymentGateway.SelectedCatalogItem.SelectParent();
	/* Проверяем что не один child*/
	if (getParameter("recipients.autoSelectRecipent", false) || inSelectSingleItemCategory(PaymentGateway.SelectedCatalogItem)) {
		while (PaymentGateway.SelectedCatalogItem != null && PaymentGateway.SelectedCatalogItem.ChildItems.Count == 1 && !PaymentGateway.SelectedCatalogItem.IsRoot)
			PaymentGateway.SelectedCatalogItem.SelectParent();
	}

	if (!_.leave("recipientCatalog")) {
		_.getLogger().error("Возврат в каталог не выполнен");
		return false;
	}
	return true;
}

// Выбор следующего поля
function selectNextField() {
	if (!getCurrentField().IsDefined || isFieldValid(getCurrentField())) {
	
		//После валидации могут появиться новые поля ввода, поэтому принудительно пытаемся перейти и, в случае неуспеха переходим на завершение операции
		var newIndex = getNextFieldIndex();
		if (newIndex != currentFieldIndex) {
			previousFieldIndex.push(currentFieldIndex);
			currentFieldIndex = newIndex;
			playSoundFunction("click.wav", false);
			_.fireEvent('fieldChanged');
		} else {
			// Нет следующего поля. Нужно что-то делать
			// if (_.mayExecute('commit')) _['commit']()
			executeCommit();
		}
	} else {
		_.lastError = 'validateFieldFailed';
		_.fireEvent('showError');		
		_.fireEvent('fieldChanged');
	}
}
// Выбор предыдущего поля
function selectPreviousField() {
	if (previousFieldIndex.length != 0) {
		currentFieldIndex = previousFieldIndex.pop();
		playSoundFunction("click.wav", false);
		_.fireEvent('fieldChanged');
	} else {
		playSoundFunction("click.wav", false);
		_['selectParent']();
	}	
}


function getCurrentField() {
	try {
		return PaymentGateway.SelectedCatalogItem.Fields.Item(currentFieldIndex);
	} catch (e) {
		return null;
	}
}


function checkNext() {
	fields = PaymentGateway.SelectedCatalogItem.Fields;
	for (var i=currentFieldIndex+1;i<fields.Count;i++) {
		field = fields.Item(i);
		if (field.IsInput) {
			return true;
		}
	}
	return false;
}


// Проверка валидности получателя
function validateCatalogItem() {
	// Проверка данных получателя

	try {
		PaymentGateway.SelectedCatalogItem.Validate();
		return true;
	} catch(e) {
		_.validateCatalogItemErrorMessage = e.message;
		_.lastError = 'validateCatalogItemFailed';
		_.fireEvent('showError');		
		return false;
	}
}


// Проверка валидности поля
function isFieldValid(field) {
				
	try {	
		field.Validate();
		return true;
	}
	catch (e) {
		_.validateErrorMessage = e.message;
		return false;
	}

}


function inUpperCaseTextCategory(field) {
	if ((PaymentGateway.SelectedRecipient && PaymentGateway.SelectedRecipient.InCategory("UpperCaseText"))
		|| PaymentGateway.SelectedCatalogItem.InCategory("UpperCaseText")
		|| (field && field.InCategory("UpperCaseText"))) {
		return true;		
	} else {
		return false;
	}

}


// Добавление символа
function appendFieldChar(ch) {
	var field = getCurrentField();
	ch = inUpperCaseTextCategory(field) ? ch.toUpperCase() : ch;
	
	switch (field.Type) {
		// string
		case 1:
			
			// Ограничение на максимальную длину строки
			if (field.Value.length == getParameter('dataEntry.maximumStringLength', 50)) {
				_.lastError = 'appendCharErrorMessage'
				_.fireEvent('showError')
				return false;
			}
			
			// Убираем HTML символы
			// Возвращается ошибка, но не выставляется сообщение, соответственно алерта не будет
			// Теоретически можно переместить в отображение
			if ((ch == '<') || (ch == '>'))
				return false;
			if (field.AppendChar(ch.charCodeAt(0))) {
				_.fireEvent('fieldValueChanged');
				return true;
			} else {
				_.lastError = 'appendCharErrorMessage'
				_.fireEvent('showError')
				return false;
			}

			// number с маской
			case 2:
				var digit = ch.charCodeAt(0) - 48;
				if ((digit >= 0) && (digit < 10))
					if (field.AppendDigit(digit)) {
						_.fireEvent('fieldValueChanged');
						return true;
					}
				else
					return false;
			// enum
			case 3:
				// Ввод символов в enum
				var digit = ch.charCodeAt(0) - 48;
				if (digit >= 1 && digit <= field.EnumItemCount) {
					_['setEnumValue'](field.GetEnumItemValue(digit-1));
				}
				return false;
			// date
			case 4:
			// decimal
			case 5:
			// ipv4
			case 6:
				if (field.AppendChar(ch.charCodeAt(0))) {
					_.fireEvent('fieldValueChanged');
					return true;
				}
			// fingerscan
			case 7:
				return false;
		}
		return false;
}


// Добавление символа
function removeFieldChar() {
	var field = getCurrentField();
	
	switch (field.Type) {
		// string
		case 1:
				field.CancelLastChar();
				_.fireEvent('fieldValueChanged');
				return true;
			// number с маской
			case 2:
				field.CancelLastDigit();
				_.fireEvent('fieldValueChanged');
				return true;
			// enum
			case 3:
				//TODO: Ввод символов в enum
				return false;
			// date
			case 4:
			// decimal
			case 5:
			// ipv4
			case 6:	
				field.CancelLastChar();
				_.fireEvent('fieldValueChanged');
				return true;
			case 7:
				return false;
		}
		return false;
}

/* проверка: все ли поля заполнены, переход либо на Check либо на Select */
function executeCommit() {

	// ещё раз проверяем корректность заполнения полей, на всякий случай (возможна ситуация, когда поля станут невалидны в последний момент)
	var fields = PaymentGateway.SelectedCatalogItem.Fields;
	previousFieldIndex = [];
	for (var i = 0, c = fields.Count; i < c; i++) {
		try {
			if (fields.Item(i).IsInput && fields.Item(i).Visibility != 4) {
				if (fields.Item(i).IsDefined) {
					fields.Item(i).Validate();
				}
				previousFieldIndex.push(i);
			}
		} catch(e) {
			currentFieldIndex = i;
			_.validateErrorMessage = e.message;
			_.fireEvent('fieldChanged');							
			_.lastError = 'validateFieldFailed';
			_.fireEvent('showError');	
			return;
		}				
	}

	// валидация элемента каталога
	if (!validateCatalogItem()) return;

	if (PaymentGateway.SelectedRecipient) {
		if (!_.leave("recipientComplete")) {
			_.getLogger().error("Окончание ввода данных получателя не обработано сценарием");
		}
		return;
	} else if (PaymentGateway.SelectedCatalogItem) {
		// Select
		if (!_.leave("catalogItemComplete")) {
			_.getLogger().error("Окончание ввода данных получателя не обработано сценарием");
		}
	}
}

})();