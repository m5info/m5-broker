﻿_.addEventListeners({
	// Обработчик загрузки текущего состояния
	"load" : function() {
		// события для взаимодействия с view
		 _.registerEvents([
			"sumChanged"
		 ]);

		// Создаем события для взаимодействия с отображением
		  _.registerVariables({
			"enteredSumm" : 0,
			"enteredSummString" : "0"
			// "payTimer" : null,
			// "payNewTimer" : null,
			// "acceptMode" : "free",
			// "canExecutePartyPayment" : false
		 });
		
		// Создаем доступные на странице действия с условиями возможности их применения
		 _.registerActions({
			"cancel" : function () { _.leave('cancel'); },
			"commit" : new StateAction(
					// Продолжать  можно если
					function ()  {
						return _.enteredSumm >= PaymentGateway.SelectedRecipient.MinLimit;
					},
					function () { 
						_.leave('commit');
					})
		});

		/*
		_.addEventListeners({
			"catalogItemChanged" : function() {
			}
		});
		*/
		// При выборе типа платежа возможны запросы, а возможен переход дальше
		// setTimeout(startSelectPaymentMethod,0);
		
		//
	},
	"inactivityTimer" : function() {
		return true;
	},
	"run" : initializePayment,
	
	"unload" : function() {
		_.getLogger().info('Выгружаем состояние');
	}
});


// Вход в состояние
function initializePayment() {
    try {
		// Иногда случается
		if (PaymentGateway.SelectedRecipient == null) {
			_.getLogger().error('PaymentGateway.SelectedRecipient is null');
			_.leave('catalogLost');
			return;
		}
		//звук на странице
		playSoundFunction("3.mp3", false);
		_.enteredSumm = PaymentGateway.SelectedRecipient.PaymentSum;
		_.enteredSummString = _.enteredSumm.toString();
		PaymentGateway.SelectedRecipient.PaymentSum = _.enteredSumm;
		_.fireEvent('actionsChanged');
		_.fireEvent('sumChanged');
    } catch(e) {
		showAlert(getMessage("acceptCashPayment.unableToAccept") + (getParameter("dataCheck.showCheckError") ? "<br>" + e.message : ""),
			{
				"ok" : _["cancel"]
			},
			{
				27: "ok"
			},
			"ok"
		);
    }
}
function validateEnterdSumm(newSumm) {
	if (PaymentGateway.CustomerBalance < newSumm) {  //пытаемся ввести большую сумму
		showAlert(getMessage("acceptWalletPayment.maxBalanceLimitReached"),
			{ "ok": function() {} },
			{ 13 : "ok", 27 : "ok" }, 
			"ok", 
			7000			
		);
		_.enteredSumm = Math.min(PaymentGateway.CustomerBalance, PaymentGateway.SelectedRecipient.MaxLimit);
		_.enteredSummString = _.enteredSumm.toString();
		PaymentGateway.SelectedRecipient.PaymentSum = _.enteredSumm;
		_.fireEvent('actionsChanged');
		_.fireEvent('sumChanged');
		return false;
	}
	return true;
}

function removeLastChar() {
	if (_.enteredSummString.length > 0) {
		_.enteredSummString = _.enteredSummString.substr(0, _.enteredSummString.length-1);
	}
	if (_.enteredSummString.length == 0) {
		_.enteredSummString = "0";
	}
	_.enteredSumm  = Number(_.enteredSummString);
	PaymentGateway.SelectedRecipient.PaymentSum = _.enteredSumm;
	_.fireEvent('actionsChanged');
	_.fireEvent('sumChanged');
}

function appendChar(ch) {
	var charCode = ch.charCodeAt(0);
	var isValid = ((charCode >= 0x30) && (charCode <= 0x39)) || (ch == '.' || ch == ',');
	if (!isValid)
		return true;

	var dotIdx = _.enteredSummString.indexOf('.');
	// Round entered sum
	if (_.enteredSummString.length - dotIdx > 2) {
		_.enteredSumm = (Math.round(_.enteredSumm*100)/100);
		_.enteredSummString = _.enteredSumm.toString();
	}
	// Append char
	var newSummString = _.enteredSummString;
	if (dotIdx < 0) {
		newSummString += ch;
	} else if (dotIdx >= 0 && ch != '.' && (newSummString.length - dotIdx <= 2)) {
		newSummString += ch;
	} else {
		return true;
	}
	if (!validateEnterdSumm(Number(newSummString))) {
		return false;
	}
	_.enteredSummString = newSummString;
	_.enteredSumm  = Number(_.enteredSummString);
	PaymentGateway.SelectedRecipient.PaymentSum = _.enteredSumm;
	_.fireEvent('actionsChanged');
	_.fireEvent('sumChanged');
	return false;
}



