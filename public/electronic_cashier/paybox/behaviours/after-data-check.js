﻿_.addEventListeners({
	"load" : function() {
		_.registerEvents(["beforeCommit"]);
		
		_.registerVariables({
			"needInput" : null 
		});
		
		try {
			_.needInput = Framework.Variables("checkResult")==1;
		} catch(e) {
			_.needInput = false;
		}
		
		_.registerActions({
			"continue" : function() {
					if (typeof(Printer) != "undefined" && Printer != null && Printer.DeviceReady)
						_.leave(_.needInput ? "commitNeedInput" : "commit");
					else
						_.leave("fail");
				},
		
			"commit" : function() {
					_.leave(_.needInput ? "commitNeedInput" : "commit");
				},
		
			"cancel" : function() { 
					if (!hasInputFieldsOnSelected())
						PaymentGateway.SelectedCatalogItem.SelectParent();
					_.leave("cancel")
				}
		});

		_.addEventListeners( { "beforeCommit" : _["continue"] } );
	},
	
	// Отправляем уведомление во view, если view не обработало событие - выходим по continue
	"run" : function() { _.fireEvent("beforeCommit"); }
	
});
