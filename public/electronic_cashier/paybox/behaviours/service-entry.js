﻿(function() {
_.addEventListeners({
	// Обработчик загрузки текущего состояния
	"load" : function() {

		_.registerVariables({
			"accessCode" : "",
			"accessCodeView" : ""
		});
		_.registerEvents(['codeChanged']);
		_.registerActions({
			"commit" : new StateAction(
				function() { return _.accessCode.length != 0; },
				commitCode),
			"cancel" : function() { _.leave("cancel") },
			"clearCode" : function () {
				Framework.Variables("workerCode") =  null;
				_.accessCode = "";
				_.accessCodeView = "";
				_.fireEvent("actionsChanged");
				_.fireEvent("codeChanged");
			},
			"appendCode" : function (digit) {
				_.accessCode += digit;
				_.accessCodeView += "*";
				_.fireEvent("actionsChanged");
				_.fireEvent("codeChanged");
			}
		});

		Framework.Variables("workerCode") = "";
	},
	"run" : function() {
		if (isCardTypeOf("encashment")) {
			_.accessCode = VirtuPOS.CardNr;
			commitCode();
		}
	},
	"billAcceptorFailure" : function () {
		return false;
	},
	"billDispenserFailure" : function () {
		return false;
	},
	"inactivityTimer" : function() {
		return false;
	}
});


function commitCode() {
	var accessRole = null;
	Framework.Variables("workerName") = "не задан";

	if (accessRole == null && encashmentCode.hasOwnProperty(_.accessCode)) {
		accessRole = "encashment";
		Framework.Variables("workerName") = encashmentCode[_.accessCode];
	}
	if (accessRole == null && encashmentCards.hasOwnProperty(_.accessCode)) {
		accessRole = "encashment";
		Framework.Variables("workerName") = encashmentCards[_.accessCode];
	}
	if (accessRole == null && serviceCode.hasOwnProperty(_.accessCode)) {
		accessRole = "service";
		Framework.Variables("workerName") = serviceCode[_.accessCode];
	}	
	if (accessRole == null && serviceCards.hasOwnProperty(_.accessCode)) {
		accessRole = "service";
		Framework.Variables("workerName") = serviceCards[_.accessCode];
	}	

	var codeNow = _.accessCode;
	_['clearCode']();

	if (accessRole == null) {
		showAlert(getMessage("serviceMenu.authFailed"), 
					{ "ok" : _["cancel"] }, 
					{ 27 : "ok" }, 
					"ok");
		return;
	}
	Framework.Variables("workerCode") = codeNow;

	_.leave(accessRole);
}
})()