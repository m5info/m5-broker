﻿(function() {
	_.addEventListeners({
		"load" : function() {
			// Основное событие для взаимодействия с view
			
			_.registerVariables({
				"errorText" : null,
				"operationCompleted" : false, // Флаг завершения операции
				"operationSucceeded" : false, // Флаг успешности завершения операции			
				"canCommit" : false
			});		
			
			_.registerActions({
				// Действие завершения состояния
				"continue" : new StateAction(
					function() {return _.operationCompleted; }, // Доступно только по окончании выполнения
					function() {
						// Здесь обработка результата и соответсвующий leave
						if (_.operationSucceeded) onSuccessLeave();
						else {
							// TODO этого тут быть не должо
							if (!hasInputFieldsOnSelected())
								PaymentGateway.SelectedCatalogItem.SelectParent();
							_.leave('fail');
						}
					}),
				// Завершение состояние с возможностью перейти в success
				"commit" : new StateAction(
					// Доступно только по окончании выполнения, 
					// возможны дополнительные условия для доступности действия
					function() {return _.operationCompleted && _.canCommit }, 
					function() {
						// Здесь обработка результата и соответсвующий leave
						onSuccessLeave();
					}),
				// Действие по выходу из состояния с отменой. Может отсутствовать вовсе.
				"cancel" : new StateAction(
					function() { return _.operationCompleted; }, // Доступно только по окончании выполнения
					function() { 
						// TODO этого тут быть не должо
						if (!hasInputFieldsOnSelected())
							PaymentGateway.SelectedCatalogItem.SelectParent();
						// Выход по отмене
						_.leave("cancel"); 
					})
			});	
			
			_.registerEvents(['operationComplete']);

			 // Добавляем обработчик по умолчанию (если view не вернуло явно в обработчике false с намерением попоказывать что-нибудь клиенту и спросить о чем-нибудь)
			 _.addEventListeners({"operationComplete" : function() { 
				 _['continue']();
				 }
			 });


			// подключение к 	
			Framework.AttachEvent(PaymentGateway, "OnSelectRequestComplete", onOperationComplete);
		},
		
		"run" : function() {
			_.addEventListeners({
					"operationComplete" : function() {
					 _.operationCompleted = true;
					 _.fireEvent('actionsChanged');
					}
				  });	
		
			try {
				PaymentGateway.Select();
			} catch (e) {
				onOperationComplete(-1, e.description);
			}
		}
	});
	function onSuccessLeave() {
		if (PaymentGateway.SelectedCatalogItem.ChildItems.Count == 1 && !PaymentGateway.SelectedCatalogItem.ChildItems.Item(0).Recipient)
			PaymentGateway.SelectedCatalogItem.ChildItems.Item(0).Select();
		_.leave('success');
	}
	function onOperationComplete(requestCompleteResult, errorText) {

		// Сохраняем результат для обработки в after-data-check
		Framework.Variables("selectResult") = requestCompleteResult;
		// Текс ошибки если есть
		_.errorText = errorText == undefined ? getMessage("select.selectValidationFailed") : errorText;
		// Успешная проверка в случае rcrSucceeded || rcrNeedNextCheck 
		_.operationSucceeded = requestCompleteResult >= 0 && PaymentGateway.SelectedCatalogItem.ChildItems.Count > 0;
		//Определяем возможность проведения операции при неуспехе
		_.canCommit = _.operationSucceeded;

		// Отправляем уведомление локально, затем во View. Если View не обработал событие, то самостоятельно вызываем commit
		_.fireEvent('operationComplete');
return;
	}
})();
