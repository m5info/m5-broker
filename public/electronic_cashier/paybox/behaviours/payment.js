﻿( function() {
var vmcounter;
_.addEventListeners({
	"load" : function() {
		Framework.AttachEvent(PaymentGateway, "OnPaymentRequestComplete", onPaymentComplete);	
		if (VirtuPOS) {
			Framework.AttachEvent(VirtuPOS, "OnCardCaptured", onCardCaptured);
			Framework.AttachEvent(VirtuPOS, "OnRereadIC", doRecipientPayment);
		}
	
		_.registerVariables({
			"operationCompleted" : false, // Флаг завершения операции
			"paymentSucceeded" : false, // Флаг успешности завершения одного платежа
			"activeRecipientIdx" : 0,
			"recipientsArray" : [ ],
			"activeRecipient" : null,
			"deferredPayment" : false,
			"cardCaptured" : false
		});	
	
		_.registerActions({
			// Действие завершения состояния
			"continue" : new StateAction(
				function() { return _.operationCompleted && !_.cardCaptured }, // Доступно только по окончании выполнения
				function() { _.leave('success'); })
		});	
	
		// События для взаимодействия с view
		_.registerEvents(['operationComplete', 'paymentComplete' , 'executingMethod' , 'tryVirtuPOSMethodAgain']);

		// Добавляем обработчик по умолчанию (если view не вернуло явно в обработчике false с намерением попоказывать что-нибудь клиенту и спросить о чем-нибудь)
		_.addEventListeners({
		
		"paymentComplete" : function() {
			
				if (!nextRecipient()) {
					_.fireEvent('operationComplete');
				} else {
					checkFBandPay();
				}
			
			},
			
		"operationComplete" : function() {
					_['continue']();
			}
			
		});		

		// потестировать коллекцию локально
		if (PaymentGateway.RecipientCollection.Count > 0){
			for (i = 0; i < PaymentGateway.RecipientCollection.Count; i++){
				_.recipientsArray.push(PaymentGateway.RecipientCollection.Item(i));
			}
		} else {
			_.recipientsArray.push(PaymentGateway.SelectedRecipient);
		}
		
	},
	"inactivityTimer" : function () {
		return false;
	},
	
	"run" : function() {
	
		// Перед обработкой view устанавливаем результат операции
		// Этот обработчик вызовется до обработчика из view зарегестрированного в load
		_.addEventListeners({
				"operationComplete" : function() {
				 _.operationCompleted = true;
				 _.fireEvent('actionsChanged');
				 if (getParameter("payment.showResults", false) && _.recipientsArray.length != 1) return false;
				}
			  });

	_.activeRecipient = _.recipientsArray[_.activeRecipientIdx];
	PaymentGateway.SelectedRecipient = _.activeRecipient;
	
	checkFBandPay();
			  
	}
		
});

function checkFBandPay() {
	vmcounter = 0;
	if (VirtuPOS && !VirtuPOS.Offline && !VirtuPOS.IsFallback && VirtuPOS.IsCardPresent) {
		VirtuPOS.RereadIC();
	} else {
		doRecipientPayment();
	}	
}

function nextRecipient(){
	_.activeRecipientIdx += 1;
	if  (_.activeRecipientIdx >= _.recipientsArray.length)
		return false;
	_.activeRecipient = _.recipientsArray[_.activeRecipientIdx];
	PaymentGateway.SelectedRecipient = _.activeRecipient;
	return PaymentGateway.SelectedRecipient;

}

function doRecipientPayment() {
	setRRN();
	vmcounter++;
	_.fireEvent('executingMethod');

	try {
		if (getParameter("payment.asyncPayment", true) && !_.activeRecipient.AsyncPaymentDisabled && !_.activeRecipient.InCategory("SyncPayment")) {
				PaymentGateway.PayAsync();
				onPaymentComplete(0, "отложенный платёж");
		}
		else {
			if (PaymentGateway.SelectedRecipient.Fields.GetFieldByID(1010))
				PaymentGateway.SelectedRecipient.Fields.GetFieldByID(1010).Value = VirtuPOS.ResponseCode;
			PaymentGateway.Pay();
		}
	} catch (e) {
			if (VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsBusy && vmcounter <= getParameter("VirtuPOS.attemptsNumber", 5)) {
				_.fireEvent('tryVirtuPOSMethodAgain');
				setTimeout(doRecipientPayment, getParameter("VirtuPOS.retryTime", 2000));
			} else	
				onPaymentComplete(-1, e.description);
	}
	
}

function onPaymentComplete(requestCompleteResult, errorText) {

	_.paymentSucceeded = requestCompleteResult === 0 || 
						requestCompleteResult === 2 || 
						(requestCompleteResult === -3 && PaymentGateway.SelectedRecipient.InCategory("VirtuPOSCheck"));
	var recipientId = _.activeRecipientIdx + 1;
	
	// сохраняем информацию о платеже
	Framework.Variables("paymentResults").length = recipientId;
	
	Framework.Variables("paymentResults")[recipientId] = {
		"recipient" : PaymentGateway.SelectedRecipient,
		"errorCode" : requestCompleteResult,
		"errorText" : errorText
	}
	
	if (PaymentGateway.SelectedRecipient.InCategory("IssueOfCash")) {
		// Наличные должны появиться для выдачи
		PaymentDevice.AccumulatedSum.AddAmount(PaymentGateway.SelectedRecipient.CurrencyCode, PaymentGateway.SelectedRecipient.PaymentSum*100);
		// alert("Withdrawal:"+PaymentDevice.AccumulatedSum(PaymentGateway.SelectedRecipient.CurrencyCode).Value);
	}
	else {
		// Наличные принятые ушли на оплату
		if (_.paymentSucceeded) {
			// alert("Pay:"+PaymentDevice.AccumulatedSum(PaymentGateway.SelectedRecipient.CurrencyCode).Value);
			if (PaymentGateway.SelectedRecipient.PaymentSum != 0)
				PaymentDevice.UseAccumulatedSum(PaymentGateway.SelectedRecipient.CurrencyCode, PaymentGateway.SelectedRecipient.PaymentSum*100);
			if (PaymentGateway.SelectedRecipient.ID == "currencyExchange") {
				var exchangeMoney = Framework.Variables("ExchangeToDispense");
				for (var i = 0; i < exchangeMoney.length; i++) {
					PaymentDevice.AccumulatedSum.AddAmount(exchangeMoney[i].currency, exchangeMoney[i].value*100);
				//	alert("Exchange:"+PaymentDevice.AccumulatedSum(exchangeMoney[i].currency).Value);
				}
			}
		}
	}
		
	if (_.recipientsArray.length == 1)
		_.fireEvent('operationComplete', requestCompleteResult, errorText);
	else if (_.recipientsArray.length > 1)
			_.fireEvent('paymentComplete');
	
}

function onCardCaptured() {
	_.cardCaptured = true;
	Framework.Variables("MaskedCardNr") = VirtuPOS.MaskedCardNr;
	_.leave("fail-card-captured");
}

function setRRN() {
	//заполнение полей RRN и PayTool
	if (Framework.Variables("PaymentMethod") == "acceptCardPayment") {	
		PaymentGateway.SelectedRecipient.RRN = VirtuPOS.RRN;
		PaymentGateway.SelectedRecipient.PaymentMethodID  = "Card";
		PaymentGateway.SelectedRecipient.IsOurCard = true;
	} else {
		PaymentGateway.SelectedRecipient.RRN = "";
		PaymentGateway.SelectedRecipient.PaymentMethodID  = "Cash";
		PaymentGateway.SelectedRecipient.IsOurCard = false;	
	}	
}

} )();
