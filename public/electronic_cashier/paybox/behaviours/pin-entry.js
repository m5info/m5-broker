﻿_.addEventListeners({
	// Обработчик загрузки текущего состояния
	"load" : function() {
		if (!VirtuPOS || VirtuPOS.Offline) {
			_.leave("fail");
			return false;
		}
	
		/* События VirtuPOS */
		Framework.AttachEvent(VirtuPOS, "OnPINCancelled", onPINCancelled);
		Framework.AttachEvent(VirtuPOS, "OnIdle", onIdle);
		Framework.AttachEvent(VirtuPOS, "OnPINBlockReady", onPINBlockReady);
		Framework.AttachEvent(VirtuPOS, "OnPINInputStarted", onPINInputStarted);
		
		// Устанавливаем сообщение по умолчанию		
		Framework.Variables("EjectReason") = getMessage("pinEntry.pinCancelled");
		
		_.registerVariables({
			"pinEntered" : false,
			"pinInputStarted" : false,
			"enteredPINLength" : 0
		});
		
		// Создаем доступные на странице действия с условиями возможности их применения
		_.registerActions({
			// Окончание ввода 
			"commit" : new StateAction(
				function() { return _.pinEntered },
				function() {
					_.leave("commit");
				}),
			// Отмена ввода 
			"cancel" : function() {
					VirtuPOS.CancelPIN();
					setTimeout("_.leave('cancel');", 5000);	
				}
		});
		// Создаем события для взаимодействия с отображением
		_.registerEvents(['pinInputStarted', 'pinChanged']);

	
		_.addEventListeners({
			"char" : function(ch, isPin) {
				if (isPin) onPinKeyPressed(ch);
				return false;
			},
			"key" : function(key, isPin) {
				if (isPin || key == 27) onPinKeyPressed(key);
				return false;
			}
		});
	},
	"run" : function() {
		// Начинаем ввод пина
		VirtuPOS.StartPINInput();
		setTimeout("onPINPadFail()", 5000);
	},
	"inactivityTimer" : function() {
		_["cancel"]();
		return true;
	}
});

function onPINPadFail() {
	if (!_.pinInputStarted) {
		Framework.Variables("EjectReason") = getMessage("pinEntry.pinPadFail");
		_["cancel"]();
	}	
}

function onPINInputStarted() {
	_.pinInputStarted = true;
	_.fireEvent('pinInputStarted');
}

function onPINCancelled() {
	_.leave("cancel");
}

function onIdle() {

	if (_.pinEntered) {
		_.leave("commit");
	}
}

function onPINBlockReady() {
	_.pinEntered = true;

}

function onPinKeyPressed(ch) {
	switch(ch) {
		// backspace
		case 8:
			if (_.enteredPINLength > 0) {
				_.enteredPINLength --;
			}
			break;
		//esc			
		case 27:
			_["cancel"]();
			break;
		default:
			_.enteredPINLength++;
	}
	_.resetInactivityTimer();
	_.fireEvent("pinChanged");
}

