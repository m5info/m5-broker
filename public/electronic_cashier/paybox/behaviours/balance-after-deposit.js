﻿(function() {
	var vmcounter = 0;
	_.addEventListeners({
		"load" : function() {

			if (!VirtuPOS || VirtuPOS.Offline) {
				_.leave("fail");
				return false;
			}
			_.registerEvents(["operationComplete" , "executingVirtuPOSMethod" , "tryVirtuPOSMethodAgain"]);

			_.registerVariables({
				"operationSucceeded" : false,
				"operationCompleted" : false,
				"availableBalance" : null
			});
			
			_.registerActions({
				"continue" : new StateAction(
					function() { return _.operationCompleted; },
					function() {
						if (_.operationSucceeded)
							_.leave("success");
						else
							_.leave("fail");
					})
			});

			Framework.AttachEvent(VirtuPOS, "OnBalanceInquiryComplete", onOperationComplete);
			Framework.AttachEvent(VirtuPOS, "OnCardCaptured", onCardCaptured);
			Framework.AttachEvent(VirtuPOS, "OnRereadIC", doQueryBalance);

			_.addEventListeners( { "operationComplete" : _["continue"] } );
		},
		
		"inactivityTimer" : function() {
			if (!_.operationCompleted) return false;
			_['continue']();
		},
		
		// Отправляем уведомление во view, если view не обработало событие - выходим по continue
		"run" : function() { 
			_.addEventListeners( { 
				"operationComplete" : function () { _.operationCompleted = true; }  
			});
			
			if (VirtuPOS && !VirtuPOS.Offline){
				if (isCardTypeOf("ours")){
					if (!VirtuPOS.IsFallback && VirtuPOS.IsCardPresent) {
						VirtuPOS.RereadIC();
					} else {
						doQueryBalance();
					}
				}
			}
		}
	});
	function onOperationComplete(rcr) {
		_.operationSucceeded = (rcr == true) && (VirtuPOS.ResponseCode == 0);
		
		if (_.operationSucceeded){
			_.availableBalance = getCurrency().format(VirtuPOS.AvailableBalance);
			Framework.Variables("availableBalance") = _.availableBalance;
		} else {
			Framework.Variables("availableBalance") = null;
		}
		_.fireEvent('operationComplete', VirtuPOS.ResponseCode);
	}

	function onCardCaptured() {
		_.leave("fail-card-captured");
	}
	
	function doQueryBalance() {
	
		vmcounter++;
		_.fireEvent('executingVirtuPOSMethod');
		try {
			VirtuPOS.QueryBalance();
		} catch(e) {
			if (VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsBusy && vmcounter <= getParameter("VirtuPOS.attemptsNumber", 5)) {
				_.fireEvent('tryVirtuPOSMethodAgain');
				setTimeout(doQueryBalance, getParameter("VirtuPOS.retryTime", 2000));
			} else
				onOperationComplete(false)
		}
		
	}
	
})()