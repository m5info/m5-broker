﻿(function() {
PaymentGateway.RecipientCollection.Clear();	
var CardCaptured = false;
var cardWaiting = getParameter("paykiosk.cardWaiting", false);
var pageReload = false;

_.addEventListeners({
	// Обработчик загрузки текущего состояния
	"load" : function() {
		//вытаскиваем карты если мы в root и не ждем карты
		if (!cardWaiting)	
			operationEjectCard();
			
		// Если пришли с получателя, то делаем selectParent
		if (PaymentGateway != null && PaymentGateway.SelectedRecipient != null && PaymentGateway.SelectedCatalogItem != null && !PaymentGateway.SelectedCatalogItem.IsRoot) {
			PaymentGateway.SelectedCatalogItem.SelectParent();
			// pageReload = _.updateExtension();
		}

		// if (PaymentGateway != null && PaymentGateway.SelectedCatalogItem != null) {
			// pageReload = _.updateExtension();
		// }
		// if (pageReload)
			// return false;
		
		// Создаем события для взаимодействия с отображением
		_.registerEvents([
			'catalogItemChanged', 
			'unableToPay' , 
			'showDescriptionOnSelected', 
			'catalogItemAskSelect',
			'billAcceptorFailureAlert',
			'cardReject',
			'captureTick',
			'readerEnabled',
			'startingApplication',
			'cardPositionChanged'
		]);
		
		_.registerVariables({
			"currentPageNr" : 0,
			"lastPageNr" : 0,
			"readerEnabled" : false,
			"cardRejectReason" : 0,
			"readerIdle" : true,
			"cardPresent" : VirtuPOS.IsCardPresent
		});		
				
		/* События VirtuPOS */
		Framework.AttachEvent(VirtuPOS, "OnCardRead", onCardRead);
		Framework.AttachEvent(VirtuPOS, "OnCardPickedUp", onCardPickedUp);
		Framework.AttachEvent(VirtuPOS, "OnCardRejected", onCardRejected);				
		Framework.AttachEvent(VirtuPOS, "OnBusy", onBusy);
		Framework.AttachEvent(VirtuPOS, "OnIdle", onIdle);
		Framework.AttachEvent(VirtuPOS, "OnCardCaptured", onCardCaptured);		
		Framework.AttachEvent(VirtuPOS, "OnCardInserting", onCardInserting);		
		Framework.AttachEvent(VirtuPOS, "OnCardRemoved", onCardRemoved);
				
		// Создаем доступные на странице действия с условиями возможности их применения
		_.registerActions({
			"selectItem" : selectItem,
			"processSelectedItem" : processSelectedItem,
			"selectParent" : new StateAction(
				function() { return !PaymentGateway.SelectedCatalogItem.IsRoot; }, 
				selectParent),
			"information" : function() {
				_.leave("information");
			},
			"changeLocale" : function(locale) {
				if (setActiveLocale(locale))
					PaymentGateway.ActiveLocale = locale;
				_.reload();
			},
			// Выход в root
			"menu" : new StateAction(
				// Продолжать  можно если
				function ()  {
					 return !PaymentGateway.SelectedCatalogItem.IsRoot && !VirtuPOS.IsCardPresent;
				},
				function () { 
					_.leave("ejectCard");
			}),
			// Выдать карту		
			"ejectCard" : new StateAction(
				function ()  {
					return (VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsCardPresent);
				},
				function() {
					Framework.Variables("EjectReason") = getMessage("ejectReason.cancel");
					_.leave("ejectCard");
			}),
			// Выдать карту		
			"find" : 
				function() {
					_.leave("find");
				}
		});
		
		if (PaymentGateway.SelectedCatalogItem == null) {
			if (!_.leave("catalogLost"))
				_.getLogger().error("Потеря каталога не обработана сценарием");
			return;
		}
		
		if (PaymentGateway.SelectedCatalogItem.ChildItems && PaymentGateway.SelectedCatalogItem.ChildItems.Count == 1 &&
			(getParameter("recipients.autoSelectRecipent", false) || inSelectSingleItemCategory(PaymentGateway.SelectedCatalogItem.ChildItems.Item(0)))) {
			if (PaymentGateway.SelectedCatalogItem.ChildItems.Item(0).Recipient)
				PaymentGateway.SelectRecipient(PaymentGateway.SelectedCatalogItem.ChildItems.Item(0).ID);
			else
				PaymentGateway.SelectedCatalogItem.ChildItems.Item(0).Select();
			processSelectedItem(false);
		}
	},
	
	"inactivityTimer" : function() {
		if (!PaymentGateway.SelectedCatalogItem) {
			_.leave("catalogLost");
		}
		else if (!PaymentGateway.SelectedCatalogItem.IsRoot) {
			PaymentGateway.SelectedCatalogItem.SelectRoot();
			_.fireEvent('catalogItemChanged');
			//_.leave("groupSelected");
		}
		else {
			_.leave("groupSelected");
		}
		return false;
	},
	"run" : function () {
		if (cardWaiting) {
			if (Framework.Variables("cardWrite")) {
				try {
					VirtuPOS.Clear();
				} catch(e) {};
				Framework.Variables("cardWrite") = false;
			}

			if (VirtuPOS.IsCardPresent && VirtuPOS.Track2Data.length == 0) {
				Framework.Variables("EjectReason") = "";
				_.leave("ejectCard");
				return false;
			}	
			
			if (!VirtuPOS || VirtuPOS.Offline)
				_.leave("cancel");
				
			if (PaymentGateway.SelectedCatalogItem.IsRoot)		
				enableReader();
		}		
	},
	"unload" : function () {
		if (cardWaiting)
			disableReader();
	}
});

function processSelectedItem(goUpLevel) {
		
	if (PaymentGateway.SelectedCatalogItem == null) {
		if (!_.leave("catalogLost"))
			_.getLogger().error("Потеря каталога не обработана сценарием");
		return;
	}	

	if (cardWaiting && !PaymentGateway.SelectedCatalogItem.IsRoot)
		disableReader();

	var appCurrent = getSelectedApplication();
	
	if (PaymentGateway.SelectedCatalogItem.InCategory("CardReading") ||
		(PaymentGateway.SelectedCatalogItem.Recipient &&
		PaymentGateway.SelectedRecipient.InCategory("CardReading"))) {
		_.leave("cardReading");
		return false;
	}
	
	if (PaymentGateway.SelectedCatalogItem == null) {
		if (!_.leave("catalogLost"))
			_.getLogger().error("Потеря каталога не обработана сценарием");
		return;
	}
	
	if (goUpLevel) {
		if (!_.leave("parentSelected")) {
			_.getLogger().debug("Выбор родительского элемента не обработан сценарием");
			return false;
		}
		return appCurrent == getSelectedApplication();
	}
	
	if (PaymentGateway.SelectedRecipient) {
		if (!_.leave("recipientSelected")) {
			_.getLogger().error("Выбор получателя не обработан сценарием");
			return false;
		}
		return appCurrent == getSelectedApplication();
	}
	
	if (PaymentGateway.SelectedCatalogItem.IsLink) {
		if (!_.leave("linkSelected")); {
			return false;
		}
		return appCurrent == getSelectedApplication();
	}
	
	if (PaymentGateway.SelectedCatalogItem.SelectDefined) {
		if (!PaymentGateway.SelectedCatalogItem.SelectRequired) {
			_.fireEvent('catalogItemAskSelect');
		}
	}
	
	if (!_.leave("groupSelected")) {
		_.getLogger().debug("Выбор группы не обработан сценарием, продолжаем отображение в текущей реализации состояния");
		return false;
	}
	// Если нет элементов внутри папки
	if (!PaymentGateway.SelectedCatalogItem.Recipient && !PaymentGateway.SelectedCatalogItem.SelectDefined && !PaymentGateway.SelectedCatalogItem.IsLink && PaymentGateway.SelectedCatalogItem.ChildItems.Count == 0) {
		PaymentGateway.SelectedCatalogItem.SelectParent();
		return;
	}
	
	return appCurrent == getSelectedApplication();
}

function selectParent() {
	//hideAlert();
	
	if (PaymentGateway.SelectedCatalogItem == null) {
		_.leave("catalogLost");
		_.getLogger().error("Потеря каталога не обработана сценарием");
		return;
	}
	
	/* кажется это что-то ненужное в основном сценарии */
	// var currentCatalogItemCardRequired = false;
	// if (inCardRequiredCategory())
		// currentCatalogItemCardRequired = true;
	
	// Наверх
	PaymentGateway.SelectedCatalogItem.SelectParent();
	pageReload = _.updateExtension();
	/* Проверяем что не один child*/
	if (getParameter("recipients.autoSelectRecipent", false) || inSelectSingleItemCategory(PaymentGateway.SelectedCatalogItem.ChildItems.Item(0))) {
		while (PaymentGateway.SelectedCatalogItem != null && PaymentGateway.SelectedCatalogItem.ChildItems.Count == 1 && !PaymentGateway.SelectedCatalogItem.IsRoot && PaymentGateway.SelectedCatalogItem.IsXMLRoot) {
			PaymentGateway.SelectedCatalogItem.SelectParent();
			pageReload = _.updateExtension();
		}
	}
	
	/* кажется это что-то ненужное в основном сценарии */
	// if (!inCardRequiredCategory() && currentCatalogItemCardRequired && VirtuPOS.IsCardPresent)
		// _.leave("ejectCard");
	
	if (processSelectedItem(true) && !pageReload) {
		_.fireEvent('actionsChanged');
		_.fireEvent('catalogItemChanged');
	}
	
	if (PaymentGateway.SelectedCatalogItem.IsRoot) {	
		if (cardWaiting) {
			enableReader();
			return;	
		}	

		if (VirtuPOS.IsCardPresent)
			_.leave('ejectCard');
	}		
}

function isRecieptRequired() {
	return (PaymentGateway.SelectedRecipient && PaymentGateway.SelectedRecipient.InCategory("ReceiptRequired"))
			|| PaymentGateway.SelectedCatalogItem.InCategory("ReceiptRequired");
}

/* кажется это что-то ненужное в основном сценарии */
// function inCardRequiredCategory() {
	// return (PaymentGateway.SelectedCatalogItem && PaymentGateway.SelectedCatalogItem.InCategory("CardRequired"));
// }

function isDescriptionDefined() {
	return PaymentGateway.SelectedCatalogItem.Description
			&& (PaymentGateway.SelectedCatalogItem.Description != "")
			&& (PaymentGateway.SelectedCatalogItem.Name != PaymentGateway.SelectedCatalogItem.Description);
}

function selectItem(itemIdx) {
	//hideAlert();
	
	if (PaymentGateway.SelectedCatalogItem == null) {
		if (!_.leave("catalogLost"))
			_.getLogger().error("Потеря каталога не обработана сценарием");
		return;
	}
	
	PaymentGateway.SelectedCatalogItem.ChildItems.Item(itemIdx).Select();
	// Если у нас нет работающего принтера, а печатать надо, то выходим
	if (!Printer.DeviceReady && isRecieptRequired()) {
		// alert(0);
		_.fireEvent('unableToPay');
		PaymentGateway.SelectedCatalogItem.SelectParent();
		// pageReload = _.updateExtension();
		return;
	}

	try {
		PaymentGateway.SelectedCatalogItem.Fields.GetFieldByID(1001).Value = VirtuPOS.Track2Data.substring(0, VirtuPOS.Track2Data.indexOf("="));
		// PaymentGateway.SelectedCatalogItem.Fields.GetFieldByID(1001).Value = "4438860495017585";
		// PaymentGateway.SelectedCatalogItem.Fields.GetFieldByID(1001).Value = "4438861132311423";
		// alert("PAN: " + PaymentGateway.SelectedCatalogItem.Fields.GetFieldByID(1001).Value)
	} catch(e) {}
	
	// if (!PaymentDevice.DeviceReady) {
	if (!PaymentDevice.IsAcceptReady && !PaymentGateway.SelectedCatalogItem.InCategory("WorkWithoutBillAcceptor")) {
		var conditions = (!PaymentGateway.SelectedCatalogItem.IsRoot &&
						!PaymentGateway.SelectedCatalogItem.InCategory("BillAcceptorDisabled") &&
						!PaymentGateway.SelectedCatalogItem.Recipient);								
		if (conditions || (!conditions && !getParameter("paykiosk.workWithoutBillAcceptor", false))) {
			_.fireEvent('billAcceptorFailureAlert');
			PaymentGateway.SelectedCatalogItem.SelectParent();			
			// pageReload = _.updateExtension();
			return;
		}
	}

	// Если у нас есть описание и его надо показывать, то показываем и только при положительном ответе продолжаем дальше
	if (getParameter('recipients.showDescriptionOnSelected', false) && 
		isDescriptionDefined() &&
		!PaymentGateway.SelectedCatalogItem.InCategory("PeerToPeer") &&
		!PaymentGateway.SelectedCatalogItem.InCategory("MiniStatementQuery") &&
		!PaymentGateway.SelectedCatalogItem.InCategory("BalanceQuery")) {
		_.fireEvent('showDescriptionOnSelected');
		return;
	}
	
	if (processSelectedItem()) {
		_.fireEvent('actionsChanged');
		_.fireEvent('catalogItemChanged');
	}
}

//вытаскиваем карты если мы в root
function operationEjectCard(){
	if (PaymentGateway.SelectedCatalogItem.IsRoot && 
		VirtuPOS && !VirtuPOS.Offline &&
		VirtuPOS.IsCardPresent){
		_.leave("ejectCard");
	}
}

function enableReader() {
	if (!_.readerEnabled) {
		try {
			setTimeout( function() { VirtuPOS.EnableReader(); }, 601);
			_.readerEnabled = true;
		} catch(e) {};
		_.fireEvent("readerEnabled");
		_.fireEvent("actionsChanged");
	}
}
function disableReader() {
	if (!VirtuPOS.IsCardPresent && _.readerEnabled) {
		try {
			VirtuPOS.DisableReader();
			_.readerEnabled = false;
		} catch(e) {};		
		_.fireEvent("actionsChanged");
	}	
}

function onCardRead() {
//	ViewButtons.update();
	Framework.Variables("MaskedCardNr") = VirtuPOS.MaskedCardNr;
	_.cardPresent = VirtuPOS.IsCardPresent;
	_.readerIdle = false;
	_.fireEvent("actionsChanged");
	_.fireEvent("cardPositionChanged");
	if (isCardTypeOf("encashment"))
		_.leave('encashment');
	else if (!isCardTypeOf("ours")) {
		Framework.Variables("EjectReason") = "Карта не обслуживается";
		_.leave("ejectCard");
	} else {
		if (VirtuPOS && !VirtuPOS.Offline && VirtuPOS.AID && VirtuPOS.ApplicationLabel && VirtuPOS.TVR) {
			Framework.Variables("cardChipInfo")	= {
				"AID" : VirtuPOS.AID,
				"APP" : VirtuPOS.ApplicationLabel,
				"TVR" : VirtuPOS.TVR
			}
		}	
		_.leave("pinEntry");	
	}
}

function onCardRejected(reason) {
	_.cardPresent = VirtuPOS.IsCardPresent;
	if (CardCaptured) return; // Карточка уже захвачена, второй onCardRejected не ловим
	_.cardRejectReason = reason;
	_.readerEnabled = false;	
	_.fireEvent("actionsChanged");
	_.fireEvent("cardPositionChanged");
	_.fireEvent("cardReject");

	(function(){
		var captureTimeoutCount = getParameter("card.captureTimeout", 30);
		var counterInterval = setInterval(
				function () {
					_.fireEvent("cardReject");
					if (captureTimeoutCount <= 0) {
						try {
							VirtuPOS.CaptureCard();
						} catch(e) {};
						clearInterval(counterInterval);
						CardCaptured = true;
					}
					else
						_.fireEvent("captureTick", captureTimeoutCount);
					captureTimeoutCount = captureTimeoutCount-1;
				}
				, 1000);
		_.fireEvent("captureTick", captureTimeoutCount);
		_.addEventListeners({
			"readerEnabled" : function() { clearInterval(counterInterval); }
		});
	})();
}

function vpEjectCard(reason) {
	try {
		if (VirtuPOS.IsCardPresent) {
			VirtuPOS.EjectCard();
			onCardRejected(reason);
		}	
	} catch(e) {
		setTimeout(function() { vpEjectCard(reason); } , 1000);
	}
}

function onCardCaptured() {
	_.leave("cardCaptured");
}

function onCardPickedUp() {
	_.readerEnabled = false;
	if (cardWaiting && PaymentGateway.SelectedCatalogItem.IsRoot)
		enableReader();
	_.cardPresent = VirtuPOS.IsCardPresent;
	_.fireEvent("actionsChanged");
	_.fireEvent("cardPositionChanged");
}

function onBusy() {
	_.readerIdle = false;
	_.fireEvent("actionsChanged");
	// _.fireEvent("cardPositionChanged");
	// _.fireEvent("startingApplication", true);
}

function onIdle() {
	_.readerIdle = true;
	_.fireEvent("actionsChanged");
	// _.fireEvent("cardPositionChanged");
	_.fireEvent("startingApplication", false);		
}

function onCardInserting() {
	// событие OnCardInserting может приходить ошибочно
	// определять можем по переменной _.readerEnabled
	// выходим, если событие ошибочное
	if (!_.readerEnabled)
		return;
	_.readerIdle = false;
	_.fireEvent("actionsChanged");
	_.fireEvent("cardPositionChanged");
	_.fireEvent("startingApplication", true);		
}
function onCardRemoved() {
	if (!_.readerEnabled && cardWaiting && PaymentGateway.SelectedCatalogItem.IsRoot) {
		enableReader();
		//hideAlert();
	}
	//hideAlert();
	_.readerIdle = true;
	_.fireEvent("actionsChanged");
	_.fireEvent("cardPositionChanged");
	_.fireEvent("startingApplication", false);		
}

})()