﻿(function(){

var vmcounter;

if (PaymentGateway.SelectedRecipient.ID == "CardWrite") {
	var CardNumber = (PaymentGateway.SelectedRecipient.Fields.getFieldByID(100).Value);
	var date = (PaymentGateway.SelectedRecipient.Fields.getFieldByID(101).Value).toString();
	var doMonth = date.slice(0,2);
	var Year = date.slice(2,4);	
	if (doMonth.charAt(0) == "0")
		var Month = doMonth.slice(1,2);
	else
		var Month = doMonth;
}

_.addEventListeners({
	"load" : function() {

		_.registerVariables({
			"operationCompleted" : false,
			"operationSucceeded" : false,
			"binTableNoneDo" : false,
			"canCommit" : false,
			"checkResult" : null,
			"errorText" : ""
		});

		_.registerActions({
			"continue" : new StateAction(
				function() {return _.operationCompleted; },
				function() {
					// Здесь обработка результата и соответсвующий leave
					if (_.operationSucceeded) _.leave('success');
					else _.leave('fail');
				}),
			"commit" : new StateAction(
				function() { return _.operationCompleted && _.canCommit },
				function() {
					_.leave('success');
				}
			),
			// Действие по выходу из состояния с отменой. Может отсутствовать вовсе.
			"cancel" : new StateAction(
				function() { return (_.operationCompleted  || _.binTableNoneDo) }, // Доступно только по окончании выполнения
				function() { 
					// чистим поля, пришедшие с сервера
					for (var i=0; i<PaymentGateway.SelectedRecipient.Fields.Count; i++) {
						if (PaymentGateway.SelectedRecipient.Fields(i).IsDefined && !PaymentGateway.SelectedRecipient.Fields(i).IsInput) {
							PaymentGateway.SelectedRecipient.Fields(i).Value = "";
						}
					}
					// Выход по отмене
					_.leave("cancel"); 
				})
		});
		
		// Регистрируем обработчик и добавляем обработчик по умолчанию (если view не вернуло явно в обработчике false с намерением попоказывать что-нибудь клиенту и спросить о чем-нибудь)
		_.registerEvents({
			"operationComplete" : _['continue'], 
			"binTableNone" : _['cancel'], 
			"executingVirtuPOSMethod" : null, 
			"executingMethod" : null,
			"tryVirtuPOSMethodAgain" : null
		});
		
		// подключение к PaymentGW
		Framework.AttachEvent(PaymentGateway, "OnCheckNumberRequestComplete", onCheckComplete);
		if (VirtuPOS && !VirtuPOS.Offline) 
			{Framework.AttachEvent(VirtuPOS, "OnRereadIC", doCheck)}
	},
	
	"run" : function() {
		// Перед обработкой view устанавливаем результат операции
		// Этот обработчик вызовется до обработчика из view зарегестрированного в load
		_.addEventListeners({
				"binTableNone" : function () {
				_.binTableNoneDo = true;
				},
				"operationComplete" : function() {
				 _.operationCompleted = true;
				}
		});
		if (VirtuPOS && !VirtuPOS.Offline && !VirtuPOS.IsFallback && VirtuPOS.IsCardPresent) {
			VirtuPOS.RereadIC();
			vmcounter = 0;
		} else {
			doCheck();
		}
	}
});

function doCheck() {
	vmcounter++;
	_.fireEvent('executingMethod');
	try {
		PaymentGateway.CheckNumber();
	} catch(e) {
		if (!VirtuPOS.Offline && VirtuPOS.IsBusy && vmcounter <= getParameter("VirtuPOS.attemptsNumber", 5)) {
			_.fireEvent('tryVirtuPOSMethodAgain');
			setTimeout(doCheck, getParameter("VirtuPOS.retryTime", 2000));
		} else
			onCheckComplete(-1, e.description);
	}	
	
}

function setCard() {

	vmcounter++;
	_.fireEvent('executingVirtuPOSMethod');
	
	if (!VirtuPOS.Offline) {
	
		try {
			VirtuPOS.SetCard(CardNumber, Number(Year), Number(Month));
		} catch(e) {
			if (VirtuPOS.IsBusy && vmcounter <= getParameter("VirtuPOS.attemptsNumber", 5)) {
				_.fireEvent('tryVirtuPOSMethodAgain');
				setTimeout(setCard, getParameter("VirtuPOS.retryTime", 2000));
				return;
			} else
				onCheckComplete(-1)
		}	
		if (!isCardTypeOf("ours")){
			_.fireEvent('binTableNone');
			return;
		}
		
		setCardComplete();
		
	} else
		onCheckComplete(-1);

}

function setCardComplete() {

	Framework.Variables("cardWrite") = true;
	
	if (VirtuPOS && !VirtuPOS.IsFallback && VirtuPOS.IsCardPresent) {
		VirtuPOS.RereadIC();
		vmcounter = 0;
	} else {
		doCheck();
	}	
	
}

/* Завершена операция */
function onCheckComplete(requestCompleteResult, errorText) {
	if (getParameter("dataCheck.showCheckError", false) || PaymentGateway.SelectedRecipient.InCategory("VirtuPOSCheck"))
		_.errorText = errorText;

	//alert("requestCompleteResult "+requestCompleteResult);

	// Сохраняем результат для обработки в after-data-check
	Framework.Variables("checkResult") = requestCompleteResult;
	// Код возврата
	_.checkResult = requestCompleteResult;
	// Успешная проверка в случае rcrSucceeded || rcrNeedNextCheck 
	_.operationSucceeded = requestCompleteResult == 0 || requestCompleteResult == 1;
	//Определяем возможность проведения операции при неуспехе
	if (_.operationSucceeded) {
		_.canCommit = true;
	} else {
				//	-2: rcrFailedTemporary (временная ошибка)
				//	-4: rcrValidationFailed (ошибка валидации)
		_.canCommit = /*!PaymentGateway.SelectedRecipient.CheckRequired ||*/
					(getParameter("dataCheck.allowSkipCheck", false) && // если разрешён платёж при временных ошибках
					(requestCompleteResult == -2 || requestCompleteResult == -4));
	}

	// Отправляем уведомление во View. Если View не обработал событие, то самостоятельно вызываем commit
	_.fireEvent('operationComplete');
}
})()
