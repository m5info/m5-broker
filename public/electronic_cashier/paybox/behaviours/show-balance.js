﻿(function() {
_.addEventListeners({
	"load" : function() {
		_.registerVariables({
			"balance" : "   Ваш баланс: "+ getCurrency().format(VirtuPOS.AvailableBalance)
		});
		
		_.registerActions({
			"continue" : function() {
						_.leave('finish')
					},
			"printBC" : printBC
		});
		
		_.registerEvents(['operationComplete', 'printerSFailure', 'printDone']);
		_.addEventListeners({"operationComplete" : function() { 
				_['continue']();
			}
		});
	
	},
	
	"run" : function() { _.fireEvent("operationComplete"); }
});

function printBC() {

	if (!(Printer && Printer.DeviceReady)) {
		_.fireEvent('printerSFailure');
		return;
	}
	
	printBalanceCoupon(VirtuPOS.AuthCode, VirtuPOS.MaskedCardNr, VirtuPOS.TransactionNumber, _.balance);
	_.fireEvent('printDone');
}

})();