﻿( function() {

_.addEventListeners({
	// Обработчик загрузки текущего состояния
	"load" : function() {
		// события для взаимодействия с view
		_.registerEvents([
			'onBillRejected',
			'onAccumulatedSumChanged',
			'onPaymentComplete',
			'billReturned',
			'billAcceptorError',
			'unableToAccept',
			'depositNotFullPayment',
			'askMoreTime'
		]);

		// Создаем события для взаимодействия с отображением
		 _.registerVariables({
			"rCollection" : PaymentGateway.RecipientCollection, // коллекция получателей для оплаты
			"acceptMode" : null, // режим приема денег
			"deviceBusy" : false, // Устройство занято
			"canCancel" : true,  
			"exit" : false,
			
			// Минимальная сумма платежа. 
			// Расчитывется  
			//	для фиксированной суммы - как требуемая к оплате.
			//  для свободной суммы - как миниальная сумма к оплате
			//  для рекомендуемой суммы - как миниальная сумма к оплате
			"minSum" : 0,  
			"minSumDisplayDeposit" : 0,  
			
			// Максимальная сумма платежа
			// Расчитывется  
			//	для фиксированной суммы - как требуемая к оплате.
			//  для свободной суммы - как максимальная сумма к оплате
			//  для рекомендуемой суммы - как максимальная сумма к оплате
			"maxSum" : 0,
			
			// Сумма накопленная в PaymentDevice+Сумма сдачи в MajorUnit
			"acceptedSum" : 0,
			// Сумма платежа
			"paymentSum" : 0,
			// Сумма к зачислению
			"transferSum" : 0,
			// Сумма коммиссий
			"commissionSum" : 0,
			// Осталось внести для фиксированной оплаты
			"remainSum" : null,
			// Рекомендованная сумма к оплате
			"recommendedSum" : null,
			// Индекс получателя на свободную сумму в коллекции
			"freeSumElementNr" : 0,
			"activeCurrency" : null
		});
		// Создаем доступные на странице действия с условиями возможности их применения
		 _.registerActions({
			"resetPayment" : function () { 
			},
			// Отменить прием платежа
			"cancel" : new StateAction(
						function() {
							if (!_.canCancel || _.deviceBusy) return false;
							if (_.acceptedSum == 0) return true;
							// TODO: здесь должна быть проверка на возможность оставления сдачи или ее выдачи
							if (getParameter("acceptCash.changePossible", true))
								return true;
							return false;
						},
					function () {
						if (_.exit) {
							_.leave('cancel');
							return;
						}	
						calculateCollection();
						// Framework.Variables("changeSum") = _.acceptedSum;
						
						// чистим поля, пришедшие с сервера
						for (var i=0; i<PaymentGateway.SelectedCatalogItem.Fields.Count; i++) {
							if (PaymentGateway.SelectedCatalogItem.Fields(i).IsDefined && !PaymentGateway.SelectedCatalogItem.Fields(i).IsInput) {
								PaymentGateway.SelectedCatalogItem.Fields(i).Value = "";
							}
						}		
						
						// Для оплаты кредитов чтобы попадать в именованный стейт
						if (VirtuPOS.IsCardPresent && getParameter("paykiosk.cardWaiting", false))
							PaymentGateway.SelectedCatalogItem.SelectParent();						

						
						try {
							PaymentDevice.CancelPayment();
						} catch(e) {}	
						calculateCollection();
						_.leave('cancel');
					}),
			// Принять платеж
			"commit" : new StateAction(
					// Продолжать  можно если
					function ()  {
						if (_.deviceBusy) return false;
						if (!PaymentDevice.HasAccumulatedSum) return false;
						if (_.acceptedSum >= _.minSum)
							return true; // Накопленная сумма > минимальной
						else
							return false;

						if (_.acceptMode == "fixed-col") {
							if (_.acceptedSum >= _.minSum)
								return true;
						} else {
							if (_.acceptedSum >= _.minSum &&
								_.acceptedSum <= _.maxSum)
								return true;							
						}
					},
					function () { 
						try { 
							PaymentDevice.CommitPayment(); 
						} catch(e){
							onPaymentComplete();
						}
						
					}),
			"continuePay" : function() {
						_.resetInactivityTimer();
						hideAlert();
					},
			"partyPayment" : new StateAction(
						function() {},
						function () {}
					),
			"payment" : new StateAction(
						function() { return true;},
						function () { _.leave('commit'); }
					),
			// Выход в root
			"menu" : new StateAction(
					// Продолжать  можно если
					function ()  {
						if (VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsCardPresent) return false;
						if (!_.canCancel) return false;
						if (_.acceptedSum == 0) return true;
						// TODO: здесь должна быть проверка на возможность оставления сдачи или ее выдачи
						if (getParameter("acceptCash.changePossible", true))
							return true;
						return false;
					},
					function () { 
						PaymentGateway.SelectedCatalogItem.SelectRoot();
						_.exit = true;
						try {
							PaymentDevice.CancelPayment();
						} catch(e) {
							_.leave('cancel');
						}
					}),
			"ejectCard" : new StateAction(
				function ()  {
					if (!_.canCancel) return false;
					if (!(VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsCardPresent)) return false;
					if (_.acceptedSum == 0) return true;
				},

				function() {
					Framework.Variables("EjectReason") = getMessage("ejectReason.cancel");
					_.leave("ejectCard");
			})		
					
					
		});
		
		//
		Framework.AttachEvent(PaymentDevice, "OnBillRejected", onBillRejected);
		Framework.AttachEvent(PaymentDevice, "OnCashDeviceIdle", onIdle);
		Framework.AttachEvent(PaymentDevice, "OnCashDeviceBusy", onBusy);
		Framework.AttachEvent(PaymentDevice, "OnCashDeviceError", onError);
		Framework.AttachEvent(PaymentDevice, "OnAccumulatedSumChanged", onAccumulatedSumChanged);
		Framework.AttachEvent(PaymentDevice, "OnPaymentComplete", onPaymentComplete);
		Framework.AttachEvent(PaymentDevice, "OnPaymentCancelled", onPaymentCancelled);
		_.addEventListeners({ 
			"billAcceptorError" : function() { 
				if (_.mayExecute('commit'))
					_['commit']()
				else
					_['cancel']()
			}/*,
			"askMoreTime" : function() {
				if (_.mayExecute('commit'))
					_['commit']()
				else
					_['cancel']()
			}*/
		});
		
		// запускаем анализ получателя и коллекции, если функция вернёт false,
		// значит, что-то пошло не так, останавливаем обработку
		// (необходимые действия/события определяются внутри функции)

		if (!analyseRecipients())
			return false;

		calculateCollection();
		
	},
	
	"inactivityTimer" : function() {
		if (_.acceptedSum > 0) {
			try { 
				PaymentDevice.CommitPayment(); 
			} catch(e){
				onPaymentComplete();
			}
			return false;
		}
		return true;
		
	},
	"run" : initializePayment,
	
	"billAcceptorFailure" : function () {
		if (_.acceptedSum == 0) {
			_.canCancel = true;
			_.fireEvent("billAcceptorError");
			return false;
		}
	}
});

// Вход в состояние
function analyseRecipients() {
	// Если нет получателя, предполагается, что есть коллекция
	if (!PaymentGateway.SelectedRecipient) {
		if (_.rCollection.Count == 0) {
			// коллекция пуста и нет выбранного получателя, оплачивать нечего, выходим		
			_.getLogger().error('PaymentGateway.SelectedRecipient is null and PaymentGateway.RecipientCollection.Count equals 0');
			_.leave('catalogLost');
			return false;				
		}
	} else {
		// Если получатель есть, предполагается, что коллекция пуста, иное ошибочно и будет проигнорировано в пользу получателя
		_.rCollection.Clear();
		_.rCollection.Add(PaymentGateway.SelectedRecipient);		
	}
	
	// коллекция точно не пуста
	var hasFixedAmount = false;
	var hasFreeAmount = false;
	var hasFreeRecommended = false;
	for (var i = 0 ; i < _.rCollection.Count; i++) {
		switch (_.rCollection.Item(i).TransferAmountType) {
			case 1: // fixed
				hasFixedAmount = true;
				break;
			case 3: // free-recommended
				hasFreeRecommended = true;
			case 2: // fixed-minsum
			case 0: // free
				if (hasFreeAmount) {
					// В коллекции имеем более одного получателя на нефиксированную сумму
					// На выход
					_.fireEvent('unableToAccept', "incorrectCollection");
					return false;
				}
				hasFreeAmount = true;
				if (hasFreeRecommended)
					_.recommendedSum = _.rCollection.Item(i).PaymentSum;	
				_.freeSumElementNr = i;
				break;
		}					
	}
	
	// Определяемся с режимом работы
	if (hasFixedAmount && !hasFreeAmount) {
		_.acceptMode = "fixed-col"; // Коллекция (1..N) на фиксировнную суммы 
	} else if (hasFixedAmount && hasFreeAmount) {
		_.acceptMode = "fixed-minsum-col";  // Коллекция (1..N) на фиксировнную суммы + 1 на свободную
	} else if (!hasFixedAmount && hasFreeRecommended) {
		_.acceptMode = "free-recommended-one"; // 1 получатель с рекомендованной суммой
	} else {
		_.acceptMode = "free-one";  // 1 получатель со свободной суммой
	}

	// если не вылетели, значит, определились с типом оплаты, приступаем к принятию купюр
	// далее состояние работает только с коллекцией, с количеством элементов >= 1			
	
	// Если в купюроприемнике уже есть валюта, то устанавливаем ее для получателей
	for (var i=0 ; i<_.rCollection.Count; i++) {
		// Получатель
		var rItem = _.rCollection.Item(i);
		
		if (_.activeCurrency == null) {
			// что принимаем
			if (rItem.CurrencyCode && rItem.CurrencyCode.length != 0) {
				// Есть получатель и у него установлена валюта
				_.activeCurrency = rItem.CurrencyCode;
			}
			else {
				// Получатель есть но валюта не указана
				if (rItem.PayLimits.Count != 0) {
					// Берем первую валюту если есть
					_.activeCurrency = rItem.CurrencyCode = rItem.PayLimits(0).CurrencyCode;
				}
			}
		}
		// 
		if (rItem.TransferAmountType == 1) {
			// Получатель на фиксированную сумму
			_.minSum += rItem.PaymentSum; // Минимальная сумма = сумма к оплате
			_.maxSum = Math.max(_.maxSum, rItem.MaxLimit);   // Максимальная сумма TODO:
		} else if (rItem.TransferAmountType == 2) {
			// Получатель с минимальной суммой
			_.minSum += rItem.PaymentSum; // Минимальная сумма = сумма к оплате
			_.maxSum = Math.max(_.maxSum, rItem.MaxLimit);   // Максимальная сумма TODO:
			rItem.PaymentSum = 0; // Обнуляем сумму к оплате
		}
		else {

			
			// Для ВЭБ
			if (rItem.inCategory("DisableMaxLimitCash")) {
				_.minSumDisplayDeposit = Math.max(rItem.MinLimit, getNumber(rItem.Fields.GetFieldByID(130).DisplayValue), getNumber(rItem.Fields.GetFieldByID(150).DisplayValue) + getNumber(rItem.Fields.GetFieldByID(130).DisplayValue));
				_.minSum = Math.max(rItem.MinLimit, getNumber(rItem.Fields.GetFieldByID(130).DisplayValue));
				if (VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsCardPresent)
					_.maxSum = Math.max(rItem.MaxLimit, 1000000, getNumber(rItem.Fields.GetFieldByID(151).DisplayValue));
				else 
					_.maxSum = Math.max(rItem.MaxLimit, getNumber(rItem.Fields.GetFieldByID(151).DisplayValue));
			} else {
				// Свободная сумма или рекомендованная
				_.minSum += rItem.MinLimit + rItem.CommissionSum; // Минимальная сумма+комиссия
				_.maxSum = Math.max(_.maxSum, rItem.MaxLimit);   // Максимальная сумма TODO:
				rItem.PaymentSum = 0; // Обнуляем сумму к оплате		
			
			}
		
		}
	}

	if (!(function() {
		// проверка коллекции на одно-валютность, сравнение с первым эл-том коллекции
		var cur = _.rCollection.Item(0).CurrencyCode;
		for (var i=0 ; i<_.rCollection.Count; i++) {
			if (_.rCollection.Item(i).CurrencyCode != cur)
				return false;
		}
		return true;
	})()) {
		_.fireEvent('unableToAccept', "incorrectCurrency");
		return false;
	}
	_.acceptedSum = PaymentDevice.AccumulatedSum(_.rCollection.Item(0).CurrencyCode).Value/100;

	return true;
}

// Расчет сумм
function calculateCollection() {
	_.commissionSum = 0; // Сумма комиссий
	for (var i=0 ; i<_.rCollection.Count; i++) {
		_.commissionSum += _.rCollection.Item(i).CommissionSum;
	}
	// Сумма к зачислению
	_.transferSum = _.rCollection.TransferSum;
	// Сумма к оплате
	_.paymentSum = _.rCollection.PaymentSum;
	// Осталось внести, актуально только для фиксированных сумм
	_.remainSum = (_.minSum - _.acceptedSum) > 0 ? _.minSum - _.acceptedSum : 0;

	if (inServerResponseCommission() && _.paymentSum > 0 && getNumber(_.rCollection.Item(0).Fields.GetFieldByID(130).DisplayValue) >= 0) {
		// пересчитываем комиссию из поля
		_.rCollection.Item(0).CommissionSum = Math.min(getNumber(_.rCollection.Item(0).Fields.GetFieldByID(130).DisplayValue), _.paymentSum);
		_.commissionSum	= _.rCollection.Item(0).CommissionSum;
		_.transferSum = _.paymentSum - _.commissionSum;
	}	
	
}

// Включение приема наличных
function initializePayment() {
    try {
		var strategy = null;
		strategy = PaymentDevice.CreateStrategy("AcceptCurrecny");
		// Ограничения
		//alert(_.acceptMode+' min:'+_.minSum+" max:"+_.maxSum+" remainSum:"+_.remainSum+" acceptedSum:"+_.acceptedSum);
		//alert(_.rCollection.Item(0).CurrencyCode);
		var payLimt = strategy.PayLimits.AddItem(
			_.rCollection.Item(0).CurrencyCode, 
			_.minSum*100, 
			_.maxSum*100);
		payLimt.Inhibits = getParameter("veb.inhibitedBills");
		strategy.NoChange = false;
		strategy.AutoCommit = true;
		strategy.Timeout = 90000;
		if (_.acceptMode.indexOf('fixed') >= 0)
			strategy.RequiredSum = _.remainSum*100;
		else
			strategy.RequiredSum = _.maxSum*100;
		strategy.CurrencyCode = _.rCollection.Item(0).CurrencyCode;
		
		PaymentDevice.AcceptPayment(strategy);
		// Включаем прием платежа в зависимости от режима
/*		if (_.acceptMode == "fixed-col")
			PaymentDevice.AcceptPayment({
				"requiredSum" : _.rCollection.PaymentSum
			});
		else if (_.acceptMode == "fixed-minsum-col")
			PaymentDevice.AcceptPayment();
		else
			PaymentDevice.AcceptPayment({
				"payLimits" : { 
					"Count" : 1, 
					"Item" : function() { 
						return {
							"CurrencyCode" : "RUB", 
							"MinLimit" : 0,
							"MaxLimit" : 15000
						}}
				},
				"requiredSum" : _.rCollection.PaymentSum
			});
//			PaymentDevice.AcceptPayment(_.rCollection.PaymentSum/*_.rCollection.Item(0).PayLimits);
*/		
		// если оплачиваем одного получателя и выставлен параметр, запрещаем приём купюр меньше минимальной суммы получателя
//		if (_.acceptMode.indexOf("one") != -1 && getParameter("acceptCash.rejectLessThanMin", false))
//			PaymentDevice.MinPaymentSum = _.rCollection.Item(0).MinLimit
		
		//звук на странице
		playSoundFunction("3.mp3", false);
		onAccumulatedSumChanged();
		
		_.fireEvent('actionsChanged');

    } catch(e) {
		_.fireEvent('unableToAccept', e.message);
    }
	
}

// Обработчики PaymentDevice
function onBillRejected(reason, hint) {

	playSoundFunction("error.wav", false);
	_.fireEvent('onBillRejected', reason, hint);
}

function onIdle() {
	_.canCancel = true;
	_.deviceBusy = false;
	_.fireEvent('actionsChanged');
}

function onBusy() {
	_.canCancel = false;
	_.deviceBusy = true;
	_.fireEvent('actionsChanged');
	// распознается купюра, никакие действия невозможны. ждем перехода в ожидание или в ошибку.
	_.resetInactivityTimer();
	_.fireEvent('actionsChanged');
}


function onError() {
	_.fireEvent('actionsChanged');
	_['cancel']();
}

function onAccumulatedSumChanged() {
	//hideAlert();
	_.acceptedSum = PaymentDevice.AccumulatedSum(_.activeCurrency).Value/100;

	 //alert(_.acceptedSum+" "+_.activeCurrency);

	if (_.acceptMode.indexOf("one") != -1) {
		_.rCollection.Item(0).PaymentSum = _.acceptedSum;
	} else if (_.acceptMode == "fixed-minsum-col") {
	
			var exceededSum = _.acceptedSum - _.minSum;
			if (exceededSum >= 0)
				_.rCollection.Item(_.freeSumElementNr).PaymentSum = exceededSum;
	}

	_.resetInactivityTimer();
	calculateCollection();
	_.fireEvent('onAccumulatedSumChanged');
		 
}

function onPaymentComplete() {
	_.fireEvent('actionsChanged');
	
	// ВЭБ Для платежа DEPOSIT при оплате суммы меньше минимальной меняем номер счета на номер связаного счета
	if (PaymentGateway.SelectedRecipient.ID == "DEPOSIT" && _.transferSum < getNumber(PaymentGateway.SelectedRecipient.Fields.GetFieldByID(150).DisplayValue)) {
		_.rCollection.Item(0).Fields.GetFieldByID(101).Value = PaymentGateway.SelectedRecipient.Fields.GetFieldByID(152).DisplayValue;
		_.fireEvent('onPaymentComplete');
		_.fireEvent('depositNotFullPayment');
		return;
	}	

	// пересчитываем сумму сдачи
	if (_.acceptMode.indexOf("one") != -1) {
		_.rCollection.Item(0).PaymentSum = _.acceptedSum;
	} else if (_.acceptMode == "fixed-minsum-col") {
		var exceededSum = _.acceptedSum - _.minSum;
		if (exceededSum > 0)
			_.rCollection.Item(_.freeSumElementNr).PaymentSum = _.rCollection.Item(_.freeSumElementNr).MinLimit + exceededSum;
	} else if (_.acceptMode == "fixed-col") {
		var exceededSum = _.acceptedSum - _.minSum;
	}
	calculateCollection();
	_.fireEvent('onPaymentComplete');
	_.leave('commit');
}

function onPaymentCancelled(moneyReturned) {
	if (_.canCancel && PaymentDevice.AccumulatedSum(_.activeCurrency).Value == 0)
		_['cancel']();
}

function inServerResponseCommission() {
	if ((PaymentGateway.SelectedRecipient && PaymentGateway.SelectedRecipient.InCategory("ServerResponseCommission"))
		|| PaymentGateway.SelectedCatalogItem.InCategory("ServerResponseCommission")) {
		return true;		
	} else {
		return false;
	}
}

function getNumber(n) {
	if (!n.length)
		return 0;
	var re = /\-?\d+\.?\,?/g;
	var nn = n.match(re,"");
	if (nn != null) 
		nn = nn.join("").replace(",", .2.toString().substr(1,1));
	else 
		nn = 0;
	if (nn < 0)
		nn = 0;
	return parseFloat(nn);
}

})();