﻿// код открытия инженерного меню
var initialServiceCode = "753951"


// коды подтверждения обслуживания для инкассаторов
var encashmentCode = {
	"1" : "Иванов В.А.", 
	"3" : "Сатин В.В.",
	"5" : "Майкова Т.С.",
	"7" : "Абрамова М.В."
};

// коды подтверждения обслуживания для сервисных инженеров
var serviceCode = {
	"2" : "Сидоров И.А.", 
	"4" : "Самохвалов. В.П.", 
	"6" : "Медраз В.Ы."
};

// Карты сервисных инженеров
var serviceCards = {
	"1111111111111111" : "Сатин В.В.",
	"2222222222222222"  : "Майкова Т.С.", 
	"3333333333333333"  : "Абрамова М.В."
};

// Карты инкассаторов
var encashmentCards = {
	"4444444444444444"  : "Сатин В.В.",
	"5555555555555555"  : "Майкова Т.С.",
	"6666666666666666" : "Абрамова М.В."
};

// скрытая область для состояния 'data-entry'
if (_.type == "data-entry")
	( function() {
	
	var hiddenBoxProperties = EWM.hiddenBox;
	if (!hiddenBoxProperties)
		hiddenBoxProperties = {
			"invisible" : true,
			// цвет кнопки, если отключена невидимость
			"color" : "red",
			"code" : initialServiceCode,
			"left" : 615,
			"top" : 906,
			"width" : 50,
			"height" : 50
		}

	this.hiddenBox = function(value) {
		var hiddenBox = document.getElementById("hiddenBox");
		if (!hiddenBox) {
			if (value != hiddenBoxProperties.code || !value)
				return;
			hiddenBox = document.createElement('div');
			hiddenBox.style.position = "absolute";
			hiddenBox.style.zIndex = "5000";
			hiddenBox.id = "hiddenBox";
			hiddenBox.style.left = hiddenBoxProperties.left;
			hiddenBox.style.top = hiddenBoxProperties.top;	
			hiddenBox.style.width = hiddenBoxProperties.width;
			hiddenBox.style.height = hiddenBoxProperties.height;
			if (hiddenBoxProperties.invisible)
				hiddenBox.innerHTML = "<table width='100%' height='100%'><tr><td></td></tr></table>";
			else
				hiddenBox.style.backgroundColor = hiddenBoxProperties.color;
			hiddenBox.onclick = function() { _.leave('encashment'); };
			document.body.appendChild(hiddenBox);		
		} else {
			if (value != hiddenBoxProperties.code || !value)
					document.body.removeChild(hiddenBox);
			else
				hiddenBox.onclick();
		}		
	}

	})();