﻿(function () {
this.canDispense = function(amount, currency) {
	if (amount == 0)
		return 0;

	// Для отладки без диспенсера. Выдаем 10 руб. и 1 долар. купюры	
	/*if (currency == "RUB")
		return 10.0*Math.floor(amount/10.0);
	else	
		return Math.floor(amount);*/

	return PaymentDevice.CanDispense(amount*100, currency)/100.0;
	// return 10.0*Math.floor(amount/10.0);
}

// Возвращает код валюты приемника
this.getTargetCurrency = function () {
	return getCurrency(PaymentGateway.SelectedRecipient.Fields.GetFieldByID('102').Value).alphaCode;
}

// Возвращает код валюты источника
this.getOperationCurrency = function() {
	return getCurrency(PaymentGateway.SelectedRecipient.Fields.GetFieldByID('101').Value).alphaCode;
}

// Единицы валюты источника
this.getSourceUnits = function() {
	return new Number(new Number(PaymentGateway.SelectedRecipient.Fields.GetFieldByID('103').Value));
}
// Единицы валюты приемника
this.getTargetUnits = function () {
	return new Number(new Number(PaymentGateway.SelectedRecipient.Fields.GetFieldByID('104').Value));
}

// Возвращает полную суммы результата обмена в валюте приемника
this.getTargetAmount = function (sourceAmount) {
	return (sourceAmount/getSourceUnits())*getTargetUnits();
}

// Рассчитывает сумму валюте источника из суммы в валюбте приемника
this.getSourceAmount = function(targetAmount) {
	return targetAmount/getTargetUnits()*getSourceUnits();
}

this.getSourceAmountLeft = function(amountTarget, amountTargetDispense) {
	return (amountTarget-amountTargetDispense)/getTargetUnits()*getSourceUnits();
}

this.isRoublesPresent = function() {
	var r = PaymentGateway.SelectedRecipient;
	var zz = 
		r.Fields.GetFieldByID('102').Value == 643 || r.Fields.GetFieldByID('102').Value == 'RUB' ||
		r.Fields.GetFieldByID('101').Value == 643 || r.Fields.GetFieldByID('101').Value == 'RUB';
	return zz;
	
}

this.isRoublesTarget = function() {
	var r = PaymentGateway.SelectedRecipient;
	return r.Fields.GetFieldByID('102').Value == 643 || r.Fields.GetFieldByID('102').Value == 'RUB';
}

// Рассчет сумм операции
this.exchangeCalculator = function(sourceAmount) {
	var sourceCurrency = getOperationCurrency();
	var targetCurrency = getTargetCurrency();
	// Полная сумма обмена в валюте приемника
	this.targetAmount = new CurrencyAmount(getTargetAmount(sourceAmount), targetCurrency);
	// Сумма обмена, которую можно будет выдать через диспенсер (поле а из ТЗ)
	this.targetDispenseAmount = new CurrencyAmount(canDispense(this.targetAmount.value, targetCurrency), targetCurrency);
	
	// Суммы сдачи
	if (isRoublesPresent()) {
		// В паре обмена присутсвуют рубли
		if (isRoublesTarget()) {
			// Сумма, которую не сможем выдать в валюте приемнике (RUB)
			this.targetAmountLeft = new CurrencyAmount(this.targetAmount.value - this.targetDispenseAmount.value, targetCurrency);
			// Сумма, которую не сможем выдать в валюте источнике
			this.sourceAmountLeft = new CurrencyAmount(0, targetCurrency);
			// Сумма которую сможем выдать
			this.sourceAmountDispense = new CurrencyAmount(0, targetCurrency);
			// Сумма которая уйдет на сдачу
			this.sourceAmountChange = this.targetAmountLeft;
		} else {
			// Сумма, которую не сможем выдать в валюте приемнике
			this.targetAmountLeft = new CurrencyAmount(this.targetAmount.value - this.targetDispenseAmount.value, targetCurrency);
			// Сумма, которую не сможем выдать в валюте источнике
			this.sourceAmountLeft = new CurrencyAmount(getSourceAmount(this.targetAmountLeft.value), sourceCurrency);
			// Сумма которую сможем выдать
			this.sourceAmountDispense = new CurrencyAmount(canDispense(this.sourceAmountLeft.value, sourceCurrency), sourceCurrency);
			// Сумма которая уйдет на сдачу
			this.sourceAmountChange = new CurrencyAmount(this.sourceAmountLeft.value - this.sourceAmountDispense.value, sourceCurrency);
		}
	} else {
		// Сумма, которую не сможем выдать в валюте приемнике
		this.targetAmountLeft = new CurrencyAmount(this.targetAmount.value - this.targetDispenseAmount.value, targetCurrency);
		// Сумма, которую не сможем выдать в валюте источнике
		this.sourceAmountLeft = new CurrencyAmount(getSourceAmount(this.targetAmountLeft.value), sourceCurrency);
		// Сумма которую сможем выдать
		this.sourceAmountDispense = new CurrencyAmount(canDispense(this.sourceAmountLeft.value, sourceCurrency), sourceCurrency);
		// Сумма которая уйдет на сдачу
		this.sourceAmountChange = new CurrencyAmount(this.sourceAmountLeft.value - this.sourceAmountDispense.value, sourceCurrency);
	}
}


this.maxLimit = function() {
	var r = PaymentGateway.SelectedRecipient;
	var isRoublesSource = r.Fields.GetFieldByID('101').Value == 643 || r.Fields.GetFieldByID('101').Value == 'RUB';
	var isRoublesTarget = r.Fields.GetFieldByID('102').Value == 643 || r.Fields.GetFieldByID('102').Value == 'RUB';
	if (isRoublesSource)
		return 15000;
	if (isRoublesTarget)
		return 15000/r.Fields.GetFieldByID('104').Value;
	return 350;
}

this.minLimit = function() {
	return PaymentGateway.SelectedRecipient.MinLimit;
}

this.commissionSum = function() {
	return PaymentGateway.SelectedRecipient.CommissionSum;
}

this.doPayment = function(sum) {
}

})()