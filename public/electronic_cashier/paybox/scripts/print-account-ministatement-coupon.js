﻿var Recipient = PaymentGateway.SelectedRecipient;

function printNoFiscalCoupon() {
	if (getParameter("printCoupon.printLogo", true)) {
		Printer.Alignment = 2;
		Printer.WriteLogo(1);
	}	
	Printer.Alignment = 0;
	Printer.Writeln("................................................");
	Printer.Writeln("Дата: " + formatDateTime(new Date(), "dd.mm.yyyy    HH:MM:SS"));
	Printer.Writeln("Номер терминала: " + couponProperties.terminalNumber());
	Printer.Writeln("Адрес терминала: " + couponProperties.terminalAddress());
	Printer.Writeln("................................................");
	getFieldString(400); // ФИО
	getFieldString(406); // Номер счета
	getFieldString(402); // Тип счета
	Printer.Writeln("................................................");
	Printer.Bold = 1;
	Printer.Writeln("");
	printLastOperations();
	Printer.Bold = 0;
	Printer.Writeln("");
	isDefined(Recipient.Fields.GetFieldByID(405).Value) ? Printer.Writeln("Доступный остаток по счету: " + HtmlDecode(Recipient.Fields.GetFieldByID(405).Value) + " RUR") : null;
	Printer.Writeln("................................................");
	Printer.Alignment = 2;
	Printer.Writeln(couponProperties.footer());
	Printer.Alignment = 0;
	Printer.Cut(false);
}

function getFieldString(ID, str) {
	if (!str)
		str = "";
	try {
		isDefined(Recipient.Fields.GetFieldByID(ID).Value) ? Printer.Writeln(Recipient.Fields.GetFieldByID(ID).Name + ": " + HtmlDecode(Recipient.Fields.GetFieldByID(ID).Value) + str) : "";
	} catch(e) {}	
}

function printLastOperations() {
	Printer.Writeln(Recipient.Fields.GetFieldByID(499).Name + "\n");

	var oper = [];
	oper = Recipient.Fields.GetFieldByID(499).Value.split("@");

	if (oper.length < 2) {
		Printer.Writeln("Нет данных для печати");
		return;
	}	
	for (var i=0; i<oper.length-1; i++) {
		Printer.Writeln(formatMinistatementStr(oper[i].split("#")));
	}
}


function formatMinistatementStr(arr) {
	var spacesStr = "                                                ";
	return (arr[4] == "KT" ? "зачисление     " : (arr[4] == "DT" ? "списание       " : "неопределенная "))
		+ arr[0] + "   " 
		+ arr[1] + " " + arr[2];
}