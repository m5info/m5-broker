﻿function maxLimit() {
	if (PaymentGateway.SelectedRecipient.MaxLimit != 0)
		return Math.min(PaymentDevice.ChangeSum, PaymentGateway.SelectedRecipient.MaxLimit);
	else
		return PaymentDevice.ChangeSum;
}

function minLimit() {
	return PaymentGateway.SelectedRecipient.MinLimit;
}

function commissionSum() {
	return PaymentGateway.SelectedRecipient.CommissionSum;
}

function doPayment(sum) {
	PaymentDevice.ChangeSum -= sum;
}