﻿var Recipient = PaymentGateway.SelectedRecipient;
		
function printNoFiscalCoupon(requestCompleteResult, errorText, incompletePayment, authCode, cardNr, tNr, AID, APP, TVR) {
	if (getParameter("printCoupon.printLogo", true)) {
		Printer.Alignment = 2;
		Printer.WriteLogo(1);
	}	
	Printer.Alignment = 2;
	Printer.Writeln("ЧЕК");
	Printer.WhiteBlackInversion = 1;
	if (Framework.Variables("PaymentMethod") == "acceptCashPayment") 
		Printer.Writeln("Платеж наличными");
	if (Framework.Variables("PaymentMethod") == "acceptCardPayment") 
		Printer.Writeln("Безналичный платеж");
	Printer.WhiteBlackInversion = 0;
	Printer.Writeln("................................................");
	Printer.Alignment = 0;
	if (Framework.Variables("PaymentMethod") == "acceptCardPayment") {
		Printer.Writeln("Карточная операция №: " + tNr);
		Printer.Writeln("Номер карты: " + cardNr);
		Printer.Writeln("Код авторизации: " + authCode);
	}	

	Printer.Writeln("Номер чека: " + Recipient.CurrentPaymentNumber);
	Printer.Writeln("Номер терминала: " + couponProperties.terminalNumber());
	Printer.Writeln("Адрес терминала: " + couponProperties.terminalAddress());
	Printer.Writeln("Номер транзакции: " + couponProperties.terminalNumber() + Recipient.CurrentPaymentNumber);
	if (PaymentGateway.SelectedContract) {	
		Printer.Writeln("................................................");
		if (PaymentGateway.SelectedContract.ProcessorID.toUpperCase().indexOf("CYBERPLAT")>=0) {
			Printer.Writeln("Платежная система КиберПлат");
			Printer.Writeln("Извещение об оплате услуг");
		}
		(isDefined(PaymentGateway.SelectedContract.FullName) ? Printer.Writeln("Кредитная организация: " + PaymentGateway.SelectedContract.FullName) : "");
		if (isDefined(PaymentGateway.SelectedContract.ContractNr) && (PaymentGateway.SelectedContract.ContractNr != "")) { 
			Printer.Writeln("Договор №"+PaymentGateway.SelectedContract.ContractNr + " от " + PaymentGateway.SelectedContract.ContractDate);
		}
		if (isDefined(PaymentGateway.SelectedContract.Address)) {
			Printer.Writeln("Адрес организации: " + PaymentGateway.SelectedContract.Address);
		}	
		if (isDefined(PaymentGateway.SelectedContract.TaxNR	)) {
			Printer.Write("ИНН: " + PaymentGateway.SelectedContract.TaxNR + " ");
		}
		if (isDefined(PaymentGateway.SelectedContract.BIN)) {
			Printer.Writeln("БИК: " + PaymentGateway.SelectedContract.BIN);
		}
	}
	Printer.Bold = 1;
	Printer.Writeln("................................................");
	if (PaymentGateway.SelectedContract) {
		if (PaymentGateway.SelectedContract.ProcessorID.toUpperCase().indexOf("CYBERPLAT")>=0) {
			Printer.Writeln("Извещение об оплате услуг");
			Printer.Writeln("");
		}
	}
	
	Printer.Writeln("Дата платежа: " + formatDateTime(new Date(), "dd.mm.yyyy HH:MM:SS"));
	Printer.Writeln("Получатель: " + Recipient.Name);

	for (var i=0; i<Recipient.Fields.Count; i++)
		if (Recipient.Fields(i).IsDefined && (Recipient.Fields(i).Visibility < 4))
			Printer.Writeln(Recipient.Fields(i).Name + ": " + HtmlDecode(Recipient.Fields(i).DisplayValue));
			
	Printer.Writeln("................................................");
	Printer.Bold = 0;
	if (Number(Recipient.PaymentSum)>0 || Number(Recipient.ChangeSum)>0) {
		Printer.Writeln("Принято: " + getCurrency().format(Recipient.PaymentSum, 'printer'));
		//if (PaymentDevice.AccumulatedSum(Recipient.CurrencyCode).Value/100 > 0)
			//Printer.Writeln("Сдача: " + getCurrency().format(PaymentDevice.AccumulatedSum(Recipient.CurrencyCode).Value/100, 'printer'));
		Printer.Writeln("Комиссия: " + getCurrency().format(Recipient.CommissionSum, 'printer'));
		Printer.Writeln("Зачислено: " + getCurrency().format(Recipient.TransferSum, 'printer'));
	}

	if (PaymentGateway.SelectedCatalogItem.ID == 8538) {
		Printer.Writeln("................................................");
		Printer.Writeln("Вы помогаете:");
			Printer.Writeln("-Тяжелобольным детям;");
			Printer.Writeln("-Семьям, усыновившим, или взявшим под опеку");
			Printer.Writeln(" детей-сирот;");
			Printer.Writeln("-Детям, с ограниченными физическими ");
			Printer.Writeln(" способностями.");
		Printer.Writeln("................................................");
		Printer.Alignment = 2;
		Printer.Writeln("Сохраняйте чек до поступления платежа");
		Printer.Writeln("................................................");
		Printer.Writeln(couponProperties.footer());
		Printer.Alignment = 0;
		Printer.Cut(false);
	} else {
		Printer.Writeln("................................................");
		Printer.Alignment = 2;
		Printer.Writeln("Сохраняйте чек до поступления платежа");
		Printer.Writeln("................................................");
		Printer.Writeln(couponProperties.footer());
		Printer.Alignment = 0;
		Printer.Cut(false);
	} 
	
}
