﻿var Recipient = PaymentGateway.SelectedRecipient;

function printCaptureCoupon() {
	if (getParameter("printCoupon.printLogo", true)) {
		Printer.Alignment = 2;
		Printer.WriteLogo(1);
	}	
	Printer.Alignment = 0;
	Printer.Writeln("................................................");
	Printer.Writeln("Дата: " + formatDateTime(new Date(), "dd.mm.yyyy    HH:MM:SS"));
	Printer.Writeln("Номер терминала: " + couponProperties.terminalNumber());
	Printer.Writeln("Адрес терминала: " + couponProperties.terminalAddress());
	Printer.Writeln("................................................");
	Printer.Bold = 1;
	Printer.Writeln("Ваша карта изъята, обратитесь по телефону в отделение банка");
	Printer.Bold = 0;
	Printer.Writeln("Номер изъятой карты: " + Framework.Variables("MaskedCardNr"));
	Printer.Writeln("................................................");
	Printer.Alignment = 2;
	Printer.Writeln(couponProperties.footer());
	Printer.Alignment = 0;
	Printer.Cut(false);
}
