﻿var Recipient = PaymentGateway.SelectedRecipient;

function printNoFiscalCoupon() {
	if (getParameter("printCoupon.printLogo", true)) {
		Printer.Alignment = 2;
		Printer.WriteLogo(1);
	}	
	Printer.Alignment = 0;
	Printer.Writeln("................................................");
	Printer.Writeln("Дата: " + formatDateTime(new Date(), "dd.mm.yyyy    HH:MM:SS"));
	Printer.Writeln("Номер терминала: " + couponProperties.terminalNumber());
	Printer.Writeln("Адрес терминала: " + couponProperties.terminalAddress());
	Printer.Writeln("................................................");
	getFieldString(300); // ФИО
	getFieldString(301); // Номер карты
	getFieldString(316); // Номер счета	
	getFieldString(317); // Вид карты
	Printer.Writeln("................................................");
	Printer.Bold = 1;
	Printer.Writeln("Текущий баланс по карте");
	Printer.Writeln("");
	getFieldString(318, false, " RUR");
	Printer.Bold = 0;
	getFieldString(319, true, " RUR");
	Printer.Writeln("................................................");
	Printer.Alignment = 2;
	Printer.Writeln(couponProperties.footer());
	Printer.Alignment = 0;
	Printer.Cut(false);
}

function getFieldString(ID, zeroNoPrint, str) {
	if (!str)
		str = "";
	try {
		if (isDefined(Recipient.Fields.GetFieldByID(ID).Value) && !(Number(Recipient.Fields.GetFieldByID(ID).Value) == 0 && zeroNoPrint)) {
			 Printer.Writeln(Recipient.Fields.GetFieldByID(ID).Name + ": " + HtmlDecode(Recipient.Fields.GetFieldByID(ID).Value) + str);
		}	 
	} catch(e) {}	
}

