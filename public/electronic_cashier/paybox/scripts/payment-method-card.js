﻿function maxLimit(newSum) {

	if (PaymentGateway.SelectedRecipient.MaxLimit != 0)
		return Math.min(PaymentGateway.SelectedRecipient.MaxLimit, VirtuPOS.AvailableBalance);
	else
		return VirtuPOS.AvailableBalance;
}

function minLimit() {
	return PaymentGateway.SelectedRecipient.MinLimit;
}

function commissionSum() {
	return PaymentGateway.SelectedRecipient.CommissionSum;
}

function doPayment(sum) {
	
}