﻿
//печать фискального чека
function printFiscalCoupon(requestCompleteResult, errorText, incompletePayment, authCode, cardNr, tNr, AID, APP, TVR) {
	var Recipient = PaymentGateway.SelectedRecipient;
	//открываем  фискальный чек. Для ПРИМ 21 доп. информация должна идти именно после открытия чека.
	//Для других типов ФР переместить строку Printer.OpenReceipt(0, 1, "", "", "", ""); в начало фискальной части
	if (Number(Recipient.PaymentSum)>0 || Number(Recipient.CommissionSum)>0) {
		Printer.OpenReceipt(0, 1, "", "", "", "");
	}	
	if (incompletePayment!=null && incompletePayment == true) {
		Printer.Writeln("Внимание! Платеж оплачен не полностью.");
	}
	if (!asyncPayment || Recipient.AsyncPaymentDisabled || inSyncPaymentCategory()) {
		if (requestCompleteResult==0 || requestCompleteResult==2) {
			Printer.Writeln("Платеж проведен успешно");
		} else {
			Printer.Writeln("Ошибка проведения платежа: " + 	errorText);		
		}	
	}	
		
	Printer.Writeln("----");
	if (tNr) {	
		ReceiptPrinter.Writeln("Карточная операция №: " + tNr);
	} 
	if (cardNr) {
		ReceiptPrinter.Writeln("Номер карты: " + cardNr);
	}
	
	if (AID) {
		ReceiptPrinter.Writeln("AID " + AID);
	}
	if (APP) {
		ReceiptPrinter.Writeln("APP PREFERRED NAME " + APP);
	}	
	if (TVR) {
		ReceiptPrinter.Writeln("TVR " + TVR);
	}
	
	if (authCode && Number(authCode)>0) {
		ReceiptPrinter.Writeln("Код авторизации: " + authCode);
	}	
	if (Number(Recipient.CurrentPaymentNumber)>0) {	
		Printer.Writeln("Квитанция №: " + Recipient.CurrentPaymentNumber);
	}
	Printer.Writeln("Номер терминала: " + couponProperties.terminalNumber());
	Printer.Writeln("Адрес терминала: " + couponProperties.terminalAddress());
	Printer.Writeln("----");
	if (PaymentGateway.SelectedContract) {	
		if (PaymentGateway.SelectedContract.ProcessorID.toUpperCase().indexOf("CYBERPLAT")>=0) {
			Printer.Writeln("Платежная система КиберПлат");
		}
		Printer.Writeln("Кредитная организация: " + (isDefined(PaymentGateway.SelectedContract.FullName) ? PaymentGateway.SelectedContract.FullName : ""));
		Printer.Writeln("Договор " + (isDefined(PaymentGateway.SelectedContract.ContractNr) ? ("№"+PaymentGateway.SelectedContract.ContractNr) : "") + (isDefined(PaymentGateway.SelectedContract.ContractDate) ? (" от " + PaymentGateway.SelectedContract.ContractDate) : ""));
		if (isDefined(PaymentGateway.SelectedContract.Address)) {
			Printer.Writeln("Адрес организации: " + PaymentGateway.SelectedContract.Address);
		}	
		if (isDefined(PaymentGateway.SelectedContract.TaxNR	)) {
			Printer.Writeln("ИНН: " + PaymentGateway.SelectedContract.TaxNR);
		}
		if (isDefined(PaymentGateway.SelectedContract.BIN)) {
			Printer.Writeln("БИК: " + PaymentGateway.SelectedContract.BIN);
		}
	}	
	try { 		
		Printer.Writeln(Framework.Properties.GetEscapedString("PayBox", "System.Phones"));		
	} 	catch(e) {
	}

	Printer.Writeln("----");
	Printer.Writeln("Дата платежа: " + formatDateTime(new Date(), "dd.mm.yyyy HH:MM:SS"));
	Printer.Writeln("Получатель: " + Recipient.Name);
	Printer.Writeln("ИНН получателя: " + Recipient.TaxNr);

	if ((Recipient.PaymentTypes.Count > 0)
			&& (Recipient.SelectedPaymentTypeIndex >= 0)) {
		Printer.Writeln("Тип платежа: " + Recipient.PaymentTypes(Recipient.SelectedPaymentTypeIndex).Name);
	}

	for (var i=0; i<Recipient.Fields.Count; i++) {
		//alert (Recipient.Fields(i).Name + " isDefined:" +Recipient.Fields(i).IsDefined +" Visibility:"+Recipient.Fields(i).Visibility);
		if (Recipient.Fields(i).IsDefined && (Recipient.Fields(i).Visibility < 4)) {
			Printer.Writeln(Recipient.Fields(i).Name + ": " + Recipient.Fields(i).DisplayValue);
		}
	}

	Printer.Writeln("----");
	if (PaymentDevice.ChangeSum > 0) {
		Printer.Writeln("Сдача: " + formatMoneyPrinter(PaymentDevice.ChangeSum));
	}	
	//Фискальная часть
	if (Number(Recipient.PaymentSum)>0 || Number(Recipient.CommissionSum)>0) {
		if (Number(Recipient.CommissionSum)>0) {
			Printer.AddItemReceipt("Комиссия", "", "ШТ", "", parseInt(Recipient.CommissionSum*100), 1000, 1);
		}
		if (Number(Recipient.TransferSum)>0) {
			Printer.AddItemReceipt("К зачислению", "", "ШТ", "", parseInt(Recipient.TransferSum*100), 1000, 1);
		}	
		Printer.TotalReceipt();
		Printer.TenderReceipt(0, parseInt(Recipient.PaymentSum*100), "");
		Printer.CloseReceipt();
	}	

}

//печать НЕфискального чека
function printNoFiscalCoupon(requestCompleteResult, errorText, incompletePayment, authCode, cardNr, tNr, AID, APP, TVR) {
	var Recipient = PaymentGateway.SelectedRecipient;
	Printer.Alignment = 0;
	if (getParameter("printCoupon.printLogo", true)) {
		Printer.WriteLogo(1);
	}	
	Printer.Writeln(couponProperties.header());
	
	Printer.Alignment = 0;
	Printer.Writeln("----");
	if (tNr) {	
		ReceiptPrinter.Writeln("Квитанция №: " + tNr);
	} 
	Printer.Writeln("Номер терминала: " + couponProperties.terminalNumber());
	Printer.Writeln("Адрес терминала: " + couponProperties.terminalAddress());
	Printer.Writeln("----");	

	// формирование даты в нужном виде
	function formatDateTime(dateTime, mask) {
		var dateTime = new Date(dateTime);
		var dt = {
			'dd' : dateTime.getDate(),
			'm'  : dateTime.getMonth() < 8 ? "0"+(dateTime.getMonth()+1) : (dateTime.getMonth()+1),
			'y'  : dateTime.getYear() < 1000 ? dateTime.getYear()+1900 : dateTime.getYear(),
			'h'  : dateTime.getHours() < 10 ? "0" + dateTime.getHours() : dateTime.getHours(), 
			'mm' : dateTime.getMinutes() < 10 ? "0" + dateTime.getMinutes() : dateTime.getMinutes(),
			'ss' : dateTime.getSeconds() < 10 ? "0" + dateTime.getSeconds() : dateTime.getSeconds()
		}
		if (mask)
			return mask.replace("dd", dt.dd).replace("mm", dt.m).replace("yyyy", dt.y).replace("HH", dt.h).replace("MM", dt.mm).replace("SS", dt.ss);
		else 	
			return dt.dd + '.' + dt.m + '.' + dt.y + ' ' + dt.h + ':' + dt.mm + ':' + dt.ss; 
	}
	//---------------------------------
	
	Printer.Writeln("Дата платежа: " + formatDateTime(new Date()));
	if (getOperationCurrency() == "RUB")
		Printer.Writeln("Операция: покупка валюты "+getTargetCurrency());
	else
		Printer.Writeln("Операция: продажа валюты "+getTargetCurrency());
	
	Printer.Writeln("Обменный курс: "+getCurrency(getOperationCurrency()).format(getSourceUnits(), "printer")+" за "+getCurrency(getTargetCurrency()).format(getTargetUnits(), "printer"));
	Printer.Writeln("");
	Printer.Writeln("Принято: "+getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'printer'));
	
	try {
		var info = Framework.Variables("PrintingFields");
		//alert(info);
		Printer.Writeln("Cуммы выдачи по валютам:");
		if (info.amountTarget > 0)
				Printer.Writeln("Сумма основной выдаваемой валюты: "+getCurrency(getTargetCurrency()).format(info.amountTarget, 'printer'));
		if (info.amountDispense > 0)
				Printer.Writeln("Сумма основной выдаваемой сдачи: "+getCurrency(getTargetCurrency()).format(info.amountDispense, 'printer'));
		if (info.amountChange > 0)
				Printer.Writeln("Остаток в рублях, \nкоторый не может быть выдан: "+getCurrency().format(info.amountChange, 'printer'));			
	} catch(e) {};
	Printer.Writeln("----");	
	
	// Printer.Bold = 0;
	// if (Number(Recipient.PaymentSum)>0 || Number(Recipient.ChangeSum)>0) {
		// Printer.Writeln("Сумма наличных: " + getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'printer'));
		// Printer.Writeln("Комиссия: " + getCurrency().format(PaymentGateway.SelectedRecipient.CommissionSum, 'printer'));
	//	if (PaymentDevice.ChangeSum > 0) {		
		// Printer.Writeln("Сдача: " + getCurrency().format(PaymentDevice.ChangeSum, 'printer'));
	//	}	
		// Printer.Writeln("К зачислению: " + getCurrency().format(PaymentGateway.SelectedRecipient.TransferSum, 'printer'));
	// }
	
	Printer.Cut(false);

}
