﻿function printBalanceCoupon(authCode, cardNr, tNr, availableBalance) {
	if (Printer && Printer.DeviceReady) {

		Printer.Writeln(couponProperties.header());
		Printer.Alignment = 0;
		Printer.Writeln("----");
		Printer.Writeln("Номер терминала: " + couponProperties.terminalNumber());
		Printer.Writeln("Адрес терминала: " + couponProperties.terminalAddress());
		Printer.Writeln("----");
		if (PaymentGateway.SelectedContract) {
			Printer.Writeln("Кредитная организация: " + (isDefined(PaymentGateway.SelectedContract.FullName) ? PaymentGateway.SelectedContract.FullName : ""));
			Printer.Writeln("Договор " + (isDefined(PaymentGateway.SelectedContract.ContractNr) ? ("№"+PaymentGateway.SelectedContract.ContractNr) : "") + (isDefined(PaymentGateway.SelectedContract.ContractDate) ? (" от " + PaymentGateway.SelectedContract.ContractDate) : ""));
			if (isDefined(PaymentGateway.SelectedContract.Address)) {
				Printer.Writeln("Адрес организации: " + PaymentGateway.SelectedContract.Address);
			}	
			if (isDefined(PaymentGateway.SelectedContract.TaxNR	)) {
				Printer.Write("ИНН: " + PaymentGateway.SelectedContract.TaxNR);
			}
			if (isDefined(PaymentGateway.SelectedContract.BIN)) {
				Printer.Writeln(" БИК: " + PaymentGateway.SelectedContract.BIN);
			}
		}
		
		try { 
			Printer.Writeln("Адрес организации: " + window.external.Framework.Properties.GetEscapedString("PayBox", "System.CompanyAddress") + "\n\
			ИНН: " + window.external.Framework.Properties.GetEscapedString("PayBox", "System.CompanyINN"));
		} 	catch(e) {}

		Printer.Writeln("----");
		Printer.Writeln("Дата : " + formatDateTime(new Date()));
		Printer.Writeln("Карточная операция №: " + tNr);
		Printer.Writeln("Номер карты: " + cardNr);
		Printer.Writeln("Код авторизации: " + authCode);
		Printer.Bold = 1;

		Printer.Writeln("--------------------------------------------");
		Printer.Writeln(availableBalance);
		Printer.Writeln("--------------------------------------------");
		Printer.Writeln("");
		
		Printer.Bold = 0;
		Printer.Alignment = 2;
		Printer.Cut(false);
	}
}

function formatPaymentsRegisterStr(date, type, Amount) {
	var date =  new Date(date);

	var spacesStr = "                    ";
	if (type) {
		type = "Списание";
	} else {
		type = "Пополнение";
	}
	return " " + spacesStr.substring((formatDateTime(date, "dd.mm.yyyy") + "").length, 12) + formatDateTime(date, "dd.mm.yyyy") + "  ¦ "
		+ spacesStr.substring((type + "").length, 10) + type + " ¦ "
		+ spacesStr.substring((Amount + "").length, 13) + Amount + "";
}