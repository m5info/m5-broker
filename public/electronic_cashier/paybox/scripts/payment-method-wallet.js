﻿function maxLimit() {
	if (PaymentGateway.SelectedRecipient.MaxLimit != 0)
		return Math.max(PaymentGateway.CustomerBalance, PaymentGateway.SelectedRecipient.MaxLimit);
	else
		return PaymentGateway.CustomerBalance;
}

function minLimit() {
	return PaymentGateway.SelectedRecipient.MinLimit;
}

function commissionSum() {
	return PaymentGateway.SelectedRecipient.CommissionSum;
}

function doPayment(sum) {
	PaymentGateway.CustomerBalance -= sum;
}