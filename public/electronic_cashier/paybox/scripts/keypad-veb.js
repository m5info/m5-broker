﻿/**
Отрисовывает стандартный цифровой keypad и привязывает
его кнопки к функциям processChar и processKey реализованные на основной странице
*/

// Префикс клавиатуры
var keypadPrefix = '<div class="inputArea"><table border="0" cellpadding="8" style="margin-left:50px;margin-top:20px;">'
// Префикс каждого ряда клавиатуры
var keypadRowPrefix = '<tr>'
// Формат кнопки. Определены name, rowNum и colNum
//var keypadButtonFormat = '<td align="center"><input type=button value="${name}" onclick="keypadPressed(${rowNum}, ${colNum}, this)"></td>';
var keypadButtonFormat = '<td colspan="${colspan}" align="center"><div style="background-image:url(\'images/keypad/${name}.png\');" class="keyPadButton ${name}" onclick="keypadPressed(${rowNum}, ${colNum}, this)" id="keypad_key_${name}"></td>';
// Заполнитель для пропусков
var keypadSpacer = '<td>&nbsp;</td>';
// Суффик каждого ряда клавиатуры
var keypadRowSuffix = '</tr>'
// Суффик клавиатуры
var keypadSuffix = '</table></div>';

// Имя элемента, который будет создан или использован на основной странице для отображения клавиатуры
var keypadElementID = "keypadDiv";

var keypadElement = null;

function KeypadButton(action, name, colspan) {
	this.action = action;
	this.name = name;
	this.colspan = colspan;
}

var Keypad = [
	[ "1", "2", "3"	],
	[ "4", "5", "6"	],
	[ "7", "8", "9"	],
	[ new KeypadButton(8, "Backspace", 2), "0" ]
//	[ null, null, new KeypadButton(function() { alert ("function"); }, "Action") ]
];

function simulatePressButton(btn) {
	btn.disabled = true;
	var idx = btn.style.backgroundImage.indexOf('.');
	var sImgName = btn.style.backgroundImage.substr(0, idx);
	var sImgExt = btn.style.backgroundImage.substr(idx);
	btn.style.backgroundImage = sImgName + "_press"+sImgExt;
	setTimeout(function() {
		var idx = btn.style.backgroundImage.indexOf('.');
		var sImgName = btn.style.backgroundImage.substr(0, idx);
		var sImgExt = btn.style.backgroundImage.substr(idx);
		idx = sImgName.indexOf("_");
		sImgName = sImgName.substr(0, idx);
		btn.style.backgroundImage = sImgName + sImgExt;
		btn.disabled = false;
	}, 200);
}

function keypadPressed(rowNum, colNum, btn) {
	// Отрабатываем реакцию на кнопки
	simulatePressButton(btn);

	// Действие
	var action = typeof(Keypad[rowNum][colNum]) == "object" ? Keypad[rowNum][colNum].action : Keypad[rowNum][colNum];
	
	if (typeof(action) == "string") {
		// нам передали символ (т.е. просто символ)
		//processChar(action);
		_.fireEvent('char', action);
	} else if (typeof(action) == "number") {
		// нам передали число (т.е. код клавиши)
		_.fireEvent('key', action);
	} else {
		action();
	}
}

function hideKeypad() {
	if (keypadElement == null)
		createKeypad();
		
	keypadElement.style.display = 'none';
	keypadElement.style.visibility = 'hidden';
}

function showKeypad() {
	if (keypadElement == null)
		createKeypad();
		
	keypadElement.style.display = 'inline';
	keypadElement.style.visibility = 'visible';
}

function createKeypad() {
	if (keypadElement != null) return;
	
	keypadElement = document.getElementById(keypadElementID);
	if (keypadElement == null) {
		keypadElement = document.createElement('div');
		keypadElement.id = keypadElementID;
		
		var bodyElement = document.getElementsByTagName("body");
		if ((bodyElement == null) || (bodyElement.length < 1)) {
			alert("No body element");
			return false;
		} else 
			bodyElement[0].appendChild(keypadElement);		
	}

	var k = [];
	
	if (keypadPrefix != null) k.push(keypadPrefix);

	for (var i=0, ci = Keypad.length; i < ci; i++) {
		if (keypadRowPrefix != null) k.push(keypadRowPrefix);
	
		for (var j=0, cj = Keypad[i].length; j < cj; j++) {
			if (Keypad[i][j] == null) {
				k.push(keypadSpacer);
			} else {
				var name = typeof(Keypad[i][j]) == "object" ? Keypad[i][j].name : Keypad[i][j];
				var colspan = typeof(Keypad[i][j]) == "object" ? Keypad[i][j].colspan : 1;
					k.push(keypadButtonFormat.replace(/\$\{(name|rowNum|colNum|colspan)}/g,
						function(str, v) {
							if (v == "name") return name;
							if (v == "colspan") return colspan;
							else if (v == "rowNum") return i;
							else if (v == "colNum") return j;
							else return str;
				}));
			  }
		}
		
		if (keypadRowSuffix != null) k.push(keypadRowSuffix);
	}
	
	if (keypadSuffix != null) k.push(keypadSuffix);

	keypadElement.innerHTML = k.join('');
}

