﻿(function() {

	var statesRequired = {};
	var handlerKey = null;
	var goFurther = false;

	_.addEventListeners({
	
		"load" : function() {
			
			// заполняем массив, добавляя в него необходимые
			statesRequired = {
			
			/* уникальное имя обработчика */
			// "myHandler" : {
				/* обязательный параметр: состояния (recipients), либо состояния с кодом получателя (data-entry-code1) */
				// "required" : [ 'recipients' , 'data-entry-code1' ]
				/* дополнительный параметр: коды получателей, для которых обработчик не будет применяться */
				// "denied" : [ 'code2' , 'code3' ]
			
				// }
								
				"whenBillAccepting" : {
					"required" : ['accept-payment-free']
				},
				
				"cardPresent" : {
					// не рекомендуется изменять это условие
					"required" : (!VirtuPOS.Offline && VirtuPOS.IsCardPresent) ? [_.type] : []
				
				},
				
				"acceptPaymentVeb" : {
					"required" : ['accept-payment']
				}		
			
			}

			for (var o in statesRequired) {
			
				if (!statesRequired[o].hasOwnProperty("required"))
					continue;
					
				for (var i=0 ; i<statesRequired[o]["required"].length; i++) {
					if (statesRequired[o]["required"][i] == _.name) 
						handlerKey = o;
					else if (statesRequired[o]["required"][i] == _.type)
						handlerKey = o;
				}
				
				if (handlerKey != null && statesRequired[o].hasOwnProperty("denied"))
					for (var i=0 ; i<statesRequired[o]["denied"].length; i++) {
						if (statesRequired[o]["denied"][i] == _.extension) {
							handlerKey = null;
							break;
						}
					}		
				
			}			
			
		},

		"run" : function() {
			// alert(handlerKey)
			if (handlerKey) {
				switch(handlerKey) {								
				// здесь описывайте реакции на события
				
				// приём наличных для ВЭБ
					case 'acceptPaymentVeb':
					_.addEventListeners({
					
						"inactivityTimer" : function() {
							return true;
						}
						
					});
					break;	
					
				// приём наличных
					case 'whenBillAccepting':
					_.addEventListeners({
					
						"inactivityTimer" : function() {
							
							if (goFurther) return true;
							showAlert('Вам нужно дополнительное время?',
								{
									"yes" : function() { _.resetInactivityTimer(); },
									"no" : function() { 
										goFurther = true;
										_.fireEvent("inactivityTimer"); 
									}
								},
								{
									13 : "yes",
									27 : "no"
								},
								"no",
								getParameter("inactivityTimeouts.acceptPaymentAskForMoreTime") > 1000 ? getParameter("inactivityTimeouts.acceptPaymentAskForMoreTime") : 7000
							);
							return false;
						}
						
					});
					break;

					// карта внутри
					case "cardPresent" :
					_.addEventListeners({
						
						"inactivityTimer" : function() {

							if (goFurther) return true;
							showAlert('Вам нужно дополнительное время?',
								{
									"yes" : function() { _.resetInactivityTimer(); },
									"no" : function() { 
										Framework.Variables("EjectReason") = "Операция не подтверждена пользователем";
										_.leave("timeout"); 
									}
								},
								{
									13 : "yes",
									27 : "no"
								},
								"no",
								getParameter("inactivityTimeouts.cardAskForMoreTime") > 1000 ? getParameter("inactivityTimeouts.cardAskForMoreTime") : 7000
							);
							return false;
						}
						
					});
					break;
					
					default: return;
					
				}
			
			}
			
		}

		
	});	
	
})();