﻿appendApplication(
	{ "paybox" : "*" }, // frames
	"paybox", // viewDocument
	function () { // entryPoint
		return "start";
	},
	 // дополнительные скрипты, подключаемые к приложению	
	['states-events.js'],
	
	// css
	"veb"
	
	//  ,"my-own-styles" //  только имя файла css, без расширения
	// ,["my-own-styles", "not-my-own-styles"] // или массив имен
);

appendParameters({
	"veb" : {
		// Запрещенные к приему номиналы купюр (в копейках: 1000 = 1000 к = 10 р)
		// "inhibitedBills" : [1000]
		"inhibitedBills" : []
	}
});	
	
/**
Можно переопределять состояния
*/
 appendStates({
	"initialize" : {
		"requires": ["find-recipients.js"]
	},
	"accept-payment-currencyExchange" : {
		"extends" : "accept-payment",
		"requires": ["exchg-summ-calc.js"]
	},
	"recipients-veb" : {
		"extends" : "recipients",
		"screen" : "recipients-veb.html",
		"behaviour" : "recipients-veb.js"
	},	
	"recipients-CREDIT" : "recipients-veb",
	"recipients-ACCOUNT" : "recipients-veb",
	"recipients-DEPOSIT" : "recipients-veb",
	"recipients-CARD" : "recipients-veb",
	"balance-request" : {
	},
	"ministatement-request" : {
	},
	"print-additional-coupon" : {
	},
	"print-credit-balance-coupon" : {
		"extends" : "print-additional-coupon",
		"requires": ["print-credit-balance-coupon.js", "terminal.js"]
	},
	"print-account-balance-coupon" : {
		"extends" : "print-additional-coupon",
		"requires": ["print-account-balance-coupon.js", "terminal.js"]
	},
	"print-deposit-balance-coupon" : {
		"extends" : "print-additional-coupon",
		"requires": ["print-deposit-balance-coupon.js", "terminal.js"]
	},
	"print-card-balance-coupon" : {
		"extends" : "print-additional-coupon",
		"requires": ["print-card-balance-coupon.js", "terminal.js"]
	},
	"print-credit-ministatement-coupon" : {
		"extends" : "print-additional-coupon",
		"requires": ["print-credit-ministatement-coupon.js", "terminal.js"]
	},
	"print-account-ministatement-coupon" : {
		"extends" : "print-additional-coupon",
		"requires": ["print-account-ministatement-coupon.js", "terminal.js"]
	},
	"print-deposit-ministatement-coupon" : {
		"extends" : "print-additional-coupon",
		"requires": ["print-deposit-ministatement-coupon.js", "terminal.js"]
	},
	"print-card-ministatement-coupon" : {
		"extends" : "print-additional-coupon",
		"requires": ["print-card-ministatement-coupon.js", "terminal.js"]
	},
	"pin-entry" : "pin-entry-veb",
	"pin-entry-veb" : {
		"screen" : "pin-entry-veb.html", 
		"behaviour" : "pin-entry.js",
		"view" : "pin-entry-view-veb.js"
	},
	"sum-entry-card" : "sum-entry-veb",
	"sum-entry-veb" : {
		"screen" : "sum-entry-veb.html",
		"behaviour" : "sum-entry.js",
		"view" : "sum-entry-view-veb.js",
		"requires" : ["keypad-veb.js", "payment-method-card.js"]
	},
	"accept-payment" : "accept-payment-veb",
	"accept-payment-veb" : {
		"screen" : "accept-payment-veb.html",
		"behaviour" : "accept-payment-veb.js",
		"view" : "accept-payment-view-veb.js"
	},
	"data-entry" : "data-entry-veb",
	"data-entry-veb" : {
		"screen" : "data-entry-veb.html",
		"behaviour" : "data-entry-veb.js",
		"view" : "data-entry-view-veb.js",
		"requires" : ["keypad-veb.js", "auth.js"]
	},
	"data-entry-8538" : {
		"behaviour" : "blag-fond-attention.js",
		"view" : "blag-fond-attention-view.js"
	},
	"data-entry-CREDIT-REQUEST" : {
		"behaviour" : "credit-request-attention.js",
		"view" : "credit-request-attention-view.js"
	},
	"data-entry-veb-CREDIT-REQUEST" : {
		"screen" : "data-entry-veb-CREDIT-REQUEST.html",
		"behaviour" : "data-entry-veb-CREDIT-REQUEST.js",
		"view" : "data-entry-view-veb-CREDIT-REQUEST.js",
		"requires" : ["keypad-veb.js", "auth.js"]
	},
	"credit-request-info" : {
		"behaviour" : "credit-request-info.js",
		"view" : "credit-request-info-view.js"
	},
	"credit-request-email" : {
		"requires" : ["terminal.js"]
	},
	"find" :  {
		"screen" : "find.html",
		"behaviour" : "find.js",
		"view" : "find-view.js"
	},
	"encashment-switch" : {
		"application" : "encashment-web"
	}
	
});

/**
Можно переопределять праила навигации
**/
appendNavigationRules({
	// ВЭБ
	// Состояние инициализации
	"initialize" : {
		"billAcceptorCassette" : "service-entry",
		"ejectCard" : "eject-card",
		"rootSelected" : [
			new NavigationRule(
				function () { return PaymentGateway.SelectedCatalogItem != null ? true : false;},
				"recipients" ),
			"initialize"
		]
	},
	"find" : {
		"ejectCard" : "eject-card",
		"initialize" : "initialize",
		"recipients" : "recipients",
		"recipientSelected" :
		[	// ================ select-payment-type ================
			new NavigationRule(
					function() { 
					return PaymentGateway.SelectedRecipient.PaymentTypes.Count > 0; },
					"select-payment-type"),	
			// ================ barcode-entry ================
			new NavigationRule(
					function() { 
					return !BarcodeReader.Offline && PaymentGateway.SelectedRecipient.Barcodes != null },
					"barcode-entry"),

			new NavigationRule(
					function() { 
					return (PaymentGateway.SelectedRecipient.InCategory("PeerToPeer") ||
					PaymentGateway.SelectedRecipient.InCategory("MiniStatementQuery") ||
					PaymentGateway.SelectedRecipient.InCategory("BalanceQuery"))
					},
					"insert-card"),					
					
			// ================ data-entry ================
			new NavigationRule(
					function() { return applicationDocument.hasInputFieldsOnSelected(); },
					"data-entry"),

			// ================ data-check ================
			new NavigationRule(function() { 
				return PaymentGateway.SelectedRecipient.CheckDefined && 
				PaymentGateway.SelectedRecipient.CheckRequired &&
				PaymentGateway.SelectedRecipient.InCategory("VirtuPOSCheck");  
				},
				"virtupos-check"),			
				
			new NavigationRule(function() { 
				return PaymentGateway.SelectedRecipient.CheckDefined;  
				},
				"data-check"),

			"after-data-check"
			
		]		
	},	
	"recipients-veb" : {
		"recipients" : "recipients",
		"selectCash" : "accept-payment",
		"selectCard" : "sum-entry-card",
		"selectBalance" : "balance-request",
		"selectMinistatement" : "ministatement-request"	
	},
	"data-entry" : {
		"recipientCatalog" : "recipients",
		"recipientComplete" : [
			new NavigationRule( 
				function () { return PaymentGateway.SelectedRecipient.InCategory("IssueOfCash"); },
				"insert-card"),
			new NavigationRule( 
				function () { return PaymentGateway.SelectedRecipient.InCategory("VirtuPOSCheck"); },
				"virtupos-check"),
			new NavigationRule( 
				function () { return PaymentGateway.SelectedRecipient.InCategory("PeerToPeer"); },
				"after-data-check"),
			new NavigationRule( 
				function () { return PaymentGateway.SelectedRecipient.CheckDefined; },
				"data-check"),				
			"after-data-check"
		],
		"catalogItemComplete" : "select",
		"initialize" : "initialize",
		"ejectCard" : "eject-card",
		"encashment" : "service-entry"
	},	
	"data-entry-veb-CREDIT-REQUEST" : {
		"recipientCatalog" : "recipients",
		"info" : "credit-request-info",
		"recipientComplete" : "credit-request-email",
		"initialize" : "initialize",
		"ejectCard" : "eject-card"		
	},
	"data-entry-CREDIT-REQUEST" : {
		"ejectCard" : "eject-card",
		"commit" : "data-entry-veb-CREDIT-REQUEST",
		"recipients" : "recipients"
	},

	"credit-request-info" : {
		"ejectCard" : "eject-card",
		"commit" : "data-entry-veb-CREDIT-REQUEST",
		"recipients" : "recipients"
	},
	"credit-request-email" : {
		"ejectCard" : "eject-card",
		"recipients" : "recipients"
	},	
	"data-entry-8538" : {
		"ejectCard" : "eject-card",
		"commit" : 	[
			// Если пополнение карты
			new NavigationRule(
				function () { 
					if (VirtuPOS && VirtuPOS.IsCardPresent ) 
						Framework.Variables("PaymentMethod") = "acceptCardPayment";
					else 
						Framework.Variables("PaymentMethod") = "acceptCashPayment";
					return VirtuPOS && VirtuPOS.IsCardPresent;
				},
					"sum-entry-card"),			
			"accept-payment"
		],	
		"recipients" : "recipients"
	},
	"sum-entry-card" : {
		// исполнение платежа
		"commit" : 
		[
			// Если пополнение карты
			new NavigationRule(
				function () { return PaymentGateway.SelectedRecipient.InCategory("PeerToPeer"); },
				"peer-to-peer"),			
			"card-payment"
		],	
		"cancel" : "eject-card",
		"recipients" : "recipients"
	},
	"accept-payment" : {
		// Потеряли SelectedRecipient
		"cancel" : 
		[	
			// Переход с принятия оплаты назад.
			new NavigationRule( 
				function () { return !VirtuPOS.IsCardPresent && applicationDocument.hasInputFieldsOnSelected(); }, 
				"data-entry"),
			"recipients"
		],
		"commit" : "payment",
		"ejectCard" : "eject-card",
		"timeout" : "initialize"
	},
	"balance-request" : {
		"cancel" : "recipients",
		"credit" : "print-credit-balance-coupon",
		"account" : "print-account-balance-coupon",
		"deposit" : "print-deposit-balance-coupon",
		"card" : "print-card-balance-coupon"
	},
	"ministatement-request" : {
		"cancel" : "recipients",
		"credit" : "print-credit-ministatement-coupon",
		"account" : "print-account-ministatement-coupon",
		"deposit" : "print-deposit-ministatement-coupon",
		"card" : "print-card-ministatement-coupon"		
	},
	"print-additional-coupon" : {
		"continue" : "recipients"
	},
	"eject-card" : {
		"capture" : "capture-card",
		"cardCaptured" : "card-captured",
		"commit" : 	"initialize"
	},
	"after-data-check" : {
		"commit" : [
			//   Несколько способов оплаты 
			new NavigationRule( 
				function () {
					Framework.Variables("PaymentMethod") = "acceptCardPayment";
					return applicationDocument.getRecipientPaymentMethodCount() > 1 && VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsCardPresent; 
				}, 
				"sum-entry-card"
			),
				
			new NavigationRule( 
				function () { 
					Framework.Variables("PaymentMethod") = "acceptCashPayment";
					return applicationDocument.getRecipientPaymentMethodCount() > 1;
				}, 
				"accept-payment"
			),
				
			// Один спосб оплаты
			// 1.  Card
			new NavigationRule( 
				function () { 
					Framework.Variables("PaymentMethod") = "acceptCardPayment";
					return VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsCardPresent && applicationDocument.getRecipientPaymentMethodCount() == 1 && applicationDocument.getRecipientPaymentMethods()[0].state == "acceptCardPayment"; 
				}, 
				"sum-entry-card"
				),
			// 2. Wallet
			new NavigationRule( 
				function () { 
					return applicationDocument.getRecipientPaymentMethodCount() == 1 && applicationDocument.getRecipientPaymentMethods()[0].state == "acceptWalletPayment"; 
				}, 
				"sum-entry-wallet"
				),
			// 3. Cash
			new NavigationRule ( 
				function () { 
					Framework.Variables("PaymentMethod") = "acceptCashPayment";
					return applicationDocument.getRecipientPaymentMethodCount() == 1 && applicationDocument.getRecipientPaymentMethods()[0].state == "acceptCashPayment";
				}, 
				"accept-payment"),
			//   Не требуется приём или списание денежных средств, т.к. по каким-то причинам мы сразу переходим к проведению платежа
			new NavigationRule( 
				function () { return PaymentGateway.SelectedRecipient.InCategory("AcceptPaymentNotRequired"); },
				"payment"),
				
			// Перезапуск, если ни одно из верхних условий не выполнилось
			"initialize"
			
		],
		
		"commitNeedInput" : "data-entry",
		
		"cancel" : [
			new NavigationRule (
				function () { return applicationDocument.hasInputFieldsOnSelected(); }, 
				"data-entry"),
					
			new NavigationRule (
				function () { return !applicationDocument.hasInputFieldsOnSelected(); }, 
				"recipients"
			)
		],
		
		"fail" : [
			new NavigationRule (
				function () { return applicationDocument.hasInputFieldsOnSelected(); }, 
				"data-entry"),
					
			new NavigationRule (
				function () { return !applicationDocument.hasInputFieldsOnSelected(); }, 
				"recipients"
			)
		]
	},
	"recipients" : {
		"find" : "find",
		"ejectCard" : "initialize",
		"information" : "information",
		"linkSelected" : 
		[
			// ================ external-link ================
			// Это ссылка и она не локальная, т.е. считаем, что уходим на веб
			new NavigationRule(
				function () { return PaymentGateway.SelectedCatalogItem.IsLink && (PaymentGateway.SelectedCatalogItem.HREF.indexOf("http")>=0) }, 
				"external-link"),

			// ================ прямая навигация внутри сценария ================
			// Это ссылка и она локальная, отображаем в основном контексте
			// TODO!
			new NavigationRule(function() { return PaymentGateway.SelectedCatalogItem.IsLink },
					function() { return PaymentGateway.SelectedCatalogItem.HREF }),
		],
		"parentSelected" : "recipients",
		"groupSelected" : 
		[
		
			new NavigationRule(
				function() { return PaymentGateway.SelectedCatalogItem.InCategory("FillingCollection"); },
				"recipients-fixedsum-collection"),		
		
			new NavigationRule(
				function() { return PaymentGateway.SelectedCatalogItem.SelectDefined && applicationDocument.hasInputFieldsOnSelected(); },
				"data-entry"),

			new NavigationRule(
				function() { return PaymentGateway.SelectedCatalogItem.SelectDefined; },
				"select"),
			"recipients"
		],
		"recipientSelected" :
		[	// ================ select-payment-type ================
			new NavigationRule(
					function() { 
					return PaymentGateway.SelectedRecipient.PaymentTypes.Count > 0; },
					"select-payment-type"),	
			// ================ barcode-entry ================
			new NavigationRule(
					function() { 
					return !BarcodeReader.Offline && PaymentGateway.SelectedRecipient.Barcodes != null },
					"barcode-entry"),

			new NavigationRule(
					function() { 
					return (PaymentGateway.SelectedRecipient.InCategory("PeerToPeer") ||
					PaymentGateway.SelectedRecipient.InCategory("MiniStatementQuery") ||
					PaymentGateway.SelectedRecipient.InCategory("BalanceQuery"))
					},
					"insert-card"),					
					
			// ================ data-entry ================
			new NavigationRule(
					function() { return applicationDocument.hasInputFieldsOnSelected(); },
					"data-entry"),

			// ================ data-check ================
			new NavigationRule(function() { 
				return PaymentGateway.SelectedRecipient.CheckDefined && 
				PaymentGateway.SelectedRecipient.CheckRequired &&
				PaymentGateway.SelectedRecipient.InCategory("VirtuPOSCheck");  
				},
				"virtupos-check"),			
				
			new NavigationRule(function() { 
				return PaymentGateway.SelectedRecipient.CheckDefined;  
				},
				"data-check"),

			"after-data-check"
			
		],
		"cardReading" : "insert-card",
		"cardCaptured" : "card-captured",
		"pinEntry" : "pin-entry"
	}	

});
// 
addLocale("ru", {

	"paykiosk" : {
		"noTerminalNumber" : "не задан",
		"balanceFormat" : "Баланс ${getCurrency(getParameter('paykiosk.walletCurrency', 'RUR')).format(PaymentGateway.CustomerBalance)}",
		"changeSumFormat" : "Остаток ${getCurrency('RUR').format(Framework.Variables('changeSum'))}",
		"billAcceptorFailure" : "Терминал временно не работает. Неисправность купюроприемника<br><br><img src='images/znak.png'>",
		"printerFailure" : "Терминал временно не работает. Неисправность принтера<br><br><img src='images/znak.png'>",
		"printerOfflineWarning" : "По техническим причинам квитанция не будет напечатана<br>Продолжить?<br>",
		"billDispenserFailure" : "Терминал временно не работает. Неисправность в устройстве выдачи купюр<br><br><img src='images/znak.png'>"
	},
	"recipientsVEB" : {
		"onlyOfficePaymentMessage" : "Пополнение данного вклада возможно только через кассу",
		"unableToPay" : "Согласно условиям договора вклад не может быть пополнен",
		"unableToPayMaxSum" : "Внимание! Минимальная сумма пополнения составляет ${getCurrency('RUR').format(Number(PaymentGateway.SelectedCatalogItem.ChildItems.Item(_.idx).Fields.GetFieldByID(150).SelectValue))}<br>Внимание! Максимальная сумма пополнения наличными через терминал ${getCurrency('RUR').format(PaymentGateway.SelectedCatalogItem.ChildItems.Item(_.idx).Recipient.MaxLimit)} (включая комиссию)"
	},
	"balanceRequest" : {
		"balanceRequestInProgress" : "Идет запрос баланса<br>Пожалуйста подождите",
		"checkFailed" : "Ошибка запроса баланса<br>Повторите, пожалуйста, позже"
	},
	"ministatementRequest" : {
		"balanceRequestInProgress" : "Идет запрос минивыписки<br>Пожалуйста подождите",
		"checkFailed" : "Ошибка запроса минивыписки<br>Повторите, пожалуйста, позже"
	},
	"printCoupon" : {
		"printDone" : "Квитанция распечатана<br>Возьмите Вашу квитанцию",
        "printDone1" : "Операция выполнена успешно<br><br>Благодарим Вас за доброе сердце и участие в жизни детей.<br>Отчет о деятельности Фонда, результаты работы и текущие акции<br>Вы всегда можете увидеть на сайте Фонда: http://www.vostfond.ru",
		"printFailed" : "Квитанция не может быть напечатана из-за технической неисправности"
	},
	"select" : {
		"selectInProgress" : "Идет запрос Ваших услуг ${PaymentGateway.SelectedCatalogItem.Name}<br>Пожалуйста, подождите....<br><br><img src='images/clock.png'>",
		"selectSucceeded" : "Исходя из введенных данных выбран получатель<br>${PaymentGateway.SelectedCatalogItem.Name}",
		"groupSelected" : "Исходя из введенных данных выбрано несколько получателей<br>Сделайте выбор одного из них<br><br>",
		"incorrectSelected" : "Ошибка выбора доступных услуг ${PaymentGateway.SelectedCatalogItem.Name}<br>Попробуйте позже или обратитесь в отделение банка",
		"selectFailed" : "Ошибка проверки данных<br>${enumerateRecipientFields(PaymentGateway.SelectedCatalogItem.Fields, _.fieldValueTemplate, false)}<br>",
		"selectFailedUniBank" : "Ошибка проверки данных с параметрами:<br><br>${enumerateRecipientFields(PaymentGateway.SelectedCatalogItem.Fields, _.fieldValueTemplate, false)}<br>",
		"checkData" : "Проверьте правильность введенных данных",
		"areYouAgree" : "Вы согласны?",
		"selectValidationFailed" : "Ошибка проверки данных"
	},
	"acceptPaymentVeb" : {
		"insertAttention" : "Вставьте купюры в купюроприемник",
		"sumAttention" : "Внимание!<br>Минимальная сумма платежа - ${getCurrency().format(Math.max(_.minSum, _.minSumDisplayDeposit), 'short')} ",
		"maxSumAttention" : "Максимальная - ${getCurrency().format(_.maxSum, 'short')} с учетом комиссии.<br>",
		"changeAttention" : "Терминал сдачи не выдает!",
		"depositNotFullPayment" : "Внимание!<br>Средства зачислены на Связанный счет ",
		"depositNotFullCurrentPayment" : "<font color=red>Вы внесли сумму менее минимальной. Средства будут зачислены на Ваш связанный счет.</font>"
	},
	"sumEntryVeb" : {
		"maxBalanceLimitReachedCard" : "Вы не можете выбрать сумму больше ${getCurrency().format(_.maxSum)}",
		"sumAttention" : "Внимание!<br>Минимальная сумма платежа - ${getCurrency().format(_.minSum, 'short')}<br> ",
		"maxSumAttention" : "Максимальная - ${getCurrency().format(_.maxSum, 'short')} с учетом комиссии.<br>"
	},
	
	"ejectReason" : {
		"cancel" : "Операция отменена пользователем",
		"success" : "Операция завершена успешно"
	},	

	"fondAttention" : {
		"info" : "<p>Восточный экспресс банк приглашает Вас принять участие в работе Благотворительного фонда 'Восточный'. Любая сумма, перечисленная Вами, будет с благодарностью принята и направлена на решение жизненно важных вопросов маленьких россиян.<br><br>Вы помогаете:<br><br><p align='left'>- тяжелобольным детям;<br>- семьям, усыновившим или взявшим под опеку детей-сирот;<br>- детям, с ограниченными физическими возможностями.</p><br><br>Именно Ваша помощь спасет жизнь маленького гражданина, поможет ребенку обрести семью, решит проблемы общения и образования детей-инвалидов.</p>"
	},
	"creditRequestInfo" : {
		"info" : "<p>Настоящим, Я (КЛИЕНТ) выражаю свое согласие на осуществление Банком обработки (сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение) использования, распространения (в том числе в целях Банка) обезличивания, блокирования и уничтожения) автоматизированным способом  своих персональных данных (фамилии, имени, отчества, года, месяца,  даты   и   места рождения,  адреса проживания  (регистрации),  номера   телефона, информации о месте работы, должности, наличии/отсутствии  родственников    и    указании степени родства (без указания ФИО родственников), сведений о банковских счетах в других банках (без указания номера счета, договора)) для проведения маркетинговых исследований, продвижения услуг Банка,  информирования КЛИЕНТА с  помощью  средств связи   (телефон, Интернет, электронная почта, почта) о продуктах  и услугах  БАНКА. Настоящее  согласие не  зависит от заключения (срока действия), договора, дано на  неопределённый  срок и   может быть отозвано КЛИЕНТОМ путём предоставления в БАНК письменного заявления.</p><p>КЛИЕНТ подтверждает, что    Банком  предоставлена   информация   о     правах     субъекта персональных   данных,  установленных  Федеральным законом от   27.07.2006 г.   № 152-ФЗ «О персональных данных».</p>"
	},
	"creditRequestAttention" : {
		"info" : "<p>Уважаемый клиент!<br><br>После заполнения формы с вами свяжется сотрудник банка,<br>который подберет индивидуально для Вас<br>наиболее подходящий продукт,<br>предоставит консультацию, ответит на все вопросы,<br>поможет в оформлении заявки на кредит.</p>"
	},
	
	"creditRequestEmail" : {
		"from" : "credit-request@express-bank.ru",
		"receiver" : "_obr_zay_s_terminala_@orient.root.biz",
		"subject" : "Запрос на кредит с терминала",
		"smtpserver" : "192.168.132.173",
		"message" : "Дата и время подачи заявки: ${_.getDbtDate().toString()}\r\nАдрес терминала и его номер: ${_.address} ${_.number}\r\n\rФИО: ${PaymentGateway.SelectedCatalogItem.Fields.GetFieldByID(100).Value}\r\nМобильный телефон: +7 ${PaymentGateway.SelectedCatalogItem.Fields.GetFieldByID(101).Value}",
		"mailSending" : "<p>${PaymentGateway.SelectedCatalogItem.Fields.GetFieldByID(100).Value},</p><p>Выполняется отправка Вашей заявки</p>Пожалуйста, подождите....<br><br><br><img src='images/clock.png'>",
		"mailSendError" : "Ошибка отправки сообщения",
		"mailSendSuccess" : "<p>${PaymentGateway.SelectedCatalogItem.Fields.GetFieldByID(100).Value},</p><p>Благодарим Вас за проявлянный интерес к услугам </p><p>Восточного Экспресс Банка.</p><p>В ближайшее время сотрудник Банка перезвонит Вам.</p>"
	}
			
	
});

/**
Можно переопределять часть сообщений
**
addLocale("en", {
			"recipientsView": {
				"paymentRulesLink": "*** Select Language",
				"groupStatusText" : "<table class=statustxt><tr><td><img width=${PaymentGateway.SelectedCatalogItem.Picture.Width} height=${PaymentGateway.SelectedCatalogItem.Picture.Height}  src='images-recipients/${getActiveLocale()}/${PaymentGateway.SelectedCatalogItem.Picture.URL}'></td><td>${PaymentGateway.SelectedCatalogItem.Name}</td></tr>",
				"pageOfThePage" : "Page ${(currentPageNr + 1)} из ${(lastPageNr + 1)}"
			}
		});
		
*/		