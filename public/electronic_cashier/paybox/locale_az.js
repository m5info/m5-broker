﻿addLocale(
		"az",
		{
			"currency" : {
				"RUR" : "Рубли РФ",
				"RUB" : "Рубли РФ",
				"USD" : "Доллары США",
				"EUR" : "Евро",
				"AZN" : "Манаты"
			},

			"validateFieldErrors" : {
				"__undefined__" : "Введенные для поля данные некорректны"
			},
			
			"validateRecipientErrors" : {
				"__undefined__" : "Введенные для получателя данные некорректны"
			},
			
			"commission" : {
				"noCommission" : "Комиссия не взимается",
				"commisionMoreThan" : "При сумме более ${limitText} комиссия составит ",
				"commissionIs" : "Комиссия составляет ",
				"atLeast" : ", но не менее чем "
			},

			"paykiosk" : {
				"noTerminalNumber" : "не задан",
				"balanceFormat" : "Баланс ${getCurrency(getParameter('paykiosk.walletCurrency', 'RUR')).format(PaymentGateway.CustomerBalance)}",
				"changeSumFormat" : "Остаток ${getCurrency(getParameter('paykiosk.walletCurrency', 'RUR')).format(PaymentDevice.ChangeSum)}",
				"billAcceptorFailure" : "Терминал временно не работает. Неисправность купюроприемника",
				"printerFailure" : "Терминал временно не работает. Неисправность принтера.",
				"printerOfflineWarning" : "ПО техническим причинам квитанция не будет напечатана<br>Продолжить?<br>",
				"billDispenserFailure" : "Терминал временно не работает. Неисправность в устройстве выдачи купюр."
			},
			
			"initialize" : {
				"terminalInUpdate" : "Терминал обновляет информацию<br>Нажмите Назад для проведения платежа",
				"reload" : "Перезагрузка..."
			},
			
			"barcodeEntry" : {
			
				"states" : {
					"NotDefined" : "<b><font style=\"color:red\">Считайте штрих-код.</font></b><br>Считайте штрихкод. Ввод данных вручную не предусмотрен.",
					"Defined" : "<b><font style=\"color:red\">Вы можете считать еще штрихкод.</font></b><br>Считайте штрихкод или нажмите кнопку Далее для ввода данных вручную.",
					"NotRequired" : "<b><font style=\"color:red\">Вы можете считать штрихкод.</font></b><br>Считайте штрихкод или нажмите кнопку Далее для ввода данных вручную."
				},			
				
				"setBarcodeErrors" : {
					"1" : "Штрихкод неверной длины",
					"2" : "Неверный маркер штрихкода",
					"3" : "Штрихкод не прошел проверку",
					"4" : "Поля штрихкода не прошли проверку",
					"__undefined__" : "Неизвестная ошибка"
				},
				
				"barcodeReadSucceeded" : "Успешно считан штрих-код ${_.barcode}. Нажмите 'OK' для продолжения.",
				"barcodeReadFailed" : "<font style=\"color:red\">Считан неправильный штрих-код ${_.barcode}.</font><br><b>${_.setBarcodeErrorMessage}.</b><br>Нажмите 'OK' для продолжения.",

				"validateRecipientFailed" : "<b><font style=\"color:red\">${_.validateRecipientErrorMessage}.</font></b>",
				
				"allFieldsComplete" : "Все штрих-коды успешно считаны и можно продолжать. Проверьте заполнение полей:<br>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br>Вы хотите откорректировать поля вручную?"
			},
			
			"recipients" : {
				"unableToPay" : "В настоящий момент платеж по <br>«${PaymentGateway.SelectedCatalogItem.Name}»<br>по техническим причинам не может быть выполнен. Приносим свои извинения",
				"unableToPayPrinterOffline" : "В настоящий момент платеж по <br>«${PaymentGateway.SelectedCatalogItem.Name}»<br> не может быть выполнен, так как принтер неисправен. Приносим свои извинения",
				"descriptionOnSelect" : "Внимание!<br>${PaymentGateway.SelectedCatalogItem.Description}<br>Приступить к вводу данных?",
				"temporaryUnavailable" : "Раздел <br>${PaymentGateway.SelectedCatalogItem.Name}<br>временно недоступен",
				"information" : "Информация",
				"billAcceptorFailureAlert" : "В настоящий момент платеж по <br>«${PaymentGateway.SelectedCatalogItem.Name}»<br> не может быть выполнен, так как купюроприёмник неисправен. Приносим свои извинения"
			},
			"recipientsView" : {
				"paymentRulesLink" : "Выберите язык интерфейса",
				"groupStatusText" : "<table class=statustxt><tr><td><img width=${PaymentGateway.SelectedCatalogItem.Picture.Width} height=${PaymentGateway.SelectedCatalogItem.Picture.Height}  src='images-recipients/${getActiveLocale()}/${PaymentGateway.SelectedCatalogItem.Picture.URL}'></td><td>${PaymentGateway.SelectedCatalogItem.Name}</td></tr>",
				"pageOfThePage" : "Страница ${(_.currentPageNr + 1)} из ${(_.lastPageNr + 1)}"
			},
			
			"recipientsFixedsumCollection" : {
				"resetAll" : "Обнулить всё",
				"totalNumberStr" : "Общее количество:<div id='totalNumber'>${_.itemCount}</div>",
				"totalSumStr" : "Общая сумма:<div id='totalSum'>${getCurrency('RUR').format(0, 'sign')}</div>"
			},			
			
			"dataEntry" : {
				"errors" : {
					"appendCharErrorMessage" : "Ограничение на длину поля ${_.getCurrentField().Name} ${_.getCurrentField().Value.length} символов.<br>Вы не можете ввести больше",
					"barcodeAlreadyRead" : "Штрих-код уже был считан<br>Чтобы считать штрих-код заново нажмите Назад и снова выберите получателя услуги",
					"barcodeNotSupported" : "Ввод данных с помощью штрих кода у данного получателя не предусмотрен",
					"validateFieldFailed" : "Введенное поле не прошло проверку<br><font style='color=red'>${_.validateErrorMessage}</font><br>Убедитесь в правильности введенных данных",
					"validateCatalogItemFailed" : "Данные не прошли проверку<br><font style='color=red'>${_.validateCatalogItemErrorMessage}</font><br>Убедитесь в правильности введённой информации"
				},
								
				"pleaseSelect" : "Выберите ",
				"pleaseInput" : "Введите ",
				"pleaseConfirm" : "Подтвердите ",
				"notSelected" : "не выбрано",
				"notRequired" : "Поле вводить не обязательно!",
				"statusText" : "<table class=statustxt><tr><td><img width=${PaymentGateway.SelectedCatalogItem.Picture.Width} height=${PaymentGateway.SelectedCatalogItem.Picture.Height}  src='images-recipients/${getActiveLocale()}/${PaymentGateway.SelectedCatalogItem.Picture.URL}'></td><td>${getCurrentField().Name}</td></tr>"
			},
			
			"dataCheck" : {
				"dataValidationFailed" : "Ошибка проверки данных",
				"dataCheckInProgress" : "Идет проверка данных, пожалуйста, подождите",
				"checkResult" : {
					// checkFailed by default				
					"__undefined__" : "В данный момент платеж с параметрами:<br>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br><font style='color:#EF1515'>не может быть выполнен<br><br></font>",
					// Succeded
					"0" : "Проверка номера прошла успешно<br>Подтвердите присланные данные:<b>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br></b>",
					// NeedNextCheck
					"1" : "Проверка номера прошла успешно<br>Подтвердите присланные данные:<b>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br></b>, <font style='color:#EF1515'>Требуется ввод дополнительных данных</font><br>Вы согласны?",
					// checkFailed
					"-2" : "В данный момент платеж с параметрами:<br>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br><font style='color:#EF1515'>не может быть выполнен<br><br></font><b>Вы можете провести платеж нажав «Далее».</b> В этом случае деньги поступят на Ваш счет <b>через некоторое время</b>",
					// checkFailedCanPay
					"-4" : "В данный момент платеж с параметрами:<br>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br><font style='color:#EF1515'>не может быть выполнен<br><br></font><b>Вы можете провести платеж нажав «Далее».</b>"
				}
			},		
			
			"afterDataCheck" : {
				"changeOrders" : "Чтобы изменить заказ",
				"goToCassa" : "Не устраивает автокасса"
			},
			
			"select" : {
				"selectInProgress" : "Идет проверка данных, пожалуйста, подождите<br><br>Группа платежа: <b>${PaymentGateway.SelectedCatalogItem.Name}<br><br></b>${generatePaymentInfo(PaymentGateway.SelectedCatalogItem.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, true)}<br>",
//				"checkSucceeded" : "Исходя из введенных данных выбран получатель<br>${PaymentGateway.SelectedRecipient.Name}<br><br>${formatCatalogItem(PaymentGateway.SelectedRecipient)}",
				"selectSucceeded" : "Исходя из введенных данных выбран получатель<br>${PaymentGateway.SelectedCatalogItem.Name}",
				"groupSelected" : "Исходя из введенных данных выбрано несколько получателей<br>Сделайте выбор одного из них<br><br>",
				"incorrectSelected" : "Введенный ${generatePaymentInfo(PaymentGateway.SelectedCatalogItem.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)} не соответствует выбранной группе ${PaymentGateway.SelectedCatalogItem.Name}<br>Убедитесь в правильности введенных данных и выборе верной группы получателей",
				"incorrectSelectedUniBank" : "Это убрать, после исправления pgw тут бкдет код ошибки",
				"selectFailed" : "Ошибка проверки данных<br><br>${enumerateRecipientFields(PaymentGateway.SelectedCatalogItem.Fields, _.fieldValueTemplate, false)}<br>",
				"selectFailedUniBank" : "Ошибка проверки данных с параметрами:<br><br>${enumerateRecipientFields(PaymentGateway.SelectedCatalogItem.Fields, _.fieldValueTemplate, false)}<br>",
				"checkData" : "Проверьте правильность введенных данных",
				"areYouAgree" : "Вы согласны?",
				"selectValidationFailed" : "Ошибка проверки данных"
			},
			
			"selectPaymentMethod" : {
				"fixedSummWarning" : "Внимание!<br>Платеж на <b>фиксированную</b> сумму ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'short')}<br>"+
									"Остаток нужно будет зачислить на счет другого получателя.<br>Приступить к оплате?",
				"minSummWarning" : "Внимание!<br><b>Минимальная</b> сумма платежа ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'short')}<br>"+
									"Остаток будет зачислен на следующий период.<br>Приступить к оплате?",
				"card" : "Банковская карта",
				"cash" : "Наличные",
				"wallet" : "Кошелек",
				
				"noPaymentMethod" : "Для данного получателя ни один из способов оплаты недоступен",
				"selectPMTitle" : "Выбор типа оплаты"				
			},
			
			"acceptCashPayment" : {
				"unableToAccept" : "Оплата в настоящий момент невозможна<br>Попробуйте, пожалуйста, позже",
				"maxSummWarning" : "Максимальный платеж  ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'short')}<br>Вы можете зачислить средства совершив еще один платеж",
				"minSummWarning" : "Минимальный платеж  ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'short')}<br>Поэтому первая купюра должна быть не менее ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}",
				"rejectOnReason4" : "Для проведения платежа требуется ",
				"firstNoteMessage" : "<font class='colorText'> (первая купюра должна быть не менее ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}).</font>",
				
				"payLimitsMessageFirstNote" : "Внимание! Минимальная сумма платежа ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}"+
											"<font class='colorText'> (первая купюра должна быть не менее ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}).</font>"+
											"Максимальная сумма ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')} "+
											"При внесении суммы более ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')} платеж проведется автоматически",
											
				"payLimitsMessageNoChangeFirstNote" : "Внимание! Минимальная сумма платежа ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}"+
													 "<font class='colorText'> (первая купюра должна быть не менее ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}). </font>"+
													 "Максимальная сумма ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')}",

				"payLimitsMessage" : "Внимание! Минимальная сумма платежа ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')} "+
										"Максимальная сумма ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')} "+
										"При внесении суммы более ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')} платеж проведется автоматически",
										
				"payLimitsMessageNoChange" : "Внимание! Минимальная сумма платежа ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')} "+
				                              "Максимальная сумма ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')}",
				
				"maxSumm" : "Максимальный платеж  ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'short')}",
				"minSumm" : "Минимальный платеж  ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'short')}",
				"sumPresent" : "Накопленной суммы достаточно для проведения платежа<br>Провести платеж?",
				"fixedSummWarning" : "Внимание! Оплата на фиксированную сумму: ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'short')} Остаток средств с платежа нужно зачислить на счет другого получателя.",
				"recommendedSummWarning" : "Внимание! Рекомендуемая сумма платежа не менее ${getCurrency().format(PaymentGateway.SelectedRecipient.TransferSum, 'short')} Минимальная сумма платежа ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}",
				"freeSummWarning" : "Внимание! Минимальная сумма платежа ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}",
				"maxPaymentSum" : "Максимальная сумма ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')}",
				"automaticPayment" : "При внесении суммы более ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')} платеж проведется автоматически",
				"fixedMinSummWarning" : "Внимание! Сумма к оплате: ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'short')}. Вы должны внести сумму, не менее ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'short')} (например ${getCurrency().format(Math.ceil(PaymentGateway.SelectedRecipient.PaymentSum/10)*10, 'short')}). Сумма переплаты будет зачислена на ваш счет на следующий период. При внесении нужной суммы платеж проведется автоматически",
				"insertMoney" : "Внесите деньги по одной купюре",
				"paymentSumStr" : "К оплате:",
				"transferSumStr" : "К зачислению:",
				"commissionSumStr" : "Комиссия:",
				"accumulatedSumStr" : "Принято:",
				"remainSumStr" : "Осталось внести",
				"toGoRecipients" : "Для выбора другого получателя - нажмите «Назад»<br>",
				"continueOrPayFree" : "Для внесения денег - нажмите «Внести купюры»<br>Для проведения платежа на ${getCurrency().format(PaymentDevice.AccumulatedSum+PaymentDevice.ChangeSum, 'short')} - нажмите «Провести платеж»\
<br>Если вы не сделаете выбор платеж проведется через ${Math.round(getParameter('acceptCash.waitTimeout', 60000)/2000)} секунд",
				"sumLeftFixed" : "Вам осталось внести ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum - PaymentDevice.AccumulatedSum - PaymentDevice.ChangeSum)}<br>"
				
			},
			
			"acceptCardPaymentUnibank" : {
				"confirmSum" : "Подтвердите сумму операции"
			},
			"acceptWalletPayment" : {
				"statusSumEntry" : "Введите сумму платежа (баланс ${getCurrency().format(PaymentGateway.CustomerBalance)}):",
				"statusSumLeft" : "Оставшаяся сумма:",
				"statusRecipient" : "Введите сумму для платежа на счет ${PaymentGateway.SelectedRecipient.Name}",
				"maxBalanceLimitReached" : "Вы не можете выбрать сумму больше ${getCurrency().format( maxLimit() )}"
			},
			
			"acceptPaymentManual" : {
				"partyPayment" : "Разделение суммы:",
				"sumLeft" : "Оставшаяся сумма:",
				"maxBalanceLimitReached" : "Вы не можете выбрать сумму больше ${getCurrency().format( maxLimit() )}"
			},
			
			"sumEntry" : {
				"status" : "Ввод суммы платежа",
				"fixedSumRecipient" : "Вы собираетесь оплатить получателя с фиксированной суммой платежа:<br><br><b>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br><br>Сумма к списанию: ${getCurrency().format(_.recipient.PaymentSum)}</b><br><br>Вы согласны?",
				"statusRecipient" : "Введите сумму для платежа на счет ${PaymentGateway.SelectedRecipient.Name}",
				"maxBalanceLimitReached" : "Вы не можете выбрать сумму больше ${getCurrency().format( maxLimit() )}"
			},			
			
			"payment" : {
				"paymentInProgress" : "Идет проведение платежа, пожалуйста, подождите....",
				"statusForCollectionCase" : "Процесс оплаты коллекции получателей",
				"tableHeader" : "Процесс оплаты коллекции:",
				"payOk" : "Успешно",
				"payFail" : "Платёж не прошёл",
				"payDeferred" : "Платёж отложен"				
			},
			
			"dispense" : {
				"deviceError" : "Сумма ${getCurrency().format(_.changeSum)} не может быть выдана из за ошибки устройства.<br>Сдачу вы можете зачислить на счет другого получателя или обратитесь к администратору",
				"pleaseWait" : "Ждите выдачи денег не отходя от терминала",
				"alertMessage" : "Ждите выдачи средств не отходя от терминала<br>К выдаче:<br> ${getCurrency().format(_.changeSum)}",
				"noRequiredSum" : "Сумма ${getCurrency().format(_.changeSum)} не может быть выдана из за отсутствия необходимого количество купюр для выдачи сдачи<br>Обратитесь к администратору",
				"dispenseError" : "Ошибка выдачи суммы ${getCurrency().format(_.changeSum)}<br>(${_.errorText})<br>Возьмите выданную квитанцию<br>Обратитесь к администратору",
				"dispenseSuccess" : "Сумма ${getCurrency().format(_.changeSum)} выдана успешно<br><b><font color='red'>Заберите купюры в лотке</font></b>",
				"dispenseErrorAfter" : "Произошла ошибка выдачи купюр<br>Возьмите выданную квитанцию<br>Невыданная сумма ${getCurrency().format(_.unDispensedSum)}<br>будет возвращена на Ваш счет",
				"dispenseErrorAfterUtinet" : "Произошла ошибка выдачи купюр<br>Возьмите выданную квитанцию<br>Невыданная сумма ${getCurrency().format(_.unDispensedSum)}<br>будет возвращена. Обратитесь к менеджеру."
			},
			
			"insertCard" : {
				"insertCardTitle" : "Ввод карты клиента",
				"pleaseInsert" : "Вставьте Вашу карту",
				"takeYourCard" : "Заберите Вашу карту",
				"cardReaderFailure" : "В данный момент операция невозможна по причине сбоя оборудования",
				
				"InsertCard_Text2" : "Карта не может быть считана",
				"InsertCard_Text3" : "Возможно, карта вставлена не той стороной",
				"InsertCard_Text5" : "Окончился срок действия карты",
				"InsertCard_Text6" : "Карта не обслуживается",
				"InsertCard_Text9" : "Пожалуйста подождите...",

				"insertCardErrors" : {
					"1" : "Карта вставлена не той стороной или не может быть считана",
					"2" : "Истёк срок действия карты, обратитесь в ваш банк",
					"3" : "Карта не обслуживается, обратитесь в ваш банк",
					"__undefined__" : "Карта не обслуживается"
				}		
			},
			"cardEject" : {
				"takeYourCard" : "Заберите Вашу карту",
				"captureTimeout" : "В случае, если карта не будет извлечена в течение&nbsp;<span id=\"captureTick\">${getParameter('card.captureTimeout', 30)}</span>&nbsp;секунд карта будет изъята терминалом",
				"cardCapturing" : "Производится изъятие карты...."
			},
			
			"cardCaptured" : {
				"message" : "Карта изъята, обратитесь по телефону:",
				"status" : "Изъята карта",				
				"servicePhone" : "+* (***) ***-**-**"
			},			
			
			"pinEntry" : { 
				"pinEntryTitle" : "Ввод ПИН-кода карты",
				"pinEntryHint" : "Введите ПИН-код",
				"pinCancelled" : "Ввод ПИН-кода отменен"
			},
			"verifyCard" : {
				"genericError" : "Ошибка проверки данных",
				"cardVerifyHint": "Идет проверка карты, пожалуйста, подождите....",
				"verifyError" : "Ошибка авторизации карты на сервере банка",
				"backToReturn" : "Нажмите «Назад» для возврата карты",
				"cardVerifyTitle": "Проверка данных на сервере банка"
			},

			"printInfo" : {
				"printerOffline" : "В данное время печать невозможна, приносим свои извинения",
				"terminalNr" : "Терминал № ",
				"receiptNr" : "  Квитанция № ",
				"terminalAddress" : "Адрес: ",
				"date" : "Дата: ",
				"time" : "  Время: ",
				"thanks" : "СПАСИБО ЗА СОТРУДНИЧЕСТВО!",
				"serviceLine" : "Телефон горячей линии: ",
				"paymentSum" : "Сумма: ",
				"changeSum" : "Сдача: ",
				"commissionSum" : "Комиссия: ",
				"transferSum" : "К зачислению: "
			},
			"printCoupon" : {
				"printDone" : "Возьмите Вашу квитанцию",
				"printFailed" : "Квитанция не может быть напечатана из-за технической неисправности"
			},
			
			"serviceMenu" : {
				"reports" : "Журналы",
				"devices" : "Устройства",
				"setup" : "Настройка",
				"exit" : "Выход",
				"payments" : "Платежи",
				"bills" : "Купюры",
				"billsDispense" : "Выдача купюр",
				"cardCapture" : "Захват карт",
				"printer" :"Принтер",
				"fiscal" : "Фискальный Регистратор",
				"billacceptor" : "Купюроприемник",
				"dispenser" : "Диспенсер купюр",
				"payKioskExit" : "'Выход из PayBox'",
				"onScreenKeyboard" : "'Экранная клавиатура'",
				"runExplorer" : "'Запустить Explorer'",
				"removeWinLocks" : "'Снять блокировки Windows'",
				"withDesktop" : "'С рабочим столом'",
				"withoutDesktop" : "'Без рабочего стола'",
				"keyboardComWarning" : "Внимание! При перезапуске PayKiosk с запущенной экранной клавиатурой порт COM1 будет занят экранной клавиатурой!",
				"property" : "Свойство",
				"status" : "Состояние",
				"interface" : "Интерфейс",
				"receiptPrinterPresent" : "${ReceiptPrinter ? \"Найден\" : \"Не найден\"}",
				"authFailed" : "Неверный код",
				"authorize" : "Авторизовать",
				"clear" : "Очистить",
				"encashment" : "Инкассация",
				"unablePrintReceipt" : "Квитанции распечатаны не будут. Принтер неисправен.",
				"printJournalAgain" : "Повторная печать журналов",
				"billAcceptor" : "Купюроприемник",
				"coinAcceptor" : "Монетоприемник",
				"billDispenser" : "Выдача купюр",
				"hoppers" : "Выдача монет",
				"enterStampNumber" : "Введите номер пломбы устанавливаемой (пустой) кассеты",
				"skipDevice" : "Пропустить"
				
			},
			
			"offline" : {
				"status" : "Терминал временно не работает",			
				"message" : "Терминал временно не работает<br>Терминал переведен в режим Offline"
			},		

			"points" : {
				"atm" : [
					{
						"city" : "Баку",
						"address" : [
							"г.Баку, ул. Р.Бейбудова 55 (Uniplaza)Центральный офис",
							"г.Баку, ул. Р.Бейбудова 55 (Uniplaza)Центральный офис",
							"г.Баку, ул.28 Мая",
							"г.Баку, Хагани 30/35 «Таир-97» маркет",
							"г.Баку, ул.С.Вургуна (у входа «Тязе Базар»)",
							"г.Баку, пр. Азербаджан 42A (Шахматный клуб)",
							"г.Баку, ул. М.Хади 2944 («Soliton» маркет)",
							"г.Баку, станция метро «Низами»",
							"г.Баку, пр Бюль-Бюль 26, (туристическое агентство «Intur», напротив AFF MALL)",
							"г.Баку, пр.Азадлыг, у входа в «Прогресс-маркет»",
							"г.Баку, Аквапарк «Новханы»",
							"г.Баку, пос. Новханы (Мурад маркет)",
							"г.Баку, пр-кт. Бюль-Бюля 7 (торговый центр «Сахиль»)",
							"г.Баку, станция метро «Ичяри Шяхер»",
							"г.Баку, ул. Зардаби 243 (маркет «Три Короны»)",
							"г.Баку, станция метро «Эльмляр Академиясы»",
							"г.Баку ул. Ататюрка 41, возле магазина «COLINS»",
							"г.Баку, ул. Гасана Алиева 88, возле универмага «Гянджлик»",
							"г.Баку, ул. Г.Гараева 66, филиал «Нефтчиляр»",
							"г.Баку, ул.Г.Джавида 21, (в аптеке напротив телеканала «SPACE»)",
							"г.Баку, ул.Д.Алиевой 243, (филиал «STB»)",
							"г.Баку, пр-т Бабека 2199, 14-ый филиал",
							"г.Баку, 5 мкрн, ул. 20-го Января 4/66, Пункт обмена валюты",
							"г.Баку, ул.Низами 66, авиакасса компании «Imair»",
							"г.Баку, ул. Шарифзаде, рядом со станцией метро «Иншаатчылар» (13-ый филиал)",
							"г.Баку, ул.Гасана Алиева 5а, 12-ый филиал",
							"г.Баку, ул.Дж.Джаббарлы 40 (Каспиан Бизнес Центр)",
							"г.Баку, пр-т Ходжалы 20/22 (Филиал STB)",
							"г.Баку, ул.Тябриз 95 (выход станции метро «Нариманов»)",
							"г.Баку, проспект Нефтяников 14 (9-ый Филиал)",
							"г.Баку, 8 мкрн., ул.Дж.Хандан 40А (8-ой филиал)",
							"г.Баку, ул.М.Хади 33(10-ый филиал)",
							"г.Баку проспект Хятаи 39(отель AYF)",
							"г.Баку ул.У.Гаджибекова 33/35 (позади Дома правительства)",
							"г.Баку, ул.С.Вургуна 19 (магазин Citimart)",
							"г.Баку, ул.Тебриз 85 (Наримановский филиал)",
							"г.Баку, 5 мкрн, ул.Тагизаде 22А (филиал «Аджеми»)",
							"г.Баку, AZ1132, Сабунчинский р-н, посёлок Бакиханов, ул. Сульх 14/2 (19-ый филиал)",
							"г.Баку, пос.Расулзаде, проспект Азадлыг 157 (Торговый центр)",
							"г.Баку, посёлок Мярдакяны, ул.С.Есенина 80 (по дороге на море)",
							"г.Баку, ул.Бакиханова 30, (магазин «Mobile»)",
							"г.Баку, проспект Азадлыг 116 (Аптека, на пересечении с улицей Гасана Алиева)",
							"г.Баку, 8 мкрн., ул.Д.Хандан 36 (Чудо-печка)",
							"г.Баку, Патамдартcкое шоссе 296 («Citimart» маркет)",
							"г.Баку, ул.Хагани 1 (Торговый Центр Хагани)",
							"г.Баку, ул.Ислама Сафарли 3 (кинотеатр «АРАЗ»)",
							"г.Баку, ул.Шарифзаде 73 (магазин «GENTLEMAN»)",
							"г.Баку, ул.Низами 68 (магазин «SONY)",
							"г.Баку, ул.Б.Багировой 2 (Ясамальский филиал)",
							"г.Баку, проспект Азадлыг 23 (напротив Нефтяной Академии)",
							"г.Баку, проспект Бюль-Бюль 10 (17-ый филиал, возле парка «Сахиль»)",
							"г.Баку, улица Истиглалият 51 (кафе «Monolit»)",
							"г.Баку, улица М.Расулзадэ 6 (возле кинотеатра «Азербайджан»)",
							"г.Баку, проспект Нефтяников, 93"
						]
					},
					
					{
						"city" : "Лянкяран",
						"address" : [
							"г.Лянкяран, ул.З. Алиевой 12 (Лянкяранский филиал)"
						]
					},
					
					{
						"city" : "Губа",
						"address" : [
							"г.Губа, пересечение улиц Г.Алиева и Т.Аббасова (Губинский филиал)"
						]
					},
					
					{
						"city" : "Шеки",
						"address" : [
							"г.Шеки, проспект М.Расулзаде 160А (Шекинский филиал)"
						]
					},
					
					{
						"city" : "Сумгаит",
						"address" : [
							"г.Сумгаит, ул.Сюльх 11/1, 30-й квартал (Сумгаитский филиал)"
						]
					},
					
					{
						"city" : "Мингячевир",
						"address" : [
							"г.Мингячевир, пр.Г.Алиева 47/2 (Мингячевирский филиал)"
						]
					},
					
					{
						"city" : "Гянджа",
						"address" : [
							"г.Гянджа, улица Ш.Гусейнова 2 (Гянджинское отделение)",
							"г.Гянджа, проспект Ататюрка 249 (Гянджинский филиал)"
						]
					},
					
					{
						"city" : "Шамкир",
						"address" : [
							"г.Шямкир, ул.20 Января, 1 (Шямкирский филиал)"
						]
					}
					
			],
			
			"office" : [
				{
						"city" : "Баку",
						"info" : [
							{
								"name" : "Центральный офис",
								"address" : "ул. Р.Бейбудова 55, Баку, AZ1022, Тел.: (+994 12) 498 22 44, Факс: (+994 12) 498 09 53",
								"worktime" : "Пн — пт 9:00–18:00 Сб. 10:00–16:00",
								"service" : "Банковские услуги частным и корпоративным клиентам."
							},	
							{
								"name" : "Филиал «STB»",
								"address" : "пр. Ходжалы 20/22 г. Баку, Xaтаинский р-н, AZ 1025, Тел: (+994 12) 498 22 44 Факс: (+994 12) 490 65 63",
								"worktime" : "Пн — пт 9:00–18:00",
								"service" : "Банковские услуги частным и корпоративным клиентам."
							},
							{
								"name" : "Филиал «Ясамал»",
								"address" : "ул. Бясти Багировой 2, Баку, AZ1065, Тел.: (+994 12) 498 22 44 Факс: (+994 12) 493 36 75",
								"worktime" : "Пн. — пт. 9:00–18:00 Сб. 10:00–16:00",
								"service" : "Банковские услуги частным и корпоративным клиентам."
							},
							{
								"name" : "Филиал «Нефтчиляр»",
								"address" : "пр. Кара Караева, 66, Баку, Хатаинский р-н., AZ1096, Тел.: (+994 12) 498 22 44 Факс: (+994 12) 422 62 44",
								"worktime" : "Пн. — пт. 9:00–18:00 Сб. 10:00–16:00",
								"service" : "Банковские услуги частным и корпоративным клиентам."
							},
							{
								"name" : "Филиал «Нариманов»",
								"address" : "ул. Табриз 85, г. Баку, AZ 1072 Тел.: (+994 12) 498 22 44 Факс: (+994 12) 567 65 72",
								"worktime" : "Пн. — пт. 9:00–18:00 Сб. 10:00–16:00",
								"service" : "Банковские услуги частным и корпоративным клиентам."
							},
							{
								"name" : "Филиал «Аджеми»",
								"address" : "ул. Тагизаде 22 (А), г. Баку, 5-ый мкр., AZ 1102, Тел.: (+994 12) 498 22 44 Факс: (+994 12) 430 56 21",
								"worktime" : "Пн. — пт. 9:00–18:00",
								"service" : "Банковские услуги частным и корпоративным клиентам."
							},
							{
								"name" : "8-ой филиал",
								"address" : "ул. Дж.Хандан 40 (А), г. Баку, AZ 1130, Тел: (+994 12) 498 22 44 Факс: (+994 12) 436 50 75",
								"worktime" : "Пн. — пт. 9:00–18:00",
								"service" : "Банковские услуги частным и корпоративным клиентам."
							},	
							{
								"name" : "9-ой филиал",
								"address" : "пр. Нефтяников 14, г. Баку, AZ 1003, Тел: (+994 12) 498 22 44 Факс: (+994 12) 491 73 68",
								"worktime" : "Пн. — пт. 9:00–18:00",
								"service" : "Банковские услуги частным и корпоративным клиентам."
							},
							{
								"name" : "10-ой филиал",
								"address" : "ул. М.Хади 33, г. Баку, AZ 1129,  Тел: (+994 12) 498 22 44 Факс: (+994 12) 470 56 67",
								"worktime" : "Пн. — пт. 9:00–21:00 Сб. 10:00–16:00",
								"service" : "Банковские услуги частным и корпоративным клиентам."
							},	
							{
								"name" : "17-ый (Сахиль) филиал",
								"address" : "пр. Бюль-Бюль 10, Баку, Сабаилский р-н, AZ 1022  Тел.: (+994 12) 498 22 44 Факс: (+994 12) 596 51 03",
								"worktime" : "Пн. — пт. 9:00–18:00",
								"service" : "Банковские услуги частным и юридическим лицам."
							},
							{
								"name" : "12-ый филиал",
								"address" : "ул. Г. Алиева д. 5a, Баку, Насиминский р-н,AZ 1110 Тел.: (+994 12) 498 22 44 Факс: (+994 12) 441 16 04",
								"worktime" : "Пн. — пт. 9:00–10:00",
								"service" : "Банковские услуги частным и корпоративным клиентам"
							},	
							{
								"name" : "13-ый филиал",
								"address" : "ул. Шярифзаде, Баку Ясамальский р-н, AZ 1138 Тел.: (+994 12) 498 22 44 Факс: (+994 12) 434 51 94",
								"worktime" : "Пн. — пт. 9:00–10:00",
								"service" : "Банковские услуги частным и корпоративным клиентам"
							},
							{
								"name" : "14-ый филиал",
								"address" : "пр. Бабека, двор 2199, Баку, Хатаинский р-н,AZ1008 Тел.: (+994 12) 498 22 44 Факс: (+994 12) 514 78 93",
								"worktime" : "Пн. — пт. 9:00–18:00",
								"service" : "Банковские услуги частным и корпоративным клиентам"
							},
							{
								"name" : "15-ый филиал",
								"address" : "ул. Д.Алиевой, д. 243, Насиминский р-н, Баку, AZ 1010 Tel.: (+994 12) 498 22 44 Fax: (+994 12) 598 40 09",
								"worktime" : "Пн.- Пт.  9:00 - 18:00 Сб. 10:00 - 16:00",
								"service" : "Банковские услуги частным и корпоративным клиентам"
							},
							{
								"name" : "16-ый филиал",
								"address" : "ул. Б.Сардарова 128, кв. 16 Баку, Сабаильский р-н, AZ1001 Тел.: (+994 12) 498 22 44 Факс: (+994 12) 437 32 80",
								"worktime" : "Пн.- Пт.  9:00 - 18:00",
								"service" : "Банковские услуги частным и корпоративным клиентам"
							},
							{
								"name" : "19-ый филиал (Бакиханова)",
								"address" : "ул. Сульх 14/2, г Баку, пос. Бакиханова, AZ 1132, Тел.: (+994 12) 498 22 44 Факс: (+994 12) 429 60 54",
								"worktime" : "Пн.- Пт.  9:00 - 18:00",
								"service" : "Банковские услуги частным и корпоративным клиентам"
							},								{
								"name" : "Отделение обмена валют №1",
								"address" : "ул. Тябриза Халилбейли 5, Баку, Сабиальский р-н., Тел.: (+994 12) 498 22 44",
								"worktime" : "Пн.- Пт.  9:00 - 18:00 Сб. 10:00 - 16:00",
								"service" : "Валютно-обменные операции."
							}								
							
					]
				},

				{
					"city" : "Губа",
						"info" : [
							{
								"name" : "Филиал «Губа»",
								"address" : "пр. Гейдара Алиева 225, г. Губа, AZ 4002, Тел.: (+994 23) 335 28 30 Факс: (+994 23) 335 37 10",
								"worktime" : "Пн — пт 9:00–18:00 Сб. 10:00–16:00",
								"service" : "Банковские услуги частным и корпоративным клиентам."
							}
						]	

				},
				
				{
					"city" : "Шеки",
						"info" : [
							{
								"name" : "Филиал «Шеки»",
								"address" : "проспект М.Э.Расулзаде 160A г. Шеки, AZ 5500 Тел.: (+994 24) 244 58 48 Факс: (+994 24) 244 55 29",
								"worktime" : "Пн — пт 9:00–18:00 Сб. 10:00–16:00",
								"service" : "Банковские услуги частным и корпоративным клиентам."
							}
						]

				},	
				
				{
					"city" : "Лянкяран",
						"info" : [
							{
								"name" : "Филиал «Лянкяран»",
								"address" : "ул. З. Алиевой 12, г. Лянкяран, AZ4200 Тел.: (+994 25) 255 15 39 Факс: (+994 25) 255 17 34",
								"worktime" : "Пн — пт 9:00–18:00 Сб. 10:00–16:00",
								"service" : "Все виды банковских услуг для частных и корпоративных клиентов."
							}
						]

				},				
				{
					"city" : "Шамкир",
						"info" : [
							{
								"name" : "Филиал «Шамкир»",
								"address" : "ул. 20 Января, 1 г. Шамкир, AZ5700 Тел.: (+994 22) 305 3600 Факс: (+994 22) 302 35 65",
								"worktime" : "Пн — пт 9:00–18:00 Сб. 10:00–16:00",
								"service" : "Банковские услуги частным и корпоративным клиентам."
							}
						]

				},	
				{
					"city" : "Мингячевир",
						"info" : [
							{
								"name" : "Филиал «Мингячевир»",
								"address" : "пр. Г.Алиева 47/2, г. Мингячевир, AZ 4500 Тел.: (+994 24) 275 88 92 Факс: (+994 24) 275 52 33",
								"worktime" : "Пн — пт 9:00–18:00 Сб. 10:00–18:00",
								"service" : "Банковские услуги частным и корпоративным клиентам."
							}
						]

				},	
				{
					"city" : "Гянджа",
						"info" : [
							{
								"name" : "Филиал «Гянджа»",
								"address" : "пр. Ататюрка 249, г. Гянджа, AZ2003, Тел.: (+994 22) 256 95 99 Факс: (+994 22) 256 77 64",
								"worktime" : "Пн — пт 9:00–18:00 Сб. 10:00–16:00",
								"service" : "Банковские услуги частным и корпоративным клиентам."
							},

							{
								"name" : "Отделение Гянджа",
								"address" : "Гянджа,  AZ 2011 Кяпезский р-н,  улица Ш. Гусейнова, 2 Тел/Факс: (+994 22) 257 18 74",
								"worktime" : "Пн — пт 9:00–18:00 Сб. 10:00–16:00",
								"service" : "Обслуживание физических лиц."
							}
						]

				},
				{
					"city" : "Сумгаит",
						"info" : [
							{
								"name" : "Филиал «Сумгаит»",
								"address" : "ул. Сулх 11/1, 30-й квартал, г. Сумгаит, AZ5000, Тел.: (+994 18) 642 11 44 Факс: (+994 18) 642 62 35",
								"worktime" : "Пн — пт 9:00–18:00 Сб. 10:00–16:00",
								"service" : "Банковский услуги частным и корпоративным клиентам."
							}
						]

				}			

			]
		}
	},		

		{
			// number должно быть целым числом		
			"getNumberName" : function(number, names) {
				// склоняемое название
				var n = number % 100;
				if ((n > 4) && (n < 21)) return names[2];
				
				n %= 10;
				if ((n === 0) || (n > 4)) return names[2];
				if (n === 1) return names[0];
				return names[1];
			},
			
			"currency" : {
				"RUR" : {
					"MajorUnit" : ["рубль", "рубля", "рублей"],
					"MinorUnit" : ["копейка", "копейки", "копеек"],
					"MajorUnitShort" : "руб.",
					"MinorUnitShort" : "коп.",
					"MajorUnitSign" : "р.",
					"MinorUnitSign" : "к."
				},
				"RUB" : {
					"MajorUnit" : ["рубль", "рубля", "рублей"],
					"MinorUnit" : ["копейка", "копейки", "копеек"],
					"MajorUnitShort" : "руб.",
					"MinorUnitShort" : "коп.",
					"MajorUnitSign" : "р.",
					"MinorUnitSign" : "к."
				},
				"AZN" : {
					"MajorUnit" : ["манат", "маната", "манат"],
					"MinorUnit" : ["гяпик", "гяпика", "гяпиков"],
					"MajorUnitShort" : "ман.",
					"MinorUnitShort" : "гяп.",
					"MajorUnitSign" : "м.",
					"MinorUnitSign" : "г."
				},
				"USD" : {
					"MajorUnit" : ["USD", "USD", "USD"],
					"MinorUnit" : ["cent", "cent", "cent"],
					"MajorUnitShort" : "$",
					"MinorUnitShort" : "c.",
					"MajorUnitSign" : "$",
					"MinorUnitSign" : ""
				}
			}
		}
);