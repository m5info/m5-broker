﻿addLocale(
		"ru",
		{
			"currency" : {
				"RUR" : "Рубли РФ",
				"RUB" : "Рубли РФ",
				"USD" : "Доллары США",
				"EUR" : "Евро",
				"AZN" : "Манаты",
				"PLZ" : "Польские Злотые"
			},
			
			"validateFieldErrors" : {
				"__undefined__" : "Введенные для поля данные некорректны"
			},
			
			"validateRecipientErrors" : {
				"__undefined__" : "Введенные для получателя данные некорректны"
			},
			
			"commission" : {
				"noCommission" : "Комиссия не взимается",
				"commisionMoreThan" : "При сумме более ${limitText} комиссия составит ",
				"commissionIs" : "Комиссия составляет ",
				"atLeast" : ", но не менее чем "
			},

			"paykiosk" : {
				"noTerminalNumber" : "не задан",
				"balanceFormat" : "Баланс ${getCurrency(getParameter('paykiosk.walletCurrency', 'RUR')).format(PaymentGateway.CustomerBalance)}",
				"changeSumFormat" : "Остаток ${getCurrency('RUB').format(Framework.Variables('changeSum'))}",
				"billAcceptorFailure" : "Терминал временно не работает. Неисправность купюроприемника",
				"printerFailure" : "Терминал временно не работает. Неисправность принтера.",
				"printerOfflineWarning" : "По техническим причинам квитанция не будет напечатана<br>Продолжить?<br>",
				"billDispenserFailure" : "Терминал временно не работает. Неисправность в устройстве выдачи купюр."
			},
			
			"VirtuPOS" : {
				"executingVirtuPOSMethod" : "<br>Выполняется запрос на сервер<br>Пожалуйста, подождите....<br><br><img src='images/clock.png'>",
				"tryVirtuPOSMethodAgain" : "<br><br><b>Система занята. Подготовка к повторной попытке...</b><br><br>"
			},
			
			"deviceStatus" : {
				"Printer" : {
					"dssUndefined" : "Не определен",
					"dssDisabled_Initialize" : "Отключен",
					"dssIdle_Normal" : "Работает",
					"dssIdle_Suspended" : "Приостановлено",
					"dssIdle_PaperNearEnd" : "Сработал датчик близкого конца бумаги",
					"dssBusy_Printing" : "Идет печать",
					"dssBusy_Holding" : "Удержание чека",
					"dssError_NoDevice" : "Устройство не найдено",
					"dssError_DeviceFailure" : "Сбой в работе устройства",
					"dssError_PaperOut"	: "Нет бумаги или неверно настроены направляющие"	
				},
				"billAcceptor" : {
					"dssUndefined" : "Не определен",
					"dssDisabled_PowerUp" : "Отключён, загружается",
					"dssDisabled_Initialize" : "Работает",
					"dssDisabled_Inhibit" : "Запрещен",
					"dssBusy_Accepting" : "Идет прием",
					"dssBusy_RejectingInhibit" : "Запрет возврата купюры",
					"dssBusy_Rejecting" : "Возврат купюры",
					"dssBusy_Escrow" : "Прием купюры",
					"dssBusy_Stacking" : "Укладка купюры",
					"dssBusy_Stacked" : "Уложил купюру",
					"dssBusy_Returning" : "Возвращаю купюру",
					"dssBusy_Returned" : "Возвратил купюру",
					"dssBusy_Paused" : "Пауза",
					"dssBusy_Holding" : "Удержание купюры",
					"dssBusy_dssDispensing" : "Занят, выдача купюр",
					"dssBusy_dssDispensed" : "Занят, купюра выдана",
					"dssBusy_dssDispensedComplete" : "Все купюры выданы",
					"dssBusy_dssUnloading" : "Возврат купюры",
					"dssBusy_dssUnloaded" : "Купюра возвращена",
					"dssBusy_Packed" : "Упакована",
					"dssError_NoDevice" : "Устройство не найдено",
					"dssError_DeviceFailure" : "Сбой в работе устройства",
					"dssError_StackerFull" : "Кассета переполнена!",
					"dssError_ValidatorJammed" : "Замята купюра в приемнике!",
					"dssError_StackerJammed" : "Замята купюра в кассете!",
					"dssError_StackerRemoved" : "Зачем то сняли стекер!",
					"dssIdle_Normal" : "Работает",
					"dssIdle_Cassette" : "Работает",
					"dssIdle_CassetteNormal" : "Работает, кассета в порядке",
					"dssIdle_CassetteNearEnd" : "Кассета почти опустела",
					"dssIdle_CassetteNotPresent" : "Работает, кассета отсутствует",
					"dssIdle_CassetteInvalid" : "Кассета неисправна",
					"dssError_CassetteNotPresent" : "Ошибка, кассета отсутствует",
					"dssIdle_Sensor" : "Работает сенсор",
					"dssIdle_Suspended" : "Приостановлено",
					"dssError_ShutterFailure" : "Шторка сломалась",
					"dssError_PoolFailure" : "Ошибка пула",
					"dssError_PotentiometerFailure" : "Ошибка потенциометра"				
				},
				"VirtuPOS" : {
					"available" : "Доступен",
					"unavailable" : "Недоступен"					
				}
			},
			
			"start" : {
					
				"printerName" : "Чековый принтер",
				"billacceptorName" : "Купюроприёмник",				
				"virtuposName" : "VirtuPOS",
				
				"devName" : "Имя устройства",
				"devState" : "Состояние"				
			
			},
			
			"initialize" : {
				"terminalInUpdate" : "Терминал обновляет информацию<br>Нажмите Назад для проведения платежа",
				"reload" : "Перезагрузка..."
			},
			
			"barcodeEntry" : {
				
				"states" : {
					"NotDefined" : "<b><font style=\"color:red\">Считайте штрих-код.</font></b><br>Считайте штрихкод. Ввод данных вручную не предусмотрен.",
					"Defined" : "<b><font style=\"color:red\">Вы можете считать еще штрихкод.</font></b><br>Считайте штрихкод или нажмите кнопку Далее для ввода данных вручную.",
					"NotRequired" : "<b><font style=\"color:red\">Вы можете считать штрихкод.</font></b><br>Считайте штрихкод или нажмите кнопку Далее для ввода данных вручную."
				},
				
				"setBarcodeErrors" : {
					"1" : "Штрихкод неверной длины",
					"2" : "Неверный маркер штрихкода",
					"3" : "Штрихкод не прошел проверку",
					"4" : "Поля штрихкода не прошли проверку",
					"__undefined__" : "Неизвестная ошибка"
				},
				
				"barcodeReadSucceeded" : "Успешно считан штрих-код ${_.barcode}. Нажмите 'OK' для продолжения.",
				"barcodeReadFailed" : "<font style=\"color:red\">Считан неправильный штрих-код ${_.barcode}.</font><br><b>${_.setBarcodeErrorMessage}.</b><br>Нажмите 'OK' для продолжения.",

				"validateRecipientFailed" : "<b><font style=\"color:red\">${_.validateRecipientErrorMessage}.</font></b>",
				
				"allFieldsComplete" : "Все штрих-коды успешно считаны и можно продолжать. Проверьте заполнение полей:<br>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br>Вы хотите откорректировать поля вручную?"
			},
			
			"recipients" : {
				"unableToPay" : "В настоящий момент платеж <br>«${PaymentGateway.SelectedCatalogItem.Name}»<br>по техническим причинам не может быть выполнен. Приносим свои извинения",
				"unableToPayPrinterOffline" : "В настоящий момент платеж по <br>«${PaymentGateway.SelectedCatalogItem.Name}»<br> не может быть выполнен, так как принтер неисправен. Приносим свои извинения",
				"descriptionOnSelect" : "Внимание!<br>${PaymentGateway.SelectedCatalogItem.Description}<br>Приступить к вводу данных?",
				"temporaryUnavailable" : "Раздел <br>${PaymentGateway.SelectedCatalogItem.Name}<br>временно недоступен",
				"information" : "Информация",
				"billAcceptorFailureAlert" : "В настоящий момент платеж по <br>«${PaymentGateway.SelectedCatalogItem.Name}»<br> не может быть выполнен, так как купюроприёмник неисправен. Приносим свои извинения"
			},

			"recipientsView" : {
				"rootStatusText" : "Выберите операцию",
				"paymentRulesLink" : "Выберите язык интерфейса",
				"groupStatusText" : "<table class=statustxt><tr><td>${PaymentGateway.SelectedCatalogItem.Name}</td></tr>",
//				"groupStatusText" : "${PaymentGateway.SelectedCatalogItem.Name}",
				"pageOfThePage" : "Страница ${(_.currentPageNr + 1)} из ${(_.lastPageNr + 1)}",
				"insertCardErrors" : {
					"1" : "Карта вставлена не той стороной или не может быть считана",
					"2" : "Истёк срок действия карты, обратитесь в ваш банк",
					"3" : "Карта не обслуживается, обратитесь в ваш банк",
					"10" : "Операция отменена пользователем",
					"11" : "Карта не обслуживается",
					"__undefined__" : "Карта не обслуживается"
				},
				"takeYourCard" : "Заберите Вашу карту",
				"captureTimeout" : "В случае, если карта не будет извлечена в течение&nbsp;<span id=\"captureTick\">${getParameter('card.captureTimeout', 30)}</span>&nbsp;секунд карта будет изъята терминалом",
				"cardCapturing" : "Производится изъятие карты....",
				"startingApplication" : "Пожалуйста, подождите...."
			},
			
			"recipientsFixedsumCollection" : {
				"resetAll" : "Обнулить всё",
				"totalNumberStr" : "Общее количество:<div id='totalNumber'>${_.itemCount}</div>",
				"totalSumStr" : "Общая сумма:<div id='totalSum'>${getCurrency('RUR').format(0, 'sign')}</div>"
			},
			
			"dataEntry" : {
				"errors" : {
					"appendCharErrorMessage" : "Ограничение на длину поля ${_.getCurrentField().Name} ${_.getCurrentField().Value.length} символов.<br>Вы не можете ввести больше",
					"barcodeAlreadyRead" : "Штрих-код уже был считан<br>Чтобы считать штрих-код заново нажмите Назад и снова выберите получателя услуги",
					"barcodeNotSupported" : "Ввод данных с помощью штрих кода у данного получателя не предусмотрен",
					"validateFieldFailed" : "Введенное поле не прошло проверку<br><font style='color=red'>${_.validateErrorMessage}</font><br>Убедитесь в правильности введенных данных",
					"validateCatalogItemFailed" : "Данные не прошли проверку<br><font style='color=red'>${_.validateCatalogItemErrorMessage}</font><br>Убедитесь в правильности введённой информации"
				},
								
				"pleaseSelect" : "Выберите ",
				"pleaseInput" : "Введите ",
				"pleaseConfirm" : "Подтвердите ",
				"notSelected" : "не выбрано",
				"notRequired" : "Поле вводить не обязательно!",
				"statusText" : "<table class=statustxt><tr><td><img width=${PaymentGateway.SelectedCatalogItem.Picture.Width} height=${PaymentGateway.SelectedCatalogItem.Picture.Height}  src='images-recipients/${getActiveLocale()}/${PaymentGateway.SelectedCatalogItem.Picture.URL}'></td><td>${getCurrentField().Name}</td></tr>",
				"fingerScanned" : "<table class=\"finger-message\"><tr><td class=\"finger-message\">Отпечаток отсканирован успешно</td></tr><tr><td align=center><img width=128 height=128 src=${_.scannedImagePath} /></td></tr></table>"
			},
			
			"dataCheck" : {
				"executingMethod" : "Пожалуйста, подождите...<br>Идет проверка данных....<br><br>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br><img src='images/clock.png'>",
				"dataValidationFailed" : "Ошибка проверки данных",
				"dataCheckInProgress" : "Идет проверка данных, пожалуйста, подождите",
				"checkResult" : {
				
					"datacheck" : {
					// checkFailed by default
					"__undefined__" : "В данный момент платеж с параметрами:<br><br>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br><font style='color:#EF1515'>не может быть выполнен<br><br>${_.errorText}<br></font>",
					// Succeded
					"0"	: "Проверка номера прошла успешно<br>Подтвердите присланные данные:<b>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br></b>Вы согласны?",
					"2"	: "Подтвердите введенные данные:<b>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}</b>",
					// NeedNextCheck
					"1" : "Проверка номера прошла успешно<br>Подтвердите присланные данные:<b>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br></b>, <font style='color:#EF1515'>Требуется ввод дополнительных данных</font><br>Вы согласны?",
					// checkFailedCanPay + payWillBeDefferred
					"-2" : "В данный момент платеж с параметрами:<br><br>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br><font style='color:#EF1515'>не может быть выполнен<br><br>${_.errorText}<br></font><br>Вы можете провести платеж, но в этом случае деньги поступят на Ваш счет <b>через некоторое время</b><br>Провести платеж ?",
					"-3" : "В данный момент платеж с параметрами:<br><br>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br><font style='color:#EF1515'>не может быть выполнен<br><br>${_.errorText}<br></font><br>Вы можете провести платеж, но в этом случае деньги поступят на Ваш счет <b>через некоторое время</b><br>Провести платеж ?",
					// checkFailedCanPay
					"-4" : "В данный момент платеж с параметрами:<br><br>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br><font style='color:#EF1515'>не может быть выполнен<br><br>${_.errorText}<br></font><br>Вы можете провести платеж, но в этом случае деньги поступят на Ваш счет <b>через некоторое время</b><br>Провести платеж ?"		
					},
					
					"virtuposcheck": {
						"__undefined__" : "В данный момент платеж с параметрами:<br><br>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br><font style='color:#EF1515'>не может быть выполнен<br><br></font>"						
					}
				
				}
			},	
			
			"afterDataCheck" : {
				"changeOrders" : "Чтобы изменить заказ",
				"goToCassa" : "Не устраивает автокасса"
			},
			
			"select" : {
				"selectInProgress" : "Идет проверка данных, пожалуйста, подождите<br><br>Группа платежа: <b>${PaymentGateway.SelectedCatalogItem.Name}<br><br></b>${generatePaymentInfo(PaymentGateway.SelectedCatalogItem.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, true)}<br>",
//				"checkSucceeded" : "Исходя из введенных данных выбран получатель<br>${PaymentGateway.SelectedRecipient.Name}<br><br>${formatCatalogItem(PaymentGateway.SelectedRecipient)}",
				"selectSucceeded" : "Исходя из введенных данных выбран получатель<br>${PaymentGateway.SelectedCatalogItem.Name}",
				"groupSelected" : "Исходя из введенных данных выбрано несколько получателей<br>Сделайте выбор одного из них<br><br>",
				"incorrectSelected" : "Введенный ${generatePaymentInfo(PaymentGateway.SelectedCatalogItem.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)} не соответствует выбранной группе ${PaymentGateway.SelectedCatalogItem.Name}<br>Убедитесь в правильности введенных данных и выборе верной группы получателей",
				"incorrectSelectedUniBank" : "Это убрать, после исправления pgw тут будет код ошибки",
				"selectFailed" : "Ошибка проверки данных<br>${enumerateRecipientFields(PaymentGateway.SelectedCatalogItem.Fields, _.fieldValueTemplate, false)}<br>",
				"selectFailedUniBank" : "Ошибка проверки данных с параметрами:<br><br>${enumerateRecipientFields(PaymentGateway.SelectedCatalogItem.Fields, _.fieldValueTemplate, false)}<br>",
				"checkData" : "Проверьте правильность введенных данных",
				"areYouAgree" : "Вы согласны?",
				"selectValidationFailed" : "Ошибка проверки данных"
			},
			
			
			"selectPaymentMethod" : {
				"fixedSummWarning" : "Внимание!<br>Платеж на <b>фиксированную</b> сумму ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'short')}<br>"+
									"Остаток нужно будет зачислить на счет другого получателя.<br>Приступить к оплате?",
				"minSummWarning" : "Внимание!<br><b>Минимальная</b> сумма платежа ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'short')}<br>"+
									"Остаток будет зачислен на следующий период.<br>Приступить к оплате?",
				"card" : "Банковская карта",
				"cash" : "Наличные",
				"wallet" : "Кошелек",
				
				"noPaymentMethod" : "Для данного получателя ни один из способов оплаты недоступен",
				"selectPMTitle" : "Выбор типа оплаты"
			},
			
			"acceptCashPayment" : {
				"statusRecipient" : "Введите сумму для платежа на счет ${_.rCollection.Item(0).Name}",			
				"unableToAccept" : "Оплата в настоящий момент невозможна<br>Попробуйте, пожалуйста, позже",
				"maxSummWarning" : "Максимальный платеж  ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'short')}<br>Вы можете зачислить средства совершив еще один платеж",
				"minSummWarning" : "Минимальный платеж  ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'short')}<br>Поэтому первая купюра должна быть не менее ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}",
				"rejectOnReason4" : "Для проведения платежа требуется ",
				"firstNoteMessage" : "<font class='colorText'> (первая купюра должна быть не менее ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}).</font>",
				
				"payLimitsMessageFirstNote" : "Внимание! Минимальная сумма платежа ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}"+
											"<font class='colorText'> (первая купюра должна быть не менее ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}).</font>"+
											"Максимальная сумма ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')} "+
											"При внесении суммы более ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')} платеж проведется автоматически",
											
				"payLimitsMessageNoChangeFirstNote" : "Внимание! Минимальная сумма платежа ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}"+
													 "<font class='colorText'> (первая купюра должна быть не менее ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}). </font>"+
													 "Максимальная сумма ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')}",

				"payLimitsMessage" : "Внимание! Минимальная сумма платежа ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')} "+
										"Максимальная сумма ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')} "+
										"При внесении суммы более ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')} платеж проведется автоматически",
										
				"payLimitsMessageNoChange" : "Внимание! Минимальная сумма платежа ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')} "+
				                              "Максимальная сумма ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')}",
				
				"maxSumm" : "Максимальный платеж  ${getCurrency().format(Math.min(_.rCollection.Item(0).MaxLimit, VirtuPOS.AvailableBalance), 'short')}",
				"minSumm" : "Минимальный платеж  ${getCurrency().format(_.rCollection.Item(0).MinLimit, 'short')}",
				"sumPresent" : "Накопленной суммы достаточно для проведения платежа<br>Провести платеж?",
				"fixedSummWarning" : "Внимание! Оплата на фиксированную сумму: ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'short')} Остаток средств с платежа нужно зачислить на счет другого получателя.",
				"recommendedSummWarning" : "Внимание! Рекомендуемая сумма платежа не менее ${getCurrency().format(_.recommendedSum, 'short')}",
				"freeSummWarning" : "Внимание! Минимальная сумма платежа ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}",
				"maxPaymentSum" : "Максимальная сумма ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')}",
				"automaticPayment" : "При внесении суммы более ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')} платеж проведется автоматически",
				"fixedMinSummWarning" : "Внимание! Сумма к оплате: ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'short')}. Вы должны внести сумму, не менее ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'short')} (например ${getCurrency().format(Math.ceil(PaymentGateway.SelectedRecipient.PaymentSum/10)*10, 'short')}). Сумма переплаты будет зачислена на ваш счет на следующий период. При внесении нужной суммы платеж проведется автоматически",
				"insertMoney" : "Внесите деньги по одной купюре",
				"paymentSumStr" : "К оплате:",
				"transferSumStr" : "К зачислению:",
				"commissionSumStr" : "Комиссия:",
				"accumulatedSumStr" : "Принято:",
				"remainSumStr" : "Осталось внести",
				"toGoRecipients" : "Для выбора другого получателя - нажмите «Назад»<br>",
				"continueOrPayFree" : "Для внесения денег - нажмите «Внести купюры»<br>Для проведения платежа на ${getCurrency().format(PaymentDevice.AccumulatedSum+PaymentDevice.ChangeSum, 'short')} - нажмите «Провести платеж»\
<br>Если вы не сделаете выбор платеж проведется через ${Math.round(getParameter('acceptCash.waitTimeout', 60000)/2000)} секунд",
				"sumLeftFixed" : "Вам осталось внести ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum - PaymentDevice.AccumulatedSum - PaymentDevice.ChangeSum)}<br>",
				"incorrectCollection" : "Невозможно обработать установленный набор получателей",
				"incorrectCurrency" : "Для получателей в наборе определены несколько валют",
				"askMoreTime" : "Вам нужно дополнительное время?"
				
			},
			
			"acceptCardPaymentUnibank" : {
				"confirmSum" : "Подтвердите сумму операции"
			},
			
			"acceptWalletPayment" : {
				"statusSumEntry" : "Введите сумму платежа (баланс ${getCurrency().format(PaymentGateway.CustomerBalance)}):",
				"statusSumLeft" : "Оставшаяся сумма:",
				"statusRecipient" : "Введите сумму для платежа на счет ${PaymentGateway.SelectedRecipient.Name}",
				"maxBalanceLimitReached" : "Вы не можете выбрать сумму больше ${getCurrency().format( maxLimit() )}"
			},
			
			"acceptPaymentManual" : {
				"partyPayment" : "Разделение суммы:",
				"sumLeft" : "Оставшаяся сумма:",
				"maxBalanceLimitReached" : "Вы не можете выбрать сумму больше ${getCurrency().format( maxLimit() )}"
			},
			
			"sumEntry" : {
				"dispenseSumStr" : "К выдаче:",
				"paymentSumStr" : "К оплате:",
				"transferSumStr" : "К зачислению:",
				"commissionSumStr" : "Комиссия:",
				"maxSumm" : "Максимальный платеж  ${getCurrency().format(_.recipient.MaxLimit, 'short')}",
				"minSumm" : "Минимальный платеж  ${getCurrency().format(_.recipient.MinLimit, 'short')}",			
				"status" : "Ввод суммы платежа",
				"fixedSumRecipient" : "Вы собираетесь оплатить получателя с фиксированной суммой платежа:<br><br><b>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br><br>Сумма к списанию: ${getCurrency().format(_.recipient.PaymentSum)}</b><br><br>Вы согласны?",
				"statusRecipient" : "Введите сумму для платежа на счет ${PaymentGateway.SelectedRecipient.Name}",
				"issueOfCash" : "Введите сумму для выдачи наличных",
				"maxBalanceLimitReached" : "Вы не можете выбрать сумму больше ${getCurrency().format( maxLimit() )}",
				"cancelOperation" : "Операция отменена"
			},
			
			"payment" : {
				"executingMethod" : "Пожалуйста, подождите...",
				"paymentInProgress" : "Идет проведение платежа, пожалуйста, подождите...",
				"statusForCollectionCase" : "Процесс оплаты коллекции получателей",
				"tableHeader" : "Процесс оплаты коллекции:",
				"payOk" : "Успешно",
				"payFail" : "Платёж не прошёл",
				"payDeferred" : "Платёж отложен"
			},
			
			"peerToPeer" : {
				"operationFailed" : "Перевод средств закончился неудачей. Проверьте номер карты получателя или попробуйте позже."
			},
			
			"dispense" : {
				"startDispense" : "Ждите выдачи средств не отходя от терминала<br>К выдаче: ${_.dispensingSum.format()}",
				"completeMessage" : "Заберите деньги<br>${_.dispensingSum.format()}",
				"deviceError" : "Сумма ${getCurrency().format(_.changeSum)} не может быть выдана из за ошибки устройства.<br>Сдачу вы можете зачислить на счет другого получателя или обратитесь к администратору",
				"pleaseWait" : "Ждите выдачи денег не отходя от терминала",
				"noRequiredSum" : "Сумма ${getCurrency().format(_.changeSum)} не может быть выдана из за отсутствия необходимого количество купюр для выдачи сдачи<br>Обратитесь к администратору",
				"dispenseError" : "Ошибка выдачи суммы ${getCurrency().format(_.changeSum)}<br>(${_.errorText})<br>Возьмите выданную квитанцию<br>Обратитесь к администратору",
				"dispenseSuccess" : "Сумма ${getCurrency().format(_.changeSum)} выдана успешно<br><b><font color='red'>Заберите купюры в лотке</font></b>",
				"dispenseErrorAfter" : "Произошла ошибка выдачи купюр<br>Возьмите выданную квитанцию<br>Невыданная сумма ${getCurrency().format(_.unDispensedSum)}<br>будет возвращена на Ваш счет",
				"dispenseErrorAfterUtinet" : "Произошла ошибка выдачи купюр<br>Возьмите выданную квитанцию<br>Невыданная сумма ${getCurrency().format(_.unDispensedSum)}<br>будет возвращена. Обратитесь к менеджеру.",
				"billReturn" : "Вставьте купюру другого номинала."
			},
			
			"insertCard" : {
				"insertCardTitle" : "Ввод карты клиента",
				"pleaseInsert" : "Вставьте Вашу карту",
				"takeYourCard" : "Заберите Вашу карту",
				"cardReaderFailure" : "В данный момент операция невозможна по причине сбоя оборудования",
				"startingApplication" : "<b>Старт приложения</b>",

				"insertCardErrors" : {
					"1" : "Карта вставлена не той стороной или не может быть считана",
					"2" : "Истёк срок действия карты, обратитесь в ваш банк",
					"3" : "Карта не обслуживается, обратитесь в ваш банк",
					"__undefined__" : "Карта не обслуживается"
				}
			},
			"cardEject" : {
				"takeYourCard" : "Заберите Вашу карту",
				"foreignCard" : "Заберите Вашу карту",
				"captureTimeout" : "В случае, если карта не будет извлечена в течение&nbsp;<span id=\"captureTick\">${getParameter('card.captureTimeout', 30)}</span>&nbsp;секунд карта будет изъята терминалом",
				"cardCapturing" : "Производится изъятие карты...."
			},
			"cardPayment" : {
					"showBalance" : "Ваш баланс: ",
					"__undefined__" : "Платеж не выполнен. Невозможно авторизовать"	
			},			
			"cardCaptured" : {
				"message" : "Карта изъята, обратитесь по телефону:",
				"status" : "Изъята карта",
				"servicePhone" : "Информационная служба 8-800-10-80-70"
			},
			
			"pinEntry" : { 
				"pinEntryTitle" : "Ввод ПИН-кода карты",
				"pinEntryHint" : "Введите ПИН-код",
				"pinCancelled" : "Ввод ПИН-кода отменен",
				"pinWait" : "Пожалуйста, подождите....<br><br><br><img src='images/clock.png'>",
				"pinPadFail" : "Ошибка устройства ввода ПИН кода"
			},
			"verifyCard" : {
				"genericError" : "Ошибка проверки данных",
				// При этом сообщении есть showAlert, соощение лишнее.
				//"cardVerifyHint": "<img src='images/clock.png'><br><b>Идет подготовка к проверке карты, пожалуйста, подождите...<b>",
				"cardVerifyHint": "",
				"verifyError" : "Ошибка авторизации карты на сервере банка",
				"backToReturn" : "Нажмите «Назад» для возврата карты",
				"cardVerifyTitle" : "Проверка данных на сервере банка",
				
				"errors" : {

					"__undefined__" : "Невозможно авторизовать"
				
				}
				
			},
			"printInfo" : {
				"printerOffline" : "В данное время печать невозможна, приносим свои извинения",
				"terminalNr" : "Терминал № ",
				"receiptNr" : "  Квитанция № ",
				"terminalAddress" : "Адрес: ",
				"date" : "Дата: ",
				"time" : "  Время: ",
				"thanks" : "СПАСИБО ЗА СОТРУДНИЧЕСТВО!",
				"serviceLine" : "Телефон горячей линии: ",
				"paymentSum" : "Сумма: ",
				"changeSum" : "Сдача: ",
				"commissionSum" : "Комиссия: ",
				"transferSum" : "К зачислению: "
			},
			"printCoupon" : {
				"printDone" : "Операция прошла успешно",
				"printDone1" : "Операция выполнена учпешно<br><br>Благодарим Вас за доброе сердце и участие в жизни детей.<br>Отчет о деятельности Фонда, результаты работы и текущие акции<br>Вы всегда можете увидеть на сайте Фонда: http://www.vostfond.ru",
				"printFailed" : "Квитанция не может быть напечатана из-за технической неисправности"
			},
			
			"ministatement" : {
				"makeChoice" : "<b>Запрос завершен успешно<br><br>Выберите способ вывода информации</b>",
				"printDone" : "<b>Возьмите Вашу квитанцию</b>",
				"failed" : "<b>Ошибка запроса минивыписки счета</b>",
				"printerFailure" : "Сбой принтера. К сожалению, в данный момент печать минивыписки невозможна."
			},			
			"showBalance" : {
				"makeChoice" : "<b>Запрос завершен успешно<br><br>Выберите способ вывода информации</b>",
				"printDone" : "<b>Возьмите Вашу квитанцию</b>",
				"printerFailure" : "Сбой принтера. К сожалению, в данный момент печать баланса невозможна."
			},
			
			"recipients-fixedsum-collection" : {
				"totalAmount" : "Общее количество",
				"totalSum" : "Общая сумма",
				"resetAll" : "Обнулить всё"
			},
			
			"serviceMenu" : {
				"reports" : "Журналы",
				"devices" : "Устройства",
				"setup" : "Настройка",
				"exit" : "Выход",
				"payments" : "Платежи",
				"bills" : "Купюры",
				"billsDispense" : "Выдача купюр",
				"cardCapture" : "Захват карт",
				"printer" :"Принтер",
				"fiscal" : "Фискальный Регистратор",
				"billacceptor" : "Купюроприемник",
				"dispenser" : "Диспенсер купюр",
				"payKioskExit" : "'Выход из PayBox'",
				"onScreenKeyboard" : "'Экранная клавиатура'",
				"runExplorer" : "'Запустить Explorer'",
				"removeWinLocks" : "'Снять блокировки Windows'",
				"withDesktop" : "'С рабочим столом'",
				"withoutDesktop" : "'Без рабочего стола'",
				"keyboardComWarning" : "Внимание! При перезапуске PayKiosk с запущенной экранной клавиатурой порт COM1 будет занят экранной клавиатурой!",
				"property" : "Свойство",
				"status" : "Состояние",
				"interface" : "Интерфейс",
				"receiptPrinterPresent" : "${ReceiptPrinter ? \"Найден\" : \"Не найден\"}",
				"authFailed" : "Неверный код",
				"authorize" : "Авторизовать",
				"clear" : "Очистить",
				"encashment" : "Инкассация",
				"unablePrintReceipt" : "Квитанции распечатаны не будут. Принтер неисправен.",
				"printJournalAgain" : "Повторная печать журналов",
				"billAcceptor" : "Купюроприемник",
				"coinAcceptor" : "Монетоприемник",
				"billDispenser" : "Выдача купюр",
				"hoppers" : "Выдача монет",
				"enterStampNumber" : "Введите номер пломбы устанавливаемой (пустой) кассеты",
				"skipDevice" : "Пропустить"
				
			},
			
			"balanceAfterDeposit" : {
				"hint" : "Отправляется запрос к серверу..."
			},
			
			"offline" : {
				"status" : "Терминал временно не работает",
				"message" : "Терминал временно не работает<br>Терминал переведен в режим Offline"
			},
			"information" : {
				"info" : 'СМП Банк (Открытое акционерное общество Банк "Северный морской путь") образован в 2001 году. СМП Банк осуществляет полный комплекс финансовых услуг для корпоративных клиентов и частных лиц. Банк имеет широкую сеть подразделений, насчитывающую более 100 офисов и охватывающую 29 регионов России. В Московской области действуют порядка 50 отделений. Кроме того, СМП Банк представлен в Латвии дочерним AS SMP Bank, обслуживающим частных лиц и корпоративных клиентов, имеющим лицензию на все виды банковской деятельности.'
			}

	},		

		{
			// number должно быть целым числом		
			"getNumberName" : function(number, names) {
				// склоняемое название
				var n = number % 100;
				if ((n > 4) && (n < 21)) return names[2];
				
				n %= 10;
				if ((n === 0) || (n > 4)) return names[2];
				if (n === 1) return names[0];
				return names[1];
			},
			
			"currency" : {
				"RUR" : {
					"MajorUnit" : ["рубль", "рубля", "рублей"],
					"MinorUnit" : ["копейка", "копейки", "копеек"],
					"MajorUnitShort" : "руб.",
					"MinorUnitShort" : "коп.",
					"MajorUnitSign" : "р.",
					"MinorUnitSign" : "к.",
					"MajorDigitGrouping" : {"quantity" : 3, "separator" : " "}			
				},
				"RUB" : {
					"MajorUnit" : ["рубль", "рубля", "рублей"],
					"MinorUnit" : ["копейка", "копейки", "копеек"],
					"MajorUnitShort" : "руб.",
					"MinorUnitShort" : "коп.",
					"MajorUnitSign" : "р.",
					"MinorUnitSign" : "к."
				},
				"AZN" : {
					"MajorUnit" : ["манат", "маната", "манат"],
					"MinorUnit" : ["гяпик", "гяпика", "гяпиков"],
					"MajorUnitShort" : "ман.",
					"MinorUnitShort" : "гяп.",
					"MajorUnitSign" : "м.",
					"MinorUnitSign" : "г."
				},
				"UZS" : {
					"MajorUnit" : ["сум", "сум", "сум"],
					"MinorUnit" : ["тийин", "тийин", "тийин"],
					"MajorUnitShort" : "сум ",
					"MinorUnitShort" : "тий.",
					"MajorUnitSign" : "с.",
					"MinorUnitSign" : "т."
				},
				"USD" : {
					"MajorUnit" : ["USD", "USD", "USD"],
					"MinorUnit" : ["cent", "cent", "cent"],
					"MajorUnitShort" : "$",
					"MinorUnitShort" : "¢",
					"MajorUnitSign" : "$",
					"MinorUnitSign" : "¢"
				},
				"EUR" : {
					"MajorUnit" : ["EUR", "EUR", "EUR"],
					"MinorUnit" : ["cent", "cent", "cent"],
					"MajorUnitShort" : "€",
					"MinorUnitShort" : "¢",
					"MajorUnitSign" : "€",
					"MinorUnitSign" : "¢"
				}

			}
		}
);