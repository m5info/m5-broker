﻿appendStates({
	"accept-card-payment" : {
		"behaviour" : "accept-card-payment.js",
		"requires" : ["keypad.js"]
	},	
	"card-payment" : {
		"view" : "card-payment-view.js",
		"behaviour" : "card-payment.js"	
	},
	"sum-entry" : {
		"requires" : ["keypad.js"]
	},
	"sum-entry-manual" : {
		"extends" : "sum-entry",
		// "screen" : "sum-entry.html",	
		// "view" : "sum-entry-view.js",
		// "behaviour" : "sum-entry.js",	
		"requires" : ["keypad.js", "payment-method-manual.js"]
	},	
	"sum-entry-wallet" : {
		"extends" : "sum-entry",
		// "screen" : "sum-entry.html",	
		// "view" : "sum-entry-view.js",
		// "behaviour" : "sum-entry.js",	
		"requires" : ["keypad.js", "payment-method-wallet.js"]
	},
	"sum-entry-card" : {
		"extends" : "sum-entry",
		// "screen" : "sum-entry.html",	
		// "view" : "sum-entry-view.js",
		// "behaviour" : "sum-entry.js",	
		"requires" : ["keypad.js", "payment-method-card.js"]
	},
	"sum-entry-card-IssueOfCash" : {
		"extends" : "sum-entry-card",
		"view" : "sum-entry-card-view-IssueOfCash.js"
//      "behaviour" : "sum-entry-card-IssueOfCash.js",
//		"screen" : "sum-entry-card-IssueOfCash.html",
//		"behaviour" : "sum-entry.js",		
//		"requires" : ["keypad.js", "payment-method-card.js"]
	},	
	"card-cash" : {
		"view" : "card-cash-view.js",
		"behaviour" : "card-cash.js"	
	},
	"accept-payment-wallet" : {
		"requires" : ["keypad.js"]
	},

	"print-coupon" : {
		"requires" : ["standard-coupon.js", "terminal.js"]
	},
	

	"ministatement" : {
		"requires" : ["ministatement-coupon.js", "terminal.js"]	
	},	
	"show-balance" : {
		"requires" : ["balance-coupon.js", "terminal.js"]	
	},
	
	// "balance-after-deposit" : {
		// "behaviour" : "balance-after-deposit.js"
	// },
	
	"data-entry" : {
		"requires" : ["keypad.js", "auth.js"]
	},
	
	"virtupos-check" : {
		"view" : "data-check-view.js"
	},
	
	"card-captured" : {
		"requires" : ["terminal.js", "capture-coupon.js"]		
	},
	"eject-card-before-print" : "eject-card",
	"eject-card-before-dispense" : "eject-card",
	
	"encashment-switch" : {
		"application" : "encashment"
	},
		
	"service-switch" : {
		"application" : "service"
	},
	
	"external-link" : {
		"application" : "external-link"
	},
		
	"recipients-ServiceMenu" : "encashment-switch",

	"service-entry" : {
		"requires" : ["auth.js"]
	}		
});

