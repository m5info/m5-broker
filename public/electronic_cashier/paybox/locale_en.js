﻿addLocale(
		"en",
		{
			"currency" : {
				"RUR" : "Russian roubles",
				"RUB" : "Russian roubles",
				"USD" : "US Dollars",
				"EUR" : "Euros",
				"AZN" : "Manats"
			},
			
			"validateFieldErrors": {
				"__undefined__": "Data entered into the field is invalid"
			},

			"validateRecipientErrors": {
				"__undefined__": "Data entered for the recipient is invalid"
			},
			
			"commission": {
				"noCommission": "The Commission is not charged,",
				"commisionMoreThan": "At the case of total sum more than ${limitText} amount of commission will be",
				"commissionIs": "The commission is,",
				"atLeast": ", but no less than"
			},

			"paykiosk": {
				"noTerminalNumber": "not specified",
				"balanceFormat": "Balance: ${getCurrency(getParameter('paykiosk.walletCurrency', 'RUR')). format (PaymentGateway.CustomerBalance)}",
				"changeSumFormat": "Balance: ${getCurrency(getParameter('paykiosk.walletCurrency', 'RUR')). format (PaymentDevice.ChangeSum)}",
				"billAcceptorFailure": "The terminal is temporarily unavailable due to billacceptor failure",
				"printerFailure" : "The terminal is temporarily unavailable due to printer failure",				
				"printerOfflineWarning": "Due to the technical reasons, a receipt won't be printed <br> Continue? <br>",
				"billDispenserFailure" : "The terminal is temporarily unavailable due to billdispenser failure"				
			},

			"VirtuPOS" : {
				"executingVirtuPOSMethod" : "<br><br><b>Executing a command</b><br><br><img src='images/clock.png'><br><br>",
				"tryVirtuPOSMethodAgain" : "<br><br><b>System is busy. Preparing for new attempt...</b><br><br>"
			},
			
			"deviceStatus" : {
				"Printer" : {
					"dssUndefined" : "Undefined",
					"dssDisabled_Initialize" : "Disabled",
					"dssIdle_Normal" : "Normal",
					"dssIdle_Suspended" : "Suspended",
					"dssIdle_PaperNearEnd" : "Forthcoming paper end detected",
					"dssBusy_Printing" : "Printing",
					"dssBusy_Holding" : "Holding",
					"dssError_NoDevice" : "No device",
					"dssError_DeviceFailure" : "Device failure",
					"dssError_PaperOut"	: "Paper out or incorrect adjustment"	
				},
				"billAcceptor" : {
					"dssUndefined" : "Undefined",
					"dssDisabled_PowerUp" : "Powering up",
					"dssDisabled_Initialize" : "Initialize",
					"dssDisabled_Inhibit" : "Inhibit",
					"dssBusy_Accepting" : "Accepting",
					"dssBusy_RejectingInhibit" : "Rejecting inhibit",
					"dssBusy_Rejecting" : "Rejecting",
					"dssBusy_Escrow" : "Escrow",
					"dssBusy_Stacking" : "Stacking",
					"dssBusy_Stacked" : "Stacked",
					"dssBusy_Returning" : "Returning",
					"dssBusy_Returned" : "Returned",
					"dssBusy_Paused" : "Paused",
					"dssBusy_Holding" : "Holding",
					"dssBusy_dssDispensing" : "Busy, dispensing",
					"dssBusy_dssDispensed" : "Busy, dispensed",
					"dssBusy_dssDispensedComplete" : "Dispense complete",
					"dssBusy_dssUnloading" : "Unloading",
					"dssBusy_dssUnloaded" : "Unloaded",
					"dssBusy_Packed" : "Packed",
					"dssError_NoDevice" : "No device",
					"dssError_DeviceFailure" : "Device failure",
					"dssError_StackerFull" : "Stacker full!",
					"dssError_ValidatorJammed" : "Validator jammed!",
					"dssError_StackerJammed" : "Stacker jammed!",
					"dssError_StackerRemoved" : "Stacker removed!",
					"dssIdle_Normal" : "Normal",
					"dssIdle_Cassette" : "Cassette",
					"dssIdle_CassetteNormal" : "Cassette normal",
					"dssIdle_CassetteNearEnd" : "Cassette near end",
					"dssIdle_CassetteNotPresent" : "Idle, but cassette not present",
					"dssIdle_CassetteInvalid" : "Invalid cassette",
					"dssError_CassetteNotPresent" : "Error, cassette not present",
					"dssIdle_Sensor" : "Sensor",
					"dssIdle_Suspended" : "dssIdle_Suspended",
					"dssError_ShutterFailure" : "Shutter failure",
					"dssError_PoolFailure" : "Pool failure",
					"dssError_PotentiometerFailure" : "Potentiometer failure"				
				},
				"VirtuPOS" : {
					"available" : "Available",
					"unavailable" : "Unavailable"					
				}
			},
			
			"start" : {
					
				"printerName" : "Receipt printer",
				"billacceptorName" : "Billacceptor",				
				"virtuposName" : "VirtuPOS",
				
				"devName" : "Device name",
				"devState" : "State"				
			
			},			
			
			"initialize" : {
				"terminalInUpdate": "The terminal is updating<br> Press Back to make a payment",
				"reload" : "Reloading..."
			},
			
			"barcodeEntry" : {
			
				"states" : {
					"NotDefined" : "<b><font style=\"color:red\">Scan a barcode.</font></b><br>Please scan a barcode.",
					"Defined" : "<b><font style=\"color:red\">You can scan additional barcode.</font></b><br>Please scan a barcode or press Next button for manual data entry.",
					"NotRequired" : "<b><font style=\"color:red\">You can scan a barcode.</font></b><br>Please scan a barcode or press Next button for manual data entry."
				},			
				
				"setBarcodeErrors" : {
					"1" : "Barcode has invalid length",
					"2" : "Barcode has no valid marker",
					"3" : "Barcode contains invalid data",
					"4" : "Barcode could not be used to set recipient fields",
					"__undefined__" : "Unknown error"
				},
				
				"barcodeReadSucceeded" : "Barcode ${_.barcode} has been read successfully. Press 'OK' to continue.",
				"barcodeReadFailed" : "<font style=\"color:red\">Invalid barcode ${_.barcode} has been read.</font><br>"
						+ "Press 'OK' to continue.",

				"validateRecipientFailed" : "<b><font style=\"color:red\">${_.validateRecipientErrorMessage}.</font></b>",		

				"allFieldsComplete" : "All barcodes has been read successfully and you can continue. Check fields:<br>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br>Do you want to change fields manually?"				
			},			
			

			"recipients": {
				"unableToPay": "The payment <br>« ${PaymentGateway.SelectedCatalogItem.Name} »<br> can not be executed for technical reasons at the moment. We apologize",
				"unableToPayPrinterOffline": "At the moment the payment <br>« ${PaymentGateway.SelectedCatalogItem.Name} »<br> can not be executed because the printer is defective. We apologize",
				"descriptionOnSelect": "Attention! <br> ${PaymentGateway.SelectedCatalogItem.Description} <br> start entering data?",
				"temporaryUnavailable": "Section <br> ${PaymentGateway.SelectedCatalogItem.Name} <br> temporarily unavailable",
				"information" : "Information",
				"billAcceptorFailureAlert" : "At the moment the payment <br>« ${PaymentGateway.SelectedCatalogItem.Name} »<br> can not be executed because the bill acceptor is defective. We apologize"
			},
			
			"recipientsView": {
				"rootStatusText" : "Choose operation",			
				"paymentRulesLink": "Select Language",
				"groupStatusText" : "<table class=statustxt><tr><td><img width=${PaymentGateway.SelectedCatalogItem.Picture.Width} height=${PaymentGateway.SelectedCatalogItem.Picture.Height}  src='images-recipients/${getActiveLocale()}/${PaymentGateway.SelectedCatalogItem.Picture.URL}'></td><td>${PaymentGateway.SelectedCatalogItem.Name}</td></tr>",
				"pageOfThePage" : "Page ${(_.currentPageNr + 1)} of ${(_.lastPageNr + 1)}"
			},	
			
			"recipientsFixedsumCollection" : {
				"resetAll" : "Reset all",
				"totalNumberStr" : "Total amount:<div id='totalNumber'>${_.itemCount}</div>",
				"totalSumStr" : "Total sum:<div id='totalSum'>${getCurrency('RUR').format(0, 'sign')}</div>"
			},			
			
			"dataEntry": {
			
				"errors" : {
					"appendCharErrorMessage": "Restriction on the length of the field ${_.getCurrentField.impl.Name} ${_.getCurrentField.impl.Value.length} characters. <br> You can not enter anymore,",
					"barcodeAlreadyRead": "The bar code has been read <br> To read the bar code again, press Back and then select the recipient of services",
					"barcodeNotSupported": "Data Entry in the bar code of the recipient is not provided",
					"validateFieldFailed": "Field's data didn't pass the verification <br> <font style='color=red'>${_.validateErrorMessage}</font> Ensure the data are correct, ",
					"validateCatalogItemFailed" : "Data didn't pass the verification<br><font style='color=red'>${_.validateCatalogItemErrorMessage}</font><br>Ensure the data are correct"				
				},

				"pleaseSelect": "Click ",
				"pleaseInput": "Enter ",
				"pleaseConfirm": "Confirm ",
				"notSelected": "not selected",
				"notRequired": "input field is not necessary!",
				"statusText" : "<table class=statustxt><tr><td><img width=${PaymentGateway.SelectedCatalogItem.Picture.Width} height=${PaymentGateway.SelectedCatalogItem.Picture.Height}  src='images-recipients/${getActiveLocale()}/${PaymentGateway.SelectedCatalogItem.Picture.URL}'></td><td>${catalogItemFields[currentFieldIndex].Name}</td></tr>"
			},		
			"dataCheck": { 

				"executingMethod" : "Please wait...",
				"dataValidationFailed" : "Failed validation",
				"dataCheckInProgress" : "Checking data, please wait",
				"checkResult" : {
				
					"datacheck" : {
					// checkFailed by default				
					"__undefined__" : "At the moment the payment with the following parameters: <br> ${generatePaymentInfo (PaymentGateway. SelectedRecipient. Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<font style = 'color: #EF1515'> can not be executed <br><br></font>",
					// Succeded
					"0" : "number check is successful <br> Confirm sent data: <b> ${generatePaymentInfo (PaymentGateway. SelectedRecipient. Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)} <br> </b>Do you agree?",
					// NeedNextCheck
					"1" : "number check is successful <br> Confirm sent data: <b> ${generatePaymentInfo (PaymentGateway. SelectedRecipient. Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)} <br> </b>, <font style = 'color: #EF1515'> You are prompted for additional information </font><br>Do you agree?",
					// checkFailedCanPay + payWillBeDefferred
					"-2" : "At the moment the payment with the following parameters: <br> ${generatePaymentInfo (PaymentGateway. SelectedRecipient. Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<font style = 'color: #EF1515'> can not be executed <br><br></font>, <b>You can spend your payment by clicking the «Next».</b>In this case, the money will be transferred to your account <b> after a while </b>",
					// checkFailedCanPay
					"-4" : "At the moment the payment with the following parameters: <br> ${generatePaymentInfo (PaymentGateway. SelectedRecipient. Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<font style = 'color: #EF1515'> can not be executed <br><br></font>, <b>You can spend your payment by clicking the «Next».</b>"		
					},
					
					"virtuposcheck": {
						"__undefined__" : "At the moment the payment with the following parameters: <br> ${generatePaymentInfo (PaymentGateway. SelectedRecipient. Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<font style = 'color: #EF1515'> can not be executed <br><br></font>"						
					}
				
				}
				
			},
			
			"afterDataCheck": {
				"changeOrders": "To change the order,",
				"goToCassa": "Not satisfied avtokassa"
			},
			
			"select": { 
				"selectInProgress": "Checking data, please wait <br> <br> group of payment: <b>${PaymentGateway.SelectedCatalogItem.Name}<br><br></b>${generatePaymentInfo(PaymentGateway.SelectedCatalogItem.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, true)}<br>",
				"selectSucceeded": "Based on the input data is selected recipient <br> ${PaymentGateway.SelectedCatalogItem.Name}", 
				"groupSelected": "Based on the data entered multiple recipients are selected <br> Choose one of them <br> <br>", 
				"incorrectSelected": "entered ${generatePaymentInfo (PaymentGateway.SelectedCatalogItem.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)} does not correspond to a selected group of ${PaymentGateway.SelectedCatalogItem.Name} <br> sure to enter your data and choosing the correct groups of recipients, ", 
				"incorrectSelectedUniBank": "It's clean, then after correcting pgw bkdet error code", 
				"selectFailed": "Failed validation <br> <br> ${enumerateRecipientFields (PaymentGateway.SelectedCatalogItem.Fields, _.fieldValueTemplate, false)} <br>", 
				"selectFailedUniBank": "Failed to check the data with the parameters: <br> <br> ${enumerateRecipientFields (PaymentGateway.SelectedCatalogItem.Fields, _.fieldValueTemplate, false)} <br>", 
				"checkData": "Verify the correctness of data", 
				"areYouAgree": "Do you agree?" ,
				"selectValidationFailed" : "Failed validation"
			},			
			"selectPaymentMethod": { 
				"fixedSummWarning": "Attention! <br> Payment on a fixed <b> </b> worth ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'short')}<br>The balance must be credited to the account of another beneficiary. <br> Proceed to pay?",
				"minSummWarning": "Attention! <br> <b> Min </b> Payment Amount ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'short')} <br>The balance will be credited to the next period. <br> Proceed to pay?", 
				"card": "Credit Card", 
				"cash": "Cash",
				"wallet": "Purse",
				"noPaymentMethod": "For a given destination, none of the payment methods available",
				"selectPMTitle" : "Select payment method"
			},
			
			"acceptCashPayment": { 
				"statusRecipient" : "Enter a sum to make payment for following recipient: ${_.rCollection.Item(0).Name}",			
				"unableToAccept": "Payment is currently impossible <br> please try later",
				"maxSummWarning": "The maximum payment is ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'short')}<br> You can enroll funds made another payment,", 
				"minSummWarning": "The minimum payment is ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'short')} <br> Therefore, the first note should be at least ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}", 
				"rejectOnReason4": "To make a payment required,", 
				"firstNoteMessage": "<font class='colorText'> (first note should be at least ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}).</font>", 
				"payLimitsMessageFirstNote": "Warning: The minimum payment ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}"+ 
						"<font Class='colorText'> (first note should be at least ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}).</font>" + 
						"the maximum amount of ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')}" + 
						"if you make more than the sum of ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')} hold payment automatically" ,
				"payLimitsMessageNoChangeFirstNote": "Warning: The minimum payment ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}"+ 
						"<font Class='colorText'> (first note should be at least ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}). </font>" + 
						"the maximum amount of ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')}", 
				"payLimitsMessage": "Warning: The minimum payment ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}" + 
						"the maximum amount of ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')}" + 
						"if you make more than the sum of ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')} hold payment automatically" ,
				"payLimitsMessageNoChange": "Warning: The minimum payment ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}" + 
						"the maximum amount of ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')}", 
				"maxSumm": "The maximum payment is ${getCurrency().format(_.rCollection.Item(0).MaxLimit, 'short')}", 
				"minSumm": "The minimum payment is ${getCurrency().format(_.rCollection.Item(0).MinLimit, 'short')}", 
				"sumPresent": "accumulated amount sufficient for payment <br> make a payment?" ,
				"fixedSummWarning": "Attention! The payment of fixed amounts: ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'short')} The balance of payment must enroll at the expense of another beneficiary." ,
				"recommendedSummWarning": "Warning: The recommended amount of the payment of at least ${getCurrency().format(PaymentGateway.SelectedRecipient.TransferSum, 'short')} Minimum payment ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull ')}", 
				"freeSummWarning": "Warning: The minimum payment ${getCurrency().format(PaymentGateway.SelectedRecipient.MinLimit, 'majorOnlyFull')}", 
				"maxPaymentSum": "The maximum amount of ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')}", 
				"automaticPayment": "If you make more than the sum of ${getCurrency().format(PaymentGateway.SelectedRecipient.MaxLimit, 'majorOnlyFull')} hold payment automatically", 
				"fixedMinSummWarning": "Warning: The amount to be paid: ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum, 'short')}. You have to contribute an amount not less than ${getCurrency().format(PaymentGateway.SelectedRecipient. PaymentSum, 'short')} (eg ${getCurrency().format(Math.ceil (PaymentGateway.SelectedRecipient.PaymentSum/10) * 10, 'short')}). amount of the overpayment will be credited to your account the next period. When you make the correct payment amount is automatically hold ", 
				"insertMoney": "Put money on a bill " ,
				"paymentSumStr": "To be paid:" ,
				"transferSumStr": "to transfer", 
				"commissionSumStr": "The Commission", 
				"accumulatedSumStr": "Accepted", 
				"remainSumStr": "It remains to make" ,
				"toGoRecipients": "To select a different destination - click «Back»<br>", 
				"continueOrPayFree": "To make money - click «Add notes»<br> To make a payment on ${getCurrency().format(PaymentDevice.AccumulatedSum + PaymentDevice.ChangeSum, 'short')} - click on «Payment»<br>If you do choose to hold the payment over ${Math.round (getParameter ('acceptCash.waitTimeout', 60000)/2000)} seconds ",
				"sumLeftFixed": "You make a left ${getCurrency().format(PaymentGateway.SelectedRecipient.PaymentSum - PaymentDevice.AccumulatedSum - PaymentDevice.ChangeSum)} <br>" 

			},
			
			"acceptWalletPayment": { 
				"statusSumEntry": "Enter the payment amount (balance of ${getCurrency().format(PaymentGateway.CustomerBalance)}):", 
				"statusSumLeft": "The remaining amount", 
				"statusRecipient": "Enter the amount for payment to ${PaymentGateway.SelectedRecipient.Name}", 
				"maxBalanceLimitReached": "You can not enter amount more than ${getCurrency().format( maxLimit() )}" 
			},
			"acceptCardPaymentUnibank" : {
				"confirmSum" : "Confirm the operation amount"
			},
			"acceptPaymentManual": { 
				"partyPayment": "Separation of the sum:" ,
				"sumLeft": "The remaining amount", 
				"maxBalanceLimitReached": "You can not enter amount more than ${getCurrency().format( maxLimit() )}" 
			},
			
			"sumEntry" : {
				"paymentSumStr": "To be paid:" ,
				"transferSumStr": "to transfer", 
				"commissionSumStr": "The Commission", 	
				"maxSumm": "The maximum payment is ${getCurrency().format(_.recipient.MaxLimit, 'short')}", 
				"minSumm": "The minimum payment is ${getCurrency().format(_.recipient.MinLimit, 'short')}", 				
				"status" : "Entering payment sum",
				"fixedSumRecipient" : "You are going to make payment for the recipient with fixed sum:<br><br><b>${generatePaymentInfo(PaymentGateway.SelectedRecipient.Fields, _.paymentInfoTemplate, _.fieldValueTemplate, false)}<br><br>Сумма к списанию: ${getCurrency().format(_.recipient.PaymentSum)}</b><br><br>Do you agree?",
				"statusRecipient" : "Enter a sum to make payment for following recipient: ${PaymentGateway.SelectedRecipient.Name}",
				"maxBalanceLimitReached" : "You can not enter amount more than ${getCurrency().format( maxLimit() )}"
			},			

			"payment": { 
				"executingMethod" : "Please wait...",			
				"paymentInProgress": "There is a realization of payment, please wait ....",
				"statusForCollectionCase" : "The payment process of recipient collection is in progress",
				"tableHeader" : "The payment process of recipient collection:",
				"payOk" : "Success",
				"payFail" : "Failed",
				"payDeferred" : "The payment has been deferred"				
			},			
			
			"peerToPeer" : {
				"operationFailed" : "Transfer failed. Check recipient's card number or try later."
			},			
			
			"dispense": { 
				"deviceError": "The sum of ${getCurrency().format(_.changeSum)} can not be issued due to device error. <br> Your change you can enroll at the expense of another recipient, or contact your system administrator", 
				"pleaseWait": "Wait for the issuance of money without leaving the terminal,", 
				"alertMessage": "Wait for the issuance of not departing from the terminal to issue <br>: <br> ${getCurrency().format(_.changeSum)}", 
				"noRequiredSum": "The sum of ${getCurrency().format(_.changeSum)} can not be issued because of the lack of the required number of notes for the issuance of delivery <br> Contact your administrator", 
				"dispenseError": "Error issuing amount of ${getCurrency().format(_.changeSum)} <br> (${_. errorText}) <br> Take the receipt issued by the <br> Contact your administrator", 
				"dispenseSuccess": "The sum of ${getCurrency().format(_.changeSum)} successfully issued <br> <b> <font color='red'> Take notes in the tray </font> </b > ", 
				"dispenseErrorAfter": "An error occurred issuing banknotes issued a receipt <br> Take <br> undistinguished sum ${getCurrency().format(_.unDispensedSum)} <br> will be credited to your account", 
				"dispenseErrorAfterUtinet": "An error occurred issuing banknotes issued a receipt <br> Take <br> undistinguished sum ${getCurrency().format(_.unDispensedSum)} <br> will be refunded. Refer to the manager.",
				"billReturn" : "Insert bill with another value."				
			}, 

			"insertCard": { 
				"insertCardTitle": "Enter the customer's card,", 
				"pleaseInsert": "Insert your card", 
				"takeYourCard": "Take your card", 
				"cardReaderFailure": "At the moment the operation is impossible due to hardware failure,", 
				"startingApplication" : "<b>Starting application...</b>",				
				
				"insertCardErrors" : {
					"1" : "Card could not be read or inserted inproperly",
					"2" : "Card is expired, contact issuer",
					"3" : "Card is not in service, contact issuer",
					"__undefined__" : "Card is not in service"
				}
			},

			"cardEject": {
				"takeYourCard": "Take your card",
				"captureTimeout": "If a card is ejected during <span id=\"captureTick\">${getParameter('card.captureTimeout', 30)}</span> seconds card will be charged the terminal,",
				"cardCapturing": "Card capturing...."
			},
			
			"cardPayment" : {
				"showBalance" : "Your balance : ",
				"__undefined__" : "Payment failed. Unable to authorize."	
			},				
			
			"cardCaptured" : {
				"message" : "Card has been kept back, please call the following number:",
				"status" : "Card being kept back",
				"servicePhone" : "+* (***) ***-**-**"
			},			
			
			"pinEntry": {
				"pinEntryTitle": "Enter PIN-code cards,",
				"pinEntryHint": "Enter PIN",
				"pinCancelled": "Entering the PIN canceled"
			},
			
			"verifyCard": {
				"genericError": "Failed validation",
				"cardVerifyHint": "There is a card verification, please wait ....",
				"verifyError": "Authorization error on the server of the bank cards,",
				"backToReturn": "Click «Back» to return the card",
				"cardVerifyTitle": "Checking on the server of the bank",
				
				"errors" : {

					"__undefined__" : "Unable to authorize"
				
				}
				
			},
			
			"printInfo": {
				"printerOffline": "At this time, printing is not possible, we apologize, ",
				"terminalNr": "Terminal № ",
				"receiptNr": "   Receipt № ",
				"terminalAddress": "Address: ",
				"date": "Date: ",
				"time": "   Time: ",
				"thanks": "THANK YOU FOR YOUR COOPERATION!",
				"serviceLine": "Hot Line ",
				"paymentSum" : "Amount: ",
				"changeSum" : "Change: ",
				"commissionSum" : "Commission: ",
				"transferSum" : "Transfer: "
			},
			
			"printCoupon": {
				"printDone" : "Take your receipt",
				"printFailed" : "Receipt cannot be printed due to technical failure"			
			},
			
			"ministatement" : {
				"makeChoice" : "<b>Request finished successfully<br><br>How do you want to get information received?</b>",
				"printDone" : "<b>Take your receipt</b>",
				"failed" : "<b>Request failed</b>",
				"printerFailure" : "Printer failure. Ministatement cannot be printed now."
			},	
			
			"showBalance" : {
				"makeChoice" : "<b>Request finished successfully<br><br>How do you want to get information received?</b>",
				"printDone" : "<b>Take your receipt</b>",
				"printerFailure" : "Printer failure. Balance receipt cannot be printed now."
			},			
			
			"recipients-fixedsum-collection" : {
				"totalAmount" : "Total quantity",
				"totalSum" : "Total sum",
				"resetAll" : "Reset all"
			},			
				
			"serviceMenu": {
				"reports": "Magazines",
				"devices": "Devices",
				"setup": "Settings",
				"exit": "Exit",
				"payments": "Payments",
				"bills": "Denominations",
				"billsDispense": "Issuance of banknotes",
				"cardCapture": "Capture cards",
				"printer": "printer",
				"fiscal": "Fiscal Registrar",
				"billacceptor": "Bill acceptor",
				"dispenser": "Dispenser bills",
				"payKioskExit": "'Out of PayBox'",
				"onScreenKeyboard": "'On-Screen Keyboard'",
				"runExplorer": "'Start the Explorer'",
				"removeWinLocks": "'Remove the lock Windows'",
				"withDesktop": "'On the desk,'",
				"withoutDesktop": "'No Wallpaper'",
				"keyboardComWarning": "Warning: When you restart PayKiosk running on-screen keyboard port COM1 is busy on-screen keyboard!",
				"property": "Property",
				"status": "status",
				"interface": "interface",
				"receiptPrinterPresent" : "${ReceiptPrinter ? \"found\" : \"not found\"}",
				"authFailed": "Invalid code",
				"authorize": "Authorize",
				"clear": "Clear",
				"encashment": "Collection",
				"unablePrintReceipt": "Receipts will not be printed. The printer is defective.",
				"printJournalAgain": "Reprint of the journals",
				"billAcceptor": "Bill acceptor",
				"coinAcceptor": "Coin",
				"billDispenser": "Issuance of banknotes",
				"hoppers": "Issuance of coins",
				"enterStampNumber": "Enter the number of seals installed (blank) tapes",
				"skipDevice": "Skip"

			},
			
			"balanceAfterDeposit" : {
				"hint" : "Sending request to the server..."
			},			
			
			"offline" : {
				"status" : "Terminal is temporary unavailable",			
				"message" : "Terminal is temporary unavailable<br>Terminal has been switched to Offline mode"
			},
			"information" : {
				"info" : 'Your information here'
			}			
		
	},		
		
		{
			// number должно быть целым числом		
			"getNumberName" : function(number, names) {
				// склоняемое название
				var n = number % 100;
				if ((n > 4) && (n < 21)) return names[2];
				
				n %= 10;
				if ((n === 0) || (n > 4)) return names[2];
				if (n === 1) return names[0];
				return names[1];
			},
			
			"currency" : {
				"RUR" : {
					"MajorUnit" : ["rouble", "roubles", "roubles"],
					"MinorUnit" : ["kopeck", "kopecks", "kopecks"],
					"MajorUnitShort" : "rub.",
					"MinorUnitShort" : "kop.",
					"MajorUnitSign" : "r.",
					"MinorUnitSign" : "k."
				},
				"RUB" : {
					"MajorUnit" : ["rouble", "roubles", "roubles"],
					"MinorUnit" : ["kopeck", "kopecks", "kopecks"],
					"MajorUnitShort" : "rub.",
					"MinorUnitShort" : "kop.",
					"MajorUnitSign" : "r.",
					"MinorUnitSign" : "k."
				},
				"AZN" : {
					"MajorUnit" : ["manat", "manats", "manats"],
					"MinorUnit" : ["gyapik", "gyapiks", "gyapiks"],
					"MajorUnitShort" : "man.",
					"MinorUnitShort" : "g.",
					"MajorUnitSign" : "m.",
					"MinorUnitSign" : "g."
				},
				"USD" : {
					"MajorUnit" : ["USD", "USD", "USD"],
					"MinorUnit" : ["cent", "cent", "cent"],
					"MajorUnitShort" : "$",
					"MinorUnitShort" : "¢",
					"MajorUnitSign" : "$",
					"MinorUnitSign" : "¢"
				},
				"EUR" : {
					"MajorUnit" : ["EUR", "EUR", "EUR"],
					"MinorUnit" : ["cent", "cent", "cent"],
					"MajorUnitShort" : "€",
					"MinorUnitShort" : "¢",
					"MajorUnitSign" : "€",
					"MinorUnitSign" : "¢"
				}
			}
		}
);