﻿( function() {
/**
Отображается информационное сообщение о необходимости подождать проверку карты
*/
_.addEventListeners({
	"load" : function() {

		document.getElementById("status").innerHTML = getMessage('verifyCard.cardVerifyTitle');
		document.getElementById("data").innerHTML = getMessage('verifyCard.cardVerifyHint');
		
		_.addEventListeners({
			"actionsChanged" : ViewButtons.update,
			"operationCompleted" : onOperationComplete,
			"executingVirtuPOSMethod" : function() {
				hideAlert();
				showAlert(getMessage('VirtuPOS.executingVirtuPOSMethod'));
			},
			"tryVirtuPOSMethodAgain" : function() {
				hideAlert();
				showAlert(getMessage('VirtuPOS.tryVirtuPOSMethodAgain'));
			}
		});
		
		ViewButtons.bind({'cancelButton' : 'cancel', 'continueButton' : 'commit'});
	},
	
	"key" : function(key) {
		switch(key) {
			// backspace
			case 8 :
			//esc
			case 27 :
				_['cancel']();
				break;
			case 13:
				_['continue']();
				break;
		}
		return false;
	}
});

function onOperationComplete(result) {

		if (!_.operationSucceeded) {
		hideAlert();
		showAlert(getMessage('verifyCard.errors', result), 
			{
				"ok" : _["continue"]
			},
			{
				13 : "ok"
			},
			"ok",
			15000
		);
		return false;
	}
	
};

})();