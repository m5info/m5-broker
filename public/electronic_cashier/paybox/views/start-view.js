﻿(function() {

_.addEventListeners({
	"load" : function() {
		_.addEventListeners({
			"operationComplete" : onOperationComplete
		});		
		
	}
});

function onOperationComplete() {

	document.getElementById("devName").innerHTML = getMessage("start.devName");
	document.getElementById("devState").innerHTML = getMessage("start.devState");	

	if (_.devInfo.hasOwnProperty("Printer")) {
		var devState = {
			1 : ["dssUndefined", "normal"],
			2 : ["dssDisabled_Initialize", "normal"],
			3 : ["dssIdle_Normal", "normal"],
			10 : ["dssIdle_Suspended", "normal"],
			4 : ["dssIdle_PaperNearEnd", "warning"],
			5 : ["dssBusy_Printing", "warning"],
			6 : ["dssBusy_Holding", "warning"],
			7 : ["dssError_NoDevice", "error"],
			8 : ["dssError_DeviceFailure", "error"],
			9 : ["dssError_PaperOut", "error"]
		};
		document.getElementById("printerName").className = "devName";
		document.getElementById("printerName").innerHTML = getMessage("start.printerName");
		document.getElementById("printerState").className = "devState";
		document.getElementById("printerState").innerHTML = getMessage("deviceStatus.Printer." + devState[_.devInfo.Printer][0]);
		
		switch (devState[_.devInfo.Printer][1]) {
			case "normal" :
				document.getElementById("printerState").className += " green";
				break;
			case "warning" :
				document.getElementById("printerState").className += " yellow";
				break;
			case "error" :
				document.getElementById("printerState").className += " red";
				break;
		}
	}
		
	if (_.devInfo.hasOwnProperty("billAcceptor")) {
		var devState = {
			1 : ["dssUndefined", "normal"],
			21 : ["dssDisabled_PowerUp", "normal"],
			2 : ["dssDisabled_Initialize", "normal"],
			3 : ["dssDisabled_Inhibit", "normal"],
			4 : ["dssBusy_Accepting", "warning"],
			5 : ["dssBusy_RejectingInhibit", "warning"],
			6 : ["dssBusy_Rejecting", "warning"],
			7 : ["dssBusy_Escrow", "warning"],
			8 : ["dssBusy_Stacking", "warning"],
			9 : ["dssBusy_Stacked", "warning"],
			10 : ["dssBusy_Returning", "warning"],
			11 : ["dssBusy_Returned", "warning"],
			12 : ["dssBusy_Paused", "warning"],
			13 : ["dssBusy_Holding", "warning"],
			22 : ["dssBusy_dssDispensing", "warning"],
			23 : ["dssBusy_dssDispensed", "warning"],
			24 : ["dssBusy_dssDispensedComplete", "warning"],
			25 : ["dssBusy_dssUnloading", "warning"],
			26 : ["dssBusy_dssUnloaded", "warning"],
			27 : ["dssBusy_Packed", "warning"],
			14 : ["dssError_NoDevice", "error"],
			15 : ["dssError_DeviceFailure", "error"],
			16 : ["dssError_StackerFull", "error"],
			17 : ["dssError_ValidatorJammed", "error"],
			18 : ["dssError_StackerJammed", "error"],
			19 : ["dssError_StackerRemoved", "error"],
			20 : ["dssIdle_Normal", "normal"],
			21 : ["dssIdle_Cassette", "normal"],
			22 : ["dssIdle_CassetteNormal", "normal"],
			23 : ["dssIdle_CassetteNearEnd", "warning"],
			24 : ["dssIdle_CassetteNotPresent", "warning"],
			25 : ["dssIdle_CassetteInvalid", "error"],
			26 : ["dssError_Cassette1NotPresent", "error"],
			27 : ["dssIdle_Sensor", "normal"],
			28 : ["dssIdle_Suspended", "normal"],
			29 : ["dssError_ShutterFailure", "error"],
			30 : ["dssError_PoolFailure", "error"],
			31 : ["dssError_PotentiometerFailure", "error"]
		};
		document.getElementById("billacceptorName").className = "devName";
		document.getElementById("billacceptorName").innerHTML = getMessage("start.billacceptorName");
		document.getElementById("billacceptorState").className = "devState";
		document.getElementById("billacceptorState").innerHTML = getMessage("deviceStatus.billAcceptor." + devState[_.devInfo.billAcceptor][0]);
		
		switch (devState[_.devInfo.billAcceptor][1]) {
			case "normal" :
				document.getElementById("billacceptorState").className += " green";
				break;
			case "warning" :
				document.getElementById("billacceptorState").className += " yellow";
				break;
			case "error" :
				document.getElementById("billacceptorState").className += " red";
				break;
		}		
			
	}	
	
	if (_.devInfo.hasOwnProperty("VirtuPOS")) {
		var devState = {
			1 : ["available", "normal"],
			0 : ["unavailable", "error"]
		};
		document.getElementById("virtuposName").className = "devName";
		document.getElementById("virtuposName").innerHTML = getMessage("start.virtuposName");
		document.getElementById("virtuposState").className = "devState";
		document.getElementById("virtuposState").innerHTML = getMessage("deviceStatus.VirtuPOS." + devState[_.devInfo.VirtuPOS][0]);
		
		switch (devState[_.devInfo.VirtuPOS][1]) {
			case "normal" :
				document.getElementById("virtuposState").className += " green";
				break;
			case "error" :
				document.getElementById("virtuposState").className += " red";
				break;
		}			
			
	}

}
})();