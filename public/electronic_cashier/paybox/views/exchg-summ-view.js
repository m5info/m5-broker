﻿( function() {

_.addEventListeners({
	"load" : function() {
	
		// _.registerVariables({
		// });	
		_.addEventListeners({
			"key" : function(key) {
				switch(key) {
					// backspace
					case 8:
					case 46:
						_["removeLastChar"]();
						return false;
					case 13:
						if (_.mayExecute('commit')) {
							_['commit']();
						} else {
							playSoundFunction("error.wav", false);
						}	
						return false;
					//esc	
					case 27:
						_['cancel']();
						return false;
				}
			},
			"char" : function(ch) {
				if (!_["appendChar"](ch))
					playSoundFunction("error.wav", false);
				return false;
			},
			"sumChanged" : function () {
				updateView();
			},
			"maxLimitReached" : function() {
					showAlert(getMessage("sumEntry.maxBalanceLimitReached"),
					{ "OK": hideAlert },
					{ 13 : "OK", 27 : "OK" }, 
					"OK", 
					7000			
				);
			}
		});
		
		ViewButtons.bind({
			"cancelButton" : "cancel",
			"continueButton" : "commit"
		});
		
		// if (_.recipient.TransferAmountType == 1) {
				// showAlert(getMessage("sumEntry.fixedSumRecipient"),
			// {
				// "yes" : _['commit'],
				// "no" : _['cancel']
			// },
			// {
				// 13 : "yes",
				// 13 : "yes",
				// 27 : "no"
			// },
			// "no"	
			// );
			// document.getElementById("alertMessageDiv").className = "fixed";
		// }		
		
	},
	
	"run" : function () {
		initialDisplay();
		ViewButtons.update();
	},	
	
	"actionsChanged" : ViewButtons.update	
});

function initialDisplay() {
	showKeypad();
	var keyPadDiv = document.getElementById("keypadDiv");
	keyPadDiv.className = "keyPadDiv";
	// SummTable
	document.getElementById('paymentSumStr').innerHTML = getMessage("currencyExchange.exchangeAmount");
	document.getElementById('amountTargetStr').innerHTML = getMessage("currencyExchange.targetAmount");
	document.getElementById('amountDispenseStr').innerHTML = getMessage("currencyExchange.amountDispense");
	document.getElementById('amountChangeStr').innerHTML = getMessage("currencyExchange.amountChange");
	
	document.getElementById("ExchangeInfo").innerHTML = getMessage("currencyExchange.exchangeHint");
	document.getElementById("ExchangeRateInfo").innerHTML = getMessage("currencyExchange.ratesHint");
	
	if (isRoublesTarget())
		document.getElementById('SummTable').rows(2).style.display="none";
	
	// if (_.recipient.MinLimit >0)	
		// commissionText.innerHTML += "&nbsp;&nbsp;<img src='images\\bullet.gif'>&nbsp;"+getMessage("acceptCashPayment.minSumm")+"<br />";
	// if (_.recipient.MaxLimit >0)		
		// commissionText.innerHTML += "&nbsp;&nbsp;<img src='images\\bullet.gif'>&nbsp;"+getMessage("acceptCashPayment.maxSumm");
	
	updateView();
}

function updateView() {
	document.getElementById('PaymentSum').innerHTML = _.enteredSum.format();
	document.getElementById('AmountTarget').innerHTML = _.amountTarget.format(); // Поле a
	document.getElementById('AmountDispense').innerHTML = _.amountDispense.format(); // Поле б
	document.getElementById('AmountChange').innerHTML = _.amountChange.format(); // Поле в

}


})();