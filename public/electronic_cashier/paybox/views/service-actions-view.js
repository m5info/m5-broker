﻿function replaceWithMessages(s) {
	return s.replace(/\$\{([^}]+)\}/g,
				function (str, exp) {
					var msg = getMessage(exp);
					return msg ? msg : str;
				});
}
_.addEventListeners({
	// Обработчик загрузки текущего состояния
	"load" : function() {
		document.getElementById("topMenu").innerText = replaceWithMessages(document.getElementById("topMenu").innerText);
		document.getElementById("setupBody").innerHTML = replaceWithMessages(document.getElementById("setupBody").innerHTML);
	},
	"inactivityTimer" : function() {
		return false;
	},
	"run" : function() {},
	
	"unload" : function() {
	}
});
