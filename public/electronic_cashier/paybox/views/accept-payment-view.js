﻿( function() {

var fieldValueTemplate;
var commissionSumStr;
var transferSumStr;
var accumulatedSum;
var remainSum;
var commissionSum;
var transferSum;
var commissionText;
//var partyPayment;
var paymentSum;
var paymentInfoHeight;

_.addEventListeners({
	"load" : function() {
		fieldValueTemplate = document.getElementById('fieldValueTemplate').innerHTML;
		commissionSumStr = document.getElementById('CommissionSumStr');
		transferSumStr = document.getElementById('TransferSumStr');
		paymentSum = document.getElementById('PaymentSum');
		accumulatedSum = document.getElementById('AccumulatedSum');
		remainSum = document.getElementById('RemainSum');
		commissionSum = document.getElementById('CommissionSum');
		transferSum = document.getElementById('TransferSum');
		commissionText = document.getElementById('commissionText');
		
		ViewButtons.bind({
			'cancelButton' : 'cancel',
			'continueButton' : 'commit',
			'menuButton' : 'menu'
			});		
	
		_.addEventListeners({
			"key" : function(key) {
					switch(key) {
						case 8:
							break;
						case 27:
							_['cancel']();
							break;
						case 13:
							if (_.mayExecute('commit')) _['commit']();
							break;
						}
			},
			"onAccumulatedSumChanged" : function (sum) {
				updateButtons();
				updateView();
			},
			"onPaymentComplete" : function () {
				updateButtons();
				updateView();
			},
			"unableToAccept" :  function(error) {
				if (!getParameter("dataCheck.showCheckError"))
					error = "";
				else if (error == "incorrectCollection")
					error = "<br>"+getMessage("acceptCashPayment.incorrectCollection");
				else if (error == "incorrectCurrency")
					error = "<br>"+getMessage("acceptCashPayment.incorrectCurrency");
				else if (error != "")
					error = "<br>"+error;
				showAlert(getMessage("acceptCashPayment.unableToAccept") + error,
					{
						"ok" : _["cancel"]
					},
					{
						27: "ok"
					},
					"ok"
				);				
			},
			"askMoreTime" : function() {
				showAlert(getMessage("acceptCashPayment.askMoreTime"),
				{
					"yes" : _['continuePay'],
					"no" : _['commit']
				},
				{
					13 : "yes",
					27 : "no"
				},
				"no"	
				);				
			},
			"onBillRejected" : function(reason, hint) {
				//предупреждения о мин и макс суммах и неправильной валюте платежа			
				switch (reason)	{
					case 2:
						showAlert(getMessage("acceptCashPayment.maxSummWarning"), {}, {}, null, 5000);
						break;
					case 3:
						showAlert(getMessage("acceptCashPayment.minSummWarning"), {}, {}, null, 5000);
						break;
					case 4:
						showAlert(getMessage("acceptCashPayment.rejectOnReason4")+hint, {}, {}, null, 5000);
						break;
					case 1:
					// Wrong no action
				}				
			}
		});

		initialDisplay();
		
	},
	"run" : function () {
		ViewButtons.update();
		updateView();
	},
	"actionsChanged" : function() {
		updateButtons();
	}
});

function initialDisplay() {
	// Status text
	document.getElementById("status").innerHTML = template('statusTemplate', getMessage("acceptCashPayment.insertMoney"));
		
	// SummTable
	transferSumStr.innerHTML = getMessage("acceptCashPayment.transferSumStr");
	commissionSumStr.innerHTML = getMessage("acceptCashPayment.commissionSumStr");
	document.getElementById("accumulatedSumStr").innerHTML = getMessage("acceptCashPayment.accumulatedSumStr");
	document.getElementById("paymentSumStr").innerHTML = getMessage("acceptCashPayment.paymentSumStr");
	document.getElementById("remainSumStr").innerHTML = getMessage("acceptCashPayment.remainSumStr");
	
	if (_.rCollection.Count == 1) {	
		document.getElementById("fieldsInfo").innerHTML = "<b>"+getMessage("acceptCashPayment.statusRecipient")+"</b><br>";
		if (getParameter("acceptCashView.showFieldsAcceptPayment", false)) {
			// Hint
			document.getElementById("fieldsInfo").innerHTML += "<table id='fieldsTable'>"+enumerateRecipientFields(_.rCollection.Item(0).Fields, fieldValueTemplate, false)+"</table>";
		} else {
			document.getElementById("fieldsInfoDiv").style.display = "none";
		}						
	}
	
	if (!getParameter("acceptCashView.showWithComission", true)) {
		commissionSumStr.className = "invisible";
		transferSumStr.className = "invisible";
	} else {
		transferSum.innerHTML = getCurrency().format(_.transferSum);
		commissionSum.innerHTML = getCurrency().format(_.commissionSum);		
	}

	if (_.acceptMode.indexOf("fixed") < 0) {
		document.getElementById("remainSumStr").className = "invisible";
		remainSum.className = "invisible";
	}
	
	accumulatedSum.innerHTML = getCurrency().format(PaymentDevice.AccumulatedSum(_.rCollection.Item(0).CurrencyCode).Value);	
	paymentSum.innerHTML = getCurrency().format(_.paymentSum);
	remainSum.innerHTML = getCurrency().format(_.remainSum);
	
	if (_.acceptMode == "free-recommended-one") {
		document.getElementById('productPaymentWarning').className = "visible";
		document.getElementById('productPaymentWarning').innerHTML = getMessage("acceptCashPayment.recommendedSummWarning");
	}
	
	commissionText.innerHTML = "";
	if (_.rCollection.Count == 1) {
		var Recipient = _.rCollection.Item(0);
		/* Commissions */
		var commissionRules = returnCommissionRules(Recipient, Recipient.CurrencyCode);
		for (var i = 0; i < commissionRules.length; i++) {
			commissionText.innerHTML += "&nbsp;&nbsp;<img src='images\\bullet.gif'>&nbsp;" + commissionRules[i] + "<br />";
		}
		
		if (_.acceptMode != "fixed-col") {
			if (Recipient.MinLimit >0)
			commissionText.innerHTML += "&nbsp;&nbsp;<img src='images\\bullet.gif'>&nbsp;"+getMessage("acceptCashPayment.minSumm")+"<br />";
			if (Recipient.MaxLimit >0)
			commissionText.innerHTML += "&nbsp;&nbsp;<img src='images\\bullet.gif'>&nbsp;"+getMessage("acceptCashPayment.maxSumm");
		}
		
	} else
		document.getElementById('commissionTextDiv').className = "invisible";

	// расчёт высоты блока paymentInfo
	var top = document.getElementById("paymentInfo").currentStyle.top;
	var bottom = document.body.clientHeight - document.getElementById("footer").clientHeight;
	top = Number(top.substring(0, top.length-2));
	paymentInfoHeight = bottom - top;
		
	updateButtons();
	updateView();		
	alignContent();
		
}

function updateView() {
	accumulatedSum.innerHTML = getCurrency().format(_.acceptedSum);
	remainSum.innerHTML = getCurrency().format(_.remainSum);
	transferSum.innerHTML = getCurrency().format(_.transferSum);
	commissionSum.innerHTML = getCurrency().format(_.commissionSum);
	paymentSum.innerHTML = getCurrency().format(_.paymentSum);
}

function alignContent() {	
	// определение суммарной высоты содержимого
	var fieldsInfoHeight = document.getElementById("fieldsInfoDiv").offsetHeight;
	var commissionTextDiv = document.getElementById("commissionTextDiv");	
	var SummTable = document.getElementById("SummTable");
	var contentHeight = fieldsInfoHeight + SummTable.offsetHeight + commissionTextDiv.offsetHeight;
	// выравнивание блоков по высоте, либо уменьшение шрифта в таблице полей
	if (paymentInfoHeight > contentHeight) {
		fieldsInfoHeight = document.getElementById("fieldsInfoDiv").offsetHeight;
		var freespace = paymentInfoHeight - contentHeight;
		SummTable.style.top = freespace/2;
		commissionTextDiv.style.top = freespace;	
	} else if (paymentInfoHeight < contentHeight) {
		// resizeFont();
	}
}


function resizeFont() {
	// уменьшение шрифта в таблице полей
	var currentFontSize = document.getElementById("fieldsTable").currentStyle.fontSize;
	currentFontSize = currentFontSize.substring(0, currentFontSize.length-2);
	if (currentFontSize > 11)
		document.getElementById("fieldsTable").style.fontSize = currentFontSize - 1;
	else
		document.getElementById("fieldsInfo").innerHTML = "<b>"+getMessage("acceptCashPayment.statusRecipient")+"</b>";
	
	alignContent();
}

function updateButtons() {

	ViewButtons.update();

	//document.getElementById('partyPayment').className = "partyPayment "+ _.mayExecute('partyPayment') ? "visible" : "hidden";
}

})();