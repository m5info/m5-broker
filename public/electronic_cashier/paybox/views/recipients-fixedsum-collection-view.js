﻿(function() {

// Шаг "ползунка" скроллера
var bulletStep;

_.addEventListeners({
	"load" : function() {
		if (!PaymentGateway.SelectedCatalogItem)
			return;
		
		// if (getParameter("paykiosk.showBanner", true)) {
			// document.getElementById('banner').innerHTML = '<embed src="images/banner.swf" type="application/x-shockwave-flash" width=100% height=100 onfocus="focus()"/>';
		// } else {
			// document.getElementById('banner').innerHTML = '';
		// };
		
		_.addEventListeners({
			"updateView" : updateView,
			"unableToPay" : function() {
				showAlert(getParameter('recipients.unableToPayWithReason', false)
						? getMessage('recipients.unableToPay')
						: getMessage('recipients.unableToPayPrinterOffline'),
						{ "ok" : _['cancel'] },
						{ 13 : "ok" },
						"cancel"
				);		
			}
		});

		_.registerActions({
			"scroll": scroll
		});
		ViewButtons.bind({
			'cancelButton' : 'cancel', 
			'continueButton' : 'continue'
		});
		
		_.collection["size"] = Math.round(_.catQuont/2);
		_.collection["position"] = 1;
		bulletStep = Math.round((document.getElementById("scrollPosition").offsetHeight - 2*document.getElementById("scrollBullet").offsetTop - document.getElementById("scrollBullet").offsetHeight)/(_.collection["size"]-3));
		document.getElementById("scrollBullet").style.pixelTop = document.getElementById("scrollBullet").offsetTop;
		
		document.getElementById("totalNumberStr").innerHTML = getMessage("recipients-fixedsum-collection.totalAmount")+":<div id='totalNumber'>"+_.itemCount+"</div>";
		document.getElementById("totalSumStr").innerHTML = getMessage("recipients-fixedsum-collection.totalSum")+":<div id='totalSum'>"+getCurrency("RUR").format(0, 'sign')+"</div>";
		document.getElementById("resetButton").innerHTML = getMessage("recipients-fixedsum-collection.resetAll");
		
		if (_.collection["size"] < 4) {
			document.getElementById("scrollbox").className = "invisible";
		}
			
		drawCatalog();
		
	},
	
	"actionsChanged" : ViewButtons.update,
	
	"inactivityTimer" : function() {
		ViewButtons.update();
		return true;
	}
});

function drawCatalog() {

	document.getElementById('groupStatus').innerHTML = getMessage("recipientsView.groupStatusText");
	
	var balanceStatus = document.getElementById('balanceStatus');
	if (PaymentGateway.CustomerLoggedIn) {
		balanceStatus.innerHTML = getMessage("paykiosk.balanceFormat");
		balanceStatus.className = "visible";
	}

	var changeSumStatus = document.getElementById('changeSumStatus');
	if (PaymentDevice.HasAccumulatedSum) {
		changeSumStatus.innerHTML = template("changeInfoTemplate", getMessage("paykiosk.changeSum"));
		changeSumStatus.className = "visible";
	}
	
	// Кэшируем шаблон для использования в этой функции
	var drawRecipient = template('collectRecipientTemplate');
	var table= "";
	
	with (PaymentGateway.SelectedCatalogItem.ChildItems) {
		for(var i = 0; i < Count; ) {
			for (var j = 0; i < Count && j < 2;) {
		
				if (!_.collection.hasOwnProperty(i)) {
					// Неправильный получатель
					i++;
					continue;
				};
			
				// А теперь вставляем получателя
				var thumbnailURL = Item(i).Thumbnail.URL && Item(i).Thumbnail.URL.length > 0 ? Item(i).Thumbnail.URL : null;
				
				var thumbnailPath = Framework.IfExistsThen("images-recipients/" + getActiveLocale() + "/" + thumbnailURL, "zzz", window);
				if ("zzz" === thumbnailPath) {
					thumbnailPath = Framework.IfExistsThen("images-recipients/" + thumbnailURL, "zzz", window);

					if ("zzz" === thumbnailPath)
						thumbnailPath = null;
				}

				var plussrc = "";
				
				if (_.collection[i]["number"] == _.collection[i]["max"]) {
					plussrc = "images/dont_plus.png";
				} else {
					plussrc = "images/do_plus.png";					
				};
				
				table += drawRecipient({
					"thumbnailWidth" : Item(i).Thumbnail.Width && Item(i).Thumbnail.Width > 0 ? Item(i).Thumbnail.Width : 240,
					"thumbnailHeight" : Item(i).Thumbnail.Height && Item(i).Thumbnail.Height > 0 ? Item(i).Thumbnail.Height : 140,
					"thumbnailURL" : thumbnailPath,
					"name" : Item(i).Name,
					"idx" : i,
					"locale" : getActiveLocale(),
					"plussrc" : plussrc,
					"transferSum" : getCurrency("RUR").format(_.collection[i]["price"], 'sign')
				});
				i++; j++;
			}
			table += '<br><br><br>';
		}
	}
	
	document.getElementById("data").innerHTML = table;
}

function scroll(way) {

	if (way == "down") {
		if (_.collection["position"] == _.collection["size"]-2) return;
		document.getElementById("data").style.pixelTop -= 217;
		_.collection["position"]++;
		document.getElementById("scrollBullet").style.pixelTop += bulletStep;
	} else if (way == "up") {
		if (_.collection["position"] == 1) return;
		document.getElementById("data").style.pixelTop += 217;
		_.collection["position"]--;
		document.getElementById("scrollBullet").style.pixelTop -= bulletStep;
	}	
	
}

function updateView(idx) {
	for (var i = idx == "reset" ? 0 : idx, c = idx == "reset" ? PaymentGateway.SelectedCatalogItem.ChildItems.Count : idx+1; i<c; i++) {
		var counterElement = document.getElementById("counter"+i);
		if (!counterElement) continue;
		
		counterElement.innerHTML = _.collection[i]["number"];

		if (_.collection[i]["number"] == 0) {
			document.getElementById("minus"+i).src = "images/dont_minus.png";
		} else {
			document.getElementById("minus"+i).src = "images/do_minus.png";					
		};
		if (_.collection[i]["number"] == _.collection[i]["max"]) {
			document.getElementById("plus"+i).src = "images/dont_plus.png";
		} else {
			document.getElementById("plus"+i).src = "images/do_plus.png";					
		};		
		
	}
	document.getElementById("totalNumber").innerHTML = _.itemCount;	
	document.getElementById("totalSum").innerHTML = getCurrency("RUR").format(_.totalSum, 'sign');
}

})();