﻿(function() {
var cardWaiting = getParameter("paykiosk.cardWaiting", false);

// Количество получателей в строке
var recipientsPerRow;
// Максимальное количество получателей на странице
var recipientsPerPage;

_.addEventListeners({
	"load" : function() {
		if (!PaymentGateway.SelectedCatalogItem)
			return;
		
		Framework.Variables("EjectReason") = "";
			
		// Количество получателей в строке
		recipientsPerRow = 2;
		// Максимальное количество получателей на странице
		recipientsPerPage = recipientsPerRow * 1;
		// Текущая отображаемая страница (от 0 до _.lastPageNr )
		_.currentPageNr = 0;
		// Номер страницы на которой еще есть данные
		_.lastPageNr = Math.floor((PaymentGateway.SelectedCatalogItem.ChildItems.Count + recipientsPerPage - 1) / recipientsPerPage) - 1;
		
		// if (getParameter("paykiosk.showBanner", true)) {
			// document.getElementById('banner').innerHTML = '<embed src="images/banner.swf" type="application/x-shockwave-flash" width=100% height=100 onfocus="focus()"/>';
		// } else {
			// document.getElementById('banner').innerHTML = '';
		// };

		_.addEventListeners({
			"catalogItemChanged" : function(pageNr) {
				_.lastPageNr = Math.floor((PaymentGateway.SelectedCatalogItem.ChildItems.Count + recipientsPerPage - 1) / recipientsPerPage) - 1;
				_.currentPageNr = 0;
				updateView();
				ViewButtons.update();
			},
			"cardPositionChanged" : function() {
				updateView();
				ViewButtons.update();
			},
			"key" : function(key) {
				switch(key) {
					case 8:
						break;
					case 27:
						_['selectParent']();
						break;
					case 33:
						_['prevPage']();
						break;
					case 34:
						_['nextPage']();
						break;
				}
			},
			"unableToPay" : function() { 		
					showAlert(getParameter('recipients.unableToPayWithReason', false)
						? getMessage('recipients.unableToPay')
						: getMessage('recipients.unableToPayPrinterOffline'),
						{ "ok" : hideAlert },
						{ 13 : "ok" },
						"ok"
					);
			},
			"billAcceptorFailureAlert" : function() {
					showAlert(getMessage('recipients.billAcceptorFailureAlert'),
						{ "ok" : hideAlert },
						{ 13 : "ok" },
						"ok"
					);				
			},
			"showDescriptionOnSelected" : function() {
					showAlert(getMessage('recipients.descriptionOnSelect'),
						{
							"yes" : function() {
									if (_['processSelectedItem']()) {
										_.fireEvent('actionsChanged');
										_.fireEvent('catalogItemChanged');
									}
							},
							"no" : _['selectParent']
						},
						{
							13 : "yes",
							27 : "no"
						},
						"no"
					);			
			},
			"catalogItemAskSelect" : function() {
					showAlert(getMessage('recipients.catalogItemAskSelect'),
						{
							"yes" : function() {
								if (!_.leave("groupSelected")) {
									_.getLogger().debug("Выбор группы не обработан сценарием, продолжаем отображение в текущей реализации состояния");
									return false;
								}
							},
							"no" : function() {
								return false;
							}
						},
						{
							13 : "yes",
							27 : "no"
						},
						"no");			
			},
			"cardReject" : function() {
				showAlert(getMessage("recipientsView.insertCardErrors", _.cardRejectReason)+'<br><br>'+
					'<b>'+getMessage("recipientsView.takeYourCard")+'</b><br><br>'+
					'<img src="images/card-eject.gif"><br>'+
					'<span id="takeYourCard">'+getMessage("recipientsView.captureTimeout")+'</span>');	
			},
			"onlyOfficePaymentMessage" : function() {
				showAlert(getMessage("recipientsVEB.onlyOfficePaymentMessage"),
						{ "ok" : hideAlert },
						{ 13 : "ok" },
						"ok",
						7000
					);	
			},	
			"unableToPayDeposit" : function() {
				showAlert(getMessage("recipientsVEB.unableToPay"),
						{ "ok" : hideAlert },
						{ 13 : "ok" },
						"ok",
						7000
					);	
			},
			"unableToPayMaxSum" : function() {
				showAlert(getMessage("recipientsVEB.unableToPayMaxSum"),
						{ "ok" : hideAlert },
						{ 13 : "ok" },
						"ok",
						12000
					);	
			},
			"captureTick" : function (captureTimeout) {
				document.getElementById("captureTick").innerHTML = captureTimeout < 10 ? ("<font color='#FF0000'>" +captureTimeout+"</font>") : captureTimeout;
			},
			"startingApplication" : function(param) {
				if (param) {
					showAlert(getMessage("recipientsView.startingApplication") + '<br><br><img src="images/clock.png">');	
				} else
					hideAlert();
			}
		});

		_.registerActions({
			"nextPage" : new StateAction(
				function () {return _.currentPageNr < _.lastPageNr; },
				function () { setPageNr(_.currentPageNr + 1); }
			),
			"prevPage" : new StateAction(
				function () {return _.currentPageNr !== 0; },
				function () { setPageNr(_.currentPageNr - 1); }
			),
			"cardEject" : new StateAction(
				function () {return VirtuPOS.IsCardPresent; },
				function () { _['ejectCard'](); }
			)
		});
		ViewButtons.bind({
			'cancelButton' : 'selectParent', 
			'leftButton' : 'prevPage', 
			'rightButton' : 'nextPage',
			'cardEjectButton' : 'cardEject'
		});
		
		updateView();
		ViewButtons.update();
	},
	
	"actionsChanged" : ViewButtons.update,
	
	"inactivityTimer" : function() {
		setPageNr(0);
		updateView();
		ViewButtons.update();
		return true;
	}
});

function setPageNr(pageNr) {
	if ((pageNr < 0) || (pageNr > _.lastPageNr)) {
		playSoundFunction("error.wav", false);
		return false;
	}

	_.currentPageNr = pageNr;

	updateView();
	ViewButtons.update();
}

function updateStatusText() {
	if (!PaymentGateway.SelectedCatalogItem.IsRoot || PaymentGateway.Locales.Count <= 1) {
		// не в корне
		document.getElementById('groupStatus').innerHTML = template("statusTemplate", getMessage("recipientsView.groupStatusText"));
		if (_.lastPageNr)
			document.getElementById('groupStatus').innerHTML += getMessage("recipientsView.pageOfThePage");
	} else {
		// в корне
		/*
		var localeTable = "";
		for (var i=0; i<PaymentGateway.Locales.Count; i++) {
			if (PaymentGateway.Locales.Item(i).Lang !="") {
				if (getActiveLocale() == PaymentGateway.Locales.Item(i).Lang) {
					localeTable += "<td width='30' onclick=''>";
					localeTable += "<img border=0 src='images\\" + PaymentGateway.Locales.Item(i).Lang + "\\flag_.png'>&nbsp;&nbsp;";
					localeTable += "</td>";
				} else {
					localeTable += "<td width='30' onclick=\"_['changeLocale']('" + PaymentGateway.Locales.Item(i).Lang + "')\">";
					localeTable += "<img border=0 src='images\\" + PaymentGateway.Locales.Item(i).Lang + "\\flag_.png'>&nbsp;&nbsp;"
					localeTable += "</td>";
				}
			}
		}
		
		document.getElementById('groupStatus').innerHTML = "<table class=statustxt><tr><td align='left' onClick='_[\"information\"]();' width='1'><img hspace='20' src='./images/i_sign.png'><span id='i-txt'>Информация</span></td><td></td>" + localeTable + "</tr></table>";
		*/
		document.getElementById('groupStatus').innerHTML = template("rootStatusTemplate", {"changeLocale" : template("changeLocaleTemplate", {})});
	}

	var balanceStatus = document.getElementById('balanceStatus');
	if (PaymentGateway.CustomerLoggedIn) {
		balanceStatus.innerHTML = getMessage("paykiosk.balanceFormat");
		balanceStatus.className = "visible";
	}

	var changeSumStatus = document.getElementById('changeSumStatus');
	if (PaymentDevice.HasAccumulatedSum) {
		changeSumStatus.innerHTML = template("changeInfoTemplate", getMessage("paykiosk.changeSum"));
		changeSumStatus.className = "visible";
	}
}



function updateView() {
	
	if (cardWaiting) {
		if (VirtuPOS.IsCardPresent)
			document.getElementById("cardWaitingMessage").className = "invisible";
		else	
			if (PaymentGateway.SelectedCatalogItem.IsRoot)
				document.getElementById("cardWaitingMessage").className = "visible";
			else 
				document.getElementById("cardWaitingMessage").className = "invisible";
	} else {
		document.getElementById("cardWaitingMessage").className = "invisible";
	}			

	var table= "";
	updateStatusText();
	
	// Кэшируем шаблон для использования в этой функции
	var drawRecipient = template('recipientTemplateCredit');
	
	with (PaymentGateway.SelectedCatalogItem.ChildItems) {
		for(var i = _.currentPageNr * recipientsPerPage, c = Math.min(i + recipientsPerPage, Count); i<c; ) {
			for (var j = 0; (j < recipientsPerRow) && (i < c); ) {
				var thumbnailURL = Item(i).Thumbnail.URL && Item(i).Thumbnail.URL.length > 0 ? Item(i).Thumbnail.URL : null;

				if (_.cardPresent) {
					if (Item(i).InCategory("HiddenWhenCardInserted") && PaymentGateway.SelectedCatalogItem.IsRoot) {
						i++; continue;
					}	
				} else {
					if (Item(i).InCategory("HiddenWhenCardNotInserted")) {
						i++; continue;
					}
				}	
				
				var thumbnailPath = Framework_IfExistsThen("images-recipients/" + getActiveLocale() + "/" + thumbnailURL, "zzz", window);
				if ("zzz" === thumbnailPath) {
					thumbnailPath = Framework_IfExistsThen("images-recipients/" + thumbnailURL, "zzz", window);

					if ("zzz" === thumbnailPath)
						thumbnailPath = null;
				}
				
				_.idx = i;
				
				var button1 = "";
				// Мин сум 0  - Согласно условиям договора вклад не может быть пополнен
				if (Number(Item(i).Fields.GetFieldByID(150).SelectValue) == 0)
					button1 = "<img onclick=\"_.fireEvent('unableToPayDeposit')\" src='images/" + getActiveLocale() + "/CashPayment.png'>";
				else
					// -1 или нет связаного счета - Пополнение вклада возможно только через кассу 
					if (Number(Item(i).Fields.GetFieldByID(150).SelectValue) == -1 || !isDefined(Item(i).Fields.GetFieldByID(152).SelectValue))
						button1 = "<img onclick=\"_.fireEvent('onlyOfficePaymentMessage')\" src='images/" + getActiveLocale() + "/CashPayment.png'>";
					else
						if (Number(Item(i).Fields.GetFieldByID(150).SelectValue + Item(i).Fields.GetFieldByID(130).SelectValue) > Item(i).Recipient.MaxLimit)
							button1 = "<img onclick=\"_.fireEvent('unableToPayMaxSum')\" src='images/" + getActiveLocale() + "/CashPayment.png'>";
						else
							button1 = "<img onclick=\"_['selectCash']("+i+")\" src='images/" + getActiveLocale() + "/CashPayment.png'>";
					
				var button2 = "";
				
				// Возможность оплаты по карте
				if (Number(Item(i).Fields.GetFieldByID(205).SelectValue) == 1)
					// Мин сум 0  - Согласно условиям договора вклад не может быть пополнен
					if (Number(Item(i).Fields.GetFieldByID(150).SelectValue) == 0)
						button2 = "<img onclick=\"_.fireEvent('unableToPayDeposit')\" src='images/" + getActiveLocale() + "/CardPayment.png'>";
					else
						// -1 или нет связаного счета - Пополнение вклада возможно только через кассу 
						if (Number(Item(i).Fields.GetFieldByID(150).SelectValue) == -1 || !isDefined(Item(i).Fields.GetFieldByID(152).SelectValue))
							button2 = "<img onclick=\"_.fireEvent('onlyOfficePaymentMessage')\" src='images/" + getActiveLocale() + "/CardPayment.png'>";
						else
							button2 = "<img onclick=\"_['selectCard']("+i+")\" src='images/" + getActiveLocale() + "/CardPayment.png'>";
				else
					button2 = "<img src='images/" + getActiveLocale() + "/CardPayment-disabled.png'>";
				
				table += drawRecipient({
						"idx" : i,
						"line1" : Item(i).Fields.GetFieldByID(101).Name + ": " + Item(i).Fields.GetFieldByID(101).SelectValue,
						"line2" : Item(i).Fields.GetFieldByID(170).Name + ": " + Item(i).Fields.GetFieldByID(170).SelectValue,
						"line3" : isDefined(Item(i).Fields.GetFieldByID(152).SelectValue) ? (Item(i).Fields.GetFieldByID(152).Name + ": " + Item(i).Fields.GetFieldByID(152).SelectValue) : null,
						"button1" : button1,
						"button2" : button2,
						"button3" : "<img onclick=\"_['selectBalance']("+i+")\" src='images/" + getActiveLocale() + "/Balance.png'>",
						"button4" : "<img onclick=\"_['selectMinistatement']("+i+")\" src='images/" + getActiveLocale() + "/Ministatement.png'>",
						"button5" : "<img onclick=\"_['selectTransfer']("+i+")\" src='images/" + getActiveLocale() + "/Transfer-disabled.png'>"
				});
				j++;
				i++;
			}
			table += '<br>';
		}
	}
	
	document.getElementById("data").innerHTML = table;
}
})()