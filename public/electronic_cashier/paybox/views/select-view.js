﻿(function() {
var i=0	

	_.addEventListeners({
		"load" : function() {
		
			_.registerVariables({
				"fieldValueTemplate" : null,
				"paymentInfoTemplate" : null
			});
			// Шаблон отображения информации о платеже
			var paymentInfoTemplate = document.getElementById('paymentInfoTemplate');
			if (paymentInfoTemplate)
				_.paymentInfoTemplate = paymentInfoTemplate.text;
			
			// Шаблон отображдения поля получателя
			var fieldValueTemplate = document.getElementById('fieldValueTemplate');
			if (fieldValueTemplate)
				_.fieldValueTemplate = fieldValueTemplate.text;
			
			// Подписываемя на основное событие состояния, завершение операции
			_.addEventListeners({
				"operationComplete" : onOperationComplete
			});
			// Привязываем кнопки к действиям если возможно
			//ViewButtons.bind({'cancelButton' : 'cancel', 'continueButton' : 'continue'});
			
			// Отображаем экран ожидание
			// Например с помощью showAlert
			
		showAlert(getMessage('select.selectInProgress'))	
			
		},
		// Обновление состояния кнопок
		"actionsChanged" : ViewButtons.update
		
	});

	// Обработка окончания выполнения операии
	//  Событие может быть с параметрами как здесь и без
	function onOperationComplete() {
		// Убираем экран ожидания
		hideWaitClock();

		hideAlert();

		if (_.operationSucceeded) { 
			if (getParameter('select.showConfirmOnSelect', false)) {
				showAlert(getMessage("select.groupSelected"),
					{
						"yes" : _['continue'],
						"no" : _['cancel']
					},
					{
						13 : "yes",
						27 : "no"
					},
					"no"
				);
				return false;
			}	
			return;
		} 
		showAlert(PaymentGateway.SelectedCatalogItem.ChildItems.Count > 0 ? getMessage("select.selectFailed") + (getParameter("select.showCheckError", true) ? "<br><b>"+_.errorText+"</b></br>" : "") + getMessage("select.checkData") : getMessage("select.incorrectSelected"),
			{
				"ok" : _['cancel']
			},
			{
				27 : "ok"
			},
			"ok",
			15000
		);
		return false;
	}
})();