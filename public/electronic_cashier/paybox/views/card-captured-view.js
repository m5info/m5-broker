﻿( function() {
	_.addEventListeners({
	
		"load" : function() {
		
			_.registerVariables({
				"servicePhone" : getMessage("cardCaptured.servicePhone")
			});	

			ViewButtons.bind({"continueButton" : "commit"});
			
			document.getElementById("status").innerHTML = template('statusTemplate', getMessage('cardCaptured.status'));
			document.getElementById("data").innerHTML = getMessage('cardCaptured.message') + "<br>" + getMessage('cardCaptured.servicePhone') + "<br><br><img src='images/znak.png'>";
		}
	});
	
})();