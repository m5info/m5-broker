﻿(function() {
_.addEventListeners({
	"load" : function() {
		// Влиятет на доступность действий, поэтому должен вызываться до _.registerActions и ViewButtons.bind
		updateFieldDiv(_.getCurrentField());
		
		_.registerActions({
			"enumUp" : new StateAction(
				function() {
					return _.getCurrentField() 
							&& _.getCurrentField().Type == 3
							&& document.getElementById("enumentry").childNodes.length != 0 
							&& enumShift > 0; 
				},
				function() { shiftEnum(true);} ),
			"enumDown" : new StateAction(
				function() {
					var enumList = document.getElementById("enumentry");					
					return _.getCurrentField() 
							&& _.getCurrentField().Type == 3 
							&& enumList.childNodes.length != 0 
							&& (enumList.offsetHeight+enumList.style.pixelTop-enumList.parentNode.parentNode.offsetHeight) > 0; 
				},
				function() { shiftEnum(false);} )			
		});
		
		ViewButtons.bind({
				'cancelButton' : 'selectParent', 
				'continueButton' : 'commit',
				'prevField' : 'prevField',
				'nextField' : 'nextField',
				'prevFieldString' : 'prevFieldString',
				'nextFieldString' : 'nextFieldString',
				'enumUp' : 'enumUp',
				'enumDown' : 'enumDown',
				'menuButton' : 'menu',
				'cardEjectButton' : 'ejectCard'
		});		
	
		_.addEventListeners({
		
			"fieldChanged" : function() {
				updateFieldDiv(_.getCurrentField());
				updateField();
			},
			
			"fieldValueChanged" : function() {
				updateField();
			},

			"fingerScanComplete" : function () {
				document.getElementById('fieldValue').innerHTML = "<img width=\"140\" height=\"140\" src=\""+_.scannedImagePath+"\"/>";
				showAlert(getMessage('dataEntry.fingerScanned'), { "ok" : _['nextField'] }, {8 : "ok", 13 : "ok", 27 : "ok"}, _['selectParent'], 10000);
				return false;
			},
			"showError" : function() {
				showAlert(getMessage('dataEntry.errors.'+_.lastError), { "ok" : hideAlert }, {8 : "ok", 13 : "ok", 27 : "ok"});		
			},
				
			"key" : function(key) {
				switch(key) {
					// backspace
					case 8 : {
						_["removeFieldChar"]();
						return false;
					}
					// delete
					case 46 : {
						_["removeFieldChar"]();
						return false;
					}
					
					//return	
					case 13 : {
						_["hiddenBox"]();
						if (_.mayExecute('nextField')) {
							_['nextField']();
						} else {
							playSoundFunction("error.wav", false);
						}	
						return false;
					}
						
					//esc	
					case 27 : {
/*							if (leftBlock.style.display == "none") {
								hideAlert();
								correctionFields();
							} else*/
							_["selectParent"]();
						return false;
						}
						
					//вверх
					case 38: {
						_["enumUp"]();
						break;
						}
						
					//вниз
					case 40: {
						_["enumDown"]();
						break;
						}
				}
			},
			"char" : function(ch) {
				_["appendFieldChar"](ch);				
			}


		});
		
		document.focus();
		//document.getElementById("enumUp").attachEvent("onclick", function() { return _["moveEnum"](true); });
		//document.getElementById("enumDown").attachEvent("onclick", function() { return _["moveEnum"](false); });
		
		var recipientImage = document.getElementById("recipientImage")
		if (recipientImage)
			recipientImage.innerHTML = "<img src='images//empty.gif' width=185px height=80px border='0' style=\"filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images-recipients/" + getActiveLocale() + "/" + PaymentGateway.SelectedCatalogItem.Picture.URL + "', sizingMethod='scale')\"/>";		
				
	},
	
	"actionsChanged" : function () { ViewButtons.update() ; try { bindButtons(); } catch(e) {} },
		
	"unload" : function () {
		hideKeyboard();
	}
});



var activeFieldType = null;

var enumItemTemplate = null;

var enumShift = 0;

function calculateFieldValue(field) {
	if (field.Type == 3) {
		if (!field.IsDefined) 
			return getMessage("dataEntry.notSelected"); 
	}
	if (field.Type == 7) {
		return "";
	}
	return field.DisplayValue;
}

function updateFieldDiv(field) {

	var dataEntryElement = document.getElementById('dataentry');
	dataEntryElement.innerHTML = template('dataentryTemplate', {
		"fieldPrompt" : 
			field.IsInput ? 
				(field.Type == 3 ? 
					getMessage("dataEntry.pleaseSelect") + field.Name : 
					getMessage("dataEntry.pleaseInput") + field.Name)
				: getMessage("dataEntry.pleaseConfirm") + field.Name,
		"fieldValue" : calculateFieldValue(field),
			// ((field.EnumItemCount == 0) || field.IsDefined) ? field.DisplayValue : getMessage("dataEntry.notSelected"),
		"fieldDescription" : (field.IsRequired ? "" : "<span class='noRequiredField'>"+getMessage("dataEntry.notRequired")+"</span><br>") + field.Description

	});	

	
	
	document.getElementById('fieldValue').focus;
	// Рисуем правую панель для ввода поля
	if (_.getCurrentField().Type != activeFieldType) {
		switch(_.getCurrentField().Type) {
			// string
			case 1:
				hideKeypad();
				hideEnum();
				showKeyboard(_.getCurrentField());
				break;
			// enum
			case 3:
				hideKeyboard();
				hideKeypad();
				showEnum();
				break;
			// number
			case 2:
			// date
			case 4:
			// decimal
			case 5:
			// ipv4
			case 6:
				hideKeyboard();
				hideEnum();
				showKeypad();
				break;
			case 7:
				hideKeyboard();
				hideEnum();
				hideKeypad();
			default: 
				hideKeyboard();
				hideEnum();
		}
	}
	
	bindButtons();

}


function shiftEnum(moveup) {
// moveup = true -> we're moving up
// moveup = false -> we're moving down

	var btn = null;
	if (moveup)
		btn = document.getElementById("enumUp");
	else
		btn = document.getElementById("enumDown");

	if (moveup) enumShift--; else enumShift++;

	var enumList = document.getElementById("enumentry");
	if (enumShift < 0 || enumShift >= enumList.childNodes.length)
		return;
	var newTop = enumList.childNodes.item(enumShift);
	enumList.style.pixelTop = -newTop.offsetTop+5;
	_.fireEvent('actionsChanged');
}

function hideEnum() {
	document.getElementById('enumInputArea').className = "enumInputArea hide";
}

function showEnum() {
	var enumList = document.getElementById('enumentry');


	// Extra controls
	if (_.getCurrentField().Type == 3) {
		
		document.getElementById('enumInputArea').className = "enumInputArea";
		// Fill enum list
		var field = _.getCurrentField();
		if (enumItemTemplate == null)
			enumItemTemplate = template('enumItemTemplate');
		var enumHTML = "";

		for (iEnum = 0, c = field.EnumItemCount; iEnum < c; iEnum++) {
			enumHTML += enumItemTemplate({
				"fieldDescription" : field.GetEnumItemDescription(iEnum),
				"fieldValue" : field.GetEnumItemValue(iEnum),
				"enumHeader" : "images/enum_header.png",
				"enumFooter" : "images/enum_footer.png"
			});
		}
		enumentry.innerHTML = enumHTML;
	}
	enumShift = 0;
	enumList.style.pixelTop = 0;
	_.fireEvent('actionsChanged');
}

function updateField() {
	// Обновляем значение поля
	document.getElementById('fieldValue').innerHTML  = calculateFieldValue(_.getCurrentField())
	setValueFieldDecoration();
}

function setValueFieldDecoration() {
	var startSize = 36;
	var endSize = 10;
	
	document.getElementById('fieldValue').style.visibility = "hidden";
	document.getElementById('fieldValue').style.fontSize = startSize;
	document.getElementById('fieldValue').innerHTML  = calculateFieldValue(_.getCurrentField());	

	while (startSize > endSize && document.getElementById('fieldValue').clientHeight > 80) {
		startSize--;
		document.getElementById('fieldValue').style.fontSize = startSize;
		document.getElementById('fieldValue').innerHTML  = calculateFieldValue(_.getCurrentField());
	}
	document.getElementById('fieldValue').style.visibility = "visible";

}
/*
function setValueFieldDecoration() {
	var newClassName = 'fieldValue';

	var typeToString = {
		'1' : 'string',
		'2' : 'number',
		'3' : 'enum',
		'4' : 'date',
		'5' : 'decimal',
		'6' : 'ipV4',
		'7' : 'finger-scan'
	};
	if (typeToString.hasOwnProperty(_.getCurrentField().Type))
		newClassName += ' field-'+typeToString[_.getCurrentField().Type];

	document.getElementById('fieldValue').style.visibility = "hidden";
	// newClassName += '  large';
	var sizes = ["large","medium","small"], idx = 0;
	while (idx < sizes.length && document.getElementById('fieldValue').clientHeight > 100) {
		document.getElementById('fieldValue').className = newClassName + ' ' + sizes[idx];

		// document.getElementById('fieldValue').style.fontSize = document.getElementById('fieldValue').style.fontSize.substring(0, document.getElementById('fieldValue').style.fontSize.length-2) - 1;
		document.getElementById('fieldValue').innerHTML  = calculateFieldValue(_.getCurrentField());
		idx++;
		// alert("in " + document.getElementById('fieldValue').className)
		// alert("in " + document.getElementById('fieldValue').clientHeight)	
	}
	document.getElementById('fieldValue').className = newClassName+' '+sizes[idx];
	// alert(document.getElementById('fieldValue').className)
	// alert(document.getElementById('fieldValue').clientHeight)
	document.getElementById('fieldValue').style.visibility = "visible";

}
*/
function updateView(){
	//bindDataButtons(null,null);
	var recipientImage = document.getElementById("recipientImage")
	if (recipientImage)
		recipientImage.innerHTML = "<img src='images//empty.gif' width=185px height=80px border='0' style=\"filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images-recipients/" + PaymentGateway.SelectedCatalogItem.Picture.URL + "', sizingMethod='scale')\"/>";
	if (activeFieldID == null) {
		//bindButtons(executeCancel, null);
	}
	if (!getCurrentField()) {
		return;
		//bindButtons(executeCancel, null);
	}
	if (activeFieldID != getCurrentField().ID) {
		// Сменилось поле, соответственно необходимо реорганизовать область отображения
		activeFieldID = getCurrentField().ID;
		// Рисуем левую панель для поля
		updateFieldDiv(getCurrentField());
	}
	updateField();

}

function bindButtons() {

	if (_.getCurrentField().Type === 1) { // строка
		document.getElementById("dataArea").className = "leftDataAreaString";
		document.getElementById("fieldDescription").style.top = "100";
		document.getElementById("fieldDescription").style.left = "600";
		document.getElementById("prevField").style.display = "none";
		document.getElementById("nextField").style.display = "none";
		document.getElementById("footer").style.display = "none";
		if (_.mayExecute('menu')) {
			document.getElementById("menuButton").style.display = "inline";
			document.getElementById("menuButton").style.bottom = "10";
		} else {
			document.getElementById("menuButton").className.display = "none";
		}	
		if (_.mayExecute('ejectCard')) {
			document.getElementById("cardEjectButton").style.display = "inline";
			document.getElementById("cardEjectButton").style.bottom = "-25";
		} else {
			document.getElementById("cardEjectButton").className.display = "none";
		}
		if (!_.checkNext()) { // последнее поле
			if (_.getCurrentField().IsDefined) {
				document.getElementById("nextFieldString").style.backgroundImage = "url('images/ru/forward_pay.png')";
				document.getElementById("nextFieldString").onclick = _['nextField'];
			} else {
				document.getElementById("nextFieldString").style.backgroundImage = "url('images/ru/forward_pay-d.png')";
				document.getElementById("nextFieldString").onclick = "";
			}	
		
		} else {
			if (_.getCurrentField().IsDefined) {
				document.getElementById("nextFieldString").style.backgroundImage = "url('images/ru/btn-vnext.png')";
				document.getElementById("nextFieldString").onclick = _['nextField'];
			} else {
				document.getElementById("nextFieldString").style.backgroundImage = "url('images/ru/btn-vnext-d.png')";
				document.getElementById("nextFieldString").onclick = "";
			}
		}
		document.getElementById("prevFieldString").style.display = "inline";
		document.getElementById("nextFieldString").style.display = "inline";
	} else {
		document.getElementById("dataArea").className = "leftDataArea";
		document.getElementById("prevFieldString").style.display = "none";
		document.getElementById("nextFieldString").style.display = "none";
		if (_.previousFieldIndex.length == 0)	
			document.getElementById("prevField").style.display = "none";
		else	
			document.getElementById("prevField").style.display = "none";
		document.getElementById("nextField").style.display = "inline";
		document.getElementById("footer").style.display = "inline";
		if (_.mayExecute('menu')) {
			document.getElementById("menuButton").style.display = "inline";
			document.getElementById("menuButton").style.bottom = "35";
		} else {
			document.getElementById("cardEjectButton").className.display = "none";
		}
		if (_.mayExecute('ejectCard')) {
			document.getElementById("cardEjectButton").style.display = "inline";
			document.getElementById("cardEjectButton").style.bottom = "0";
		} else {
			document.getElementById("cardEjectButton").className.display = "none";
		}

		if (!_.checkNext()) { // последнее поле
			if (_.getCurrentField().IsDefined) {
				document.getElementById("nextField").style.backgroundImage = "url('images/ru/forward_pay.png')";
				document.getElementById("nextField").onclick = _['nextField'];
			} else {
				document.getElementById("nextField").style.backgroundImage = "url('images/ru/forward_pay-d.png')";
				document.getElementById("nextField").onclick = "";
			}	
		} else {
			if (_.getCurrentField().IsDefined) {
				document.getElementById("nextField").style.backgroundImage = "url('images/ru/btn-vnext.png')";
				document.getElementById("nextField").onclick = _['nextField'];

			} else {
				document.getElementById("nextField").style.backgroundImage = "url('images/ru/btn-vnext-d.png')";
				document.getElementById("nextField").onclick = "";
			}	
		}
	}

}

})();