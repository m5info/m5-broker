﻿(function() {
_.addEventListeners({
	"load" : function() {
		document.getElementById("data").innerHTML = getMessage("creditRequestInfo.info");
		
			ViewButtons.bind({
			'cancelButton' : 'commit', 
			'menuButton' : 'menu',
			'cardEjectButton' : 'ejectCard'
		});	
	}
});

})();