﻿_.addEventListeners({
	"load" : function() {
		document.getElementById("status").innerHTML = template('statusTemplate', getMessage('pinEntry.pinEntryTitle'));
		
		document.getElementById("data").innerHTML = getMessage('pinEntry.pinEntryHint');
		document.getElementById("data").innerHTML += "<div id='pin' class='pinEntryField'></div>"
		showAlert(getMessage('pinEntry.pinWait'));
		_.addEventListeners({
			"pinInputStarted" : function() {
				hideAlert();
			},
			"pinChanged" : function() {
				var s = "";
				for (var i = 0, c = _.enteredPINLength; i < c; i++)
					s += "&#8226";
				document.getElementById("pin").innerHTML = s;
			}
		});
		ViewButtons.bind({"cancelButton" : "cancel"});
	}
});

