﻿var productPaymentWarning;
var fieldsInfo;
var fieldValueTemplate;
var commissionSumStr;
var transferSumStr;
var accumulatedSum;
var remainSum;
var commissionSum;
var transferSum;
var commissionText;
var partyPayment;
var paymentSum;


_.addEventListeners({
	"load" : function() {
	
		productPaymentWarning = document.getElementById('productPaymentWarning');
		fieldsInfo = document.getElementById('fieldsInfo');
		fieldValueTemplate = document.getElementById('fieldValueTemplate').innerHTML;
		commissionSumStr = document.getElementById('CommissionSumStr');
		transferSumStr = document.getElementById('TransferSumStr');
		paymentSum = document.getElementById('PaymentSum');
		accumulatedSum = document.getElementById('AccumulatedSum');
		remainSum = document.getElementById('RemainSum');
		commissionSum = document.getElementById('CommissionSum');
		transferSum = document.getElementById('TransferSum');
		commissionText = document.getElementById('commissionText');
		partyPayment = document.getElementById('partyPayment');
	
		_.addEventListeners({
			"key" : function(key) {
				playSoundFunction("click.wav");
				switch(key) {
					case 8:
						// backspace
						removeLastChar();
						return false;
					// delete
					case 46:
						removeLastChar();
						return false;
					//return	
					case 27:
						_['cancel']();
						return false;
					case 13:
						if (_.mayExecute('commit')) _['commit']();
						return false;
				}
				return true;
			},
			"char" : function(ch) {
				playSoundFunction("click.wav");
				return appendChar(ch);
			},
			"sumChanged" : updateView
		});
		
		ViewButtons.bind({
			"cancelButton" : "cancel",
			"continueButton" : "commit"
		});			
		
	},
	
	"run" : function () {
		initialDisplay();
		ViewButtons.update();
	},

	"actionsChanged" : ViewButtons.update,
	
	"unload" : function () {
	}
});

function initialDisplay() {
	showKeypad();
	var keyPadDiv = document.getElementById("keypadDiv");
	keyPadDiv.className = "keyPadDiv";
	var Recipient = PaymentGateway.SelectedRecipient;

	// Satus text
	document.getElementById("statusSumEntry").innerHTML = getMessage("acceptWalletPayment.statusSumEntry");
	document.getElementById("statusSumLeft").innerHTML = getMessage("acceptWalletPayment.statusSumLeft");
	
	// Hint
	document.getElementById("fieldsInfo").innerHTML = "<b>"+getMessage("acceptWalletPayment.statusRecipient")+"</b>";
	
	// SummTable
	document.getElementById("paymentSumStr").innerHTML = getMessage("acceptCashPayment.paymentSumStr");
	document.getElementById("transferSumStr").innerHTML = getMessage("acceptCashPayment.transferSumStr");
	document.getElementById("commissionSumStr").innerHTML = getMessage("acceptCashPayment.commissionSumStr");

	// Warning
	// var warnText = getMessage("acceptCashPayment.freeSummWarning");
	// if (Recipient.MinLimit > 10)
		// warnText += " "+getMessage("acceptCashPayment.firstNoteMessage");
	// warnText += " "+getMessage("acceptCashPayment.maxPaymentSum");
	// if (getParameter("acceptCash.changePossible", true))
		// warnText += " "+getMessage("acceptCashPayment.automaticPayment");
	// document.getElementById("productPaymentWarning").innerHTML = warnText;
	
	if (getParameter("acceptCashView.showFieldsAcceptPayment", false)) {
		fieldsInfo.innerHTML += "<br/>"+enumerateRecipientFields(Recipient.Fields, fieldValueTemplate, false);
	} else {
		document.getElementById("fieldsInfoDiv").style.display = "none";
	}
	
	if (!getParameter("acceptCashView.showWithComission", true)) {
		commissionSumStr.className = "invisible";
		transferSumStr.className = "invisible";
	}

	paymentSum.innerHTML = getCurrency().format(PaymentDevice.AccumulatedSum(Recipient.CurrencyCode).Value);
	transferSum.innerHTML = getCurrency().format(Recipient.TransferSum);
	commissionSum.innerHTML = getCurrency().format(Recipient.CommissionSum);
	
	/* Commissions */
	commissionText.innerHTML = "";
	var commissionRules = returnCommissionRules(Recipient, Recipient.CurrencyCode);
	for (var i = 0; i < commissionRules.length; i++) {
		commissionText.innerHTML += "&nbsp;&nbsp;<img src='images\\bullet.gif'>&nbsp;" + commissionRules[i] + "<br />";
	}
if (PaymentGateway.SelectedRecipient.MinLimit >0)
	commissionText.innerHTML += "&nbsp;&nbsp;<img src='images\\bullet.gif'>&nbsp;"+getMessage("acceptCashPayment.minSumm")+"<br />";
	if (PaymentGateway.SelectedRecipient.MaxLimit >0)
	commissionText.innerHTML += "&nbsp;&nbsp;<img src='images\\bullet.gif'>&nbsp;"+getMessage("acceptCashPayment.maxSumm");
	
	/* change */
	if (getParameter("acceptCash.changePossible", true)) {
	} else {
	allowBack = false;
	allowPartyPayment = false;
	}
	updateView();
}

function updateView() {
	var Recipient = PaymentGateway.SelectedRecipient;

	// Status
	document.getElementById("receivedAmount").innerHTML = getCurrency().format(PaymentGateway.CustomerBalance-Recipient.PaymentSum);
	
	paymentSum.innerHTML = getCurrency().format(Recipient.PaymentSum);
	transferSum.innerHTML = getCurrency().format(Recipient.TransferSum);
	commissionSum.innerHTML = getCurrency().format(Recipient.CommissionSum);
}
