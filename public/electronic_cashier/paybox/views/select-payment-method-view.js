﻿(function() {
	_.addEventListeners({
		"load" : function() {
		
			ViewButtons.bind({
				"cancelButton" : "cancel",
				"continueButton" : "continue"
			});
		
			document.getElementById("status").innerHTML = template('statusTemplate', getMessage('selectPaymentMethod.selectPMTitle'));	
		
			_.addEventListeners({
				"paymentMethodSelected" : showFixedSumWarning
			});
			
			var drawPaymentMethod = template('paymentMethodTemplate');
			var paymentMethodTemplate = "";
			
			for (var i = 0, c = _.paymentMethods.length; i < c; i++) {	
				paymentMethodTemplate += drawPaymentMethod({
					"name" : _.paymentMethods[i].hint,
					"imageURL" : _.paymentMethods[i].image,
					"idx" : i				
				});
			}
			document.getElementById("data").innerHTML =	template('borderedMessageTemplate', paymentMethodTemplate);
		},
		
		"actionsChanged" : ViewButtons.update,
		
		"key" : function(key) {
			switch(key) {
				case 8:
				case 27:
					_['cancel']();
					break;
			}
		},
			
		"char" : function(ch) {
			var idx = (""+ch).charCodeAt(0)-49;
			if (idx < 0 || idx >= _.paymentMethods.length)
				return true;
			_["selectPaymentMethod"](idx);
			return true;
		}
	});
	
	
	function showFixedSumWarning() {
		Recipient = PaymentGateway.SelectedRecipient;

		var warningMsg = null;
		if (getParameter("selectPaymentMethod.showFixedSummMessage") && Recipient.TransferAmountType > 0 && Recipient.TransferSum > 0) {
			switch (Recipient.TransferAmountType) {
				case 1 : {	
					warningMsg = "selectPaymentMethod.fixedSummWarning";
					break;			
				}
				case 2 : {	
					warningMsg = "selectPaymentMethod.minSummWarning";
					break;			
				}
				case 3 : {	
					warningMsg = "selectPaymentMethod.recommendedSummWarning";
					break;			
				}
			}
		}	

		if (warningMsg != null) {
			showAlert(getMessage(warningMsg),
				{
					"yes" : _["continue"],
					"no" : _["cancel"]
				},
				{ 13 : "yes", 27: "no" },
				"no"
			);
			return false;
		}	
	}

})();
