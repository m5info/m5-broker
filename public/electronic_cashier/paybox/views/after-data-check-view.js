﻿_.addEventListeners({
	"load" : function() {
		PaymentGateway.SelectedRecipient.CurrentCheckAuthCode = "CurrentCheckAuthCode";
	
		_.addEventListeners({
			"beforeCommit" : function () {
				// Предупреждение о неработоспособности принтера
				if (!( typeof(Printer) != "undefined" && Printer != null && Printer.DeviceReady) && getParameter("paykiosk.printerOfflineWarning", true)) { 
					showAlert(getMessage("paykiosk.printerOfflineWarning"),
						{
							"yes" : _["commit"],
							"no" : _["cancel"]
						},
						{
							// return
							13 : "yes", 
							// esc
							27 : "no" 
						},
						"no",
						getParameter("afterDataCheck.printerOfflineAlertTimeout", 30000)
					);
					return false;
				}
			}
		})	
	}
});
