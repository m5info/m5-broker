﻿( function() { 
// Отображение информации при провреке данных
_.addEventListeners({
	"load" : function() {
		// Отображаем экран ожидание
		// Основной обработчик отображения
		_.addEventListeners({
			"operationComplete" : onPaymentComplete,
			"actionsChanged" : ViewButtons.update,
			"executingVirtuPOSMethod" : function() {
				hideAlert();
				showAlert(getMessage('VirtuPOS.executingVirtuPOSMethod'));
			},
			"tryVirtuPOSMethodAgain" : function() {
				hideAlert();
				showAlert(getMessage('VirtuPOS.tryVirtuPOSMethodAgain'));
			}
		});
		
		// ViewButtons.bind({'continueButton' : 'continue'});		

		if (_.recipientsArray.length == 1 )
			showAlert(getMessage('payment.paymentInProgress'));
		else {
			document.getElementById("status").innerHTML = template('statusTemplate', getMessage('payment.statusForCollectionCase'));
			document.getElementById("data").innerHTML = template("paymentResultsTable", {})
		}
		
	}
});

function onPaymentComplete() {

	if (!_.paymentSucceeded && _.recipientsArray.length == 1 && !_.virtuPOSCheck) {
		var errorText = Framework.Variables("paymentResults")[1].errorText;
		showAlert(
			errorText, 
			{
				"ok" : _["continue"]
			},
			{
				13 : "ok"
			},
			"ok",
			10000
		);
		return false;
	}		
		

	if (!_.paymentSucceeded && _.recipientsArray.length != 1 && !_.deferredPayment) {
		document.getElementById(_.activeRecipient.ID).className = "td2-fail";
		document.getElementById("description"+_.activeRecipient.ID).innerHTML = getMessage('payment.payFail');
		// нужно получить доступ к информации о возможности отложенного платежа
		// флаг возможности допроведения - _.deferredPayment
	} else if (!_.paymentSucceeded && _.recipientsArray.length != 1 && _.deferredPayment) {
		document.getElementById(_.activeRecipient.ID).className = "td2-nn";	
		document.getElementById("description"+_.activeRecipient.ID).innerHTML = getMessage('payment.payDeferred');		
	} else if (_.paymentSucceeded && _.recipientsArray.length != 1) {
		document.getElementById(_.activeRecipient.ID).className = "td2-ok";
		document.getElementById("description"+_.activeRecipient.ID).innerHTML = getMessage('payment.payOk');		
	}
}

} )();