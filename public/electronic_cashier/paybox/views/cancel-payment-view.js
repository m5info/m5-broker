﻿
// Отображение информации при отмене платежа
_.addEventListeners({
	"load" : function() {
		//кнопки
		document.getElementById("cancel_admin").onclick = function() {  _['cancel']('Admin')};
		document.getElementById("cancel_cassa").onclick = function() {  _['cancel']('ManualKassa')};
		document.getElementById("backButton").onclick = function() {  _['commit']()};
	},
	
	"key" : function(key) {
		switch(key) {
			// backspace
			case 8 :
			//esc
			case 27 :
				_['cancel']();
				return false;
		}
		return true;
	},

	"run" : function() {
		
	},
	
	"actionsChanged" : function() {
		if (_.mayExecute("cancel")) {
			document.getElementById("cancelTable").className = "cancelTableShow";
			document.getElementById("overLay").className = "overLayShowHide";
		} else {
			document.getElementById("cancelTable").className = "cancelTableHide";
			document.getElementById("overLay").className = "overLayShowShow";
		}
	}	
});

