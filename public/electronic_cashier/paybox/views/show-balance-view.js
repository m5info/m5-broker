﻿(function() {
_.addEventListeners({
	"load" : function() {
		_.addEventListeners({
			"operationComplete" : onOperationComplete,
			"printerSFailure" : function() {
				showAlert(getMessage('showBalance.printerFailure'), 
					{
						"ok" : _["continue"]
					},
					{
						13 : "ok"
					},
					"ok",
					12000
				);
			},
			"printDone" : function() {
				showAlert(getMessage('showBalance.printDone'), 
					{
						"ok" : _["continue"]
					},
					{
						13 : "ok"
					},
					"ok",
					12000
				);
			}
		});
	}
});

function onOperationComplete() {

		showAlert(getMessage('showBalance.makeChoice'), 
			{
				"receipt" : _["printBC"],
				"screen" : showBalance
			},
			{},
			_["continue"],
			15000
		);		
	return false;
	
}

function showBalance() {

var balance = "";
balance += "--------------------------------------------<br>";
balance += _.balance+"<br>";
balance += "--------------------------------------------<br>";
		
		showAlert(_.balance, 
			{
				"ok" : _["continue"]
			},
			{
				13 : "ok"
			},
			"ok",
			getParameter("showBalance.showTime", 15000)
		);

}

})();