﻿var fieldValueTemplate = null;
var paymentInfoTemplate = null;

// Отображение информации при провреке данных
_.addEventListeners({
	"load" : function() {
		var msg;
		var result;
		try {
			result = Framework.Variables("dispenseResult");
		} catch (e) {
			result = 2;
		}
		switch (result) {
			case 0:
				break;
			case 1:
				msg = getMessage("dispense.noRequiredSum");
				break;
			case 2:
				msg = getMessage("dispense.dispenseError");
				break;
			case 3:
			default:
				msg = getMessage("dispense.dispenseErrorAfter");
				break;
		}
		showAlert(msg, 
			{ "ok" : _["commit"] },
			{ 13 : "ok", 27 : "ok"},
			"ok"
			);
	},

	"actionsChanged" : function() {
		// bindButtons({
			// "cancelButton" : _.mayExecute('cancel') ? _['cancel'] : null,
			// "continueButton" : _.mayExecute('commit') ? _['commit'] : null
		// });
	},
	"key" : function(key) {
		switch(key) {
			// backspace
			case 8 :
			//esc
			case 27 :
				_['cancel']();
				break;
				
			//return
			case 13 :
				_['commit']();
				break;
		}
		return false;
	}
});
