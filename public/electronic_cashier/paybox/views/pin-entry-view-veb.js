﻿_.addEventListeners({
	"load" : function() {		
		document.getElementById("pinInfo").innerHTML = getMessage('pinEntry.pinEntryHint');
		document.getElementById("data").innerHTML = "<div class='pinEntryField'></div>"
		showAlert(getMessage('pinEntry.pinWait'));
		_.addEventListeners({
			"pinInputStarted" : function() {
				hideAlert();
			},
			"pinChanged" : function() {
				var s = "";
				for (var i = 0, c = _.enteredPINLength; i < c; i++)
					s += "&#8226";
				document.getElementById("pin").innerHTML = s;
			}
		});
		ViewButtons.bind({"cancelButton" : "cancel"});
	}
});

