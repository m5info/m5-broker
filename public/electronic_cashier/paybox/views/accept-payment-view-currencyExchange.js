﻿( function() {

_.addEventListeners({
	"load" : function() {
		_.registerVariables({
			"sourceCurrencyName" : getMessage('currency.'+getOperationCurrency()),
			"targetCurrencyName" : getMessage('currency.'+getTargetCurrency()),
			"amountTarget" : new CurrencyAmount(0.0, getTargetCurrency()),
			"amountDispense" : new CurrencyAmount(0.0, getOperationCurrency()),
			"amountChange" : new CurrencyAmount(0.0, getOperationCurrency())
		});
		ViewButtons.bind({
			'cancelButton' : 'cancel',
			'continueButton' : 'commit'
			});		
	
		_.addEventListeners({
			"key" : function(key) {
					switch(key) {
						case 8:
							break;
						case 27:
							_['cancel']();
							break;
						case 13:
							if (_.mayExecute('commit')) _['commit']();
							break;
						}
			},
			"onAccumulatedSumChanged" : function (sum) {
				var calculated = new exchangeCalculator(_.acceptedSum);
				_.amountTarget = calculated.targetDispenseAmount;
				_.amountDispense = calculated.sourceAmountDispense;
				_.amountChange = calculated.sourceAmountChange;
				/*
				*/
				Framework.Variables("ExchangeToDispense") = [
					_.amountDispense, _.amountTarget, _.amountChange
				];
				ViewButtons.update();
				updateView();
			},
			"onPaymentComplete" : function () {
				ViewButtons.update();
				updateView();
			},
			"unableToAccept" :  function(error) {
				if (!getParameter("dataCheck.showCheckError"))
					error = "";
				else if (error == "incorrectCollection")
					error = "<br>"+getMessage("acceptCashPayment.incorrectCollection");
				else if (error == "incorrectCurrency")
					error = "<br>"+getMessage("acceptCashPayment.incorrectCurrency");
				else if (error != "")
					error = "<br>"+error;
				showAlert(getMessage("acceptCashPayment.unableToAccept") + error,
					{
						"ok" : _["cancel"]
					},
					{
						27: "ok"
					},
					"ok"
				);				
			},
			"askMoreTime" : function() {
				showAlert(getMessage("acceptCashPayment.askMoreTime"),
				{
					"yes" : _['continuePay'],
					"no" : _['commit']
				},
				{
					13 : "yes",
					27 : "no"
				},
				"no"	
				);				
			},
			"onBillRejected" : function(reason, hint) {
				//предупреждения о мин и макс суммах и неправильной валюте платежа			
				switch (reason)	{
					case 2:
						showAlert(getMessage("acceptCashPayment.maxSummWarning"), {}, {}, null, 5000);
						break;
					case 3:
						showAlert(getMessage("acceptCashPayment.minSummWarning"), {}, {}, null, 5000);
						break;
					case 4:
						showAlert(getMessage("acceptCashPayment.rejectOnReason4")+hint, {}, {}, null, 5000);
						break;
					case 1:
					// Wrong no action
				}				
			}
		});

		initialDisplay();
		
	},
	
	"run" : function () {
		ViewButtons.update();
		updateView();
	},

	"actionsChanged" : function() {
		ViewButtons.update();
	}
});

function initialDisplay() {
	// Status text
	document.getElementById("status").innerHTML = template('statusTemplate', getMessage("acceptCashPayment.insertMoney"));

	document.getElementById("ExchangeInfo").innerHTML = getMessage("currencyExchange.exchangeHint");
	document.getElementById("ExchangeRateInfo").innerHTML = getMessage("currencyExchange.ratesHint");

	// SummTable
	document.getElementById('paymentSumStr').innerHTML = getMessage("currencyExchange.insertedAmount");
	document.getElementById('amountTargetStr').innerHTML = getMessage("currencyExchange.targetAmount");
	document.getElementById('amountDispenseStr').innerHTML = getMessage("currencyExchange.amountDispense");
	document.getElementById('amountChangeStr').innerHTML = getMessage("currencyExchange.amountChange");
	document.getElementById('remainSumStr').innerHTML = getMessage("currencyExchange.remainSum");
	
	var calculated = new exchangeCalculator(_.acceptedSum);
	_.amountTarget = calculated.targetDispenseAmount;
	_.amountDispense = calculated.sourceAmountDispense;
	_.amountChange = calculated.sourceAmountChange;
}

function updateView() {
	document.getElementById('PaymentSum').innerHTML = getCurrency(getOperationCurrency()).format(_.acceptedSum);
	document.getElementById('AmountTarget').innerHTML = _.amountTarget.format(); // Поле a
	if (isRoublesTarget())
		document.getElementById('SummTable').rows(2).style.display="none";
	document.getElementById('AmountDispense').innerHTML = _.amountDispense.format(); // Поле б
	document.getElementById('AmountChange').innerHTML = _.amountChange.format(); // Поле в
	document.getElementById('remainSum').innerHTML = getCurrency(getOperationCurrency()).format(_.remainSum);
}
})();