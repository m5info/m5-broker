﻿(function() {
// Отображение информации при провреке данных
_.addEventListeners({
	"load" : function() {
		_.registerVariables({
			"fieldValueTemplate" : null,
			"paymentInfoTemplate" : null
		});
		// Шаблон отображения информации о платеже
		var paymentInfoTemplate = document.getElementById('paymentInfoTemplate');
		if (paymentInfoTemplate)
			_.paymentInfoTemplate = paymentInfoTemplate.innerHTML;
		
		// Шаблон отображдения поля получателя

		var fieldValueTemplate = document.getElementById('fieldValueTemplate');
		if (fieldValueTemplate)
			_.fieldValueTemplate = fieldValueTemplate.innerHTML;

		_.addEventListeners({
			"operationComplete" : onOperationComplete	
		});
		// Отображаем экран ожидание
		showAlert(getMessage('ministatementRequest.balanceRequestInProgress') + "<br><br><br><img src='images//clock.png'>"); 
	},

	"key" : function(key) {
		switch(key) {
			// backspace
			case 8 :
			//esc
			case 27 :
				_['continue']();
				break;
				
			//return
			case 13 :
				_['continue']();
				break;
		}
		return false;
	}
});


// Обработка окончания выполнения операии

function onOperationComplete() {
	// Показываем сообщение о неудаче
	showAlert(getMessage("ministatementRequest.checkFailed"),
		{
			"OK" :  _['cancel']
		},
		{
			27: "OK"
		},
		"OK",
		5000
		);

	return false;
}
})();