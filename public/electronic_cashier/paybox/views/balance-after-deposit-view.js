﻿(function () {
_.addEventListeners({
	"load" : function() {

		document.getElementById("data").innerHTML = getMessage('balanceAfterDeposit.hint');
	
		_.addEventListeners({
			"executingVirtuPOSMethod" : function() {
				hideWaitClock();
				hideAlert();
				showAlert(getMessage('VirtuPOS.executingVirtuPOSMethod'));
			},
			"tryVirtuPOSMethodAgain" : function() {
				hideAlert();
				showAlert(getMessage('VirtuPOS.tryVirtuPOSMethodAgain'));
			}
		});	
		
	}
});
})();