﻿(function() {
_.addEventListeners({
	"load" : function() {
		// Подписываемя на основное событие состояния, завершение операции
		_.addEventListeners({
			"operationComplete" : onOperationComplete
		});
		
		// Отображаем экран ожидание
		// Например с помощью showAlert
		showWaitClock();
	}
	
});


// Обработка окончания выполнения операии
//  Событие может быть с параметрами как здесь и без
function onOperationComplete() {
	// Убираем экран ожидания
	hideWaitClock();
	
	// в завимости от результатов и задач отображаем результат исполнения:
	if (_.operationSucceeded) {
		if (PaymentGateway.SelectedCatalogItem.ID == 8538) {
			showAlert(
			getMessage("printCoupon.printDone1"), 
			{ "ok" : _["continue"] },
			{ 13 : "ok", 27 : "ok"}, 
			_['continue'], 
			getParameter("printCoupon.printAlertShowTime", 10000));
			return false;
		} else{
		showAlert(
		getMessage("printCoupon.printDone"), 
		{ "ok" : _["continue"] },
		{ 13 : "ok", 27 : "ok"}, 
		_['continue'], 
		getParameter("printCoupon.printAlertShowTime", 10000));
		return false;}
	} else {
		showAlert(
		getMessage("printCoupon.printFailed"), 
		{ "ok" : _["continue"] },
		{ 13 : "ok", 27 : "ok"}, 
		_['continue'], 
		getParameter("printCoupon.printAlertShowTime", 10000));
		return false;		
	}
}
})();