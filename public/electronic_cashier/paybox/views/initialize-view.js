﻿(function() {
_.addEventListeners({
	"load" : function() {
	
		// Отображаем экран ожидания (сообщение + символичный циферблат ожидания)
		showAlert(getMessage("initialize.reload"));
		showWaitClock();

	}
});
})();