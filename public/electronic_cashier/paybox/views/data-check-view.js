﻿(function() {
// Отображение информации при провреке данных
_.addEventListeners({
	"load" : function() {
		_.registerVariables({
			"fieldValueTemplate" : null,
			"paymentInfoTemplate" : null
		});
		// Шаблон отображения информации о платеже
		var paymentInfoTemplate = document.getElementById('paymentInfoTemplate');
		if (paymentInfoTemplate)
			_.paymentInfoTemplate = paymentInfoTemplate.innerHTML;
		
		// Шаблон отображдения поля получателя

		var fieldValueTemplate = document.getElementById('fieldValueTemplate');
		if (fieldValueTemplate)
			_.fieldValueTemplate = fieldValueTemplate.innerHTML;

		_.addEventListeners({
			"operationComplete" : onOperationComplete,
			"executingVirtuPOSMethod" : function() {
				hideAlert();
				showAlert(getMessage('VirtuPOS.executingVirtuPOSMethod'));
			},
			"executingMethod" : function() {
				hideAlert();
				showAlert(getMessage('dataCheck.executingMethod'));
			},			
			"tryVirtuPOSMethodAgain" : function() {
				hideAlert();
				showAlert(getMessage('VirtuPOS.tryVirtuPOSMethodAgain'));
			}			
		});
		// Отображаем экран ожидание
		showAlert(getMessage('dataCheck.dataCheckInProgress')); 
	},

	"key" : function(key) {
		switch(key) {
			// backspace
			case 8 :
			//esc
			case 27 :
				_['cancel']();
				break;
				
			//return
			case 13 :
				_['commit']();
				break;
		}
		return false;
	}
});


// Обработка окончания выполнения операии
//
function onOperationComplete() {
	if (_.operationSucceeded) {
		//Все хорошо. Возможны варианты сообщений
		// Отображать результат успешной проверки		
		if ((getParameter("dataCheck.responseTextCheck", false) || _.checkResult == 1) && 
			!PaymentGateway.SelectedRecipient.InCategory("VirtuPOSCheck") ) { // Требуется ввод данных доп. данных или подтверждение введённых ранее
			// Показываем сообщение
			if (PaymentGateway.SelectedCatalogItem.ID == 8541) {
		showAlert(getMessage("dataCheck.checkResult.datacheck.2"), 
			{
				"yes" : _["commit"],
				"no" : _["cancel"]
			},
			{
				13 : "yes",
				27 : "no"
			},
			"no");
	
	return false;	}
	else{
			showAlert(getMessage("dataCheck.checkResult."+_.type.replace("-", ""), _.checkResult),
				{
					"yes" : _["continue"],
					"no" : _["cancel"]
				},
				{
					13 : "yes",
					27 : "no"
				},
				"no"
				);
			return false;
			}
		}
		// Ничего не отображали все сделает behaviour
		return;
	}
	// Что-то не так отработало
	if (!_.mayExecute('commit')) {

		// Не можем проводить платеж, информируем пользователя, и cancel
		showAlert(getMessage("dataCheck.checkResult."+_.type.replace("-", ""), "__undefined__"),
			{
				"OK" :  _['cancel']
			},
			{
				27: "OK"
			},
			"OK"
		);
	} else {
		// Отработало не так, но можно пропустить проверку
		// Запрашиваем подтверждение на разрешение проведения платежа
		showAlert(getMessage("dataCheck.checkResult."+_.type.replace("-", ""), _.checkResult), 
			{
				"yes" : _["commit"],
				"no" : _["cancel"]
			},
			{
				13 : "yes",
				27 : "no"
			},
			"no");
	
	}
	return false;	
	
/* что это такое - непонятно; закомментируем и пока оставим, нужно понять */
	// if (VirtuPOS && !VirtuPOS.Offline && PaymentGateway.SelectedRecipient.InCategory("CardOperation")) {
		// Карточные операции обрабатываем отдельно
		// onCardOperationCheckComplete(_.checkResult, errorText);
		// return;
	// }
	
}
})();