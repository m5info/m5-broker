﻿(function() {
_.addEventListeners({
	// Обработчик загрузки текущего состояния
	"load" : function() {
		_.addEventListeners( {
			"codeChanged" : function () {
				document.getElementById("accessCodeTEXT").innerHTML = _.accessCodeView;
			}
		});
		setupAuthButtons();
	},
	"actionsChanged" : ViewButtons.update,
	"key" : function(key) {
		switch(key) {
			// backspace
			case 8 :
				return false;
			//esc
			case 27 :
				_['cancel']();
				return false;
			case 13 :
				_['commit']();
				return false;
		}
		return true;
	},
	"char" : function(ch) {
		if (ch >= '0' && ch <= '9') {
			_['appendCode'](ch);
			return false;
		}
		return true;
	}
});


function setupAuthButtons() {
	document.getElementById("clearCode").value = getMessage("serviceMenu.clear");
	document.getElementById("commitAuth").value = getMessage("serviceMenu.authorize");
	var numButtons = new Array();

	var i = 0;
	while (i < 10) {
		j = Math.floor(Math.random() * 10);
		if (numButtons[j] == null) {
			numButtons[j] = i;
			i++;
		}
	}

	var allButtons = { 
		"cancelButton" : "cancel",
		"clearCode" : "clearCode",
		"commitAuth" : "commit"
		};
	for (var i=0; i<numButtons.length; i++) {
		document.getElementById("entryBtn"+i).value = numButtons[i];
		allButtons["entryBtn"+i] = new Function("playSoundFunction(\"click.wav\"); _['appendCode'](this.value)");
	}
	ViewButtons.bind(allButtons);
	_['clearCode']();
}

})()