﻿(function() {
_.addEventListeners({
	"load" : function() {
		document.getElementById("data").innerHTML = getMessage("creditRequestAttention.info");
		
			ViewButtons.bind({
			'cancelButton' : 'menu', 
			'continueButton' : 'commit', 
			'menuButton' : 'menu',
			'cardEjectButton' : 'ejectCard'
		});	
	}
});

})();