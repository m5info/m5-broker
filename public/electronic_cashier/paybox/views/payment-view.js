﻿( function() { 
// Отображение информации при провреке данных
_.addEventListeners({
	"load" : function() {
		// Отображаем экран ожидание
		// Основной обработчик отображения
		_.addEventListeners({
			"paymentComplete" : onPaymentComplete,
			"operationComplete" : onOperationComplete,
			"actionsChanged" : ViewButtons.update,
			"executingMethod" : function() {
				hideAlert();
				showAlert(getMessage('payment.executingMethod'));
			},
			"tryVirtuPOSMethodAgain" : function() {
				hideAlert();
				showAlert(getMessage('VirtuPOS.tryVirtuPOSMethodAgain'));
			}			
		});
		
		ViewButtons.bind({'continueButton' : 'continue'});		

		if (_.recipientsArray.length == 1 )
		// в случае одного получателя выводим сообщение, что оплата в процессе
			showAlert(getMessage('payment.paymentInProgress'));
		else {
		// в случае коллекции рисуем таблицу
			document.getElementById("status").innerHTML = template('statusTemplate', getMessage('payment.statusForCollectionCase'));
			document.getElementById("data").innerHTML = template("paymentResultsTable", {})
		}
		
	}
});

function onOperationComplete(requestCompleteResult, errorText) {
	// в случае одного получателя при неуспешном платеже выводим сообщение об ошибке

	if (!_.paymentSucceeded && _.recipientsArray.length == 1) {
		hideAlert();
		if (VirtuPOS && !VirtuPOS.Offline && _.extension == "CardDeposit") {
			showAlert(errorText, {},{},null,5000);
		} else {
			showAlert(errorText, 
				{
					"ok" : _["continue"]
				},
				{
					13 : "ok"
				},
				"ok",
				10000
			);
		}
		return false;
	}
	
}

function onPaymentComplete() {

	// в случае коллекции заполняем таблицу
	if (!_.paymentSucceeded && _.recipientsArray.length != 1 && !_.deferredPayment) {
		document.getElementById(_.activeRecipient.ID).className = "td2-fail";
		document.getElementById("description"+_.activeRecipient.ID).innerHTML = getMessage('payment.payFail');
		// нужно получить доступ к информации о возможности отложенного платежа
		// флаг возможности допроведения - _.deferredPayment
	} else if (!_.paymentSucceeded && _.recipientsArray.length != 1 && _.deferredPayment) {
		document.getElementById(_.activeRecipient.ID).className = "td2-nn";	
		document.getElementById("description"+_.activeRecipient.ID).innerHTML = getMessage('payment.payDeferred');		
	} else if (_.paymentSucceeded && _.recipientsArray.length != 1) {
		document.getElementById(_.activeRecipient.ID).className = "td2-ok";
		document.getElementById("description"+_.activeRecipient.ID).innerHTML = getMessage('payment.payOk');		
	}
}

} )();