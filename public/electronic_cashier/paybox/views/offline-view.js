﻿(function() {

_.addEventListeners({
	"load" : function() {

		document.getElementById("status").innerHTML = template('statusTemplate', getMessage('offline.status'));
		
		var info = document.getElementById("data");
		var borderedMessage = template('borderedMessageTemplate');
		
		info.innerHTML = borderedMessage(["<b>"+getMessage('offline.message')+"</b><br><br><img src='images/p1-2.gif'><br><br>"]);
	
	}

});
	
})();