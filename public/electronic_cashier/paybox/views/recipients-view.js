﻿(function() {

var cardWaiting = getParameter("paykiosk.cardWaiting", false);

// Количество получателей в строке
var recipientsPerRow;
// Максимальное количество получателей на странице
var recipientsPerPage;

_.addEventListeners({
	"load" : function() {
		if (!PaymentGateway.SelectedCatalogItem)
			return;
			
		// Текущая отображаемая страница (от 0 до _.lastPageNr )
		_.currentPageNr = 0;
		
		// пересчитываем количество получателей на страницу при их динамическом изменении
		reloadDim();
		// if (getParameter("paykiosk.showBanner", true)) {
			// document.getElementById('banner').innerHTML = '<embed src="images/banner.swf" type="application/x-shockwave-flash" width=100% height=100 onfocus="focus()"/>';
		// } else {
			// document.getElementById('banner').innerHTML = '';
		// };
		
		_.addEventListeners({
			"catalogItemChanged" : function(pageNr) {
				_.lastPageNr = Math.floor((PaymentGateway.SelectedCatalogItem.ChildItems.Count + recipientsPerPage - 1) / recipientsPerPage) - 1;
				_.currentPageNr = 0;
				updateView();
				ViewButtons.update();
			//alert (_.currentPageNr());	
	//RecipientTable.innerHTML == 1;
	//_.lastPageNr.innerHTML = "";
	//if (_.currentPageNr != "") { _.currentPageNr.innerHTML ='Оставьте заявку на добавление поставщиков на сайте express-bank.ru в разделе "Частным лицам - Платежные терминалы"';}
	
			},
			"cardPositionChanged" : function() {
				updateView();
				ViewButtons.update();
			},
			"key" : function(key) {
				switch(key) {
					case 8:
						break;
					case 27:
						_['selectParent']();
						break;
					case 33:
						_['prevPage']();
						break;
					case 34:
						_['nextPage']();
						break;
				}
			},
			"unableToPay" : function() { 		
					showAlert(getParameter('recipients.unableToPayWithReason', false)
						? getMessage('recipients.unableToPay')
						: getMessage('recipients.unableToPayPrinterOffline'),
						{ "ok" : hideAlert },
						{ 13 : "ok" },
						"ok"
					);
			},
			"billAcceptorFailureAlert" : function() {
					showAlert(getMessage('recipients.billAcceptorFailureAlert'),
						{ "ok" : hideAlert },
						{ 13 : "ok" },
						"ok"
					);				
			},
			"showDescriptionOnSelected" : function() {
					showAlert(getMessage('recipients.descriptionOnSelect'),
						{
							"yes" : function() {
									if (_['processSelectedItem']()) {
										_.fireEvent('actionsChanged');
										_.fireEvent('catalogItemChanged');
									}
							},
							"no" : _['selectParent']
						},
						{
							13 : "yes",
							27 : "no"
						},
						"no"
					);			
			},
			"catalogItemAskSelect" : function() {
					showAlert(getMessage('recipients.catalogItemAskSelect'),
						{
							"yes" : function() {
								if (!_.leave("groupSelected")) {
									_.getLogger().debug("Выбор группы не обработан сценарием, продолжаем отображение в текущей реализации состояния");
									return false;
								}
							},
							"no" : function() {
								return false;
							}
						},
						{
							13 : "yes",
							27 : "no"
						},
						"no");			
			},
			"cardReject" : function() {
				showAlert(getMessage("recipientsView.insertCardErrors", _.cardRejectReason)+'<br><br>'+
					'<b>'+getMessage("recipientsView.takeYourCard")+'</b><br><br>'+
					'<img src="images/card-eject.png"><br>'+
					'<span id="takeYourCard">'+getMessage("recipientsView.captureTimeout")+'</span>');	
			},
			"captureTick" : function (captureTimeout) {
				document.getElementById("captureTick").innerHTML = captureTimeout < 10 ? ("<font color='#FF0000'>" +captureTimeout+"</font>") : captureTimeout;
			},
			"startingApplication" : function(param) {
				if (param) {
					showAlert(getMessage("recipientsView.startingApplication") + '<br><br><img src="images/clock.png">');	
				} else
					hideAlert();
			}
		});

		_.registerActions({
			"nextPage" : new StateAction(
				function () { return _.currentPageNr < _.lastPageNr; },
				function () { setPageNr(_.currentPageNr + 1); }
			),
			"prevPage" : new StateAction(
				function () {return _.currentPageNr !== 0; },
				function () { setPageNr(_.currentPageNr - 1); }
			),
			"cardEject" : new StateAction(
				function () {return VirtuPOS.IsCardPresent; },
				function () { _['ejectCard'](); }
			)
		});
		ViewButtons.bind({
			'cancelButton' : ['prevPage', 'selectParent'], 
			'continueButton' : 'nextPage',
			'menuButton' : 'menu',
			'cardEjectButton' : 'ejectCard'
		});
		
		updateView();
		ViewButtons.update();
	},
	
	"actionsChanged" : ViewButtons.update,
	
	"inactivityTimer" : function() {
		setPageNr(0);
		updateView();
		ViewButtons.update();
		return true;
	}
});

function reloadDim() {
	if (!PaymentGateway.SelectedCatalogItem.inCategory("DisableFoundButton")) {
		// Количество получателей в строке
		recipientsPerRow = getParameter('recipientsView.recipientsPerRowFound', 4);
	} else {
		// Количество получателей в строке
		recipientsPerRow = getParameter('recipientsView.recipientsPerRow', 4);
	}
	// Максимальное количество получателей на странице
	recipientsPerPage = recipientsPerRow * getParameter('recipientsView.recipientsPerColumn', 4) - 1;
	// Номер страницы на которой еще есть данные
	_.lastPageNr = Math.floor((PaymentGateway.SelectedCatalogItem.ChildItems.Count + recipientsPerPage - 1) / recipientsPerPage) - 1;

	// Освобождаем место для кнопки Далее
	if (_.lastPageNr > 0 && _.currentPageNr !=_.lastPageNr) recipientsPerPage = recipientsPerPage - 1;
	
		
}

function setPageNr(pageNr) {
	if ((pageNr < 0) || (pageNr > _.lastPageNr)) {
		playSoundFunction("error.wav", false);
		return false;
	}

	_.currentPageNr = pageNr;

	updateView();
	ViewButtons.update();
}

function updateStatusText() {
	if (!PaymentGateway.SelectedCatalogItem.IsRoot || PaymentGateway.Locales.Count <= 1) {
		// не в корне
		document.getElementById('groupStatus').innerHTML = template("statusTemplate", getMessage("recipientsView.groupStatusText"));
		if (_.lastPageNr)
			document.getElementById('groupStatus').innerHTML += getMessage("recipientsView.pageOfThePage");
			
	} else {
		// в корне
		/*
		var localeTable = "";
		for (var i=0; i<PaymentGateway.Locales.Count; i++) {
			if (PaymentGateway.Locales.Item(i).Lang !="") {
				if (getActiveLocale() == PaymentGateway.Locales.Item(i).Lang) {
					localeTable += "<td width='30' onclick=''>";
					localeTable += "<img border=0 src='images\\" + PaymentGateway.Locales.Item(i).Lang + "\\flag_.png'>&nbsp;&nbsp;";
					localeTable += "</td>";
				} else {
					localeTable += "<td width='30' onclick=\"_['changeLocale']('" + PaymentGateway.Locales.Item(i).Lang + "')\">";
					localeTable += "<img border=0 src='images\\" + PaymentGateway.Locales.Item(i).Lang + "\\flag_.png'>&nbsp;&nbsp;"
					localeTable += "</td>";
				}
			}
		}
		
		document.getElementById('groupStatus').innerHTML = "<table class=statustxt><tr><td align='left' onClick='_[\"information\"]();' width='1'><img hspace='20' src='./images/i_sign.png'><span id='i-txt'>Информация</span></td><td></td>" + localeTable + "</tr></table>";
		*/
		document.getElementById('groupStatus').innerHTML = template("rootStatusTemplate", {"changeLocale" : template("changeLocaleTemplate", {})});
	}

	var balanceStatus = document.getElementById('balanceStatus');
	if (PaymentGateway.CustomerLoggedIn) {
		balanceStatus.innerHTML = getMessage("paykiosk.balanceFormat");
		balanceStatus.className = "visible";
	}

	var changeSumStatus = document.getElementById('changeSumStatus');
	if (PaymentDevice.HasAccumulatedSum) {
		changeSumStatus.innerHTML = template("changeInfoTemplate", getMessage("paykiosk.changeSum"));
		changeSumStatus.className = "visible";
	}
}



function updateView() {
	reloadDim();
	
	if (cardWaiting) {
		if (VirtuPOS.IsCardPresent)
			document.getElementById("cardWaitingMessage").className = "invisible";
		else	
			if (PaymentGateway.SelectedCatalogItem.IsRoot)
				document.getElementById("cardWaitingMessage").className = "visible";
			else 
				document.getElementById("cardWaitingMessage").className = "invisible";
	} else {
		document.getElementById("cardWaitingMessage").className = "invisible";
	}			

	var table= "";
	updateStatusText();
	
	// Кэшируем шаблон для использования в этой функции
	var drawRecipient = template('recipientTemplate');
	with (PaymentGateway.SelectedCatalogItem.ChildItems) {
		for (var i = _.currentPageNr * recipientsPerPage, c = Math.min(i + recipientsPerPage, Count); i<c; ) {
			for (var j = 0; (j < recipientsPerRow) && (i < c); ) {
				var thumbnailURL = Item(i).Thumbnail.URL && Item(i).Thumbnail.URL.length > 0 ? Item(i).Thumbnail.URL : null;

				if (_.cardPresent) {
					if (Item(i).InCategory("HiddenWhenCardInserted") && PaymentGateway.SelectedCatalogItem.IsRoot) {
						i++; continue;
					}	
				} else {
					if (Item(i).InCategory("HiddenWhenCardNotInserted")) {
						i++; continue;
					}
				}	
				
				var thumbnailPath = Framework_IfExistsThen("images-recipients/" + getActiveLocale() + "/" + thumbnailURL, "zzz", window);
				if ("zzz" === thumbnailPath) {
					thumbnailPath = Framework_IfExistsThen("images-recipients/" + thumbnailURL, "zzz", window);

					if ("zzz" === thumbnailPath)
						thumbnailPath = null;
				}
		
				table += drawRecipient({
					"thumbnailWidth" : Item(i).Thumbnail.Width && Item(i).Thumbnail.Width > 0 ? Item(i).Thumbnail.Width : 240,
					"thumbnailHeight" : Item(i).Thumbnail.Height && Item(i).Thumbnail.Height > 0 ? Item(i).Thumbnail.Height : 140,
					"thumbnailURL" : thumbnailPath,
					"name" : Item(i).Name,
					"idx" : i,
					"locale" : getActiveLocale()
				});
							
				j++;
				i++;
			}
		}
	}	
		
	if (_.mayExecute('nextPage')) {
		table += drawRecipient({
			"thumbnailWidth" : 250,
			"thumbnailHeight" : 142,
			"thumbnailURL" : "images/frmFind/Next.png",
			"name" : "",
			"idx" : -2,
			"locale" : getActiveLocale()
		});			
	} 		
	
	if (!PaymentGateway.SelectedCatalogItem.inCategory("DisableFoundButton")) {
		if (document.getElementById("data").className.indexOf("found") < 0) {
			document.getElementById("data").className += " found";
			document.getElementById("continueButton").className += " found";
		}	

		table += drawRecipient({
			"thumbnailWidth" : 250,
			"thumbnailHeight" : 142,
			"thumbnailURL" : "images/frmFind/Find.png",
			"name" : "",
			"idx" : -1,
			"locale" : getActiveLocale()
		});			
	} else {
		document.getElementById("data").className = document.getElementById("data").className.replace(" found", "");
		document.getElementById("continueButton").className = document.getElementById("data").className.replace(" found", "");
	}	


	table += '<br>';
	document.getElementById("data").innerHTML = table;
}
})()