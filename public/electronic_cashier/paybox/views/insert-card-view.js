﻿(function() {

_.addEventListeners({
	"load" : function() {
	
		ViewButtons.bind({
			"cancelButton" : "cancel"
		});		
	
		// Отображаем экран приглашения
		document.getElementById("status").innerHTML = template('statusTemplate', getMessage('insertCard.insertCardTitle'));
		document.getElementById("data").innerHTML = getMessage('insertCard.pleaseInsert') + "<br><br><div style='text-align:center;'><img src='images/card-insert.gif' /></div>";
		
		// Основной обработчик отображения
		_.addEventListeners({
			"actionsChanged" : ViewButtons.update,
			"cardReject" : function() {
				var borderedMessage = template('borderedMessageTemplate')
				showAlert(
					borderedMessage('<br>'+getMessage("insertCard.insertCardErrors", _.cardRejectReason)+'<br><br>')+
					'<b>'+getMessage("cardEject.takeYourCard")+'</b><br><br>'+
					'<img src="images/card-eject.gif"><br>'+
					borderedMessage('<span id="takeYourCard">'+getMessage("cardEject.captureTimeout")+'</span>')
				);	
			},
			"captureTick" : function (captureTimeout) {
				document.getElementById("captureTick").innerHTML = captureTimeout < 10 ? ("<font color='#FF0000'>" +captureTimeout+"</font>") : captureTimeout;
			},
			"startingApplication" : function(param) {
				if (param) {
					var borderedMessage = template('borderedMessageTemplate');
					showAlert(
						'<br><br><br>'+borderedMessage('<br>'+getMessage("insertCard.startingApplication")+
						'<br><br><img src="images/clock.png"><br><br>')
					);	
				} else
					hideAlert();
			}
		});
		ViewButtons.update();
	},
	
	"key" : function(key) {
		switch(key) {
			// backspace
			case 8 :
			//esc
			case 27 :
				_['cancel']();
				break;
				
			//return
			case 13 :
				_['commit']();
				break;
		}
		return false;
	}
});
})();