﻿
// Отображение информации при отмене платежа
_.addEventListeners({
	"load" : function() {
		showAlert(
		"Отменить платеж и вернуть внесенные деньги?", 
		{
			"yes" : _['commit'],
			"no" : _['cancel']
		},
		{
			27 : "no",
			8  : "yes"
		})
	},
	
	"key" : function(key) {
		switch(key) {
			// backspace
			case 8 :
			//esc
			case 27 :
				_['cancel']();
				return false; 
		}
		return true;
	},

	"run" : function() {
		
	},
	
	"actionsChanged" : function() {
		if (_.mayExecute("cancel")) {
			//document.getElementById("cancelTable").style.visibility = "visible";
			//document.getElementById("overLay").style.visibility = "hidden";
		} else {
			//document.getElementById("cancelTable").style.visibility = "hidden";
			//document.getElementById("overLay").style.visibility = "visible";
		}
	}	
});

