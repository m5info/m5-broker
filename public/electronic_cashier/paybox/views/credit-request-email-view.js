﻿(function() {
_.addEventListeners({
	"load" : function() {
		
		/*ViewButtons.bind({
			'cancelButton' : 'commit', 
			'cardEjectButton' : 'ejectCard'
		});	*/

		_.addEventListeners({
			"mailSending" : function () {
				mailSending();
			},
			"mailSend" : function () {
				mailSend();
			}
		})
	}
});

function mailSending() {
	showAlert(getMessage("creditRequestEmail.mailSending"),
		{},
		{},
		"");
}

function mailSend() {
	if (_.sendSuccess) {
		showAlert(
			getMessage("creditRequestEmail.mailSendSuccess"),
			{
				"ok" :  function() { _["commit"]() }
			},
			{
				27: "ok"
			},
			"ok", 10000
		);
	} else {
		showAlert(
			getMessage("creditRequestEmail.mailSendError"),
			{
				"ok" :  function() { _["commit"]() }
			},
			{
				27: "ok"
			},
			"ok", 7000
		);	
	}
}


})();