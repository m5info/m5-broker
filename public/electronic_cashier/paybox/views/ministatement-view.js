﻿(function() {
_.addEventListeners({
	"load" : function() {
		_.addEventListeners({
			"operationComplete" : onOperationComplete,
			"printerSFailure" : function() {
				showAlert(getMessage('ministatement.printerFailure'), 
					{
						"ok" : _["continue"]
					},
					{
						13 : "ok"
					},
					"ok",
					12000
				);
			},
			"printDone" : function() {
				showAlert(getMessage('ministatement.printDone'), 
					{
						"ok" : _["continue"]
					},
					{
						13 : "ok"
					},
					"ok",
					12000
				);
			}
		});

		showWaitClock();
	}
	
});

function onOperationComplete() {

	hideWaitClock();
	

	if (_.operationSucceeded) {
			showAlert(getMessage('ministatement.makeChoice'), 
				{
					"receipt" : _["printBC"],
					"screen" : showMinistatement
				},
				{},
				_["continue"],
				15000
			);		
		return false;
	} else {
			showAlert(getMessage('ministatement.failed'), 
				{
					"ok" : _["continue"]
				},
				{
					13 : "ok"
				},
				"ok",
				10000
			);			
		return false;
	}
	
}

function showMinistatement() {

var ministatement = "";

		if (_.collection.Count>0) {
			ministatement += "--------------------------------------------<br>";
			ministatement += " Дата операции ¦    Тип     ¦    Сумма<br>";
			ministatement += "---------------+------------+---------------<br>";		
			for (var i=0;i<_.collection.Count;i++) {
				ministatement += formatPaymentsRegisterStr(_.collection.Item(i).Date, _.collection.Item(i).IsDebit, getCurrency(_.collection.Item(i).CurrencyCode).format(_.collection.Item(i).Amount/100, 'sign'))+"<br>";
			}
			ministatement += "---------------------------------------------<br>";
		} else {
			ministatement += "<center>----</center><br>";		
			ministatement += "Операции за период минивыписки по карте не производились<br>";		
			ministatement += "<center>----</center><br>";		
		}
		
		showAlert(ministatement, 
			{
				"ok" : _["continue"]
			},
			{
				13 : "ok"
			},
			"ok",
			getParameter("ministatement.showTime", 30000)
		);

}

})();