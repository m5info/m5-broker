﻿(function() {// Отображение информации
_.addEventListeners({
	"load" : function() {

		_.addEventListeners({
			"operationComplete" : onOperationComplete,
			"executingVirtuPOSMethod" : function() {
				hideWaitClock();
				hideAlert();
				showAlert(getMessage('VirtuPOS.executingVirtuPOSMethod'));
			},
			"tryVirtuPOSMethodAgain" : function() {
				hideAlert();
				showAlert(getMessage('VirtuPOS.tryVirtuPOSMethodAgain'));
			}			
		});
		
		showWaitClock();
	}
});

function onOperationComplete() {

	hideAlert();
	
	if (!_.operationSucceeded) {
		showAlert(getMessage("peerToPeer.operationFailed"),
			{ "OK": _['continue'] },
			{ 13 : "OK", 27 : "OK" }, 
			"OK", 
			7000			
		);
		return false;
	}
}
})();