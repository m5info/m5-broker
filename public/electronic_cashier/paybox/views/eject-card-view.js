﻿function soundLoop() {
	playSoundFunction("error.wav", false);
}	

_.addEventListeners({
	"load" : function() {
	
		
	var paymentInfo = document.getElementById("data");
	var errorMsg = Framework.Variables("EjectReason") != "" ? Framework.Variables("EjectReason")+"<br><br>" : "";
	paymentInfo.innerHTML = errorMsg +"<b>"+getMessage('cardEject.takeYourCard')+"</b><br><br>";
	paymentInfo.innerHTML += "<img src='images/card-eject.png'><br><br>";		
	paymentInfo.innerHTML += getMessage('cardEject.captureTimeout');
	
	soundLoop();
	soundInterval = setInterval(soundLoop, 700);
	
	// Основной обработчик отображения
	_.addEventListeners({
		"captureTick" : function (captureTimeout) {
			document.getElementById("captureTick").innerHTML = captureTimeout < 10 ? ("<font color='#FF0000'>" +captureTimeout+"</font>") : captureTimeout;
		}
	});
	}
});