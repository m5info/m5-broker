﻿(function() {
_.addEventListeners({
	"load" : function() {

			ViewButtons.bind({
			'cancelButton' : 'cancel', 
			'menuButton' : 'menu',
			'cardEjectButton' : 'ejectCard'
		});	
	},
	"run" : showPaymentTypes
});

function showPaymentTypes() {
	for (var i=0; i < PaymentGateway.SelectedRecipient.PaymentTypes.Count; i++) {
		document.getElementById("data").innerHTML += "<div class='paymentType'><table onclick=_['selectPaymentType'](" + i + ")><tr><td><div class='paymentTypeTop'></div><div class='paymentTypeData'>" + PaymentGateway.SelectedRecipient.PaymentTypes.Item(i).Name + "</div><div class='paymentTypeBottom'></div></tr></td></table></div>";
	}
}

})();