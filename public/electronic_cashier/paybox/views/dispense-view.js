﻿(function() {
// Отображение информации
_.addEventListeners({
	"load" : function() {
		// Подписываемя на основное событие состояния, завершение операции
		_.addEventListeners({
			"operationComplete" : onOperationComplete,
			"dispenseStart" : function () {
				showAlert(getMessage("dispense.startDispense"));
			},
			"dispenseComplete" : function () {	
				if (_.waitForTake == null || _.waitForTake.length == 0) {
					// Все забрали или нет презентера
					showAlert(getMessage("dispense.completeMessage"),
						{
							"ok" : _['nextDispense']
						},
						{
							13 : "ok",
							27 : "ok"
						},
						_['retractNotes'],
						getParameter("dispenser.takeTimeout", 15000)
					);
				} else {
					showAlert(getMessage("dispense.completeMessage"),
						{},
						{},
						_['retractNotes'],
						getParameter("dispenser.takeTimeout", 15000)
					);
				}
				return false;
			},
			"taken" : hideAlert
		});
		// Привязываем кнопки к действиям если возможно
		//ViewButtons.bind({'cancelButton' : 'cancel', 'continueButton' : 'continue'});
	},
	// Обновление состояния кнопок
	"actionsChanged" : ViewButtons.update
});


// Обработка окончания выполнения операии
//  Событие может быть с параметрами как здесь и без
function onOperationComplete() {
	// Убираем экран ожидания
	//hideWaitClock();
	// в завимости от результатов и задач отображаем результат исполнения:
	if (_.operationSucceeded) {
		// Операция завершена успешно.
		// Возможен вызов continue либо cancel
		// Если выполнен  continue либо cancel то требуется return false
		// return; // View не выполнил действий -> поведение по умолчанию
	}
	// Операция завершена неуспешно
	if (_.operationCommitAllowed) {
		// Если выполнение commit разрешено, то view может спросить разрешение на продолжение у клиента
		// Если клиент согласен то должно вызваться _.['commit']()
		// Если запрос нет то выход по умолчанию
		// return;
	}
	// Проигнорировать ошибку нельзя 
	// Здесь можно просто уведомить пользователя об ошибке
	// return;
}
})()
