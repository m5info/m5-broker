﻿(function() {

_.addEventListeners({
	"load" : function() {
	
		_.registerVariables({
			"paymentInfoTemplate" : document.getElementById('paymentInfoTemplate').innerHTML,
			"fieldValueTemplate" : document.getElementById('fieldValueTemplate').innerHTML
		});	
	
		_.addEventListeners({
			"textChanged" : function(state) {
				// возможны различные варианты сообщений в зависимости от состояния штрих-кодов получателя
				// подробнее смотрите в локали
				document.getElementById("data").innerHTML = getMessage('barcodeEntry.states.'+state);
			},		
			"allFieldsComplete" : function() {
				showAlert(
					getMessage('barcodeEntry.allFieldsComplete'),
					{ 
						"yes" : _['correct'],
						"no" : _['commit']
					},
					{
						13 : "yes",
						27 : "no"
					},
					"no",
					15000
				)					
				return false;						
			},
			"barcodeComplete" : function() {
				showAlert(
					getMessage('barcodeEntry.barcodeReadSucceeded'),
					{ "ok" : 
						function() { 
							hideAlert();
							_['testBarcodesState']();							
						} 
					},
					{8 : "ok", 13 : "ok", 27 : "ok"},
					"ok",
					5000
				)
				return false;
			},
			"barcodeError" : function() {
				showAlert(
					getMessage('barcodeEntry.barcodeReadFailed'),
					{ "ok" : 
						function() { 
							hideAlert(); 
							_['enableReading']();							
						} 
					},
					{8 : "ok", 13 : "ok", 27 : "ok"},
					"ok",
					8000
				)
				return false;
			},
			"validateRecipientFailed" : function() {
				showAlert(
					getMessage('barcodeEntry.validateRecipientFailed'),
					{ "ok" : _['cancel'] },
					{8 : "ok", 13 : "ok", 27 : "ok"},
					"ok",
					10000
				)
				return false;				
			}
		});
		
		ViewButtons.bind({'cancelButton' : 'cancel', 'continueButton' : 'continue'});

	},

	"actionsChanged" : ViewButtons.update
});

})();