﻿( function() {

_.addEventListeners({
	"load" : function() {
	
		document.getElementById("status").innerHTML = template('statusTemplate', getMessage('sumEntry.status'));
	
		_.registerVariables({
			"fieldValueTemplate" : document.getElementById('fieldValueTemplate').innerHTML,
			"paymentInfoTemplate" : document.getElementById('paymentInfoTemplate').innerHTML,
			"paymentInfoHeight" : null
		});	
	
		_.addEventListeners({
			"key" : function(key) {
				switch(key) {
					// backspace
					case 8:
					case 46:
						_["removeLastChar"]();
						return false;
					case 13:
						if (_.mayExecute('commit')) {
							_['commit']();
						} else {
							playSoundFunction("error.wav", false);
						}	
						return false;
					//esc	
					case 27:
						_['cancel']();
						return false;
				}
			},
			"char" : function(ch) {
				if (!_["appendChar"](ch))
					playSoundFunction("error.wav", false);
				return false;
			},
			"sumChanged" : function () {
				updateView();
			},
			"maxLimitReached" : function() {
					showAlert(getMessage("sumEntry.maxBalanceLimitReached"),
					{ "OK": hideAlert },
					{ 13 : "OK", 27 : "OK" }, 
					"OK", 
					7000			
				);
			}
		});
		
		ViewButtons.bind({
			"cancelButton" : "recipients",
			"continueButton" : "commit",
			'cardEjectButton' : 'cancel'
		});
		
		if (_.recipient.TransferAmountType == 1) {
			showAlert(getMessage("sumEntry.fixedSumRecipient"),
				{
					"yes" : _['commit'],
					"no" : _['cancel']
				},
				{
					13 : "yes",
					27 : "no"
				},
				"no"	
			);
			document.getElementById("alertMessageDiv").className = "fixed";
			return;
		}

		initialDisplay();		
		
	},
	
	"actionsChanged" : ViewButtons.update	
});

function initialDisplay() {
	showKeypad();
	var keyPadDiv = document.getElementById("keypadDiv");
	keyPadDiv.className = "keyPadDiv";
	
	// SummTable
	document.getElementById("paymentSumStr").innerHTML = getMessage("sumEntry.paymentSumStr");
	document.getElementById("transferSumStr").innerHTML = getMessage("sumEntry.transferSumStr");
	document.getElementById("commissionSumStr").innerHTML = getMessage("sumEntry.commissionSumStr");
	
	document.getElementById("fieldsInfo").innerHTML = "<b>"+getMessage("sumEntry.statusRecipient")+"</b><br>";
	if (getParameter("sumEntry.showFields", false)) {
		// Hint
		document.getElementById("fieldsInfo").innerHTML += "<table id='fieldsTable'>"+enumerateRecipientFields(_.recipient.Fields, _.fieldValueTemplate, false)+"</table>";
	} else {
		document.getElementById("fieldsInfoDiv").style.display = "none";
	}
	
	document.getElementById('PaymentSum').innerHTML = getCurrency().format(PaymentDevice.AccumulatedSum(_.recipient.CurrencyCode).Value);
	document.getElementById('TransferSum').innerHTML = getCurrency().format(_.recipient.TransferSum);
	document.getElementById('CommissionSum').innerHTML = getCurrency().format(_.recipient.CommissionSum);

	/* Commissions */
	var	commissionText = document.getElementById('commissionText');
	commissionText.innerHTML = "";
	var commissionRules = returnCommissionRules(_.recipient, _.recipient.CurrencyCode);
	for (var i = 0; i < commissionRules.length; i++) {
		commissionText.innerHTML += "&nbsp;&nbsp;<img src='images\\bullet.gif'>&nbsp;" + commissionRules[i] + "<br />";
	}
		
	if (_.recipient.MinLimit >0)	
		commissionText.innerHTML += "&nbsp;&nbsp;<img src='images\\bullet.gif'>&nbsp;"+getMessage("sumEntry.minSumm")+"<br />";
	if (_.recipient.MaxLimit >0)		
		commissionText.innerHTML += "&nbsp;&nbsp;<img src='images\\bullet.gif'>&nbsp;"+getMessage("sumEntry.maxSumm");

	// расчёт высоты блока paymentInfo
	var top = document.getElementById("paymentInfo").currentStyle.top;
	var bottom = document.body.clientHeight - document.getElementById("footer").clientHeight;
	top = Number(top.substring(0, top.length-2));
	_.paymentInfoHeight = bottom - top;		
		
	updateView();
	alignContent();
}

function updateView() {
	document.getElementById('PaymentSum').innerHTML = getCurrency().format(_.recipient.PaymentSum);
	document.getElementById('TransferSum').innerHTML = getCurrency().format(_.recipient.TransferSum);
	document.getElementById('CommissionSum').innerHTML = getCurrency().format(_.recipient.CommissionSum);
}

function alignContent() {	
	// определение суммарной высоты содержимого
	var fieldsInfoHeight = document.getElementById("fieldsInfoDiv").offsetHeight;
	var SummTable = document.getElementById("SummTable");
	var commissionTextDiv = document.getElementById("commissionTextDiv");
	var contentHeight = fieldsInfoHeight + SummTable.offsetHeight + commissionTextDiv.offsetHeight;
	// выравнивание блоков по высоте, либо уменьшение шрифта в таблице полей
	if (_.paymentInfoHeight > contentHeight) {
		fieldsInfoHeight = document.getElementById("fieldsInfoDiv").offsetHeight;
		var freespace = _.paymentInfoHeight - contentHeight;
		SummTable.style.top = freespace/2;
		commissionTextDiv.style.top = freespace;		
	} else if (_.paymentInfoHeight < contentHeight)
		resizeFont();
}

function resizeFont() {
	// уменьшение шрифта в таблице полей
	var currentFontSize = document.getElementById("fieldsTable").currentStyle.fontSize;
	currentFontSize = currentFontSize.substring(0, currentFontSize.length-2);
	if (currentFontSize > 11)
		document.getElementById("fieldsTable").style.fontSize = currentFontSize - 1;
	else
		document.getElementById("fieldsInfo").innerHTML = "<b>"+getMessage("sumEntry.statusRecipient")+"</b>";
	
	alignContent();
}

})();