﻿(function() {

var frmFind_findText = "";

//кнопка, которую нажали
var pressKey_key;

var isRus = true;

var arrRecipientsHash = Framework.Variables("arrRecipientsHash");

var arrFindRecipients = [];

_.addEventListeners({
	"load" : function() {
		// document.getElementById("data").innerHTML = getMessage("creditRequestInfo.info");
		
			ViewButtons.bind({
			'cancelButton' : 'cancel', 
			'menuButton' : 'menu',
			'cardEjectButton' : 'ejectCard'
		});	
			
		_.registerActions({
			"inputKey" : function (key) {
				inputKey(key);
			},
			"removeKey" : function () {
				removeKey();
			},
			"pressKey" : function (key) {
				pressKey(key);
			},
			"ChangeKeyboardLayout" : function () {
				ChangeKeyboardLayout();
			},
			"nextPage" : function () {
				nextPage();
			},
			"prevPage" : function () {
				prevPage();
			}
				
		});
		
		_.addEventListeners({				
			"key" : function(key) {
				switch(key) {
					// backspace
					case 8 : {
						_["removeKey"]();
						return false;
					}
					// delete
					case 46 : {
						_["removeKey"]();
						return false;
					}
					
					//return	
					case 13 : {

						return false;
					}
						
					//esc	
					case 27 : {
/*							if (leftBlock.style.display == "none") {
								hideAlert();
								correctionFields();
							} else*/
							_["menu"]();
						return false;
						}
						
					//вверх

				}
			},
			"char" : function(ch) {
				_["inputKey"](ch);				
			},
			"unableToPay" : function() { 		
				showAlert(getParameter('recipients.unableToPayWithReason', false)
					? getMessage('recipients.unableToPay')
					: getMessage('recipients.unableToPayPrinterOffline'),
					{ "ok" : hideAlert },
					{ 13 : "ok" },
					"ok"
				);
			}
		});
	},
	"run" : function() {
		FindRecipients();
		ShowFindRecipients();
	}
});

//добавляем символ в строку ввода
function inputKey(key) {
	//ограничиваем длину текста
	if (frmFind_findText.length < 23 && arrFindRecipients.length > 0) frmFind_findText += key;	//добавляем символ к строке поиска
	//отображаем текст в поле ввода
	document.all.fieldValue.innerText = frmFind_findText;
	//ищем поставщиков
	FindRecipients();
	//отображаем найденных поставщиков
	ShowFindRecipients();
}

//удаляем символ из строки ввода
function removeKey() {
	//если стринг не пустой, отрезаем от него один символ
	if (frmFind_findText.length > 0) frmFind_findText = frmFind_findText.substr(0, frmFind_findText.length - 1);
	//ищем поставщиков
	FindRecipients();
	//отображаем найденных поставщиков
	ShowFindRecipients();
}

//ищем поставщиков
function FindRecipients() {
	//сбрасываем массив с найденными получателями
	arrFindRecipients = [];
	// alert([arrFindRecipients.length, arrFindRecipients])
	//текущая страница
	frmFind_CurPage = 1;
	//проверяем не пустая ли строка поиска	(а иначе при пустой строке будут отображаться все ресипиенты)
	//if (frmFind_findText.length < 1) return;
	//текущий каталог
	// var Catalog = PaymentGateway.SelectedCatalogItem;

	//строка поиска
	var strText = frmFind_findText.toLocaleUpperCase();

	//пробегаемся по хешированному массиву получателей и ищем соответсующих строке поиска
	for (var i = 0; i < arrRecipientsHash.length; i++)
	{
		//имя текущего ресипиента в верхнем регистре
		var findString = arrRecipientsHash[i].findString.toLocaleUpperCase();
		//проверяем есть ли в этом имени подстрока поиска
		if (strText == "" || findString.indexOf(strText) >= 0)
		{
			//включаем данного получателя в массив найденных получателей
			arrFindRecipients.push(arrRecipientsHash[i]);
		}
	}
		// alert([arrFindRecipients.length, arrFindRecipients])

	// alert(arrFindRecipients[0].thumbnail)
}

//отображаем найденных поставщиков
function ShowFindRecipients()
{
	//всего кнопок в каталоге
	var nCount = arrFindRecipients.length;
	//столбцов на странице  
	var nCols = 5;
	//надпись "страница 1 из 3", если же страница только одна, то будет &nbsp
	var PageText = "&nbsp";
	//всего страниц
	var nPages = 1;
	//проверяем нужно ли более одной страницы для отображения всех картинок
	//т.е. разбираемся с надписью типа "страница 1 из 3"
	if (nCount > nCols)nPages = Math.ceil(nCount / nCols); //количество страниц
	//проверяем номер страницы
	if (frmFind_CurPage > nPages) frmFind_CurPage = 1;			
	//строка типа "страница 1 из 3"
	if (nPages > 1) PageText = "страница " + frmFind_CurPage + " из " + nPages;
	//начинаем таблицу, в оторой располагаются кнопки ресипиентов
	var table = "<table id='catalogTable' border=0 cellspacing=0 cellpadding=10 width=auto height=auto>\r\n";
	//строка типа "страница 1 из 3"
	table += " <tr valign=middle style='color: gray'>\r\n";
	table += "   <td colspan=" + (nCols+1) + " align='right'>" + PageText + "</td>\r\n";
	table += " </tr>";
	//индекс картинки в верхнем/левом углу страницы
	var lt = (frmFind_CurPage-1) * nCols;
	//осталось картинок в каталоге
	var nItems = nCount - lt;
	//текущая колонка, в которую вставляем картинку
	var iCol = 0;
	//новая колонка
	table += " <tr valign=middle>\r\n";
	//вставляем кнопку назад
	if (nPages > 1)
		if (frmFind_CurPage != 1)
			//вставляем картинку в колонку и реакцию на нажатие 'prevPage()'
			table += "  <td align=center onclick=_['prevPage']()><img src='images/frmFind/frmFind_Prev.png'/></td>\r\n";
		else
			//вставляем пустую картинку в колонку 
			table += "  <td align=center><img src='images/frmFind/frmFind_NextPrevTransparent.png'/></td>\r\n";
	//расставляем картинки в таблицу
	for (var i = nCols , index = lt; i > 0 && index < nCount; i--, index++)
	{
		//проверяем, если картинка пустая, то делаем пустую колонку
		if (arrFindRecipients[index].thumbnail == "")
			table += "  <td align=center onclick=''></td>\r\n";
		//вставляем картинку в колонку и реакцию на нажатие  'selectItem(index)'
		else
			table += "  <td align=center onclick=_['selectItem'](" + arrFindRecipients[index].id + ")>" + formatCatalogItemFind(arrFindRecipients[index].thumbnail) + "</td>\r\n";
			//table += "  <td title='" + arrFindRecipients[index].Name + "' align=center onclick='selectItem(" + index + ")'>" + formatCatalogItemFind(arrFindRecipients[index]) + "</td>\r\n";

		//увеличиваем колонку
		iCol++;
	}

	//расставляем пустые картинки в таблицу
	if(nPages>1)
		for (; iCol < nCols; iCol++)
			//вставляем колонку с прозрачной кнопкой
			table += "  <td align=center onclick=''><img src='images/frmFind/frmFind_RecipientEmpty.png'/></td>\r\n";

	//вставляем кнопку Далее
	if (nPages > 1)
		if (frmFind_CurPage != nPages)
			//вставляем картинку в колонку и реакцию на нажатие 'nextPage()'
			table += "  <td align=center onclick=_['nextPage']()><img src='images/frmFind/frmFind_Next.png'/></td>\r\n";
		else
			//вставляем пустую картинку в колонку 
			table += "  <td align=center><img src='images/frmFind/frmFind_NextPrevTransparent.png'/></td>\r\n";
	
	//завершаем строку
	table += " </tr>\r\n"; 
	
	//строка для "уравновешивания" надписи "страница 1 из 3"
	table += " <tr valign=middle>\r\n";
	table += "   <td colspan=" + (nCols+1) + " align='right'>&nbsp</td>\r\n";
	table += " </tr>";

	//заканчиваем общую таблицу с кнопками 
	table += "</table>";

	//проверяем вдруг нету ресипиентов
	if (nCount == 0)
		if (frmFind_findText.length > 0)
			table = "<table height='100%'><tr valign='middle'><td><div class='findValue' style='text-align:center'>Провайдеров, соответствующих введеному названию,<br/>не найдено</div></td></tr></table>";
		else
			table = "<table height='100%'><tr valign='middle'><td><div class='findValue' style='text-align:center'>Не введено ни одной буквы в названии провайдера</div></td></tr></table>";

	//теперь полученную табличку ставим в тег <div id="RecipientFindTable">
	document.all.RecipientFindTable.innerHTML = table;
	
	//отображаем текст в поле ввода
	if (frmFind_findText.length > 0)
		document.all.fieldValue.innerText = frmFind_findText;
	else
		document.all.fieldValue.innerText = "<название провайдера>";
	
}

//переход на следующую страницу (используется при наличии кнопки Далее в поиске)
function nextPage()
{
	playSoundFunction("click.wav", false);
	frmFind_CurPage++;
	ShowFindRecipients();  //обновляем страничку
}

//переход на следующую страницу (используется при наличии кнопки Далее в поиске)
function prevPage()
{
	if(frmFind_CurPage>1)frmFind_CurPage--;
	ShowFindRecipients();  //обновляем страничку
}


function formatCatalogItemFind(pic)
{
	return "<img src='images/frmFind/" + pic + "'>";
}

//изменить раскладку клавиатуры
function ChangeKeyboardLayout()
{
	//проверяем, какая клавитура включена
	if (document.all.KeysRus.style.display == 'inline') 
	{
		document.all.KeysRus.style.display = 'none';
		document.all.KeysEng.style.display = 'inline';
		document.all.numRusEng.innerHTML = "Rus";
	}
	else
	{
		document.all.KeysRus.style.display = 'inline';
		document.all.KeysEng.style.display = 'none';
		document.all.numRusEng.innerHTML = "Eng";
	}
}

//нажали на кнопку
function pressKey(keyid)
{
	//запоминаем старое
	pressKey_key = document.getElementById(keyid);
	//нажатый цвет
	pressKey_key.style.color='red';
	//возвращаем старый цвет
	setTimeout(function () { pressKey_key.style.color='black'; }, 200);
}


})();