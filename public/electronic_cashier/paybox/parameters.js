﻿appendParameters({
	"paykiosk" : {
		"receiptHeader" : "",
		"receiptFooter" : "\nИнформационная служба 8-800-10-80-70\n................................................\nСпасибо за использование системы",
		// Показ часов с циферблатом
		"clockOn" : false,
		"defaultAlertTimeout" : 100000,
		// Язык, который будет выбран для интерфейса по умолчанию
		"defaultLocale" : "ru",
		// Отображать предупреждение о невозможности печати чека перед приемом денег
		"printerOfflineWarning" : true,
		// Валюта, в которой считается кошелек
		"walletCurrency" : "RUR",
		// Явное задание количества разрядов, в соответствии с которым будет выполняться 
		// группировка по разрядам в денежных суммах. Если параметер отсутствует, по умолчанию = 3
		"digitGrouping" :  {"quantity" : 3, "separator" : " "},
		// Занулять сумму сдачи при инициализации
		"resetChangeSum" : true,
		// Ожидание карты на recipients
		"cardWaiting" : true
		
	},
	
	// выставьте значение таймаута для состояния в миллимесундах,
	// чтобы переназначить его для данного состояния.
	// для защиты от неправильного поведения минимальное значение таймаута здесь = 1000 мс
	// все значение < 1000 будут проигнорированы
	"inactivityTimeouts" : {
	
	// конкретный "экран":
	// "<state>-<extension>" : [число]
	// множество "экранов", объединённых общим состоянием:
	// "<state>" : [число]
	// 
	// ПРИМЕР: 
	// "data-entry-CardWrite" : 30000,
	// "insert-card": 15000
	// 
		
	/*	 ниже несколько глобальных таймаутов, не убирайте их		  	*/
	/*	 обратите внимание, что их также нужно выставлять >= 1000 мс 	*/
	
	// уменьшенное время для pin-entry, чтобы не зависал PIN пад
		"pin-entry" : 10000,
	// время показа сообщения "Вам нужно дополнительное время?", если карта внутри
		"cardAskForMoreTime" : 8000,
	// время показа сообщения "Вам нужно дополнительное время?", если происходит приём наличных
		"acceptPaymentAskForMoreTime" : 8000
	},
	
	"sounds" : {
	
	// конкретный "экран":
	// "<state>-<extension>" : "файл.wav"
	// множество "экранов", объединённых общим состоянием:
	// "<state>" : "файл.wav"
	// 
	// ПРИМЕР: 
	// "data-entry-CardWrite" : "alert_1.wav",
	// "insert-card": "alert_2.wav"
	// 
	
	},
	
	"barcodeEntry" : {
		// Автоматически переходить на следующий шаг, если введены достаточные штрих-коды
		"commitWhenDefined" : true
	},
	
	"recipients" : {
		// Показывать или нет причину, по которой невозможно провести платёж
		"unableToPayWithReason" : true,
		// Показывать или нет описание получателя, если оно есть
		//TODO: По умолчанию - FALSE???
		"showDescriptionOnSelected" : true,
		// Автоматически выбирать получателя, если он единственный в группе
		"autoSelectRecipent" : false,
		// Автоматически выходить из каталога в родительский каталог если поднимаясь наверх попадаем в динамический XML каталог
		"autoSelectParentXMLCatalog" : true

	},
	
	"recipientsView" : {
		// количество получателей в колонке и соответственно строк на экранах каталога
		"recipientsPerColumn" : 3,
		// количество получателей в строке и соответственно колонок на экранах каталога
		"recipientsPerRow" : 4
	},

	"dataEntry" : {
		// Ограничение на максимальную длину вводимой строки
		"maximumStringLength" : 50
	},

	"virtuposCheck" : {
		// Виртуальная сумма, используемая при проверке
		"virtualSum" : 501.11
	},
	
	"dataCheck" : {
		// Разрешить платеж при временных ошибках
		"allowSkipCheck" : false,
		// Показывать информацию об ошибке
		"showCheckError" : true,
		// Показывать окно после проверки с информацией - "Проверка прошла успешно. Подтвердите введенные данные"
		"responseTextCheck" : true
	},
	
	"afterDataCheck" : {
		// Время (в мс) показа уведомления о неработающем принтере
		"printerOfflineAlertTimeout": 30000
	},

	"select" : {
		// Запрашивать подтверждение при удачной проверке
		"showConfirmOnSelect" : false,
		// Показывать информацию об ошибке
		"showCheckError" : true	
	},

	"selectPaymentMethod" : {
		// выводить предупреждения об оплате получателей с фиксированный / минимальной / рекомендованной суммами
		"showFixedSummMessage" : true
	},
	
	"acceptCash" : {
		// Таймаут на странице приема денег  в мс (изменять не рекомендуется)
		"waitTimeout" : 60000, 
		//возможность работы со сдачей при платеже НЕ на фиксированную сумму
		//при платеже на фиксированную сумму работа со сдачей разрешена автоматически
		"changePossible" : false,
		// не принимать купюры номиналов менее минимальной суммы получателя (для оплат одного получателя, а не коллекции)
		"rejectLessThanMin" : false
	},
	
	"acceptCashView" : {
		//вывод названия получателя и  полей получателя (номер телефона) на странице ввода денег
		"showFieldsAcceptPayment" : false,
		// показывать на экране оплаты принятую сумму и  сумму с учетом комиссии и сумму комиссии
		"showWithComission" : true
	},
	
	"sumEntry" : {
		//вывод названия получателя и полей получателя (напр. - номера телефона) на странице ввода денег
		"showFields" : false,
		// запрашивать подтверждение перед оплатой получателя с фиксированной суммой (иначе - автоматически)
		"confirmFixedSum" : true
	},

	"payment" : {
		// асинхронное проведение платежа
		"asyncPayment" : true,
		// останавливаться для показа сводной таблицы оплаты коллекции
		"showResults" : true
	},
	"printCoupon" : {
		// выводить или нет логотип на чеке
		"printLogo" : true,
		// время показа сообщения при успешной/неуспешной печати [мс]
		"printAlertShowTime" : 7000
	},
	"cardPayment" : {
		// останавливаться для показа сводной таблицы оплаты коллекции
		"showResults" : true,	
		// карточный платёж методом VirtuPOS.Purchase
		"cardPurchase" : true,
		// не учитывать комиссию при карточном платеже
		"disableCardPaymentCommission" : false,
		// после пополнения карты с использованием карты повторно запрашивать баланс для печати обновлённого баланса в чеке
		"showBalance" : false,
		// Выдача наличных по карте
		"issueOfCash" : false
	},
	"verifyCard" : {
		//Делать проверку карты перед проведением платежа по карте
		"cardVerify": true,
		//Делать проверку Чужой карты перед проведением платежа по карте при cardVerify = TRUE (см. c:\VirtuPOS\conf\bintable.csv)
		"cardVerifyForeignCards" : true,
		//Делать проверку карты перед проведением платежа по карте методом Запроса Баланса - TRUE, или Verify - FALSE
		"cardVerifyBalance" : true
	},
	"pinEntry" : {
		// время ожидания ответа от пинпада при вставке карты в ридер
		"waitPinpadTimer" : 15000
	},
	"ministatement" : {
		// время показа минивыписки на экране (мс)
		"showTime" : 30000
	},	
	"showBalance" : {
		// время показа баланса на экране (мс)
		"showTime" : 15000
	},
	"dispenser" : {
		"hasPresenter" : true,
		"takeTimout" : 15000,
		"retractTimeout" : 10000
	}
});