﻿appendNavigationRules({
	// Общие выходы для всех состояний
	"*" : {
		// переход в offline
		"offline" : "offline",
		
		// переход в online
		"online" : "initialize",
	
		// Устройства и мониторинг
		"billAcceptorFailure" : "billAcceptor-failure",
		
		"billAcceptorCassette" : "service-entry",
		"billDispenserCassette" : "service-entry",
		
		"printerFailure" : "printer-failure",
		"billDispenserFailure" : "billDispenser-failure",
		
		"timeout" : "initialize",
		"catalogLost" : "initialize"
		// Разделение суммы, выход одинаковый для всех AcceptPayment состояний
	},
	
	// Новая точка входа в киоск, показ состояния оборудования
	"start" : {
		"continue" : "initialize"
	},
	
	// Состояние инициализации
	"initialize" : {
		"ejectCard" : "eject-card",
		"rootSelected" : [
			new NavigationRule(
				function () { return PaymentGateway.SelectedCatalogItem != null ? true : false;},
				"recipients" ),
			"initialize"
		]
	},
	
	"billAcceptor-failure" : {
		"deviceOK" : "initialize"
	},
	"billDispenser-failure" : {
		"deviceOK" : "initialize"
	},
	"printer-failure" : {
		"deviceOK" : "initialize"
	},
	
	"information" : {
		"cancel" : "recipients"
	},	
	"select-payment-type" : {
		"recipients" : "recipients",
		"menu" : "initialize",
		"ejectCard" : "eject-card",
		"paymentTypeSelected" :
		[
			// ================ barcode-entry ================
			new NavigationRule(
					function() { 
					return !BarcodeReader.Offline && PaymentGateway.SelectedRecipient.Barcodes != null },
					"barcode-entry"),

			new NavigationRule(
					function() { 
					return (PaymentGateway.SelectedRecipient.InCategory("PeerToPeer") ||
					PaymentGateway.SelectedRecipient.InCategory("MiniStatementQuery") ||
					PaymentGateway.SelectedRecipient.InCategory("BalanceQuery"))
					},
					"insert-card"),					
					
			// ================ data-entry ================
			new NavigationRule(
					function() { return applicationDocument.hasInputFieldsOnSelected(); },
					"data-entry"),

			// ================ data-check ================
			new NavigationRule(function() { 
				return PaymentGateway.SelectedRecipient.CheckDefined && 
				PaymentGateway.SelectedRecipient.CheckRequired &&
				PaymentGateway.SelectedRecipient.InCategory("VirtuPOSCheck");  
				},
				"virtupos-check"),			
				
			new NavigationRule(function() { 
				return PaymentGateway.SelectedRecipient.CheckDefined;  
				},
				"data-check"),

			new NavigationRule(function() { 
				return PaymentGateway.SelectedRecipient.CheckDefined;  
				},
				"data-check-8538"),
				
			"after-data-check"
			
		]	
	},
	"recipients" : {
		"ejectCard" : "initialize",
		"information" : "information",
		"linkSelected" : 
		[
			// ================ external-link ================
			// Это ссылка и она не локальная, т.е. считаем, что уходим на веб
			new NavigationRule(
				function () { return PaymentGateway.SelectedCatalogItem.IsLink && (PaymentGateway.SelectedCatalogItem.HREF.indexOf("http")>=0) }, 
				"external-link"),

			// ================ прямая навигация внутри сценария ================
			// Это ссылка и она локальная, отображаем в основном контексте
			// TODO!
			new NavigationRule(function() { return PaymentGateway.SelectedCatalogItem.IsLink },
					function() { return PaymentGateway.SelectedCatalogItem.HREF }),
		],
		"parentSelected" : "recipients",
		"groupSelected" : 
		[
		
			new NavigationRule(
				function() { return PaymentGateway.SelectedCatalogItem.InCategory("FillingCollection"); },
				"recipients-fixedsum-collection"),		
		
			new NavigationRule(
				function() { return PaymentGateway.SelectedCatalogItem.SelectDefined && applicationDocument.hasInputFieldsOnSelected(); },
				"data-entry"),

			new NavigationRule(
				function() { return PaymentGateway.SelectedCatalogItem.SelectDefined; },
				"select"),
			"recipients"
		],
		"recipientSelected" :
		[
			// ================ select-payment-type ================
			new NavigationRule(
					function() { 
					return PaymentGateway.SelectedRecipient.PaymentTypes.Count > 0; },
					"select-payment-type"),	
			// ================ barcode-entry ================
			new NavigationRule(
					function() { 
					return !BarcodeReader.Offline && PaymentGateway.SelectedRecipient.Barcodes != null },
					"barcode-entry"),

			new NavigationRule(
					function() { 
					return (PaymentGateway.SelectedRecipient.InCategory("PeerToPeer") ||
					PaymentGateway.SelectedRecipient.InCategory("MiniStatementQuery") ||
					PaymentGateway.SelectedRecipient.InCategory("BalanceQuery"))
					},
					"insert-card"),					
					
			// ================ data-entry ================
			new NavigationRule(
					function() { return applicationDocument.hasInputFieldsOnSelected(); },
					"data-entry"),

			// ================ data-check ================
			new NavigationRule(function() { 
				return PaymentGateway.SelectedRecipient.CheckDefined && 
				PaymentGateway.SelectedRecipient.CheckRequired &&
				PaymentGateway.SelectedRecipient.InCategory("VirtuPOSCheck");  
				},
				"virtupos-check"),			
				
			new NavigationRule(function() { 
				return PaymentGateway.SelectedRecipient.CheckDefined;  
				},
				"data-check"),

			new NavigationRule(function() { 
				return PaymentGateway.SelectedRecipient.CheckDefined;  
				},
				"data-check-8538"),
				
			"after-data-check"
			
		],
		"cardReading" : "insert-card",
		"cardCaptured" : "card-captured",
		"pinEntry" : "pin-entry"

	},	
	"recipients-fixedsum-collection" : {
		"cancel" : "initialize",
		"continue" : "accept-payment"
	},

	"data-entry" : {
		"recipientCatalog" : "recipients",
		"recipientComplete" : [
			new NavigationRule( 
				function () { return PaymentGateway.SelectedRecipient.InCategory("IssueOfCash"); },
				"insert-card"),
			new NavigationRule( 
				function () { return PaymentGateway.SelectedRecipient.InCategory("VirtuPOSCheck"); },
				"virtupos-check"),
			new NavigationRule( 
				function () { return PaymentGateway.SelectedRecipient.InCategory("PeerToPeer"); },
				"after-data-check"),
			new NavigationRule( 
				function () { return PaymentGateway.SelectedRecipient.CheckDefined; },
				"data-check"),		
            new NavigationRule( 
				function () { return PaymentGateway.SelectedRecipient.CheckDefined; },
				"data-check-8538"),					
			"after-data-check"
		],
		"catalogItemComplete" : "select",
		"encashment" : "service-entry"
	},

	"data-check" : {
		"success" : "after-data-check",
		"fail" : [
			new NavigationRule (
				function () { return applicationDocument.hasInputFieldsOnSelected(); }, 
				"data-entry"),
			"recipients"
		],
		"cancel" : [
			new NavigationRule (
				function () { return applicationDocument.hasInputFieldsOnSelected(); }, 
				"data-entry"),
			"recipients"
		]
	},
	"data-check-8538" : {
		"success" : "after-data-check",
		"fail" : [
			new NavigationRule (
				function () { return applicationDocument.hasInputFieldsOnSelected(); }, 
				"data-entry"),
			"recipients"
		],
		"cancel" : [
			new NavigationRule (
				function () { return applicationDocument.hasInputFieldsOnSelected(); }, 
				"data-entry"),
			"recipients"
		]
	},
	
	"virtupos-check" : {
		"success" : "after-data-check",
		"fail" : [
			new NavigationRule (
				function () { return applicationDocument.hasInputFieldsOnSelected(); }, 
				"data-entry"),
			"recipients"
		],
		"cancel" : [
			new NavigationRule (
				function () { return applicationDocument.hasInputFieldsOnSelected(); }, 
				"data-entry"),
			"recipients"
		]
	},
	
	"after-data-check" : {
		"commit" : [
			//   Несколько способов оплаты переход на выбор
			new NavigationRule( 
				function () { return applicationDocument.getRecipientPaymentMethodCount() > 1;}, 
				"select-payment-method"
				),
				
			// Один спосб оплаты
			
			// НЕ УБИРАТЬ!
			// 1. Card
			new NavigationRule( 
				function () { 
					return applicationDocument.getRecipientPaymentMethodCount() == 1 && applicationDocument.getRecipientPaymentMethods()[0].state == "acceptCardPayment"; 
				}, 
				"sum-entry-card"
				),
				new NavigationRule( 
				function () { 
					return PaymentGateway.SelectedRecipient !== null || PaymentGateway.RecipientCollection.Count > 0;
				}, 
				"accept-payment"
				),	
			
			


			// 2. Wallet
			new NavigationRule( 
				function () { 
					return applicationDocument.getRecipientPaymentMethodCount() == 1 && applicationDocument.getRecipientPaymentMethods()[0].state == "acceptWalletPayment"; 
				}, 
				"sum-entry-wallet"
				),
			// 3. Cash
			new NavigationRule ( 
				function () { 
					return applicationDocument.getRecipientPaymentMethodCount() == 1 && applicationDocument.getRecipientPaymentMethods()[0].state == "acceptCashPayment";
				}, 
				"accept-payment"),
			//   Не требуется приём или списание денежных средств, т.к. по каким-то причинам мы сразу переходим к проведению платежа
			new NavigationRule( 
				function () { return PaymentGateway.SelectedRecipient.InCategory("AcceptPaymentNotRequired"); },
				"payment"),
				
			// Перезапуск, если ни одно из верхних условий не выполнилось
			"initialize"
			
		],
		
		"commitNeedInput" : "data-entry",
		
		"cancel" : [
			new NavigationRule (
				function () { return applicationDocument.hasInputFieldsOnSelected(); }, 
				"data-entry"),
					
			new NavigationRule (
				function () { return !applicationDocument.hasInputFieldsOnSelected(); }, 
				"recipients"
			)
		],
		
		"fail" : [
			new NavigationRule (
				function () { return applicationDocument.hasInputFieldsOnSelected(); }, 
				"data-entry"),
					
			new NavigationRule (
				function () { return !applicationDocument.hasInputFieldsOnSelected(); }, 
				"recipients"
			)
		]
	},

	"select" : {
		"success" : [
			// new NavigationRule (
				// function () { return hasInputFieldsOnSelected(); }, 
				// "data-entry" ),
			"recipients"
		],
		"cancel" : [
			new NavigationRule (
				function () { return applicationDocument.hasInputFieldsOnSelected(); }, 
				"data-entry" ),
					
			new NavigationRule (
				function () { return !applicationDocument.hasInputFieldsOnSelected(); },
				"recipients"),
		],
		"fail" : [
			new NavigationRule (
				function () { return applicationDocument.hasInputFieldsOnSelected(); }, 
				"data-entry" ),
					
			new NavigationRule (
				function () { return !applicationDocument.hasInputFieldsOnSelected(); },
				"recipients"),
		]
	},

	"select-payment-method" : {
		"cancel" : [
			new NavigationRule (
				function() { return applicationDocument.hasInputFieldsOnSelected(); }, 
				"data-entry"),
			"recipients"
		],
		"acceptCardPayment" : [
			new NavigationRule ( null
			, "insert-card")
		],
		"acceptCashPayment" : "accept-payment" ,
		"acceptWalletPayment" : "accept-payment-wallet"
	},
	
	"accept-payment" : {
		// Потеряли SelectedRecipient
		"cancel" : 
		[	
			new NavigationRule(
				function () { return VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsCardPresent; },
				"eject-card"
			),
			// Переход с принятия оплаты назад.
			//   Один или ни одного сопособа оплаты-> возврат в Data-entry
			new NavigationRule( 
				function () { return applicationDocument.hasInputFieldsOnSelected() && applicationDocument.getRecipientPaymentMethodCount() <= 1 }, 
				"data-entry"),
			//   Больше одного сопособа оплаты -> возврат в select-payment-method
			// new NavigationRule( 
				// function () { return applicationDocument.getRecipientPaymentMethodCount() > 1 }, 
				// "select-payment-method"),
			// Назад но нет Input fields. значит на recipients
			// Переход с принятия оплаты назад.
			new NavigationRule( 
				function () { return Dispenser && Dispenser.DeviceReady && PaymentDevice.ChangeSum > 0;	},
				"dispense"
			),		
			"recipients"
		],
		"commit" : [
			new NavigationRule( 
				function () { return PaymentGateway.SelectedRecipient.InCategory("VirtuPOSCheck"); }, 
				"card-payment"),		
			"payment"
		]
	},
	
	"accept-payment-wallet" : {
		"cancel" : 
		[
			// Переход с принятия оплаты назад.
			//   Один или ни одного сопособа оплаты-> возврат в Data-entry
			new NavigationRule( 
				function () { return applicationDocument.hasInputFieldsOnSelected() && applicationDocument.getRecipientPaymentMethodCount() <= 1 }, 
				"data-entry"),
			//   Больше одного сопособа оплаты -> возврат в select-payment-method
			new NavigationRule( 
				function () { return applicationDocument.getRecipientPaymentMethodCount() > 1 }, 
				"select-payment-method"),
			// Назад но нет Input fields. значит на recipients
			"recipients"
		],
		"commit" : "payment"
	},
	"sum-entry-card-IssueOfCash" : {
		"commit" : "card-cash",
		//"commit" : "dispense-IssueOfCash",
		"cancel" : "eject-card"
	},
	"sum-entry-card" : {
		// исполнение платежа
		"commit" : 
		[
			// Если пополнение карты
			new NavigationRule(
				function () { return PaymentGateway.SelectedRecipient.InCategory("PeerToPeer"); },
				"peer-to-peer"),
			// new NavigationRule(
				// function () { return PaymentGateway.SelectedRecipient.ID == "CardDeposit"; },
				// "payment"),				
			"card-payment"
		],	
		"cancel" : "eject-card"
	},
	"sum-entry-manual" : {
		"cancel" : "select-payment-method",
		// исполнение платежа
		"commit" : "payment"
	},
	"sum-entry-wallet" : {
		"cancel" : "select-payment-method",
		// исполнение платежа
		"commit" : "payment"
	},	
	
	"card-payment" : {
		"success" : "payment",
		"fail" : "eject-card",
		"fail-card-captured" : "card-captured"
	},
	"payment" : 
	{
		"success" : [
			new NavigationRule (
				function () { 
					return 
						!VirtuPOS.Offline && VirtuPOS.IsCardPresent && 
						getParameter("paykiosk.dispenseChangeSum", false) && PaymentDevice.HasAccumulatedSum; 
				},
				"eject-card-before-dispense"
			),
			new NavigationRule(
				function () { return !VirtuPOS.Offline && VirtuPOS.IsCardPresent; },
				"eject-card-before-print"
			), 
			new NavigationRule (
				function () { 
					return getParameter("paykiosk.dispenseChangeSum", false) && PaymentDevice.HasAccumulatedSum; 
				},
				"dispense"
			),
			 new NavigationRule (
				 function () { return getParameter("cardPayment.showBalance", false) && getParameter("verifyCard.cardVerifyBalance", false) && PaymentGateway.SelectedRecipient.InCategory("CardDeposit"); },
				 "balance-after-deposit"
			 ),
			"print-coupon"
		],
		"fail-card-captured" : "card-captured"
	},
	"payment-IssueOfCash" : 
	{
		"success" : [
			new NavigationRule (
				function () { 
					return !VirtuPOS.Offline && VirtuPOS.IsCardPresent && PaymentDevice.HasAccumulatedSum; 
				},
				"eject-card-before-dispense"
			),
			new NavigationRule(
				function () { return !VirtuPOS.Offline && VirtuPOS.IsCardPresent; },
				"eject-card-before-print"
			), 
			"print-coupon"
		],
		"fail-card-captured" : "card-captured"
	},
	"peer-to-peer" : {
		"continue" : "print-coupon"
	},	
	
	"ministatement" : {
		"finish" : null
	},	
	"show-balance" : {
		"finish" : null
	},
	
	"balance-after-deposit" : {
		"success" : "print-coupon",
		"fail" : "print-coupon",
		"fail-card-captured" : "card-captured"
	},
	
	"print-coupon" : { 
		"success" : 
		[
		new NavigationRule( 
				function () { return Framework.Variables("paymentResults").length > 0 }, 
				"print-coupon"),
			"initialize"
		],
		"fail" : "initialize"
	},

	"dispense" : {
		"success" : [
			// new NavigationRule(
				// function () { return Framework.Variables("dispeseNonePayment")},
				// "print-coupon-error"
				// ),
				"print-coupon"
		],
		"retract" : [
			new NavigationRule(
				function () { return getParameter("dispenser.hasPresenter", false); },
				"retract"
			),
			"print-coupon"
		],
		"fail" : "dispense-error"
	},
	
	"dispense-error" : {
		"commit" : "print-coupon",
		"cancel" : "print-coupon"
	},	
	"retract" : {
		"success" : [
			new NavigationRule(
				function () { return !VirtuPOS.Offline && VirtuPOS.IsCardPresent; },
				"capture-card"
			), 
			"initialize" 
		],
		"fail" : [
			new NavigationRule(
				function () { return !VirtuPOS.Offline && VirtuPOS.IsCardPresent; },
				"capture-card"
			), 
			"initialize" 
		]
	},
	"barcode-entry" : {
		"commit" : "data-check",
		"correct" : "data-entry",
		"cancel" : "recipients"
	},
	// "insert-card-CardPayment" : {
		// "cancel" : "recipients",
		// "ejectCard" : "eject-card"
	// },
	// "insert-card-CardDeposit" : {
		// "commit" : [
			// new NavigationRule(
				// function () { return !applicationDocument.isCardTypeOf("ours"); },
				// "eject-card"
			// ),
			// "pin-entry"
		// ],
		// "cancel" : [
			// new NavigationRule(
				// function () { return VirtuPOS.IsCardPresent; },
				// "eject-card"
			// ),
			// Переход с принятия оплаты назад.
			  // Один или ни одного способа оплаты-> возврат в Data-entry
			// new NavigationRule( 
				// function () { return applicationDocument.hasInputFieldsOnSelected() && applicationDocument.getRecipientPaymentMethodCount() <= 1 }, 
				// "data-entry"),
			  // Больше одного сопособа оплаты -> возврат в select-payment-method
			// new NavigationRule( 
				// function () { return applicationDocument.getRecipientPaymentMethodCount() > 1 }, 
				// "select-payment-method"),
			// Назад но нет Input fields. значит на recipients
			// "recipients"
		// ],
		// "cardCaptured" : "card-captured",
		// "encashment" : "encashment-switch"
	// },
	"insert-card" : {
		"commit" : "pin-entry",
		"cancel" : [
			new NavigationRule(
				function () { return VirtuPOS && !VirtuPOS.Offline && VirtuPOS.IsCardPresent; },
				"eject-card"
			),
			// Переход с принятия оплаты назад.
			//   Один или ни одного способа оплаты-> возврат в Data-entry
			new NavigationRule( 
				function () { return applicationDocument.hasInputFieldsOnSelected() && 
									applicationDocument.getRecipientPaymentMethodCount() <= 1 &&
									!(PaymentGateway.SelectedRecipient.InCategory("PeerToPeer") ||
									PaymentGateway.SelectedRecipient.InCategory("MiniStatementQuery") ||
									PaymentGateway.SelectedRecipient.InCategory("BalanceQuery"))}, 
				"data-entry"),
			//   Больше одного сопособа оплаты -> возврат в select-payment-method
			// new NavigationRule( 
				// function () { return applicationDocument.getRecipientPaymentMethodCount() > 1 }, 
				// "select-payment-method"),
			// Назад но нет Input fields. значит на recipients
			"recipients"
		],
		"cardCaptured" : "card-captured",
		"capture" : "capture-card",
		"encashment" : "service-entry"
	},
	"pin-entry" : {
		"commit" : "verify-card",
		"cancel" : "eject-card"
	},
	"eject-card" : {
		"capture" : "capture-card",
		"cardCaptured" : "card-captured",
		"commit" : [
			new NavigationRule (
				function () { return Framework.Variables("paymentResults").length > 0 },
				"print-coupon"
			),
			"initialize"
			]
	},
	"eject-card-before-print" : {
		"capture" : "capture-card",
		"cardCaptured" : "card-captured",
		"commit" : "print-coupon"
	},
	"eject-card-before-dispense" : {
		"capture" : "capture-card",
		"cardCaptured" : "card-captured",
		"commit" : "dispense"
	},
	"capture-card" : {
		"success" : "card-captured",
		"fail" : "card-captured"
	},

	"card-captured" : {
		"commit" : "initialize"
	},
	"card-cash" : {
		"success" : "payment",
		"fail" : "eject-card"
	},
	"verify-card" : {
		"success" : [
			new NavigationRule(
				function () {
					return PaymentGateway.SelectedRecipient == null;
				},
				"recipients"
			),
			new NavigationRule(
				function () {
					return PaymentGateway.SelectedRecipient.InCategory("PeerToPeer");
				},
				"data-entry"
			),
			new NavigationRule(
				function () {
					return PaymentGateway.SelectedRecipient.InCategory("MiniStatementQuery");
				},
				"ministatement"
			),
			new NavigationRule(
				function () {
					return PaymentGateway.SelectedRecipient.InCategory("BalanceQuery");
				},
				"show-balance"
			),			
			new NavigationRule(
				function () {
					return PaymentGateway.SelectedRecipient.InCategory("CardDeposit");
				},
			 "accept-payment"
			 ),
			"sum-entry-card"
		],
		"cancel" : "eject-card",
		"fail" : "eject-card",
		"fail-card-captured" : "card-captured"
	},

	"service-entry" : {
		"cancel" : "initialize",
		"service" : "service-switch",
		"encashment" : "encashment-switch"
		//"encashment" : "encashment-cardcaptured"
	},
	
	"encashment-switch" : {
		"complete" : null
	},
		
	"service-switch" : {
		"complete" : null
	},
	
	"external-link" : {
		"complete" : null
	}
});
