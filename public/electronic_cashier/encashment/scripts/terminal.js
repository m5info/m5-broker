﻿var couponProperties = {
	"header" : 	function() {
					var rh = getParameter("paykiosk.receiptHeader", null);
					if (rh) {
						return rh;
					} else {
						try {
							return Framework.Properties.GetEscapedString("PayBox", "System.ProcessorText");
						} catch(e) {
							return "";
						} 
					}
				},
	"terminalNumber" : 	function () {
							if (EWM.Machine.SerialNr) {
								return EWM.Machine.SerialNr;
							}
							else {
								return getMessage("paykiosk.noTerminalNumber");
							}
						},
	"terminalAddress" : function () {
							if (EWM.Machine.Description) {
								return EWM.Machine.Description;
							}
							else {
								return getMessage("paykiosk.noTerminalNumber");
							}
						}
}