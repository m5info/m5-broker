﻿addLocale(
		"en",
		{
			"paykiosk": {
				"noTerminalNumber": "not specified"
			},
			"start": {
				"startEncashment": "Encashment № ",
				"operator": "Operator "
			},
			"end": {
				"encashment": "Encashment № ",
				"end": " complete"
			},
			"encashmentBillAcceptor" : {
				"encashmentBillAcceptor": "Changing the cassette of billacceptor",
				"device": "Device",
				"askEncashment": "Replace the cassete?",
				"doEncashment": "collect",
				"bill": "Bill",
				"countShort": "Count",
				"summ": "Total",
				"total": "Total",
				"totalAll": "Total Results",
				"totalCount": "bills",
				"noBillsAccepted": "During the period of encashment is not taken notes",
				"enter_unitId": "Enter the number of cassete installed",
				"skipDevice": "Skip",
				"noReceipt": "Print a receipt can not print <br> device available",
				"wait": "Please wait ....",
				"print": "Printed report on the replacement of the cassette acceptor",
				"registerError": "Unable to close journal Accepted bills \nYou Journal nekorretnom damaged during shutdown of the terminal. Delete \n from the magazine unreadable characters \nError:",
				"couponCapture": "-= Summary for receiving bills =-",
				"encashmentNr": "Encashment №: ",
				"operator": "Operator: ",
				"registerOpen": "Report opened: ",
				"registerClose": "Report closed: ",
				"device": "Device: ",
				"ID": "ID device: ",
				"terminalNr": "Terminal Number: ",
				"terminalAddress": "Terminal Location: ",
				"replacedCasseteNr": "Number of previous cassete: ",
				"installedCasseteNr": "Number of installed cassete: ",
				"noCasseteNr": "not specified",
				"couponDate": "Date of print: ",
				"currency": " Curr.: ",
				"billTableHeader": "Nominal   |   Count |    Amount", // ​​<**********|**********|**********>
				"total": "Total",
				"totalAll": "Total bills in the cassete: ",
				"sign": "Signature: ",
				"registerError": "Error: There is not a private journal of bills.",
				"printerFailed": "The device is not available for printing"
			},
			"encashmentCoinAcceptor" : {
				"encashmentCoinAcceptor" : "Замена кассеты монетоприемника",
				"askEncashment" : "Заменить стекер?",
				"doEncashment" : "Инкассировать",
				"coin" : "Монета",
				"countShort" : "Кол-во",
				"summ" : "Сумма",
				"total" : "Итого",
				"totalAll" : "Итого Всего",
				"totalCount" : "монет",
				"noCoinsAccepted" : "Журналов принятых монет нет",
				"skipDevice" : "Пропустить",
				"noReceipt" : "Печать квитанции невозможна<br>Устройство печати недоступно",
				"waitPrint" : "Печатается отчет о замене кассеты<br>Пожалуйста, подождите...."
			},
			"encashmentBillDispenser" : {
				"encashmentBillDispenser" : "Замена кассет диспенсера",
				"askEncashment" : "${(_.printerReady ? \"\" : \"<br><span style='color:red'>Печать квитанции невозможна<br>Устройство печати недоступно</span><br>	\")}Вы хотите заменить одну или несколько кассет в диспенсере?",
				"askExchangeCassette" : "Кассета №${_.CurrentCassette+1}<br><table width=\"200px\"><tr><td>Номинал</td><td>${_.selectedCassete.note}</td></tr>"+
					"<tr><td>Валюта</td><td>${_.selectedCassete.currency}</td></tr>"+
					"<tr><td>Купюр для выдачи</td><td>${_.selectedCassete.countLeft}</td></tr>"+
					"</table>"+
					"Вы хотите заменить кассету?",
				"doEncashment" : "Заменить",
				"skipDevice" : "Пропустить",
				"noReceipt" : "Печать квитанции невозможна<br>Устройство печати недоступно",
				"exchangeCasssetteNr" : "Замена кассеты №${_.CurrentCassette+1}",
				"enter_unitId" : "Введите номер устанавливаемой кассеты",
				"enter_note" : "Выберите номинал купюр в кассете",
				"enter_currency" : "Выберите валюту купюр в кассете",
				"enter_countLoad" : "Введите количесвто купюр в кассете",
				"enter_countMin" :  "Введите минимальный остаток купюр в кассете",
				"waitPrint" : "Печатается отчет о замене кассет<br>Пожалуйста, подождите...."
			},
			"encashmentHopper" : {
				"encashmentHopper" : "Замена кассеты хоппера",
				"askEncashment" : "${(_.printerReady ? \"\" : \"<br><span style='color:red'>Печать квитанции невозможна<br>Устройство печати недоступно</span><br>	\")}Вы хотите заменить кассету в хоппере?",
				"askExchangeCassette" : "Кассета №${_.CurrentCassette+1}<br><table width=\"200px\"><tr><td>Номинал</td><td>${_.selectedCassete.note}</td></tr>"+
					"<tr><td>Валюта</td><td>${_.selectedCassete.currency}</td></tr>"+
					"<tr><td>Монет для выдачи</td><td>${_.selectedCassete.countLeft}</td></tr>"+
					"</table>"+
					"Вы хотите заменить кассету?",
				"doEncashment" : "Заменить",
				"skipDevice" : "Пропустить",
				"noReceipt" : "Печать квитанции невозможна<br>Устройство печати недоступно",
				"exchangeCasssetteNr" : "Замена кассеты №${_.CurrentCassette+1}",
				"enter_unitId" : "Введите номер устанавливаемой кассеты",
				"enter_note" : "Выберите номинал монет в кассете",
				"enter_currency" : "Выберите валюту монет в кассете",
				"enter_countLoad" : "Введите количесвто монет в кассете",
				"enter_countMin" :  "Введите минимальный остаток монет в кассете",
				"waitPrint" : "Печатается отчет о замене кассет<br>Пожалуйста, подождите...."
			},
			"encashmentPayments" : {
				"encashmentPayments": "Report of payments",
				"askEncashment": "Close the Report payments?",
				"recipient": "Recipient",
				"countShort": "Number",
				"summ": "Total",
				"total": "Total",
				"totalAll": "Total Results",
				"payments": "payments",
				"noPaymentsAccepted": "No payment received",
				"noReceipt": "Print a receipt can not print <br> device available",
				"waitPrint": "Printing Report payments <br> Please wait ....",
				"couponCapture": " = Summary Report on reception of payments =-",
				"encashmentNr": "Encashment №: ",
				"operator": "Operator: ",
				"registerOpen": "Report opened: ",
				"registerClose": "Report closed: ",
				"terminalNr": "Terminal Number: ",
				"terminalAddress": "Terminal Location: ",
				"couponDate": "Date of print: ",
				"paymentTableHeader": "Recipient        |Count|       Amount", // ​​<*****************|*****|*************>
				"couponTotal": "Total",
				"couponTotalAll": "Total Results",
				"sign": "Signature: ",
				"registerError": "Error: There is not a private journal on payments.",
				"printerFailed": "The device is not available for printing"
			},
			"encashmentCardCaptured" : {
				"cardCaptured" : "Печать журнала захваченных карт",
				"noCardCaptured" : "Нет захваченных карт",
				"noReceipt" : "Печать квитанции невозможна<br>Устройство печати недоступно",
				"askEncashment" : "Вы можете напечатать журнал без закрытия или с закрытием журнала захваченных карт",
				"number" : "№№",
				"dateTime" : "Дата/время",
				"cardNr" : "Номер карты",
				"responseCode" : "Причина захвата"
			},
			"encashmentZReport" : {
				"askPrintReport": "Do you want to print a Z report?",
				"waitPrint": "Printed Report <br> Z Please wait ....",
				"reportPrintingComplete": "Print a report is completed. Collect receipts",
				"printerFailed": "Fiscal registrar is not working properly <br> Print Z Report impossible"	
			},
			"encashmentReject" : {
				"encashmentReject" : "Инкассация reject кассеты",
				"askEncashment" : "Инкассировать reject кассету?",
				"askClear" : "Освободить reject кассету?",
				"clearHint" : "Извлеките все купюры из reject кассеты и нажмите OK",
				"paymentDeviceHint" : "Извлеките все купюры из reject кассеты и вставьте их в купюроприемник.<br>По окончании нажмите OK",
				"bill" : "Номинал",
				"currency" : "Валюта",
				"countShort" : "Кол-во",
				"summ" : "Сумма",
				"rejectedNotesList" : "Перечень купюр"
			},
			"deviceStatus" : {
				"Printer" : {
					"dssUndefined" : "Не определен",
					"dssDisabled_Initialize" : "Отключен",
					"dssIdle_Normal" : "Работает",
					"dssIdle_Suspended" : "Приостановлено",
					"dssIdle_PaperNearEnd" : "Сработал датчик близкого конца бумаги",
					"dssBusy_Printing" : "Идет печать",
					"dssBusy_Holding" : "Удержание чека",
					"dssError_NoDevice" : "Устройство не найдено",
					"dssError_DeviceFailure" : "Сбой в работе устройства",
					"dssError_PaperOut"	: "Нет бумаги или неверно настроены направляющие"	
				},
				"billAcceptor" : {
					"dssUndefined" : "Не определен",
					"dssDisabled_PowerUp" : "Отключён, загружается",
					"dssDisabled_Initialize" : "Работает",
					"dssDisabled_Inhibit" : "Запрещен",
					"dssBusy_Accepting" : "Идет прием",
					"dssBusy_RejectingInhibit" : "Запрет возврата купюры",
					"dssBusy_Rejecting" : "Возврат купюры",
					"dssBusy_Escrow" : "Прием купюры",
					"dssBusy_Stacking" : "Укладка купюры",
					"dssBusy_Stacked" : "Уложил купюру",
					"dssBusy_Returning" : "Возвращаю купюру",
					"dssBusy_Returned" : "Возвратил купюру",
					"dssBusy_Paused" : "Пауза",
					"dssBusy_Holding" : "Удержание купюры",
					"dssBusy_dssDispensing" : "Занят, выдача купюр",
					"dssBusy_dssDispensed" : "Занят, купюра выдана",
					"dssBusy_dssDispensedComplete" : "Все купюры выданы",
					"dssBusy_dssUnloading" : "Возврат купюры",
					"dssBusy_dssUnloaded" : "Купюра возвращена",
					"dssBusy_Packed" : "Упакована",
					"dssError_NoDevice" : "Устройство не найдено",
					"dssError_DeviceFailure" : "Сбой в работе устройства",
					"dssError_StackerFull" : "Кассета переполнена!",
					"dssError_ValidatorJammed" : "Замята купюра в приемнике!",
					"dssError_StackerJammed" : "Замята купюра в кассете!",
					"dssError_StackerRemoved" : "Зачем то сняли стекер!",
					"dssIdle_Normal" : "Работает",
					"dssIdle_Cassette" : "Работает",
					"dssIdle_CassetteNormal" : "Работает, кассета в порядке",
					"dssIdle_CassetteNearEnd" : "Кассета почти опустела",
					"dssIdle_CassetteNotPresent" : "Работает, кассета отсутствует",
					"dssIdle_CassetteInvalid" : "Кассета неисправна",
					"dssError_CassetteNotPresent" : "Ошибка, кассета отсутствует",
					"dssIdle_Sensor" : "Работает сенсор",
					"dssIdle_Suspended" : "Приостановлено",
					"dssError_ShutterFailure" : "Шторка сломалась",
					"dssError_PoolFailure" : "Ошибка пула",
					"dssError_PotentiometerFailure" : "Ошибка потенциометра"				
				},
				"VirtuPOS" : {
					"available" : "Доступен",
					"unavailable" : "Недоступен"					
				}
			}		
		});