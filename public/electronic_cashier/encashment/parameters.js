﻿appendParameters({
	"paykiosk" : {
		"stackerSize" : 1200, // Размер стэкера по умолчанию
		"receiptWidth" : 48, // Ширина чека инкассации в символах
		"encashmentBillacceptor" : true, //Возможность инкассировать купюроприемник
		"encashmentBilldispenser" : true, //Возможность инкассировать диспенсер
		"encashmentCoinacceptor" : true, //Возможность инкассировать монетник
		"encashmentHopper" : true, //Возможность инкассировать хоппер
		"encashmentPayments" : false, //Возможность закрытие журнала платежей
		"encashmentCardCaptured" : false, //Возможность закрытия журнала захваченных карт
		"encashmentZReportPrint" : false // Возможность печати Z-отчёта
	}
})