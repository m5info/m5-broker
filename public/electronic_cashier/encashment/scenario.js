﻿appendNavigationRules({

	"encashment-start" : {
		"encashment" : "encashment-paymentdevice",
		"error" : null
	},
	"encashment-coinacceptor" : {
		"commit" : "encashment-coinacceptor-print",
		"skip" : "encashment-paymentdevice"
	},
	"encashment-coinacceptor-print" : {
		"commit" : "encashment-paymentdevice",
		"error" : "encashment-paymentdevice"
	},
	"encashment-billacceptor" : {
		"commit" : "encashment-billacceptor-print",
		"skip" : "encashment-paymentdevice"
	},
	"encashment-billacceptor-print" : {
		"commit" : "encashment-paymentdevice",
		"error" : "encashment-paymentdevice"
	},
	"encashment-billdispenser" : {
		"commit" : "encashment-billdispenser-print",
		"skip" : "encashment-paymentdevice"
	},
	"encashment-billdispenser-print" : {
		"commit" : "encashment-paymentdevice",
		"error" : "encashment-paymentdevice"
	},
	"encashment-hopper" : {
		"commit" : "encashment-hopper-print",
		"skip" : "encashment-paymentdevice"
	},
	"encashment-hopper-print" : {
		"commit" : "encashment-paymentdevice",
		"error" : "encashment-paymentdevice"
	},
	"encashment-payments" : {
		"commit" : "encashment-payments-print",
		"skip" : "encashment-cardcaptured",
		"error" : "encashment-cardcaptured"
	},
	"encashment-payments-print" : {
		"commit" : "encashment-cardcaptured",
		"error" : "encashment-cardcaptured"
	},
	"encashment-cardcaptured" : {
		"success" : "encashment-cardcaptured-print",
		"skip" : "encashment-zreport-print",
		"error" : "encashment-zreport-print" 
	},
	"encashment-cardcaptured-print" : {
		"commit" : "encashment-zreport-print",
		"error" : "encashment-zreport-print"
	},
	"encashment-zreport-print" : {
		"commit" : "encashment-end",
		"error" : "encashment-end",
		"skip" : "encashment-end"
	},
	
	"encashment-end" : {
		"commit" : null
	},
	
	"encashment-paymentdevice" : {
		"commit" :
		[
			// ================ encashment-payments ================
			// Если нет инкассируемых устройств или все устройства проинкассированы переходим на encashment-payments и далее по сценарию
			new NavigationRule(
				function () { return Framework.Variables("encashment").count <= Framework.Variables("exchangePosition") }, 
					"encashment-payments"),

			// ================ encashment-billacceptor ================
			// Купюроприемник
			new NavigationRule(
				function() { return Framework.Variables("encashment")[Framework.Variables("exchangePosition")] == 3; },
					"encashment-billacceptor"),
						
			// ================ encashment-billdispenser ================
			// Деспенсер
			new NavigationRule(
				function() { return Framework.Variables("encashment")[Framework.Variables("exchangePosition")] == 1; },
					"encashment-billdispenser"),	
					
			// ================ encashment-hopper ================
			// Хоппер
			new NavigationRule(
				function() { return Framework.Variables("encashment")[Framework.Variables("exchangePosition")] == 2; },
					"encashment-hopper"),		
					
			// ================ encashment-coinacceptor ================
			// Монетоприемник
			new NavigationRule(
				function() { return Framework.Variables("encashment")[Framework.Variables("exchangePosition")] == 4; },
					"encashment-coinacceptor"),
					
			// ================ encashment-coinchanger ================
			// Монетообменник
			new NavigationRule(
				function() { return Framework.Variables("encashment")[Framework.Variables("exchangePosition")] == 6 },
					"encashment-hopper"),

			// ================ encashment-billrecycler ================
			// Ресайклер
			new NavigationRule(
				function() { return Framework.Variables("encashment")[Framework.Variables("exchangePosition")] == 5; },
					"encashment-billrecycler"),

			"encashment-end"
		]
	}
});