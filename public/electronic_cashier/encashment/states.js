﻿appendStates(
{	
	"encashment-billacceptor" : {
		"requires" : ["keypad.js"],
		"screen" : "encashment-acceptors.html"
	},

	"encashment-coinacceptor" : {
		"requires" : ["keypad.js"],
		"screen" : "encashment-acceptors.html"
	},
	
	"encashment-billdispenser" : {
		"requires" : ["keypad.js"],
		"screen" : "encashment-dispensers.html"
	},
	
	"encashment-hopper" : {
		"requires" : ["keypad.js"],
		"screen" : "encashment-dispensers.html"
	},
	
	"encashment-billacceptor-print" : {
		"requires" : ["terminal.js"]
	},
	
	"encashment-billdispenser-print": {
		"requires" : ["terminal.js"]	
	},
	"encashment-cardcaptured-print": {
		"requires" : ["terminal.js"]	
	},
	
	"encashment-coinacceptor-print": {
		"requires" : ["terminal.js"]	
	},
	
	"encashment-hopper-print": {
		"requires" : ["terminal.js"]	
	},
	
	"encashment-payments-print": {
		"requires" : ["terminal.js"]	
	},
	
	"encashment-reject-print": {
		"requires" : ["terminal.js"]	
	}
});
