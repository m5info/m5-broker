﻿_.addEventListeners({
	"load" : function() {
		if (!getParameter("paykiosk.encashmentZReportPrint", false)){
			_.leave("skip");
		return false;	
		}		
	
		if (!FiscalPrinter.Offline) {
			Framework.AttachEvent(FiscalPrinter, "OnAllReportsPrinted", onReportPrintComplete);
			Framework.AttachEvent(FiscalPrinter, "OnPresenterEmptyEvent", onPresenterEmptyEvent);
		}
		
		_.registerEvents(['printerStatusChanged', 'reportPrinting', 'reportReady']);

		_.registerVariables({
			"ReportPrintedCount" : getParameter("encashment.printZReportsFromBuffer", true) ? 1 : 0
		});
	
		_.registerActions({
			"commit" : function() { _.leave("commit") },
			"error" : function() { _.leave("error"); },
			"skip" : function() { _.leave("skip"); }
		});
		// Printer
		if (!ReceiptPrinter.Offline)
			Framework.AttachEvent(ReceiptPrinter, "OnPrinterEvent", this.updatePrinterStatus);
		if (!FiscalPrinter.Offline)
			Framework.AttachEvent(FiscalPrinter, "OnFiscalPrinterEvent", this.updatePrinterStatus);
		updatePrinterStatus();		
		
	},
	
	"billAcceptorFailure" : function () {
		return false;
	},

	"printerFailure" : function () {
		return false;
	},
	"printerStatus" : function () {
		// (FiscalPrinter.Status.SubStatus);
		return true;
	},
	"inactivityTimer" : function() {
//		_.leave("commit");
		return false;
	},
	
	"run" : function() { 
	}
});

function updatePrinterStatus() {
	_.printerReady = !FiscalPrinter.Offline && FiscalPrinter.DeviceReady;
	_.fireEvent("printerStatusChanged");
}


function onPresenterEmptyEvent() {
	if (_.ReportPrintedCount <= 0) {
		_.leave('commit');
		return;
	}
	printZReport();
}

function onReportPrintComplete() {
	_.fireEvent("reportReady");
	_.ReportPrintedCount = _.ReportPrintedCount-1;
}
function printZReport() {
	_.fireEvent("reportPrinting");

	if (Printer.ZReportInBufferCount > 0 && getParameter("encashment.printZReportsFromBuffer", true)) {
		_.ReportPrintedCount = 1;
		FiscalPrinter.PrintZReportFromBuffer();
	} else {
		_.ReportPrintedCount = 0;
		FiscalPrinter.PrintZReport();
	}
}
