﻿(function() {

_.addEventListeners({
	"load" : function () {
			
		if (!PaymentDevice || !getParameter("paykiosk.encashmentHopper", false)) {
			_.leave('skip');
			return false;
		}	
		
		var encashment = PaymentDevice.StartEncashment(Framework.Variables("workerCode") + " " + Framework.Variables("workerName"));
		var device = encashment.CashDevices(Framework.Variables("exchangePosition"));
		Framework.Variables("exchangePosition")++;
		Framework.Variables("device") = {
			"DeviceId" : device.DeviceId,
			"Name" : device.Name
		}		
		
		_.registerVariables({
			"currentCassette" : 0,
			"selectedCassette" : null,
			"cassettes" : [],
			"cassettesNew" : [],
			"cassettesOld" : [],
			"device" : device,
			"cashUnits" : "",
			"encashment" : encashment,
			"workerCode" : Framework.Variables("workerCode") ? Framework.Variables("workerCode") : "unknown"
		});
		
		_.registerEvents(['askExchangeCassette', 'encashmentReady', 'startEnterData']);

		_.registerActions({
			"commit" : function() { endEncashment(); },
			"nextCassette" : nextCassette,
			"skip" : new StateAction(
				function() { return _.cassettesNew.length == 0; },
				function() { _.leave("skip"); } ),
			"getType" : function(type) { return getType(type); }
		});
		// Printer
		if (!ReceiptPrinter.Offline)
			Framework.AttachEvent(ReceiptPrinter, "OnPrinterEvent", updatePrinterStatus);
		if (!FiscalPrinter.Offline)
			Framework.AttachEvent(FiscalPrinter, "OnFiscalPrinterEvent", updatePrinterStatus);
			
		// Encashment events
		Framework.AttachEvent(PaymentDevice, "OnEncashmentReady", onEncashmentReady);
		
		Framework.AttachEvent(_.encashment, "OnDeviceEncashmentReady", onDeviceEncashmentReady);
		Framework.AttachEvent(_.encashment, "OnDeviceEncashmentComplete", onDeviceEncashmentComplete);
		
		// Cassette events
		Framework.AttachEvent(_.encashment, "OnCassetteExchangeReady", onCassetteExchangeReady);
		Framework.AttachEvent(_.encashment, "OnCassetteExchangeComplete", onCassetteExchangeComplete);			
		
	},
	"run" : function() {
		_.addEventListeners({
			"processCassette" : processCassette,
			"deviceEncashment" : deviceEncashment,
			"exchangeCassette" : exchangeCassette
		});
		
		updatePrinterStatus();
		
		// ***** Убрать ?	
		onEncashmentReady();
	},
	"unload" : function() {
		return false;
	},	
	"billAcceptorFailure" : function () {
		return false;
	},

	"printerFailure" : function () {
		return false;
	},
	"inactivityTimer" : function() {
		return false;
	}
});

function onEncashmentReady() {
	// Перешли в режим инкассации, спрашиваем инкассиовать ли устройство
	_.fireEvent("encashmentReady");
}

function deviceEncashment() {
	// alert("deviceEncashment");
	// Начало инкассации устройства
	_.encashment.StartEncashment(_.device);
	_.cashUnits = _.device.cashUnits;

	for (var i = 0; i < _.cashUnits.Count; i++) {
		var cassette = _.cashUnits.Item(i);

		if (!(cassette.Type && cassette.Type == 5)) continue

		var o = new CassetteInfo();
		o.cassetteIdx = i;
		o.type = cassette.Type;
		o.number = cassette.Number;
		o.unitId = cassette.UnitId;
		o.unitName = cassette.UnitName;
		o.value = cassette.Value;
		o.currencyId = cassette.CurrencyId;
		o.initialCount = cassette.InitialCount;
		o.currentCount = cassette.CurrentCount;
		o.minimum = cassette.Minimum;
		o.dispensedCount = cassette.DispensedCount;
		o.presentedCount = cassette.PresentedCount;
		o.retractedCount = cassette.RetractedCount;
		o.rejectedCount = cassette.RejectedCount;
		o.toDispenseCount = o.currentCount > o.minimum ? (o.currentCount - o.minimum) : 0;
		o.dispensedAmount = o.dispensedCount * o.value;
		o.status = cassette.status;

		_.cassettes.push(o);
	}	

	if (_.cassettes.length == 0)
		_["commit"]();
		
	// **** Убрать
	// onDeviceEncashmentReady();
}


function onDeviceEncashmentReady() {
	_.selectedCassette = clone(_.cassettes[_.currentCassette]);
	_.fireEvent('askExchangeCassette');
	
}

function exchangeCassette() {
	// alert("exchangeCassette")
	_.cashUnit = _.cashUnits.Item(_.currentCassette);
	_.exchange = _.encashment.StartExchange(_.cashUnit);

	// **** Убрать
	//onCassetteExchangeReady();
}

function onCassetteExchangeReady() {
	// alert("onCassetteExchangeReady");
	if (_.cassettes[_.currentCassette].type == 1)
		processCassette();
	else
		_.fireEvent("startEnterData");		
}

function onCassetteExchangeComplete() {
	nextCassette();
}

function onDeviceEncashmentComplete() {
	// alert("onDeviceEncashmentComplete");
	_.leave('commit');
}

function CassetteInfo() {
	this.cassetteIdx = 0;
	this.type = 0;
	this.number = "";
	this.unitId = "";
	this.unitName = "";
	this.value = 0;
	this.currencyId = "RUB";
	this.initialCount = 0;
	this.currentCount = 0;
	this.minimum = 0;
	this.dispensedCount = 0;
	this.presentedCount = 0;
	this.retractedCount = 0;
	this.rejectedCount = 0;
	this.toDispenseCount = 0;
	this.dispensedAmount = 0;
	this.status = 0;
}

// Возвращает копию объекта (новый экземпляр)
function clone(obj) {
	var c = {};
	for (var p in obj) if (obj.hasOwnProperty(p)) {
		c[p] = obj[p];
	}
	return c;
}

function updatePrinterStatus() {
	_.printerReady = Printer && !Printer.Offline && Printer.DeviceReady;
}

function nextCassette(param) {

	_.currentCassette = _.currentCassette + 1;
	if (_.currentCassette >= _.cassettes.length) {
		_['commit']();
		return;
	}
	onDeviceEncashmentReady();	
}

function processCassette() {
	// Do exchange operations here
	var newCassette = clone(_.selectedCassette);
	var oldCassette = clone(_.cassettes[_.currentCassette]);

	if (document.getElementById("addItems").checked) 
		newCassette.initialCount = Number(newCassette.initialCount) + Number(oldCassette.currentCount);
	
	newCassette.dispensedAmount = 0;
	newCassette.toDispenseCount = newCassette.initialCount > newCassette.minimum ? (newCassette.initialCount - newCassette.minimum) : 0;

	if (newCassette.type == 1)
		newCassette.initialCount = 0;
	
	_.cassettesNew.push(newCassette);
	_.cassettesOld.push(oldCassette);

	// alert("set cassette " + newCassette.unitId);

	_.encashment.EndExchange(
		newCassette.unitId,
		newCassette.currencyId,
		newCassette.value,
		newCassette.initialCount,
		newCassette.minimum,
		0);	
}

function endEncashment() {
	Framework.Variables("CassettesRemoved") = _.cassettesOld;
	Framework.Variables("CassettesInstalled") = _.cassettesNew;
	_.encashment.EndEncashment();
}

function getType(type) {
	switch(Number(type)) {
	 case 0 : return "cutNotApplicable";
     case 1 : return "cutReject";
     case 2 : return "cutBill";
     case 3 : return "cutCoinCylinder";
     case 4 : return "cutCoinDispenser";
     case 5 : return "cutRetract";
     case 6 : return "cutCoupon";
     case 7 : return "cutDocument";
     case 8 : return "cutCashIn";
     case 9 : return "cutEscrow";
     case 10 : return "cutReplenishmentContainer";
     case 11 : return "cutRecycling";
     default : return "unknown";
	}
}

})();