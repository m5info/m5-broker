﻿_.addEventListeners({
	"load" : function() {
		Framework.AttachEvent(Printer, "OnPresenterEmptyEvent", function() { _['commit'](); } );

		_.registerActions({
			"commit" : function() { _.leave("commit") },
			"error" : function() { _.leave("error"); }
		});
	},
	
	"billAcceptorFailure" : function () {
		return false;
	},

	"printerFailure" : function () {
		return false;
	},
	"inactivityTimer" : function() {
		return false;
	},
	
	"run" : function() { 
		if (!Printer || !Printer.DeviceReady) {
			printErrorMessage();
			_.leave('error');
		}
		printRejectReport(true);
	}
});

function printErrorMessage() {
	if (!(Printer && Printer.DeviceReady)) {
		showAlert("Устройство для печати недоступно", {}, {}, _['error'], 3000);
	}
}

function formatBillsRegisterStr(note, count, sum) {
	var spacesStr = "          ";

	return note + spacesStr.substring(("" + note).length, 10) + "|"
		+ spacesStr.substring((count + "").length, 10) + count + "|"
		+ spacesStr.substring((sum + "").length, 10) + sum;
}


function printRejectReport(printClosed) {
	var reportNotes = Framework.Variables("NotesRejected");

	try {Printer.Alignment = 2;} catch(e){}
	Printer.Writeln(couponProperties.header());
	try {Printer.Alignment = 0;} catch(e){}
	Printer.Writeln("----");
	Printer.Writeln("Отчет по невыданным купюрам");
	Printer.Writeln("----");
	Printer.Writeln("Оператор: " + Framework.Variables("workerCode"));
	Printer.Writeln("Номер терминала: " + couponProperties.terminalNumber());
	Printer.Writeln("Адрес терминала: " + couponProperties.terminalAddress());
	Printer.Writeln("Дата печати: " + formatDateTime(new Date()));

	try {Printer.Bold = 1;} catch(e){}
	Printer.Writeln("---- Купюры ----");
	try {Printer.Bold = 0;} catch(e){}
	Printer.Writeln("--------------------------------");
	Printer.Writeln("Номинал   |   Кол-во |     Сумма");
	Printer.Writeln("----------+----------+----------");
	
	var amountByCurrency = new Object();
	var totalCount = 0;
	for (var c in reportNotes) {
		Printer.Writeln(formatBillsRegisterStr(reportNotes[c].note+" "+reportNotes[c].currency,
			reportNotes[c].count,
			reportNotes[c].note*reportNotes[c].count));
		if (amountByCurrency.hasOwnProperty(reportNotes[c].currency))
			amountByCurrency[reportNotes[c].currency] = amountByCurrency[reportNotes[c].currency] + reportNotes[c].note*reportNotes[c].count;
		else
			amountByCurrency[reportNotes[c].currency] = reportNotes[c].note*reportNotes[c].count;
		totalCount += reportNotes[c].count;
	}
	Printer.Writeln("--------------------------------");
	Printer.Writeln("Итого купюр : " + totalCount);
	Printer.Writeln("--------------------------------");

	for (var c in amountByCurrency) {
		Printer.Writeln("Итого "+c+": " + amountByCurrency[c]);
	}
	Printer.Writeln("--------------------------------");

	Printer.Writeln("");
	Printer.Writeln("");
	Printer.Writeln("Подпись: ");
	Printer.Writeln("");		
	Printer.Writeln("-------------------------------------");
	Printer.Cut(false);
}
