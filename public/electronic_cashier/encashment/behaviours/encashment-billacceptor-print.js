﻿(function() {

var PaymentsRegister;
var BillsRegister;
var CardOperationRegister;
var ClosedCardOperationRegister;
var ClosedPaymentsRegister;

_.addEventListeners({
	"load" : function() {
		Framework.AttachEvent(Printer, "OnPresenterEmptyEvent", function() { _['commit'](); } );
		
		_.registerVariables({
			"BillsRegister" : ""
		});

		reloadRegisters();

		_.registerActions({
			"commit" : function() { _.leave("commit"); },
			"error" : function() { _.leave("error"); }
		});
	},
	
	"billAcceptorFailure" : function () {
		return false;
	},

	"printerFailure" : function () {
		return false;
	},
	"inactivityTimer" : function() {
//		_.leave("commit");
		return false;
	},
	
	"run" : function() { 
		if (Printer.Offline || !Printer.DeviceReady) {
			printBillsRegisterMessage();
			_.leave('error');
		}
		printBillsRegister(true);
	}
});

function printBillsRegisterMessage() {
	if (Printer.Offline) {
		showAlert(getMessage("encashmentBillAcceptor.printerFailed"), {}, {}, hideAlert, 3000);
	}
}

function formatBillsRegisterStr(note, count, sum) {
	var spacesStr = "          ";

	return note + spacesStr.substring(("" + note).length, 10) + "|"
		+ spacesStr.substring((count + "").length, 10) + count + "|"
		+ spacesStr.substring((sum + "").length, 10) + sum;
}


function printBillsRegister() {
	if (_.BillsRegister != null && _.BillsRegister.Closed) {
	
		// открытие сессии
		try {Printer.Alignment = 2;} catch(e){}		
		Printer.Writeln("Квитанция об открытии сессии");
		try {Printer.Alignment = 0;} catch(e){}
		Printer.Writeln(getMessage("encashmentBillAcceptor.terminalNr") + couponProperties.terminalNumber());
		Printer.Writeln(getMessage("encashmentBillAcceptor.terminalAddress") + couponProperties.terminalAddress());	
		Printer.Writeln(getMessage("encashmentBillAcceptor.nextEncashmentNr") + (Number(Framework.Variables("encashment").id)+1));		
		Printer.Writeln(getMessage("encashmentBillAcceptor.installedCasseteNr") + (Framework.Variables("InstalledCasseteNr") ? Framework.Variables("InstalledCasseteNr") : getMessage("encashmentBillAcceptor.noCasseteNr")));
		Printer.Writeln(getMessage("encashmentBillAcceptor.registerOpen") + formatDateTime(_.BillsRegister.CloseDateTime));
		Printer.Writeln("--------------------------------");
		Printer.Writeln("");
		Printer.Writeln(getMessage("encashmentBillAcceptor.sign"));
		Printer.Writeln("");	
		Printer.Writeln("--------------------------------");		
		Printer.Cut(false);
	
		//Закрытие сессии
		try {Printer.Alignment = 2;} catch(e){}
		Printer.Writeln(couponProperties.header());
		Printer.Writeln(getMessage("encashmentBillAcceptor.couponCapture"));
		try {Printer.Alignment = 0;} catch(e){}
		Printer.Writeln(getMessage("encashmentBillAcceptor.terminalNr") + couponProperties.terminalNumber());
		Printer.Writeln(getMessage("encashmentBillAcceptor.terminalAddress") + couponProperties.terminalAddress());	
		Printer.Writeln(getMessage("encashmentBillAcceptor.replacedCasseteNr") + (Framework.Variables("ReplacedCasseteNr") ? Framework.Variables("ReplacedCasseteNr") : getMessage("encashmentBillAcceptor.noCasseteNr")));
		Printer.Writeln(getMessage("encashmentBillAcceptor.installedCasseteNr") + (Framework.Variables("InstalledCasseteNr") ? Framework.Variables("InstalledCasseteNr") : getMessage("encashmentBillAcceptor.noCasseteNr")));
		Printer.Writeln(getMessage("encashmentBillAcceptor.couponDate") + formatDateTime(new Date()));
		Printer.Writeln(getMessage("encashmentBillAcceptor.encashmentNr") + Framework.Variables("encashment").id);
		if (_.BillsRegister.Count > 0)	
			Printer.Writeln(getMessage("encashmentBillAcceptor.registerOpen") + formatDateTime(_.BillsRegister.Item(0).Occured));
		else	
			Printer.Writeln(getMessage("encashmentBillAcceptor.registerOpen") + formatDateTime(_.BillsRegister.CloseDateTime));
		Printer.Writeln(getMessage("encashmentBillAcceptor.registerClose") + formatDateTime(_.BillsRegister.CloseDateTime));
		
		// Printer.Writeln(getMessage("encashmentBillAcceptor.operator") + Framework.Variables("workerName"));
		// Printer.Writeln(getMessage("encashmentBillAcceptor.device") + Framework.Variables("device").Name);
		// Printer.Writeln(getMessage("encashmentBillAcceptor.ID") + Framework.Variables("device").DeviceId);
		
		var TotalCount = 0;
		var TotalAmount = 0;
		var TotalCountAll = 0;
		var currentCurrencyCode = "";
		var AggregatedBills = _.BillsRegister.Aggregate("CurrencyCode:group,sort;Amount:group,sort,sum,count;");
		for (var i=0; i<AggregatedBills.Count;) {
			currentCurrencyCode = AggregatedBills(i).CurrencyCode;
		
			Printer.Writeln("--------------------------------");
			Printer.Writeln("********* " + getMessage("encashmentBillAcceptor.currency") + (isDefined(currentCurrencyCode) && currentCurrencyCode!='undefined' ? currentCurrencyCode : '???') + " **********");
			Printer.Writeln("--------------------------------");
			Printer.Writeln(getMessage("encashmentBillAcceptor.billTableHeader"));
			Printer.Writeln("----------+----------+----------");
		
			var TotalCount = 0;
			var TotalAmount = 0;
		
			do {
				TotalCountAll += AggregatedBills(i).Count;
				TotalCount += AggregatedBills(i).Count;
				TotalAmount += AggregatedBills(i).SumAmount;
				Printer.Writeln(formatBillsRegisterStr(AggregatedBills(i).Amount,
						AggregatedBills(i).Count,
						AggregatedBills(i).SumAmount));
				
				i++;
			} while ((i<AggregatedBills.Count) && (AggregatedBills(i).CurrencyCode == currentCurrencyCode));
			
			Printer.Writeln("----------+----------+----------");
			Printer.Writeln(formatBillsRegisterStr(getMessage("encashmentBillAcceptor.total"), TotalCount, TotalAmount));	
		}
		Printer.Writeln("--------------------------------");
		Printer.Writeln("Обработано банкнот (" + currentCurrencyCode + "): " + (TotalCount));
		Printer.Writeln("Итого сумма выгрузки: " + TotalAmount + " " + currentCurrencyCode);
		Printer.Writeln("Совершено операций в " + currentCurrencyCode + ":");	
		Printer.Writeln(formatPaymentsRegisterStr("Всего операций", "-----", TotalAmount));		
		printPaymentsRegister(currentCurrencyCode);		
		Printer.Writeln("--------------------------------");
		Printer.Writeln(getMessage("encashmentBillAcceptor.totalAll") + TotalCountAll);
		Printer.Writeln("--------------------------------");
		Printer.Writeln("");
		Printer.Writeln(getMessage("encashmentBillAcceptor.sign"));
		Printer.Writeln("");	
		Printer.Writeln("--------------------------------");
		Printer.Cut(false);
	} else {
		alert(getMessage("encashmentBillAcceptor.registerError"));
	}	
}

function printPaymentsRegister(cur) {
	reloadRegisters();
	if (ClosedPaymentsRegister != null) {

		var CardCount = 0;
		var CardAmount = 0;
		var TotalAmount = 0;

		if (ClosedCardOperationRegister != null) {
			var AggregatedICardOperation = ClosedCardOperationRegister.Aggregate("Amount:count;");
		} 
		
		var AggregatedPayments = ClosedPaymentsRegister.Aggregate("Amount:count,sum;");
		for (var i=0; i<AggregatedPayments.Count; i++) {
			//if (AggregatedPayments(i).CurrencyCode == cur) {
				TotalAmount+=AggregatedPayments(i).Amount;
				if (ClosedCardOperationRegister != null) {
					for (var j=0; j<AggregatedICardOperation.Count; j++) {
						if (AggregatedPayments(i).DocumentNumber == AggregatedICardOperation(j).DocumentNumber && AggregatedICardOperation(j).OperationName != "Deposit") {
							CardCount++;
							CardAmount+=AggregatedPayments(i).Amount;
							break;
						}
					}
				//}	
			}	
		}	
		Printer.Writeln(formatPaymentsRegisterStr("Успешных наличными", AggregatedPayments.Count-CardCount, Math.round((TotalAmount-CardAmount)*100)/100));
	} else {
		Printer.Writeln("Ошибка инкассации\nВозможно нет журналов Принятых платежей");	
		Printer.Cut(false);
		alert("Ошибка инкассации\nВозможно нет журналов Принятых платежей");
		return;
	}		
}

function formatPaymentsRegisterStr(RecipientName, Count, Amount) {
	var spacesStr = "                    ";
	var temp = "";
	/*if (RecipientName.length > 20) {
		RecipientName = RecipientName.substring(1, 19);
	}*/

	 for (var i=0; i<Math.ceil(RecipientName.length/18)-1; i++) {
          temp+=RecipientName.substring(i*18,(i+1)*18) +  "¦  -  ¦      -    \n";     
     }
	 temp+=RecipientName.substring(i*18,(i+1)*18);     

	return temp + spacesStr.substring(RecipientName.substring(i*18,(i+1)*18).length, 18) + "¦"
		+ spacesStr.substring((Count + "").length, 5) + Count + "¦"
		+ spacesStr.substring((Amount + "").length, 13) + Amount;
}

function reloadRegisters() {
	_.BillsRegister = EWM.EventRegisters("IBillReceptionEvent", Framework.Variables("device").DeviceId);
	
	if (_.BillsRegister != null && _.BillsRegister.Count > 0 && _.BillsRegister.Item(0).Closed)
		_.BillsRegister = _.BillsRegister.Item(0);
	else
		_.BillsRegister = null;

	PaymentsRegister = EWM.EventRegisters("IPaymentAcceptedEvent");
	if ((PaymentsRegister != null) && (PaymentsRegister.Count > 0)) {
		if (!PaymentsRegister.Item(0).Closed && PaymentsRegister.Count > 1) {
			ClosedPaymentsRegister = PaymentsRegister.Item(1);
		} else {
			ClosedPaymentsRegister = PaymentsRegister.Item(0);
		}	
		PaymentsRegister = PaymentsRegister.Item(0);
	} else {
		PaymentsRegister = null;
	}

	CardOperationRegister = EWM.EventRegisters("ICardOperationEvent");
	if ((CardOperationRegister != null) && (CardOperationRegister.Count > 0)) {
		if (!CardOperationRegister.Item(0).Closed && CardOperationRegister.Count > 1) {
			ClosedCardOperationRegister = CardOperationRegister.Item(1);
		} else {
			ClosedCardOperationRegister = CardOperationRegister.Item(0);
		}
		CardOperationRegister = CardOperationRegister.Item(0);
	} else {
		CardOperationRegister = null;
	}		
}
})();