﻿(function() {
_.addEventListeners({
	"load" : function () {
		
		if (!PaymentDevice || !getParameter("paykiosk.encashmentBillacceptor", false)) {
			_.leave('skip');
			return false;
		}
		
		var encashment = PaymentDevice.StartEncashment(Framework.Variables("workerCode") + " " + Framework.Variables("workerName"));
		var device = encashment.CashDevices(Framework.Variables("exchangePosition"));
		Framework.Variables("exchangePosition")++;
		Framework.Variables("device") = {
			"DeviceId" : device.DeviceId,
			"Name" : device.Name
		}
	
		_.registerVariables({
			"encashment" : encashment,
			"device" : device,
			"exchange" : "",
			"cashUnit" : "",
			"cashUnits" : "",
			"BillsRegister" : "",
			"InstalledCasseteNr" : "",
			"enterCasseteNrMode" : false,
			"workerCode" : Framework.Variables("workerCode") ? Framework.Variables("workerCode") : "unknown",
			"ReplacedCasseteNr" : ""
		});

		_.registerActions({
			"commit" : new StateAction(
				function() { return _.InstalledCasseteNr.length > 0; },
				function() { encashBillAcceptor() } ),
			"skip" : function() { _.leave("skip"); }
		});

		_.registerEvents(['enterCasseteNr', 'deviceEncashment', 'encashmentReady']);
			
		_.addEventListeners({
			"deviceEncashment" : deviceEncashment
		});
		
		// Printer events
		if (!ReceiptPrinter.Offline)
			Framework.AttachEvent(ReceiptPrinter, "OnPrinterEvent", updatePrinterStatus);
		if (!FiscalPrinter.Offline)
			Framework.AttachEvent(FiscalPrinter, "OnFiscalPrinterEvent", updatePrinterStatus);
					
		Framework.AttachEvent(_.encashment, "OnDeviceEncashmentReady", onDeviceEncashmentReady);
		Framework.AttachEvent(_.encashment, "OnDeviceEncashmentComplete", onDeviceEncashmentComplete);
		
		// Cassette events
		Framework.AttachEvent(_.encashment, "OnCassetteExchangeReady", onCassetteExchangeReady);
		Framework.AttachEvent(_.encashment, "OnCassetteExchangeComplete", onCassetteExchangeComplete);			
	
	},
	"run" : function() {
		updatePrinterStatus();
		onEncashmentReady();
	},
	"unload" : function() {
		return true;
	},
	"billAcceptorFailure" : function () {
		return false;
	},

	"printerFailure" : function () {
		return false;
	},
	"inactivityTimer" : function() {
		return false;
	}
});

function updatePrinterStatus() {
	_.printerReady = !Printer.Offline && Printer.Deviceready;	
}

function onEncashmentReady() {
	// Перешли в режим инкассации, спрашиваем инкассиовать ли устройство
	reloadRegisters();
	_.fireEvent("encashmentReady");
}

function deviceEncashment() {
	// alert("deviceEncashment");
	// Начало инкассации устройства
	_.encashment.StartEncashment(_.device);
	_.cashUnits = _.device.cashUnits;
	// **** Убрать
	// onDeviceEncashmentReady();
}


function onDeviceEncashmentReady() {
	 // alert("onDeviceEncashmentReady");
	_.cashUnit = _.cashUnits.Item(0);
	Framework.Variables("ReplacedCasseteNr") = _.cashUnit.UnitId;	
	_.exchange = _.encashment.StartExchange(_.cashUnit);
	// **** Убрать
	// onCassetteExchangeReady();
}

function onCassetteExchangeReady() {
	// alert("onCassetteExchangeReady");
	encashBillAcceptor();
}

function onCassetteExchangeComplete() {
	// alert("onCassetteExchangeComplete");
	_.encashment.EndEncashment();
	// onDeviceEncashmentComplete();
}

function onDeviceEncashmentComplete() {
	// alert("onDeviceEncashmentComplete");
	_.leave('commit');
}

function reloadRegisters() {
	// alert(Framework.Variables("device").DeviceId)
	// alert(Framework.Variables("device").Name)
	_.BillsRegister = EWM.EventRegisters("IBillReceptionEvent", Framework.Variables("device").DeviceId);
	
	if (_.BillsRegister != null && _.BillsRegister.Count > 0 && !_.BillsRegister.Item(0).Closed)
		_.BillsRegister = _.BillsRegister.Item(0);
	else
		_.BillsRegister = null;
		// alert(_.BillsRegister.Count)
}

function executeRotateBills() {
	try {
		EWM.Rotate("IBillReceptionEvent", Framework.Variables("device").DeviceId);
	} catch (e) {
		alert(getMessage("encashmentBillAcceptor.registerError") + e.message  + " (" + e.description + ")");
	}
}

function executeRotatePayments() {
	try {
		EWM.Rotate("IPaymentAcceptedEvent");
	} catch (e) {
		alert("Невозможно закрыть журнал Принятых платежей\nВозможно нет описания события c:\\EventWatch\\Monitor2\\work\\definitions\\IPaymentAcceptedEvent.def\nДобавьте описание и попробуйте снова\nОшибка: " + e.message  + " (" + e.description + ")");
	}
}

function executeRotateCardOperations() {
	try {
		EWM.Rotate("ICardOperationEvent");
	} catch (e) {
		alert("Невозможно закрыть журнал Операций по карте\nВозможно нет описания события c:\\EventWatch\\Monitor2\\work\\definitions\\ICardOperationEventt.def\nДобавьте описание и попробуйте снова\nОшибка: " + e.message  + " (" + e.description + ")");
	}
}


function encashBillAcceptor() {
	if (_.InstalledCasseteNr.length == 0) {
		_.enterCasseteNrMode = true;
		_.fireEvent('enterCasseteNr');
		return;
	}
	_.enterCasseteNrMode = false;
	
	executeRotatePayments();
	executeRotateBills();
	executeRotateCardOperations();	
	
	Framework.Variables("InstalledCasseteNr") = _.InstalledCasseteNr;

	_.encashment.EndExchange(
		_.InstalledCasseteNr,
		"",
		0,
		0,
		0,
		getParameter("paykiosk.stackerSize")
	);	
}
})();