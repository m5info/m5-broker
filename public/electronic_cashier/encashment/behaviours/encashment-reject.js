﻿_.addEventListeners({
	"load" : function () {
		if (!getParameter("paykiosk.encashmentBilldispenser", false))
			_.leave("skip");
		if (!Dispenser || !PaymentDevice) {
			_.leave('skip');
			return;
		}

		Framework.AttachEvent(PaymentDevice, "OnBillRejected", onBillRejected);
		Framework.AttachEvent(PaymentDevice, "OnBillStacked", onBillStacked);
		
		_.registerActions({
		"commit" : completeEncashment,
		"skip" : new StateAction(
			function() { return notes.length == 0; },
			function() { 
				try {
					PaymentDevice.CancelPayment();
				} catch(e) {}
				_.leave("skip"); 
			} )
		});

		_.registerEvents([
			'printerStatusChanged', 
			'askExchangeCassette', 
			'noteAccepted']);

		_.registerVariables({
			"workerCode" : Framework.Variables("workerCode")
		});
	},
	"run" : function() {
		updatePrinterStatus();
	},
	"billAcceptorFailure" : function () {
		return false;
	},

	"printerFailure" : function () {
		return false;
	},
	"inactivityTimer" : function() {
		return false;
	}
});

function updatePrinterStatus() {
	_.printerReady = Printer && Printer.DeviceReady;
	_.fireEvent("printerStatusChanged");
}

var notes = [];

function enableBillAcceptor() {
	// Вошли в состояние с включенным купюроприемником, выключаем
	if (PaymentDevice.Status.Status == 2) {
		PaymentDevice.CancelPayment();
	}
	PaymentDevice.ChangePossible = false;
	PaymentDevice.AcceptPayment();
}

function onBillStacked(value, currency) {
	notes.push({ "note" : value, "currency" : currency, "count" : 1 });
	_.fireEvent('noteAccepted', value, currency);
	_.fireEvent('actionsChanged');
}

// Обработчики PaymentDevice
function onBillRejected(reason, hint) {
	playSoundFunction("error.wav", false);
}

function completeEncashment() {
	try {
		PaymentDevice.CommitPayment();
	} catch(e) {}
	
	var reportNotes = {};
	for (var i = 0; i < notes.length; i++) {
		var id = notes[i].currency+'_'+notes[i].note;
		if (reportNotes.hasOwnProperty(id))
			reportNotes[id].count = reportNotes[notes[i].currency].count+1;
		else {
			reportNotes[id] = notes[i];
		}
	}
	Framework.Variables("NotesRejected") = reportNotes;
	_.leave('commit');
}