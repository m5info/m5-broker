﻿(function() {
_.addEventListeners({
	"load" : function () {
	
		if (!getParameter("paykiosk.encashmentPayments", false)){
			_.leave("skip");
			return false;
		}	
	
		_.registerActions({
			"commit" : function() { closePaymentRegister(); },
			// "skip" : checkFiscalPrinter
			"skip" :  function() { _.leave("skip"); }
		});

		_.registerEvents(['printerStatusChanged']);

		_.registerVariables({
			"workerCode" : Framework.Variables("workerCode") ? Framework.Variables("workerCode") : "unknown",
			"PaymentsRegister" : ""
		});

		// Printer
		if (!ReceiptPrinter.Offline)
			Framework.AttachEvent(ReceiptPrinter, "OnPrinterEvent", this.updatePrinterStatus);
		if (!FiscalPrinter.Offline)
			Framework.AttachEvent(FiscalPrinter, "OnFiscalPrinterEvent", this.updatePrinterStatus);
		reloadRegisters();
	},
	"run" : function() {
		updatePrinterStatus();
	},
	"billAcceptorFailure" : function () {
		return false;
	},

	"printerFailure" : function () {
		return false;
	},
	"inactivityTimer" : function() {
		return false;
	}
});

function updatePrinterStatus() {
	_.printerReady = !Printer.Offline && Printer.Deviceready;
	_.fireEvent("printerStatusChanged");
}

function reloadRegisters() {
	_.PaymentsRegister = EWM.EventRegisters("IPaymentAcceptedEvent");
	
	if (_.PaymentsRegister != null && _.PaymentsRegister.Count > 0 && !_.PaymentsRegister.Item(0).Closed)
		_.PaymentsRegister = _.PaymentsRegister.Item(0);
	else
		_.PaymentsRegister = null;
}


function executeRotatePayments() {
	try {
		EWM.Rotate("IPaymentAcceptedEvent");
	} catch (e) {
		alert(getMessage("encashmentPayments.registerError") + e.message  + " (" + e.description + ")");
	}
}

function checkFiscalPrinter() {
	if (!Printer.Offline && Printer.DeviceReady)
		_.leave('commit');
	else
		_.leave('error');
}

function closePaymentRegister() {
	executeRotatePayments();
	checkFiscalPrinter();	
}
})();