﻿(function() {
_.addEventListeners({
	"load" : function() {
		Framework.AttachEvent(Printer, "OnPresenterEmptyEvent", function() { _['commit'](); } );
		
		_.registerVariables({
			"CoinsRegister" : ""
		});

		reloadRegisters();

		_.registerActions({
			"commit" : function() { _.leave("commit"); },
			"error" : function() { _.leave("error"); }
		});
	},

	"printerFailure" : function () {
		return false;
	},
	"inactivityTimer" : function() {
//		_.leave("commit");
		return false;
	},
	
	"run" : function() { 
		if (Printer.Offline || !Printer.DeviceReady) {
			printCoinsRegisterMessage();
			_.leave('error');
		}
		printCoinsRegister(true);
	}
});

function printCoinsRegisterMessage() {
	if (Printer.Offline) {
		showAlert(getMessage("encashmentCoinAcceptor.printerFailed"), {}, {}, hideAlert, 3000);
	}
}

function formatCoinsRegisterStr(note, count, sum) {
	var spacesStr = "          ";

	return note + spacesStr.substring(("" + note).length, 10) + "|"
		+ spacesStr.substring((count + "").length, 10) + count + "|"
		+ spacesStr.substring((sum + "").length, 10) + sum;
}


function printCoinsRegister(printClosed) {
	if (_.CoinsRegister != null && _.CoinsRegister.Closed) {
	
		try {Printer.Alignment = 2;} catch(e){}
		Printer.Writeln(couponProperties.header());
		try {Printer.Alignment = 0;} catch(e){}
		try {Printer.WhiteBlackInversion = 1;} catch(e){}
		Printer.Writeln(getMessage("encashmentCoinAcceptor.couponCapture"));
		try {Printer.WhiteBlackInversion = 0;} catch(e){}
		Printer.Writeln(getMessage("encashmentCoinAcceptor.encashmentNr") + Framework.Variables("encashment").id);
		Printer.Writeln(getMessage("encashmentCoinAcceptor.operator") + Framework.Variables("workerName"));
		if (_.CoinsRegister.Count > 0)	
			Printer.Writeln(getMessage("encashmentCoinAcceptor.registerOpen") + _.CoinsRegister.Item(0).Occured);
		else	
			Printer.Writeln(getMessage("encashmentCoinAcceptor.registerOpen") + formatDateTime(_.CoinsRegister.CloseDateTime));
		Printer.Writeln(getMessage("encashmentCoinAcceptor.registerClose") + formatDateTime(_.CoinsRegister.CloseDateTime));
		Printer.Writeln(getMessage("encashmentCoinAcceptor.device") + Framework.Variables("device").Name);
		Printer.Writeln(getMessage("encashmentCoinAcceptor.ID") + Framework.Variables("device").DeviceId);
		Printer.Writeln(getMessage("encashmentCoinAcceptor.terminalNr") + couponProperties.terminalNumber());
		Printer.Writeln(getMessage("encashmentCoinAcceptor.terminalAddress") + couponProperties.terminalAddress());
		Printer.Writeln(getMessage("encashmentCoinAcceptor.replacedCasseteNr") + (Framework.Variables("ReplacedCasseteNr") ? Framework.Variables("ReplacedCasseteNr") : getMessage("encashmentCoinAcceptor.noCasseteNr")));
		Printer.Writeln(getMessage("encashmentCoinAcceptor.installedCasseteNr") + (Framework.Variables("InstalledCasseteNr") ? Framework.Variables("InstalledCasseteNr") : getMessage("encashmentCoinAcceptor.noCasseteNr")));
		
		Printer.Writeln(getMessage("encashmentCoinAcceptor.couponDate") + formatDateTime(new Date()));
		
		var TotalCount = 0;
		var TotalAmount = 0;
		var TotalCountAll = 0;
		var AggregatedCoins = _.CoinsRegister.Aggregate("CurrencyCode:group,sort;Amount:group,sort,sum,count;");
		for (var i=0; i<AggregatedCoins.Count;) {
			var currentCurrencyCode = AggregatedCoins(i).CurrencyCode;
		
			Printer.Writeln("--------------------------------");
			Printer.Writeln("********* " + getMessage("encashmentCoinAcceptor.currency") + (isDefined(currentCurrencyCode) && currentCurrencyCode!='undefined' ? currentCurrencyCode : '???') + " **********");
			Printer.Writeln("--------------------------------");
			Printer.Writeln(getMessage("encashmentCoinAcceptor.coinTableHeader"));
			Printer.Writeln("----------+----------+----------");
		
			var TotalCount = 0;
			var TotalAmount = 0;
		
			do {
				TotalCountAll += AggregatedCoins(i).Count;
				TotalCount += AggregatedCoins(i).Count;
				TotalAmount += AggregatedCoins(i).SumAmount/100;
				Printer.Writeln(formatCoinsRegisterStr(AggregatedCoins(i).Amount/100,
						AggregatedCoins(i).Count,
						AggregatedCoins(i).SumAmount/100));
				
				i++;
			} while ((i<AggregatedCoins.Count) && (AggregatedCoins(i).CurrencyCode == currentCurrencyCode));
			
			Printer.Writeln("----------+----------+----------");
			Printer.Writeln(formatCoinsRegisterStr(getMessage("encashmentCoinAcceptor.total"), TotalCount, TotalAmount));	
			Printer.Writeln("--------------------------------");
		}
		try {Printer.Bold = 1;} catch(e){}
		Printer.Writeln("--------------------------------");
		Printer.Writeln(getMessage("encashmentCoinAcceptor.totalAll") + TotalCountAll);
		Printer.Writeln("--------------------------------");
		try {Printer.Bold = 0;} catch(e){}
		Printer.Writeln("");
		Printer.Writeln(getMessage("encashmentCoinAcceptor.sign"));
		Printer.Writeln("");	
		Printer.Writeln("--------------------------------");
		Printer.Cut(false);
	} else {
		alert(getMessage("encashmentCoinAcceptor.registerError"));
	}	
}

function reloadRegisters() {
	_.CoinsRegister = EWM.EventRegisters("ICoinReceptionEvent", Framework.Variables("device").DeviceId);
	
	if (_.CoinsRegister != null && _.CoinsRegister.Count > 0 && _.CoinsRegister.Item(0).Closed)
		_.CoinsRegister = _.CoinsRegister.Item(0);
	else
		_.CoinsRegister = null;
}
})();