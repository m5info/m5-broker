﻿(function() {
_.addEventListeners({
	"load" : function() {
		Framework.AttachEvent(Printer, "OnPresenterEmptyEvent", function() { _['commit'](); } );
	
		_.registerVariables({
			"PaymentsRegister" : ""
		});	
		
		reloadRegisters();
	
		_.registerActions({
			"commit" : function() { _.leave("commit"); },
			"error" : function() { _.leave("error"); }
		});
	},
	
	"billAcceptorFailure" : function () {
		return false;
	},

	"printerFailure" : function () {
		return false;
	},
	"inactivityTimer" : function() {
//		_.leave("commit");
		return false;
	},
	
	"run" : function() { 
		if (Printer.Offline) {
			printErrorMessage();
			_.leave('error');
		}
		printPaymentsRegister(true);
	}
});

function printErrorMessage() {
	if (!(Printer && Printer.DeviceReady)) {
		showAlert("", {}, {}, _['error'], 3000);
	}
}

function formatPaymentsRegisterStr(RecipientName, Count, Amount) {
	var spacesStr = "                    ";
	var temp = "";
	/*if (RecipientName.length > 20) {
		RecipientName = RecipientName.substring(1, 19);
	}*/

	for (var i=0; i<Math.ceil(RecipientName.length/17)-1; i++) {
        temp+=RecipientName.substring(i*17,(i+1)*17) +  "|  -  |      -    \n";     
    }
	temp+=RecipientName.substring(i*17,(i+1)*17);     

	return temp + spacesStr.substring(RecipientName.substring(i*17,(i+1)*17).length, 17) + "|"
		+ spacesStr.substring((Count + "").length, 5) + Count + "|"
		+ spacesStr.substring((Amount + "").length, 13) + Amount;
}


function printPaymentsRegister() {
	if (_.PaymentsRegister != null && _.PaymentsRegister.Closed) {
		try {Printer.Alignment = 2;} catch(e){}
		Printer.Writeln(couponProperties.header());
		try {Printer.Alignment = 0;} catch(e){}
		try {Printer.WhiteBlackInversion = 1;} catch(e){}
		Printer.Writeln(getMessage("encashmentPayments.couponCapture"));
		try {Printer.WhiteBlackInversion = 0;} catch(e){}
		try {Printer.Alignment = 0;} catch(e){}
		Printer.Writeln(getMessage("encashmentPayments.encashmentNr") + Framework.Variables("encashment").id);
		Printer.Writeln(getMessage("encashmentPayments.operator") + Framework.Variables("workerName"));
		if (_.PaymentsRegister.Count > 0)	
			Printer.Writeln(getMessage("encashmentPayments.registerOpen") + _.PaymentsRegister.Item(0).Occured);
		else	
			Printer.Writeln(getMessage("encashmentPayments.registerOpen") + formatDateTime(_.PaymentsRegister.CloseDateTime));
		Printer.Writeln(getMessage("encashmentPayments.registerClose") + formatDateTime(_.PaymentsRegister.CloseDateTime));
		Printer.Writeln(getMessage("encashmentPayments.terminalNr") + couponProperties.terminalNumber());
		Printer.Writeln(getMessage("encashmentPayments.terminalAddress") + couponProperties.terminalAddress());
		Printer.Writeln(getMessage("encashmentPayments.couponDate") + formatDateTime(new Date()));
		Printer.Writeln("-------------------------------------");
		Printer.Writeln(getMessage("encashmentPayments.paymentTableHeader"));
		Printer.Writeln("-----------------+-----+-------------");
			
		var TotalCount = 0;
		var TotalAmount = 0;

		var AggregatedPayments = _.PaymentsRegister.Aggregate("RecipientName:group,sort;Amount:sum,count;CurrencyCode:group;");

		for (var i=0; i<AggregatedPayments.Count;) {
			var currentCurrencyCode = AggregatedPayments(i).currencyCode;
			do {
				TotalCount += AggregatedPayments(i).Count;
				TotalAmount += AggregatedPayments(i).Amount;

				Printer.Writeln(formatPaymentsRegisterStr(AggregatedPayments(i).RecipientName,
						AggregatedPayments(i).Count,
						Math.round(AggregatedPayments(i).Amount*100)/100 + " " + (isDefined(currentCurrencyCode) && currentCurrencyCode!='undefined' ? " " + currentCurrencyCode : " ???")));
				Printer.Writeln("-----------------+-----+-------------");
				i++;			
			} while ((i<AggregatedPayments.Count) && (AggregatedPayments(i).currencyCode == currentCurrencyCode));

		}
			
		var AggregatedPayments = _.PaymentsRegister.Aggregate("Amount:sum,count;CurrencyCode:group;");

		var TotalCountAll = 0;

		for (var i=0; i<AggregatedPayments.Count;) {
		
			var TotalCount = 0;
			var TotalAmount = 0;
		
			var currentCurrencyCode = AggregatedPayments(i).currencyCode;
			do {
				TotalCount += AggregatedPayments(i).Count;
				TotalAmount += AggregatedPayments(i).Amount;
				try {Printer.Bold = 1;} catch(e){}
				Printer.Writeln(formatPaymentsRegisterStr(getMessage("encashmentPayments.couponTotal") + (isDefined(AggregatedPayments(i).currencyCode) && AggregatedPayments(i).currencyCode!='undefined' ? " " + AggregatedPayments(i).currencyCode : " ???"), TotalCount, Math.round(AggregatedPayments(i).Amount*100)/100));
				Printer.Writeln("-----------------+-----+-------------");
				i++;			
			} while ((i<AggregatedPayments.Count) && (AggregatedPayments(i).currencyCode == currentCurrencyCode));
			TotalCountAll += TotalCount;
		}		
		Printer.Writeln(formatPaymentsRegisterStr(getMessage("encashmentPayments.couponTotalAll"), TotalCountAll, "-"));
		try {Printer.Bold = 0;} catch(e){}
		
		Printer.Writeln("-------------------------------------");
		Printer.Writeln("");
		Printer.Writeln(getMessage("encashmentPayments.sign"));
		Printer.Writeln("");		
		Printer.Writeln("-------------------------------------");
		Printer.Cut(false);
	} else {
		alert(getMessage("encashmentPayments.registerError"));
	}		
}

function reloadRegisters() {
	_.PaymentsRegister = EWM.EventRegisters("IPaymentAcceptedEvent");
	
	if (_.PaymentsRegister != null && _.PaymentsRegister.Count > 0 && _.PaymentsRegister.Item(0).Closed)
		_.PaymentsRegister = _.PaymentsRegister.Item(0);
	else
		_.PaymentsRegister = null;
}
})();