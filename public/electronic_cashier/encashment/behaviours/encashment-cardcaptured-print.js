﻿

_.addEventListeners({
	"load" : function() {
		
		CardsRegister = EWM.EventRegisters("ICardCapturedEvent");
		if ((CardsRegister != null) && (CardsRegister.Count > 0)) {
			CardsRegister = CardsRegister.Item(0);
		} else {
			CardsRegister = null;
		}
	
		_.registerActions({
			"commit" : function() { _.leave("commit") },
			"error" : function() { _.leave("error"); }
		});
	},
	
	"billAcceptorFailure" : function () {
		return false;
	},

	"printerFailure" : function () {
		return false;
	},
	"inactivityTimer" : function() {
//		_.leave("commit");
		return false;
	},
	
	"run" : function() { 
		if (FiscalPrinter.Offline) {
			printErrorMessage();
			_.leave('error');
		}
		printCardsRegister(true);
	}
});

function printErrorMessage() {
	if (!(Printer && Printer.DeviceReady)) {
		showAlert("Устройство для печати недоступно", {}, {}, _['error'], 3000);
	}
}

function formatCardsRegisterStr(RecipientName, Count, Amount) {
	var spacesStr = "                    ";
	var temp = "";
	/*if (RecipientName.length > 20) {
		RecipientName = RecipientName.substring(1, 19);
	}*/

	 for (var i=0; i<Math.ceil(RecipientName.length/17)-1; i++) {
          temp+=RecipientName.substring(i*17,(i+1)*17) +  "|  -  |      -    \n";     
     }
	 temp+=RecipientName.substring(i*17,(i+1)*17);     

	return temp + spacesStr.substring(RecipientName.substring(i*17,(i+1)*17).length, 17) + "|"
		+ spacesStr.substring((Count + "").length, 5) + Count + "|"
		+ spacesStr.substring((Amount + "").length, 13) + Amount;
}


function printCardsRegister(printClosed) {
	if (CardsRegister != null && ((CardsRegister.Closed && printClosed) || (!CardsRegister.Closed))) {
		try {Printer.Alignment = 2;} catch(e){}
		Printer.Writeln(couponProperties.header());

		try {Printer.Alignment = 0;} catch(e){}

		Printer.Writeln("----- Сводный журнал по захваченным картам -----");
		if (CardsRegister.Closed) {
			Printer.Writeln("Журнал закрыт: " + formatDateTime(CardsRegister.CloseDateTime));
		} else {
			Printer.Writeln("Журнал не закрыт");
		}
		Printer.Writeln("Оператор: " + Framework.Variables("workerCode"));
		Printer.Writeln("Номер терминала: " + couponProperties.terminalNumber());
		Printer.Writeln("Адрес терминала: " + couponProperties.terminalAddress());
		Printer.Writeln("Дата печати: " + formatDateTime(new Date()));
		Printer.Writeln("------------------------------------------------");
		Printer.Writeln(" №|   Дата/Время    |   Номер карты   |  Причина");
		Printer.Writeln("--+-----------------+-----------------+---------");
			


		var AggregatedCards = CardsRegister;//.Aggregate("Occured:group;CardNumber:group;ResponseCode");
				for (var i=0, c = AggregatedCards.Count; i<c; i++) {
					Printer.Writeln((i+1)+" "+AggregatedCards(i).Occured+" "+AggregatedCards(i).CardNumber+"     "+AggregatedCards(i).ResponseCode);
				}



		// for (var i=0; i<AggregatedPayments.Count;) {
		
			// var TotalCount = 0;
			// var TotalAmount = 0;
		
			// var currentCurrencyCode = AggregatedPayments(i).currencyCode;
			// do {
				// TotalCount += AggregatedPayments(i).Count;
				// TotalAmount += AggregatedPayments(i).Amount;
				// try {Printer.Bold = 1;} catch(e){}
				// Printer.Writeln(formatCardsRegisterStr("Итого " + (isDefined(AggregatedPayments(i).currencyCode) && AggregatedPayments(i).currencyCode!='undefined' ? " " + AggregatedPayments(i).currencyCode : " ???"), TotalCount, Math.round(AggregatedPayments(i).Amount*100)/100));
				//Printer.Writeln("-----------------+-----+-------------");
				// i++;			
			// } while ((i<AggregatedPayments.Count) && (AggregatedPayments(i).currencyCode == currentCurrencyCode));
			// TotalCountAll += TotalCount;
		// }		
		//Printer.Writeln(formatCardsRegisterStr("Итого Всего", TotalCountAll, "-"));
		//try {Printer.Bold = 0;} catch(e){}
		
		Printer.Writeln("------------------------------------------------");
		Printer.Writeln("");
		Printer.Writeln("Подпись:");
		Printer.Writeln("");		
		Printer.Writeln("------------------------------------------------");
		Printer.Cut(false);
	} else {
		alert("Нет ни одного журнала по платежам. Печать журнала платежей невозможна");
	}		
}
