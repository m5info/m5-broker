﻿(function() {
_.addEventListeners({
	"load" : function () {
		if (!getParameter("paykiosk.encashmentCardCaptured", false)){
			_.leave("skip");
		return false;	
		}	
		_.registerActions({
			"close" : new StateAction(
				function() { return _.cardsRegister != null && !_.cardsRegister.Closed; },
				function() { closeCardsRegister(); }
			),
			"skip" : function() {_.leave('skip')},
			"print" : function() { _.leave('success') }
		});

		_.registerEvents(['printerStatusChanged']);

		_.registerVariables({
			"workerCode" : Framework.Variables("workerCode"),
			"cardsRegister" : null,
			"cardCount" : null
		});

		// Printer
		if (!ReceiptPrinter.Offline)
			Framework.AttachEvent(ReceiptPrinter, "OnPrinterEvent", this.updatePrinterStatus);
		if (!FiscalPrinter.Offline)
			Framework.AttachEvent(FiscalPrinter, "OnFiscalPrinterEvent", this.updatePrinterStatus);
		reloadRegisters();
	},
	"run" : function() {
		updatePrinterStatus();
	},
	"billAcceptorFailure" : function () {
		return false;
	},

	"printerFailure" : function () {
		return false;
	},
	"inactivityTimer" : function() {
		return false;
	}
});

function updatePrinterStatus() {
	_.printerReady = Printer && Printer.DeviceReady;
	_.fireEvent("printerStatusChanged");
}


function reloadRegisters() {
	_.cardsRegister = EWM.EventRegisters("ICardCapturedEvent");
	if ((_.cardsRegister != null) && (_.cardsRegister.Count > 0)) {
		_.cardsRegister = _.cardsRegister.Item(0);
	} else {
		_.cardsRegister = null;
	}
}

function executeRotateCardCaptured() {
	try {
		if (_.cardsRegister != null && !_.cardsRegister.Closed) {
			EWM.Rotate("ICardCapturedEvent");
		}	
	} catch (e) {
		alert("Невозможно закрыть журнал Принятых платежей\nВозможно журнал уже закрыт\nОшибка: " + e.message  + " (" + e.description + ")");	
	}
}

function writeCardCapturedExceptionEvent() {
	try {
		var CardCaptureExceptionEvent = EWM.CreateEvent("ICardCapturedExceptionEvent");
	} catch (e) {
		alert("Журнал закрыт, однако событие на сервер мониторинга отправить невозможно\nВозможно нет описания события c:\\EventWatch\\Monitor2\\work\\definitions\\ICardCapturedClosedEvent.def\nДобавьте описание на терминал для корректной работы\nОшибка: " + e.message  + " (" + e.description + ")");
		return;
	}
	CardCaptureExceptionEvent.WorkerCode = _.workerCode;
	CardCaptureExceptionEvent.Count = _.cardCount;
	EWM.WriteEvent(CardCaptureExceptionEvent);
}

function closeCardsRegister() {
	executeRotateCardCaptured();
	writeCardCapturedExceptionEvent();
	if (!Printer || !Printer.DeviceReady) {
		_.leave('error');
	} else {
		_.leave('success');
	}
}
})()