﻿(function() {

_.addEventListeners({
	"load" : function() {
		Framework.AttachEvent(Printer, "OnPresenterEmptyEvent", function() { _['commit'](); } );
	
		_.registerActions({
			"commit" : function() { _.leave("commit"); },
			"error" : function() { _.leave("error"); }
		});
	},
	
	"billAcceptorFailure" : function () {
		return false;
	},

	"printerFailure" : function () {
		return false;
	},
	"inactivityTimer" : function() {
//		_.leave("commit");
		return false;
	},
	
	"run" : function() { 
		if (!Printer || !Printer.DeviceReady) {
			printErrorMessage();
			_.leave('error');
		}
		printCassettesReport(true);
	}
});

function printErrorMessage() {
	if (!(Printer && Printer.DeviceReady)) {
		showAlert("Устройство для печати недоступно", {}, {}, _['error'], 3000);
	}
}

function formatCountersStr(name, count) {
	var spacesStr = formatLine(".");		
	return name.toString() + spacesStr.substring(1, getParameter("paykiosk.receiptWidth") - name.toString().length - count.toString().length + 1) + count.toString();
}

function formatCenter(str) {
	var spacesStr = formatLine(" ");		
	var left = Math.ceil((getParameter("paykiosk.receiptWidth") - str.length)/2);
	var right = getParameter("paykiosk.receiptWidth") - str.length - left;
	return spacesStr.substring(0, left) + str.toString() + spacesStr.substring(0, right);
}

function formatLine(ch) {
	var str = "";
	while (str.length < getParameter("paykiosk.receiptWidth")) {
		str += ch;
	}
	return str.substring(0, getParameter("paykiosk.receiptWidth"));
}


function printCassettesReport() {
	var cassettesOld = Framework.Variables("CassettesRemoved");
	var cassettesNew = Framework.Variables("CassettesInstalled");

	try {Printer.Alignment = 2;} catch(e){}
	Printer.Writeln(couponProperties.header());
	try {Printer.Alignment = 0;} catch(e){}
	try {Printer.WhiteBlackInversion = 1;} catch(e){}
	Printer.Writeln(formatCenter(getMessage("encashmentBillDispenser.couponCapture")));
	try {Printer.WhiteBlackInversion = 0;} catch(e){}
	Printer.Writeln(getMessage("encashmentBillDispenser.encashmentNr") + Framework.Variables("encashment").id);
	Printer.Writeln(getMessage("encashmentBillDispenser.operator") + Framework.Variables("workerName"));
	Printer.Writeln(getMessage("encashmentBillDispenser.device") + Framework.Variables("device").Name);
	Printer.Writeln(getMessage("encashmentBillDispenser.ID") + Framework.Variables("device").DeviceId);
	Printer.Writeln(getMessage("encashmentBillDispenser.terminalNr") + couponProperties.terminalNumber());
	Printer.Writeln(getMessage("encashmentBillDispenser.terminalAddress") + couponProperties.terminalAddress());
	
	Printer.Writeln(getMessage("encashmentBillDispenser.couponDate") + formatDateTime(new Date()));

	try {Printer.Bold = 1;} catch(e){}
	try {Printer.WhiteBlackInversion = 1;} catch(e){}
	Printer.Writeln(formatCenter(getMessage("encashmentBillDispenser.replacedCassettes")));
	try {Printer.WhiteBlackInversion = 0;} catch(e){}
	try {Printer.Bold = 0;} catch(e){}
	for (var i = 0; i < cassettesOld.length; i++) {
		try {Printer.Bold = 1;} catch(e){}
		Printer.Writeln(formatLine("-"));
		Printer.Writeln(formatCenter(getMessage("encashmentBillDispenser.cassetteNr") + cassettesOld[i].number + " (" + cassettesOld[i].unitId + ")"));
		Printer.Writeln(formatLine("-"));
		try {Printer.Bold = 0;} catch(e){}
		var ratio = typeof getCurrency(cassettesOld[i].currencyId) !== 'undefined' ? getCurrency(cassettesOld[i].currencyId).ratio : 100;
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteType"), getMessage("encashmentBillDispenser." + getType(cassettesOld[i].type))));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteStatus"), getMessage("encashmentBillDispenser." + getStatus(cassettesOld[i].status))));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.nominal"), Math.round(Number(cassettesOld[i].value)/ratio) + " " + cassettesOld[i].currencyId));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteLoad"), cassettesOld[i].initialCount + " шт"));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteDispensed"), cassettesOld[i].dispensedCount + " шт"));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteRetracted"), cassettesOld[i].retractedCount + " шт"));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteRecipt"), cassettesOld[i].presentedCount + " шт"));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteDispenseSum"), Math.round(cassettesOld[i].dispensedAmount/ratio) + " " + cassettesOld[i].currencyId));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteRejected"), cassettesOld[i].rejectedCount +  " шт"));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteCount"), cassettesOld[i].currentCount + " шт"));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteMin"), + cassettesOld[i].minimum + " шт"));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteToDispense"), cassettesOld[i].toDispenseCount + " шт"));
		if (i == cassettesOld.length - 1) {
			try {Printer.Bold = 1;} catch(e){}
			Printer.Writeln(formatLine("-"));
			try {Printer.Bold = 0;} catch(e){}
			Printer.Writeln("");
		}	
	}

	try {Printer.Bold = 1;} catch(e){}
	try {Printer.WhiteBlackInversion = 1;} catch(e){}
	Printer.Writeln(formatCenter(getMessage("encashmentBillDispenser.installedCassettes")));
	try {Printer.WhiteBlackInversion = 0;} catch(e){}
	try {Printer.Bold = 0;} catch(e){}
	for (var i = 0; i < cassettesNew.length; i++) {
		try {Printer.Bold = 1;} catch(e){}
		Printer.Writeln(formatLine("-"));
		Printer.Writeln(formatCenter(getMessage("encashmentBillDispenser.cassetteNr") + cassettesNew[i].number + " (" + cassettesNew[i].unitId + ")"));
		Printer.Writeln(formatLine("-"));
		try {Printer.Bold = 0;} catch(e){}
		var ratio = typeof getCurrency(cassettesNew[i].currencyId) !== 'undefined' ? getCurrency(cassettesNew[i].currencyId).ratio : 100;
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteType"), getMessage("encashmentBillDispenser." + getType(cassettesNew[i].type))));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteStatus"), getMessage("encashmentBillDispenser." + getStatus(cassettesNew[i].status))));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.nominal"), Math.round(cassettesNew[i].value/ratio) + " " + cassettesNew[i].currencyId));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteLoad"), cassettesNew[i].initialCount + " шт"));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteMin"), + cassettesNew[i].minimum + " шт"));
		Printer.Writeln(formatCountersStr(getMessage("encashmentBillDispenser.cassetteToDispense"), cassettesNew[i].toDispenseCount + " шт"));
		if (i == cassettesNew.length - 1) {
			try {Printer.Bold = 1;} catch(e){}
			Printer.Writeln(formatLine("-"));
			try {Printer.Bold = 0;} catch(e){}
			Printer.Writeln("");
		}	
	}

	Printer.Writeln("");
	Printer.Writeln(getMessage("encashmentBillDispenser.sign"));
	Printer.Writeln("");		
	Printer.Writeln(formatLine("-"));
	Printer.Cut(false);
}

function getStatus(status) {
	switch(Number(status)) {
	 case 0 : return "normal";
	 case 1 : return "cusFull";
     case 2 : return "cusHigh";
     case 3 : return "cusLow";
     case 4 : return "cusEmpty";
     case 5 : return "cusInoperative";
     case 6 : return "cusMissing";
     case 7 : return "cusNoValue";
     default : return "unknown";
	}
}

function getType(type) {
	switch(Number(type)) {
	 case 0 : return "cutNotApplicable";
     case 1 : return "cutReject";
     case 2 : return "cutBill";
     case 3 : return "cutCoinCylinder";
     case 4 : return "cutCoinDispenser";
     case 5 : return "cutRetract";
     case 6 : return "cutCoupon";
     case 7 : return "cutDocument";
     case 8 : return "cutCashIn";
     case 9 : return "cutEscrow";
     case 10 : return "cutReplenishmentContainer";
     case 11 : return "cutRecycling";
     default : return "unknown";
	}
}
})();