﻿(function() {

_.addEventListeners({
	"load" : function () {
		showAlert(getMessage("encashmentCoinAcceptor.preparation") + "<br>" + getMessage("encashmentCoinAcceptor.wait"), {}, {}, "", 0);

		_.addEventListeners({
			"enterCasseteNr" : startEnterCasseteNr,
			"encashmentReady" : encashmentReady
		});
		
		document.getElementById("continueButton").value = getMessage("encashmentCoinAcceptor.skipDevice");
		document.getElementById("fieldName").innerHTML = getMessage("encashmentCoinAcceptor.enter_unitId");
		ViewButtons.bind({"continueButton" : "skip"});
	},
	"actionsChanged" : updateButtons,
	"char" : function (ch) {
		if (_.enterCasseteNrMode) {
			appendChar(ch)
			return false;
		}
		return true;
	},
	"key" : function(key) {
		switch(key) {
			// backspace
			case 8 :
				if (_.enterCasseteNrMode)
					removeChar();
				return false;
			//esc
			case 27 : {
				_['skip']();
				return false;
			}
			case 13 :
				if (_.enterCasseteNrMode) {
					onEntryComplete();
					return false;
				}
				_['commit']();
				return false;
		}
		return true;
	},
	"run" : function() {
	}
});

function encashmentReady() {
	// alert("encashmentReady")
	document.getElementById("status").innerHTML = getMessage("encashmentCoinAcceptor.encashmentCoinAcceptor") + "<br>" + getMessage("encashmentCoinAcceptor.device") + Framework.Variables("device").Name + " (" + Framework.Variables("device").DeviceId + ")";
	
	var out = "";
	if (_.CoinsRegister != null) {
		out += "<table class='coinTable'>";
		out += "<tr class='coinTableHeader'><td>"+getMessage("encashmentCoinAcceptor.coin")+"</td>";
		out += "<td>"+getMessage("encashmentCoinAcceptor.countShort")+"</td>";
		out += "<td>"+getMessage("encashmentCoinAcceptor.summ")+"</td></tr>";
		out += "<tr><td colspan=3></td></tr>";

		var AggregatedCoins = _.CoinsRegister.Aggregate("CurrencyCode:group,sort;Amount:group,sort,sum,count;");

		var TotalCountAll = 0;
		var TotalAmountAll = 0;

		for (var i=0; i<AggregatedCoins.Count;) {
			var TotalCount = 0;
			var TotalAmount = 0;
			var currentCurrencyCode = AggregatedCoins(i).CurrencyCode;
			do {
				TotalCountAll += AggregatedCoins(i).Count;
				TotalAmountAll += AggregatedCoins(i).Amount/100;
				TotalCount += AggregatedCoins(i).Count;
				TotalAmount += AggregatedCoins(i).SumAmount/100;
				out += "<tr><td align=right>" + AggregatedCoins(i).Amount/100 + (isDefined(currentCurrencyCode) && currentCurrencyCode!='undefined' ? " " + currentCurrencyCode : " ???") + "</td><td align=right>" + AggregatedCoins(i).Count + "</td><td align=right>" + AggregatedCoins(i).SumAmount/100 + "</td></tr>";
				i++;
			} while ((i<AggregatedCoins.Count) && (AggregatedCoins(i).CurrencyCode == currentCurrencyCode));
			out += "<tr><td colspan=3></td></tr>";
			out += "<tr class='coinTotal'><td><b>"+getMessage("encashmentCoinAcceptor.total")+" " + (isDefined(currentCurrencyCode) && currentCurrencyCode!='undefined' ? " " + currentCurrencyCode : " ???") + "</b></td><td align=right><b>" + TotalCount + "</b></td><td align=right><b>" + TotalAmount + "</b></td></tr>";
			out += "<tr><td colspan=3></td></tr>";
		}
		out += "<tr class='coinTableFooter'><td><b>"+getMessage("encashmentCoinAcceptor.totalAll")+"</b></td><td align=right><b>" + TotalCountAll + "</b></td><td align=right><b> "+getMessage("encashmentCoinAcceptor.totalCount")+"</b></td></tr>";
		out += "</table>";
	} else {
		out += "<table><tr><td>";
		out += "<div class='redCaption'>"+getMessage("encashmentCoinAcceptor.noCoinsAccepted")+"</div>";
		out += "</td></tr></table>";
	}
	showAlert(getMessage("encashmentCoinAcceptor.encashmentCoinAcceptor") +
		"<br>" + getMessage("encashmentCoinAcceptor.device") + Framework.Variables("device").Name + " (" + Framework.Variables("device").DeviceId + 
		")<br><br>" +  out + "<br>" + (_.printerReady ? "" : "<span class='redText'>" + getMessage("encashmentCoinAcceptor.noReceipt") + "</span><br><br>") + 
		getMessage("encashmentCoinAcceptor.askEncashment"),
		{
			"yes" : function() { _.fireEvent('deviceEncashment') },
			"no" : _['skip']
		},
		{
			13 : "yes",
			27 : "no"
		},"no", 0
	)		
}

function updateButtons() {
	ViewButtons.update();
	if (_.InstalledCasseteNr && _.InstalledCasseteNr.length > 0) {
		document.getElementById("fieldComplete").className = '';
		document.getElementById("fieldComplete").onclick = onEntryComplete;
	} else {
		document.getElementById("fieldComplete").className = 'disabled';
		document.getElementById("fieldComplete").onclick = null;
	}
}

function isDefined(str) {
	return str && (str != null) && (str.length > 0);
}

function startEnterCasseteNr() {
	document.getElementById("status").style.display = "inline";
	document.getElementById("cassetteInput").className = "visible";
	document.getElementById("fieldName").className = "visible";
	document.getElementById("fieldValue").className = "visible";
	document.getElementById("fieldComplete").className = "disabled";	
	showKeypad();
	document.getElementById("keypadDiv").className = 'keypadDiv';
}

function appendChar(ch) {
	_.InstalledCasseteNr += ch;
	document.getElementById("fieldValue").innerHTML = _.InstalledCasseteNr;
	updateButtons();
}

function removeChar() {
	if (_.InstalledCasseteNr.length > 0) {
		_.InstalledCasseteNr = _.InstalledCasseteNr.substr(0, _.InstalledCasseteNr.length-1);
	}
	document.getElementById("fieldValue").innerHTML = _.InstalledCasseteNr;
	updateButtons();
}

function onEntryComplete() {
	if (_.enterCasseteNrMode && _.InstalledCasseteNr.length > 0) {
		document.getElementById("cassetteInput").className = "";
		hideKeypad();
		_['commit']();
	}
}

})();