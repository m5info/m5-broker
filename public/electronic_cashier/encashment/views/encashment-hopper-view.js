﻿(function() {

_.addEventListeners({
	"load" : function () {
		showAlert(getMessage("encashmentHopper.preparation") + "<br>" + getMessage("encashmentHopper.wait"), {}, {}, "", 0);
	
		_.registerVariables({
			"currentFieldValue" : "",
			"entryField" : ""
		});
		
		_.registerEvents(['processCassette', 'deviceEncashment', 'exchangeCassette']);
		
		_.addEventListeners({
			"askExchangeCassette" : function () {
				document.getElementById("status").style.display = "none";
				document.getElementById("add").style.display = "none";
				document.getElementById("entryState").className = "entryState";
				var info = getMessage("encashmentHopper.cassette") + (_.selectedCassette.number) + "<br><br>" + 
					"<table class='cassetteTable'>\
						<tr>\
							<td>" + getMessage("encashmentHopper.cassetteType") + "</td><td align='right'>" + getMessage("encashmentHopper." + _["getType"](_.selectedCassette.type)) + "</td>\
						</tr>\
						<tr>\
							<td>" + getMessage("encashmentHopper.bill") + "</td><td align='right'>" + _.selectedCassette.value/100 + " " + _.selectedCassette.currencyId + "</td>\
						</tr>\
						<tr><td>" + getMessage("encashmentHopper.toDispense") + "</td><td align='right'>" + _.selectedCassette.toDispenseCount + "</td></tr>\
					</table><br>" + 
					getMessage("encashmentHopper.askCassette");

				showAlert(info,
				{
					"yes" : function() { _.fireEvent("exchangeCassette"); },
					"no" : _['nextCassette']
				},
				{
					13 : "yes",
					27 : "no"
				},"no", 0
				);
			},
			"encashmentReady" : encashmentReady,
			"startEnterData" : function() { startEnterData("unitId"); }

		});
		document.getElementById("status").innerHTML = getMessage("encashmentHopper.encashmentBillDispenser") + "<br>" + getMessage("encashmentHopper.device") + Framework.Variables("device").Name + " (" + Framework.Variables("device").DeviceId + ")";		
		ViewButtons.bind({'cancelButton' : 'skip'});

	},
	"char" : function (ch) {
		if (_.entryField.length>0) {
			appendChar(ch)
			return false;
		}
		return true;
	},
	"key" : function(key) {
		switch(key) {
			// backspace
			case 8 :
				if (_.entryField.length>0) removeChar();
				return false;
			//esc
			case 27 : {
				_['skip']();
				return false;
			}
			case 13 :
				if (_.entryField.length>0) onEntryComplete();
				return false;
		}
		return true;
	},
	"run" : function() {
	}
});

function encashmentReady() {
	document.getElementById("entryState").className = "entryState";
	showAlert(getMessage("encashmentHopper.encashmentBillDispenser") + "<br>" + getMessage("encashmentHopper.device") + Framework.Variables("device").Name + " (" + Framework.Variables("device").DeviceId + ")" + "<br><br>" + (_.printerReady ? "" : "<span class='redText'>Печать квитанции невозможна<br>Устройство печати недоступно</span><br><br>") + getMessage("encashmentHopper.askEncashment"), 
		{
			"yes" : function () { _.fireEvent("deviceEncashment"); },
			"no" : _["skip"]
		},
		{
			13 : "yes",
			27 : "no"
		},"no", 0
	);
}

function updateButtons() {
	ViewButtons.update();
	
	if (_.entryField == "initialCount" && Number(_.currentFieldValue) < _.cassettes[_.currentCassette].minimum) {
		document.getElementById("minCountWarning").innerHTML = getMessage("encashmentHopper.minCountWarning") + _.cassettes[_.currentCassette].minimum + getMessage("encashmentHopper.pcs");
	} else {
		document.getElementById("minCountWarning").innerHTML = "";
	}
	if (_.currentFieldValue.toString().length > 0) {
		document.getElementById("fieldComplete").className = '';
		document.getElementById("fieldComplete").onclick = onEntryComplete;
	} else {
		document.getElementById("fieldComplete").className = 'disabled';
		document.getElementById("fieldComplete").onclick = null;
	}
}

function activateBills() {
	var out = "";
	if (BillsRegister != null) {
		out += "<table border=0 class=billTable>";
		out += "<tr><td ><b>"+getMessage("encashmentHopper.bill")+"</b></td>";
		out += "<td><b>"+getMessage("encashmentHopper.countShort")+"</b></td>";
		out += "<td><b>"+getMessage("encashmentHopper.summ")+"</b></td></tr>";
		out += "<tr><td colspan=3></td></tr>";
	
		var AggregatedBills = BillsRegister.Aggregate("currencyCode:group,sort;Value:group,sort,sum,count;");

		var TotalCountAll=0;
		var TotalAmountAll=0;

		for (var i=0; i<AggregatedBills.Count;) {
			var TotalCount = 0;
			var TotalAmount = 0;
			var currentCurrencyCode = AggregatedBills(i).currencyCode;
			do {
				TotalCountAll += AggregatedBills(i).Count;
				TotalAmountAll += AggregatedBills(i).Value;
				TotalCount += AggregatedBills(i).Count;
				TotalAmount += AggregatedBills(i).SumValue;
				out += "<tr><td align=right>" + AggregatedBills(i).Value + (isDefined(currentCurrencyCode) && currentCurrencyCode!='undefined' ? " " + currentCurrencyCode : " ???") + "</td><td align=right>" + AggregatedBills(i).Count + "</td><td align=right>" + AggregatedBills(i).SumValue + "</td></tr>";
				i++;
			} while ((i<AggregatedBills.Count) && (AggregatedBills(i).currencyCode == currentCurrencyCode));
			out += "<tr><td colspan=3></td></tr>";
			out += "<tr style='background:#c8c8c8'><td><b>"+getMessage("encashmentHopper.total")+" " + (isDefined(currentCurrencyCode) && currentCurrencyCode!='undefined' ? " " + currentCurrencyCode : " ???") + "</b></td><td align=right><b>" + TotalCount + "</b></td><td align=right><b>" + TotalAmount + "</b></td></tr>";
			out += "<tr><td colspan=3></td></tr>";
		}
		out += "<tr style='background:yellow'><td><b>"+getMessage("encashmentHopper.totalAll")+"</b></td><td align=right><b>" + TotalCountAll + "</b></td><td align=right><b> "+getMessage("encashmentHopper.totalCount")+"</b></td></tr>";
		out += "</table>";
	} else {
		out += "<table border=0 class='contentTable'><tr><td>";
		out += "<div class=\"redCaption\">"+getMessage("encashmentHopper.noBillsAccepted")+"</div>";
		out += "</td></tr></table>";
	}
}

function startEnterData(ef) {
	document.getElementById("entryState").className = "visible";
	_.currentFieldValue = _.selectedCassette[ef] ? _.selectedCassette[ef] : "";
	switch (ef) {
		case "unitId":
			document.getElementById("entry_unitId").className = "ActiveItem";
			document.getElementById("entry_currencyId").className = "InActiveItem";
			document.getElementById("entry_value").className = "InActiveItem";
			document.getElementById("entry_initialCount").className = "InActiveItem";
			break;
		case "currencyId":
			document.getElementById("entry_unitId").className = "ReadyItem";
			document.getElementById("entry_currencyId").className = "ActiveItem";
			document.getElementById("entry_value").className = "InActiveItem";
			document.getElementById("entry_initialCount").className = "InActiveItem";
			break;
		case "value":
			document.getElementById("entry_unitId").className = "ReadyItem";
			document.getElementById("entry_currencyId").className = "ReadyItem";
			document.getElementById("entry_value").className = "ActiveItem";
			document.getElementById("entry_initialCount").className = "InActiveItem";
			break;
		case "initialCount":
			document.getElementById("entry_unitId").className = "ReadyItem";
			document.getElementById("entry_currencyId").className = "ReadyItem";
			document.getElementById("entry_value").className = "ReadyItem";
			document.getElementById("entry_initialCount").className = "ActiveItem";
			break;
	}

	_.entryField = ef;
	document.getElementById("status").style.display = "inline";
	document.getElementById("add").style.display = "inline";
	document.getElementById("status").innerHTML = getMessage("encashmentHopper.exchangeCasssetteNr") + _.selectedCassette.number;
	document.getElementById("caption").innerHTML = getMessage("encashmentHopper.addCoins");
	document.getElementById("caption").onclick = function() { document.getElementById("addItems").checked = !document.getElementById("addItems").checked; };
	document.getElementById("fieldName").innerHTML = getMessage("encashmentHopper.enter_" + _.entryField);

	var selectValue = document.getElementById("selectValue");

	selectValue.onchange = function () {
		_.currentFieldValue = selectValue.value;
		updateButtons(); 
	}
	
	if (ef == "currencyId" || ef == "value") {
		hideKeypad();
		document.getElementById("fieldValue").className = "";
		selectValue.className = "visible";
		
		if (ef == "currencyId") {
			while (selectValue.options.length != 0) selectValue.options.remove(0);
			var availableCurrencies = getCurrency(null);
			for (var i = 0; i < availableCurrencies.length; i++) {
				var oOption = document.createElement("OPTION");
				selectValue.options.add(oOption);
				oOption.innerText = availableCurrencies[i].alphaCode;
				oOption.value = availableCurrencies[i].alphaCode;
				oOption.onchange = function() {  };
			}
			selectValue.value = _.currentFieldValue;
		}
		
		if (ef == "value") {
			while (selectValue.options.length != 0) selectValue.options.remove(0);
			var currency = getCurrency(_.selectedCassette.currencyId);
			for (var i = 0; i < currency.notes.length; i++) {
				var oOption = document.createElement("OPTION");
				selectValue.options.add(oOption);
				oOption.innerText = currency.notes[i]/currency.ratio;
				oOption.value = currency.notes[i];
			}
			selectValue.value = _.currentFieldValue;
		}
	} else {
		showKeypad();
		selectValue.className = "";
		document.getElementById("fieldValue").className = "visible";
		document.getElementById("fieldValue").innerHTML = _.currentFieldValue;
	}
	
	document.getElementById("cassetteInput").className = "visible";
	document.getElementById("keypadDiv").className = 'keypadDiv';
	updateButtons();
}

function appendChar(ch) {
	if (document.getElementById("fieldValue").style.display == "none") return;
	_.currentFieldValue += (Number(ch) ? ch : 0);
	document.getElementById("fieldValue").innerHTML = _.currentFieldValue;
	updateButtons();
}

function removeChar() {
	if (document.getElementById("fieldValue").style.display == "none") return;
	var s = _.currentFieldValue.toString();
	
	if (s.length > 0) {
		_.currentFieldValue = s.substr(0, s.length-1);
	}
	document.getElementById("fieldValue").innerHTML = _.currentFieldValue;
	updateButtons();
}

function onEntryComplete() {
	if (_.entryField.length > 0 && _.currentFieldValue.toString().length > 0) {
		document.getElementById("cassetteInput").className = "";
		hideKeypad();
		_.selectedCassette[_.entryField] = _.currentFieldValue;

		switch (_.entryField) {
			case "unitId" : 
				startEnterData("currencyId");
				break;
			case "currencyId":
				startEnterData("value");
				break;
			case "value":
				startEnterData("initialCount");
				break;
			case "initialCount":
				_.entryField = "";
				_.fireEvent("processCassette");
				break;
		}
	}
}
})();