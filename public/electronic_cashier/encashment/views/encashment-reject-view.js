﻿_.addEventListeners({
	"load" : function () {
		 _.addEventListeners({
			"noteAccepted" : onNoteAccepted
		 });
		document.getElementById("status").innerHTML = getMessage("encashmentReject.encashmentReject");
		ViewButtons.bind({ "continueButton" : "skip" });
	},
	"actionsChanged" : function () { ViewButtons.update(); },
	"key" : function(key) {
		switch(key) {
			// backspace
			case 8 :
				return false;
			//esc
			case 27 : {
				_['skip']();
				return false;
			}
			case 13 :
				_['commit']();
				return false;
		}
		return true;
	},
	"run" : function() {
		document.getElementById("entryState").className = "";
		showAlert( getMessage(getParameter("encashment.rejectClearOnly", false) ? "askClear" : "encashmentReject.askEncashment" ),
		{
			"yes" : getParameter("encashment.rejectClearOnly", false) ? showClearMessage : startEncashment,
			"no" : _["skip"]
		},
		{
			13 : "yes",
			27 : "no"
		},"no", 0
		);
	}
});

function showClearMessage() {
	showAlert(getMessage("encashmentReject.clearHint"),
	{ "ok" : _['commit'] },
	{
		13:"ok",
		27:"ok"
	},
	"ok",
	0
	);
}
function startEncashment() {
	document.getElementById("entryState").className = "visible";
	document.getElementById("billsTitle").innerHTML = getMessage("encashmentReject.rejectedNotesList");
	document.getElementById("hint").innerHTML = getMessage("encashmentReject.paymentDeviceHint");
	document.getElementById("note").innerHTML = getMessage("encashmentReject.bill");
	document.getElementById("currency").innerHTML = getMessage("encashmentReject.currency");
	document.getElementById("count").innerHTML = getMessage("encashmentReject.countShort");
	document.getElementById("summ").innerHTML = getMessage("encashmentReject.summ");
	document.getElementById("complete").onclick = _['commit'];
	enableBillAcceptor();
}

function onNoteAccepted(value, currency) {
	var billTable = document.getElementById("billTable");
	
	var rowId = "note_"+currency+"_"+value;
	var accpetedRow = document.getElementById(rowId);
	
	if (accpetedRow == null) {
		accpetedRow = billTable.insertRow();
		accpetedRow.id = rowId;
		
		var c;
		// номинал
		c = accpetedRow.insertCell();
		c.innerHTML = value;
		// валюта
		c = accpetedRow.insertCell();
		c.innerHTML = currency;
		// количество
		c = accpetedRow.insertCell();
		c.innerHTML = 1;
		// сумма
		c = accpetedRow.insertCell();
		c.innerHTML = value;
	} else {
		accpetedRow.cells[2].innerHTML = Number(accpetedRow.cells[2].innerHTML) + 1;
		accpetedRow.cells[3].innerHTML = value * Number(accpetedRow.cells[2].innerHTML);
	}
}