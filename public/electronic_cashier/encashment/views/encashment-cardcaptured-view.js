﻿(function () {
_.addEventListeners({
	"load" : function () {
		//document.getElementById("status").innerHTML = getMessage("encashmentCardCaptured.cardCaptured");
		ViewButtons.bind({ "continueButton" : "skip" });
	},
	"actionsChanged" : function () { ViewButtons.update(); },
	"key" : function(key) {
		switch(key) {
			// backspace
			case 8 :
				return false;
			//esc
			case 27 : {
				_['skip']();
				return false;
			}
			case 13 :
				_['commit']();
				return false;
		}
		return true;
	},
	"run" : function() {
		activatePayments();
	}
});

function activatePayments() {
	var out = "";

	if (_.cardsRegister != null && !_.cardsRegister.Closed) {
		out += "<br><div style=\"width:700px;overflow-x:hidden;overflow-y:auto;height:"+(_.printerReady ? "400px" : "300px")+"\" >"
		out += "<table border=0 aling='right' class=billTable>";
		out += "<tr><td><b>"+getMessage("encashmentCardCaptured.number")+"</b></td>";
		out += "<td><b>"+getMessage("encashmentCardCaptured.dateTime")+"</b></td>";
		out += "<td><b>"+getMessage("encashmentCardCaptured.cardNr")+"</b></td>";
		out += "<td><b>"+getMessage("encashmentCardCaptured.responseCode")+"</b></td></tr>";		

	
		var AggregatedCards = _.cardsRegister;//.Aggregate("Occured:group;CardNumber:group;ResponseCode");
		for (var i=0, c = AggregatedCards.Count; i<c; i++) {
			out += "<tr><td align=left>" + (i+1) + "</td><td align=right>" + AggregatedCards(i).Occured + "</td><td align=right>" + AggregatedCards(i).CardNumber + " " + "</td><td>"+AggregatedCards(i).ResponseCode+"</td></tr>";
		_.cardCount = (i+1);
		}

	//	out += "<tr style='background:yellow'><td align=left><b>"+getMessage("encashmentPayments.totalAll")+"</b></td><td align=right><b>" + 0 + "</b></td><td align=right><b>"+getMessage("encashmentPayments.payments")+"</b></td><td>тест</td></tr>";
		out += "</table></div>";

	} else {
			out += "<table border=0 class='contentTable'><tr><td>";
			out += "<div class=\"redCaption\">"+getMessage("encashmentCardCaptured.noCardCaptured")+"</div>";
			out += "</td></tr></table><br><br>";
	}
	
	var buttons = _.mayExecute("close") ? 
		{
			"print" : _['print'],
			"close" : _['close']
		} :
		{
			"next" : _['skip']
		};
		
	showAlert(getMessage("encashmentCardCaptured.cardCaptured")+"<br><br>"+out+(_.printerReady ? "" : "<br><span style='color:red'>"+getMessage("encashmentCardCaptured.noReceipt")+"</span><br>")+(!_.cardsRegister.Closed ? getMessage("encashmentCardCaptured.askEncashment") : ""),
		buttons,
		null , null, 0);
}
})()