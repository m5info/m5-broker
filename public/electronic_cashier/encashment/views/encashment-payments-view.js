﻿_.addEventListeners({
	"load" : function () {
		document.getElementById("status").innerHTML = getMessage("encashmentPayments.encashmentPayments");
		ViewButtons.bind({ "continueButton" : "skip" });
	},
	"actionsChanged" : function () { ViewButtons.update(); },
	"key" : function(key) {
		switch(key) {
			// backspace
			case 8 :
				return false;
			//esc
			case 27 : {
				_['skip']();
				return false;
			}
			case 13 :
				_['commit']();
				return false;
		}
		return true;
	},
	"run" : function() {
		activatePayments();
	}
});

function activatePayments() {
	var out = "";

	if (_.PaymentsRegister != null) {
		out += "<div class='paymentTableDiv'>"
		out += "<table class='paymentTable'>";
		out += "<tr class='paymentTableHeader'><td>"+getMessage("encashmentPayments.recipient")+"</td>";
		out += "<td>"+getMessage("encashmentPayments.countShort")+"</td>";
		out += "<td>"+getMessage("encashmentPayments.summ")+"</td></tr>";
		out += "<tr><td colspan=3></td></tr>";
	
		var TotalCount = 0;
		var TotalAmount = 0;

		var AggregatedPayments = _.PaymentsRegister.Aggregate("RecipientName:group,sort;Amount:sum,count;CurrencyCode:group;");
		for (var i=0; i<AggregatedPayments.Count;) {
			var currentCurrencyCode = AggregatedPayments(i).CurrencyCode;
			do {
				TotalCount += AggregatedPayments(i).Count;
				TotalAmount += AggregatedPayments(i).Amount;
				out += "<tr><td align=left>" + AggregatedPayments(i).RecipientName + "</td><td align=right>" + AggregatedPayments(i).Count + "</td><td align=right>" + Math.round(AggregatedPayments(i).Amount*100)/100 + " " + ((isDefined(AggregatedPayments(i).CurrencyCode) && AggregatedPayments(i).CurrencyCode!='undefined' ? " " + AggregatedPayments(i).CurrencyCode : " ???"))  + "</td></tr>";
				i++;			
			} while ((i<AggregatedPayments.Count) && (AggregatedPayments(i).CurrencyCode == currentCurrencyCode));
			
		}
		out += "<tr><td colspan=3></td></tr>";

		var TotalCountAll = 0;

		var AggregatedPayments = _.PaymentsRegister.Aggregate("Amount:sum,count;CurrencyCode:group;");
		for (var i=0; i<AggregatedPayments.Count;) {
		
			var TotalCount = 0;
			var TotalAmount = 0;
		
			var currentCurrencyCode = AggregatedPayments(i).CurrencyCode;
			do {
				TotalCount += AggregatedPayments(i).Count;
				TotalAmount += AggregatedPayments(i).Amount;
				out += "<tr class='paymentTotal'><td align=left><b>"+getMessage("encashmentPayments.total")+ " "
				+ (isDefined(AggregatedPayments(i).CurrencyCode) && AggregatedPayments(i).CurrencyCode!='undefined' ? " " + AggregatedPayments(i).CurrencyCode : " ???") 
				+ "</b></td><td align=right><b>" + AggregatedPayments(i).Count + "</b></td><td align=right><b>" 
				+ Math.round(AggregatedPayments(i).Amount*100)/100 + "</b></td></tr>";
				i++;			
			} while ((i<AggregatedPayments.Count) && (AggregatedPayments(i).CurrencyCode == currentCurrencyCode));
			TotalCountAll += TotalCount;
		}
		out += "<tr><td colspan=3></td></tr>";
		out += "<tr class='paymentTableFooter'><td align=left><b>"+getMessage("encashmentPayments.totalAll")+"</b></td><td align=right><b>" + TotalCountAll + "</b></td><td align=right><b>"+getMessage("encashmentPayments.payments")+"</b></td></tr>";
		out += "</table></div>";

	} else {
			out += "<table><tr><td>";
			out += "<div class='redCaption'>"+getMessage("encashmentPayments.noPaymentsAccepted")+"</div>";
			out += "</td></tr></table>";
	}

	showAlert(getMessage("encashmentPayments.encashmentPayments")+"<br><br>"+out+(_.printerReady ? "" : "<br><span class='redText'>"+getMessage("encashmentPayments.noReceipt")+"</span><br>")+ "<br>" + getMessage("encashmentPayments.askEncashment"),
		{
			"yes" : _['commit'],
			"no" : _['skip']
		},
		{
			13 : "yes",
			27 : "no"
		},"no", 0);
}
