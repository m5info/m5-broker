﻿_.addEventListeners({
	"load" : function() {
		_.addEventListeners({
			"reportPrinting" : function() {
				showAlert(getMessage("encashmentZReport.waitPrint"), {}, {}, _['commit'], 10000);
			},
			"reportReady" : function () {
				showAlert(getMessage("encashmentZReport.takeReceipt"), {}, {}, null, 0);
			}
		});
	},
	"run" : function() { 
		if (!_.printerReady) {
		showAlert("<span class='redText'>" + getMessage("encashmentZReport.printerFailed") + "</span>",
			{
				"ok" : _['skip']
			},
			{
				13 : "ok",
				27 : "ok"
			},"ok", 0);
			return;
		}
		showAlert(getMessage("encashmentZReport.askPrintReport"),
			{
				"yes" : printZReport,
				"no" : _['skip']
			},
			{
				13 : "yes",
				27 : "no"
			},"no", 0);
		
	}
});
