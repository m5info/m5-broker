﻿(function() {
_.addEventListeners({
	"load" : function() {
	},
	"run" : function() { 
		showAlert(getMessage("encashmentBillDispenser.print") + "<br>" + getMessage("encashmentBillAcceptor.device")  + Framework.Variables("device").Name + " (" + Framework.Variables("device").DeviceId + ")" + "<br><br>" + getMessage("encashmentBillAcceptor.wait"), {}, {}, _['commit'], 5000);
	}
});
})();