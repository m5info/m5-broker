﻿/* Способы оплаты получателя */
/* Работает с текущим получателем*/
function getRecipientPaymentMethodCount() {
	var c = 0;

	if (canAcceptCardPayment()) c++;
	if (canAcceptCashPayment()) c++;
	if (canAcceptWalletPayment()) c++;
	return c;
}

/* Описание способа оплаты */
function paymentMethod(hint, image, state) {
	this.hint = hint;     // Наименование способа оплаты
	this.image = image;   // Изображение
	this.state = state;   // Состояние входа для этого метода оплаты
}

function getRecipientPaymentMethods() {
	var paymentMethods = new Array();
	if (canAcceptCardPayment()) {
		paymentMethods.push(new paymentMethod(getMessage("selectPaymentMethod.card"), "images\\card.gif", "acceptCardPayment"));
	}
	if (canAcceptCashPayment()) {
		paymentMethods.push(new paymentMethod(getMessage("selectPaymentMethod.cash"), "images\\cash.gif", "acceptCashPayment"));
	}
	if (canAcceptWalletPayment()) {
		paymentMethods.push(new paymentMethod(getMessage("selectPaymentMethod.wallet"), "images\\wallet.png", "acceptWalletPayment"));
	}
	return paymentMethods;
}

function canAcceptCardPayment() {
	if ((typeof(VirtuPOS) == "undefined") || VirtuPOS == null || VirtuPOS.Offline)
		return false;
	if (PaymentGateway.SelectedRecipient)
		return !PaymentGateway.SelectedRecipient.InCategory("DisableCardPayment");
	if (PaymentGateway.SelectedCatalogItem)
		return !PaymentGateway.SelectedCatalogItem.InCategory("DisableCardPayment");
	return false;
}
function canAcceptCashPayment() {
	if ((typeof(PaymentDevice) == "undefined") || PaymentDevice == null|| !PaymentDevice.DeviceReady)
		return false;
	if (PaymentGateway.SelectedRecipient)
		return !PaymentGateway.SelectedRecipient.InCategory("DisableCashPayment");
	if (PaymentGateway.SelectedCatalogItem)
		return !PaymentGateway.SelectedCatalogItem.InCategory("DisableCashPayment");
	return false;
}
function canAcceptWalletPayment() {
	if ((typeof(PaymentGateway) == "undefined") || PaymentGateway == null)
		return false;

	var canUseWallet = PaymentGateway.CustomerLoggedIn;
	
	if (!canUseWallet && PaymentGateway.SelectedRecipient && PaymentGateway.SelectedRecipient.Fields.GetFieldByID(18))
		canUseWallet = PaymentGateway.SelectedRecipient.Fields.GetFieldByID(18).length != 0 && PaymentGateway.CustomerBalance != 0;

	if (!canUseWallet)
		return false;
	if (PaymentGateway.SelectedRecipient)
		return !PaymentGateway.SelectedRecipient.InCategory("DisableWalletPayment");
	if (PaymentGateway.SelectedCatalogItem)
		return !PaymentGateway.SelectedCatalogItem.InCategory("DisableWalletPayment");
	return false;
}

/* Проверка у получателя полей для ввода */
function hasInputFieldsOnSelected() {
	var PGW = PaymentGateway;
	if (typeof(PGW) === "undefined")
		PGW = PaymentGateway;
	
	if ((typeof(PGW) === "undefined") || PGW == null)
		return false;
	if (!PGW.SelectedCatalogItem)
		return false;
		
	var selCatalogItemFields = PGW.SelectedCatalogItem.Fields;
	for (var i = selCatalogItemFields.Count-1; i >= 0; i--) {
		if (selCatalogItemFields.Item(i).IsInput)
			return true;
	}
	return false;
}					

/**/
function enumerateRecipientFields(fields, template, isInput) {
	var output = "";
	var field;
	for (var i = 0, c = fields.Count; i < c; i++) {
		var field = fields.Item(i);
		if (isInput && (!field.isInput || field.Visibility >= 2 || !field.IsDefined))
			continue;
		if (!isInput && (field.Visibility >= 2 || field.Value == undefined || field.Value == null))
			continue;	
		if (field.Value == "" && field.DisplayValue == "")
			continue;
			
		var f = {
			"fieldName" : field.Name,
			//"fieldID" : field.ID,
			"fieldValue" : (field.DisplayValue != undefined && field.DisplayValue != null)  ? field.DisplayValue : field.Value
		};
		
		output += template.replace(/\$\{([^}]+)\}/g,
			function (str, exp) {
			// alert(f[exp]);			
				// При некотором желании, можно проверить содержимое f[exp] и изменить отображение вышележащего div (если это портит отображение)
				return f.hasOwnProperty(exp) ? f[exp] : str;
			});
	}
	return output;
}

function generatePaymentInfo(fields, template, fieldTemplate, isInput) {

	var f = {
		"catalogItemName" : PaymentGateway.SelectedCatalogItem.Name,
		"catlogItemFields" : enumerateRecipientFields(fields, fieldTemplate, isInput)
	};
	
	var output = "";
	output += template.replace(/\$\{([^}]+)\}/g,
		function (str, exp) {
			// При некотором желании, можно проверить содержимое f[exp] и изменить отображение вышележащего div (если это портит отображение)
			return f.hasOwnProperty(exp) ? f[exp] : str;
		});
	return output;
}

function hasVisibleOutputFields(fields) {
	for (var i = 0, c = fields.Count; i < c; i++) {
		field = fields.Item(i);
		if (!field.IsInput && (field.Visibility < 2) && field.IsDefined)
			return true;
	}
	return false;
}


function returnCommissionRulesForCurrency(RecipientCurrency) {
	var withCommissions = false;  // узнаем есть ли комиссия 
	try {
		if (RecipientCurrency) {
			for (var i=0; i < RecipientCurrency.Commissions.Count; i++) {
					if ((RecipientCurrency.Commissions(i).Amount > 0) || (RecipientCurrency.Commissions(i).MinimalSum > 0)) {
							withCommissions = true;
							break;
					}
			}
		}
	} catch (e) {}
	if (!withCommissions) {
		// без комиссии   
		return getMessage("commission.noCommission");
	}
	// с комиссией
	if (RecipientCurrency.Commissions.Count == 0)
		return getMessage("commission.noCommission");

	var CommissionText =  "";

	for (var i = 0; i < RecipientCurrency.Commissions.Count; i ++) {
		var s = "";
		if (i == 0) {
			s = getMessage("commission.commissionIs");
		} else {
			s += getMessage("commission.commisionMoreThan");
		}
		if (RecipientCurrency.Commissions(i).IsAbsolute) {
			s += " " + getCurrency(RecipientCurrency.CurrencyCode).format(RecipientCurrency.Commissions(i).Amount, 'short');
		}
		else {
			s += " " + RecipientCurrency.Commissions(i).Amount + "%";
		}
		if (RecipientCurrency.Commissions(i).MinimalSum > 0)
			s += getMessage("commission.atLeast")+getCurrency(RecipientCurrency.CurrencyCode).format(RecipientCurrency.Commissions(i).MinimalSum, 'short');
		if (i+1 < RecipientCurrency.Commissions.Count)	

		s += "<br>";
		CommissionText += s.replace("${limitText}", getCurrency(RecipientCurrency.CurrencyCode).format(RecipientCurrency.Commissions(i).Limit, 'short'));
	}
	return CommissionText;
}

function returnCommissionRules(Recipient, curr) {
	if (typeof(curr) != undefined && curr != null && curr.length > 0) {
		for (var j=0; j< Recipient.Currencies.Count; j++) {
			if (Recipient.Currencies.Item(j).CurrencyCode == curr) {
				return [returnCommissionRulesForCurrency(Recipient.Currencies.Item(j))]
			} 	
		}	
		return [getMessage("commission.noCommission")];
	} else {
		var resultList = [];
		for (var j=0; j<Recipient.Currencies.Count; j++) {
			resultList.push(getMessage('currency.'+Recipient.Currencies.Item(j).CurrencyCode) + ":" + returnCommissionRulesForCurrency(Recipient.Currencies.Item(j)));
		}	
		return resultList;
	}	
}
/* Категории */
function inSelectSingleItemCategory(item) {
	if (item && item.InCategory("SelectSingleItem")) {
		return true;		
	} else {
		return false;
	}
}

function isCardTypeOf(type) {
	// Нет VirtuPOS нечего проверять
	if (!VirtuPOS || VirtuPOS.Offline) 
		return false;

	var paramValue = getParameter("card.cardTypes."+type);
	if (typeof(paramValue) === 'undefined') 
		return false;
	if (Object.prototype.toString.call(paramValue) !== "[object Array]") { // !Array
		paramValue = [paramValue];
	}

	var recipientID = VirtuPOS.RecipientID;
	for (var i = 0; i < paramValue.length; i++) {
		if (typeof(paramValue[i]) === 'string') {
			var range = paramValue[i].split('-');
			if (range.length == 2 && recipientID >= Number(range[0]) && recipientID <= Number(range[1]))
				return true;
			if (range.length == 1 && recipientID === Number(range[0])) return true;
		} else {
			if (Number(paramValue[i]) === recipientID) return true;
		}
	}
	return false;
};