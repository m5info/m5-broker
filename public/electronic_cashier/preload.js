﻿/**
Загрузка состояний

Переменные, которые могут быть определены:

var keyActions = {
	<keyCode> : null // Для блокировки клавиши
	<keyCode> : <функция> // Для обработки клавиши особым образом
}

keyCode -- Скан-код клавиши по версии хост-браузера, т.е. IE (см. также http://unixpapa.com/js/key.html)

// Переопределение скриптов реализации поведения и отображения и загрузка дополнительных модулей
// Если одно из свойств отсутствует, то используется стандартная реализация.
// Для параметров могут использоваться строки или массивы строк
var implementation = {
	"behaviour" : <Имя файла реализации поведения> или null для блокирования загрузки стандартного,
	"view" : <Имя файла отображения> или null для блокирования загрузки стандартного
	"requires" : <Имя файла дополнительного сервиса>
}

// Для обработки завершении загрузки документа и всех скриптов необходимо подключить
// обработчик события onLoad для объекта currentState. Обработчиков может быть несколько. Если обработчик возвращает false, то вызов следующих за ним не выполняется
// document.onload использовать НЕЛЬЗЯ
_.addEventListener('load', function() {
	// do something
});

_.addEventListener('unload', function() {
	// do something
});


Функции, которые следует реализовать в состоянии

// Обрабатывает конкретный печатный символ ch (тип строка), введенный с клавиатуры или другим способом
function processChar(ch);

*/

/* KBExternal */
var KBExternal = null;
var hasKeyboard = false;
if (window.external.Framework) {
	hasKeyboard = true;
	KBExternal = window.external;
} else {
	KBExternal = new ActiveXObject("KBFrameworkAlone.KBExternal");
}

var Framework = KBExternal.Framework ? KBExternal.Framework : {'Offline' : true};
var VirtuPOS = KBExternal.VirtuPOS ? KBExternal.VirtuPOS : {'Offline' : true};
var PaymentDevice = KBExternal.PaymentDevice ? KBExternal.PaymentDevice : {'Offline' : true};
var FiscalPrinter = KBExternal.FiscalPrinter ? KBExternal.FiscalPrinter : {'Offline' : true};
var ReceiptPrinter = KBExternal.ReceiptPrinter ? KBExternal.ReceiptPrinter : {'Offline' : true};
var FingerScaner = KBExternal.FingerScaner ? KBExternal.FingerScaner : {'Offline' : true};
// Какой-нибудь принтер

var Printer = FiscalPrinter;
if (!Printer || !Printer.DeviceReady) {
	Printer = ReceiptPrinter;
}

var PaymentGateway = KBExternal.PaymentGateway ? KBExternal.PaymentGateway : {'Offline' : true};
var BarcodeReader = KBExternal.BarcodeReader ? KBExternal.BarcodeReader : {'Offline' : true};
var EWM = KBExternal.EWM ? KBExternal.EWM : {'Offline' : true};

/**************************************************************/
function playSoundFunction(file, loop) {
	try {
		if (getParameter("paykiosk.playSoundButtons", false) && (file === "error.wav" || file === "click.wav")) {
			Framework.PlaySnd(file, loop);
			return;
		}
		
		if (getParameter("paykiosk.playSound", false)) { 
			Framework.PlaySnd(file, loop);
		}	
	} catch(e) {}	
}

var keyboardShow = false;

function isKeyboardPresent() {
	return hasKeyboard;
}

function isKeyboardShow() {
	return hasKeyboard ? Framework.IsKeyboardShow : keyboardShow;
}


function inCyrillicLayoutCategory(field) {
	if (!PaymentGateway.SelectedRecipient && !PaymentGateway.SelectedCatalogItem) return false;

	if (PaymentGateway.SelectedRecipient.InCategory("CyrillicLayout") 
	|| PaymentGateway.SelectedCatalogItem.InCategory("CyrillicLayout")
	|| (field && field.InCategory("CyrillicLayout"))) {
		return true;		
	} else {
		return false;
	}

}

function showKeyboard(field) {
	if (!hasKeyboard) {
		keyboardShow = true;
		return;
	}

	if (inCyrillicLayoutCategory(field)) {
		//cyrillic
		window.external.Framework.ActiveLocale = 0x0419;
	} else {
		//latin
		window.external.Framework.ActiveLocale = 0x0409;
	}	

	if (!Framework.IsKeyboardShow) {
		Framework.ShowKeyboard();
	}	
}

function hideKeyboard() {
	if (!hasKeyboard) {
		keyboardShow = false;
		return;
	}
	if (Framework.IsKeyboardShow) {
		Framework.ShowKeyboard();
	}	
}


// Привязка локальных методов к методам, загруженным не локально
	(function (source, methodNames) {
		var methodName,
			f = function(m) { this[m] = function() { return source[m].apply(this, arguments); }; };
		while (methodName = methodNames.shift()) {
			f(methodName);
		}
	})(parent, ['enumerateLocales', 'getActiveLocale', 'getMessage', 'getNumberName', 'setActiveLocale'
			, 'getApplicationState', 'setActiveApplication', 'getSelectedApplication', 'getAdditionalScripts' , 'getApplicationCSS'
			, 'getCurrency', 'CurrencyAmount'
			, 'getLogger'
			, 'getParameter'
			, 'navigateNextState'
			, 'template'
			, 'getTopWindow','getApplicationDocument'
			, 'Framework_IfExistsThen'
			]);

var preloadLogger = getLogger("preload");

// ================================ CurrentState =================================
function StateAction(actionGuard, action) {
	this.mayExecute = actionGuard;
	this.execute = action;
}

function CurrentState() {
	// идентификация состояния
	// this.name - название
	// this.type  - тип состояния
	// this.extension - id выбранного элемента какталога

	// URL страницы
	// Парметры в URL
	this.pageParameters = {};
	var args = location.search.substring(1).split('&');
	for (var i = 0; i < args.length; i++) {
		var valuePair = args[i].split('=');
		this.pageParameters[valuePair[0]] = unescape(valuePair[1]);
	}

	var extension = null;
	if (typeof (PaymentGateway) !== "undefined" && PaymentGateway != null && PaymentGateway.SelectedCatalogItem) {
		var extension = PaymentGateway.SelectedCatalogItem.ID;
		if (extension.length == 0)
			extension = PaymentGateway.SelectedCatalogItem.IsRoot ? "root" : null;
		if (this.pageParameters.hasOwnProperty("extension"))
			extension = this.pageParameters["extension"];
	}
	// Need state Type
	var stateType = null;
	if (this.pageParameters.hasOwnProperty("state")) {
		// Тип состояния через параметры в URL
		stateType = this.pageParameters["state"];
	}
	else {
		// Тип состояния через имя html
		var s = document.location.href;
		var idxBegin = s.lastIndexOf('/')+1;
		var idxEnd = s.lastIndexOf('.');
		s = s.substr(idxBegin, idxEnd-idxBegin);
		var idx = extension ? s.indexOf("-"+extension) : -1;
		if (idx > 0)
			stateType = s.substr(0, idx);
		else
			stateType = s;
	}

	this.name = stateType + (extension ? "-"+extension : "");
	this.type = stateType;
	this.extension = extension ? extension : "";

	this.screensRoot = location.href;
	if (this.screensRoot.indexOf("file:///") == 0) this.screensRoot = this.screensRoot.substr(8);
	this.screensRoot = this.screensRoot.substring(0, this.screensRoot.lastIndexOf("/"));
	this.applicationRoot = this.screensRoot.substring(0, this.screensRoot.lastIndexOf("/"));
	this.paykioskRoot = this.applicationRoot.substring(0, this.applicationRoot.lastIndexOf("/"));

	preloadLogger.info('Загружается состояние "' + this.name + '". Тип состояния: "' + this.type + '"');
	
}

CurrentState.prototype = {
	"listeners" : {
		"LOAD" : { "handlers" : [], "resetInactivityTimer" : false },
		"UNLOAD" : { "handlers" : [], "resetInactivityTimer" : false },
		"CHAR" : { "handlers" : [], "resetInactivityTimer" : true },
		"KEY" : { "handlers" : [], "resetInactivityTimer" : true },
		"ACTIONSCHANGED" : { "handlers" : [], "resetInactivityTimer" : false },
		"RUN" : { "handlers" : [], "resetInactivityTimer" : false },
		"INACTIVITYTIMER" : { "handlers" : [], "resetInactivityTimer" : false },
		// Ошибки устройств
		"BILLACCEPTORSTATUS" : { "handlers" : [], "resetInactivityTimer" : false },
		"BILLACCEPTORFAILURE" : { "handlers" : [], "resetInactivityTimer" : false },
		"PRINTERSTATUS" : { "handlers" : [], "resetInactivityTimer" : false },
		"PRINTERFAILURE" : { "handlers" : [], "resetInactivityTimer" : false },
		"BILLDISPENSERSTATUS" : { "handlers" : [], "resetInactivityTimer" : false },
		"BILLDISPENSERFAILURE" : { "handlers" : [], "resetInactivityTimer" : false },
		// События мониторинга
		"TERMINALBEFORESHUTDOWN" : { "handlers" : [], "resetInactivityTimer" : false },
		"TERMINALSHUTDOWN" : { "handlers" : [], "resetInactivityTimer" : false },
		"TERMINALBEFOREDISABLE" : { "handlers" : [], "resetInactivityTimer" : false },
		"TERMINALDISABLE" : { "handlers" : [], "resetInactivityTimer" : false },
		"TERMINALENABLE" : { "handlers" : [], "resetInactivityTimer" : false }
	},
	
	"mayExecuteActions" : {},
	
	// Создает дополнительные обработчики, которые будут использоваться при взаимодействии вида и контроллера
	"registerEvents" : function(events, resetInactivityTimer) {
		if (resetInactivityTimer === undefined)
			resetInactivityTimer = true;
		
		if (typeof events === "string")
			events = [events]; // Как одно событие
			
		if (Object.prototype.toString.call(events) === "[object Array]") {
			// События в массиве по именам, преобразоване в свойства объекта
			var eventsObject = {};
			for(var i = 0, c = events.length; i<c; i++)
				eventsObject[events[i]] = null;
			events = eventsObject;
		}

		// События как свойства объекта
		for(var eventName in events) {
			var eventNameUpper = eventName.toUpperCase(); 
			if (this.listeners.hasOwnProperty(eventNameUpper)) {
				_.getLogger().error('Событие "' + eventName + '" в текущем состоянии уже зарегистрировано');
				continue;
			}
			this.listeners[eventNameUpper] = { "handlers": events[eventName] ? [events[eventName]] : [], "resetInactivityTimer" : resetInactivityTimer };
		}
	},
	
	// Создает дополнительные переменные состояния, которые будут использоваться при взаимодействии вида и контроллера
	"registerVariables" : function(variables) {
		for (var i in variables) {
			if (!variables.hasOwnProperty(i)) continue;
			
			if (this.hasOwnProperty(i)) {
				_.getLogger().error('Переменная "' + i + '" в текущем состоянии уже зарегистрирована');
				continue;
			}
			
			this[i] = variables[i];
		}
	},
	
	// Создает действия в состоянии
	"registerActions" : function(actions) {
		for (var i in actions) {
			if (!actions.hasOwnProperty(i)) continue;
			
			if (this.hasOwnProperty(i)) {
				_.getLogger().error('Действие "' + i + '" в текущем состоянии уже зарегистрировано');
				continue;
			}
			
			if (typeof actions[i] === 'function')
				this.mayExecuteActions[i] = true;
			else
				this.mayExecuteActions[i] = actions[i].mayExecute;
			
			var that = this;
			(function(a) {
				that[i] = function() {
					if (typeof a === 'function')
						return a.apply(this, arguments);
					else if (typeof a.mayExecute === 'boolean')
						return a.execute.apply(this, arguments);
					else if (a.mayExecute())
						return false !== a.execute.apply(this, arguments);
					else
						return false;
				};
			})(actions[i]);
		}
		this.fireEvent('actionsChanged');
	},
	
	// подключает обработчик
	"addEventListener" : function(eventName, listener) {
		var e = eventName.toUpperCase();
		if (this.listeners.hasOwnProperty(e)) {
			var l = this.listeners[e].handlers;
			
			for (var i=0, c = l.length; i<c; i++)
				if (listener === l[i]) {
					this.getLogger().error('Невозможно добавить обработчик "' + listener + '", так как для события "' + eventName + '" он уже определен.');
					return false;
				}
			
			l.push(listener);
			return true;
		} else {
			this.getLogger().error('Невозможно добавить обработчик события "' + eventName + '", которого не существует');
			return false;
		}
	},

	// подключает обработчики в виде целого объекта
	"addEventListeners" : function(eventListeners) {
		for (var eventName in eventListeners) {
			if (!eventListeners.hasOwnProperty(eventName))
				continue;
			
			this.addEventListener(eventName, eventListeners[eventName]);
		}
	},

	// отключает обработчик
	"removeEventListener" : function(eventName, listener) {
		var e = eventName.toUpperCase();
		if (this.listeners.hasOwnProperty(e)) {
			var l = this.listeners[e].handlers;
			
			if (l.length === 0) {
				this.getLogger().error('Невозможно удалить обработчик, так как для события "' + eventName + '" не определено ни одного.');
				return false;
			}
			
			if (typeof listener !== 'undefined') {
				for (var i=0, c=l.length; i<c; i++) {
					if (listener === l[i]) {
						l.splice(i, 1);
						return true;
					}
				}
					
				this.getLogger().error('Невозможно удалить обработчик "' + listener + '", так как для события "' + eventName + '" он не определен.');
				return false;
			}
			l.pop();
			return true;
		} else {
			this.getLogger().error('Невозможно удалить обработчик события "' + eventName + '", которого не существует');
			return false;
		}
	},
	
	// Вызывает все обработчики указанного события
	"fireEvent" : function() {
		var e = arguments[0].toUpperCase();
		var args = Array.prototype.slice.call(arguments); args.shift();
		if (this.listeners.hasOwnProperty(e)) {
			var c = this.listeners[e].handlers.length;
			if (c !== 0) {
				while(c--) {
					if (false === this.listeners[e].handlers[c].apply(this, args)) {
						if (this.listeners[e].resetInactivityTimer)
							this.resetInactivityTimer();
						return false;
					}
				}
				if (this.listeners[e].resetInactivityTimer)
					this.resetInactivityTimer();
			} else {
				if (e !== "INACTIVITYTIMER") {
					this.getLogger().warn('В реализации состояния "' + document.location.href + '" не зарегистрирован обработчик события "' + arguments[0] + '"');
				}
			}
			/* Обрабатываем InactivityTimeout */
			if (e === "INACTIVITYTIMER") {
				/* Inactivity timer  если не отработали в state то выходим по InactivityTimeout*/
				this.leave("timeout");
				return true;
			}
			
			return true;
		} else {
			this.getLogger().error('Невозможно выполнить обработчики события "' + arguments[0] + '", которого не существует');
			return true;
		}
	},
	
	"mayExecute" : function(actionName) {
		var a = this.mayExecuteActions[actionName];
		
		if (typeof a === 'undefined') return false;
		else if (typeof a === 'boolean') return a;
		else return a();
	},
	
	"getLogger" : function() {
		if (typeof this.logger === 'undefined')
			this.logger = getLogger(this.type + (this.extension ? '-' + this.extension : ''));

		return this.logger;
	},
	
	"leave" : function(outcome) {
		this.getLogger().info('Выходим из реализации состояния "' + document.location.href + '" в выход "' + outcome + '"');	
		return navigateNextState(this, outcome);
	},
	
	// Перезагружает состояние
	"reload" : function() {
		var search = document.location.search;
		var url = document.location.href;
		url = url.substr(0, url.indexOf(search));

		search = '?';
		for (var p in this.pageParameters) {
			if (!this.pageParameters.hasOwnProperty(p)) continue; 
			if (!this.pageParameters[p]) continue;
			search += p+'='+this.pageParameters[p]+'&';
		}
		search = search.substr(0, search.length-1);
		this.getLogger().info('Перезагружаем состояние "' + document.location.href + '"');
		document.location.replace(url+search);
//		document.location.reload();
	},
	
	
	// суффикс расширения состояния
	"extension" : null,
	//
	"updateExtension" : function () {
		var oldExtension = this.extension;
		var extension = null;
		if (typeof (PaymentGateway) !== "undefined" && PaymentGateway != null && PaymentGateway.SelectedCatalogItem) {
			extension = PaymentGateway.SelectedCatalogItem.ID;
			if (extension.length == 0)
				extension = PaymentGateway.SelectedCatalogItem.IsRoot ? "root" : null;
		}
		this.extension = extension ? extension : oldExtension;
		this.pageParameters['extension'] = extension;
		if (this.extension !== oldExtension) {
			this.reload();
			return true;
		}
		return false;
	},
	// тип состояния
	"type" : null,
	// Путь к папке screens
	"screensRoot" : null,
	// Путь к папке приложения
	"applicationRoot" : null,
	// Путь к папке сценария
	"paykioskRoot" : null,

	// Inactivity Timer
	inactivityTimerId : null,
	//
	'lastError' : undefined,
	
	"resetInactivityTimer" : function (elapse) {
		if (typeof(elapse) === "undefined" || elapse == null) {
			elapse = getParameter("inactivityTimeouts."+_.name);
			if (typeof(elapse) === "undefined" || elapse == null) {
				elapse = getParameter("inactivityTimeouts."+_.type);
				if (typeof(elapse) === "undefined" || elapse == null || elapse < 1000)
					elapse = getParameter("paykiosk.inactivityTimeout", 60000);
			} else if (elapse < 1000)
						elapse = getParameter("paykiosk.inactivityTimeout", 60000);
		}
		
		if (this.inactivityTimerId) {
			clearTimeout(this.inactivityTimerId);
			this.inactivityTimerId = null;
		}
		_.inactivityTimerId = setTimeout(function() { _.fireEvent("inactivityTimer") }, elapse);
	},
	
	"attachSystemEvents" : function() {
		/* Сигнализация статусов устройств */
		if (PaymentDevice)
			Framework.AttachEvent(PaymentDevice, "OnCashDeviceStatusChanged", this.OnCashDeviceStatusChanged);
		if (ReceiptPrinter)
			Framework.AttachEvent(ReceiptPrinter, "OnPrinterEvent", this.onPrinterDeviceStatus);
		if (FiscalPrinter)
			Framework.AttachEvent(FiscalPrinter, "OnFiscalPrinterEvent", this.onPrinterDeviceStatus);
		if (EWM) {
			Framework.AttachEvent(EWM, "OnBeforeShutdown", this.onEWMBeforeShutdown);
			Framework.AttachEvent(EWM, "OnShutdown", this.onEWMShutdown);
			Framework.AttachEvent(EWM, "OnBeforeDisable", this.onEWMBeforeDisable);
			Framework.AttachEvent(EWM, "OnDisable", this.onEWMDisable);
			Framework.AttachEvent(EWM, "OnEnable", this.onEWMEnable);
		}
	},
	"refreshDeviceStatuses" : function() {
		// if (PaymentDevice) 
			// _.onPaymentDeviceStatus();
			
		_.onPrinterDeviceStatus();
	},
	//
	"OnCashDeviceStatusChanged" : function() {
		if (!_.fireEvent("billAcceptorStatus", newStatus, newSubStatus)) 
			return;
		
		var newStatus = 0;
		var newSubStatus = 0;

		for (var i=0; i<PaymentDevice.CashDevices.Count; i++) {
			if (PaymentDevice.CashDevices.Item(i).Type === 3) {
				newStatus = PaymentDevice.CashDevices.Item(i).Status.Status;
				newSubStatus = PaymentDevice.CashDevices.Item(i).Status.StatusEx;
				if (newStatus == 3 && newSubStatus == 21 /*dssError_StackerRemoved*/) {
					if (_.fireEvent("billAcceptorFailure", newStatus, newSubStatus)) {
						_.leave('billAcceptorCassette');
						return;
					}
				}
			}
		}	

	},
	"onPrinterDeviceStatus" : function() {
		var status = 4;
		var subsstatus = 8;
		if (Printer && Printer.Status) {
			status = Printer.Status.Status;
			subsstatus = Printer.Status.SubStatus;
		}
		if (!_.fireEvent("printerStatus", status, subsstatus)) 
			return;
		if (status == 4) {
			if (getParameter("paykiosk.printerRequired", false) && _.fireEvent("printerFailure")) {
				_.leave('printerFailure');
			}
		}
	},
	
	"onEWMBeforeShutdown" : function() {
		var canShutdown = _.fireEvent("terminalBeforeShutdown");
		if (canShutdown) {
			return true; 
		}
		return false;
	},
	"onEWMShutdown" : function() {
		_.fireEvent("terminalShutdown");
	},
	"onEWMBeforeDisable" : function() { 
		var canShutdown = _.fireEvent("terminalBeforeDisable");
		if (canShutdown) {
			return true; 
		}
		return false;
	},
	"onEWMDisable" : function() { 
		_.fireEvent("terminalDisable");
	},
	"onEWMEnable" : function() { 
		_.fireEvent("terminalEnable");
	}
}

var _ = new CurrentState();

// ===== Загрузка дополнительных скриптов и подготовка к запуску состояния ======
(function() {

	// Обработчики клавиатуры
	function onKeyPressHandler(e) {
		return _.fireEvent('char', String.fromCharCode(e.keyCode), typeof(e["pinKey"]) !== "undefined" ? true : false);
	}
	function onKeyDownHandler(e) {
		return _.fireEvent('key', e.keyCode, typeof(e["pinKey"]) !== "undefined" ? true : false);
	}

	function onKeyPressHandlerNew(e) {
		if (e.charCode)
			if (!_.fireEvent('char', String.fromCharCode(e.charCode), typeof(e["pinKey"]) !== "undefined" ? true : false)) {
				e.preventDefault();
				return false;
			}
		return true;
	}
	function onKeyDownHandlerNew(e) {
		if (!_.fireEvent('key', e.keyCode, typeof(e["pinKey"]) !== "undefined" ? true : false)) {
			e.preventDefault();
			return false;
		}
		return true;
	}

	function activateKeyboardHandlers(doc) {
		//TODO: В событии key не обрабатываются модификаторы, т.е. Ctrl+Space обработать не получится
		if (doc.attachEvent != undefined) {
			doc.attachEvent("onkeypress", onKeyPressHandler);
			doc.attachEvent("onkeydown", onKeyDownHandler);
		} else {
			doc.addEventListener("keypress", onKeyPressHandlerNew, false);
			doc.addEventListener("keydown", onKeyDownHandlerNew, false);
		}
	}
	function deactivateKeyboardHandlers(doc) {
		if (doc.detachEvent != undefined) {
			doc.detachEvent("onkeypress", onKeyPressHandler);
			doc.detachEvent("onkeydown", onKeyDownHandler);
		} else {
			doc.removeEventListener("keypress", onKeyPressHandlerNew, false);
			doc.removeEventListener("keydown", onKeyDownHandlerNew, false);
		}
	}

	// подключение таблицы стилей cssPath
	function appendCSS(cssPath) {
		var aCSShref = Framework_IfExistsThen(cssPath, "zzz", getTopWindow());
		if (aCSShref != "zzz") {
			var aCSS = document.createElement("link");
			aCSS.rel = "stylesheet";
			aCSS.type = "text/css";
			aCSS.href = aCSShref;
			document.getElementsByTagName("head")[0].appendChild(aCSS);
		}
}

	function documentLoaded() {

	 //alert(_.name);	
	
		try {
			if (Framework.Variables("onlineSignal") == 1 && _.name.indexOf("offline") > 0)
			_.fireEvent("terminalEnable");
		} catch (e) {
			Framework.Variables("onlineSignal") = 0;
		};
		
		preloadLogger.info('Документ и дополнительные скрипты загружены');
		
		/* +++ Обновление CSS */
	
		var currentApp = getSelectedApplication();
		var currentLoc = getActiveLocale();
			
		// подключение главной таблицы стилей <application>/screens/styles/<application>.css
		appendCSS(_.applicationRoot + "/screens/styles/" + currentApp +".css");
		// подключение локальной таблицы стилей <application>/screens/styles/<application>-<locale>.css
		appendCSS(_.applicationRoot + "/screens/styles/" + currentApp + "-" + currentLoc +".css");
		
		var applicationCSSs = getApplicationCSS();
		if (applicationCSSs) {
			if (typeof(applicationCSSs) === "string")
				applicationCSSs = [applicationCSSs];
			// Этот кусок ты тоже правильно написал
			for (var i = 0, c = applicationCSSs.length; i < c; i+=1) {
				// Базовая CSS
				appendCSS(_.applicationRoot + "/screens/styles/" + applicationCSSs[i]+".css");
				// Локализация CSS
				appendCSS(_.applicationRoot + "/screens/styles/" + applicationCSSs[i] + "-" + currentLoc +".css");
			}
		}
		/* --- Обновление CSS */
		/* Обновление класса Body*/

		document.body.className = document.body.className + ' ' + stateImplementation.inheritanceChain.join(' ') + ' ' + _.extension + ' ' + currentLoc;	
		
		_.addEventListeners({
			"terminalBeforeDisable" : function () {
				if (!(!VirtuPOS.Offline && VirtuPOS.IsCardPresent) &&
					!PaymentDevice.HasAccumulatedSum &&
					_.name.indexOf("encashment") < 0 &&
					_.name.indexOf("service-entry") < 0 &&
					_.name.indexOf("accept-payment") < 0 &&
					_.name.indexOf("card-eject") < 0
					) 
					return true
				else
					return false
			},
			
			"terminalDisable" : function() {
				_.leave('offline');
			},
			
			"terminalEnable" : function() {
				// Если киоск не находится в состояниях encashment или service-entry, условие будет выполняться
				if (_.name.indexOf("encashment")*_.name.indexOf("service-entry") > 0) {
					Framework.Variables("onlineSignal") = 0;
					_.leave('online');
				} else Framework.Variables("onlineSignal") = 1;
			}
		});		
		
		_.attachSystemEvents();

		// Если никто из обработчиков не запретил дальнейшей работы, то продолжаем загрузку состояния
		if (_.fireEvent('load')) {

			// Подключаем обработчики клавиатуры по умолчанию после активации состояния
			activateKeyboardHandlers(getTopWindow().document);
			activateKeyboardHandlers(document);
			document.attachEvent('onselectstart',function(){ return false;});			
			document.attachEvent('ondragstart',function(){ return false;});			
			
			// Подключение к обработке ввода с пин-пада
			if (!VirtuPOS.Offline) {
				Framework.AttachEvent(VirtuPOS, "OnKeyPressed", function(keyCode) {
					if ((keyCode >= 48) && (keyCode < 58))
						onKeyPressHandler({"keyCode" : keyCode, "pinKey" : true});
					else
						onKeyDownHandler({"keyCode" : keyCode, "pinKey" : true});
				});	
			}
			// Обновление статусов устройств
			_.refreshDeviceStatuses();
			// таймаут
			_.resetInactivityTimer();
			
			_.fireEvent('run');
			
			var sound = getParameter("sounds."+_.name);
			if (typeof(sound) === "undefined" || sound == null) {
				sound = getParameter("sounds."+_.type);
			}
			if (typeof(sound) === 'string' && sound.indexOf(".wav") > 0)
				playSoundFunction(sound, false);
			
		}
		
		if (!hideBanner) loadBanner(true);
		
	}

	var scriptElements = document.getElementsByTagName('script');
	
	var requiredScripts = ['alert.js', 'recipient-object.js', 'view.js'];
		
	// добавляем скрипты приложения
	if (getAdditionalScripts()) {
		var currentApp = getSelectedApplication();	
		var appScripts = getAdditionalScripts();
		if (Object.prototype.toString.call(appScripts) !== "[object Array]") {
			if (typeof(appScripts) === 'string')
				appScripts = [appScripts];
			else
				appScripts = null;
		}
		if (appScripts) {
			for (var i = 0; i < appScripts.length; i++)
				requiredScripts.push(appScripts[i]);
		}
	}
	var stateImplementation = getApplicationState(_.type, _.extension);
	
	if (stateImplementation.view && stateImplementation.view.join) {
		for (var i = 0, c = stateImplementation.view.length; i < c; i++)
			requiredScripts.push(stateImplementation.view[i]);
	}
	else if (stateImplementation.view)
		requiredScripts.push(stateImplementation.view);
		
	if (stateImplementation.behaviour.join) {
		for (var i = 0, c = stateImplementation.behaviour.length; i < c; i++)
			requiredScripts.push(stateImplementation.behaviour[i]);
	}
	else
		requiredScripts.push(stateImplementation.behaviour);


	if (stateImplementation != null && stateImplementation.hasOwnProperty("requires") && stateImplementation.requires) {
		preloadLogger.debug('Перед активацией состояния будут загружены дополнительные библиотеки');
		
		var requires = stateImplementation.requires;
		if (Object.prototype.toString.call(requires) !== "[object Array]")
			requires = [requires];
		
		for (var j = 0, cc = requires.length; j < cc; j++) {
			var additionalScript = requires[j];
			for (var i = 0, c = requiredScripts.length; i < c; i++) {
				if (requiredScripts[i] === additionalScript) {
					preloadLogger.error('Скрипт "' + additionalScript + '" загружается автоматически и не должен включаться в implementation.requires');
					break;
				}
			}
			if (i == c)
				requiredScripts.push(additionalScript);
		};
	}

	var scriptsReady = false;
	var pageReady = false;
	
	function onPageReady() {
		if (scriptsReady)
			documentLoaded();
			
		pageReady = true;
	}
	
	function onPageUnload() {
		Framework.DetachEvents();
		_.fireEvent('unload');
		// Отключаем обработку клавиатуры
		deactivateKeyboardHandlers(getTopWindow().document);
		deactivateKeyboardHandlers(document);
		// выключаем таймер
		if (this.inactivityTimerId) {
			clearTimeout(this.inactivityTimerId);
			this.inactivityTimerId = null;
		}
	}
	
	if (window.addEventListener) {
		window.addEventListener('load', onPageReady, false);
		window.addEventListener('unload', onPageUnload, false);
	} else {
		window.attachEvent('onload', onPageReady);
		window.attachEvent('onunload', onPageUnload);
	}
	
	function appendScript() {
	
		var scriptFile = requiredScripts.shift();
		if (scriptFile != null) {
			//TODO: Обработать ситуацию с несуществующими скриптами. Это фатально для Firefox
			var scriptElement = document.createElement('script');
		
			scriptElement.language = 'javascript';
			scriptElement.type = 'text/javascript';
			//if (scriptFile.indexOf("terminal") >= 0)
			//	alert (scriptFile+ " == "+(_.applicationRoot+"/scripts/"+scriptFile)+" == "+_.paykioskRoot+"/"+scriptFile);
			if (scriptFile.indexOf('/') < 0)
				scriptElement.src = Framework_IfExistsThen(_.applicationRoot+"/scripts/"+scriptFile, _.paykioskRoot+"/"+scriptFile, getTopWindow());
			else
				scriptElement.src = scriptFile;

			preloadLogger.debug('Загружается скрипт: "' + scriptElement.src + '"');
			
			if (scriptElement.addEventListener) {
				scriptElement.addEventListener('load', appendScript, false);
				scriptElement.addEventListener('error', appendScript, false);
			} else
				scriptElement.attachEvent('onreadystatechange', function() {
					if (scriptElement.readyState.match(/loaded|complete/)) {
						appendScript();
					}
				});
				
			document.getElementsByTagName('head')[0].appendChild(scriptElement);
		} else {
			if (pageReady)
				documentLoaded();
				
			scriptsReady = true;
		}
	};
	
	appendScript();
})();

function formatDateTime(dateTime, mask) {
	var dateTime = new Date(dateTime);
	var dt = {
		'dd' : dateTime.getDate(),
		'm'  : dateTime.getMonth() < 8 ? "0"+(dateTime.getMonth()+1) : (dateTime.getMonth()+1),
		'y'  : dateTime.getYear() < 1000 ? dateTime.getYear()+1900 : dateTime.getYear(),
		'h'  : dateTime.getHours() < 10 ? "0" + dateTime.getHours() : dateTime.getHours(), 
		'mm' : dateTime.getMinutes() < 10 ? "0" + dateTime.getMinutes() : dateTime.getMinutes(),
		'ss' : dateTime.getSeconds() < 10 ? "0" + dateTime.getSeconds() : dateTime.getSeconds()
	}
	if (mask)
		return mask.replace("dd", dt.dd).replace("mm", dt.m).replace("yyyy", dt.y).replace("HH", dt.h).replace("MM", dt.mm).replace("SS", dt.ss);
	else 	
		return dt.dd + '.' + dt.m + '.' + dt.y + ' ' + dt.h + ':' + dt.mm + ':' + dt.ss; 
}

// TODO isDefined
function isDefined(a) {
	return typeof a != "undefined" && a != null && a.length>0;
}

function HtmlEncode(s) {
	var el = document.createElement("div");
	el.innerText = el.textContent = s;
	s = el.innerHTML;
	return s;
}

function HtmlDecode(s) {
	var el = document.createElement("div");
	el.innerHTML = s;
	s = el.innerText;
	return s;
}
