﻿// ================================ Currency =================================
// http://en.wikipedia.org/wiki/List_of_circulating_currencies
/*
major currency unit
minor currency unit
*/
function Currency(alphaCode, numericCode, ratio, notes, coins) {
	this.ratio = ratio;
	this.alphaCode = alphaCode;
	this.numericCode = Number(numericCode);
	this.notes = notes;
	this.coins = coins;
}

Currency.prototype = {
	"alphaCode" : null,
	"numericCode" : null,

	"getMinorAmount" : function(amount) {
		return Math.round(Number(amount) * this.ratio);
	},
	
	"getMoney" : function(amount) {
		var amountMinor = this.getMinorAmount(amount);
		return {
			"basic" : Math.floor(amountMinor / this.ratio),
			"fractional" : amountMinor % this.ratio
		}
	},
	
	"format" : function(amount, style) {
		var amountMoney = this.getMoney(amount);
		//TODO: Ratio не равный 100 и вообще не кратный 10 (так тоже бывает)
		var formattedFractional = (amountMoney.fractional < 10 ? '0' : '') + amountMoney.fractional;
		
		if (!style) style = this.styles['__default__'];
		
		var styleFunc = this.styles[style];
		while (styleFunc && (typeof styleFunc !== 'function'))
			styleFunc = this.styles[styleFunc];
			
		if (styleFunc) return styleFunc(amountMoney.basic, formattedFractional, this);
		else
			return basic + '.' + fractional;
	},
	
	// Возвращает положительное значение, если a1 больше a2, отрицательное значение, если a2 больше a1 и 0, если они равны
	"compare" : function(a1, a2) {
		return Math.floor(a1 * this.ratio) - Math.floor(a2 * this.ratio);
	},
	
	// Можно делать функции или ссылки на другие стили (аналогично __default__)
	"styles" : 	{
		"full" : function(basic, fractional, currency) {
				return [basic,
						getNumberName(basic, 'currency.' + currency.alphaCode, 'MajorUnit'),
						fractional,
						getNumberName(fractional, 'currency.' + currency.alphaCode, 'MinorUnit')].join(' ');
			},
			
		"majorOnlyFull" : function(basic, fractional, currency) {
				return [basic,
						getNumberName(basic, 'currency.' + currency.alphaCode, 'MajorUnit')].join(' ');
			},
			
		"printer" : function(basic, fractional, currency) {
			return [basic + '.' + fractional,
				getNumberName(basic, 'currency.' + currency.alphaCode, 'MajorUnitShort')].join(' ');
			},
			
		"short" : function(basic, fractional, currency) {
				return [basic,
						getNumberName(basic, 'currency.' + currency.alphaCode, 'MajorUnitShort'),
						fractional,
						getNumberName(fractional, 'currency.' + currency.alphaCode, 'MinorUnitShort')].join(' ');
			},

		// 1 р. 00 к.
		"sign" : function(basic, fractional, currency) {
		
				var result = groupDigits(basic, 'currency.' + currency.alphaCode, 'MajorDigitGrouping') + " " + getNumberName(basic, 'currency.' + currency.alphaCode, 'MajorUnitSign') + 
				" " + fractional + " " + getNumberName(fractional, 'currency.' + currency.alphaCode, 'MinorUnitSign');
//alert (groupDigits(basic, 'currency.' + currency.alphaCode + 'MajorDigitSeparation'));
				return result;
			},
			
		"__default__" : "short"
	}
}
// Здесь хранятся валюты по числовым и буквенным кодам
var availableCurrencies = null;

function getCurrency(currencyCode) {
	if (!availableCurrencies || (typeof(currencyCode) !== "undefined" && currencyCode == null)) {
		var a = [
			new Currency("USD", 840, 100, [100,500,1000,2000,5000,10000,200],[1,5,10,25,50,100]), // Доллары США (http://en.wikipedia.org/wiki/United_States_dollar)
			new Currency("EUR", 978, 100, [500,1000,2000,5000,10000,20000,50000], [1,2,5,10,20,50,100,200]), // Евро (http://en.wikipedia.org/wiki/Euro)
			new Currency("RUB", 643, 100, [5000,10000,50000,100000,500000,1000,500],[100,200,500,1000,1,5,10]), // Рубли РФ (http://en.wikipedia.org/wiki/Russian_ruble) 
			new Currency("GBP", 826, 100, [500,1000,2000,5000], [1,2,5,10,20,50,100,200]), // Английский фунт (http://en.wikipedia.org/wiki/Pound_sterling)
			new Currency("UZS", 860, 100, [100,300,500,1000,2500,5000,10000,20000,50000,100000], [1,5,10,25,50,100]), // Узбекские сумы (http://en.wikipedia.org/wiki/Uzbekistan_som)
			new Currency("TJS", 972, 100, [1,5,20,50, 100,300,500,1000,2000,5000, 10000, 20000, 50000], [5,10,20,25,50,100,300,500]), // Таджикский сомони (http://en.wikipedia.org/wiki/Tajikistani_somoni)
			new Currency("KGS", 417, 100, [2000,5000,10000,20000,50000,100000,500000], [100,300,500,1000]), // Киргизские сомы (http://en.wikipedia.org/wiki/Kyrgyzstani_som)
			new Currency("KZT", 398, 100, [20000,50000,100000,200000,500000,1000000], [100,200,500,1000,2000,5000,10000]), // Казахские тэнге (http://en.wikipedia.org/wiki/Kazakhstani_tenge)
			new Currency("AZN", 944, 100, [100,500,1000,2000,5000,10000], [1,3,5,10,20,50]), // Манаты Азербайджана (http://en.wikipedia.org/wiki/Azerbaijani_manat)
			new Currency("RUR", 810, 100, [5000,10000,50000,100000,500000,1000,500],[100,200,500,1000,1,5,10]), // Рубли РФ (до деноминации)
			new Currency("PLZ", 985, 100, [1000,2000,5000,10000,20000],[100,200,500,1000,2000,5000,1,5,10])
		];
		if (typeof(currencyCode) !== "undefined" && currencyCode == null)
			return a;
		
		availableCurrencies = {};
		for (var i = 0, c = a.length; i < c; i++) {
			availableCurrencies[a[i].alphaCode] = a[i];
			availableCurrencies[a[i].numericCode] = a[i];
		}
	};
	if (currencyCode)
		return availableCurrencies[currencyCode];
	else if ((typeof this.Recipient !== 'undefined') && this.Recipient && this.Recipient.CurrencyCode && (this.Recipient.CurrencyCode.length > 0))
		return availableCurrencies[this.Recipient.CurrencyCode];
	else if ((typeof PaymentGateway.SelectedRecipient !== 'undefined') && PaymentGateway.SelectedRecipient && PaymentGateway.SelectedRecipient.CurrencyCode && (PaymentGateway.SelectedRecipient.CurrencyCode.length > 0))
		return availableCurrencies[PaymentGateway.SelectedRecipient.CurrencyCode];
	else if ((typeof PaymentGateway.RecipientCollection !== 'undefined') && PaymentGateway.RecipientCollection && PaymentGateway.RecipientCollection.Count != 0)
		return availableCurrencies[PaymentGateway.RecipientCollection(0).CurrencyCode];

}

/* Объект для хранения какой-либо суммы в валюте */
function CurrencyAmount(value, currency) {
	this.value = value;
	this.currency = currency;
	this.format = function (style) { 
		return getCurrency(currency).format(this.value, style); 
	}
}