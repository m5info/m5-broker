﻿var applicationsScenario = {};
var scenarioLogger = getLogger('scenario');

function NavigationRule(condition, toView) {
	this.condition = condition;
	this.toView = toView;
}
NavigationRule.prototype.getView = function(scenario, currentState, targetState) {
	var state = null;
	if (typeof(this.toView) === "string") {
		state = this.toView;
	}
	else if (typeof(this.toView) === "function") {
		state = this.toView();
	}
	
	return scenario.urlFromState(currentState, state, targetState);
}

function ApplicationScenario(application, fromOutcomeRules) {
	this.application = application;
	this.fromOutcomeRules = fromOutcomeRules;

	// Возвращает null, если выполняется переход на другое приложение
	// или url если выполняется переход в другое состояние
	this.urlFromState = function(currentState, state, targetState) {
		var extension = PaymentGateway && PaymentGateway.SelectedCatalogItem ? PaymentGateway.SelectedCatalogItem.ID : "";
		if (PaymentGateway && PaymentGateway.SelectedCatalogItem && extension.length == 0 && PaymentGateway.SelectedCatalogItem.IsRoot)
			extension = "root";
		if (extension.length == 0)
			extension = null;

		// Выход из приложения (или перезапуск, если оно главное)
		if (state.indexOf('../') === 0) {
			setActiveApplication(state, currentState);
			return null;
		}
					
		var stateImplementation = getApplicationState(state, extension);
		//
		for (var p in stateImplementation) targetState[p] = stateImplementation[p];
		
		// Вход в приложение
		if (stateImplementation.application && (stateImplementation.application !== selectedApplication)) {
			setActiveApplication(stateImplementation.application, {"type" : state, "extension" : extension});
			return null;
		}
		
		return stateImplementation.screen;
	}
	
	//Возвращает undefined, если правило не сработало
	//Возвращает null при переходе на другое приложение (см. urlFromState)
	//url при переходе на другое состояние
	this.urlFromRule = function(rule, currentState, targetState) {
		if (!rule) return this.urlFromState(currentState, "../complete", targetState);
		if (typeof(rule) === "function") return this.urlFromState(currentState, rule(), targetState);
		if (typeof(rule) === "string") return this.urlFromState(currentState, rule, targetState);
		if (rule.hasOwnProperty('toView')) {
			//
			if (rule.hasOwnProperty('condition') && rule.condition)
				if (!rule.condition()) return;
			
			return this.urlFromRule(rule.toView, currentState, targetState);
		}
	}

	//Возвращает undefined, если никакое правило из группы не сработало
	//Возвращает null при переходе на другое приложение (см. urlFromState)
	//url при переходе на другое состояние
	this.processRules = function (currentState, rules, targetState) {
		if (rules && rules.hasOwnProperty('0')) {
			for (var i = 0, c = rules.length; i < c; i++) {
				var v = this.urlFromRule(rules[i], currentState, targetState);
				if (typeof(v) !== 'undefined') return v;
			}
		} else {
			return this.urlFromRule(rules, currentState, targetState);
		}
	}
	
	this.implicitStateRulesCache = {};
	
	this.hasImplicitStateRules = function(stateChain, targetState, extension) {
		if (!extension) return false;
		scenarioLogger.info("hasImplicitStateRules:"+targetState.stateType + "-"+extension);
		if (fromOutcomeRules.hasOwnProperty(targetState.stateType + "-"+extension)) {
			return true;
		}
		return false;
	}
	
	this.getNextStateView = function(currentState, outcome, stateChain, targetState) {
		var navigationResult;

		var nextState;
		var stateName;
		
		// Пробуем найти правила выходы
		for (var stateIdx = stateChain.length-1; stateIdx >= 0; stateIdx--) {
			// Type + Extension
			stateName = stateChain[stateIdx] + '-' + currentState.extension;
			
			if (fromOutcomeRules.hasOwnProperty(stateName)) {
				if (fromOutcomeRules[stateName].hasOwnProperty(outcome)) {
					nextState = this.processRules(currentState, fromOutcomeRules[stateName][outcome], targetState);
				}
			}
			if (typeof(nextState) !== 'undefined')
				return nextState;
			
			// Type + Only
			stateName = stateChain[stateIdx];
			if (fromOutcomeRules.hasOwnProperty(stateName)) {
				if (fromOutcomeRules[stateName].hasOwnProperty(outcome)) {
					nextState = this.processRules(currentState, fromOutcomeRules[stateName][outcome], targetState);
				}
			}
			if (typeof(nextState) !== 'undefined')
				return nextState;
		}
		/* Обработка общих для всех выходов. Сюда попадаем если выход не был найден ни в одном из состояний */
		if (fromOutcomeRules.hasOwnProperty("*")) {
			if (fromOutcomeRules["*"].hasOwnProperty(outcome)) {
				nextState = this.processRules(currentState, fromOutcomeRules["*"][outcome], targetState);
			}
		}
		
		if (typeof(nextState) !== 'undefined')
			return nextState;
			
		return;
	}
}

function navigateNextState(currentState, outcome) {

//alert("from "+currentState.name+" using outcome: "+outcome);

	var currentApplication = selectedApplication;

	var applicationScenario = applicationsScenario[currentApplication];
	var targetState = {};
	// Реализация текущего состояния
	var sourceState = getApplicationState(currentState.type, currentState.extension);
	var toState = applicationScenario.getNextStateView(currentState, outcome, sourceState.inheritanceChain, targetState);
	// Нового состояния не нашлось.
	if (typeof(toState) === 'undefined')
		return false;

	// Изменилось приложение, соответственно все переходы обработаны где-то еще
	if (!toState)
		return true;

	var extension = PaymentGateway && PaymentGateway.SelectedCatalogItem ? PaymentGateway.SelectedCatalogItem.ID : "";
	if (PaymentGateway && PaymentGateway.SelectedCatalogItem && extension.length == 0 && PaymentGateway.SelectedCatalogItem.IsRoot)
		extension = "root";
	if (extension.length == 0)
		extension = null;
	// Append parameters
	toState += "?state="+targetState.stateType+(extension != null ? "&extension="+extension : "");

	var fromState = applicationDocument.location.href;
	var fromStateWithoutParam = fromState.substr(0, fromState.indexOf('?'));
	if (fromStateWithoutParam.indexOf('file:///') != -1) fromStateWithoutParam = fromStateWithoutParam.substr(8); // remove  file:///
	var toStateWithoutParam = toState.substr(0, toState.indexOf('?'));

	var mustReload = 
		(toStateWithoutParam != fromStateWithoutParam) || 
		(sourceState.view != targetState.view) ||
		(sourceState.behaviour != targetState.behaviour) ||
		(sourceState.requires && targetState.requires && (sourceState.requires.length != targetState.requires.length)) ||
		applicationScenario.hasImplicitStateRules(sourceState.inheritanceChain, targetState, extension);
		
	if (!mustReload && sourceState.requires && targetState.requires) {
		for (var i = 0; !mustReload && i < sourceState.requires.length; i++)
			if (targetState.requires[i] != sourceState.requires[i])
				mustReload = true;
	}
	
	if (!mustReload) {
		scenarioLogger.info("Смена страницы '" + toState + "' не требуется");
		applicationDocument.document.body.className = targetState.inheritanceChain.join(' ') + ' ' + (extension != null ? extension : "") + ' ' + getActiveLocale();
		return true;
	}
	
	scenarioLogger.info('Переход на ' + toState);
	applicationDocument.location.href = toState;	
	return true;
}



function appendNavigationRules(additionalRules) {
	if (applicationsScenario.hasOwnProperty(activeApplication)) {
		for (var s in additionalRules) {
			// Добавляем новое состояние только, если оно не добавлено ранее для того, чтобы можно было переопределить состояние
			// в начальном файле приложения
			if (!additionalRules.hasOwnProperty(s) || applicationsScenario[activeApplication].fromOutcomeRules.hasOwnProperty(s))
				continue;
			applicationsScenario[activeApplication].fromOutcomeRules[s] = additionalRules[s];
		}
	} else {
		applicationsScenario[activeApplication] = new ApplicationScenario(activeApplication, additionalRules);
	}
}
