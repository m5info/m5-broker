var DADATA_TOKEN = "5f6f4a246bd9d39b1a60c5058e4aaa359bed9c04";//"8bf005d678fc776388757fb206902b63c128e1d5"; //2db5a8f85ea762d911f7bf65b7371a076a7aebae
var DADATA_AUTOCOMPLETE_URL = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs';


$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    startMainFunctions();

    loaderHide()

});

function clear_all_messages() {
    $('.span-x-notif').each(function (item, i) {
        i.click();
    });
    $('#clear_all_messages_wrapper').css('display', 'none');
    $('#_count_notification').html(0);

}

function startMainFunctions() {
    //динамическиое ограничение выбора даты
    let endYear = new Date().getFullYear() + 5;
    $(document)
        .on('focus', '.datepicker', function () {
            $(this).datepicker({
                dateFormat: 'dd.mm.yy',
                changeMonth: true,
                changeYear: true,
                yearRange: '1930:' + endYear
            });
        }).on('keyup', '.date', function () {
        $(this).mask('99.99.9999');
    })
        .on('focus', ".disable_keypress", function () {
            $(this).keypress(function (e) {

                e.preventDefault();
            });
        })
        .on('focus', ".datetimepicker", function () {
            $(this).bootstrapMaterialDatePicker({
                format: 'DD.MM.YYYY HH:mm',
                lang: 'ru',
                weekStart: 1,
                cancelText: 'Отмена',
            });

        })
        .on('change', '.percents_input_validation', function(){

            val = $(this).val().toString();
            val = val.replace(/ /g, '');
            val = val.split(',');

            if(parseInt(val[0]) === 100 && val[1] !== undefined && val[1] > 0){
                $(this).val(100);
            }
            if(val[0] > 100 && (val[1] === undefined || val[1] === '')){
                $(this).val(100);
            }
            if(parseInt(val[0]) > 100){
                $(this).val(100);
            }
        });

    $(document).on('focus', '.datepicker_start', function () {
        $(this).datepicker({
            dateFormat: 'dd.mm.yy',

            onSelect: function (date) {
                set_end_dates(date);
            },
            changeMonth: true,
            changeYear: true,
            yearRange: '2015:2030'

        });

    });

    $('.sum')
        .change(function () {
            $(this).val(CommaFormatted($(this).val()));
        })
        .blur(function () {
            $(this).val(CommaFormatted($(this).val()));
        })
        .keyup(function () {
            $(this).val(CommaFormatted($(this).val()));
        });

    $('.float').keypress(function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $('.phone').mask('+7 (999) 999-99-99');

    $('.price').mask('000 000 000.00', {reverse: true});

    $('.time').mask('99:99');

    $(document).on('change', '.ru_sumb', function() {
        $(this).removeClass('red_input');
        var regV = /[a-zA-z]/gi;

        var result = ($(this).val()).match(regV);

        if(result){
            alert('Используйте русские символы для ввода');
            $(this).addClass('red_input');
        }
    });

    $(document).on('change', '.en_sumb', function() {
        $(this).removeClass('red_input');
        var regV = /[а-яА-Я]/gi;

        var result = ($(this).val()).match(regV);

        if(result){
            alert('Используйте латиницу для ввода');
            $(this).addClass('red_input');
        }
    });


    $('[name="contract[object][foreign_reg_number]"]').on('change', function() {
        if($(this).prop('checked')){
            $('[name="contract[object][reg_number]"]').removeClass('ru_sumb');
        } else {
            $('[name="contract[object][reg_number]"]').addClass('ru_sumb');
        }
    });

    $('.vin_validation').on('change keyup', function() {
        $(this).removeClass('red_input');

        var resultLength = ($(this).val()).length;

        if(resultLength > 17){
            alert('VIN номер не может быть длиннее 17 символов');
            $(this).addClass('red_input');
        }
    });

    $('[name="contract[object][vin]"]').on('change keyup', function() {
        $(this).val($(this).val().toUpperCase());
    });

    $('[name="contract[object][reg_number]"]').on('change keyup', function() {
        $(this).val($(this).val().toUpperCase());
    });


    $('body').on('click', '.fancybox\\.iframe', function () {
        $.fancybox.open({
            type: 'iframe',
            href: $(this).attr('href'),
        });
    });

    $('.fancybox').fancybox();

    $('.fancybox-custom').fancybox({
        type: 'iframe',
        href: $(this).attr('href'),
        height: $(this).data('height'),
        width: $(this).data('width'),
        autoDimensions: false,
        autoSize: false
    });

    $('.fancybox-parent').click(function () {
        var href = $(this).attr('href');
        parent.$.fancybox({
            type: 'iframe',
            href: href,
            height: '360px',
            fitToView: false,
            autoSize: false
        });
        return false;
    });

    $('.numbers').keypress(function (event) {
        if (event.which == 8 || event.keyCode == 46)
            return true;
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    initSelect2();


    $(".party-autocomplete").suggestions({
        serviceUrl: DADATA_AUTOCOMPLETE_URL,
        token: DADATA_TOKEN,
        type: "PARTY",
        count: 5,
        onSelect: function (suggestion) {
            var data = suggestion.data;
            var subjectType = $(this).data('party-type');


            $('[data-name=' + subjectType + '_title]').val(suggestion.value);
            $('[data-name=' + subjectType + '_title_doc]').val(suggestion.unrestricted_value);
            $('[data-name=' + subjectType + '_inn]').val(data.inn);
            $('[data-name=' + subjectType + '_kpp]').val(data.kpp);
            $('[data-name=' + subjectType + '_ogrn]').val(data.ogrn);

            if (data.management && data.management.name) {
                $('[data-name=' + subjectType + '_general_manager]').val(data.management.name);
            }

            $('[data-name=' + subjectType + '_address]').val(data.address.value);

        }
    });


    $(document)
        .on('focus', '.fio-autocomplete', function () {
            $(this).suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "NAME",
                count: 5,
                onSelect: function (suggestion) {
                }
            });
        })

        .on('keyup', '.documentSeries', function () {
            $(this).val($(this).val().toUpperCase());
        })
        .on('keypress', '.foreignDocumentSeries', function (e) {
            var regex = new RegExp("^[a-zA-Z0-9]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }

            e.preventDefault();
            return false;
        });

    $(".address-autocomplete").suggestions({
        serviceUrl: DADATA_AUTOCOMPLETE_URL,
        token: DADATA_TOKEN,
        type: "ADDRESS",
        count: 5,
        onSelect: function (suggestion) {
            var data = suggestion.data;

            var subjectType = $(this).data('address-type');

            $('[data-name=' + subjectType + '_country]').val(data.country);
            $('[data-name=' + subjectType + '_city]').val(data.city);

            $('[data-name=' + subjectType + '_latitude]').val(data.geo_lat);
            $('[data-name=' + subjectType + '_longitude]').val(data.geo_lon);

            $('[data-name=' + subjectType + '_area_title]').val(data.city_district);
            $('[data-name=' + subjectType + '_district_text]').val(data.city_area);

            $(this).change();

        }
    });
}

function myGetAjax(urls) {
    var res = (function () {
        start_wait();

        var val = null;

        $.ajax({
            'async': false,
            'url': urls,
            'success': function (data) {
                val = data;
                end_wait();
            }
        }).always(function () {
            end_wait();
        });

        return val;
    })();

    return res;
}

function myPostAjax(urls, param) {

    var res = (function () {
        start_wait();

        var val = null;

        $.ajax({
            type: "POST",
            data: param + '&_token=' + $('meta[name="csrf-token"]').attr('content'),
            'async': false,
            'url': urls,
            'success': function (data) {
                val = data;

            }
        }).always(function () {
            end_wait();
        });

        return val;
    })();
    end_wait();
    return res;
}

function start_wait() {
    //$('#shadow').fadeIn(200);
}

function end_wait() {
    //$('#shadow').fadeOut(200);
}




function CommaFormatted(amount) {
    String.prototype.replaceAll = function (search, replace) {
        return this.split(search).join(replace);
    }

    var delimiter = " "; // replace comma if desired
    amount = new String(amount);
    amount = amount.replaceAll(' ', '');
    amount = amount.replaceAll('ю', ',');
    amount = amount.replaceAll('б', ',');
    amount = amount.replaceAll('.', ',');

    var state = 0;

    var sim = '';
    sim = ',';
    if (amount.search(",") > 0) {
        state = 1;
    }

    var a = amount.split(sim, 2);
    var d = a[1];

    var i = parseInt(a[0], 10);
    if (isNaN(i)) {
        return '';
    }
    var minus = '';
    if (i < 0) {
        minus = '-';
    }
    i = Math.abs(i);
    var n = new String(i);
    var a = [];
    while (n.length > 3) {
        var nn = n.substr(n.length - 3);
        a.unshift(nn);
        n = n.substr(0, n.length - 3);
    }
    if (n.length > 0) {
        a.unshift(n);
    }
    n = a.join(delimiter);
    //alert(d);
    if (d) {
        amount = n + ',' + d;
    } else {
        amount = n;
        if (state == 1) {
            amount += sim;
        }
    }

    // amount = n;
    amount = minus + amount;
    return amount;
}


/**************************/


//--------------------------------
//Следующий год
//--------------------------------

function a_get_next_year(curr, s_year) {
    original = ToUSADate(curr.value);
    if (original) original = strtotime(original);
    else return "";

    if (s_year) result = date('d.m.Y', strtotime('-1 days +' + s_year + ' years', original));
    else result = date('d.m.Y', strtotime('-1 days +1 years', original));
    return result;
}

//--------------------------------
//Конвертирование формата даты
//--------------------------------

function ToUSADate(curr_date) {
    curr_date = explode(' ', curr_date);
    curr_date[0] = explode('.', curr_date[0]);

    if (!curr_date[0][2]) return false;
    if (!is_numeric(curr_date[0][0])) return false;
    if (!is_numeric(curr_date[0][1])) return false;
    if (!is_numeric(curr_date[0][2])) return false;

    curr_date[0] = curr_date[0][2] + '-' + curr_date[0][1] + '-' + curr_date[0][0];
    curr_date = implode(' ', curr_date);

    return curr_date;
}


//--------------------------------
//Smart авто-подстановка
//--------------------------------

var _a_auto_array;

function a_auto(from, to) {
    if (typeof (a_auto_array) != 'object') a_auto_array = new Object();
    if (typeof (from) == 'object') from = from.value;
    to = $('#' + to);

    var may_change = true;
    if (from) {
        if (!to.val()) may_change = true;
        else if (a_auto_array[to.attr('id')] == to.val()) may_change = true;

        if (to.attr('readonly')) may_change = true;
    }

    if (may_change) {


        to.val(from);
        to.change();
        a_auto_array[to.attr('id')] = from;
    } else if (from) {

    }
}

function reload() {
    window.location.reload();
}

function openpage(url) {
    window.location = url;
}

function parent_reload() {
    window.parent.location.reload();
}

function format_price(price) {
    price = +price;
    return price.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1 ');
}

function StringToFloat(str) {
    String.prototype.replaceAll = function (search, replace) {
        return this.split(search).join(replace);
    }

    var delimiter = " "; // replace comma if desired
    amount = new String(str);
    amount = amount.replaceAll(' ', '');
    amount = amount.replaceAll('ю', ',');
    amount = amount.replaceAll('б', ',');
    amount = amount.replaceAll(',', '.');
    return amount;
}

function getFloatFormat(str) {
    str = str.replace(" ", "");
    str = str.replace(",", ".");
    return +str;
}


function set_end_dates(start_date) {
    var cur_date_tmp = start_date.split(".");

    var cur_date = new Date(cur_date_tmp[2], cur_date_tmp[1] - 1, cur_date_tmp[0]);

    var new_date = new Date(cur_date.setYear(cur_date.getFullYear() + 1));

    var new_date2 = new Date(new_date.setDate(new_date.getDate() - 1));

    $('.datepicker_end').val(getFormattedDate(new_date2));
}


function getFormattedDate(date) {
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return day + '.' + month + '.' + year;
}

function getSelectOptions(array, selectedValue) {
    return array.reduce(function (prev, item) {
        var selected = item.id == selectedValue ? 'selected' : '';
        return prev + "<option value='" + item.id + "' " + selected + ">" + item.title + "</option>";
    }, []);
}

function resetFilter() {
    window.location = window.location.href.split('?')[0];
}


function getPlanningTimeHous(km) {

    hour = Math.ceil(km / 70);
    hour_rest = Math.ceil(hour / 10) * 8;
    hour = (hour + hour_rest);

    return hour;
}

function getPlanningTimeHousToDay(hour) {
    temp = parseInt(hour / 24);
    if (temp > 0) {
        temp_hour = hour - parseInt(temp * 24);
        return temp + " д " + temp_hour + "ч";
    }
    return hour + " ч";
}


function exportString(type) {
    head = $('#export_head').html();
    data = $('#export_data').html();

    res = myPostAjax('/export/', "type=" + type + "&head=" + head + "&data=" + data);
    return res;
}


function exportXls() {
    str = exportString(1);
    exportSend(str, 1);
}

function exportCsv() {
    str = exportString(2);
    exportSend(str, 2);
}

function exportSend(str, type) {
    $('#export_type').val(type);
    $('#export_str').val(str);
    $("#export_file").submit();
}


function sendSecurityService(id, type) {
    res = myPostAjax('/security/create', "id=" + id + "&types=" + type);
    if (res) reload();
}

function sendUnderwritingService(id, type) {
    res = myPostAjax('/underwriting/create', "id=" + id + "&types=" + type);
    if (res) reload();
}


function makeSelectOptions(optionsArray, selectedId) {
    var options = '';
    $.map(optionsArray, function (item) {
        var selected = item.id == selectedId ? 'selected' : '';
        options += "<option value='" + item.id + "' " + selected + ">" + item.title + "</option>";
    });
    return options;
}

function openFancyBoxFrame(href) {
    $('.fancybox').fancybox(
        parent.$.fancybox({
            type: 'iframe',
            href: href,
            fitToView: false,
            autoSize: true
        })
    );
}

function openPage(href) {
    window.location = href;
}



function setSelectUsers(select_name, val, response) {

    var options = "<option value='0'>Не выбрано</option>";
    response.map(function (item) {
        options += "<option value='" + item.id + "'>" + item.name + "</option>";
    });
    $("select." + select_name).html(options).select2('val', val);

}


function reloadTab() {

    window.parent.jQuery.fancybox.close();
    if (window.parent.TAB_INDEX) {
        window.parent.selectTab(window.parent.TAB_INDEX);
    }

    if (window.parent.pageRelode) {
        window.parent.initReload();
    }


}

function validate() {

    var msg_arr = [];

    $('.validate').each(function () {
        val = ($(this).val()) ? $(this).val() : $(this).html();
        if (val.length < 1) {
            msg_arr.push('Заполните все поля');
            $(this).css("border-color", "red");
        }

    });

    if (msg_arr.length) {
        return false;
    }
    return true;

}


function deleteItem(url, id) {
    newCustomConfirm(function (result) {
        if (result){
            $.post(url + id, {
                _method: 'delete'
            }, function () {
                parent_reload();
            });
        }
    });
}


function openLogEvents(object_id, type_id, view_all) {

    urls = '/log/events/?object_id=' + object_id + '&type_id=' + type_id + '&view_all=' + view_all;
    //view_all 0 по объекту, 1 Видеть по родителю, 2 Видеть все
    if (window.parent) {
        window.parent.jQuery.fancybox.close();
        window.parent.openFancyBoxFrame(urls);
    } else {
        openFancyBoxFrame(urls);
    }


}


//ВЫНЕСТИ В ОТДЕЛЬНЫЙ ФАЙЛ
//ПОИСК БСО
function activSearchBso(object_id, key, type, bso_used = []) {

    $('#' + object_id + key).suggestions({
        serviceUrl: "/bso/actions/get_bso/",
        type: "PARTY",
        params: {
            type_bso: type,
            bso_supplier_id: $('#bso_supplier_id' + key).val(),
            bso_agent_id: $('#agent_id' + key).val(),
            bso_used: bso_used,
            from_url: window.location.pathname,
        },
        count: 5,
        minChars: 3,
        beforeRender: function(container, suggestions){
            container.find('.suggestions-unavailable').each(function(i, suggestion){
                var ts = $(suggestion).parent();
                ts.removeClass('suggestions-suggestion').addClass('suggestions-hint').addClass('suggestions-suggestions_disabled');
            });
        },
        formatResult: function (e, t, n, i) {
            var s = this;
            var title = n.value;
            var bso_type = n.data.bso_type;
            var bso_sk = n.data.bso_sk;
            var agent_name = n.data.agent_name;

            var view_res = title;

            if (n.data.isset_hold_kv == 0){ // если hold_kv не создан

                var view_res = '<span style="color:red">Не настроен продукт у страховщика</span>';

                // добавляем класс uggestions-unavailable чтоб сделать вариант некликабельным
                view_res += '<div class="suggestions-unavailable ' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">СК</span>' + bso_sk + "</div>";
                view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Тип</span>' + bso_type + "</div>";
                view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Агент</span>' + agent_name + "</div>";


            } else{
                view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">СК</span>' + bso_sk + "</div>";
                view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Тип</span>' + bso_type + "</div>";
                view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Агент</span>' + agent_name + "</div>";
            }

            return view_res;
        },
        onSelect: function (suggestion) {

            selectBso(object_id, key, type, suggestion);

        }
    });
}


function activSearchBsoSold(object_id, key, type, bso_used = [], state = 2, checkAlgo = false) {

    $('#' + object_id + key).suggestions({
        serviceUrl: "/bso/actions/get_bso_sold/",
        type: "PARTY",
        params: {
            type_bso: type,
            bso_supplier_id: $('#bso_supplier_id' + key).val(),
            bso_agent_id: $('#agent_id' + key).val(),
            bso_used: bso_used,
            state: state,
            checkAlgo: checkAlgo,
        },
        count: 5,
        minChars: 3,
        formatResult: function (e, t, n, i) {
            var s = this;
            var title = n.value;
            var bso_type = n.data.bso_type;
            var bso_sk = n.data.bso_sk;
            var agent_name = n.data.agent_name;

            var view_res = title;
            view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">СК</span>' + bso_sk + "</div>";
            view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Тип</span>' + bso_type + "</div>";
            view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Агент</span>' + agent_name + "</div>";

            return view_res;
        },
        onSelect: function (suggestion) {


            selectBso(object_id, key, type, suggestion);

        }
    });
}


function activSearchBsoOrders(object_id, key, type, order_id) {

    $('#' + object_id + key).suggestions({
        serviceUrl: "/bso/actions/get_bso_order/",
        type: "PARTY",
        params: {
            type_bso: type,
            bso_supplier_id: $('#bso_supplier_id' + key).val(),
            bso_agent_id: $('#agent_id' + key).val(),
            order_id: order_id
        },
        count: 5,
        minChars: 3,
        beforeRender: function(container, suggestions){
            container.find('.suggestions-unavailable').each(function(i, suggestion){
                var ts = $(suggestion).parent();
                ts.removeClass('suggestions-suggestion').addClass('suggestions-hint').addClass('suggestions-suggestions_disabled');
            });
        },
        formatResult: function (e, t, n, i) {
            var s = this;
            var title = n.value;
            var bso_type = n.data.bso_type;
            var bso_sk = n.data.bso_sk;
            var agent_name = n.data.agent_name;

            var view_res = title;

            if (n.data.isset_hold_kv == 0){ // если hold_kv не создан

                var view_res = '<span style="color:red">Не настроен продукт у страховщика</span>';

                // добавляем класс uggestions-unavailable чтоб сделать вариант некликабельным
                view_res += '<div class="suggestions-unavailable ' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">СК</span>' + bso_sk + "</div>";
                view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Тип</span>' + bso_type + "</div>";
                view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Агент</span>' + agent_name + "</div>";


            } else{
                view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">СК</span>' + bso_sk + "</div>";
                view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Тип</span>' + bso_type + "</div>";
                view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Агент</span>' + agent_name + "</div>";
            }

            return view_res;
        },
        onSelect: function (suggestion) {

            selectBso(object_id, key, type, suggestion);

        }
    });
}



function getInputsInstallmentAlgorithms(object, algorithm_id, key, contract_id) {

    $.post("/bso/actions/get_inputs_installment_algorithms/", {algorithm_id: algorithm_id, key: key, contract_id: contract_id}, function (response) {
        $("#" + object).html(response);
        $('.sum')
            .change(function () {
                $(this).val(CommaFormatted($(this).val()));
            })
            .blur(function () {
                $(this).val(CommaFormatted($(this).val()));
            })
            .keyup(function () {
                $(this).val(CommaFormatted($(this).val()));
            });
    });

    return true;
}

function getOptionInstallmentAlgorithms(object_id, insurance_companies_id, select_id) {

    $.getJSON("/bso/actions/get_installment_algorithms/", {insurance_companies_id: insurance_companies_id}, function (response) {
        var options = "<option value='0'>Не выбрано</option>";
        response.map(function (item) {
            atrr = '';

            if (parseInt(select_id) == item.id) {
                atrr = 'selected';
                $('#payment_algo').show();
            }

            options += "<option " + atrr + " value='" + item.id + "'>" + item.title + "</option>";
        });
        $("#" + object_id).html(options);
    });

}

function getOptionFinancialPolicy(object_id, insurance_companies_id, bso_supplier_id, product_id, select_id, sign_date = null) {

    $.getJSON("/bso/actions/get_financial_policy/", {
        insurance_companies_id: insurance_companies_id,
        bso_supplier_id: bso_supplier_id,
        product_id: product_id,
        sign_date: sign_date
    }, function (response) {
        var options = "<option value='0'>Не выбрано</option>";
        response.map(function (item) {
            atrr = '';
            if (parseInt(select_id) == item.id) atrr = 'selected';
            if(atrr == '' && parseInt(item.fix_price) == 1){
                atrr = 'selected';
            }

            // if (item.title.length > 85){
            //     options += "<option " + atrr + " value='" + item.id + "'>" + item.title.substring(0, 85)+"..." + "</option>";
            // } else{
            //     options += "<option " + atrr + " value='" + item.id + "'>" + item.title + "</option>";
            // }

            options += "<option " + atrr + " value='" + item.id + "'>" + item.title + "</option>";

        });
        $("#" + object_id).html(options);
        $("#" + object_id).change();
    });

}


function getOptionFinancialPolicyForParent(object_id, insurance_companies_id, bso_supplier_id, product_id, select_id, sign_date = null) {

    $.getJSON("/bso/actions/get_financial_policy/", {
        insurance_companies_id: insurance_companies_id,
        bso_supplier_id: bso_supplier_id,
        product_id: product_id,
        sign_date: sign_date
    }, function (response) {
        var options = "<option value='0'>Не выбрано</option>";
        response.map(function (item) {
            atrr = '';
            if (parseInt(select_id) == item.id) atrr = 'selected';
            if(atrr == '' && parseInt(item.fix_price) == 1){
                atrr = 'selected';
            }

            options += "<option " + atrr + " value='" + item.id + "'>" + item.title + "</option>";
        });

        window.parent.jQuery("#" + object_id).html(options);
        window.parent.jQuery("#" + object_id).change()
    });

}



// инпуты КВ разблокируются по параметрам из продукта
function getKvAccesses(key, product_id) {

    $.getJSON("/bso/actions/get_accesses_products/", {
        product_id: product_id
    }, function (response) {

        var official = $('#official_discount' + key);
        var informal = $('#informal_discount' + key);
        var bank = $('#bank_kv' + key);

        official.prop("disabled", true);
        informal.prop("disabled", true);
        bank.prop("disabled", true);

        official.removeClass('valid_fast_accept');
        informal.removeClass('valid_fast_accept');
        bank.removeClass('valid_fast_accept');

        if (response.kv_official_available == 1) {
            official.prop("disabled", false);
            official.addClass('valid_fast_accept');
        }
        if (response.kv_informal_available == 1) {
            informal.prop("disabled", false);
            informal.addClass('valid_fast_accept');
        }
        if (response.kv_bank_available == 1) {
            bank.prop("disabled", false);
            bank.addClass('valid_fast_accept');
        }
    });
}

//ПОИСК ЗАЯВКИ ИЗ ФРОНТ ОФИСА
function activSearchOrdersToFront(object_id, key, callback) {

    //здесь вызовем функцию для проверки наличия заяки и будем блочить менеджера
    block_manager_on_order(object_id, key);

    if (!(callback instanceof Function)) {
        callback = selectOrder
    }

    $('#' + object_id + key).suggestions({
        serviceUrl: "/bso/actions/get_orders_front/",
        type: "PARTY",
        count: 5,
        minChars: 3,
        timeout: 20000,
        noCache: true,
        onSelect: function (suggestion) {

            callback(object_id, key, suggestion);

        }
    });
}


function selectOrder(object_id, key, suggestion) {

    var data = suggestion.data;

    if (parseInt(data.id) > 0) {


        $('#' + object_id + key).val(data.title);
        $('#order_id' + key).val(data.id);
        $('#order_sort_id' + key).val(data.number_order);

        loaderShow();
        $.get("/bso/actions/get_order_id_front/", {order_id: data.id}, function (response) {
            loaderHide();

            if (response) {

                $('#' + object_id + key).css('borderColor', '#e0e0e0');

                if (response.insurer_phone[0] === "8") {
                    //response.insurer_phone = "+7" + response.insurer_phone.substring(1)
                }

                setNotNullValue('#sign_date' + key, response.sign_date);
                setNotNullValue('#begin_date' + key, response.begin_date);
                setNotNullValue('#end_date' + key, response.end_date);
                setNotNullValue('#delivery_date' + key, response.delivery_date);

                if (parseInt(response.is_personal_sales) == 1) {
                    $('#is_personal_sales' + key).prop('checked', true);
                } else {
                    $('#is_personal_sales' + key).prop('checked', false);
                }

                if (response.manager_name == ''){
                    $('#manager_id' + key).parent().show();
                    if ($('#manager_view_outer').parent().find('.manager_name_view').length != 0){
                        $('#manager_view_outer').parent().find('.manager_name_view').remove();
                    }
                } else{
                    $('#manager_id' + key).parent().hide();
                    $('#manager_id' + key).val(response.manager_id).change();
                    if ($('#manager_view_outer' + key).parent().find('.manager_name_view').length != 0){
                        $('#manager_view_outer' + key).parent().find('.manager_name_view').remove();
                    }
                    $('#manager_view_outer' + key).parent().append('<div class="manager_name_view wraper-inline-100">'+response.manager_name+'</div>');
                }

                setNotNullValue('#insurer_type' + key, response.insurer_type).change();
                setNotNullValue('#insurer_fio' + key, response.insurer);
                setNotNullValue('#insurer_title' + key, response.insurer);
                setNotNullValue('#insurer_phone' + key, response.insurer_phone);
                setNotNullValue('#insurer_add_phone' + key, response.insurer_add_phone);
                setNotNullValue('#insurer_email' + key, response.insurer_email);
                setNotNullValue('#delivery_address' + key, response.insurer_delivery_address);
                setNotNullValue('#insurer_underground' + key, response.insurer_underground);

                $('#payment_total' + key).val(CommaFormatted(response.payment_total)).change();

                setNotNullValue('#payment_data' + key, response.payment_data).change();

                setNotNullValue('#order_source' + key, response.source);

                setNotNullValue('#official_discount' + key, response.official_discount);
                setNotNullValue('#informal_discount' + key, response.informal_discount);
                setNotNullValue('#bank_kv' + key, response.bank_kv);

                if (!$('[name="contract['+key.replace('_','')+'][second_payment]"]').prop('checked')){
                    setNotNullValue('#payment_number' + key, response.payment_number);
                }

                setNotNullValue('#object_insurer_title' + key, response.object_insurer_title);


                if (response.object_insurer_auto) {

                    setNotNullValue('#object_insurer_car_year' + key, response.object_insurer_auto.car_year);
                    setNotNullValue('#object_insurer_ts_power' + key, response.object_insurer_auto.power);
                    setNotNullValue('#object_insurer_ts_reg_number' + key, response.object_insurer_auto.reg_number);
                    setNotNullValue('#object_insurer_ts_vin' + key, response.object_insurer_auto.vin);

                    $('#object_insurer_ts_mark_id' + key).select2('val', response.object_insurer_auto.mark_id);
                    String.prototype.replaceAll = function (search, replace) {
                        return this.split(search).join(replace);
                    };
                    var intkey = key;
                    intkey = new String(intkey).replaceAll('_', '');
                    secondPaymentToggle(intkey, false);
                    getModelsObjectInsurer(parseInt(intkey), response.object_insurer_auto.model_id);

                }

                /*
                if($('*').is('.load_files_front')){
                    $(".load_files_front").css('display', 'block');
                }
                */

            }


        }).done(function () {
            loaderShow();
        })
            .fail(function () {
                loaderHide();
            })
            .always(function () {
                loaderHide();
            });


    } else {
        $('#' + object_id + key).val('');
    }

}

function setNotNullValue(key, value){
    obj = $(key);

    if (value && value.toString().length > 0){
        obj.val(value.toString());
    }

    return obj;
}

function block_manager_on_order(object_id, key) {

    var user = getCurrentUserData();

    if ($('#' + object_id + key).val().length > 1 && user.role_id !== 1){ // фронт заявка забита и юзер не админ
        var manager = $('#manager_id' + key);
        var manager_title = manager.find('option[value="'+ manager.val() +'"]').text();

        $('#manager_id' + key).parent().hide();
        $('#manager_id' + key).parent().parent().find('.manager_name_view').remove();
        $('#manager_id' + key).parent().parent().append('<div class="manager_name_view wraper-inline-100">'+manager_title+'</div>');
    } else{
        $('#manager_id' + key).parent().show();
        $('#manager_view_outer' + key).parent().find('.manager_name_view').remove();
    }



}

function getCurrentUserData(){
    var user = null;

    $.ajax({
        url: '/users/getCurrentUserData',
        type: 'GET',
        async: false,
        success: function (response) {
            user = response;
        }
    });

    return user;
}


function build_query(params) {
    var esc = encodeURIComponent;
    return Object.keys(params).map(function (k) {
        return esc(k) + '=' + esc(params[k])
    }).join('&');
}


function flashMessage(type, msg, timeout = 4000) {
    var msg_block = $('<div class="alert alert-' + type + '"><button class="close" data-close="alert"></button><span>' + msg + '</span>\n</div>');
    setTimeout(function () {
        msg_block.animate({
            opacity: 0.1,
            //top: "-=250"
        }, 800, function () {
            $(this).remove();
        });
    }, timeout);
    $('#messages').append($(msg_block));

}


function flashValidationErrors(response_errors) {
    $.each(response_errors, function (k, v) {
        flashMessage('danger', v[0]);
    })
}


function setLastMonth() {
    var date = new Date();
    var todayDate = (date.getDate().toString().length > 1 ? date.getDate() : "0" + date.getDate()) + '.' + ((date.getMonth() + 1).toString().length > 1 ? date.getMonth() + 1 : "0" + (date.getMonth() + 1)) + '.' + date.getFullYear();
    date.setMonth(date.getMonth() - 1);
    var prevDate = (date.getDate().toString().length > 1 ? date.getDate() : "0" + date.getDate()) + '.' + ((date.getMonth() + 1).toString().length > 1 ? date.getMonth() + 1 : "0" + (date.getMonth() + 1)) + '.' + date.getFullYear();
    $('#begin_date').val(prevDate);
    $('#end_date').val(todayDate);
    $('#end_date').blur();
}

$('.collapse').on('show.bs.collapse', function () {
    $('.collapse.in').each(function () {
        $(this).collapse('hide');
    });
});

function initSelect2() {
    $('.select2').select2("destroy").select2({
        width: '100%',
        minimumInputLength: 3,
        dropdownCssClass: "bigdrop",
        dropdownAutoWidth: true
    });

    $('.select2-all').select2("destroy").select2({
        width: '100%',
        dropdownCssClass: "bigdrop",
        dropdownAutoWidth: true
    });

    $('.select2-ws').select2("destroy").select2({
        width: '100%',
        dropdownCssClass: "bigdrop",
        dropdownAutoWidth: true,
        minimumResultsForSearch: -1
    });

    $('.select2-all-full').select2("destroy").select2({
        width: '100%',
        dropdownCssClass: "bigdrop",
    });

    $('.select2-ws-min-3').select2("destroy").select2({
        width: '100%',
        dropdownCssClass: "bigdrop",
        dropdownAutoWidth: true,
        minimumResultsForSearch: -1,
        minimumInputLength: 3
    });
}


function deleteTempContract(contract_id)
{
    Swal.fire({
        title: 'Удалить договор и платежи?',
        text: "Вы действительно хотите удалить весь договор",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Да удалить!',
        cancelButtonText: 'Отмена'
    }).then((result) => {
        if (result.value) {
            if (myGetAjax("/contracts/temp_contracts/contract/"+contract_id+"/delete_contract")) {
                window.location = '/contracts/temp_contracts/';
            }
        }
    });
}

function sendNotifications(user_id, msg)
{
    if (myPostAjax("/account/send-notifications/?user_id="+user_id, "msg="+msg)) {
        return true;
    }
    return false;
}


function downloadFile(contract_id)
{
    window.location = "/contracts/actions/contract/"+contract_id+"/zip/";
}


/* ТАБЛИЦА */
var vue_table = false;

$(function () {

    if ($('#table_vue').length > 0) {
        vue_table = new Vue({
            el: '#table_vue',
            data: {
                message: 'Hello Vue!',
                windowHeight: window.innerHeight,
                windowOffset: $(document).scrollTop(),
                //max_table_height:  $('#table_vue table').height(),
                max_w: 0,
                table_offset: 0,
                table_offset_l: ($('#table').offset()).left,
                table_height: $('#table_vue').height(),
                table_width: $('#table').width(),
                custom_offset: 76,
                custom_height_val: 0,
                head_init: false,
                head_offset: 0,
                first_init: false,
            },

            methods: {

                getWindowHeight(event) {
                    this.windowOffset = $(document).scrollTop();
                    this.max_w = $('#table_vue #table table').width() + 1;
                    this.windowHeight = window.innerHeight;
                    this.table_offset = ($('#table').offset()).top;

                },

                getHeadOffset() {
                    if (this.head_offset >= 0) {
                        this.head_offset = this.windowOffset - this.table_offset + this.custom_offset;

                    }
                    if (this.head_offset < 0)
                        this.head_offset = 0;


                },
                initHead() {
                    /*копируем шапку*/
                    if (!this.head_init) {


                        //  $('#table').prepend("<div class='cloned_head_wrapper table  table-bordered bso_items_table'></div>");
                        /*каждый блок клонирует ширину + погрешность*/
                        $('#table_vue #table thead tr').each(function () {
                            $(this).children('th').each(function () {
                                $(this).css('width', $(this).width() + 1).css('min-width', $(this).width() + 1);
                            });

                        });
                        /*клонируем, скрываем, добавляем класс новой шапке*/
                        let new_head = ($('#table_vue thead')).first().clone();
                        $('#table_vue thead').css('visibility', 'hidden');
                        new_head.addClass('cloned_head');


                        $('.cloned_head_wrapper table').html(new_head);

                        /*запрещаем вставлять еще шапки*/
                        this.head_init = true;
                        var ol = this.table_offset_l;

                    }

                },
                resetHead() {
                    $('.cloned_head_wrapper table').html('');
                    this.head_init = false;

                },
            },
            beforeDestroy() {

                window.removeEventListener('scroll', this.getWindowHeight);
                window.removeEventListener('scroll', this.initHead);
                window.removeEventListener('scroll', this.getHeadOffset);
            },

            created() {

            },
            mounted() {
                this.$nextTick(function () {

                    window.addEventListener('scroll', this.getWindowHeight);
                    /*window.addEventListener('scroll', this.getTableHeight);*/
                    window.addEventListener('scroll', this.initHead);
                    window.addEventListener('scroll', this.getHeadOffset);

                    this.getWindowHeight();
                    this.getHeadOffset();

                    this.table_offset = (($('#table').offset()).top);

                    jQuery('.scrollbar-inner').scrollbar({"scrolly": "false"});
                    $('.scroll-bar').addClass('btn-primary');
                    $('.scroll-x').css('left', this.table_offset_l).css('width', this.table_width);
                    jQuery('.scrollbar-inner').scrollbar({"scrolly": "false"});

                });

            },
        });
    }
});


$(function () {

    $('.guide-frame-button, .guide-fuq-elem').on('click',function () {
        let id = $(this).data().id ? $(this).data().id: '';
        openFancyBoxFrame_customHeight('/informing/guide/show/'+id,600);
        return false;
    })



});