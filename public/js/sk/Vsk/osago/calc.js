/*
 Пример названия функции refresh_calculate_id_<dirname>_<slug>
 */

var Vsk_osago_ws = null;
var Vsk_osago_ws_session = null;
var sms_interval = null;
var sms_counter = 30;

function refresh_calculate_id_Vsk_osago(dir_name, slug, calculation_id) {

    Vsk_osago_ws = new ab.connect(
        protocol + '://' + pusher_uri + ':8081',
        function (session) {

            Vsk_osago_ws_session = session;

            $.get('/contracts/online/calculate_sk/' + calculation_id, {}, function (response) {

                /* Завершение web-socket, если ответ содежит ошибку */

                if (response.indexOf('Ошибки валидации :') !== -1){
                    Vsk_osago_ws_session.close();
                    $('#calculation_'+calculation_id).html(response);
                }
            });

            session.subscribe(dir_name + '_' + slug + '_' + calculation_id, function (topic, data, session) {


                $.get('/contracts/online/calculate_sk/' + calculation_id, {}, function (response) {
                    console.log(response.indexOf('Ошибки валидации :'));
                    if (response.indexOf('Ошибки валидации :') !== -1){

                        $('#calculation_'+calculation_id).html(response);
                        Vsk_osago_ws_session.close();

                    }else{

                        if (data.step === 'end_point'){
                            $('#calculation_'+calculation_id).html(response);
                            Vsk_osago_ws_session.close();
                        }
                    }
                });





            });

        },
        function (code, reason, detail) {
            console.warn('WebSocket connection closed: code=' + code + '; reason=' + reason + '; detail=' + detail);
        },
        {
            'maxRetries': 60,
            'retryDelay': 4000,
            'skipSubprotocolCheck': true
        }
    );



}

function confirmSMSCode(dir_name, slug, calculation_id){


    loaderShow();

    sms_counter = 10;
    $('#counter_sms').html(sms_counter);

    $('#sms_confirm').hide();
    $('#repeat_sms_send').attr('disabled', 'disabled');

    sms_interval = setInterval(function(){
        counter_sms = $('#counter_sms').html();
        sms_counter--;
        $('#counter_sms').html(sms_counter);
        if (sms_counter === 0){
            clearInterval(sms_interval);
            $('#repeat_sms_send').removeAttr('disabled');
        }
    }, 1000);


    /*$.post('/contracts/online/accept_save_policy_sk/'+calc, {}, function(){
        setTimeout(function(){
            $('#sms_verify').show();
            loaderHide();
        }, 700);
    });*/

    $('#sms_verify').show();
    loaderHide();


    Vsk_osago_ws = new ab.connect(
        protocol + '://' + pusher_uri + ':8081',
        function (session) {

            Vsk_osago_ws_session = session;

            $.post('/contracts/online/accept_save_policy_sk/' + calculation_id, {}, function (response) {
                if (response === false){
                    flashMessage('danger', 'Ошибка валидации. Вернитесь в расчет и пересчитайте.');
                }else{
                    flashMessage('success', 'Запрос отправлен, ожидайте СМС с кодом');
                }
            });

            session.subscribe(dir_name + '_' + slug + '_' + calculation_id, function (topic, data, session) {


                $.post('/contracts/online/accept_save_policy_sk/' + calculation_id, {}, function (response) {
                    console.log(response);
                    if (response === false){
                        flashMessage('danger', 'Что-то пошло не так, вернитесь на калькуляцию и пересчитайте полис');
                        Vsk_osago_ws_session.close();
                    }else{
                        if (data.step === 'end_point'){
                            Vsk_osago_ws_session.close();
                        }
                    }
                });
            });

        },
        function (code, reason, detail) {
            console.warn('WebSocket connection closed: code=' + code + '; reason=' + reason + '; detail=' + detail);
        },
        {
            'maxRetries': 60,
            'retryDelay': 4000,
            'skipSubprotocolCheck': true
        }
    );
}

function sendSMSCode(dir_name, slug, calculation_id, contract_id) {


    Vsk_osago_ws = new ab.connect(
        protocol + '://' + pusher_uri + ':8081',
        function (session) {

            Vsk_osago_ws_session = session;
            $.post("/contracts/online/"+contract_id+"/confirm_sms", {sms_code: $('[name="sms_code"]').val()}, function (res) {

                $('#sms_verify').addClass('hidden');
                $('#loader_div_1').removeClass('hidden');

            });

            session.subscribe(dir_name + '_' + slug + '_' + calculation_id, function (topic, data, session) {

                $.post("/contracts/online/"+contract_id+"/confirm_sms", {sms_code: $('[name="sms_code"]').val()}, function (res) {
                    
                    if (data.hasOwnProperty('error')){
                        flashMessage('danger', data.error);
                        $('#sms_verify').removeClass('hidden');
                        $('#loader_div_1').addClass('hidden');
                        Vsk_osago_ws_session.close();
                    }else{
                        if (data.response.policyNumber){
                            flashMessage('success', 'СМС код подтвержден!');
                            $('#sms_verify').removeClass('hidden');
                            $('#loader_div_1').addClass('hidden');
                            Vsk_osago_ws_session.close();
                        }else{
                            flashMessage('danger', 'Что-то пошло не так. Попробуйте снова.');
                            $('#sms_verify').removeClass('hidden');
                            $('#loader_div_1').addClass('hidden');
                            Vsk_osago_ws_session.close();
                        }
                    }

                });
            });

        },
        function (code, reason, detail) {
            console.warn('WebSocket connection closed: code=' + code + '; reason=' + reason + '; detail=' + detail);
        },
        {
            'maxRetries': 60,
            'retryDelay': 4000,
            'skipSubprotocolCheck': true
        }
    );

}
