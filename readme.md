## Установка
0) Выполняем команду

``` apt-get update ```

1) Установить докер https://docs.docker.com/engine/getstarted/step_one/ 

``` apt-get install docker ```

2) Установить докер-композ https://docs.docker.com/compose/

``` apt-get install docker-compose ```

3) Копируем файлы
```bash 
git clone git@gitlab.sst.cat:stage/m5_back.git
```
4) Если установка на боевом сервере, то задаем свои порты к контейнерам и пароли к бд и phpmyadmin в файлике
docker-compose.yml

5) Выполняем команду

```bash 
docker-compose up -d
```

После запуска:
http://127.0.0.1:8000 - скрипт
http://127.0.0.1:8080 - adminer

6) Заходим в adminer и выбираем в окошке тип подключения mysql

System	Server	== database
Username	    == root
Password	    == secret
Database	
создаем бд, называем ее как угодно и импортируем sql файл из корня dump
7) Создаем .env на основе .env.example. Редактируем .env и задаем параметры
8) в папке deploy выполняем команды из скрипта docker.sh

### Доп команды
1) * Сбросить кэш конфигов - в контейнере m5_app ``` php artisan key:generate && php artisan config:clear && php artisan config:cache ```  
2) Пересобрать приложение - ``` docker-compose up -d --force-recreate --build ```
3) * Применить миграции - в контейнере m5_app ``` php artisan migrate ```
4) * Создать новую миграцию - ``` php artisan make:migration new_migration ```

Если что-то не работает, то скорее всего проблема с правами на папках 
для изменения прав заходим внутрь докер контейнера 

```  docker exec -it m5_app /bin/bash ```

и выполняем команду
``` chmod 777 myfile ```
или 

``` chmod -R 777 mydirectory ```

* Команды выполняются внутри контейнера m5_app