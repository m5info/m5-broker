<?php

Route::group(['middleware' => 'guest', 'prefix' => 'electronic-cashier', 'namespace' => 'ElectronicCashier'], function () {
    Route::get('/', 'ElectronicCashierController@index');

});
Route::get('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/test/phpinfo', 'Search\SearchController@phpinfo');




    Route::group(['prefix' => 'search', 'namespace' => 'Search'], function () {
        Route::get('/', 'SearchController@index');
    });

    Route::group(['prefix' => 'account', 'namespace' => 'Account'], function(){
        Route::group(['prefix' => 'table_setting'], function(){
            Route::get('/{table_key}/edit/', 'TableSettingController@edit');
            Route::post('/{table_key}/save/', 'TableSettingController@save');
        });

        Route::post('/send-notifications/', 'ActionsController@sendNotifications');
    });

    Route::group(['prefix' => 'analitics', 'namespace' => 'Analitics'], function () {

        Route::group(['prefix' => 'delivery', 'namespace' => 'Delivery'], function () {
            Route::get('/', 'DeliveryAnaliticsController@index');
            Route::post('/', 'DeliveryAnaliticsController@get_table')->name('dilevery_analitics_get_table');
            Route::post('/details', 'DeliveryAnaliticsController@details');
            Route::post('get_fresh_worked_day', 'DeliveryAnaliticsController@getFreshWorkedDay')->name('get_fresh_worked_day');
            Route::post('set_zero_cost_delivery', 'DeliveryAnaliticsController@setZeroCostDelivery')->name('set_zero_cost_delivery');
            Route::post('delete_delivery', 'DeliveryAnaliticsController@deleteDelivery')->name('delete_delivery');
            Route::post('set_cost_delivery', 'DeliveryAnaliticsController@setCostDelivery')->name('set_cost_delivery');
            Route::get('new_delivery', 'DeliveryAnaliticsController@newDelivery')->name('new_delivery');
            Route::post('add_new_delivery', 'DeliveryAnaliticsController@AddNewDelivery')->name('add_new_delivery');
            Route::post('upload_file_delivery', 'DeliveryAnaliticsController@uploadFileDelivery')->name('upload_file_delivery');
            Route::get('get_all_doc_delivery', 'DeliveryAnaliticsController@getAllDocDelivery')->name('get_all_doc_delivery');

            Route::get('/get_delivery_table_to_excel', 'DeliveryAnaliticsController@get_delivery_table_to_excel');
            Route::get('/get_delivery_detail_table_to_excel', 'DeliveryAnaliticsController@get_delivery_detail_table_to_excel');

            Route::post('delete_delivery_doc', 'DeliveryAnaliticsController@deleteDeliveryDoc')->name('delete_delivery_doc');


        });

        Route::group(['prefix' => 'accepted', 'namespace' => 'Accepted'], function () {
            Route::get('/', 'AcceptedController@index');
            Route::post('get_table', 'AcceptedController@get_table');
        });

        Route::group(['prefix' => 'clients', 'namespace' => 'Clients'], function () {
            Route::get('/', 'ClientsController@index');
            Route::post('get_table', 'ClientsController@get_table');
        });

        Route::group(['prefix' => 'analitics_common', 'namespace' => 'Common'], function () {

            Route::get('/', 'CommonAnalyticsController@index');
            Route::get('/get_payments_table', 'CommonAnalyticsController@get_payments_table');
            Route::get('/get_payments_table_to_excel', 'CommonAnalyticsController@get_payments_table_to_excel');
        });

        Route::group(['prefix' => 'kv_collections', 'namespace' => 'Collections'], function () {

            Route::get('/', 'KvCollectionsController@index');
            Route::get('/get_payments_table', 'KvCollectionsController@get_payments_table');
            Route::get('/get_payments_table_to_excel', 'KvCollectionsController@get_payments_table_to_excel');
        });

        Route::group(['prefix' => 'analitics_total', 'namespace' => 'Total'], function () {
            Route::get('/', 'TotalAnalyticsController@index');

            Route::post('/get_filters', 'TotalAnalyticsController@get_filters');
            Route::post('/get_charts', 'TotalAnalyticsController@get_charts');

        });

        Route::group(['prefix' => 'analitic_point_sale', 'namespace' => 'PointSale'], function () {
            Route::get('/', 'PointSaleAnalyticsController@index');
            Route::post('/get_filters', 'PointSaleAnalyticsController@get_filters');
            Route::get('/get_table', 'PointSaleAnalyticsController@get_table');

            Route::get('/{point_sale_id}/details', 'PointSaleAnalyticsController@details');

        });

        Route::group(['prefix' => 'analytics_techunder', 'namespace' => 'Techunder'], function () {
            Route::get('/', 'TechunderAnalyticsController@index');
            Route::post('/get_charts', 'TechunderAnalyticsController@get_charts');
            Route::post('/get_table', 'TechunderAnalyticsController@get_table');
            Route::post('/get_filters', 'TechunderAnalyticsController@get_filters');
            Route::get('/details', 'TechunderAnalyticsController@details');
        });

        Route::group(['prefix' => 'analytics_underfillers', 'namespace' => 'Underfillers'], function () {
            Route::get('/', 'UnderfillersAnalyticsController@index');
            Route::post('/get_charts', 'UnderfillersAnalyticsController@get_charts');
            Route::post('/get_filters', 'UnderfillersAnalyticsController@get_filters');
            Route::get('/details', 'UnderfillersAnalyticsController@details');

        });

        Route::group(['prefix' => 'contracts_terminations', 'namespace' => 'RemovedBsos'], function () {
            Route::get('/', 'RemovedBsosController@index');
            Route::post('get_table', 'RemovedBsosController@get_table');
            Route::group(['prefix' => '{id}'], function () {
                Route::post('restore_item', 'RemovedBsosController@restoreItem');
            });
        });

        Route::group(['prefix' => 'basket_items', 'namespace' => 'Basket'], function () {
            Route::get('/', 'BasketItemsController@index');
            Route::post('get_table', 'BasketItemsController@get_table');
            Route::post('{id}/restore_item', 'BasketItemsController@restore_item');
            Route::post('{id}/remove_item', 'BasketItemsController@remove_item');
        });

        Route::group(['prefix' => 'k_manager', 'namespace' => 'Kansalting\Manager'], function () {
            Route::get('/', 'KansaltingManagerController@index');
            Route::post('/get-data', 'KansaltingManagerController@getData');
            Route::post('/get-details/{id}/{status}', 'KansaltingManagerController@getDetails');
        });

    });

    Route::group(['prefix' => 'add_payment/{bso_id}', 'namespace' => 'Payment'], function () {
        Route::get('/', 'PaymentController@add_payment');
    });

    Route::group(['namespace' => 'Payment'], function () {
        Route::get('/second_payment/{contract_id}', 'SecondPayment@second_payment');
        Route::post('/add_second_payment', 'SecondPayment@add_second_payment');
    });

    Route::group(['prefix' => 'payment/{id}', 'namespace' => 'Payment'], function () {
        Route::get('/', 'PaymentController@index');
        Route::post('/', 'PaymentController@save');
        Route::post('/save_comment', 'PaymentController@saveComment');
        Route::get('/get_pay_method_key_type', 'PaymentController@get_pay_method_key_type');
        Route::post('/detach_receipt', 'PaymentController@detach_receipt');


        Route::get('/delete', 'PaymentController@delete');

    });

    Route::group(['prefix' => 'cashbox', 'namespace' => 'Cashbox'], function () {


        Route::group(['prefix' => 'ofd_cashbox', 'namespace' => 'OFDCashbox'], function () {

            Route::get('/', 'OFDCashboxController@index');


            Route::group(['prefix' => '/{org_id}'], function () {

                Route::get('/view', 'OFDCashboxController@view');
                Route::get('/edit/{id}', 'OFDCashboxController@edit');
                Route::get('/show/{id}', 'OFDCashboxController@show');
                Route::post('/edit/{id}', 'OFDCashboxController@save');


            });


        });

        Route::group(['prefix' => 'operation_list', 'namespace' => 'OperationList'], function () {

            Route::get('/', 'OperationListController@index');
            Route::get('/{id}/show', 'OperationListController@operation_list');

        });


        Route::group(['prefix' => 'acts_to_underwriting', 'namespace' => 'ActsToUnderwriting'], function () {

            Route::get('/', 'ActsToUnderwritingController@index');

            Route::get('/get_view', 'ActsToUnderwritingController@get_view');

            Route::post('/acts/list', 'ActsToUnderwritingController@acts_list');

            Route::post('/contract/list', 'ActsToUnderwritingController@contract_list');
            Route::post('/contract/create_get_acts', 'ActsToUnderwritingController@create_get_acts');


            Route::post('/submitted/list', 'ActsToUnderwritingController@submitted_list');

            Route::group(['prefix' => 'details/{act_id}', 'namespace' => 'Details'], function () {
                Route::get('/', 'ActsToUnderwritingDetailsController@index');


                Route::get('/export', 'ActsToUnderwritingDetailsController@export');


                Route::post('/delete_items', 'ActsToUnderwritingDetailsController@delete_items');
                Route::post('/delete_act', 'ActsToUnderwritingDetailsController@delete_act');
                Route::post('/accept', 'ActsToUnderwritingDetailsController@accept');


            });

        });


        Route::group(['prefix' => 'invoice', 'namespace' => 'Invoice'], function () {

            Route::get('/', 'CashboxInvoiceController@index');
            Route::post('/get_invoices_table', 'CashboxInvoiceController@get_invoices_table');
            Route::post('/get_invoices_tab', 'CashboxInvoiceController@get_invoices_tab');

            Route::get('{id}/edit', 'CashboxInvoiceController@edit')->where('id', '[0-9]+');
            Route::post('{id}/data_invoice_payment', 'CashboxInvoiceController@data_invoice_payment')->where('id', '[0-9]+');
            Route::post('{id}/in_waiting', 'CashboxInvoiceController@in_waiting')->where('id', '[0-9]+');

            Route::post('{id}/save', 'CashboxInvoiceController@save')->where('id', '[0-9]+');

            Route::post('{id}/save-invoice-type-payment', 'CashboxInvoiceController@saveInvoiceTypePayment')->where('id', '[0-9]+');


        });

        Route::group(['prefix' => 'incomes_expenses', 'namespace' => 'IncomesExpenses'], function () {


            Route::get('/', 'IncomesExpensesController@index');
            Route::post('get_categories', 'IncomesExpensesController@get_categories');
            Route::post('get_incomes_expenses_table', 'IncomesExpensesController@get_incomes_expenses_table');


            Route::get('create', 'IncomesExpensesController@create');
            Route::post('store', 'IncomesExpensesController@store');

            Route::get('{id}/edit', 'IncomesExpensesController@edit')->where('id', '[0-9]+');
            Route::post('{id}/save', 'IncomesExpensesController@save')->where('id', '[0-9]+');

            Route::get('{id}/export', 'IncomesExpensesController@export')->where('id', '[0-9]+');

            Route::post('{id}/accept', 'IncomesExpensesController@accept')->where('id', '[0-9]+');
            Route::post('{id}/add_files', 'IncomesExpensesFileController@addFiles')->where('id', '[0-9]+');

            Route::delete('{id}/delete_files/{file_id}', 'IncomesExpensesFileController@deleteFiles');

        });


        Route::group(['prefix' => 'user_balance', 'namespace' => 'Balance'], function () {

            Route::get('/', 'BalanceController@index');
            Route::post('get_data_balance', 'BalanceController@get_data_balance');
            Route::post('get_balance', 'BalanceController@get_balance');

            Route::group(['prefix' => 'balance/{id}'], function(){
                Route::get('edit', 'BalanceController@edit');
                Route::post('edit', 'BalanceController@save');

                Route::get('/transfer', 'BalanceController@transfer_view');
                Route::post('/transfer', 'BalanceController@transfer_save');
                Route::get('/export_balance', 'BalanceController@export_balance');
            });

        });

        Route::group(['prefix' => 'collection', 'namespace' => 'Collection'], function () {

            Route::get('/', 'CollectionController@index');

            Route::get('/{id}/edit', 'CollectionController@edit');
            Route::post('/{id}/edit', 'CollectionController@save');

            Route::get('/{id}/get_details', 'CollectionController@get_details');
            Route::get('/{id}/set_balance', 'CollectionController@view_balance');
            Route::post('/{id}/set_balance', 'CollectionController@set_balance');

            Route::get('/{id}/set_collection', 'CollectionController@view_collection');
            Route::post('/{id}/set_collection', 'CollectionController@set_collection');

        });

        Route::group(['prefix' => 'foundation_payments', 'namespace' => 'FoundationPayments'], function () {

            Route::get('/', 'FoundationPaymentsController@index');
            Route::get('/{month}/{year}/', 'FoundationPaymentsController@info');
            Route::get('/{month}/{year}/accept_foundation_payments', 'FoundationPaymentsController@accept_foundation_payments');
            Route::get('/{month}/{year}/{type}/detail', 'FoundationPaymentsController@detail');
            Route::get('/{month}/{year}/incomes_expenses', 'FoundationPaymentsController@incomes_expenses');

            Route::post('/save_incomes_expenses', 'FoundationPaymentsController@save_incomes_expenses');

            Route::get('/{month}/{year}/foundation', 'FoundationPaymentsController@foundation');
            Route::post('/save_foundation', 'FoundationPaymentsController@save_foundation');


            Route::get('/{month}/{year}/balance_foundation_payments', 'FoundationPaymentsController@balance_foundation_payments');
            Route::post('/set_balance_foundation_payments', 'FoundationPaymentsController@set_balance_foundation_payments');



        });



    });

    Route::group(['prefix' => 'finance', 'namespace' => 'Finance'], function () {

        Route::group(['prefix' => 'debts', 'namespace' => 'Debts'], function () {

            Route::get('/', 'AgentController@index');
            Route::post('/get_agent_table', 'AgentController@get_agent_table');


            Route::get('/{agent_id}/detail', 'DetailController@index')->where('agent_id', '[0-9]+');
            Route::post('/{agent_id}/get_detail_table', 'DetailController@get_detail_table')->where('agent_id', '[0-9]+');

        });

        Route::group(['prefix' => 'invoice', 'namespace' => 'Invoice'], function () {

            Route::get('/', 'MainController@index');


            Route::group(['prefix' => 'payments'], function () {
                Route::get('/', 'PaymentsController@index');
                Route::post('get_payments_table', 'PaymentsController@get_payments_table');

                Route::get('update_invoice', 'PaymentsController@update_invoice');
                Route::post('update_invoice', 'PaymentsController@update_invoice');

            });


            Route::group(['prefix' => 'invoices'], function () {
                Route::get('/', 'InvoicesController@index');
                Route::post('get_invoices_table', 'InvoicesController@get_invoices_table');

                Route::get('export_many', 'InvoicesController@export_many');

                Route::get('create', 'InvoicesController@create');
                Route::post('create', 'InvoicesController@store');

                Route::get('/{invoice_id}/edit', 'InvoicesController@edit')->where('invoice_id', '[0-9]+');
                Route::post('/{invoice_id}/save', 'InvoicesController@save')->where('invoice_id', '[0-9]+');


                Route::post('/{invoice_id}/delete_payments/', 'InvoicesController@delete_payments')->where(['invoice_id' => '[0-9]+']);
                Route::post('/{invoice_id}/delete_invoice', 'InvoicesController@delete_invoice')->where(['invoice_id' => '[0-9]+']);

                Route::get('/{invoice_id}/direction', 'InvoicesController@direction')->where(['invoice_id' => '[0-9]+']);
                Route::get('/{invoice_id}/act_export', 'InvoicesController@act_export')->where(['invoice_id' => '[0-9]+']);
                Route::get('/{invoice_id}/act_export_under', 'InvoicesController@act_export_under')->where(['invoice_id' => '[0-9]+']);

                Route::get('/{invoice_id}/act_for_pay_cash', 'InvoicesController@act_for_pay_cash')->where(['invoice_id' => '[0-9]+']);
                Route::get('/{invoice_id}/act_for_pay_cashless', 'InvoicesController@act_for_pay_cashless')->where(['invoice_id' => '[0-9]+']);
                Route::get('/{invoice_id}/act_for_pay_cashless_card_broker', 'InvoicesController@act_for_pay_cashless_card_broker')->where(['invoice_id' => '[0-9]+']);


            });

            Route::group(['prefix' => 'reservation'], function () {
                Route::get('/', 'ReservationController@index');
                Route::post('/get_reservation_table', 'ReservationController@get_reservation_table');

                Route::get('/create', 'ReservationController@create');
                Route::post('/create', 'ReservationController@store');

                Route::get('/{reservation_id}/edit', 'ReservationController@edit')->where(['reservation_id' => '[0-9]+']);
                Route::post('/{reservation_id}/edit', 'ReservationController@save')->where(['reservation_id' => '[0-9]+']);

                Route::post('/{reservation_id}/delete', 'ReservationController@delete')->where(['reservation_id' => '[0-9]+']);

                Route::get('/{reservation_id}/export', 'ReservationController@export')->where(['reservation_id' => '[0-9]+']);
                Route::post('/{reservation_id}/export_reservation', 'ReservationController@export_reservation')->where(['reservation_id' => '[0-9]+']);


            });


            Route::get('/{id}/view', 'MainController@view');

        });


        Route::group(['prefix' => 'expected_payments', 'namespace' => 'ExpectedPayments'], function () {

            Route::get('/', 'ExpectedPaymentsController@index');
            Route::get('/get_payments_table', 'ExpectedPaymentsController@get_payments_table');



        });


        Route::group(['prefix' => 'payment/{id}', 'namespace' => 'PaymentTemp'], function () {

            Route::get('/edit', 'PaymentTempController@index');
            Route::post('/edit', 'PaymentTempController@save');

            Route::delete('/delete', 'PaymentTempController@delete_payment');


        });


    });


    Route::group(['prefix' => 'bso', 'namespace' => 'BSO'], function () {

        Route::get('/add_bso_warehouse', 'AddBsoWarehouseController@index');
        Route::post('/add_bso_warehouse/add_bso', 'AddBsoWarehouseController@add_bso');
        Route::post('/delete_bso_warehouse/{id}/delete_bso', 'DeleteBsoWarehouseController@delete_bso');

        Route::group(['prefix' => 'transfer', 'namespace' => 'Transfer'], function () {

            Route::get('/', 'TransferBsoController@index');
            Route::get('/get_user_ban_reason/', 'TransferBsoController@get_user_ban_reason');
            Route::get('/get_agent_info/', 'TransferBsoController@get_agent_info');
            Route::get('/create_bso_cart/', 'TransferBsoController@create_bso_cart');
            Route::get('/bso_cart_content/', 'TransferBsoController@bso_cart_content');
            Route::get('/get_bso_types/', 'TransferBsoController@get_bso_types');
            Route::get('/rit_bso_selector/', 'TransferBsoController@rit_bso_selector');
            Route::get('/get_bsos/', 'TransferBsoController@get_bsos');
            Route::get('/get_all_bsos/', 'TransferBsoController@get_all_bsos');
            Route::get('/move_to_cart/', 'TransferBsoController@move_to_cart');
            Route::get('/remove_from_bso_cart/', 'TransferBsoController@remove_from_bso_cart');
            Route::get('/rit_bso_transfer/', 'TransferBsoController@rit_bso_transfer');

            Route::get('reserve_export', 'TransferBsoController@reserve_export');

            Route::get('/transfer_bso/', 'TransferFinishBsoController@transfer_bso');

            Route::get('/transfer_bso_act_agent/', 'TransferFinishBsoController@transfer_bso_act_agent');


        });


        Route::group(['prefix' => 'inventory_agents'], function () {
            Route::get('/', 'InventoryAgentsController@index');
            Route::get('/reports_table_export', 'InventoryAgentsController@reports_table_export');
            Route::post('/get_agents_table', 'InventoryAgentsController@get_agents_table');

            Route::get('/export', 'InventoryAgentsController@inventory_bso_export');
            Route::get('/details', 'InventoryAgentsController@details');
            Route::post('/details_list', 'InventoryAgentsController@details_list');
            Route::post('/get_details_table', 'InventoryAgentsController@get_details_table');
            Route::get('/details_export', 'InventoryAgentsController@details_export');
            Route::post('/refresh_agent_date', 'InventoryAgentsController@refresh_agent_date');


        });


        Route::group(['prefix' => 'inventory_bso'], function () {
            Route::get('/', 'InventoryBsoController@index');
            Route::get('/export', 'InventoryBsoController@inventory_bso_export');
            Route::get('/details', 'InventoryBsoController@details');
            Route::post('/details_list', 'InventoryBsoController@details_list');
            Route::post('/get_details_table', 'InventoryBsoController@get_details_table');
            Route::get('/details_export', 'InventoryBsoController@details_export');

        });

        Route::group(['prefix' => 'bso_statuses_check'], function () {
            Route::get('/', 'BsoStatusesController@index');
            Route::post('/check', 'BsoStatusesController@check');
        });

        Route::group(['prefix' => 'ruin_bso'], function () {
            Route::get('/', 'RuinBsoController@index');
            Route::get('/get_table', 'RuinBsoController@get_table');
            Route::post('/ruin', 'RuinBsoController@ruin');
            Route::post('/show_error', 'RuinBsoController@show_error');
            Route::get('/export/{agentId}', 'RuinBsoController@export');
        });

        Route::group(['prefix' => 'actions'], function () {


            Route::get('/get_bso_type', 'ActionsController@get_bso_type');
            Route::get('/get_bso_product', 'ActionsController@get_bso_product');
            Route::get('/get_supplier_product', 'ActionsController@get_supplier_product');

            Route::get('/get_series', 'ActionsController@get_series');
            Route::get('/get_dop_series', 'ActionsController@get_dop_series');
            Route::get('/bso_number_to', 'ActionsController@bso_number_to');
            Route::get('/create_transfer_act', 'ActionsController@create_transfer_act');

            Route::post('/get_bso/suggest/party/', 'ActionsController@get_bso');
            Route::post('/get_bso_sold/suggest/party/', 'ActionsController@get_bso_sold');
            Route::post('/get_bso_order/suggest/party/', 'ActionsController@get_bso_order');
            Route::post('/get_clear_bso/suggest/party/', 'ActionsController@get_clear_bso');
            Route::post('/get_all_bso/suggest/party/', 'ActionsController@get_all_bso');

            Route::get('/get_bso/status/party/', 'ActionsController@status_party');
            Route::get('/get_bso_sold/status/party/', 'ActionsController@status_party');
            Route::get('/get_bso_order/status/party/', 'ActionsController@status_party');
            Route::get('/get_clear_bso/status/party/', 'ActionsController@status_party');

            Route::get('/get_bso/iplocate/address/', 'ActionsController@status_party');
            Route::get('/get_bso_sold/iplocate/address/', 'ActionsController@status_party');
            Route::get('/get_bso_order/iplocate/address/', 'ActionsController@status_party');
            Route::get('/get_clear_bso/iplocate/address/', 'ActionsController@status_party');


            Route::post('/get_bso_sold/findById/party/', 'ActionsController@status_party');




            Route::post('/get_inputs_installment_algorithms', 'ActionsController@get_inputs_installment_algorithms');
            Route::get('/get_installment_algorithms', 'ActionsController@get_installment_algorithms');
            Route::get('/get_financial_policy', 'ActionsController@get_financial_policy');
            Route::get('/get_accesses_products', 'ActionsController@get_accesses_products');

            Route::get('/get_html_mini_contract_object_insurer', 'ActionsController@get_html_mini_contract_object_insurer');
            Route::get('/get_html_mini_contract_object_insurer_with_data', 'ActionsController@get_html_mini_contract_object_insurer_with_data');


            Route::post('/get_orders_front/suggest/party/', 'ActionsController@get_orders_front');

            Route::get('/get_order_id_front', 'ActionsController@get_order_id_front');
            Route::get('/get_payments_accepts', 'ActionsController@get_payments_accepts');
            Route::post('/check_exsistence_bso', 'ActionsController@check_exsistence_bso');

            /*Route::get('/update_source_from_front', 'ActionsController@update_source_from_front');*/ //скрипт для обогащения источника из РИТ
            /*Route::get('/update_subjects_from_front/{from}/{to}', 'ActionsController@update_subjects_from_front');*/ //скрипт для обогащения данных из РИТ






        });

        Route::group(['prefix' => '/items', 'namespace' => 'Items'], function () {

            Route::get('{id}/', 'BsoItemsController@index');
            Route::post('{id}/save_status', 'BsoItemsController@save_status');

            Route::get('{id}/edit_supplier_org', 'BsoItemsController@supplier_org');
            Route::post('{id}/edit_supplier_org', 'BsoItemsController@edit_supplier_org');
            Route::get('{id}/edit_accept_status', 'BsoItemsController@accept_status');
            Route::post('{id}/edit_accept_status', 'BsoItemsController@edit_accept_status');

            Route::get('{id}/edit_bso_title', 'BsoItemsController@bso_title');
            Route::post('{id}/edit_bso_title', 'BsoItemsController@edit_bso_title');


            Route::post('{id}/send-contract-front', 'BsoItemsController@sendContractFront');
            Route::post('{id}/destroy-contract-front', 'BsoItemsController@destroyContractFront');


            Route::get('{id}/clear-all-contract', 'BsoItemsController@clearAllContract');

        });


    });

    Route::group(['prefix' => 'reports', 'namespace' => 'Reports'], function () {

        Route::group(['prefix' => 'reports_sk', 'namespace' => 'ReportsSK'], function () {
            Route::get('/', 'ReportsSKController@index');

            Route::post('/get_filters', 'ReportsSKController@get_filters');
            Route::post('/get_table', 'ReportsSKController@get_table');

            Route::group(['prefix' => '/{supplier_id}', 'where' => ['supplier_id' => '[0-9]+']], function () {

                Route::get('/reports', 'ReportsSKController@reports');
                Route::post('/reports/table', 'ReportsSKController@reports_table');

                Route::group(['prefix' => '/bordereau'], function () {
                    Route::get('/', "ReportsSKFormationController@index");
                    Route::post('/get_table', "ReportsSKFormationController@get_table");
                    Route::post('/get_action_table', "ReportsSKFormationController@get_action_table");
                    Route::post('/execute', "ReportsSKFormationController@execute");
                    Route::post('/filter_last_reports', "ReportsSKFormationController@filter_last_reports");
                });

                Route::group(['prefix' => '/dvoy'], function () {
                    Route::get('/', "ReportsSKFormationController@index");
                    Route::post('/get_table', "ReportsSKFormationController@get_table");
                    Route::post('/get_action_table', "ReportsSKFormationController@get_action_table");
                    Route::post('/execute', "ReportsSKFormationController@execute");
                    Route::post('/filter_last_reports', "ReportsSKFormationController@filter_last_reports");
                });


            });


        });

        Route::group(['prefix' => '/order/{report_id}', 'namespace' => 'ReportsSK', 'where' => ['report_id' => '[0-9]+']], function () {

            Route::get('/', 'ReportsSKOrderController@index');
            Route::post('/upload_file', 'ReportsSKOrderController@upload_file');

            Route::post('/', 'ReportsSKOrderController@save');
            Route::post('/generate_dvou_report', 'ReportsSKOrderController@generate_dvou_report');
            Route::post('/recalc_kv', 'ReportsSKOrderController@recalc_kv');
            Route::post('/delete_payments', 'ReportsSKOrderController@delete_payments');
            Route::post('/delete_report_with_payments', 'ReportsSKOrderController@delete_report_with_payments');
            Route::post('/get_table', 'ReportsSKOrderController@get_table');
            Route::post('/marker_payments', 'ReportsSKOrderController@marker_payments');
            Route::get('/form_report', 'ReportsSKOrderController@form_report');

            Route::get('/set_edit_base_payments', 'ReportsSKOrderController@set_edit_base_payments');

            Route::group(['prefix' => 'payment_sum', 'namespace' => 'PaymentSum'], function () {
                Route::get('create', 'ReportPaymentSumController@create');
                Route::get('{payment_sum_id}/edit', 'ReportPaymentSumController@edit');

                Route::post('store', 'ReportPaymentSumController@store');
                Route::post('{payment_sum_id}/save', 'ReportPaymentSumController@save');
                Route::post('{payment_sum_id}/delete', 'ReportPaymentSumController@delete');
            });

        });



        Route::group(['prefix' => 'reports_sk_summary', 'namespace' => 'ReportsSKSummary'], function () {
            Route::get('/', 'ReportsSKSummaryController@index');
            Route::post('/get_filters', 'ReportsSKSummaryController@get_filters');

            Route::post('/get_table', 'ReportsSKSummaryController@get_table');

        });

        Route::group(['prefix' => 'promotion', 'namespace' => 'ReportsAdverstising'], function () {
            Route::get('/', 'ReportsAdverstisingController@index');
            Route::post('/get_filters', 'ReportsAdverstisingController@get_filters');
            Route::post('/get_table', 'ReportsAdverstisingController@get_table');

            Route::post('/update_adverstising', 'ReportsAdverstisingController@update_adverstising');
            Route::post('/recount', 'ReportsAdverstisingController@recount_adverstising');

            Route::group(['prefix' => '/{supplier_id}', 'where' => ['supplier_id' => '[0-9]+']], function () {

                Route::get('/adverstisings', 'ReportsAdverstisingController@adverstisings');
                Route::post('/adverstisings/table', 'ReportsAdverstisingController@adverstisings_table');

                Route::group(['prefix' => '/{adverstising_id}', 'where' => ['adverstising_id' => '[0-9]+']], function () {

                    Route::get('/reports', 'ReportsAdverstisingController@reports');
                    Route::post('/reports/table', 'ReportsAdverstisingController@reports_table');

                });
            });
        });
    });


    Route::group(['prefix' => 'bso_acts', 'namespace' => 'BsoActs'], function () {

        Route::get('show_bso_act/{id}', 'ShowBsoActController@index');
        Route::post('update/{id}', 'ShowBsoActController@update');
        Route::get('export/{id}', 'ShowBsoActController@export');
        Route::post('export_bso_act/{id}', 'ShowBsoActController@export_bso_act');


        Route::group(['prefix' => 'acts_sk', 'namespace' => 'ActsSK'], function () {
            Route::get('/', 'ActsSKController@index');

            Route::post('get_filters', 'ActsSKController@get_filters');
            Route::post('get_table', 'ActsSKController@get_table');


            Route::group(['prefix' => '{supplier_id}', 'where' => ['supplier_id' => '[0-9]+']], function () {

                Route::post('acts_list_table', "ActsSKController@acts_list_table");


                Route::group(['prefix' => 'acts'], function () {
                    Route::get('/', "ActsSKController@acts");
                    Route::group(['prefix' => '{act_id}', 'where' => ['act_id' => '[0-9]+']], function () {
                        Route::get('edit', "ActsSKController@edit");
                        Route::get('export', "ActsSKController@export");
                        Route::post('upload_file', "ActsSKController@upload_file");

                        Route::post('accept', "ActsSKController@accept");
                        Route::post('unaccept', "ActsSKController@unaccept");
                        Route::post('act_files', "ActsSKController@act_files");
                        Route::post('update', "ActsSKController@update");
                        Route::post('delete_items', "ActsSKController@delete_items");

                        Route::post('delete_bsos', "ActsSKController@delete_bsos");
                        Route::post('delete_payments', "ActsSKController@delete_payments");
                    });
                });


                Route::group(['prefix' => '/bso', 'namespace' => 'Bso'], function () {
                    Route::get('/', "BsoActsController@index");
                    Route::post('get_table', "BsoActsController@get_table");
                    Route::post('get_action_table', "BsoActsController@get_action_table");
                    Route::post('execute_bso', "BsoActsController@execute_bso");
                });

                Route::group(['prefix' => 'contracts', 'namespace' => 'Contracts'], function () {
                    Route::get('/', "ContractActsController@index");
                    Route::post('get_table', "ContractActsController@get_table");
                    Route::post('get_action_table', "ContractActsController@get_action_table");
                    Route::post('execute_payments', "ContractActsController@execute_payments");

                });

            });


        });


        Route::group(['prefix' => 'acts_transfer', 'namespace' => 'ActsTransfer'], function () {
            Route::get('/', 'ActsTransferController@index');
            Route::post('/get_acts_table', 'ActsTransferController@get_acts_table');

        });

        Route::group(['prefix' => 'acts_reserve', 'namespace' => 'ActsReserve'], function () {
            Route::get('/', 'ActsReserveController@index');
            Route::post('/get_acts_table', 'ActsReserveController@get_acts_table');

        });

        Route::group(['prefix' => 'acts_implemented', 'namespace' => 'ActsImplemented'], function () {
            Route::get('/', 'ActsImplementedController@index');

            Route::get('/get_view', 'ActsImplementedController@get_view');

            Route::get('/spoiled/edit', 'ActsImplementedController@edit_spoiled');
            Route::post('/spoiled/edit', 'ActsImplementedController@save_spoiled');
            Route::post('/spoiled/list', 'ActsImplementedController@spoiled_list');
            Route::post('/spoiled/create_get_realized_acts', 'ActsImplementedController@create_spoiled_realized_acts');


            Route::post('/acts/list', 'ActsImplementedController@acts_list');

            Route::post('/get_realized_acts', 'ActsImplementedController@get_realized_acts');

            Route::post('/contract/list', 'ActsImplementedController@contract_list');
            Route::post('/contract/create_get_realized_acts', 'ActsImplementedController@create_get_realized_acts');


            Route::group(['prefix' => 'details/{act_id}', 'namespace' => 'Details'], function () {
                Route::get('/', 'ActsImplementedDetailsController@index');
                Route::get('/export', 'ActsImplementedDetailsController@export');
                Route::post('/accept', 'ActsImplementedDetailsController@accept');
                Route::post('/delete_items', 'ActsImplementedDetailsController@delete_items');
                Route::post('/delete_act', 'ActsImplementedDetailsController@delete_act');
            });


        });

    });

    Route::group(['prefix' => 'settings/news', 'namespace' => 'News'], function () {


    });


    Route::group(['prefix' => 'informing', 'namespace' => 'Informing'], function () {


        /* NEWS */
        Route::group(['prefix' => 'news'], function(){
            Route::get('/', 'NewsController@index');
            Route::get('/{id}', 'NewsController@show');
            Route::post('confirm-read', 'NewsController@confirmRead');
        });

        Route::group(['prefix' => 'edit-news'], function(){
            Route::get('/', 'NewsController@edit');
            Route::get('/create', 'NewsController@form_create');
            Route::post('/create', 'NewsController@create');

            Route::group(['prefix' => '/{id}'], function(){
                Route::get('/edit', 'NewsController@edit_new');
                Route::post('/file/{file_id}', 'NewsController@delete_file');
                Route::post('/update_file/', 'NewsController@update_file');
                Route::post('/delete_file/{file_name}', 'NewsController@delete_file');
                Route::post('/update', 'NewsController@update');
                Route::post('/delete', 'NewsController@delete');
            });
        });

        /* SEGMENTS */
        Route::group(['prefix' => 'segments'], function(){
           Route::get('/', 'SegmentsController@index');
        });

        Route::group(['prefix' => 'edit-segments'], function(){
            Route::get('/', 'SegmentsController@edit_segments');
            Route::get('/create', 'SegmentsController@edit_segment');
            Route::post('/create', 'SegmentsController@save_segment');


            Route::group(['prefix' => '/{id}'], function(){
                Route::get('/', 'SegmentsController@edit_segment_form');
                Route::post('/update', 'SegmentsController@update_segment');
                Route::post('/file/{file_id}', 'SegmentsController@delete_file');
                Route::post('/update_file', 'SegmentsController@update_file');
                Route::post('/delete_segment', 'SegmentsController@delete_segment');
            });

        });

        /* KV */
        Route::group(['prefix' => 'kv'], function(){
            Route::get('/', 'KvController@index');
        });

        Route::group(['prefix' => 'edit-kv'], function(){
            Route::get('/', 'KvController@edit_kvs');
            Route::get('/create', 'KvController@edit_kv');
            Route::post('/create', 'KvController@save_kv');


            Route::group(['prefix' => '/{id}'], function(){
                Route::get('/', 'KvController@edit_kv_form');
                Route::post('/update', 'KvController@update_kv');
                Route::post('/file/{file_id}', 'KvController@delete_file');
                Route::post('/update_file', 'KvController@update_file');
                Route::post('/delete_kv', 'KvController@delete_kv');
            });

        });

        /* GUID*/
        Route::group(['prefix' => 'guide','namespace'=>'Guides'], function(){


            Route::get('/', 'GuidesController@index');

            Route::group(['prefix' => '/edit'], function() {

                Route::get('/', 'GuidesController@edit');
                Route::get('/{id}', 'GuidesController@edit');
                Route::get('/group/', 'GuidesController@edit');

                Route::post('/add_block', 'GuidesController@create');
                Route::post('/delete', 'GuidesController@delete');

                Route::post('/{id}', 'GuidesController@save');
            });


            Route::get('/show', 'GuidesController@frame');
            Route::get('/show/{id}', 'GuidesController@frame');

        });

       /* Route::group(['prefix' => 'edit-news'], function(){
            Route::get('/', 'NewsController@edit');
            Route::get('/create', 'NewsController@form_create');
            Route::post('/create', 'NewsController@create');

            Route::group(['prefix' => '/{id}'], function(){
                Route::get('/edit', 'NewsController@edit_new');
                Route::post('/file/{file_id}', 'NewsController@delete_file');
                Route::post('/update_file/', 'NewsController@update_file');
                Route::post('/delete_file/{file_name}', 'NewsController@delete_file');
                Route::post('/update', 'NewsController@update');
                Route::post('/delete', 'NewsController@delete');
            });
        });*/




    });

    Route::group(['prefix' => 'orders', 'namespace' => 'Orders'], function () {

        Route::get('/', 'OrderInspectionController@index');
        Route::get('get_tab', 'OrderInspectionController@get_tab');
        Route::get('get_table', 'OrderInspectionController@get_table');
        Route::get('/create', 'OrderInspectionController@create');
        Route::get('/edit/{id}', 'OrderInspectionController@edit');
        Route::post('/edit/{id}/get_autowashes', 'OrderInspectionController@get_autowashes');
        Route::post('/edit/{id}/change_event', 'OrderInspectionController@change_event');
        Route::post('/edit/{id}/change_status', 'OrderInspectionController@change_status');
        Route::post('/edit/{id}/set_executor', 'OrderInspectionController@set_executor');
        Route::post('/edit/{id}/decline', 'OrderInspectionController@decline_user');
        Route::post('/edit/{id}/processing_user', 'OrderInspectionController@processing_user');
        Route::post('/edit/{id}/arrived_order', 'OrderInspectionController@arrived_order');
        Route::post('/edit/{id}/finish_order', 'OrderInspectionController@finish_order');
        Route::post('/edit/{id}/get_people', 'OrderInspectionController@get_people');
        Route::post('/edit/{id}/get_moving_logs', 'OrderInspectionController@get_moving_logs');
        Route::post('/edit/{id}/get_executor_detail', 'OrderInspectionController@get_executor_detail');
        Route::get('/edit/{id}/export_act', 'OrderInspectionController@export_act');
        Route::post('/save/{id}', 'OrderInspectionController@save');
        Route::post('/delete/{id}', 'OrderInspectionController@delete');

        Route::get('/accept/{id}', 'OrderInspectionController@accept');



        Route::group(['prefix' => 'acts_of_completion'], function(){
            Route::get('/', 'ActsOfCompletionController@index');
            Route::get('/get_table', 'ActsOfCompletionController@index_table');
            Route::group(['prefix' => '/{org_id}', 'where' => ['supplier_id' => '[0-9]+']], function(){

                /* Созданные отчеты */
                Route::group(['prefix' => 'created_reports'], function(){
                    Route::get('/', 'ActsOfCompletionController@created_reports');
                    Route::get('/get_table', 'ActsOfCompletionController@created_reports_get_table');

                    /* Осмотры внутри созданного отчета */
                    Route::group(['prefix' => '{inspection_id}'], function(){
                        Route::get('/', 'ActsOfCompletionController@inspections_created_report');
                        Route::get('/get_table', 'ActsOfCompletionController@inspections_created_report_get_table');
                        Route::post('/', 'ActsOfCompletionController@inspections_created_report_update');
                        Route::post('/upload_file', 'ActsOfCompletionController@inspections_created_report_upload_file');
                        Route::delete('/delete_file', 'ActsOfCompletionController@inspections_created_report_delete_file');
                    });

                });

                /* Формирование отчетов */
                Route::group(['prefix' => 'reports_create'], function(){
                    Route::get('/', 'ActsOfCompletionController@reports_create');
                    Route::get('/get_table', 'ActsOfCompletionController@reports_create_get_table');
                    Route::get('/get_actions', 'ActsOfCompletionController@reports_create_get_actions');
                    Route::post('/execute', 'ActsOfCompletionController@reports_create_execute');
                });

            });

        });





    });


    Route::group(['prefix' => 'orders/processing', 'namespace' => 'Orders'], function () {

        Route::get('/', 'ProcessingOrdersController@index');

        Route::post('/create/', 'ProcessingOrdersController@create');

        Route::get('/bso-supplier-product/{id}', 'ProcessingOrdersController@bsoSupplierProduct');

        Route::post('/order/{order_id}/bso_cart', 'ProcessingOrdersController@saveBsoCart');

        Route::get('/contract/{id}', 'ProcessingOrdersController@contract');
        Route::post('/contract/{id}/save', 'ProcessingOrdersController@save');
        Route::post('/contract/{id}/status-to-change', 'ProcessingOrdersController@status_to_change');
        Route::post('/contract/{id}/delete-order', 'ProcessingOrdersController@delete_order');
        Route::post('/contract/{id}/clone', 'ProcessingOrdersController@clone_contract');


        Route::get('/contract/{id}/get_html_block', 'ProcessingOrdersController@get_html_block');


        Route::post('get_table', 'ProcessingOrdersController@get_table');

        Route::get('/combine', 'ProcessingOrdersController@combine');
        Route::post('/combine', 'ProcessingOrdersController@set_combine');

        Route::get('/assign', 'ProcessingOrdersController@assign');
        Route::post('/assign', 'ProcessingOrdersController@set_assign');

        Route::get('/order/{id}', 'ProcessingOrdersController@order');
        Route::post('/order/{id}/save', 'ProcessingOrdersController@save_order');


        Route::post('/order/{id}/close', 'ProcessingOrdersController@close_order');







    });

    Route::group(['prefix' => 'orders/departures_courier_acts', 'namespace' => 'Orders'], function () {
        Route::get('/', 'DeparturesCourierActsController@index');

        Route::get('/orders', 'DeparturesCourierActsController@orders');
        Route::get('/acts', 'DeparturesCourierActsController@acts');
        Route::post('/get_table', 'DeparturesCourierActsController@get_table');

        Route::post('/set_acts', 'DeparturesCourierActsController@set_acts');

        Route::post('/get_acts', 'DeparturesCourierActsController@get_acts');

        Route::get('/acts/{id}', 'DeparturesCourierActsController@view_act');

        Route::post('/acts/{id}', 'DeparturesCourierActsController@save_acts');

        Route::post('/acts/{id}/delete_orders_act', 'DeparturesCourierActsController@delete_orders_act');
        Route::post('/acts/{id}/delete_act', 'DeparturesCourierActsController@delete_act');


    });

    Route::group(['prefix' => 'orders/contracts/orders', 'namespace' => 'Contracts\Orders'], function () {
        Route::get('/', 'OrdersController@index');
    });



    Route::group(['prefix' => 'contracts', 'namespace' => 'Contracts'], function () {

        Route::group(['namespace' => 'Underfillers'], function () {
            Route::get('underfillers', 'UnderfillersController@index');
            Route::get('underfillers/complete_polices', 'UnderfillersController@complete_polices');
            Route::get('underfillers/get_table', 'UnderfillersController@get_table');
            Route::get('underfillers/get_complete_polices_table', 'UnderfillersController@get_complete_polices_table');
            Route::get('underfillers/edit/{id}', 'UnderfillersController@edit');
            Route::get('underfillers/get_models', 'UnderfillersController@get_models');

            Route::post('underfillers/save/{id}', 'UnderfillersController@save_contract');
            Route::post('underfillers/save_calc/{id}', 'UnderfillersController@saveCalc');


            Route::get('underfillers/close_contract/{id}', 'UnderfillersController@close_contract');

        });


        Route::group(['prefix' => 'reception_contracts', 'namespace' => 'ReceptionContracts'], function () {
            Route::get('/', 'ReceptionContractsController@index');
            Route::post('get_tab', 'ReceptionContractsController@get_tab');
            Route::post('get_table', 'ReceptionContractsController@get_table');


            Route::group(['prefix' => 'details/{act_id}', 'namespace' => 'Details'], function () {
                Route::get('/', 'ReceptionContractsDetailsController@index');
                Route::get('/accept', 'ReceptionContractsDetailsController@accept');
                Route::post('/delete_items', 'ReceptionContractsDetailsController@delete_items');
                Route::post('/delete_act', 'ReceptionContractsDetailsController@delete_act');
            });

        });

        Route::group(['prefix' => 'orders', 'namespace' => 'Orders'], function () {
            Route::get('/create', 'OrdersController@create');
            Route::get('/get_tab', 'OrdersController@get_tab');
            Route::get('/get_table', 'OrdersController@get_table');
            Route::get('/edit/{id}', 'OrdersController@edit');
            Route::get('/edit/{id}/delete', 'OrdersController@delete');
            Route::get('/edit/{id}/act_export_delivery', 'OrdersController@act_export_delivery');
            Route::post('/save/{id}', 'OrdersController@save');
            Route::get('/create_contracts/{id}', 'OrdersController@create_contracts_template');
            Route::post('/create_contracts/{id}', 'OrdersController@create_contracts');
            Route::post('/change_status/{id}', 'OrdersController@change_status');
            Route::post('/document/{id}', 'OrdersController@document');
            Route::post('/contract/{contract_id}/edit', 'OrdersController@order_contracts_save');
            Route::get('/contract/{contract_id}/add_pay', 'OrdersController@add_pay');

            Route::get('/created_contracts/edit/{id}', 'OrdersController@created_contracts_edit');
            Route::post('/get_bso_cart/suggest/party/', 'OrdersController@get_bso_cart');

        });

        Route::group(['prefix' => 'online', 'namespace' => 'Online'], function () {
            Route::get('/', 'OnlineController@index');
            Route::get('/create/{product_id}', 'OnlineController@create');
            Route::get('/edit/{id}', 'OnlineController@edit');

            Route::get('/set_sk_calc', 'OnlineController@set_sk_calc');
            Route::get('/release_calculate', 'OnlineController@release_calculate');

            Route::post('/{contract_id}/payment_confirm', 'OnlineController@payment_confirm');
            Route::post('/{contract_id}/confirm_sms', 'OnlineController@confirm_sms');
            Route::post('/{contract_id}/return_calculation', 'OnlineController@return_calculation');
            Route::post('/{contract_id}/check_status', 'OnlineController@check_status');

            Route::post('/save/{product_id}', 'OnlineController@save');
            Route::post('/underfiller_save/{product_id}', 'OnlineController@underfillersSave');


            Route::get('/action/{id}/get-prolongation', 'ActionController@get_prolongation');
            Route::get('/action/{id}/subject', 'ActionController@subject');

            Route::get('/driver_form/{id}', 'OnlineController@driver_form');
            Route::get('/driver_form_calc/{id}', 'OnlineController@driver_form_calc');
            Route::get('/driver_old_license_form/{driver_id}', 'OnlineController@driver_old_license_form');
            Route::post('/driver_old_license_save/{driver_id}', 'OnlineController@driver_old_license_save');
            Route::post('/driver_old_license_delete/{driver_id}', 'OnlineController@driver_old_license_delete');

            Route::post('/create_product/{id}/{id_insurance}', 'OnlineController@create_product');

            Route::post('/issue_form/{id}/{id_insurance}', 'OnlineController@issue_form');

            Route::post('/calculate/{product_id}', 'OnlineController@calculate');
            Route::post('/temp_calculate/{product_id}', 'OnlineController@temp_calculate');

            Route::get('/get_calculate_list/{contract_id}', 'OnlineController@get_calculate_list');
            Route::get('/get_calculate_sk/{calculation_id}', 'OnlineController@get_calculate_sk');
            Route::get('/calculate_sk/{calculation_id}', 'OnlineController@calculate_sk');

            /* Метод для подтверждения сохранения полиса */
            Route::post('/accept_save_policy_sk/{calculation_id}', 'OnlineController@accept_save_policy_sk');



            Route::post('/update_calculation/{id}', 'OnlineController@update_calculation');


            Route::post('/get_kbm/', 'ActionController@get_kbm');
            Route::post('/delete_driver/', 'ActionController@delete_driver');

            Route::post('/optional_equipment/', 'OnlineController@optional_equipment');
            Route::post('/optional_equipment/all/', 'OnlineController@optional_equipment_all');

            Route::post('/get_drivers/{id}', 'OnlineController@get_drivers');


            Route::get('/creare_new_calc/{id}', 'ActionController@creare_new_calc');
            Route::post('/creare_new_calc/{id}', 'ActionController@set_new_calc');

            Route::get('/release_boxes/{contract_id}', 'OnlineController@release_boxes');



        });

        Route::group(['prefix' => 'partner-leads', 'namespace' => 'Partners'], function () {
            Route::get('/', 'LeadController@index');
            Route::get('/{id}', 'LeadController@view');
            Route::post('/leads_list', 'LeadController@leads_list');
            Route::post('/{id}/edit', 'LeadController@edit');
        });

        Route::group(['prefix' => 'fast_accept', 'namespace' => 'FastAccept'], function () {
            Route::get('/', 'FastAcceptController@index');
            Route::get('/form_fast_acceptance/', 'FastAcceptController@form_fast_acceptance');

            Route::get('/create_receipt_frame', 'FastAcceptController@create_receipt_frame');
            Route::post('/create_receipt', 'FastAcceptController@create_receipt');


            Route::post('/', 'FastAcceptController@set_fast_accept');
            Route::post('/check_correct_bso', 'FastAcceptController@check_correct_bso');

            Route::post('autosave', 'FastAcceptController@autosave');
            Route::get('get_autosave', 'FastAcceptController@get_autosave');


        });

        Route::group(['prefix' => 'primary', 'namespace' => 'Primary'], function () {
            Route::get('/', 'PrimaryController@index');
            Route::get('/form_fast_acceptance/', 'PrimaryController@form_fast_acceptance');
            Route::post('/', 'PrimaryController@set_fast_accept');
            Route::post('autosave', 'PrimaryController@autosave');
            Route::get('get_autosave', 'PrimaryController@get_autosave');
        });

        Route::group(['prefix' => 'primary-agent', 'namespace' => 'PrimaryAgent'], function () {
            Route::get('/', 'PrimaryAgentController@index');
            Route::get('/form_fast_acceptance/', 'PrimaryAgentController@form_fast_acceptance');
            Route::post('/', 'PrimaryAgentController@set_fast_accept');
            Route::post('autosave', 'PrimaryAgentController@autosave');
            Route::get('get_autosave', 'PrimaryAgentController@get_autosave');
        });

        Route::group(['prefix' => 'verification', 'namespace' => 'Verification'], function () {
            Route::get('/', 'VerificationController@index');
            Route::get('/get_tab', 'VerificationController@get_tab');
            Route::get('/get_table', 'VerificationController@get_table');
            Route::get('/get_export_table/', 'VerificationController@get_export_table');
            Route::get('/get_export_act_of_disagreement/', 'VerificationController@get_export_act_of_disagreement');

            Route::group(['prefix' => 'contract/{contract_id}', 'namespace' => 'Contract'], function () {
                Route::get('edit', 'VerificationContractController@edit');
                Route::get('return_to_check', 'VerificationContractController@return_to_check');
            });

        });

        Route::group(['prefix' => 'temp_contracts', 'namespace' => 'TempContracts'], function () {
            Route::get('/', 'TempContractsController@index');
            Route::get('/get_tab', 'TempContractsController@get_tab');
            Route::get('/get_table', 'TempContractsController@get_table');

            Route::get('/create_receipt_frame', 'TempContractsController@create_receipt_frame');
            Route::post('/create_receipt', 'TempContractsController@create_receipt');

            Route::group(['prefix' => 'add'], function () {

                Route::post('/contract/', 'TempContractsController@create_contract');

                Route::get('/electronic', 'TempContractsController@add_electronic');
                Route::get('/electronic_for_primary', 'TempContractsController@electronic_for_primary');
                Route::get('/paper', 'TempContractsController@add_paper');
                Route::get('/payment', 'TempContractsController@add_payment');
                Route::get('/receipt', 'TempContractsController@add_receipt');
                Route::post('/receipt', 'TempContractsController@create_receipt');

                Route::post('/electronic/', 'TempContractsController@create_bso_contract');
                Route::post('/electronic_for_primary/', 'TempContractsController@create_bso_contract_for_primary');


                Route::post('/payment', 'TempContractsController@create_to_payment');


            });

            Route::group(['prefix' => 'contract/{contract_id}'], function () {

                Route::get('/edit', 'TempContractsController@temp_contracts_edit');
                Route::post('/edit', 'TempContractsController@temp_contracts_save');

                Route::get('/add_pay', 'TempContractsController@add_pay');
                Route::get('/del_pay', 'TempContractsController@del_pay');

                Route::post('/set_comment', 'TempContractsController@set_comment');

                Route::post('/send_check', 'TempContractsController@send_check');
                Route::post('/send_to_fixed', 'TempContractsController@send_to_fixed');
                Route::post('/error_contract', 'TempContractsController@error_contract');
                Route::post('/saveOnlyMessage', 'TempContractsController@saveOnlyMessage');

                Route::post('/accept_contract', 'TempContractsController@accept_contract');


                Route::get('/delete_contract', 'TempContractsController@delete_contract');

            });


        });


        Route::group(['prefix' => 'prolongation', 'namespace' => 'Prolongation'], function () {
            Route::get('/', 'ProlongationController@index');
            Route::get('/get_table', 'ProlongationController@get_table');
            Route::get('/get_table_custom', 'ProlongationController@get_table_custom');
            Route::get('/test_look', 'ProlongationController@index_custom');
            Route::get('/get_table_for_export', 'ProlongationController@get_table_for_export');
            Route::get('/{contract_id}', 'ProlongationController@contract');
        });


        Route::group(['prefix' => 'actions'], function () {

            Route::get('/get_models', 'ActionsController@get_models');
            Route::get('/classification', 'ActionsController@classification');

            Route::resource('/contract/{contract_id}/scans', 'ActionsScansController');
            Route::resource('/contract/{contract_id}/masks', 'ActionsMasksController');

            Route::get('/get_contract_info', 'ActionsController@get_contract_info');

            Route::post('/set_programs_list', 'ActionsController@set_programs_list');


            Route::post('/contract/{contract_id}/document', 'ActionsScansController@document');

            Route::get('/contract/{contract_id}/download_all_scans', 'ActionsScansController@download_all_scans');
            Route::get('/contract/{contract_id}/delete_scans_archive', 'ActionsScansController@delete_scans_archive');
            Route::get('/contract/{contract_id}/load-files-front', 'ActionsScansController@load_files_front');

            Route::get('/contract/{contract_id}/zip', 'ActionsScansController@download_zip');

            Route::post('/contract/{contract_id}/document/{document_id}', 'ActionsScansController@addDocument');
            Route::delete('/contract/{contract_id}/document/{document_id}', 'ActionsScansController@deleteDocument');

            Route::get('/contract/{contract_id}/set-check-user', 'ActionsController@setCheckUser');


            Route::post('/chat/{contract_id}/push', 'ActionsChatController@setPush');
            Route::get('/chat/{contract_id}/read', 'ActionsChatController@read');

            Route::get('/chat/{contract_id}/documents', 'ActionsChatController@documents');


            Route::get('/get_category_okp', 'ActionsController@get_category_okp');



        });


        Route::group(['prefix' => 'contracts_techunder', 'namespace' => 'Techunder'], function () {
            Route::get('/', 'TechunderController@index');
            Route::any('/get_table', 'TechunderController@get_table');
        });

        Route::group(['prefix' => 'downloading_contracts', 'namespace' => 'DownloadingContracts'], function () {
            Route::get('/', 'DownloadingContractsController@index');
            Route::post('/upload', 'DownloadingContractsController@uploadFile');

            Route::post('/load', 'DownloadingContractsController@loadData');


        });




    });

    Route::group(['prefix' => 'settings', 'namespace' => 'Settings'], function () {

        Route::get('financial_policy/copy', 'FinancialPolicyController@copy_template');
        Route::post('financial_policy/copy', 'FinancialPolicyController@copy');
        Route::resource('financial_policy', 'FinancialPolicyController');

        Route::resource('banks', 'BanksController');

        Route::resource('installment_algorithms_list', 'InstallmentAlgorithmsListController');
        Route::post('installment_algorithms_list/get_quantity', 'InstallmentAlgorithmsListController@get_quantity');

        Route::resource('pay_methods', 'PayMethodsController');


        Route::resource('incomes_expenses_categories', 'IncomeExpenseCategoryController');
        Route::post('incomes_expenses_cetegories_table', 'IncomeExpenseCategoryController@get_table');

        Route::resource('departments', 'DepartmentsController');
        Route::post('departments_table', 'DepartmentsController@get_table');


        Route::resource('filials', 'FilialsController');
        Route::resource('type_org', 'TypeOrgController'); // ТИП Организация

        Route::resource('citys', 'CityController'); // Города
        Route::resource('points_sale', 'PointsSaleController'); // Точки продаж


        Route::resource('salaries_states', 'SalariesStatesController');

        Route::resource('user_balance', 'UserBalanceController');

        Route::resource('departures_courier_price', 'DeparturesCourierPriceController');

        Route::resource('templates', 'TemplatesController');
        Route::post('/templates/get_table/', 'TemplatesController@get_table');

        Route::get('/system_project_features', 'SystemProjectFeaturesController@index');
        Route::post('/system_project_features', 'SystemProjectFeaturesController@action');
        Route::post('/system_project_features/any_login', 'SystemProjectFeaturesController@any_login');


        Route::get('/system', 'SettingsSystemController@index');
        Route::post('/system', 'SettingsSystemController@save');


        Route::group(['prefix' => 'system'], function () {
            Route::get('/integration', 'SettingsSystemController@addIntegration');
            Route::post('/integration', 'SettingsSystemController@addIntegration');

            Route::get('/integration/{id}', 'SettingsSystemController@editIntegration');
            Route::post('/integration/{id}', 'SettingsSystemController@editIntegration');
            Route::delete('/integration/{id}', 'SettingsSystemController@deleteIntegration');


            Route::get('/integration/{integration_id}/edit/{id}', 'SettingsSystemController@editVersion');
            Route::post('/integration/{integration_id}/edit/{id}', 'SettingsSystemController@editVersion');

            Route::get('/integration/{integration_id}/add_version/', 'SettingsSystemController@addVersion');
            Route::post('/integration/{integration_id}/add_version/', 'SettingsSystemController@addVersion');

            Route::delete('/integration/{integration_id}/delete/{id}', 'SettingsSystemController@deleteVersion');
            Route::get('/integration/{integration_id}/edit/{id}/main_form', 'SettingsSystemController@versionMainForm');
            Route::post('/integration/{integration_id}/edit/{id}/main_form', 'SettingsSystemController@versionMainForm');

        });

    });


    Route::group(['prefix' => 'log', 'namespace' => 'Log'], function () {
        Route::get('/events', 'LogEventsController@index');

    });


    Route::group(['prefix' => 'directories', 'namespace' => 'Directories'], function () {

        Route::resource('purpose_of_use', 'PurposeOfUse\PurposeOfUseController');

        Route::resource('categories', 'Categories\CategoriesController');

        Route::resource('anti_theft_devices', 'AntiTheftDevices\AntiTheftDevicesController');

        Route::resource('vehicle_colors', 'VehicleColors\VehicleColorsController');

        Route::get('/marks_and_models', 'MarksAndModels\MarksAndModelsController@marks_and_models');

        Route::group(['prefix' => 'delivery', 'namespace' => 'Delivery'], function () {
            Route::get('/', 'DeliveryController@index')->name('directories_delivery');
            Route::post('/', 'DeliveryController@save')->name('directories_delivery_post');

            Route::get('/create', 'DeliveryController@create')->name('directories_delivery_create');
            Route::post('/create', 'DeliveryController@create_new')->name('directories_delivery_create_new');

            Route::post('delete/{product_id}', 'DeliveryController@delete')->name('directories_delivery_delete');
            Route::post('update/{product_id}', 'DeliveryController@update')->name('directories_delivery_update');
            Route::get('edit/{product_id}', 'DeliveryController@edit')->name('directories_delivery_edit');
        });

        Route::group(['prefix' => 'avinfobot', 'namespace' => 'Avinfobot'], function () {
            Route::get('/', 'AvinfobotController@index')->name('avinfobot');
            Route::post('get_info', 'AvinfobotController@get_info')->name('get_info_avinfobot');
        });

        Route::group(['prefix' => 'marks_and_models', 'namespace' => 'MarksAndModels'], function () {

            Route::get('/mark/{mark_id}', 'MarksAndModelsController@show_models');
            Route::get('/mark/{mark_id}/edit', 'MarksAndModelsController@edit_mark');
            Route::post('/mark/{mark_id}/update_mark', 'MarksAndModelsController@update_mark');

            Route::get('/mark/{mark_id}/model/{model_id}/edit', 'MarksAndModelsController@edit_model');
            Route::post('/mark/{mark_id}/model/{model_id}/update_model', 'MarksAndModelsController@update_model');

            Route::get('/create', 'MarksAndModelsController@mark_create');
            Route::post('/save', 'MarksAndModelsController@mark_save');
            Route::post('/delete', 'MarksAndModelsController@mark_delete');


            Route::get('/mark/{mark_id}/create', 'MarksAndModelsController@model_create');
            Route::post('/mark/{mark_id}/save', 'MarksAndModelsController@model_save');
            Route::post('/mark/{mark_id}/delete', 'MarksAndModelsController@model_delete');
        });


        Route::group(['prefix' => 'organizations', 'namespace' => 'Organizations'], function () {

            Route::post('{id}/delete', 'OrganizationsController@delete');
            Route::get('{id}/documentation', 'OrganizationsController@documentation');

            Route::get('{id}/get_html_block', 'OrganizationsController@get_html_block');

            Route::resource('organizations', 'OrganizationsController');

            Route::get('organizations/{id}/add_document', 'OrganizationsController@document_template');
            Route::post('organizations/{id}/add_document', 'OrganizationsController@addDocument');

            Route::resource('org_bank_account', 'OrgBankAccountController');

            Route::resource('organizations/{org_id}/scans', 'ScansController');

            Route::post('organizations/{org_id}/get_users_table', 'OrganizationsController@get_users_table');

            Route::delete('organizations/{org_id}/delete_scans/{file_id}', 'ScansController@deleteScans');

            Route::delete('delete_scans/{file_id}', 'ScansController@deleteScans');

            Route::group(['prefix' => '{id}/point_department/{point_department_id}'], function () {
                Route::get('/', 'PointsDepartmentsController@edit');
                Route::post('/', 'PointsDepartmentsController@save');
                Route::delete('/', 'PointsDepartmentsController@delete');
            });
        });

        Route::resource('products', 'ProductsController');



        Route::group(['namespace' => 'ProductsPrograms', 'prefix' => 'products/{id}/edit/programs/{program}'], function () {
            Route::get('/', 'ProductsProgramsController@index');
            Route::post('/', 'ProductsProgramsController@save');

            Route::get('/get_api_programs', 'ProductsProgramsController@get_api_programs');

            Route::delete('/', 'ProductsProgramsController@destroy');
        });

        Route::group(['namespace' => 'InsuranceCompanies', 'prefix' => 'insurance_companies'], function () {

            Route::get('/', 'InsuranceCompaniesController@index');
            Route::post('/sort', 'InsuranceCompaniesController@sort');
            Route::get('/{id}', 'InsuranceCompaniesController@edit');
            Route::post('/{id}', 'InsuranceCompaniesController@save');

            Route::group(['namespace' => 'InstallmentAlgorithms', 'prefix' => '/{id}/installment_algorithms'], function () {
                Route::get('/{algorithm_id}', 'InstallmentAlgorithmsController@edit');
                Route::post('/{algorithm_id}', 'InstallmentAlgorithmsController@save');
                Route::delete('/{algorithm_id}', 'InstallmentAlgorithmsController@delete');
            });

            Route::group(['namespace' => 'BaseRates', 'prefix' => '/{id}/base_rates'], function () {
                Route::get('/add', 'BaseRatesController@add_form');
                Route::post('/add', 'BaseRatesController@add');
                Route::post('/{base_rate}/update/', 'BaseRatesController@update');
                Route::get('/{base_rate}/edit', 'BaseRatesController@edit');
                Route::post('/{base_rate}/delete', 'BaseRatesController@delete');
            });

            Route::group(['namespace' => 'TypeBso', 'prefix' => '/{id}/type_bso'], function () {
                Route::get('/{type_bso_id}', 'TypeBsoController@edit');
                Route::post('/{type_bso_id}', 'TypeBsoController@save');

                Route::group(['prefix' => '/{type_bso_id}/bso_serie'], function () {
                    Route::get('/{bso_serie_id}', 'BsoSerieController@edit');
                    Route::post('/{bso_serie_id}', 'BsoSerieController@save');
                });

                Route::group(['prefix' => '/{type_bso_id}/bso_serie/{bso_serie_id}/bso_dop_serie'], function () {
                    Route::get('/{bso_dop_serie_id}', 'BsoSerieController@dop_edit');
                    Route::post('/{bso_dop_serie_id}', 'BsoSerieController@dop_save');
                });

            });


            Route::group(['namespace' => 'BsoSuppliers', 'prefix' => '/{id}/bso_suppliers'], function () {

                Route::get('/{bso_supplier_id}', 'BsoSuppliersController@edit');
                Route::post('/{bso_supplier_id}', 'BsoSuppliersController@save');


                Route::get('/{bso_supplier_id}/hold_kv/{hold_kv_id}/get_table_group', 'HoldKvController@get_table_group');
                Route::resource('/{bso_supplier_id}/hold_kv', 'HoldKvController');

                Route::get('/{bso_supplier_id}/hold_kv/{hold_kv_id}/supplier_form', 'HoldKvController@supplier_form');
                Route::post('/{bso_supplier_id}/hold_kv/{hold_kv_id}/supplier_form', 'HoldKvController@supplier_form');


                Route::get('/{bso_supplier_id}/hold_kv/{hold_kv_id}/supplier_form/{version_id}', 'HoldKvController@supplier_form_edit');
                Route::post('/{bso_supplier_id}/hold_kv/{hold_kv_id}/supplier_form/{version_id}', 'HoldKvController@supplier_form_edit');


                Route::post('/{bso_supplier_id}/hold_kv/{hold_kv_id}/supplier_select_form', 'HoldKvController@supplier_select_form');


                Route::group(['namespace' => 'ProductsSettings', 'prefix' => '/{bso_supplier_id}/products_settings'], function () {
                    Route::post('/{product_id}', 'ProductsSettingsController@save');
                });

                Route::group(['namespace' => 'FinancialPolicy', 'prefix' => '/{bso_supplier_id}/financial_policy'], function () {
                    Route::get('/{financial_policy_id}', 'FinancialPolicyController@edit');
                    Route::post('/get_table', 'FinancialPolicyController@get_table');
                    Route::post('/{financial_policy_id}', 'FinancialPolicyController@save');

                    Route::group(['prefix' => '/{financial_policy_id}/segments'], function () {
                        Route::get('/{segment_id}', 'SegmentsController@edit');
                        Route::post('/{segment_id}', 'SegmentsController@save');
                    });
                });

                Route::group(['namespace' => 'Agent', 'prefix' => '/{bso_supplier_id}/agent'], function () {
                    Route::get('/', 'AgentController@agent_edit');
                    Route::post('/', 'AgentController@agent_save');
                    Route::post('/document', 'AgentController@addDocument');
                    Route::delete('/document', 'AgentController@deleteDocument');
                });

                Route::group(['namespace' => 'APISettings', 'prefix' => '/{bso_supplier_id}/api'], function () {
                    Route::get('/', 'APISettingsController@api_edit');
                    Route::post('/', 'APISettingsController@api_save');


                    Route::get('/vehicle_categories_sk', 'APIVehicleCategoriesController@edit');
                    Route::get('/vehicle_categories_sk/updata', 'APIVehicleCategoriesController@updata');
                    Route::post('/vehicle_categories_sk/save', 'APIVehicleCategoriesController@save');

                    Route::get('/marks_and_models', 'APIMarksModelController@marks_and_models');
                    Route::get('/marks_and_models/updata_mark', 'APIMarksModelController@updata_mark');

                    Route::get('/vehicle_colors', 'APIVehicleColorsController@vehicle_colors');
                    Route::get('/anti_theft_devices', 'APIAntiTheftDevicesController@anti_theft_devices');

                    Route::get('/mark/{mark_id}', 'APIMarksModelController@edit_models');
                    Route::post('/mark/{mark_id}/save', 'APIMarksModelController@save_models');
                    Route::get('/mark/{mark_id}/edit', 'APIMarksModelController@edit_mark');
                    Route::post('/mark/{mark_id}/save_mark', 'APIMarksModelController@save_mark');

                    Route::get('/vehicle_purpose_sk', 'APIVehiclePurposeController@edit');
                    Route::get('/vehicle_purpose_sk/updata', 'APIVehiclePurposeController@updata');
                    Route::post('/vehicle_purpose_sk/save', 'APIVehiclePurposeController@save');

                    Route::get('/dictionary_sks', 'APIDictionarySks@edit');
                    Route::get('/dictionary_sks/updata', 'APIDictionarySks@updata');
                    Route::post('/dictionary_sks/save', 'APIDictionarySks@save');


                });

            });
        });


        Route::group(['namespace' => 'ProductsInfo', 'prefix' => 'products/{product_id}/edit/info/'], function () {

            Route::get('/', 'ProductsInfoController@index');
            Route::get('/{type}/{id}/edit', 'ProductsInfoController@edit');
            Route::post('/{type}/{id}/edit', 'ProductsInfoController@save');
            Route::post('/sort', 'ProductsInfoController@sort');

            Route::delete('/{type}/delete/{id}', 'ProductsInfoController@destroy');

        });


    });

    Route::get('users/getCurrentUserData', 'HomeController@getCurrentUserData');
    Route::get('test', 'HomeController@test');
    //Route::get('test', 'HomeController@test');

    Route::group(['prefix' => 'users', 'namespace' => 'Users'], function () {

        Route::get('frame', 'UsersFrameController@frame');
        Route::post('frame', 'UsersFrameController@save');

        Route::get('limit', 'UsersFrameController@limit');
        Route::post('limit', 'UsersFrameController@save_limit');




        Route::get('users/get_table', 'UsersController@get_table');
        Route::resource('users', 'UsersController', ['except' => 'destroy']);
        Route::post('users/{user_id}/get_points_departments', 'UsersController@get_points_departments');


        Route::resource('roles', 'Roles\RolesController');

        Route::group(['prefix' => 'roles', 'namespace' => 'Roles'], function () {

            Route::get('{role_id}/permission/{permission_id}/subpermissions', 'SubpermissionController@index');
            Route::post('{role_id}/permission/{permission_id}/subpermissions', 'SubpermissionController@save');


        });

        Route::get('/{user_id}/generate_contract', 'UsersController@generate_contract');

        Route::post('actions/search_front_user/suggest/party', 'ActionsController@search_fron_users');
        Route::get('actions/search_front_user/status/party', 'ActionsController@get_response');
        Route::get('actions/search_front_user/detectAddressByIp', 'ActionsController@get_response');

        Route::group(['namespace' => 'Users', 'prefix' => 'users/{user_id}'], function () {

            Route::resource('scans', 'ScansController');
            Route::delete('delete_scans/{file_id}', 'ScansFileController@deleteScans');


            Route::group(['prefix' => 'agent_contracts', 'namespace' => 'AgentContracts'], function () {

                Route::get('{id}/edit', 'AgentContractsController@edit');
                Route::post('{id}/get_agent_contract', 'AgentContractsController@get_agent_contract');
                Route::post('download_agent_contract', 'AgentContractsController@download');
                Route::post('{id}/edit', 'AgentContractsController@save');

            });


            Route::group(['prefix' => 'agent_financial_policies', 'namespace' => 'AgentFinancialPolicies'], function () {

                Route::get('{id}/edit', 'AgentFinancialPoliciesController@edit');
                Route::post('{id}/edit', 'AgentFinancialPoliciesController@save');
                Route::delete('{id}/delete', 'AgentFinancialPoliciesController@delete');

            });

        });

        Route::group(['prefix' => 'notification', 'namespace' => 'Notification'], function () {

            Route::get('/', 'NotificationController@index');
            Route::get('get_table', 'NotificationController@get_table');


            Route::post('{id}/read', 'NotificationController@read');

        });


    });


    Route::group(['prefix' => 'enrichment', 'namespace' => 'Enrichment'], function () {

        Route::group(['prefix' => 'av100', 'namespace' => 'AV100'], function () {

            Route::get('/', 'AV100Controller@index');

            Route::post('/upload', 'AV100Controller@uploadFile');

            Route::post('/load', 'AV100Controller@loadData');

        });

        Route::group(['prefix' => 'm5_back_old', 'namespace' => 'M5BackOld'], function () {

            Route::get('/', 'M5BackOldController@index');

            Route::post('/start-info', 'M5BackOldController@startInfo');
            Route::post('/updata-info', 'M5BackOldController@updataInfo');
            Route::post('/clear-system', 'M5BackOldController@clearSystem');

        });

    });


    Route::group(['prefix' => 'account'], function () {
        Route::post('photo', 'Account\PhotoController@store');
    });


    Route::get('cron/planning', 'Cron\CronController@planning');


    Route::group(['prefix' => 'exports', 'namespace' => 'Exports'], function () {
        Route::get('table2excel', 'ExportsController@table2excel');
        Route::post('table2excel', 'ExportsController@Table2ExcelTempGenerate');
    });


    Route::get('cron/test', 'Cron\CronController@test');

    Route::get('test/{id}', 'Test\TestController@test');


    Route::group(['prefix' => 'atol/{atol_id}', 'namespace' => 'Payment'], function () {
        Route::get('/check', 'AtolCheckController@print_check');


    });

    Route::group(['prefix' => 'logging', 'namespace' => 'Log'], function(){
        Route::group(['prefix' => 'actions'], function(){
            Route::get('/', 'CudLogsController@actions');
            Route::get('get_table', 'CudLogsController@get_table');
            Route::get('get_context_id', 'CudLogsController@get_context_id');
        });
    });


});


Route::group(['prefix' => 'front', 'namespace' => 'FrontFrame'], function(){


    Route::get('/expected_payments', 'FrontFrameController@expected_payments');
    Route::get('/expected_payments/get_payments_table', 'FrontFrameController@get_payments_table');

    Route::post('/expected_payments/send-payments', 'FrontFrameController@sendPayments');






});



Route::get('cron/cubegroup', 'Cron\CronController@cubegroup');
Route::get('cron/myip', 'Cron\CronController@myip');
Route::post('cron/myip', 'Cron\CronController@myip');

//Route::get('cron/site', 'Cron\CronController@site');
Route::post('cron/site', 'Cron\CronController@site');


Route::resource('files', 'FilesController', ['only' => ['show', 'destroy']]);
Route::resource('mobile-file', 'FilesMobileController', ['only' => ['show']]);

Route::group(['prefix' => 'thumbs'], function () {
    Route::get('/{filename}', 'FilesController@thumb');
});


Route::post('/suggest/{type}', 'SuggestsController@suggest');

Route::group(['prefix' => '/custom_suggestions/{name}'], function () {
    Route::post('/suggest/{type}', 'CustomsuggestsController@suggest');
});

Route::post('/menu-settings', 'MenuSettingsController@update');


Route::group(['middleware' => 'non_dismissed'], function (){

    Auth::routes();

});

//Route::get('gpx/{order_id}', 'Orders\FactRoadsController@gpx');

Route::get('/', 'HomeController@index');
Route::get('/get_transaction_table', 'HomeController@get_transaction_table');

Route::post('/change_password', 'HomeController@change_password');

Route::get('user/themes', 'HomeController@getThemes');
Route::post('user/themes_change', 'HomeController@themeChange');
//Route::get('/get_form_change_password', function () {
    //return View::make('partials.change_password');
//});

Route::get('/get_mobile_act/{token_id}', 'Orders\OrderInspectionController@get_mobile_act');

Route::post('hooks', 'HooksController@store');


Route::group(['namespace' => 'Test', 'prefix' => 'test'], function () {
    Route::get('soap', 'SoapController@index');
    Route::get('cache', 'CacheController@index');
});

Route::get('/sk-partner-integration', 'Partners\IntegrationController@index');
Route::post('/sk-partner-integration', 'Partners\IntegrationController@index')->name('sk-partner-integration');

Route::get('/sk-partner-integration/add-token', 'Partners\IntegrationController@addToken');
Route::get('/sk-partner-integration/avdata-stats', 'Partners\IntegrationController@avdataStats');


