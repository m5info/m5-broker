<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// /api/mobile/
Route::group(['prefix' => 'mobile', 'namespace' => 'MobileInspectionAPI'], function () {

    Route::get('test', 'MobileInspectionController@index');

    Route::post('auth', 'MobileInspectionController@auth');
    Route::post('balance', 'MobileInspectionController@balance');
    Route::post('user-work', 'MobileInspectionController@userIsWork');
    Route::post('user-geo', 'MobileInspectionController@userGeo');

    Route::post('firebase', 'MobileInspectionController@firebase');


    Route::post('costs', 'MobileInspectionController@costs');
    Route::post('costs-category', 'MobileInspectionController@costsCategory');
    Route::post('costs-add', 'MobileInspectionController@costsAdd');


    Route::post('orders', 'MobileInspectionController@orders');

    Route::post('order', 'MobileInspectionController@order');
    Route::post('order-event', 'MobileInspectionController@orderEvent');

    Route::post('order-media', 'MobileInspectionController@getOrderMedia');
    Route::post('order-file', 'MobileInspectionController@orderFile');
    Route::post('order-file-delete', 'MobileInspectionController@orderFileDelete');



    Route::post('order-temple', 'MobileInspectionController@temple');
    Route::post('order-temple-save', 'MobileInspectionController@saveTemple');


    Route::post('directory', 'MobileInspectionController@directory');
    Route::post('save-auto-data', 'MobileInspectionController@saveAutoData');

    Route::post('order-chat-list', 'MobileInspectionController@orderChatList');
    Route::post('order-chat-send', 'MobileInspectionController@orderChatSend');


});

Route::group(['prefix' => 'policy', 'namespace' => 'PolicyAPI'], function () {

    Route::post('osago/tinkoff/confirm', 'SK\Osago\TinkoffConfirmPayment@handle');

    Route::post('auth', 'Auth@get_token');

    Route::post('get_dictionary', 'Dictionary@get_dictionary');

    Route::group(['prefix' => 'osago', 'namespace' => 'Osago'], function () {

        Route::post('calculate', 'CalculateApi@calculate');

    });

});


Route::group(['prefix' => 'electronic_cashier', 'namespace' => 'ElectronicCashier'], function () {

    Route::group(['prefix' => 'users', 'namespace' => 'Users'], function () {


        Route::post('/set_user_balance/', 'UsersApiController@set_user_balance');


        Route::get('/{id}/', 'UsersApiController@info');



    });

});

Route::group(['prefix'=>'integration_receiver', 'namespace' => 'IntegrationReceiversAPI'], function(){

    /* API-приемник для компании MUST */
    Route::group(['prefix' => 'must'], function(){
        Route::post('scoring/auth', 'ReceiverMUST@getToken');
        Route::post('scoring/init', 'ReceiverMUST@initProcess');
    });

    /* API-приемник для компании ВСК */
    Route::group(['prefix' => 'vsk'], function(){

        Route::group(['prefix' => 'response'], function(){
            /* Калькуляция */
            Route::post('calc_response', 'ReceiverVSK@calc_response');
            /* Сохранение полиса */
            Route::post('save_policy_response', 'ReceiverVSK@save_policy_response');
            /* Подтверждение полиса */
            Route::post('sign_response', 'ReceiverVSK@sign_response');
        });

        Route::get('show_responses', 'ReceiverVSK@show_responses');
        Route::get('clear_responses', 'ReceiverVSK@clear_responses');
    });
});
