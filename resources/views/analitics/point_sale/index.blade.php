@extends('layouts.app')



@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">Аналитика - Точки продаж</h1>
    </div>
    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="filter-group" id="filters">
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="result_table"></div>
        </div>
    </div>



@endsection



@section('js')
    <script>


        $(function(){

            loadItems();

        });


        function getData(){
            return {
                type_date:$('[name="type_date"]').val(),
                payment_type:$('[name="payment_type"]').val(),
                period:$('[name="period"]').val(),
                year:$('[name="year"]').val(),
                month:$('[name="month"]').val(),
                from:$('[name="from"]').val(),
                to:$('[name="to"]').val(),
            }
        }


        function loadItems(){
            loaderShow();
            $.post('/analitics/analitic_point_sale/get_filters', getData(), function(res){
                $('#filters').html(res);
                $('.select2-ws').select2("destroy").select2({
                    width: '100%',
                    dropdownCssClass: "bigdrop",
                    dropdownAutoWidth: true,
                    minimumResultsForSearch: -1
                });

                //Таблица
                loadTable();
            });

        }

        function loadTable(){


            $('#result_table').html('');


            $.get('/analitics/analitic_point_sale/get_table', getData(), function(res){


                $('#result_table').html(res);

            }).always(function(){
                loaderHide();
            });

        }


        function detailsPointSale(point_sale_id)
        {
            openFancyBoxFrame("/analitics/analitic_point_sale/"+point_sale_id+"/details?"+$.param(getData()));
        }

    </script>
@endsection