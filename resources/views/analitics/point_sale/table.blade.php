<table class="table table-striped table-bordered">
    <thead>
    <tr>

        <th>Точка</th>
        <th>Оборот</th>
        <th>КВ СК</th>
        <th>КВ Агента</th>
        <th>Маржа</th>
    </tr>
    </thead>
    <tbody>

    @php($all_payment_total = 0)
    @php($all_all_kv_total = 0)
    @php($all_agent_kv_total = 0)
    @php($all_marjing_total = 0)

    @if($payments)
        @foreach($payments as $payment)

            @php($all_payment_total += $payment->payment_total)
            @php($all_all_kv_total += $payment->all_kv_total)
            @php($all_agent_kv_total += $payment->agent_kv_total)
            @php($all_marjing_total += $payment->marjing_total)

            <tr style="cursor: pointer" @if($payment->point_sale_id) onclick="detailsPointSale('{{$payment->point_sale_id}}')" @endif>
                <td>{{$payment->point_sale_title}}</td>
                <td>{{titleFloatFormat($payment->payment_total)}}</td>
                <td>{{titleFloatFormat($payment->all_kv_total)}}</td>
                <td>{{titleFloatFormat($payment->agent_kv_total)}}</td>
                <td>{{titleFloatFormat($payment->marjing_total)}}</td>
            </tr>
        @endforeach
    @endif
    </tbody>

    <tfoot>
        <tr>
            <th>Итого</th>
            <th>{{titleFloatFormat($all_payment_total)}}</th>
            <th>{{titleFloatFormat($all_all_kv_total)}}</th>
            <th>{{titleFloatFormat($all_agent_kv_total)}}</th>
            <th>{{titleFloatFormat($all_marjing_total)}}</th>
        </tr>
    </tfoot>

</table>

