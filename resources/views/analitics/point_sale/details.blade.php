@extends('layouts.frame')


@section('title')

    Детализация

@endsection

@section('content')



    <table class="table table-bordered">
        <thead>
        <tr>

            <th>Агент</th>
            <th>Оборот</th>
            <th>КВ СК</th>
            <th>КВ Агента</th>
            <th>Маржа</th>
        </tr>
        </thead>
        <tbody>

        @php($all_payment_total = 0)
        @php($all_all_kv_total = 0)
        @php($all_agent_kv_total = 0)
        @php($all_marjing_total = 0)

        @if($payments)
            @foreach($payments as $payment)

                @php($all_payment_total += $payment->payment_total)
                @php($all_all_kv_total += $payment->all_kv_total)
                @php($all_agent_kv_total += $payment->agent_kv_total)
                @php($all_marjing_total += $payment->marjing_total)

                <tr>
                    <td align="left">{{$payment->agent_title}}</td>
                    <td>{{titleFloatFormat($payment->payment_total)}}</td>
                    <td>{{titleFloatFormat($payment->all_kv_total)}}</td>
                    <td>{{titleFloatFormat($payment->agent_kv_total)}}</td>
                    <td>{{titleFloatFormat($payment->marjing_total)}}</td>
                </tr>
            @endforeach
        @endif
        </tbody>

        <tfoot>
        <tr>
            <th align="left">Итого</th>
            <th>{{titleFloatFormat($all_payment_total)}}</th>
            <th>{{titleFloatFormat($all_all_kv_total)}}</th>
            <th>{{titleFloatFormat($all_agent_kv_total)}}</th>
            <th>{{titleFloatFormat($all_marjing_total)}}</th>
        </tr>
        </tfoot>

    </table>




@endsection

@section('footer')


@endsection


