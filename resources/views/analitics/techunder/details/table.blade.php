<table class="tov-table">
    <thead>
    <tr>
        <th>#</th>
        <th>#</th>
        <th>Статус</th>
        <th>СК</th>
        <th>БСО</th>
        <th>Продукт</th>
        <th>Страхователь</th>
        <th>Дата заключения</th>
        <th>Дата акцепта</th>
        <th>Период</th>
        <th>Агент</th>
        <th>Курьер</th>
        <th>Андеррайтер</th>
    </tr>
    </thead>
    <tbody>
        @foreach($contracts as $key => $contract)
            @php
                $fixed_status = false;
                $last_contract = $contract->logs->last();
                $temp_contracts = request()->route()->getPrefix() == 'contracts/temp_contracts';
                if($temp_contracts && $last_contract){
                    $fixed_status = $last_contract->status_id == 2 && $contract->statys_id == 1;
                }
            @endphp
            <tr class="{{ $fixed_status ? 'bg-yellow' : '' }}">
                <td>{{$key + 1}}</td>
                <td>{{$contract->id}}</td>
                <td>{{\App\Models\Contracts\Contracts::STATYS[$contract->statys_id]}}</td>{{-- ({{ $contract->kind_acceptance  ? 'Безусловный' : 'Условный'}})--}}
                <td>{{$contract->insurance_companies ? $contract->insurance_companies->title : ""}}</td>
                <td>{{$contract->bso ? $contract->bso->bso_title : ""}}</td>
                <td>{{$contract->bso && $contract->bso->product ? $contract->bso->product->title : ""}}</td>
                <td>{{($contract->insurer)?$contract->insurer->title:''}}</td>
                <td>{{setDateTimeFormatRu($contract->sign_date, 1)}}</td>
                <td>{{$contract->check_date ? setDateTimeFormatRu($contract->check_date, 1) : ""}}</td>
                <td>{{setDateTimeFormatRu($contract->begin_date, 1)}} - {{setDateTimeFormatRu($contract->end_date, 1)}}</td>
                <td>{{$contract->agent->name}}</td>
                <td>{{$contract->courier()}}</td>
                <td>{{$contract->check_user ? $contract->check_user->name : ''}}</td>
            </tr>
        @endforeach
    </tbody>
</table>