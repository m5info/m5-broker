@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">
@append

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h2 class="inline-h1">Акцептованные договора</h2>
            <a href="{{ back()->getTargetUrl() }}" class="btn btn-primary btn-right">Назад</a>
            <span id="analytics_techunder_export_xls" class="btn btn-success btn-right">Экспорт</span>
        </div>
        <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="filter-group" id="filters">
                        @include('analitics.techunder.filters', ['underwriters' => \App\Models\Characters\Underwriter::all(), 'agents' => \App\Models\Characters\Agent::all()])
                    </div>
                </div>
            </div>
        </div>

        <div class="block-main">
            <div class="block-sub">
                <div class="form-horizontal">
                    <div id="table">
                        @if(sizeof($contracts))
                            <table class="tov-table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Статус</th>
                                    <th>СК</th>
                                    <th>БСО</th>
                                    <th>Продукт</th>
                                    <th>Страхователь</th>
                                    <th>Дата заключения</th>
                                    <th>Дата акцепта</th>
                                    <th>Период</th>
                                    <th>Агент</th>
                                    <th>Курьер</th>
                                    <th>Андеррайтер</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($contracts as $contract)

                                    @php

                                        $fixed_status = false;
                                        $last_contract = $contract->logs->last();
                                        $temp_contracts = request()->route()->getPrefix() == 'contracts/temp_contracts';
                                        if($temp_contracts && $last_contract){
                                            $fixed_status = $last_contract->status_id == 2 && $contract->statys_id == 1;
                                        }

                                    @endphp
                                    <tr class="{{ $fixed_status ? 'bg-yellow' : '' }}">
                                        <td>{{$contract->id}}</td>
                                        <td>{{\App\Models\Contracts\Contracts::STATYS[$contract->statys_id]}}</td>{{-- ({{ $contract->kind_acceptance  ? 'Безусловный' : 'Условный'}})--}}
                                        <td>{{$contract->insurance_companies ? $contract->insurance_companies->title : ""}}</td>
                                        <td>{{$contract->bso ? $contract->bso->bso_title : ""}}</td>
                                        <td>{{$contract->bso && $contract->bso->product ? $contract->bso->product->title : ""}}</td>
                                        <td>{{($contract->insurer)?$contract->insurer->title:''}}</td>
                                        <td>{{setDateTimeFormatRu($contract->sign_date, 1)}}</td>
                                        <td>{{$contract->check_date ? setDateTimeFormatRu($contract->check_date, 1) : ""}}</td>
                                        <td>{{setDateTimeFormatRu($contract->begin_date, 1)}} - {{setDateTimeFormatRu($contract->end_date, 1)}}</td>
                                        <td>{{$contract->agent ? $contract->agent->name : ''}}</td>
                                        <td>{{$contract->courier()}}</td>
                                        <td>{{$contract->check_user ? $contract->check_user->name : ''}}</td>


                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('_chunks._pagination',['callback'=>'loadItems'])

@endsection

@section('js')
    <script>

        $(function () {
           loadItems();
        });
        function getData(is_export = 0){
            return {
                underwriter_id:$('[name="underwriter_id"]').val(),
                product_ids:$('[name="product_ids[]"]').val(),
                from:$('[name="from"]').val(),
                to:$('[name="to"]').val(),
                sk:$('[name="sk"]').val(),
                insurer:$('[name="insurer"]').val(),
                agent_id:$('[name="agent_id"]').val(),
                is_export: is_export,


                page_count: $('[name="page_count"]').val(),
                PAGE: PAGE,
            }
        }

        $(document).on('click', '#analytics_techunder_export_xls', function () {
            var data = getData(1);
            var query = $.param({ method: 'Analitics\\Techunder\\TechunderAnalyticsController@get_table', param: data});
            location.href = '/exports/table2excel?'+query;
        });

        function loadItems(resetPage = false){

            if(resetPage){
                PAGE = 1
            }

            $('#page_list').html('');
            $('#table_row').html('');
            $('#view_row').html('');
            $('#max_row').html('');

            loaderShow();

            $.post('/analitics/analytics_techunder/get_table', getData(), function(table_res){

                $('#table').html(table_res.html);

                $('#view_row').html(table_res.view_row);
                $('#max_row').html(table_res.max_row);


                ajaxPaginationUpdate(table_res.page_max,loadItems);


            }).always(function(){
                loaderHide();
            });


            loaderHide();

        }

    </script>
@endsection