@php
    $agents = \App\Models\Characters\Agent::all();
@endphp
<div class="row">
    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <label class="control-label">Андеррайтеры</label>
        {{ Form::select('underwriter_id', $underwriters->pluck('name', 'id')->prepend('Все', '0'), request('underwriter_id', 0), ['class' => 'form-control select2-ws', 'onchange' => 'loadItems()']) }}
    </div>
    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <label class="control-label">Агент</label>
        {{ Form::select('agent_id', $agents->pluck('name', 'id')->prepend('Все', '0'), request('agent_id', 0), ['class' => 'form-control select2-ws', 'onchange' => 'loadItems()']) }}
    </div>
    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <label class="control-label">Продукт</label>
        @php
            $products = \App\Models\Directories\Products::query()->where('is_actual', 1)->get();
            $products->map(function ($item){
                $item->title = mb_substr($item->title, 0, 100);
                return $item;
            });
        @endphp
        {{ Form::select('product_ids[]', $products->pluck('title', 'id')->prepend('Все', 0), request('product_ids') ?? '', ['class' => 'form-control select2-all', 'onchange' => 'loadItems()', 'multiple' => true]) }}
    </div>
    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <label class="control-label">Страхователь</label>
        {{ Form::text('insurer', request('insurer'), ['class' => 'form-control', 'onchange' => 'loadItems()']) }}
    </div>
</div>


<div class="row">
    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <label class="control-label" for="sk">СК</label>
        {{ Form::select('sk', \App\Models\Directories\InsuranceCompanies::where('is_actual', 1)->pluck('title', 'id')->prepend('Все', 0), request('sk', 0), ['class' => 'form-control select2-all',  'onchange'=>'loadItems()']) }}
    </div>
    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <label class="control-label" for="from">C</label>
        {{ Form::text('from', request('from', date('d.m.Y', time()-60*60*24*30)), ['class' => 'form-control datepicker date', 'onchange' => 'loadItems()']) }}
    </div>
    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <label class="control-label" for="to">По</label>
        {{ Form::text('to', request('to', date('d.m.Y')), ['class' => 'form-control datepicker date', 'onchange' => 'loadItems()']) }}
    </div>
</div>

