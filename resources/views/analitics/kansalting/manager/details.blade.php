<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
    <span class="btn btn-info pull-left" onclick="closeDetails()">
        Назад
    </span>
</div>
<table class="table table-striped table-bordered">


    <tr style="background-color: #ddd">
        <td>№ кон.</td>
        <td>Время посл. звонка</td>
        <td>Время след. звонка</td>
        <td>Дата дос. результата</td>
        <td>Дата реализации</td>
        <td>Менеджер</td>
        <td>Клиент</td>
        <td>Вид страхования</td>
        <td>Объект страхования</td>
        <td>Источник</td>
        <td>Результат</td>
        <td>Примечание (Причина отказа)</td>
        <td>Статус полиса</td>
        <td>Страховая компания</td>
        <td>Сумма страхового сбора</td>
        <td>Прибыль</td>
    </tr>



    @foreach($results as $result)

        <tr style="cursor: pointer;" onclick="openOrderRit('{{$result->guid}}')">
            <td>{{(int)titleFloatFormat((int)$result->num)}}</td>
            <td>{{setDateTimeFormatRu($result->date_last_phone)}}</td>
            <td>{{setDateTimeFormatRu($result->date_phone)}}</td>
            <td>{{setDateTimeFormatRu($result->date_res)}}</td>
            <td>{{setDateTimeFormatRu($result->date_deliveri)}}</td>
            <td>{{$result->manager}}</td>
            <td>{{$result->client}}</td>
            <td>{{$result->product}}</td>
            <td>{{$result->obj_insh}}</td>
            <td>{{$result->source}}</td>
            <td>{{$result->status}}</td>
            <td>{{$result->not_work}}</td>
            <td>{{$result->contract_status}}</td>
            <td>{{$result->sk_name}}</td>
            <td>{{titleFloatFormat($result->payment_total)}}</td>
            <td>{{titleFloatFormat($result->payment_manager)}}</td>
        </tr>

    @endforeach

</table>