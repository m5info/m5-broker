@extends('layouts.app')



@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">Аналитика - менеджеры</h1>
    </div>
    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="filter-group" id="filters">



                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-2">
                        <label class="control-label" for="from">C</label>
                        {{ Form::text('date_from', request('from', date('d.m.Y')), ['class' => 'form-control datepicker date']) }}
                    </div>
                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-2">
                        <label class="control-label" for="to">По</label>
                        {{ Form::text('date_to', request('to', date('d.m.Y')), ['class' => 'form-control datepicker date']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-2">
                        <label class="control-label" for="to">Вид страхования</label>
                        {{ Form::select('products', collect([-1=>'Все','КАСКО'=>'КАСКО','КАСКО + ОСАГО'=>'КАСКО + ОСАГО','ОСАГО'=>'ОСАГО']) , -1, ['class' => 'form-control select2-ws']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-2">
                        <label class="control-label" for="to">Группа</label>
                        {{ Form::select('department[]', collect($department) , [], ['class' => 'form-control select2-all', 'multiple' => true]) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-2">
                        <label class="control-label" for="to">Источник</label>
                        {{ Form::select('source[]', collect($source) , [], ['class' => 'form-control select2-all', 'multiple' => true]) }}
                    </div>

                    <br/>

                    <div class="row">
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
                        <span class="btn btn-success pull-left" onclick="loadItems()">
                            Применить
                        </span>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="result_table"></div>
            <div id="result_details"></div>
        </div>
    </div>



@endsection



@section('js')
    <script>


        $(function(){

            loadItems();

        });


        function getData() {
            return {
                date_from:$('[name="date_from"]').val(),
                date_to:$('[name="date_to"]').val(),
                products:$('[name="products"]').val(),
                department:$('[name="department[]"]').val(),
                source:$('[name="source[]"]').val(),
            }
        }


        function loadItems(){
            loaderShow();
            $("#result_details").html('');
            $("#result_table").show();
            $.post('/analitics/k_manager/get-data', getData(), function(res){

                $("#result_table").html(res);

            }).always(function(){
                loaderHide();
            });
        }


        function getDetailsManager(user_id, status) {


            loaderShow();
            $.post('/analitics/k_manager/get-details/'+user_id+'/'+status, getData(), function(res){

                $("#result_details").html(res);
                $("#result_table").hide();

            }).always(function(){
                loaderHide();
            });



        }

        function closeDetails() {
            $("#result_details").html('');
            $("#result_table").show();
        }


        function openOrderRit(guid) {


            var strWindowFeatures = "location=yes,height=570,width=1020,scrollbars=yes,status=yes";
            var URL = "http://192.168.2.18/k/DefaultFrameKonsalting/ViewKonsaltingResultItem.aspx?id_node=" + guid;
            var win = window.open(URL, "_blank", strWindowFeatures);

            //window.open('http://192.168.2.18/k/DefaultFrameKonsalting/ViewKonsaltingResultItem.aspx?id_node='+guid);


        }


    </script>
@endsection