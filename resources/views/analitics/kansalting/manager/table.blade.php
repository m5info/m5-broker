<table class="table table-striped table-bordered">


    @php($AllOrder = 0)
    @php($Work = 0)
    @php($NotWork = 0)
    @php($FinishWork = 0)


    @php($group_name = '')
    @foreach($results as $result)

        @if($group_name != $result->manager_dep)
            <tr>
                <td colspan="5"><b>{{$result->manager_dep}}</b></td>
            </tr>

            <tr style="background-color: #ddd">
                <td>Менеджер</td>
                <td>Полученно заявок</td>
                <td>В работе</td>
                <td>Отказ</td>
                <td>Реализованно</td>
            </tr>
        @endif

            @php($AllOrder += (int)$result->AllOrder)
            @php($Work += (int)$result->Work)
            @php($NotWork += (int)$result->NotWork)
            @php($FinishWork += (int)$result->FinishWork)

        <tr>
            <td>{{$result->magager_title}}</td>
            <td style="cursor: pointer;" onclick="getDetailsManager({{$result->user_id}}, 'AllOrder')">{{$result->AllOrder}}</td>
            <td style="cursor: pointer;" onclick="getDetailsManager({{$result->user_id}}, 'Work')">{{$result->Work}}</td>
            <td style="cursor: pointer;" onclick="getDetailsManager({{$result->user_id}}, 'NotWork')">{{$result->NotWork}}</td>
            <td style="cursor: pointer;" onclick="getDetailsManager({{$result->user_id}}, 'FinishWork')">{{$result->FinishWork}}</td>


        </tr>


        @php($group_name = $result->manager_dep)
    @endforeach

    <tr>
        <td colspan="5"><b>Итого</b></td>
    </tr>

    <tr>
        <td></td>
        <td>{{titleFloatFormat($AllOrder)}}</td>
        <td>{{titleFloatFormat($Work)}}</td>
        <td>{{titleFloatFormat($NotWork)}}</td>
        <td>{{titleFloatFormat($FinishWork)}}</td>
    </tr>


</table>