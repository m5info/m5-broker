@extends('layouts.frame')

@section('content')

    <div style="height: 500px;">
        @if(sizeof($files))
            @foreach($files as $key => $delivery)
                @php
                $file = $delivery->file;
                @endphp

                @if (in_array($file->ext, ['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG']))

                <div style="display: none; max-height: 500px" id="fs-galery{{$key}}">
                    <a target="_blank" href="{{ $file->url }}">
                        <img style="max-height: 500px;margin: 0 auto;" class="fs-galery"
                             id="fs-galery{{$key}}" src="{{ $file->url }}"
                             onerror="this.onerror=null;this.src='/images/extensions/unknown.png';">
                    </a>
                    <p class="file_name col-xs-12 mt-15 text-center">
                        {{ $file->original_name }}
                        <br>
                        <a href="{{ $file->url }}" target="_blank">Открыть оригинал</a>
                        <a href="{{ $file->url }}" download>Скачать</a>

{{--                        <a id="rotate_right" href="#"><img src="/images/rotate_right.png" alt=""></a>--}}
{{--                        <a id="rotate_left" href="#"><img src="/images/rotate_left.png" alt=""></a>--}}
                    </p>
                </div>

                <a class="fancybox iframe" href="#fs-galery{{$key}}" rel="galery" style="padding: 0 10px;">
                    <img class="media-object preview-image"  style="margin-bottom: unset;"src="{{url($file->url)}}">
                </a>
                <a onclick="deleteDoc({{$file->id}})" style="cursor: pointer; position: relative; bottom: 90px; right: 10px;">x</a>

                @else

                    <div style="display: none; max-height: 500px" id="fs-galery{{$key}}">
                        <a target="_blank"  href="{{ $file->url }}">
                            <img style="max-height: 150px;margin: 0 auto;" class="fs-galery"
                                 id="fs-galery{{$key}}" src="/images/extensions/{{mb_strtolower($file->ext)}}.png"
                                 onerror="this.onerror=null;this.src='/images/extensions/unknown.png';">
                        </a>
                        <p class="file_name col-xs-12 mt-15 text-center">
                            <a href="{{ $file->url }}" download>{{ $file->original_name }}</a>
                        </p>
                    </div>

                    <a class="iframe" rel="group" href="#fs-galery{{$file->id}}" rel="group" style="padding: 0 10px;">
                        <img class="media-object preview-icon"
                             src="/images/extensions/{{mb_strtolower($file->ext)}}.png">
                    </a>
                    <a onclick="deleteDoc({{$file->id}})" style="cursor: pointer; position: relative; bottom: 90px; right: 10px;">x</a>

                    <p class="file_name col-xs-12 mt-15">
                        <a href="{{ $file->url }}" download>{{ $file->original_name }}</a>
                    </p>

                @endif


            @endforeach

        @else
            <div>
                <h1>Нет файлов</h1>
            </div>
        @endif
            {!! Form::open(['url'=> route('upload_file_delivery').'?delivery_id='.$delivery_id,'method' => 'post', 'class' => 'dropzone_uploadFileDelivery', 'id' => 'uploadFileDelivery']) !!}
            <div class="dz-message" data-dz-message>
                <p>Перетащите сюда файлы</p>
                <p class="dz-link">или выберите с диска</p>
            </div>
            {!! Form::close() !!}

            <style>
                .dropzone_uploadFileDelivery{
                    border: 1px solid black;
                    padding: 50px;
                }
                .dflexwrap{
                    display: flex;
                    flex-wrap: wrap;
                    padding: 0!important;
                }

                .dz-message{
                    width: 100%;
                }
                .dz-success-mark, .dz-error-mark, .dz-size{
                    display: none;
                }
                .dz-preview{
                    padding: 1em;
                }
            </style>
    </div>


@endsection

@section('js')

    <script>

        var arrDoc = [];

        var myDropzone;

        $(function() {

            Dropzone.options.uploadFileDelivery = {
                /*autoProcessQueue: false,*/
                addRemoveLinks: true
            };
            Dropzone.prototype.defaultOptions.dictRemoveFile = "Удалить";

            myDropzone = new Dropzone("#uploadFileDelivery");

            myDropzone.on("drop", function(file) {
                $('.dz-message').addClass('hide')
                $('#uploadFileDelivery').addClass('dflexwrap')
                setTimeout(function (){
                    window.parent.$('.fancybox-inner').height($('html').height())
                }, 200)

            });
            myDropzone.on("queuecomplete", function(file) {
                location.reload();
            });


            myDropzone.on("reset", function(file) {
                $('.dz-message').removeClass('hide')
                $('#uploadFileDelivery').removeClass('dflexwrap')
                setTimeout(function (){
                    window.parent.$('.fancybox-inner').height($('html').height())
                }, 200)
            });
        })

        function deleteDoc(file_id){
            Swal.fire({
                title: 'Подтвердите действие!',
                text: "Уверены что хотите удалить данный файл?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Да',
                cancelButtonText: 'Нет'
            }).then((result) => {
                if (result.isConfirmed) {

                    $.post('{{route('delete_delivery_doc')}}', {file_id: file_id}, function (){
                        Swal.fire(
                            'Файл удалён!',
                            '',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                location.reload()
                            }
                        })
                    })
                }
            })
        }

    </script>
@endsection
