@extends('layouts.frame')


@section('title')

    Добавление выезда

@endsection

@section('content')

    <div style="/*height: 350px;*/">
        <div class="col-sm-12 col-lg-12">
            <label class="control-label">Эксперт</label>
            {{ Form::select('expert_id', $expert, 0, ['class'=>'form-control select2-all']) }}
        </div>

        <div class="col-sm-6 col-lg-6">
            <label class="control-label">Менеджер</label>
            {{ Form::select('agent_id', $users, 0, ['class'=>'form-control select2-all']) }}
        </div>

        <div class="col-sm-6 col-lg-6">
            <label class="control-label">ФИО Клиента</label>
            {{ Form::text('insurer_title', '', ['class' => 'form-control']) }}
        </div>

        <div class="col-sm-6 col-lg-6">
            <label class="control-label">Продукт</label>
            {{ Form::select('product_id', $product, 0, ['class'=>'form-control select2-all']) }}
        </div>

        <div class="col-sm-6 col-lg-6">
            <label class="control-label">Дата</label>
            {{ Form::text('date', date('d.m.Y'), ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off', 'id' => 'date']) }}
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12">
            <input type="button" class="btn btn-success pull-right" value="Начислить" onclick="addNewDelivery();">
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            {!! Form::open(['url'=> route('upload_file_delivery'),'method' => 'post', 'class' => 'dropzone_uploadFileDelivery', 'id' => 'uploadFileDelivery']) !!}
            <div class="dz-message" data-dz-message>
                <p>Перетащите сюда файлы</p>
                <p class="dz-link">или выберите с диска</p>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <style>
        .dropzone_uploadFileDelivery{
            border: 1px solid black;
            padding: 50px;
        }
        .dflexwrap{
            display: flex;
            flex-wrap: wrap;
            padding: 0!important;
        }
        .dz-message{
            width: 100%;
        }
        .dz-success-mark, .dz-error-mark, .dz-size{
            display: none;
        }
        .dz-preview{
            padding: 1em;
        }
    </style>


    <script>

        var arrDoc = [];

        var myDropzone;

        var emptyDropzone = 0;


        /** Вычисляем дату до которой можно **/
        @php
            $date = new DateTime();
            $date->modify('-3 month');
        @endphp
        var succsessDate = "{{$date->format('m-d-Y')}}";
        /** **/

        $(function() {

            $('.datepicker').datepicker({
                minDate: new Date("{{$date->format('m-d-Y')}}")/*,
                minDate: new Date(succsessDate)/*,
                maxDate: new Date('03/05/2015'),
                setDate: new Date('30/11/2014')*/
            });

            Dropzone.options.uploadFileDelivery = {
                autoProcessQueue: false,
                addRemoveLinks: true
            };
            Dropzone.prototype.defaultOptions.dictRemoveFile = "Удалить";

            myDropzone = new Dropzone("#uploadFileDelivery");

            myDropzone.on("drop", function(file) {
                emptyDropzone++;
                $('.dz-message').addClass('hide')
                $('#uploadFileDelivery').addClass('dflexwrap')
                setTimeout(function (){
                    window.parent.$('.fancybox-inner').height($('html').height())
                }, 200)
            });

            myDropzone.on("removedfile", function(file) {
                emptyDropzone--;
            });

            myDropzone.on("complete", function(file) {

                arrDoc.push((JSON.parse(file.xhr.response)).file)

            });

            myDropzone.on("reset", function(file) {
                $('.dz-message').removeClass('hide')
                $('#uploadFileDelivery').removeClass('dflexwrap')
                setTimeout(function (){
                    window.parent.$('.fancybox-inner').height($('html').height())
                }, 200)
            });
        })

        function addNewDelivery(){

            if(emptyDropzone == 0){
                Swal.fire(
                    'Ошибка!',
                    'Необходимо добавить файлы!',
                    'error'
                )
                return
            }

            myDropzone.processQueue();

            myDropzone.on("queuecomplete", function(file) {
                sendDelivery()
            });
        }

        function sendDelivery(){

            /**     Внимание говнокод =))
             * Вычисляем дату которую внесли
             */
            var setDay = $('#date').val()[0]+''+$('#date').val()[1]
            var setMounth = $('#date').val()[3]+''+$('#date').val()[4]
            if($('#date').val().length == 10){
                var setYear = $('#date').val()[6]+''+$('#date').val()[7]+''+$('#date').val()[8]+''+$('#date').val()[9]
            }else if($('#date').val().length == 8){
                var setYear = '20'+$('#date').val()[6]+''+$('#date').val()[7]
            }
            var setDate = new Date(setMounth+'.'+setDay+'.'+setYear)
            var Days = Math.floor((setDate.getTime() - new Date(succsessDate).getTime())/(1000*60*60*24));
            /** **/

            if(Days >= 0){
                var date = $('.form-control').serialize()

                $.post("{{route('add_new_delivery')}}", {data_form: date, arrDoc: arrDoc}, function (res) {
                    if(res.result){
                        console.log(res.result);
                        Swal.fire({
                            icon: 'success',
                            title: 'Отлично!',
                            text: 'Выезд добавлен!',
                            preConfirm: () => {
                                window.parent.jQuery.fancybox.close();
                            }
                        })
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Ошибка!',
                            text: 'Чт-то пошло не так! Попробуй еще раз!',
                            preConfirm: () => {
                                window.parent.jQuery.fancybox.close();
                            }
                        })
                    }
                });
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Ошибка!',
                    text: 'Проверь дату!'
                })
            }
        }

    </script>

@endsection

