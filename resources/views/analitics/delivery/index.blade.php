@extends('layouts.app')

@section('content')

    @php
        $days = [
                    0 => 'Все',
                    1 => 'Будни',
                    2 => 'Выходные'
                ];
    @endphp

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h1 class="inline-h1">Аналитика выездов</h1>
            <button class="btn btn-primary btn-right" onclick="getFreshWorkedDay();">Обновить кол-во отработанных дней</button>
            <input type="submit" id="print_analytics_delivery" class="btn btn-success btn-right" value="Печать" name="print">
            <button class="btn btn-primary btn-right" onclick="addDelivery();">Добавить выезд</button>
        </div>


        <form name="analitics_common" id="analitics_common_form">
            <div class="block-main">
                <div class="block-sub">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <h2 style="padding-left: 30px;">Фильтр</h2>

                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <div class="row">
                                    <div class="col-sm-12 col-lg-12">
                                        <label class="control-label">Агент</label>
                                        {{ Form::select('agent_id', \App\Models\User::all()->pluck('name', 'id')->prepend('Все', -1), -1, ['class'=>'form-control select2-all']) }}
                                    </div>
                                    <div class="col-sm-12 col-lg-12">
                                        <label class="control-label">Руководитель</label>
                                        {{ Form::select('parent_id', \App\Models\User::where('role_id', 6)->get()->pluck('name', 'id')->prepend('Все', -1), -1, ['class'=>'form-control select2-all']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <div class="row">

                                    <div class="col-sm-12 col-lg-12">
                                        <label class="control-label">Дни</label>
                                        {{ Form::select('days', collect($days), 0, ['class' => 'form-control select2-all']) }}
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <label class="control-label">Месяц</label>
                                        {{ Form::select('mounth', collect(getRuMonthes()), date('m'), ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off', 'id' => 'data_mounth']) }}
                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        <label class="control-label">Год</label>
                                        {{ Form::select('year', collect(getYearsArrey()), date('Y'), ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off', 'id' => 'data_year']) }}
                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12" style="padding-top: 1em;">
                                <input type="button" class="btn btn-success" value="Применить фильтры" onclick="loadItems();">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div id="table"></div>

    </div>

@endsection

@section('js')


    <script>

        $(function () {

            loadItems();

        });

        function loadItems(){

            loaderShow();

            $.post('{{route('dilevery_analitics_get_table')}}', getData(), function(table_res){

                $('#table').html(table_res.html);

            }).always(function(){
                loaderHide();
            });


            loaderHide();

        }

        function getData(){
            return $('#analitics_common_form').serialize()
        }

        function openDetailUsers(user_id){
            mounth = $('#data_mounth').val();
            year = $('#data_year').val();

            $.post('{{url('/analitics/delivery/details')}}', {user: user_id, mounth: mounth, year: year}, function (res){
                $('#main_delivery_table').hide()
                $('#detail_table').html(res)
            })

            return true;
        }

        function getFreshWorkedDay(){

            loaderShow();

            mounth = $('#data_mounth').val();
            year = $('#data_year').val();

            data = {
                mounth:mounth,
                year:year,
            }
            $.ajax({
                type: "POST",
                url: "{{route('get_fresh_worked_day')}}",
                data: data,
                success: function (respons){

                    loaderHide();
                    loadItems();
                }
            });
        }

        function addDelivery(){
            openFancyBoxFrame('/analitics/delivery/new_delivery');
        }

        $(document).on('click', '#print_analytics_delivery', function () {
            $.post('/exports/table2excel', {param:getDataforExel(), method: 'Analitics\\Delivery\\DeliveryAnaliticsController@get_delivery_table_to_excel'}, function(link){
                location.href = link;
            });
        });

        function getDataforExel() {
            return {
                agent_id: $('[name="agent_id"]').val(),
                parent_id: $('[name="parent_id"]').val(),
                days: $('[name="days"]').val(),
                mounth: $('[name="mounth"]').val(),
                year: $('[name="year"]').val(),
                // PAGE: PAGE,
            }
        }

        const getSort = ({ target }) => {
            const order = (target.dataset.order = -(target.dataset.order || -1));
            const index = [...target.parentNode.cells].indexOf(target);
            const collator = new Intl.Collator(['en', 'ru'], { numeric: true });
            const comparator = (index, order) => (a, b) => order * collator.compare(
                a.children[index].innerHTML,
                b.children[index].innerHTML
            );

            for(const tBody of target.closest('table').tBodies)
                tBody.append(...[...tBody.rows].sort(comparator(index, order)));

            for(const cell of target.parentNode.cells)
                cell.classList.toggle('sorted', cell === target);
        };

    </script>


@endsection