<style>
    th{
        cursor: pointer;
    }
    th:hover{
        color: red;
    }
</style>

    <h3>{{$user->name}}</h3>

    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-1">
        <span class="btn btn-info" onclick="closeDetails()">
            Назад
        </span>
    </div>

    <input type="submit" id="print_analytics_delivery" class="btn btn-success /*btn-left*/" value="Печать" name="print">

    <div style="display: flex; padding: 10px 0;">
        <div style="padding: 0 10px; background-color: #F0E68C;">Электронное ОСАГО</div>
        <div style="padding: 0 10px; background-color: #ffcccc;">Выезд в выходной, а деньги сдали в будни</div>
        <div style="padding: 0 10px; background-color: #DDF;">Бесплатный выезд</div>
        <div style="padding: 0 10px; background-color: #ddfdff;">Договор развязан</div>
    </div>

    <table class="table-bordered table ">
        <thead class="table_sort">
        <tr>
            <th>№</th>
            <th>Дата выезда</th>
            <th>Дата в кассу</th>
            <th>Продукт</th>
            <th>Договор</th>
            <th>Дата заключения</th>
            <th>Дата начала</th>
            <th>Менеджер</th>
            <th>Страхователь</th>
            <th>Сумма платежа</th>
            <th>КВ агента</th>
            <th>День</th>
            <th>Сумма</th>
            <th>Обнулить выезд</th>
            <th>Файлы</th>
        </tr>
        </thead>
        <tbody>

        @php
        function getColorDelivery($delivery){
            $color = '#FFF';
            if($delivery->payment && $delivery->payment->bso && $delivery->payment->bso->bso_class_id == 1 && $delivery->payment->bso->product_id == 3){
                $color = '#F0E68C';
            }
            if($delivery->payment && $delivery->payment->invoice_payment_date && $delivery->delivery_date > $delivery->payment->invoice_payment_date && $delivery->delivery_cost > 0){
                $color = '#ffcccc';
            }
            if( $delivery->delivery_cost == 0 ){
                $color = '#DDF!important';
            }
            if( $delivery->status == 2 ){
                $color = '#ddfdff!important';
            }
            return $color;
        }
        @endphp
        @if(sizeof($deliverys))
            @foreach($deliverys as $key => $delivery)
                @php

                if($delivery->contract && $delivery->contract->insurer){
                    $insurer = $delivery->contract->insurer->title;
                }elseif(isset($delivery->insurer_title)){
                    $insurer = $delivery->insurer_title;
                }else{
                    $insurer = '';
                }

                if($delivery->contract && $delivery->contract->manager){
                    $agent_name = $delivery->contract->manager->name;
                }elseif(isset($delivery->agent_id)){
                    $agent_name = $delivery->agent_name;
                }else{
                    $agent_name = '';
                }

                @endphp
                <tr style="background-color: {{getColorDelivery($delivery)}};">
                    <td nowrap> {{ $key+1 }}</td>
                    <td nowrap>{{getDateFormatRu($delivery->delivery_date)}}</td>
                    <td nowrap>{{($delivery->payment) ? getDateFormatRu($delivery->payment->invoice_payment_date) : ''}}</td>
                    <td nowrap>{{(isset($delivery->product_id)) ? $delivery->product->product_title : $delivery->contract->product->title}}</td>
                    <td nowrap><a href="{{($delivery->contract && $delivery->contract->bso) ? url("/bso/items/{$delivery->contract->bso_id}/") : ''}}" target="_blank">{{($delivery->contract && $delivery->contract->bso) ? $delivery->contract->bso->bso_title : ''}}</a></td>
                    <td nowrap>{{($delivery->contract) ? getDateFormatRu($delivery->contract->sign_date) : ''}}</td>
                    <td nowrap>{{($delivery->contract) ? getDateFormatRu($delivery->contract->begin_date) : ''}}</td>
                    <td nowrap>{{$agent_name}}</td>
                    <td nowrap>{{$insurer}}</td>
                    <td nowrap>{{($delivery->payment) ? titleFloatFormat($delivery->payment->payment_total) : ''}}</td>
                    <td nowrap>{{($delivery->payment) ? ( ($delivery->status == 2) ? 0 : titleFloatFormat($delivery->payment->financial_policy_kv_agent_total) ) : ''}}</td>
                    <td nowrap>{{\App\Models\Delivery\Delivery::WEEKEND[$delivery->weekend]}}</td>
                    <td nowrap>{{titleFloatFormat($delivery->delivery_cost)}}</td>
                    @if(!isset($delivery->product_id))
                        @if( $delivery->delivery_cost > 0 )
                            <td nowrap style="cursor: pointer;" class="red" onclick="setZeroCostDelivery({{$delivery->id}})">Обнулить?</td>
                        @else
                            <td nowrap style="cursor: pointer;" class="red" onclick="setCostDelivery({{$delivery->id}})">Начислить?</td>
                        @endif
                    @else
                        <td nowrap style="cursor: pointer;" class="red" onclick="deleteDelivery({{$delivery->id}})">Удалить?</td>
                    @endif
                    <td nowrap onclick="openFancyBoxFrame('{{route('get_all_doc_delivery')}}?delivery_id={{$delivery->id}}');" style="cursor:pointer;">Файлы</td>


                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="12" class="text-center">Нет записей</td>
            </tr>
        @endif


        </tbody>

    </table>

    <script>

        function setZeroCostDelivery(id_delivery){
            Swal.fire({
                title: 'Хотите обнулить данный выезд?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Да, обнулить!',
                cancelButtonText: 'Отмена'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('set_zero_cost_delivery')}}",
                        data: {id:id_delivery},
                        success: function (res){
                            Swal.fire(
                                'Обнулили!',
                                '',
                                'success'
                            ).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload()
                                }
                            });
                        }
                    });
                }
            })
        }

        function setCostDelivery(id_delivery){
            Swal.fire({
                title: 'Начисление за выезд',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Начислить',
                cancelButtonText: 'Отмена',
                showLoaderOnConfirm: true,
                preConfirm: (cost) => {
                    return parseInt(cost);
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('set_cost_delivery')}}",
                        data: {id:id_delivery, cost: result.value},
                        success: function (res){
                            Swal.fire(
                                'Начислено!',
                                '',
                                'success'
                            ).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload()
                                }
                            });
                        }
                    });
                }
            })
        }

        function deleteDelivery(id_delivery){
            Swal.fire({
                title: 'Хотите удалить данный выезд?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Да, удалить!',
                cancelButtonText: 'Отмена'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('delete_delivery')}}",
                        data: {id:id_delivery},
                        success: function (res){
                            Swal.fire(
                                'Удалили!',
                                '',
                                'success'
                            ).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload()
                                }
                            });
                        }
                    });
                }
            })
        }

        $(document).on('click', '#print_analytics_delivery', function () {
            $.post('/exports/table2excel', {param:getDataforExel(), method: 'Analitics\\Delivery\\DeliveryAnaliticsController@get_delivery_detail_table_to_excel'}, function(link){
                location.href = link;
            });
        });

        function getDataforExel() {
            return {
                user: "{{$dataExel->user}}",
                mounth: "{{$dataExel->mounth}}",
                year: "{{$dataExel->year}}",
                exel: '1',
            }
        }

        function closeDetails(){
            $('#detail_table').html('');
            $('#main_delivery_table').show()
        }

        document.querySelectorAll('.table_sort').forEach(tableTH => tableTH.addEventListener('click', () => getSort(event)));


    </script>

