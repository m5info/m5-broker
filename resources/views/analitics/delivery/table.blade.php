<div id="main_delivery_table">
    <table class="table-bordered table">
        <thead>
        <tr>
            <th>Курьер</th>
            <th>Кол-во выездов</th>
            <th>КВ агента</th>
            <th>Стоимость выездов</th>
            <th>Кол-во рабочих дней / Отработано</th>
            <th>Расчёт оклада</th>
            <th>Доп. фин. расходы</th>
            <th>Итого</th>
        </tr>
        </thead>
        <tbody>

        @if(sizeof($deliverys))
            @php
                $sum_c_deliveri = 0;
                $sum_delivery = 0;
                $sum_zp = 0;
                $sum_dop_fin_rash = 0;
                $sum_itogo = 0;
                $sum_financial_policy_kv_agent_total = 0;
            @endphp
            @foreach($deliverys as $delivery)
                @php
                    $job_day = \App\Models\Users\UsersWorkDays::getWorkDayUser($delivery->user_id, (int)$request->mounth, (int)$request->year);
                    $zp = ( getFloatFormat($delivery->zp) / (int)$count_worked_day_mounth ) * $job_day;
                    $dop_fin_rash = getFloatFormat($delivery->financial_expenses);

                    $sum_c_deliveri += $delivery->c_deliveri;
                    $sum_delivery += $delivery->payments_summ;
                    $sum_zp += $zp;
                    $sum_dop_fin_rash += $dop_fin_rash;
                    $sum_itogo += $delivery->payments_summ + $zp + $dop_fin_rash;
                    $sum_financial_policy_kv_agent_total += $delivery->financial_policy_kv_agent_total;
                @endphp
                <tr style="cursor: pointer;" onclick="openDetailUsers({{$delivery->user_id}})" >
                    <td>{{ $delivery->name }}</td>
                    <td>{{ $delivery->c_deliveri }}</td>
                    <td>{{titleFloatFormat($delivery->financial_policy_kv_agent_total)}}</td> {{----}}
                    <td>{{ titleFloatFormat($delivery->payments_summ) }}</td>
                    <td>{{ (int)$count_worked_day_mounth }} / {{ $job_day }}</td>
                    <td>{{ titleFloatFormat( $zp ) }}</td>
                    <td>{{ titleFloatFormat( $dop_fin_rash ) }}</td>
                    <td>{{ titleFloatFormat( $delivery->payments_summ + $zp + $dop_fin_rash) }}</td>
                </tr>
            @endforeach
            <tr>
                <td>ИТОГО: </td>
                <td>{{ $sum_c_deliveri }}</td>
                <td>{{ titleFloatFormat($sum_financial_policy_kv_agent_total) }}</td>
                <td>{{ titleFloatFormat($sum_delivery) }}</td>
                <td></td>
                <td>{{ titleFloatFormat( $sum_zp ) }}</td>
                <td>{{ titleFloatFormat( $sum_dop_fin_rash ) }}</td>
                <td>{{ titleFloatFormat( $sum_itogo ) }}</td>
            </tr>
        @else
            <tr>
                <td colspan="12" class="text-center">Нет записей</td>
            </tr>
        @endif

        </tbody>

    </table>
</div>

<div id="detail_table"></div>