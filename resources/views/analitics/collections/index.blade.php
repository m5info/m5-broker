@extends('layouts.app')

@section('head')
    <style>
        .payments_table td, .payments_table th {
            white-space: nowrap;
        }

        .filters_table td, .filters_table th {
            white-space: nowrap;
        }

        .content {
            overflow-x: scroll;
        }

    </style>
@endsection



@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h1 class="inline-h1">Аналитика - Сборы КВ</h1>
            <input type="submit" id="print_analytics" class="btn btn-success btn-right" value="Печать" name="print">
            <input type="button" class="btn btn-success btn-right" value="Применить фильтры" onclick="loadItems();">
            <button onclick="openFancyBoxFrame('/account/table_setting/{{ $table_key }}/edit/?controller=App\\Http\\Controllers\\Analitics\\Common\\CommonAnalyticsController&in_detail=1')"
                    class="btn btn-primary btn-right">Настроить отображение
            </button>
        </div>


        <form name="analitics_common" id="analitics_common_form">
            <div class="block-main">
                <div class="block-sub">
                    <div class="form-horizontal">
                        <div class="form-group">

                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <h2>Период</h2>
                                <div class="row">
                                    <div class="col-sm-12 col-lg-12">
                                        <label class="control-label"></label>
                                        @php
                                            $datesArr = [1 => 'Дата оплаты', 2 => 'Дата договора', 3 => 'Дата по кассе'];
                                        @endphp

                                        {{ Form::select('payment_date_type_id', collect($datesArr), 3, ['class' => 'form-control select2-all']) }}
                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        <label class="control-label">С</label>
                                        {{ Form::text('date_from', date('d.m.Y', strtotime('-1 months')), ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off']) }}
                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        <label class="control-label">По</label>
                                        {{ Form::text('date_to', date('d.m.Y'), ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off']) }}
                                    </div>
                                </div>
                            </div>

                            @if(!$limited_functionality_in_common_analytics)
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <div class="col-sm-12 col-lg-12">
                                    <h2>Основные</h2>
                                </div>
                                <div class="col-sm-4 col-lg-4" style="height: 56px;">
                                    <label class="control-label">Продукт</label>
                                    @php
                                        $products = \App\Models\Directories\Products::query()->where('is_actual', 1)->get();
                                        $products->map(function ($item){
                                            $item->title = mb_substr($item->title, 0, 100);
                                            return $item;
                                        });
                                    @endphp
                                    {{ Form::select('product_id[]', $products->pluck('title', 'id')->prepend('Все', 0), request('product_id[]') ?? '', ['class' => 'form-control select2-all', 'multiple' => true]) }}
                                </div>
                                <div class="col-sm-4 col-lg-4" style="display: none">
                                    <label class="control-label">Транзакции</label>
                                    {{ Form::select('is_deleted', collect([-1=>'Все', 0 => 'Транзакции', 1 => 'Развязанные']), -1, ['class' => 'form-control select2-all']) }}
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label">Подразделение</label>
                                    {{ Form::select('department_ids[]', \App\Models\Settings\Department::all()->pluck('title', 'id'), [10, 13, 17, 14, 15], ['class' => 'form-control select2-all', 'multiple' => true]) }}
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label">Менеджер</label>
                                    {{ Form::select('manager_ids[]', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id')->prepend('Нет', 0), [], ['class' => 'form-control select2 select2-all', 'id'=>'manager_ids', 'multiple' => true, 'required', 'onchange' => 'loadItems()']) }}
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label">Агент</label>
                                    {{ Form::select('agent_id', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id')->prepend('Нет', 0), request('agent_id') ? request()->query('agent_id') : 0, ['class' => 'form-control select2 select2', 'id'=>'agent_id', 'required', 'onchange' => 'loadItems()']) }}
                                </div>
                                <div class="col-sm-4 col-lg-4">
                                    <label class="control-label">СК</label>
                                    {{ Form::select('insurance_ids[]', $insurances->pluck('title', 'id'), 0, ['class' => 'form-control select2-all', 'multiple' => true]) }}
                                </div>
                                <div class="col-sm-4 col-lg-4">
                                    <label class="control-label">Организация</label>
                                    {{ Form::select('org_ids[]', $organizations->pluck('title', 'id'), 0, ['class' => 'form-control select2-all', 'multiple' => true]) }}
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label">Страхователь</label>
                                    {{ Form::text('insurer_title', '', ['class' => 'form-control', 'id'=>'insurer_title', 'multiple' => true, 'required', 'onchange' => 'loadItems()']) }}
                                </div>
                                <div class="col-sm-4" style="display: none">
                                    <label class="control-label">Тип акцепта</label>
                                    {{ Form::select('kind_acceptance', collect(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE), 1, ['class' => 'form-control select2 select2-all']) }}
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="block-sub-collapser" data-title="Основные"></div>
            </div>

        </form>
    </div>

    <div id="statistic" class="block-inner"></div>



    @include('._chunks._pagination',['callback'=>'loadItems', 'count_pagination' => $count_pagination])

    @include('_chunks._vue_table')

@endsection

@section('js')

    <script>

        // Create a clone of the menu, right next to original.


        function getSorting(){

            let sorting = {};
            if ($('.cloned_head th').length !== 0){
                $('.cloned_head th').each(function(i, el){
                    let _el = $(el).children().children();
                    sorting[_el.data('column')] = _el.data('sort');
                });
            }else{
                $('#table .sorting th').each(function(i, el){
                    let _el = $(el).children().children();
                    console.log(_el);
                    sorting[_el.data('column')] = _el.data('sort');
                });
            }

            return sorting;
        }

        function getData() {
            return {
                product_id: $('[name="product_id[]"]').val(),
                agent_id: $('[name="agent_id"]').val(),
                nop_id: $('[name="nop_id"]').val(),
                manager_ids: $('[name="manager_ids[]"]').val(),
                payment_status_id: $('[name="payment_status_id"]').val(),
                type_id_: $('[name="type_id_"]').val(),
                personal_selling: $('[name="personal_selling"]').val(),
                org_ids: $('[name="org_ids[]"]').val(),
                department_ids: $('[name="department_ids[]"]').val(),
                insurer_title: $('[name="insurer_title"]').val(),
                payment_user_id: $('[name="payment_user_id"]').val(),
                submit_receiver: $('[name="submit_receiver"]').val(),
                terms_sale: $('[name="terms_sale"]').val(),
                kind_acceptance: $('[name="kind_acceptance"]').val(),
                is_deleted: $('[name="is_deleted"]').val(),
                prolongation_or_not: $('[name="prolongation_or_not"]').val(),
                payment_type: $('[name="payment_type"]').val(),
                point_sale: $('[name="point_sale"]').val(),
                page_count: $('[name="page_count"]').val(),
                payment_date_type_id: $('[name="payment_date_type_id"]').val(),
                date_to: $('[name="date_to"]').val(),
                date_from: $('[name="date_from"]').val(),
                sorting: getSorting(),
                PAGE: PAGE,
            }
        }

        $(function () {


            loadItems();

            var scroller_elem = $('#scroller');
            var custom_scroll_elem = $('.custom-scroll');

            var widthContent = $('.content').innerWidth();

            custom_scroll_elem.css('width', widthContent);
            scroller_elem.css('width', widthContent / 3 + 'px');

            $(document).on('click', '#print_analytics', function () {

                /*var data = getData();
                var query = $.param({
                    method: 'Analitics\\Common\\CommonAnalyticsController@get_payments_table_to_excel',
                    param: data
                });
                location.href = '/exports/table2excel?' + query;*/

                $.post('/exports/table2excel', {param:getData(), method: 'Analitics\\Collections\\KvCollectionsController@get_payments_table_to_excel'}, function(link){
                    location.href = link;
                });

            });

            function sortItems(field) {
                orderBy = field;
                orderDirection = (orderDirection === 'asc') ? 'desc' : 'asc';
                loadItems();
            }

            $('.wrapper-custom-scroll').css('width', $('.huck').css('width'));

            $(document).on('click', '.sorting th', function(){
                let el = $(this).children().children();
                let sort = el.data('sort');
                let column = el.data('column');

                $('.sorting th .sorting-item a')
                    .removeClass('sorting-toggle-down')
                    .removeClass('sorting-toggle-up')
                    .data('sort', 'off');



                if(sort == 'off'){
                    el.data('sort','asc');
                    $('.cloned_head a[data-column="'+column+'"]').data('sort', 'asc');
                }
                else if(sort == 'asc'){
                    el.data('sort','desc');
                    $('.cloned_head a[data-column="'+column+'"]').data('sort', 'desc');
                }
                else{
                    el.data('sort','off');
                    $('.cloned_head a[data-column="'+column+'"]').data('sort', 'off');
                }

                loadItems();
            });


            $(document).on('mousemove', function (e) {
                if (scroller_elem.attr('active') == 1) {

                    if (e.clientX - scroller_elem.offset().left > -1) {

                        var curentLeft = document.getElementById("scroller").style.left;

                        if (curentLeft.indexOf('px') !== -1) {
                            curentLeft = curentLeft.replace('px', '');
                        }

                        var newValue = curentLeft + 0.2;
                        console.log(newValue);

                        document.getElementById('scroller').style.left = newValue + "px";
                    }
                }
            });


            if ($('.sk_id_checkbox:checked').length == $('.sk_id_checkbox').length && $('.sk_id_checkbox:checked').length > 0) {
                $('.sk_id_select_all_chechbox').attr('checked', true);
            } else {
                $('.sk_id_select_all_chechbox').attr('checked', false);
            }
            if ($('.org_id_checkbox:checked').length == $('.org_id_checkbox').length && $('.org_id_checkbox:checked').length > 0) {
                $('.org_id_select_all_chechbox').attr('checked', true);
            } else {
                $('.org_id_select_all_chechbox').attr('checked', false);
            }

            $('.sale_channel_id_select_all_checkbox').attr('checked', $('.sale_channel_id_checkbox:checked').length == $('.sale_channel_id_checkbox').length && $('.sale_channel_id_checkbox:checked').length > 0);

        });


        function loadItems(reset_page = false) {


            date_from = $('[name="date_from"]').val();
            date_to = $('[name="date_to"]').val();

            if (date_from === '') {
                $('input[name="date_from"]').css('border', '1px solid red');
                return;
            }else{
                $('input[name="date_from"]').css('border', '1px solid rgb(226, 226, 226)');
            }

            if (date_to === '') {
                $('input[name="date_to"]').css('border', '1px solid red');
                return;
            }else{
                $('input[name="date_to"]').css('border', '1px solid rgb(226, 226, 226)');
            }

            //data = $('form[name="analitics_common"]').serialize() + "&PAGE=" + PAGE + "&page_count=" + $('[name=page_count]').val();
            data = getData();
            loaderShow();
            $.get("{{url("/analitics/kv_collections/get_payments_table")}}", data, function (res) {
                $('#table').html(res.html.table);
                $('#statistic').html(res.html.statistic);
                $('#view_row').html(res.view_row);
                $('#max_row').html(res.max_row);

                ajaxPaginationUpdate(res.page_max, loadItems);


                loaderHide();
            }).always(function () {


                /*scrollIntervalID = setInterval(stickIt, 10);


                function stickIt() {

                    var orgElementPos = $('.tr-sticky').offset();
                    orgElementTop = orgElementPos.top;

                    if ($(window).scrollTop() >= (orgElementTop) - 70) {
                        orgElement = $('.tr-sticky');
                        coordsOrgElement = orgElement.offset();
                        leftOrgElement = coordsOrgElement.left;
                        widthOrgElement = orgElement.css('width');
                        /!*$('.tr-sticky').css('top',80).css('width',widthOrgElement).show();*!/
                        $('.tr-sticky').css('top', $('.tr-sticky').offset().top).css('position', 'fixed');
                    } else {
                        $('.tr-sticky').css('position','static');
                    }
                }*/

                /*$('.tr-sticky').addClass('original')..css('position','fixed').css('top','80').css('margin-top','80').css('z-index','500').removeClass('original').hide();*/

                scrollIntervalID = setInterval(stickIt, 10);

                //console.log(document.getElementsByClassName("tr-sticky")[0].children[0].attributes);

                function stickIt() {

                    var orgElementPos = $('.tr-sticky').offset();

                    if (orgElementPos) {

                        orgElementTop = orgElementPos.top;


                        if ($(window).scrollTop() >= (orgElementTop) - 70) {
                            orgElement = $('.tr-sticky');
                            coordsOrgElement = orgElement.offset();
                            leftOrgElement = coordsOrgElement.left;
                            widthOrgElement = orgElement.css('width');

                            $('.tr-sticky-scroll').css('left', leftOrgElement + 'px').css('top', 80).css('width', widthOrgElement).show();
                        } else {
                            $('.tr-sticky').css('visibility', 'visible');
                        }
                    }
                }

                document.getElementsByClassName('content')[0].style.overflowX = 'unset';
                $('.wrapper-custom-scroll').css('width', $('.huck').css('width'));

                $('.custom-scroll').scroll(function (e) {

                    e.preventDefault();
                    var prok = $('.wrapper-custom-scroll').offset().left;
                    var cut = $('.custom-scroll').offset().left;
                    var delta = prok - cut;
                    var over = delta * -1;
                    var elem = document.getElementById('container-for-table');
                    elem.scrollLeft = over;

                });

                $('.wrapper1').on('scroll', function (e) {
                    $('.wrapper2').scrollLeft($('.wrapper1').scrollLeft());
                });
                $('.wrapper2').on('scroll', function (e) {
                    $('.wrapper1').scrollLeft($('.wrapper2').scrollLeft());
                });

                $('.div1').width($('.payments_table').width());
                $('.div2').width($('.payments_table').width());

                loaderHide();
            });

        }


        function check_all_sk(obj) {
            $('.sk_id_checkbox').attr('checked', $(obj).is(':checked'));
        }

        function check_all_org(obj) {
            $('.org_id_checkbox').attr('checked', $(obj).is(':checked'));
        }

        function check_all_sale_channels(obj) {
            $('.sale_channel_id_checkbox').attr('checked', $(obj).is(':checked'));
        }
    </script>

@endsection