<table class="table table-bordered text-left payments_table huck sorting">
    <thead>
    <tr>
        @if($check_result)
            @foreach($user_columns as $column)
                @if(in_array($column['column_name'], $visible_columns))
                    <th>
                        @if($column['sorting'] == 1)
                            {{ $column['column_name'] }}
                            <div class="sorting-item">
                                @php
                                    $sortingClass = '';
                                    if (isset($sorting[$column['column_key']])){
                                        if ($sorting[$column['column_key']] == 'asc')
                                        {
                                            $sortingClass = 'sorting-toggle-down';
                                        }
                                        elseif($sorting[$column['column_key']] == 'desc'){
                                            $sortingClass = 'sorting-toggle-up';
                                        }
                                    }
                                @endphp
                                <a data-column="{{ $column['column_key'] }}" class="sorting-toggle link {{$sortingClass}}"
                                   data-status="off" data-sort="{{$sorting[$column['column_key']] ?? 'off'}}"></a>
                            </div>
                        @else
                            {{ $column['column_name'] }}
                        @endif
                    </th>
                @endif
            @endforeach
        @else
            <p class="text-center" style="margin-top: 35px; color: #d40303;">Доступ к колонкам таблицы ограничен. <button onclick="openFancyBoxFrame('/account/table_setting/analitics_common/edit/?controller=App\\Http\\Controllers\\Analitics\\Common\\CommonAnalyticsController&amp;in_detail=1')" class="btn btn-primary inline">Настройте отображение
                </button> или обратитесь к администратору</p>
            @if(request('method') == 'Analitics\Common\CommonAnalyticsController@get_payments_table')
                <th></th>
            @endif
        @endif
    </tr>
    </thead>
    <tbody>
    <tr>
        @if($check_result)
            @foreach($user_columns as $column)
                @if(in_array($column['column_name'], $visible_columns))
                    @if((int)$column['is_summary'] == 1)
                        <td>{{getSqlSumRow($payments_sql, $column['column_key'], true)}}</td>
                    @else
                        <td></td>
                    @endif
                @endif
            @endforeach
        @endif
    </tr>
    @if($check_result)
        @foreach($payments as $key => $payment)
            <tr data-id="{{$payment->id}}" @if($payment->is_deleted == 1) style="background-color: #ffcccc" @endif>
                @foreach($user_columns as $column)
                    @if(in_array($column['column_name'], $visible_columns))
                        @if($column['_key'] == 'financial_policies_title')
                            <td title="{{$payment[$column['_key']]}}">
                        @else
                            <td>
                        @endif
                            @if($column['column_name'] == '№')
                                {{ $key+1 }}
                            @else
                                @if(!is_array($column['_key']) && isset($payment[$column['_key']]) )
                                    @if($column['_key'] == 'payments_payment_flow')
                                        {{ \App\Models\Contracts\Payments::PAYMENT_FLOW[$payment[$column['_key']]] }}
                                    @elseif($column['_key'] == 'contracts_kind_accepance')
                                        {{ \App\Models\Contracts\Contracts::KIND_ACCEPTANCE[$payment[$column['_key']]] }}
                                    @elseif($column['_key'] == 'date_contract' || $column['_key'] == 'payments_payment_data' || $column['_key'] == 'payments_invoice_payment_date' || $column['_key'] == 'date_acceptance')
                                        {{ $payment[$column['_key']] != null ? setDateTimeFormatRu($payment[$column['_key']], 1) : '' }}
                                    @elseif($column['_key'] == 'reports_orders_id')
                                        <a href="{{url("/reports/order/{$payment[$column['_key']]}")}}"
                                           target="_blank">{{$payment[$column['_key']]}}</a>
                                    @elseif($column['_key'] == 'contract_sale_condition')
                                        {{ \App\Models\Contracts\Contracts::SALES_CONDITION[$payment[$column['_key']]] }}
                                    @elseif($column['_key'] == 'payment_type')
                                        {{ \App\Models\Contracts\Payments::PAYMENT_TYPE[$payment[$column['_key']]] }}
                                    @elseif($column['_key'] == 'payment_type_id')
                                        {{ \App\Models\Contracts\Payments::TRANSACTION_TYPE[$payment[$column['_key']]] }}
                                    @elseif($column['_key'] == 'contracts_is_personal_sales')
                                        {{ \App\Models\Contracts\Contracts::IS_PERSONAL_SALES[$payment[$column['_key']]] }}
                                    @elseif($column['_key'] == 'payment_status')
                                        {{ \App\Models\Contracts\Payments::STATUS[$payment[$column['_key']]] }}
                                    @elseif($column['_key'] == 'reports_orders_accept_status')
                                        {{ \App\Models\Reports\ReportOrders::STATE[$payment[$column['_key']]] }}
                                    @elseif($column['_key'] == 'contracts_type_id')
                                        {{ \App\Models\Contracts\Contracts::TYPE[$payment[$column['_key']]] }}
                                    @elseif($column['_key'] == 'margin_procent')
                                        {{ $payment->margin_procent.'%' }}
                                    @elseif($column['_key'] == 'margin_sum')
                                        {{ getPriceFormat($payment->margin_sum) }}
                                    @elseif($column['_key'] == 'payments_order_title')
                                        {{ parseOrderNumber($payment[$column['_key']]) }}
                                    @elseif($column['_key'] == 'insurer_title')
                                        {{ $payment[$column['_key']] }}
                                        @if($payment->is_deleted == 1) - УДАЛЁН @endif
                                    @elseif($column['_key'] == 'receipt_number')
                                        <a href="{{url("/bso/items/{$payment->bso_receipt_id}")}}"
                                           target="_blank">{{ $payment[$column['_key']] }}</a>
                                    @elseif($column['_key'] == 'bso_item_bso_title')
                                        <a href="{{url("/bso/items/{$payment->bso_id}")}}"
                                           target="_blank">{{ $payment[$column['_key']] }}</a>
                                    @elseif($column['_key'] == 'broker_reward_sum')
                                        {{$payment->is_deleted == 1 ? '0,00' : getPriceFormat($payment->financial_policy_kv_bordereau_total + $payment->financial_policy_kv_dvoy_total)}}
                                    @elseif($column['_key'] == 'payments_invoice_id')
                                        <a href="{{url("/finance/invoice/invoices/{$payment->invoice_id}/edit")}}"
                                           target="_blank"> {{$payment->invoice_id>0?$payment->invoice_id:''}}
                                        </a>
                                    @elseif($column['_key'] == 'payments_financial_policy_kv_bordereau' || $column['_key'] == 'broker_reward_procent'|| $column['_key'] == 'payment_informal_discount')
                                        {{ $payment->is_deleted == 1 ? '0,00%' : $payment[$column['_key']] }}
                                    @elseif($column['_key'] == 'product_title')
                                        {!! implode('<br>', mb_str_split($payment[$column['_key']], 60)) !!}
                                    @elseif($column['_key'] == 'financial_policies_title')
                                        {{ cutToLength($payment[$column['_key']], 60) }}
                                    @elseif($column['_key'] == 'official_discount_total_procent')
                                        {{ ($payment->official_discount_total && $payment->official_discount && $payment->payment_total != '0.00') ? ( $payment->official_discount_total * 100 / $payment->payment_total ).'%' : '0%' }}
                                    @elseif($column['_key'] == 'official_discount_total' || $column['_key'] == 'payment_informal_discount_total' || $column['_key'] == 'payment_total' || $column['_key'] == 'invoice_payment_total' || $column['_key'] == 'payments_financial_policy_kv_dvoy' || $column['_key'] == 'payments_financial_policy_kv_bordereau_total' || $column['_key'] == 'contracts_sum_sk' || $column['_key'] == 'payment_informal_discount_total' || $column['_key'] == 'payments_acquire_total' || $column['_key'] == 'payments_financial_policy_kv_agent_total')
                                        {{ $payment->is_deleted == 1 ? '0,00' : getPriceFormat($payment[$column['_key']]) }}
                                    @else
                                        {{ $payment[$column['_key']] }}
                                    @endif
                                @endif
                            @endif
                        </td>
                    @endif
                @endforeach
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
