<div style="overflow-x: scroll;" id="container-for-table">
    <table class="table table-bordered text-left huck">
        <tbody>
        <tr class="tr-sticky" style="font-weight: 800;">
            <th>№</th>
            <th>Организация</th>
            <th>Дата</th>
            <th>ФИО клиента</th>
            <th>Марка / Модель</th>
            <th>Год выпуска</th>
            <th>Стоимость ТС</th>
            <th colspan="3">ОСАГО</th>
            <th colspan="3">КАСКО</th>
            <th colspan="3">КРЕДИТ</th>
        </tr>
        @foreach($data['contractsAccepted'] as $key => $contract)
            <tr>
                <th>{{$contract->id}}</th>
                <th>{{$contract->insurance_companies->title}}</th>
                <th>{{getDateFormatRu($contract->sign_date)}}</th>
                <th>{{$contract->insurer->get_info()->fio}}</th>
                <th>{{$contract->object_insurer_auto->mark ? $contract->object_insurer_auto->mark->title : ''}} {{ $contract->object_insurer_auto->model ? $contract->object_insurer_auto->model->title : '' }}</th>
                <th>{{$contract->object_insurer_auto->year_of_car != 0 ? $contract->object_insurer_auto->year_of_car : ''}}</th>
                <th>{{$contract->object_insurer_auto->price}}</th>

                <th>{{$contract->product_id == 10 ? $contract->payment_total : ''}}</th>
                <th>{{$contract->product_id == 10 ? 25 : ''}}</th>
                <th>{{$contract->product_id == 10 ? getTotalSumToPrice($contract->payment_total, 25) : ''}}</th>

                <th>{{$contract->product_id == 11 ? $contract->payment_total : ''}}</th>
                <th>{{$contract->product_id == 11 ? 25 : ''}}</th>
                <th>{{$contract->product_id == 11 ? getTotalSumToPrice($contract->payment_total, 25) : ''}}</th>

                <th>{{$contract->bank ? $contract->bank->title : ''}}</th>
                <th> </th>
                <th> </th>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
