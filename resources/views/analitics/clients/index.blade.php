@extends('layouts.app')

@section('content')


    <div class="page-heading">
        <h1 class="inline-h1">Клиенты</h1>
        <a class="btn btn-success btn-right" id="export_accepts">Выгрузить в .xls</a>
    </div>
    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="filter-group" id="filters">

                    @include('analitics.clients.filters')

                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div id="table"></div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <center style="margin-top: 25px; margin-left: 48%; display: inline-block">
                <span id="view_row"></span>/<span id="max_row"></span>
            </center>
            <div id="page_list" class="easyui-pagination pull-right"></div>
        </div>
    </div>

@endsection

@section('js')

    <script>

        $(document).on('change', '[name="period"]', function () {
            var period = $(this).val();
            $('.period').hide();
            $('.period_'+period).show();
            if (period == 2){
                $('.period').show();
            }
        });

        var PAGE = 1;

        $(function () {

            loadItems();

            $(document).on('click', '#export_accepts', function () {
                var data = getData();
                var query = $.param({ method: 'Contracts\\Accepted\\AcceptedController@get_table', param: data});
                location.href = '/exports/table2excel?'+query;
            });
        });


        function setPage(field) {
            PAGE = field;
            loadItems();
        }


        function getData(){
            return {

                period: $('[name="period"]').val(),
                year:$('[name = "year"]').val(),
                month:$('[name = "month"]').val(),
                from:$('[name = "from"]').val(),
                to:$('[name = "to"]').val(),

                page_count: $('[name="page_count"]').val(),
                PAGE: PAGE,
            }
        }


        function loadItems(resetPage = false){

            if(resetPage){
                PAGE = 1
            }

            $('#page_list').html('');
            $('#table_row').html('');
            $('#view_row').html(0);
            $('#max_row').html(0);

            loaderShow();

            $.post('/analitics/clients/get_table', getData(), function(table_res){

                $('#table').html(table_res.html);

                $('#view_row').html(table_res.view_row);
                $('#max_row').html(table_res.max_row);

                $('#page_list').pagination({
                    total:table_res.page_max,
                    pageSize:1,
                    pageNumber: PAGE,
                    layout:['first','prev','links','next','last'],
                    onSelectPage: function(pageNumber, pageSize){
                        setPage(pageNumber);
                    }
                });

            }).always(function(){
                loaderHide();
            });


            loaderHide();

        }

    </script>


@endsection