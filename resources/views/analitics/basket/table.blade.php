<div style="overflow-x: scroll;" id="container-for-table">
    <table class="table table-bordered text-left huck">
        <tbody>
        <tr class="tr-sticky" style="font-weight: 800;">
            <th>№</th>
            <th>Название</th>
            <th>Кто удалил</th>
            <th>Когда</th>
            <th></th>
            <th></th>
        </tr>
        @foreach($items as $key => $item)
            <tr>
                <td>{{$item->removed_item_id}}</td>
                <td>
                    @if($item->type_id == 1)
                        <a href="{{ url("logging/actions/?change_table=bso_items&change_id={$item->removed_item_id}") }}">{{$item->bso ? $item->bso->bso_title : 'Нет данных'}}</a>
                    @elseif($item->type_id == 2)
                        <a href="{{ url("logging/actions/?change_table=payments&change_id={$item->removed_item_id}") }}">Платёж</a>
                    @endif
                </td>
                <td>{{$item->removed_user->name}}</td>
                <td>{{$item->created_at}}</td>
                <td>
                    <span class="btn btn-info" onclick="restoreBasketItem({{$item->id}});">Восстановить</span>
                </td>
                <td>
                    <span class="btn btn-danger" onclick="removeBasketItem({{$item->id}});">Удалить полностью</span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
