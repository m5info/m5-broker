@extends('layouts.app')



@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">Корзина</h1>
    </div>
    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="filter-group" id="filters">
                    <div class="col-sm-4 col-lg-4">
                        <label class="control-label">Тип</label>
                        {{ Form::select('type_id', collect(\App\Models\Basket\BasketItems::TYPES_RU), 1, ['class' => 'form-control select2-all', 'onchange' => 'loadItems()']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div id="table"></div>
        </div>
    </div>

    @include('_chunks/_pagination',['callback'=>'loadItems'])

@endsection



@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
        $(function () {

            loadItems();

        });

        function restoreBasketItem(id) {

            Swal.fire({
                title: 'Восстановить?',
                text: "Вы действительно хотите восстановить",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#eb5a2d',
                cancelButtonColor: 'rgb(229, 30, 36)',
                confirmButtonText: 'Да',
                cancelButtonText: 'Отмена'
            }).then((result) => {
                $.post('{{url("analitics/basket_items")}}/' + id + '/restore_item', {
                    _method: 'POST'
                }, function () {
                    reload();
                });
            });

        }

        function removeBasketItem(id) {

            Swal.fire({
                title: 'Удалить?',
                text: "Вы действительно хотите удалить полностью",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#eb5a2d',
                cancelButtonColor: 'rgb(229, 30, 36)',
                confirmButtonText: 'Да',
                cancelButtonText: 'Отмена'
            }).then((result) => {
                $.post('{{url("analitics/basket_items")}}/' + id + '/remove_item', {
                    _method: 'POST'
                }, function () {
                    reload();
                });
            });

        }

        function getData(){
            return {
                type_id: $('[name="type_id"]').val(),
                page_count: $('[name="page_count"]').val(),
                PAGE: PAGE,
            }
        }


        function loadItems(){

            $('#page_list').html('');
            $('#table_row').html('');
            $('#view_row').html(0);
            $('#max_row').html(0);

            loaderShow();

            $.post('/analitics/basket_items/get_table', getData(), function(table_res){

                $('#table').html(table_res.html);

                $('#view_row').html(table_res.view_row);
                $('#max_row').html(table_res.max_row);

                ajaxPaginationUpdate(table_res.page_max,loadItems);

            }).always(function(){
                loaderHide();
            });


            loaderHide();

        }
    </script>


@endsection