
<div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <label class="control-label">Дозабивщики</label>
    {{ Form::select('underfiller_id', $underfillers->pluck('name', 'id')->prepend('Все', '0'), request('underfiller_id', 0), ['class' => 'form-control select2-ws', 'onchange' => 'loadItems()']) }}
</div>

<div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <label class="control-label">Продукт</label>
    {{ Form::select('product_id', \App\Models\Directories\Products::where('is_actual', '=','1')->get()->pluck('title','id')->prepend('Все', '0'), request('product_id') ?? '', ['class' => 'form-control select2-ws', 'onchange' => 'loadItems()']) }}
</div>


<div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <label class="control-label" for="from">C</label>
    {{ Form::text('from', request('from', date('d.m.Y', time()-60*60*24*30)), ['class' => 'form-control datepicker date', 'onchange' => 'loadItems()']) }}
</div>
<div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <label class="control-label" for="to">По</label>
    {{ Form::text('to', request('to', date('d.m.Y')), ['class' => 'form-control datepicker date', 'onchange' => 'loadItems()']) }}
</div>


