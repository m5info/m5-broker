@extends('layouts.app')

@section('head')
<style>

</style>
@endsection



@section('content')

<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-subheading">
        <h1 class="inline-h1">Аналитика - Дозаполнение полисов</h1>
    </div>
    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="filter-group" id="filters">
                </div>
            </div>
        </div>
    </div>

</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <h3>Общая статистика</h3>
        <canvas id="profit" width="100%" height="45px"></canvas>
        <canvas id="agentspie" width="100%" height="45px"></canvas>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h3>Всего дозабитых договоров</h3>
                <canvas id="total" height="85px"></canvas>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 40px;">
        <h3>Распределение по дозабивщикам</h3>
        <div id="result_table" style="overflow-x: auto;"></div>
    </div>

</div>


@endsection



@section('js')
    <script src="/plugins/chartjs/chart-js-2.7.3.js"></script>
    <script>

    var Charts = {};


    $(function () {

        Chart.pluginService.register({
            beforeDraw: function (chart) {
                if (chart.config.options.elements.center) {
                    var ctx = chart.chart.ctx;
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = centerConfig.fontStyle || 'Arial';
                    var txt = centerConfig.text;
                    var color = centerConfig.color || '#000';
                    var sidePadding = centerConfig.sidePadding || 20;
                    var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                    ctx.font = "30px " + fontStyle;
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(30 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;
                    ctx.fillText(txt, centerX, centerY);
                }
            }
        });


        loadItems();


    });

    function getData() {
        return {
            underfiller_id: $('[name="underfiller_id"]').val(),
            product_id: $('[name="product_id"]').val(),
            from: $('[name="from"]').val(),
            to: $('[name="to"]').val(),
        }
    }

    function loadItems() {
        loaderShow();
        $.post('/analitics/analytics_underfillers/get_filters', getData(), function (res) {
            $('#filters').html(res);
            $('.select2-ws').select2("destroy").select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true,
                //minimumResultsForSearch: -1
            });
            loadCharts();
        });

    }


    function loadCharts() {

        $.each(Charts, function (k, v) {
            v.destroy();
        });

        $('#result_table').html('');
        $.post('/analitics/analytics_underfillers/get_charts', getData(), function (res) {


            $.each(res.charts, function (k, v) {
                Charts[k] = new Chart($("#" + k), {
                    type: v.type,
                    data: v.data,
                    options: v.options
                });
            });

            var result_table = $('<table class="table table-bordered contracts-total"><tbody></tbody></table>');

            var products = "";
            if (Object.keys(res.products).length >= 1) {
                $.each(res.products, function (key, value) {
                    products += "<td>" + value + "</td>";
                });
                result_table.find('tbody').append('<tr class="removable">' +
                    '<th></th>' +
                    '<td>Общее кол-во дозабитых договоров</td> ' +
                    products +
                    '<td></td>' +
                    '</tr>');
            }


            $.each(res.result, function (k, v) {
                var products = "";



                if (Object.keys(res.products).length >= 1) {
                    $.each(res.products, function (key, value) {
                        var pdata = getData();
                        pdata.underfiller_id = v.underfiller_id;
                        pdata.product_id = key;

                        products += "<td><a href='/analitics/analytics_underfillers/details?"+$.param(pdata)+"'>" + v.products[key] + "</a></td>";
                    });
                }

                var palldata = getData();
                palldata.underfiller_id = v.underfiller_id;

                var row = $('<tr class="removable">' +
                    '<th>' + v.title + '</th>' +
                    '<td><a href="/analitics/analytics_underfillers/details?'+ $.param(palldata)+'">' + v.total + '</a></td>' + products +
                    '<td style="background-color: ' + v.color + '"></td>' +
                    '</tr>');
                result_table.find('tbody').append(row)
            });
            $('#result_table').append(result_table);

        }).always(function () {
            loaderHide();
        });
    }

</script>

@endsection
