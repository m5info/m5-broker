@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">
@append

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h2 class="inline-h1">Дозабитые договора</h2>
            <a href="{{ back()->getTargetUrl() }}" class="btn btn-primary btn-right">Назад</a>

        </div>
        <div class="block-main">
            <div class="block-sub">
                <div class="form-horizontal">
                    <div id="table">
                        @if(sizeof($contracts))
                            <table class="tov-table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Статус</th>
                                    <th>СК</th>
                                    <th>БСО</th>
                                    <th>Продукт</th>
                                    <th>Страхователь</th>
                                    <th>Дата заключения</th>
                                    <th>Дата акцепта</th>
                                    <th>Период</th>
                                    <th>Агент</th>
                                    <th>Курьер</th>
                                    <th>Андеррайтер</th>
                                    <th></th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($contracts as $contract)

                                    @php

                                        $fixed_status = false;
                                        $last_contract = $contract->logs->last();
                                        $temp_contracts = request()->route()->getPrefix() == 'contracts/temp_contracts';
                                        if($temp_contracts && $last_contract){
                                            $fixed_status = $last_contract->status_id == 2 && $contract->statys_id == 1;
                                        }

                                    @endphp
                                    <tr class="{{ $fixed_status ? 'bg-yellow' : '' }}">
                                        <td>{{$contract->id}}</td>
                                        <td>{{\App\Models\Contracts\Contracts::STATYS[$contract->statys_id]}}</td>{{-- ({{ $contract->kind_acceptance  ? 'Безусловный' : 'Условный'}})--}}
                                        <td>{{$contract->insurance_companies ? $contract->insurance_companies->title : ""}}</td>
                                        <td>{{$contract->bso ? $contract->bso->bso_title : ""}}</td>
                                        <td>{{$contract->bso && $contract->bso->product ? $contract->bso->product->title : ""}}</td>
                                        <td>{{($contract->insurer)?$contract->insurer->title:''}}</td>
                                        <td>{{setDateTimeFormatRu($contract->sign_date, 1)}}</td>
                                        <td>{{$contract->check_date ? setDateTimeFormatRu($contract->check_date, 1) : ""}}</td>
                                        <td>{{setDateTimeFormatRu($contract->begin_date, 1)}} - {{setDateTimeFormatRu($contract->end_date, 1)}}</td>
                                        <td>{{$contract->agent->name}}</td>
                                        <td>{{$contract->courier()}}</td>
                                        <td>{{$contract->check_user ? $contract->check_user->name : ''}}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{ url("/contracts/underfillers/edit/{$contract->id}") }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
