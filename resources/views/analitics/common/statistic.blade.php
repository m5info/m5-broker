@php
    $analiticsVisibility = auth()->user()->role->visibility('analitics');

    /*Не доделаны значения Акцепт 	Дата по кассе 	Условие продажи 	Тип оплаты 	Тип платежа 	Личная продажа 	Маржа и тд*/


@endphp

<div class="inline-block" style="padding: 0 10px 10px 10px;">

    <div class="inline-block total_sum_block_sub">
        <span style="display: inline-block"><label for="">Итог взносы: </label> <span class="bold">{{getPriceFormat($statistic->sum_payments_total)}} ₽</span></span>
    </div>
    <div class="inline-block total_sum_block_sub">
        <span style="display: inline-block"><label for="">Итог наличные: </label> <span class="bold">{{getPriceFormat($sym_nalichnie)}} ₽</span></span>
    </div>
    <div class="inline-block total_sum_block_sub">
        <span style="display: inline-block"><label for="">Итог безналичные: </label> <span class="bold">{{getPriceFormat($sym_beznalichnie)}} ₽</span></span>
    </div>
    <div class="inline-block total_sum_block_sub">
        <span style="display: inline-block"><label for="">Итог безналичные СК: </label> <span class="bold">{{getPriceFormat($sym_beznalichnie_sk)}} ₽</span></span>
    </div>
    <br>
    {{--<div class="inline-block total_sum_block_sub">
        <span style="display: inline-block"><label for="">Итог вознаграждения агента: </label> <span
                    class="bold">{{getPriceFormat($statistic->sum_financial_policy_kv_agent_total)}}  ₽</span></span>
    </div>


    @if(isset($visibility['parent']) || isset($visibility['report']))
        <div class="inline-block total_sum_block_sub">
        <span style="display: inline-block"><label for="">Итог вознаграждения брокера: </label> <span
                    class="bold">{{getPriceFormat($statistic->sum_all_kv_total)}} ₽</span></span>
        </div>


        <div class="inline-block total_sum_block_sub">
        <span style="display: inline-block"><label for="">Итог маржа: </label> <span
                    class="bold">{{titleFloatFormat($margin)}} ₽</span></span>
        </div>
    @endif--}}
</div>
<input type="button" class="btn btn-success btn-right" value="Применить фильтры" onclick="loadItems();">
