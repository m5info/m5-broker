<div style="overflow-x: scroll;" id="container-for-table">
    <table class="table table-bordered text-left huck">
        <tbody>
        <tr class="tr-sticky" style="font-weight: 800;">
            <th>№</th>
            <th>Номер БСО</th>
            <th>Удалил</th>
            <th>Дата удаления</th>
            <th>Удаленные платежи</th>
            <th>Статус до удаления</th>
            <th></th>
        </tr>
        @foreach($items as $key => $item)
            @php
                $bso = \App\Models\BSO\BsoItem::find($item['bso_item_id']);
                $contract = \App\Models\Contracts\Contracts::find($item['contract_id']);
                $payments = \App\Models\Contracts\Payments::whereIn('id', $item['payment_ids'])->get();
                $user = \App\Models\User::find($item['removed_user_id']);
                $bso_item_old_data = $item['bso_item_old_data'];
                $contract_old_data = $item['contract_old_data'];
            @endphp
            <tr data-removed_bso_item_id="{{$item['removed_bso_item_id']}}">
                <td>{{$item['bso_item_id']}}</td>
                <td>
                    <a href="{{ url("bso/items/{$item['bso_item_id']}") }}">{{$bso ? $bso->bso_title : 'Нет данных'}}</a>
                </td>
                <td>{{$user->name}}</td>
                <td>{{$item['created_at']}}</td>
                <td>
                    @foreach($payments as $payment)
                        @if($payment)
                            <a onclick="openFancyBoxFrame('{{ url("payment/{$payment->id}") }}')" href="#">{{$payment->payment_total}}</a>
                        @endif
                    @endforeach
                </td>
                <td>
                    {{ isset($contract_old_data->statys_id) ? \App\Models\Contracts\Contracts::STATYS[$contract_old_data->statys_id] : '' }}
                </td>
                <td>
                    <span class="btn btn-info" onclick="restoreBsoItem({{$item['removed_bso_item_id']}});">Восстановить</span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
