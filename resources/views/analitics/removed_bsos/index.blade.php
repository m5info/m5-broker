@extends('layouts.app')



@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">Развязанные договоры</h1>
    </div>
    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="filter-group" id="filters">
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div id="table"></div>
        </div>
    </div>

    @include('_chunks/_pagination',['callback'=>'loadItems'])

@endsection



@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
        $(function () {

            loadItems();

        });

        function restoreBsoItem(id) {

            Swal.fire({
                title: 'Восстановить договор и платежи?',
                text: "Вы действительно хотите восстановить весь договор",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#eb5a2d',
                cancelButtonColor: 'rgb(229, 30, 36)',
                confirmButtonText: 'Да',
                cancelButtonText: 'Отмена'
            }).then((result) => {
                $.post('{{url("analitics/contracts_terminations")}}/' + id + '/restore_item', {
                    _method: 'POST'
                }, function () {
                    reload();
                });
            });

        }

        function getData(){
            return {
                page_count: $('[name="page_count"]').val(),
                PAGE: PAGE,
            }
        }


        function loadItems(){

            $('#page_list').html('');
            $('#table_row').html('');
            $('#view_row').html(0);
            $('#max_row').html(0);

            loaderShow();

            $.post('/analitics/contracts_terminations/get_table', getData(), function(table_res){

                $('#table').html(table_res.html);

                $('#view_row').html(table_res.view_row);
                $('#max_row').html(table_res.max_row);

                ajaxPaginationUpdate(table_res.page_max,loadItems);

            }).always(function(){
                loaderHide();
            });


            loaderHide();

        }
    </script>


@endsection