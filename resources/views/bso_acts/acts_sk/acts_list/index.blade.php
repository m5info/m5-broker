@extends('layouts.app')


@section('content')

<div class="page-heading">
    <h2 class="inline-h1">Акты в СК по {{ $supplier->title }}</h2>
    <a href="/bso_acts/acts_sk/" class="btn btn-primary btn-right">Назад</a>
</div>



<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 5px;">
    <div class="row form-group">

        <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2">
            <label class="control-label">Месяц</label>
            @php($type_select = collect(getRuMonthes())->prepend('Все', 0))
            {{ Form::select('month', $type_select, 0, ['class' => 'form-control select2-all', 'onchange' => 'loadItems()']) }}
        </div>

        <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2">
            <label class="control-label">Год</label>
            @php($type_select = collect(getYearsRange(-5, +1))->prepend('Все', 0))
            {{ Form::select('year', $type_select, 0, ['class' => 'form-control select2-all', 'onchange' => 'loadItems()']) }}
        </div>

        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <label class="control-label">Тип</label>
            @php($type_select = collect([0=>'БСО', 1=>'Договор'])->prepend('Все', -1))
            {{ Form::select('type', $type_select, -1, ['class' => 'form-control select2-all', 'onchange' => 'loadItems()']) }}
        </div>

        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <label class="control-label">Название</label>
            @php($type_select = collect([0=>'БСО', 1=>'Договор'])->prepend('Все', -1))
            {{ Form::text('title', '', ['class' => 'form-control select2-all', 'onkeyup' => 'loadItems()', 'autocomplete' => 'off']) }}
        </div>

    </div>
</div>
<div id="table"></div>

@include('_chunks._pagination',['callback'=>'loadItems'])

@endsection

@section('js')
<script>

    $(function () {
        loadItems();
    });

    function loadItems(){
        loaderShow();

        $('#page_list').html('');
        $('#table_row').html('');
        $('#view_row').html(0);
        $('#max_row').html(0);

        $.post("/bso_acts/acts_sk/{{$supplier->id}}/acts_list_table", getData(), function (response) {

            $('#table').html(response.html);
            $('#view_row').html(response.view_row);
            $('#max_row').html(response.max_row);

            ajaxPaginationUpdate(response.max_row,loadItems);

        }).fail(function(){
            $('#table').html('');
        }).always(function() {
            loaderHide();
        });
    }

    function getData(){
        return {
            month: $('[name="month"]').val(),
            year: $('[name="year"]').val(),
            type: $('[name="type"]').val(),
            title: $('[name="title"]').val(),
            page_count: $('[name="page_count"]').val(),
            PAGE: PAGE,
        }
    }

</script>
@endsection
