@php($select_params = ['class'=>'form-control select2-all','onchange'=>'loadItems()'])

<div class="btn-group col-xs-12 col-sm-12 col-md-4 col-lg-3">
    <label class="control-label" for="user_id_from">Организация</label>
    {{ Form::select('org_id', \App\Models\Organizations\Organization::all()->pluck('title', 'id')->prepend('Не выбрано', -1), request()->has('org_id') ? request()->get('org_id') : -1, $select_params) }}
</div>

<div class="btn-group col-xs-12 col-sm-12 col-md-4 col-lg-3">
    <label class="control-label" for="user_id_from">СК</label>
    {{ Form::select('insurance_id', $insurances->pluck('title', 'id')->prepend('Не выбрано', -1), request()->has('insurance_id') ? request()->get('insurance_id') : -1, $select_params) }}
</div>



<div class="btn-group col-xs-12 col-sm-12 col-md-4 col-lg-3">
    <label class="control-label" for="user_id_from">Поставщик</label>
    {{ Form::select('supplier_id', $suppliers->pluck('title', 'id')->prepend('Не выбрано', -1), request()->has('supplier_id') ? request()->get('supplier_id') : -1, $select_params) }}
</div>

<div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <label class="control-label" for="user_id_from">Агент</label>
    {{ Form::select('agent_id', \App\Models\Characters\Agent::all()->pluck('name', 'id')->prepend('Агент',-1), \Request::query('agent_id'), ['class' => 'form-control select2', 'id'=>'agent_id', 'onchange'=>'loadItems()']) }}
</div>

