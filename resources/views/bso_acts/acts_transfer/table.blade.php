@if($acts)
    @php
        if(auth()->user()->hasPermission('bso', 'items'))
         $view_perm = true;
    @endphp
    @foreach($acts as $key => $act)
        @php

            $style = (isset($act->user_to)) ? 'background-color: #e6ffe6;' : '';
        @endphp

        <tr class="clickable-row-blank invoice_row" id="{{$act->id}}" data-href="/bso_acts/show_bso_act/{{$act['id']}}/"
            style="cursor: pointer;{{$style}}">
            <td>
                <div id="bso_tooltip_{{ $act->id }}"
                     style="padding: 10px; background-color: #eeeeed;display:block; position: absolute;border: #c3c3c3 1px solid;display: none">
                    @php
                        if($act->logs){
                        foreach($act->logs as $payment){
                           $bso_title = $payment->bso ? $payment->bso->bso_title : "";
                            $bso_product = $payment->bso && $payment->bso->product ? $payment->bso->product->title : "";

                            if(isset($view_perm) && $payment->bso){
                                echo "<a href=".url('bso/items', $payment->bso->id)." target='_blank'>$bso_title</a> $bso_product<br>";
                            }else{
                                echo "<span title='Не безусловный'>$bso_title</span> $bso_product<br>";
                            }
                        }
                    }
                    @endphp
                </div>
                {{$act->act_number}}
            </td>
            <td>{{ $act->date_ru }}</td>
            <td>{!! $act->type->title !!}</td>
            <td>{{($act->user_from)?$act->user_from->name:''}} </td>
            <td>{{($act->user_to)?$act->user_to->name:''}} </td>
            <td>{{($act->bso_manager)?$act->bso_manager->name:''}} </td>
            <td>{{($act->point_sale)?$act->point_sale->title:''}}</td>
        </tr>
    @endforeach

    <script>
        var mouse = {};

        $(function () {
            $('body').on("mousemove", function (event) {
                mouse.x = event.pageX;
                mouse.y = event.pageY;
                console.log();
            });
            $('tr.invoice_row td:first-child').on("mouseover", function () {

                var id = $(this).parent('tr').attr('id');
                console.log(id);

                if ($('#bso_tooltip_' + id).html() != '')
                    $('.div_tooltip').html($('#bso_tooltip_' + id).clone());
                else
                    $('.div_tooltip').html('<span>Пусто</span>');

                $('.div_tooltip').show().css('top', mouse.y - $(document).scrollTop() - 5).css('left', mouse.x - 5);
            }).on("mouseout", function () {

                var id = $(this).parent('tr').attr('id');
                $('.div_tooltip').hide();
            });
        });

    </script>


@endif
