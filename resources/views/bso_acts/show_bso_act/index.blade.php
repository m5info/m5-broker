@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h2 class="inline-h1">Акт # {{$act->act_number}}</h2>
        <a class="btn btn-primary btn-right pull-right doc_export_btn" href="/bso_acts/export/{{$act->id}}/">Сформировать Акт</a>
        <a class="btn btn-success btn-right pull-right" id="export_bso_table">Выгрузить в XLS</a>
    </div>


    <div class="" id="main_container" style="margin-top: 15px">
        <div class="block-main">
            <div class="block-sub">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label class="col-sm-12 control-label">Дата/время</label>
                            <div class="col-sm-12">
                                {{setDateTimeFormatRu($act->time_create)}}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label class="col-sm-12 control-label">Тип акта</label>
                            <div class="col-sm-12">
                                {!!$act->type->title!!}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label class="col-sm-12 control-label">Статус БСО</label>
                            <div class="col-sm-12">
                                {{$act->bso_state->title}}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label class="col-sm-12 control-label">Точка продаж</label>
                            <div class="col-sm-12">
                                {{($act->point_sale)?$act->point_sale->title:''}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label class="col-sm-12 control-label">Выдал</label>
                            <div class="col-sm-12">
                                {{($act->user_from)?$act->user_from->name:''}}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label class="col-sm-12 control-label">Получил</label>
                            <div class="col-sm-12">
                                {{($act->user_to)?$act->user_to->name:''}}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label class="col-sm-12 control-label">Сотрудник</label>
                            <div class="col-sm-12">
                                {{($act->bso_manager)?$act->bso_manager->name:''}}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label class="col-sm-12 control-label">Организация для акта</label>
                            <div class="col-sm-12">
                                {{Form::select( 'act_org_id', \App\Models\Organizations\Organization::getOrgProvider()->get()->pluck('title', 'id'), $main_org['id'], ['class' => "form-control select2-all", 'onchange' => 'saveAct()'] )}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-horizontal">
                    {!! $bso_table !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>

        $(document).on('click', '.doc_export_btn', function () {
            var select = $('select[name="act_org_id"]').val();

            if (select == 0){
                flashHeaderMessage('Выберите организацию для акта', 'danger')
                return false;
            } else {
                return true;
            }

        });

        $(function () {

            $(document).on('click', '#export_bso_table', function(){
                var query = $.param({ method:  'BsoActs\\ShowBsoActController@get_bso_table', method_param: {id:'{{$act->id}}'} });
                location.href = '/exports/table2excel?'+query;
            });
        });


        function saveAct(){
            $.post('/bso_acts/update/{{$act->id}}', getData(), function(res){
                if(res.status === 'ok'){
                    flashHeaderMessage('Данные успешно сохранены', 'success')
                }
            });
        }


        function getData(){
            return {
                act_org_id: $('[name="act_org_id"]').val()
            }
        }

    </script>


@endsection