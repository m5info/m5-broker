<table class="table table-bordered">
    <thead>
    <tr>
        <th>Акт</th>
        <th>Номер заявки</th>
        <th>Время создания</th>
        <th>Тип</th>
        <th>Передал</th>
        <th>Принял</th>
        <th>Сотрудник</th>
        <th>Статус</th>
        <th>Точка продаж</th>
    </tr>
    </thead>
@if($acts)
    <tbody>
    @foreach($acts as $key => $act)
        <tr class="clickable-row-blank" data-href="/bso/transfer/?bso_cart_id={{$act['id']}}" style="cursor: pointer;">
            <td>{{$act->id}}</td>
            <td></td>
            <td>{{setDateTimeFormatRu($act->time_create)}}</td>
            <td>{{$act->type ? $act->type->title : ""}}</td>
            <td>{{($act->user_from)?$act->user_from->name:''}} </td>
            <td>{{($act->user_to)?$act->user_to->name:''}} </td>
            <td>{{($act->bso_manager)?$act->bso_manager->name:''}} </td>
            <td>{{\App\Models\BSO\BsoCarts::STATE_CAR[$act->cart_state_id]}}</td>
            <td>{{$act->point_sale ? $act->point_sale->title : ""}}</td>
        </tr>
    @endforeach
    </tbody>
@endif
</table>
