@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h2>Акты Резервные</h2>
    </div>


    @if(auth()->user()->role->rolesVisibility(5)->visibility == 0 || auth()->user()->role->rolesVisibility(5)->visibility == 3)

        <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="filter-group">
                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="bso_cart_type">Тип</label>
                            {{ Form::select('bso_cart_type', \App\Models\BSO\BsoCartType::all()->pluck('title', 'id')->prepend('Не выбрано', -1), -1, ['class' => 'form-control select2-ws', 'id'=>'bso_cart_type', 'onchange'=>'loadItems()']) }}
                        </div>

                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="user_id_from">Передал</label>
                            {{ Form::select('user_id_from', \App\Models\User::getALLUserWhere()->pluck('name', 'id')->prepend('Не выбрано', -1), -1, ['class' => 'form-control select2', 'id'=>'user_id_from', 'onchange'=>'loadItems()']) }}
                        </div>

                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="user_id_to">Принял</label>
                            {{ Form::select('user_id_to', \App\Models\User::getALLUserWhere()->pluck('name', 'id')->prepend('Не выбрано', -1), -1, ['class' => 'form-control select2', 'id'=>'user_id_to', 'onchange'=>'loadItems()']) }}
                        </div>

                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="bso_manager_id">Сотрудник</label>
                            {{ Form::select('bso_manager_id', \App\Models\User::getALLUserWhere()->pluck('name', 'id')->prepend('Не выбрано', -1), -1, ['class' => 'form-control select2', 'id'=>'bso_manager_id', 'onchange'=>'loadItems()']) }}
                        </div>

                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="tp_id">Точка продаж</label>
                            {{ Form::select('tp_id', \App\Models\Settings\PointsSale::all()->pluck('title', 'id')->prepend('Не выбрано', -1), -1, ['class' => 'form-control select2-ws', 'id'=>'tp_id', 'onchange'=>'loadItems()']) }}
                        </div>

                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="tp_id">Статус</label>
                            {{ Form::select('status_id', collect(\App\Models\BSO\BsoCarts::STATE_CAR)->prepend('Не выбрано', -1), -1, ['class' => 'form-control select2-ws', 'id'=>'status_id', 'onchange'=>'loadItems()']) }}
                        </div>

                    </div>
                </div>
            </div>
        </div>




    @else

        <input type="hidden" id="bso_cart_type" value="-1"/>
        <input type="hidden" id="user_id_from" value="-1"/>
        <input type="hidden" id="user_id_to" value="-1"/>
        <input type="hidden" id="bso_manager_id" value="-1"/>
        <input type="hidden" id="tp_id" value="-1"/>

    @endif



    <div class="block-inner sorting col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow: auto;">
        <div id="table_row"></div>
    </div>

    @include('_chunks/_pagination',['callback'=>'loadItems'])


@endsection

@section('js')


    <script>


        $(function () {
            loadItems();
        });

        function loadItems() {
            var data = {

                bso_cart_type: $('#bso_cart_type').val(),
                user_id_from: $('#user_id_from').val(),
                user_id_to: $('#user_id_to').val(),
                bso_manager_id: $('#bso_manager_id').val(),
                tp_id: $('#tp_id').val(),
                status_id: $('#status_id').val(),
                page_count: $("#page_count").val(),
                PAGE: PAGE,
            };

            $('#page_list').html('');
            $('#table_row').html('');
            $('#view_row').html(0);
            $('#max_row').html(0);


            loaderShow();

            $.post("{{url("/bso_acts/acts_reserve/get_acts_table/")}}", data, function (response) {

                $('#view_row').html(response.view_row);
                $('#max_row').html(response.max_row);

                $('#table_row').html(response.html);

                $(".clickable-row-blank").click( function(){
                    if ($(this).attr('data-href')) {
                        window.open($(this).attr('data-href'), '_blank');
                    }
                });

                $('td').css({'border-right': '1px #e0e0e0 solid'});
                $.each($('tr'), function(i, v){
                    var children = $(v).children().first();
                    if(children[0].localName === 'td'){
                        children.css({'border-left': '1px #e0e0e0 solid'});
                    }
                });

                ajaxPaginationUpdate(response.page_max,loadItems);

                loaderHide();

            }).always(function() {
                loaderHide();
            });

        }

    </script>

@endsection