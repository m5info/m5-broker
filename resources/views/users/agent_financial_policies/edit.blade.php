@extends('layouts.frame')


@section('title')

    Финансовая группа

@endsection

@section('content')


    {{ Form::open(['url' => url("/users/users/{$user_id}/agent_financial_policies/{$id}/edit"), 'method' => 'post', 'class' => 'form-horizontal']) }}

    <div class="form-horizontal" style="min-height: 400px !important;">


       <div class="form-group">
            <label class="col-sm-3 control-label">Финансовая группа</label>
            <div class="col-sm-9">
                {{ Form::select('financial_group_id', $financial_policies->pluck('title', 'id')->prepend('По умолчанию', 0), isset($agent_financial_policies) ? $agent_financial_policies->financial_group_id : 0, ['class' => 'form-control select2-ws', 'required']) }}
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-12">
                <label class="col-sm-3 control-label">Дата действия</label>
                <div class="col-sm-9">
                    <div class="col-sm-6 row">
                        {{ Form::text('begin_date', ($agent_financial_policies)?setDateTimeFormatRu($agent_financial_policies->begin_date, 1):'', ['class' => 'form-control datepicker date', 'placeholder'=>'Дата начала', 'id'=>'agent_begin_date', "onchange"=>"setDataEnd()"]) }}
                    </div>

                    <div class="col-sm-6">
                        {{ Form::text('end_date', ($agent_financial_policies)?setDateTimeFormatRu($agent_financial_policies->end_date, 1):'', ['class' => 'form-control datepicker date', 'placeholder'=>'Дата окончания', 'id'=>'agent_end_date']) }}
                    </div>
                </div>

            </div>
        </div>





    </div>


    {{Form::close()}}




@endsection

@section('footer')

    @if($id > 0)
    <button class="btn btn-danger pull-left" onclick="deleteFP('{{url("/users/users/{$user_id}/agent_financial_policies/{$id}/delete")}}')">{{ trans('form.buttons.delete') }}</button>
    @endif

    <button onclick="submitForm()" type="submit" class="btn btn-primary" style="float: right;">{{ trans('form.buttons.save') }}</button>


@endsection




@section('js')
    <script>
        $(function () {


        });


        function deleteFP(url) {
            if (!customConfirm()) return false;

            $.post(url, {
                _method: 'delete'
            }, function () {
                parent_reload();
            });
        }


        function setDataEnd() {

            start_date = $('#agent_begin_date').val();
            var cur_date_tmp = start_date.split(".");

            var cur_date = new Date(cur_date_tmp[2], cur_date_tmp[1] - 1, cur_date_tmp[0]);

            var new_date = new Date(cur_date.setYear(cur_date.getFullYear() + 1));
            var new_date2 = new Date(new_date.setDate(new_date.getDate() - 1));
            $('#agent_end_date').val(getFormattedDate(new_date2));

        }


    </script>
@append