<link rel="stylesheet" type="text/css" href="/assets/new/lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/assets/new/css/font.css">
<link rel="stylesheet" type="text/css" href="/assets/new/css/main.css">
<link href="/plugins/select2/select2.css" rel="stylesheet">
<link href="/plugins/bootstrap/select2.css" rel="stylesheet">
<script src="/plugins/jquery/jquery.min.js"></script>
<div class="col-xs-12">

    <div class="page-header col-xs-12">
        <h1>Мои настройки</h1>
    </div>

    <div class="form-group col-xs-12">

        <h2>Тема</h2>

        @foreach(\App\Models\Users\UserThemes::themes as $key => $t)

            <div class="theme col-xs-3 @if($t['file'] == $u_theme['css_file']) active @endif" data-theme="{{ $key }}">
                <div class="theme_preview">
                    <div class="main_color">
                        <div class="color_1" style="background-color: {{ $t['c1'] }};"></div>
                    </div>
                    <div class="sub_color">
                        <div class="color_2" style="padding: 4px;color: {{ $t['c2'] }};font-size: {{ $t['fs'] }}px ;">
                            А
                        </div>
                        <div class="color_3" style="background-color: {{ $t['c3'] }}"></div>
                    </div>
                </div>
                <div class="theme_name">{{ $t['name'] }}</div>
            </div>
        @endforeach

    </div>

    <div class="form-group col-xs-12">
        <h2>Размер шрифта</h2>
        <div class="col-xs-6">
            <input type="range" class="custom-range" value="{{  $u_theme['custom_styles'] }}" min="12" max="18"
                   id="customRange2">

            <div class=" pull-left" style="font-size: 12px">Мелкий</div>
            <div class="  pull-right" style="font-size: 18px"> Большой</div>
        </div>
    </div>
    {{ csrf_field() }}

    @if (auth()->user()->hasPermission('role_owns', 'login_any_user') || session()->has('any_login'))
        <div class="block-main">
            <div class="block-sub">
                <div class="form-horizontal">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" style="margin-left: 20px">Вход под другой логин</label>
                            <div class="col-sm-6">
                                {{ Form::select('login_user_id', collect(\App\Models\User::all()->pluck('name', 'id'))->prepend('Выберите пользователя', 0), 0, ['class' => 'form-control select2-all', 'id'=>'login_user_id', 'required']) }}
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-1">
                                <span class="btn btn-primary pull-right" id="any_login">Вход</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

</div>

@include('layouts.js')

<script>
    $(document).on('click', '#any_login', function () {
        window.parent.$.post('/settings/system_project_features/any_login', {login_user_id:$('#login_user_id').val()}).done(function (res) {
            window.parent.location.reload();
        });
    });

    $(function () {
        $('.theme').on('click', function () {

            var active = $(this);
            setTheme(active);

        });
        $('input[type=range]').on('change', function () {
            setTheme($('.active'));
        });

        function setTheme(active) {
            var data = {
                theme: $(active).data() ? $(active).data().theme : 0,
                custom_styles: '',
                font: $('#customRange2').val(),
                _token: $('input[name="_token"]').val()
            };
            $.post("{{url("/user/themes_change")}}", data, function (res) {
                $('.theme').removeClass('active');
                $(active).addClass('active');

                console.log(data.font);
                var style = $(window.parent.document.head).children('style[data-html="user"]');

                var new_style = style.clone();
                style.remove();

                new_style.text('\n\r body>*, body { \n font-size:' + data.font + 'px!important; }');

                $(window.parent.document.head).append(new_style);
                $(window.parent.document.head).children('link[data-html="user"]').attr('href', '/assets/new/css/user_themes/' + res + '.css');

            });
        }
    });
</script>
<style>
    .theme {
        text-align: left;
    }

    .theme:hover, .theme:hover .theme_name  {
        text-decoration: underline;
        color: #fb4700;
        cursor: pointer;
    }

    .theme_name {
        color: #5a5a5a;
    }

    .theme_preview {
        width: 40px;
        height: 40px;
        margin: 10px 0;
        border-radius: 8px;
        /* border: 1px solid #c7c7c7; */
        overflow: hidden;
        box-shadow: 1px 1px 5px #a2a2a2;
    }


    .main_color, .sub_color {
        float: left;
        height: 100%;
    }

    .main_color > div {
        height: 40px;
        width: 20px;
    }

    .sub_color > div {

        height: 20px;
        width: 20px;
    }

    .active .theme_preview,.theme:hover .theme_preview {
        box-shadow: 1px 1px 5px #fb4700;
        transition: 0.2s all;
    }

    .active .theme_name {
        text-decoration: underline;
    }
    .custom-range{
        margin-bottom: 15px;
        margin-top: 15px;
    }

/*
    input[type=range] {
        -webkit-appearance: none;
        margin: 18px 0;
        width: 100%;
    }
    input[type=range]:focus {
        outline: none;
    }
    input[type=range]::-webkit-slider-runnable-track {
        width: 100%;
        height: 8.4px;
        cursor: pointer;
        animate: 0.2s;
        box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
        background: #3071a9;
        border-radius: 1.3px;
        border: 0.2px solid #010101;
    }
    input[type=range]::-webkit-slider-thumb {
        box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
        border: 1px solid #000000;
        height: 36px;
        width: 16px;
        border-radius: 3px;
        background: #ffffff;
        cursor: pointer;
        -webkit-appearance: none;
        margin-top: -14px;
    }
    input[type=range]:focus::-webkit-slider-runnable-track {
        background: #367ebd;
    }
    input[type=range]::-moz-range-track {
        width: 100%;
        height: 8.4px;
        cursor: pointer;
        animate: 0.2s;
        box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
        background: #3071a9;
        border-radius: 1.3px;
        border: 0.2px solid #010101;
    }
    input[type=range]::-moz-range-thumb {
        box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
        border: 1px solid #000000;
        height: 36px;
        width: 16px;
        border-radius: 3px;
        background: #ffffff;
        cursor: pointer;
    }
    input[type=range]::-ms-track {
        width: 100%;
        height: 8.4px;
        cursor: pointer;
        animate: 0.2s;
        background: transparent;
        border-color: transparent;
        border-width: 16px 0;
        color: transparent;
    }
    input[type=range]::-ms-fill-lower {
        background: #955400;
        border: 0.2px solid #010101;
        border-radius: 2.6px;
        box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
    }
    input[type=range]::-ms-fill-upper {
        background: #a9490f;
        border: 0.2px solid #010101;
        border-radius: 2.6px;
        box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
    }
    input[type=range]::-ms-thumb {
        box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
        border: 1px solid #000000;
        height: 36px;
        width: 16px;
        border-radius: 3px;
        background: #ffffff;
        cursor: pointer;
    }
    input[type=range]:focus::-ms-fill-lower {
        background: #3071a9;
    }
    input[type=range]:focus::-ms-fill-upper {
        background: #367ebd;
    }*/
</style>