@if(sizeof($notifications))

    <table class="tov-table">
        <thead>
        <tr class="sort-row">
            <th>{{ trans('users/roles.index.title') }}</th>
        </tr>
        </thead>


        <tbody>
        @foreach($notifications as $notification)
            <tr class="clickable-row-blank" data-href="">
                <td width="70%">
                    {!! $notification->notifier()->display() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@else
    нет уведомлений
@endif