<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="page-subheading">
        <h2 class="inline-h1">Основная информация @if(isset($user)) ID {{$user->id}} @endif</h2>
        @if(isset($user))
            <span class="btn btn-info pull-right" onclick="openLogEvents('{{$user->id}}', 1, 0)"><i class="fa fa-history"></i> </span>
        @endif
    </div>
    <div class="block-main">
        <div class="block-sub">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ trans('users/users.edit.name') }}</label>
                    <div class="col-sm-3">
                        <input class="form-control surname" id="disabledInput" type="text" value=""  disabled>
                        <p class="help-block">Фамилия</p>
                    </div>
                    <div class="col-sm-3">
                        <input class="form-control name" id="disabledInput" type="text" value="" disabled>
                        <p class="help-block">Имя</p>
                    </div>
                    <div class="col-sm-3">
                        <input class="form-control lastname" id="disabledInput" type="text" value="" disabled>
                        <p class="help-block">Отчество</p>
                    </div>
                    {{ Form::hidden('name', old('name'), ['class' => 'form-control', 'readonly', 'required']) }}
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Статус</label>
                    <div class="col-sm-9">
                        {{ Form::select('status_user_id', [
                            \App\Models\Subject\Type::WORK => "Работает",
                            \App\Models\Subject\Type::NOT_WORK => "Уволен",
                        ], isset($user) ? $user->status_user_id : '',  ['class' => 'form-control status_user_id select2-ws']) }}
                    </div>
                </div>


                <div class="divider"></div>
                <br/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{{ trans('users/users.edit.subject_type') }}</label>
                        <div class="col-sm-9">
                            {{ Form::select('subject_type_id', [
                   \App\Models\Subject\Type::PHYSICAL => trans('users/users.edit.physical'),
                   \App\Models\Subject\Type::JURIDICAL => trans('users/users.edit.juridical'),
               ], isset($user) ? $user->subject_type_id : '',  ['class' => 'form-control subject_type_id select2-ws']) }}
                        </div>
                    </div>



                    @foreach($userInfoFields as $userInfoGroup => $userInfoGroupFields)

                        @foreach($userInfoGroupFields as $userInfoGroupField)


                            <div class="form-group {{ $userInfoGroup }}">
                                <label class="col-sm-3 control-label">{{ trans('users/users.edit.' . $userInfoGroupField) }}</label>
                                <div class="col-sm-9">
                                    {{ Form::text($userInfoGroupField, (isset($user) && $user->info ? $user->info[$userInfoGroupField] : ''), ['class' => 'form-control']) }}
                                </div>
                            </div>

                        @endforeach

                    @endforeach

                </div>

                <div class="divider"></div>
                <br/>

                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ trans('users/users.edit.email') }} <span class="required">*</span></label>
                    <div class="col-sm-9">
                        {{ Form::text('email', old('email'), ['class' => 'form-control', 'required']) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Пароль <span class="required">*</span></label>
                    <div class="col-sm-9">
                        {{ Form::password('password', ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ trans('users/users.edit.work_phone') }}</label>
                    <div class="col-sm-9">
                        {{ Form::text('work_phone', old('work_phone'), ['class' => 'form-control phone']) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ trans('users/users.edit.mobile_phone') }}</label>
                    <div class="col-sm-9">
                        {{ Form::text('mobile_phone', old('mobile_phone'), ['class' => 'form-control phone']) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ trans('users/users.edit.role') }}</label>
                    <div class="col-sm-9">
                        {{ Form::select('role_id', $roles, old('role_id'), ['class' => 'form-control select2-ws', 'required']) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ trans('users/users.edit.organization') }}</label>
                    <div class="col-sm-9">
                        {{ Form::select('organization_id', $organizations, old('organization_id'), ['class' => 'form-control select2-ws', 'required', 'id' => 'organization_id']) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Точка</label>
                    <div class="col-sm-9" id="point_department_id">
                        {{ Form::select('point_department_id', collect($points_departments), old('point_department_id'), ['class' => 'form-control select2-ws']) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Представитель поставщика</label>
                    <div class="col-sm-9">
                        {{ Form::select('bso_supplier_id', $suppliers, old('bso_supplier_id'), ['class' => 'form-control select2-ws']) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ trans('users/users.edit.region') }}</label>
                    <div class="col-sm-9">
                        {{ Form::text('region', old('region'), ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ trans('users/users.edit.city') }}</label>
                    <div class="col-sm-9">
                        {{ Form::select('city_id', \App\Models\Settings\City::all()->pluck('title', 'id'), old('city_id'), ['class' => 'form-control select2-ws', 'required']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ trans('users/users.edit.department') }}</label>
                    <div class="col-sm-9">
                        {{ Form::select('department_id', \App\Models\Settings\Department::all()->pluck('title', 'id'), old('department_id'), ['class' => 'form-control select2-ws', 'required']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Точка продаж</label>
                    <div class="col-sm-9">
                        {{ Form::select('point_sale_id', \App\Models\Settings\PointsSale::where('is_actual', 1)->get()->pluck('title', 'id'), old('point_sale_id'), ['class' => 'form-control select2-ws', 'required']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ trans('users/users.edit.is_parent') }}</label>
                    <div class="col-sm-9">
                        {{ Form::checkbox('is_parent', 1, old('is_parent')) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Руководитель</label>
                    <div class="col-sm-9">
                        {{ Form::select('parent_id', \App\Models\User::getALLParent()->pluck('name', 'id')->prepend('Отсутствует', 0), isset($user) ? $user->parent_id : '',  ['class' => 'form-control select2']) }}
                    </div>
                </div>

            </div>
        </div>



        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary pull-right">
                    Сохранить
                </button>
            </div>
        </div>

    </div>
</div>
@if(isset($user))
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="page-subheading">
        <h2 class="inline-h1">Агентский договор основной</h2>
        <a class="btn btn-success btn-right doc_export_btn" href="/directories/organizations/{{$user->organization->id}}/documentation?user={{$user->id}}">Сформировать</a>
    </div>
    <div class="block-main">
        <div class="block-sub">
            <div class="form-horizontal">

                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Агентский договор
                        <i class="fa fa-cog" style="font-size: 16px;cursor: pointer;" onclick="createAgentContract()"></i>
                    </label>
                    <div class="row col-sm-9">

                        <div class="col-sm-6">
                            {{ Form::text('agent_contract_title', old('agent_contract_title'), ['class' => 'form-control', 'placeholder'=>'Номер договора', 'id'=>'agent_contract_title']) }}
                        </div>

                        <div class="col-sm-3">
                            {{ Form::text('agent_contract_begin_date', ($user)?setDateTimeFormatRu($user->agent_contract_begin_date, 1):'', ['class' => 'form-control datepicker date', 'placeholder'=>'Дата начала', 'id'=>'agent_contract_begin_date', "onchange"=>"setDataEnd()"]) }}
                        </div>

                        <div class="col-sm-3">
                            {{ Form::text('agent_contract_end_date', ($user)?setDateTimeFormatRu($user->agent_contract_end_date, 1):'', ['class' => 'form-control datepicker date', 'placeholder'=>'Дата окончания', 'id'=>'agent_contract_end_date']) }}
                        </div>


                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ trans('users/users.edit.сurator') }}</label>
                    <div class="col-sm-9">
                        {{ Form::select('curator_id', \App\Models\User::where('is_parent', '=', '1')->get()->pluck('name', 'id')->prepend('Отсутствует', 0), isset($user) ? $user->curator_id : '',  ['class' => 'form-control select2']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Финансовая группа</label>
                    <div class="col-sm-9">
                        {{ Form::select('financial_group_id', \App\Models\Settings\FinancialGroup::where('is_actual', '=', '1')->get()->pluck('title', 'id')->prepend('По умолчанию', 0), old('financial_group_id'), ['class' => 'form-control select2-ws', 'required']) }}
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-3 control-label">Дата действия ФП</label>
                    <div class="col-sm-9">
                        {{ Form::text('financial_group_date', ($user)?setDateTimeFormatRu($user->financial_group_date, 1):'', ['class' => 'form-control datepicker date']) }}
                    </div>
                </div>



                <div class="form-group">
                    <label class="col-sm-3 control-label">Выдача БСО</label>
                    <div class="col-sm-9">
                        {{ Form::select('ban_level', collect([0=>'По умолчанию', 1=>'Частичная выдача', 2=>'Запрет'])->prepend('Отсутствует', -1), old('ban_level'), ['class' => 'form-control select2-ws', 'required']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Примечания</label>
                    <div class="col-sm-9">
                        {{ Form::text('ban_reason', old('ban_reason'), ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Фронт-офис: Пользователь</label>
                    <div class="col-sm-9">
                        {{ Form::text('front_user_title', old('front_user_title'), ['class' => 'form-control', 'id'=>'front_user_title']) }}
                        <input type="hidden" name="front_user_id" id="front_user_id" value="{{$user->front_user_id}}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Подписант</label>
                    <div class="col-sm-9">
                        {{ Form::text('signer_fio', old('signer_fio'), ['class' => 'form-control', 'id'=>'signer_fio', 'placeholder' => 'Иванова Ивана']) }}
                    </div>
                </div>

             @if(isset($user) )
                <div class="form-group">
                    <label class="col-sm-3 control-label">Оклад</label>
                    <div class="col-sm-9">
                        {{ Form::text('zp', titleFloatFormat($user->zp), ['class' => 'form-control sum', 'id'=>'zp', 'placeholder' => '10000']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Доп. фин. расходы</label>
                    <div class="col-sm-9">
                        {{ Form::text('financial_expenses', titleFloatFormat($user->financial_expenses), ['class' => 'form-control sum', 'id'=>'financial_expenses', 'placeholder' => '10000']) }}
                    </div>
                </div>
                @endif

                @if(isset($user) && $user->is('referencer')) <!--рекомендодатель-->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Доля рекомендодателя</label>
                        <div class="col-sm-9">
                            {{ Form::text('referencer_kv', old('referencer_kv'), ['class' => 'form-control sum percents_input_validation', 'id'=>'referencer_kv']) }}
                        </div>
                    </div>
                @endif

                @if(isset($user) && $user->id>0)
                    <div class="form-group">
                        <div class="col-sm-11">
                            <span class="btn btn-primary btn-left" onclick="openFancyBoxFrame('{{url("/users/limit/?user_id={$user->id}")}}')">Редактировать лимиты</span><br>
                        </div>
                    </div>
                @endif

            </div>
        </div>

    </div>



    <div class="page-subheading">
        <h2 class="inline-h1">Агентские договора дополнительные</h2>
        @if(isset($user) && $user->id>0)
            <span class="btn btn-success btn-right" onclick="openFancyBoxFrame('{{url("/users/users/{$user->id}/agent_contracts/0/edit")}}')">Добавить</span>
        @endif
    </div>
    <div class="block-main">
        <div class="block-sub">
            <div class="form-horizontal">

                @if($user && $user->agent_contracts)

                    <table class="tov-table">
                        <tr>
                            <th>Организация</th>
                            <th>Номер договора</th>
                            <th>Дата начала</th>
                            <th>Дата окончания</th>
                            <th>Договор</th>
                        </tr>

                        @foreach($user->agent_contracts as $agent_contract)
                            @if($agent_contract && $agent_contract->organization)
                                @php($onclick = 'onclick=openFancyBoxFrame("'.url("/users/users/{$user->id}/agent_contracts/{$agent_contract->id}/edit").'")')
                                <tr style="cursor: pointer;@if($agent_contract->end_date < date("Y-m-d")) background-color:#FDD; @endif">
                                    <td {{$onclick}}>{{$agent_contract->organization->title}}</td>
                                    <td {{$onclick}}>{{$agent_contract->agent_contract_title}}</td>
                                    <td {{$onclick}}>{{setDateTimeFormatRu($agent_contract->begin_date, 1)}}</td>
                                    <td {{$onclick}}>{{setDateTimeFormatRu($agent_contract->end_date, 1)}}</td>
                                    <td>
                                        @if(\App\Models\Settings\Template::where('org_id', $agent_contract->organization->id)->first())
                                            <a class="btn btn-success" href="/directories/organizations/{{$agent_contract->organization->id}}/documentation">Скачать</a>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @endforeach


                    </table>

                @endif

            </div>
        </div>

    </div>



    <div class="page-subheading">
        <h2 class="inline-h1">Финансовые группы дополнительные</h2>
        @if(isset($user) && $user->id>0)
            <span class="btn btn-success btn-right" onclick="openFancyBoxFrame('{{url("/users/users/{$user->id}/agent_financial_policies/0/edit")}}')">Добавить</span>
        @endif
    </div>
    <div class="block-main">
        <div class="block-sub">
            <div class="form-horizontal">

                @if($user && $user->agent_financial_policies)

                    <table class="tov-table">
                        <tr>
                            <th>Финансовая группа</th>
                            <th>Дата начала</th>
                            <th>Дата окончания</th>
                        </tr>

                        @foreach($user->agent_financial_policies as $agent_financial_policies)
                            <tr style="cursor: pointer;@if($agent_financial_policies->end_date < date("Y-m-d")) background-color:#FDD; @endif"
                                onclick="openFancyBoxFrame('{{url("/users/users/{$user->id}/agent_financial_policies/{$agent_financial_policies->id}/edit")}}')">
                                <td>{{$agent_financial_policies->financial_policies_group->title}}</td>
                                <td>{{setDateTimeFormatRu($agent_financial_policies->begin_date, 1)}}</td>
                                <td>{{setDateTimeFormatRu($agent_financial_policies->end_date, 1)}}</td>
                            </tr>
                        @endforeach


                    </table>

                @endif

            </div>
        </div>

    </div>


</div>
@endif

@section('js')
    <script>

        $(document).on('change', '#organization_id', function () {
            var org_id = $(this).val();

            @if(isset($user) && $user->id>0)
                $.post('{{url("users/users/$user->id/get_points_departments")}}', {org_id: org_id}, function (response) {
                    $('#point_department_id').html(response);
                });

            @endif
        });

        function downloadAgentContract(org_id) {

            $.post('{{isset($user) ? url("users/users/$user->id/agent_contracts/download_agent_contract"): ''}}', {org_id: org_id}, function (response) {
                var link = document.createElement('a');

                var href = '{{ url("files")}}/'+ response['name'];

                link.setAttribute('href',href);
                link.setAttribute('download','download');

                link.click();
            });
        }

        $(function () {
            $('select.subject_type_id').change(function () {
                visibleUserTypeFields();
            });

            visibleUserTypeFields();

            visibleDriverFields();

            $('[name=second_name], [name=first_name], [name=middle_name], [name=title]').change(function () {
                setTitle();
            });

            $('[name=department_id]').change(function () {
                visibleDriverFields();
            });

            $('.image-input').change(function () {
                onChangeImageHandler(this, $('.car-image'))
            });

            searchFrontUser();

        });

        function visibleUserTypeFields() {
            if ($('select.subject_type_id').val() == '{{ \App\Models\Subject\Type::PHYSICAL }}') {
                $('.juridical').hide();
                $('.physical').show();
            } else {
                $('.juridical').show();
                $('.physical').hide();
            }
        }

        setTitle();

        function setTitle() {

            var title = '';

            if ($('select.subject_type_id').val() == '{{ \App\Models\Subject\Type::PHYSICAL }}') {

                var firstName = $('[name=first_name]').val();

                var secondName = $('[name=second_name]').val();

                var middleName = $('[name=middle_name]').val();

                $(".surname").val(secondName);
                $(".name").val(firstName);
                $(".lastname").val(middleName);

                title += secondName + ' ' + firstName + ' ' + middleName;

            } else {

                title = $('[name=title]').val();
            }

            $('[name=name]').val(title);
        }

        function visibleDriverFields() {
            var selectedOption = $('[name=department_id] option:selected');
            var isDriver = selectedOption.data('user-type-id') == '{{\App\Models\Users\Type::DRIVER}}';
            $('.driver-field').toggleClass('hidden', !isDriver);
        }

        function triggerInputFile() {
            $('.image-input').trigger('click');
        }

        function onChangeImageHandler(obj, imageSelector) {
            if (obj.files && obj.files[0]) {
                var FR = new FileReader();
                FR.onload = function (e) {
                    imageSelector.prop('src', e.target.result);
                    imageSelector.removeClass('hidden');
                };
                FR.readAsDataURL(obj.files[0]);
            }
        }


        function createAgentContract() {

            $('#agent_contract_title').val( $('[name="email"]').val());
            $('#agent_contract_begin_date').val('{{date("d.m.Y")}}');
            $('#agent_contract_end_date').val('{{date("d.m.Y", strtotime("+ 547 days"))}}');

        }

        function searchFrontUser()
        {
            $('#front_user_title').suggestions({
                serviceUrl: "/users/actions/search_front_user/",
                type: "PARTY",
                count: 5,
                minChars: 3,
                onSelect: function (suggestion) {

                    $('#front_user_title').val(suggestion.value);
                    $('#front_user_id').val(suggestion.data.id);



                }
            });
        }


        function setDataEnd() {

            start_date = $('#agent_contract_begin_date').val();
            var cur_date_tmp = start_date.split(".");

            var cur_date = new Date(cur_date_tmp[2], cur_date_tmp[1] - 1, cur_date_tmp[0]);

            var new_date = new Date(cur_date.setYear(cur_date.getFullYear() + 2));
            var new_date2 = new Date(new_date.setDate(new_date.getDate() - 182));
            $('#agent_contract_end_date').val(getFormattedDate(new_date2));

        }


    </script>
@append