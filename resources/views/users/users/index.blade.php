@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">{{ trans('menu.users') }}</h1>
        <a href="/users/users/create" class="btn btn-primary btn-right">
            {{ trans('form.buttons.create') }}
        </a>
    </div>

    <div class="row">
        <div class="col-lg-3" style="margin-left:10px;">
            {{ Form::text('fio', Request()->fio, ['class' => 'form-control form-control-with-button','placeholder' => 'ФИО', 'id'=>'fio', 'onkeyup' => 'loadItems()']) }}
            <span onclick="loadItems()" type="submit" class="btn btn-primary inline-block">Поиск</span>
        </div>

        <div class="col-lg-3">
            <input type="text" class="form-control inline-block" placeholder="Руководитель" onkeyup="loadItems()"
                   id="supervisor">
        </div>


    </div>

    <div id="table">

    </div>

    @include('_chunks/_pagination',['callback'=>'loadItems'])

@endsection

@section('js')

    <script src="/js/jquery.easyui.min.js"></script>


    <script>

        function get_data() {

            data = {
                supervisor: $('#supervisor').val(),
                fio: $('#fio').val(),
                page_count: $('[name="page_count"]').val(),
                page: PAGE
            };

            return data;
        }

        function loadItems() {

            $.get('/users/users/get_table', get_data(), function (res) {

                $('#view_row').html(res.view_row);
                $('#max_row').html(res.max_row);

                ajaxPaginationUpdate(res.page_max, loadItems);

                $('#table').html(res.result);


            })
        }

        $(function () {
            loadItems();
        })
    </script>
@endsection
