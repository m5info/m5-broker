@extends('layouts.frame')


@section('title')

    Пользователь {{$user->name}}
    @if((int)$agent_contract->id == 0)
        <button onclick="generateNumber({{$agent_contract->id}})" type="submit" class="btn-right btn btn-success">Сгенерировать</button>
    @endif
@endsection

@section('content')


    {{ Form::open(['url' => url("/users/users/{$user->id}/agent_contracts/".(int)$agent_contract->id."/edit"), 'method' => 'post', 'class' => 'form-horizontal']) }}

    <input type="hidden" name="finish" value="{{$finish}}"/>

    <div class="form-horizontal">


       <div class="form-group">
            <label class="col-sm-3 control-label">Организация</label>
            <div class="col-sm-9">
                {{ Form::select('org_id', $organizations->pluck('title', 'id'), isset($agent_contract) ? $agent_contract->org_id : 0, ['class' => 'form-control', 'required']) }}
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-12">
                <div class="col-sm-6">
                    {{ Form::text('agent_contract_title', isset($agent_contract) ? $agent_contract->agent_contract_title : '', ['class' => 'form-control', 'placeholder'=>'Номер договора', 'id'=>'agent_contract_title']) }}
                </div>

                <div class="col-sm-3">
                    {{ Form::text('begin_date', ($agent_contract)?setDateTimeFormatRu($agent_contract->begin_date, 1):'', ['class' => 'form-control datepicker date', 'placeholder'=>'Дата начала', 'id'=>'agent_contract_begin_date', "onchange"=>"setDataEnd()"]) }}
                </div>

                <div class="col-sm-3">
                    {{ Form::text('end_date', ($agent_contract)?setDateTimeFormatRu($agent_contract->end_date, 1):'', ['class' => 'form-control datepicker date', 'placeholder'=>'Дата окончания', 'id'=>'agent_contract_end_date']) }}
                </div>
            </div>
        </div>





    </div>


    {{Form::close()}}




@endsection

@section('footer')
    @if((int)$agent_contract->id == 0)
        @php($style = 'display:none')
    @else
        @php($style = '')
    @endif

    <span class="btn btn-success download_doc" style="float: left;{{ $style }}" >Скачать АД</span>

    <button onclick="submitForm()" type="submit" class="btn btn-primary" style="float: right;">{{ trans('form.buttons.save') }}</button>


@endsection




@section('js')
    <script>
        $(function () {


        });

        function getAgentContractNumb(){
            // получение title из расчета всех агентских договоров организации + 1
        }

        function getData(){
            return {
                org_id:$('[name="org_id"]').val(),
                agent_contract_title: $('[name="agent_contract_title"]').val(),
                begin_date: $('[name="begin_date"]').val(),
                end_date: $('[name="end_date"]').val(),
            }
        }

        function generateNumber(id) {

            $('[name="begin_date"]').val('{{ \Carbon\Carbon::now()->format('d.m.Y') }}');
            setDataEnd();
            @php($agent_contract_id = (int)$agent_contract->id)
            $.post('{{url("users/users/$user->id/agent_contracts/$agent_contract_id/get_agent_contract")}}', getData(), function(response){
                $('[name="agent_contract_title"]').val(response['agent_contract_title']);
                openFancyBoxFrame("/users/users/"+response['user_id']+"/agent_contracts/"+response['id']+"/edit?finish={{$finish}}");
                $('.download_doc').show();
            });
        }

        $('.download_doc').on('click', function () {
            var org_id = $('[name="org_id"]').val();
            location.href = '/directories/organizations/'+org_id+'/documentation?user={{$user->id}}';
        });

        function downloadAgentContract(user_id) {
            $.post('{{url("users/users/$user->id/agent_contracts/download_agent_contract")}}', {org_id:$('[name="org_id"]').val()}, function(response){
                var link = document.createElement('a');

                var href = '{{ url("files")}}/'+ response['name'];

                link.setAttribute('href',href);
                link.setAttribute('download','download');

                link.click();
            });
        }

        function setDataEnd() {

            start_date = $('#agent_contract_begin_date').val();
            var cur_date_tmp = start_date.split(".");

            var cur_date = new Date(cur_date_tmp[2], cur_date_tmp[1] - 1, cur_date_tmp[0]);

            var new_date = new Date(cur_date.setYear(cur_date.getFullYear() + 2));
            var new_date2 = new Date(new_date.setDate(new_date.getDate() - 182));
            $('#agent_contract_end_date').val(getFormattedDate(new_date2));

        }


    </script>
@append