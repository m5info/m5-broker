
<div class="wrapper1">
    <div class="div1 row col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
    </div>
</div>
<div class="wrapper2">
    <div class="div2 row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="container-for-table">
            <table class="table table-bordered payments_table">
                <thead>
                <tr class="tr-sticky">
                    <th>#</th>
                    <th>СК</th>
                    <th>Продукт</th>
                    <th>Полис №</th>
                    <th>Страхователь</th>
                    <th>Дата договора</th>
                    <th>Дата оплаты</th>
                    <th>Тип</th>
                    <th>Взнос</th>
                    <th>Оф. скидка</th>
                    <th>Неоф. скидка</th>
                    <th>Агент / Менеджер</th>
                </tr>
                </thead>
                <tbody>
                @foreach($payments as $key => $payment)
                    <tr @if($payment->payment_data <= date("Y-m-d")) style="background-color: red;" @endif>
                        <td>
                            <input type="checkbox" class="payment_ids" name="payment[]" value="{{$payment->id}}"/>
                        </td>
                        <td>{{$payment->bso && $payment->bso->insurance ? $payment->bso->insurance->title : "" }}</td>
                        <td>{{ $payment->bso && $payment->bso->product ? $payment->bso->product->title : ""}}</td>
                        <td>{{$payment->bso->bso_title}}</td>
                        <td>{{ $payment->Insurer }}</td>
                        <td>{{ setDateTimeFormatRu($payment->contract->sign_date,1) }}</td>
                        <td>{{ setDateTimeFormatRu($payment->payment_data,1) }}</td>
                        <td>{{\App\Models\Contracts\Payments::TRANSACTION_TYPE[$payment->type_id]}} @if($payment->type_id == 0) {{$payment->payment_number}} @endif</td>
                        <td>{{titleFloatFormat($payment->payment_total)}}</td>
                        <td>{{ getPriceFormat($payment->official_discount) }}</td>
                        <td>{{ getPriceFormat($payment->informal_discount) }}</td>
                        <td>{{$payment->manager ? $payment->manager->name : $payment->agent->name }}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $('.wrapper1').on('scroll', function (e) {
        $('.wrapper2').scrollLeft($('.wrapper1').scrollLeft());
    });
    $('.wrapper2').on('scroll', function (e) {
        $('.wrapper1').scrollLeft($('.wrapper2').scrollLeft());
    });

    $('.div1').width($('.payments_table').width());
    $('.div2').width($('.payments_table').width());
</script>

<style>

    .wrapper1, .wrapper2 { width: 100%; overflow-x: scroll; overflow-y: hidden; }
    .wrapper1 { height: 20px; }
    .wrapper2 {}
    .div1 { height: 20px; }
    .div2 { overflow: none; }


</style>