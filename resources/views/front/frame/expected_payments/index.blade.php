@extends('layouts.front')

@section('content')

    <div class="page-heading">
        <h2>Ожидаемые платежи</h2>
    </div>
    <div class="divider"></div>

    <form name="main_container">
        <div class="block-main">
            <div class="block-sub">
                <div class="form-horizontal">
                    <div class="form-group">



                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6 col-lg-2">
                                    <label class="control-label">Дата оплаты с</label>
                                    {{ Form::text('date_from', '', ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off']) }}
                                </div>
                                <div class="col-sm-6 col-lg-2">
                                    <label class="control-label">Дата оплаты по</label>
                                    {{ Form::text('date_to', '', ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off']) }}
                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <label class="control-label">БСО</label>
                                    {{ Form::text('bso_title', '', ['class' => 'form-control inline', 'autocomplete' => 'off']) }}
                                </div>

                                <span class="btn btn-primary pull-left" onclick="loadItems()" style="margin-top: 20px;">Применить</span>

                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="row">

                                <div class="col-sm-6 col-lg-4">
                                    <label class="control-label">Агент / Менеджер</label>
                                    {{ Form::select('agent_id', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id')->prepend('Не выбран', 0), 0, ['class' => 'form-control select2 ', 'id'=>'agent_id', 'required']) }}

                                </div>

                                <span class="btn btn-success pull-left" onclick="setSecondPayment()" style="margin-top: 20px;">Назначить</span>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="data_table" >

    </div>

@endsection


@section('js')

    <script>


        $(function () {
            loadItems();

        });



        function loadItems()
        {
            var data = $('form[name="main_container"]').serialize();

            $.get("{{url("/front/expected_payments/get_payments_table")}}", data, function (res) {

                $('#data_table').html(res);

            });

        }


        function setSecondPayment() {


            if($("#agent_id").val() == 0){

                newAlert2("Не выбран менеджер", "Обязательное поле");
                return null;
            }

            if($('[name="payment[]"]:checked').length == 0){

                newAlert2("Выберите платежи", "Обязательное поле");
                return null;
            }




            loaderShow();
            $.post("{{url("/front/expected_payments/send-payments")}}", getEventData(), function (response) {

                loaderHide();

                loadItems();

            }).done(function () {
                loaderShow();
            }).fail(function () {
                loaderHide();
            }).always(function () {
                loaderHide();
            });




        }


        function getEventData() {
            var event_data = {
                payment_ids: [],
                agent_id: $("#agent_id").val(),

            };
            $.each($('[name="payment[]"]:checked'), function (k, v) {
                event_data.payment_ids.push($(v).val());
            });
            return event_data;
        }


    </script>


@endsection