@extends('layouts.app')

@section('head')

    <style>
        tr.green{
            background-color: #ffd6cc;
        }
    </style>

@append

@section('content')
    <div class="page-heading">
        <h2>Акт передачи БСО в Андеррайтинг # {{$act->act_number}} от {{setDateTimeFormatRu($act->time_create)}}</h2>
    </div>
    <div class="divider"></div>

    <br/>
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="form-horizontal">
                <div class="form-group">

                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <label class="col-sm-12 control-label">Тип акта</label>
                        <div class="col-sm-12">
                            {{$act->act_name}}
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <label class="col-sm-12 control-label">Создал</label>
                        <div class="col-sm-12">
                            {{ $act->user_from ? $act->user_from->name : ""}}
                        </div>
                    </div>

                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <a class="btn btn-success pull-right doc_export_btn" href="/cashbox/acts_to_underwriting/details/{{$act->id}}/export">Выгрузить в XLS</a>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

                        @if($act->state_id == 0)
                            <span class="btn btn-danger pull-right" onclick="deleteAct()">Удалить акт</span>
                            <hr/>
                            @include('cashbox.acts_to_underwriting.info.details.edit', ['act'=>$act])
                        @endif


                    </div>
                </div>
            </div>


        </div>

    </div>

    <div class="divider"></div>



    @include('cashbox.acts_to_underwriting.info.contract.list', ['act'=>$act, 'payments'=> $payments])




@endsection

@section('js')

    <script>

        $(function () {

        });

        function show_checked_options() {
            show_actions();
        }

        function check_all_bso(obj) {
            $('.bso_item_checkbox').attr('checked', $(obj).is(':checked'));
            show_actions();
        }

        function show_actions() {
            if ($('.bso_item_checkbox:checked').length > 0) {
                $('.event_form').show();
            } else {
                $('.event_form').hide();
            }

            $('.event_td').addClass('hidden');
            $('.event_' + $('#event_id').val()).removeClass('hidden');
            highlightSelected();

        }

        function highlightSelected() {
            $('.bso_item_checkbox').each(function(){
                $(this).closest('tr').toggleClass('green', $(this).is(':checked'));
            });
        }



        @if($act->state_id == 0)

        function deleteAct() {

            loaderShow();

            $.post("{{url("/cashbox/acts_to_underwriting/details/{$act->id}/delete_act")}}", {}, function (response) {
                loaderHide();
                window.location = '{{url("/cashbox/acts_to_underwriting/")}}';

            }).always(function() {
                loaderHide();
            });

        }

        function deleteItemsAct() {

            var item_array = [];
            $('.bso_item_checkbox:checked').each(function () {
                item_array.push($(this).val());
            });


            loaderShow();

            $.post("{{url("/cashbox/acts_to_underwriting/details/{$act->id}/delete_items")}}", {item_array: JSON.stringify(item_array)}, function (response) {
                loaderHide();

                reload();

            }).always(function() {
                loaderHide();
            });


        }

        @endif

    </script>


@endsection