@if(sizeof($payments))
    <div class="form-horizontal">
        <table class="tov-table table table-bordered">
            <thead>
            <tr class="sort-row">
                <th>№ п\п</th>
                <th>Агент</th>
                <th>СК</th>
                <th>Вид страхования</th>
                <th>Страхователь</th>
                <th>№ Бланка</th>
                <th>№ Квитанции</th>
                <th>Неоф. скидка %</th>
                <th>Статус</th>
                <th>Дата создания</th>
                <th>Дата принятия в кассу</th>
            </tr>
            </thead>
            <tbody>

            @foreach($payments as $key => $payment)
                <tr class="clickable-row">
                    <td>{{$key+1}}</td>
                    <td>{{$payment->agent? $payment->agent->name : ""}}</td>
                    <td>{{$payment->bso && $payment->bso->supplier ? $payment->bso->supplier->title : ""}}</td>
                    <td>{{$payment->bso && $payment->bso->type ? $payment->bso->type->title : ""}}</td>
                    <td>{{$payment->contract->insurer ? $payment->contract->insurer->title : ""}}</td>
                    <td>{{$payment->bso ? $payment->bso->bso_title : ""}}</td>

                    <td>{{$payment->bso_receipt}}</td>

                    <td>

                        <a href="{{ $payment->invoice_id > 0 ? "/finance/invoice/invoices/{$payment->invoice_id}/edit" : "#" }}" target="_blank">
                            {{getPriceFormat($payment->informal_discount)}}
                        </a>
                    </td>

                    <td>{{\App\Models\Contracts\Contracts::STATYS[$payment->contract->statys_id]}}</td>
                    <td>{{getDateFormatRu($payment->contract->created_at)}}</td>
                    <td>{{$payment->invoice_payment_date ? getDateFormatRu($payment->invoice_payment_date) : ""}}</td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
@else
    <h1>Договоры отсутствуют</h1>
@endif

