
<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="filter-group">

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="user_id_from">Агент/Менеджер</label>
                    {{ Form::select('agent_id', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id')->prepend('Все',0), 0, ['class' => 'form-control select2-all', 'onchange'=>'loadContent()']) }}
                </div>
                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="user_id_from">Номер акта</label>
                     {{ Form::text('number', '', ['class' => 'form-control', 'onkeyup' => 'loadContent()']) }}
                </div>
                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="user_id_from">Дата с</label>
                     {{ Form::text('date_from', '', ['class' => 'form-control date datepicker', 'onchange' => 'loadContent()']) }}
                </div>
                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="user_id_from">Дата по</label>
                     {{ Form::text('date_to', '', ['class' => 'form-control date datepicker', 'onchange' => 'loadContent()']) }}
                </div>

            </div>
        </div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div id="table"></div>
    </div>
</div>

<script>


    function initTab() {
        startMainFunctions();
        loadContent();
    }

    function loadContent(resetPage = false){
        if(resetPage){
            PAGE = 1
        }
        loaderShow();

        $('#page_list').html('');
        $('#table_row').html('');
        $('#view_row').html(0);
        $('#max_row').html(0);

        $.post("{{url("/cashbox/acts_to_underwriting/acts/list/")}}", getData(), function (response) {
            loaderHide();

            $('#table').html(response.html);

            $('#view_row').html(response.view_row);
            $('#max_row').html(response.max_row);

            ajaxPaginationUpdate(response.page_max,loadContent);

            $(".clickable-row").click( function(){
                if ($(this).attr('data-href')) {
                    window.location = $(this).attr('data-href');
                }
            });

        }).always(function() {
            loaderHide();
        });
    }

    function getData(){
        return {
            PAGE: PAGE,
            page_count: $('[name="page_count"]').val(),
            agent_id: $('[name="agent_id"]').val(),
            number: $('[name="number"]').val(),
            date_from: $('[name="date_from"]').val(),
            date_to: $('[name="date_to"]').val(),
        };
    }



</script>