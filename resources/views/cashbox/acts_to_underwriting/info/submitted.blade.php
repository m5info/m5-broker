
<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="filter-group">

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="user_id_from">Агент</label>
                    {{ Form::select('agent_id', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id')->prepend('Все', 0), 0, ['class' => 'form-control select2-all', 'onchange'=>'loadContent()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="user_id_from">Акцепт</label>
                    {{ Form::select('kind_acceptance', collect(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE)->prepend('Не выбрано', -1), -1, ['class' => 'form-control select2-ws', 'onchange'=>'loadContent()']) }}
                </div>
                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="user_id_from">Дата с</label>
                    {{ Form::text('date_from', '', ['class' => 'form-control date datepicker', 'onchange' => 'loadContent()']) }}
                </div>
                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="user_id_from">Дата по</label>
                    {{ Form::text('date_to', '', ['class' => 'form-control date datepicker', 'onchange' => 'loadContent()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="user_id_from">Вид страхования</label>
                    {{ Form::select('product', \App\Models\Directories\Products::all()->pluck('title','id')->prepend('Все',0), 0, ['class' => 'form-control select2-ws', 'onchange'=>'loadContent()']) }}
                </div>
                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="user_id_from">СК</label>
                    {{ Form::select('sk', \App\Models\Directories\InsuranceCompanies::all()->pluck('title','id')->prepend('Все',0), 0, ['class' => 'form-control select2-ws', 'onchange'=>'loadContent()']) }}
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="data_list" style="overflow: auto;">

</div>


<script>

    function initTab() {
        startMainFunctions();
        loadContent();
    }

    function loadContent(){

        loaderShow();
        $('#page_list').html('');
        $('#table_row').html('');
        $('#view_row').html(0);
        $('#max_row').html(0);
        $.post("{{url("/cashbox/acts_to_underwriting/submitted/list/")}}", getData(), function (response) {
            loaderHide();
            $("#data_list").html(response.html);

            $('#view_row').html(response.view_row);
            $('#max_row').html(response.max_row);

            ajaxPaginationUpdate(response.page_max,loadContent);

        }).always(function() {
            loaderHide();
        });
    }

    function getData(){
        return {
            kind_acceptance: $('[name="kind_acceptance"]').val(),
            page_count: $('[name = "page_count"]').val(),
            date_from: $('[name="date_from"]').val(),
            agent_id: $('[name="agent_id"]').val(),
            product: $('[name="product"]').val(),
            date_to: $('[name="date_to"]').val(),
            sk: $('[name="sk"]').val(),
            PAGE: PAGE
        };

    }


</script>
