@extends('layouts.frame')

@section('title')
@php
    $types = [
        'report' => 'Отчет',
        'incomes' => 'Доп. доходы',
        'expenses' => 'Расходы'
    ];
@endphp
    <h2 class="inline-h1">Учредительские выплаты - {{ $types[$type] }} - {{getMonthById($month)}} {{$year}}</h2>

@endsection

@section('content')

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Категория</th>
                <th>Статус</th>
                <th>Дата платежа</th>
                <th>Тип оплаты</th>
                <th>Сумма</th>
                <th>Комиссия</th>
                <th>Итого</th>
                <th>Комментарий</th>
            </tr>
            </thead>
            <tbody>
            @if(sizeof($incomes_expenses))
                @php
                    $minus_sum = 1;
                    $minus_total = 1;
                    $fix = 0;
                @endphp

                @foreach($incomes_expenses as $income_expense)
                    @php
                        $ied =new DateTime($income_expense->date);

                        if ($accept_date && $ied > $accept_date){
                            $fix += 1;
                        }
                    @endphp
                    @if($fix == 1)

                        <tr class="border-grey">
                            <td colspan="8">Произведена фиксация</td>
                        </tr>
                    @endif
                    <tr @if($income_expense->status_id == 1) class="bg-yellow" @elseif($income_expense->status_id == 2) class="bg-red" @elseif($income_expense->payment_type == 0) class="bg-green"  @endif>
                        <td>{{ $income_expense->category ? $income_expense->category->title : "" }}</td>
                        <td>{{ $income_expense->status_ru() }}</td>
                        <td>{{ setDateTimeFormatRu($income_expense->date, 1) }}</td>
                        <td>{{ $income_expense->payment_type_ru('payment_type') }}</td>
                        <td class="nowrap">{{ getPriceFormat($income_expense->category->type == 2 ? $income_expense->sum * -1 : $income_expense->sum) }}</td>
                        <td class="nowrap">{{ getPriceFormat($income_expense->commission_total) }}</td>
                        <td class="nowrap">{{ getPriceFormat($income_expense->category->type == 2 ? $income_expense->total * -1 : $income_expense->total) }}</td>
                        <td>{{ $income_expense->comment }}</td>
                    </tr>
                    @php
                        if($income_expense->category->type == 2){
                            $minus_sum = -1;
                            $minus_total = -1;
                        }
                    @endphp
                @endforeach
                @if($fix == 0)
                    <tr class="border-grey">
                        <td colspan="8">Произведена фиксация</td>
                    </tr>
                @endif
                <tr>
                    <th colspan="4"></th>
                    <th class="nowrap">{{titleFloatFormat($incomes_expenses->sum('sum') * $minus_sum)}}</th>
                    <th class="nowrap">{{titleFloatFormat($incomes_expenses->sum('commission_total'))}}</th>
                    <th class="nowrap">{{titleFloatFormat($incomes_expenses->sum('total') * $minus_total)}}</th>
                    <th colspan="3"></th>
                </tr>
            @else
                <tr>
                    <td colspan="8" class="text-center">Нет расходов/доп доходов</td>
                </tr>
            @endif
            </tbody>
        </table>

        <style>
            .block-main .block-sub {
                padding: 0;
            }
            tr th, tr td {
                padding: 7px;
            }
        </style>
@endsection



@section('js')

    <script>

        $(document).on('click', '.close-fb', function () {
            $.fancybox.close()
        })
    </script>

@endsection

@section('footer')


@endsection

