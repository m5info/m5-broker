@extends('layouts.frame')

@section('title')
    @php
        $types = [
            'report' => 'Отчет',
            'incomes' => 'Доп. доходы',
            'expenses' => 'Расходы'
        ];
    @endphp
    <h2 class="inline-h1">Учредительские выплаты - {{ $types[$type] }}</h2>

@endsection

@section('content')

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Название</th>
            <th>СК</th>
            <th>СП</th>
            <th>Комиссия общая</th>
            <th>Суммы к перечислению</th>
            <th>Суммы к возврату</th>
            <th>Комментарий</th>
        </tr>
        </thead>
        <tbody>
        @if(sizeof($report))
            @foreach($report as $rep)
                <tr>
                    <td><a href="{{url("/reports/order/{$rep->id}")}}" target="_blank">{{$rep->title}}</a> </td>
                    <td>{{$rep->bso_supplier && $rep->bso_supplier->insurance ? $rep->bso_supplier->insurance->title : 'Неизвестно'}}</td>
                    <td class="nowrap">{{titleFloatFormat($rep->payment_total)}}</td>
                    <td class="nowrap">{{titleFloatFormat($rep->amount_total)}}</td>
                    <td class="nowrap">{{titleFloatFormat($rep->to_transfer_total)}}</td>
                    <td class="nowrap">{{titleFloatFormat($rep->to_return_total)}}</td>
                    <td>{{$rep->comments}}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td class="text-center" colspan="7">Нет отчетов</td>
            </tr>
        @endif
        </tbody>
    </table>

    <style>
        .block-main .block-sub {
            padding: 0;
        }
        tr {
            background-color: #e8e8e8;
        }
        tr th, tr td {
            padding: 7px;
        }
    </style>
@endsection


@section('js')

    <script>

        $(document).on('click', '.close-fb', function () {
            $.fancybox.close()
        })
    </script>

@endsection

@section('footer')


@endsection

