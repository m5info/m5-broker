@extends('layouts.frame')

@section('title')

    Расход / доп доход


@endsection

@section('content')

    {{ Form::open(['url' => url('/cashbox/foundation_payments/save_incomes_expenses'), 'method' => 'post', 'class' => 'form-horizontal']) }}
    <input type="hidden" name="fact_id" value="{{$fact_id}}"/>

    <div class="form-group">
        <label class="col-xs-2 col-sm-3 control-label">Тип</label>
        <div class="col-xs-10 col-sm-9">
            {{ Form::select('category_type', collect(\App\Models\Settings\IncomeExpenseCategory::TYPE), ($total>0)?1:2,  ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-2 col-sm-3 control-label">Категория</label>
        <div class="col-xs-10 col-sm-9">
            {{ Form::select('category_id', [], null, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-2 col-sm-3 control-label">Тип оплаты</label>
        <div class="col-xs-10 col-sm-9">
            {{ Form::select('payment_type', (\App\Models\Cashbox\IncomeExpense::PAYMENT_TYPE), 0, ['class' => 'form-control']) }}
        </div>
    </div>


    <div class="form-group">
        <label class="col-xs-2 col-sm-3 control-label">Сумма</label>
        <div class="col-xs-10 col-sm-9">
            {{ Form::text('sum', titleFloatFormat(($total<0)?($total*-1):$total), ['class' => 'form-control sum']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-2 col-sm-3 control-label">Дата платежа</label>
        <div class="col-xs-10 col-sm-9">
            {{ Form::text('date', date("d.m.Y"), ['class' => 'form-control datepicker date']) }}
        </div>
    </div>

    {{Form::close()}}

@endsection


@section('footer')


    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection



@section('js')
    <script>

        $(function(){

            load_categories();

            $(document).on('change', '[name="category_type"]', function(){
                load_categories();
            })

        });


        function load_categories(){
            loaderShow();
            var data = {category_type: $('[name="category_type"]').val(),};
            $.post('/cashbox/incomes_expenses/get_categories/', data, function(res){
                var select = $('[name="category_id"]');
                var options = '';

                $.each(res, function(k,v){
                    options += '<option value="'+v.id+'" '
                        + ((parseInt(v.id) === parseInt(select)) ? 'selected' : '')
                        +'>'+v.title+'</option>';
                });

                select.html(options);
            }).always(function(){
                loaderHide();
            });

        }
    </script>
@endsection