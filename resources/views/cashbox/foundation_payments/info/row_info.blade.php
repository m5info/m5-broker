<tr>
    @if(isset($type))
        <td onclick="openFancyBoxFrame('{{ url("/cashbox/foundation_payments/{$month}/{$year}/{$type}/detail")  }}')" >
            <a href="#">{{$name}}</a></td>
    @else
        <td>{{$name}}</td>
    @endif
    <td style="color: {{isColoRedGrin($fact, 1)}}">{{titleFloatFormat($fact)}}</td>
    <td style="color: {{isColoRedGrin($payment, 1)}}">{{titleFloatFormat($payment)}}</td>
    <td style="color: {{isColoRedGrin($total, 1)}}">{{titleFloatFormat($total)}}</td>
    @if($fact_id > 0)
        @if($result->fact_id > 0)
            <td style="color: {{isColoRedGrin($sum)}}">{{titleFloatFormat($sum)}}</td>
        @else
            <td>---</td>
        @endif
    @else
        <td>---</td>
    @endif
</tr>