@extends('layouts.app')

@section('content')


<div class="col-md margin20 animated fadeInRight">
    <h2 class="inline-h1">Учредительские выплаты - {{getMonthById($month)}} {{$year}}
    @if($result->fact_id > 0)
        - Фиксация {{getDateFormatRu($result->fact_date)}} {{($result->user)?$result->user->name:''}}
    @endif

    @if($result->payment_profit_total != null)
        - Выплата {{getDateFormatRu($result->fact->payment_date)}}
    @endif
    </h2>

</div>

<br/><br/>
<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">




    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">



        <table class="tov-table">
            <tr>
                <th>Тип</th>
                <th>Фактические</th>
                <th>Выплата</th>
                <th>Текущие</th>
                <th>Дельта</th>
            </tr>


            @include('cashbox.foundation_payments.info.row_info', ['name'=> 'Отчет', 'fact_id'=>$result->fact_id, 'fact'=>$result->fact_report_total, 'payment'=>$result->payment_report_total, 'total'=>$result->report_total, 'sum'=>($result->report_total - ($result->payment_report_total?:$result->fact_report_total)), 'type' => 'report'])


            @include('cashbox.foundation_payments.info.row_info', ['name'=> 'Доп. доходы', 'fact_id'=>$result->fact_id, 'fact'=>$result->fact_incomes_total, 'payment'=>$result->payment_incomes_total, 'total'=>$result->incomes_total, 'sum'=>($result->incomes_total - ($result->payment_incomes_total?:$result->fact_incomes_total)), 'type' => 'incomes'])


            @include('cashbox.foundation_payments.info.row_info', ['name'=> 'Расходы', 'fact_id'=>$result->fact_id, 'fact'=>$result->fact_expenses_total, 'total'=>$result->expenses_total, 'payment'=>$result->payment_expenses_total, 'sum'=>(($result->payment_expenses_total?:$result->fact_expenses_total)- $result->expenses_total), 'type' => 'expenses'])


            @include('cashbox.foundation_payments.info.row_info', ['name'=> 'Прибыль', 'fact_id'=>$result->fact_id, 'fact'=>$result->fact_profit_total, 'total'=>$result->profit_total, 'payment'=>$result->payment_profit_total, 'sum'=>($result->profit_total - ($result->payment_profit_total?:$result->fact_profit_total)  )])


        </table>





        @if($result->status_id == 0 || $result->payment_profit_total == null)

            <div class="row" style="padding-top: 15px;">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    {{ Form::text('dates', getDateRu(), ['class' => 'form-control datepicker date', 'id' => 'dates']) }}
                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <span class="btn btn-primary doc_export_btn pull-left" onclick="setFoundationPayments()">{{ $result->status_id == 0 ? 'Фиксировать данные' : 'Фиксировать выплату'}} </span>
                </div>

            </div>


        @endif


        @if($result->status_id == 1)
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
        <br/><br/>
        <h3>Зачисления на баланс <span class="btn btn-primary doc_export_btn pull-right" onclick="setFoundationPaymentsBalance()">Начислить</span></h3>
        <table class="tov-table">
            <tr>
                <th>Дата</th>
                <th>Получатель</th>
                <th>Сумма</th>
                <th>Комментарий</th>
            </tr>
            @if($result->fact && sizeof($result->fact->foundation_payments_balance))
                @foreach($result->fact->foundation_payments_balance as $balance)
                    <tr>
                        <td>{{ setDateTimeFormatRu($balance->event_date) }}</td>
                        <td>{{ $balance->balanse->agent->name }}</td>
                        <td>{{ titleFloatFormat($balance->total_sum) }}</td>
                        <td>{{ $balance->purpose_payment }}</td>
                    </tr>
                @endforeach
            @endif
            <tr>
                <th colspan="2">Итого</th>
                <th><span style="font-size: 16px;">{{ titleFloatFormat($result->fact->foundation_payments_balance->sum('total_sum')) }}</span></th>
                <th></th>
            </tr>
        </table>
        @endif


    </div>

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">



        <table class="tov-table">
            <tr>
                <th>Тип</th>
                <th>Категория</th>
                <th>Статус</th>
                <th>Дата</th>
                <th>Сумма</th>
            </tr>

            @if($result->fact && sizeof($result->fact->incomes_expenses))
                @foreach($result->fact->incomes_expenses as $income_expense)
                    <tr class="clickable-row-blank" data-href="/cashbox/incomes_expenses/{{$income_expense->id}}/edit">
                        <td>{{ \App\Models\Settings\IncomeExpenseCategory::TYPE[$income_expense->category ? $income_expense->category->type : 2] }}</td>
                        <td>{{ $income_expense->category->title }}</td>
                        <td>{{ $income_expense->status_ru() }}</td>
                        <td>{{ setDateTimeFormatRu($income_expense->date, 1) }}</td>
                        <td>{{ getPriceFormat($income_expense->sum) }}</td>
                    </tr>

                @endforeach
            @endif

        </table>

        <br/>
        @if($result->fact_id > 0)

            @if($result->status_id == 1)
            <span class="btn btn-success doc_export_btn pull-left" onclick="openFancyBoxFrame('{{url("/cashbox/foundation_payments/$month/$year/foundation")}}?total={{$result->fact_profit_total}}')">Учредительские</span>

            @endif

            <span class="btn btn-primary doc_export_btn pull-right" onclick="openFancyBoxFrame('{{url("/cashbox/foundation_payments/$month/$year/incomes_expenses")}}?total={{$result->profit_total - $result->fact_profit_total-$result->fact_delta_total}}')">Расходы / Доходы</span>


        @endif

    </div>



</div>


@endsection


@section('js')

<script>

    $(function () {


    });


    function setFoundationPayments()
    {
        if(myGetAjax('{{url("/cashbox/foundation_payments/$month/$year/accept_foundation_payments")}}?dates='+$("#dates").val()) == 200){
            reload();
        }


    }


    function setFoundationPaymentsBalance()
    {
        openFancyBoxFrame('{{url("/cashbox/foundation_payments/$month/$year/balance_foundation_payments")}}');
    }





</script>
@endsection

