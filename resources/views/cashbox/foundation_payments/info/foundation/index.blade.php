@extends('layouts.frame')

@section('title')

    Учредительские выплаты


@endsection

@section('content')

    {{ Form::open(['url' => url('/cashbox/foundation_payments/save_foundation'), 'method' => 'post', 'class' => 'form-horizontal']) }}
    <input type="hidden" name="fact_id" value="{{$fact_id}}"/>



    <div class="form-group">
        <label class="col-xs-2 col-sm-3 control-label">Категория расхода</label>
        <div class="col-xs-10 col-sm-9">
            <input type="hidden" name="category_type" value="2"/>
            {{ Form::select('category_id', [], null, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-2 col-sm-3 control-label">Отчет</label>
        <div class="col-xs-10 col-sm-5">
            {{ Form::select('month', collect(getRuMonthes()), date("m"), ['class' => 'form-control']) }}
        </div>
        <div class="col-xs-10 col-sm-4">
            {{ Form::select('year', collect(getYearsArrey()), date("Y"), ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-2 col-sm-3 control-label">Категория дохода</label>
        <div class="col-xs-10 col-sm-9">
            <input type="hidden" name="income_category_type" value="1"/>
            {{ Form::select('income_category_id', [], null, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-2 col-sm-3 control-label">Тип оплаты</label>
        <div class="col-xs-10 col-sm-9">
            {{ Form::select('payment_type', (\App\Models\Cashbox\IncomeExpense::PAYMENT_TYPE), 1, ['class' => 'form-control ', 'disabled']) }}
        </div>
    </div>


    <div class="form-group">
        <label class="col-xs-2 col-sm-3 control-label">Сумма</label>
        <div class="col-xs-10 col-sm-9">
            {{ Form::text('sum', titleFloatFormat(($total<0)?($total*-1):$total), ['class' => 'form-control sum']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-2 col-sm-3 control-label">Дата платежа</label>
        <div class="col-xs-10 col-sm-9">
            {{ Form::text('date', date("d.m.Y"), ['class' => 'form-control datepicker date']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-12 col-sm-12 control-label">Комментарий</label>
        <div class="col-xs-12 col-sm-12">
            {{ Form::textarea('comment', '', ['class' => 'form-control']) }}
        </div>
    </div>

    {{Form::close()}}

@endsection


@section('footer')


    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection



@section('js')
    <script>

        $(function(){

            load_categories(2, 'category_id');
            load_categories(1, 'income_category_id');

        });


        function load_categories(category_type, select_category_name){
            loaderShow();
            $.post('/cashbox/incomes_expenses/get_categories/', {category_type: category_type}, function(res){
                var select = $('[name="'+select_category_name+'"]');
                var options = '';

                $.each(res, function(k,v){
                    options += '<option value="'+v.id+'" '
                        + ((parseInt(v.id) === parseInt(select)) ? 'selected' : '')
                        +'>'+v.title+'</option>';
                });

                select.html(options);
            }).always(function(){
                loaderHide();
            });

        }
    </script>
@endsection