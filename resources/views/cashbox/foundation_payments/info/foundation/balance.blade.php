@extends('layouts.frame')

@section('title')

    Учредительские выплаты


@endsection

@section('content')


    {{ Form::open(['url' => url('/cashbox/foundation_payments/set_balance_foundation_payments'), 'method' => 'post', 'class' => 'form-horizontal']) }}
    <input type="hidden" name="fact_id" value="{{$fact->fact_id}}"/>



    <div class="form-group">
        <div class="col-sm-12">
            <label class="col-sm-12 control-label">Получатель</label>
            <div class="col-sm-12">
                <div class="col-sm-8">
                    {{ Form::select('agent_id', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id'), 0, ['class' => 'form-control select2', 'id'=>'agent_id', 'onchange'=>'loadBalance()']) }}
                </div>
                <div class="col-sm-4" id="view_balance" role="group" aria-label="">
                </div>
            </div>

        </div>
    </div>


    <div class="form-group">
        <label class="col-xs-2 col-sm-3 control-label">Сумма</label>
        <div class="col-xs-10 col-sm-9">
            {{ Form::text('sum', '', ['class' => 'form-control sum']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-2 col-sm-3 control-label">Комментарий</label>
        <div class="col-xs-12 col-sm-12">
            {{ Form::textarea('comments', '', ['class' => 'form-control']) }}
        </div>
    </div>

    {{Form::close()}}

@endsection


@section('footer')


    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection



@section('js')
    <script>

        $(function(){

            loadBalance();


        });


        function loadBalance(){
            //$("#balanse_info").html("");
            $.post("{{url("/cashbox/user_balance/get_balance/")}}", {agent_id: $('#agent_id').val(), }, function (response) {

                $("#view_balance").html(response.select_balance);
                setTypeId();

            });
        }

    </script>
@endsection