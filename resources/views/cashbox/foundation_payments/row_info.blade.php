
<tr style="cursor: pointer;@if($result->fact_id>0 && $result->status_id == 1) background-color:#ccffdd @endif" onclick="openpage('{{url("/cashbox/foundation_payments/$month/$year/")}}')">
    <td>{{getOneMonth($month)}}</td>

    @include('cashbox.foundation_payments.row_td', ['fact'=>$result->fact_report_total, 'total'=>$result->report_total, 'fact_id'=>$result->fact_id])
    @include('cashbox.foundation_payments.row_td', ['fact'=>$result->fact_incomes_total, 'total'=>$result->incomes_total, 'fact_id'=>$result->fact_id])
    @include('cashbox.foundation_payments.row_td', ['fact'=>$result->fact_expenses_total, 'total'=>$result->expenses_total, 'fact_id'=>$result->fact_id])
    @include('cashbox.foundation_payments.row_td', ['fact'=>$result->fact_profit_total, 'total'=>($result->profit_total-$result->fact_delta_total), 'fact_id'=>$result->fact_id])

</tr>