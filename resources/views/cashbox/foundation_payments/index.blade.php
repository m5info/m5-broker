@extends('layouts.app')

@section('content')


<div class="col-md margin20 animated fadeInRight">
    <h2 class="inline-h1">Учредительские выплаты</h2>
    <br><br>
    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row form-group">
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <label class="control-label">Год</label>
                {{ Form::select('year', collect(getYearsArrey()), $year, ['class' => 'form-control select2-ws', 'id'=>'year', 'onchange' => 'loadItems()']) }}
            </div>

        </div>
    </div>
</div>

<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">

    <table class="tov-table table table-bordered">
        <thead>
        <tr>
            <th>Месяц</th>
            <th colspan="2">Отчеты</th>
            <th colspan="2">Доп. доходы</th>
            <th colspan="2">Расходы</th>
            <th colspan="2">Прибыль</th>
        </tr>
        </thead>
        <tbody>
        @for($month=1;$month<=12;$month++)
            @php $result =\App\Models\Cashbox\FoundationPayments::getInfoReportData($month, $year) @endphp
            <tr style="cursor: pointer;@if($result->fact_id>0 && $result->status_id == 1) background-color:#ccffdd @endif" onclick="openpage('{{url("/cashbox/foundation_payments/$month/$year/")}}')">
                <td>{{getOneMonth($month)}}</td>

                @include('cashbox.foundation_payments.row_td', ['fact'=>$result->fact_report_total, 'total'=>$result->report_total, 'fact_id'=>$result->fact_id, 'status_id'=>$result->status_id])
                @include('cashbox.foundation_payments.row_td', ['fact'=>$result->fact_incomes_total, 'total'=>$result->incomes_total, 'fact_id'=>$result->fact_id, 'status_id'=>$result->status_id])
                @include('cashbox.foundation_payments.row_td', ['fact'=>$result->fact_expenses_total, 'total'=>$result->expenses_total, 'fact_id'=>$result->fact_id, 'status_id'=>$result->status_id])
                @include('cashbox.foundation_payments.row_td', ['fact'=>$result->fact_profit_total, 'total'=>($result->profit_total-$result->fact_delta_total), 'fact_id'=>$result->fact_id, 'status_id'=>$result->status_id])

            </tr>
        @endfor
        </tbody>
    </table>
</div>
@endsection


@section('js')

<script>

    function loadItems()
    {
        window.location = '/cashbox/foundation_payments/?year='+$("#year").val();
    }

</script>
@endsection

