@if($fact_id > 0 && $fact!=$total && $status_id == 1)

    <td>{{titleFloatFormat($fact)}}</td>
    <td style="color: {{isColoRedFoundation($fact, $total)}}">{{titleFloatFormat($total-$fact)}}</td>

@else
    <td colspan="2">{{titleFloatFormat($total)}}</td>
@endif
