<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="filter-group" id="filters">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                    <label class="control-label">Агент</label>
                    {{ Form::select('agent_id', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id')->prepend('Все', 0), 0, ['class' => 'form-control select2', 'id'=>'agent_id', 'onchange'=>'loadData()']) }}
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <label class="control-label">Номер счета</label>
                    {{ Form::text('invoice_number', '', ['class' => 'form-control', 'onchange' => 'loadData()']) }}
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <label class="control-label">Статус</label>
                    @php($status_select = collect(\App\Models\Finance\Invoice::STATUSES)->prepend('Все', '-1'))
                    {{ Form::select('status', $status_select, 1, ['class' => 'form-control select2-all', 'id'=>'status', 'required', 'onchange' => 'loadData()']) }}
                </div>
                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label">На странице</label>
                    {{ Form::select('page_count', collect([25=>25, 50 => 50, 100=>100, 0=>'Все']), 25, ['class' => 'form-control select2-all', 'id'=>'status', 'required', 'onchange' => 'loadData(true)']) }}
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" >
                    <label class="control-label">Дата от</label>
                    {{ Form::text('date_from', '', ['class' => 'form-control datepicker', 'onchange' => 'loadData()']) }}
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <label class="control-label">Дата по</label>
                    {{ Form::text('date_to', '', ['class' => 'form-control datepicker', 'onchange' => 'loadData()']) }}
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div id="table" class="table"></div>
    </div>
</div>