@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">

@append

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">Счета</h1>
    </div>
    <div class="header_bab">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @if(sizeof($count_arr))
                <div id="tt" class="easyui-tabs" data-options="tools:'#tab-tools'">
                    @foreach($count_arr as $key => $count)
                        <div title="{{$count['title']}} {{($count['count']>0)?$count['count']:''}}" id="tab-{{$key}}" data-view="{{$count['type']}}"></div>
                    @endforeach
                </div>
            @else
                У вас нет доступных вкладок в этом разделе
            @endif
        </div>
    </div>
    <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container" style="margin-top: -5px;overflow: auto;">
        <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="filter-group" id="filters">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                            <label class="control-label">Агент</label>
                            {{ Form::select('agent_id', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id')->prepend('Все', 0), 0, ['class' => 'form-control select2', 'id'=>'agent_id', 'onchange'=>'loadData()']) }}
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <label class="control-label">Номер счета</label>
                            {{ Form::text('invoice_number', '', ['class' => 'form-control', 'onchange' => 'loadData()']) }}
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <label class="control-label">Статус</label>
                            @php($status_select = collect(\App\Models\Finance\Invoice::STATUSES)->prepend('Все', '-1'))
                            {{ Form::select('status', $status_select, 1, ['class' => 'form-control select2-all', 'id'=>'status', 'required', 'onchange' => 'loadData()']) }}
                        </div>
                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label">На странице</label>
                            {{ Form::select('page_count', collect([25=>25, 50 => 50, 100=>100, 0=>'Все']), 25, ['class' => 'form-control select2-all', 'id'=>'status', 'required', 'onchange' => 'loadData(true)']) }}
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" >
                            <label class="control-label">Дата от</label>
                            {{ Form::text('date_from', '', ['class' => 'form-control datepicker', 'onchange' => 'loadData()']) }}
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <label class="control-label">Дата по</label>
                            {{ Form::text('date_to', '', ['class' => 'form-control datepicker', 'onchange' => 'loadData()']) }}
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="table" class="table"></div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <center style="margin-top: 25px; margin-left: 48%; display: inline-block">
                <span id="view_row"></span>/<span id="max_row"></span>
            </center>
            <div id="page_list" class="easyui-pagination pull-right"></div>
        </div>
    </div>
@endsection

@section('js')

    <script>

        var PAGE = 1;

        $(function () {
            loadData();
        });

        function setPage(field) {
            PAGE = field;
            loadData();
        }

        var map;
        var TAB_INDEX = 0;

        $(function () {

            if($('[data-view]').length > 0){
                $('#tt').tabs({
                    border:false,
                    pill: false,
                    plain: true,
                    onSelect: function(title, index){
                        return selectTab(index);
                    }
                });
                selectTab(0);
            }
        });



        function selectTab(id) {

            TAB_INDEX = id;

            loaderShow();
            loadData();
            initTab();
            loaderHide();

            {{--$.post("{{url("/cashbox/invoice/get_invoices_tab")}}", getData(), function (response) {--}}

            {{--    $("#main_container").html(response);--}}
            {{--    loadData();--}}
            {{--    initTab();--}}

            {{--}).always(function() {--}}
            {{--    loaderHide();--}}
            {{--});--}}

        }

        function loadData(resetPage = false){
            if(resetPage){
                PAGE = 1
            }

            $.post("{{url("/cashbox/invoice/get_invoices_table")}}", getData(), function(response){
                $('#table').html(response.html);

                $('#view_row').html(response.view_row);
                $('#max_row').html(response.max_row);

                $('#page_list').pagination({
                    total:response.page_max,
                    pageSize:1,
                    pageNumber: PAGE,
                    layout:['first','prev','links','next','last'],
                    onSelectPage: function(pageNumber, pageSize){
                        setPage(pageNumber);
                    }
                });


                $(".clickable-row").click( function(){
                    if ($(this).attr('data-href')) {
                        window.location = $(this).attr('data-href');
                    }
                });

            }).fail(function(){
                $('#invoices_info').html('');
            }).always(function() {
                loaderHide();
            });
        }



        function getData(){
            var tab = $('#tt').tabs('getSelected');
            var load = tab.data('view');

            return {
                with_tabs: 1,
                agent_id: $('[name="agent_id"]').val(),
                type: load,
                status: $('[name="status"]').val(),
                date_from: $('[name="date_from"]').val(),
                date_to: $('[name="date_to"]').val(),

                page_count: $('[name="page_count"]').val(),
                PAGE: PAGE,
                invoice_number: $('[name="invoice_number"]').val(),
            }
        }

        function initTab() {
            //startMainFunctions();

        }


    </script>


@endsection