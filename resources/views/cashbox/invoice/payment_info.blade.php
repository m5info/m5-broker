<div class="block-inner">

    <div id="table_vue" class="scrollbar-inner">
        {{--Обертка для клона шапки--}}
        <div v-bind:style="{ top: head_offset + 'px', width: max_w + 'px'}" class='cloned_head_wrapper'>
            <table v-bind:style="{width: max_w + 'px'}" class='table  table-bordered bso_items_table'></table>
        </div>

        {{--Обычная таблица как везде, вставляем bind на высоту--}}

        <div id="table" style="overflow-y: visible; }}"
                {{--v-bind:style="{ 'max-height' : table_height + 'px'}"--}}>
            <table class="table  table-bordered bso_items_table">
                <thead>
                <tr>
                    <th>{{ Form::checkbox('all_payments') }}</th>
                    <th>№</th>
                    <th>Индекс</th>
                    <th>Организация</th>
                    <th>СК</th>
                    <th>Страхователь</th>
                    <th>Продукт</th>
                    <th>Тип платежа</th>
                    <th>Поток оплаты</th>
                    <th>№ договора</th>
                    <th>Квитанция</th>
                    <th>Сумма</th>
                    <th>Реальная сумма в кассу</th>
                    <th>Оф.скидка</th>
                    <th>Неоф.скидка</th>
                    <th>Размер скидки</th>
                    <th>КВ агента, %</th>
                    <th>КВ агента, руб</th>
                    <th>К оплате</th>
                    <th>Комментарий</th>
                    @if($invoice->status_id != 2)
                        <th></th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @php
                    $total = 0;
                    $total_kv_agent = 0;
                    $total_sum = 0;
                @endphp
                @if(sizeof($invoice->payments))
                    @foreach($invoice->payments as $key => $payment)
                        @php
                            if((int)$payment->payment_flow == 0){
                                $invoice_payment_total = $payment->invoice_payment_total * ((100 - $payment->informal_discount)/100);
                                $real_sum = ($payment->invoice_payment_total!=0.00)? ($invoice_payment_total):0;
                            }else{
                                $real_sum = ($payment->invoice_payment_total!=0.00)?($payment->invoice_payment_total):0;
                            }
                        @endphp
                        @php($total += $payment->payment_total)
                        @php($total_kv_agent += $payment->financial_policy_kv_agent_total)
                        @php($total_sum += $payment->getPaymentAgentSum())
{{--                        @php($total_sum += $real_sum)--}}
                        <tr {!! $payment->is_deleted == 1 ? 'title=Удален' : '' !!} {!! $payment->is_deleted == 1 ? 'style="background-color: #ffcccc;"' : "" !!}
                                {!! $payment->bso && $payment->bso->location_id == 4 ? 'style="background-color: #96abff61;"' : ""!!} {!! $payment->bso && $payment->bso->location_id == 4 ? 'title="Принят от агента"' : '' !!}>

                            <td>
                                {{ Form::checkbox('payment[]', $payment->id) }}
                                <input type="hidden" name="bso[]" value="{{$payment->bso_id}}"/>
                            </td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $payment->id }}</td>
                            <td>{{ $payment->bso && $payment->bso->supplier_org ? $payment->bso->supplier_org->title : "" }}</td>
                            <td>{{ $payment->bso && $payment->bso->insurance ? $payment->bso->insurance->title : "" }}</td>
                            <td>{{ $payment->Insurer }}</td>
                            <td>{{ $payment->bso && $payment->bso->product ? $payment->bso->product->title : ""}}</td>
                            <td>{{ \App\Models\Contracts\Payments::PAYMENT_TYPE[$payment->payment_type] }}</td>
                            <td>{{ \App\Models\Contracts\Payments::PAYMENT_FLOW[$payment->payment_flow] }}</td>
                            <td data-title="№ Договора" {!! $payment->reports_order_id > 0 && $payment->reports_border && $payment->reports_border->accept_status == 6 ? "style='background: #acdcac !important;'"."title=".\App\Models\Reports\ReportOrders::STATE[$payment->reports_border->accept_status] : '' !!}><nobr>
                                <a onclick="openFancyBoxFrame('{{url("/payment/".$payment->id)}}')" href="javascript:void(0)">
                                    {{ $payment->bso ? $payment->bso->bso_title : "" }}
                                </a></nobr>
                            </td>
                            <td><nobr>{{ $payment->bso_receipt ? : "" }}</nobr></td>
                            <td><nobr>{{ getPriceFormat($payment->payment_total) }}</nobr></td>
                            <td class="nowrap">{{ getPriceFormat($real_sum) }}</td>
                            <td class="nowrap">{{ getPriceFormat($payment->official_discount) }}</td>
                            <td class="nowrap">{{ getPriceFormat($payment->informal_discount) }}</td>
                            <td class="nowrap">{{ getPriceFormat($payment->informal_discount_total) }}</td>
                            <td class="nowrap">{{ $payment->financial_policy_kv_agent }}</td>
                            <td><nobr>{{ getPriceFormat($payment->financial_policy_kv_agent_total) }}</nobr></td>
                            <td><nobr>{{ getPriceFormat($payment->getPaymentAgentSum()) }}</nobr></td>
{{--                            <td><nobr>{{ getPriceFormat($real_sum) }}</nobr></td>--}}
                            <td>{{ App\Models\Contracts\Contracts::query()->where('id','=', $payment->contract_id)->select('comment')->get()->first()->comment }}</td>
                            @if($invoice->status_id != 2)
                                <td>
                                    <span class="btn-sm btn-danger" onclick="deleteOneFromInvoice({{ $payment->id }})">Исключить</span>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan='10'>&nbsp</td>
                        <td><strong class="itogo">ИТОГО:</strong></td>
                        <td class="nowrap"><strong>{{ getPriceFormat($total) }}</strong></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td class="nowrap"><strong>{{ getPriceFormat($total_kv_agent) }}</strong></td>
                        <td class="nowrap"><strong>{{ getPriceFormat($total_sum) }}</strong></td>
                        <td></td>
                    </tr>

                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

<style>
    .scrollbar-inner > .scroll-element.scroll-x {
        bottom: 2px;
        left: 280px;
        width: 60%;
        position: fixed;
    }
</style>


