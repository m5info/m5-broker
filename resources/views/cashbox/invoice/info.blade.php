@if(sizeof($invoices))
    <div class="div_tooltip"></div>
    <table class="tov-table table table-striped table-bordered">
        <thead>
        <tr>
            <th>Номер</th>
            <th>Юр. лицо</th>
            <th>Тип счёта</th>
            <th>Агент</th>
            <th>Дата создания</th>
            <th>Статус</th>
            <th>Кол-во опл.\неопл.</th>
            <th>Общая сумма</th>
            <th>Комментарии</th>
        </tr>
        </thead>
        @foreach($invoices as $invoice)
            @php
                $background = '';
                $title = '';
                $payments = '';
                $paid = 0;
                $comments = '';

                foreach($invoice->payments as $payment){
                    if ((int)$payment->reports_order_id > 0 && $payment->reports_border && $payment->reports_border->accept_status == 6){
                        $background = '#acdcac;';
                        $title = "Частично оплачен";
                        $paid++;
                    }

                    $comment = App\Models\Contracts\Contracts::query()->where('id','=', $payment->contract_id)->select('comment')->get()->first()->comment;
                    if (!empty($comment)){
                    $comments.=$comment.PHP_EOL;
                    }
                }
            @endphp
            <tr class="clickable-row invoice_row" style="background: {{$background}}" title="{{$title}}" id="{{$invoice->id}}" data-href="{{url("/cashbox/invoice/{$invoice->id}/edit")}}">
                <td><div class="glyphicon glyphicon-new-window"></div>
                    <div id="bso_tooltip_{{ $invoice->id }}" class="bso_tooltip" style="">
                        @php
                            foreach($invoice->payments as $payment){

                                $bso_title = $payment->bso ? $payment->bso->bso_title : "";
                                $bso_product = $payment->bso && $payment->bso->product ? $payment->bso->product->title : "";

                                if(($payment->contract->kind_acceptance) == 1){
                                    echo "<a href=".url('bso/items', $payment->bso->id)." target='_blank'>$bso_title</a> $bso_product<br>";
                                }else{
                                    echo "<span title='Не безусловный'>$bso_title</span> $bso_product<br>";
                                }
                           }
                        @endphp
                    </div>
                    {{ $invoice->id }}
                </td>
                <td>{{ $invoice->org ? $invoice->org->title : "" }}</td>
                <td>{{ $invoice->types_ru('type') }}</td>
                <td>{{ $invoice->agent ? $invoice->agent->name : "" }}</td>
                <td>{{ getDateFormatRu($invoice->created_at) }}</td>
                <td>{{ $invoice->statuses_ru('status_id')}}</td>
                <td>{{$paid}}\{{$invoice->payments->count()}}</td>
                <td>{{ getPriceFormat($invoice->payments->sum('payment_total')) }}</td>
                <td>{{ $comments }}</td>
            </tr>
        @endforeach
    </table>
@else
    {{ trans('form.empty') }}
@endif

<script>
    var mouse = {};
    $('body').on( "mousemove", function( event ) {
        mouse.x = event.pageX ;
        mouse.y = event.pageY ;
        console.log();
    });
    $('tr.invoice_row td:first-child').on("mouseover", function() {

        var id = $(this).parent('tr').attr('id');
        console.log(id);

        if($('#bso_tooltip_'+ id).html() != '')
            $('.div_tooltip').html($('#bso_tooltip_'+ id).clone());
        else
            $('.div_tooltip').html('<span>Пусто</span>');

        $('.div_tooltip').show().css('top',mouse.y - $(document).scrollTop()-5).css('left',mouse.x-5);
    }).on("mouseout", function() {

        var id =  $(this).parent('tr').attr('id');
        $('.div_tooltip').hide();
    });
</script>