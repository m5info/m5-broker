@extends('layouts.app')


@section('content')


    @include("payments.invoice.head", ["invoice" => $invoice])

    {{ Form::model($invoice, ['url' => url("/cashbox/invoice/{$invoice->id}/save"), 'id' => 'formInvoice', 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) }}

    @include("payments.invoice.body", ["invoice" => $invoice, 'need_withdraw' => $need_withdraw])

    {{Form::close()}}

    @include("cashbox.invoice.payment_info", ["invoice" => $invoice])




@endsection



@section('js')
    <script>

        $(function () {

            initInvoce();

            $(document).on('click', '#delete_invoice', function () {
                if (confirm('Расформировать счёт?')) {
                    $.post('/finance/invoice/invoices/{{$invoice->id}}/delete_invoice', {}, function (res) {
                        location.href = '/cashbox/invoice'
                    });
                }
            });


            $(document).on('change', '[name="payment[]"]', function () {
                var uncheckeds = $('[name="payment[]"]').length - $('[name="payment[]"]:checked').length;
                $('[name="all_payments"]').prop('checked', uncheckeds === 0);
                showActions();
            });

            $(document).on('change', '[name="all_payments"]', function () {
                var checked = $(this).prop('checked');
                $('[name="payment[]"]').prop('checked', checked);
                showActions();
            });

            $(document).on('click', '#remove_from_invoice', function () {


                if (confirm('Исключить платёж из счёта?')) {
                    $.post('/finance/invoice/invoices/{{$invoice->id}}/delete_payments/', getActionData(), function (res) {
                        if (res.type === 'invoice') {
                            location.href = '/cashbox/invoice'
                        } else if (res.type === 'payment') {
                            location.reload();
                        }
                    });
                }


            });

        });

        function create_act() {

            var payment_array = [];

            $('input[name="payment[]"]').each(function () {
                payment_array.push($(this).val());
            });

            var data = {
                agent_id: '{{$invoice->agent_id}}',
                payment_array: JSON.stringify(payment_array),
            };

            loaderShow();
            $.post("{{url("/cashbox/acts_to_underwriting/contract/create_get_acts/")}}", data, function (response) {
                loaderHide();
                // show_actions();
                // loadContent();
                location.href = '{{url("/finance/invoice/invoices/$invoice->id/edit")}}';
            }).always(function() {
                loaderHide();
            });

        }


        function deleteOneFromInvoice(payment_id){
            var data = {
                payment: []
            };

            data.payment.push(payment_id);

            if (confirm('Исключить платёж из счёта?')) {
                $.post('/finance/invoice/invoices/{{$invoice->id}}/delete_payments/', data, function (res) {
                    if (res.type === 'invoice') {
                        location.href = '/cashbox/invoice'
                    } else if (res.type === 'payment') {
                        location.reload();
                    }
                });
            }

        }

        function getActionData() {
            var data = {
                payment: []
            };

            $.each($('[name="payment[]"]:checked'), function (k, v) {
                data.payment.push($(v).val())
            });
            return data;

        }


        function showActions() {
            if ($('[name="payment[]"]:checked').length > 0) {
                $('#action_table').show();
            } else {
                $('#action_table').hide();
            }
        }

        function saveInvoiceTypePayment() {

            loaderShow();
            $.post("{{url("/cashbox/invoice/{$invoice->id}/save-invoice-type-payment")}}", {type_invoice_payment:$("#type_invoice_payment").val(), invoice_payment_com:$("#invoice_payment_comm").val()}, function (response) {
                loaderHide();

                reload();

            }).always(function() {
                loaderHide();
            });


        }


    </script>

@endsection


