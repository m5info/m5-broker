@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">

@append

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">Счета</h1>
    </div>
    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="filter-group" id="filters">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                        <label class="control-label">Агент</label>
                        {{ Form::select('agent_id', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id')->prepend('Все', 0), 0, ['class' => 'form-control select2', 'id'=>'agent_id', 'onchange'=>'loadContent()']) }}
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <label class="control-label">Тип</label>
                        @php($type_select = collect(['cash'=>'Наличные', 'cashless'=>'Безнал Брокер', 'card'=>'Безнал Карта', 'sk' =>'СК'])->prepend('Все', ''))
                        {{ Form::select('type', $type_select,  "", ['class' => 'form-control select2-all', 'id'=>'type', 'required', 'onchange' => 'loadContent()']) }}
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <label class="control-label">Статус</label>
                        @php($status_select = collect(\App\Models\Finance\Invoice::STATUSES)->prepend('Все', '-1'))
                        {{ Form::select('status', $status_select, 1, ['class' => 'form-control select2-all', 'id'=>'status', 'required', 'onchange' => 'loadContent()']) }}
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" >
                        <label class="control-label">Дата от</label>
                        {{ Form::text('date_from', '', ['class' => 'form-control datepicker', 'onchange' => 'loadContent()']) }}
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <label class="control-label">Дата по</label>
                        {{ Form::text('date_to', '', ['class' => 'form-control datepicker', 'onchange' => 'loadContent()']) }}
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <label class="control-label">Номер счета</label>
                        {{ Form::text('invoice_number', '', ['class' => 'form-control', 'onchange' => 'loadContent()']) }}
                    </div>

                </div>
            </div>
        </div>
    </div>

    @include('_chunks/_vue_table',['class'=>'pull-right','callback'=>'loadContent'])

    @include('_chunks/_pagination',['class'=>'pull-right','callback'=>'loadContent'])

@endsection

@section('js')

    <script>

        $(function () {
            loadContent();
        });

        function loadContent() {

            $('#page_list').html('');
            $('#table_row').html('');
            $('#view_row').html(0);
            $('#max_row').html(0);

            loaderShow();
            $.post("{{url("/cashbox/invoice/get_invoices_table")}}", getData(), function (response) {

                $('#table').html(response.html);

                $('#view_row').html(response.view_row);
                $('#max_row').html(response.max_row);

                ajaxPaginationUpdate(response.page_max, loadContent);

                $(".clickable-row").click( function(){
                    if ($(this).attr('data-href')) {
                        window.location = $(this).attr('data-href');
                    }
                });

            }).fail(function(){
                $('#invoices_info').html('');
            }).always(function() {
                loaderHide();
            });
        }


        function getData(){
            return {
                with_tabs: 0,
                agent_id: $('[name="agent_id"]').val(),
                type: $('[name="type"]').val(),
                status: $('[name="status"]').val(),
                date_from: $('[name="date_from"]').val(),
                date_to: $('[name="date_to"]').val(),

                page_count: $('[name="page_count"]').val(),
                PAGE: PAGE,
                invoice_number: $('[name="invoice_number"]').val(),
            }
        }

    </script>


@endsection