@php
    $category_id = isset($income_expense) ? $income_expense->category->type : 2;
@endphp

<div class="form-group">
    <label class="col-xs-2 col-sm-3 control-label">Тип</label>
    <div class="col-xs-10 col-sm-9">
        {{ Form::select('category_type', collect(\App\Models\Settings\IncomeExpenseCategory::TYPE), $category_id,  ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-xs-2 col-sm-3 control-label">Категория</label>
    <div class="col-xs-10 col-sm-9">
        {{ Form::select('category_id', [], isset($income_expense) ? $income_expense->category_id : old('category_id'), ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-xs-2 col-sm-3 control-label">Тип оплаты</label>
    <div class="col-xs-10 col-sm-9">
        {{ Form::select('payment_type', (\App\Models\Cashbox\IncomeExpense::PAYMENT_TYPE), isset($income_expense) ? $income_expense->payment_type : old('payment_type'), ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-xs-2 col-sm-3 control-label">Пользователь</label>
    <div class="col-xs-10 col-sm-9">
        {{ Form::select('payment_user', collect(\App\Models\User::all())->pluck('name', 'id')->prepend('Не выбрано', 0), isset($income_expense) ? ($income_expense->payment_user ? $income_expense->payment_user_id : 0) : old('payment_user'), ['class' => 'form-control select2']) }}
    </div>
</div>


<div class="form-group">
    <label class="col-xs-2 col-sm-3 control-label">Сумма</label>
    <div class="col-xs-10 col-sm-9">
        {{ Form::text('sum', isset($income_expense) ? titleFloatFormat($income_expense->sum) : old('sum'), ['class' => 'form-control sum']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-xs-2 col-sm-3 control-label">Дата платежа</label>
    <div class="col-xs-10 col-sm-9">
        {{ Form::text('date', isset($income_expense) ? getDateFormatRu($income_expense->date) : date("d.m.Y"), ['class' => 'form-control datepicker date']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-xs-2 col-sm-3 control-label">Комиссия %</label>
    <div class="col-xs-10 col-sm-9">
        {{ Form::text('commission', isset($income_expense) ? $income_expense->commission : old('commission'), ['class' => 'form-control sum']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-12 control-label">Комментарий</label>
    <div class="col-sm-12">
        {{ Form::textarea('comment', isset($income_expense) ? $income_expense->comment : old('comment'), ['class' => 'form-control']) }}
    </div>
</div>

@section('js')
<script>

    $(function(){

        load_categories();

        $(document).on('change', '[name="category_type"]', function(){
            load_categories();
        })

    });


    function load_categories(){
        loaderShow();
        var data = {category_type: $('[name="category_type"]').val(),};
        $.post('/cashbox/incomes_expenses/get_categories', data, function(res){
            var select = $('[name="category_id"]');
            var options = '';

            $.each(res, function(k,v){
                options += '<option value="'+v.id+'" '
                    + ((parseInt(v.id) === parseInt('{{isset($income_expense) ? $income_expense->category_id : 0}}')) ? 'selected' : '')
                    +'>'+v.title+'</option>';
            });

            select.html(options);
        }).always(function(){
            loaderHide();
        });

    }
</script>
@endsection