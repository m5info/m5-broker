@extends('layouts.app')

@section('content')
<div class="col-md margin20 animated fadeInRight">
    <input type="hidden" name="overdue" value="0" onchange="loadItems()">
    <h2 class="inline-h1">Расходы/Доп доходы</h2>
    <a class="btn btn-success btn-right" href="/cashbox/incomes_expenses/create/">Создать</a>

    <br><br>
    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row form-group">
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <label class="control-label">Категория</label>

                <div class="pull-right">
                    <label class="control-label">Учредительские</label>
                    <input type="checkbox" name="is_foundation" id="is_foundation" value="1" onclick="loadItems()"/>
                </div>

                {{ Form::select('category_id', collect(\App\Models\Settings\IncomeExpenseCategory::all())->pluck('title', 'id'), request('category_id') ? request()->query('category_id') : 0, ['class' => 'form-control select2-all', "multiple"=>"multiple", 'id'=>'category_id', 'onchange' => 'loadItems()']) }}
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <label class="control-label">Тип</label>
                {{ Form::select('type_id', collect(\App\Models\Settings\IncomeExpenseCategory::TYPE)->prepend('Любой', -1), request('type_id') ? request()->query('type_id') : -1, ['class' => 'form-control select2-ws', 'onchange' => 'loadItems()']) }}
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <label class="control-label">Статус</label>
                {{ Form::select('status_id', collect(\App\Models\Cashbox\IncomeExpense::STATUS)->prepend('Любой', -1), request('status_id') ? request()->query('status_id') : -1, ['class' => 'form-control select2-ws', 'onchange' => 'loadItems()']) }}
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <label class="control-label">Тип оплаты</label>
                {{ Form::select('payment_id', collect(\App\Models\Cashbox\IncomeExpense::PAYMENT_TYPE)->prepend('Любой', -1), request('payment_id') ? request()->query('payment_id') : -1, ['class' => 'form-control select2-ws', 'onchange' => 'loadItems()']) }}
            </div>
        </div>

        <div class="row form-group">
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <label class="control-label">Пользователь</label>
                {{ Form::select('payment_user', collect(\App\Models\User::all())->pluck('name', 'id')->prepend('Не выбрано', 0), request('payment_user') ? request()->query('payment_user') : -1, ['class' => 'form-control select2', 'onchange' => 'loadItems()']) }}
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <label class="control-label">Дата оплаты с</label>
                <input class="form-control datepicker date" placeholder="Дата с" onchange="loadItems()" id="date_from" name="date_from" type="text" value="{{Carbon\Carbon::now()->format('d.m.Y')}}">
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <label class="control-label">Дата оплаты по</label>
                <input class="form-control datepicker date" placeholder="Дата по" onchange="loadItems()" id="date_to" name="date_to" type="text" value="">
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <label class="control-label">Разделить приход\расход</label>
                {{ Form::select('divide', collect([0 => 'Нет', 1=>'Да']), 0, ['class' => 'form-control select2-ws', 'onchange' => 'loadItems()']) }}
            </div>
        </div>






    <div id="table" class="table-responsive"></div>

    @include('_chunks/_pagination',['callback'=>'loadItems'])
</div>
</div>

@endsection

<style>
    @media screen and (max-width: 1315px) {
        td .btn-sm {
            margin-top: 4px;
            display: block;
            text-align: center;
            padding: 2px 7px;
        }
    }

    .no-label-margin {
        margin-top: 19px;
    }

</style>

@section('js')

<script>

    $(function () {

        $('#category_id').select2({
            placeholder: 'Категория'
        });

        loadItems();
    });

    $('[name = "divide"]').change(function(){
       if ($(this).val() == 0){
           $('[name = "type_id"]').removeAttr('disabled');
       }else if($(this).val() == 1){
           $('[name = "type_id"]').attr('disabled', 'disabled');
       }
    });

    function getData() {
        return {
            category_id: $('[name="category_id"]').val(),
            page_count: $('[name="page_count"]').val(),
            type_id: $('[name="type_id"]').val(),
            status_id: $('[name="status_id"]').val(),
            payment_id: $('[name="payment_id"]').val(),
            payment_user: $('[name="payment_user"]').val(),
            date_from: $('[name="date_from"]').val(),
            date_to: $('[name="date_to"]').val(),
            divide: $('[name="divide"]').val(),
            is_foundation: $('[name="is_foundation"]').prop('checked')?1:0,

            PAGE: PAGE
        };
    }

    function loadItems() {
        loaderShow();

        var data = getData();
        $('#page_list').html('');
        $('#table_row').html('');
        $('#view_row').html(0);
        $('#max_row').html(0);
        $.post("{{url('/cashbox/incomes_expenses/get_incomes_expenses_table')}}", data, function (response) {

            $('#table').html(response.html);
            $('#view_row').html(response.view_row);
            $('#max_row').html(response.max_row);

            ajaxPaginationUpdate(response.page_max,loadItems);

            $(document).on('click', '.clickable-row', function () {
                var link = $(this).data('href');
                if (link) {
                    location.href = link;
                }
            })
        }).always(function () {
            loaderHide();
        });
    }

    $(".datepicker").datepicker({maxDate: '0'});



</script>
@endsection

