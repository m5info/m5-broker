@extends('layouts.app')


@section('content')
    <div class="row col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="page-subheading">
            <h1 class="inline-h1">Добавление расхода/доп дохода</h1>
            <a class="btn btn-primary btn-right" href="/cashbox/incomes_expenses">Назад</a>
        </div>

        <div class="block-main">
            <div class="block-sub">
                <div class="form-horizontal">

                    {{ Form::open(['method' => 'post', 'url' => '/cashbox/incomes_expenses/store', 'id' => 'form_incomes_expenses']) }}

                        @include('cashbox.incomes_expenses.form')

                    {{ Form::close() }}

                </div>

                <button type="submit" form="form_incomes_expenses" class="btn btn-success pull-right">Сохранить</button><br><br><br>

            </div>
        </div>
    </div>

@endsection

