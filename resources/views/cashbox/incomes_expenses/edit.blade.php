@extends('layouts.app')


@section('content')
    <div class="row col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="page-subheading">
            <h1 class="inline-h1">Редактирование расхода/доп дохода</h1>
            <a class="btn btn-primary btn-right" href="/cashbox/incomes_expenses">Назад</a>
            <a href="/cashbox/incomes_expenses/{{$income_expense->id}}/export?type={{$income_expense->category ? $income_expense->category->type : '2'}}"
               class="btn btn-success btn-right doc_export_btn">Печать</a>

        </div>

        <div class="block-main">
            <div class="block-sub">
                <div class="form-horizontal">

                    {{ Form::open(['method' => 'post', 'url' => "/cashbox/incomes_expenses/{$income_expense->id}/save", 'id' => 'form_incomes_expenses']) }}

                    @include('cashbox.incomes_expenses.form')

                    {{ Form::close() }}

                </div>


                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        @if($income_expense->status_id == 1)

                            <span class="btn btn-primary pull-left"
                                  onclick="setPaymentIncomesExpenses();">Оплатить</span>

                        @endif
                        @if($income_expense->status_id == 1 || auth()->user()->hasPermission('cashbox','edit_paid_incomes_expenses'))

                            <button type="submit" form="form_incomes_expenses" class="btn btn-success btn-right">
                                Сохранить
                            </button><br><br><br>

                        @endif
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="row col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin:5px;">

        <div class="page-subheading">
            <center><h1 class="inline-h1">Документы</h1></center>
        </div>

        @foreach($income_expense->documents as $file)

            <div class="col-lg-3">
                <div class="upload-dot">
                    <div class="block-image">
                        @if (in_array($file->ext, ['jpg', 'jpeg', 'png', 'gif']))
                            <a href="{{ url($file->url) }}" target="_blank">
                                <img class="media-object preview-image" src="{{ url($file->preview) }}"
                                     onerror="this.onerror=null;this.src='/images/extensions/unknown.png';">
                            </a>
                        @else
                            <a href="{{ url($file->url) }}" target="_blank">
                                <img class="media-object preview-icon" src="/images/extensions/{{$file->ext}}.png">
                            </a>
                        @endif
                        <div class="upload-close">
                            <div class="" style="float:right;color:red;">
                                <a href="javascript:void(0);" onclick="removeIncomesDocument('{{ $file->name }}')">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">
            {!! Form::open(['url'=>"/cashbox/incomes_expenses/{$income_expense->id}/add_files",'method' => 'post', 'class' => 'dropzone', 'id' => 'addManyDocForm']) !!}
            <div class="dz-message" data-dz-message>
                <p>Перетащите сюда файлы</p>
                <p class="dz-link">или выберите с диска</p>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
    <style>

        @media screen and (max-width: 1540px) {
            .page-subheading a {
                margin-bottom: 20px;
            }
        }

    </style>
@endsection


<script>

    document.addEventListener('DOMContentLoaded', function(){
        doc = document.querySelector('[name = "category_type"]');
        doc.addEventListener('change', function(){
            print_ = document.querySelector('.doc_export_btn');
            print_.setAttribute('href','/cashbox/incomes_expenses/{{$income_expense->id}}/export?type='+this.value);
        });
    });

    function removeIncomesDocument(fileName) {
        newCustomConfirm(function (result) {
            if (result){
                var filesUrl = '{{url("/cashbox/incomes_expenses/{$income_expense->id}/delete_files")}}';
                var fileUrl = filesUrl + '/' + fileName;
                $.post(fileUrl, {
                    _method: 'DELETE'
                }, function () {
                    reload();
                });
            }
        });
    }


    function setPaymentIncomesExpenses() {
        $.post('{{url("/cashbox/incomes_expenses/{$income_expense->id}/accept")}}', {}, function (res) {

            if (parseInt(res.state) == 1) {
                reload();
            } else {
                flashMessage('danger', res.msg)
            }

        });
    }


</script>


