<div class="row">
    <div class="col-lg-6" style="overflow-x: auto;">
        <label class="control-label">Приход</label>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Категория</th>
                <th>Статус</th>
                <th>Оператор</th>
                <th>Пользователь</th>
                <th>Дата платежа</th>
                <th>Тип оплаты</th>
                <th>Сумма</th>
                <th>Комиссия</th>
                <th>Комментарий</th>
                <th>Итого</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @if(sizeof($incomes_expenses_getting))
                @php
                    $minus_sum = 0;
                    $minus_total = 0;
                @endphp
                @foreach($incomes_expenses_getting as $income_expense)
                    <tr @if($income_expense->status_id == 1) class="bg-yellow"
                        @elseif($income_expense->status_id == 2) class="bg-red"
                        @elseif($income_expense->payment_type == 0) class="bg-green" @endif>
                        <td>{{ $income_expense->category ? $income_expense->category->title : "" }}</td>
                        <td>{{ $income_expense->status_ru() }}</td>
                        <td>{{ $income_expense->created_user ? $income_expense->created_user->name : "" }}</td>
                        <td>{{ $income_expense->payment_user ? $income_expense->payment_user->name : "" }}</td>
                        <td>{{ setDateTimeFormatRu($income_expense->date, 1) }}</td>
                        <td>{{ $income_expense->payment_type_ru('payment_type') }}</td>

                        <td class="nowrap">{{ getPriceFormat($income_expense->category->type == 2 ? $income_expense->sum * -1 : $income_expense->sum) }}</td>
                        <td>{{ getPriceFormat($income_expense->commission_total) }}</td>
                        <td>{{ $income_expense->comment }}</td>
                        <td class="nowrap">{{ getPriceFormat($income_expense->category->type == 2 ? $income_expense->total * -1 : $income_expense->total) }}</td>
                        <td>
                            {{--<a href="/cashbox/incomes_expenses/{{$income_expense->id}}/edit" class="btn-sm btn-danger btn-right">Удалить</a>--}}
                            <div>
                                <a href="/cashbox/incomes_expenses/{{$income_expense->id}}/edit"
                                   class="btn-sm btn-primary">Открыть</a>
                            </div>
                            <div style="margin-top: 10px;">
                                <a href="/cashbox/incomes_expenses/{{$income_expense->id}}/export?type={{$income_expense->category ? $income_expense->category->type : ''}}"
                                   class="btn-sm btn-success doc_export_btn">Печать</a>
                            </div>
                        </td>
                    </tr>
                    @php
                        if($income_expense->category->type == 2){
                            $minus_sum = $minus_sum + $income_expense->sum;
                            $minus_total = $minus_total + $income_expense->total;
                        }
                    @endphp
                @endforeach
                <tr>
                    <th colspan="6"></th>
                    <th>{{titleFloatFormat($incomes_expenses_getting->sum('sum') - $minus_sum - $minus_sum)}}</th>
                    <th>{{titleFloatFormat($incomes_expenses_getting->sum('commission_total'))}}</th>
                    <th></th>
                    <th>{{titleFloatFormat($incomes_expenses_getting->sum('total') - $minus_total - $minus_total)}}</th>
                    <th colspan="3"></th>
                </tr>
            @else
                <tr>
                    <td colspan="11" class="text-center">Нет расходов/доп доходов</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
    <div class="col-lg-6" style="overflow-x: auto;">
        <label class="control-label">Расход</label>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Категория</th>
                <th>Статус</th>
                <th>Оператор</th>
                <th>Пользователь</th>
                <th>Дата платежа</th>
                <th>Тип оплаты</th>
                <th>Сумма</th>
                <th>Комиссия</th>
                <th>Комментарий</th>
                <th>Итого</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @if(sizeof($incomes_expenses_spending))
                @php
                    $minus_sum = 0;
                    $minus_total = 0;
                @endphp
                @foreach($incomes_expenses_spending as $income_expense)
                    <tr @if($income_expense->status_id == 1) class="bg-yellow"
                        @elseif($income_expense->status_id == 2) class="bg-red"
                        @elseif($income_expense->payment_type == 0) class="bg-green" @endif>
                        <td>{{ $income_expense->category ? $income_expense->category->title : "" }}</td>
                        <td>{{ $income_expense->status_ru() }}</td>
                        <td>{{ $income_expense->created_user ? $income_expense->created_user->name : "" }}</td>
                        <td>{{ $income_expense->payment_user ? $income_expense->payment_user->name : "" }}</td>
                        <td>{{ setDateTimeFormatRu($income_expense->date, 1) }}</td>
                        <td>{{ $income_expense->payment_type_ru('payment_type') }}</td>

                        <td class="nowrap">{{ getPriceFormat($income_expense->category->type == 2 ? $income_expense->sum * -1 : $income_expense->sum) }}</td>
                        <td>{{ getPriceFormat($income_expense->commission_total) }}</td>
                        <td>{{ $income_expense->comment }}</td>
                        <td class="nowrap">{{ getPriceFormat($income_expense->category->type == 2 ? $income_expense->total * -1 : $income_expense->total) }}</td>
                        <td>
                            {{--<a href="/cashbox/incomes_expenses/{{$income_expense->id}}/edit" class="btn-sm btn-danger btn-right">Удалить</a>--}}
                            <div>
                                <a href="/cashbox/incomes_expenses/{{$income_expense->id}}/edit"
                                   class="btn-sm btn-primary">Открыть</a>
                            </div>
                            <div style="margin-top: 10px;">
                                <a href="/cashbox/incomes_expenses/{{$income_expense->id}}/export?type={{$income_expense->category ? $income_expense->category->type : ''}}"
                                   class="btn-sm btn-success doc_export_btn">Печать</a>
                            </div>
                        </td>
                    </tr>
                    @php
                        if($income_expense->category->type == 2){
                            $minus_sum = $minus_sum + $income_expense->sum;
                            $minus_total = $minus_total + $income_expense->total;
                        }
                    @endphp
                @endforeach
                <tr>
                    <th colspan="6"></th>
                    <th>{{titleFloatFormat($incomes_expenses_spending->sum('sum') - $minus_sum - $minus_sum)}}</th>
                    <th>{{titleFloatFormat($incomes_expenses_spending->sum('commission_total'))}}</th>
                    <th></th>
                    <th>{{titleFloatFormat($incomes_expenses_spending->sum('total') - $minus_total - $minus_total)}}</th>
                    <th colspan="3"></th>
                </tr>
            @else
                <tr>
                    <td colspan="11" class="text-center">Нет расходов/доп доходов</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>
