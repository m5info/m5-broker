@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">

@append

@section('content')


    <div class="page-heading">
        <h2>Балансы</h2>
    </div>

    <div class="divider"></div>

    <br/>

    <div class="filter-group row">
        <div class="btn-group col-xs-12 col-sm-12 col-md-4 col-lg-3">
            {{ Form::select('agent_id', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id'), $agent->id, ['class' => 'form-control select2', 'id'=>'agent_id', 'onchange'=>'loadBalance()']) }}
        </div>

        @if($agent)
        <div class="btn-group col-xs-7 col-sm-7 col-md-3 col-lg-4">
            <div id="view_balance" role="group" aria-label="">

            </div>
        </div>

        <div class="col-sm-4 col-md-4 col-lg-4 btn-right">
            <div class="col-sm-6 col-md-7 col-lg-6">
                <span class="btn btn-primary btn-right" onclick="transferBalance()">Перевод</span>
            </div>
            <div class="col-sm-6 col-md-5 col-lg-4">
                <span class="btn btn-success btn-right" onclick="editBalance()">Действиие</span>
            </div>
        </div>


        @endif

    </div>

    <div class="divider"></div>

    <div class="filter-group row">

        <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>



        <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h4 id="debts"></h4>
        </div>

    </div>

    <div class="divider"></div>


    <div class="filter-group row">

        <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>

        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
            {{ Form::select('type_id', collect(\App\Models\Users\UsersBalance::TYPE_ID)->prepend('Тип все', -1), -1, ['class' => 'form-control', 'id'=>'type_id', 'onchange'=>'loadContent()']) }}
        </div>

        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
            {{ Form::select('event_type_id', collect(\App\Models\Users\UsersBalance::EVENT_TYPE_ID)->prepend('Событие все', -1), -1, ['class' => 'form-control', 'id'=>'event_type_id', 'onchange'=>'loadContent()']) }}
        </div>

        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-2">
            <input type="text" placeholder="Дата начала" class="form-control date short-date datepicker date"  name="begin_date" id="begin_date" onchange="loadContent()" onblur="loadContent()"
                   value="{{date("d.m.Y")}}"/>
        </div>
        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-2">
            <input type="text" placeholder="Дата окончания" class="form-control date short-date datepicker date" name="end_date" id="end_date" onchange="loadContent()" onblur="loadContent()"
                   value=""/>
        </div>
        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-1">
             <span class="btn btn-primary" onclick="setLastMonth()">Последний месяц</span>
        </div>
    </div>


    <div class="divider"></div>
    <br/>
    <div id="balanse_info">

    </div>



@endsection

@section('js')

    <script>

        var balanceId = '{{$balance_id}}';

        $(function () {

           loadBalance();




        });

        function loadBalance(){
            //$("#balanse_info").html("");
            $.post("{{url("/cashbox/user_balance/get_balance/")}}", {agent_id: $('#agent_id').val(), }, function (response) {

                $("#view_balance").html(response.select_balance);
                $("#debts").html(response.debts);

                if($('[onclick="setTypeId(this)"][class*="active"]').length === 0){
                    $($('[onclick="setTypeId(this)"]')[0]).addClass('active');
                }
                loadContent();
            });
        }



        function editBalance(){
            openFancyBoxFrame('/cashbox/user_balance/balance/'+$('#balance_id').val()+'/edit/');
        }

        function transferBalance() {
            openFancyBoxFrame('/cashbox/user_balance/balance/'+$('#balance_id').val()+'/transfer/');
        }

        function setTypeId() {

            loadContent();
        }

        function loadContent() {

            loaderShow();

           $.post("{{url("/cashbox/user_balance/get_data_balance/")}}",
                {
                    agent_id: $('#agent_id').val(),
                    balance_id: $("#balance_id").val(),
                    type_id: $('#type_id').val(),
                    event_type_id: $('#event_type_id').val(),
                    begin_date: $('#begin_date').val(),
                    end_date: $('#end_date').val()
                }, function (response) {


                $("#balanse_info").html(response);




            }).done(function(response) {

            }).fail(function() {

            }).always(function() {

            });
            loaderHide();
        }

    </script>


@endsection