@foreach($user_balances as $balance)
    <button type="button" class="btn btn-primary type-button @if($balance_id == $agent->getBalance($balance->id)->balance_id) active @endif" onclick="setTypeId(this)" data-type-id="{{$agent->getBalance($balance->id)->id}}">
        {{$balance->title}} {{titleFloatFormat($agent->getBalance($balance->id)->balance)}}
    </button>
@endforeach