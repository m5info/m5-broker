@extends('layouts.frame')


@section('title')

    {{$balance->agent->name}} - {{$balance->type_balanse->title}} {{titleFloatFormat($balance->balance)}}

@endsection

@section('content')


    {{ Form::open(['url' => url("/cashbox/user_balance/balance/{$balance->id}/edit"), 'method' => 'post', 'class' => 'form-horizontal']) }}

        <div class="form-group">
            <div class="col-sm-4">
                <label class="col-sm-12 control-label">Дата</label>
                <div class="col-sm-12">
                    {{ Form::text('create_date', date("d.m.Y"), ['class' => 'form-control date short-date datepicker', 'required']) }}
                </div>
            </div>
            <div class="col-sm-2">
                <label class="col-sm-12 control-label">Время</label>
                <div class="col-sm-12">
                    {{ Form::text('create_date_time', date("H:i"), ['class' => 'form-control time', 'required']) }}
                </div>
            </div>
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Тип</label>
                <div class="col-sm-12">
                    {{ Form::select('type_id', collect(\App\Models\Users\UsersBalance::TYPE_ID), 1, ['class' => 'form-control', 'id'=>'type_id']) }}
                </div>
            </div>
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Событие</label>
                <div class="col-sm-12">
                    {{ Form::select('event_type_id', collect(\App\Models\Users\UsersBalance::EVENT_TYPE_ID), 0, ['class' => 'form-control', 'id'=>'event_type_id']) }}
                </div>
            </div>
        </div>

    <div class="form-group">
        <div class="col-sm-12">
            <label class="col-sm-12 control-label">Сумма</label>
            <div class="col-sm-12">
                {{ Form::text('total_sum', '', ['class' => 'form-control sum', 'required']) }}
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <label class="col-sm-12 control-label">БСО</label>
            <div class="col-sm-12">
                {{ Form::text('bso_suggest', '', ['class' => 'form-control', 'required']) }}
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <label class="col-sm-12 control-label">Назначение платежа</label>
            <div class="col-sm-12">
                {{ Form::textarea('purpose_payment', '', ['class' => 'form-control ', 'required']) }}
            </div>
        </div>
    </div>


    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection



@section('js')

    <script>

        $(function () {
            $('[name="bso_suggest"]').suggestions({
                serviceUrl: "/bso/actions/get_bso/",
                type: "PARTY",
                params:{type_bso:-1, bso_supplier_id:-1, bso_agent_id:-1},
                count: 5,
                minChars: 3,
                formatResult: function (e, t, n, i) {
                    var s = this;
                    var title = n.value;
                    var bso_type = n.data.bso_type;
                    var bso_sk = n.data.bso_sk;
                    var agent_name = n.data.agent_name;

                    var view_res = title;
                    view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">СК</span>' + bso_sk + "</div>";
                    view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Тип</span>' + bso_type + "</div>";
                    view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Агент</span>' + agent_name + "</div>";

                    return view_res;
                },
                onSelect: function (suggestion) {
                    var tarea = $('[name="purpose_payment"]');
                    tarea.html((tarea.html().length > 0 ? tarea.html() + ", " : "") + suggestion.value);
                    $('[name="bso_suggest"]').val('');
                }
            });


        });


    </script>


@endsection