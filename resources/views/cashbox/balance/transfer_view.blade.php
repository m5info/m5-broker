@extends('layouts.frame')


@section('title')

    {{$balance->agent->name}} - {{$balance->type_balanse->title}} {{titleFloatFormat($balance->balance)}}

@endsection

@section('content')


    {{ Form::open(['url' => url("/cashbox/user_balance/balance/{$balance->id}/transfer"), 'method' => 'post', 'class' => 'form-horizontal']) }}


    <div class="form-group">
        <div class="col-sm-12">
            <label class="col-sm-12 control-label">Получатель</label>
            <div class="col-sm-12">
                <div class="col-sm-8">
                    {{ Form::select('agent_id', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id'), $agent->id, ['class' => 'form-control select2', 'id'=>'agent_id', 'onchange'=>'loadBalance()']) }}
                </div>
                <div class="col-sm-4" id="view_balance" role="group" aria-label="">
                </div>
            </div>

        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <label class="col-sm-12 control-label">Сумма</label>
            <div class="col-sm-12">
                {{ Form::text('total_sum', '', ['class' => 'form-control sum', 'required']) }}
            </div>
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-12">
            <label class="col-sm-12 control-label">Назначение платежа</label>
            <div class="col-sm-12">
                {{ Form::textarea('purpose_payment', '', ['class' => 'form-control ', 'required']) }}
            </div>
        </div>
    </div>


    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection



@section('js')

    <script>

        var balanceId = '{{$balance->id}}';

        $(function () {

            loadBalance();


        });


        function loadBalance(){
            //$("#balanse_info").html("");
            $.post("{{url("/cashbox/user_balance/get_balance/")}}", {agent_id: $('#agent_id').val(), }, function (response) {

                $("#view_balance").html(response.select_balance);
                setTypeId();

            });
        }


        function setTypeId() {
            balanceId = $("#balance_id").val();
        }


    </script>


@endsection