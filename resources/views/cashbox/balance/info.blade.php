@if(sizeof($transactions))
    <table class="tov-table">
        <thead>
        <tr>
            <th><a href="javascript:void(0);">ID</a></th>
            <th><a href="javascript:void(0);">Дата</a></th>
            <th><a href="javascript:void(0);">Дней</a></th>
            <th><a href="javascript:void(0);">Оператор</a></th>
            <th><a href="javascript:void(0);">Событие</a></th>
            <th><a href="javascript:void(0);">Тип</a></th>
            <th><a href="javascript:void(0);">Сумма</a></th>
            <th><a href="javascript:void(0);">Сальдо</a></th>
            <th><a href="javascript:void(0);">Назначение платежа</a></th>
            <th><a href="javascript:void(0);"></a></th>
        </tr>
        </thead>
        @foreach($transactions as $transaction)
            <tr>
                <td>{{$transaction->id}}</td>
                <td>{{setDateTimeFormatRu($transaction->event_date)}}</td>
                <td>{{(int)countDayToDates(setDateTimeFormatRu($transaction->event_date, 1), date("d.m.Y"))}}</td>
                <td>{{$transaction->user->name}}</td>
                <td>{{\App\Models\Users\UsersBalance::EVENT_TYPE_ID[$transaction->event_type_id]}}</td>
                <td>{{\App\Models\Users\UsersBalance::TYPE_ID[$transaction->type_id]}}</td>
                <td>{{titleFloatFormat($transaction->total_sum)}}</td>
                <td>{{titleFloatFormat($transaction->residue)}}</td>
                <td>{!! $transaction->purpose_payment  !!}</td>
                <td><a id="download_document" data-event-type="{{$transaction->event_type_id}}" data-id="{{$transaction->id}}" class="btn btn-success inline-block doc_export_btn" title="Shift + click для перехода на страницу документации" href="/cashbox/user_balance/balance/{{$transaction->id}}/export_balance?transaction_type={{$transaction->event_type_id}}&transaction_id={{$transaction->id}}">{{$transaction->event_type_id == 1 ? 'Скачать РКО' : 'Скачать ПКО'}}</a></td>

            </tr>
        @endforeach

    </table>
@else
    {{ trans('form.empty') }}
@endif