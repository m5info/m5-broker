<div class="header_bab">


    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="tt" class="easyui-tabs" data-options="tools:'#tab-tools'">
            <div title="Транзакции" id="tab-0" data-view="0"></div>
            <div title="Инкассации" id="tab-1" data-view="1"></div>
            <div title="Приход/Расход" id="tab-2" data-view="2"></div>
        </div>
    </div>
</div>


<div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12"
     style="margin-top: -5px;overflow: auto;">
    <div class="row">

        <div class="btn-group col-xs-6 col-sm-4 col-md-2 col-lg-2">
            {{ Form::text('begin_date', date("d.m.Y"), ['class' => 'form-control short-date datepicker date', 'id' => 'begin_date', 'placeholder' => 'Дата начала', 'onchange' => 'loadItems()']) }}
        </div>
        <div class="btn-group col-xs-6 col-sm-4 col-md-2 col-lg-2">
            {{ Form::text('end_date', date("d.m.Y"), ['class' => 'form-control short-date datepicker date', 'id' => 'end_date', 'placeholder' => 'Дата окончания', 'onchange' => 'loadItems()']) }}
        </div>
        <div class="btn-group col-xs-6 col-sm-4 col-md-3 col-lg-2">
            {{Form::select('type_payment_id', collect(\App\Models\Cashbox\CashboxTransactions::TYPE_ID)->prepend('Не выбрано', -1), -1, ['class' => 'form-control select2-all', 'onclick' => 'setLastMonthInTrans()', 'id'=>'type_id'])}}
        </div>
        <div class="btn-group col-xs-6 col-sm-4 col-md-2 col-lg-2 giving">
            <label for="">Приход | Расход</label>
            <select name="get_give" onchange="loadItems()" class="form-control select2-ws select2-offscreen">
                <option value="0">Не делить</option>
                <option value="1">Делить</option>
            </select>
        </div>
        <div class=" col-xs-3 col-sm-8 col-md-3 col-lg-3">
            <span class="btn btn-primary btn-right" onclick="setLastMonthInTrans()">Последний месяц</span>
        </div>


    </div>

    <div id="main_container">

    </div>


</div>

<style>
    .sorting .giving {
        margin-top: -12px;
    }

    @media (max-width: 980px){
        .sorting .giving {
            margin-top: 4px;
        }
    }

    .second_total_block {
        margin-left: 100px;
    }

    @media (max-width: 930px) {
        .second_total_block {
            margin-left: 0px;
        }
    }
</style>

<script>

    var map;
    var TAB_INDEX = 0;

    document.addEventListener("DOMContentLoaded", function(){
        loaderShow();
        loadItems();
        loaderHide();
    });

    function initInfo() {
        $('#tt').tabs({
            border: false,
            pill: false,
            plain: true,
            onSelect: function (title, index) {
                return selectTab(index);
            }
        });


        selectTab(0);
    }

    function setLastMonthInTrans() {
        setLastMonth();
        loadItems();

    }

    function loadItems() {

        loaderShow();

        $.get("{{url("/cashbox/collection/{$cashbox->id}/get_details")}}", get_data(), function (response) {

            $("#main_container").html(response);

        }).always(function () {
            loaderHide();
        });
    }


    function selectTab(id) {
        var tab = $('#tt').tabs('getSelected');
        TAB_INDEX = tab.data('view');

        loaderShow();

        $.get("{{url("/cashbox/collection/{$cashbox->id}/get_details")}}", get_data(), function (response) {
            $("#main_container").html(response);
        }).always(function () {
            loaderHide();
        });

    }

    function get_data(){
        return {
            type_id: TAB_INDEX,
            type_payment_id: $("[name='type_payment_id']").val(),
            begin_date: $("#begin_date").val(),
            end_date: $("#end_date").val(),
            divide: $('[name = "get_give"]').val()
        };
    }

</script>