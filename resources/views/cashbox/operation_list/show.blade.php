@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">

@append

@section('content')


    <div class="page-heading">
        <h2>Касса #{{($cashbox->id)?:' новая'}}

            @if($cashbox->id > 0)
                <div class="pull-right">{{titleFloatFormat($cashbox->balance)}}</div>
            @endif
        </h2>




    </div>

    <br/>

    <div class="divider"></div>
    <br/>


    <div class="row form-horizontal">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


            @if($cashbox->id > 0)


                @include('cashbox.operation_list.info', ['cashbox' => $cashbox])


            @endif

        </div>

    </div>


@endsection

@section('js')

    <script>

        $(function () {

            @if($cashbox->id > 0)
            initInfo();
            @endif


        });

    </script>


@endsection