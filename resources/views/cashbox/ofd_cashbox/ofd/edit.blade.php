@extends('layouts.frame')

@section('title')

    {{ $organization->title }}

    @if(isset($response_json))

        <br/>
        <span style="color: red;">
            {{(string)$response_json->Error->Message}}
        </span>



    @endif

@endsection

@section('content')


    {{ Form::open(['url' => url("/cashbox/ofd_cashbox/{$organization->id}/edit/".(int)$ofd->id), 'method' => 'post', 'id' => 'controlForm', 'class' => 'form-horizontal']) }}

    <input type="hidden" id="status_cashbox" value="0" name="status_cashbox"/>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div class="row form-horizontal">

            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
                <label class="control-label">Тип</label>
                {{ Form::select('client_type_id', collect([0=>"ФЛ", 1=>'ЮЛ']), $ofd->client_type_id, ['class' => 'form-control select2-ws', 'id'=>'client_type_id']) }}
            </div>

            <div class="client_0 client_data col-xs-9 col-sm-9 col-md-9 col-lg-9" >
                <label class="control-label">ФИО</label>
                {{ Form::text('client_fio', $ofd->client_title, ['class' => 'form-control', 'id'=>'client_fio']) }}
            </div>

            <div class="client_1 client_data col-xs-9 col-sm-9 col-md-9 col-lg-9" >
                <label class="control-label">Название</label>
                {{ Form::text('client_title', $ofd->client_title, ['class' => 'form-control', 'id'=>'client_title']) }}
            </div>

        </div>
    </div>


    <div class="client_0 client_data col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div class="row form-horizontal">

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                <label class="control-label">Паспорт серия</label>
                {{ Form::text('client_doc_serie', $ofd->client_doc_serie, ['class' => 'form-control', 'id'=>'client_doc_serie']) }}
            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                <label class="control-label">Паспорт номер</label>
                {{ Form::text('client_doc_number', $ofd->client_doc_number, ['class' => 'form-control', 'id'=>'client_doc_number']) }}
            </div>

        </div>
    </div>


    <div class="client_1 client_data col-xs-12 col-sm-12 col-md-12 col-lg-12"  >
        <div class="row form-horizontal">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <label class="control-label">ИНН</label>
                {{ Form::text('client_inn', $ofd->client_inn, ['class' => 'form-control', 'id'=>'client_inn']) }}
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div class="row form-horizontal">

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                <label class="control-label">Телефон</label>
                {{ Form::text('client_phone', $ofd->client_phone, ['class' => 'form-control phone', 'id'=>'client_phone']) }}
            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                <label class="control-label">Email</label>
                {{ Form::text('client_email', $ofd->client_email, ['class' => 'form-control', 'id'=>'client_email']) }}
            </div>

        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"  >
        <div class="row form-horizontal">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <label class="control-label">Категория</label>
                {{ Form::select('type_id', collect(\App\Models\Organizations\Organization::REQUEST_TYPE), $ofd->type_id, ['class' => 'form-control select2-ws', 'id'=>'payment_method_id']) }}
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div class="row form-horizontal">

            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
                <label class="control-label">Дата</label>
                {{ Form::text('create_date', (getDateFormatRu($ofd->create_date) == '')?date("d.m.Y"):getDateFormatRu($ofd->create_date), ['class' => 'form-control date short-date datepicker', 'id'=>'create_date']) }}
            </div>

            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
                <label class="control-label">Время</label>
                {{ Form::text('create_time', (getDateFormatTimeRu($ofd->create_date) == '')?date("H:i"):getDateFormatTimeRu($ofd->create_date), ['class' => 'form-control time', 'id'=>'create_date']) }}
            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                <label class="control-label">Инициатор</label>
                {{ Form::select('user_id', $agents->prepend('Нет', 0), $ofd->user_id, ['class' => 'form-control select2', 'id'=>'user_id']) }}

            </div>

        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div class="row form-horizontal">

            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
                <label class="control-label">Метод</label>
                {{ Form::select('payment_method_id', collect(\App\Services\FNS\FNSFerma::PaymentMethodType), $ofd->payment_method_id, ['class' => 'form-control select2-ws', 'id'=>'payment_method_id']) }}
            </div>

            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
                <label class="control-label">Тип оплаты</label>
                {{ Form::select('payment_type', collect(\App\Services\FNS\FNSFerma::PaymentTypeType), $ofd->payment_type, ['class' => 'form-control select2-ws', 'id'=>'payment_type']) }}
            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                <label class="control-label">Сумма</label>
                {{ Form::text('payment_total', ($ofd->payment_total>0)?titleFloatFormat($ofd->payment_total):'', ['class' => 'form-control sum', 'id'=>'payment_total']) }}
            </div>

        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"  >
        <div class="row form-horizontal">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <label class="control-label">Товар</label>
                {{ Form::text('product_title', $ofd->product_title, ['class' => 'form-control', 'id'=>'product_title', 'placeholder'=>'ОСАГО MMM 0000000']) }}
            </div>
        </div>
    </div>


    {{Form::close()}}


@endsection



@section('js')

    <script>

        $(function () {

            $('#client_type_id').change(function () {

                $('.client_data').hide();
                $('.client_'+$(this).val()).show();
            });
            $('#client_type_id').change();


        });

        $('#client_fio').suggestions({
            serviceUrl: '{{url("/custom_suggestions/suggest_physical/")}}',
            token: DADATA_TOKEN,
            type: "NAME",
            count: 5,
            onSelect: function (suggestion) {
                var data = suggestion.data;
                $('#client_fio').val(suggestion.value);

            }
        });

        $('#client_title, #client_inn').suggestions({
            serviceUrl: '{{url("/custom_suggestions/suggest_jure/")}}',
            token: DADATA_TOKEN,
            type: "PARTY",
            count: 5,
            onSelect: function (suggestion) {
                var data = suggestion.data;
                $('#client_title').val(suggestion.value);
                $('#client_inn').val(data.inn);
            }
        });


        function sendCashbox()
        {

            $("#status_cashbox").val(1);
            $("#controlForm").submit();
        }

    </script>


@endsection

@section('footer')

    <span class="btn btn-success pull-left" onclick="sendCashbox()">Выпустить чек</span>

    <button onclick="submitForm()" type="submit" class="btn btn-primary pull-right">Сохранить</button>

@endsection

