@extends('layouts.clear')

@section('title')




@endsection

@section('content')



    <div id="758de03ce0a94968e45e4aa4308cc278mailsub">
        <table align="center" bgcolor="ffffff" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"
               style="color:#0d3349;font-family:'arial'">
            <tbody>

            <tr>
                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="640">
                        <tbody>
                        <tr>
                            <td width="50"> </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                       style="color:#0d3349;font-family:'arial'">
                                    <tbody>

                                    <tr>
                                        <td align="center" colspan="2">

                                            <table bgcolor="f0f5f6" border="0" width="460"
                                                   style="border:1px solid rgba( 20 , 41 , 54 , 0.2 );color:#00306f;font:14px 'arial' , sans-serif">
                                                <tbody>

                                                <tr>
                                                    <td align="center">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="400" style="border-collapse:separate;border-spacing:3px 10px;color:#00306f;font:14px 'arial' , sans-serif;word-break:break-all">
                                                            <tbody>

                                                            <tr>
                                                                <td align="center" colspan="2"
                                                                    style="color:#86abc1;font-size:20px;font-weight:700">
                                                                    Кассовый чек / Приход
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" height="15"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" colspan="2">Общество с ограниченной
                                                                    ответственностью Страховой Дом "Еврогарант"
                                                                </td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">ФИСКАЛЬНЫЙ ДОКУМЕНТ</td>
                                                                <td align="right" width="50%">#14</td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">ДАТА ВЫДАЧИ</td>
                                                                <td align="right" width="50%">03.08.19 15:51</td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">АДРЕС РАСЧЁТОВ</td>
                                                                <td align="right" width="50%"
                                                                    style="word-wrap:break-word">г. Москва, ул.
                                                                    Ленинская Слобода, д.19, 2 этаж, комната 21и1-24
                                                                </td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">МЕСТО РАСЧЁТОВ</td>
                                                                <td align="right" width="50%"
                                                                    style="word-wrap:break-word">eurogarant.ru
                                                                </td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">КАССИР</td>
                                                                <td align="right" width="50%">СИС. АДМИНИСТРАТОР</td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td width="50%">НОМЕР СМЕНЫ</td>
                                                                <td align="right" width="50%">#2</td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">ДОКУМЕНТ В СМЕНЕ</td>
                                                                <td align="right" width="50%">#4</td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">ЭЛ. АДР. ПОКУПАТЕЛЯ</td>
                                                                <td align="right" width="50%"><a
                                                                            href="mailto:narf-narf@yandex.ru">narf-narf@yandex.ru</a></td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">РН <span
                                                                            class="wmi-callto">0003578225050335</span>
                                                                </td>
                                                                <td align="right" width="50%">ИНН <span
                                                                            class="wmi-callto">7718290690</span></td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">ФН <span
                                                                            class="wmi-callto">9287440300423233</span>
                                                                </td>
                                                                <td align="right" width="50%">ФПД <span
                                                                            class="wmi-callto">3290359308</span></td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">ОФД</td>
                                                                <td align="right" width="50%">ООО ПС СТ</td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">Сайт ОФД</td>
                                                                <td align="right" width="50%">ofd.ru</td>
                                                            </tr>
                                                            <tr>
                                                                <td bgcolor="e4e9ef" colspan="2" height="2"></td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%"></td>
                                                                <td align="right" valign="bottom" width="50%">
                                                                    1 X 10.00<br/>
                                                                    = 10.00
                                                                </td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="right" colspan="2">
                                                                    в т.ч. НДС НЕ ОБЛАГАЕТСЯ = 0.00<br/>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td bgcolor="e4e9ef" colspan="2" height="2"></td>
                                                            </tr>
                                                            <tr valign="top" style="font-size:30px">
                                                                <td align="left" width="50%">ИТОГ</td>
                                                                <td align="right" width="50%">10.00</td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">ПРЕДВАРИТЕЛЬНАЯ ОПЛАТА<br/>(АВАНС)
                                                                </td>
                                                                <td align="right" width="50%">0.00</td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">ПОСЛЕДУЮЩАЯ ОПЛАТА<br/>(КРЕДИТ)
                                                                </td>
                                                                <td align="right" width="50%">0.00</td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" width="50%">ИНАЯ ФОРМА ОПЛАТЫ</td>
                                                                <td align="right" width="50%">0.00</td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">Наличными</td>
                                                                <td align="right" width="50%">0.00</td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">Безналичными</td>
                                                                <td align="right" width="50%">10.00</td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">  в т.ч. налоги</td>
                                                                <td align="right" width="50%"> </td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">  НДС НЕ ОБЛАГАЕТСЯ</td>
                                                                <td align="right" width="50%">10.00</td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" width="50%">Система налогообложения
                                                                </td>
                                                                <td align="right" width="50%">УСН доход - расход</td>
                                                            </tr>
                                                            <tr>
                                                                <td bgcolor="e4e9ef" colspan="2" height="2"></td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td align="left" colspan="2">
                                                                    Сервис проверки чеков: <a target="_blank" href="https://check.ofd.ru/">https://check.ofd.ru/</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" height="15"> </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td width="50"> </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>

            <tr>
                <td align="center" bgcolor="ffffff" valign="top">

                </td>
            </tr>
            </tbody>
        </table>
    </div>


@endsection



@section('js')

    <script>

        $(function () {



        });



    </script>


@endsection

@section('footer')



@endsection

