@extends('layouts.app')

@section('head')

@append

@section('content')


    <div class="page-heading">
        <h2>Организации</h2>




    </div>

    <br/>

    <div class="divider"></div>
    <br/>


    <div class="row form-horizontal">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @if(sizeof($organizations))
                <table class="tov-table">
                    <thead>
                    <tr>
                        <th><a href="javascript:void(0);">{{ trans('organizations/organizations.title') }}</a></th>
                        <th><a href="javascript:void(0);">Тип</a></th>
                        <th><a href="javascript:void(0);">ИНН</a></th>
                        <th><a href="javascript:void(0);">КПП</a></th>
                        <th><a href="javascript:void(0);">Тип кассы</a></th>
                    </tr>
                    </thead>
                    @foreach($organizations as $organization)
                        <tr class="clickable-row" data-href="{{url ("/cashbox/ofd_cashbox/$organization->id/view")}}">
                            <td>{{ $organization->title }}</td>
                            <td>{{ $organization->org_type ? $organization->org_type->title : "" }}</td>
                            <td>{{ $organization->inn }}</td>
                            <td>{{ $organization->kpp }}</td>
                            <td>{{ \App\Models\Organizations\Organization::CASHBOX_API_TYPE[$organization->cashbox_api_type_id] }}</td>
                        </tr>
                    @endforeach
                </table>
            @else
                {{ trans('form.empty') }}
            @endif

        </div>
    </div>


@endsection

@section('js')

    <script>

        $(function () {




        });

    </script>


@endsection