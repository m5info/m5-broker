@extends('layouts.app')

@section('head')

@append

@section('content')


    <div class="page-heading">
        <h1 class="inline-h1">{{ $organization->title }}</h1>

        <span class="btn btn-primary btn-right " onclick="openFancyBoxFrame('{{ url("/cashbox/ofd_cashbox/{$organization->id}/edit/0")  }}')">
            {{ trans('form.buttons.create') }}
        </span>

    </div>


    <div class="divider"></div>
    <br/>


    <div class="row form-horizontal">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            Фильтры по всем колонкам, кол-во на странице и пагинация. Фильтры можно сделать GET

        </div>
    </div>

    <div class="divider"></div>
    <br/>


    <div class="row form-horizontal">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


            @if(sizeof($ofd_cashbox))
                <table class="table">
                    <thead>
                    <tr>
                        <th><a href="javascript:void(0);">Дата время</a></th>
                        <th><a href="javascript:void(0);">Статус</a></th>
                        <th><a href="javascript:void(0);">Категория</a></th>
                        <th><a href="javascript:void(0);">Метод</a></th>

                        <th><a href="javascript:void(0);">Тип оплаты</a></th>
                        <th><a href="javascript:void(0);">Сумма</a></th>
                        <th><a href="javascript:void(0);">Клиент</a></th>
                        <th><a href="javascript:void(0);">Товар</a></th>
                        <th><a href="javascript:void(0);">Инициатор</a></th>
                        <th><a href="javascript:void(0);"> </a></th>
                    </tr>
                    </thead>
                    @foreach($ofd_cashbox as $ofd)
                        <tr

                        @if($ofd->status == 'Success')

                            style="background-color: #e6ffe6"

                        @elseif($ofd->status == 'Failed')

                            style="background-color: #ffe6e6"

                        @endif


                        >
                            <td>{{ setDateTimeFormatRu($ofd->create_date) }}</td>
                            <td>{{ isset(\App\Services\FNS\FNSFerma::STATUS[$ofd->status])?\App\Services\FNS\FNSFerma::STATUS[$ofd->status]:'' }}</td>
                            <td>{{ \App\Models\Organizations\Organization::REQUEST_TYPE[$ofd->type_id] }}</td>
                            <td>{{ \App\Services\FNS\FNSFerma::PaymentMethodType[$ofd->payment_method_id] }}</td>
                            <td>{{ \App\Services\FNS\FNSFerma::PaymentTypeType[$ofd->payment_type] }}</td>
                            <td>{{ titleFloatFormat($ofd->payment_total) }}</td>
                            <td>{{ $ofd->client_title }}</td>
                            <td>{{ $ofd->product_title }}</td>
                            <td>{{ ($ofd->user)?$ofd->user->name:'' }}</td>
                            <td>

                                @if($ofd->status == 'Success')

                                    <span class="btn btn-success btn-right " onclick="openFancyBoxFrame('{{ url("/cashbox/ofd_cashbox/{$organization->id}/show/{$ofd->id}")  }}')">
                                        <i class="fa fa-eye"></i>
                                    </span>

                                @else

                                    <span class="btn btn-primary btn-right " onclick="openFancyBoxFrame('{{ url("/cashbox/ofd_cashbox/{$organization->id}/edit/{$ofd->id}")  }}')">
                                        <i class="fa fa-edit"></i>
                                    </span>

                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            @else
                {{ trans('form.empty') }}
            @endif




        </div>
    </div>


@endsection

@section('js')

    <script>

        $(function () {




        });

    </script>


@endsection