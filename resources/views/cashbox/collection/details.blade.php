<div style="padding: 10px 0;">
    <div class="inline-block total_sum_block">
        <div class="inline-block total_sum_block_sub">
            <span style="display: inline-block"><label for="">Итог вчерашнего дня: </label> <span class="bold"> {{$last_day_sum}} ₽</span></span>
        </div><div class="inline-block total_sum_block_sub">
            <span style="display: inline-block"><label for="">Приход: </label> <span class="bold"> {{$last_day_sum_comming}} ₽</span></span>
        </div><div class="inline-block total_sum_block_sub">
            <span style="display: inline-block"><label for="">Расход: </label> <span class="bold"> {{$last_day_sum_spend}} ₽</span></span>
        </div>
    </div>
    <div class="inline-block total_sum_block second_total_block">

        <div class="inline-block total_sum_block_sub">
            <span style="display: inline-block;"><label for="">Итог за период: </label> <span class="bold"> {{$current_day_sum}} ₽</span></span>
        </div>
        <div class="inline-block total_sum_block_sub">
            <span style="display: inline-block"><label for="">Приход: </label> <span class="bold"> {{$current_day_sum_comming}} ₽</span></span>
        </div>
        <div class="inline-block total_sum_block_sub">
            <span style="display: inline-block"><label for="">Расход: </label> <span class="bold"> {{$current_day_sum_spend}} ₽</span></span>
        </div>

    </div>
</div>
@if(sizeof($transactions))
    <table class="tov-table">
        <thead>
        <tr>
            <th><a href="javascript:void(0);">ID</a></th>
            <th><a href="javascript:void(0);">Дата</a></th>
            <th><a href="javascript:void(0);">Оператор</a></th>
            <th><a href="javascript:void(0);">Событие</a></th>
            <th><a href="javascript:void(0);">Нал \ Безнал</a></th>
            <th><a href="javascript:void(0);">Сумма</a></th>
            <th><a href="javascript:void(0);">Сальдо</a></th>
            <th><a href="javascript:void(0);">Назначение платежа</a></th>
            @if($type_id == 2)
                <th><a href="#">Агент/Менеджер</a></th>
            @endif
        </tr>
        </thead>
        @foreach($transactions as $transaction)
            <tr>
                <td>{{$transaction->id}}</td>
                <td>{{setDateTimeFormatRu($transaction->event_date)}}</td>
                <td>{{$transaction->user->name}}</td>
                <td>{{\App\Models\Users\UsersBalance::EVENT_TYPE_ID[$transaction->event_type_id]}}</td>
                <td>{{\App\Models\Users\UsersBalance::TYPE_ID[$transaction->type_id]}}</td>
                <td>{{titleFloatFormat($transaction->total_sum)}}</td>
                <td>{{titleFloatFormat($transaction->residue)}}</td>
                <td>{!! $transaction->purpose_payment  !!}</td>
                @if($type_id == 2)
                    <td>{{ $transaction->invoice_payment_user ? $transaction->invoice_payment_user->name : "" }}</td>
                @endif
            </tr>
        @endforeach

    </table>
@else
    <div class="form_empty_position">
        {{ trans('form.empty') }}
    </div>
@endif