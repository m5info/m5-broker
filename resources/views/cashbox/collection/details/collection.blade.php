
@if(sizeof($transactions))
    <table class="tov-table">
        <thead>
        <tr>
            <th><a href="javascript:void(0);">ID</a></th>
            <th><a href="javascript:void(0);">Дата</a></th>
            <th><a href="javascript:void(0);">Оператор</a></th>
            <th><a href="javascript:void(0);">Событие</a></th>
            <th><a href="javascript:void(0);">Сумма</a></th>
            <th><a href="javascript:void(0);">Сальдо</a></th>
            <th><a href="javascript:void(0);">Назначение платежа</a></th>
        </tr>
        </thead>
        @foreach($transactions as $transaction)
            <tr>
                <td>{{$transaction->id}}</td>
                <td>{{setDateTimeFormatRu($transaction->event_date)}}</td>
                <td>{{$transaction->user->name}}</td>
                <td>{{\App\Models\Users\UsersBalance::EVENT_TYPE_ID[$transaction->event_type_id]}}</td>
                <td>{{titleFloatFormat($transaction->total_sum)}}</td>
                <td>{{titleFloatFormat($transaction->residue)}}</td>
                <td>{!! $transaction->purpose_payment !!}</td>
            </tr>
        @endforeach

    </table>
@else
    <div class="form_empty_position">
        {{ trans('form.empty') }}
    </div>
@endif