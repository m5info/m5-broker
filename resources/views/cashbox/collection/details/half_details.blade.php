@include('cashbox.collection.details.summary', ['cashbox'=>$cashbox, 'summary'=>$summary])

@php
    $total_p = 0;
    $total_s = 0;

@endphp



<div class="row">
    <div class="col-md-6">
        @if(sizeof($transactions))
            <h3>Приход</h3>
            <table class="tov-table optimize-table">
                <thead>
                <tr>
                    <th><a href="javascript:void(0);">ID</a></th>
                    <th><a href="javascript:void(0);">Дата</a></th>
                    <th><a href="javascript:void(0);">Оператор</a></th>
                    <th><a href="javascript:void(0);">Нал \ Безнал</a></th>
                    <th><a href="javascript:void(0);">Сумма</a></th>
                    <th><a href="javascript:void(0);">Назначение платежа</a></th>
                    <th><a href="#">Агент/Менеджер</a></th>
                </tr>
                </thead>
                @foreach($transactions as $transaction)
                    @if($transaction->event_type_id == 0)
                        <tr>
                            <td>{{$transaction->id}}</td>
                            <td>{{setDateTimeFormatRu($transaction->event_date)}}</td>
                            <td>{{$transaction->user->name}}</td>
                            <td>{{\App\Models\Cashbox\CashboxTransactions::TYPE_ID[$transaction->type_id]}}</td>
                            <td nowrap>{{titleFloatFormat($transaction->total_sum)}}</td>
                            <td>{!! $transaction->purpose_payment  !!}</td>
                            <td>{{ $transaction->invoice_payment_user ? $transaction->invoice_payment_user->name : "" }}</td>
                        </tr>
                        @php $total_p += $transaction->total_sum; @endphp
                    @endif
                @endforeach
                <tr>
                    <td colspan="4">Итого</td>
                    <td nowrap>{{titleFloatFormat($total_p)}}</td>
                    <td colspan="3"></td>
                </tr>
            </table>

        @else
            <div class="form_empty_position">
                {{ trans('form.empty') }}
            </div>
        @endif
    </div>

    <div class="col-md-6">
        @if(sizeof($transactions))
            <h3>Списание</h3>
            <table class="tov-table optimize-table">
                <thead>
                <tr>
                    <th><a href="javascript:void(0);">ID</a></th>
                    <th><a href="javascript:void(0);">Дата</a></th>
                    <th><a href="javascript:void(0);">Оператор</a></th>
                    <th><a href="javascript:void(0);">Нал \ Безнал</a></th>
                    <th><a href="javascript:void(0);">Сумма</a></th>
                    <th><a href="javascript:void(0);">Назначение платежа</a></th>
                    <th><a href="#">Агент/Менеджер</a></th>
                </tr>
                </thead>
                @foreach($transactions as $transaction)
                    @if($transaction->event_type_id == 1)
                        <tr>
                            <td>{{$transaction->id}}</td>
                            <td>{{setDateTimeFormatRu($transaction->event_date)}}</td>
                            <td>{{$transaction->user->name}}</td>
                            <td>{{\App\Models\Cashbox\CashboxTransactions::TYPE_ID[$transaction->type_id]}}</td>
                            <td nowrap>{{titleFloatFormat($transaction->total_sum)}}</td>
                            <td>{!! $transaction->purpose_payment  !!}</td>
                            <td>{{ $transaction->invoice_payment_user ? $transaction->invoice_payment_user->name : "" }}</td>
                        </tr>
                        @php $total_s += $transaction->total_sum; @endphp
                    @endif
                @endforeach
                <tr>
                    <td colspan="4">Итого</td>
                    <td nowrap>{{titleFloatFormat($total_s)}}</td>
                    <td colspan="3"></td>
                </tr>
            </table>
        @else
            <div class="form_empty_position">
                {{ trans('form.empty') }}
            </div>
        @endif
    </div>

</div>