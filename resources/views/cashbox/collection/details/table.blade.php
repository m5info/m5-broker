@include('cashbox.collection.details.summary', ['cashbox'=>$cashbox, 'summary'=>$summary])

@if(sizeof($transactions))
    <table class="tov-table">
        <thead>
        <tr>
            <th><a href="javascript:void(0);">ID</a></th>
            <th><a href="javascript:void(0);">Дата</a></th>
            <th><a href="javascript:void(0);">Оператор</a></th>
            <th><a href="javascript:void(0);">Тип</a></th>
            <th><a href="javascript:void(0);">Событие</a></th>
            <th><a href="javascript:void(0);">Сумма</a></th>
            <th><a href="javascript:void(0);">Сальдо</a></th>
            <th><a href="javascript:void(0);">Назначение платежа</a></th>
            <th><a href="javascript:void(0);">Агент/Менеджер</a></th>
        </tr>
        </thead>
        @foreach($transactions as $transaction)
            <tr>
                <td>{{$transaction->id}}</td>
                <td>{{setDateTimeFormatRu($transaction->event_date)}}</td>
                <td>{{($transaction->user)?$transaction->user->name:''}}</td>
                <td>{{\App\Models\Cashbox\CashboxTransactions::TYPE_ID[$transaction->type_id]}}</td>
                <td>{{\App\Models\Users\UsersBalance::EVENT_TYPE_ID[$transaction->event_type_id]}}</td>
                <td class="nowrap">{{titleFloatFormat($transaction->total_sum)}}</td>
                <td class="nowrap">{{titleFloatFormat($transaction->residue)}}</td>
                <td>{!! $transaction->purpose_payment !!}</td>
                <td>{{ $transaction->invoice_payment_user ? $transaction->invoice_payment_user->name : "" }}</td>
            </tr>
        @endforeach

    </table>
@else
    <div class="form_empty_position">
        {{ trans('form.empty') }}
    </div>
@endif