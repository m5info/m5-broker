@php


    $summary_early = $cashbox->getEarlyTotalSummary($start_date);

    $summary_early['_total'] = $summary_early['balance'] + $summary['all'];
@endphp


<div style="padding: 10px 0;" >
    <div class="inline-block total_sum_block">
        <table style="background: #efecec;">
            <tbody>
                <tr>
                    <td>
                        <div class="inline-block total_sum_block_sub">
                            <span style="display: inline-block"><label for="">Итог предыдущего дня: </label> <span class="bold"> {{titleFloatFormat($summary_early['all'])}} ₽</span></span>
                        </div>
                    </td>
                    <td>
                        <div class="inline-block total_sum_block_sub">
                            <span style="display: inline-block"><label for="">Баланс предыдущего дня: </label> <span class="bold"> {{titleFloatFormat($summary_early['balance'])}} ₽</span></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="inline-block total_sum_block_sub">
                            <span style="display: inline-block"><label for="">Приход: </label> <span class="bold"> {{titleFloatFormat($summary_early['coming'])}} ₽</span></span>
                        </div>
                    </td>
                    <td>
                        <div class="inline-block total_sum_block_sub">
                            <span style="display: inline-block"><label for="">Расход: </label> <span class="bold"> {{titleFloatFormat($summary_early['consumption'])}} ₽</span></span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="inline-block total_sum_block" style="margin-left: 34px;">
        <table style="background: #efecec;">
            <tbody>
                <tr>
                    <td>
                        <div class="inline-block total_sum_block_sub">
                            <span style="display: inline-block;"><label for="">Итог за период: </label> <span class="bold"> {{titleFloatFormat($summary['all'])}} ₽</span></span>
                        </div>
                    </td>
                    <td>
                        <div class="inline-block total_sum_block_sub">
                            <span style="display: inline-block"><label for="">Общий итог: </label> <span class="bold"> {{titleFloatFormat($summary_early['_total'])}} ₽</span></span>
                        </div>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="inline-block total_sum_block_sub">
                            <span style="display: inline-block"><label for="">Приход: </label> <span class="bold"> {{titleFloatFormat($summary['coming'])}} ₽</span></span>
                        </div>
                    </td>
                    <td>
                        <div class="inline-block total_sum_block_sub">
                            <span style="display: inline-block"><label for="">Расход: </label> <span class="bold"> {{titleFloatFormat($summary['consumption'])}} ₽</span></span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>