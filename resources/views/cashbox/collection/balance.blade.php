@extends('layouts.frame')


@section('title')


    Касса #{{$cashbox->id}} {{ $cashbox->title }}

@endsection

@section('content')


    {{ Form::open(['url' => url("/cashbox/collection/{$cashbox->id}/set_balance"), 'method' => 'post', 'class' => 'form-horizontal']) }}

    <input type="hidden" name="type_id" value="0"/>

    <div class="form-group">
        <div class="col-sm-6">
            <label class="col-sm-12 control-label">Сумма</label>
            <div class="col-sm-12">
                {{ Form::text('total_sum', '', ['class' => 'form-control sum', 'required']) }}
                @if(isset($error_balance))
                    <p style="color: red">Недостаточно средств</p>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <label class="col-sm-12 control-label">Событие</label>
            <div class="col-sm-12">
                {{ Form::select('event_type_id', collect(\App\Models\Users\UsersBalance::EVENT_TYPE_ID), 0, ['class' => 'form-control', 'id'=>'event_type_id']) }}
            </div>
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-12">
            <label class="col-sm-12 control-label">Назначение платежа</label>
            <div class="col-sm-12">
                {{ Form::textarea('purpose_payment', '', ['class' => 'form-control ', 'required']) }}
            </div>
        </div>
    </div>


    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection




@section('js')

    <script>




        $(function () {


        });




    </script>


@endsection

