@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">

@append

@section('content')


    <div class="page-heading">
        <h2>Касса #{{($cashbox->id)?:' новая'}}

            @if($cashbox->id > 0)
                <span class="btn btn-danger pull-right" style="margin-right: 1.5%;"
                      onclick="openFancyBoxFrame('{{url("/cashbox/collection/{$cashbox->id}/set_collection")}}')">Провести инкассацию</span>
            @endif
        </h2>


    </div>

    <br/>

    <div class="divider"></div>
    <br/>


    <div class="row form-horizontal">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div class="block-main">
                    <div class="block-sub">
                        <div class="row">

                            {{ Form::open(['url' => url("/cashbox/collection/".(int)$cashbox->id."/edit"), 'method' => 'post', 'class' => 'form-horizontal']) }}

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <label class="control-label pull-left">
                                    Название
                                </label>
                                <label class="control-label pull-right">Актуально {{ Form::checkbox('is_actual', 1, $cashbox->is_actual) }} </label>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
                                {{ Form::text('title', $cashbox->title, ['class' => 'form-control', 'id'=>'title' ]) }}
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label class="control-label">Ответственный</label>
                                {{ Form::select('user_id', \App\Models\User::getALLUser()->pluck('name', 'id')->prepend('Выберите значение', 0), $cashbox->user_id, ['class' => 'form-control select2', 'id'=>'user_id']) }}
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label class="control-label">Баланс</label>
                                {{ Form::text('balance', titleFloatFormat($cashbox->balance), ['class' => 'form-control sum', 'id'=>'balance', 'readonly' ]) }}
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label class="control-label">Максимальный баланс</label>
                                {{ Form::text('max_balance', ($cashbox->max_balance>0)?titleFloatFormat($cashbox->max_balance):'', ['class' => 'form-control sum', 'id'=>'max_balance' ]) }}
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label class="control-label">Организация</label>
                                {{ Form::select('organization_id', \App\Models\Organizations\Organization::all()->pluck('title', 'id'), $cashbox->organization_id, ['class' => 'form-control select2', 'id'=>'user_id']) }}
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <button type="submit" class="btn btn-primary pull-left">Сохранить</button>

                                @if($cashbox->id > 0)
                                    <span class="btn btn-success pull-right"
                                          onclick="openFancyBoxFrame('{{url("/cashbox/collection/{$cashbox->id}/set_balance")}}')">Операции с балансом</span>
                                @endif


                            </div>

                            {{Form::close()}}


                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
    <div class="form-horizontal">


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            @if($cashbox->id > 0)


                @include('cashbox.collection.info', ['cashbox' => $cashbox])


            @endif

        </div>
    </div>


@endsection

@section('js')

    <script>

        $(function () {

            @if($cashbox->id > 0)
            initInfo();
            @endif


        });

    </script>


@endsection