<div class="header_bab">


    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="tt" class="easyui-tabs" data-options="tools:'#tab-tools'">
            <div title="Приход/Расход (наличные)" id="tab-2" data-view="2"></div>
            <div title="Транзакции" id="tab-0" data-view="0"></div>
            <div title="Инкассации" id="tab-1" data-view="1"></div>

        </div>
    </div>
</div>


<div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12"
     style="margin-top: -5px;overflow: auto;">
    <div class="row">

        <div class="btn-group col-xs-6 col-sm-2 col-md-2 col-lg-2">
            {{ Form::text('begin_date', date("d.m.Y"), ['class' => 'form-control short-date datepicker date', 'id' => 'begin_date', 'placeholder' => 'Дата начала', 'onchange' => 'loadItems()']) }}
        </div>
        <div class="btn-group col-xs-6 col-sm-2 col-md-2 col-lg-2">
            {{ Form::text('end_date', date("d.m.Y"), ['class' => 'form-control short-date datepicker date', 'id' => 'end_date', 'placeholder' => 'Дата окончания', 'onchange' => 'loadItems()']) }}
        </div>

        <div class="btn-group col-xs-6 col-sm-2 col-md-2 col-lg-2">
            {{ Form::select('type_payment_id', collect(\App\Models\Cashbox\CashboxTransactions::TYPE_ID)->prepend('Не выбрано', -1), -1, ['class' => 'form-control short-date datepicker date', 'id' => 'end_date', 'onchange' => 'loadItems()']) }}
        </div>

        <div class="btn-group col-xs-6 col-sm-2 col-md-2 col-lg-2">
            {{ Form::select('event_type_id', collect(\App\Models\Users\UsersBalance::EVENT_TYPE_ID)->prepend('Не выбрано', -1), -1, ['class' => 'form-control', 'onchange' => 'loadItems()']) }}
        </div>


        <div class="btn-group col-xs-6 col-sm-3 col-md-3 col-lg-2" style="margin-top: -12px;">
            <label for="">Приход | Расход</label>
            <select name="get_give" onchange="loadItems()" class="form-control select2-ws select2-offscreen">
                <option value="0">Не делить</option>
                <option value="1">Делить</option>
            </select>
        </div>
        <div class="btn-group col-xs-6 col-sm-3 col-md-2 col-lg-2">
            <span class="btn btn-primary btn-left" onclick="setLastMonthInTrans()">Последний месяц</span>
        </div>


    </div>

    <div id="main_container">

    </div>


</div>


<script src="/plugins/jquery/jquery.min.js"></script>

<script>

    var map;
    var TAB_INDEX = 0;



    function initInfo() {
        $('#tt').tabs({
            border: false,
            pill: false,
            plain: true,
            onSelect: function (title, index) {
                return loadItems();
            }
        });

        loadItems();


    }

    function setLastMonthInTrans() {
        setLastMonth();
        loadItems();

    }

    function loadItems() {



        var tab = $('#tt').tabs('getSelected');
        type = tab.data('view');//$("#tab-"+id).data('view');
        TAB_INDEX = type;


        if ($('[name = "get_give"]').val() == 1){
            $('[name = "event_type_id"]').attr('disabled', 'disabled');
        }else{
            $('[name = "event_type_id"]').removeAttr('disabled');
        }

        loaderShow();
        $.get("{{url("/cashbox/collection/{$cashbox->id}/get_details")}}", {
            type_id: TAB_INDEX,
            begin_date: $("#begin_date").val(),
            end_date: $("#end_date").val(),
            divide: $('[name = "get_give"]').val(),
            type_payment_id: $('[name = "type_payment_id"]').val(),
            event_type_id: $('[name = "event_type_id"]').val()

        }, function (response) {
            loaderHide();
            $("#main_container").html(response);


        }).done(function () {
            loaderShow();
        })
            .fail(function () {
                loaderHide();
            })
            .always(function () {
                loaderHide();
            });
    }



</script>