<div style="padding: 10px 0;" >
    <div class="inline-block total_sum_block">
        <div class="inline-block total_sum_block_sub">
            <span style="display: inline-block"><label for="">Итог вчерашнего дня: </label> <span class="bold"> {{$last_day_sum}} ₽</span></span>
        </div><div class="inline-block total_sum_block_sub">
            <span style="display: inline-block"><label for="">Приход: </label> <span class="bold"> {{$last_day_sum_comming}} ₽</span></span>
        </div><div class="inline-block total_sum_block_sub">
            <span style="display: inline-block"><label for="">Расход: </label> <span class="bold"> {{$last_day_sum_spend}} ₽</span></span>
        </div>
    </div>
    <div class="inline-block total_sum_block" style="margin-left: 50px;">

        <div class="inline-block total_sum_block_sub">
            <span style="display: inline-block;"><label for="">Итог за период: </label> <span class="bold"> {{$current_day_sum}} ₽</span></span>
        </div><div class="inline-block total_sum_block_sub">
            <span style="display: inline-block"><label for="">Приход: </label> <span class="bold"> {{$current_day_sum_comming}} ₽</span></span>
        </div><div class="inline-block total_sum_block_sub">
            <span style="display: inline-block"><label for="">Расход: </label> <span class="bold"> {{$current_day_sum_spend}} ₽</span></span>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @if(sizeof($transactions))
            <h3>Приход</h3>
            <table class="tov-table optimize-table">
                <thead>
                <tr>
                    <th><a href="javascript:void(0);">ID</a></th>
                    <th><a href="javascript:void(0);">Дата</a></th>
                    <th><a href="javascript:void(0);">Оператор</a></th>
                    <th><a href="javascript:void(0);">Нал \ Безнал</a></th>
                    <th><a href="javascript:void(0);">Сумма</a></th>
                    <th><a href="javascript:void(0);">Сальдо</a></th>
                    <th><a href="javascript:void(0);">Назначение платежа</a></th>
                    @if($type_id == 2)
                        <th><a href="#">Агент/Менеджер</a></th>
                    @endif
                </tr>
                </thead>
                @foreach($transactions as $transaction)
                    @if($transaction->event_type_id == 0)
                        <tr>
                            <td>{{$transaction->id}}</td>
                            <td>{{setDateTimeFormatRu($transaction->event_date)}}</td>
                            <td>{{$transaction->user->name}}</td>
                            <td>{{\App\Models\Users\UsersBalance::TYPE_ID[$transaction->type_id]}}</td>
                            <td>{{titleFloatFormat($transaction->total_sum)}}</td>
                            <td>{{titleFloatFormat($transaction->residue)}}</td>
                            <td>{!! $transaction->purpose_payment  !!}</td>
                            @if($type_id == 2)
                                <td>{{ $transaction->invoice_payment_user ? $transaction->invoice_payment_user->name : "" }}</td>
                            @endif
                        </tr>
                    @endif
                @endforeach

            </table>

        @else
            <div class="form_empty_position">
                {{ trans('form.empty') }}
            </div>
        @endif
    </div>

    <div class="col-md-6">
        @if(sizeof($transactions))
            <h3>Списание</h3>
            <table class="tov-table optimize-table">
                <thead>
                <tr>
                    <th><a href="javascript:void(0);">ID</a></th>
                    <th><a href="javascript:void(0);">Дата</a></th>
                    <th><a href="javascript:void(0);">Оператор</a></th>
                    <th><a href="javascript:void(0);">Нал \ Безнал</a></th>
                    <th><a href="javascript:void(0);">Сумма</a></th>
                    <th><a href="javascript:void(0);">Сальдо</a></th>
                    <th><a href="javascript:void(0);">Назначение платежа</a></th>
                    @if($type_id == 2)
                        <th><a href="#">Агент/Менеджер</a></th>
                    @endif
                </tr>
                </thead>
                @foreach($transactions as $transaction)
                    @if($transaction->event_type_id == 1)
                        <tr>
                            <td>{{$transaction->id}}</td>
                            <td>{{setDateTimeFormatRu($transaction->event_date)}}</td>
                            <td>{{$transaction->user->name}}</td>
                            <td>{{\App\Models\Users\UsersBalance::TYPE_ID[$transaction->type_id]}}</td>
                            <td>{{titleFloatFormat($transaction->total_sum)}}</td>
                            <td>{{titleFloatFormat($transaction->residue)}}</td>
                            <td>{!! $transaction->purpose_payment  !!}</td>
                            @if($type_id == 2)
                                <td>{{ $transaction->invoice_payment_user ? $transaction->invoice_payment_user->name : "" }}</td>
                            @endif
                        </tr>
                    @endif
                @endforeach

            </table>
        @else
            <div class="form_empty_position">
                {{ trans('form.empty') }}
            </div>
        @endif
    </div>

</div>