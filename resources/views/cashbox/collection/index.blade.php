@extends('layouts.app')

@php
    if(stripos(Request::path(), 'operation_list') == false)
    {
        $toggle = false; // Не содержит operation_list
    }else{
        $toggle = true; // Содержит operation_list
    }
@endphp

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">

@append

@section('content')


    <div class="page-heading">
        <h2>{{$toggle ? 'Список операций' : 'Инкассация'}}

            @if(!$toggle)
                <a class="btn btn-success pull-right" href="{{url('/cashbox/collection/0/edit')}}">Добавить</a>
            @endif

        </h2>
    </div>

    <br/>

    <div class="divider"></div>
    <br/>


    @if(sizeof($cashbox))
        <table class="tov-table">
            <thead>
            <tr>
                <th><a href="#">Название</a></th>
                <th><a href="#">Актуально</a></th>
                <th><a href="#">Ответственный</a></th>
                <th><a href="#">Баланс</a></th>
                <th><a href="#">Максимальный баланс</a></th>
            </tr>
            </thead>
            @foreach($cashbox as $box)
                <tr  @if($box->max_balance > 0 && $box->max_balance < $box->balance) class="bg-red" @endif
                        onclick="window.location='{{$toggle ? url("/cashbox/operation_list/{$box->id}/show") : url("/cashbox/collection/{$box->id}/edit")}}'">
                    <td>{{ $box->title }}</td>
                    <td>{{ ($box->is_actual==1)? trans('form.yes') :trans('form.no') }}</td>
                    <td>{{ $box->user?$box->user->name:'' }}</td>
                    <td>{{ titleFloatFormat($box->balance) }}</td>
                    <td>{{ titleFloatFormat($box->max_balance) }}</td>
                </tr>
            @endforeach

        </table>
    @else
        {{ trans('form.empty') }}
    @endif



@endsection

@section('js')

    <script>


        $(function () {


        });

    </script>


@endsection