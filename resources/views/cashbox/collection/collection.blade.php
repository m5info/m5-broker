@extends('layouts.frame')


@section('title')

    Касса #{{$cashbox->id}} {{ $cashbox->title }}

@endsection

@section('content')


    {{ Form::open(['url' => url("/cashbox/collection/{$cashbox->id}/set_collection"), 'method' => 'post', 'class' => 'form-horizontal']) }}

    <input type="hidden" name="type_id" value="1"/>

    <div class="form-group">
        <div class="col-sm-12">
            <label style="float: left; padding-left: 15px">
                Текущий баланс: {{getPriceFormat($cashbox->balance)}}
            </label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="col-sm-12 control-label">Сумма</label>
            <div class="col-sm-12">
                {{ Form::text('total_sum', '', ['class' => 'form-control sum', 'required']) }}
            </div>
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-12">
            <label class="col-sm-12 control-label">Комментарий</label>
            <div class="col-sm-12">
                {{ Form::textarea('purpose_payment', '', ['class' => 'form-control ', 'required']) }}
            </div>
        </div>
    </div>


    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection




@section('js')

    <script>




        $(function () {


        });




    </script>


@endsection

