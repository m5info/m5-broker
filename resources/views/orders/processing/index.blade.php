@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">
@append

@section('content')


    <div class="page-heading">
        <h2>Заявки на оформление
            <div class="pull-right">
                <span class="btn btn-primary btn-right" onclick="openFancyBoxFrame('{{url("/orders/processing/bso-supplier-product/0")}}')">
                    <i class="fa fa-plus"></i>Создать
                </span>
            </div>
        </h2>
    </div>

    <div class="header_bab">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @if(sizeof($count_arr))
                <div id="tt" class="easyui-tabs" data-options="tools:'#tab-tools'">
                    @foreach($count_arr as $key => $count)
                        <div title="{{$count['title']}} {{($count['count']>0)?$count['count']:''}}" id="tab-{{$key}}" data-view="{{$key}}"></div>
                    @endforeach
                </div>
            @else
                У вас нет доступных вкладок в этом разделе
            @endif
        </div>
    </div>

    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -5px;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="filter-group">

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label" for="bso_title">БСО</label>
                        {{ Form::text('bso_title', request('bso_title', ''), ['class' => 'form-control',  'onkeyup'=>'loadData()']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label" for="insurer">Страхователь</label>
                        {{ Form::text('insurer', request('insurer', ''), ['class' => 'form-control',  'onkeyup'=>'loadData()']) }}
                    </div>


                    <div class="btn-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <label class="control-label">Агент/Курьер</label>
                        @include('partials.elements.users', ['title'=>'agent_id', 'select'=> request('agent', 0), 'array' => ['onchange' => 'loadData()']])
                    </div>

                    <div class="btn-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <label class="control-label">Подразделение</label>
                        {{ Form::select('department_ids[]', \App\Models\Settings\Department::all()->pluck('title', 'id'), request('department_ids', ''), ['class' => 'form-control select2-all', 'multiple' => true, 'onchange' => 'loadData()']) }}
                    </div>


                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label" for="conclusion_date_from">Город</label>
                        @include('orders.order.partials.cities_select', ['inspection' => null])
                    </div>



                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label" for="conclusion_date_from">Дата доставки с</label>
                        {{ Form::text('conclusion_date_from', request('conclusion_date_from', ''), ['class' => 'form-control datepicker date',  'onchange'=>'loadData()']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label" for="conclusion_date_to">Дата доставки по</label>
                        {{ Form::text('conclusion_date_to', request('conclusion_date_to', ''), ['class' => 'form-control datepicker date',  'onchange'=>'loadData()']) }}
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="conclusion_date_to">Кол-во на стр</label>
                                {{Form::select('page_count', collect([25=>'25', 50=>'50', 100=>'100', 150=>'150']), request()->has('page')?request()->session()->get('page'):25, ['class' => 'form-control select2-ws', 'id'=>'page_count', 'onchange'=>'loadData()'])}}
                            <br/>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                                <span id="view_row">0</span>/<span id="max_row">0</span>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container" style="margin-top: -5px;overflow: auto;"></div>
        </div>
    </div>


    <div class="row">
        <div id="page_list" class="easyui-pagination pull-right"></div>
    </div>



@endsection



@section('js')

    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>



    <script>

        var map;
        var TAB_INDEX = 0;
        var PAGE = 1;

        $(function () {

            if($('[data-view]').length > 0){
                $('#tt').tabs({
                    border:false,
                    pill: false,
                    plain: true,
                    onSelect: function(title, index){
                        return selectTab(index);
                    }
                });
                selectTab(0);
            }
        });



        function selectTab(id) {

            TAB_INDEX = id;

            loaderShow();

            $.post("{{url("/orders/processing/get_table")}}", getData(), function (response) {

                $("#main_container").html(response.result);

                $('#view_row').html(response.view_row);
                $('#max_row').html(response.max_row);

                $('#page_list').pagination({
                    total: response.page_max,
                    pageSize: 1,
                    pageNumber: PAGE,
                    layout: ['first', 'prev', 'links', 'next', 'last'],
                    onSelectPage: function (pageNumber, pageSize) {
                        setPage(pageNumber);
                    }
                });

                initFormTab();

            }).always(function() {
                loaderHide();
            });

        }

        function setPage(field) {
            PAGE = field;
            loadData();
        }

        function loadData() {
            selectTab(TAB_INDEX);
        }

        function get_executors() {
            selectTab(TAB_INDEX);
        }

        function getData(){

            var tab = $('#tt').tabs('getSelected');
            var load = tab.data('view');//$("#tab-"+id).data('view');

            return {
                status: load,
                sk: $('[name="sk"]').val(),
                bso_title: $('[name="bso_title"]').val(),
                product: $('[name="product"]').val(),
                insurer: $('[name="insurer"]').val(),
                department_ids: $('[name="department_ids[]"]').val(),
                agent: $('[name="agent_id"]').val(),
                conclusion_date_from: $('[name="conclusion_date_from"]').val(),
                conclusion_date_to: $('[name="conclusion_date_to"]').val(),
                city: $('#cities').val(),
                page_count: $('#page_count').val(),
                page:PAGE,
            }

        }




    </script>


@endsection