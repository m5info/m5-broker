@extends('layouts.frame')

@section('title')

    Доставка объеденение заявок


@endsection

@section('content')

    {{ Form::open(['url' => url('/orders/processing/combine'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    <input type="hidden" name="orders_arr" value="{{$orders_arr}}"/>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Основная</th>
            <th>Страхователь</th>
            <th>Менеджер/Агент</th>
            <th>Доставка</th>
            <th>Метро</th>
            <th>Адрес</th>
        </tr>
        </thead>
        <tbody>
        @if(sizeof($orders))
            @foreach($orders as $order)
                <tr>
                    <td><input type="radio" value="{{$order->id}}" name="active"/> </td>
                    <td>{{$order->main_contract_id > 0 ? $order->main_contract->insurer->title : ""}}</td>
                    <td>{{ ($order->manager)? $order->manager->name : '' }}</td>
                    <td>{{setDateTimeFormatRu($order->delivery_date, 1)}} {{$order->delivery_time}}</td>
                    <td>{{ $order->delivery_metro }}</td>
                    <td>{{ $order->address }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="13" class="text-center">Нет записей</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{Form::close()}}

@endsection

@section('footer')


    <button onclick="submitForm()" type="submit" class="btn btn-primary">Объединить</button>

@endsection

