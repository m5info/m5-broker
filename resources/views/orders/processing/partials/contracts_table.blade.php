<table class="tov-table table  table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>Страхователь</th>
        <th>Продукт</th>
        <th>СК</th>
        <th>Менеджер/Агент</th>
        <th>Доставка</th>
        <th>Город</th>
        <th>Метро</th>
        <th>Адрес</th>
        <th>Оформитель</th>
        <th>Коментарий</th>
        <th>Действие</th>
    </tr>
    </thead>
    <tbody>
    @if(sizeof($orders))
        @foreach($orders as $order)
            <tr {{$order->urgency ? 'class=bg-red' : ''}}>
                <td>{{$order->id}}</td>

                <td>{{$order->main_contract_id > 0 ? $order->main_contract->insurer->title : ""}}</td>

                <td>{{$order->main_contract->product->title}}</td>
                <td>{{$order->main_contract->insurance_companies->title}}</td>


                <td>{{ ($order->manager)? $order->manager->name : '' }}</td>
                <td>{{setDateTimeFormatRu($order->delivery_date, 1)}} {{$order->delivery_time}}</td>

                <td>{{ ($order->delivery_city)?$order->delivery_city->title : '' }}</td>
                <td>{{ $order->delivery_metro }}</td>
                <td>{{ $order->address }}</td>

                <td>{{($order->main_contract->check_user)?$order->main_contract->check_user->name:''}}</td>

                <td>{{ $order->delivery_comment }}</td>

                <td>
                    @if($tabs_visibility[$order->status_id]['edit'])
                        @if($order->status_id == 0)
                            <a class="btn btn-danger btn-right" href="{{url("contracts/orders/edit/{$order->id}/delete")}}">
                                Удалить
                            </a>
                        @endif

                    @endif


                    <a class="btn btn-success btn-left" href="{{url("/orders/processing/contract/{$order->main_contract_id}/")}}">
                        <i class="fa fa-eye"></i>
                    </a>

                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="13" class="text-center">Нет записей</td>
        </tr>
    @endif
    </tbody>
</table>



<script>
    
    function initFormTab() {
        
    }
    
</script>