@extends('layouts.frame')

@section('title')

    Доставка назначения курьера


@endsection

@section('content')

    {{ Form::open(['url' => url('/orders/processing/assign'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    <input type="hidden" name="orders_arr" value="{{$orders_arr}}"/>


    <div class="form-group">
        <label class="col-sm-4 control-label">Курьер</label>
        <div class="col-sm-8">
            @include('partials.elements.users', ['title'=>'courier', 'select'=> null])
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Стоимость</label>
        <div class="col-sm-8">
            {{Form::select('departures_courier_price_id', \App\Models\Settings\DeparturesCourierPrice::getDeparturesCourierPrice(request('city'))->pluck('title','id'), 0, ['class' => 'form-control select2-ws'])}}
        </div>
    </div>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Страхователь</th>
            <th>Менеджер/Агент</th>
            <th>Доставка</th>
            <th>Метро</th>
            <th>Адрес</th>
        </tr>
        </thead>
        <tbody>
        @if(sizeof($orders))
            @foreach($orders as $order)
                <tr>
                    <td>{{$order->id}}</td>
                    <td>{{$order->main_contract_id > 0 ? $order->main_contract->insurer->title : ""}}</td>
                    <td>{{ ($order->manager)? $order->manager->name : '' }}</td>
                    <td>{{setDateTimeFormatRu($order->delivery_date, 1)}} {{$order->delivery_time}}</td>
                    <td>{{ $order->delivery_metro }}</td>
                    <td>{{ $order->address }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="13" class="text-center">Нет записей</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{Form::close()}}

@endsection

@section('footer')


    <button onclick="submitForm()" type="submit" class="btn btn-primary">Назначить</button>

@endsection

