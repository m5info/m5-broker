<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >



    <div class="row">
        <span class="btn btn-primary btn-left" id="buttonCombine" onclick="setCombine()">
            Объединить
        </span>
        <span class="btn btn-success btn-right" id="buttonAssign" onclick="setAssign()">
            Назначить
        </span>
    </div>

    <h3>Распределение</h3>
    @if(count($orders))
        <table class="table table-striped table-bordered table_for_yamap">
            @foreach($orders as $order)
                <tr onclick="go_point({{$order->id}})" data-id="{{$order->id}}" data-geo_lat="{{$order->address_width}}" data-geo_long="{{$order->address_longitude}}" data-title="Заявка #{{$order->id}} {{setDateTimeFormatRu($order->delivery_date, 1)}} {{$order->delivery_time}}" data-coment="{{$order->address}} - {{$order->delivery_comment}}">
                    <td><input type="checkbox" class="events_orders" value="{{$order->id}}"/> </td>
                    <td>{{setDateTimeFormatRu($order->delivery_date, 1)}} {{$order->delivery_time}}</td>

                    <td><a href="{{url("/orders/processing/order/{$order->id}")}}">#{{ $order->id }}</a></td>
                    <td>{{$order->main_contract_id > 0 ? $order->main_contract->insurer->title : ""}}</td>
                    <td>{{ ($order->manager)? $order->manager->name : '' }}</td>

                    <td>{{ $order->delivery_metro }}</td>
                    <td>{{ $order->address }}</td>
                    <td>{{ $order->delivery_comment }}</td>
                </tr>
            @endforeach
        </table>

    @endif

</div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
    @include("orders.order.partials.map")
</div>


<script>

    var map;

    function initFormTab() {
        initMaps();

        $("#buttonCombine").hide();
        $("#buttonAssign").hide();

        $(".events_orders").click(function () {
            activeButtonEvents();
        });

        setTimeout(function () {
            activePoints();
        }, 500);
    }



    function setEvents()
    {

    }

    function getOrderEvents(){
        arr = [];
        $(".events_orders").each(function (index, value){
            if($(this).is(':checked')){
                arr.push($(this).val());
            }
        });
        return arr;
    }

    function activeButtonEvents()
    {
        $("#buttonCombine").hide();
        $("#buttonAssign").hide();

        arr = getOrderEvents();
        if(arr.length > 1){
            $("#buttonCombine").show();

        }

        if(arr.length > 0){
            $("#buttonAssign").show();
        }

    }

    function setCombine()
    {
        orders = JSON.stringify(getOrderEvents());
        openFancyBoxFrame("{{url("/orders/processing/combine")}}?orders="+orders);
    }

    function setAssign()
    {
        orders = JSON.stringify(getOrderEvents());
        openFancyBoxFrame("{{url("/orders/processing/assign")}}?city="+$( "#cities").val()+"&orders="+orders);
    }




    function activePoints() {
        var tr = $('.table_for_yamap tr');
        tr.each(function (index, value){

            setPoinsMap($(this).data('id'), $(this).data('geo_lat'), $(this).data('geo_long'), $(this).data('title'), $(this).data('coment'));
            
            
        });
    }
    
    function setPoinsMap(id, geo_lat, geo_long, title, coment)
    {
        myGeoObject_e = new ymaps.GeoObject({
            geometry: {
                type: "Point",// тип геометрии - точка
                coordinates: [geo_lat, geo_long] // координаты точки
            },
            properties: {
                balloonContentHeader: title,
                balloonContentBody: coment,
                id: id
            }
        });

        myGeoObject_e.events.add('click', function (e) {
            // Метка, на которой сработало событие
            var params = e.get('target').properties._data;
            set_tr(params.id);
        });

        map.geoObjects.add(myGeoObject_e);
    }


    function set_tr(id){
        var tr = $('.table_for_yamap tr');


        tr.each(function (index, value){
            if ($(this).data('id') == id) {
                $(this).addClass('choosen_yamap');

            }else{
                $(this).removeClass('choosen_yamap');
            }
        });
    }

    function go_point(id){
        map.geoObjects.each(function (item) {

            if (item.properties.get('id') == id) {
                var coord = item.geometry.getCoordinates();

                map.panTo([parseFloat(coord[0]), parseFloat(coord[1])], {
                    // Задержка между перемещениями.
                    delay: 1500
                });
                item.balloon.open();
            }
        });

        set_tr(id);

    }



</script>

<style>
    .block-view{
        padding: 20px 35px 35px 35px;
    }
    .contract-block{
        display: block;
        padding: 15px 15px 15px 15px;
        border: 1px solid #e4e4e4;
        margin-bottom: 11px;
        margin-top: 10px;
        border-radius: 8px;
        background-color: #fff;
        box-shadow: 0 5px 20px rgba(0, 0, 0, .15);
    }
    .contract-block div{
        color: #7d7f81;
        font-family: "AgoraSansProRegular", "serif";
        font-size: 14px;
        font-weight: normal;
    }
    .contract-block:hover,
    .contract-block:visited,
    .contract-block:link,
    .contract-block:active
    {
        text-decoration: none;
    }
    .choosen_yamap {
        background: #d9ebc6 !important;
    }
</style>