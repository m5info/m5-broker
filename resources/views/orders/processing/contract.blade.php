@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">
@append


@section('work-area')
    @include("orders.processing.contract.partials.workarea", ["contract"=>$contract, "order"=>$contract->order, 'temp'=>$temp])
@append

@section('content')


    @include("orders.processing.contract.{$temp}.index", ["contract"=>$contract])



@endsection





@include("orders.processing.contract.js", ["contract"=>$contract])


@section('pusher')

    <script>

        var channelChat = pusher.subscribe('chat-contract-{{ $contract->id }}');

        channelChat.bind('new-message', function (data) {
            addMessage(data);
            readMessage();
            readedMessages();
        });

        channelChat.bind('new-event-view', function (data) {

            if(data.event == 'reload')
            {
                reload();
            }

            if(data.event == 'add-file')
            {
                addFileContract(data.id);
            }

            if(data.event == 'delete-file')
            {

                $("#document-"+data.id).remove();
                activeDocuments();
            }


        });

    </script>

@append

@include('partials.pusher')