@extends('layouts.frame')


@section('title')

    Полис

@endsection

@section('content')


    {{ Form::open(['url' => url('/orders/processing/create'), 'method' => 'post', 'class' => 'form-horizontal', 'id'=>'myFormData']) }}

    <input type="hidden" name="id" value="{{$id}}"/>



    <div class="form-group">
        <label class="col-sm-4 control-label">Договор (полис)</label>
        <div class="col-sm-8">
            {{ Form::text('bso_title', $contract->bso_title, ['class' => 'form-control', 'id'=>'bso_title']) }}
            <input type="hidden" name="bso_id" id="bso_id" value="{{$contract->bso_id}}"/>
            <input type="hidden" name="insurance_companies_id" id="insurance_companies_id" />
            <input type="hidden" name="agent_id" id="agent_id" value="{{$contract->agent_id}}"/>
        </div>
    </div>



    <div class="form-group">
        <label class="col-sm-4 control-label">СК</label>
        <div class="col-sm-8">
            {{ Form::select('bso_supplier_id', $bso_suppliers->pluck('title', 'id')->prepend('Выберите страховщика', 0), $contract->bso_supplier_id, ['class' => 'form-control select2-all', 'id'=>'bso_supplier_id', "onchange"=>"getSuppliersProduct()"]) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label" >Продукт</label>
        <div class="col-sm-8">
            {{ Form::select('product_id', collect([]), '', ['class' => 'form-control', 'id'=>'product_id']) }}
        </div>
    </div>

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="createBsoAndContract()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection




@section('js')

    <script>




        function selectBso(object_id, key, type, suggestion)
        {

            var data = suggestion.data;

            if(parseInt(type) == 1){ // БСО
                $('#bso_id'+key).val(data.bso_id);
                $('#bso_supplier_id'+key).select2('val', data.bso_supplier_id);
                $('#insurance_companies_id'+key).val(data.insurance_companies_id);
                $('#product_id'+key).val(data.product_id);
                $('#agent_id'+key).val(data.agent_id);

            }

        }




        $(function () {



            activSearchBsoOrders("bso_title", '', 1, '{{$contract->order_form_id}}');




            $('#bso_title').click(function () {
                controlValid("bso_title", 0);
            });

            $('#bso_type_id').change(function () {
                controlValid($(this).attr("id"), 0);
            });



            getSuppliersProduct();

        });


        function getSuppliersProduct()
        {
            product_id = parseInt("{{$contract->product_id}}");

            bso_supplier_id = $("#bso_supplier_id").val();

            if(parseInt(bso_supplier_id)>0){

                $.getJSON('{{url('/bso/actions/get_bso_product/')}}', {bso_supplier_id:bso_supplier_id}, function (response) {

                    var options = "<option value='0'>Не выбрано</option>";
                    response.map(function (item) {
                        options += "<option value='" + item.product_id + "'>" + item.title + "</option>";
                    });

                    $("#product_id").html(options).val(product_id);
                    controlValid("product_id", product_id);

                });

            }
        }




        function createBsoAndContract()
        {
            bso_supplier_id = $('#bso_supplier_id').val();
            product_id = $("#product_id").val();

            if(parseInt(bso_supplier_id) > 0){

                if(parseInt(product_id) > 0){

                    $("#myFormData").submit();

                }else{
                    controlValid("product_id", 1);
                    return;
                }
            }else{
                controlValid("bso_supplier_id", 1);
                return;
            }
        }


        function controlValid(element_id, state)
        {
            if(state == 1){
                $("#"+element_id).css("border-color","red");
            }else{
                $("#"+element_id).css("border-color","");
            }
        }




    </script>


@endsection

