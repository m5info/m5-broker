<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

@php

    $order = $contract->order;


@endphp





        <div class="page-subheading">



            <h2 class="pull-left">
                    Доставка
                    {{ Form::checkbox('order[0][delivery][is_delivery]', 1, (isset($order->is_delivery) && $order->is_delivery == 1) ? 1 : 0,['style' => 'width:18px;height:18px;margin-left:5px;position:absolute;', 'id' => 'is_delivery']) }}
            </h2>

            <h2 class="pull-right">
                Срочно
                {{ Form::checkbox('order[0][urgency]', 1, $order->urgency ? 1 : 0) }}
            </h2>


        </div>

        <div class="is_delivery">

            <div class="block-main">
                <div class="block-sub">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">
                            <div>
                                <div class="col-lg-4">
                                    <div class="field form-col">
                                        <label class="control-label">Дата</label>
                                        @php
                                            $date = setDateTimeFormatRu($order->delivery_date, 1);
                                            $time = $order->delivery_time;
                                        @endphp
                                        {{ Form::text('order[0][delivery][date]', $date, ['class' => 'form-control format-date datepicker date']) }}
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="field form-col">
                                        <label class="control-label">Время</label>
                                        {{ Form::text('order[0][delivery][time]', ($time) ? $time : "12:00", ['class' => 'form-control format-time']) }}
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="field form-col">
                                        <label class="control-label">Контактный телефон</label>
                                        {{ Form::text('order[0][delivery][delivery_phone]', $order->delivery_phone, ['class' => 'form-control format-date phone']) }}
                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-12">
                                <label class="control-label">Город</label>
                                {{ Form::select('order[0][delivery][city_id]', \App\Models\Settings\City::where('is_actual', '=', 1)->get()->pluck('title', 'id'), $order->delivery_city_id, ['class' => 'form-control select2']) }}
                            </div>
                            <div class="col-lg-12">
                                <label class="control-label">Метро</label>
                                {{ Form::text('order[0][delivery][metro]', $order->delivery_metro, ['class' => 'form-control', 'id' => 'delivery_metro']) }}
                            </div>
                            <div class="col-lg-12">
                                <label class="control-label">Адрес</label>
                                {{ Form::text('order[0][delivery][address]', $order->address, ['class' => 'form-control', 'id' => 'delivery_address']) }}
                                {{ Form::hidden('order[0][delivery][address_kladr]', $order->address_kladr, ['id' => 'address_kladr']) }}
                                {{ Form::hidden('order[0][delivery][address_width]', $order->address_width, ['id' => 'address_width']) }}
                                {{ Form::hidden('order[0][delivery][address_longitude]', $order->address_longitude, ['id' => 'address_longitude']) }}
                            </div>
                            @if($order->status_id >= 3)
                                <div class="col-lg-12">
                                    <div class="field form-col">
                                        <label class="control-label">Курьер</label>
                                        @include('partials.elements.users', ['title'=>'order[0][delivery][courier]', 'select'=> $order->delivery_user_id])
                                    </div>
                                </div>
                            @endif
                            <div class="col-lg-12">
                                <label class="control-label">Комментарий к доставке</label>
                                {{ Form::textarea('order[0][delivery][comment]', $order->delivery_comment, ['class' => 'form-control']) }}
                            </div>
                            @if($order->delivery_user_id && $order->status_id >= 3)
                                <div class="col-lg-12">
                                    <span class="btn btn-success btn-right" onclick="RunForestRun({{$order->status_id}});">
                                        Выезд курьера
                                    </span>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>




</div>

<span class="btn btn-primary btn-right" onclick="saveTab()">
    Сохранить
</span>


<script src="/js/jquery.datetimepicker.full.min.js"></script>
<link rel="stylesheet" href="/css/jquery.datetimepicker.min.css">

<script>

    function initTab()
    {

        activeDelivery();

    }

    function saveTab()
    {
        saveContractData();
    }


</script>


