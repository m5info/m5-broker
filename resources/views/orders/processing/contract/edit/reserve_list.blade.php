<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

    @include("orders.processing.contract.partials.reserve_list", ['temp' => 'edit', 'order'=>$contract->order])

</div>


<script>

    function initTab()
    {


        $("#cart_tab_create").on('click', function () {

            var bso_cart_id = createCart();
            $('#bso_cart_id').val(bso_cart_id);
            saveContractData();
            selectTab(TAB_INDEX);
            return;
        });



        $(function () {

            $(document).on('click', '.reserve_acts', function(){
                href = $(this).data('href');
                window.open(href, "_blank");
            });


            $("#bso_cart_type").on('change', function () {
                show_hide_controls($(this).val(), 0);
            });

            openViewCar();

            $('#user_id_to').on('change', function () {
                getUserInfo($(this).val());
            });

            $(document).on(
                "click",
                ".unban_user",
                function () {
                    if ($(this).prop("checked")) {
                        $('#cart_create').show();
                    }
                    else {
                        $('#cart_create').hide();
                    }
                }
            );


            $("#cart_create").on('click', function () {
                createCart();
            });

            $('#sk_user_id').on('change', function () {
                $("#bso_type_id").html(myGetAjax('/bso/transfer/get_bso_types/?sk_user_id=' + $(this).val()));
            });

            $(document).on(
                "click",
                ".cb_left_column_style",
                function () {

                    if ($(this).prop("checked")) {
                        $('.tr_type_selector').hide();
                        $('.button_sk_selector').show();
                        $('#bsos').hide();
                        $('#rit_bsos').show();
                        $('.save_transmit_bso').show();
                        document.cookie = "bso_transfer_left_column_style=1";
                    }
                    else {
                        $('.tr_type_selector').show();
                        $('.button_sk_selector').hide();
                        $('#bsos').show();
                        $('#rit_bsos').hide();
                        $('.save_transmit_bso').hide();
                        document.cookie = "bso_transfer_left_column_style=0";
                    }

                }
            );


            $(document).on(
                "click",
                ".button_sk_selector",
                function () {


                    if ($('.bso_table[sk_user_id=' + $('#sk_user_id').val() + ']').length == 0) {
                        res = myGetAjax('/bso/transfer/rit_bso_selector/?sk_user_id=' + $('#sk_user_id').val());
                        $('#rit_bsos').append(res);
                    }
                }
            );

            $(document).on(
                "click",
                ".remove_string_button",
                function () {

                    $(this).parent().parent().remove();
                }
            );

            $(document).on(
                "click",
                ".remove_sk_button",
                function () {

                    $(this).parent().remove();
                }
            );

            $(document).on(
                "click",
                ".add_string_button",
                function () {

                    var curr_index = $('#rit_bsos .bso_table > tbody > tr').length;

                    var sk_user_id = $(this).attr('sk_user_id');
                    // Создаем элемент
                    var el = $('<tr>', {
                        class: 'table_row row_'+curr_index,
                        completed: '0',
                        'data-index': curr_index
                    });

                    var new_tr = $('.new_tr[sk_user_id=' + sk_user_id + ']').val();
                    var new_tr = new_tr.replace(/:KEY:/g, curr_index);

                    el.html(new_tr);

                    // Помещаем в таблицу
                    $('.bso_table[sk_user_id=' + sk_user_id + ']').append(el);


                });



            $(document).on(
                "change",
                ".type_selector",
                function () {

                    selectBsoType(this);

                }
            );


            $(document).on(
                "blur",
                ".bso_number",
                function () {
                    selectBsoNumber(this);
                }
            );

            $(document).on(
                "blur",
                ".bso_qty",
                function () {

                    selectBsoQty(this);
                }
            );


            $("#bso_type_id").on('change', function () {
                getBsosList($(this).val());
            });

            $("#bso_state_id").on('change', function () {
                getBsosList($("#bso_type_id").val());
            });

            $(document).on(
                "click",
                "#check_all",
                function () {
                    if ($(this).prop("checked")) {
                        $(".cb_bso").prop("checked", true);
                    }
                    else {
                        $(".cb_bso").prop("checked", false);
                    }
                }
            );


            $(document).on(
                "click",
                "#move_to_cart",
                function () {

                    var bsos = '';
                    $('.cb_bso:checked').each(function () {
                        bsos += $(this).attr('bso_id') + ',';
                    });

                    //Резервируем бсо
                    var res = myGetAjax('/bso/transfer/move_to_cart/?bso_cart_id=' + $("#bso_cart_id").val() + '&bsos=' + bsos);

                    //Обновляем левую корзину
                    getBsosList($("#bso_type_id").val());

                    //Обновляем правую корзину
                    openViewCar();
                }
            );


            $(document).on(
                "click",
                ".cb_right_column_style",
                function () {

                    if ($(this).prop("checked")) {
                        $('#group_by_items').hide();
                        $('#group_by_types').show();
                        document.cookie = "bso_transfer_right_column_style=1";
                    }
                    else {
                        $('#group_by_items').show();
                        $('#group_by_types').hide();
                        document.cookie = "bso_transfer_right_column_style=0";
                    }

                }
            );


            $(document).on(
                "click",
                ".remove_button",
                function () {
                    var bso_id = $(this).attr('bso_id');
                    removeBsosCar(bso_id, 0);
                }
            );

            $(document).on(
                "click",
                ".remove_type_button",
                function () {
                    var bso_type_id = $(this).attr('type_id');
                    removeBsosCar(0, bso_type_id);
                }
            );


            $(document).on(
                "click",
                ".save_transmit_bso",
                function () {
                    var errors_qty = 0;
                    $.each($('.table_row[completed=0]'), function () {

                        $(this).find('.error_div').html('');
                        $(this).find('td:nth-child(2)').children('.error_div').html('');

                        obj = new Object();
                        obj.sk_user_id = $(this).find('.sk_user_id').val();
                        obj.type_id = $(this).find('.type_selector').val();
                        obj.serie_id = $(this).find('.series_selector').val();
                        obj.bso_qty = $(this).find('.bso_qty').val();
                        obj.number = $(this).find('.bso_number').val();
                        obj.tp_id = $("#tp").val();
                        obj.bso_cart_id = $("#bso_cart_id").val();
                        res = myGetAjax('/bso/transfer/rit_bso_transfer/?obj=' + JSON.stringify(obj));

                        //res = JSON.parse(r);
                        if (res.error_state == 1) {
                            $(this).find('td:nth-child(' + res.error_attr + ')').children('.error_div').html(res.error_title);
                            $(this).children().css('background-color', '#FFEEEE');
                            errors_qty++;
                        }
                        else {
                            $(this).attr('completed', 1);
                            $(this).find('select').prop('disabled', true);
                            $(this).find('input').prop('disabled', true);
                            if (res.error_state != 2) $(this).children().css('background-color', '#EEFFEE');
                        }

                    });

                    if (errors_qty > 0) {
                        alert('Перемещены не все БСО!');
                    }

                    openViewCar();

                }
            );



            $("#b_transfer_bso").on('click', function () {

                if (!confirm('Подвердите передачу БСО')) return false;

                var bso_cart_id = $("#bso_cart_id").val();
                var res = myGetAjax('/bso/transfer/transfer_bso/?bso_cart_id=' + bso_cart_id);

                if (res == 'empty cart') {
                    alert('В корзине отсутствуют БСО.');
                    return false;
                }

                reload();

            });


            $("#show_all_bsos").on('click', function () {
                var user_id_from = $('#user_id_from').val();
                var bso_state_id = $('#bso_state_id').val();
                $('#bsos').html(myGetAjax('/bso/transfer/get_all_bsos/?user_id_from=' + user_id_from + '&bso_state_id=' + bso_state_id));

            });


        });

    }

    function saveTab()
    {

    }

</script>