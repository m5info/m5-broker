<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <table class="table table-striped table-bordered">
        <th>Дата</th>
        <th>Назначил</th>
        <th>Статус</th>
        @if($contract->order_log_states)
            @foreach($contract->order_log_states as $log)

                <tr>
                    <td>{{ setDateTimeFormatRu($log->date_sent) }}</td>
                    <td>{{ $log->user ? $log->user->name : '' }}</td>
                    <td>{{ $log->text }}</td>

                </tr>
            @endforeach
        @endif
    </table>
</div>


<script>

    function initTab()
    {

    }

    function saveTab()
    {

    }

</script>