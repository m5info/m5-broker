@extends('layouts.app')

@section('head')

@append

@section('content')



    {{Form::open(['url' => url("/orders/processing/contract/{$contract->id}/save"), 'id' => 'formData', 'class' => 'form-horizontal'])}}


    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >




        <!-- Условия договора -->
        @include("orders.processing.contract.partials.terms.edit")

        <!-- Платежи -->
        @include("contracts.contract_templates.default.payments.edit_orders")


    </div>

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-top:15px;">
        @include("orders.processing.contract.partials.information_tabs", ["contract"=>$contract])
    </div>

    {{Form::close()}}




@endsection


@section('js')

    <script src="/js/bso/transfer.js"></script>
    @include('contracts.contract_templates.default.js')


    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


    <script>



        $(function () {

            $('.class_bso_receipt').each(function () {
                key = $(this).attr("id").split('_');
                activSearchBsoOrders("bso_receipt", "_" + key[2], 2, '{{$contract->order_form_id}}');

            });


            @if(!$contract->check_user_id && $contract->order->status_id == 1)

                Swal.fire({
                title: 'Вы будете оформлять договор?',
                text: "",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Да',
                cancelButtonText: 'Нет'
                }).then((result) => {
                    if (result.value) {

                        myGetAjax("/contracts/actions/contract/{{$contract->id}}/set-check-user?user_id={{auth()->id()}}")

                    }
                });

            @endif


        });





        function saveContractData(status)
        {
            $.post('{{url("/orders/processing/contract/{$contract->id}/save")}}', $('#formData').serialize(), function (response) {

                if (parseInt(response.state) === 0) {
                    //flashMessage('success', "Данные успешно сохранены!");

                    if(parseInt(status) >= 0){
                        setStatus(status);
                    }


                } else {
                    flashHeaderMessage("Ошибка", 'danger');
                }

            }).always(function () {

            });


        }

        function setStatus(status)
        {
            $.post('{{url("/orders/processing/contract/{$contract->id}/status-to-change")}}', {status: status}, function (response) {
                if (parseInt(response.state) === 1) {
                    reload();
                } else {
                    $.each(response.msg, function (index, value) {
                        flashHeaderMessage(value, 'danger');
                    });
                }
            }).always(function () {

            });

        }

        function deleteOrder()
        {
            $.post('{{url("/orders/processing/contract/{$contract->id}/delete-order")}}', {}, function (response) {
                if (parseInt(response.state) === 0) {
                    window.location = "/orders/processing/";
                } else {
                    flashHeaderMessage(response.msg, 'danger');
                }
            }).always(function () {
            });

        }

        function cloneContract()
        {
            $.post('{{url("/orders/processing/contract/{$contract->id}/clone")}}', {}, function (response) {
                if (parseInt(response.state) === 0) {
                    window.location = response.msg;
                }else {
                    flashHeaderMessage(response.msg, 'danger');
                }
            }).always(function () {

            });
        }


    </script>


@endsection