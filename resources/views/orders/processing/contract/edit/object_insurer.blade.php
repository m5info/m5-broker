<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <!-- Страхователь -->
    @include("contracts.contract_templates.default.insurer.edit")

    <!-- Объект страхования -->
    @include("contracts.contract_templates.default.insurance_object.edit")

    <span class="btn btn-primary btn-right" onclick="saveTab()">
        Сохранить
    </span>

</div>


<script>

    function initTab()
    {
        activeObjectInsurer('{{$contract->product?1:0}}');
    }

    function saveTab()
    {
        saveContractData();
    }






</script>