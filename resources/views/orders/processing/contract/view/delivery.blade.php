
@php

    $order = $contract->order;

@endphp


    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="block-view">
            <div class="block-sub">
                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">Доставка</span>
                            <span class="view-value">{{(isset($order->is_delivery) && $order->is_delivery == 1) ? "Да" : "Нет"}}</span>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">Срочно</span>
                            <span class="view-value">{{(isset($order->urgency) && $order->urgency == 1) ? "Да" : "Нет"}}</span>
                        </div>
                    </div>

                    @if(isset($order->is_delivery) && $order->is_delivery == 1)


                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">Дата</span>
                            <span class="view-value">{{setDateTimeFormatRu($order->delivery_date)}} {{$order->delivery_time}}</span>
                        </div>
                    </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="view-field">
                                <span class="view-label">Контактный телефон</span>
                                <span class="view-value">{{$order->delivery_phone}}</span>
                            </div>
                        </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">Город</span>
                            <span class="view-value">{{$order->delivery_city->title}}</span>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">Метро</span>
                            <span class="view-value">{{$order->delivery_metro}}</span>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">Адрес</span>
                            <span class="view-value">{{$order->address}}</span>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">Курьер</span>
                            <span class="view-value">{{($order->delivery_user)?$order->delivery_user->name:''}}</span>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">Комментарий</span>
                            <span class="view-value">{{$order->delivery_comment}}</span>
                        </div>
                    </div>

                    @endif



                </div>
            </div>
        </div>
    </div>




<script>

    function initTab()
    {


    }

    function saveTab()
    {

    }


</script>


