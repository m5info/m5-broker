@extends('layouts.app')

@section('head')

@append

@section('content')



    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >

        <!-- Информация по договору -->
        @include("contracts.contract_templates.default.contract.view")

        <!-- Платежи -->
        @include("contracts.contract_templates.default.payments.view", ["view_type" => 'all'])


    </div>

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-top:15px;">
        @include("orders.processing.contract.partials.information_tabs", ["contract"=>$contract])
    </div>





@endsection


@section('js')

    <script src="/js/bso/transfer.js"></script>
    @include('contracts.contract_templates.default.js')

    <script>



        $(function () {



        });


        function saveContractData()
        {

        }

        function cloneContract()
        {
            $.post('{{url("/orders/processing/contract/{$contract->id}/clone")}}', {}, function (response) {
                if (parseInt(response.state) === 0) {
                    window.location = response.msg;
                }else {
                    flashHeaderMessage(response.msg, 'danger');
                }
            }).always(function () {

            });
        }


    </script>


@endsection