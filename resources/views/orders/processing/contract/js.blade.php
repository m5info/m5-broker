
<script>

    function activeDocuments() {
        $("a.iframe").fancybox({
            afterShow: function(){
                var click = 1;
                $('.fancybox-wrap').on('click', '#rotate_left', function(){
                    var n = 90 * click++;
                    odd++;

                    $('.fancybox-inner img.fs-galery').css('webkitTransform', 'rotate(-' + n + 'deg)');
                    $('.fancybox-inner img.fs-galery').css('mozTransform', 'rotate(-' + n + 'deg)');

                    var width = $('.fancybox-inner img.fs-galery').width();
                    var height = $('.fancybox-inner img.fs-galery').width();

                    if (odd % 2 == 0) {
                        $('.fancybox-skin').width(width + get_percent_from(width, 10));
                        $('.fancybox-inner').width(width + get_percent_from(width, 10));
                        $('.fancybox-skin').height(height + get_percent_from(width, 60));
                        $('.fancybox-inner').height(height + get_percent_from(width, 60));
                    }else{
                        $('.fancybox-skin').width(width + get_percent_from(width, 10));
                        $('.fancybox-inner').width(width + get_percent_from(width, 10));
                        $('.fancybox-skin').height(height + get_percent_from(width, 10));
                        $('.fancybox-inner').height(height + get_percent_from(width, 10));
                    }
                });

                $('.fancybox-wrap').on('click', '#rotate_right', function(){
                    var n = 90 * click++;
                    odd++;

                    $('.fancybox-inner img.fs-galery').css('webkitTransform', 'rotate(' + n + 'deg)');
                    $('.fancybox-inner img.fs-galery').css('mozTransform', 'rotate(' + n + 'deg)');

                    var width = $('.fancybox-inner img.fs-galery').width();
                    var height = $('.fancybox-inner img.fs-galery').width();

                    if (odd % 2 == 0) {
                        $('.fancybox-skin').width(width + get_percent_from(width, 10));
                        $('.fancybox-inner').width(width + get_percent_from(width, 10));
                        $('.fancybox-skin').height(height + get_percent_from(width, 60));
                        $('.fancybox-inner').height(height + get_percent_from(width, 60));
                    }else{
                        $('.fancybox-skin').width(width + get_percent_from(width, 10));
                        $('.fancybox-inner').width(width + get_percent_from(width, 10));
                        $('.fancybox-skin').height(height + get_percent_from(width, 10));
                        $('.fancybox-inner').height(height + get_percent_from(width, 10));
                    }
                });
            }
        })
    }


    function formatTime() {
        var configuration = {
            timepicker: true,
            datepicker: false,
            format: 'H:i',
            scrollInput: false
        };
        $.datetimepicker.setLocale('ru');
        $('input.format-time').datetimepicker(configuration).keyup(function (event) {
            if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 38 && event.keyCode != 40) {
                var pattern = new RegExp("[0-9:]{5}");
                if (pattern.test($(this).val())) {
                    $(this).datetimepicker('hide');
                    $(this).datetimepicker('show');
                }
            }
        });
        $('input.format-time').each(function () {
            var im = new Inputmask("99:99", {
                "oncomplete": function () {
                }
            });
            im.mask($(this));
        });
    }



    function activeDelivery()
    {
        $('#is_delivery').change( function () {
            if (this.checked) {
                $('.is_delivery').show();
            } else {
                $('.is_delivery').hide();
            }
        });

        $('#is_delivery').change();



        $('#delivery_address').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#address_kladr').val(suggestion.data.kladr_id);
                $('#address_width').val(suggestion.data.geo_lat);
                $('#address_longitude').val(suggestion.data.geo_lon);

                //$('#delivery_metro').val(suggestion.data.metro);

            }
        });

        formatTime();

        $('.phone').mask('+7 (999) 999-99-99');

    }


    function activeObjectInsurer(activeAuto)
    {
        $('#insurer_type_0').change(function () {

            if ($("#insurer_type_owner").is(':checked')) {
                return;
            }


            if (parseInt($(this).val()) == 0) {
                $('.insurer_fl').show();
                $('.insurer_ul').hide();
                $('.insurer_ip').hide();
            } else if (parseInt($(this).val()) == 1) {
                $('.insurer_fl').hide();
                $('.insurer_ul').show();
                $('.insurer_ip').hide();
            } else if (parseInt($(this).val()) == 2) {
                $('.insurer_fl').hide();
                $('.insurer_ul').hide();
                $('.insurer_ip').show();
            }


        });

        $('#insurer_type_0').change();

        $('#insurer_fio_0').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "NAME",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#insurer_title_' + key).val($(this).val());
            }
        });

        KEY = 0;

        $('#insurer_title_' + KEY + ', #insurer_inn_' + KEY + ', #insurer_kpp_' + KEY).suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "PARTY",
            count: 5,
            onSelect: function (suggestion) {
                var data = suggestion.data;

                key = $(this).data('key');


                $('#insurer_title_' + key).val(suggestion.value);
                $('#insurer_inn_' + key).val(data.inn);
                $('#insurer_kpp_' + key).val(data.kpp);

                $('#insurer_fio_' + key).val($('#insurer_title_' + key).val());

            }
        });

        $('.select2-all').select2();

        $('.phone').mask('+7 (999) 999-99-99');

    }



    function getModelsObjectInsurer(KEY, select_model_id) {

        $.getJSON(
            '{{url("/contracts/actions/get_models")}}',
            {categoryId:$('#object_insurer_ts_category_'+KEY).val(),markId: $('#object_insurer_ts_mark_id_'+KEY).select2('val')},
            function (response) {

                var options = "<option value='0'>Не выбрано</option>";
                response.map(function (item) {
                    options += "<option value='" + item.id + "'>" + item.title + "</option>";
                });

                $('#object_insurer_ts_model_id_'+KEY).html(options).select2('val', select_model_id);

            });

    }


</script>