
@if($order->status_id == 0)
    <div class="work-area-group">
        <div class="work-area-item">
            <span class="btn btn-primary" onclick="saveContractData(1)">На оформление</span>
        </div>
        <br/>
        <div class="work-area-item">
            <span class="btn btn-primary" onclick="saveContractData(3)">Доставка</span>
        </div>
        <br/>
        <div class="work-area-item">
            <span class="btn btn-danger" onclick="deleteOrder()">Удалить</span>
        </div>
    </div>

@endif


@if($order->status_id == 1)

    <div class="work-area-group">
        <div class="work-area-item">
            <span class="btn btn-primary" onclick="saveContractData(2)">На печать</span>
        </div>
        <br/>
        <div class="work-area-item">
            <span class="btn btn-danger" onclick="saveContractData(0)">Вернуть во временные</span>
        </div>
    </div>

@endif

@if($order->status_id == 2)

    <div class="work-area-group">
        <div class="work-area-item">
            <span class="btn btn-primary" onclick="saveContractData(3)">Доставка</span>
        </div>
        <br/>
        <div class="work-area-item">
            <span class="btn btn-danger" onclick="saveContractData(1)">Вернуть - На оформление</span>
        </div>
    </div>

@endif

@if($order->status_id == 3)

    <div class="work-area-group">
        <div class="work-area-item">
            <span class="btn btn-danger" onclick="saveContractData(2)">Вернуть - На печать</span>
        </div>
    </div>

@endif

<br/><br/>
<div class="work-area-group">
    <div class="work-area-item">
        <span class="btn btn-primary" onclick="cloneContract()">Дублировать</span>
    </div>

</div>