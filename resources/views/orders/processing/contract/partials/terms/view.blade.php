<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">

    <div class="block-view">
        <div class="block-sub">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">Дата заключения</span>
                        <span class="view-value">{{setDateTimeFormatRu($contract->sign_date, 1)}}</span>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">Период действия</span>
                        <span class="view-value">{{setDateTimeFormatRu($contract->begin_date, 1)}} - {{setDateTimeFormatRu($contract->end_date, 1)}}</span>
                    </div>
                </div>

                @if(auth()->user()->hasPermission('contracts', 'select_financial_policy'))
                    @if($contract->financial_policy_kv_bordereau || $contract->financial_policy_kv_dvoy || $contract->financial_policy_kv_agent || $contract->financial_policy_kv_parent)
                        @isset($contract->financial_policies->title)
                            <h3>{{ $contract->financial_policies->title }}</h3>
                        @endisset
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >

                            @if(auth()->user()->is('under'))
                            <div class="view-field">
                                <span class="view-label">Бордеро %</span>
                                <span class="view-value">{{$contract->financial_policy_kv_bordereau}}</span>
                            </div>

                            <div class="view-field">
                                <span class="view-label">ДВОУ %</span>
                                <span class="view-value">{{$contract->financial_policy_kv_bordereau}}</span>
                            </div>
                            @endif

                            <div class="view-field">
                                <span class="view-label">КВ Агента %</span>
                                <span class="view-value">{{$contract->financial_policy_kv_bordereau}}</span>
                            </div>

                            @if(auth()->user()->is('under') || auth()->user()->role->rolesVisibility(7)->visibility != 2)
                                <div class="view-field">
                                    <span class="view-label">КВ Руков. %</span>
                                    <span class="view-value">{{$contract->financial_policy_kv_bordereau}}</span>
                                </div>
                            @endif


                            @isset($contract->all_payments->first()->official_discount)
                                <div class="view-field">
                                    <span class="view-label">Оф. скидка %</span>
                                    <span class="view-value">{{ $contract->all_payments->first()->official_discount }}</span>
                                </div>
                            @endisset
                            @isset($contract->all_payments->first()->informal_discount)
                                <div class="view-field">
                                    <span class="view-label">Неоф. скидка %</span>
                                    <span class="view-value">{{ $contract->all_payments->first()->informal_discount }}</span>
                                </div>
                            @endisset
                            @isset($contract->all_payments->first()->bank_kv)
                                @if($contract->all_payments->first()->bank_kv != 0)
                                    <div class="view-field">
                                        <span class="view-label">Кв банка %</span>
                                        <span class="view-value">{{ $contract->all_payments->first()->bank_kv }}</span>
                                    </div>
                                @endif
                            @endisset

                        </div>
                    @endif
                @endif


                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">Алгоритм рассрочки</span>
                        <span class="view-value">
                            {{--
                            @if($contract[0]['installment_algorithms_id'])
                                {{$contract[0]['installment_algorithms_id']}}
                            @else
                                отсутствует
                            @endif
                            --}}
                      </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>