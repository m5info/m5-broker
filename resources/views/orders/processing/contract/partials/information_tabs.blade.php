    <div id="tt" class="easyui-tabs" data-options="tools:'#tab-tools'">


        <div title="Страхователь" id="tab-0" data-view="object_insurer"></div>
        <div title="Документы" id="tab-1" data-view="documents"></div>
        <div title="Полис" id="tab-6" data-view="mask"></div>
        <div title="Доставка" id="tab-2" data-view="delivery"></div>



        <div title="Чат" id="tab-3" data-view="chat"></div>


            @if($contract->order->status_id != 4)
                <div title="Резерв" id="tab-5" data-view="reserve_list"></div>
            @endif


        <div title="История" id="tab-4" data-view="logs"></div>

    </div>

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_tabs_container" style="margin-top: 15px;">

    </div>

    <script>

        var TAB_INDEX = -1;


        var _tempActivTab = 0;

        $(function () {



            $('#tt').tabs({
                border:false,
                pill: false,
                plain: true,
                onSelect: function(title, index){
                    return selectTab(index);
                }
            });

            selectTab(0);

            _tempActivTab = 1;

        });

        function selectTab(id) {

            if(parseInt(_tempActivTab) == 1){
                saveTab();
            }

            var tab = $('#tt').tabs('getSelected');
            load = tab.data('view');//$("#tab-"+id).data('view');
            TAB_INDEX = id;

            loaderShow();

            $.get("/orders/processing/contract/{{(int)$contract->id}}/get_html_block", {view:load}, function (response) {
                loaderHide();
                $("#main_tabs_container").html(response);

                initTab();

            }).done(function() {
                loaderShow();
            })
            .fail(function() {
                loaderHide();
            })
            .always(function() {
                loaderHide();
            });

        }
    </script>