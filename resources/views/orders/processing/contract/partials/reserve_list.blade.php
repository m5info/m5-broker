@if(auth()->user()->is('formalizator') && $temp != 'view')
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">
        <div class="col-lg-12">
            @if($order->status_id != 4 && !$order->bso_cart_id)
                <span class="btn btn-success pull-right" id="cart_tab_create">
                Зарезервировать
            </span>
            @endif
        </div>
    </div>
@endif


@if($order->bso_cart_id)
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row form-horizontal">
            @if(auth()->user()->is('formalizator'))
                <input type="hidden" name="order[0][formalizator_id]" id="formalizator_id" value="{{auth()->id()}}">
            @endif
            @php
                $formalizator = $order->formalizator_id ? \App\Models\User::find($order->formalizator_id) : false;
            @endphp


            {{--                                        <input type="hidden" name="" id="sk_user_id" value="">--}}
            {{--                                        <input type="hidden" name="" id="bso_state_id" value="">--}}
            <input type="hidden" name="user_id_from" id="user_id_from" value="0">
            <input type="hidden" name="bso_cart_type" id="bso_cart_type" value="1">
            <input type="hidden" name="bso_cart_id" id="bso_cart_id" value="{{$order->bso_cart_id}}">
            {{ Form::select('tp', collect([auth()->user()->point_sale_id => '']), auth()->user()->point_sale_id, ['style' => 'display: none', 'class' => 'tp']) }}

            @include("bso.transfer.edit_carts_min", ['bso_cart' => \App\Models\BSO\BsoCarts::find($order->bso_cart_id), 'im' => auth()->user()])
        </div>
    </div>
@endif


<script>

    function createCart()
    {
        var user_id_from = $("#user_id_from").val();
        var user_id_to = '{{ auth()->id() }}';
        var tp_change_selected = $("#tp_change_selected").is(':checked') ? 1 : 0;
        var sk_id_to = $("#sk_id_to").val();
        var bso_cart_type = 1;
        var bso_state_id = $("#bso_state_id").val();
        var tp_id = '{{ auth()->user()->point_sale_id }}';
        var tp_new_id = $("#tp_new").val();
        var tp_bso_manager_id = $("#tp_bso_manager").val();
        var courier_id = $(".couriers").val();
        switch (bso_cart_type) {
            case '1':
                // Передача со склада агенту
                if (user_id_to == 0) {
                    alert('Укажите агента-получателя');
                    return false;
                }
                break;
            case '6':
                // Передача БСО курьеру
                if (user_id_to == 0) {
                    alert('Укажите агента-получателя');
                    return false;
                }
                break;
            case '2':
                // Передача от агента-агенту
                if (user_id_from == 0) {
                    alert('Укажите агента-отправителя');
                    return false;
                }
                if (user_id_to == 0) {
                    alert('Укажите агента-получателя');
                    return false;
                }
                break;
            case '3':
                // Прием БСО от агента
                if (user_id_from == 0) {
                    alert('Укажите агента-отправителя');
                    return false;
                }
                break;
        }
        var bso_cart_id = myGetAjax('/bso/transfer/create_bso_cart/?user_id_from=' + user_id_from + '&user_id_to=' + user_id_to + '&sk_id_to=' + sk_id_to + '&bso_cart_type=' + bso_cart_type + '&bso_state_id=' + bso_state_id + '&tp_id=' + tp_id+ '&tp_new_id=' + tp_new_id + '&tp_bso_manager_id=' + tp_bso_manager_id + '&courier_id=' + courier_id + '&tp_change_selected=' + tp_change_selected);

        // if(parseInt(bso_cart_id)0) window.location = '/bso/transfer/?bso_cart_id=' + bso_cart_id;

        myPostAjax("{{url("/orders/processing/order/{$order->id}/bso_cart")}}", "bso_cart_id="+bso_cart_id);

        return bso_cart_id;
    }

</script>