<div class="work-area-group">
    <h3>Действия</h3>

    @if($temp == 'edit')
        @if(auth()->user()->hasPermission('orders', 'order_statuses'))
        <div class="work-area-group">
            <div class="work-area-item">
                {{ Form::select('order_status_to_change', collect(\App\Models\Contracts\Orders::STATUSES), $order->status_id, ['class' => 'form-control select2-ws pull-right', 'id' => 'status_to_change']) }}
            </div>
            <br/> <br/>
            <div class="work-area-item">
                <span class="btn btn-primary" onclick="saveContractData($('#status_to_change').val())">Сохранить</span>
            </div>
        </div>

        @else

            @include("orders.processing.contract.partials.buttons_status", ["order"=>$order])

        @endif

    @else
        <br/><br/>
        <div class="work-area-group">
            <div class="work-area-item">
                <span class="btn btn-primary" onclick="cloneContract()">Дублировать</span>
            </div>
        </div>
    @endif





</div>