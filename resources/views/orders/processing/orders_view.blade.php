@if($status == 3)
    @if($temp == 'edit')
        @include("orders.processing.partials.orders_map")
    @else
        @include("orders.processing.partials.orders_table")
    @endif
@elseif($status == 4 || $status == 5)
    @include("orders.processing.partials.orders_table")
@else
    @include("orders.processing.partials.contracts_table")
@endif

