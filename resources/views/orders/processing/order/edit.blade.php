@extends('layouts.app')

@section('head')

@append



@section('content')



    <div class="page-heading">
        <div class="col-lg-12">
            <h2>Заявка на оформление №{{ $order->id }} - {{ \App\Models\Contracts\Orders::STATUSES[$order->status_id] }}</h2>
        </div>



        {{Form::open(['url' => url("/orders/processing/order/{$order->id}/save"), 'id' => 'formData', 'class' => 'form-horizontal'])}}


        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
            <div class="page-subheading">
                <h2>Доставка</h2>
                <a href="/contracts/orders/edit/{{$order->id}}/act_export_delivery" class="btn btn-info btn-right doc_export_btn">Направление</a>
            </div>

            <div class="block-main">
                <div class="block-sub">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">
                            <div>
                                <div class="col-lg-6">
                                    <div class="field form-col">
                                        <label class="control-label">Дата</label>
                                        @php
                                            $date = setDateTimeFormatRu($order->delivery_date, 1);
                                            $time = $order->delivery_time;
                                        @endphp
                                        {{ Form::text('order[0][delivery][date]', $date, ['class' => 'form-control format-date datepicker date']) }}
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="field form-col">
                                        <label class="control-label">Время</label>
                                        {{ Form::text('order[0][delivery][time]', ($time) ? $time : "12:00", ['class' => 'form-control format-time']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <label class="control-label">Город</label>
                                {{ Form::select('order[0][delivery][city_id]', \App\Models\Settings\City::where('is_actual', '=', 1)->get()->pluck('title', 'id'), $order->delivery_city_id, ['class' => 'form-control select2']) }}
                            </div>
                            <div class="col-lg-12">
                                <label class="control-label">Метро</label>
                                {{ Form::text('order[0][delivery][metro]', $order->delivery_metro, ['class' => 'form-control', 'id' => 'delivery_metro']) }}
                            </div>
                            <div class="col-lg-12">
                                <label class="control-label">Адрес</label>
                                {{ Form::text('order[0][delivery][address]', $order->address, ['class' => 'form-control', 'id' => 'delivery_address']) }}
                                {{ Form::hidden('order[0][delivery][address_kladr]', $order->address_kladr, ['id' => 'address_kladr']) }}
                                {{ Form::hidden('order[0][delivery][address_width]', $order->address_width, ['id' => 'address_width']) }}
                                {{ Form::hidden('order[0][delivery][address_longitude]', $order->address_longitude, ['id' => 'address_longitude']) }}
                            </div>


                            @if($order->status_id >= 3)
                                <div class="col-lg-12">
                                    <span class="btn btn-success btn-right" onclick="saveContractData()">
                                        Сохранить
                                    </span>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >

            <div class="page-subheading">
                <h2>&nbsp;</h2>

                @if($order->delivery_user_id > 0)
                    <span class="btn btn-primary btn-left doc_export_btn" onclick="closeOrders(0)">Завершить</span>
                    <span class="btn btn-danger btn-right doc_export_btn" onclick="closeOrders(-1)">Архив</span>
                @endif
            </div>

            <div class="block-main">
                <div class="block-sub">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">
                            <div>
                                <div class="col-lg-12">
                                    <div class="field form-col">
                                        <label class="control-label">Курьер</label>
                                        @include('partials.elements.users', ['title'=>'order[0][delivery][courier]', 'select'=> $order->delivery_user_id])
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="field form-col">
                                        <label class="control-label">Стоимость</label>
                                        {{Form::select('order[0][delivery][departures_courier_price_id]', \App\Models\Settings\DeparturesCourierPrice::getDeparturesCourierPrice($order->delivery_city_id)->pluck('title','id'), $order->departures_courier_price_id, ['class' => 'form-control select2-ws'])}}
                                    </div>
                                </div>


                                <div class="col-lg-12">
                                    <label class="control-label">Комментарий к доствке</label>
                                    {{ Form::textarea('order[0][delivery][comment]', $order->delivery_comment, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        {{Form::close()}}


        <div class="row form-horizontal">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">
                <div class="col-lg-12">
                    @if(count($order->contracts))
                        <div class="page-subheading">
                            <h2>Созданные договора
                            </h2>
                            <a href="{{url("/bso/transfer/?bso_cart_id={$order->bso_cart_id}")}}" class="pull-right" style="margin-top: 30px;" target="_blank">Акт БСО</a>
                        </div>
                        <div class="block-main">
                            <div class="block-sub">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                                    <div class="row form-horizontal">
                                        <table class="tov-table">
                                            <tr>
                                                <th>ID</th>
                                                <th>СК</th>
                                                <th>Менеджер</th>
                                                <th>Номер договора</th>
                                                <th>Продукт</th>
                                                <th>Страхователь</th>
                                                <th>Страховая премия</th>
                                                <th></th>
                                            </tr>
                                            @foreach($order->contracts as $contract)
                                                @php

                                                    if($contract->statys_id > 0){
                                                        $link = "class=clickable-row data-href=" .url("/contracts/temp_contracts/contract/{$contract->id}/edit");
                                                    }else{
                                                        $link = "class=clickable-row data-href=" .url("/orders/processing/contract/$contract->id");
                                                    }


                                                @endphp
                                                <tr @if($contract->statys_id > 0) style="background-color: #e6ffe6; @endif">
                                                    <td {{$link}}>{{ $contract->id }}</td>
                                                    <td {{$link}}>{{ ($contract->insurance_companies)?$contract->insurance_companies->title:'' }}</td>
                                                    <td>{{ ($contract->manager)? $contract->manager->name : '' }}</td>
                                                    <td {{$link}}>{{ ($contract->bso)?$contract->bso->bso_title:'' }}</td>
                                                    <td {{$link}}>{{ $contract->product->title }}</td>
                                                    <td {{$link}}>{{ $contract->insurer->title }}</td>
                                                    <td {{$link}}>{{ titleFloatFormat($contract->payment_total) }}</td>
                                                    <td>
                                                        @if($contract->statys_id == -2)
                                                            <span class="btn btn-primary btn-right doc_export_btn" onclick="closeOrders('{{$contract->id}}')">В проверку</span>
                                                        @endif
                                                    </td>

                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')

    <script src="/js/jquery.datetimepicker.full.min.js"></script>
    <link rel="stylesheet" href="/css/jquery.datetimepicker.min.css">

    <script>



        $(function () {


            $('#delivery_address').suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "ADDRESS",
                count: 5,
                onSelect: function (suggestion) {
                    key = $(this).data('key');
                    $('#address_kladr').val(suggestion.data.kladr_id);
                    $('#address_width').val(suggestion.data.geo_lat);
                    $('#address_longitude').val(suggestion.data.geo_lon);

                    //$('#delivery_metro').val(suggestion.data.metro);

                }
            });

            formatTime();

        });


        function formatTime() {
            var configuration = {
                timepicker: true,
                datepicker: false,
                format: 'H:i',
                scrollInput: false
            };
            $.datetimepicker.setLocale('ru');
            $('input.format-time').datetimepicker(configuration).keyup(function (event) {
                if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 38 && event.keyCode != 40) {
                    var pattern = new RegExp("[0-9:]{5}");
                    if (pattern.test($(this).val())) {
                        $(this).datetimepicker('hide');
                        $(this).datetimepicker('show');
                    }
                }
            });
            $('input.format-time').each(function () {
                var im = new Inputmask("99:99", {
                    "oncomplete": function () {
                    }
                });
                im.mask($(this));
            });
        }



        function saveContractData()
        {
            $.post('{{url("/orders/processing/order/{$order->id}/save")}}', $('#formData').serialize(), function (response) {

                if (parseInt(response.state) === 0) {
                    flashMessage('success', "Данные успешно сохранены!");

                } else {
                    flashHeaderMessage("Ошибка", 'danger');
                }

            }).always(function () {

            });
        }

        function closeOrders(contract)
        {
            $.post('{{url("/orders/processing/order/{$order->id}/close")}}?contract_id='+contract, $('#formData').serialize(), function (response) {

                if (parseInt(response.state) === 0) {
                    window.location = "/orders/processing";
                }else if(parseInt(response.state) === 1){
                    reload();
                }else {
                    flashHeaderMessage("Ошибка", 'danger');
                }

            }).always(function () {

            });
        }



    </script>


@endsection
