@extends('layouts.app')

@section('head')

@append



@section('content')



    <div class="page-heading">


        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
            <div class="page-subheading">
                <h2>Доставка</h2>
                <a href="/contracts/orders/edit/{{$order->id}}/act_export_delivery" class="btn btn-info btn-right doc_export_btn">Направление</a>
            </div>

            <div class="block-main">
                <div class="block-sub">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="view-field">
                                    <span class="view-label">Дата</span>
                                    <span class="view-value">{{setDateTimeFormatRu($order->delivery_date, 1)}} {{$order->delivery_time}}</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="view-field">
                                    <span class="view-label">Город</span>
                                    <span class="view-value">{{($order->delivery_city)?$order->delivery_city->title:''}}</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="view-field">
                                    <span class="view-label">Метро</span>
                                    <span class="view-value">{{$order->delivery_metro}}</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="view-field">
                                    <span class="view-label">Адрес</span>
                                    <span class="view-value">{{$order->address}}</span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >

            <div class="page-subheading">
                <h2>Исполнитель</h2>
            </div>

            <div class="block-main">
                <div class="block-sub">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">
                            <div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="view-field">
                                        <span class="view-label">Курьер</span>
                                        <span class="view-value">{{$order->courier?$order->courier->name:''}}</span>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="view-field">
                                        <span class="view-label">Стоимость</span>
                                        <span class="view-value">{{$order->departures_courier_price?$order->departures_courier_price->title.' '.titleFloatFormat($order->departures_courier_price->price_total):''}}</span>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <label class="control-label">Комментарий к доствке</label>
                                    <br/>
                                    {{$order->delivery_comment}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>



        <div class="row form-horizontal">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">
                <div class="col-lg-12">
                    @if(count($order->contracts))
                        <div class="page-subheading">
                            <h2>Созданные договора</h2>
                        </div>
                        <div class="block-main">
                            <div class="block-sub">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                                    <div class="row form-horizontal">
                                        <table class="tov-table">
                                            <tr>
                                                <th>ID</th>
                                                <th>СК</th>
                                                <th>Менеджер</th>
                                                <th>Номер договора</th>
                                                <th>Продукт</th>
                                                <th>Страхователь</th>
                                                <th>Страховая премия</th>
                                            </tr>
                                            @foreach($order->contracts as $contract)
                                                @php
                                                    $link = "class=clickable-row data-href=" .url("/orders/processing/contract/$contract->id");
                                                @endphp
                                                <tr>
                                                    <td {{$link}}>{{ $contract->id }}</td>
                                                    <td {{$link}}>{{ ($contract->insurance_companies)?$contract->insurance_companies->title:'' }}</td>
                                                    <td>{{ ($contract->manager)? $contract->manager->name : '' }}</td>
                                                    <td {{$link}}>{{ ($contract->bso)?$contract->bso->bso_title:'' }}</td>
                                                    <td {{$link}}>{{ $contract->product->title }}</td>
                                                    <td {{$link}}>{{ $contract->insurer->title }}</td>
                                                    <td {{$link}}>{{ titleFloatFormat($contract->payment_total) }}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')


    <script>



        $(function () {




        });





    </script>


@endsection
