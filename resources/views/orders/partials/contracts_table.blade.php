<table class="tov-table">
    <thead>
    <tr>
        <th>№</th>
        <th>Тип</th>
        <th>Страхователь</th>
        <th>Объект</th>
        <th>Дата осмотра</th>
        <th>Телефон</th>
{{--        <th></th>--}}
    </tr>
    </thead>
    <tbody>
        @if(sizeof($orders))
            @foreach($orders as $order)
                @php($tr_style = '')
                @if($order->inspection_logs)
                    @if($order->inspection_logs->where('declined_user_id', '=', auth()->id())->count())
                        @php($tr_style = 'bg-yellow')
                    @elseif($users_log = $order->inspection_logs->where('user_id', '=', auth()->id())->count())
                        @if($order->inspection->accept_user_id == '')
                            @php($tr_style = 'bg-red')
                        @endif
                    @endif
                @endif
                <tr class="{{ $tr_style }} clickable-row" data-href="{{url("orders/edit/{$order->id}/")}}">
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->product->title }}</td>
                    <td>{{ $order->insurer ? $order->insurer->title : "" }}</td>
                    <td>{{ $order->object_insurer->data()->mark_id ? \App\Models\Vehicle\VehicleMarks::where('id', $order->object_insurer->data()->mark_id)->first()->title : '' }} {{ $order->object_insurer->data()->model_id ? \App\Models\Vehicle\VehicleModels::where('id', $order->object_insurer->data()->model_id)->first()->title : '' }} {{$order->object_insurer->data()->reg_number}}</td>
                    <td>{{ $order->begin_date ? setDateTimeFormatRu($order->begin_date) : '' }}</td>
                    <td>{{ $order->insurer ? $order->insurer->phone : '' }}</td>
{{--                    <td>--}}
{{--                        @if($tabs_visibility[$order->status_order_id]['edit'])--}}
{{--                            <a class="btn btn-success" href="{{url("orders/edit/{$order->id}/")}}">--}}
{{--                                <i class="fa fa-edit"></i>--}}
{{--                            </a>--}}
{{--                        @elseif($tabs_visibility[$order->status_order_id]['view'])--}}
{{--                            <a class="btn btn-success" href="{{url("orders/edit/{$order->id}/")}}">--}}
{{--                                <i class="fa fa-eye"></i>--}}
{{--                            </a>--}}
{{--                        @endif--}}
{{--                    </td>--}}
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="13" class="text-center">Нет записей</td>
            </tr>
        @endif
    </tbody>
</table>