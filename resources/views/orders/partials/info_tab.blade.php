<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="filter-group">

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="insurer">Страхователь</label>
                    {{ Form::text('insurer', request('insurer', ''), ['class' => 'form-control',  'onkeyup'=>'loadData()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="conclusion_date_from">Дата с</label>
                    {{ Form::text('conclusion_date_from', request('conclusion_date_from', ''), ['class' => 'form-control datepicker date',  'onchange'=>'loadData()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="conclusion_date_to">Дата по</label>
                    {{ Form::text('conclusion_date_to', request('conclusion_date_to', ''), ['class' => 'form-control datepicker date',  'onchange'=>'loadData()']) }}
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div id="table" class="table"></div>
    </div>
</div>