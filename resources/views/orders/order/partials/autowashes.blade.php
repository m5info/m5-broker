@if(count($orgs))
    <h3>Распределение</h3>
    <div id="executor_detail">
        @if(isset($choosen_org) && $org = \App\Models\Organizations\PointsDepartments::find($choosen_org))
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">
                    <div class="row">
                        <h3>Информация по {{ $org->title }}</h3>
                        <div class="view-field">
                            <span class="view-label">ИНН</span>
                            <span class="view-value">{{ $org->inn }}</span>
                        </div>
                        <div class="view-field">
                            <span class="view-label">КПП</span>
                            <span class="view-value">{{ $org->kpp }}</span>
                        </div>
                        <div class="view-field">
                            <span class="view-label">Фактический адрес</span>
                            <span class="view-value">{{ $org->fact_address }}</span>
                        </div>
                        <div class="view-field">
                            <span class="view-label">Юридический адрес</span>
                            <span class="view-value">{{ $org->address }}</span>
                        </div>
                        <div class="view-field">
                            <span class="view-label">Генеральный директор</span>
                            <span class="view-value">{{ $org->general_manager }}</span>
                        </div>
                        <div class="view-field">
                            <span class="view-label">Контактное лицо</span>
                            <span class="view-value">{{ $org->user_contact_title }}</span>
                        </div>
                        <div class="view-field">
                            <span class="view-label">Телефон</span>
                            <span class="view-value">{{ $org->phone }}</span>
                        </div>
                        <div class="view-field">
                            <span class="view-label">E-mail</span>
                            <span class="view-value">{{ $org->email }}</span>
                        </div>
                    </div>
            </div>
        @endif
    </div>
    <table class="table table-striped table-bordered table_for_yamap">
        @foreach($orgs as $org)
            <tr data-id="{{$org->id}}" {{ (isset($choosen_org) && $choosen_org == $org->id) ? 'class=choosen_yamap' : '' }}>
                <td><b>{{ $org->title }}</b></td>
                <td>{{ $org->address }}</td>
            </tr>
        @endforeach
    </table>
    <script>
        ymaps.ready(function(){

            setTimeout(function () {
                var tr = $('.table_for_yamap tr');

                tr.each(function (index, value){
                    if($(value).data('id') == '{{$choosen_org}}'){
                        value.click();
                    }
                });
            }, 100)

        });
    </script>
@endif
