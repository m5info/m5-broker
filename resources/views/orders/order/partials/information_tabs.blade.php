<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-top:15px;">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#panel1">Фото/Видео</a></li>
        <li><a data-toggle="tab" href="#panel2">Чат</a></li>
        <li><a data-toggle="tab" href="#panel3">История изменений</a></li>
        @if(!(int)$order->inspection->position_type_id)
            <li><a data-toggle="tab" href="#panel4">Визуализация</a></li>
        @endif
    </ul>

    <div class="tab-content">
        <div id="panel1" class="tab-pane fade in active">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="page-subheading">
                    <h2>Фото/Видео</h2>
                </div>
                <div class="block-main">
                    <div class="block-sub">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <div class="row form-horizontal">

                                @include('orders.order.documents', ['order' => $order, 'hold_kv_product' => $hold_kv_product])

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="panel2" class="tab-pane fade">
            <div class="col-lg-12">
                @include('contracts.chat.chat', ['contract' => $order])
            </div>
        </div>
        <div id="panel3" class="tab-pane fade">

            <h3>Логи</h3>
            <table class="table table-striped table-bordered">
                <th>Назначил/изменил</th>
                <th>Назначенный исполнитель</th>
                <th>Статус</th>
                <th>Дата</th>
                @if($order->inspection_logs)
                    @foreach($order->inspection_logs as $log)
                        @php($class = '')
                        @if(isset($log->executor_user_id) && $log->executor_user_id == $log->contract->inspection->processing_user_id)
                            @php($class = 'bg-green')
                        @endif
                        @if($log->status_title == 'Отказ')
                            @php($class = 'bg-red')
                        @elseif($log->status_title == 'Назначен исполнитель')
                            @php($class = 'bg-green')
                        @endif
                        <tr class="{{$class}}">
                            <td><b>{{ $log->user_id ? \App\Models\User::find($log->user_id)->name : '' }}</b></td>
                            @if((int)$order->inspection->position_type_id)
                                <td>{{ $order->last_inspection_log() && $order->inspection->org_point_id ? \App\Models\Organizations\PointsDepartments::find($order->inspection->org_point_id)->title : '' }}</td>
                            @else
                                <td>{{ $log->executor_user_id ? \App\Models\User::find($log->executor_user_id)->name : '' }}</td>
                            @endif
                            <td>{{ $log->status_title }}</td>
                            <td>{{ setDateTimeFormatRu($log->created_at) }}</td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>
        @if(!(int)$order->inspection->position_type_id)
            <div id="panel4" class="tab-pane fade">
                <div class="col-lg-12">
                    @include('orders.order.partials.map')
                </div>
            </div>
        @endif
    </div>
</div>

@include('partials.pusher')