<h3>Распределение</h3>
<div id="executor_detail">
    @if(isset($choosen_user) && $user = \App\Models\User::find($choosen_user))
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">
            <div class="row">
                <h3>Исполнитель - {{ $user->name }}</h3>
                <div class="view-field">
                    <span class="view-label">E-mail</span>
                    <span class="view-value">{{ $user->email }}</span>
                </div>
                <div class="view-field">
                    <span class="view-label">Рабочий телефон</span>
                    <span class="view-value">{{ $user->work_phone }}</span>
                </div>
                <div class="view-field">
                    <span class="view-label">Организация</span>
                    <span class="view-value">{{ $user->organization->title }}</span>
                </div>
            </div>
        </div>
    @endif
</div>
@if(count($users))
    <table class="table table-striped table-bordered table_for_yamap">
        @foreach($users as $user)
            <tr data-id="{{$user->id}}" {{ (isset($choosen_user) && $choosen_user == $user->id) ? 'class=choosen_yamap' : '' }}>
                <td><b>{{ $user->name }}</b></td>
                <td>{{ $user->title }}</td>
            </tr>
        @endforeach
    </table>
    <script>
        ymaps.ready(function(){

            setTimeout(function () {
                var tr = $('.table_for_yamap tr');

                tr.each(function (index, value){
                    if($(value).data('id') == '{{$choosen_user}}'){
                        value.click();
                    }
                });
            }, 100)

        });
    </script>
@endif


