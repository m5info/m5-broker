<div id="map" style="width:100%; height:500px">

</div>


<script>

    function initMaps() {

        var option = $( "#cities option:selected");

        if (option.length == 0){
            var option = $("input#cities")
        }
        latitude = option.data('geo_lat');
        longitude = option.data('geo_lon');


        ymaps.ready(function(){

            map = new ymaps.Map('map', {
                center: [parseFloat(latitude), parseFloat(longitude)],
                zoom: 11,
                controls: ["typeSelector"]
            });
            openFlagToMap()
        });
    }


    function openFlagToMap() {
        var option = $( "#cities option:selected");

        if (option.length == 0){
            var option = $("input#cities")
        }

        latitude = option.data('geo_lat');
        longitude = option.data('geo_lon');

        if(latitude.length > 0 && longitude.length > 0){

            // map.geoObjects.removeAll();
            //
            // myGeoObject_e = new ymaps.GeoObject({
            //     geometry: {
            //         type: "Point",// тип геометрии - точка
            //         coordinates: [latitude, longitude] // координаты точки
            //     }
            // });
            //
            // myGeoObject_e.events.add('click', function (e) {
            //     // Метка, на которой сработало событие
            //     var params = e.get('target').properties._data;
            //
            //     set_tr(params.id);
            // });
            // map.geoObjects.add(myGeoObject_e);
            map.setCenter([latitude, longitude], 10);
            map.setZoom(11);
        }
    }

</script>