@if($order->status_order_id == 3 || $order->status_order_id == 4)

    @if(auth()->user()->is('under') && $order->kind_acceptance != 2)


        <span class="btn btn-success btn-left" onclick="changeStatusAccept();">
            Акцепт
        </span>


    @endif
@endif

@if($order->status_order_id == 0)
    <span class="btn btn-primary btn-right" onclick="change_event(1);change_status(1)">
        На распределение
    </span>
@endif

@if($order->status_order_id == 1)
    <span class="btn btn-primary btn-right" onclick="change_event(4);change_status(2)">
        В работу
    </span>

    <span class="btn btn-primary btn-right" onclick="change_event(3);change_status(0)">
        Вернуть во временные
    </span>
@endif

@if($order->status_order_id == 2)
    <span class="btn btn-primary btn-right" onclick="change_event(4);change_status(3)">
        В проверку
    </span>

    <span class="btn btn-primary btn-right" onclick="change_event(3);change_status(1)">
        Вернуть на распределение
    </span>
@endif

@if($order->status_order_id == 3)
    <span class="btn btn-primary btn-right" onclick="change_event(4);change_status(4)">
        На согласование
    </span>

    <span class="btn btn-primary btn-right" onclick="change_event(5);change_status(2)">
        Вернуть в работу
    </span>
@endif

@if($order->status_order_id == 4)
    <span class="btn btn-primary btn-right" onclick="change_event(6);change_status(5)">
        В архив
    </span>

    <span class="btn btn-primary btn-right" onclick="change_event(4);change_status(4)">
        Вернуть в проверку
    </span>
@endif

@if($order->status_order_id == 5)
    <span class="btn btn-primary btn-right" onclick="change_event(6);change_status(4)">
        Вернуть на согласование
    </span>
@endif

@if($order->status_order_id == 0 && $tabs_visibility[$order->status_order_id]['edit'])
    <span class="btn btn-danger btn-right" onclick="deleteOrder({{$order->id}});">
        Удалить заявку
    </span>
@endif
{{--            <span class="btn btn-info btn-right" id="inspection_export_act">Акт</span>--}}
@if($order->status_order_id >= 2 && isset($order->inspection->position_type_id) && !(int)$order->inspection->position_type_id)
    <a href="{{ url("get_mobile_act/".$order->getTokenAct()) }}" target="_blank" class="btn btn-info btn-right">Акт</a>
@endif