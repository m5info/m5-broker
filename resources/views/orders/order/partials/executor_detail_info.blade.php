@if(!$position_type)
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">
        <div class="row">
            <h3>{{ $executor->name }}</h3>
            <div class="view-field">
                <span class="view-label">E-mail</span>
                <span class="view-value">{{ $executor->email }}</span>
            </div>
            <div class="view-field">
                <span class="view-label">Рабочий телефон</span>
                <span class="view-value">{{ $executor->work_phone }}</span>
            </div>
            <div class="view-field">
                <span class="view-label">Организация</span>
                <span class="view-value">{{ $executor->organization->title }}</span>
            </div>
        </div>
    </div>
@else
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">
        <div class="row">
            <h3>Информация по {{ $executor->pd_title }}</h3>
            <div class="view-field">
                <span class="view-label">Организация</span>
                <span class="view-value">{{ $executor->org_title }}</span>
            </div>
            <div class="view-field">
                <span class="view-label">ИНН</span>
                <span class="view-value">{{ $executor->inn }}</span>
            </div>
            <div class="view-field">
                <span class="view-label">КПП</span>
                <span class="view-value">{{ $executor->kpp }}</span>
            </div>

            <div class="view-field">
                <span class="view-label">Контактное лицо</span>
                <span class="view-value">{{ $executor->user_contact_title }}</span>
            </div>
            <div class="view-field">
                <span class="view-label">Телефон</span>
                <span class="view-value">{{ $executor->phone }}</span>
            </div>
            <div class="view-field">
                <span class="view-label">E-mail</span>
                <span class="view-value">{{ $executor->email }}</span>
            </div>
        </div>
    </div>
@endif