<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title></title>
  <style>
    /*@font-face {*/
    /*  font-family:'Symbol';*/
    /*  font-style:normal;*/
    /*  font-weight:normal;*/
    /*  src:local('вє'), url('font001.woff') format('woff');*/
    /*}*/
    /*@font-face {*/
    /*  font-family:'Times New Roman';*/
    /*  font-style:normal;*/
    /*  font-weight:bold;*/
    /*  src:local('вє'), url('font002.woff') format('woff');*/
    /*}*/
    /*@font-face {*/
    /*  font-family:'Times New Roman';*/
    /*  font-style:normal;*/
    /*  font-weight:normal;*/
    /*  src:local('вє'), url('font003.woff') format('woff');*/
    /*}*/
    .wcdiv {
      position:absolute;
    }
    .wcspan {
      position:absolute;
      white-space:pre;
      color:#000000;
      font-size:12pt;
    }
    .wcimg {
      position:absolute;
    }
    .wcsvg {
      position:absolute;
    }
    .dmg_transport img{

      width: 80px;
    }
    .wcpage {
      position:relative;
      margin:10pt auto 10pt auto;
      overflow:hidden;
    }
    .wctext001 {
      font-family:'Times New Roman';
      font-style:normal;
      font-weight:normal;
    }
    .wctext002 {
      font-family:'Times New Roman';
      font-style:normal;
      font-weight:bold;
    }
    .wctext003 {
      font-family:'Symbol';
      font-style:normal;
      font-weight:normal;
    }
    /*@font-face {*/
    /*  font-family:'Calibri';*/
    /*  font-style:normal;*/
    /*  font-weight:normal;*/
    /*  src:local('☺'), url('font001.woff') format('woff');*/
    /*}*/
    /*@font-face {*/
    /*  font-family:'Symbol';*/
    /*  font-style:normal;*/
    /*  font-weight:normal;*/
    /*  src:local('☺'), url('font002.woff') format('woff');*/
    /*}*/
    /*@font-face {*/
    /*  font-family:'Times New Roman';*/
    /*  font-style:normal;*/
    /*  font-weight:bold;*/
    /*  src:local('☺'), url('font003.woff') format('woff');*/
    /*}*/
    /*@font-face {*/
    /*  font-family:'Times New Roman';*/
    /*  font-style:normal;*/
    /*  font-weight:normal;*/
    /*  src:local('☺'), url('font004.woff') format('woff');*/
    /*}*/
    .wcdiv {
      position:absolute;
    }
    .wcspan {
      position:absolute;
      white-space:pre;
      color:#000000;
      font-size:12pt;
    }
    .wcimg {
      position:absolute;
    }
    .wcsvg {
      position:absolute;
    }
    .wcpage {
      position:relative;
      margin:10pt auto 10pt auto;
      overflow:hidden;
    }
    .wctext001 {
      font-family:'Calibri';
      font-style:normal;
      font-weight:normal;
    }
    .wctext002 {
      font-family:'Times New Roman';
      font-style:normal;
      font-weight:normal;
    }
    .wctext003 {
      font-family:'Times New Roman';
      font-style:normal;
      font-weight:bold;
    }
    .wctext004 {
      font-family:'Symbol';
      font-style:normal;
      font-weight:normal;
    }
    body {
      margin:     0;
      padding:    0;
      width:      21cm;
      height:     29.7cm;
      text-align: justify;

    }
  </style>
</head>
<body>
  <div class="wcdiv wcpage" style="width:595.3pt; height:841.9pt;margin:-35px 0;">
    <div class="wcdiv" style="left:42.55pt; top:28.35pt;">
      <div class="wcdiv" style="left:368.55pt; z-index:1;"><span class="wcspan wctext001" style="font-size:5pt; left:12.44pt; top:0.21pt;">Приложение 5 к Положению Банка России от 19 сентября 2014 года N&nbsp;</span> <span class="wcspan wctext001" style="font-size:5pt; left:150.27pt; top:5.96pt;">431-П</span> <span class="wcspan wctext001" style="font-size:5pt; left:48.01pt; top:11.71pt;">«О правилах обязательного страхования гражданской</span> <span class="wcspan wctext001" style="font-size:5pt; left:51.12pt; top:17.46pt;">ответственности владельцев транспортных средств»</span></div>
      <div class="wcdiv" style="left:-1.4pt; top:23pt; z-index:1;">
        <div class="wcdiv" style="clip:rect(-1pt,463.1pt,14.65pt,-1pt);">
          <div class="wcdiv" style="left:121.9pt; z-index:3;"><span class="wcspan wctext002" style="font-size:11pt; left:0pt; top:0.47pt;">Извещение о дорожно-транспортном происшествии</span></div>
        </div>
        <div class="wcdiv" style="top:12.65pt;">
          <div class="wcdiv" style="clip:rect(-1pt,463.1pt,10.62pt,-1pt);">
            <div class="wcdiv" style="left:107.75pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">Составляется водителями ТС. Содержит данные об обстоятельствах ДТП, его участниках.</span></div>
          </div>
          <div class="wcdiv" style="left:462.1pt; clip:rect(-1pt,64.75pt,10.62pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7.5pt; left:60.95pt; top:0.32pt;">&nbsp;</span></div>
          </div>
        </div>
      </div>
      <div class="wcdiv" style="top:44.27pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:6.32pt;">1. Место ДТП</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:44.84pt; top:6.32pt;">  {{ $data->info->address }}</span></div>
      <div class="wcdiv" style="left:49.05pt; top:58.9pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:152.73pt; top:1.75pt;">(республика, край, область, район, населенный пункт, улица, дом)</span></div>
      <div class="wcdiv" style="left:47.6pt; top:58.9pt; width:478.25pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.4pt; top:67.3pt; z-index:1;">
        <div class="wcdiv" style="clip:rect(-1pt,52pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">2. Дата ДТП</span></div>
        </div>
        <div class="wcdiv" style="left:51pt; clip:rect(-0.5pt,8.5pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.73pt; top:0.32pt;">{{ date('d', strtotime($data->info->dates))[0] }}</span></div>
        </div>
        <div class="wcdiv" style="left:58.75pt; clip:rect(-0.5pt,8.5pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.73pt; top:0.32pt;">{{ date('d', strtotime($data->info->dates))[1] }}</span></div>
        </div>
        <div class="wcdiv" style="left:66.5pt; clip:rect(-0.5pt,8.5pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:1.66pt; top:0.32pt;">.</span></div>
        </div>
        <div class="wcdiv" style="left:74.25pt; clip:rect(-0.5pt,8.5pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.73pt; top:0.32pt;">{{ date('m', strtotime($data->info->dates))[0] }}</span></div>
        </div>
        <div class="wcdiv" style="left:82pt; clip:rect(-0.5pt,8.5pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.73pt; top:0.32pt;">{{ date('m', strtotime($data->info->dates))[1] }}</span></div>
        </div>
        <div class="wcdiv" style="left:89.75pt; clip:rect(-0.5pt,8.5pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:1.66pt; top:0.32pt;">.</span></div>
        </div>
        <div class="wcdiv" style="left:97.5pt; clip:rect(-0.5pt,8.55pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.75pt; top:0.32pt;">{{ date('Y', strtotime($data->info->dates))[0] }}</span></div>
        </div>
        <div class="wcdiv" style="left:105.3pt; clip:rect(-0.5pt,8.5pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.73pt; top:0.32pt;">{{ date('Y', strtotime($data->info->dates))[1] }}</span></div>
        </div>
        <div class="wcdiv" style="left:113.05pt; clip:rect(-0.5pt,8.5pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.73pt; top:0.32pt;">{{ date('Y', strtotime($data->info->dates))[2] }}</span></div>
        </div>
        <div class="wcdiv" style="left:120.8pt; clip:rect(-0.5pt,8.5pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.73pt; top:0.32pt;">{{ date('Y', strtotime($data->info->dates))[3] }}</span></div>
        </div>
        <div class="wcdiv" style="left:136.3pt; clip:rect(-0.5pt,8.5pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.73pt; top:0.32pt;">{{ date('H', strtotime($data->info->dates))[0] }}</span></div>
        </div>
        <div class="wcdiv" style="left:144.05pt; clip:rect(-0.5pt,8.55pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.75pt; top:0.32pt;">{{ date('H', strtotime($data->info->dates))[1] }}</span></div>
        </div>
        <div class="wcdiv" style="left:151.85pt; clip:rect(-0.5pt,8.5pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:1.56pt; top:0.32pt;">:</span></div>
        </div>
        <div class="wcdiv" style="left:159.6pt; clip:rect(-0.5pt,8.5pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.73pt; top:0.32pt;">{{ date('i', strtotime($data->info->dates))[0] }}</span></div>
        </div>
        <div class="wcdiv" style="left:167.35pt; clip:rect(-0.5pt,8.5pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.73pt; top:0.32pt;">{{ date('i', strtotime($data->info->dates))[1] }}</span></div>
        </div>
        <div class="wcdiv" style="left:185.55pt; clip:rect(-1pt,142.7pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">3. Количество поврежденных ТС</span></div>
        </div>
        <div class="wcdiv" style="left:327.25pt; clip:rect(-0.5pt,23.85pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:8.4pt; top:0.32pt;">{{ $data->info->numberDamaged }}</span></div>
        </div>
        <div class="wcdiv" style="top:9.12pt;">
          <div class="wcdiv" style="left:51.05pt; clip:rect(-1pt,78.95pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:6pt; left:16.94pt; top:0.25pt;">день, месяц, год</span></div>
          </div>
          <div class="wcdiv" style="left:136.1pt; clip:rect(-1pt,40.2pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:6pt; left:0.58pt; top:0.25pt;">часы, минуты</span></div>
          </div>
          <div class="wcdiv" style="left:327.45pt; clip:rect(-1pt,24.15pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:6pt; left:2.73pt; top:0.25pt;">число</span></div>
          </div>
        </div>
        <div class="wcdiv" style="top:16.52pt;">
          <div class="wcdiv" style="clip:rect(-1pt,258.85pt,11.12pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">4. Количество раненых (лиц, получивших телесные повреждения)</span></div>
          </div>
          <div class="wcdiv" style="left:257.85pt; clip:rect(-0.5pt,23.95pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:7.5pt; left:8.45pt; top:0.32pt;">{{ $data->info->numberInjured }}</span></div>
          </div>
          <div class="wcdiv" style="left:281.05pt; clip:rect(-1pt,47.85pt,11.12pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:7.5pt; left:5.92pt; top:0.32pt;">погибших</span></div>
          </div>
          <div class="wcdiv" style="left:327.9pt; clip:rect(-0.5pt,23.95pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:7.5pt; left:8.45pt; top:0.32pt;">{{ $data->info->numberDead }}</span></div>
          </div>
        </div>
        <div class="wcdiv" style="top:25.65pt;">
          <div class="wcdiv" style="left:258pt; clip:rect(-1pt,24.25pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:6pt; left:2.78pt; top:0.25pt;">число</span></div>
          </div>
          <div class="wcdiv" style="left:328.05pt; clip:rect(-1pt,24.25pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:6pt; left:2.78pt; top:0.25pt;">число</span></div>
          </div>
        </div>
        <div class="wcdiv" style="top:33.05pt;">
          <div class="wcdiv" style="clip:rect(-1pt,299.1pt,11.12pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">5. Проводилось ли освидетельствование участников ДТП на состояние опьянения</span></div>
          </div>
          <div class="wcdiv" style="left:298.1pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">{{ $data->info->examinationIntoxication ? '' : 'X' }}</span></div>
          </div>
          <div class="wcdiv" style="left:306.05pt; clip:rect(-1pt,22.2pt,11.12pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">Да</span></div>
          </div>
          <div class="wcdiv" style="left:327.25pt; clip:rect(-0.5pt,8.75pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.02pt; top:0.32pt;">{{ $data->info->examinationIntoxication ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:335.25pt; clip:rect(-1pt,22.3pt,11.12pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">Нет</span></div>
          </div>
        </div>
        <div class="wcdiv" style="left:50.8pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:58.55pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:66.3pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:74.05pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:81.85pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:89.6pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:97.35pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:105.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:112.9pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:120.65pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:128.45pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:136.2pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:143.95pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:151.75pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:159.5pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:167.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:175.05pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:327.2pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:350.35pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:257.75pt; top:16.52pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:281pt; top:17.02pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:327.8pt; top:16.52pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:351.05pt; top:17.02pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:298pt; top:33.05pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:305.95pt; top:33.55pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:327.2pt; top:33.05pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:335.15pt; top:33.55pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:50.8pt; top:0pt; width:78.15pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:136.2pt; top:0pt; width:39.35pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:327.2pt; top:0pt; width:23.65pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:51.05pt; top:9.12pt; width:77.65pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:136.45pt; top:9.12pt; width:38.85pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:327.45pt; top:9.12pt; width:23.15pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:257.75pt; top:16.52pt; width:23.75pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:327.8pt; top:16.52pt; width:23.75pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:258pt; top:25.65pt; width:23.25pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:328.05pt; top:25.65pt; width:23.25pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:298pt; top:33.05pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:327.2pt; top:33.05pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:298pt; top:42.17pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:327.2pt; top:42.17pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="left:297.7pt; top:109.97pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:3.32pt; top:0.25pt;">нужное отметить</span></div>
      <div class="wcdiv" style="left:-1.4pt; top:116.86pt; z-index:1;">
        <div class="wcdiv" style="clip:rect(-1pt,314.55pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">6. Материальный ущерб, нанесенный другим транспортным средствам (кроме "А" и "В")</span></div>
        </div>
        <div class="wcdiv" style="left:313.55pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">{{ $data->info->materialDamageVehicle ? '' : 'X' }}</span></div>
        </div>
        <div class="wcdiv" style="left:321.5pt; clip:rect(-1pt,22.25pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">Да</span></div>
        </div>
        <div class="wcdiv" style="left:342.75pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">{{ $data->info->materialDamageVehicle ? 'X' : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:350.7pt; clip:rect(-1pt,41.55pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">Нет</span></div>
        </div>
        <div class="wcdiv" style="left:391.25pt; clip:rect(-1pt,80.95pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">другому имуществу</span></div>
        </div>
        <div class="wcdiv" style="left:471.2pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">{{ $data->info->materialDamageProperty ? '' : 'X' }}</span></div>
        </div>
        <div class="wcdiv" style="left:479.15pt; clip:rect(-1pt,18.65pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">Да</span></div>
        </div>
        <div class="wcdiv" style="left:496.8pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">{{ $data->info->materialDamageProperty ? 'X' : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:504.7pt; clip:rect(-1pt,22.3pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">Нет</span></div>
        </div>
        <div class="wcdiv" style="top:9.12pt;">
          <div class="wcdiv" style="left:313.25pt; clip:rect(-1pt,57.7pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:6pt; left:4.95pt; top:0.25pt;">нужное отметить</span></div>
          </div>
          <div class="wcdiv" style="left:469.2pt; clip:rect(-1pt,57.85pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:6pt; left:5.02pt; top:0.25pt;">нужное отметить</span></div>
          </div>
        </div>
        <div class="wcdiv" style="left:313.3pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:321.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:342.5pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:350.45pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:470.95pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:478.9pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:496.5pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:504.45pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:313.3pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:342.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:470.95pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:496.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:313.3pt; top:9.12pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:342.5pt; top:9.12pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:470.95pt; top:9.12pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:496.5pt; top:9.12pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="top:133.39pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">7. Свидетели ДТП:</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:60.64pt; top:0.32pt;">&nbsp;</span></div>
      <div class="wcdiv" style="top:133.39pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:70pt; top:0.32pt;">{{ str_split_by($data->info->witnesses, 120, 2, 1) }}</span></div>
      <div class="wcdiv" style="top:151pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:10pt; top:0.32pt;">{{ str_split_by($data->info->witnesses, 120, 2, 2) }}</span></div>
      <div class="wcdiv" style="left:63.8pt; top:142.01pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:166.14pt; top:1.75pt;">(фамилия, имя, отчество, адрес места жительства)</span></div>
      <div class="wcdiv" style="left:62.35pt; top:142.01pt; width:463.5pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:159.61pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.4pt; top:162.26pt; z-index:1;">
        <div class="wcdiv" style="clip:rect(-1pt,314.55pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">8. Проводилось ли оформление сотрудником ГИБДД</span></div>
        </div>
        <div class="wcdiv" style="left:313.55pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">{{ $data->info->clearanceEmployee ? 'X' : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:321.45pt; clip:rect(-1pt,29.35pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">Нет</span></div>
        </div>
        <div class="wcdiv" style="left:349.8pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">{{ $data->info->clearanceEmployee ? '' : 'X' }}</span></div>
        </div>
        <div class="wcdiv" style="left:357.7pt; clip:rect(-1pt,29.4pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0pt; top:0.32pt;">Да</span></div>
        </div>
        <div class="wcdiv" style="left:386.1pt; clip:rect(-0.5pt,85.75pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:29.98pt; top:0.32pt;">{{ $data->info->numberEmployee }}</span></div>
        </div>
        <div class="wcdiv" style="top:9.12pt;">
          <div class="wcdiv" style="left:313.55pt; clip:rect(-1pt,73.55pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; left:12.87pt; top:0.25pt;">нужное отметить</span></div>
          </div>
          <div class="wcdiv" style="left:386.1pt; clip:rect(-1pt,86pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; left:10.32pt; top:0.25pt;">номер нагрудного знака</span></div>
          </div>
        </div>
        <div class="wcdiv" style="left:313.3pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:321.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:349.6pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:357.55pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:385.9pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:470.95pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:313.3pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:349.6pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:385.9pt; top:0pt; width:85.55pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:313.55pt; top:9.12pt; width:7.95pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:349.6pt; top:9.12pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:386.15pt; top:9.12pt; width:85.05pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
      </div>
    </div>
    <div class="wcdiv" style="left:42.55pt; top:216.03pt; z-index:0;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:2.32pt;">Транспортное средство "А"&nbsp;</span> <span class="wcspan wctext001" style="font-size:5pt; left:90.17pt; top:2.27pt;">*</span>
      <div class="wcdiv" style="top:10.62pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">9. Марка, модель ТС</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:66.23pt; top:0.32pt;">&nbsp;{{ $data->vehicle_a->mark }}, {{ $data->vehicle_a->model }}</span></div>
      <div class="wcdiv" style="left:68pt; top:19.25pt; width:98.78pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:30.52pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="top:33.17pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">Идентификационный номер (</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:94.41pt; top:0.32pt;">VIN) ТС</span></div>
      <div class="wcdiv" style="left:-1.4pt; top:41.8pt; z-index:1;">
        @php($a_vin = $data->vehicle_a->vin)
        <div class="wcdiv" style="clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.68pt; top:0.32pt;">{{isset($a_vin[0]) ? $a_vin[0]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($a_vin[1]) ? $a_vin[1]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:15.85pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($a_vin[2]) ? $a_vin[2]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:23.8pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($a_vin[3]) ? $a_vin[3]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:31.75pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.68pt; top:0.32pt;">{{isset($a_vin[4]) ? $a_vin[4]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:39.65pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($a_vin[5]) ? $a_vin[5]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:47.6pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($a_vin[6]) ? $a_vin[6]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:55.55pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($a_vin[7]) ? $a_vin[7]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:63.5pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.68pt; top:0.32pt;">{{isset($a_vin[8]) ? $a_vin[8]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:71.4pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($a_vin[9]) ? $a_vin[9]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:79.35pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($a_vin[10]) ? $a_vin[10]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:87.3pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($a_vin[11]) ? $a_vin[11]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:95.25pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.68pt; top:0.32pt;">{{isset($a_vin[12]) ? $a_vin[12]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:103.15pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($a_vin[13]) ? $a_vin[13]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:111.1pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($a_vin[14]) ? $a_vin[14]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:119.05pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($a_vin[15]) ? $a_vin[15]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:127pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($a_vin[16]) ? $a_vin[16]  : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:15.65pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:23.6pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:31.55pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:39.5pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:47.45pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:55.4pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:63.35pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:71.3pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:79.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:87.2pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:95.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:103.1pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:111.05pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:119pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:126.95pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:134.9pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:-0.25pt; top:0pt; width:135.65pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:-0.25pt; top:9.12pt; width:135.65pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="left:-1.4pt; top:53.72pt; z-index:1;">
        <div class="wcdiv" style="clip:rect(-1pt,100.25pt,11pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:5pt; color:#00000a; left:0pt; top:2.96pt;">Государственный регистрационный знак ТС</span></div>
        </div>
        @php($a_regnum = $data->vehicle_a->regNumber)
        <div class="wcdiv" style="left:99.25pt; clip:rect(-0.5pt,8.4pt,11pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.93pt; top:0.75pt;">{{ isset($a_regnum[0]) ? $a_regnum[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:106.9pt; clip:rect(-0.5pt,8.45pt,11pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.95pt; top:0.75pt;">{{ isset($a_regnum[1]) ? $a_regnum[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:114.6pt; clip:rect(-0.5pt,8.4pt,11pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.93pt; top:0.75pt;">{{ isset($a_regnum[2]) ? $a_regnum[2] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:122.25pt; clip:rect(-0.5pt,8.4pt,11pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.93pt; top:0.75pt;">{{ isset($a_regnum[3]) ? $a_regnum[3] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:129.9pt; clip:rect(-0.5pt,8.4pt,11pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.93pt; top:0.75pt;">{{ isset($a_regnum[4]) ? $a_regnum[4] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:137.55pt; clip:rect(-0.5pt,8.4pt,11pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.93pt; top:0.75pt;">{{ isset($a_regnum[5]) ? $a_regnum[5] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:145.2pt; clip:rect(-0.5pt,8.4pt,11pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.93pt; top:0.75pt;">{{ isset($a_regnum[6]) ? $a_regnum[6] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:152.85pt; clip:rect(-0.5pt,8.1pt,11pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.77pt; top:0.75pt;">{{ isset($a_regnum[7]) ? $a_regnum[7] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:160.2pt; clip:rect(-0.5pt,8.05pt,11pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.75pt; top:0.75pt;">{{ isset($a_regnum[8]) ? $a_regnum[8] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:99pt; top:0pt; width:0pt; height:9pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:106.65pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:114.35pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:122pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:129.65pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:137.3pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:144.95pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:152.6pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:159.95pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:167.25pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:99pt; top:0pt; width:68.75pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:99pt; top:9pt; width:68.75pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="left:-1.4pt; top:65.52pt; z-index:1;">
        <div class="wcdiv" style="clip:rect(-1pt,84.3pt,10.55pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:5pt; color:#00000a; left:0pt; top:2.51pt;">Свидетельство о регистрации ТС</span></div>
        </div>
        @php($a_stsser = $data->vehicle_a->stsSeries)
        @php($a_stsnum = $data->vehicle_a->stsNumber)
        <div class="wcdiv" style="left:83.3pt; clip:rect(-0.5pt,8.7pt,10.55pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.08pt; top:0.3pt;">{{ isset($a_stsser[0]) ? $a_stsser[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:91.25pt; clip:rect(-0.5pt,8.65pt,10.55pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.05pt; top:0.3pt;">{{ isset($a_stsser[1]) ? $a_stsser[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:99.15pt; clip:rect(-0.5pt,8.7pt,10.55pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.08pt; top:0.3pt;">{{ isset($a_stsser[2]) ? $a_stsser[2] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:107.1pt; clip:rect(-0.5pt,8.65pt,10.55pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.05pt; top:0.3pt;">{{ isset($a_stsser[3]) ? $a_stsser[3] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:119.65pt; clip:rect(-0.5pt,8.65pt,10.55pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.05pt; top:0.3pt;">{{ isset($a_stsnum[0]) ? $a_stsnum[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:127.55pt; clip:rect(-0.5pt,8.7pt,10.55pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.08pt; top:0.3pt;">{{ isset($a_stsnum[1]) ? $a_stsnum[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:135.5pt; clip:rect(-0.5pt,8.65pt,10.55pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.05pt; top:0.3pt;">{{ isset($a_stsnum[2]) ? $a_stsnum[2] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:143.4pt; clip:rect(-0.5pt,8.7pt,10.55pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.08pt; top:0.3pt;">{{ isset($a_stsnum[3]) ? $a_stsnum[3] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:151.35pt; clip:rect(-0.5pt,8.65pt,10.55pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.05pt; top:0.3pt;">{{ isset($a_stsnum[4]) ? $a_stsnum[4] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:159.25pt; clip:rect(-0.5pt,8.7pt,10.55pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.08pt; top:0.3pt;">{{ isset($a_stsnum[5]) ? $a_stsnum[5] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="top:8.55pt;">
          <div class="wcdiv" style="left:83.3pt; clip:rect(-1pt,32.7pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:7.3pt; top:0.25pt;">серия</span></div>
          </div>
          <div class="wcdiv" style="left:119.65pt; clip:rect(-1pt,48.55pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:14.54pt; top:0.25pt;">номер</span></div>
          </div>
        </div>
        <div class="wcdiv" style="left:83.05pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:91pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:98.9pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:106.85pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:114.75pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:119.4pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:127.3pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:135.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:143.15pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:151.1pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:159pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:166.95pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:83.05pt; top:0pt; width:32.2pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:119.4pt; top:0pt; width:48.05pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:83.3pt; top:8.55pt; width:31.7pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:119.65pt; top:8.55pt; width:47.55pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="top:81.47pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">10. Собственник ТС</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:64.44pt; top:0.32pt;">&nbsp;{{ $data->vehicle_a->owner }}</span></div>
      <div class="wcdiv" style="left:68.05pt; top:90.09pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:35.63pt; top:1.75pt;">(фамилия,</span></div>
      <div class="wcdiv" style="left:66.6pt; top:90.09pt; width:100.18pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="top:107.11pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:7.49pt; top:1.75pt;">имя, отчество (полное наименование юридического лица))</span></div>
      <div class="wcdiv" style="left:-1.45pt; top:107.11pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="top:115.51pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">Адрес</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:19.64pt; top:0.32pt;">&nbsp;{{ $data->vehicle_a->ownerAddress }}</span></div>
      <div class="wcdiv" style="left:22.65pt; top:124.14pt; width:144.13pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:135.41pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="top:138.06pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">11. Водитель ТС</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:53.06pt; top:0.32pt;">&nbsp;{{ $data->vehicle_a->driver }}</span></div>
      <div class="wcdiv" style="left:56.7pt; top:146.69pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:21.96pt; top:1.75pt;">(фамилия, имя, отчество)</span></div>
      <div class="wcdiv" style="left:55.25pt; top:146.69pt; width:111.53pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:163.71pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.4pt; top:168.36pt; z-index:1;">
        @php($a_driverDateBirth = $data->vehicle_a->driverDateBirth)
        <div class="wcdiv" style="clip:rect(-1pt,87.55pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">Дата рождения</span></div>
        </div>
        <div class="wcdiv" style="left:86.55pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverDateBirth) ? date('d', strtotime($a_driverDateBirth))[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:94.5pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverDateBirth) ? date('d', strtotime($a_driverDateBirth))[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:102.45pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:1.89pt; top:0.32pt;">.</span></div>
        </div>
        <div class="wcdiv" style="left:110.4pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverDateBirth) ? date('m', strtotime($a_driverDateBirth))[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:118.35pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_driverDateBirth) ? date('m', strtotime($a_driverDateBirth))[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:126.25pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:1.89pt; top:0.32pt;">.</span></div>
        </div>
        <div class="wcdiv" style="left:134.2pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverDateBirth) ? date('Y', strtotime($a_driverDateBirth))[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:142.15pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_driverDateBirth) ? date('Y', strtotime($a_driverDateBirth))[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:150.05pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverDateBirth) ? date('Y', strtotime($a_driverDateBirth))[2] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:158pt; clip:rect(-0.5pt,8.6pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.9pt; top:0.32pt;">{{ isset($a_driverDateBirth) ? date('Y', strtotime($a_driverDateBirth))[3] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:86.3pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:94.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:102.2pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:110.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:118.1pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:126pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:133.95pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:141.9pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:149.8pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:157.75pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:165.6pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:86.3pt; top:0pt; width:79.8pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:86.3pt; top:9.12pt; width:79.8pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="left:85.05pt; top:177.98pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:19.51pt; top:0.25pt;">день, месяц, год</span></div>
      <div class="wcdiv" style="top:184.88pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">Адрес</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:19.64pt; top:0.32pt;">&nbsp;{{ $data->vehicle_a->driverAddress }}</span></div>
      <div class="wcdiv" style="left:22.65pt; top:193.51pt; width:144.13pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:204.78pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.4pt; top:209.43pt; z-index:1;">
        @php($a_driverPhone = $data->vehicle_a->driverPhone)
        <div class="wcdiv" style="clip:rect(-1pt,47.9pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">Телефон</span></div>
        </div>
        <div class="wcdiv" style="left:46.9pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.69pt; top:0.32pt;">{{ isset($a_driverPhone[0]) ? $a_driverPhone[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:54.8pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverPhone[1]) ? $a_driverPhone[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:62.75pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverPhone[2]) ? $a_driverPhone[2] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:70.7pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_driverPhone[3]) ? $a_driverPhone[3] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:78.6pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverPhone[4]) ? $a_driverPhone[4] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:86.55pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverPhone[5]) ? $a_driverPhone[5] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:94.5pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverPhone[6]) ? $a_driverPhone[6] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:102.45pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverPhone[7]) ? $a_driverPhone[7] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:110.4pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverPhone[8]) ? $a_driverPhone[8] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:118.35pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_driverPhone[9]) ? $a_driverPhone[9] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:126.25pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverPhone[10]) ? $a_driverPhone[10] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:134.2pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverPhone[11]) ? $a_driverPhone[11] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:142.15pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverPhone[12]) ? $a_driverPhone[12] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:150.1pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverPhone[13]) ? $a_driverPhone[13] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:158.05pt; clip:rect(-0.5pt,8.55pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.88pt; top:0.32pt;">{{ isset($a_driverPhone[14]) ? $a_driverPhone[14] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:46.65pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:54.55pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:62.5pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:70.45pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:78.35pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:86.3pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:94.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:102.2pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:110.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:118.1pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:126pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:133.95pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:141.9pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:149.85pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:157.8pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:165.6pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:46.65pt; top:0pt; width:119.45pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:46.65pt; top:9.12pt; width:119.45pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="left:-1.4pt; top:221.35pt; z-index:1;">
        @php($a_driverlicenseSeries = $data->vehicle_a->driverlicenseSeries)
        @php($a_driverlicenseNumber = $data->vehicle_a->driverlicenseNumber)
        <div class="wcdiv" style="clip:rect(-1pt,82.6pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:0pt; top:1.98pt;">Водительское удостоверение</span></div>
        </div>
        <div class="wcdiv" style="left:81.6pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverlicenseSeries[0]) ? $a_driverlicenseSeries[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:89.55pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverlicenseSeries[1]) ? $a_driverlicenseSeries[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:97.5pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverlicenseSeries[2]) ? $a_driverlicenseSeries[2] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:105.45pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_driverlicenseSeries[3]) ? $a_driverlicenseSeries[3] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:117.95pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverlicenseNumber[0]) ? $a_driverlicenseNumber[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:125.9pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_driverlicenseNumber[1]) ? $a_driverlicenseNumber[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:133.8pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverlicenseNumber[2]) ? $a_driverlicenseNumber[2] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:141.75pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_driverlicenseNumber[3]) ? $a_driverlicenseNumber[3] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:149.65pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverlicenseNumber[4]) ? $a_driverlicenseNumber[4] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:157.6pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_driverlicenseNumber[5]) ? $a_driverlicenseNumber[5] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="top:9.12pt;">
          <div class="wcdiv" style="left:81.6pt; clip:rect(-1pt,32.75pt,12.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:7.33pt; top:0.25pt;">серия</span></div>
          </div>
          <div class="wcdiv" style="left:117.95pt; clip:rect(-1pt,48.55pt,12.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:14.54pt; top:0.25pt;">номер</span></div>
          </div>
        </div>
        <div class="wcdiv" style="left:81.35pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:89.3pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:97.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:105.2pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:113.1pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:117.7pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:125.65pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:133.55pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:141.5pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:149.4pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:157.35pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:165.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:81.35pt; top:0pt; width:32.25pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:117.7pt; top:0pt; width:48.05pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:81.6pt; top:9.12pt; width:31.75pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:117.95pt; top:9.12pt; width:47.55pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="left:-1.4pt; top:242.03pt; z-index:1;">
        @php($a_driverlicenseDate = $data->vehicle_a->driverlicenseDate)

        <div class="wcdiv" style="clip:rect(-1pt,47.85pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">Категория</span></div>
        </div>
        <div class="wcdiv" style="left:46.85pt; clip:rect(-0.5pt,32.45pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:12.2pt; top:0.32pt;">{{ $data->vehicle_a->category }}</span></div>
        </div>
        <div class="wcdiv" style="left:86.5pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverlicenseDate) ? date('d', strtotime($a_driverlicenseDate))[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:94.45pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverlicenseDate) ? date('d', strtotime($a_driverlicenseDate))[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:102.4pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:1.89pt; top:0.32pt;">.</span></div>
        </div>
        <div class="wcdiv" style="left:110.35pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverlicenseDate) ? date('m', strtotime($a_driverlicenseDate))[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:118.3pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_driverlicenseDate) ? date('m', strtotime($a_driverlicenseDate))[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:126.2pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:1.89pt; top:0.32pt;">.</span></div>
        </div>
        <div class="wcdiv" style="left:134.15pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverlicenseDate) ? date('Y', strtotime($a_driverlicenseDate))[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:142.1pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_driverlicenseDate) ? date('Y', strtotime($a_driverlicenseDate))[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:150pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_driverlicenseDate) ? date('Y', strtotime($a_driverlicenseDate))[2] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:157.95pt; clip:rect(-0.5pt,8.6pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.9pt; top:0.32pt;">{{ isset($a_driverlicenseDate) ? date('Y', strtotime($a_driverlicenseDate))[3] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="top:9.12pt;">
          <div class="wcdiv" style="left:46.85pt; clip:rect(-1pt,32.7pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:1.28pt; top:0.25pt;">A B C D E</span></div>
          </div>
          <div class="wcdiv" style="left:86.5pt; clip:rect(-1pt,80.3pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:22.59pt; top:0.25pt;">дата выдачи</span></div>
          </div>
        </div>
        <div class="wcdiv" style="left:46.6pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:78.3pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:86.25pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:94.2pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:102.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:110.1pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:118.05pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:125.95pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:133.9pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:141.85pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:149.75pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:157.7pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:165.55pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:46.6pt; top:0pt; width:32.2pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:86.25pt; top:0pt; width:79.8pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:46.85pt; top:9.12pt; width:31.7pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:86.5pt; top:9.12pt; width:79.3pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="top:258.55pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">Документ на право владения, пользования,</span> <span class="wcspan wctext001" style="font-size:8pt; left:137.82pt; top:-0.13pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:8.94pt;">распоряжения ТС</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:56.82pt; top:8.94pt;">&nbsp;{{ $data->vehicle_a->titleDeed }}</span></div>
      <div class="wcdiv" style="left:60.4pt; top:275.8pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:0.64pt; top:1.75pt;">(доверенность, договор аренды, путевой&nbsp;</span> <span class="wcspan wctext001" style="font-size:6pt; left:38.2pt; top:8.65pt;">лист и т.п.)</span></div>
      <div class="wcdiv" style="left:58.95pt; top:275.8pt; width:107.83pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="top:291.1pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">12. Страховщик</span></div>
      <div class="wcdiv" style="top:299.34pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:0.4pt; top:1.75pt;">{{ $data->vehicle_a->sk }}</span></div>
      <div class="wcdiv" style="top:308.34pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:0.4pt; top:1.75pt;">(наименование страховщика, застраховавшего ответственность)</span></div>
      <div class="wcdiv" style="left:-1.45pt; top:308.34pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.4pt; top:318.74pt; z-index:1;">
        @php($a_bsoSeries = $data->vehicle_a->bsoSeries)
        @php($a_bsoNumber = $data->vehicle_a->bsoNumber)
        <div class="wcdiv" style="clip:rect(-1pt,59.1pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">Страховой полис</span></div>
        </div>
        <div class="wcdiv" style="left:58.1pt; clip:rect(-0.5pt,8.05pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">{{ isset($a_bsoSeries[0]) ? $a_bsoSeries[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:65.4pt; clip:rect(-0.5pt,8.1pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">{{ isset($a_bsoSeries[1]) ? $a_bsoSeries[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:72.75pt; clip:rect(-0.5pt,8.1pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">{{ isset($a_bsoSeries[2]) ? $a_bsoSeries[2] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:88pt; clip:rect(-0.5pt,8.75pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.98pt; top:0.32pt;">{{ isset($a_bsoNumber[0]) ? $a_bsoNumber[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:96pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_bsoNumber[1]) ? $a_bsoNumber[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:103.95pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_bsoNumber[2]) ? $a_bsoNumber[2] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:111.85pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_bsoNumber[3]) ? $a_bsoNumber[3] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:119.75pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_bsoNumber[4]) ? $a_bsoNumber[4] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:127.7pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_bsoNumber[5]) ? $a_bsoNumber[5] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:135.65pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_bsoNumber[6]) ? $a_bsoNumber[6] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:143.6pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_bsoNumber[7]) ? $a_bsoNumber[7] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:151.5pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_bsoNumber[8]) ? $a_bsoNumber[8] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:159.4pt; clip:rect(-0.5pt,8.8pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:1pt; top:0.32pt;">{{ isset($a_bsoNumber[9]) ? $a_bsoNumber[9] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="top:9.12pt;">
          <div class="wcdiv" style="left:58.1pt; clip:rect(-1pt,23pt,12.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:2.45pt; top:0.25pt;">серия</span></div>
          </div>
          <div class="wcdiv" style="left:88pt; clip:rect(-1pt,80.45pt,12.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:30.49pt; top:0.25pt;">номер</span></div>
          </div>
        </div>
        <div class="wcdiv" style="left:57.85pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:65.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:72.5pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:79.85pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:87.75pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:95.75pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:103.7pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:111.6pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:119.5pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:127.45pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:135.4pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:143.35pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:151.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:159.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:167.2pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:57.85pt; top:0pt; width:22.5pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:87.75pt; top:0pt; width:79.95pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:58.1pt; top:9.12pt; width:22pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
        <div class="wcdiv" style="left:88pt; top:9.12pt; width:79.45pt; height:0pt; border-top:solid 0.75pt #00000a;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="left:-1.4pt; top:339.42pt; z-index:1;">
        @php($a_contractEndData = $data->vehicle_a->contractEndData)
        <div class="wcdiv" style="clip:rect(-1pt,88.9pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">Действителен до</span></div>
        </div>
        <div class="wcdiv" style="left:87.9pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_contractEndData) ? date('d', strtotime($a_contractEndData))[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:95.85pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_contractEndData) ? date('d', strtotime($a_contractEndData))[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:103.75pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:1.89pt; top:0.32pt;">.</span></div>
        </div>
        <div class="wcdiv" style="left:111.7pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_contractEndData) ? date('m', strtotime($a_contractEndData))[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:119.65pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_contractEndData) ? date('m', strtotime($a_contractEndData))[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:127.55pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:1.89pt; top:0.32pt;">.</span></div>
        </div>
        <div class="wcdiv" style="left:135.5pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_contractEndData) ? date('Y', strtotime($a_contractEndData))[0] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:143.45pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($a_contractEndData) ? date('Y', strtotime($a_contractEndData))[1] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:151.35pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_contractEndData) ? date('Y', strtotime($a_contractEndData))[2] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:159.3pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($a_contractEndData) ? date('Y', strtotime($a_contractEndData))[3] : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:87.65pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:95.6pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:103.55pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:111.5pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:119.45pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:127.4pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:135.35pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:143.3pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:151.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:159.2pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:167.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:87.65pt; top:0pt; width:80pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:87.65pt; top:9.12pt; width:80pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="left:85.05pt; top:349.04pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:19.51pt; top:0.25pt;">день, месяц, год</span></div>
      <div class="wcdiv" style="left:-1.4pt; top:356.94pt; z-index:1;">
        <div class="wcdiv" style="clip:rect(-1pt,111pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">ТС застраховано от ущерба</span></div>
        </div>
        <div class="wcdiv" style="left:109.95pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.09pt; top:0.32pt;">{{ $data->vehicle_a->contractInsuredDamage ? '' : 'X' }}</span></div>
        </div>
        <div class="wcdiv" style="left:117.85pt; clip:rect(-1pt,24.85pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:4.51pt; top:0.32pt;">Нет</span></div>
        </div>
        <div class="wcdiv" style="left:141.7pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
          <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.09pt; top:0.32pt;">{{ $data->vehicle_a->contractInsuredDamage ? 'X' : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:149.6pt; clip:rect(-1pt,17pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:2.38pt; top:0.32pt;">Да</span></div>
        </div>
        <div class="wcdiv" style="left:109.75pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:117.7pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:141.5pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:149.45pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:109.75pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:141.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:109.75pt; top:9.12pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:141.5pt; top:9.12pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="top:366.56pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">13. Место первоначального удара</span></div>
      <div class="wcdiv" style="left:78pt; top:375.19pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:0.69pt;">Указать стрелкой (</span> <span class="wcspan wctext003" style="font-size:6pt; left:48.28pt; top:0pt; font-family:serif;">→</span> <span class="wcspan wctext001" style="font-size:6pt; left:54.21pt; top:0.69pt;">)</span></div>
      <div class="wcdiv" style="left:35.45pt; top:450.53pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">14. Характер и перечень видимых&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:8.94pt;">поврежденных деталей и элементов</span></div>
      <div class="wcdiv" style="top:468pt; z-index:1;">
        <span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:1.75pt;">{{ str_split_by($data->vehicle_a->infoDamage, 45, 6, 1) }}</span>
      </div>
      <div class="wcdiv" style="top:479pt; z-index:1;">
        <span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:1.75pt;">{{ str_split_by($data->vehicle_a->infoDamage, 45, 6, 2) }}</span>
      </div>
      <div class="wcdiv" style="top:490pt; z-index:1;">
        <span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:1.75pt;">{{ str_split_by($data->vehicle_a->infoDamage, 45, 6, 3) }}</span>
      </div>
      <div class="wcdiv" style="top:501pt; z-index:1;">
        <span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:1.75pt;">{{ str_split_by($data->vehicle_a->infoDamage, 45, 6, 4) }}</span>
      </div>
      <div class="wcdiv" style="top:512pt; z-index:1;">
        <span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:1.75pt;">{{ str_split_by($data->vehicle_a->infoDamage, 45, 6, 5) }}</span>
      </div>
      <div class="wcdiv" style="top:523pt; z-index:1;">
        <span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:1.75pt;">{{ str_split_by($data->vehicle_a->infoDamage, 45, 6, 6) }}</span>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:477.4pt; width:137.63pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:488.68pt; width:137.63pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:499.95pt; width:137.63pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:511.22pt; width:137.63pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:522.5pt; width:137.63pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:533.77pt; width:137.63pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="top:536.42pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">15. Замечания</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:44.99pt; top:0.32pt;">&nbsp;</span></div>
      <div class="wcdiv" style="top:468pt; z-index:1;">
        <span class="wcspan wctext001" style="font-size:6pt; left:48pt; top:70.75pt;">{{ str_split_by($data->vehicle_a->comment, 37, 3, 1) }}</span>
      </div>
      <div class="wcdiv" style="top:479pt; z-index:1;">
        <span class="wcspan wctext001" style="font-size:6pt; left:5pt; top:70.75pt;">{{ str_split_by($data->vehicle_a->comment, 37, 3, 2) }}</span>
      </div>
      <div class="wcdiv" style="left:47.05pt; top:545.05pt; width:119.73pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:556.32pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="top:558.97pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:8.32pt;">Подпись водителя ТС "А"*</span></div>
      <div class="wcdiv" style="left:-1.45pt; top:575.59pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="top:578.24pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:0.25pt;">* Составляется водителем транспортного</span></div>
      <div class="wcdiv" style="left:3.7pt; top:585.14pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:0.25pt;">средства "А" в отношении своего ТС.</span></div>
      <div class="wcdiv" style="left:179.53pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:2.32pt;">"А"&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:19.45pt; top:2.32pt;">16. Обстоятельства ДТП&nbsp;</span> <span class="wcspan wctext001" style="font-size:6pt; left:99.85pt; top:3.66pt;">(нужное отметить)</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:153.32pt; top:2.32pt;">"В"</span>
        <div class="wcdiv" style="left:-1.4pt; top:10.62pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->parking->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">1</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">1</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->parking->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.95pt; clip:rect(-1pt,12.35pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:5.65pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:0pt; top:0.3pt;"></span></div>
          </div>
          <div class="wcdiv" style="left:147.45pt; clip:rect(-1pt,12.35pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:0.8pt; top:0.3pt;"></span></div>
          </div>
          <div class="wcdiv" style="top:8.55pt;">
            <div class="wcdiv" style="left:19.3pt; top:-8.55pt; clip:rect(-1pt,129.15pt,18.6pt,-1pt);">
              <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:5;"><span class="wcspan wctext001" style="font-size:7pt; left:7.2pt; top:0.3pt;">ТС находилось на стоянке, парковке,&nbsp;</span> <span class="wcspan wctext001" style="font-size:7pt; left:1.3pt; top:8.35pt;">обочине и т.п. в неподвижном состоянии</span></div>
            </div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.5pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:0pt; top:8.55pt; width:7.95pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.8pt; top:8.55pt; width:7.95pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:28.37pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->driverAbsent->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">2</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">2</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->driverAbsent->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:19.3pt; clip:rect(-1pt,129.15pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:6.57pt; top:0.3pt;">Водитель отсутствовал на месте ДТП</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.5pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:39.72pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->movedParking->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">3</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">3</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->movedParking->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:19.3pt; clip:rect(-1pt,129.15pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:32.12pt; top:0.3pt;">Двигался на стоянке</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.5pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:51.07pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->droveParking->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">4</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">4</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->droveParking->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:19.3pt; clip:rect(-1pt,129.15pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:4.2pt; top:0.3pt;">Выезжал со стоянки, с места парковки,</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.5pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:60.12pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7pt; left:16.19pt; top:0.3pt;">остановки, со двора, второстепенной дороги</span></div>
        <div class="wcdiv" style="left:-1.4pt; top:68.17pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->comeParking->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">5</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">5</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->comeParking->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:19.3pt; clip:rect(-1pt,129.15pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:4.45pt; top:0.3pt;">Заезжал на стоянку, парковку, во двор,</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.5pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:77.22pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7pt; left:42.67pt; top:0.3pt;">на второстепенную дорогу</span></div>
        <div class="wcdiv" style="left:-1.4pt; top:85.27pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->movedStraight->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">6</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">6</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->movedStraight->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:19.3pt; clip:rect(-1pt,129.15pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:10.36pt; top:0.3pt;">Двигался прямо (не маневрировал)</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.5pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:96.61pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->movedCrossroads->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">7</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">7</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->movedCrossroads->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:19.3pt; clip:rect(-1pt,129.15pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:25.61pt; top:0.3pt;">Двигался на перекрестке</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.5pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:107.96pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->comeCrossroads->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">8</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">8</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->comeCrossroads->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:19.3pt; clip:rect(-1pt,129.15pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:27.27pt; top:0.3pt;">Заезжал на перекресток</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.5pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:117.01pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7pt; left:47.97pt; top:0.3pt;">с круговым движением</span></div>
        <div class="wcdiv" style="left:-1.4pt; top:125.06pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->movedByCrossroads->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">9</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">9</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->movedByCrossroads->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:19.3pt; clip:rect(-1pt,129.15pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:25.22pt; top:0.3pt;">Двигался по перекрестку</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.5pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.55pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:134.11pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7pt; left:47.97pt; top:0.3pt;">с круговым движением</span></div>
        <div class="wcdiv" style="left:-1.4pt; top:142.16pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->movedSameDirectionSameLane->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">10</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">10</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->movedSameDirectionSameLane->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:10.84pt; top:0.3pt;">Столкнулся с ТС, двигавшимся</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.45pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:151.21pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7pt; left:34.38pt; top:0.3pt;">в том же направлении по той же</span> <span class="wcspan wctext001" style="font-size:7pt; left:72.44pt; top:8.35pt;">полосе</span></div>
        <div class="wcdiv" style="left:-1.4pt; top:167.31pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->movedSameDirectionOtherLane->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">11</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">11</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->movedSameDirectionOtherLane->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:10.84pt; top:0.3pt;">Столкнулся с ТС, двигавшимся</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.45pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:176.36pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7pt; left:34.03pt; top:0.3pt;">в том же направлении по другой</span> <span class="wcspan wctext001" style="font-size:7pt; left:48.26pt; top:8.35pt;">полосе (в другом ряду)</span></div>
        <div class="wcdiv" style="left:-1.4pt; top:192.45pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->changedLane->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">12</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">12</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->changedLane->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:36.66pt; top:0.3pt;">Менял полосу</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.45pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:201.5pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7pt; left:37.47pt; top:0.3pt;">(перестраивался в другой ряд)</span></div>
        <div class="wcdiv" style="left:-1.4pt; top:209.55pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->outrun->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">13</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">13</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->outrun->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:45.13pt; top:0.3pt;">Обгонял</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.45pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:220.9pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->turnedRight->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">14</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">14</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->turnedRight->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:25.4pt; top:0.3pt;">Поворачивал направо</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.45pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:232.25pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->turnedLeft->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">15</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">15</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->turnedLeft->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:27.28pt; top:0.3pt;">Поворачивал налево</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.45pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:243.6pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->madeUTurn->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">16</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">16</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->madeUTurn->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:28.82pt; top:0.3pt;">Совершал разворот</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.45pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:254.95pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->backedUp->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">17</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">17</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->backedUp->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:22.36pt; top:0.3pt;">Двигался задним ходом</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.45pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:266.3pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->wentOncomingTraffic->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">18</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">18</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->wentOncomingTraffic->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:17.34pt; top:0.3pt;">Выехал на сторону дороги,</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.45pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:275.35pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7pt; left:32.55pt; top:0.3pt;">предназначенную для встречного</span> <span class="wcspan wctext001" style="font-size:7pt; left:68.03pt; top:8.35pt;">движения</span></div>
        <div class="wcdiv" style="left:-1.4pt; top:291.44pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->secondVehicleWasLeft->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">19</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">19</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->secondVehicleWasLeft->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:2.52pt; top:0.3pt;">Второе ТС находилось слева от меня</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.45pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:302.79pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->didNotComplyPriorityMarkRequirement->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">20</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">20</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->didNotComplyPriorityMarkRequirement->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:20.28pt; top:0.3pt;">Не выполнил требование</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.45pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:311.84pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7pt; left:56.69pt; top:0.3pt;">знака приоритета</span></div>
        <div class="wcdiv" style="left:-1.4pt; top:319.89pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->madeRunOver->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">21</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">21</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->madeRunOver->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:0.96pt; top:0.3pt;">Совершил наезд (на неподвижное ТС,</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.45pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:328.94pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7pt; left:38.12pt; top:0.3pt;">препятствие, пешехода и т.п.)</span></div>
        <div class="wcdiv" style="left:-1.4pt; top:336.99pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->trafficLightInhibitStopped->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">22</span></div>
          </div>
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">22</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->trafficLightInhibitStopped->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:22.93pt; top:0.3pt;">Остановился (стоял) на</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.45pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:346.04pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7pt; left:34.1pt; top:0.3pt;">запрещающий сигнал светофора</span></div>
        <div class="wcdiv" style="left:-1.4pt; top:354.09pt;">
          <div class="wcdiv">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.27pt; top:0.3pt;">{{ $data->inquirer->other->is_a ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:5.15pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0pt; top:0.3pt;">23</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:14.85pt; top:0.3pt;">Иное (для водителя ТС "А"):</span></div>
          </div>
          <div class="wcdiv" style="left:12.1pt;">
            <div class="wcdiv" style="left:1.4pt; top:10pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:15.05pt; top:0.3pt;">{{ str_split_by($data->inquirer->otherCommentA, 30, 1, 1) }}</span> </div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:0pt; top:8.55pt; width:7.95pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:24.1pt; top:17.1pt; width:118.5pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:372.83pt;">
          <div class="wcdiv" style="left:147.35pt; clip:rect(-0.5pt,12.05pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.25pt; top:0.3pt;">24</span></div>
          </div>
          <div class="wcdiv" style="left:158.65pt;">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.35pt; top:0.3pt;">{{ $data->inquirer->other->is_b ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:24.1pt; clip:rect(-1pt,119.5pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:15.05pt; top:0.3pt;">Иное (для водителя ТС "</span> <span class="wcspan wctext001" style="font-size:7pt; left:88.85pt; top:0.3pt;">B</span> <span class="wcspan wctext001" style="font-size:7pt; left:93.52pt; top:0.3pt;">"):</span></div>
          </div>
          <div class="wcdiv" style="left:12.1pt;">
            <div class="wcdiv" style="left:1.4pt; top:10pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:15.05pt; top:0.3pt;">{{ str_split_by($data->inquirer->otherCommentB, 30, 1, 1) }}</span> </div>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.45pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:158.5pt; top:8.55pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:24.1pt; top:17.1pt; width:118.5pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:392.73pt;">
          <div class="wcdiv" style="left:15.9pt; clip:rect(-1pt,136.25pt,18.6pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7pt; left:17.6pt; top:0.3pt;">Указать количество отмеченных</span> <span class="wcspan wctext001" style="font-size:7pt; left:56.24pt; top:8.35pt;">клеток</span></div>
          </div>
          <?php
          $a_count = 0;
          $b_count = 0;
          $arr_inquirer = (array)$data->inquirer;
          foreach($arr_inquirer as $key => $value){
            if(isset($value->is_a) && $value->is_a){
              $a_count++;
            }
            if(isset($value->is_b) && $value->is_b){
              $b_count++;
            }
          }
          ?>
          <div class="wcdiv" style="left:15.9pt;">
            <div class="wcdiv" style="left:-13pt; top:0.5pt; z-index:4;">{{ $a_count }}</div>
          </div>
          <div class="wcdiv" style="left:15.9pt;">
            <div class="wcdiv" style="left:138pt; top:0.5pt; z-index:4;">{{ $b_count }}</div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:16.1pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:15.65pt; top:0.5pt; width:0pt; height:16.1pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:150.9pt; top:0pt; width:0pt; height:16.6pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.8pt; top:0.5pt; width:0pt; height:16.1pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:16.4pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:150.9pt; top:0pt; width:16.4pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:16.6pt; width:16.4pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:150.9pt; top:16.6pt; width:16.4pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:409.83pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:1.32pt;">17.</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:70.9pt; top:1.32pt;">Схема ДТП</span></div>
        <div class="wcdiv" style="top:522.94pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.59pt;">18. Подписи водителей, удостоверяющие&nbsp;</span> <span class="wcspan wctext003" style="font-size:7.5pt; left:134.38pt; top:-0.26pt;"></span> <span class="wcspan wctext001" style="font-size:7.5pt; left:138.88pt; top:0.59pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:7.03pt;">отсутствие&nbsp;</span> <span class="wcspan wctext003" style="font-size:7.5pt; left:36.75pt; top:6.17pt;"></span> <span class="wcspan wctext001" style="font-size:7.5pt; left:41.25pt; top:7.03pt;">&nbsp;наличие (указываются в п. 7&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:13.08pt;">оборотной стороны Извещения) разногласий</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:144.04pt; top:13.08pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:145.92pt; top:13.08pt;">по п.&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:19.12pt;">14, 15, 16, 17</span></div>
        <div class="wcdiv" style="top:555.88pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:-1.78pt;">Водитель ТС "А"</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:100.19pt; top:-1.78pt;">Водитель ТС "В"</span></div>
        <div class="wcdiv" style="left:-1.4pt; top:563.92pt;">
          <div class="wcdiv" style="top:6.04pt;">
            <div class="wcdiv" style="clip:rect(-1pt,80.4pt,7.33pt,-1pt);">
              <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:5;"><span class="wcspan wctext001" style="font-size:6pt; left:25.76pt; top:-1.43pt;">(подпись)</span></div>
            </div>
            <div class="wcdiv" style="left:87.9pt; clip:rect(-1pt,79.8pt,7.33pt,-1pt);">
              <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:5;"><span class="wcspan wctext001" style="font-size:6pt; left:25.46pt; top:-1.43pt;">(подпись)</span></div>
            </div>
          </div>
          <div class="wcdiv" style="left:0pt; top:6.04pt; width:79.4pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:87.9pt; top:6.04pt; width:78.8pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:575.28pt; z-index:2;"><span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:0.25pt;">Заполняется в случае оформления ДТП без участия сотрудни-</span> <span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:7.15pt;">ков ГИБДД ***. Ничего не изменять после подписания обоими&nbsp;</span> <span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:14.05pt;">водителями и разъединения бланков.</span></div>
      </div>
      <div class="wcdiv" style="left:171.93pt; top:0pt; width:0pt; height:598.94pt; border-left:solid 1pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:359.07pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:2.32pt;">Транспортное средство "В"&nbsp;</span> <span class="wcspan wctext001" style="font-size:5pt; left:89.76pt; top:2.27pt;">**</span>
        <div class="wcdiv" style="top:10.62pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">9. Марка, модель ТС</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:66.23pt; top:0.32pt;">&nbsp;{{ $data->vehicle_b->mark }}, {{ $data->vehicle_b->model }}</span></div>
        <div class="wcdiv" style="left:68pt; top:19.25pt; width:98.78pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:-1.45pt; top:30.52pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="top:33.17pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">Идентификационный номер (</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:94.41pt; top:0.32pt;">VIN) ТС</span></div>
        <div class="wcdiv" style="left:-1.4pt; top:41.8pt; z-index:1;">
          @php($b_vin = $data->vehicle_b->vin)
          <div class="wcdiv" style="clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.68pt; top:0.32pt;">{{isset($b_vin[0]) ? $b_vin[0]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:7.9pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($b_vin[1]) ? $b_vin[1]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:15.85pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($b_vin[2]) ? $b_vin[2]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:23.8pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($b_vin[3]) ? $b_vin[3]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:31.75pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.68pt; top:0.32pt;">{{isset($b_vin[4]) ? $b_vin[4]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:39.65pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($b_vin[5]) ? $b_vin[5]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:47.6pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($b_vin[6]) ? $b_vin[6]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:55.55pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($b_vin[7]) ? $b_vin[7]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:63.5pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.68pt; top:0.32pt;">{{isset($b_vin[8]) ? $b_vin[8]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:71.4pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($b_vin[9]) ? $b_vin[9]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:79.35pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($b_vin[10]) ? $b_vin[10]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:87.3pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($b_vin[11]) ? $b_vin[11]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:95.25pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.68pt; top:0.32pt;">{{isset($b_vin[12]) ? $b_vin[12]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:103.15pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($b_vin[13]) ? $b_vin[13]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:111.1pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($b_vin[14]) ? $b_vin[14]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:119.05pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($b_vin[15]) ? $b_vin[15]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:127pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; left:0.7pt; top:0.32pt;">{{isset($b_vin[16]) ? $b_vin[16]  : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:7.7pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:15.65pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:23.6pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:31.55pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:39.5pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:47.45pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:55.4pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:63.35pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:71.3pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:79.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:87.2pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:95.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:103.1pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:111.05pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:119pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:126.95pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:134.9pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:0pt; width:135.65pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:-0.25pt; top:9.12pt; width:135.65pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:53.72pt; z-index:1;">
          <div class="wcdiv" style="clip:rect(-1pt,100.25pt,11pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:5pt; color:#00000a; left:0pt; top:2.96pt;">Государственный регистрационный знак ТС</span></div>
          </div>
          @php($b_regnum = $data->vehicle_b->regNumber)
          <div class="wcdiv" style="left:99.25pt; clip:rect(-0.5pt,8.4pt,11pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.93pt; top:0.75pt;">{{ isset($b_regnum[0]) ? $b_regnum[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:106.9pt; clip:rect(-0.5pt,8.45pt,11pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.95pt; top:0.75pt;">{{ isset($b_regnum[1]) ? $b_regnum[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:114.6pt; clip:rect(-0.5pt,8.4pt,11pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.93pt; top:0.75pt;">{{ isset($b_regnum[2]) ? $b_regnum[2] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:122.25pt; clip:rect(-0.5pt,8.4pt,11pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.93pt; top:0.75pt;">{{ isset($b_regnum[3]) ? $b_regnum[3] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:129.9pt; clip:rect(-0.5pt,8.4pt,11pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.93pt; top:0.75pt;">{{ isset($b_regnum[4]) ? $b_regnum[4] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:137.55pt; clip:rect(-0.5pt,8.4pt,11pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.93pt; top:0.75pt;">{{ isset($b_regnum[5]) ? $b_regnum[5] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:145.2pt; clip:rect(-0.5pt,8.4pt,11pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.93pt; top:0.75pt;">{{ isset($b_regnum[6]) ? $b_regnum[6] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:152.85pt; clip:rect(-0.5pt,8.1pt,11pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.77pt; top:0.75pt;">{{ isset($b_regnum[7]) ? $b_regnum[7] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:160.2pt; clip:rect(-0.5pt,8.05pt,11pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:0.75pt; top:0.75pt;">{{ isset($b_regnum[8]) ? $b_regnum[8] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:99pt; top:0pt; width:0pt; height:9pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:106.65pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:114.35pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:122pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:129.65pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:137.3pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:144.95pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:152.6pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:159.95pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:167.25pt; top:0.5pt; width:0pt; height:8.5pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:99pt; top:0pt; width:68.75pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:99pt; top:9pt; width:68.75pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:65.52pt; z-index:1;">
          <div class="wcdiv" style="clip:rect(-1pt,84.3pt,10.55pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:5pt; color:#00000a; left:0pt; top:2.51pt;">Свидетельство о регистрации ТС</span></div>
          </div>
          @php($b_stsser = $data->vehicle_b->stsSeries)
          @php($b_stsnum = $data->vehicle_b->stsNumber)
          <div class="wcdiv" style="left:83.3pt; clip:rect(-0.5pt,8.7pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.08pt; top:0.3pt;">{{ isset($b_stsser[0]) ? $b_stsser[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:91.25pt; clip:rect(-0.5pt,8.65pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.05pt; top:0.3pt;">{{ isset($b_stsser[1]) ? $b_stsser[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:99.15pt; clip:rect(-0.5pt,8.7pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.08pt; top:0.3pt;">{{ isset($b_stsser[2]) ? $b_stsser[2] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:107.1pt; clip:rect(-0.5pt,8.65pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.05pt; top:0.3pt;">{{ isset($b_stsser[3]) ? $b_stsser[3] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:119.65pt; clip:rect(-0.5pt,8.65pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.05pt; top:0.3pt;">{{ isset($b_stsnum[0]) ? $b_stsnum[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:127.55pt; clip:rect(-0.5pt,8.7pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.08pt; top:0.3pt;">{{ isset($b_stsnum[1]) ? $b_stsnum[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:135.5pt; clip:rect(-0.5pt,8.65pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.05pt; top:0.3pt;">{{ isset($b_stsnum[2]) ? $b_stsnum[2] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:143.4pt; clip:rect(-0.5pt,8.7pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.08pt; top:0.3pt;">{{ isset($b_stsnum[3]) ? $b_stsnum[3] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:151.35pt; clip:rect(-0.5pt,8.65pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.05pt; top:0.3pt;">{{ isset($b_stsnum[4]) ? $b_stsnum[4] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:159.25pt; clip:rect(-0.5pt,8.7pt,10.55pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7pt; color:#00000a; left:1.08pt; top:0.3pt;">{{ isset($b_stsnum[5]) ? $b_stsnum[5] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="top:8.55pt;">
            <div class="wcdiv" style="left:83.3pt; clip:rect(-1pt,32.7pt,9.4pt,-1pt);">
              <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:7.3pt; top:0.25pt;">серия</span></div>
            </div>
            <div class="wcdiv" style="left:119.65pt; clip:rect(-1pt,48.55pt,9.4pt,-1pt);">
              <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:14.54pt; top:0.25pt;">номер</span></div>
            </div>
          </div>
          <div class="wcdiv" style="left:83.05pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:91pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:98.9pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:106.85pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:114.75pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:119.4pt; top:0pt; width:0pt; height:8.55pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:127.3pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:135.25pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:143.15pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:151.1pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:159pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:166.95pt; top:0.5pt; width:0pt; height:8.05pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:83.05pt; top:0pt; width:32.2pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:119.4pt; top:0pt; width:48.05pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:83.3pt; top:8.55pt; width:31.7pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:119.65pt; top:8.55pt; width:47.55pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:81.47pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">10. Собственник ТС</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:64.44pt; top:0.32pt;">&nbsp;{{ $data->vehicle_b->owner }}</span></div>
        <div class="wcdiv" style="left:68.05pt; top:90.09pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:35.63pt; top:1.75pt;">(фамилия,</span></div>
        <div class="wcdiv" style="left:66.6pt; top:90.09pt; width:100.18pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="top:107.11pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:7.49pt; top:1.75pt;">имя, отчество (полное наименование юридического лица))</span></div>
        <div class="wcdiv" style="left:-1.45pt; top:107.11pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="top:115.51pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">Адрес</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:19.64pt; top:0.32pt;">&nbsp;{{ $data->vehicle_b->ownerAddress }}</span></div>
        <div class="wcdiv" style="left:22.65pt; top:124.14pt; width:144.13pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:-1.45pt; top:135.41pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="top:138.06pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">11. Водитель ТС</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:53.06pt; top:0.32pt;">&nbsp;{{ $data->vehicle_b->driver }}</span></div>
        <div class="wcdiv" style="left:56.7pt; top:146.69pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:21.96pt; top:1.75pt;">(фамилия, имя, отчество)</span></div>
        <div class="wcdiv" style="left:55.25pt; top:146.69pt; width:111.53pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:-1.45pt; top:163.71pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:168.36pt; z-index:1;">
          @php($b_driverDateBirth = $data->vehicle_b->driverDateBirth)
          <div class="wcdiv" style="clip:rect(-1pt,87.55pt,11.12pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">Дата рождения</span></div>
          </div>
          <div class="wcdiv" style="left:86.55pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverDateBirth) ? date('d', strtotime($b_driverDateBirth))[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:94.5pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverDateBirth) ? date('d', strtotime($b_driverDateBirth))[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:102.45pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:1.89pt; top:0.32pt;">.</span></div>
          </div>
          <div class="wcdiv" style="left:110.4pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverDateBirth) ? date('m', strtotime($b_driverDateBirth))[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:118.35pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_driverDateBirth) ? date('m', strtotime($b_driverDateBirth))[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:126.25pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:1.89pt; top:0.32pt;">.</span></div>
          </div>
          <div class="wcdiv" style="left:134.2pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverDateBirth) ? date('Y', strtotime($b_driverDateBirth))[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:142.15pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_driverDateBirth) ? date('Y', strtotime($b_driverDateBirth))[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:150.05pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverDateBirth) ? date('Y', strtotime($b_driverDateBirth))[2] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:158pt; clip:rect(-0.5pt,8.6pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.9pt; top:0.32pt;">{{ isset($b_driverDateBirth) ? date('Y', strtotime($b_driverDateBirth))[3] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:86.3pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:94.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:102.2pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:110.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:118.1pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:126pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:133.95pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:141.9pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:149.8pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:157.75pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:165.6pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:86.3pt; top:0pt; width:79.8pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:86.3pt; top:9.12pt; width:79.8pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:85.05pt; top:177.98pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:19.51pt; top:0.25pt;">день, месяц, год</span></div>
        <div class="wcdiv" style="top:184.88pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">Адрес</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:19.64pt; top:0.32pt;">&nbsp;{{ $data->vehicle_b->driverAddress }}</span></div>
        <div class="wcdiv" style="left:22.65pt; top:193.51pt; width:144.13pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:-1.45pt; top:204.78pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:209.43pt; z-index:1;">
          @php($b_driverPhone = $data->vehicle_b->driverPhone)
          <div class="wcdiv" style="clip:rect(-1pt,47.9pt,11.12pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">Телефон</span></div>
          </div>
          <div class="wcdiv" style="left:46.9pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.69pt; top:0.32pt;">{{ isset($b_driverPhone[0]) ? $b_driverPhone[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:54.8pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverPhone[1]) ? $b_driverPhone[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:62.75pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverPhone[2]) ? $b_driverPhone[2] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:70.7pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_driverPhone[3]) ? $b_driverPhone[3] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:78.6pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverPhone[4]) ? $b_driverPhone[4] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:86.55pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverPhone[5]) ? $b_driverPhone[5] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:94.5pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverPhone[6]) ? $b_driverPhone[6] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:102.45pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverPhone[7]) ? $b_driverPhone[7] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:110.4pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverPhone[8]) ? $b_driverPhone[8] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:118.35pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_driverPhone[9]) ? $b_driverPhone[9] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:126.25pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverPhone[10]) ? $b_driverPhone[10] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:134.2pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverPhone[11]) ? $b_driverPhone[11] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:142.15pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverPhone[12]) ? $b_driverPhone[12] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:150.1pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverPhone[13]) ? $b_driverPhone[13] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:158.05pt; clip:rect(-0.5pt,8.55pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.88pt; top:0.32pt;">{{ isset($b_driverPhone[14]) ? $b_driverPhone[14] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:46.65pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:54.55pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:62.5pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:70.45pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:78.35pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:86.3pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:94.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:102.2pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:110.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:118.1pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:126pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:133.95pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:141.9pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:149.85pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:157.8pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:165.6pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:46.65pt; top:0pt; width:119.45pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:46.65pt; top:9.12pt; width:119.45pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:221.35pt; z-index:1;">
          @php($b_driverlicenseSeries = $data->vehicle_b->driverlicenseSeries)
          @php($b_driverlicenseNumber = $data->vehicle_b->driverlicenseNumber)
          <div class="wcdiv" style="clip:rect(-1pt,82.6pt,11.12pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:0pt; top:1.98pt;">Водительское удостоверение</span></div>
          </div>
          <div class="wcdiv" style="left:81.6pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverlicenseSeries[0]) ? $b_driverlicenseSeries[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:89.55pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverlicenseSeries[1]) ? $b_driverlicenseSeries[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:97.5pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverlicenseSeries[2]) ? $b_driverlicenseSeries[2] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:105.45pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_driverlicenseSeries[3]) ? $b_driverlicenseSeries[3] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:117.95pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverlicenseNumber[0]) ? $b_driverlicenseNumber[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:125.9pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_driverlicenseNumber[1]) ? $b_driverlicenseNumber[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:133.8pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverlicenseNumber[2]) ? $b_driverlicenseNumber[2] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:141.75pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_driverlicenseNumber[3]) ? $b_driverlicenseNumber[3] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:149.65pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverlicenseNumber[4]) ? $b_driverlicenseNumber[4] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:157.6pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_driverlicenseNumber[5]) ? $b_driverlicenseNumber[5] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="top:9.12pt;">
            <div class="wcdiv" style="left:81.6pt; clip:rect(-1pt,32.75pt,12.4pt,-1pt);">
              <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:7.33pt; top:0.25pt;">серия</span></div>
            </div>
            <div class="wcdiv" style="left:117.95pt; clip:rect(-1pt,48.55pt,12.4pt,-1pt);">
              <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:14.54pt; top:0.25pt;">номер</span></div>
            </div>
          </div>
          <div class="wcdiv" style="left:81.35pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:89.3pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:97.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:105.2pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:113.1pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:117.7pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:125.65pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:133.55pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:141.5pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:149.4pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:157.35pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:165.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:81.35pt; top:0pt; width:32.25pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:117.7pt; top:0pt; width:48.05pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:81.6pt; top:9.12pt; width:31.75pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:117.95pt; top:9.12pt; width:47.55pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:242.03pt; z-index:1;">
          @php($b_driverlicenseDate = $data->vehicle_b->driverlicenseDate)

          <div class="wcdiv" style="clip:rect(-1pt,47.85pt,11.12pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">Категория</span></div>
          </div>
          <div class="wcdiv" style="left:46.85pt; clip:rect(-0.5pt,32.45pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:12.2pt; top:0.32pt;">{{ $data->vehicle_b->category }}</span></div>
          </div>
          <div class="wcdiv" style="left:86.5pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverlicenseDate) ? date('d', strtotime($b_driverlicenseDate))[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:94.45pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverlicenseDate) ? date('d', strtotime($b_driverlicenseDate))[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:102.4pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:1.89pt; top:0.32pt;">.</span></div>
          </div>
          <div class="wcdiv" style="left:110.35pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverlicenseDate) ? date('m', strtotime($b_driverlicenseDate))[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:118.3pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_driverlicenseDate) ? date('m', strtotime($b_driverlicenseDate))[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:126.2pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:1.89pt; top:0.32pt;">.</span></div>
          </div>
          <div class="wcdiv" style="left:134.15pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverlicenseDate) ? date('Y', strtotime($b_driverlicenseDate))[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:142.1pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_driverlicenseDate) ? date('Y', strtotime($b_driverlicenseDate))[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:150pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_driverlicenseDate) ? date('Y', strtotime($b_driverlicenseDate))[2] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:157.95pt; clip:rect(-0.5pt,8.6pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.9pt; top:0.32pt;">{{ isset($b_driverlicenseDate) ? date('Y', strtotime($b_driverlicenseDate))[3] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="top:9.12pt;">
            <div class="wcdiv" style="left:46.85pt; clip:rect(-1pt,32.7pt,9.4pt,-1pt);">
              <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:1.28pt; top:0.25pt;">A B C D E</span></div>
            </div>
            <div class="wcdiv" style="left:86.5pt; clip:rect(-1pt,80.3pt,9.4pt,-1pt);">
              <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:22.59pt; top:0.25pt;">дата выдачи</span></div>
            </div>
          </div>
          <div class="wcdiv" style="left:46.6pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:78.3pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:86.25pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:94.2pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:102.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:110.1pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:118.05pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:125.95pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:133.9pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:141.85pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:149.75pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:157.7pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:165.55pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:46.6pt; top:0pt; width:32.2pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:86.25pt; top:0pt; width:79.8pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:46.85pt; top:9.12pt; width:31.7pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:86.5pt; top:9.12pt; width:79.3pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:258.55pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">Документ на право владения, пользования,</span> <span class="wcspan wctext001" style="font-size:8pt; left:137.82pt; top:-0.13pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:8.94pt;">распоряжения ТС</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:56.82pt; top:8.94pt;">&nbsp;{{ $data->vehicle_a->titleDeed }}</span></div>
        <div class="wcdiv" style="left:60.4pt; top:275.8pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:0.64pt; top:1.75pt;">(доверенность, договор аренды, путевой&nbsp;</span> <span class="wcspan wctext001" style="font-size:6pt; left:38.2pt; top:8.65pt;">лист и т.п.)</span></div>
        <div class="wcdiv" style="left:58.95pt; top:275.8pt; width:107.83pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="top:291.1pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">12. Страховщик</span></div>
        <div class="wcdiv" style="top:299.34pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:0.4pt; top:1.75pt;">{{ $data->vehicle_a->sk }}</span></div>
        <div class="wcdiv" style="top:308.34pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:0.4pt; top:1.75pt;">(наименование страховщика, застраховавшего ответственность)</span></div>
        <div class="wcdiv" style="left:-1.45pt; top:308.34pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:318.74pt; z-index:1;">
          @php($b_bsoSeries = $data->vehicle_b->bsoSeries)
          @php($b_bsoNumber = $data->vehicle_b->bsoNumber)
          <div class="wcdiv" style="clip:rect(-1pt,59.1pt,11.12pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">Страховой полис</span></div>
          </div>
          <div class="wcdiv" style="left:58.1pt; clip:rect(-0.5pt,8.05pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">{{ isset($b_bsoSeries[0]) ? $b_bsoSeries[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:65.4pt; clip:rect(-0.5pt,8.1pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">{{ isset($b_bsoSeries[1]) ? $b_bsoSeries[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:72.75pt; clip:rect(-0.5pt,8.1pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">{{ isset($b_bsoSeries[2]) ? $b_bsoSeries[2] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:88pt; clip:rect(-0.5pt,8.75pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.98pt; top:0.32pt;">{{ isset($b_bsoNumber[0]) ? $b_bsoNumber[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:96pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_bsoNumber[1]) ? $b_bsoNumber[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:103.95pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_bsoNumber[2]) ? $b_bsoNumber[2] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:111.85pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_bsoNumber[3]) ? $b_bsoNumber[3] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:119.75pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_bsoNumber[4]) ? $b_bsoNumber[4] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:127.7pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_bsoNumber[5]) ? $b_bsoNumber[5] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:135.65pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_bsoNumber[6]) ? $b_bsoNumber[6] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:143.6pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_bsoNumber[7]) ? $b_bsoNumber[7] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:151.5pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_bsoNumber[8]) ? $b_bsoNumber[8] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:159.4pt; clip:rect(-0.5pt,8.8pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:1pt; top:0.32pt;">{{ isset($b_bsoNumber[9]) ? $b_bsoNumber[9] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="top:9.12pt;">
            <div class="wcdiv" style="left:58.1pt; clip:rect(-1pt,23pt,12.4pt,-1pt);">
              <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:2.45pt; top:0.25pt;">серия</span></div>
            </div>
            <div class="wcdiv" style="left:88pt; clip:rect(-1pt,80.45pt,12.4pt,-1pt);">
              <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext002" style="font-size:6pt; color:#00000a; left:30.49pt; top:0.25pt;">номер</span></div>
            </div>
          </div>
          <div class="wcdiv" style="left:57.85pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:65.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:72.5pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:79.85pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:87.75pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:95.75pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:103.7pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:111.6pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:119.5pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:127.45pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:135.4pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:143.35pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:151.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:159.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:167.2pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:57.85pt; top:0pt; width:22.5pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:87.75pt; top:0pt; width:79.95pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:58.1pt; top:9.12pt; width:22pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
          <div class="wcdiv" style="left:88pt; top:9.12pt; width:79.45pt; height:0pt; border-top:solid 0.75pt #00000a;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:-1.4pt; top:339.42pt; z-index:1;">
          @php($b_contractEndData = $data->vehicle_b->contractEndData)
          <div class="wcdiv" style="clip:rect(-1pt,88.9pt,11.12pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0pt; top:0.32pt;">Действителен до</span></div>
          </div>
          <div class="wcdiv" style="left:87.9pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_contractEndData) ? date('d', strtotime($b_contractEndData))[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:95.85pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_contractEndData) ? date('d', strtotime($b_contractEndData))[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:103.75pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:1.89pt; top:0.32pt;">.</span></div>
          </div>
          <div class="wcdiv" style="left:111.7pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_contractEndData) ? date('m', strtotime($b_contractEndData))[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:119.65pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_contractEndData) ? date('m', strtotime($b_contractEndData))[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:127.55pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:1.89pt; top:0.32pt;">.</span></div>
          </div>
          <div class="wcdiv" style="left:135.5pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_contractEndData) ? date('Y', strtotime($b_contractEndData))[0] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:143.45pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.93pt; top:0.32pt;">{{ isset($b_contractEndData) ? date('Y', strtotime($b_contractEndData))[1] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:151.35pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_contractEndData) ? date('Y', strtotime($b_contractEndData))[2] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:159.3pt; clip:rect(-0.5pt,8.7pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ isset($b_contractEndData) ? date('Y', strtotime($b_contractEndData))[3] : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:87.65pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:95.6pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:103.55pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:111.5pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:119.45pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:127.4pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:135.35pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:143.3pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:151.25pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:159.2pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:167.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:87.65pt; top:0pt; width:80pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:87.65pt; top:9.12pt; width:80pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="left:85.05pt; top:349.04pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:19.51pt; top:0.25pt;">день, месяц, год</span></div>
        <div class="wcdiv" style="left:-1.4pt; top:356.94pt; z-index:1;">
          <div class="wcdiv" style="clip:rect(-1pt,111pt,11.12pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">ТС застраховано от ущерба</span></div>
          </div>
          <div class="wcdiv" style="left:109.95pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.09pt; top:0.32pt;">{{ $data->vehicle_b->contractInsuredDamage ? '' : 'X' }}</span></div>
          </div>
          <div class="wcdiv" style="left:117.85pt; clip:rect(-1pt,24.85pt,11.12pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:4.51pt; top:0.32pt;">Нет</span></div>
          </div>
          <div class="wcdiv" style="left:141.7pt; clip:rect(-0.5pt,8.65pt,11.12pt,-0.75pt);">
            <div class="wcdiv" style="left:0.9pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.09pt; top:0.32pt;">{{ $data->vehicle_b->contractInsuredDamage ? 'X' : '' }}</span></div>
          </div>
          <div class="wcdiv" style="left:149.6pt; clip:rect(-1pt,17pt,11.12pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:2.38pt; top:0.32pt;">Да</span></div>
          </div>
          <div class="wcdiv" style="left:109.75pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:117.7pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:141.5pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:149.45pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:109.75pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:141.5pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:109.75pt; top:9.12pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
          <div class="wcdiv" style="left:141.5pt; top:9.12pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
            <br>
          </div>
        </div>
        <div class="wcdiv" style="top:365.56pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">13. Место первоначального удара</span></div>
        <div class="wcdiv" style="left:78pt; top:374.19pt; z-index:2;"><span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:0.69pt;">Указать стрелкой (</span> <span class="wcspan wctext003" style="font-size:6pt; left:48.28pt; top:0pt; font-family:serif;">→</span> <span class="wcspan wctext001" style="font-size:6pt; left:54.21pt; top:0.69pt;">)</span></div>
        <div class="wcdiv" style="left:35.45pt; top:450.53pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">14. Характер и перечень видимых&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:8.94pt;">поврежденных деталей и элементов</span></div>
        <div class="wcdiv" style="top:468pt; z-index:1;">
          <span class="wcspan wctext001" style="font-size:6pt; left:33.4pt; top:1.75pt;">{{ str_split_by($data->vehicle_b->infoDamage, 45, 6, 1) }}</span>
        </div>
        <div class="wcdiv" style="top:479pt; z-index:1;">
          <span class="wcspan wctext001" style="font-size:6pt; left:33.4pt; top:1.75pt;">{{ str_split_by($data->vehicle_b->infoDamage, 45, 6, 2) }}</span>
        </div>
        <div class="wcdiv" style="top:490pt; z-index:1;">
          <span class="wcspan wctext001" style="font-size:6pt; left:33.4pt; top:1.75pt;">{{ str_split_by($data->vehicle_b->infoDamage, 45, 6, 3) }}</span>
        </div>
        <div class="wcdiv" style="top:501pt; z-index:1;">
          <span class="wcspan wctext001" style="font-size:6pt; left:33.4pt; top:1.75pt;">{{ str_split_by($data->vehicle_b->infoDamage, 45, 6, 4) }}</span>
        </div>
        <div class="wcdiv" style="top:512pt; z-index:1;">
          <span class="wcspan wctext001" style="font-size:6pt; left:33.4pt; top:1.75pt;">{{ str_split_by($data->vehicle_b->infoDamage, 45, 6, 5) }}</span>
        </div>
        <div class="wcdiv" style="top:523pt; z-index:1;">
          <span class="wcspan wctext001" style="font-size:6pt; left:33.4pt; top:1.75pt;">{{ str_split_by($data->vehicle_b->infoDamage, 45, 6, 6) }}</span>
        </div>
        <div class="wcdiv" style="left:30pt; top:476.4pt; width:136.78pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:30pt; top:487.68pt; width:136.78pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:30pt; top:498.95pt; width:136.78pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:30pt; top:510.22pt; width:136.78pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:30pt; top:521.5pt; width:136.78pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:30pt; top:532.77pt; width:136.78pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="top:536.42pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">15. Замечания</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:44.99pt; top:0.32pt;">&nbsp;</span></div>
        <div class="wcdiv" style="top:468pt; z-index:1;">
          <span class="wcspan wctext001" style="font-size:6pt; left:48pt; top:70.75pt;">{{ str_split_by($data->vehicle_b->comment, 37, 3, 1) }}</span>
        </div>
        <div class="wcdiv" style="top:479pt; z-index:1;">
          <span class="wcspan wctext001" style="font-size:6pt; left:5pt; top:70.75pt;">{{ str_split_by($data->vehicle_b->comment, 37, 3, 2) }}</span>
        </div>
        <div class="wcdiv" style="left:47.05pt; top:544.05pt; width:119.73pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:-1.45pt; top:555.32pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="top:557.97pt; z-index:2;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:8.32pt;">Подпись водителя ТС "В"**</span></div>
        <div class="wcdiv" style="left:-1.45pt; top:574.59pt; width:168.23pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="top:577.24pt; z-index:2;"><span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:0.25pt;">** Составляется водителем транспортного</span></div>
        <div class="wcdiv" style="left:5.4pt; top:584.14pt; z-index:2;"><span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:0.25pt;">средства "В" в отношении своего ТС.</span></div>
      </div>
      <div class="wcdiv" style="left:351.47pt; top:0pt; width:0pt; height:598.94pt; border-left:solid 1pt #000000;">
        <br>
      </div>
    </div>
    <div class="wcdiv" style="left:181.5pt; top:638.12pt;">
      <div class="wcdiv" style="left:-0.05pt; top:-0.05pt; width:246.5pt; height:97.89pt; background-color:#ffffff; border:solid 0.1pt #ffffff;">
        <br>
      </div>
      <div class="wcdiv" style="left:-0.38pt; top:-0.38pt; width:245.85pt; height:97.24pt; border:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="top:70pt; z-index:1;">
        <div class="wcdiv" style="left:-0.05pt; top:-0.05pt; width:246.5pt; height:29.4pt; background-color:#ffffff; border:solid 0.1pt #ffffff;">
          <br>
        </div>
        <div class="wcdiv" style="left:-0.38pt; top:-0.38pt; width:245.85pt; height:28.75pt; border:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:4.06pt; top:3.98pt; clip:rect(-0.38pt,248.29pt,22.93pt,-9.81pt); z-index:2;"><span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:0.25pt;">1. План (схема) дороги – с указанием названий улиц.</span> <span class="wcspan wctext001" style="font-size:6pt; left:148.85pt; top:0.25pt;">2. Направление движения ТС "А"</span> <span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:7.15pt;">и "В".</span> <span class="wcspan wctext001" style="font-size:6pt; left:27.3pt; top:7.15pt;">3. Расположение ТС "А" и "В" в момент столкновения.</span> <span class="wcspan wctext001" style="font-size:6pt; left:174pt; top:7.15pt;">4. Конечное положение</span> <span class="wcspan wctext001" style="font-size:6pt; left:0pt; top:14.05pt;">ТС "А" и "В".</span> <span class="wcspan wctext001" style="font-size:6pt; left:49.65pt; top:14.05pt;">5. Дорожные знаки, указатели, светофоры, дорожная разметка.</span></div>
      </div>
    </div>
    <div class="wcdiv" style="left:466.57pt; top:609.69pt;">
      <div class="dmg_transport">
          <img src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAIcAAABaCAYAAACSR0X7AAAVv0lEQVR4Ae09CVBTV9c3CxgWWUISdlDAhSCuKKIICEwVFETr0vFHQEu1KtaOtbbYWlFEbT+1WNuvOkMFVFpAbSuUYnVERaqiKAIuSAEJiMSwJSyWLXn/nOjLlwSiLNlK353J3P1s7+Td+849914S6keorq62yszMDLlx44bH48ePx3G5XAuBQGDc2dk5oqenhyoSicgYhpGkQZHJZBGFQhHq6Oh06+vrv2AwGA329vaciRMnFs2bN++8n59fHolE6pbuQ6S1SwIyD1SatBs3brjv2LFjDyhEe3u7gXQdnqbT6U1mZmaNI0eObKXRaB1QDkrS2dlJEwgERjwej9Xa2joSby8dk0gkDJRl9erVSVFRUV+bmZm1SNcTac1LoE/lCAoKysjMzAzqizxXV9cS+FVUVDhVVlaONjEx4VtaWj6zs7Or6erqGoH34fF4TA6HYycQCEyYTGY9n883ef78uTleLx0bGhq2paamvrNw4cIs6XIirVkJ9FKO+vr6kW5ubiUwVNjY2DwF8oqKiibBv5xCoRg1NDSYsFis9smTJ/OYTOYLKpUqeh0LPT09ZC6Xa1BQUGDZ1NSky2KxqkUiEamqqmrU5MmT70Hfp0+f2oCSFRcXTyOGmtdJU711vZQD0BcUFEx98uSJI07K1q1b//Pxxx8n79q164uQkBDk6Cipwpv0K66oqMDOnj3bsnPnzh2HDh366MCBAx/jHUePHl3h5uZ2F88TseYl0KdyyJPl6upa7Ovrq1dbW+t0+vRp+eoB5cPDw58aGBhk5uXleZaUlEwcUGeisfZJgM1mV5qYmGBlZWXYUENVVRVmYmLS6uzs/FD7OCUoGrAEGAxG14oVK4aqF5L+YWFhPDMzs4YBE0J00C4JVFVVraFSqdjly5clD3eoidzcXIxKpQrLy8udtIvb/lODYRi5pqbGuqqqajT8FH2J9R+i9rWkvo4kDMMoq1atOmhmZobgp6xgYmKCGAyGaPv27XsRQsuVBVfZcGpra21PnDgRCvOjx48fj62vr2e+ePFCHwx/JBKpz/ka2G/A8Aef5ywWi+fi4vLA19c3Z9WqVWlGRkaNyqZRY/AKCwtjLC0tMRcXF6y4uHioLwxJf4AFMK2trZvz8/PdNMZgH4gxDNPdsmXLQTqdDg8Sk//p6+u3u7i43J81a9afPj4+lz09PXMhhvykSZPuvRoue/Ujk8lCGxubmv3792/DMEynD9RaV6TwzYFhmN68efM+2bFjB/r++++VTjiZTEYxMTGk6OjoAwghH6UjGATAlpYWMwaD8bixsbHXa9LLyysXjHnXrl2bA28OsAqzWKznenp6YsswhUIR1dXVWcCbA6zEYD2urKx0AHsRkAIx2HM+++yzvQkJCe/xeLw5LBaLOwgy1dZFoXJcvHjxyJMnT2iRkZEqUQ7gMCIiwvjQoUPs7OzseQEBAX+ojWsFiMrLy23gITo7Oz+SbhIYGGiem5s7m8vldk2YMKGDTCYzhUIhs66uTrqZOM1gMMRDcHd3t4WpqSnG4/EENBqNC2tN0ACUh8Ph2APPCKHkXgC0qKBP5cAwzGzatGkRe/fuRTo6qnsDUqlUFBcXZ/Dpp58exDDsIolEeq21VdVymzp1alF+fv5bHR0d+oaGhu2v8Om89957eRERERRnZ2c9hBD8+h1iYmJezJ8/PyUwMPB36NTd3a3T1tZmCAuP4eHh/YajNQ1TUlKy3N3dMZFIJJ4nuLq6Kn3OATDx4OHh8ezUqVOhWiOAV4R8/vnne44ePXrMxsYGEwqFOLkDitPT0+v9/PwuRUZGJhQWFk7WNh5fR0+vN0dHR4cTm80OSExMhFfg6/oqre6rr75ihIaG7sMw7DSJROpUGuAhAoKFxcLCQr+wsDAEc6TBhODgYMaGDRumtbS0jGxvbzccDAxN9enF8dGjR39is9kkLy8vtdHk6empM3HiRFJ8fPx6tSHtByIMw/Tz8vLMIyIi+tG67yYjRoxAy5cv54L7Qt8ttLdU5s3R0tIy29vb2y0uLg6dO3cOVktRW1sbqq+vR8ePH0cWFhZK4YTL5YphfvnllwiEZ2lpiUJDQy03bdq0o7Gx8bi2+HbU1dW5mpubozFjxgyJ7/DwcLvExET1vIaHRKlsZxnlyMzM/KaxsRHt3LlT/MBsbW2RoaEhWrBgAaJQKKi5uVm29yBzAAtgArzOzk50/fp1VFtbS+7o6KBv3br1a4TQu4MErdRulZWVdm+99daQYc6YMUOPRCJ13b9/fwJCKG/IADUBYO3atY8OHz48oAmXMhtfuXLlOZvNfqAJ3uVx1tfXz9XR0cESEhKUwuKoUaP+Dg4OPiePR5vzMnOOVz6fGqPXzMxMADRojAApxKmpqTutra2Rnt6AvlylIMgmWSwW7cqVK75gXJSt0d6czLCivWSqlzIwobu5uc12cHBQGmJdXV00fvx4flpa2iKEUKrSAKsQkMybQ4V4/lGgHzx4sOH58+dUmIwqMwQGBhokJiYO/tNHmcT0AxahHH0IKSkpKQpsG8q283h6eprevn3bA5b6+0CrdUUyykGj0Xh8Pl9jY35zczMH3+KgKUlhGGZ98uRJR1WYtuGzfenSpXUpKSn/pyn+BoJXMufAMMzi4MGDZjExMeTY2Fg0cuRIZGpqimCsVGXo6OhAfD4ftbS0wJ4X323btn19+/ZtVaJ8LeysrKxd4EA9duzY17YbbGV4eLhtZGTkGoTQV4OFoa5+YuXAMMw4Ojr6YU5OjmlaWhqys7NDNTU1YHsQG8FUSQxuBLOyskIcDoe8e/fuDfv27RNER0fHqhKvIthJSUlLh2IRVQQXL/fw8NAXCoXGt2/fnjF9+vRbeLnWxhkZGdlOTk6wvKyUb/qhAKmpqYGdcIKcnBxvdQsMbBvGxsYYn88Xs7By5UosJSVlKOxI+np6emLXrl0T5/fs2VO+YcOG79TN34DxYRhmP27cOBH4dWpLyMrK6pw2bZp4w9OAGRpChyNHjlwBhcCDqpSDw+EIGQxGI4Zhkh2CQyBbZV3Jf/zxR4y+vj5pzpw5KkMyUMDz58/XFQgE1jdv3pw50L6DbQ+2jeTk5NmqmIjK02RnZ0eeNGlS85kzZ4Ll69SRB99gcGek0Wh/Hz58eJMinOSsrCx/dQhEEQF9lcPyeGRkJO/s2bOL+6pXRRnYNrhcLtXf318C3t3dXTz/khQMITF37lxwqpZAiIiIMEhOTg6TFKgxwePxGOAKSaVSe2JjY79QhJpaVFTEWrJkiaJ6jZWz2WzTY8eOwUKVWkJSUtI7Xl5eTYWFhc9xhLNnzxYn79y5gxcNOl60aBFqb29HOCxbW1vypUuX/GFLg7m5uQTnoBEMoGNPT4/4ExRcF8EzTVFXakVFhc5g974qAqqM8jFjxhiWlZUNba28n4Q0NjZ6eHl5uZWWllLS0tLo+vr64k94cFfo6enpJ5TXNwOXSAzDkFD4PzOSubn5MzKZ/OL1PTVXS62rqyPBApO2BdgSAftE1EHXs2fPLGEcBr8VkUgEzjlo9OjR6JNPPpEZCoZCy7NnzxB48kP44YcfxDGdTrdiMpmtQ4Gryr5wKs+gXeBUSRjAlj8tSJX4wMcEjH6pqS/XxBISEpSKDmDDJvQpU6age/fuIZiDaHuQMZ9rO7HqoC89PR29//77KkEFCrhu3Tr0448/qgS+soESyiEnUXCNHKpboBxImayLiwtYgmXK1J3R0dFphv0zsF0TYkX4CeWQkwys9cA/XFUBJqZ///23qsC/ES6GYbbXr18/vXDhwk4zMzNHb29vfmlp6bi+OkoW3vqqJMqGlwQwDGNu2rSp9NatW/pRUVEIfITz8/NHeXp63svJyZnv6+t7VZpjQjmkpTHM03fu3NmUnZ2tf//+fUSj0cTc+vj4IDc3N90PPvjgvwghF2kREMOKtDSGebq5ufmFs7OzRDFwdidNmvScz+cb4Xk8JpQDl8S/IHZzcztaWlranJmZKZmEdnZ21m/btu3eokWLennGE8PKMFYKHo9nkZOT4wMHBgOb8Jm+fPnyo+vXr1+zcePGEUwmU1BeXk6fMGGC8TvvvFN27Nixtbg43N3dbxPKgUtjmMWPHz8eP2fOnF+nT59eAAfOSLO3ePHi8xiGeZeVlZkyGIxaKyurv+7fvy8z34iPj/+QUA5pqQ2zdFxc3GdLly49K80W7P+Nj4+Ht4Q1GORKSkqM4Ovl+vXrU6WPpeJyuSxCOaQlN4zS48aNK0UIwU8mPHr06Ku9e/dal5SUoFdbL0ibN29mwkHECCHwbRUHCwsLHqEcuDT+BTGsVa1YsWJNdHS0zOpwbGys3oQJExZVVFSMcXR0/AsXBfG1gkvi3xE7Z2Vl6TU1NaEjR45IODYyMoKN7bzs7GyZXeOEckhENPwT1dXVbrA6fOPGDbR4sayTHZvNpjx8+JAtLQVCOaSlMczTHA7HAjeCgauGdBg7dmwXnHYoXUYoh7Q0hnl65MiRPDg4JzAwEB09elSG24aGhg4DAwOZT15COWRENLwzTk5Ov5eXl6NVq1ahwsJCdPHiRQnDDx48aGOz2TJHbBLKIRHP8E/Am2PcuHGCR48eoQsXLiBfX1+caezWrVsj3NzcZHbgEcqBi2cYx2VlZQ4RERFJb7/99tn169d/+9FHH4Gjj8Rv5dy5c1dqa2vF163BBY0JCQmRIA5COYaxUuCs7d279ws7O7vqb7755sO1a9d+QSKRHsXExLR3dXWJiouLz2/dutXu22+/3TBt2rQSUIyDBw9uKSoqmkAYwXAJDuO4vLzcEQ7Jtba2rgE2uVzu3I0bN35vaWk5l8lkjt69e/cOf3//y69EcHvBggWVNTU19sSbYxgrhTxrHA4n1NnZucfZ2Zmbk5OzGMMwEx6PN3bjxo0pc+fOzRcIBHTpPsSbQ1oawzxdXFzsZGtrS/nzzz97cerp6WnG4XBgA1MTXkkoBy6Jf0kMFx2AlVQ+4Lc6SJcTw4q0NIi0jAQI5ZARB5GRlgChHNLSQAjBCmVrq+q2r8KKKJ0uM++To0B7soRyyD2L6dOno7w81R1Pnpubi+Dcj39CIJRD7im9++67Yl+H6upquZqhZx88eCDeJwtrG+oMVlZWdffu3ZsE152+CS+4B9bV1VnCVknia+XlvWvN9fX17fv37y8A4c2YMcPa3d3d3s/Pr8PY2Fh2bftN0lVQX1NTI7x27ZpBUFBQeUpKCn7hsloOxdu2bdv+LVu2HGKxWKMUkCcufuWUfO3DDz88PH369LtwBwicj/a6PhqpEwgELfb29phAIDBRNQEYhtHi4uLuFhQUSC7MEQqFlO7ubl2RSKSUe1LgU1FXV7cbrhAFfoRCoXDWrFmHoqOjv1Q1fzj8jIyM3ceOHdvx22+/4UWS2NXV9a8TJ04smzJlSgl+1x7x5ngpHtGhQ4ecoqKidNR1kM3du3cfnjlzZhlCSG3KgT90iUbIJahUqki6DaEcL4eVrnXr1uXRaLS5a9dK9vXIiU652fDwcE5YWNgf+BlhyoWuHGhvnKAoB432Q1m9enVscnKyUuYXb+K2ra3tcUZGhseKFSt+elNbTdYTyvFK+jNnzoRVybabN2+q/HmcOXOm2MfH5yrsDVE5siEgIJRDSnhhYWHpyckqvyRamJiYaLdmzZpEKdRamSSUQ+qxwNBy+vRpEZzuo6rw5MmTvNLS0tFBQUHZqsKhLLiEckhJ0srKqnrq1KlVGRkZklJY3oZLeYbyO3XqlAReUlJSw8qVK3+C2yIlhVqaIJRD7sGEhYV9l5SUJCmFU4c9PDzEB8yCPWigP7ieA+6TgYBhWPOJEyfcwsPD/4dAgkn7EoRyyD2T0NDQo/n5+T11dXXiGjgvtLS0FFzr5Fq+OQunH2dmZqKQkBBx46tXr+YZGxvzp0yZovYbId5Mbe8WhHLIyYREIr1YsmTJDXwoAOeY+fPnix+yXNM3ZmGRDW6YtLF5uZEsMTGRqsmJKJlMrquoqGjcunXrA9jYFBQU1L1s2TJYNrja0NBg3BdD+PUiWhXz+XwB/Mv6IljVZbm5ufNcXFyEuEDS09OxgIAAPNvveNOmTVhcXJy4fWtra6mJiUkzLGypmn5F8DEM0zlw4MBHixcv/hlGOVNT0yYmk8mDtZekpKQwOOJbvm+/mVVnQ00qBxxV4Ojo2FpQUCBmuaWlBTMyMsIg7m8QiUSYra0t9vDhQ3GXpKSk0yEhIb/KC18T+Z9//hmuycDGjBlT5urqWqyIBmJY6UMycKpvRETEWXxiCpchwvUa58+f76N130VgFofbF2DjMqyzHT9+3PafMhHFOSKUA5eEXBwWFrY7LS1N1NX18osTjiz49df+//GhLT4RxW0bISEhv8uh0eosoRwKHo+9vX0lm81+ii9vBwUFoezsbNTd3a2gh2zxL7/8IjkD459k25DmglAOaWnIpdesWfNffGixsLCAu+jR5cv4xjC5xlLZsrIysW1jxowZYtvGyZMnp0VERKjcLi9FglKSZLjXVf4gD6VAVgIQXV1djVoRAwICvsvLy+vh8V6uj8Ewce5cr7Nce3GKDylgVQXbhpGRkWDy5MmFvRpqqAB8NigUivBNt3+TWSyWEC4Y1rZQW1sroNPpkt1XmqCPxWK1BQcH38bvR8GV402eczCk4PMNsG3ADndN0K8Ip5+f37WZM2fehJN8QkJCFGu7t7d306VLl/r7haa2dhkZGTcXLFiQpYhBVZY3NTUZV1VVjYZfampq5MSJE7sbGhoEjY2N/PHjx4vy8/MVyoHD4XSbmpqKuFwu/8mTJ4/AVpOfn+8GsCorK+21/S5ZGblu3rw5Lz4+XiGzmqqIjY3N/Pzzz/fIEKumTHBwMPybwLFW5gf+n3p6enBhn0y5fF5XV7dTvgzyFAqlZ9WqVSfUxMbQ0Vy5cmWZi4uLSFNK0BdeoVD4wsHBoQruex86hwOHAHjpdHojg8Gol3/I8IBhDgHGI3izBQQE/A4/Hx+fHCcnp79GjBgB6/0yygNwQkNDTzk6OpaDJXLgFGmoB1gD2Wx2ozYNLWfPnj0zc+bMGxoSiRhtSUnJxBUrVqTCsODi4nIfTryxt7evUvRWwBUC3i7QzsPD43pQUFDGvHnzzpubm3M9PT2vXbhwwU+TPA0Ut9jtPjs7e/G6devSCgoKdJhMtdzWqZDO6urqQnd3d6v09PS3vby8ep8VoLCnairq6uqYly9f9s/Ly/MsKiqaVF5e7iQQCIyNjIxaDAwM2mg0GgwhsNWA0tbWZsjn803gK8DBwaHS1dW1xMPD44a/v/8FBwcHzV7sNgjxSPZk7Nq1a39mZuZ727dvp3p7e4OL+iDADb6LSCQSXrhwoWTfvn3MdevWfR8VFfXd4KGptifsc6mpqaG3traadnR0iK88gj0pdDq92crKqolEIskc2ahaalQH/f8BQJOAT1RCmKwAAAAASUVORK5CYII=" alt=""/>
        </div>
    </div>
    <div class="wcdiv" style="left:72pt; top:609.69pt;">
      <div class="dmg_transport">
        <img src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAIcAAABaCAYAAACSR0X7AAAVv0lEQVR4Ae09CVBTV9c3CxgWWUISdlDAhSCuKKIICEwVFETr0vFHQEu1KtaOtbbYWlFEbT+1WNuvOkMFVFpAbSuUYnVERaqiKAIuSAEJiMSwJSyWLXn/nOjLlwSiLNlK353J3P1s7+Td+849914S6keorq62yszMDLlx44bH48ePx3G5XAuBQGDc2dk5oqenhyoSicgYhpGkQZHJZBGFQhHq6Oh06+vrv2AwGA329vaciRMnFs2bN++8n59fHolE6pbuQ6S1SwIyD1SatBs3brjv2LFjDyhEe3u7gXQdnqbT6U1mZmaNI0eObKXRaB1QDkrS2dlJEwgERjwej9Xa2joSby8dk0gkDJRl9erVSVFRUV+bmZm1SNcTac1LoE/lCAoKysjMzAzqizxXV9cS+FVUVDhVVlaONjEx4VtaWj6zs7Or6erqGoH34fF4TA6HYycQCEyYTGY9n883ef78uTleLx0bGhq2paamvrNw4cIs6XIirVkJ9FKO+vr6kW5ubiUwVNjY2DwF8oqKiibBv5xCoRg1NDSYsFis9smTJ/OYTOYLKpUqeh0LPT09ZC6Xa1BQUGDZ1NSky2KxqkUiEamqqmrU5MmT70Hfp0+f2oCSFRcXTyOGmtdJU711vZQD0BcUFEx98uSJI07K1q1b//Pxxx8n79q164uQkBDk6Cipwpv0K66oqMDOnj3bsnPnzh2HDh366MCBAx/jHUePHl3h5uZ2F88TseYl0KdyyJPl6upa7Ovrq1dbW+t0+vRp+eoB5cPDw58aGBhk5uXleZaUlEwcUGeisfZJgM1mV5qYmGBlZWXYUENVVRVmYmLS6uzs/FD7OCUoGrAEGAxG14oVK4aqF5L+YWFhPDMzs4YBE0J00C4JVFVVraFSqdjly5clD3eoidzcXIxKpQrLy8udtIvb/lODYRi5pqbGuqqqajT8FH2J9R+i9rWkvo4kDMMoq1atOmhmZobgp6xgYmKCGAyGaPv27XsRQsuVBVfZcGpra21PnDgRCvOjx48fj62vr2e+ePFCHwx/JBKpz/ka2G/A8Aef5ywWi+fi4vLA19c3Z9WqVWlGRkaNyqZRY/AKCwtjLC0tMRcXF6y4uHioLwxJf4AFMK2trZvz8/PdNMZgH4gxDNPdsmXLQTqdDg8Sk//p6+u3u7i43J81a9afPj4+lz09PXMhhvykSZPuvRoue/Ujk8lCGxubmv3792/DMEynD9RaV6TwzYFhmN68efM+2bFjB/r++++VTjiZTEYxMTGk6OjoAwghH6UjGATAlpYWMwaD8bixsbHXa9LLyysXjHnXrl2bA28OsAqzWKznenp6YsswhUIR1dXVWcCbA6zEYD2urKx0AHsRkAIx2HM+++yzvQkJCe/xeLw5LBaLOwgy1dZFoXJcvHjxyJMnT2iRkZEqUQ7gMCIiwvjQoUPs7OzseQEBAX+ojWsFiMrLy23gITo7Oz+SbhIYGGiem5s7m8vldk2YMKGDTCYzhUIhs66uTrqZOM1gMMRDcHd3t4WpqSnG4/EENBqNC2tN0ACUh8Ph2APPCKHkXgC0qKBP5cAwzGzatGkRe/fuRTo6qnsDUqlUFBcXZ/Dpp58exDDsIolEeq21VdVymzp1alF+fv5bHR0d+oaGhu2v8Om89957eRERERRnZ2c9hBD8+h1iYmJezJ8/PyUwMPB36NTd3a3T1tZmCAuP4eHh/YajNQ1TUlKy3N3dMZFIJJ4nuLq6Kn3OATDx4OHh8ezUqVOhWiOAV4R8/vnne44ePXrMxsYGEwqFOLkDitPT0+v9/PwuRUZGJhQWFk7WNh5fR0+vN0dHR4cTm80OSExMhFfg6/oqre6rr75ihIaG7sMw7DSJROpUGuAhAoKFxcLCQr+wsDAEc6TBhODgYMaGDRumtbS0jGxvbzccDAxN9enF8dGjR39is9kkLy8vtdHk6empM3HiRFJ8fPx6tSHtByIMw/Tz8vLMIyIi+tG67yYjRoxAy5cv54L7Qt8ttLdU5s3R0tIy29vb2y0uLg6dO3cOVktRW1sbqq+vR8ePH0cWFhZK4YTL5YphfvnllwiEZ2lpiUJDQy03bdq0o7Gx8bi2+HbU1dW5mpubozFjxgyJ7/DwcLvExET1vIaHRKlsZxnlyMzM/KaxsRHt3LlT/MBsbW2RoaEhWrBgAaJQKKi5uVm29yBzAAtgArzOzk50/fp1VFtbS+7o6KBv3br1a4TQu4MErdRulZWVdm+99daQYc6YMUOPRCJ13b9/fwJCKG/IADUBYO3atY8OHz48oAmXMhtfuXLlOZvNfqAJ3uVx1tfXz9XR0cESEhKUwuKoUaP+Dg4OPiePR5vzMnOOVz6fGqPXzMxMADRojAApxKmpqTutra2Rnt6AvlylIMgmWSwW7cqVK75gXJSt0d6czLCivWSqlzIwobu5uc12cHBQGmJdXV00fvx4flpa2iKEUKrSAKsQkMybQ4V4/lGgHzx4sOH58+dUmIwqMwQGBhokJiYO/tNHmcT0AxahHH0IKSkpKQpsG8q283h6eprevn3bA5b6+0CrdUUyykGj0Xh8Pl9jY35zczMH3+KgKUlhGGZ98uRJR1WYtuGzfenSpXUpKSn/pyn+BoJXMufAMMzi4MGDZjExMeTY2Fg0cuRIZGpqimCsVGXo6OhAfD4ftbS0wJ4X323btn19+/ZtVaJ8LeysrKxd4EA9duzY17YbbGV4eLhtZGTkGoTQV4OFoa5+YuXAMMw4Ojr6YU5OjmlaWhqys7NDNTU1YHsQG8FUSQxuBLOyskIcDoe8e/fuDfv27RNER0fHqhKvIthJSUlLh2IRVQQXL/fw8NAXCoXGt2/fnjF9+vRbeLnWxhkZGdlOTk6wvKyUb/qhAKmpqYGdcIKcnBxvdQsMbBvGxsYYn88Xs7By5UosJSVlKOxI+np6emLXrl0T5/fs2VO+YcOG79TN34DxYRhmP27cOBH4dWpLyMrK6pw2bZp4w9OAGRpChyNHjlwBhcCDqpSDw+EIGQxGI4Zhkh2CQyBbZV3Jf/zxR4y+vj5pzpw5KkMyUMDz58/XFQgE1jdv3pw50L6DbQ+2jeTk5NmqmIjK02RnZ0eeNGlS85kzZ4Ll69SRB99gcGek0Wh/Hz58eJMinOSsrCx/dQhEEQF9lcPyeGRkJO/s2bOL+6pXRRnYNrhcLtXf318C3t3dXTz/khQMITF37lxwqpZAiIiIMEhOTg6TFKgxwePxGOAKSaVSe2JjY79QhJpaVFTEWrJkiaJ6jZWz2WzTY8eOwUKVWkJSUtI7Xl5eTYWFhc9xhLNnzxYn79y5gxcNOl60aBFqb29HOCxbW1vypUuX/GFLg7m5uQTnoBEMoGNPT4/4ExRcF8EzTVFXakVFhc5g974qAqqM8jFjxhiWlZUNba28n4Q0NjZ6eHl5uZWWllLS0tLo+vr64k94cFfo6enpJ5TXNwOXSAzDkFD4PzOSubn5MzKZ/OL1PTVXS62rqyPBApO2BdgSAftE1EHXs2fPLGEcBr8VkUgEzjlo9OjR6JNPPpEZCoZCy7NnzxB48kP44YcfxDGdTrdiMpmtQ4Gryr5wKs+gXeBUSRjAlj8tSJX4wMcEjH6pqS/XxBISEpSKDmDDJvQpU6age/fuIZiDaHuQMZ9rO7HqoC89PR29//77KkEFCrhu3Tr0448/qgS+soESyiEnUXCNHKpboBxImayLiwtYgmXK1J3R0dFphv0zsF0TYkX4CeWQkwys9cA/XFUBJqZ///23qsC/ES6GYbbXr18/vXDhwk4zMzNHb29vfmlp6bi+OkoW3vqqJMqGlwQwDGNu2rSp9NatW/pRUVEIfITz8/NHeXp63svJyZnv6+t7VZpjQjmkpTHM03fu3NmUnZ2tf//+fUSj0cTc+vj4IDc3N90PPvjgvwghF2kREMOKtDSGebq5ufmFs7OzRDFwdidNmvScz+cb4Xk8JpQDl8S/IHZzcztaWlranJmZKZmEdnZ21m/btu3eokWLennGE8PKMFYKHo9nkZOT4wMHBgOb8Jm+fPnyo+vXr1+zcePGEUwmU1BeXk6fMGGC8TvvvFN27Nixtbg43N3dbxPKgUtjmMWPHz8eP2fOnF+nT59eAAfOSLO3ePHi8xiGeZeVlZkyGIxaKyurv+7fvy8z34iPj/+QUA5pqQ2zdFxc3GdLly49K80W7P+Nj4+Ht4Q1GORKSkqM4Ovl+vXrU6WPpeJyuSxCOaQlN4zS48aNK0UIwU8mPHr06Ku9e/dal5SUoFdbL0ibN29mwkHECCHwbRUHCwsLHqEcuDT+BTGsVa1YsWJNdHS0zOpwbGys3oQJExZVVFSMcXR0/AsXBfG1gkvi3xE7Z2Vl6TU1NaEjR45IODYyMoKN7bzs7GyZXeOEckhENPwT1dXVbrA6fOPGDbR4sayTHZvNpjx8+JAtLQVCOaSlMczTHA7HAjeCgauGdBg7dmwXnHYoXUYoh7Q0hnl65MiRPDg4JzAwEB09elSG24aGhg4DAwOZT15COWRENLwzTk5Ov5eXl6NVq1ahwsJCdPHiRQnDDx48aGOz2TJHbBLKIRHP8E/Am2PcuHGCR48eoQsXLiBfX1+caezWrVsj3NzcZHbgEcqBi2cYx2VlZQ4RERFJb7/99tn169d/+9FHH4Gjj8Rv5dy5c1dqa2vF163BBY0JCQmRIA5COYaxUuCs7d279ws7O7vqb7755sO1a9d+QSKRHsXExLR3dXWJiouLz2/dutXu22+/3TBt2rQSUIyDBw9uKSoqmkAYwXAJDuO4vLzcEQ7Jtba2rgE2uVzu3I0bN35vaWk5l8lkjt69e/cOf3//y69EcHvBggWVNTU19sSbYxgrhTxrHA4n1NnZucfZ2Zmbk5OzGMMwEx6PN3bjxo0pc+fOzRcIBHTpPsSbQ1oawzxdXFzsZGtrS/nzzz97cerp6WnG4XBgA1MTXkkoBy6Jf0kMFx2AlVQ+4Lc6SJcTw4q0NIi0jAQI5ZARB5GRlgChHNLSQAjBCmVrq+q2r8KKKJ0uM++To0B7soRyyD2L6dOno7w81R1Pnpubi+Dcj39CIJRD7im9++67Yl+H6upquZqhZx88eCDeJwtrG+oMVlZWdffu3ZsE152+CS+4B9bV1VnCVknia+XlvWvN9fX17fv37y8A4c2YMcPa3d3d3s/Pr8PY2Fh2bftN0lVQX1NTI7x27ZpBUFBQeUpKCn7hsloOxdu2bdv+LVu2HGKxWKMUkCcufuWUfO3DDz88PH369LtwBwicj/a6PhqpEwgELfb29phAIDBRNQEYhtHi4uLuFhQUSC7MEQqFlO7ubl2RSKSUe1LgU1FXV7cbrhAFfoRCoXDWrFmHoqOjv1Q1fzj8jIyM3ceOHdvx22+/4UWS2NXV9a8TJ04smzJlSgl+1x7x5ngpHtGhQ4ecoqKidNR1kM3du3cfnjlzZhlCSG3KgT90iUbIJahUqki6DaEcL4eVrnXr1uXRaLS5a9dK9vXIiU652fDwcE5YWNgf+BlhyoWuHGhvnKAoB432Q1m9enVscnKyUuYXb+K2ra3tcUZGhseKFSt+elNbTdYTyvFK+jNnzoRVybabN2+q/HmcOXOm2MfH5yrsDVE5siEgIJRDSnhhYWHpyckqvyRamJiYaLdmzZpEKdRamSSUQ+qxwNBy+vRpEZzuo6rw5MmTvNLS0tFBQUHZqsKhLLiEckhJ0srKqnrq1KlVGRkZklJY3oZLeYbyO3XqlAReUlJSw8qVK3+C2yIlhVqaIJRD7sGEhYV9l5SUJCmFU4c9PDzEB8yCPWigP7ieA+6TgYBhWPOJEyfcwsPD/4dAgkn7EoRyyD2T0NDQo/n5+T11dXXiGjgvtLS0FFzr5Fq+OQunH2dmZqKQkBBx46tXr+YZGxvzp0yZovYbId5Mbe8WhHLIyYREIr1YsmTJDXwoAOeY+fPnix+yXNM3ZmGRDW6YtLF5uZEsMTGRqsmJKJlMrquoqGjcunXrA9jYFBQU1L1s2TJYNrja0NBg3BdD+PUiWhXz+XwB/Mv6IljVZbm5ufNcXFyEuEDS09OxgIAAPNvveNOmTVhcXJy4fWtra6mJiUkzLGypmn5F8DEM0zlw4MBHixcv/hlGOVNT0yYmk8mDtZekpKQwOOJbvm+/mVVnQ00qBxxV4Ojo2FpQUCBmuaWlBTMyMsIg7m8QiUSYra0t9vDhQ3GXpKSk0yEhIb/KC18T+Z9//hmuycDGjBlT5urqWqyIBmJY6UMycKpvRETEWXxiCpchwvUa58+f76N130VgFofbF2DjMqyzHT9+3PafMhHFOSKUA5eEXBwWFrY7LS1N1NX18osTjiz49df+//GhLT4RxW0bISEhv8uh0eosoRwKHo+9vX0lm81+ii9vBwUFoezsbNTd3a2gh2zxL7/8IjkD459k25DmglAOaWnIpdesWfNffGixsLCAu+jR5cv4xjC5xlLZsrIysW1jxowZYtvGyZMnp0VERKjcLi9FglKSZLjXVf4gD6VAVgIQXV1djVoRAwICvsvLy+vh8V6uj8Ewce5cr7Nce3GKDylgVQXbhpGRkWDy5MmFvRpqqAB8NigUivBNt3+TWSyWEC4Y1rZQW1sroNPpkt1XmqCPxWK1BQcH38bvR8GV402eczCk4PMNsG3ADndN0K8Ip5+f37WZM2fehJN8QkJCFGu7t7d306VLl/r7haa2dhkZGTcXLFiQpYhBVZY3NTUZV1VVjYZfampq5MSJE7sbGhoEjY2N/PHjx4vy8/MVyoHD4XSbmpqKuFwu/8mTJ4/AVpOfn+8GsCorK+21/S5ZGblu3rw5Lz4+XiGzmqqIjY3N/Pzzz/fIEKumTHBwMPybwLFW5gf+n3p6enBhn0y5fF5XV7dTvgzyFAqlZ9WqVSfUxMbQ0Vy5cmWZi4uLSFNK0BdeoVD4wsHBoQruex86hwOHAHjpdHojg8Gol3/I8IBhDgHGI3izBQQE/A4/Hx+fHCcnp79GjBgB6/0yygNwQkNDTzk6OpaDJXLgFGmoB1gD2Wx2ozYNLWfPnj0zc+bMGxoSiRhtSUnJxBUrVqTCsODi4nIfTryxt7evUvRWwBUC3i7QzsPD43pQUFDGvHnzzpubm3M9PT2vXbhwwU+TPA0Ut9jtPjs7e/G6devSCgoKdJhMtdzWqZDO6urqQnd3d6v09PS3vby8ep8VoLCnairq6uqYly9f9s/Ly/MsKiqaVF5e7iQQCIyNjIxaDAwM2mg0GgwhsNWA0tbWZsjn803gK8DBwaHS1dW1xMPD44a/v/8FBwcHzV7sNgjxSPZk7Nq1a39mZuZ727dvp3p7e4OL+iDADb6LSCQSXrhwoWTfvn3MdevWfR8VFfXd4KGptifsc6mpqaG3traadnR0iK88gj0pdDq92crKqolEIskc2ahaalQH/f8BQJOAT1RCmKwAAAAASUVORK5CYII=" alt=""/>
      </div>
    </div>
    <div class="wcdiv" style="left:43.2pt; top:215.58pt;">
      <div class="wcdiv" style="left:0pt; top:0pt; width:524.4pt; height:0pt; background-color:#ffffff;">
        <br>
      </div>
      <div class="wcdiv" style="left:0pt; top:-0.25pt; width:524.4pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
    </div>
  </div>
  <div class="wcdiv wcpage" style="width:595.3pt; height:841.9pt;margin:-55px 0;">
    <div class="wcdiv" style="left:42.55pt; top:42.55pt;">
      <div class="wcdiv" style="left:-1.4pt; z-index:1;">
        <div class="wcdiv" style="clip:rect(-1pt,130pt,14pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:10pt; left:0pt; top:0.42pt;">1. Транспортное средство</span></div>
        </div>
        <div class="wcdiv" style="left:143.2pt; clip:rect(-1pt,36.4pt,14pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:10pt; left:8.61pt; top:0.42pt;">"А"</span></div>
        </div>
        <div class="wcdiv" style="left:143.2pt;">
          <div class="wcdiv" style="left:-19.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:10pt; left:8.61pt; top:0.42pt;">{{ $data->full_info->vehicle ? 'X' : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:192.8pt; clip:rect(-1pt,23.7pt,14pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:10pt; left:5.07pt; top:0.42pt;">"В"</span></div>
        </div>
        <div class="wcdiv" style="left:143.2pt;">
          <div class="wcdiv" style="left:30.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:10pt; left:8.61pt; top:0.42pt;">{{ $data->full_info->vehicle ? '' : 'X' }}</span></div>
        </div>
        <div class="wcdiv" style="left:128.75pt; top:0pt; width:0pt; height:12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:142.95pt; top:0.5pt; width:0pt; height:11.5pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:178.35pt; top:0pt; width:0pt; height:12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:192.55pt; top:0.5pt; width:0pt; height:11.5pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:128.75pt; top:0pt; width:14.7pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:178.35pt; top:0pt; width:14.7pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:128.75pt; top:12pt; width:14.7pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:178.35pt; top:12pt; width:14.7pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="left:127.6pt; top:12.5pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:9.9pt; top:0.25pt;">нужное отметить</span></div>
      <div class="wcdiv" style="top:19.4pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:24.32pt;">2. Обстоятельства ДТП</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:74.77pt; top:24.32pt;">&nbsp;</span></div>
      <div class="wcdiv" style="left:77.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:43.5pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->detalization, 150, 13, 1) }}</span></div>
      </div>
      <div class="wcdiv" style="left:37.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:55pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->detalization, 150, 13, 2) }}</span></div>
      </div>
      <div class="wcdiv" style="left:37.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:66pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->detalization, 150, 13, 3) }}</span></div>
      </div>
      <div class="wcdiv" style="left:37.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:77pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->detalization, 150, 13, 4) }}</span></div>
      </div>
      <div class="wcdiv" style="left:37.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:88pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->detalization, 150, 13, 5) }}</span></div>
      </div>
      <div class="wcdiv" style="left:37.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:99pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->detalization, 150, 13, 6) }}</span></div>
      </div>
      <div class="wcdiv" style="left:37.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:111pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->detalization, 150, 13, 7) }}</span></div>
      </div>
      <div class="wcdiv" style="left:37.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:122pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->detalization, 150, 13, 8) }}</span></div>
      </div>
      <div class="wcdiv" style="left:37.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:133pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->detalization, 150, 13, 9) }}</span></div>
      </div>
      <div class="wcdiv" style="left:37.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:144pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->detalization, 150, 13, 10) }}</span></div>
      </div>
      <div class="wcdiv" style="left:37.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:155pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->detalization, 150, 13, 11) }}</span></div>
      </div>
      <div class="wcdiv" style="left:37.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:168pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->detalization, 150, 13, 12) }}</span></div>
      </div>
      <div class="wcdiv" style="left:37.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:180pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->detalization, 150, 13, 13) }}</span></div>
      </div>

      <div class="wcdiv" style="left:77.1pt; top:52.02pt; width:448.75pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:63.3pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:74.57pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:85.84pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:97.12pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:108.39pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:119.67pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:130.94pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:142.21pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:153.49pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:164.76pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:176.04pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:187.31pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.4pt; top:213.96pt; z-index:1;">
        <div class="wcdiv" style="clip:rect(-1pt,122.9pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">3. ТС находилось под управлением</span></div>
        </div>
        <div class="wcdiv" style="left:129.85pt; clip:rect(-1pt,63.95pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:5.31pt; top:0.32pt;">собственника ТС</span></div>
        </div>
        <div class="wcdiv" style="left:129.85pt;">
          <div class="wcdiv" style="left:-12.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7.5pt; left:5.31pt; top:0.32pt;">{{ $data->full_info->driver ? 'X' : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:121.65pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:129.6pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:121.65pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:121.65pt; top:9.12pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="left:-1.4pt; top:225.88pt; z-index:1;">
        <div class="wcdiv" style="left:129.85pt; clip:rect(-1pt,149pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:4.9pt; top:0.32pt;">иного лица, допущенного к управлению ТС</span></div>
        </div>
        <div class="wcdiv" style="left:129.85pt;">
          <div class="wcdiv" style="left:-12.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:7.5pt; left:5.31pt; top:0.32pt;">{{ $data->full_info->driver ? '' : 'X' }}</span></div>
        </div>
        <div class="wcdiv" style="left:121.65pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:129.6pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:121.65pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:121.65pt; top:9.12pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="top:235.51pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:24.32pt;">4. В случае, если в ДТП участвовало более 2-х ТС, указать сведения об этих ТС</span></div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:269pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->moreVehicle, 150, 8, 1) }}</span></div>
      </div>
      <div class="wcdiv" style="top:276.76pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:184.24pt; top:1.75pt;">(марка, модель ТС, государственный регистрационный знак;</span></div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:286pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->moreVehicle, 150, 8, 2) }}</span></div>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:276.76pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="top:293.78pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:169.95pt; top:1.75pt;">наименование страховой организации, серия, номер страхового полиса)</span></div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:303pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->moreVehicle, 150, 8, 3) }}</span></div>
      </div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:314pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->moreVehicle, 150, 8, 4) }}</span></div>
      </div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:325pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->moreVehicle, 150, 8, 5) }}</span></div>
      </div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:336pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->moreVehicle, 150, 8, 6) }}</span></div>
      </div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:347pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->moreVehicle, 150, 8, 7) }}</span></div>
      </div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:0.9pt; top:358pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->moreVehicle, 150, 8, 8) }}</span></div>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:293.78pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:310.8pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:322.08pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:333.35pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:344.62pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:355.9pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:367.17pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="top:369.82pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:24.32pt;">5. Повреждения иного имущества, чем ТС</span></div>
      <div class="wcdiv" style="left:28.35pt; top:402.45pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">Наименование</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:46.82pt; top:0.32pt;">&nbsp;</span></div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:80pt; top:403pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->damageOtherName, 130, 1, 1) }}</span></div>
      </div>
      <div class="wcdiv" style="left:78.8pt; top:411.07pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:167.37pt; top:1.75pt;">(наименование поврежденного имущества)</span></div>
      <div class="wcdiv" style="left:77.35pt; top:411.07pt; width:448.5pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:28.35pt; top:419.47pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">Кому принадлежит</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:61.6pt; top:0.32pt;">&nbsp;</span></div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:90pt; top:420pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->damageOtherOwner, 130, 1, 1) }}</span></div>
      </div>
      <div class="wcdiv" style="left:93.55pt; top:428.09pt; z-index:1;"><span class="wcspan wctext001" style="font-size:6pt; left:168.49pt; top:1.75pt;">(заполняется при наличии сведений)</span></div>
      <div class="wcdiv" style="left:92.1pt; top:428.09pt; width:433.75pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.4pt; top:460.49pt; z-index:1;">
        <div class="wcdiv" style="clip:rect(-1pt,165.45pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">6. Может ли ТС передвигаться своим ходом?</span></div>
        </div>
        <div class="wcdiv" style="left:172.4pt; clip:rect(-1pt,21.4pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">Да</span></div>
        </div>
        <div class="wcdiv" style="left:163.9pt;">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">{{ $data->full_info->canVehicleMove ? 'X' : '' }}</span></div>
        </div>
        <div class="wcdiv" style="left:200.75pt; clip:rect(-1pt,21.4pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">Нет</span></div>
        </div>
        <div class="wcdiv" style="left:192.1pt;">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">{{ $data->full_info->canVehicleMove ? '' : 'X' }}</span></div>
        </div>
        <div class="wcdiv" style="left:164.2pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:172.15pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:192.55pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:200.5pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:164.2pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:192.55pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:164.2pt; top:9.12pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:192.55pt; top:9.12pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="left:28.35pt; top:470.12pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">если "Нет", то где сейчас находится ТС</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:126.35pt; top:0.32pt;">&nbsp;</span></div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:155pt; top:470pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->whereVehicle, 100, 3, 1) }}</span></div>
      </div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:80pt; top:482pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->whereVehicle, 100, 3, 2) }}</span></div>
      </div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:80pt; top:493pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->whereVehicle, 100, 3, 3) }}</span></div>
      </div>
      <div class="wcdiv" style="left:157.3pt; top:478.74pt; width:368.55pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:490.01pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:501.29pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="top:503.94pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:2.32pt;">7. Примечание, в том числе разногласия по п. 14, 15, 16, 17 (при наличии):</span></div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:10pt; top:515pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->comment, 120, 6, 1) }}</span></div>
      </div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:10pt; top:526pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->comment, 120, 6, 2) }}</span></div>
      </div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:10pt; top:538pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->comment, 120, 6, 3) }}</span></div>
      </div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:10pt; top:549pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->comment, 120, 6, 4) }}</span></div>
      </div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:10pt; top:560pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->comment, 120, 6, 5) }}</span></div>
      </div>
      <div class="wcdiv" style="left:3.35pt;">
        <div class="wcdiv" style="left:10pt; top:571pt; z-index:3;"><span class="wcspan wctext002" style="font-size:7.5pt; color:#00000a; left:0.95pt; top:0.32pt;">{{ str_split_by($data->full_info->comment, 120, 6, 6) }}</span></div>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:523.19pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:534.46pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:545.73pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:557.01pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:568.28pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.45pt; top:579.56pt; width:527.3pt; height:0pt; border-top:solid 0.75pt #000000;">
        <br>
      </div>
      <div class="wcdiv" style="left:-1.4pt; top:588.21pt; z-index:1;">
        <div class="wcdiv" style="clip:rect(-1pt,8.1pt,10.62pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0.97pt; top:0.32pt;">“</span></div>
        </div>
        <div class="wcdiv" style="left:26.95pt; clip:rect(-1pt,12.35pt,10.62pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">”</span></div>
        </div>
        <div class="wcdiv" style="left:114.85pt; clip:rect(-1pt,15.2pt,10.62pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:3.9pt; top:0.32pt;">20</span></div>
        </div>
        <div class="wcdiv" style="left:143.25pt; clip:rect(-1pt,59.5pt,10.62pt,-1pt);">
          <div class="wcdiv" style="left:4.25pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">г.</span></div>
        </div>
        <div class="wcdiv" style="left:322.25pt; clip:rect(-1pt,50.6pt,10.62pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:44.3pt; top:0.32pt;">(</span></div>
        </div>
        <div class="wcdiv" style="left:520.1pt; clip:rect(-1pt,8.1pt,10.62pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">)</span></div>
        </div>
        <div class="wcdiv" style="top:8.62pt;">
          <div class="wcdiv" style="left:38.3pt; clip:rect(-1pt,77.55pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:6pt; left:13.98pt; top:0.25pt;">(дата заполнения)</span></div>
          </div>
          <div class="wcdiv" style="left:201.75pt; clip:rect(-1pt,121.5pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:6pt; left:46.31pt; top:0.25pt;">(подпись)</span></div>
          </div>
          <div class="wcdiv" style="left:371.85pt; clip:rect(-1pt,149.25pt,9.4pt,-1pt);">
            <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:4;"><span class="wcspan wctext001" style="font-size:6pt; left:45.09pt; top:0.25pt;">(фамилия, инициалы)</span></div>
          </div>
        </div>
        <div class="wcdiv" style="left:7.1pt; top:8.62pt; width:19.85pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:38.3pt; top:8.62pt; width:76.55pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:129.05pt; top:8.62pt; width:14.2pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:201.75pt; top:8.62pt; width:120.5pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:371.85pt; top:8.62pt; width:148.25pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="left:455.5pt; top:611.38pt; z-index:1;">
        <div class="wcdiv" style="clip:rect(-1pt,63.35pt,11.12pt,-1pt);">
          <div class="wcdiv" style="left:1.4pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">С приложением</span></div>
        </div>
          <div class="wcdiv" style="left:63pt; top:0.5pt; z-index:3;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">{{ $data->full_info->withAppAct ? 'X' : '' }}</span></div>
        <div class="wcdiv" style="left:62.1pt; top:0pt; width:0pt; height:9.12pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:70.05pt; top:0.5pt; width:0pt; height:8.62pt; border-left:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:62.1pt; top:0pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
        <div class="wcdiv" style="left:62.1pt; top:9.12pt; width:8.45pt; height:0pt; border-top:solid 0.75pt #000000;">
          <br>
        </div>
      </div>
      <div class="wcdiv" style="top:621pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:12.32pt;">*** ДТП без участия сотрудников ГИБДД может оформляться в случае одновременно следующих обстоятельств:</span></div>
      <div class="wcdiv" style="left:14.2pt; top:641.63pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">- в результате дорожно-транспортного происшествия вред причинен только имуществу;</span></div>
      <div class="wcdiv" style="left:14.2pt; top:650.25pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">- дорожно-транспортное</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.49pt; left:78.46pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:80.83pt; top:0.32pt;">происшествие</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.49pt; left:126.28pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:128.65pt; top:0.32pt;">произошло</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.49pt; left:164.16pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:166.53pt; top:0.32pt;">с</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.49pt; left:169.86pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:172.23pt; top:0.32pt;">участием</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.49pt; left:201.77pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:204.14pt; top:0.32pt;">двух</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.49pt; left:219pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:221.37pt; top:0.32pt;">транспортных</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.49pt; left:266.66pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:269.03pt; top:0.32pt;">средств,</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.49pt; left:295.28pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:297.65pt; top:0.32pt;">гражданская</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.49pt; left:337.89pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:340.26pt; top:0.32pt;">ответственность</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.49pt; left:392.72pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:395.09pt; top:0.32pt;">владельцев</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.49pt; left:430.89pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:433.26pt; top:0.32pt;">которых</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.49pt; left:460.22pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:462.59pt; top:0.32pt;">застрахована</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.49pt; left:504.28pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:506.65pt; top:0.32pt;">в&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:-14.2pt; top:8.94pt;">соответствии с законодательством;</span></div>
      <div class="wcdiv" style="left:14.2pt; top:667.5pt; z-index:1;"><span class="wcspan wctext001" style="font-size:7.5pt; left:0pt; top:0.32pt;">- обстоятельства</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:52.99pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:55.22pt; top:0.32pt;">причинения</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:93.6pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:95.82pt; top:0.32pt;">вреда</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:113.59pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:115.81pt; top:0.32pt;">в</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:119.36pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:121.58pt; top:0.32pt;">связи</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:138.88pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:141.1pt; top:0.32pt;">с</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:144.43pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:146.65pt; top:0.32pt;">повреждением</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:193.47pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:195.69pt; top:0.32pt;">имущества</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:230.78pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:233.01pt; top:0.32pt;">в</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:236.55pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:238.77pt; top:0.32pt;">результате</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:272.94pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:275.17pt; top:0.32pt;">ДТП,</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:292.15pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:294.38pt; top:0.32pt;">характер</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:322.54pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:324.76pt; top:0.32pt;">и</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:328.78pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:331pt; top:0.32pt;">перечень</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:359.96pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:362.18pt; top:0.32pt;">видимых</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:391.1pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:393.33pt; top:0.32pt;">повреждений</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:436.08pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:438.3pt; top:0.32pt;">транспортных</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.35pt; left:483.6pt; top:0.32pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:485.82pt; top:0.32pt;">средств&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:-14.2pt; top:8.94pt;">не</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:-6.86pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:-4.58pt; top:8.94pt;">вызывают</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:27.76pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:30.04pt; top:8.94pt;">разногласий</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:69.35pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:71.62pt; top:8.94pt;">участников</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:108.05pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:110.33pt; top:8.94pt;">ДТП</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:125.44pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:127.72pt; top:8.94pt;">(за</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:136.51pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:138.79pt; top:8.94pt;">исключением</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:182.32pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:184.6pt; top:8.94pt;">случаев</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:209.4pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:211.68pt; top:8.94pt;">оформления</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:251.08pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:253.36pt; top:8.94pt;">документов</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:290.98pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:293.26pt; top:8.94pt;">о</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:297.01pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:299.29pt; top:8.94pt;">дорожно-транспортном</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:374.79pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:377.07pt; top:8.94pt;">происшествии</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:423.21pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:425.49pt; top:8.94pt;">для</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:436.5pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:438.78pt; top:8.94pt;">получения</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.41pt; left:472.61pt; top:8.94pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:474.89pt; top:8.94pt;">страхового&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:-14.2pt; top:17.57pt;">возмещения</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:24.71pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:27.27pt; top:17.57pt;">в</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:30.81pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:33.37pt; top:17.57pt;">пределах</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:62.43pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:64.99pt; top:17.57pt;">100</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:76.24pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:78.8pt; top:17.57pt;">тысяч</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:97.67pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:100.23pt; top:17.57pt;">рублей</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:122.63pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:125.19pt; top:17.57pt;">в</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:128.73pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:131.29pt; top:17.57pt;">порядке,</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:158.92pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:161.48pt; top:17.57pt;">предусмотренном</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:218.84pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:221.4pt; top:17.57pt;">пунктом</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:248.59pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:251.15pt; top:17.57pt;">5</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:254.9pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:257.46pt; top:17.57pt;">статьи</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:278.11pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:280.67pt; top:17.57pt;">11</span> <span class="wcspan wctext001" style="font-size:5pt; left:288.17pt; top:17.52pt;">1</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:290.67pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:293.23pt; top:17.57pt;">Федерального</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:338.46pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:341.02pt; top:17.57pt;">закона</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:362.05pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:364.61pt; top:17.57pt;">«Об</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:377.59pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:380.15pt; top:17.57pt;">обязательном</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:423.74pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:426.3pt; top:17.57pt;">страховании</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:0.69pt; left:466.39pt; top:17.57pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:468.95pt; top:17.57pt;">гражданской&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:-14.2pt; top:26.19pt;">ответственности</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:38.85pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:42.92pt; top:26.19pt;">владельцев</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:78.73pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:82.8pt; top:26.19pt;">транспортных</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:128.09pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:132.16pt; top:26.19pt;">средств»)</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:162.78pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:166.85pt; top:26.19pt;">и</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:170.87pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:174.94pt; top:26.19pt;">зафиксированы</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:224.51pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:228.59pt; top:26.19pt;">в</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:232.13pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:236.2pt; top:26.19pt;">извещениях</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:274.38pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:278.45pt; top:26.19pt;">о</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:282.2pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:286.27pt; top:26.19pt;">ДТП,</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:303.26pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:307.33pt; top:26.19pt;">бланки</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:329.89pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:333.96pt; top:26.19pt;">которых</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:360.92pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:364.99pt; top:26.19pt;">заполнены</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:399.19pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:403.26pt; top:26.19pt;">водителями</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:440.94pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:445.01pt; top:26.19pt;">причастных</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:483.3pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:487.37pt; top:26.19pt;">к</span> <span class="wcspan wctext001" style="font-size:7.5pt; letter-spacing:2.2pt; left:491.01pt; top:26.19pt;">&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:495.08pt; top:26.19pt;">ДТП&nbsp;</span> <span class="wcspan wctext001" style="font-size:7.5pt; left:-14.2pt; top:34.81pt;">транспортных средств в соответствии с правилами обязательного страхования.</span></div>
      <div class="wcdiv" style="top:710.62pt; z-index:1;"><span class="wcspan wctext002" style="font-size:7.5pt; left:21.37pt; top:6.32pt;">Заполняется в двух экземплярах. Каждый участник ДТП направляет свой экземпляр настоящего бланка страховщику, застраховавшему его&nbsp;</span> <span class="wcspan wctext002" style="font-size:7.5pt; left:0.28pt; top:14.94pt;">гражданскую ответственность. В случае ненаправления виновником ДТП своего экземпляра бланка извещения в течение пяти рабочих дней со дня ДТП&nbsp;</span> <span class="wcspan wctext002" style="font-size:7.5pt; left:133.76pt; top:23.57pt;">страховщик вправе взыскать с него убытки в размере страховой выплаты.</span></div>
    </div>
  </div>
</body>
</html>