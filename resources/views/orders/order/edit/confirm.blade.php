<div class="row form-horizontal" id="main_container">
    <div class="policy-blocks">

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-top:15px; @if($order->status_order_id == 0) padding-bottom:200px @endif">
            <div class="block-view">
                <div class="row">
                    <div class="is_delivery">
                        <div>
                            @php
                                $date = $order->begin_date ? setDateTimeFormatRu($order->begin_date, 1) : \Carbon\Carbon::now()->format('d.m.Y');
                                $time = $order->begin_date ? getDateFormatTimeRu($order->begin_date) : \Carbon\Carbon::now()->format('H:i');
                                $inspection = $order->inspection;
                            @endphp
                            <div class="col-lg-4">
                                <div class="field form-col">
                                    <label class="control-label">Город</label>
                                    @include('orders.order.partials.cities_select', ['inspection' => $inspection])
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="field form-col">
                                    <label class="control-label">Время</label>
                                    {{ Form::text('order[0][inspection][time]', ($time) ? $time : \Carbon\Carbon::now()->format('H:i'), ['class' => 'form-control format-time']) }}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="field form-col">
                                    <label class="control-label">Дата</label>
                                    {{ Form::text('order[0][inspection][date]', $date, ['class' => 'form-control format-date datepicker date']) }}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="field form-col">
                                    <label class="control-label">Продукт</label>
                                    {{ Form::select('order[0][product_id]', $products, $order->product_id, ['class' => 'form-control select2-ws']) }}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="field form-col">
                                    <label class="control-label">Тип</label>
                                    {{ Form::select('order[0][inspection][position_type_id]', \App\Models\Orders\InspectionOrders::POSITION_TYPE, $inspection->position_type_id ? $inspection->position_type_id : 0, ['class' => 'form-control select2-ws', 'id' => 'position_type', 'onchange' => 'get_executors()']) }}
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="field form-col">
                                    <label class="control-label">Адрес</label>
                                    {{ Form::text('order[0][inspection][address]', $inspection->address, ['class' => 'form-control address-autocomplete', 'data-name' => 'organizations_address', 'data-address-type' => 'organizations', 'id' => 'address', $inspection->position_type_id ? 'disabled' : '']) }}
                                    <input type="hidden" name="order[0][inspection][geo_lat]" data-name="organizations_latitude" value="{{ $inspection->geo_lat }}">
                                    <input type="hidden" name="order[0][inspection][geo_lon]" data-name="organizations_longitude" value="{{ $inspection->geo_lon }}">
                                </div>
                            </div>
                            @if(auth()->user()->hasPermission('orders', 'order_supplier_and_fp'))
                                <div class="col-lg-12">
                                    <div class="field form-col">
                                        <label class="control-label">СК</label>
                                        {{ Form::select('order[0][insurance_companies_id]', $sks, $order->insurance_companies_id, ['class' => 'form-control select2-all']) }}
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="field form-col">
                                        <label class="control-label">Поставщик БСО</label>
                                        {{ Form::select('order[0][bso_supplier_id]', $bso_suppliers, $order->bso_supplier_id, ['class' => 'form-control select2-all']) }}
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="field form-col">
                                        <label class="control-label">Фин. политика</label>
                                        {{ Form::select('order[0][financial_policy_id]', \App\Models\Directories\FinancialPolicy::where('is_actual', 1)->where('bso_supplier_id', $order->bso_supplier_id)->where('product_id', $order->product_id)->pluck('title', 'id'), $order->financial_policy_id, ['class' => 'form-control', 'id'=>'financial_policy_id'] ) }}
                                    </div>
                                </div>
                            @endif
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block-view">
                <div class="row">
                    <div class="is_delivery">
                        <div>
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                <label class="control-label">Тип</label>
                                {{ Form::select('order[0][insurer][type]', collect([0=>"ФЛ", 1=>'ЮЛ']), ($order->insurer)?$order->insurer->type:0, ['class' => 'form-control select2-ws', 'id'=>'insurer_type_0', 'data-key'=>'0']) }}
                            </div>
                            <input type="hidden" name="order[0][insurer][id]" value="{{($order->insurer)?$order->insurer->id:0}}"/>
                            <div class="insurer_fl col-xs-12 col-sm-12 col-md-6 col-lg-4">
                                <label class="control-label">ФИО</label>
                                {{ Form::text('order[0][insurer][fio]', ($order->insurer)?$order->insurer->title:'', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_fio_0', 'data-key'=>'0']) }}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label class="control-label">Телефон</label>
                                {{ Form::text("order[0][insurer][phone]", ($order->insurer)?$order->insurer->phone:'', ['class' => 'form-control phone validate']) }}
                            </div>
                            <div class="insurer_ul col-xs-12 col-sm-12 col-md-6 col-lg-4" style="display: none">
                                <label class="control-label">Название</label>
                                {{ Form::text('order[0][insurer][title]', ($order->insurer)?$order->insurer->title:'', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_title_0', 'data-key'=>'0']) }}
                            </div>
                            <input type="hidden" name="order[0][object_insurer][title]" id="object_insurer_title_0" value="{{$object->data()->title}}"/>
                            <input type="hidden" name="order[0][object_insurer][id]"  value="{{$object->data()->id}}"/>
                            <input type="hidden" name="order[0][object_insurer][type]"  value="{{$object->data()->type}}"/>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label class="control-label">Марка</label>
                                {{Form::select("order[0][object_insurer][mark_id]", \App\Models\Vehicle\VehicleMarks::orderBy('title')->get()->pluck('title', 'id')->prepend('Не выбрано', 0), $object->data()->mark_id, ['class' => 'mark-id mark_id select2-all validate', "id"=>"object_insurer_ts_mark_id_0", 'style'=>'width: 100%;', 'onchange'=>"getModelsObjectInsurer(0, 0);"])}}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label class="control-label">Модель</label>
                                {{Form::select("order[0][object_insurer][model_id]", \App\Models\Vehicle\VehicleModels::where('mark_id', $object->data()->mark_id)->orderBy('title')->pluck('title', 'id')->prepend('Не выбрано', 0), $object->data()->model_id, ['class' => 'model_id model-id select2-all validate', "id"=>"object_insurer_ts_model_id_0", 'style'=>'width: 100%;'])}}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label class="control-label">Рег. номер</label>
                                {{ Form::text("order[0][object_insurer][reg_number]", $object->data()->reg_number, ['class' => 'form-control ru_sumb', "id"=>"object_insurer_ts_reg_number_0"]) }}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label class="control-label">Серия БСО</label>
                                {{ Form::text("order[0][inspection][bso_serie]", $inspection->bso_serie, ['class' => 'form-control']) }}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label class="control-label">Номер БСО</label>
                                {{ Form::text("order[0][bso_title]", $order->bso_title, ['class' => 'form-control']) }}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label class="control-label">Клиент</label>
                                {{Form::select("order[0][inspection][client_event_type_id]", \App\Models\Orders\InspectionOrders::CLIENT_EVENT_TYPE, $inspection->client_event_type_id ? $inspection->client_event_type_id : 0, ['class' => 'model_id model-id select2-all', 'style'=>'width: 100%;'])}}
                            </div>

                            @if((int)$order->inspection->position_type_id)
                                @if($order->status_order_id == 2)
                                    @if(auth()->user()->organization_id == $order->inspection->processing_org_id))
                                        @if((int)$order->inspection->status_id < 5)
                                            <span class="btn btn-success btn-right" onclick="arrivedOrder({{$order->id}});">
                                                Приехал
                                            </span>
                                            <span class="btn btn-danger btn-right" onclick="declineOrder({{$order->id}});">
                                                Клиент не приехал
                                            </span>
                                        @elseif((int)$order->inspection->status_id == 5)
                                            <span class="btn btn-success btn-right" onclick="finishOrder({{$order->id}});">
                                                Завершить
                                            </span>
                                        @endif
                                    @endif
                                @endif
                            @else
                                @if($order->status_order_id == 2)
                                    @if($order->inspection->processing_user_id == auth()->id() && $order->inspection->status_id == 4)
                                        <span class="btn btn-success btn-right" onclick="arrivedOrder({{$order->id}});">
                                            Приехал
                                        </span>
                                    @endif
                                    @if($order->inspection->processing_user_id == auth()->id() && $order->inspection->status_id == 5)
                                        <span class="btn btn-success btn-right" onclick="finishOrder({{$order->id}});">
                                            Завершить
                                        </span>
                                    @endif
                                @endif
                            @endif
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
                        </div>
                    </div>
                </div>
            </div>
            @if($tabs_visibility[$order->status_order_id]['edit'])
                <span class="btn btn-success btn-right" onclick="saveOrder({{$order->id}});">
                    Сохранить заявку
                </span>
            @endif
        </div>

        @include('orders.order.partials.information_tabs')

    </div>
</div>