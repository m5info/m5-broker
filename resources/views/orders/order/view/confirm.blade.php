<div class="row form-horizontal" id="main_container">
    <div class="policy-blocks">

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-top:15px; @if($order->status_order_id == 0) padding-bottom:200px @endif">
            <div class="block-view">
                <div class="row">
                    <div class="is_delivery">
                        <div>
                            @php
                                $date = $order->begin_date ? setDateTimeFormatRu($order->begin_date, 1) : \Carbon\Carbon::now()->format('d.m.Y');
                                $time = $order->begin_date ? getDateFormatTimeRu($order->begin_date) : \Carbon\Carbon::now()->format('H:i');
                                $inspection = $order->inspection;
                            @endphp

                            <div class="col-lg-6">
                                <div class="view-field">
                                    <span class="view-label">Город</span>
                                    <span class="view-value">{{ \App\Models\Settings\City::find($inspection->city_id)->title }}</span>
                                    <input type="hidden" name="order[0][inspection][city_id]" value="{{ $inspection->city_id }}">

                                </div>
                                <div class="view-field">
                                    <span class="view-label">Время</span>
                                    <span class="view-value">{{ ($time) ? $time : \Carbon\Carbon::now()->format('H:i') }}</span>
                                    <input type="hidden" name="order[0][inspection][time]" value="{{ ($time) ? $time : \Carbon\Carbon::now()->format('H:i') }}">

                                </div>
                                <div class="view-field">
                                    <span class="view-label">Дата</span>
                                    <span class="view-value">{{ $date }}</span>
                                    <input type="hidden" name="order[0][inspection][date]" value="{{ $date }}">
                                </div>
                                <div class="view-field">
                                    <span class="view-label">Продукт</span>
                                    <span class="view-value">{{ \App\Models\Directories\Products::find($order->product_id)->title }}</span>
                                    <input type="hidden" name="order[0][product_id]" value="{{ $order->product_id }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="view-field">
                                    <span class="view-label">Тип</span>
                                    <span class="view-value">{{ \App\Models\Orders\InspectionOrders::POSITION_TYPE[$inspection->position_type_id ? $inspection->position_type_id : 0] }}</span>
                                    <input type="hidden" id="position_type" name="order[0][inspection][position_type_id]" value="{{ $inspection->position_type_id ? $inspection->position_type_id : 0 }}">
                                </div>
                                <div class="view-field">
                                    <span class="view-label">Адрес</span>
                                    <span class="view-value">{{ $inspection->address }}</span>
                                </div>
                                <input type="hidden" name="order[0][inspection][address]" value="{{ $inspection->address }}">
                                <input type="hidden" name="order[0][inspection][geo_lat]" data-name="organizations_latitude" value="{{ $inspection->geo_lat }}">
                                <input type="hidden" name="order[0][inspection][geo_lon]" data-name="organizations_longitude" value="{{ $inspection->geo_lon }}">
                                <div class="view-field">
                                    <span class="view-label">СК</span>
                                    <span class="view-value">{{ \App\Models\Directories\InsuranceCompanies::find($order->insurance_companies_id)->title }}</span>
                                    <input type="hidden" name="order[0][insurance_companies_id]" value="{{ $order->insurance_companies_id }}">
                                </div>
                                <div class="view-field">
                                    <span class="view-label">Поставщик БСО</span>
                                    <span class="view-value">{{ \App\Models\Directories\BsoSuppliers::find($order->bso_supplier_id)->title }}</span>
                                    <input type="hidden" name="order[0][bso_supplier_id]" value="{{ $order->bso_supplier_id }}">

                                </div>
                                <div class="view-field">
                                    <span class="view-label">Фин. политика</span>
                                    <span class="view-value">{{ $order->financial_policy_id ? \App\Models\Directories\FinancialPolicy::find($order->financial_policy_id)->title : '' }}</span>
                                    <input type="hidden" name="order[0][financial_policy_id]" value="{{ $order->financial_policy_id }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="view-field">
                                    <span class="view-label">Тип</span>
                                    <span class="view-value">{{ [0=>"ФЛ", 1=>'ЮЛ'][($order->insurer)?$order->insurer->type:0] }}</span>
                                </div>
                                <input type="hidden" name="order[0][insurer][type]" value="{{ ($order->insurer)?$order->insurer->type:0 }}">
                                <input type="hidden" name="order[0][insurer][id]" value="{{($order->insurer)?$order->insurer->id:0}}"/>
                                <input type="hidden" name="order[0][object_insurer][title]" id="object_insurer_title_0" value="{{$object->data()->title}}"/>
                                <div class="view-field">
                                    <span class="view-label">ФИО / Название</span>
                                    <span class="view-value">{{ ($order->insurer)?$order->insurer->title:'' }}</span>
                                    <input type="hidden" name="order[0][insurer][fio]" value="{{ ($order->insurer)?$order->insurer->title:'' }}">
                                </div>
                                <div class="view-field">
                                    <span class="view-label">Телефон</span>
                                    <span class="view-value">{{ ($order->insurer)?$order->insurer->phone:'' }}</span>
                                </div>
                                <input type="hidden" name="order[0][insurer][phone]" value="{{ ($order->insurer)?$order->insurer->phone:'' }}">
                                <input type="hidden" name="order[0][object_insurer][id]"  value="{{$object->data()->id}}"/>
                                <input type="hidden" name="order[0][object_insurer][type]"  value="{{$object->data()->type}}"/>
                            </div>
                            <div class="col-lg-6">
                                <div class="view-field">
                                    <span class="view-label">Марка / Модель</span>
                                    <span class="view-value">
                                        {{ $object->data()->mark_id ? \App\Models\Vehicle\VehicleMarks::find($object->data()->mark_id)->title : '' }} /
                                        {{ $object->data()->model_id ? \App\Models\Vehicle\VehicleModels::find($object->data()->model_id)->title : '' }}
                                    </span>
                                    <input type="hidden" name="order[0][object_insurer][mark_id]" value="{{ $object->data()->mark_id }}">
                                    <input type="hidden" name="order[0][object_insurer][model_id]" value="{{ $object->data()->model_id }}">
                                </div>

                                <div class="view-field">
                                    <span class="view-label">Рег. номер</span>
                                    <span class="view-value">{{ $object->data()->reg_number }}</span>
                                    <input type="hidden" name="order[0][object_insurer][reg_number]" value="{{ $object->data()->reg_number }}">

                                </div>
                                <div class="view-field">
                                    <span class="view-label">Полис</span>
                                    <span class="view-value">{{ $inspection->bso_serie }} {{ $order->bso_title }}</span>
                                    <input type="hidden" name="order[0][inspection][bso_serie]" value="{{ $inspection->bso_serie }}">
                                    <input type="hidden" name="order[0][bso_title]" value="{{ $order->bso_title }}">
                                </div>

                                <div class="view-field">
                                    <span class="view-label">Клиент</span>
                                    <span class="view-value">{{ \App\Models\Orders\InspectionOrders::CLIENT_EVENT_TYPE[$inspection->client_event_type_id ? $inspection->client_event_type_id : 0] }}</span>
                                    <input type="hidden" name="order[0][inspection][client_event_type_id]" value="{{ $inspection->client_event_type_id ? $inspection->client_event_type_id : 0 }}">

                                </div>
                            </div>

                            @if((int)$order->inspection->position_type_id)
                                @if($order->status_order_id == 2)
                                    @if(auth()->user()->organization_id == $order->inspection->processing_org_id)
                                        @if((int)$order->inspection->status_id < 5)
                                            <span class="btn btn-success btn-right" onclick="arrivedOrder({{$order->id}});">
                                                Приехал
                                            </span>
                                            <span class="btn btn-danger btn-right" onclick="declineOrder({{$order->id}});">
                                                Клиент не приехал
                                            </span>
                                        @elseif((int)$order->inspection->status_id == 5)
                                            <span class="btn btn-success btn-right" onclick="finishOrder({{$order->id}});">
                                                Завершить
                                            </span>
                                        @endif
                                    @endif
                                @endif
                            @else
                                @if($order->status_order_id == 2)
                                    @if($order->inspection->processing_user_id == auth()->id() && $order->inspection->status_id == 4)
                                        <span class="btn btn-success btn-right" onclick="arrivedOrder({{$order->id}});">
                                            Приехал
                                        </span>
                                    @endif
                                    @if($order->inspection->processing_user_id == auth()->id() && $order->inspection->status_id == 5)
                                        <span class="btn btn-success btn-right" onclick="finishOrder({{$order->id}});">
                                            Завершить
                                        </span>
                                    @endif
                                @endif
                            @endif
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
                        </div>
                    </div>
                </div>
            </div>
            @if($tabs_visibility[$order->status_order_id]['edit'])
                <span class="btn btn-success btn-right" onclick="saveOrder({{$order->id}});">
                    Сохранить заявку
                </span>
            @endif
        </div>

{{--        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-top:15px;">--}}
{{--            <div class="block-view">--}}
{{--                <div class="row">--}}
{{--                    <div class="is_delivery">--}}
{{--                        <div>--}}
{{--                            --}}
{{--                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}



        @include('orders.order.partials.information_tabs')
    </div>
</div>