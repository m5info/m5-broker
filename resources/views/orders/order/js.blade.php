@section('js')
    <style>
        .block-view{
            padding: 20px 35px 35px 35px;
        }
        .contract-block{
            display: block;
            padding: 15px 15px 15px 15px;
            border: 1px solid #e4e4e4;
            margin-bottom: 11px;
            margin-top: 10px;
            border-radius: 8px;
            background-color: #fff;
            box-shadow: 0 5px 20px rgba(0, 0, 0, .15);
        }
        .contract-block div{
            color: #7d7f81;
            font-family: "AgoraSansProRegular", "serif";
            font-size: 14px;
            font-weight: normal;
        }
        .contract-block:hover,
        .contract-block:visited,
        .contract-block:link,
        .contract-block:active
        {
            text-decoration: none;
        }
        .choosen_yamap {
            background: #d9ebc6 !important;
        }
    </style>

    <script src="/js/jquery.datetimepicker.full.min.js"></script>
    <script>

        function go_point(id){
            map.geoObjects.each(function (item) {

                if (item.properties.get('id') == id) {
                    var coord = item.geometry.getCoordinates();

                    map.panTo([parseFloat(coord[0]), parseFloat(coord[1])], {
                        // Задержка между перемещениями.
                        delay: 1500
                    });
                    item.balloon.open();
                }
            });

        }

        function moveTo() {

            var option = $("#cities option:selected");

            if (option.length == 0){
                var option = $("input#cities")
            }

            latitude = option.data('geo_lat');
            longitude = option.data('geo_lon');

            map.panTo([parseFloat(latitude), parseFloat(longitude)], {
                // Задержка между перемещениями.
                delay: 1500
            });


        }

        function set_tr(id){
            var tr = $('.table_for_yamap tr');

            var position_type = $('#position_type').val();

            if (position_type == 1){
                $('#orginization_id').val(id);
            }else{
                $('#user_org_id').val(id);
            }

            tr.each(function (index, value){
                if ($(this).data('id') == id) {
                    $(this).addClass('choosen_yamap');
                    $(this).click();
                }else{
                    $(this).removeClass('choosen_yamap');
                }
            });
        }

        $(function(){
            // для назначения
            @if($order->status_order_id == 1)
                initMaps();
                setTimeout(function () {
                    get_executors()
                }, 500);
            @endif

            // для визуализации перемещения
            @if($order->status_order_id > 1 && \App\Models\Orders\InspectionOrdersLogs::where('inspection_order_id', '=', $order->id)->where('geo_lon', '>', 0)->get()->count())
                initMaps();
                setTimeout(function () {
                    get_moving_logs()
                }, 500);
            @endif
        });


        $(document).on('click', '#inspection_export_act', function () {

            loaderShow();
            get_act();

        });

        function get_act(){
            $.ajax({
                type: "GET",
                url: "{{url("get_mobile_act/".$order->getTokenAct())}}",
                async: true,

                success: function (response) {
                    var link = document.createElement('a');

                    link.setAttribute('href', response);
                    link.setAttribute('download', 'download');

                    link.click();
                    loaderHide();
                }
            });
        }

        $(document).on('click', '.table_for_yamap tr', function () {

            var tr = $('.table_for_yamap tr');

            var position_type = $('#position_type').val();

            if (position_type == 1){
                get_executor_detail(position_type, $(this).data('id'));
                $('#orginization_id').val($(this).data('id'));
            }else{
                get_executor_detail(position_type, $(this).data('id'));
                $('#user_org_id').val($(this).data('id'));
            }

            go_point($(this).data('id'));

            tr.each(function (index, value){
                $(this).removeClass('choosen_yamap');
            });
            $(this).addClass('choosen_yamap');
        });

        function get_executor_detail(position_type, executor_id){

            $.ajax({
                type: "POST",
                url: "{{url("/orders/edit/{$order->id}/get_executor_detail")}}",
                async: false,
                data: {'position_type': position_type, 'executor_id': executor_id },

                success: function (response) {
                    $('#executor_detail').html(response['html']);

                }
            });

        }


        $('.validate').change(function () {

            $(this).css("border-color", "");

            if ($(this)[0].localName === 'select') {
                var container = $(this).prev('.select2-container');
                container.css({'border': 'none'});
            }
        });

        function change_event(status_id) {
            if (validationInputsOrder()) {

                $.ajax({
                    type: "POST",
                    url: "{{url("/orders/edit/{$order->id}/change_event")}}",
                    async: false,
                    data: 'status_id='+status_id,

                    success: function (response) {
                        saveOrder({{$order->id}});
                    }
                });
            }
        }

        function change_status(status_id) {
            if (validationInputsOrder()) {

                $.ajax({
                    type: "POST",
                    url: "{{url("/orders/edit/{$order->id}/change_status")}}",
                    async: false,
                    data: 'status_id='+status_id,

                    success: function (response) {
                        saveOrder({{$order->id}});
                    }
                });
            }
        }

        function validationInputsOrder() {

            var first_err;
            var msg_arr = [];
            var i = 0;

            $('.validate').each(function () {

                var tag = $(this)[0].localName;

                var val = '';
                if (['select', 'input'].indexOf(tag) !== -1) {
                    val = $(this).val();
                } else {
                    val = $(this).html();
                }


                if (tag === 'select' && parseInt(val) === 0) {

                    if(i == 0 && $(this).parent('div').css('display') != 'none'){
                        msg_arr.push('Заполните все поля');
                        first_err = $(this);
                        i++;
                    }
                    var container = $(this).prev('.select2-container');
                    container.css({
                        'height': '36px',
                        'border': '1px red solid'
                    });
                }

                if (val.length < 1) {
                    if(i == 0 && $(this).parent('div').css('display') != 'none'){
                        first_err = $(this);
                        i++;
                    }
                    msg_arr.push('Заполните все поля');

                    var elem = $(this);

                    elem.css("border-color", "red");


                }

            });

            // проверка на существование бсо
            $('.temp_contract').each(function (index, value) {
                var bso = $('input[name="contract['+index+'][bso_title]"]').val();

                $.ajax({
                    type: "POST",
                    url: "{{url('/contracts/fast_accept/check_correct_bso')}}",
                    async: false,
                    data: {'bso':bso},
                    success: function (res) {

                        if (res == 0){
                            first_err = $('input[name="contract['+index+'][bso_title]"]');
                            i++;
                            msg_arr.push('Заполните все поля');

                            var elem = $('input[name="contract['+index+'][bso_title]"]');
                            elem.css("border-color", "red");
                        }
                    }
                });
            });

            // проверка на существование квитанции
            $('.temp_contract_receipt').each(function (index, value) {
                var bso = $('input[name="contract['+index+'][payment][0][bso_receipt]"]').val();

                $.ajax({
                    type: "POST",
                    url: "{{url('/contracts/fast_accept/check_correct_bso')}}",
                    async: false,
                    data: {'bso':bso},
                    success: function (res) {

                        if (res == 0 && $('input[name="contract['+index+'][payment][0][bso_receipt]"]').parent('div').css('display') != 'none'){
                            first_err =  $('input[name="contract['+index+'][payment][0][bso_receipt]"]');
                            i++;
                            msg_arr.push('Заполните все поля');

                            var elem =  $('input[name="contract['+index+'][payment][0][bso_receipt]"]');
                            elem.css("border-color", "red");
                        }
                    }
                });
            });

            if (msg_arr.length) {
                $('html, body').animate({scrollTop: $(first_err).offset().top - 300}, 800);

                return false;
            }


            return true;
        }

        function get_executors() {


            var position_type = $('#position_type').val();
            var city_id = $('#cities').val();

            if (position_type == 1){
                $('#address').prop("disabled", true);
                get_autowashes(city_id);
            }else{
                $('#address').prop("disabled", false);
                get_people(city_id);
            }

            moveTo();

        }

        function get_autowashes(city_id){

            $.ajax({
                type: "POST",
                url: "{{url("/orders/edit/{$order->id}/get_autowashes")}}",
                async: true,
                data: {'city_id': city_id},

                success: function (response) {
                    $('#executers').html(response['html']);

                    map.geoObjects.removeAll();

                    response['orgs'].map(function (item) {

                        myGeoObject_e = new ymaps.GeoObject({
                            geometry: {
                                type: "Point",// тип геометрии - точка
                                coordinates: [item.geo_lat, item.geo_lng] // координаты точки
                            },
                            properties: {
                                balloonContentHeader: item.title,
                                balloonContentBody: item.address,
                                id: item.id
                            }
                        });

                        myGeoObject_e.events.add('click', function (e) {
                            // Метка, на которой сработало событие
                            var params = e.get('target').properties._data;

                            set_tr(params.id);
                        });

                        return map.geoObjects.add(myGeoObject_e);
                    });


                }
            });

        }

        function get_people(city_id){

            $.ajax({
                type: "POST",
                url: "{{url("/orders/edit/{$order->id}/get_people")}}",
                async: true,
                data: {'city_id': city_id},

                success: function (response) {
                    $('#executers').html(response['html']);


                    response['orgs'].map(function (item) {

                        myGeoObject_e = new ymaps.GeoObject({
                            geometry: {
                                type: "Point",// тип геометрии - точка
                                coordinates: [item.fact_address_wth, item.fact_address_lng] // координаты точки
                            },
                            properties: {
                                balloonContentHeader: item.name,
                                balloonContentBody: item.title,
                                id: item.id
                            }
                        });

                        myGeoObject_e.events.add('click', function (e) {
                            // Метка, на которой сработало событие
                            var params = e.get('target').properties._data;

                            set_tr(params.id);
                        });

                        map.geoObjects.add(myGeoObject_e);
                    });


                    if(response['mission_point']){

                        myGeoObject_m = new ymaps.GeoObject({
                            geometry: {
                                type: "Point",// тип геометрии - точка
                                coordinates: [response['mission_point'].geo_lat, response['mission_point'].geo_lon] // координаты точки
                            },
                            properties: {
                                balloonContentHeader: response['mission_point'].address,
                                balloonContentBody: 'Точка выезда',
                                id: response['mission_point'].id
                            }
                        }, {
                            preset: 'islands#redAutoIcon',
                            hintHideTimeout: 0
                        });

                        map.geoObjects.add(myGeoObject_m);
                    }
                }
            });

        }

        function changeStatus(status_id) {

            if (validationInputsOrder()) {
                var data = $("#main_container").find("select, textarea, input").serialize();
                var message = $('[name="order[0][workarea_comment]"]').serialize();
                var status_to_change = $('#status_to_change').serialize();

                $.ajax({
                    type: "POST",
                    url: "{{url("/orders/save/{$order->id}")}}",
                    async: false,
                    data: data + '&' +message+ '&' +status_to_change,

                    success: function (response) {
                        flashHeaderMessage('Успешно сохранено!', 'success');
                        reload();
                    }
                });
            }

        }


        function changeStatusDown(status_id) {

            if (validationInputsOrder()) {
                var data = $("#main_container").find("select, textarea, input").serialize();
                var message = $('[name="order[0][workarea_comment]"]').serialize();
                var status_to_change = $('#status_back').serialize();

                $.ajax({
                    type: "POST",
                    url: "{{url("/orders/save/{$order->id}")}}",
                    async: false,
                    data: data + '&' +message+ '&' +status_to_change,

                    success: function (response) {
                        flashHeaderMessage('Успешно сохранено!', 'success');
                        reload();
                    }
                });
            }

        }

        function saveOrder(id){

            if (validationInputsOrder()) {
                var data = $("#main_container").find("select, textarea, input").serialize();
                var message = $('[name="order[0][workarea_comment]"]').serialize();

                $.ajax({
                    type: "POST",
                    url: "{{url("/orders/save/{$order->id}")}}",
                    async: false,
                    data: data + '&' +message,

                    success: function (response) {
                        flashHeaderMessage('Успешно сохранено!', 'success');
                        reload();
                    }
                });
            }

        }

        function setExecutor(id){

            if (validationInputsOrder()){
                var data = $("#main_container").find("select, textarea, input").serialize();
                var message = $('[name="order[0][workarea_comment]"]').serialize();
                var set = 'order%5B0%5D%5Bset%5D=1';


                $.ajax({
                    type: "POST",
                    url: "{{url("/orders/edit/{$order->id}/set_executor")}}",
                    async: false,
                    data: data + '&' +message + '&' + set,

                    success: function (response) {
                        saveOrder({{ $order->id }});
                    }
                });
            }

        }

        function declineOrder() {

            var data = $("#main_container").find("select, textarea, input").serialize();
            var message = $('[name="order[0][workarea_comment]"]').serialize();
            var decline = 'order%5B0%5D%5Bdecline%5D=1';


            $.ajax({
                type: "POST",
                url: "{{url("/orders/edit/{$order->id}/decline")}}",
                async: false,
                data: data + '&' +message + '&' + decline,

                success: function (response) {
                    saveOrder({{ $order->id }});
                }
            });

        }


        function processingUser() {

            var data = $("#main_container").find("select, textarea, input").serialize();
            var message = $('[name="order[0][workarea_comment]"]').serialize();

            $.ajax({
                type: "POST",
                url: "{{url("/orders/edit/{$order->id}/processing_user")}}",
                async: false,
                data: data + '&' +message,

                success: function (response) {
                    saveOrder({{ $order->id }});
                }
            });

        }

        function arrivedOrder() {

            var data = $("#main_container").find("select, textarea, input").serialize();
            var message = $('[name="order[0][workarea_comment]"]').serialize();


            $.ajax({
                type: "POST",
                url: "{{url("/orders/edit/{$order->id}/arrived_order")}}",
                async: false,
                data: data + '&' +message,

                success: function (response) {
                    saveOrder({{ $order->id }});
                }
            });

        }

        function finishOrder() {

            var data = $("#main_container").find("select, textarea, input").serialize();
            var message = $('[name="order[0][workarea_comment]"]').serialize();


            $.ajax({
                type: "POST",
                url: "{{url("/orders/edit/{$order->id}/finish_order")}}",
                async: false,
                data: data + '&' +message,

                success: function (response) {
                    saveOrder({{ $order->id }});
                }
            });

        }

        function deleteOrder(id){
            $.ajax({
                type: "POST",
                url: "{{url("/orders/delete/{$order->id}")}}",
                async: false,
                data: {id: id},
                success: function (response) {
                    location.href = '{{ url("orders") }}';
                }
            });
        }


        // function initManyDocuments() {
        //     $("#addManyDocForm").dropzone({
        //         //Dropzone.options.addOrgDocForm = {
        //         paramName: 'file',
        //         maxFilesize: 10,
        //         acceptedFiles: ".png, .jpg, .jpeg, .txt, .csv, .mp4,.mov,.avi,.mpeg4,.flv,.3gpp,image/*",
        //         init: function () {
        //             this.on("complete", function () {
        //                 if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
        //                     reload();
        //                 }
        //
        //             });
        //         }
        //     });
        //
        // }

        var odd = 0;

        function get_percent_from(integer, percent){
            return integer/(100/percent);
        }

        function activeDocuments() {
            $("a.iframe").fancybox({
                afterShow: function(){
                    var click = 1;
                    $('.fancybox-wrap').on('click', '#rotate_left', function(){
                        var n = 90 * click++;
                        odd++;

                        $('.fancybox-inner img.fs-galery').css('webkitTransform', 'rotate(-' + n + 'deg)');
                        $('.fancybox-inner img.fs-galery').css('mozTransform', 'rotate(-' + n + 'deg)');

                        var width = $('.fancybox-inner img.fs-galery').width();
                        var height = $('.fancybox-inner img.fs-galery').width();

                        if (odd % 2 == 0) {
                            $('.fancybox-skin').width(width + get_percent_from(width, 10));
                            $('.fancybox-inner').width(width + get_percent_from(width, 10));
                            $('.fancybox-skin').height(height + get_percent_from(width, 60));
                            $('.fancybox-inner').height(height + get_percent_from(width, 60));
                        }else{
                            $('.fancybox-skin').width(width + get_percent_from(width, 10));
                            $('.fancybox-inner').width(width + get_percent_from(width, 10));
                            $('.fancybox-skin').height(height + get_percent_from(width, 10));
                            $('.fancybox-inner').height(height + get_percent_from(width, 10));
                        }
                    });

                    $('.fancybox-wrap').on('click', '#rotate_right', function(){
                        var n = 90 * click++;
                        odd++;

                        $('.fancybox-inner img.fs-galery').css('webkitTransform', 'rotate(' + n + 'deg)');
                        $('.fancybox-inner img.fs-galery').css('mozTransform', 'rotate(' + n + 'deg)');

                        var width = $('.fancybox-inner img.fs-galery').width();
                        var height = $('.fancybox-inner img.fs-galery').width();

                        if (odd % 2 == 0) {
                            $('.fancybox-skin').width(width + get_percent_from(width, 10));
                            $('.fancybox-inner').width(width + get_percent_from(width, 10));
                            $('.fancybox-skin').height(height + get_percent_from(width, 60));
                            $('.fancybox-inner').height(height + get_percent_from(width, 60));
                        }else{
                            $('.fancybox-skin').width(width + get_percent_from(width, 10));
                            $('.fancybox-inner').width(width + get_percent_from(width, 10));
                            $('.fancybox-skin').height(height + get_percent_from(width, 10));
                            $('.fancybox-inner').height(height + get_percent_from(width, 10));
                        }
                    });
                }
            })
        }

        $(document).ready(function() {
            activeDocuments();
        });

        function removeDocument(fileName, documentId) {
            if (!customConfirm()) {
                return false;
            }
            var filesUrl = '{{ url("/contracts/actions/contract/{$order->id}/document/") }}';
            var fileUrl = filesUrl + '/' + documentId;
            $.post(fileUrl, {
                _method: 'DELETE'
            }, function () {
                reload();
            });
        }

        $( ".datepicker" ).datepicker({ dateFormat: 'dd.mm.yy' });


        function formatTime() {
            var configuration = {
                timepicker: true,
                datepicker: false,
                format: 'H:i',
                scrollInput: false
            };
            $.datetimepicker.setLocale('ru');
            $('input.format-time').datetimepicker(configuration).keyup(function (event) {
                if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 38 && event.keyCode != 40) {
                    var pattern = new RegExp("[0-9:]{5}");
                    if (pattern.test($(this).val())) {
                        $(this).datetimepicker('hide');
                        $(this).datetimepicker('show');
                    }
                }
            });
            $('input.format-time').each(function () {
                var im = new Inputmask("99:99", {
                    "oncomplete": function () {
                    }
                });
                im.mask($(this));
            });
        }

        function init()
        {

            formatTime();

            $('#is_delivery').change( function () {
                if (this.checked) {
                    $('.is_delivery').show();
                } else {
                    $('.is_delivery').hide();
                }
            });

            $('#insurer_type_0').change(function () {

                if ($("#insurer_type_owner").is(':checked')) {
                    return;
                }


                if (parseInt($(this).val()) == 0) {
                    $('.insurer_fl').show();
                    $('.insurer_ul').hide();
                    $('.insurer_ip').hide();
                } else if (parseInt($(this).val()) == 1) {
                    $('.insurer_fl').hide();
                    $('.insurer_ul').show();
                    $('.insurer_ip').hide();
                } else if (parseInt($(this).val()) == 2) {
                    $('.insurer_fl').hide();
                    $('.insurer_ul').hide();
                    $('.insurer_ip').show();
                }


            });


            $("#insurer_type_owner").change(function () {
                if (this.checked) {
                    $('.insurer_fl').hide();
                    $('.insurer_ul').hide();
                    $('.insurer_ip').hide();
                } else {
                    $('#insurer_type_0').trigger('change');
                }
            });


            $('#owner_type_0').change(function () {
                if (parseInt($(this).val()) == 0) {
                    $('.owner_fl').show();
                    $('.owner_ul').hide();
                    $('.owner_ip').hide();
                } else if (parseInt($(this).val()) == 1) {
                    $('.owner_fl').hide();
                    $('.owner_ul').show();
                    $('.owner_ip').hide();
                } else if (parseInt($(this).val()) == 2) {
                    $('.owner_fl').hide();
                    $('.owner_ul').hide();
                    $('.owner_ip').show();
                }

            });

            $('#driver_type_0').change(function () {
                if (parseInt($(this).val()) == 0) {
                    $('.driver_limited').show();
                    $('.driver_count').show();
                    $('.driver_unlimited').hide();
                    $('.driver').show();

                } else if (parseInt($(this).val()) == 1) {
                    $('.driver_limited').hide();
                    $('.driver_count').hide();
                    $('.driver_unlimited').show();
                    $('.driver').hide();
                }

            });


            $('#insurer_type_0').change();

            $('#owner_type_0').change();
            $('#driver_type_0').change();
            $('#driver_count_0').change();
            /*$('#object_insurer_ts_mark_id_0').change();*/

            $('#insurer_fio_' + 0).suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "NAME",
                count: 5,
                onSelect: function (suggestion) {
                    key = $(this).data('key');
                    $('#insurer_title_' + key).val($(this).val());

                }
            });


            $('#delivery_address').suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "ADDRESS",
                count: 5,
                onSelect: function (suggestion) {
                    key = $(this).data('key');
                    $('#address_kladr').val(suggestion.data.kladr_id);
                    $('#address_width').val(suggestion.data.geo_lat);
                    $('#address_longitude').val(suggestion.data.geo_lon);
                }
            });


            var im = new Inputmask("9999", {"yearrange": {minyear: 1900, maxyear: 2099}});
            im.mask($('#carYear'));


            @if($order->status_order_id > 1)
                initManyDocuments();
            @endif
        }


        function getModelsObjectInsurer(KEY, select_model_id)
        {

            $.getJSON('{{url("/contracts/actions/get_models")}}', {categoryId: $('#object_insurer_ts_category_' + KEY).val(), markId: $('#object_insurer_ts_mark_id_' + KEY).select2('val')}, function (response) {

                var options = "<option value='0'>Не выбрано</option>";
                response.map(function (item) {
                    options += "<option value='" + item.id + "'>" + item.title + "</option>";
                });
                $('#object_insurer_ts_model_id_' + KEY).html(options).select2('val', select_model_id);


            });

            var current = parseInt($('#object_insurer_ts_category_' + KEY).val());

            if (current == 3) {
                $('#object_insurer_weight_' + KEY).show();
                $('#object_insurer_capacity_' + KEY).show();
                $('#object_insurer_passengers_count_' + KEY).hide();
            } else if (current == 4) {
                $('#object_insurer_weight_' + KEY).hide();
                $('#object_insurer_capacity_' + KEY).hide();
                $('#object_insurer_passengers_count_' + KEY).show();
            } else {
                $('#object_insurer_weight_' + KEY).hide();
                $('#object_insurer_capacity_' + KEY).hide();
                $('#object_insurer_passengers_count_' + KEY).hide();
            }
        }

        function join(arr /*, separator */) {
            var separator = arguments.length > 1 ? arguments[1] : ", ";
            return arr.filter(function (n) {
                return n
            }).join(separator);
        }

        function formatCity(suggestion) {
            var address = suggestion.data;
            if (address.city_with_type === address.region_with_type) {
                return address.settlement_with_type || "";
            } else {
                return join([
                    address.city_with_type,
                    address.settlement_with_type]);
            }
        }




        document.addEventListener("DOMContentLoaded", function (event) {
            init();
        });

        function get_moving_logs() {

            var position_type = $('#position_type').val();
            var city_id = $('#cities').val();

            if (position_type != 1){


                $.ajax({
                    type: "POST",
                    url: "{{url("/orders/edit/{$order->id}/get_moving_logs")}}",
                    async: false,
                    data: {'city_id': city_id},

                    success: function (response) {

                        var coordinates = [];

                        response['moving_logs'].map(function (item) {
                            var new_item = [item.geo_lat , item.geo_lon];

                            coordinates.push(new_item);

                            var deal = '';
                            switch (item.status) {
                                case 3:
                                    deal = 'отказался от заявки';
                                    break;
                                case 4:
                                    deal = 'принял заявку';
                                    break;
                                case 5:
                                    deal = 'приехал на заявку';
                                    break;
                                case 6:
                                    deal = 'завершил заявку';
                                    break;
                            }

                            myGeoObject_e = new ymaps.GeoObject({
                                geometry: {
                                    type: "Point",// тип геометрии - точка
                                    coordinates: [item.geo_lat, item.geo_lon] // координаты точки
                                },
                                properties: {
                                    balloonContentHeader: deal,
                                    balloonContentBody: item.name + ' ' + deal,
                                    id: item.id
                                }
                            });

                            map.geoObjects.add(myGeoObject_e);
                        });

                        // Создаем ломаную, используя класс GeoObject.
                        var myGeoObject = new ymaps.GeoObject({
                            geometry: {
                                type: "LineString",
                                // Указываем координаты вершин ломаной.
                                coordinates: coordinates
                            },
                            properties:{
                                hintContent: "Перемещение",
                                balloonContent: "Перемещение"
                            }
                        }, {
                            // Задаем опции геообъекта.
                            strokeColor: "#3f3f3f",
                            // Ширина линии.
                            strokeWidth: 7
                        });

                        map.geoObjects.add(myGeoObject);

                        if(response['mission_point']){

                            myGeoObject_m = new ymaps.GeoObject({
                                geometry: {
                                    type: "Point",// тип геометрии - точка
                                    coordinates: [response['mission_point'].geo_lat, response['mission_point'].geo_lon] // координаты точки
                                },
                                properties: {
                                    balloonContentHeader: response['mission_point'].address,
                                    balloonContentBody: 'Точка выезда',
                                    id: response['mission_point'].id
                                }
                            }, {
                                preset: 'islands#redAutoIcon',
                                hintHideTimeout: 0
                            });

                            map.geoObjects.add(myGeoObject_m);
                        }
                    }
                });

            }

            moveTo();
        }


        function changeStatusAccept()
        {
            response = myGetAjax("{{url("/orders/accept/{$order->id}/")}}");
            if(response.status == true){
                flashHeaderMessage(response.msg, 'info');
                window.location = '{{url("/orders/")}}';
            }else{
                flashHeaderMessage(response.msg, 'danger');
            }
        }

    </script>
    <link rel="stylesheet" href="/css/jquery.datetimepicker.min.css">
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

@endsection