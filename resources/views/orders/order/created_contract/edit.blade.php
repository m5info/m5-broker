@extends('layouts.app')
@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">
@append

@section('work-area')
    @include("contracts.orders.order.created_contract.partials.workarea")
@append

@section('content')
<div id="main_container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Условия договора -->
    @include("contracts.orders.order.created_contract.partials.terms")


        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <!-- Страхователь -->
        @include("contracts.orders.order.created_contract.partials.insurer")

        <!-- Объект страхования -->
        @if(!$permission)
            @include("contracts.orders.order.created_contract.partials.insurance_object")
        @endif

        <!-- Документы -->
            @include("contracts.orders.order.created_contract.partials.documents")
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <!-- Платежи -->
        @include("contracts.orders.order.created_contract.partials.payments")

    </div>
</div>
@endsection
@section('js')
    <script>

        $(function(){
            initWorkArea();
        });

        function save_contract(){

            $.ajax({
                type: "POST",
                url: "{{url("/contracts/temp_contracts/contract/{$contract->id}/edit")}}",
                async: false,
                data: $("#main_container").find("select, textarea, input").serialize(),

                success: function (response) {
                    flashHeaderMessage('Успешно сохранено!', 'success');
                }
            });
        }


        function process(operations = []) {
            $.each(operations, function (k, operation) {
                if (isFunction(operation)) {
                    window[operation]()
                }
            })
        }

        function selectBso(object_id, key, type, suggestion) {
            var data = suggestion.data;
            $("#bso_receipt_id" + key).val(data.bso_id);
        }



        function addPayment() {

            save_contract();

            setTimeout(function(){
                if (myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/add_pay/")}}")) {
                    reload();

                }
            }, 50);

        }

    </script>
@endsection