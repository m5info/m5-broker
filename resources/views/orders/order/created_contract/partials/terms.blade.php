<div class="row col-xs-12 col-sm-6 col-md-6 col-lg-6">
    <div class="page-subheading">
        <h2>Условия договора</h2>
    </div>
    <div class="block-main">
        <div class="block-sub">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">Андеррайтер</label>
                    <br>
                    <br>
                    <p>{{$contract->check_user ? $contract->check_user->name : "Нет"}}</p>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">Номер договора</label>
                    {{ Form::text('contract[0][bso_title]', '', ['class' => 'form-control', 'id'=>'bso_title_0'  ]) }}
                    <input type="hidden" name="contract[0][bso_id]" id="bso_id_0" value=" "/>
                    <input type="hidden" name="contract[0][bso_supplier_id]" id="bso_supplier_id_0"  value="" />
                    <input type="hidden" name="contract[0][insurance_companies_id]" id="insurance_companies_id_0"  value=" " />
                    <input type="hidden" name="contract[0][product_id]" id="product_id_0"  value=" " />
                    <input type="hidden" name="contract[0][agent_id]" id="agent_id_0"  value=" " />

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label>{{($contract->bso)?$contract->bso->insurance->title:''}} {{($contract->bso)?$contract->bso->supplier_org->title:''}}</label>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">ФИО Агента</label>
                    <br>
                    <br>
                    <p>{{$contract->agent ? $contract->agent->name : ""}}</p>
                </div>

                @if(!$permission)

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">Заявка из фронт офиса</label>
                    <input class="form-control" id="order_title_0" name="contract[0][order_title]" type="text" value="{{$contract->order_title}}" >
                    <input type="hidden" name="contract[0][order_id]" id="order_id_0" value="{{$contract->order_id}}">
                </div>

                @else
                    <input type="hidden" id="order_title_0" name="contract[0][order_title]"  value="" >
                    <input type="hidden" name="contract[0][order_id]" id="order_id_0" value="0">

                @endif




                @if(!$permission)

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label">Тип договора</label>
                        {{ Form::select('contract[0][type_id]', collect(\App\Models\Contracts\Contracts::TYPE), $contract->type_id, ['class' => 'form-control select2-ws', 'id'=>'type_id_0']) }}
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label">Страховая сумма</label>
                        <input class="form-control sum" id="insurance_amount_0" name="contract[0][insurance_amount]" type="text" value="{{$contract->getInsuranceAmount()}}" >
                    </div>

                @else
                    <input type="hidden" name="contract[0][type_id]"  value="1" >
                    <input type="hidden" name="contract[0][insurance_amount]" value="0">

                @endif



                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">Дата заключения</label>
                    <input class="form-control datepicker valid_fast_accept date" id="sign_date_0" {{--onchange="setAllDates(0)"--}} name="contract[0][sign_date]" type="text" value="{{setDateTimeFormatRu($contract->sign_date, 1)}}">
                </div>

                @if(!$contract->is_online)
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label">Период действия</label>
                        <div class="row form-horizontal">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <input class="form-control datepicker valid_fast_accept date" id="begin_date_0" placeholder="Дата начала" onchange="setEndDates(0)" name="contract[0][begin_date]" type="text" value="{{setDateTimeFormatRu($contract->begin_date, 1)}}">
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <input class="form-control datepicker valid_fast_accept date" placeholder="Дата окончания" id="end_date_0" name="contract[0][end_date]" type="text" value="{{setDateTimeFormatRu($contract->end_date, 1)}}">
                            </div>
                        </div>
                    </div>
                @endif



                @if(!$permission)


                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">Банк</label>
                    {{ Form::select('contract[0][bank_id]', collect(\App\Models\Settings\Bank::all())->pluck('title', 'id')->prepend('Не выбрано', 0), $contract->bank_id, ['class' => 'form-control select2-all', 'id'=>'bank_id_0']) }}
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">Условие продажи</label>
                    {{ Form::select('contract[0][sales_condition]', collect(\App\Models\Contracts\Contracts::SALES_CONDITION), $contract->sales_condition, ['class' => 'form-control select2-ws', 'id'=>'sales_condition_0', "onchange" =>"viewControllerSalesCondition()"]) }}
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sales_condition_not_agent">
                    <label class="control-label pull-left" style="margin-top: 5px;">
                        Менеджер/Агент
                    </label>
                    <label class="control-label pull-right">Личная продажа <input type="checkbox" value="1" id="is_personal_sales_0" name="contract[0][is_personal_sales]" @if($contract->is_personal_sales == 1) checked @endif/> </label>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ></div>
                    {{ Form::select('contract[0][manager_id]', $agents->prepend('Выберите значение', 0), $contract->manager_id, ['class' => 'form-control', 'id'=>'manager_id_0']) }}
                </div>


                @else
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label">Условие продажи - Агентская</label>
                        <input type="hidden" name="contract[0][sales_condition]" value="0"/>
                        <input type="hidden" name="contract[0][is_personal_sales]" value="0"/>
                        <input type="hidden" name="contract[0][manager_id]" value="0"/>
                        <input type="hidden" name="contract[0][bank_id]" value="0"/>
                    </div>

                @endif

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sales_condition_not_agent">
                    <br/><br/>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <label class="control-label">Премия по договору</label>
                    {{ Form::text('contract[0][payment_total]', ($contract->payment_total>0)?titleFloatFormat($contract->payment_total):'', ['class' => 'form-control sum', 'id'=>'payment_contract_total_0']) }}
                </div>



                @if(auth()->user()->hasPermission('contracts', 'select_financial_policy'))
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label class="control-label pull-left" style="margin-top: 5px;">
                            Финансовая политика
                        </label>
                        @if(auth()->user()->hasPermission('contracts', 'set_financial_policy_manually'))
                            <label class="control-label pull-right">Указать в ручную <input type="checkbox" value="1" @if($contract->financial_policy_manually_set==1) checked @endif name="contract[0][financial_policy_manually_set]" id="financial_policy_manually_set_0" onchange="viewSetFinancialPolicyManually(this, '0')"/> </label>
                        @endif
                        <div class="wraper-inline-100" id="financial_policy_id_block">
                            {{ Form::select( 'contract[0][financial_policy_id]', collect([0=>'Укажите номер договора']), 0, ['class' => 'form-control select2-all', 'id'=>'financial_policy_id_0'] ) }}
                        </div>
                    </div>


                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >

                        @if(auth()->user()->hasPermission('contracts', 'set_financial_policy_manually'))
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ></div>
                            <div id="financial_policy_manually_0" style="display: none;">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    <label class="control-label">Бордеро</label>
                                    {{ Form::text('contract[0][financial_policy_kv_bordereau]', $contract->financial_policy_kv_bordereau, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_bordereau_0']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    <label class="control-label">ДВОУ</label>
                                    {{ Form::text('contract[0][financial_policy_kv_dvoy]', $contract->financial_policy_kv_dvoy, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_dvoy_0']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    <label class="control-label">Агента</label>
                                    {{ Form::text('contract[0][financial_policy_kv_agent]', $contract->financial_policy_kv_agent, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_agent_0']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    <label class="control-label">Руков.</label>
                                    {{ Form::text('contract[0][financial_policy_kv_parent]', $contract->financial_policy_kv_parent, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_parent_0']) }}
                                </div>
                            </div>
                        @endif
                    </div>
                @endif


                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <label class="control-label">Алгоритм рассрочки</label>
                    {{ Form::select('contract[0][installment_algorithms_id]', collect([0=>'Укажите номер договора']), 0, ['class' => 'form-control select2-ws', 'id'=>'installment_algorithms_id_0']) }}
                </div>

                <div id='payment_algo' style="display: none;">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                        <label class="control-label">Дата следующего платежа</label>
                        {{ Form::text('contract[0][payment_next_date]', setDateTimeFormatRu($contract->payment_next_date, 1) ?? '', ['class' => 'form-control datepicker date', 'id'=>'payment_next_date_0']) }}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                        <label class="control-label">Сумма следующего платежа</label>
                        {{ Form::text('contract[0][payment_next_sum]', ($contract->payment_next_sum>0)?titleFloatFormat($contract->payment_next_sum):'', ['class' => 'form-control sum', 'id'=>'payment_next_sum_0', 'onchange'=>'setPaymentQty(0)']) }}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
