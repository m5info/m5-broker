@if(isset($hold_kv_product) && (int)$hold_kv_product->is_many_files == 1)

    @include('contracts.partials.many_files_document', ['contract' => $order, 'hold_kv_product'=>$hold_kv_product])

    <br/>
@endif


@include('contracts.partials.files_document', ['contract' => $order, 'hold_kv_product'=>$hold_kv_product])


<script>

    function initDocuments() {

        @if(isset($hold_kv_product) && (int)$hold_kv_product->is_many_files == 1)

        initManyDocuments();

        @endif

    }

    function initManyDocuments() {
        $("#addManyDocForm").dropzone({
            //Dropzone.options.addOrgDocForm = {
            paramName: 'file',
            maxFilesize: 1000,
            acceptedFiles: ".png, .jpg, .jpeg, .txt, .csv, .mp4, .mkv, .avi ,.mov,.avi,.mpeg4,.flv,.3gpp,image/*, .xls, .xlsx, .pdf, .doc, .docx",
            // init: function () {
            //     this.on("queuecomplete", function () {
            //         if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
            //             reload();
            //         }
            //     });
            // },
            init: function () {
                this.on("queuecomplete", function () {
                    location.reload();
                });
            }

        });

    }
    function process(operations = []){
        $.each(operations, function(k,operation){
            if(isFunction(operation)){
                window[operation]()
            }
        })
    }

</script>