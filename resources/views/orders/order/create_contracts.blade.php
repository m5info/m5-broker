@extends('layouts.frame')


@section('title')

    <h2>Создание договоров по заявке №{{ $order->id }}</h2>

@endsection

@section('content')


    {{ Form::open(['url' => url("/contracts/orders/create_contracts/{$order->id}"), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @foreach($available_products as $available_product)
        <div class="form-group">
            <label class="col-sm-4 control-label">{{ $available_product->title }}</label>
            <div class="col-sm-8">
                {{ Form::checkbox('products[]', $available_product->id) }}
            </div>
        </div>
    @endforeach
    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
