@extends('layouts.app')

@section('content')

@section('work-area')
    @include("orders.order.workarea", ['order', $order])
@append

    <div class="page-heading">
        <div class="col-lg-6">
            <h2>Заявка на осмотр №{{ $order->id }} - {{ \App\Models\Orders\InspectionOrders::STATUSES[$order->status_order_id] }}</h2>
        </div>
        <div class="col-lg-6">

            @include('orders.order.partials.statuses_buttons')
        </div>

        @if($order->status_order_id < 2)
            @include("orders.order.{$temp}.distribution")
        @else
            @include("orders.order.{$temp}.confirm")
        @endif
    </div>

@endsection


<style>
    .messages .panel {
        border: #ccc 1px solid;
        -moz-border-radius: 40px;
        -webkit-border-radius: 40px;
        /*border-radius: 0 0 0 0;*/
        border-radius: 40px;
        padding: 8px 15px;
        margin-top: 5px;
        margin-bottom: 5px;
        display: inline-block;
    }
    .messages .panel-body {
        padding: 5px;
        border-color: #fff;

    }
    .messages .panel{
        -webkit-box-shadow: 0 0 0 0;
        box-shadow: 0 0 0 0;
    }
    .messages .panel-default > .panel-heading{
        color: unset;
        background-color: unset;
        border-color: unset;
    }
    .messages .panel-heading{
        border-bottom: unset;
    }
    .messages .panel-success > .panel-heading{
        color: unset;
        background-color: unset;
        border-color: unset;
    }
    .messages .panel-success > .panel-body{
        color: #3c763d;
        background-color: #dff0d8;
        border-color: #dff0d8;
    }
    .messages .panel-success{
        color: #3c763d;
        background-color: #dff0d8;
        border-color: #d6e9c6;
    }
    .messages .panel-default> .panel-heading{
        color: unset;
        background-color: unset;
        border-color: unset;
    }
    .messages .panel-default> .panel-body{
        color: #404040;
        background-color: #f5f5f5;
        border-color: #f5f5f5;
    }
    .messages .panel-default{
        color: #333333;
        background-color: #f5f5f5;
        border-color: #f5f5f5;
    }
    .messages .panel-heading span{
        margin-left: 10px;
    }
    .messages .panel-footer {
        border: 1px solid #fff;
    }
</style>
@include('orders.order.js')