<div class="work-area-group">
    <h3>Общие</h3>
    <div class="work-area-item">
        <div class="row">
            <div class="col-lg-12">
                <div>
                    @if(isset($order->inspection_comments))
                        @foreach($order->inspection_comments as $comment)
                            <div class="col-lg-12" style="padding: 14px 20px;margin-bottom: 18px;background-color: #f5f5f5;border-radius: 2px;">
                                @php
                                    if($author_id = collect($comment->only('proccesing_user_id', 'agent_id'))->max() != 0){
                                         $author = \App\Models\User::find($author_id)->name;
                                    }else{
                                        $author = '';
                                    }
                                @endphp
                                {{ $author }} ({{$comment->created_at}}):<br><br>{!! $comment->comment !!}
                            </div>
                        @endforeach
                    @endif
                </div>
                <label class="control-label">Сообщение</label>
                {{ Form::textarea('order[0][workarea_comment]', '', ['class' => 'form-control']) }}
            </div>
        </div>
    </div>
    <div class="work-area-item">
        <span class="btn btn-success" onclick="saveOrder({{$order->id}});">
            Сохранить заявку
        </span>
    </div>
    <div class="work-area-item">
        <span class="btn btn-danger" onclick="deleteOrder({{$order->id}});">
            Удалить заявку
        </span>
    </div>
</div>


