<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>Название</th>
        <th class="text-center">Количество осмотров</th>
        <th class="text-center">Осталось денег</th>
    </tr>

    </thead>
    <tbody>
    @foreach($orgs as $key_org => $org)
        <tr>
            <td >
                <a href="/orders/acts_of_completion/{{$org->id}}/created_reports">{{$org->title}} </a>(Всего отчетов {{ \App\Models\Orders\InspectionOrdersReports::query()->where('org_id', $org->id)->count() }})
            </td>
            <td class="text-center">{{$org->getInspectionOrdersAcceptNotReport()->count()}}</td>
            <td class="text-center"><a href="/orders/acts_of_completion/{{$org->id}}/reports_create" target="_blank">{{titleFloatFormat($org->getInspectionOrdersAcceptNotReport()->sum('financial_policy_kv_agent_total'))}}</a></td>
        </tr>
    @endforeach
    </tbody>
</table>