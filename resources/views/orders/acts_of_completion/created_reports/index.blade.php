@extends('layouts.app')

@section('content')

    <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container" style="margin-top: -5px;overflow: auto;">
        <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="filter-group" id="filters">

                        <div class="col-lg-12">

                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="like_title">ID</label>
                                {{ Form::text('id', '', ['class'=>'form-control','onchange'=>'loadItems()']) }}
                            </div>

                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="like_title">Название</label>
                                {{ Form::text('like_title', '', ['class'=>'form-control','onchange'=>'loadItems()']) }}
                            </div>

                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="report_month">Месяц</label>
                                {{ Form::select('report_month', collect(getRuMonthes()), -1, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                            </div>

                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="year">Год</label>
                                {{ Form::text('report_year', '', ['class'=>'form-control','onchange'=>'loadItems()']) }}
                            </div>
                            
                            <div class="clearfix"></div>

                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="accept_status">Статус</label>
                                {{ Form::select('accept_status', collect(\App\Models\Orders\InspectionOrdersReports::STATE), -1, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                            </div>

                            <div class="btn-group col-xs-12 col-sm-12 col-sm-offset-6 col-md-3 col-lg-3" style="margin-top: 20px;">
                                <button class="btn btn-danger pull-right" id="drop_filters">Сбросить фильтры</button>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>



    @include('_chunks._vue_table')
    @include('_chunks._pagination',['callback'=>'loadItems'])
    {{--<div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <button onclick="loadItemByButton()" type="submit" class="btn btn-success">Применить</button>
    </div>--}}


@endsection

@section('js')

    <script>

        function event_change(e){
            $('#add_to_ad').hide();
            $('#ad_create').hide();

            if (e.val == 1){
                $('#ad_create').show();
            } else if(e.val == 2) {
                $('#add_to_ad').show();
            }
        }

        $(document).on('click', '#drop_filters', function(){
            console.log(12);
            $('[name="id"]').val('');
            $('[name="like_title"]').val('');
            $('[name="report_month"]').val('');
            $('[name="report_year"]').val('');
            $('[name="accept_status"]').val(-1);
            $('[name="report_month"]').change();

        });

        $(document).on('click', '#execute', function () {
            var event = $('[name="event_id"]').val();
            var name = $('[name="advertising_name"]').val();
            var adverstising_id = $('[name="advertising_id"]').val();

            /*$.post('', {event: event, title: name, supplier_id: '222', adverstising_id: adverstising_id, reports_list: reports_list}, function (res) {

            }).always(function () {
                window.location.reload();
            });*/
        });

        Array.prototype.remove = function(value) {
            var idx = this.indexOf(value);
            if (idx != -1) {
                // Второй параметр - число элементов, которые необходимо удалить
                return this.splice(idx, 1);
            }
            return false;
        }

        $(function () {
            loadItems();
        });

        var reports_list = [];

        $(document).on('change', '.reports_in_list', function () {
            var checked = $(this).prop('checked');
            if (checked){
                reports_list.push($(this).val());
            } else{
                reports_list.remove($(this).val());
            }

            if (reports_list.length > 0){
                $('.advertising_edit').show();
            } else{
                $('.advertising_edit').hide();
            }
        });

        function getData(use_by_button) {

            var arr = [];

            $.each((reports_list), function(k, v){
                arr.push(v);
            });

            return {
                accept_status: $('[name="accept_status"]').val(),
                like_title: $('[name="like_title"]').val(),
                report_year: $('[name="report_year"]').val(),
                report_month: $('[name="report_month"]').val(),
                id: $('[name="id"]').val(),
                use_by_button: use_by_button,
                used_by_button: used_by_button,
                page: PAGE,
            }
        }

        $(document).on('change', '[name="all_in_list"]', function () {
            var checked = $(this).prop('checked');
            $('[name="reports_in_list[]"]').prop('checked', checked);
        });

        function loadItemByButton() {
            var use_by_button = 1;

            loadItems(use_by_button);
        }

        var used_by_button = 0;

        function loadItems(use_by_button = 0) {

            if (used_by_button == 1){
                use_by_button = 1;
            }
            loaderShow();

            /* "/reports/reports_sk/{$bso_supplier->id}/reports/table" */

            $.get('{{url("/orders/acts_of_completion/{$org->id}/created_reports/get_table")}}', getData(use_by_button), function (table_res) {

                $('#table').html(table_res.html);

                if (table_res.count < 1) {
                    table_res.count = 1;
                }

                let maxpage = Math.ceil(table_res.count / table_res.perpage);

                $('#view_row').html(PAGE);
                $('#max_row').html(maxpage);

                ajaxPaginationUpdate(maxpage, loadItems);

                $(".clickable-row").click(function () {
                    if ($(this).attr('data-href')) {
                        window.location = $(this).attr('data-href');
                    }
                });

                if (table_res.used_by_button == 1){
                    used_by_button = 1;
                }

            }).always(function () {
                loaderHide();
            });

        }

        function deleteReport(event, id) {

            if (confirm("Вы уверены что хотите отчет "+id)) {
                loaderShow();
                var url = '{{ url("/reports/order") }}/'+ id +'/delete_report_with_payments';
                $.post(url, '', function (res) {
                    if (parseInt(res.status) === 1) {
                        var tr = $(event).parent().parent();
                        tr.hide();
                    }
                }).always(function () {
                    loaderHide();
                });
            }
        }

    </script>

@endsection