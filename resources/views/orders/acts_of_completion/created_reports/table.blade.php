<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th>Дата создания</th>
            <th>Месяц</th>
            <th>Год</th>
            <th>Статус</th>
            <th>Сумма</th>
        </tr>
    </thead>
    <tbody>

    @if(sizeof($inspection_orders_reports))
        @php
        $total_sum_all = 0;
        @endphp
        @foreach($inspection_orders_reports as $ins_ord_rep)
            <tr style="cursor: pointer" onclick="location.href = '{{url('/orders/acts_of_completion/'.$org_id.'/created_reports/'.$ins_ord_rep->id)}}'">
                <td>{{$ins_ord_rep->id}}</td>
                <td>{{$ins_ord_rep->title}}</td>
                <td>{{$ins_ord_rep->created_at ? getDateFormatRu($ins_ord_rep->created_at) : ''}}</td>
                <td>{{getRuMonthes()[$ins_ord_rep->report_month]}}</td>
                <td>{{$ins_ord_rep->report_year}}</td>
                <td>{{isset(\App\Models\Orders\InspectionOrdersReports::STATE[$ins_ord_rep->accept_status]) ? \App\Models\Orders\InspectionOrdersReports::STATE[$ins_ord_rep->accept_status] : ''}}</td>
                <td>{{getPriceFormat($ins_ord_rep->total_sum)}}</td>
                @php
                    $total_sum_all += $ins_ord_rep->total_sum;
                @endphp
            </tr>
        @endforeach
            <tr>
                <td colspan="6"><span class="pull-right">Итого: </span></td>
                <td>{{getPriceFormat($total_sum_all)}}</td>
            </tr>
    @else
        <tr class="clickable-row">
            <td colspan="100" class="text-center">Нет результатов</td>
        </tr>
    @endif
    </tbody>
</table>