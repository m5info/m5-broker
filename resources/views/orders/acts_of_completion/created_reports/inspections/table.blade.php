<table style="overflow:hidden" class="table table-bordered">

    <thead>
    <tr>
        <th>СК</th>
        <th>Дата создания</th>
        <th>Дата проведения</th>
        <th>Город</th>
        <th>Тип позиции</th>
        <th>Страхователь</th>
        <th>Исполнитель</th>
        <th>Менеджер</th>
        <th>Адрес</th>
        <th>Стоимость</th>
    </tr>
    </thead>
    <tbody>
    @if(sizeof($inspections))
        @php
            $total_sum_all = 0;
        @endphp
        @foreach($inspections as $ins)
            <tr>
                <td>{{$ins->contract->insurance_companies ? $ins->contract->insurance_companies->title : ""}}</td>
                <td>{{setDateTimeFormatRu($ins->order->sign_date, 1)}}</td>
                <td>{{setDateTimeFormatRu($ins->order->begin_date, 1)}}</td>
                <td>{{$ins->city ? $ins->city->title : 'Не указано'}}</td>
                <td>{{isset(\App\Models\Orders\InspectionOrders::POSITION_TYPE[$ins->position_type_id]) ? \App\Models\Orders\InspectionOrders::POSITION_TYPE[$ins->position_type_id] : 'Не указано'}}</td>
                <td>{{ $ins->contract->insurer ? $ins->contract->insurer->title:''}}</td>
                <td>{{$ins->processing_user ? $ins->processing_user->name : ''}}</td>
                <td>{{$ins->accept_user ? $ins->accept_user->name : ''}}</td>
                <td>{{$ins->address ? $ins->address : 'Не указано'}}</td>
                <td>{{titleFloatFormat($ins->financial_policy_kv_agent_total)}}</td>
            </tr>
            @php
                $total_sum_all += $ins->financial_policy_kv_agent_total;
            @endphp
        @endforeach
            <tr>
                <td colspan="9"><span class="pull-right">Итого: </span></td>
                <td>{{getPriceFormat($total_sum_all)}}</td>
            </tr>
    @else
        <tr>
            <td colspan="{{ isset($is_reports) ? '31' : '25' }}" class="text-center">Нет осмотров</td>
        </tr>
    @endif

    </tbody>
</table>

