@extends('layouts.app')

@section('content')

    <div class="block-main" style="margin-top: 5px;">
        <div class="block-sub">
            {{ Form::open(['url' => url("/orders/acts_of_completion/{$org->id}/created_reports/{$inspection_order->id}/"), 'method' => 'post', 'class' => 'form-horizontal']) }}
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        Название ID {{$inspection_order->id}}
                        (Сформирован: {{setDateTimeFormatRu($inspection_order->created_at)}} {{$inspection_order->create_user ? "пользователем {$inspection_order->create_user->name}" : ""}}
                        )
                    </label>
                    <div class="col-sm-12">
                        {{ Form::text('title', $inspection_order->title, ['class' => 'form-control', 'required']) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-6 ">Отчетный период</label>
                    <label class="col-sm-3 ">Договора с</label>
                    <label class="col-sm-3 ">Договора по</label>
                    <div class="col-sm-6">
                        {{ Form::select('report_month', getRuMonthes(), $inspection_order->report_month, ['class' => 'form-control', 'style' => 'width: 48%; display: inline']) }}
                        /
                        {{ Form::select('report_year', getYearsRange('-5', '+1'), $inspection_order->report_year, ['class' => 'form-control', 'style' => 'width: 48%; display: inline']) }}
                    </div>
                    <div class="col-sm-3">
                        {{ Form::text('report_date_start', setDateTimeFormatRu($inspection_order->report_date_start, 1), ['class' => 'form-control datepicker date']) }}

                    </div>
                    <div class="col-sm-3">
                        {{ Form::text('report_date_end', setDateTimeFormatRu($inspection_order->report_date_end, 1), ['class' => 'form-control datepicker date']) }}

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-6 ">Подписант организации</label>
                    <label class="col-sm-6 ">Подписант поставщика</label>

                    <div class="col-sm-6">
                        {{ Form::text('signatory_org', $inspection_order->signatory_org, ['class' => 'form-control']) }}
                    </div>

                    <div class="col-sm-6">
                        {{ Form::text('signatory_sk_bso_supplier', $inspection_order->signatory_sk_bso_supplier, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">

                        <a class="btn btn-success doc_export_btn pull-left" id="obj_export_xls">Скачать отчет</a>


                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-12">Комментарий</label>
                    <div class="col-sm-12">
                        {{ Form::textarea('comment', $inspection_order->comment, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-12 ">Статус
                        @if($inspection_order->accept_status==2)
                            <span class="pull-right">Акцепт: {{setDateTimeFormatRu($inspection_order->accepted_at)}} {{$inspection_order->accept_user ? "пользователем {$inspection_order->accept_user->name}" : ""}} </span>
                        @endif
                    </label>

                    <div class="col-sm-12">
                        {{ Form::select('accept_status', collect(\App\Models\Orders\InspectionOrdersReports::STATE), $inspection_order->accept_status, ['class' => 'form-control']) }}
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="submit" class="btn btn-primary btn-right pull-right" value="Сохранить"/>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

                {{Form::close()}}

                @include("orders.acts_of_completion.partials.documents", ['report' => $inspection_order])

            </div>
        </div>
    </div>

    @include('_chunks._vue_table')
    @include('_chunks._pagination',['callback'=>'loadItems','count_pagination'=>[25=>'25', 50=>'50', 200=>'200', 1000=>'1000', 5000=>'5000'], 'b_class' => 'pull-right', 'statistic' => true])

@endsection

@section('js')

    <script>
        $(function () {

            $(document).on('change', '[name="inspection[]"]', function () {
                var uncheckeds = $('[name="inspection[]"]').length - $('[name="inspection[]"]:checked').length;
                $('[name="all_inspections"]').prop('checked', uncheckeds === 0);
                showActions();
            });

            $(document).on('click', '#obj_export_xls', function () {
                var query = $.param({
                    method: 'Orders\\ActsOfCompletionController@inspections_created_report_get_table',
                    param: getData(),
                    method_param: [
                        '{{$org->id}}',
                        '{{$inspection_order->id}}'
                    ]
                });
                location.href = '/exports/table2excel?' + query;
            });


            $(document).on('click', '#select_all_products', function () {
                $.each($('[name="product_id"] option'), function (k, v) {
                    $(v).prop('selected', true);
                });
                $('[name="product_id"]').change();
            });


            $(document).on('change', '[name="all_inspections"]', function () {
                var checked = $(this).prop('checked');
                $('[name="inspection[]"]').prop('checked', checked);
                showActions();
            });


            $(document).on('click', '#execute', function () {

                loaderShow();

                var event_data = getEventData();
                $.post('/orders/acts_of_completion/{{$org->id}}/reports_create/execute', event_data, function (res) {

                    if (res.errors){
                        $.each(res.errors, function(i, v){
                            flashMessage('danger',v);
                        });
                        return;
                    }

                    flashMessage('success', 'Отчет  <a href="/orders/acts_of_completion/{{$org->id}}/created_reports">'+$('[name="report_name"]').val()+'</a> создан');
                    loadItems();

                    loaderHide();

                })
            });

            loadItems();

        });


        function selectTab(index) {
            var report_id = $($('[data-report_id]')[index]).data('report_id');
            $('[name="report_id"]').val(report_id);
            loadItems();
        }


        function getData(use_by_button) {
            return {
                date_type: $('[name="date_type"]').val(),
                date_from: $('[name="date_from"]').val(),
                date_to: $('[name="date_to"]').val(),
                page_count: $('[name="page_count"]').val(),
                use_by_button: use_by_button,
                used_by_button: used_by_button,
                page: PAGE,
            }
        }


        function getEventData() {
            var event_data = {
                inspection_ids: [],
                report_name: $('[name="report_name"]').val(),
                report_year: $('[name="report_year"]').val(),
                report_month: $('[name="report_month"]').val(),
                report_date_start: $('[name="report_date_start"]').val(),
                report_date_end: $('[name="report_date_end"]').val(),
                to_report_id: $('[name="to_report_id"]').val(),
                event_id: $('[name="event_id"]').val(),
                org_id: '{{$org->id}}'

            };
            $.each($('[name="inspection[]"]:checked'), function (k, v) {
                event_data.inspection_ids.push($(v).val());
            });
            if ($('[name="and_dvou_report"]').prop('checked')) {
                event_data.and_dvou_report = 1;
            }
            return event_data;
        }

        var used_by_button = 0;

        function loadItems(use_by_button = 0) {

            loaderShow();

            if (used_by_button == 1){
                use_by_button = 1;
            }

            $.get('/orders/acts_of_completion/{{$org->id}}/created_reports/{{$inspection_id}}/get_table', getData(use_by_button), function (table_res) {
                $('#table').html(table_res['html']);
                /*
                               for(let i = 0; i <50 ; i++)
                                    $('#table').append(table_res);*/
                if (table_res.count < 1) {
                    table_res.count = 1;
                }

                let maxpage = Math.ceil(table_res.count / table_res.perpage);

                $('#view_row').html(PAGE);
                $('#max_row').html(maxpage);

                $('.wrapper1').on('scroll', function (e) {
                    $('.wrapper2').scrollLeft($('.wrapper1').scrollLeft());
                });
                $('.wrapper2').on('scroll', function (e) {
                    $('.wrapper1').scrollLeft($('.wrapper2').scrollLeft());
                });

                $('.div1').width($('.bso_items_table').width());
                $('.div2').width($('.bso_items_table').width());

                ajaxPaginationUpdate(maxpage, loadItems);

                if (vue_table)
                    vue_table.resetHead();
                showActions();

            }).always(function () {
                loaderHide();
            });

        }


        function showActions() {
            if ($('[name="inspection[]"]:checked').length > 0) {

                $.get('/orders/acts_of_completion/{{$org->id}}/reports_create/get_actions', getData(), function (actions_res) {
                    $('#action_table').html(actions_res)
                });

            } else {
                $('#action_table').html('')
            }

        }


    </script>

    <style>
        .wrapper1, .wrapper2 {
            width: 100%;
            overflow-x: scroll;
            overflow-y: hidden;
        }

        .wrapper1 {
            height: 20px;
        }

        .wrapper2 {
        }

        .div1 {
            height: 20px;
        }

        .div2 {
            overflow: none;
        }

    </style>

@endsection