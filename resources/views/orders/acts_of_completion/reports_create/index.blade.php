@extends('layouts.app')

@section('content')



    <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container"
         style="margin-top: -5px;overflow: auto;">
        <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="filter-group" id="filters">
                        <div class="col-sm-12 col-md-4 col-lg-4">
                            <h2>Период</h2>
                            <div class="col-sm-12 col-ls-12" style="margin-top: 25px;">
                                {{ Form::select('date_type', collect([1 => 'Дата создания', 2 => 'Дата проведения']), 1, ['class'=>'form-control select2-all','onchange'=>'loadItems()']) }}
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                <label class="control-label">С</label>
                                {{ Form::text('date_from', ''/*date('d.m.Y', strtotime('-1 months'))*/, ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off', 'onchange' => 'loadItems()']) }}
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                <label class="control-label">По</label>
                                {{ Form::text('date_to', ''/*date('d.m.Y')*/, ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off', 'onchange' => 'loadItems()']) }}
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-8 col-lg-8">
                            <div class="col-sm-12 col-lg-12">
                                <h2>Детали</h2>
                            </div>

                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="product_id">
                                    Продукт
                                    <sup><a href="javascript:void(0);" class="btn-xs btn-success"
                                            id="select_all_products">выбрать все</a></sup>
                                </label>
                                {{ Form::select('products', \App\Models\Directories\Products::query()->where('for_inspections', '1')->pluck('title', 'id'), -1, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                            </div>

                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="product_id">
                                    Исполнитель\Менеджер
                                </label>
                                {{ Form::select('executor_manager', \App\Models\User::getALLUserWhere()->pluck('name', 'id')->prepend('Не выбрано', -1), -1, ['class'=>'form-control select2','onchange'=>'loadItems()']) }}
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div id="action_table"></div>
            </div>
        </div>


    </div>
    
    <div style="display: block;">
        @include('_chunks._pagination',['callback'=>'loadItems','count_pagination'=>[25=>'25', 50=>'50', 200=>'200', 1000=>'1000', 5000=>'5000'], 'b_class' => 'pull-right', 'statistic' => true])
    </div>

    @include('_chunks._vue_table')

@endsection

@section('js')

    <script>
        $(function () {

            $(document).on('change', '[name="inspection[]"]', function () {
                var uncheckeds = $('[name="inspection[]"]').length - $('[name="inspection[]"]:checked').length;
                $('[name="all_inspections"]').prop('checked', uncheckeds === 0);
                showActions();
            });


            $(document).on('click', '#select_all_products', function () {
                $.each($('[name="product_id"] option'), function (k, v) {
                    $(v).prop('selected', true);
                });
                $('[name="product_id"]').change();
            });


            $(document).on('change', '[name="all_inspections"]', function () {
                var checked = $(this).prop('checked');
                $('[name="inspection[]"]').prop('checked', checked);
                showActions();
            });


            $(document).on('click', '#execute', function () {

                loaderShow();

                var event_data = getEventData();
                $.post('/orders/acts_of_completion/{{$org->id}}/reports_create/execute', event_data, function (res) {

                    if (res.errors){
                        $.each(res.errors, function(i, v){
                           flashMessage('danger',v);
                        });
                        return;
                    }

                    flashMessage('success', 'Отчет  <a href="/orders/acts_of_completion/{{$org->id}}/created_reports/'+res.id+'">'+$('[name="report_name"]').val()+'</a> создан');
                    loadItems();

                    loaderHide();

                })
            });

            loadItems();

        });


        function selectTab(index) {
            var report_id = $($('[data-report_id]')[index]).data('report_id');
            $('[name="report_id"]').val(report_id);
            loadItems();
        }


        function getData(use_by_button) {
            return {
                executor_manager: $('[name="executor_manager"]').val(),
                date_type: $('[name="date_type"]').val(),
                date_from: $('[name="date_from"]').val(),
                date_to: $('[name="date_to"]').val(),
                page_count: $('[name="page_count"]').val(),
                products: $('[name="products"]').val(),
                use_by_button: use_by_button,
                used_by_button: used_by_button,
                page: PAGE,
            }
        }


        function getEventData() {
            var event_data = {
                inspection_ids: [],
                report_name: $('[name="report_name"]').val(),
                report_year: $('[name="report_year"]').val(),
                report_month: $('[name="report_month"]').val(),
                report_date_start: $('[name="report_date_start"]').val(),
                report_date_end: $('[name="report_date_end"]').val(),
                to_report_id: $('[name="to_report_id"]').val(),
                event_id: $('[name="event_id"]').val(),
                org_id: '{{$org->id}}'

            };
            $.each($('[name="inspection[]"]:checked'), function (k, v) {
                event_data.inspection_ids.push($(v).val());
            });
            if ($('[name="and_dvou_report"]').prop('checked')) {
                event_data.and_dvou_report = 1;
            }
            return event_data;
        }

        var used_by_button = 0;

        function loadItems(use_by_button = 0) {

            loaderShow();

            if (used_by_button == 1){
                use_by_button = 1;
            }

            $.get('/orders/acts_of_completion/{{$org->id}}/reports_create/get_table', getData(use_by_button), function (table_res) {
                $('#table').html(table_res['table']);
                /*
                               for(let i = 0; i <50 ; i++)
                                    $('#table').append(table_res);*/
                if (table_res.count < 1) {
                    table_res.count = 1;
                }

                let maxpage = Math.ceil(table_res.count / table_res.perpage);

                $('#view_row').html(PAGE);
                $('#max_row').html(maxpage);

                $('.wrapper1').on('scroll', function (e) {
                    $('.wrapper2').scrollLeft($('.wrapper1').scrollLeft());
                });
                $('.wrapper2').on('scroll', function (e) {
                    $('.wrapper1').scrollLeft($('.wrapper2').scrollLeft());
                });

                $('.div1').width($('.bso_items_table').width());
                $('.div2').width($('.bso_items_table').width());

                ajaxPaginationUpdate(maxpage, loadItems);

                if (vue_table)
                    vue_table.resetHead();
                showActions();

            }).always(function () {
                loaderHide();
            });

        }


        function showActions() {
            if ($('[name="inspection[]"]:checked').length > 0) {

                $.get('/orders/acts_of_completion/{{$org->id}}/reports_create/get_actions', getData(), function (actions_res) {
                    $('#action_table').html(actions_res)
                });

            } else {
                $('#action_table').html('')
            }

        }


    </script>

    <style>
        .wrapper1, .wrapper2 {
            width: 100%;
            overflow-x: scroll;
            overflow-y: hidden;
        }

        .wrapper1 {
            height: 20px;
        }

        .wrapper2 {
        }

        .div1 {
            height: 20px;
        }

        .div2 {
            overflow: none;
        }

    </style>

@endsection