<div id="action_table"><table class="table table-bordered">
        <thead>
        <tr>
            <th>Доступные действия</th>
            <th width="20%">&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <select class="form-control select2-all" name="event_id">
                    <option value="1">Создать отчет</option>
                </select>
            </td>
            <td class="text-center">
                <a class="btn btn-primary" id="execute">Выполнить</a>
            </td>
        </tr>
        </tbody>
    </table>

    <div data-additional="1" style="display:block">
        <div class="block-main">
            <div class="block-sub">
                <div class="filter-group">
                    <div class="row">
                        <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label" for="act_name">Название</label>
                            <input class="form-control" autocomplete="off" name="report_name" type="text" value="">
                        </div>
                    </div>

                    <div class="row">
                        <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label" for="user_id_from">Отчётный период</label>
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    {{Form::select('report_year', getYearsRange(-5, +1), date("Y"), ['class' => 'form-control', 'onclick' => 'filterLastReports()'])}}
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    {{Form::select('report_month', getRuMonthes(), 1, ['class' => 'form-control'])}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label" for="report_date_start">Дата заключения договора с</label>
                            <input class="form-control datepicker date" autocomplete="off" name="report_date_start" type="text" value="">
                        </div>
                    </div>

                    <div class="row">
                        <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label" for="report_date_end">Дата заключения договора по</label>
                            <input class="form-control datepicker date" autocomplete="off" name="report_date_end" type="text" value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div data-additional="2" style="display: none;">
        Добавить в акт
        <div class="block-main">
            <div class="block-sub">
                <div class="filter-group">
                    <div class="row">
                        <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label" for="act_name">№ Отчета</label>
                            <select class="form-control" name="to_report_id"><option value="27">Пример  (Создан автоматически)</option><option value="23">190 (Создан автоматически)</option><option value="20">Как хотим (Создан автоматически)</option><option value="12">тест 21.06.2019 (Создан автоматически)</option><option value="8">ihdjksfjsdfl</option><option value="7">ihdjksfjsdfl</option><option value="4">олропа (Создан автоматически)</option></select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>

        $(function () {
            $(document).on('change', '[name="event_id"]', function () {
                var val = $(this).val();
                $('[data-additional]').hide();
                $('[data-additional="' + val + '"]').show();
            });

            filterLastReports();
        });

        function filterLastReports() {

            var data = {
                'year': $('[name="report_year"]').val(),
                'month': $('[name="report_month"]').val()
            };

        }
    </script>
</div>