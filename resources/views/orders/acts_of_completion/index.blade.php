@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">
@append

@section('content')

    @php
        $select_params = ['class'=>'form-control select2-all select2-ws','onchange'=>'loadItems()']
    @endphp

    <div class="page-heading">
        <h2>Акты выполненных работ</h2>
    </div>
    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="filter-group">

            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <label class="control-label" for="user_id_from">Организация</label>
                {{ Form::select('organization_id', $orgs->pluck('title','id')->prepend('Не выбрано', -1), request()->has('organization_id') ? request()->get('organization_id') : -1, $select_params) }}
            </div>

        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div id="table">

            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>

        function getData(){
            return {
                organization_id: $('[name = "organization_id"]').val()
            }
        }

        function loadItems(){
            $.get('/orders/acts_of_completion/get_table', getData(), function(res){
                $('#table').html(res);
            })
        }

        $(document).ready(function(){
           loadItems();
        });
    </script>
@endsection