@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">
@append

@section('content')


    <div class="page-heading">
        <h2>Заявки на осмотр
            @if($tabs_visibility[0]['edit'] == 1)
                <div class="pull-right">
                    <a href="{{url("/orders/create")}}">
                        <span class="btn btn-primary btn-right">
                            <i class="fa fa-plus"></i>Новый осмотр
                        </span>
                    </a>
                </div>
            @endif
        </h2>
    </div>

    <div class="header_bab">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @if(sizeof($count_arr))
                <div id="tt" class="easyui-tabs" data-options="tools:'#tab-tools'">
                    @foreach($count_arr as $key => $count)
                        <div title="{{$count['title']}} {{($count['count']>0)?$count['count']:''}}" id="tab-{{$key}}" data-view="{{$key}}"></div>
                    @endforeach
                </div>
            @else
                У вас нет доступных вкладок в этом разделе
            @endif
        </div>
    </div>
    <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container" style="margin-top: -5px;overflow: auto;"></div>

@endsection

@section('js')

    <script>

        $(document).on('click', ".clickable-row", function () {
            if ($(this).attr('data-href')) {
                window.location = $(this).attr('data-href');
            }
        });

        var map;
        var TAB_INDEX = 0;

        $(function () {

            if($('[data-view]').length > 0){
                $('#tt').tabs({
                    border:false,
                    pill: false,
                    plain: true,
                    onSelect: function(title, index){
                        return selectTab(index);
                    }
                });
                selectTab(0);
            }
        });


        function selectTab(id) {

            TAB_INDEX = id;

            loaderShow();

            $.get("{{url("/orders/get_tab")}}", getData(), function (response) {

                $("#main_container").html(response);
                loadData();
                initTab();

            }).always(function() {
                loaderHide();
            });

        }

        function loadData(){
            $.get("{{url("/orders/get_table")}}", getData(), function(table_response){
                $('[name="agent"]').addClass('select2-ws');
                $('[name="product"]').addClass('select2-ws');
                $('[name="contract_type"]').addClass('select2-ws');
                $('#table').html(table_response);
                $('.select2-ws').select2("destroy").select2({
                    width: '100%',
                    dropdownCssClass: "bigdrop",
                    dropdownAutoWidth: true,
                    minimumResultsForSearch: -1
                });


            });
        }


        function getData(){

            var tab = $('#tt').tabs('getSelected');
            var load = tab.data('view');//$("#tab-"+id).data('view');

            return {
                status: load,
                sk: $('[name="sk"]').val(),
                bso_title: $('[name="bso_title"]').val(),
                product: $('[name="product"]').val(),
                insurer: $('[name="insurer"]').val(),
                agent: $('[name="agent"]').val(),
                conclusion_date_from: $('[name="conclusion_date_from"]').val(),
                conclusion_date_to: $('[name="conclusion_date_to"]').val(),
            }

        }

        function initTab() {
            startMainFunctions();

        }


    </script>


@endsection