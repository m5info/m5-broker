<table class="tov-table table  table-bordered">
    <thead>
    <tr>
        <th><input type="checkbox" id="events_orders_all" onclick="selectAllRow()"/> </th>
        <th>#</th>
        <th>Страхователь</th>
        <th>Курьер</th>
        <th>Доставка</th>
        <th>Город</th>
        <th>Метро</th>
        <th>Адрес</th>
        <th>Цена</th>
        <th>Действие</th>
    </tr>
    </thead>
    <tbody>
    @if(sizeof($orders))
        @foreach($orders as $order)
            <tr {{$order->urgency ? 'class=bg-red' : ''}}>

                <td><input type="checkbox" class="events_orders" value="{{$order->id}}" onclick="selectRow()"/> </td>

                <td>{{$order->id}}</td>

                <td>{{$order->main_contract_id > 0 ? $order->main_contract->insurer->title : ""}}</td>
                <td>{{ ($order->courier)? $order->courier->name : '' }}</td>
                <td>{{setDateTimeFormatRu($order->delivery_date, 1)}} {{$order->delivery_time}}</td>

                <td>{{ ($order->delivery_city)?$order->delivery_city->title : '' }}</td>
                <td>{{ $order->delivery_metro }}</td>
                <td>{{ $order->address }}</td>
                <td>{{ titleFloatFormat($order->departures_courier_price_total) }}</td>

                <td>

                    <a class="btn btn-success btn-left" href="{{url("/orders/processing/order/{$order->id}")}}">
                        <i class="fa fa-eye"></i>
                    </a>

                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="13" class="text-center">Нет записей</td>
        </tr>
    @endif
    </tbody>
</table>



<script>
    
    function initFormTab() {

    }


    function selectAllRow()
    {


        if($("#events_orders_all").is(':checked')){
            $(".events_orders").prop("checked", true);
            activeEventsFormOrders(1);
        }else{
            $(".events_orders").prop("checked", false);
            activeEventsFormOrders(0);
        }

        return true;
    }

    function selectRow()
    {
        EventsState = 0;

        $(".events_orders").each(function( index ) {
            if($(this).is(':checked')){
                EventsState = 1;
            }
        });

        activeEventsFormOrders(EventsState);

        return true;
    }








    
</script>