
<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -5px;">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="filter-group">

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="bso_title">БСО</label>
                    {{ Form::text('bso_title', request('bso_title', ''), ['class' => 'form-control',  'onkeyup'=>'loadData()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="insurer">Страхователь</label>
                    {{ Form::text('insurer', request('insurer', ''), ['class' => 'form-control',  'onkeyup'=>'loadData()']) }}
                </div>


                <div class="btn-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="control-label">Агент/Курьер</label>
                    @include('partials.elements.users', ['title'=>'agent_id', 'select'=> request('agent', 0), 'array' => ['onchange' => 'loadData()']])
                </div>

                <div class="btn-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="control-label">Подразделение</label>
                    {{ Form::select('department_ids[]', \App\Models\Settings\Department::all()->pluck('title', 'id'), request('department_ids', ''), ['class' => 'form-control select2-all', 'id'=>'department_ids', 'multiple' => true, 'onchange' => 'loadData()']) }}
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="conclusion_date_from">Город</label>
                    @include('orders.order.partials.cities_select', ['inspection' => null])
                </div>



                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="conclusion_date_from">Дата доставки с</label>
                    {{ Form::text('conclusion_date_from', request('conclusion_date_from', ''), ['class' => 'form-control datepicker date',  'onchange'=>'loadData()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="conclusion_date_to">Дата доставки по</label>
                    {{ Form::text('conclusion_date_to', request('conclusion_date_to', ''), ['class' => 'form-control datepicker date',  'onchange'=>'loadData()']) }}
                </div>

                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="conclusion_date_to">Кол-во на стр</label>
                    {{Form::select('page_count', collect([25=>'25', 50=>'50', 100=>'100', 150=>'150']), request()->has('page')?request()->session()->get('page'):25, ['class' => 'form-control select2-ws', 'id'=>'page_count', 'onchange'=>'loadData()'])}}
                    <br/>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                        <span id="view_row">0</span>/<span id="max_row">0</span>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -5px;" id="eventsFormOrders">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            {{Form::select('act_id', collect(\App\Models\Orders\DeparturesCourierActs::getActs()->whereIn('status_id', [0, 1])->get()->pluck('title','id'))->prepend('Создать акт', 0), 0, ['class' => 'form-control select2-ws', 'id'=>'act_id', 'onchange'=>'activeViewActForms()'])}}
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id="act_name">
            {{Form::text('act_name', '', ['class' => 'form-control ', 'placeholder'=>'Название акта'])}}
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <span class="btn btn-primary btn-left doc_export_btn" onclick="setOrdersAct()">Выполнить</span>
        </div>
    </div>
</div>

<div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_table_container" style="margin-top: -5px;overflow: auto;"></div>


<div class="row">
    <div id="page_list" class="easyui-pagination pull-right"></div>
</div>


<script>


    var PAGE = 1;

    function startMainFunctions()
    {
        initSelect2();

        activeEventsFormOrders(0);
        loadData();

    }

    function setPage(field) {
        PAGE = field;
        loadData();
    }

    function get_executors() {
        loadData();
    }

    function loadData() {
        loaderShow();

        $.post("{{url("/orders/departures_courier_acts/get_table")}}", getData(), function (response) {

            $("#main_table_container").html(response.result);

            $('#view_row').html(response.view_row);
            $('#max_row').html(response.max_row);

            $('#page_list').pagination({
                total: response.page_max,
                pageSize: 1,
                pageNumber: PAGE,
                layout: ['first', 'prev', 'links', 'next', 'last'],
                onSelectPage: function (pageNumber, pageSize) {
                    setPage(pageNumber);
                }
            });

            initFormTab();

        }).always(function() {
            loaderHide();
        });

    }

    function getData(){

        var tab = $('#tt').tabs('getSelected');
        var load = tab.data('view');//$("#tab-"+id).data('view');

        return {
            status: 5,
            sk: $('[name="sk"]').val(),
            bso_title: $('[name="bso_title"]').val(),
            product: $('[name="product"]').val(),
            insurer: $('[name="insurer"]').val(),
            department_ids: $('[name="department_ids[]"]').val(),
            agent: $('[name="agent_id"]').val(),
            conclusion_date_from: $('[name="conclusion_date_from"]').val(),
            conclusion_date_to: $('[name="conclusion_date_to"]').val(),
            city: $('#cities').val(),
            page_count: $('#page_count').val(),
            page:PAGE,
        }

    }


    function activeEventsFormOrders(state)
    {
        if(state == 0){
            $("#eventsFormOrders").hide();
        }else{
            $("#eventsFormOrders").show();
        }

        activeViewActForms();

    }

    function activeViewActForms()
    {
        if($("#act_id").val() == 0){
            $("#act_name").show();
        }else{
            $("#act_name").hide();
        }
    }
    
    function setOrdersAct()
    {
        orders = [];
        $(".events_orders").each(function( index ) {
            if($(this).is(':checked')){
                orders.push($(this).val());
            }
        });

        data = {
            act_id: $("#act_id").val(),
            act_name: $('[name="act_name"]').val(),
            orders:orders
        }

        loaderShow();

        $.post("{{url("/orders/departures_courier_acts/set_acts")}}", data, function (response) {

            $("#act_id").val(0);
            $('[name="act_name"]').val('');

            reloadTab();

        }).always(function() {
            loaderHide();
        });

    }


</script>
