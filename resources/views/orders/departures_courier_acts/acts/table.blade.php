<table class="tov-table table  table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>Название</th>
        <th>Статус</th>
        <th>Создал</th>
        <th>Дата/Время</th>
        <th>Сумма</th>
        <th>Действие</th>
    </tr>
    </thead>
    <tbody>
    @if(sizeof($acts))
        @foreach($acts as $act)
            <tr>

                <td>{{$act->id}}</td>
                <td>{{$act->title}}</td>
                <td>{{\App\Models\Orders\DeparturesCourierActs::STATUS[$act->status_id]}}</td>
                <td>{{$act->user->name}}</td>

                <td>{{setDateTimeFormatRu($act->created_at)}}</td>
                <td>{{titleFloatFormat($act->price_total)}}</td>
                <td>
                    <a class="btn btn-success btn-left" href="{{url("/orders/departures_courier_acts/acts/{$act->id}")}}">
                        <i class="fa fa-eye"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="13" class="text-center">Нет записей</td>
        </tr>
    @endif
    </tbody>
</table>



<script>
    
    function initFormTab() {

    }


    
</script>