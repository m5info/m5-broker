

<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -5px;">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="filter-group">




                <div class="btn-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="control-label">Пользователь</label>
                    @include('partials.elements.users', ['title'=>'user_id', 'select'=> request('user_id', 0), 'array' => ['onchange' => 'loadData()']])
                </div>

                <div class="btn-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="control-label">Статус</label>
                    {{ Form::select('status_id[]', collect(\App\Models\Orders\DeparturesCourierActs::STATUS), request('status_id', ''), ['class' => 'form-control select2-all', 'id'=>'status_id', 'multiple' => true, 'onchange' => 'loadData()']) }}
                </div>




                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="conclusion_date_to">Кол-во на стр</label>
                    {{Form::select('page_count', collect([25=>'25', 50=>'50', 100=>'100', 150=>'150']), request()->has('page')?request()->session()->get('page'):25, ['class' => 'form-control select2-ws', 'id'=>'page_count', 'onchange'=>'loadData()'])}}
                    <br/>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                        <span id="view_row">0</span>/<span id="max_row">0</span>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>


<div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_table_container" style="margin-top: -5px;overflow: auto;"></div>


<div class="row">
    <div id="page_list" class="easyui-pagination pull-right"></div>
</div>


<script>

    var PAGE = 1;

    function startMainFunctions()
    {
        initSelect2();

        loadData();

    }

    function setPage(field) {
        PAGE = field;
        loadData();
    }

    function loadData() {
        loaderShow();

        $.post("{{url("/orders/departures_courier_acts/get_acts")}}",
            {
                status: $('[name="status_id[]"]').val(),
                user: $('[name="user_id"]').val(),
                page_count: $('#page_count').val(),
                page:PAGE
            }, function (response) {

            $("#main_table_container").html(response.result);

            $('#view_row').html(response.view_row);
            $('#max_row').html(response.max_row);

            $('#page_list').pagination({
                total: response.page_max,
                pageSize: 1,
                pageNumber: PAGE,
                layout: ['first', 'prev', 'links', 'next', 'last'],
                onSelectPage: function (pageNumber, pageSize) {
                    setPage(pageNumber);
                }
            });

            initFormTab();

        }).always(function() {
            loaderHide();
        });

    }


</script>
