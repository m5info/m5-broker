@extends('layouts.app')


@section('content')



    <div class="block-main" style="margin-top: 5px;">
        <div class="block-sub">
            {{ Form::open(['url' => url("/orders/departures_courier_acts/acts/{$act->id}/"), 'method' => 'post', 'class' => 'form-horizontal']) }}

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

                <div class="form-group">
                    <label class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        Название ID {{$act->id}}
                        (Сформирован: {{setDateTimeFormatRu($act->created_at)}} {{$act->create_user ? "пользователем {$act->user->name}" : ""}}
                        )
                    </label>
                    <div class="col-sm-12">
                        {{ Form::text('title', $act->title, ['class' => 'form-control', 'required']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-12">Комментарий</label>
                    <div class="col-sm-12">
                        {{ Form::textarea('comments', $act->comments, ['class' => 'form-control']) }}
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-12 ">Статус</label>
                    <div class="col-sm-12">
                        {{ Form::select('status_id', collect(\App\Models\Orders\DeparturesCourierActs::STATUS), $act->status_id, ['class' => 'form-control']) }}
                    </div>

                </div>

                <div class="form-group">
                    <div class="col-sm-6">
                        @if($act->status_id != 2)
                            <span class="btn btn-danger btn-left" onclick="deleteAct()">Удалить акт</span>
                        @endif
                    </div>
                    <div class="col-sm-6">


                        <input type="submit" class="btn btn-primary btn-right" value="Сохранить"/>

                    </div>
                </div>


            </div>

            {{Form::close()}}


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Курьер</th>
                            <th>Кол-во</th>
                            <th>Сумма</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($act_users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->count_orders}}</td>
                                <td>{{titleFloatFormat($user->price_orders)}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>Итого</td>
                            <td>{{($act->orders->count('id'))}}</td>
                            <td>{{titleFloatFormat($act->orders->sum('departures_courier_price_total'))}}</td>
                        </tr>
                    </tbody>
                </table>

            </div>




        </div>
    </div>



    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -5px;" id="eventsFormOrders">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if($act->status_id != 2)
                    <span class="btn btn-danger btn-left" onclick="deleteOrdersAct()">Удалить выбранные</span>
                @endif
            </div>
        </div>
    </div>

    <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_table_container" style="margin-top: -5px;overflow: auto;">
        @include('orders.departures_courier_acts.orders.table', ['orders'=>$act->orders])

    </div>



@endsection

@section('js')

    <script>


        $(function () {

            activeEventsFormOrders(0);

        });


        function activeEventsFormOrders(state)
        {
            if(state == 0){
                $("#eventsFormOrders").hide();
            }else{
                $("#eventsFormOrders").show();
            }
        }


        function deleteOrdersAct() {


            orders = [];
            $(".events_orders").each(function( index ) {
                if($(this).is(':checked')){
                    orders.push($(this).val());
                }
            });

            loaderShow();

            $.post("{{url("/orders/departures_courier_acts/acts/{$act->id}/delete_orders_act")}}", {orders:orders}, function (response) {

                reload();

            }).always(function() {
                loaderHide();
            });

        }

        function deleteAct() {


            loaderShow();

            $.post("{{url("/orders/departures_courier_acts/acts/{$act->id}/delete_act")}}", {}, function (response) {

                window.location = '/orders/departures_courier_acts/';

            }).always(function() {
                loaderHide();
            });

        }


    </script>


@endsection