@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">{{ trans('menu.organizations') }}</h1>
        @if(auth()->user()->hasPermission('directories', 'organizations_edit'))
            <a class="btn btn-primary btn-right" href="{{ url("$control_url/organizations/create")  }}">
                {{ trans('form.buttons.create') }}
            </a>
        @endif
    </div>

    @if(sizeof($organizations))
        <table class="tov-table">
            <thead>
                <tr>
                    <th><a href="javascript:void(0);">{{ trans('organizations/organizations.title') }}</a></th>
                    <th><a href="javascript:void(0);">Тип</a></th>
                    <th><a href="javascript:void(0);">Руководитель организации</a></th>
                    <th><a href="javascript:void(0);">Контактное лицо</a></th>
                    <th><a href="javascript:void(0);">{{ trans('organizations/organizations.phone') }}</a></th>
                </tr>
            </thead>
            @foreach($organizations as $organization)
                <tr class="clickable-row" data-href="{{url ("$control_url/organizations/$organization->id/edit")}}">
                    <td>{{ $organization->title }}</td>
                    <td>{{ $organization->org_type ? $organization->org_type->title : "" }}</td>
                    <td>{{ (isset($organization->parent_user))?$organization->parent_user->name: '' }}</td>
                    <td>{{ $organization->user_contact_title }}</td>
                    <td>{{ $organization->phone }}</td>
                </tr>
            @endforeach
        </table>
    @else
        {{ trans('form.empty') }}
    @endif





@endsection



