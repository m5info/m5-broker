@extends('layouts.frame')


@section('title')

    Агентский договор для организации {{ $org['title'] }}

@endsection

@section('content')

@php($id = $org['id'])
@php($organization = \App\Models\Organizations\Organization::where('id', $id)->get()->first())
    {{ Form::open(['url' => url("/directories/organizations/organizations/$id/add_document"), 'method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}


    <div class="form-horizontal">

        <div class="form-group">
            <label class="col-sm-4 control-label">Загрузите агентский договор</label>
            <div class="col-sm-9">
                {{ Form::file('agent_contract',  ['class' => 'form-control', 'required']) }}
            </div>

        </div>
        @if(\App\Models\Organizations\AgentContractDocument::where('org_id',$org['id'])->first())
            Последний загруженный: {{ $organization->agent_contracts ? $organization->agent_contracts[0]['original_name'] : '' }}
        @endif
    </div>

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection