@extends('layouts.frame')

@section('title')

    Точка
@endsection

@section('content')



    {{ Form::open(['url' => url("/directories/organizations/$organization->id/point_department/".(int)$point_department->id), 'method' => 'post', 'class' => 'form-horizontal']) }}

    <div class="form-group">
        <label class="col-sm-4 control-label">Название</label>
        <div class="col-sm-8">

            {{ Form::text("title", $point_department->title, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Адрес</label>
        <div class="col-sm-8">
            {{ Form::text('address', $point_department->address, ['class' => 'form-control address-autocomplete', 'data-name' => 'organizations_address', 'data-address-type' => 'organizations', 'id' => 'address']) }}

            <input type="hidden" name="geo_lat" data-name="organizations_latitude" value="{{ $point_department->geo_lat }}">
            <input type="hidden" name="geo_lon" data-name="organizations_longitude" value="{{ $point_department->geo_lon }}">
        </div>
    </div>

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="deletePoint()" type="submit" class="btn btn-danger pull-left">{{ trans('form.buttons.delete') }}</button>
    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>
    <script>
        function deletePoint() {
            newCustomConfirm(function (result) {
                if (result){
                    $.post('{{url("/directories/organizations/{$organization->id}/point_department/".((int)$point_department->id)."/")}}', {
                        _method: 'DELETE'
                    }, function () {
                        parent_reload();
                    });
                }
            });
        }
    </script>
@endsection