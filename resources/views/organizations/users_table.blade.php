<table class="tov-table">
    <tbody>
        <tr class="sort-row">
            <th>{{ trans('users/users.index.name') }}</th>
            <th>Подразделение</th>
            <th>Статус</th>
            @if(auth()->user()->hasPermission('directories', 'organizations_user'))
                <th>
                    <span onclick="openFancyBoxFrame('/users/frame/?user_id=0&org_id={{$organization->id}}')" class="btn btn-primary pull-right">
                        {{ trans('form.buttons.add') }}
                    </span>
                </th>
            @endif
        </tr>
        @if(sizeof($users))
            @foreach($users as $user)
                <tr class="clickable-row">
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->department ? $user->department->title : '' }}</td>
                    <td>{{ \App\Models\User::STATUS_USER[$user->status_user_id] }}</td>
                    @if(auth()->user()->hasPermission('directories', 'organizations_user'))
                        <td>
                            <span onclick="openFancyBoxFrame('/users/frame/?user_id={{$user->id}}&org_id={{$organization->id}}')" class="btn btn-primary pull-right">Редактировать</span>
                        </td>
                    @endif
                </tr>
            @endforeach

        @else
            <tr>
                <td colspan="4" class="text-center">Не создано ни одного пользователя</td>
            </tr>
        @endif
    </tbody>
</table>