<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="block-main">
        <div class="block-sub">

            @if(auth()->user()->hasPermission('directories', 'organizations_edit'))
                {{ Form::model($organization, ['url' => url($send_urls), 'method' => 'put',  'class' => 'form-horizontal']) }}
            @endif

                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Тип НДС</label>
                        <div class="col-sm-8">
                            {{ Form::select('nds_type_id', collect(\App\Models\Organizations\Organization::NDS_TYPE)->prepend('Не выбрано', 0),  $organization->nds_type_id,  ['class' => 'form-control select2-all']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Тип налогообложения</label>
                        <div class="col-sm-8">
                            {{ Form::select('taxation_system_type_id', collect(\App\Models\Organizations\Organization::TAXATION_SYSTEM_TYPE)->prepend('Не выбрано', 0),  $organization->taxation_system_type_id,  ['class' => 'form-control select2-all']) }}
                        </div>
                    </div>

                    {{--
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Метод подтверждения оплаты</label>
                        <div class="col-sm-8">
                            {{ Form::select('request_type_id', collect(\App\Models\Organizations\Organization::REQUEST_TYPE)->prepend('Не выбрано', 0),  $organization->request_type_id,  ['class' => 'form-control select2-all']) }}
                        </div>
                    </div>
                    --}}

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Тип кассы api</label>
                        <div class="col-sm-8">
                            {{ Form::select('cashbox_api_type_id', collect(\App\Models\Organizations\Organization::CASHBOX_API_TYPE)->prepend('Не выбрано', 0),  $organization->cashbox_api_type_id,  ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Логин</label>
                        <div class="col-sm-8">
                            {{ Form::text('cashbox_api_login',  $organization->cashbox_api_login,  ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Пароль</label>
                        <div class="col-sm-8">
                            {{ Form::text('cashbox_api_pass',  $organization->cashbox_api_pass,  ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Token</label>
                        <div class="col-sm-8">
                            {{ Form::text('cashbox_api_token',  $organization->cashbox_api_token,  ['class' => 'form-control']) }}
                        </div>
                    </div>


                    @if(auth()->user()->hasPermission('directories', 'organizations_edit'))
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    @endif

                </div>

            {{ Form::close() }}
        </div>
    </div>
</div>