@extends('layouts.frame')

@section('title')

    Банковские реквизиты

@endsection

@section('content')



{{ Form::model($org_bank_account, ['url' => url("/directories/organizations/org_bank_account/$org_bank_account->id"), 'method' => 'put', 'class' => 'form-horizontal']) }}

@include('organizations.org_bank_account.form')

{{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>
    @if($bank_account_id)
        <button onclick="deleteBank({{$bank_account_id}})" class="btn btn-danger pull-left">{{ trans('form.buttons.delete') }}</button>
    @endif

    <script>
        function deleteBank(id){

            $.post({
                url: '/directories/organizations/org_bank_account/{{$org_bank_account->id}}',
                type: 'delete',
                data: {
                  id: id,
                },
                method: 'delete',
                success: function(){
                    window.parent.reload();
                }
            })
        }
    </script>

@endsection

