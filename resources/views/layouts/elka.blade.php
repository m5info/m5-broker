<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name') }}</title>
        <meta name="description" content="Система для страховых компаний"/>

        <link rel="shortcut icon" href="/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />    <!-- Долой quirks mode -->
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" /> <!-- Чтобы работали Media Queries запросы -->

        {{-- без этого не работает нормально навбар --}}
        <link rel="stylesheet" type="text/css" href="/assets/new/lib/bootstrap/css/bootstrap.css">

        {{-- а это ток который был на старом дизайне. он лучше и ведёт себе адекватнее.
        надо либо переносить точечно необходимые стили из первого или оставлять так,
        чтоб он перекрывал неадекват от первого --}}
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">

        <link rel="stylesheet" type="text/css" href="/assets/new/css/font.css">
        <link rel="stylesheet" type="text/css" href="/assets/new/css/main.css">




        <script src="/plugins/jquery/jquery.min.js"></script>


        <script language="javascript" src="/electronic_cashier/logger.js?p={{time()}}" type="text/javascript"></script>
        <script language="javascript" src="/electronic_cashier/parameters.js?p={{time()}}" type="text/javascript"></script>
        <script language="javascript" src="/electronic_cashier/states.js?p={{time()}}" type="text/javascript"></script>
        <script language="javascript" src="/electronic_cashier/scenario.js?p={{time()}}" type="text/javascript"></script>
        <script language="javascript" src="/electronic_cashier/messages.js?p={{time()}}" type="text/javascript"></script>
        <script language="javascript" src="/electronic_cashier/currency.js?p={{time()}}" type="text/javascript"></script>
        <script language="javascript" src="/electronic_cashier/applications.js?p={{time()}}" type="text/javascript"></script>
        <script language="javascript" src="/electronic_cashier/templates.js?p={{time()}}" type="text/javascript"></script>

        <script language="JScript" for="BarcodeReader" event="BarcodeRead()">
            var a = 0;
        </script>

        <script language="JScript" for="ReceiptPrinter" event="OnPrinterEvent()">
            var a = 0;
        </script>


        <script src="/electronic_cashier/m_controllers/main_ecashier.js?p={{time()}}"></script>
        <script src="/electronic_cashier/m_controllers/scaner_barcode.js?p={{time()}}"></script>
        <script src="/electronic_cashier/m_controllers/printer.js?p={{time()}}"></script>
        <script src="/electronic_cashier/m_controllers/payment.js?p={{time()}}"></script>
        
        @include('layouts.css')
        @yield('head')
    </head>
    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">

                <div class="col-md-2 col-md-offset-0 text-left">
                    <div class="divide10"></div>
                    <span style=" font-size: 20px">
                    <b style="color: #feda44;">M5</b> <span style="color: #fff;" >версия 2.0</span>
                </span>
                </div>

                <div class="hidden-xs col-md-4 col-md-offset-6 nav-user text-right">
                    <div class="divide10"></div>
                    <span style="color: #fff;font-size: 10px;" >©2013. M5 Group, All Rights Reserved. Manufacturing of М5-INFO</span>
                </div>


            </div>
        </nav>
        <div id="container" class="container-fluid">
            <div class="wrapper">
                <div class="cont-in cont-in-mcontent">

                    <div class="content">


                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        <div class='loader'></div>


        @yield('js')


    </body>
</html>

