{{--<script src="/assets/new/lib/jquery/js/jquery.js"></script>--}}

<script src="/plugins/jquery-ui/jquery-ui.min.js"></script>


<script src="/assets/new/lib/bootstrap/js/bootstrap.js"></script>
<script src="/assets/new/lib/inputmask/js/inputmask.js"></script>
<script src="/assets/new/js/isLoading.js"></script>
<script src="/assets/new/lib/nicescroll/js/jquery.nicescroll.js"></script>
<script src="/assets/js/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables.bootstrap.js"></script>

<script src="/plugins/fancybox/jquery.fancybox.js"></script>
<script src="/js/jquery.easyui.min.js"></script>
<script src="/plugins/jquery-mask/jquery.mask.min.js"></script>
<script src="/plugins/select2/select2.min.js"></script>
<script src="/plugins/select2/select2_locale_ru.js"></script>
{{--<script src="/assets/js/jquery.suggestions.min.js"></script>--}}
<script src="/assets/js/jquery.new.suggestions.min.js"></script> {{-- Добавлено 24 декабря 2019 --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>


<script src="/plugins/datepicker/jquery.datepicker.js"></script>
<script src="/plugins/moment/moment-with-locales.min.js"></script>
<script src="/plugins/datetimepicker/bootstrap-material-datetimepicker.js"></script>
<script src="/plugins/datepicker/datepicker-ru.js"></script>
<script src="/js/jquery-ui.multidatespicker.js"></script>
<script src="/assets/js/toastr.js"></script>
<script src="/plugins/jquery-floatThead/jquery.floatThead.js"></script>

<script src="/js/theme.js?p={{time()}}"></script>
<script src="/js/custom.js?p={{time()}}"></script>
<script src="/assets/new/js/main.js?p={{time()}}"></script>
<script src="/assets/new/js/workarea.js?p={{time()}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>




<script>

    Dropzone.options.addManyDocForm = {
        maxFileSize: 100,
    };

    $(function () {
        $(".datepicker").datepicker({
            minDate: '-5Y',
            maxDate: '+5Y',
            showButtonPanel: true,
            changeYear: true
        });

        $(".main-menu-expand").click();
    });

    function customConfirm() {

        return confirm('{{trans('form.are_you_sure')}}');
    }

    function newCustomConfirm(func, title = '{{trans('form.are_you_sure')}}', text = '{{trans('form.confirm_action')}}') {
        Swal.fire({
            title: title,
            text: text,
            // icon: 'question',
            showCancelButton: true,
            confirmButtonColor: 'rgb(234, 137, 58)',
            cancelButtonColor: 'rgba(138, 138, 138, 0.78)',
            confirmButtonText: 'Да',
            cancelButtonText: 'Отмена',
            onOpen: (toast) => {
                $('.swal2-confirm').css('background-image', '-webkit-gradient(linear, left bottom, right bottom, color-stop(0%, #eb5a2d), color-stop(100%, #f3bb53))');
            }
        }).then((result) => {
            func(result.value);
        });
    }

    function process(operations = []){
        $.each(operations, function(k,operation){
            if(isFunction(operation)){
                window[operation]()
            }
        })
    }

    function removeFile(fileName) {
        newCustomConfirm(function (result) {
            if (result){
                process(['save_contract']);

                var filesUrl = '{{ url(\App\Models\File::URL) }}';
                var fileUrl = filesUrl + '/' + fileName;
                $.post(fileUrl, {
                    _method: 'DELETE'
                }, function () {
                    reload();
                });
            }
        });
    }

    function _removeFile(fileName){
        newCustomConfirm(function (result) {
            if (result){
                process(['save_contract']);

                var filesUrl = '{{ url(\App\Models\File::URL) }}';
                var fileUrl = filesUrl + '/' + fileName;
                $.post(fileUrl, {
                    _method: 'DELETE'
                }, function () {
                    reload();
                });
            }
        });
    }

    $(function(){
        $('#addManyDocForm').on("DOMNodeInserted", function (event) {

            process(['save_contract']);
            $('#addManyDocForm').off();
            if(isFunction('saveContract')){
                saveContract();
            }
            /*setTimeout(function(){
                location.reload();
            }, 1000);*/

        });
    })



    function newAlert2(title, text) {
        Swal.fire({
            title: title,
            text: text,
        });
    }

</script>

@if(session()->has('callback'))
    <script>
        $(function(){
            eval('{!! session('callback') !!}');
        });
    </script>
@endif
