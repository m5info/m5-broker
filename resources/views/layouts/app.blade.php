<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <meta name="description" content="Система для страховых компаний"/>

    <link rel="shortcut icon" href="/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>    <!-- Долой quirks mode -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1"/>
    <!-- Чтобы работали Media Queries запросы -->

    {{--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>--}}
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <link data-html="user" rel="stylesheet" type="text/css"
          href="/assets/new/css/user_themes/{{ auth()->user()->theme->css_file ?? \App\Models\Users\UserThemes::themes[0]['file'] }}.css">
    {{--Изменение темы / шрифт--}}
    @if( isset(auth()->user()->theme->custom_styles))
        <style type="text/css" data-html="user">
            body > *, body {
                font-size: {{ auth()->user()->theme->custom_styles }}px !important;
            }

        </style>
    @else
        <style type="text/css" data-html="user">body {
            }</style>
    @endif
    {{-- без этого не работает нормально навбар --}}
    <link rel="stylesheet" type="text/css" href="/assets/new/lib/bootstrap/css/bootstrap.css">

    {{-- а это ток который был на старом дизайне. он лучше и ведёт себе адекватнее.
    надо либо переносить точечно необходимые стили из первого или оставлять так,
    чтоб он перекрывал неадекват от первого --}}
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="/assets/new/css/font.css">
    <link rel="stylesheet" type="text/css" href="/assets/new/css/main.css">

    <link rel="stylesheet" type="text/css" href="/assets/new/css/guide.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="/plugins/jquery/jquery.min.js"></script>
    @include('layouts.css')
    @yield('head')
</head>
<body>

<!-- WORKAREA -->
<span id="work-area-hook" class="btn btn-success" style="display: none">действия</span>
<div id="work-area" style="display: none">
    @yield('work-area')
</div>

<!-- SIDEBAR -->
@if(!isset(auth()->user()->settings) || !auth()->user()->settings['menu'])
    <nav id="sidebar">
        <div id="dismiss">
            <i class="glyphicon glyphicon-arrow-left"></i>
        </div>
        <div class="sidebar-header">
            <h3>M5 версия 2.0</h3>
        </div>

        @include('partials.menu')
        @include('partials.user_settings')

    </nav>
@endif

<div class="overlay  "></div>
<div id="header" class="container-fluid">
    <div class="nav-container">
        <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button id="sidebarCollapse" type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="/" class="logo"></a>


                <div id="search-box">
                    <div>
                        <div class="input-group">

                            <form action="/search" id="my_search" method="get">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-4" style="padding-right: 0;">
                                        {{ Form::select('global_search_sk', \App\Models\Directories\InsuranceCompanies::all()->pluck('title','id')->prepend('Все',0), session()->get('search.find_name_sk')?:'', ['class' => 'form-control select2-ws']) }}
                                    </div>
                                    <div class="col-lg-5 col-sm-5" style="padding: 0;">
                                        <input type="search" name="find" class="form-control"
                                               value="{{session()->get('search.find')?:''}}"/>
                                    </div>
                                    <div class="col-lg-3 col-sm-3" style="padding: 0 16px 0 0;">
                                        {{ Form::select('search_type', collect([1 => 'БСО', 2 => 'Страхователь', 3 => 'Счет', 4 => 'Направление']), session()->get('search.find_search_type')?:'', ['class' => 'form-control select2-ws']) }}
                                    </div>
                                </div>
                            </form>
                            <div class="input-group-btn">
                                <button class="btn btn-primary"
                                        style="margin: -5px 0 0 0px;height: 31px; padding: 7px 12px;" type="submit"
                                        onclick="$('#my_search').submit()">
                                    Найти
                                </button>
                            </div>
                        </div>
                        <small>
                                    <span class="search-note">Например,
                                        <a href="#" class="link link-grey" alt="Найти">XXX 0000000</a> или
                                        <a href="#" class="link link-grey" alt="Найти">Можайкин Андрей</a>
                                    </span>
                        </small>
                    </div>
                </div>

            </div>
        </nav>
        <div id="messages">
            @include('layouts.messages')

        </div>

        <div class="header-top-right">
            @include('layouts.notifications')
            <div id="userData" class="user-data">
                <a href="#" class="user-name"> {{ auth()->user()->name }} </a>
                <div class="user-company">{{ auth()->user()->organization()->exists() ? auth()->user()->organization->title : ''}}</div>
                <div class="user-data-container">
                    <div class="user-description">
                        <span class="role-name">{{ auth()->user()->email }}</span>
                        <hr>
                        <div class="role-description"></div>
                    </div>
                    <ul>

                        @php $guides = \App\Models\Guides\Guide::getGuidesListByGroup()  @endphp
                        @if($guides['guides'])
                            <li>
                                <a onclick="openFancyBoxFrame_customHeight('{{url("/informing/guide/show/?group=".$guides['group'])}}', 400)">
                                    <span class="glyphicon glyphicon-question-sign"></span>{{ $guides['guides'] }}
                                </a>
                            </li>
                        @endif


                        <li>
                            <a onclick="openFancyBoxFrame_customHeight('{{url("/user/themes")}}', 400)">
                                <span class="glyphicon glyphicon-text-background"></span>
                                Отображение
                            </a>
                        </li>
                        <li>
                            <a onclick="openFancyBoxFrame_customHeight('{{url("users/get_form_change_password")}}', 400)">
                                <span class="glyphicon glyphicon-cog"></span>
                                Изменить пароль
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div title="выйти из системы" class="user-logout">
                <a href="{{ url('/logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">&nbsp;
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>
<div id="container" class="container-fluid">
    <div class="wrapper">
        <div class="cont-in cont-in-mcontent">
            @if(isset(auth()->user()->settings)>0 && auth()->user()->settings['menu'])
                <style>
                    .menu-fixed {
                        /*height:100% !important;*/
                        width: 250px;
                        margin-right: 15px;
                        float: left;
                        margin-left: -35px;
                        margin-top: -22px;
                    }

                    .menu-fixed #sidebar {
                        width: 250px;
                        position: static;
                        top: 0;
                        left: 0px;
                        height: auto;
                        z-index: 999;
                        background: #fff;
                        transition: all 0.3s;
                        overflow-y: hidden;
                        box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.2);
                        border-left: 1px solid #e0e0e0;
                    }

                    .user-settings-block {
                        position: static !important;
                    }

                    .user-settings-block a {
                        margin-bottom: 15px;
                    }

                    #sidebarCollapse {
                        display: none !important;
                    }

                    #sidebar .sidebar-header h3 {
                        margin-bottom: 0px !important;
                    }

                    #sidebar .sidebar-header {
                        height: auto !important;
                        padding: 0px 20px;
                        background-color: #de4020;
                        background-image: -moz-linear-gradient(left, #de4020 0%, #eb8a3a 100%);
                        background-image: -webkit-linear-gradient(left, #de4020 0%, #eb8a3a 100%);
                        background-image: -ms-linear-gradient(left, #de4020 0%, #eb8a3a 100%);
                        background-image: linear-gradient(left, #de4020 0%, #eb8a3a 100%);
                        background-image: -o-linear-gradient(left, #de4020 0%, #eb8a3a 100%);
                        background-image: -webkit-gradient(linear, left bottom, right bottom, color-stop(0%, #de4020), color-stop(100%, #eb8a3a));
                        overflow: hidden;
                        height: 34px;
                    }

                </style>
                <div class="menu-fixed">
                    <nav id="sidebar">
                        <div id="dismiss">
                            <i class="glyphicon glyphicon-arrow-left"></i>
                        </div>
                        <div class="sidebar-header">
                            <h3>M5 версия 2.0</h3>
                        </div>
                        @include('partials.menu')
                        @include('partials.user_settings')

                    </nav>
                </div>
            @endif
            <div class="content">

                @if(isset($breadcrumbs))
                    <div class="page-heading">
                        {!! breadcrumb($breadcrumbs) !!}
                    </div>
                @endif

                @yield('content')
            </div>
        </div>
        <div class='loader'></div>
{{--        @if( request()->route()->uri() == "/")
            <div id="footer">
                <div class="footer-in">
                    <div class="cont-in">
                        <div class="foot-right">
                            <div class="copyright">© 2019 Все права защищены.</div>
                            --}}{{--<div class="rights"><a href="https://api.sst.cat/agree/" target="_blank">Условия использования</a>&nbsp;&nbsp;<a href="https://sst.cat/policy/" target="_blank">Политика конфиденциальности</a></div>--}}{{--
                            --}}{{--<div class="mail">e-mail: <a href="mailto:info@sst.cat">info@sst.cat</a></div>--}}{{--
                        </div>
                        <div class="foot-des">--}}{{--<p>По всем вопросам обращайтесь <a href="mailto:support@sst.cat">support@sst.cat</a></p>--}}{{--
                            <p>Для оперативности решения технических ошибок, при обращении указывайте номер полиса и/или Ф.И.О. клиента, описание проблемы, а так же PrintScr (
                                <a href="https://yandex.ru/support/common/support/screenshot.xml" target="_blank">инструкция</a>
                                ).
                            </p>
                            <p>--}}{{--<a href="https://sst.cat/" target="_blank">ООО «Современные Страховые Технологии»</a>--}}{{--</p>
                        </div>
                    </div>
                </div>
            </div>
        @endif--}}
    </div>

    @include('layouts.js')

    <script src="/js/jquery.easyui.min.js"></script>
    <script src="/js/jquery.scrollbar.min.js"></script>

    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">

    @yield('js')

    @yield('js_home')

    <script id="debug_info">

        $(function () {
            $('form#my_search input[type=search]').keypress(function (e) {
                if (e.which == 13) {
                    $('form#my_search').submit();
                    return false;    //<---- Add this line
                }
            });
        });

        @php
            $notify = \App\Models\User::query()
                ->where('id', auth()->id())
                ->where('notification_news', 0)
                ->first();
        @endphp

        @if($notify)
            Swal.fire({
                text: "У вас есть непрочитанные новости",
                icon: 'warning',
                confirmButtonText: 'Ознакомиться',
                cancelButtonText: 'Ок. Потом',
                showCancelButton: true,
                confirmButtonColor: '#ea893a'
            }).then((result) => {
                $.post('/informing/news/confirm-read', function(){
                    if (result.isConfirmed){
                        location.href = '/informing/news';
                    }
                });
        });
        @endif

    </script>


</body>
</html>

