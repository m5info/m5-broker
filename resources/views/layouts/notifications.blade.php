@php
    $notifications = auth()->user()->getNotifications()->get();
@endphp

<div class="user-messages">
    <a href="/users/notification/">
        <span class="glyphicon glyphicon-bell bell-first"></span>
        <span class="glyphicon glyphicon-bell bell-second"></span>

        <div class="user-alerts-counter">
            <span class="round-big"></span>
            <span class="round-small"
                  id="_count_notification" data-count="{{$notifications->count()}}">{{ $notifications->count() > 9 ? "9+" : $notifications->count() }}</span>
        </div>
    </a>
    <div class="user-messages-container">
        @if(sizeof($notifications))
            <div style="" id="clear_all_messages_wrapper">
                <ul style="display: inline-block;">
                    <li onclick="clear_all_messages();">Убрать
                        все уведомления
                    </li>
                </ul>
            </div>
            @foreach($notifications as $notification)
                {!! $notification->notifier()->display() !!}
            @endforeach
        @else
            <p style="margin-top: 12px; padding-bottom: 10px;">Нет уведомлений</p>
        @endif
    </div>

</div>


