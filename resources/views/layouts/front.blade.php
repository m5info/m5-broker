<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <meta name="description" content="Система для страховых компаний"/>

    <link rel="shortcut icon" href="/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>    <!-- Долой quirks mode -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1"/>
    <!-- Чтобы работали Media Queries запросы -->

    {{--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>--}}
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <link data-html="user" rel="stylesheet" type="text/css"
          href="/assets/new/css/user_themes/{{ auth()->user()->theme->css_file ?? \App\Models\Users\UserThemes::themes[0]['file'] }}.css">
    {{--Изменение темы / шрифт--}}
    @if( isset(auth()->user()->theme->custom_styles))
        <style type="text/css" data-html="user">
            body > *, body {
                font-size: {{ auth()->user()->theme->custom_styles }}px !important;
            }

        </style>
    @else
        <style type="text/css" data-html="user">body {
            }</style>
    @endif
    {{-- без этого не работает нормально навбар --}}
    <link rel="stylesheet" type="text/css" href="/assets/new/lib/bootstrap/css/bootstrap.css">

    {{-- а это ток который был на старом дизайне. он лучше и ведёт себе адекватнее.
    надо либо переносить точечно необходимые стили из первого или оставлять так,
    чтоб он перекрывал неадекват от первого --}}
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="/assets/new/css/font.css">
    <link rel="stylesheet" type="text/css" href="/assets/new/css/main.css">

    <link rel="stylesheet" type="text/css" href="/assets/new/css/guide.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="/plugins/jquery/jquery.min.js"></script>
    @include('layouts.css')
    @yield('head')
</head>
<body>

<div id="container" class="container-fluid">
    <div>
        <div class="content" style="margin-top:0px;">
            @yield('content')
        </div>
    </div>
    <div class='loader'></div>
</div>

@include('layouts.js')

@yield('js')


</body>
</html>