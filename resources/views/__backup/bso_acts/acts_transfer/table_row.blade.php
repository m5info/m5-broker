@if($data)
@foreach($data as $key => $obj)
    <tr class="clickable-row-blank" data-href="/bso_acts/show_bso_act/{{$obj['id']}}/" style="cursor: pointer;">
        @include('bso_acts.acts_transfer.row', ["act" => \App\Models\BSO\BsoActs::getActId($obj["id"])])
    </tr>
@endforeach
@endif