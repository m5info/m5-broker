@extends('layouts.app')

@section('content')


    <div class="page-heading">
        <h2>Акты Прием/Передача
        </h2>

    </div>


    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <div class="filter-group">

                СДЕЛАТЬ ФИЛЬТРЫ Время создания; Тип; Передал; Принял; Сотрудник; Точка продаж




                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                <div class="filter-group">
                    <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        {{Form::select('pageCount', collect([25=>'25', 50=>'50', 100=>'100', 150=>'150']), request()->has('page')?request()->session()->get('page'):25, ['class' => 'form-control select2-all', 'id'=>'pageCount', 'onchange'=>'loadItems()'])}}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                        <span id="view_row"></span>/<span id="max_row"></span>
                    </div>
                </div>
            </div>


        </div>
    </div>



    <div class="block-inner sorting col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow: auto;">
        <table class="dataTable">
            <thead>
            <tr>
                <th>Акт</th>
                <th>Номер заявки</th>
                <th>Время создания</th>
                <th>Тип</th>
                <th>Передал</th>
                <th>Принял</th>
                <th>Сотрудник</th>
                <th>Статус</th>
                <th>Точка продаж</th>
            </tr>
            </thead>
            <tbody id="table_row"></tbody>
        </table>
        <div class="row">
            <div id="page_list" class="easyui-pagination pull-right"></div>
        </div>
    </div>




@endsection

@section('js')



    <script>

        var PAGE = 1;

        $(function () {

            loadItems();


        });

        function loadItems() {
            var data = {

                pageCount: $("#pageCount").val(),
                PAGE: PAGE,

            };


            $('#page_list').html('');
            $('#table_row').html('');
            $('#view_row').html(0);
            $('#max_row').html(0);


            loaderShow();
            $.post("{{url("/bso_acts/acts_reserve/list/")}}", {data:data}, function (response) {


                $('#view_row').html(response.view_row);
                $('#max_row').html(response.max_row);

                $('#page_list').pagination({
                    total:response.page_max,
                    pageSize:1,
                    pageNumber: data.PAGE,
                    layout:['first','prev','links','next','last'],
                    onSelectPage: function(pageNumber, pageSize){
                        setPage(pageNumber);
                    }
                });
                loaderHide();

                createRow(response.result);


            }).done(function() {
                loaderShow();
            }).fail(function() {
                loaderHide();
            }).always(function() {
                loaderHide();
            });


        }

        function setPage(field) {
            PAGE = field;
            loadItems();
        }

        function createRow(data){
            if(data) {


                $.post("{{url("/bso_acts/acts_reserve/get_view_row/")}}", {data:data}, function(res){

                    $('#table_row').html(res);

                    $(".clickable-row-blank").click( function(){
                        if ($(this).attr('data-href')) {
                            window.open($(this).attr('data-href'), '_blank');
                        }
                    });

                    $('td').css({'border-right': '1px #e0e0e0 solid'});
                    $.each($('tr'), function(i, v){
                        var children = $(v).children().first();
                        if(children[0].localName === 'td'){
                            children.css({'border-left': '1px #e0e0e0 solid'});
                        }
                    });

                });

            }
        }

    </script>


@endsection