@extends('layouts.app')

@section('content')


    <div class="page-heading">
        <h2>Акт # {{$act->act_number}}
            <span class="btn btn-success">Выгрузить в XLS</span>
        </h2>

    </div>


    <div class="row form-horizontal" id="main_container">
        <div class="block-main">
            <div class="block-sub">

                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Дата/время</label>
                                <div class="col-sm-12">
                                    {{setDateTimeFormatRu($act->time_create)}}
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Тип акта</label>
                                <div class="col-sm-12">
                                    {{$act->type->title}}
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Статус БСО</label>
                                <div class="col-sm-12">
                                    {{$act->bso_state->title}}
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Точка продаж</label>
                                <div class="col-sm-12">
                                    {{$act->point_sale->title}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label class="col-sm-12 control-label">Выдал</label>
                                <div class="col-sm-12">
                                    {{($act->user_from)?$act->user_from->name:''}}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="col-sm-12 control-label">Получил</label>
                                <div class="col-sm-12">
                                    {{($act->user_to)?$act->user_to->name:''}}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="col-sm-12 control-label">Сотрудник</label>
                                <div class="col-sm-12">
                                    {{($act->bso_manager)?$act->bso_manager->name:''}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal">

                        @include("bso_acts.show_bso_act.table.view", ['act' => $act])

                    </div>

            </div>
        </div>
    </div>




@endsection

@section('js')

    <script>


        $(function () {




        });



    </script>


@endsection