<table class="table table-bordered bso_items_table">
    <tr>
        <th>№ п/п</th>
        <th>Организация</th>
        <th>Страховая компания</th>
        <th>Вид страхования</th>
        <th>№ полиса / квит. / сер.карт с</th>
        <th>№ бланка</th>
        <th>Событие</th>
        <th>Статус</th>
    </tr>

    @if(sizeof($act->logs))
        @foreach($act->logs as $key => $bso_log)

            <tr>
                <td>{{$key+1}}</td>
                <td>{{$bso_log->bso->supplier_org->title}}</td>
                <td>{{$bso_log->bso->supplier->title}}</td>
                <td>{{$bso_log->bso->type->title}}</td>
                <td><a href="{{url("/bso/items/{$bso_log->bso->id}/")}}">{{$bso_log->bso->bso_title}}</a></td>
                <td>{{$bso_log->bso->bso_blank_title}}</td>
                <td>{{$bso_log->bso_location->title}}</td>
                <td>{{$bso_log->bso_state->title}}</td>
            </tr>

        @endforeach
    @endif

</table>

<style>




    .bso_table {
       /* font: 12px arial;*/
        border: 1px solid #777;
        border-collapse: collapse;
    }
    .bso_table td, th {
        border: 1px solid #777;
        padding: 5px;
        /*font: 12px arial;*/
    }

    .bso_table th {
        background-color: #EEE;
    }

    .bso_table td {
        background-color: #FFF;
    }

    .bso_header {
        font: 12px arial;
        border: none;
        border-collapse: collapse;
        width: 100%;
    }
    .bso_header td {
        padding: 5px;
        border: none;
        font: 12px arial;
        background-color: #F3F3F3;
    }

    .sk_header {
        font: bold 17px arial !important;
    }

    .center {
        text-align: center; !important;
    }

    .gray {
        background-color: #EEE !important;
    }
    input[type=button] {
        cursor: pointer;
    }


</style>