<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

    <div class="block-inner">



            <table class="dataTable">
                <thead>
                <tr>
                    <th>{{ trans('users/users.index.name') }}</th>
                    <th>Подразделение</th>
                    <th>Статус</th>
                    @if(auth()->user()->hasPermission('directories', 'organizations_user'))
                    <th>

                        <span onclick="openFancyBoxFrame('/users/frame/?user_id=0&org_id={{$organization->id}}')" class="btn btn-primary pull-right">
                            {{ trans('form.buttons.add') }}
                        </span>

                    </th>
                    @endif
                </tr>
                </thead>
                @if(sizeof($organization->users))
                @foreach($organization->users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->department ? $user->department->title : '' }}</td>
                        <td>{{ \App\Models\User::STATUS_USER[$user->status_user_id] }}</td>
                        @if(auth()->user()->hasPermission('directories', 'organizations_user'))
                        <td>

                            <span onclick="openFancyBoxFrame('/users/frame/?user_id={{$user->id}}&org_id={{$organization->id}}')" class="btn btn-primary pull-right">Редактировать</span>
                        </td>
                        @endif
                    </tr>
                @endforeach
                @endif
            </table>


    </div>
</div>