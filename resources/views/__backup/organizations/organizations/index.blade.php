@extends('layouts.app')

@section('content')


        <div class="page-heading">
            <h1>{{ trans('menu.organizations') }}</h1>
            @if(auth()->user()->hasPermission('directories', 'organizations_edit'))
            <a class="btn btn-primary" href="{{ url("$control_url/organizations/create")  }}">
                {{ trans('form.buttons.create') }}
            </a>
            @endif
        </div>


        <div class="block-inner">
        @if(sizeof($organizations))
            <table class="largeTable">
                <thead>
                <tr>
                    <th></th>
                    <th>{{ trans('organizations/organizations.title') }}</th>
                    <th>Тип</th>
                    <th>Руководитель организации</th>
                    <th>Контактное лицо</th>
                    <th>{{ trans('organizations/organizations.phone') }}</th>
                </tr>
                </thead>
                @foreach($organizations as $organization)
                    <tr>
                        <td><a class="btn btn-primary" href="{{url ("$control_url/organizations/$organization->id/edit")}}">Открыть</a> </td>
                        <td>{{ $organization->title }}</td>
                        <td>{{ $organization->org_type->title }}</td>
                        <td>{{ (isset($organization->parent_user))?$organization->parent_user->name: '' }}</td>
                        <td>{{ $organization->user_contact_title }}</td>
                        <td>{{ $organization->phone }}</td>
                    </tr>
                @endforeach

            </table>
        @else
            {{ trans('form.empty') }}
        @endif
        </div>

@endsection



