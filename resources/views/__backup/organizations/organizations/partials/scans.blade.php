<div class="block-main col-xs-12 col-sm-12 col-md-12 col-lg-6">
    @if($organization->scans->count())
        <table class="table orderStatusTable dataTable no-footer">
            <thead>
            <tr>
                <th>{{ trans('users/users.edit.title') }}</th>
                @if(auth()->user()->hasPermission('directories', 'organizations_edit'))<th>&nbsp;</th>@endif
            </tr>
            </thead>
            <tbody>
            @foreach($organization->scans as $file)
                <tr>
                    <td>
                        <a href="{{ url($file->url) }}"  target="_blank">
                            {{ $file->original_name }}
                        </a>
                    </td>
                    @if(auth()->user()->hasPermission('directories', 'organizations_edit'))

                    <td>
                        <button class="btn btn-danger" type="button" onclick="removeFile('{{ $file->name }}', 1)">
                            {{ trans('form.buttons.delete') }}
                        </button>
                    </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    @else

        <center><h3>{{ trans('form.empty') }}</h3></center>
    @endif
</div>



<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
{!! Form::open(['url'=>"/directories/organizations/organizations/$organization->id/scans",'method' => 'post', 'class' => 'dropzone', 'id' => 'addOrgDocForm']) !!}
{!! Form::close() !!}

</div>