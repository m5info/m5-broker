

@if(auth()->user()->hasPermission('directories', 'organizations_edit'))
{{ Form::model($organization, ['url' => url($send_urls), 'method' => 'put',  'class' => 'form-horizontal']) }}
@endif

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="block-main">
        <div class="block-sub">
            <div class="form-horizontal">


                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ trans('organizations/organizations.is_actual') }}</label>
                    <div class="col-sm-4">
                        {{ Form::checkbox('is_actual', 1, ((int)$organization->id>0)?$organization->is_actual:1) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ trans('organizations/organizations.type') }}</label>
                    <div class="col-sm-8">
                        {{ Form::select('org_type_id', \App\Models\Settings\TypeOrg::where('is_actual', 1)->get()->pluck('title', 'id'),  $organization->org_type_id,  ['class' => 'form-control select2-all']) }}
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-4 control-label">Руководитель организации</label>
                    <div class="col-sm-8">
                        {{ Form::select('parent_user_id', \App\Models\User::where('is_parent', 1)->where('organization_id', (isset($organization) ? $organization->id : -1))->orderBy('name')->get()->pluck('name', 'id')->prepend('Нет', 0),  (isset($organization) ? $organization->parent_user_id : auth()->id()),  ['class' => 'form-control select2-all']) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ trans('organizations/organizations.title') }}</label>
                    <div class="col-sm-8">
                        {{ Form::text('title', $organization->title, ['class' => 'form-control party-autocomplete ', 'data-party-type' => 'organizations', 'data-name' => 'organizations_title']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ trans('organizations/organizations.title_doc') }}</label>
                    <div class="col-sm-8">
                        {{ Form::text('title_doc', $organization->title_doc, ['class' => 'form-control ', 'data-name' => 'organizations_title_doc']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ trans('organizations/organizations.general_manager') }}</label>
                    <div class="col-sm-8">
                        {{ Form::text('general_manager', $organization->general_manager, ['class' => 'form-control ', 'data-name' => 'organizations_general_manager']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ trans('organizations/organizations.inn') }}</label>
                    <div class="col-sm-8">
                        {{ Form::text('inn', $organization->inn, ['class' => 'form-control party-autocomplete ', 'data-name' => 'organizations_inn', 'data-party-type' => 'organizations']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">КПП</label>
                    <div class="col-sm-8">
                        {{ Form::text('kpp', $organization->kpp, ['class' => 'form-control', 'data-name' => 'organizations_kpp']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ trans('organizations/organizations.address') }}</label>
                    <div class="col-sm-8">
                        {{ Form::text('address', $organization->address, ['class' => 'form-control address-autocomplete ', 'data-name' => 'organizations_address', 'data-address-type' => 'organizations']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Фактический адрес</label>
                    <div class="col-sm-8">
                        {{ Form::text('fact_address', $organization->fact_address, ['class' => 'form-control address-autocomplete ', 'data-name' => 'organizations_address', 'data-address-type' => 'organizations']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Контактное лицо</label>
                    <div class="col-sm-8">
                        {{ Form::text('user_contact_title', $organization->user_contact_title, ['class' => 'form-control fio-autocomplete', '']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ trans('organizations/organizations.phone') }}</label>
                    <div class="col-sm-8">
                        {{ Form::text('phone', $organization->phone, ['class' => 'form-control phone', '']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ trans('organizations/organizations.email') }}</label>
                    <div class="col-sm-8">
                        {{ Form::text('email', $organization->email, ['class' => 'form-control', '']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ trans('organizations/organizations.comment') }}</label>
                    <div class="col-sm-8">
                        {{ Form::textarea('comment', $organization->comment, ['class' => 'form-control', '']) }}
                    </div>
                </div>


                @if(auth()->user()->hasPermission('directories', 'organizations_edit'))
                <div class="form-group">
                    <div class="col-sm-12">
                        <span id="delete_org" class="btn btn-danger col-sm-2 pull-left" onclick="myOrgDelete()">
                            Удалить
                        </span>
                        <button type="submit" class="btn btn-primary pull-right">
                            Сохранить
                        </button>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@if(auth()->user()->hasPermission('directories', 'organizations_edit'))
{{Form::close()}}
@endif

<script>

    $(function(){

    });

    function initTab() {
        startMainFunctions();

    }
    @if(auth()->user()->hasPermission('directories', 'organizations_edit'))
    function myOrgDelete() {
        if(confirm("Удалить организацию?")){
            $.post('{{url("/directories/organizations/{$organization->id}/delete")}}', {}, function(){
                location.href = '/directories/organizations/organizations/';
            });
        }

    }
    @endif

</script>