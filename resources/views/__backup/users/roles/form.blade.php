<div class="block-sub">
    <div class="block-main-heading">Пользователь</div>
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 control-label">{{ trans('users/roles.edit.title') }}</label>
            <div class="col-sm-9">
                {{ Form::text('title', old('title'), ['class' => 'form-control', 'required']) }}
            </div>
        </div>
    </div>
</div>
@foreach($groups as $group)
    @if(sizeof($group->permissions))
        <div class="divider"></div>
        <div class="block-sub">
            <div class="block-main-heading">{{ trans('users/roles.groups_titles.' . $group->title) }}</div>
            <div class="form-horizontal">
                <div class="row">
                    @foreach($group->permissions->chunk(3) as $permissionsChunk)
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            @foreach($permissionsChunk as $permission)
                                <div class="form-group">
                                    <label class="col-sm-8 control-label">{{ trans('users/roles.titles.' . $permission->title) }}</label>
                                    <div class="col-sm-4">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"  name="permissions[]" value="{{ $permission->id }}"
                                                       @if(isset($role) && $role->hasPermission($permission->id))
                                                       checked
                                                        @endif
                                                >
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    @endforeach
                </div>
                @if($group->is_visibility == 1)
                    <div class="form-group">
                        <span class="col-sm-4 control-label">Видимость по базе</span>
                        <div class="col-sm-6">
                            <input type="hidden" name="groups[]" value="{{$group->id}}"/>
                            {{ Form::select("visibility[$group->id]", collect([
                                0 => 'Все',
                                1 => 'Все в рамках организации',
                                2 => 'Только свои',
                            ]), ((isset($role) && $role->rolesVisibility($group->id))?$role->rolesVisibility($group->id)->visibility:0), ['class' => 'form-control', '']) }}
                        </div>
                    </div>
                @endif
            </div>
        </div>
    @endif
@endforeach


<button class="btn btn-primary btn-lg mb-30 pull-right" style="margin-top: 20px;">Сохранить</button>
