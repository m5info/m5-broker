@extends('layouts.app')

@section('content')



        <div class="page-heading">
            <h1>{{ trans('menu.roles') }}</h1>
            <a class="btn btn-primary" href="{{ url('/users/roles/create')  }}">
                {{ trans('form.buttons.create') }}
            </a>
        </div>

        <div class="block-main">
        @if(sizeof($roles))
            <table class="noScrollTable">
                <thead>
                <tr>
                    <th>{{ trans('users/roles.index.title') }}</th>
                </tr>
                </thead>
                @foreach($roles as $role)
                    <tr class="clickable-row" data-href="{{url ("/users/roles/$role->id/edit")}}">
                        <td width="70%">{{ $role->title }}</td>
                    </tr>
                @endforeach

            </table>
        @else
            {{ trans('form.empty') }}
        @endif
        </div>

@endsection

