@extends('layouts.app')

@section('content')




        <div class="page-heading">
            <h1>{{ trans('menu.users') }}</h1>
            <a class="btn btn-primary" href="{{ url('/users/users/create')  }}">
                {{ trans('form.buttons.create') }}
            </a>
        </div>


        <div class="block-inner">
        @if(sizeof($users))


                <table class="largeTable">
                    <thead>
                    <tr>
                        <th>{{ trans('users/users.index.name') }}</th>
                        <th>{{ trans('users/users.index.role') }}</th>
                        <th>Организация</th>
                    </tr>
                    </thead>
                    @foreach($users as $user)
                        <tr>
                            <td><a href="{{url ("/users/users/$user->id/edit")}}"> {{ $user->name }} </a></td>
                            <td>{{ $user->role ? $user->role->title : '' }}</td>
                            <td>{{ $user->organization ? $user->organization->title : '' }}</td>
                        </tr>
                    @endforeach

                </table>
        @else
            {{ trans('form.empty') }}
        @endif
        </div>

@endsection
