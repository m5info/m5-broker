@extends('layouts.app')

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h2>Результат поиска</h2>

        </div>
        <div class="block-main">
            <div class="block-sub">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        @if(sizeof($bsos))

                            <table class="dataTable">
                                <tr>
                                    <th>Событие</th>
                                    <th>Статус</th>
                                    <th>СК</th>
                                    <th>БСО</th>
                                    <th>Тип</th>
                                    <th>Страхователь</th>
                                    <th>Период</th>
                                    <th></th>
                                </tr>

                                <tbody>

                                @foreach($bsos as $bso)
                                    <tr>
                                        <td>{{$bso->location ? $bso->location->title : ""}}</td>
                                        <td>{{$bso->state ? $bso->state->title : ""}}</td>
                                        <td>{{$bso->supplier ? $bso->supplier->title : ""}}</td>
                                        <td>{{$bso->bso_title}}</td>
                                        <td>{{$bso->type ? $bso->type->title : ""}}</td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            @if(auth()->user()->hasPermission('bso', 'items'))
                                                <a href="/bso/items/{{$bso->id}}/" class="btn btn-success">Открыть</a>
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>


                        @else

                            Нет записей

                        @endif

                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection

@section('js')



@endsection