@if ($errors->any())
    @foreach($errors->all() as $error)
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            <span> {{ $error }}</span>
        </div>
    @endforeach
@endif

@if (session('success') && !count($errors))
    <div class="alert alert-success">
        <button class="close" data-close="alert"></button>
        {{ session('success') }}
    </div>
@endif