<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }}</title>

    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="/assets/material/material-icons.css">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="/assets/new/lib/jquery/js/jquery.js"></script>
    @include('layouts.css')

    @yield('head')


</head>
<body class="yui-skin-sam">


    <div class="header">
        <div class="left-side">
            <a href="/" alt="logo" class="logo" ><img src="/assets/img/logo.png"></a>
            <div class="search-box">
                <div class="search">
                    <form action="{{url("search")}}" id="my_search" method="get">
                        <input type="text" name="find">
                        <span class="input-group-btn">
                    <button class="btn btn-primary" type="button" onclick="$('#my_search').submit()">Найти</button>
                </span>
                    </form>
                </div>
                <span class="search-note">Например, <a href="#" class="link link-grey" alt="Найти">XXX 0000000</a> или <a href="#" class="link link-grey" alt="Найти">Можайкин Андрей</a></span>
            </div>
        </div>
        <div class="right-side">
            <ul class="quick-menu list-unstyled">
                <li>
                    @include('partials.orders-messages')
                </li>
                <li><a href="#" class="link link-dark-grey navbar-top-ico-notice">Уведомления</a></li>
                <li><a href="#" class="link link-dark-grey navbar-top-ico-help">Помощь</a></li>
            </ul>

            @include('partials.account-info')
        </div>
    </div>





<div class="content">

    @include('partials.menu')

    <div class="shutter" data-status="off"></div>

    <div class="container-fluid">

        @include('layouts.messages')

        @yield('content')

    </div>

</div>

<div class='loader hidden'></div>


<div class="container dummy">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 tc">
            <img class="logo" src="/assets/img/logo.png" alt="">
            <img src="/assets/img/dummy.jpg" alt="">
            <p>Для того чтобы начать пользоваться программой, необходимо устройство с разрешением экрана не менее 1024px.</p>
        </div>
    </div>
</div>


@include('layouts.js')

@yield('js')

{{ Form::open(['method' => 'post', 'id' => 'export_file', 'url' => '/export_file']) }}
    <input type="hidden" name="type" id="export_type" value=""/>
    <input type="hidden" name="str" id="export_str" value=""/>
{{ Form::close() }}

</body>
</html>
