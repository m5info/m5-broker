<script src="/plugins/jquery/jquery.min.js"></script>

<script src="/plugins/jquery-ui/jquery-ui.min.js"></script>

<script src="/js/jscolor.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<script src="https://cdn.jsdelivr.net/jquery.suggestions/17.2/js/jquery.suggestions.min.js"></script>

<script src="/assets/js/bootstrap.js"></script>

<script src="/assets/js/main.js"></script>

<script src="/assets/js/toastr.js"></script>

<script src="/assets/js/jquery.dataTables.js"></script>

<script src="/assets/js/dataTables.bootstrap.js"></script>

<script src="/plugins/jasny-bootstrap/jasny-bootstrap.min.js"></script>

<script src="/plugins/datepicker/jquery.datepicker.js"></script>

<script src="/plugins/jquery-mask/jquery.mask.min.js"></script>

<script src="/plugins/select2/select2.min.js"></script>

<script src="/plugins/select2/select2_locale_ru.js"></script>

<script src="/plugins/fancybox/jquery.fancybox.js"></script>

<script src="/plugins/moment/moment-with-locales.min.js"></script>

<script src="/plugins/datetimepicker/bootstrap-material-datetimepicker.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>

<script src="/js/theme.js"></script>

<script src="/js/custom.js"></script>

<script src="/plugins/datepicker/datepicker-ru.js"></script>

<script src="/js/jquery-ui.multidatespicker.js"></script>

<script src="/js/jquery.easyui.min.js"></script>







<script>

    $(function () {
        $(".main-menu-expand").click();
    });


    function customConfirm() {

        return confirm('{{trans('form.are_you_sure')}}');
    }


    function removeFile(fileName) {
        if (!customConfirm()) {
            return false;
        }
        var filesUrl = '{{ url(\App\Models\File::URL) }}';
        var fileUrl = filesUrl + '/' + fileName;
        $.post(fileUrl, {
            _method: 'DELETE'
        }, function () {
            reload();

        });
    }

</script>


