<div class="main-menu main-menu-nested">
    <div class="main-menu-list">

        @foreach($menuItems as $groupTitle => $groupMenuItems)

            @if(is_array($groupMenuItems))

                @if(auth()->user()->hasGroupPermission($groupTitle))

                    <a href="#" class="main-menu-item" id="{{$groupTitle}}">
                        <span class="main-menu-item-ico {{$menuItems[$groupTitle]['ico']}}"></span>
                        <div class="main-menu-item-title">{{ trans('menu.' . $groupTitle) }}</div>
                    </a>
                @endif

            @endif
        @endforeach
    </div>

    @foreach($menuItems as $groupTitle => $groupMenuItems)
        @if(is_array($groupMenuItems))
            @if(auth()->user()->hasGroupPermission($groupTitle))

                <div class="drowdown-panel" id="{{$groupTitle}}-dropdown" data-status="close">
                    <span class="title">{{ trans('menu.' . $menuItems[$groupTitle]['form_title']) }}</span>

                    <ul class="list-unstyled">
                        @foreach($menuItems[$groupTitle]['links'] as $menuItem => $menuItemTitle)
                            @if(auth()->user()->hasPermission($groupTitle, $menuItemTitle['name']))
                                <li>
                                    <a class="{{$menuItemTitle['ico']}}" href="{{ url($groupTitle . "/" . $menuItemTitle['link']) }}">
                                        {{ trans('menu.'.$menuItemTitle['name']) }}
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                    @if($menuItems[$groupTitle]['form_button_link']!='')
                        <a href="{{url($menuItems[$groupTitle]['form_button_link'])}}" class="btn btn-primary {{$menuItems[$groupTitle]['form_button_class']}}">
                            {{ trans($menuItems[$groupTitle]['form_button_name']) }}
                        </a>
                    @endif
                </div>

            @endif
        @endif
    @endforeach

    <div class="main-menu-expand">
        <span class="main-menu-expand-ico"></span>
        <div class="main-menu-expand-title">Свернуть</div>
    </div>

</div>
