@extends('layouts.auth')

@section('content')

    <form role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="login">{{ trans('auth.login.login') }}</label>
            <input type="text" class="form-control" name="email"  placeholder="{{ trans('auth.login.login') }}" required>
        </div>
        <div class="form-group">
            <label for="password">{{ trans('auth.login.password') }}</label>
            <input type="password" class="form-control" name="password" autocomplete="off" placeholder="{{ trans('auth.login.password') }}" required>
        </div>
        <button type="submit" name="auth" class="btn btn-primary btn-login">{{ trans('form.buttons.enter') }}</button>
    </form>

@endsection
