<div class="row form-horizontal">


    <input type="hidden" name="contract[{{$key}}][object_insurer][title]" id="object_insurer_title_{{$key}}" value="{{$object->title}}"/>
    <input type="hidden" name="contract[{{$key}}][object_insurer][id]"  value="{{$object->id}}"/>
    <input type="hidden" name="contract[{{$key}}][object_insurer][type]"  value="{{$object->type}}"/>



    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" >
        <label>Категория</label>
        {{Form::select("contract[$key][object_insurer][ts_category]", \App\Models\Vehicle\VehicleCategories::query()->get()->pluck('title', 'id'), $object->data()->ts_category, ['class' => 'form-control', 'id'=>"object_insurer_ts_category_$key", 'onchange'=>"getModelsObjectInsurer($key, 0);"])}}

    </div>

    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" >
        <label>Мощность (л.с.)</label>
        {{ Form::text("contract[$key][object_insurer][power]", $object->data()->power, ['class' => 'form-control sum', "id"=>"object_insurer_ts_power_$key"]) }}
    </div>

    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5" >
        <label>Рег. номер</label>
        {{ Form::text("contract[$key][object_insurer][reg_number]", $object->data()->reg_number, ['class' => 'form-control ru_sumb', "id"=>"object_insurer_ts_reg_number_$key"]) }}
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ></div>

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
        <label>Марка</label>
        {{Form::select("contract[$key][object_insurer][mark_id]", \App\Models\Vehicle\VehicleMarks::orderBy('title')->get()->pluck('title', 'id')->prepend('Не выбрано', 0), $object->data()->mark_id, ['class' => 'mark-id mark_id select2', "id"=>"object_insurer_ts_mark_id_$key", 'style'=>'width: 100%;', 'onchange'=>"getModelsObjectInsurer($key, 0);"])}}
    </div>

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
        <label>Модель</label>
        {{Form::select("contract[$key][object_insurer][model_id]", \App\Models\Vehicle\VehicleModels::where('mark_id', $object->data()->mark_id)->orderBy('title')->pluck('title', 'id')->prepend('Не выбрано', 0), $object->data()->model_id, ['class' => 'model_id model-id select2', "id"=>"object_insurer_ts_model_id_$key", 'style'=>'width: 100%;'])}}

    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <label>VIN</label>
        {{ Form::text("contract[$key][object_insurer][vin]", $object->data()->vin, ['class' => 'form-control', "id"=>"object_insurer_ts_vin_$key"]) }}
    </div>

</div>