@extends('layouts.app')

@section('content')


    <div class="page-heading">
        <h2>Быстрый акцепт

            <span class="btn btn-success" id="controlSaveButtonBox" onclick="saveFastAccept()">
                Акцепт
            </span>

            <span class="btn btn-primary pull-right" onclick="addContractCongestion()">
                Добавить
            </span>
        </h2>


    </div>





        {{ Form::open(['url' => url('/contracts/fast_accept/'), 'method' => 'post',  'class' => 'form-horizontal', 'id' => 'formContract']) }}


        <div class="form-group">
            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="control">
            </div>
        </div>

        {{Form::close()}}





@endsection

@section('js')

    <script>




        $(function () {
            controlSaveButton();


        });




        var MY_COUNT_TEMP = 0;

        String.prototype.replaceAll = function (search, replace) {
            return this.split(search).join(replace);
        }

        function addContractCongestion(){

            myHtml = myGetAjax("{{url("/contracts/fast_accept/form_fast_acceptance/")}}");

            myHtml = myHtml.replaceAll('[:KEY:]', MY_COUNT_TEMP);
            myHtml = myHtml.replaceAll('[:NUM:]', (MY_COUNT_TEMP+1));

            $('#control').append(myHtml);

            startContractFunctions(MY_COUNT_TEMP);

            MY_COUNT_TEMP = MY_COUNT_TEMP+1;

            updateContractIndexes();

            controlSaveButton();



        }

        function deleteContract(id){
            $('#contract_'+parseInt(id)).remove();
            updateContractIndexes();
            controlSaveButton();
        }

        function getContract() {
            return $('.temp_contract');
        }



        function updateContractIndexes() {
            var contracts = getContract();
            contracts.each(function (i, contract) {
                $(contract).find('.number').html(i + 1);
            });
        }


        function startContractFunctions(KEY){


            activSearchBso("bso_title",'_'+KEY, 1);



            $('#manager_id_'+KEY).select2({
                width: '100%',
                minimumInputLength: 3,
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true
            });




            $('#insurer_type_'+KEY).change(function () {

                key = $(this).data('key');
                dataForm = $("#contract_"+key);

                type = $(this).val();

                if(parseInt(type) == 0){
                    $(dataForm).find('.insurer_fl').show();
                    $(dataForm).find('.insurer_ul').hide();
                }else{
                    $(dataForm).find('.insurer_fl').hide();
                    $(dataForm).find('.insurer_ul').show();
                }

            });


            $('#insurer_fio_'+KEY).suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "NAME",
                count: 5,
                onSelect: function (suggestion) {
                    key = $(this).data('key');
                    $('#insurer_title_'+key).val($(this).val());

                }
            });

            $('#insurer_title_'+KEY+', #insurer_inn_'+KEY+', #insurer_kpp_'+KEY).suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "PARTY",
                count: 5,
                onSelect: function (suggestion) {
                    var data = suggestion.data;

                    key = $(this).data('key');


                    $('#insurer_title_'+key).val(suggestion.value);
                    $('#insurer_inn_'+key).val(data.inn);
                    $('#insurer_kpp_'+key).val(data.kpp);

                    $('#insurer_fio_'+key).val($('#insurer_title_'+key).val());

                }
            });







            $('.sum')
                .change(function () {
                    $(this).val(CommaFormatted($(this).val()));
                })
                .blur(function () {
                    $(this).val(CommaFormatted($(this).val()));
                })
                .keyup(function () {
                    $(this).val(CommaFormatted($(this).val()));
                });


            $('.phone').mask('+7 (999) 999-99-99');

        }



        function selectBso(object_id, key, type, suggestion)
        {

            var data = suggestion.data;

            if(parseInt(type) == 1){ // БСО
                $('#bso_id'+key).val(data.bso_id);
                $('#bso_supplier_id'+key).val(data.bso_supplier_id);
                $('#insurance_companies_id'+key).val(data.insurance_companies_id);
                $('#product_id'+key).val(data.product_id);
                $('#agent_id'+key).val(data.agent_id);


                getOptionInstallmentAlgorithms("installment_algorithms_id"+key, data.insurance_companies_id, 0);
                getOptionFinancialPolicy("financial_policy_id"+key, data.insurance_companies_id, data.bso_supplier_id, data.product_id, 0);

                intkey = key.replaceAll('_', '');
                object_insurer = myGetAjax('{{url("/bso/actions/get_html_mini_contract_object_insurer/")}}?product_id='+data.product_id+'&key='+intkey);

                //object_insurer = object_insurer.replaceAll('[:KEY:]', parseInt(intkey));

                $("#contract_object_insurer"+key).html(object_insurer);

                startContractObjectInsurerFunctions(parseInt(intkey));

                activSearchBso("bso_receipt",key, 2);

                activSearchOrdersToFront("order_title",key);

            }

            if(parseInt(type) == 2){ // Квитанция
                $('#bso_receipt_id'+key).val(data.bso_id);

            }



        }

        function setPaymentQty(key) {
            $('#payment_total_'+key).val($('#payment_total_'+key).val());
        }

        function controlSaveButton() {

            $('.valid_fast_accept').click(function () {
                $(this).css("border-color","");
            });

            if(getContract().length > 0){
                $("#controlSaveButtonBox").show();
            }else{
                $("#controlSaveButtonBox").hide();
            }
        }


        function saveFastAccept() {
            if(validFastAccept() == true){
                $("#formContract").submit();
            }else{
                alert("Некорректно введены данные!");
            }
        }

        function validFastAccept() {


            var msg_arr = [];

            $('.valid_fast_accept').each(function() {
                val = ($(this).val())?$(this).val():$(this).html();
                if(val.length<1){
                    msg_arr.push('Заполните все поля');
                    $(this).css("border-color","red");
                }


            });



            if (msg_arr.length) {
                return false;
            }


            return true;
        }

        function get_end_dates(start_date) {
            var cur_date_tmp = start_date.split(".");
            var cur_date = new Date(cur_date_tmp[2], cur_date_tmp[1] - 1, cur_date_tmp[0]);
            var new_date = new Date(cur_date.setYear(cur_date.getFullYear() + 1));
            var new_date2 = new Date(new_date.setDate(new_date.getDate() - 1));
            return getFormattedDate(new_date2);
        }

        function setAllDates(key){
            sign_date = $("#sign_date_"+key).val();
            $("#begin_date_"+key).val(sign_date);

            return setEndDates(key);
        }

        function setEndDates(key){
            begin_date = $("#begin_date_"+key).val();
            $("#end_date_"+key).val(get_end_dates(begin_date));
        }


        function viewSetFinancialPolicyManually(obj, key)
        {
            if($(obj).is(':checked')){
                $("#financial_policy_manually_"+key).show();
                $("#financial_policy_id_"+key).hide();
            }else{
                $("#financial_policy_manually_"+key).hide();
                $("#financial_policy_id_"+key).show();
            }
        }

        function viewSetBsoNotReceipt(obj, key)
        {
            if($(obj).is(':checked')){
                $("#bso_receipt_"+key).attr('disabled','disabled');
                $("#bso_receipt_"+key).removeClass('valid_fast_accept');

            }else{
                $("#bso_receipt_"+key).removeAttr('disabled');
                $("#bso_receipt_"+key).addClass('valid_fast_accept');
            }

            $("#bso_receipt_"+key).css("border-color","");
        }


        function startContractObjectInsurerFunctions(KEY){
            $('#object_insurer_ts_mark_id_'+KEY).select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true
            });

            $('#object_insurer_ts_model_id_'+KEY).select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true
            });

        }

        function getModelsObjectInsurer(KEY, select_model_id)
        {

            $.getJSON('{{url("/contracts/actions/get_models")}}', {categoryId:$('#object_insurer_ts_category_'+KEY).val(),markId: $('#object_insurer_ts_mark_id_'+KEY).select2('val')}, function (response) {


                var options = "<option value='0'>Не выбрано</option>";
                response.map(function (item) {
                    options += "<option value='" + item.id + "'>" + item.title + "</option>";
                });
                $('#object_insurer_ts_model_id_'+KEY).html(options).select2('val', select_model_id);


            });

        }


    </script>


@endsection