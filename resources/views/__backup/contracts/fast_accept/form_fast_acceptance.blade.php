<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 temp_contract" id="contract_[:KEY:]">
    <div class="trucks-item order-item">
        <div class="info">
            Договор <span class="number">[:NUM:]</span>

            <button onclick="deleteContract('[:KEY:]')" style="background-color: #FFF;font-size: 15px;margin-top: -5px;border: none;" class="pull-right view_but" type="button">
                <i class="fa fa-remove" style="color: red;"></i>
            </button>

        </div>
        <div class="divider"></div>
        <div class="info">

            <div class="form-group">


                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" >
                    <h3>Договор (полис)</h3>
                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label>Номер договора</label>
                        {{ Form::text('contract[[:KEY:]][bso_title]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'bso_title_[:KEY:]']) }}
                        <input type="hidden" name="contract[[:KEY:]][bso_id]" id="bso_id_[:KEY:]" />
                        <input type="hidden" name="contract[[:KEY:]][bso_supplier_id]" id="bso_supplier_id_[:KEY:]" />
                        <input type="hidden" name="contract[[:KEY:]][insurance_companies_id]" id="insurance_companies_id_[:KEY:]" />
                        <input type="hidden" name="contract[[:KEY:]][product_id]" id="product_id_[:KEY:]" />
                        <input type="hidden" name="contract[[:KEY:]][agent_id]" id="agent_id_[:KEY:]" />
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label>Заявка из фронт офиса</label>
                        {{ Form::text('contract[[:KEY:]][order_title]', '', ['class' => 'form-control', 'id'=>'order_title_[:KEY:]']) }}
                        <input type="hidden" name="contract[[:KEY:]][order_id]" id="order_id_[:KEY:]" />
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label>Дата заключения</label>
                        {{ Form::text('contract[[:KEY:]][sign_date]', '', ['class' => 'form-control datepicker valid_fast_accept', 'id'=>'sign_date_[:KEY:]', 'onchange'=>'setAllDates([:KEY:])']) }}
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label>Период действия</label>
                        <div class="row form-horizontal">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                {{ Form::text('contract[[:KEY:]][begin_date]', '', ['class' => 'form-control datepicker valid_fast_accept', 'id'=>'begin_date_[:KEY:]', 'placeholder'=>'Дата начала', 'onchange'=>'setEndDates([:KEY:])']) }}
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                {{ Form::text('contract[[:KEY:]][end_date]', '', ['class' => 'form-control datepicker valid_fast_accept', 'placeholder'=>'Дата окончания', 'id'=>'end_date_[:KEY:]']) }}
                            </div>
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label>Условие продажи</label>
                        {{ Form::select('contract[[:KEY:]][sales_condition]', collect(\App\Models\Contracts\Contracts::SALES_CONDITION), 1, ['class' => 'form-control', 'id'=>'sales_condition_[:KEY:]']) }}
                    </div>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label class="pull-left" style="margin-top: 5px;">
                            Менеджер
                        </label>
                        <label class="pull-right">Личная продажа <input type="checkbox" value="1" id="is_personal_sales_[:KEY:]" name="contract[[:KEY:]][is_personal_sales]"/> </label>
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right: 0px;" >
                            {{ Form::select('contract[[:KEY:]][manager_id]', $agents->prepend('Выберите значение', 0), 0, ['class' => 'form-control', 'id'=>'manager_id_[:KEY:]']) }}
                        </div>
                    </div>

                </div>

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" >
                    <h3>Порядок оплаты</h3>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label>Премия по договору</label>
                                {{ Form::text('contract[[:KEY:]][payment_total]', '', ['class' => 'form-control sum valid_fast_accept', 'id'=>'payment_total_[:KEY:]', 'onchange'=>'setPaymentQty([:KEY:])']) }}
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label>Дата оплаты</label>
                                {{ Form::text('contract[[:KEY:]][payment_data]', '', ['class' => 'form-control datepicker valid_fast_accept', 'id'=>'payment_data_[:KEY:]']) }}

                            </div>


                        </div>
                    </div>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label>Сумма платежа</label>
                                {{ Form::text('contract[[:KEY:]][payment_total]', '', ['class' => 'form-control sum valid_fast_accept', 'id'=>'payment_total_[:KEY:]']) }}
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <div class="row form-horizontal">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                        <label>Взнос №</label>
                                        {{ Form::text('contract[[:KEY:]][payment_number]', 1, ['class' => 'form-control valid_fast_accept', 'id'=>'payment_number_[:KEY:]']) }}
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                        <label>Тип</label>
                                        {{ Form::select('contract[[:KEY:]][type_id]', collect(\App\Models\Contracts\Payments::TRANSACTION_TYPE), 0, ['class' => 'form-control']) }}
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >

                        <div class="row form-horizontal">

                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                {{ Form::text('contract[[:KEY:]][official_discount]', '', ['class' => 'form-control sum', 'id'=>'official_discount_[:KEY:]', 'placeholder'=>'Офици. %']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                {{ Form::text('contract[[:KEY:]][informal_discount]', '', ['class' => 'form-control sum', 'id'=>'informal_discount_[:KEY:]', 'placeholder'=>'Неофиц. %']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                {{ Form::text('contract[[:KEY:]][bank_kv]', '', ['class' => 'form-control sum', 'id'=>'bank_kv_[:KEY:]', 'placeholder'=>'Банк %']) }}
                            </div>
                        </div>
                    </div>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                <label>Тип оплаты</label>
                                {{ Form::select('contract[[:KEY:]][payment_type]', collect(\App\Models\Contracts\Payments::PAYMENT_TYPE), 0, ['class' => 'form-control']) }}
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                <label>Поток оплаты</label>
                                {{ Form::select('contract[[:KEY:]][payment_flow]', collect(\App\Models\Contracts\Payments::PAYMENT_FLOW), 0, ['class' => 'form-control']) }}
                            </div>
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label class="pull-left" style="margin-top: 5px;">
                            Квитанция
                        </label>
                        <label class="pull-right">Без квитанции <input type="checkbox" value="1" name="contract[[:KEY:]][bso_not_receipt]" onchange="viewSetBsoNotReceipt(this, '[:KEY:]')"/> </label>
                        {{ Form::text('contract[[:KEY:]][bso_receipt]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'bso_receipt_[:KEY:]']) }}
                        <input type="hidden" name="contract[[:KEY:]][bso_receipt_id]" id="bso_receipt_id_[:KEY:]" />

                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label class="pull-left" style="margin-top: 5px;">
                            Финансовая политика
                        </label>
                        <label class="pull-right">Указать в ручную <input type="checkbox" value="1" name="contract[[:KEY:]][financial_policy_manually_set]" onchange="viewSetFinancialPolicyManually(this, '[:KEY:]')"/> </label>
                        {{ Form::select('contract[[:KEY:]][financial_policy_id]', collect([0=>'Укажите номер договора']), 0, ['class' => 'form-control', 'id'=>'financial_policy_id_[:KEY:]']) }}


                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" ></div>
                        <div class="row form-horizontal" id="financial_policy_manually_[:KEY:]" style="display: none;">

                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                {{ Form::text('contract[[:KEY:]][financial_policy_kv_bordereau]', '', ['class' => 'form-control sum', 'id'=>'financial_policy_kv_bordereau_[:KEY:]', 'placeholder'=>'Бордеро']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                {{ Form::text('contract[[:KEY:]][financial_policy_kv_dvoy]', '', ['class' => 'form-control sum', 'id'=>'financial_policy_kv_dvoy_[:KEY:]', 'placeholder'=>'ДВОУ']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                {{ Form::text('contract[[:KEY:]][financial_policy_kv_agent]', '', ['class' => 'form-control sum', 'id'=>'financial_policy_kv_agent_[:KEY:]', 'placeholder'=>'Агента']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                {{ Form::text('contract[[:KEY:]][financial_policy_kv_parent]', '', ['class' => 'form-control sum', 'id'=>'financial_policy_kv_parent_[:KEY:]', 'placeholder'=>'Руков.']) }}
                            </div>
                        </div>


                    </div>





                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label>Алгоритм рассрочки</label>
                        {{ Form::select('contract[[:KEY:]][installment_algorithms_id]', collect([0=>'Укажите номер договора']), 0, ['class' => 'form-control', 'id'=>'installment_algorithms_id_[:KEY:]']) }}
                    </div>



                </div>





                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-5" >
                    <h3>Cтрахователь</h3>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" >
                                <label>Тип</label>
                                {{ Form::select('contract[[:KEY:]][insurer][type]', collect([0=>"ФЛ", 1=>'ЮЛ']), 0, ['class' => 'form-control', 'id'=>'insurer_type_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                            <div class="insurer_fl col-xs-12 col-sm-12 col-md-9 col-lg-9" >
                                <label>ФИО</label>
                                {{ Form::text('contract[[:KEY:]][insurer][fio]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_fio_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                            <div class="insurer_ul col-xs-12 col-sm-12 col-md-9 col-lg-9" style="display: none">
                                <label>Название</label>
                                {{ Form::text('contract[[:KEY:]][insurer][title]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_title_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                        </div>
                    </div>

                    <div class="row insurer_fl col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label>Паспорт</label>
                        <div class="row form-horizontal">
                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" >
                                {{ Form::text('contract[[:KEY:]][insurer][doc_serie]', '', ['class' => 'form-control', 'placeholder'=>'Серия']) }}
                            </div>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" >
                                {{ Form::text('contract[[:KEY:]][insurer][doc_number]', '', ['class' => 'form-control', 'placeholder'=>'Номер']) }}
                            </div>
                        </div>
                    </div>

                    <div class="row insurer_ul col-xs-12 col-sm-12 col-md-12 col-lg-12"  style="display: none">
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label>ИНН</label>
                                {{ Form::text('contract[[:KEY:]][insurer][inn]', '', ['class' => 'form-control', 'id'=>'insurer_inn_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label>КПП</label>
                                {{ Form::text('contract[[:KEY:]][insurer][kpp]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                        </div>
                    </div>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label>Телефон</label>
                                {{ Form::text('contract[[:KEY:]][insurer][phone]', '', ['class' => 'form-control phone', 'id'=>'insurer_phone_[:KEY:]']) }}
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label>Email</label>
                                {{ Form::text('contract[[:KEY:]][insurer][email]', '', ['class' => 'form-control', 'id'=>'insurer_email_[:KEY:]']) }}
                            </div>

                        </div>
                    </div>



                    <h3>Объект страхования</h3>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="contract_object_insurer_[:KEY:]">

                        <center><strong>Укажите номер договора</strong></center>

                    </div>


                </div>




            </div>


        </div>
    </div>
</div>