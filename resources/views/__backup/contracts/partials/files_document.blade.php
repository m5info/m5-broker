<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >

    @if(sizeof($hold_kv_product->documents))

        {{ Form::open(['url' => url("/contracts/actions/contract/{$contract->id}/document"), 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) }}

        <table id="tab_files" class="dataTable">
            <tr>
                <td><center><strong>Документ</strong></center></td>
                <td><center><strong>-</strong></center></td>
            </tr>
            @foreach($hold_kv_product->documents as $key => $document)
                <tr>
                    <td>{{$document->file_title}}
                        @if($contract->document($document->id))
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <a href="{{ url($contract->document($document->id)->file->url) }}" target="_blank">{{$contract->document($document->id)->file->original_name}}</a>
                                </div>
                            </div>
                        @endif
                    </td>
                    <td>
                        @if($contract->statys_id == 0)
                        <input type="file" name="file[{{$document->id}}]"/>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
        <br/>
        @if($contract->statys_id == 0)
        <input type="submit" class="btn btn-success" value="Сохранить документы"/>
        @endif
        {{Form::close()}}





    @endif


</div>