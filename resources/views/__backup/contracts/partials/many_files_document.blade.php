<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <span class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="color: red;">{{$hold_kv_product->many_text}}</span>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @if($contract->scans->count())
            <table class="table orderStatusTable dataTable no-footer">
                <thead>
                <tr>
                    <th>{{ trans('users/users.edit.title') }}</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                @foreach($contract->scans as $file)
                    <tr>
                        <td>{{ $file->original_name }}</td>
                        <td>
                            <a href="{{ url($file->url) }}" class="btn btn-primary" target="_blank">
                                {{ trans('form.buttons.download') }}
                            </a>
                        </td>
                        <td>
                            <button class="btn btn-danger" type="button" onclick="removeFile('{{ $file->name }}')">
                                {{ trans('form.buttons.delete') }}
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <h3>{{ trans('form.empty') }}</h3>
        @endif
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        {!! Form::open(['url'=>"/contracts/actions/contract/$contract->id/scans",'method' => 'post', 'class' => 'dropzone', 'id' => 'addManyDocForm']) !!}
        {!! Form::close() !!}
    </div>
</div>


<script>

    function initManyDocuments() {
        $("#addManyDocForm").dropzone({
            //Dropzone.options.addOrgDocForm = {
            paramName: 'file',
            maxFilesize: 10,
            //acceptedFiles: "image/*",
            init: function () {
                this.on("complete", function () {
                    if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                        reload();
                    }

                });
            }
        });

    }



</script>
