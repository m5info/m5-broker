@if(sizeof($contracts))
    <table class="dataTable">
        <tr>
            <th>#</th>
            <th>Статус</th>
            <th>СК</th>
            <th>БСО</th>
            <th>Продукт</th>
            <th>Страхователь</th>
            <th>Дата заключения</th>
            <th>Период</th>
            <th>Агент</th>
            <th></th>
            <th></th>
        </tr>

        <tbody>

        @foreach($contracts as $contract)
            <tr>
                <td>{{$contract->id}}</td>
                <td>{{\App\Models\Contracts\Contracts::STATYS[$contract->statys_id]}}</td>
                <td>{{$contract->insurance_companies->title}}</td>
                <td>{{$contract->bso->bso_title}}</td>
                <td>{{$contract->bso->product->title}}</td>
                <td>{{($contract->insurer)?$contract->insurer->title:''}}</td>
                <td>{{setDateTimeFormatRu($contract->sign_date, 1)}}</td>
                <td>{{setDateTimeFormatRu($contract->begin_date, 1)}} - {{setDateTimeFormatRu($contract->end_date, 1)}}</td>
                <td>{{$contract->agent->name}}</td>
                <td><span style="color: red;">{{$contract->error_accept}}</span></td>
                <td>
                    <a class="btn btn-success" href="{{url("/contracts/temp_contracts/contract/{$contract->id}/edit")}}">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>

            </tr>
        @endforeach

        </tbody>
    </table>
@endif