

@if(isset($hold_kv_product) && (int)$hold_kv_product->is_many_files == 1)

    @include('contracts.partials.many_files_document', ['contract' => $contract, 'hold_kv_product'=>$hold_kv_product])

    <br/>
@endif


@include('contracts.partials.files_document', ['contract' => $contract, 'hold_kv_product'=>$hold_kv_product])


<script>



    function initDocuments() {

        @if(isset($hold_kv_product) && (int)$hold_kv_product->is_many_files == 1)

        initManyDocuments();

        @endif

    }



</script>