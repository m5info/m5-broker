@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">
@append

@section('content')


    <div class="page-heading">
        <h2>Проверка/Подтверждение

            <div class="pull-right">
                <span class="btn btn-primary" onclick="openFancyBoxFrame('{{url("/contracts/temp_contracts/add/electronic/")}}')">
                    <i class="fa fa-plus"></i> E-Полис
                </span>

                <span class="btn btn-primary" onclick="openFancyBoxFrame('{{url("/contracts/temp_contracts/add/paper/")}}')">
                    <i class="fa fa-plus"></i> Бумажный договор
                </span>

                <span class="btn btn-success" onclick="openFancyBoxFrame('{{url("/contracts/temp_contracts/add/payment/")}}')">
                    <i class="fa fa-plus-circle"></i>  Второй взнос
                </span>
            </div>

        </h2>
    </div>

    <div class="header_bab" >


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
            <div id="tt" class="easyui-tabs" data-options="tools:'#tab-tools'">
                @foreach($count_arr as $key => $count)

                    <div title="{{$count['title']}} {{($count['count']>0)?$count['count']:''}}"  id="tab-{{$key}}" data-view="{{$key}}"></div>

                @endforeach
            </div>
        </div>
    </div>


    <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container" style="margin-top: -5px;overflow: auto;">
    </div>






@endsection

@section('js')

    <script>

        var map;
        var TAB_INDEX = 0;

        $(function () {


            $('#tt').tabs({
                border:false,
                pill: false,
                plain: true,
                onSelect: function(title, index){
                    return selectTab(index);
                }
            });


            selectTab(0);



        });



        function selectTab(id) {
            var tab = $('#tt').tabs('getSelected');
            load = tab.data('view');//$("#tab-"+id).data('view');
            TAB_INDEX = id;

            loaderShow();

            $.get("{{url("/contracts/temp_contracts/get_table")}}", {statys:load}, function (response) {
                loaderHide();
                $("#main_container").html(response);

                initTab();

            })  .done(function() {
                loaderShow();
            })
                .fail(function() {
                    loaderHide();
                })
                .always(function() {
                    loaderHide();
                });

        }

        //



    </script>


@endsection