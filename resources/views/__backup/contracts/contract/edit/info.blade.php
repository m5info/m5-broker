<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-subheading">
        <h2>Условия договора</h2>
    </div>
    <div class="block-main">
        <div class="block-sub">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label>Номер договора</label>
                    {{ Form::text('contract[0][bso_title]', $contract->bso->bso_title, ['class' => 'form-control', 'id'=>'bso_title_0' , 'disabled']) }}
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label>Заявка из фронт офиса</label>
                    <input class="form-control" id="order_title_0" name="contract[0][order_title]" type="text" value="{{$contract->order_title}}" >
                    <input type="hidden" name="contract[0][order_id]" id="order_id_0" value="{{$contract->order_id}}">
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label>Дата заключения</label>
                    <input class="form-control datepicker valid_fast_accept" id="sign_date_0" onchange="setAllDates(0)" name="contract[0][sign_date]" type="text" value="{{setDateTimeFormatRu($contract->sign_date, 1)}}">
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label>Период действия</label>
                    <div class="row form-horizontal">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <input class="form-control datepicker valid_fast_accept" id="begin_date_0" placeholder="Дата начала" onchange="setEndDates(0)" name="contract[0][begin_date]" type="text" value="{{setDateTimeFormatRu($contract->begin_date, 1)}}">
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <input class="form-control datepicker valid_fast_accept" placeholder="Дата окончания" id="end_date_0" name="contract[0][end_date]" type="text" value="{{setDateTimeFormatRu($contract->end_date, 1)}}">
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label>Условие продажи</label>
                    {{ Form::select('contract[0][sales_condition]', collect(\App\Models\Contracts\Contracts::SALES_CONDITION), $contract->sales_condition, ['class' => 'form-control', 'id'=>'sales_condition_0', "onchange" =>"viewControllerSalesCondition()"]) }}
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sales_condition_not_agent">
                    <label class="pull-left" style="margin-top: 5px;">
                        Менеджер/Агент
                    </label>
                    <label class="pull-right">Личная продажа <input type="checkbox" value="1" id="is_personal_sales_0" name="contract[0][is_personal_sales]" @if($contract->is_personal_sales == 1) checked @endif/> </label>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ></div>
                    {{ Form::select('contract[0][manager_id]', $agents->prepend('Выберите значение', 0), $contract->manager_id, ['class' => 'form-control select2', 'id'=>'manager_id_0']) }}
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sales_condition_not_agent">
                    <br/><br/>
                </div>



                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <label>Премия по договору</label>
                    {{ Form::text('contract[0][payment_total]', ($contract->payment_total>0)?titleFloatFormat($contract->payment_total):'', ['class' => 'form-control sum', 'id'=>'payment_total_0', 'onchange'=>'setPaymentQty(0)']) }}
                </div>


                @if(auth()->user()->hasPermission('contracts', 'select_financial_policy'))
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label class="pull-left" style="margin-top: 5px;">
                            Финансовая политика
                        </label>
                        @if(auth()->user()->hasPermission('contracts', 'set_financial_policy_manually'))
                            <label class="pull-right">Указать в ручную <input type="checkbox" value="1" @if($contract->financial_policy_manually_set==1) checked @endif name="contract[0][financial_policy_manually_set]" id="financial_policy_manually_set_0" onchange="viewSetFinancialPolicyManually(this, '0')"/> </label>
                        @endif
                        {{ Form::select('contract[0][financial_policy_id]', collect([0=>'Укажите номер договора']), 0, ['class' => 'form-control', 'id'=>'financial_policy_id_0']) }}

                        @if(auth()->user()->hasPermission('contracts', 'set_financial_policy_manually'))
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ></div>
                            <div id="financial_policy_manually_0" style="display: none;">

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    {{ Form::text('contract[0][financial_policy_kv_bordereau]', $contract->financial_policy_kv_bordereau, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_bordereau_0', 'placeholder'=>'Бордеро']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    {{ Form::text('contract[0][financial_policy_kv_dvoy]', $contract->financial_policy_kv_dvoy, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_dvoy_0', 'placeholder'=>'ДВОУ']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    {{ Form::text('contract[0][financial_policy_kv_agent]', $contract->financial_policy_kv_agent, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_agent_0', 'placeholder'=>'Агента']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    {{ Form::text('contract[0][financial_policy_kv_parent]', $contract->financial_policy_kv_parent, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_parent_0', 'placeholder'=>'Руков.']) }}
                                </div>
                            </div>
                        @endif

                    </div>
                @endif

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <label>Алгоритм рассрочки</label>
                    {{ Form::select('contract[0][installment_algorithms_id]', collect([0=>'Укажите номер договора']), 0, ['class' => 'form-control', 'id'=>'installment_algorithms_id_0']) }}
                </div>

            </div>
        </div>
    </div>
</div>

<script>

    function initInfo()
    {
        viewControllerSalesCondition();

        getOptionInstallmentAlgorithms("installment_algorithms_id_0", '{{$contract->insurance_companies_id}}', '{{$contract->installment_algorithms_id}}');
        @if(auth()->user()->hasPermission('contracts', 'select_financial_policy'))
            getOptionFinancialPolicy("financial_policy_id_0", '{{$contract->insurance_companies_id}}', '{{$contract->bso_supplier_id}}', '{{$contract->product_id}}', '{{$contract->financial_policy_id}}');
        @endif

        @if(auth()->user()->hasPermission('contracts', 'set_financial_policy_manually'))
            viewSetFinancialPolicyManually($("#financial_policy_manually_set_0"), '0');
        @endif

        activSearchOrdersToFront("order_title", "_0");

    }


    function saveContract() {

        $.ajax({
            type: "POST",
            url: "{{url("/contracts/temp_contracts/contract/{$contract->id}/edit")}}",
            async:false,
            data: $("#main_container").find("select, textarea, input").serialize(),
            success: function (response) {

                if(response.sate == 0){
                    //reload();
                }
            }
        });

        return true;
    }

    function get_end_dates(start_date) {
        var cur_date_tmp = start_date.split(".");
        var cur_date = new Date(cur_date_tmp[2], cur_date_tmp[1] - 1, cur_date_tmp[0]);
        var new_date = new Date(cur_date.setYear(cur_date.getFullYear() + 1));
        var new_date2 = new Date(new_date.setDate(new_date.getDate() - 1));
        return getFormattedDate(new_date2);
    }

    function setAllDates(key){
        sign_date = $("#sign_date_"+key).val();
        $("#begin_date_"+key).val(sign_date);

        return setEndDates(key);
    }

    function setEndDates(key){
        begin_date = $("#begin_date_"+key).val();
        $("#end_date_"+key).val(get_end_dates(begin_date));
    }


    function viewControllerSalesCondition()
    {
        if($("#sales_condition_0").val() != 0)
        {
            $(".sales_condition_not_agent").show();
        }else
        {
            $(".sales_condition_not_agent").hide();
        }

    }

    function viewSetFinancialPolicyManually(obj, key)
    {
        if($(obj).is(':checked')){
            $("#financial_policy_manually_"+key).show();
            $("#financial_policy_id_"+key).hide();
        }else{
            $("#financial_policy_manually_"+key).hide();
            $("#financial_policy_id_"+key).show();
        }
    }

</script>