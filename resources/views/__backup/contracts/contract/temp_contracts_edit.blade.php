@extends('layouts.app')

@section('content')


    <div class="page-heading">
        <h2>Временный договор # {{$contract->id}}

            @if($contract->statys_id == 0)
                <span class="btn btn-primary" id="controlSaveButtonBox" onclick="saveContract()">
                    Сохранить
                </span>

                <span class="btn btn-success pull-right" onclick="sendCheckContract()">
                    В проверку
                </span>
            @endif

        </h2>




    </div>


    <div class="row form-horizontal" id="main_container">

        @if($contract->statys_id == 1 && auth()->user()->hasPermission('contracts', 'verification'))
        <!-- Техандерайтинг -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="page-subheading">
                <h2>Техандерайтинг</h2>
            </div>
            <div class="block-main">
                <div class="block-sub">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label>Комментарий</label>
                            {{ Form::textarea('contract[0][error_accept]', $contract->error_accept, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <span class="btn btn-danger pull-left" onclick="errorContract()">Выставить ошибку</span>
                            <span class="btn btn-success pull-right" onclick="acceptContract()">Акцепт</span>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endif

        @if($contract->statys_id == 2)
            <!-- Техандерайтинг -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="page-subheading">
                        <h2>Техандерайтинг</h2>
                    </div>
                    <div class="block-main">
                        <div class="block-sub">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <label>Комментарий ({{$contract->check_user->name}})</label>
                                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" style="color: red;">
                                    {{$contract->error_accept}}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <span class="btn btn-success pull-left" onclick="sendCheckContract()">Исправил</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        @endif



        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">



            <!-- Условия договора -->
            @include('contracts.contract.edit.info', ['contract' => $contract])

            <!-- Платежи -->
            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="page-subheading">
                    <h2>Платежи</h2>
                    @if($contract->statys_id == 0)
                        <span class="btn btn-success pull-right" style="font-size: 10px;" onclick="addTempPayment()">
                            <i class="fa fa-plus"></i>
                        </span>
                    @endif
                </div>
                <div class="block-main">
                    <div class="block-sub">
                        <div class="row">
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                @if(sizeof($contract->temp_payments))

                                    @foreach($contract->temp_payments as $key => $payment)

                                        @include('contracts.partials.temp_payments', ['contract' => $contract, 'payment'=>$payment, 'key'=>$key])
                                    @endforeach
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>




        </div>


        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">


            <!-- Участники договора -->
            @include('contracts.contract.edit.insurer_info', ['contract' => $contract])


        </div>

    </div>




@endsection

@section('js')

    <script>


        $(function () {


            initInfo();
            initInsurer();


        });







        function sendCheckContract()
        {
            if(saveContract() == true) {
                if (myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/send_check/")}}")) {
                    reload();
                }
            }
        }

        @if($contract->statys_id == 1 && auth()->user()->hasPermission('contracts', 'verification'))


            function errorContract()
            {
                if(saveContract() == true){
                    if(myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/error_contract/")}}")){
                        reload();
                    }
                }

            }

        @endif


        function addTempPayment()
        {
            if(saveContract() == true){
                if(myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/add_pay/")}}")){
                    reload();
                }
            }

        }

        function deleteTempPayment(pay)
        {
            if(saveContract() == true) {
                if (myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/del_pay/?pay=")}}" + pay)) {
                    reload();
                }
            }
        }







        function viewSetBsoNotReceipt(obj, key)
        {

            if($(obj).is(':checked')){

                $("#bso_receipt_"+key).attr('disabled','disabled');
                $("#bso_receipt_"+key).removeClass('valid_fast_accept');

            }else{
                $("#bso_receipt_"+key).removeAttr('disabled');
                $("#bso_receipt_"+key).addClass('valid_fast_accept');
            }

            $("#bso_receipt_"+key).css("border-color","");
        }

    </script>


@endsection