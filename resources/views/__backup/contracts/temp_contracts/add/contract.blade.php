@extends('layouts.frame')


@section('title')

    {{ $title }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/contracts/temp_contracts/add/contract/'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    <div class="form-group">
        <label class="col-sm-4 control-label">Договор (полис)</label>
        <div class="col-sm-8">
            {{ Form::text('bso_title', '', ['class' => 'form-control', 'id'=>'bso_title']) }}
            <input type="hidden" name="bso_id" id="bso_id" />
            <input type="hidden" name="bso_supplier_id" id="bso_supplier_id" />
            <input type="hidden" name="insurance_companies_id" id="insurance_companies_id" />
            <input type="hidden" name="product_id" id="product_id" />
            <input type="hidden" name="agent_id" id="agent_id" />
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">СК</label>
        <div class="col-sm-8">
            {{ Form::text('sk_title', '', ['class' => 'form-control', 'id'=>'sk_title', 'disabled']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Агент</label>
        <div class="col-sm-8">
            {{ Form::text('agent_title', '', ['class' => 'form-control', 'id'=>'agent_title', 'disabled']) }}
        </div>
    </div>

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection




@section('js')

    <script>




        $(function () {

            activSearchBso("bso_title", '', 1);

        });


        function selectBso(object_id, key, type, suggestion)
        {

            var data = suggestion.data;

            if(parseInt(type) == 1){ // БСО
                $('#bso_id'+key).val(data.bso_id);
                $('#bso_supplier_id'+key).val(data.bso_supplier_id);
                $('#insurance_companies_id'+key).val(data.insurance_companies_id);
                $('#product_id'+key).val(data.product_id);
                $('#agent_id'+key).val(data.agent_id);


                $('#sk_title').val(data.bso_sk);
                $('#agent_title').val(data.agent_name);


            }





        }




    </script>


@endsection

