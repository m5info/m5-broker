@extends('layouts.app')

@section('content')



        <div class="page-heading">
            <h1>{{ trans('menu.salaries_states') }}</h1>
            <a class="btn btn-primary fancybox fancybox.iframe" href="{{ url('/settings/salaries_states/create')  }}">
                {{ trans('form.buttons.create') }}
            </a>
        </div>


        <div class="block-main">
        @if(sizeof($states))
            <table class="noScrollTable">
                <thead>
                <tr>
                    <th>{{ trans('settings/salaries_states.title') }}</th>
                    <th>{{ trans('settings/salaries_states.prefix') }}</th>
                </tr>
                </thead>
                @foreach($states as $state)
                    <tr class="clickable-row fancybox fancybox.iframe" href="{{ url("/settings/salaries_states/$state->id/edit") }}">
                        <td>{{ $state->title }}</td>
                        <td>{{ $state->prefix }}</td>
                    </tr>
                @endforeach
            </table>
        @else
            {{ trans('form.empty') }}
        @endif
        </div>


@endsection

