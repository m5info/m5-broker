@extends('layouts.frame')


@section('title')

    {{ trans('menu.salaries_states') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/settings/salaries_states'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('settings.salaries_states.form')

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection