@extends('layouts.app')

@section('content')



        <div class="page-heading">
            <h1>{{ trans('menu.departments') }}</h1>
            <a class="btn btn-primary fancybox fancybox.iframe" href="{{ url('/settings/departments/create')  }}">
                {{ trans('form.buttons.create') }}
            </a>
        </div>


        <div class="block-main">
        @if(sizeof($departments))
            <table class="noScrollTable">
                <thead>
                    <th>{{ trans('settings/departments.title') }}</th>
                    <th>Тип</th>
                    <th>Роль</th>
                </tr>
                </thead>
                @foreach($departments as $department)
                    <tr class="clickable-row fancybox fancybox.iframe" href="{{url ("/settings/departments/$department->id/edit")}}" >
                        <td>{{ $department->title }}</td>
                        <td>{{ ($department->type_org) ?$department->type_org->title:''}}</td>
                        <td>{{ ($department->role)?$department->role->title:'' }}</td>

                    </tr>
                @endforeach

            </table>
        @else
            {{ trans('form.empty') }}
        @endif
        </div>

@endsection



