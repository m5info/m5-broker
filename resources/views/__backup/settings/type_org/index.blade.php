@extends('layouts.app')

@section('content')



        <div class="page-heading">
            <h1>{{ trans('menu.type_org') }}</h1>
            <a class="btn btn-primary fancybox fancybox.iframe" href="{{ url('/settings/type_org/create')  }}">
                {{ trans('form.buttons.create') }}
            </a>
        </div>



        <div class="block-main">
        @if(sizeof($type_orgs))
            <table class="noScrollTable">
                <thead>
                <tr>
                    <th>{{ trans('settings/banks.title') }}</th>
                    <th>{{ trans('settings/banks.is_actual') }}</th>
                    <th>Поставщик БСО</th>
                </tr>
                </thead>
                @foreach($type_orgs as $type_org)
                    <tr class="clickable-row fancybox fancybox.iframe" href="{{url ("/settings/type_org/$type_org->id/edit")}}">
                        <td>{{ $type_org->title }}</td>
                        <td>{{ ($type_org->is_actual==1)? trans('form.yes') :trans('form.no') }}</td>
                        <td>{{ ($type_org->is_provider==1)? trans('form.yes') :trans('form.no') }}</td>
                    </tr>
                @endforeach

            </table>
        @else
            {{ trans('form.empty') }}
        @endif
        </div>


@endsection

