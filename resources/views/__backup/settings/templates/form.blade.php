<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/templates.title') }}</label>
    <div class="col-sm-8">
        {{ Form::text('title', old('title'), ['class' => 'form-control', 'required']) }}
    </div>
</div>

@if(!empty($template))
    @if($template->file)
        <div class="form-group">
            <label class="col-sm-4 control-label">{{ trans('settings/templates.file') }}</label>
            <div class="col-sm-8">
                <a href="{{ $template->file->getUrlAttribute() }}" target="_blank">{{ $template->file->original_name }}</a>
            </div>
        </div>
    @endif
@endif

<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/templates.new_file') }}</label>
    <div class="col-sm-8">
        {{ Form::file('file', ['class' => 'file-input']) }}
    </div>
</div>


@if(!empty($template))
    <div class="form-group">
        <label class="col-sm-4 control-label">Тестовая выгрузка</label>
        <div class="col-sm-8">
            <a href="{{ url("/settings/templates/{$template->id}/test-download/") }}">Скачать</a>
        </div>
    </div>
@endif
