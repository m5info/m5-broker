@extends('layouts.app')

@section('content')

    <div class="page-heading">
            <h1>{{ trans('menu.templates') }}</h1>
            <a class="btn btn-primary fancybox fancybox.iframe" href="{{ url('/settings/templates/create')  }}">
                {{ trans('form.buttons.create') }}
            </a>
        </div>


    <div class="block-main">
        @if(sizeof($templates))
            <table class="noScrollTable">
                <thead>
                <tr>
                    <th>{{ trans('settings/templates.title') }}</th>
                    <th>{{ trans('settings/templates.file') }}</th>
                </tr>
                </thead>
                @foreach($templates as $template)
                    <tr class="clickable-row fancybox fancybox.iframe" href="{{ url("/settings/templates/$template->id/edit") }}">
                        <td>{{ $template->title }}</td>
                        <td>
                            @if($template->file)
                               {{ $template->file->original_name }}
                            @else
                                Не загружен
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            {{ trans('form.empty') }}
        @endif
        </div>


@endsection


@section('js')
    <script>
        function deleteItem(id) {
            if (!customConfirm()) return false;
            $.post('{{ url('/settings/templates') }}/' + id, {
                _method: 'delete'
            }, function () {
                window.location.reload();
            });
        }
    </script>

@endsection



