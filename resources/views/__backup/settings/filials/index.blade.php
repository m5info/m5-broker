@extends('layouts.app')

@section('content')



        <div class="page-heading">
            <h1>{{ trans('menu.filials') }}</h1>
            <a class="btn btn-primary fancybox fancybox.iframe" href="{{ url('/settings/filials/create')  }}">
                {{ trans('form.buttons.create') }}
            </a>
        </div>


        <div class="block-main">
        @if(sizeof($filials))
            <table class="noScrollTable">
                <thead>
                    <th>{{ trans('settings/departments.title') }}</th>

                </tr>
                </thead>
                @foreach($filials as $filial)
                    <tr class="clickable-row fancybox fancybox.iframe" href="{{url ("/settings/filials/$filial->id/edit")}}" >
                        <td>{{ $filial->title }}</td>
                    </tr>
                @endforeach

            </table>
        @else
            {{ trans('form.empty') }}
        @endif
        </div>

@endsection



