@extends('layouts.app')

@section('content')



        <div class="page-heading">
            <h1>{{ trans('menu.banks') }}</h1>
            <a class="btn btn-primary fancybox fancybox.iframe" href="{{ url('/settings/banks/create')  }}">
                {{ trans('form.buttons.create') }}
            </a>
        </div>



        <div class="block-main">
        @if(sizeof($banks))
            <table class="noScrollTable">
                <thead>
                <tr>
                    <th>{{ trans('settings/banks.title') }}</th>
                    <th>{{ trans('settings/banks.is_actual') }}</th>

                </tr>
                </thead>
                @foreach($banks as $bank)
                    <tr class="clickable-row fancybox fancybox.iframe" href="{{url ("/settings/banks/$bank->id/edit")}}">
                        <td>{{ $bank->title }}</td>
                        <td>{{ ($bank->is_actual==1)? trans('form.yes') :trans('form.no') }}</td>
                    </tr>
                @endforeach

            </table>
        @else
            {{ trans('form.empty') }}
        @endif
        </div>


@endsection

