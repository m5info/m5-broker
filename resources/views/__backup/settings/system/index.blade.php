@extends('layouts.app')

@section('content')



        <div class="page-heading">
            <h1>{{ trans('menu.system') }}</h1>

        </div>

        {{ Form::open(['url' => url('/settings/system'), 'method' => 'post', "autocomplete" =>"off", 'files' => true]) }}




        <div class="block-main">
            <div class="block-sub">
                <div class="form-horizontal">

                    <h4>Базовые настройки</h4>
                    <br/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Название</label>
                        <div class="col-sm-9">
                            {{ Form::text('base[system_name]', \App\Models\Settings\SettingsSystem::getDataParam('base', 'system_name'), ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Техподдержка</label>
                        <div class="col-sm-9">
                            <div class="row form-horizontal">
                                <div class="col-sm-6">
                                    {{ Form::text('base[phone]', \App\Models\Settings\SettingsSystem::getDataParam('base', 'phone'), ['class' => 'form-control phone', 'placeholder'=>'Телефо']) }}
                                </div>
                                <div class="col-sm-6">
                                    {{ Form::text('base[email]', \App\Models\Settings\SettingsSystem::getDataParam('base', 'email'), ['class' => 'form-control', 'placeholder'=>'Email']) }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="divider"></div>


                    <h4>Интеграция: Фронт-офис</h4>
                    <br/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Протокол</label>
                        <div class="col-sm-9">
                            {{ Form::select('front[protocol]', collect(\App\Models\Settings\SettingsSystem::FRONT_PROTOCOL)->prepend('Не выбрано', 0), (int)(\App\Models\Settings\SettingsSystem::getDataParam('front', 'protocol')), ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">URL</label>
                        <div class="col-sm-9">
                            {{ Form::text('front[url]', \App\Models\Settings\SettingsSystem::getDataParam('front', 'url'), ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Авторизация</label>
                        <div class="col-sm-9">
                            <div class="row form-horizontal">
                                <div class="col-sm-6">
                                    {{ Form::text('front[login]', \App\Models\Settings\SettingsSystem::getDataParam('front', 'login'), ['class' => 'form-control', 'placeholder'=>'Логин']) }}
                                </div>
                                <div class="col-sm-6">
                                    <input name="front[pass]" class="form-control" type="password" value="{{\App\Models\Settings\SettingsSystem::getDataParam('front', 'pass')}}" placeholder="Пароль">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Token/Ключ</label>
                        <div class="col-sm-9">
                            {{ Form::textarea('front[token]', \App\Models\Settings\SettingsSystem::getDataParam('front', 'token'), ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="divider"></div>

                    <br/>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary pull-right">
                                Сохранить
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        {{ Form::close() }}

@endsection

