@extends('layouts.app')

@section('content')



        <div class="page-heading">
            <h1>{{ trans('menu.citys') }}</h1>
            <span class="btn btn-primary" onclick="openFancyBoxFrame('{{ url('/settings/citys/create')  }}')">
                {{ trans('form.buttons.create') }}
            </span>
        </div>



        <div class="block-main">
        @if(sizeof($citys))
            <table class="noScrollTable">
                <thead>
                <tr>
                    <th>{{ trans('settings/banks.title') }}</th>
                    <th>{{ trans('settings/banks.is_actual') }}</th>

                </tr>
                </thead>
                @foreach($citys as $city)
                    <tr class="clickable-row fancybox fancybox.iframe" href="{{url ("/settings/citys/$city->id/edit")}}">
                        <td>{{ $city->title }}</td>
                        <td>{{ ($city->is_actual==1)? trans('form.yes') :trans('form.no') }}</td>
                    </tr>
                @endforeach

            </table>
        @else
            {{ trans('form.empty') }}
        @endif
        </div>


@endsection

