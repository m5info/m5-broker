@extends('layouts.app')

@section('content')



        <div class="page-heading">
            <h1>{{ trans('menu.points_sale') }}</h1>
            <a class="btn btn-primary fancybox fancybox.iframe" href="{{ url('/settings/points_sale/create')  }}">
                {{ trans('form.buttons.create') }}
            </a>
        </div>



        <div class="block-main">
        @if(sizeof($points))
            <table class="noScrollTable">
                <thead>
                <tr>
                    <th>{{ trans('settings/banks.title') }}</th>
                    <th>{{ trans('settings/banks.is_actual') }}</th>
                    <th>Город</th>

                </tr>
                </thead>
                @foreach($points as $point)
                    <tr class="clickable-row fancybox fancybox.iframe" href="{{url ("/settings/points_sale/$point->id/edit")}}">
                        <td>{{ $point->title }}</td>
                        <td>{{ ($point->is_actual==1)? trans('form.yes') :trans('form.no') }}</td>
                        <td>{{ ($point->city)?$point->city->title:'' }}</td>
                    </tr>
                @endforeach

            </table>
        @else
            {{ trans('form.empty') }}
        @endif
        </div>


@endsection

