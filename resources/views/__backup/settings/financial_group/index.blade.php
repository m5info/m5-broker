@extends('layouts.app')

@section('content')



        <div class="page-heading">
            <h1>{{ trans('menu.financial_policy') }}</h1>
            <a class="btn btn-primary fancybox fancybox.iframe" href="{{ url('/settings/financial_policy/create')  }}">
                {{ trans('form.buttons.create') }}
            </a>
        </div>



        <div class="block-main">
        @if(sizeof($financial_groups))
            <table class="noScrollTable">
                <thead>
                <tr>
                    <th>{{ trans('settings/banks.title') }}</th>
                    <th>{{ trans('settings/banks.is_actual') }}</th>

                </tr>
                </thead>
                @foreach($financial_groups as $financial_group)
                    <tr class="clickable-row fancybox fancybox.iframe" href="{{url ("/settings/financial_policy/$financial_group->id/edit")}}">
                        <td>{{ $financial_group->title }}</td>
                        <td>{{ ($financial_group->is_actual==1)? trans('form.yes') :trans('form.no') }}</td>
                    </tr>
                @endforeach

            </table>
        @else
            {{ trans('form.empty') }}
        @endif
        </div>


@endsection

