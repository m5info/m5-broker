@extends('layouts.app')

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h2>Прием передача БСО</h2>

            <button class="btn btn-danger pull-right" id="transfer_delete">Отменить передачу</button>

        </div>
        <div class="block-main">
            <div class="block-sub">
                <div class="form-horizontal">
                    {{ Form::hidden('transfer_id', $transfer->id) }}

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Тип</label>
                        <div class="col-sm-3">
                             {{ Form::select('type_id', collect(\App\Models\BSO\BsoTransfer::TYPES), $transfer->type_id, ['class' => 'form-control type_id', 'id'=>'type_id', 'required', 'readonly']) }}
                        </div>
                    </div>
                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Агент-получатель</label>
                        <div class="col-sm-3">
                            {{ Form::select('user_id_to', collect(\App\Models\User::all()->pluck('name', 'id'))->prepend('Выберите агента получателя', 0), $transfer->user_id_to, ['class' => 'form-control', 'id'=>'user_id_to', 'required', 'readonly']) }}
                        </div>
                    </div>

                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Агент-отправитель</label>
                        <div class="col-sm-3">
                            {{ Form::select('user_id_from', collect(\App\Models\User::all()->pluck('name', 'id'))->prepend('Выберите агента отправителя', 0), $transfer->user_id_from, ['class' => 'form-control', 'id'=>'user_id_from', 'required', 'readonly']) }}
                        </div>
                    </div>

                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Точка продаж</label>
                        <div class="col-sm-3">
                            {{ Form::select('point_sale_id', collect(\App\Models\Settings\PointsSale::all()->pluck('title', 'id'))->prepend('Выберите точку продаж', 0), $transfer->point_sale_id, ['class' => 'form-control', 'id'=>'point_sale_id', 'required', 'readonly']) }}
                        </div>
                    </div>

                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Сотрудник</label>
                        <div class="col-sm-3">
                            {{ Form::select('bso_manager_id', collect(\App\Models\User::all()->pluck('name', 'id'))->prepend('Выберите сотрудника', 0), $transfer->bso_manager_id, ['class' => 'form-control', 'id'=>'bso_manager_id', 'required', 'readonly']) }}
                        </div>
                    </div>
                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Курьер</label>
                        <div class="col-sm-3">
                            {{ Form::select('courier_id', collect(\App\Models\User::all()->pluck('name', 'id'))->prepend('Выберите курьера', 0), $transfer->courier_id, ['class' => 'form-control', 'id'=>'courier_id', 'required', 'readonly']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="transfer_setting_form">
            <div class="block-main">
                <div class="block-sub">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Указывать диапазон бсо</label>
                            <div class="col-sm-3">
                                {{ Form::checkbox('bso_range', 1, 0) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Страховая компания</label>
                            <div class="col-sm-3">
                                {{ Form::select('bso_supplier_id', \App\Models\Directories\BsoSuppliers::where('is_actual', 1)->get()->pluck('title', 'id'), 0, ['class' => 'form-control select2-all', 'id'=>'bso_supplier_id', 'required']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Тип</label>
                            <div class="col-sm-3">
                                {{ Form::select('bso_type_id', collect([])->prepend('Выберите тип', 0), 0, ['class' => 'form-control', 'id'=>'bso_type_id', 'required']) }}
                            </div>
                        </div>
                        <div class="btn btn-primary" id="add_range">Добавить</div>
                    </div>
                </div>
            </div>
        </div>


        <div id="transfer_form">
            <div class="block-main">
                <div class="block-sub">
                    <div class="row">
                        <div class="col-sm-6">
                            <div id="bso_transfer_ranges"></div>
                            <div id="bso_transfer_single"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="page-subheading">
                                <h2>Корзина</h2>
                                @if(session('group_bso'))
                                    <div class="btn btn-primary pull-right" id="view_type">Не группировать</div>
                                @else
                                    <div class="btn btn-primary pull-right" id="view_type">Группировка по типам</div>
                                @endif
                            </div>
                            <div id="basket_table"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function(){

            var type_id = $('[name="type_id"]').val();

            var visible_fields_types = {
                1: ['point_sale_id', 'user_id_to'],
                2: ['point_sale_id', 'bso_manager_id'],
                3: ['user_id_to', 'courier'],
                4: ['user_id_from', 'user_id_to', 'tp_change_selected'],
            };

            var transfer_settings = {
                dataElems: {
                    transfer_id: function(){ return $('[name="transfer_id"]').val() },
                    bso_supplier_id: function () { return $('[name="bso_supplier_id"]').val() },
                    bso_range: function () { return $('[name="bso_range"]').prop('checked') },
                    bso_type_id: function () { return $('[name="bso_type_id"]').val() },
                },
                get: function(name){ return this.dataElems[name](); },
                getData: function () {
                    var data = {};
                    $.each(this.dataElems, function (k, v) {
                        data[k] = v()
                    });
                    return data;
                },

            };

            init(type_id);

            $(document).on('change', '.bso_table_single input[type="checkbox"]', function(){
                var checked = $(this).prop('checked');
                var tagname = $(this).parent()[0].tagName;
                if(tagname === 'TH'){
                    $(this).closest('table').find('td').find('input[type="checkbox"]').prop('checked', checked);
                }else{
                    var length = $(this).closest('table').find('td').find('input[type="checkbox"]').length;
                    var length_checked = $(this).closest('table').find('td').find('input[type="checkbox"]:checked').length;
                    $(this).closest('table').find('th').find('input[type="checkbox"]').prop('checked', length === length_checked)
                }
            });

            $(document).on('click', '#add_range', function(){
                addRange();
            });

            $(document).on('change', '[name="bso_range"]', function(){
                toggleRangeSingle();
            });

            $(document).on('change', '[name="bso_supplier_id"]', function(){
                reloadBsoTypes();
            });

            $(document).on('change', '[name="bso_type_id"]', function(){
                window.reloadSingle();
            });

            $(document).on('click', '#transfer_delete', function(){
                if(confirm('Удалить данную передачу БСО?')){
                    $.post('/bso/transfer/process/'+transfer_settings.get('transfer_id')+'/delete', {}, function(res){
                        if(res.status === 'ok'){
                            location.href = '/bso/transfer';
                        }
                    })
                }
            });


            $(document).on('click', '#view_type', function(){
                var btn = $(this);
                $.post('/bso/transfer/process/toggle-view-type', {}, function(res){
                    if(res.status === 'ok'){
                        var btn_text = res.group_bso ? 'Не группировать' : 'Группировка по типам';
                        btn.html(btn_text);
                        window.reloadBasket();
                    }
                })
            });

            function toggleRangeSingle(){
                var elements = {
                    ranges: $('#bso_transfer_ranges'),
                    single: $('#bso_transfer_single'),
                    bso_type: $('[name="bso_type_id"]').closest('.form-group'),
                    add_btn: $('#add_range'),
                };
                $.each(elements, function(k,v){
                    $(v).hide();
                });

                /**
                 * Если диапазон
                 */
                if(transfer_settings.get('bso_range')){
                    elements.ranges.show();
                    elements.add_btn.show();
                }else{
                    elements.single.show();
                    elements.bso_type.show();
                }
            }

            function addRange(){
                if($('[data-supplier="'+transfer_settings.get('bso_supplier_id')+'"]').length === 0){
                    $.getJSON('/bso/transfer/process/get-range-table', transfer_settings.getData(), function(res){
                        $('#bso_transfer_ranges').append(res.html);
                    });
                }
            }


            function reloadBsoTypes(){
                if(transfer_settings.get('bso_supplier_id') !== undefined){
                    $.getJSON('/bso/transfer/process/get-suppliers-bso-types/' + transfer_settings.get('bso_supplier_id'), {}, function(res){
                        if(res.status = 'ok'){
                            $('#bso_type_id').find('option[value != 0]').remove();
                            $.each(res.data, function(k, v){
                                $('#bso_type_id').append($('<option value="'+v.id+'">'+v.title+'</option>'));
                            })
                        }
                    });
                }
            }


            function init(type_id){

                $('.toggled-setting').hide();

                $.each(visible_fields_types[type_id], function(k,v){
                    $('[name="'+v+'"]').closest('.toggled-setting').show();
                });

                window.reloadBasket = function(){
                    $.getJSON('/bso/transfer/process/get-basket-table/' + transfer_settings.get('transfer_id'), {}, function(res){
                        $('#basket_table').html(res.html);
                    });
                    console.log('reloadBasket');
                };

                window.reloadSingle = function(){
                    $.getJSON('/bso/transfer/process/get-single-table', transfer_settings.getData(), function(res){
                        $('#bso_transfer_single').html(res.html);
                    });
                };

                window.reloadBasket();
                toggleRangeSingle();
                reloadBsoTypes();
            }

        });

    </script>
@endsection