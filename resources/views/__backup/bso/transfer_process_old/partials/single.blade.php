<div class="page-subheading">
    <h2>{{ $bso_supplier->title }}</h2>
    <div class="btn btn-primary pull-right" id="single_bso_append">Добавить</div>
</div>

@if(sizeof($bso_items))

    <table class="bso_table bso_table_single" style="float: left;">
        <tbody>
            <tr>
                <th>№</th>
                <th>БСО</th>
                <th width="15%">Выбор <input type="checkbox" id="check_all"></th>
            </tr>
            @foreach($bso_items as $bso_item)
                <tr>
                    <td>{{$bso_item->id}}</td>
                    <td>{{$bso_item->bso_title}}</td>
                    <td>
                        <input type="checkbox" class="cb_bso" name="bso_id" value="{{$bso_item->id}}">
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@else

    Нет БСО

@endif


<script>

    $(function(){

        $(document).on('click', '#single_bso_append', function(){

            var data = {
                transfer_id: $('[name="transfer_id"]').val(),
                bso_supplier_id: $('[name="bso_supplier_id"]').val(),
                bso_type: $('[name="bso_type_id"]').val(),
                ids: {}
            };

            $.each($('[name="bso_id"]:checked'), function(k,v){
                data.ids[k] = $(v).val();
            });

            $.post('/bso/transfer/process/add-single-bso-to-cart', data, function(res){

                if(res.status === 'ok'){
                    window.reloadSingle();
                    window.reloadBasket();
                }
            }, 'json');

        });

    });

</script>