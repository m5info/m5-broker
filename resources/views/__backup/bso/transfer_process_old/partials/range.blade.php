<div data-supplier="{{$bso_supplier->id}}">
    <div class="page-subheading">
        <h2>{{ $bso_supplier->title }}</h2>
        <span class="btn btn-danger btn-xs remove_supplier pull-right" style="color: white;font-size: 18px; width: 32px;"><i class="fa fa-close"></i></span>
    </div>
    <table class="bso_table bso_table_range">
        <tr>
            <th width="20%">Тип</th>
            <th width="13%">Серия</th>
            <th width="11%">Кол-во</th>
            <th class="bso_number_td">№ полиса / квит. / сер.карт с</th>
            <th class="bso_number_td">№ по</th>
            <th width="13%">Доп. серия</th>
            <th width="9%">Удалить</th>
            <th width="9%">В корзину</th>
        </tr>
        <tr class="table_row">
            <td>
               {{ Form::select('bso_type', collect($bso_supplier->type_bso)->pluck('title', 'id')->prepend('---', 0), 0, ['class' => 'form-control bso_type']) }}
                <div class="error_div"></div>
            </td>
            <td>
                {{ Form::select('bso_serie', collect([])->pluck('title', 'id')->prepend('---', 0), 0, ['class' => 'form-control bso_serie']) }}
                <div class="error_div"></div>
            </td>
            <td class="bso_qty_td">
                {{ Form::text('bso_qty', '', ['class' => 'form-control bso_qty', 'type' => 'number']) }}
                <div class="error_div"></div>
            </td>
            <td>
                {{ Form::text('bso_number', '', ['class' => 'form-control bso_number', 'type' => 'number']) }}
                <div class="error_div"></div>
            </td>
            <td>
                {{ Form::text('bso_number_to', '', ['class' => 'form-control bso_number_to', 'readonly']) }}
            </td>
            <td>
                {{ Form::select('bso_dop_serie', collect([])->pluck('title', 'id')->prepend('---', 0), 0, ['class' => 'form-control bso_dop_serie']) }}
            </td>
            <td style="text-align: center;">
                <span class="btn remove_range_row" style="color: red;font-size: 18px;"><i class="fa fa-close"></i></span>
            </td>
            <td style="text-align: center;">
                <span class="btn" style="color: green;font-size: 18px;" data-share_supplier_row="{{$bso_supplier->id}}"><i class="fa fa-share"></i></span>
            </td>
        </tr>
    </table>


    <div class="btn btn-primary pull-right mt-10" data-add_supplier_row="{{$bso_supplier->id}}">
        Добавить строку
    </div>

</div>
<div class="clearfix"></div>

<script>
    $(function(){

        var supplier_id = {{$bso_supplier->id}};

        $(document).on('click', '[data-add_supplier_row="'+supplier_id+'"]', function(){

            var block = $(this).closest('[data-supplier]');
            var row = block.find('tr.table_row:first');
            var table = block.find('table');
            var new_row = row.clone();

            $.each(new_row.find('select'), function(k,v){
                $(v).val(0);
            });
            $.each(new_row.find('input'), function(k,v){
                $(v).val('');
            });

            table.append(new_row);
        });


        $(document).on('click', '.remove_range_row', function(){
            if($(this).closest('table').find('tr.table_row').length > 1){
                $(this).closest('tr').remove();
            }
        });

        $(document).on('click', '.remove_supplier', function(){
            $(this).closest('[data-supplier]').remove();
        });


        $(document).on('change', 'select[name="bso_type"]', function(){

            var data = {
                bso_supplier_id: $(this).closest('[data-supplier]').data('supplier'),
                bso_type_id: $(this).val(),
            };
            var bso_serie_select = $(this).closest('tr').find('select[name="bso_serie"]');

            $.getJSON('/bso/transfer/process/get-bso-serie', data, function(res){
                if(res.status = 'ok'){
                    bso_serie_select.find('option[value != 0]').remove();
                    $.each(res.data, function(k, v){
                        bso_serie_select.append($('<option value="'+v.id+'">'+v.bso_serie+'</option>'));
                    })
                }
            });
        });


        $(document).on('change', 'select[name="bso_serie"]', function(){
            var data = {
                bso_serie_id: $(this).val()
            };

            var bso_dop_serie_select = $(this).closest('tr').find('select[name="bso_dop_serie"]');
            $.getJSON('/bso/transfer/process/get-bso-dop-serie', data, function(res){
                if(res.status = 'ok'){
                    bso_dop_serie_select.find('option[value != 0]').remove();
                    $.each(res.data, function(k, v){
                        bso_dop_serie_select.append($('<option value="'+v.id+'">'+v.bso_dop_serie+'</option>'));
                    })
                }
            });
        });

        $(document).on('keyup', '[name="bso_number"], [name="bso_qty"]', function(){

            $(this).val($(this).val().replace(/[^0-9]/g, ''));
            $(this).val($(this).val() > 0 ? $(this).val() : 1);

            var diff = $('[name="bso_qty"]').val();

            diff = diff > 0 ? diff-1 : 0;

            var bso_from_field = $(this).closest('tr').find('[name="bso_number"]');
            var bso_to_field = $(this).closest('tr').find('[name="bso_number_to"]');

            bso_to_field.val(bso_from_field.val()*1 + diff*1)
        });


        /**
         * Добавление диапазона в корзину
         */
        $(document).on('click', '[data-share_supplier_row="'+supplier_id+'"]', function(){
            var row = $(this).closest('.table_row');

            var data = {
                transfer_id: $('[name="transfer_id"]').val(),
                bso_supplier_id: $('[name="bso_supplier_id"]').val(),

                bso_type: row.find('[name="bso_type"]').val(),
                bso_serie_id: row.find('[name="bso_serie"]').val(),
                bso_number: row.find('[name="bso_number"]').val(),
                bso_number_to: row.find('[name="bso_number_to"]').val(),
                bso_dop_serie_id: row.find('[name="bso_dop_serie"]').val(),
            };


            $.post('/bso/transfer/process/add-range-bso-to-cart', data, function(res){
                if(res.status === 'ok'){
                    if(row.closest('table').find('.table-row').length > 1){
                        row.remove();
                    }else{
                        row.closest('[data-supplier]').remove();
                    }
                    window.reloadBasket();
                }else{
                    alert(res.msg);
                }
            }, 'json')
        });

    })

</script>