@if(sizeof($transfer_items))
    <table class="table table-bordered bso_items_table">
        <tbody>
            <tr>
                <th>№</th>
                <th>СК</th>
                <th>Тип</th>
                <th>{{ session('group_bso') ? "Кол-во" : "Номер"  }}</th>
                <th>Удалить</th>
            </tr>

            @if(session('group_bso'))
                @foreach($transfer_items as $supplier_id => $supplier)
                    @if(sizeof($supplier['_sub']))

                        @php($i=1)@foreach($supplier['_sub'] as $type_id => $type)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$supplier['supplier_title']}}</td>
                                <td>{{$type['type_title']}}</td>
                                <td>{{count($type['_sub'])}}</td>
                                <td style="text-align: center;">
                                    <span class="btn remove_group_basket_row" data-supplier_id="{{$supplier_id}}" data-type_id="{{$type_id}}" style="color: red;font-size: 18px;">
                                        <i class="fa fa-close"></i>
                                    </span>
                                </td>
                            </tr>
                        @php($i++)@endforeach

                    @endif
                @endforeach
            @else
                @foreach($transfer_items as $transfer_item)
                    <tr>
                        <td>{{$transfer_item->id}}</td>
                        <td>{{$transfer_item->supplier->title}}</td>
                        <td>{{$transfer_item->type->title}}</td>
                        <td>{{$transfer_item->bso_title }}</td>
                        <td style="text-align: center;">
                            <span class="btn remove_basket_row" data-item_id="{{$transfer_item->id}}" style="color: red;font-size: 18px;">
                                <i class="fa fa-close"></i>
                            </span>
                        </td>
                    </tr>
                @endforeach
            @endif

        </tbody>
    </table>
    @if($transfer->type_id == 1)
        <div class="btn btn-success pull-right mt-10" id="complete_transfer">
            Передать
        </div>
    @elseif($transfer->type_id == 2)
        <div class="btn btn-success mt-10" id="download_pp_act">
            Акт приема-передачи
        </div>
    @endif
@else
    Нет добавленных БСО
@endif

<script>

    $(function(){

        $(document).on('click', '#download_pp_act', function(){
            location.href = "/bso/transfer/process/" + $('[name="transfer_id"]').val() + "/download-pp-act";
        });

        $('.remove_basket_row').click(function(){
            var data = {
                transfer_id: $('[name="transfer_id"]').val(),
                item_id: $(this).data('item_id'),
            };
            $.post('/bso/transfer/process/remove-item/', data, function(res){
                window.reloadBasket();
                window.reloadSingle();
            });
        });

        $('.remove_group_basket_row').click(function(){
            var data = {
                transfer_id: $('[name="transfer_id"]').val(),
                supplier_id: $(this).data('supplier_id'),
                type_id: $(this).data('type_id'),
            };
            $.post('/bso/transfer/process/remove-group/', data, function(res){
                window.reloadBasket();
                window.reloadSingle();
            });
        });


        $(document).on('click', '#complete_transfer', function(){
            var transfer_id = $('[name="transfer_id"]').val();
            location.href = '/bso/transfer/process/'+transfer_id+'/complete-transfer/';
        });

    });

</script>