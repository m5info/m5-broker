@extends('layouts.app')
@section('content')
    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h2>Прием передача БСО</h2>
            <button class="btn btn-success pull-right">Передача выполнена</button>
        </div>
        <div class="block-main">
            <div class="block-sub">
                <div class="form-horizontal">
                    {{ Form::hidden('transfer_id', $transfer->id) }}
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Тип</label>
                        <div class="col-sm-3">
                             {{ Form::select('type_id', collect(\App\Models\BSO\BsoTransfer::TYPES), $transfer->type_id, ['class' => 'form-control type_id', 'id'=>'type_id', 'required', 'readonly']) }}
                        </div>
                    </div>
                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Агент-получатель</label>
                        <div class="col-sm-3">
                            {{ Form::select('user_id_to', collect(\App\Models\User::all()->pluck('name', 'id'))->prepend('Выберите агента получателя', 0), $transfer->user_id_to, ['class' => 'form-control', 'id'=>'user_id_to', 'required', 'readonly']) }}
                        </div>
                    </div>

                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Агент-отправитель</label>
                        <div class="col-sm-3">
                            {{ Form::select('user_id_from', collect(\App\Models\User::all()->pluck('name', 'id'))->prepend('Выберите агента отправителя', 0), $transfer->user_id_from, ['class' => 'form-control', 'id'=>'user_id_from', 'required', 'readonly']) }}
                        </div>
                    </div>

                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Точка продаж</label>
                        <div class="col-sm-3">
                            {{ Form::select('point_sale_id', collect(\App\Models\Settings\PointsSale::all()->pluck('title', 'id'))->prepend('Выберите точку продаж', 0), $transfer->point_sale_id, ['class' => 'form-control', 'id'=>'point_sale_id', 'required', 'readonly']) }}
                        </div>
                    </div>

                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Сотрудник</label>
                        <div class="col-sm-3">
                            {{ Form::select('bso_manager_id', collect(\App\Models\User::all()->pluck('name', 'id'))->prepend('Выберите сотрудника', 0), $transfer->bso_manager_id, ['class' => 'form-control', 'id'=>'bso_manager_id', 'required', 'readonly']) }}
                        </div>
                    </div>
                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Курьер</label>
                        <div class="col-sm-3">
                            {{ Form::select('courier_id', collect(\App\Models\User::all()->pluck('name', 'id'))->prepend('Выберите курьера', 0), $transfer->courier_id, ['class' => 'form-control', 'id'=>'courier_id', 'required', 'readonly']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="transfer_form">
            <div class="block-main">
                <div class="block-sub">
                    <div class="row">
                        <div class="col-sm-6">
                            <div id="bso_transfer_ranges"></div>
                            <div id="bso_transfer_single"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="page-subheading">
                                <h2>Переданные акты</h2>
                                @if(session('group_bso'))
                                    <div class="btn btn-primary pull-right" id="view_type">Не группировать</div>
                                @else
                                    <div class="btn btn-primary pull-right" id="view_type">Группировка по типам</div>
                                @endif
                            </div>
                            <div id="basket_table"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(function(){

            var type_id = $('[name="type_id"]').val();

            var visible_fields_types = {
                1: ['point_sale_id', 'user_id_to'],
                2: ['point_sale_id', 'bso_manager_id'],
                3: ['user_id_to', 'courier'],
                4: ['user_id_from', 'user_id_to', 'tp_change_selected'],
            };

            init(type_id);

            $(document).on('click', '#transfer_delete', function(){
                if(confirm('Удалить данную передачу БСО?')){
                    $.post('/bso/transfer/process/'+$('[name="transfer_id"]').val()+'/delete', {}, function(res){
                        if(res.status === 'ok'){
                            location.href = '/bso/transfer';
                        }
                    })
                }
            });

            $(document).on('click', '#view_type', function(){
                var btn = $(this);
                $.post('/bso/transfer/process/toggle-view-type', {}, function(res){
                    if(res.status === 'ok'){
                        var btn_text = res.group_bso ? 'Не группировать' : 'Группировка по типам';
                        btn.html(btn_text);
                        window.reloadBasket();
                    }
                })
            });

            function init(type_id){
                $('.toggled-setting').hide();
                $.each(visible_fields_types[type_id], function(k,v){
                    $('[name="'+v+'"]').closest('.toggled-setting').show();
                });

                window.reloadBasket = function(){
                    $.getJSON('/bso/transfer/process/get-basket-table/' + $('[name="transfer_id"]').val(), {}, function(res){
                        $('#basket_table').html(res.html);
                    });
                };

                window.reloadBasket();
            }
        });

    </script>
@endsection