<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <br/>

    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">{{$bso->type->title}}
                    @if(auth()->user()->hasPermission('bso', 'edit_bso_title'))
                        <a href="" class="fancybox fancybox.iframe underline">Изменить</a>
                    @endif

                </label>
                <div class="col-sm-12">
                    {{$bso->bso_title}}
                </div>
            </div>
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Организация получатель</label>
                <div class="col-sm-12">
                    {{$bso->supplier_org->title}}

                    @if(auth()->user()->hasPermission('bso', 'edit_supplier_org'))
                        <a href="" class="fancybox fancybox.iframe underline">Изменить</a>
                    @endif

                </div>
            </div>
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Страховая компания</label>
                <div class="col-sm-12">
                    {{$bso->supplier->title}}
                </div>
            </div>
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Точка продаж</label>
                <div class="col-sm-12">
                    {{$bso->point_sale->title}}
                </div>
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-4">
                <label class="col-sm-12 control-label">Агент получивший полис</label>
                <div class="col-sm-12">
                    @if($bso->is_reserved == 1 && $bso->bso_cart_id > 0)
                        <a href="/bso/transfer/?bso_cart_id={{$bso->bso_cart_id}}" class="underline">Резерв: {{($bso->cars->user_to)?$bso->cars->user_to->name:''}}</a>
                    @else
                        {{($bso->agent)?$bso->agent->name:''}}
                    @endif

                </div>
            </div>
            <div class="col-sm-4">
                <label class="col-sm-12 control-label">У кого сейчас</label>
                <div class="col-sm-12">
                    {{($bso->user)?$bso->user->name:''}}
                </div>
            </div>
            <div class="col-sm-4">
                <label class="col-sm-12 control-label">Событие / Статус</label>
                <div class="row col-sm-12">
                    <div class="col-sm-6">
                        {{Form::select('location_id', \App\Models\BSO\BsoLocations::all()->pluck('title', 'id'), $bso->location_id,  ['class' => 'form-control'])}}
                    </div>
                    <div class="col-sm-6">
                        {{Form::select('state_id', \App\Models\BSO\BsoState::all()->pluck('title', 'id'), $bso->state_id,  ['class' => 'form-control'])}}
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="divider"></div>

    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Дата приема из СК</label>
                <div class="col-sm-12">
                    {{setDateTimeFormatRu($bso->time_target, 1)}}
                </div>
                <label class="col-sm-12 control-label">Дата создания</label>
                <div class="col-sm-12">
                    {{setDateTimeFormatRu($bso->time_create)}}
                </div>
            </div>
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Дата передачи Агенту</label>
                <div class="col-sm-12">
                    {{setDateTimeFormatRu($bso->transfer_to_agent_time)}}
                </div>
            </div>
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Дата приема в организацию</label>
                <div class="col-sm-12">
                    {{setDateTimeFormatRu($bso->transfer_to_org_time)}}
                </div>
            </div>
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Дата передачи в СК</label>
                <div class="col-sm-12">
                    {{setDateTimeFormatRu($bso->transfer_to_sk_time)}}
                </div>
            </div>
        </div>


    </div>



    <div class="divider"></div>


        <div class="form-horizontal">
            <table class="dataTable">
                <thead>
                <tr>
                    <th>Дата/время</th>
                    <th>Событие</th>
                    <th>Статус</th>
                    <th>Выдал</th>
                    <th>Получил</th>
                    <th>Сотрудник</th>
                    <th>Акт</th>
                </tr>
                </thead>
                <tbody>


                    @if(sizeof($bso->logs))

                        @foreach($bso->logs as $bso_logs)

                            <tr>
                                <td>{{setDateTimeFormatRu($bso_logs->log_time)}}</td>
                                <td>{{$bso_logs->bso_location->title}}</td>
                                <td>{{$bso_logs->bso_state->title}}</td>
                                @if($bso_logs->act)
                                    <td>{{($bso_logs->act->user_from)?$bso_logs->act->user_from->name:''}}</td>
                                    <td>{{($bso_logs->act->user_to)?$bso_logs->act->user_to->name:''}}</td>
                                    <td>{{($bso_logs->act->bso_manager)?$bso_logs->act->bso_manager->name:''}}</td>
                                    <td><a style="color:black !important;text-decoration:underline;" href="{{url("/bso_acts/show_bso_act/{$bso_logs->act->id}/")}}">Акт № {{$bso_logs->act->act_number}}</a></td>

                                @else
                                    <td colspan="4"></td>
                                @endif

                            </tr>

                        @endforeach
                    @endif


                </tbody>
            </table>
        </div>


</div>