<div class="row form-horizontal" id="main_container">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

        <!-- Условия договора -->
        @include('contracts.contract.edit.info', ['contract' => $contract])

        <span class="btn btn-primary" id="controlSaveButtonBox" onclick="saveContract()">
            Сохранить
        </span>

    </div>


    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">


        <!-- Участники договора -->
        @include('contracts.contract.edit.insurer_info', ['contract' => $contract])


    </div>

</div>