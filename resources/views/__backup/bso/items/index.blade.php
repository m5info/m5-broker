@extends('layouts.app')

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-11 col-lg-11">

        <div class="block-main">
            <div class="block-sub">
                <div class="easyui-accordion">
                    <div title="БСО - {{$bso->type->title}} - {{$bso->bso_title}}">
                        <div class="row col-xs-11 col-sm-11 col-md-11 col-lg-11">
                            @include('bso.items.info', ['bso' => $bso])
                        </div>
                    </div>


                    @if($bso->contract && $bso->bso_class_id !== 100)
                    <div title="Договор">
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            @include('bso.items.contract', ['contract' => $bso->contract, 'agents' => $agents, 'object_insurer' => $bso->contract->object_insurer])
                        </div>
                    </div>
                    @endif



                </div>
            </div>
        </div>


    </div>
@endsection

@section('js')


    <script>

        $(function () {


            $('.easyui-datalist').datalist({


            });

            @if($bso->contract)

                initInfo();
                initInsurer();

            @endif



        });


    </script>

@endsection