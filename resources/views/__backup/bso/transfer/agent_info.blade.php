
<input type="hidden" value="0" id="limit_ban">
<table class="bso_table">
    <tbody><tr>
        <td rowspan="2">БСО На руках</td>
        <td rowspan="2">Из них просроченных</td>
        <td colspan="3" class="center">Фин. долги</td>
    </tr>
    <tr>
        <td class="center">Нал</td>
        <td class="center">Безнал</td>
        <td class="center">Всего</td>
    </tr>
    <tr style="background-color: #FDD;">
        <td class="right"><a target="_blank" href="show_bsos_agent.php?type=on_hands&amp;agent_id=10">{{$agent->getUserLimitBSOToProduct(0, 1)}}</a></td>
        <td class="right"><a target="_blank" href="show_bsos_agent.php?type=on_hands&amp;agent_id=10">0</a></td>
        <td class="right"><a target="_blank" href="../agent_debt/financial_debts_info.php?agent_id=10&amp;flow=nal">0,00</a></td>
        <td class="right"><a target="_blank" href="../agent_debt/financial_debts_info.php?agent_id=10&amp;flow=bn">0,00</a></td>
        <td class="right"><a target="_blank" href="../agent_debt/financial_debts_info.php?agent_id=10">0,00</a></td>
    </tr>
    </tbody></table>
<br>


<span class="btn btn-primary" onclick="openFancyBoxFrame('{{url("/users/limit/?user_id={$agent->id}")}}')">Редактировать лимиты</span><br>

@if(sizeof($products))
    <table class="bso_table">
        <tr>
            <td>Тип</td>
            <td>Лимит</td>
            <td>У агента</td>
        </tr>
        @foreach($products as $product)
            <tr style="background-color: #FEE;">
                <td>{{$product->title}}</td>
                <td>{{$agent->getUserLimitBSOToProduct($product->id, 0)}}</td>
                <td>{{$agent->getUserLimitBSOToProduct($product->id, 1)}}</td>
            </tr>


        @endforeach

    </table>
@else
    {{ trans('form.empty') }}
@endif



