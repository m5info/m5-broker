@extends('layouts.frame_new')

@section('title')

    {{$title}}

    <span class="btn btn-success pull-right" id="obj_export_xls" >Выгрузка в .xls</span>

@endsection

@section('content')


    <table class="bso_table">
        <tr>
            <th>№ п/п</th>
            <th>Организация</th>
            <th>Страховая компания</th>
            <th>Вид страхования</th>
            <th>№ полиса / квит. / сер.карт с</th>
            <th>№ бланка</th>
            <th>Точка оборота БСО</th>
            <th>Событие</th>
            <th>Статус</th>
            <th>Агент</th>
            <th>НОП</th>
            <th>Дата приема из СК</th>
            <th>Дней на складе</th>
            <th>Дата последней операции</th>
            <th>Дней у агента</th>
            <th>Акт приема передачи в СК</th>
            <th>Номер Отчета</th>
            <th>История</th>
        </tr>

        @if(sizeof($bso_items))
            @foreach($bso_items as $key => $bso)

                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$bso->supplier_org->title}}</td>
                    <td>{{$bso->supplier->title}}</td>
                    <td>{{$bso->type->title}}</td>
                    <td>{{$bso->bso_title}}</td>
                    <td>{{$bso->bso_blank_title}}</td>
                    <td>{{$bso->point_sale->title}}</td>
                    <td>{{$bso->bso_locations->title}}</td>
                    <td>{{$bso->bso_states->title}}</td>
                    <td>{{($bso->user)?$bso->user->name:''}}</td>
                    <td>{{($bso->user)?(($bso->user->perent)?$bso->user->perent->name:''):''}}</td>
                    <td>{{setDateTimeFormatRu($bso->time_create, 1)}}</td>
                    <td>{{round((time() - strtotime($bso->time_create)) / 86400)}}</td>
                    <td>{{setDateTimeFormatRu($bso->last_operation_time, 1)}}</td>
                    <td>---</td>

                    <td><a target="_blank" href="#">---</a></td>
                    <td><a target="_blank" href="#">---</a></td>
                    <td><a href="{{url("/bso/items/{$bso->id}/")}}" target="_blank">подробнее</a></td>
                </tr>

            @endforeach
        @endif

    </table>



@endsection


@section('footer')

    Сделать страницы

@endsection

@section('js')




    <script type="text/javascript">



        $(function() {


        });



    </script>

    <style>


        .modal .modal-body {
            padding: 0px;
        }

        .bso_table {
            font: 12px arial;
            border: 1px solid #777;
            border-collapse: collapse;
        }
        .bso_table td, th {
            border: 1px solid #777;
            padding: 5px;
            font: 12px arial;
        }

        .bso_table th {
            background-color: #EEE;
        }

        .bso_table td {
            background-color: #FFF;
        }

        .bso_header {
            font: 12px arial;
            border: none;
            border-collapse: collapse;
            width: 100%;
        }
        .bso_header td {
            padding: 5px;
            border: none;
            font: 12px arial;
            background-color: #F3F3F3;
        }

        .sk_header {
            font: bold 17px arial !important;
        }

        .center {
            text-align: center; !important;
        }

        .gray {
            background-color: #EEE !important;
        }
        input[type=button] {
            cursor: pointer;
        }


    </style>

@endsection

