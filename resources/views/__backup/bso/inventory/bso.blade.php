@extends('layouts.app')



@section('content')


    <div class="page-heading">
        <h1>Инвентаризация БСО</h1>
        <span class="btn btn-success" id="obj_export_xls" >Выгрузка в .xls</span>

    </div>

    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div class="filter-group">


                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {{ Form::select('insurance_companies_id', \App\Models\Directories\InsuranceCompanies::where('is_actual', 1)->get()->pluck('title', 'id')->prepend('Страховая компания', -1), -1, ['class' => 'form-control', 'id'=>'insurance_companies_id', 'onchange'=>'loadItems()']) }}
                    </div>
                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {{ Form::select('bso_supplier_id', App\Models\Organizations\Organization::getOrgProvider()->get()->pluck('title', 'id')->prepend('Организация', -1), -1, ['class' => 'form-control', 'id'=>'bso_supplier_id', 'onchange'=>'loadItems()']) }}
                    </div>
                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {{ Form::select('point_sale_id', \App\Models\Settings\PointsSale::where('is_actual', 1)->get()->pluck('title', 'id')->prepend('Точка продаж', -1), -1, ['class' => 'form-control', 'id'=>'point_sale_id', 'onchange'=>'loadItems()']) }}
                    </div>
                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {{ Form::select('type_bso_id', \App\Models\BSO\BsoType::getDistinctType()->get()->pluck('title', 'product_id')->prepend('Вид', -1), -1, ['class' => 'form-control', 'id'=>'type_bso_id', 'onchange'=>'loadItems()']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>


                </div>


            </div>




        </div>
    </div>


    <div class="block-inner sorting col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow: auto;">

        <table class="dataTable">
            <tbody>

                <tr>
                    <th rowspan="2">Вид</th>
                    <th rowspan="2">Итого<br>Принято из СК</th>
                    <th rowspan="2">В сейфе</th>
                    <th rowspan="2">Резерв</th>
                    <th rowspan="2">Передано агентам</th>
                    <th colspan="3">Реализовано</th>
                    <th colspan="4">Передано в СК</th>
                </tr>
                <tr>
                    <th>Проданы</th>
                    <th>Испорчены</th>
                    <th>Иные</th>
                    <th>Чистые</th>
                    <th>Проданы</th>
                    <th>Испорчены</th>
                    <th>Иные</th>
                </tr>

                @if(sizeof($bso_items))


                    @php $sk_id_temp = ''; @endphp
                    @foreach($bso_items as $bso)

                        @if($sk_id_temp != $bso->sk_title)
                            <tr><td colspan="12" class="sk_header">{{$bso->sk_title}}</td></tr>
                        @endif

                        <tr>
                            <td>{{$bso->type_title}}</td>
                            <td class="center gray"><span onclick="detailsBso('all', '{{$bso->type_bso_id}}')">{{$bso->qty_all}}</span></td>
                            <td class="center"><span onclick="detailsBso('stock', '{{$bso->type_bso_id}}')">{{$bso->qty_stock}}</span></td>
                            <td class="center gray"><span onclick="detailsBso('reserv', '{{$bso->type_bso_id}}')">{{$bso->qty_reserv}}</span></td>
                            <td class="center"><span onclick="detailsBso('agents', '{{$bso->type_bso_id}}')">{{$bso->qty_agents}}</span></td>

                            <td class="center"><span onclick="detailsBso('sold', '{{$bso->type_bso_id}}')">{{$bso->qty_sold}}</span></td>
                            <td class="center"><span onclick="detailsBso('spoiled', '{{$bso->type_bso_id}}')">{{$bso->qty_spoiled}}</span></td>
                            <td class="center"><span onclick="detailsBso('other', '{{$bso->type_bso_id}}')">{{$bso->qty_other}}</span></td>

                            <td class="center gray"><span onclick="detailsBso('sk_blank', '{{$bso->type_bso_id}}')">{{$bso->qty_sk_blank}}</span></td>
                            <td class="center gray"><span onclick="detailsBso('sk_sold', '{{$bso->type_bso_id}}')">{{$bso->qty_sk_sold}}</span></td>
                            <td class="center gray"><span onclick="detailsBso('sk_spoile', '{{$bso->type_bso_id}}')">{{$bso->qty_sk_spoiled}}</span></td>
                            <td class="center gray"><span onclick="detailsBso('sk_other', '{{$bso->type_bso_id}}')">{{$bso->qty_sk_other}}</span></td>

                        </tr>

                        @php $sk_id_temp = $bso->sk_title; @endphp

                    @endforeach
                @endif






            </tbody>
        </table>

    </div>





@endsection

@section('js')




    <script type="text/javascript">


        $(function() {


        });


        function loadItems()
        {
            window.location = "/bso/inventory_bso/?"+getParam();
        }

        function detailsBso(types, type_bso_id)
        {
            openFancyBoxFrame("/bso/inventory_bso/details/?"+getParam2()+"&type_bso_id="+type_bso_id+"&types="+types);
        }


        function getParam()
        {
            return "insurance_companies_id="+$("#insurance_companies_id").val()+"&bso_supplier_id="+$("#bso_supplier_id").val()+"&point_sale_id="+$("#point_sale_id").val()+"&type_bso_id="+$("#type_bso_id").val();
        }

        function getParam2()
        {
            return "insurance_companies_id="+$("#insurance_companies_id").val()+"&bso_supplier_id="+$("#bso_supplier_id").val()+"&point_sale_id="+$("#point_sale_id").val();
        }

    </script>

    <style>

        .center > span {
            color: blue;
            cursor: pointer;
        }

    </style>

@endsection

