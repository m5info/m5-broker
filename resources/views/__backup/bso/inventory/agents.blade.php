@extends('layouts.app')



@section('content')


    <div class="page-heading">
        <h1>Инвентаризация по агентам</h1>

        <span class="btn btn-success" id="obj_export_xls" >Выгрузка в .xls</span>

    </div>

    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div class="filter-group">


                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {{ Form::select('agent_id', $agents->prepend('Агент', -1), -1, ['class' => 'form-control select2', 'id'=>'agent_id', 'onchange'=>'loadItems()']) }}
                    </div>
                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {{ Form::select('nop_id', $agents->prepend('Куратор', -1), -1, ['class' => 'form-control select2', 'id'=>'nop_id', 'onchange'=>'loadItems()']) }}
                    </div>
                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {{ Form::select('point_sale_id', \App\Models\Settings\PointsSale::where('is_actual', 1)->get()->pluck('title', 'id')->prepend('Точка продаж', -1), -1, ['class' => 'form-control', 'id'=>'point_sale_id', 'onchange'=>'loadItems()']) }}
                    </div>
                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {{ Form::select('type_bso_id', \App\Models\BSO\BsoType::getDistinctType()->get()->pluck('title', 'product_id')->prepend('Вид', -1), -1, ['class' => 'form-control', 'id'=>'type_bso_id', 'onchange'=>'loadItems()']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>


                </div>


            </div>




        </div>
    </div>


    <div class="block-inner sorting col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow: auto;">

        <table class="dataTable">
            <tbody>


                <tr>
                    <th>Агент</th>
                    <th>Куратор</th>
                    <th>На руках</th>
                    <th>Из них старых (более 30 дней) *</th>
                    <th>Из них старых (более 90 дней)</th>
                </tr>




            </tbody>
        </table>

    </div>





@endsection

@section('js')




    <script type="text/javascript">


        $(function() {


        });


        function loadItems()
        {
            window.location = "/bso/inventory_agents/?"+getParam();
        }



        function getParam()
        {
            return "agent_id="+$("#agent_id").val()+"&nop_id="+$("#nop_id").val()+"&point_sale_id="+$("#point_sale_id").val()+"&type_bso_id="+$("#type_bso_id").val();
        }



    </script>



@endsection

