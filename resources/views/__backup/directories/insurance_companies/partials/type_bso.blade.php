<div class="row col-xs-12 col-sm-6 col-md-6 col-lg-6">
    <div class="page-subheading">
        <h2>Типы БСО</h2>
        <a href="/directories/insurance_companies/{{$insurance_companies->id}}/type_bso/0/"
           class="fancybox fancybox.iframe btn btn-primary pull-right">
            {{ trans('form.buttons.add') }}
        </a>
    </div>
    <div class="block-main">
        <div class="block-sub">
            @if($insurance_companies->type_bso)
                <table class="dataTable" >
                    <thead>
                    <tr>
                        <th>Продукт</th>
                        <th>Актуальность</th>
                        <th>Мин. желтый</th>
                        <th>Мин. красный</th>
                        <th>Дней от СК</th>
                        <th>Дней у агента</th>
                    </tr>
                    </thead>
                    @if(sizeof($insurance_companies->type_bso))
                        @foreach($insurance_companies->type_bso as $type_bso)
                            <tr href="/directories/insurance_companies/{{$insurance_companies->id}}/type_bso/{{$type_bso->id}}/" class="clickable-row fancybox fancybox.iframe">
                                <td>{{ $type_bso->title  }}</td>
                                <td>{{ ($type_bso->is_actual==1)? trans('form.yes') :trans('form.no')  }}</td>
                                <td>{{ ($type_bso->min_yellow == 0)?'':$type_bso->min_yellow }}</td>
                                <td>{{ ($type_bso->min_red == 0)?'':$type_bso->min_red }}</td>
                                <td>{{ ($type_bso->day_sk == 0)?'':$type_bso->day_sk }}</td>
                                <td>{{ ($type_bso->day_agent == 0)?'':$type_bso->day_agent }}</td>
                            </tr>
                        @endforeach
                    @endif
                </table>
            @else
                {{ trans('form.empty') }}
            @endif
        </div>
    </div>
</div>