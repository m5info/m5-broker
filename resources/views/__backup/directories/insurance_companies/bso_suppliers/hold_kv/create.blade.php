@extends('layouts.frame')


@section('title')

    Удержание КВ

@endsection

@section('content')


    {{ Form::open(['url' => url("/directories/insurance_companies/{$insurance_companies->id}/bso_suppliers/{$bso_supplier->id}/hold_kv"), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('directories.insurance_companies.bso_suppliers.hold_kv.form')

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
