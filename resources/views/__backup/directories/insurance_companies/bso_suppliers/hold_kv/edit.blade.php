@extends('layouts.frame')


@section('title')

    Удержание КВ

@endsection

@section('content')

    {{ Form::model($hold_kv, ['url' => url("/directories/insurance_companies/{$insurance_companies->id}/bso_suppliers/{$bso_supplier->id}/hold_kv/{$hold_kv->id}/"), 'method' => 'put', 'class' => 'form-horizontal']) }}


    @include('directories.insurance_companies.bso_suppliers.hold_kv.form')


    <div class="form-group">

        <label class="col-sm-3 control-label" >Много файлов</label>
        <div class="col-sm-1">
            {{ Form::checkbox('is_many_files', 1, $hold_kv->is_many_files, ['class' => 'form-control', 'id'=>"is_many_files", "onchange"=>"setManyFiles()"]) }}
        </div>

        <label class="col-sm-3 control-label">Проверять договор</label>
        <div class="col-sm-1">
            {{ Form::checkbox('is_check_policy', 1, $hold_kv->is_check_policy, ['class' => 'form-control', 'required']) }}
        </div>

    </div>

    <div class="form-group">

        <div class="col-sm-12" >
            {{ Form::text('many_text', $hold_kv->many_text, ['class' => 'form-control', 'id'=>'many_text', "placeholder"=>"Перечень документов"]) }}
        </div>
    </div>

    <div class="row form-group">
        <table id="tab_files" class="dataTable">
            <tr>
                <td><center><strong>Документ</strong></center></td>
                <td><center><strong>Название для api</strong></center></td>
                <td><span class="btn btn-success pull-right" onclick="addFormFiles()"><i class="fa fa-plus"></i>Добавить</span></td>
            </tr>

            @if(sizeof($hold_kv->documents))

                @foreach($hold_kv->documents as $key => $document)
                    <tr id='file_tr_{{$key}}'>
                        <td><input id='file_title_{{$key}}' name='file_title[]' value='{{$document->file_title}}' class='form-control' type='text'></td>
                        <td><input id='file_name_{{$key}}' name='file_name[]' value='{{$document->file_name}}' class='form-control' type='text'></td>
                        <td><span class='btn btn-primary pull-right' onclick='delFormFiles({{$key}})'><i class='fa fa-minus'></i>Удалить</span></td>
                    </tr>
                @endforeach

            @endif

        </table>
    </div>

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection


@section('js')

    <script>

        $(function () {



            setManyFiles()



        });


        function setManyFiles()
        {

            if($("#is_many_files").is(':checked')){
                $("#many_text").show();
            }else{
                $("#many_text").hide();
            }

        }

        var MY_COUNT_FILES = "{{ ($hold_kv->documents)?count($hold_kv->documents):0}}";

        function addFormFiles(){

            tr = "<tr id='file_tr_"+MY_COUNT_FILES+"'>" +
                "<td><input id='file_title_"+MY_COUNT_FILES+"' name='file_title[]' value='' class='form-control' type='text'></td>" +
                "<td><input id='file_name_"+MY_COUNT_FILES+"' name='file_name[]' value='' class='form-control' type='text'></td>" +
                "<td><span class='btn btn-primary pull-right' onclick='delFormFiles("+MY_COUNT_FILES+")'><i class='fa fa-minus'></i>Удалить</span></td>" +
                "</tr>";

            $('#tab_files tr:last').after(tr);

            MY_COUNT_FILES = parseInt(MY_COUNT_FILES)+1;
        }

        function delFormFiles(id){
            $('#file_tr_'+id).remove();
        }


    </script>

@endsection