@extends('layouts.app')

@section('content')



        <div class="page-heading">
            <h1>{{ trans('menu.insurance_companies') }}</h1>
            <a class="btn btn-primary" href="{{ url('/directories/insurance_companies/0/')  }}">
                {{ trans('form.buttons.create') }}
            </a>
        </div>



        <div class="block-main">
        @if(sizeof($insurance_companies))
            <table class="noScrollTable">
                <thead>
                <tr>

                    <th>{{ trans('settings/banks.title') }}</th>
                    <th>{{ trans('settings/banks.is_actual') }}</th>
                    <th>Логотип</th>
                    <th></th>

                </tr>
                </thead>
                @foreach($insurance_companies as $sk)
                    <tr>

                        <td>{{ $sk->title }}</td>
                        <td>{{ ($sk->is_actual==1)? trans('form.yes') :trans('form.no') }}</td>
                        <td class="text-center">
                            @if($sk->logo_id)
                                <img src="{{ url($sk->logo->url) }}" width="154" height="44">
                            @endif
                        </td>
                        <td>
                            <a class="btn btn-primary" href="{{url ("/directories/insurance_companies/$sk->id/")}}">
                                Открыть
                            </a>

                        </td>
                    </tr>
                @endforeach

            </table>
        @else
            {{ trans('form.empty') }}
        @endif
        </div>


@endsection

