@extends('layouts.app')

@section('content')



        <div class="page-heading">
            <h1>{{ trans('menu.products') }}</h1>
            <a class="btn btn-primary fancybox fancybox.iframe" href="{{ url('/directories/products/create')  }}">
                {{ trans('form.buttons.create') }}
            </a>
        </div>



        <div class="block-main">
        @if(sizeof($products))
            <table class="noScrollTable">
                <thead>
                <tr>
                    <th>{{ trans('settings/banks.title') }}</th>
                    <th>{{ trans('settings/banks.is_actual') }}</th>

                    <th>Тип фин. политики</th>
                    <th>Категория</th>

                </tr>
                </thead>
                @foreach($products as $product)
                    <tr class="clickable-row fancybox fancybox.iframe" href="{{url ("/directories/products/$product->id/edit")}}">
                        <td>{{ $product->title }}</td>
                        <td>{{ ($product->is_actual==1)? trans('form.yes') :trans('form.no') }}</td>
                        <td>{{ \App\Models\Directories\Products::FIN_TYPE[$product->financial_policy_type_id] }}</td>
                        <td>{{ $product->category->title }}</td>
                    </tr>
                @endforeach

            </table>
        @else
            {{ trans('form.empty') }}
        @endif
        </div>


@endsection

