@extends('layouts.frame')

@section('title')

    {{ trans('menu.products') }}
    <span class="btn btn-info pull-right" onclick="openLogEvents('{{$product->id}}', 8, 0)"><i class="fa fa-history"></i> </span>


@endsection

@section('content')

    {{ Form::model($product, ['url' => url("/directories/products/$product->id"), 'method' => 'put', 'class' => 'form-horizontal']) }}

    @include('directories.products.form')

    {{Form::close()}}

@endsection

@section('footer')

    <button class="btn btn-danger pull-left" onclick="deleteItem('/directories/products/', '{{ $product->id }}')">{{ trans('form.buttons.delete') }}</button>

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

