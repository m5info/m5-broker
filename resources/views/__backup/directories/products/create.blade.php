@extends('layouts.frame')


@section('title')

    {{ trans('menu.products') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/directories/products'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('directories.products.form')

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
