
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/banks.is_actual') }}</label>
    <div class="col-sm-8">
        {{ Form::checkbox('is_actual', 1, old('is_actual')) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/banks.title') }}</label>
    <div class="col-sm-8">
        {{ Form::text('title', old('title'), ['class' => 'form-control', 'required']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">Тип фин. политики</label>
    <div class="col-sm-8">
        {{ Form::select('financial_policy_type_id', collect(\App\Models\Directories\Products::FIN_TYPE), old('financial_policy_type_id'), ['class' => 'form-control', 'required']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">Категория</label>
    <div class="col-sm-8">
        {{ Form::select('category_id', \App\Models\Directories\ProductsCategory::orderBy('sort', 'asc')->get()->pluck('title', 'id'), old('category_id'), ['class' => 'form-control', 'required']) }}
    </div>
</div>
