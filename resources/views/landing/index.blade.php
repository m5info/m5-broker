<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>InsPlay - новый продукт в страховнии</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="/landing/css/main.css">
    <link rel="stylesheet" type="text/css" href="/landing/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="/landing/slick/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="/landing/css/garage.css">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="apple-touch-icon-57x57.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="apple-touch-icon-60x60.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="apple-touch-icon-120x120.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="apple-touch-icon-76x76.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="apple-touch-icon-152x152.png"/>
    <link rel="icon" type="image/png" href="favicon-196x196.png" sizes="196x196"/>
    <link rel="icon" type="image/png" href="favicon-96x96.png" sizes="96x96"/>
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32"/>
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16"/>
    <link rel="icon" type="image/png" href="favicon-128.png" sizes="128x128"/>
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF"/>
    <meta name="msapplication-TileImage" content="mstile-144x144.png"/>
    <meta name="msapplication-square70x70logo" content="mstile-70x70.png"/>
    <meta name="msapplication-square150x150logo" content="mstile-150x150.png"/>
    <meta name="msapplication-wide310x150logo" content="mstile-310x150.png"/>
    <meta name="msapplication-square310x310logo" content="mstile-310x310.png"/>

</head>

<body>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter45042704 = new Ya.Metrika({
                    id:45042704,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45042704" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<nav class="navbar fixed-top navbar-toggleable-md navbar-light bg-faded">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand"><img src="/landing/i/logo (1).png" alt=""></a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" class="nav-link pseudo" href="/login">Личный кабинет</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#block-0">Услуги</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#block-1">Как это работает</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#block-2">Партнерам</a>
            </li>
        </ul>
    </div>
</nav>

<section class="teaser" id="block-0">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6  text-center text-sm-left">
                <h2>Застрахуй семью и друзей!</h2>
                <p>Скачивай приложение и сделай страховку играючи.</p>
                <a class="btn btn-secondary" href="/login">Заказать осмотр</a>
            </div>
        </div>
    </div>
</section>
<section class="block block-1" id="block-1">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 mb-5">
                <h2 class="text-center">Как это работает</h2>
            </div>
            <div class="col-md-4 text-center">
                <div class="mb-5">
                    <img src="/landing/i/block-2-1.svg" class="img-fluid col-4 mb-3" alt="">
                    <h4>Продукты</h4>
                    <p>Мы оказываем такие услуги как: ОСАГО, КАСКО, Диагностические карты, Сервисные карты, Страхование недвижимости, ВЗР, Медицина, Продленная гарантия</p>
                </div>
                <div class="mb-5">
                    <img src="/landing/i/block-2-2.svg" class="img-fluid col-4 mb-3" alt="">
                    <h4>Приведи друзей</h4>
                    <p>
                        Приведи 10 друзей и получи скидку на свои покупки

                    </p>
                </div>
            </div>
            <div class="col-md-4 text-center mb-5">
                <img src="http://insplay.ru/landing/i/credit.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-4 text-center">
                <div class="mb-5">
                    <img src="/landing/i/block-2-3.svg" class="img-fluid col-4 mb-3" alt="">
                    <h4>Получай бонусы</h4>
                    <p>Получай бонусы с каждой покупки своих друзей</p>
                </div>
                <div class="mb-5">
                    <img src="/landing/i/block-2-4.svg" class="img-fluid col-4 mb-3" alt="">
                    <h4>Пополнение баланса</h4>
                    <p>Пополняй свой баланс через QIWI Кошелек или Яндекс деньги и оплачивай продукты баллами</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="block block-2" id="block-2">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-5">
                <h2 class="text-center" style="color: black;">iFrame который Вы можету установить у себя на сайте</h2>
            </div>

            <div class="col-12" >
                <div class="container">

                    <div class="row">
                        <div class="col-sm-6 col-lg-6 wow slideInUp" style="visibility: visible; animation-name: slideInUp;">
                            <div class="service-item ">
                                <a class="service-link" href="http://garage-studio.pro/%d0%b2%d1%8b%d0%b5%d0%b7%d0%b4%d0%bd%d0%b0%d1%8f-%d0%b4%d0%b8%d0%b0%d0%b3%d0%bd%d0%be%d1%81%d1%82%d0%b8%d0%ba%d0%b0/"><span class="screen-reader-text">Выездная диагностика</span></a>
                                <div class="service-image icon-image"><img src="http://garage-studio.pro/wp-content/uploads/2017/03/car-placeholder.jpg" alt=""></div>                                    <div class="service-content">
                                    <h4 class="service-title">Выездная диагностика</h4>
                                    <p class="service-p">Детальный осмотр автомобиля профессионалами своего дела. Выезд по городу и региону.В услугу входит:– первичный осмотр автомобиля– проверка юридической чистоты документов– проверка кузова и ЛКП на повреждения и наличия ремонта– диагностика …</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-6 wow slideInUp" style="visibility: visible; animation-name: slideInUp;">
                            <div class="service-item ">
                                <a class="service-link" href="http://garage-studio.pro/%d1%8d%d0%ba%d1%81%d0%bf%d0%b5%d1%80%d1%82-%d0%bd%d0%b0-%d0%b4%d0%b5%d0%bd%d1%8c-2/"><span class="screen-reader-text">Эксперт на день</span></a>
                                <div class="service-image icon-image"><img src="http://garage-studio.pro/wp-content/uploads/2017/03/rent-a-car.jpg" alt=""></div>                                    <div class="service-content">
                                    <h4 class="service-title">Эксперт на день</h4>
                                    <p class="service-p">Осмотр автоэкспертом неограниченного количества автомобилей в течении дня в любом месте!Крайне удобная услуга, чтобы за день осмотреть несколько автомобилей в разных местах или городах.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-6 clearleft wow slideInUp" style="visibility: visible; animation-name: slideInUp;">
                            <div class="service-item ">
                                <a class="service-link" href="http://garage-studio.pro/%d1%8e%d1%80%d0%b8%d0%b4%d0%b8%d1%87%d0%b5%d1%81%d0%ba%d0%b0%d1%8f-%d0%bf%d1%80%d0%be%d0%b2%d0%b5%d1%80%d0%ba%d0%b0-%d0%b4%d0%be%d0%ba%d1%83%d0%bc%d0%b5%d0%bd%d1%82%d0%be%d0%b2/"><span class="screen-reader-text">Юридическая проверка документов</span></a>
                                <div class="service-image icon-image"><img src="http://garage-studio.pro/wp-content/uploads/2017/03/verification-of-delivery-list-clipboard-symbol.jpg" alt=""></div>                                    <div class="service-content">
                                    <h4 class="service-title">Юридическая проверка документов</h4>
                                    <p class="service-p">Юридическая проверка документовВсесторонняя юридическая проверка автомобиля на предмет участия в:&nbsp;ДТП,&nbsp;залогов,&nbsp;запретов,&nbsp;арестов,&nbsp;угонов,&nbsp;и других ограничений.&nbsp;Полный отчет за 5 минут! Экспресс – проверка: при оплате услуги онлайн мы присылаем отчет в течении 5 мин!! …</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-6 wow slideInUp" style="visibility: visible; animation-name: slideInUp;">
                            <div class="service-item ">
                                <a class="service-link" href="http://garage-studio.pro/%d0%b4%d0%ba%d0%bf-24-%d1%87%d0%b0%d1%81%d0%b0/"><span class="screen-reader-text">ДКП 24 часа</span></a>
                                <div class="service-image icon-image"><img src="http://garage-studio.pro/wp-content/uploads/2017/03/text-documents.jpg" alt=""></div>                                    <div class="service-content">
                                    <h4 class="service-title">ДКП 24 часа</h4>
                                    <p class="service-p">Оформление договора купли/продажи автоОформление договора купли – продажи автомобиля КРУГЛОСУТОЧНО в Новокузнецке с выездом. По предварительному звонку. Помощь в оформлении полного пакета документов. Сопровождение в ГИБДД.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<section class="block block-3" id="block-3">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-5">
                <h2 class="text-center">Экраны приложения</h2>
            </div>
            <div class="responsive slider">
                <!-- slider -->
                <div><img src="http://insplay.ru/landing/i/slide-1.jpg" class="img-fluid" alt="">
                    <p class="slider-text text-center mt-3">Меню</p>
                </div>
                <div><img src="http://insplay.ru/landing/i/slide-2.jpg" class="img-fluid" alt="">
                    <p class="slider-text text-center mt-3">Калькулятор ОСАГО</p>
                </div>
                <div><img src="http://insplay.ru/landing/i/slide-3.jpg" class="img-fluid" alt="">
                    <p class="slider-text text-center mt-3">Формирование заявки</p>
                </div>
                <div><img src="http://insplay.ru/landing/i/slide-4.jpg" class="img-fluid" alt="">
                    <p class="slider-text text-center mt-3">Выполненная заявка</p>
                </div>
                <div><img src="http://insplay.ru/landing/i/slide-5.jpg" class="img-fluid" alt="">
                    <p class="slider-text text-center mt-3">Баланс</p>
                </div>
                <div><img src="http://insplay.ru/landing/i/slide-6.jpg" class="img-fluid" alt="">
                    <p class="slider-text text-center mt-3">Чат с оформителем</p>
                </div>
                <!-- /slider -->
            </div>
        </div>
    </div>
</section>

<section class="block block-4" id="block-4">
    <div class="container">
        <div class="section-title-area">
            <h5 class="section-subtitle">пока ваш автомобиль не купил кто-то другой</h5>                    <h2 class="section-title">оформите заявку</h2>                                    </div>
        <div class="row">
            <div class="contact-form col-sm-6 wow slideInUp" style="visibility: visible; animation-name: slideInUp;">
                <div role="form" class="wpcf7" id="wpcf7-f6-o1" lang="en-US" dir="ltr">
                    <div class="screen-reader-response"></div>
                    <form action="/#wpcf7-f6-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                        <div style="display: none;">
                            <input type="hidden" name="_wpcf7" value="6">
                            <input type="hidden" name="_wpcf7_version" value="4.6.1">
                            <input type="hidden" name="_wpcf7_locale" value="en_US">
                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f6-o1">
                            <input type="hidden" name="_wpnonce" value="37205278e7">
                        </div>
                        <p>Ваше имя<br>
                            <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span> </p>
                        <p>Ваш e-mail (обязательно)<br>
                            <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </p>
                        <p>Тема<br>
                            <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span> </p>
                        <p>Сообщение<br>
                            <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </p>
                        <div class="g-recaptcha" data-sitekey="6LcrCEAUAAAAAGg3tNNwSgUG2pyiWk3PO3brxNmA"><div style="width: 304px; height: 78px;"><div><iframe src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LcrCEAUAAAAAGg3tNNwSgUG2pyiWk3PO3brxNmA&amp;co=aHR0cDovL2dhcmFnZS1zdHVkaW8ucHJvOjgw&amp;hl=ru&amp;v=vJuUWXolyYJx1oqUVmpPuryQ&amp;size=normal&amp;cb=ub986pbzlx04" width="304" height="78" role="presentation" name="a-15it1f979q1d" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe></div><textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea></div></div>
                        <p><input type="submit" value="Отправить голубями" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span></p>
                        <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>                            </div>

            <div class="col-sm-6 wow slideInUp" style="visibility: visible; animation-name: slideInUp;">
                <br>
                <h4>Мы с удовольствием читаем Ваши письма</h4>
                Напишите нам всё максимально подробно, либо просто скиньте ссылку на объявление и телефон. Мы свяжемся с Вами, посмотрим автомобиль и расскажем всё, что узнали! Зачастую, хорошие варианты продаются за часы. Не упускайте свой шанс - звоните!                        <br><br>

                <div class="address-box">

                    <h3>Позвонить - быстрее!</h3>


                    <div class="address-contact">
                        <span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-phone fa-stack-1x fa-inverse"></i></span>

                        <div class="address-content">89617244666</div>
                    </div>

                    <div class="address-contact">
                        <span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i></span>

                        <div class="address-content"><a href="mailto:garageauto42@gmail.com">garageauto42@gmail.com</a></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="block-app" id="download">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 text-center text-sm-left" style="color: black;">
                <h2>Наша система InsCar24</h2>
                <p>Присоединяйся к игрокам и ты поймешь что посредники не нужны</p>

                <div style="font-size: 10px;line-height: 0px;">
                    <h4 data-menu="welcome">Реквизиты</h4>
                    <p><b>Общество с ограниченной ответственностью «СТРАХОВАЯ ПЛАТФОРМА» </b></p>
                    <p><b>Юридический адрес:</b>129090, г. Москва, Проспект Мира д. 16, стр. 2, пом.II. комн.3.</p>
                    <p><b>ИНН / КПП:</b>7702421387 / 770201001</p>
                    <p><b>Банк:</b> ПАО АКБ «АВАНГАРД»</p>
                    <p><b>Расчетный счет:</b> 40702810300120033933</p>
                    <p><b>Корреспондентский счет:</b> 30101810000000000201</p>
                    <p><b>БИК:</b> 044525201</p>
                    <p><b>ОГРН:</b> 1177746843438</p>
                    <p><b>Email:</b> ihelp@insplay.ru</p>
                    <p><b>Телефон:</b> +7 495 247-89-02 </p>
                </div>

            </div>
            <div class="col-md-3 offset-md-2 flex-first flex-md-last">
                <img src="http://insplay.ru/landing/i/block-app.png" class="img-fluid" alt="">
            </div>


        </div>
    </div>
</section>
<section class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3 text-center text-sm-left">
                &copy; 2017 InsPlay
            </div>

            <div class="col-12 col-md-6 text-center text-sm-left">
                <a class="pseudo" href="http://insplay.ru/privacy-policy">Политика конфиденциальности</a>
                <a class="pseudo" data-toggle="modal" data-target="#support">Написать в техподдержку</a>
            </div>

            <div class="col-12 col-md-3 phone text-center text-sm-right">
                +7 495 247-89-02
            </div>

        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="support" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Написать в техподдержку:</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="support-form">
                    <div class="form-group">
                        <label for="contact-text" class="form-control-label">Телефон/email:</label>
                        <input type="text" class="form-control" id="contact-text"/>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="form-control-label">Сообщение:</label>
                        <textarea class="form-control" id="message-text"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary" onclick="submitSupportForm()">Отправить</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade large" id="segments" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Сегменты</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <table class="table">
                        <tr>
                            <th width="50%">Город</th>
                            <td width="50%" class="col-md-6">
                                <select class="form-control" onChange="loadSegments()" name="location_id"><option value="" selected="selected">Все</option><option value="1">Москва</option><option value="2">Московская область</option><option value="3">Санкт-Петербург</option><option value="1761">Ленинградская область</option><option value="1815">Брянск и область</option><option value="1758">Вологда и область</option><option value="1835">Екатеринбург и область</option><option value="9">Калуга и область</option><option value="12">Курск и область</option><option value="1890">Орел и область</option><option value="16">Оренбург и область</option><option value="1754">Тула и область</option></select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content">

                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script src='https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js'></script>
<script src="http://insplay.ru/landing/slick/slick.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    $('.responsive').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        arrows: false,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
</script>
<script type="text/javascript">
    $('a').click(function () {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 500);
        return false;
    });

    $(function () {
        var hash = getHash();

        if (hash == 'download') {
            $('html, body').animate({
                scrollTop: $("#download").offset().top
            }, 1000);
        }

        if (hash == 'agree') {
            $('#agree').modal('show');
        }


    });

    function getHash() {
        return window.location.hash.substr(1);
    }

    function submitSupportForm() {
        var message = $('#message-text').val();
        var contact = $('#contact-text').val();
        if (!message || !contact) {
            return false;
        }
        $.post('http://insplay.ru/support', {
            message : message,
            contact : contact,
            _token: '4sVeOkAY0w9T92mUIAZCLYg0QKZ4ORWI2Act2i35'
        }, function () {
            $('#support').modal('hide');
            $('#message-text').val('');
        });
    }

</script>
<script>
    $(function () {
        loadSegments();

    });

    function loadSegments() {
        var segments = $.get('http://insplay.ru/segments', {
            location_id: $('[name=location_id]').val()
        }, function (response) {
            $('#segments').find('.content').html(response);
        });
    }
</script>
</body>

</html>
