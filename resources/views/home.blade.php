@extends('layouts.app')

@section('head')

@append

@section('content')

    <h1>Добро пожаловать!</h1>
    <div class="row">
        <div class="col-sm-8">
            <div class="col-sm-12">
                <ul class="nav nav-buttons inline-block-li" style="margin-bottom: 15px;">
                    @foreach($balances as $key => $balance)
                        <li>
                            <a class="balances-links @if($key == 0) active @endif"
                               data-id="{{$balance->id}}">{{$balance->title}}
                            </a>
                        </li>
                    @endforeach


                </ul>

                <div class="inline-block total_sum_block_sub pull-right" style="margin-top: -45px;">
                    <span style="display: inline-block">
                        <label for="">Долг: </label>
                        <a class="bold" href="/finance/debts/{{auth()->id()}}/detail">{{titleFloatFormat($debt)}}  ₽</a>
                    </span>
                </div>



            </div>
            <div class="col-sm-12">
                <h2>Детализация: <span id="select_balance_name"></span></h2>
                <div class="block-main">
                    <div class="block-sub" style="min-height: 162px;">
                        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">


                            <div class="col-sm-3">
                                <label for="">Тип</label>
                                {{ Form::select('event_type_id', collect(\App\Models\Users\UsersBalance::EVENT_TYPE_ID)->prepend('Все', -1), -1, ['class' => 'form-control select2-ws', 'onchange' => 'loadItemsBalance()'])}}
                            </div>

                            <div class="col-sm-3">
                                <label for="">Вид</label>
                                {{ Form::select('type_id', collect(\App\Models\Users\UsersBalance::TYPE_ID)->prepend('Все', -1), -1, ['class' => 'form-control select2-ws', 'onchange' => 'loadItemsBalance()'])}}
                            </div>

                            <div class="col-sm-3">
                                <label for="">Дата начала</label>
                                {{ Form::input('text','date_from', date('d.m.Y', strtotime("-1 months")), ['class' => 'form-control date datepicker date_from', 'placeholder' => 'Транзации с', 'onchange' => 'loadItemsBalance()']) }}
                            </div>
                            <div class="col-sm-3">
                                <label for="">Дата окончания</label>
                                {{ Form::input('text','date_to', date('d.m.Y'), ['class' => 'form-control date datepicker date_from', 'placeholder' => 'Транзации по', 'onchange' => 'loadItemsBalance()']) }}
                            </div>





                        </div>

                        <div class="row" id="table_transactions" style="overflow-y: auto; max-height: 300px;">
                            {{-- JS --}}
                        </div>

                    </div>
                </div>
            </div>
            <div style="margin-left: 15px;">
                <div class="clearfix"></div>
                <h2 style="margin-top: 30px;">Базовые ставки</h2>
                <br>

                <div class="block-main">
                    <div class="block-sub">


                        @if(\App\Models\Settings\BaseRate::all()->isNotEmpty())
                            <table class="table" style="width: 100%; overflow-x: auto; ">
                                <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Страховая компания</td>
                                    @foreach(\App\Models\Settings\BaseRate::groupBy('city_id')->get() as $base_rate)
                                        <td>{{$base_rate->city->title}}</td>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>

                                @foreach(\App\Models\Settings\BaseRate::groupBy('company_id')->get() as $key => $base_rate_company)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{ $base_rate_company->company ? $base_rate_company->company->title : ''}}</td>

                                        @foreach(\App\Models\Settings\BaseRate::groupBy('city_id')->get() as $base_rate)
                                            <td>
                                                {{\App\Models\Settings\BaseRate::getBaseRateTitle($base_rate->city_id, $base_rate_company->company_id)}}
                                            </td>
                                        @endforeach
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        @else
                            <p style="margin-top: 10px;">Базовые ставки не настроены</p>
                        @endif

                    </div>
                </div>


            </div>


        </div>
        <div class="col-sm-4">


            <div class="col-sm-12">
            <div class="documents-info" >

                @if($actual_segment)
                    <div class="form-control-half" style="height: auto;">
                        <p style="text-align: center;" class="form-control">Актуально
                            с {{$actual_segment->actual_from ? setDateTimeFormatRu($actual_segment->actual_from, 1) : ''}}</p>
                        <a href="{{'/files/'}}{{$actual_segment->file ? $actual_segment->file->name : ''}}"
                           class="btn btn-success form-control">
                            Скачать сегменты
                        </a>
                    </div>
                @endif

                @if($actual_kv)
                    <div class="form-control-half" style="height: auto;">
                        <p style="text-align: center;" class="form-control">Актуально
                            с {{$actual_kv->actual_from ? setDateTimeFormatRu($actual_kv->actual_from, 1) : ''}}</p>
                        <a href="{{'/files/'}}{{$actual_kv->file ? $actual_kv->file->name : ''}}"
                           class="btn btn-success form-control">
                            Скачать КВ
                        </a>
                    </div>
                @endif

            </div>

            </div>

            <div class="col-sm-12">
                <h2 style="margin: 0 0 20px 0">Новости</h2>
            @if(!empty($news))
                <div class="wrapper-news">
                    @foreach($news as $new)
                        <div class="container-news">
                            <div class="img-container" onclick="location.href = '/informing/news/{{$new->id}}'">
                                <span class="_img-span"><i class="ico-time"></i>{{setDateTimeFormatRu($new->created_at)}}</span>
                                <h2 class="img-header" style="padding-top: 20px;">
                                    <p>{{$new->header}}</p>
                                </h2>
                                <p class="img-content">
                                    {{ $new->short_desc }}
                                </p>
                            </div>
                        </div>
                        <br>
                    @endforeach
                </div>
            @else
                <p>На данный момент новостей нет..</p>
            @endif

            </div>
        </div>
    </div>

    @if (session('login-success') && !count($errors))
        @include('partials.success-login')
    @endif

    <style>
        button.close_button_filter{
            background: transparent;
            border: none;
            color: red;
        }

        .container-news{
            padding: 15px;
        }

        .container-news .img-header p{
            display: inline-block;
            font-size: 20px;
            font-weight: 600;
            color: #6d6d6d;
        }

        .container-news ._img-span{
            font-size: 14px;
        }

        .container-news ._img-span .ico-time{
            font-size: 14px;
        }
    </style>
@endsection

@section('js')

@endsection

@section('js_home')
{{--
    При загрузке страницы после авторизации - @section('js') не работает,
    для этого юзается @section('js_home')
 --}}
    <script>

        function loadItemsBalance() {


            getTable();

        }

        function getData() {

            data = {};

            data.date_from = $('[name = "date_from"]').val();
            data.date_to = $('[name = "date_to"]').val();
            data.type_id = $('[name = "type_id"]').val();
            data.event_type_id = $('[name = "event_type_id"]').val();
            if ($('.balances-links.active').length) {
                data.balance = ($('.balances-links.active')[0]).getAttribute('data-id');
            }

            return data;
        }

        function getTable() {

            $.get({
                url: '/get_transaction_table',
                data: getData(),
                success: function (res) {
                    $('#table_transactions').html(res);
                }
            });

        }

        $(function () {
            //getTable();
            $(".balances-links.active").click();
        });

        $(document).on('click', '.balances-links', function () {

            data_id = $(this).data('id');
            $('.balances-links').removeClass('active');
            $('.balances-links').removeAttr();
            $(this).addClass('active');

            $('#select_balance_name').html($(this).html());


            loadItemsBalance();

        });
    </script>
@endsection

