@extends('layouts.frame')


@section('title')

    Страховая ставка по городу {{$base_rate->city ? $base_rate->city->title : ''}}

@endsection

@section('content')


    {{ Form::open(['url' => url("/directories/insurance_companies/{$insurance_company_id}/base_rates/{$base_rate->id}/update"), 'id' => 'form_rate', 'method' => 'post', 'class' => 'form-horizontal']) }}

    <input type="hidden" id="bso_supplier_id" value=''/>
    <div class="form-group">
        <label class="col-sm-3 control-label">Ставка</label>
        <div class="col-sm-9">
            <input type="text" name="rate" value="{{titleFloatFormat($base_rate->rate, 2)}}" class="form-control sum">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Город</label>
        <div class="col-sm-9">
            {{Form::select('city_id', collect(\App\Models\Settings\City::all()->pluck('title','id')), $base_rate->city_id, ['class' => 'form-control'])}}
        </div>
    </div>

    {{Form::close()}}


@endsection

@section('footer')


    <button id="deleteRate" data-city="{{$base_rate->city ? $base_rate->city->title : ''}}" type="button"
            class="btn btn-danger pull-left">{{ trans('form.buttons.delete') }}</button>

    <button id="updateRate" type="button" class="btn btn-primary pull-right">{{ trans('form.buttons.save') }}</button>


@endsection

@section('js')

    <script>

        $(function () {
            $(document).on('click', '#updateRate', function(){
                res = myPostAjax("/directories/insurance_companies/{{$insurance_company_id}}/base_rates/{{$base_rate->id}}/update", $('#form_rate').serialize());
                if (res == 200){
                    parent.reload();
                }

            });

            $(document).on('click', '#deleteRate', function () {
                answer = confirm('Удалить страховую ставку по городу ' + $(this).data('city'));
                if (answer) {
                    $.post({
                        url: "/directories/insurance_companies/{{$insurance_company_id}}/base_rates/{{$base_rate->id}}/delete",
                        data: {
                            id: '{{$base_rate->id}}'
                        },
                        success: function (res) {
                            parent.reload();
                        }
                    })
                }
            });
        });


    </script>


@endsection
