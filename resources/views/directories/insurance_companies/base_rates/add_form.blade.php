@extends('layouts.frame')


@section('title')

    Добавить базовую ставку

@endsection

@section('content')


    {{ Form::open(['url' => url("/directories/insurance_companies/{$insurance_company_id}/base_rates/add"), 'name' => 'createRateForm', 'method' => 'post', 'class' => 'form-horizontal']) }}

    <input type="hidden" id="bso_supplier_id" value=''/>

    <div class="form-group">
        <label class="col-sm-3 control-label">Ставка</label>
        <div class="col-sm-9">
            <input type="text" name="rate" class="form-control sum">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Город</label>
        <div class="col-sm-9">
            {{Form::select('city_id', collect(\App\Models\Settings\City::all()->pluck('title','id')), 0, ['class' => 'form-control'])}}
        </div>
    </div>


    {{Form::close()}}


@endsection

@section('footer')

    <button id="createBaseRate" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

@section('js')
    <script>
        $(document).on('click', '#createBaseRate', function(){
            data = $('[name = "createRateForm"]').serialize();
            res = myPostAjax("/directories/insurance_companies/{{$insurance_company_id}}/base_rates/add", data);
            if (res == 200){
                parent.reload();
            }
        })
    </script>
@endsection