@if($financial_policy)
    <table class="tov-table">
        <tbody>
        <tr>
            <th>Продукт</th>
            <th>Дата действия</th>
            <th>Название</th>
            @isset($fix_price)
                @if($fix_price)
                    <th>Себестоимость</th>
                @else
                    <th>КВ Бордеро</th>
                    <th>КВ Двоу</th>
                    <th>КВ СК</th>
                @endif
            @endisset

            <th>КВ Агента / Менеджера</th>
            <th>КВ Руководителя</th>
            <th>Актуально</th>
        </tr>
        @if(sizeof($financial_policy))
            @foreach($financial_policy as $finPolicy)
                <tr class="clickable-row"
                    {{ ($finPolicy->is_actual!=1) ? 'style=background-color:#ffcccc' : ''}} data-href="{{url("/directories/insurance_companies/{$insurance_companies->id}/bso_suppliers/{$bso_supplier->id}/financial_policy/{$finPolicy->id}/")}}">
                    <td>{{ $finPolicy->product ? $finPolicy->product->title : "Все"  }}</td>
                    <td>{{ setDateTimeFormatRu($finPolicy->date_active, 1) }}</td>
                    <td>{{ $finPolicy->title }}</td>
                    @isset($fix_price)
                        @if($fix_price)
                            <td>{{ $finPolicy->fix_price_sum ? getPriceFormat($finPolicy->fix_price_sum) : '0.00' }}</td>
                        @else
                            <td>{{ titleFloatFormat($finPolicy->kv_bordereau) }}</td>
                            <td>{{ titleFloatFormat($finPolicy->kv_dvou) }}</td>
                            <td>{{ titleFloatFormat($finPolicy->kv_sk) }}</td>
                        @endif
                    @endisset

                    <td>{{ titleFloatFormat($finPolicy->kv_agent) }}</td>
                    <td>{{ titleFloatFormat($finPolicy->kv_parent) }}</td>
                    <td>{{($finPolicy->is_actual==1)? trans('form.yes') :trans('form.no')}}</td>
                </tr>
            @endforeach
        @endif
        </tbody>

    </table>
@else
    {{ trans('form.empty') }}
@endif