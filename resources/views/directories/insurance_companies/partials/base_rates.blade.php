<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-top: 10px;">
    <div class="page-subheading">
        <h2 class="inline-h1"> Базовые ставки </h2>
        <a onclick="openFancyBoxFrame_customHeight('/directories/insurance_companies/{{$insurance_companies->id}}/base_rates/add', 400)"
           class="btn btn-primary pull-right">
            {{ trans('form.buttons.add') }}
        </a>
    </div>


    <div class="block-main">
        <div class="block-sub">
            @if(!$insurance_companies->base_rates->isEmpty())
                <table class="tov-table" >
                    <tbody>
                    <tr>
                        <th>Город</th>
                        <th>Ставка</th>
                    </tr>
                    @if(sizeof($insurance_companies->base_rates))
                        @foreach($insurance_companies->base_rates as $rate)
                            <tr class="clickable-row" onclick="openFancyBoxFrame_customHeight('/directories/insurance_companies/{{$insurance_companies->id}}/base_rates/{{$rate->id}}/edit', 400)">
                                <td>{{$rate->city ? $rate->city->title : ''}}</td>
                                <td>{{titleFloatFormat($rate->rate)}}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>

                </table>
            @else
                <p style="margin-top: 10px;">Пока не указана ни одна базовая ставка</p>
            @endif
        </div>
    </div>



</div>