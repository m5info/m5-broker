<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-subheading">
        <h2 class="inline-h1">Финансовые политики</h2>
        <a href="{{url("/directories/insurance_companies/{$insurance_companies->id}/bso_suppliers/{$bso_supplier->id}/financial_policy/0/")}}" class="btn btn-primary btn-right">
            {{ trans('form.buttons.add') }}
        </a>
    </div>
    <div class="block-main">
        <div class="block-sub ">
            <div>
                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="product">Продукт</label>
                    @php
                        $products = \App\Models\Directories\Products::query()->where('is_actual', 1)->get();
                        $products->map(function ($item){
                            $item->title = mb_substr($item->title, 0, 100);
                            return $item;
                        });
                    @endphp
                    {{ Form::select('product', $products->pluck('title', 'id')->prepend('Все', 0), request('product'), ['class' => 'form-control select2-ws',  'onchange'=>'loadDataFP()']) }}
                </div>
                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="product">Актуально</label>
                    {{ Form::select('is_actual_fp', collect([0 => 'Нет', 1 => 'Да']), 1, ['class' => 'form-control select2-ws',  'onchange'=>'loadDataFP()']) }}
                </div>
                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="product">Фиксированная стоимость</label>
                    {{ Form::select('fix_price', collect([0 => 'Нет', 1 => 'Да']), 0, ['class' => 'form-control select2-ws',  'onchange'=>'loadDataFP()']) }}
                </div>
            </div>
            <div class="clear"></div>
            <div class="financial-policy-table" style="margin-top: 10px;">
                @include('directories.insurance_companies.partials.financial_policy_table', ['financial_policy' => $bso_supplier->financial_policy])
            </div>
        </div>
    </div>
</div>
<script>


    function loadDataFP(){
        $.post("{{url("/directories/insurance_companies/{$insurance_companies->id}/bso_suppliers/{$bso_supplier->id}/financial_policy/get_table")}}", getData(), function(response){
            $('.financial-policy-table').html(response);

            $(".clickable-row").click(function () {
                if ($(this).attr('data-href')) {
                    window.location = $(this).attr('data-href');
                }
            });


        });
    }

    function getData(){
        return {
            is_actual_fp: $('[name="is_actual_fp"]').val(),
            product: $('[name="product"]').val(),
            fix_price: $('[name="fix_price"]').val(),
        }
    }
</script>