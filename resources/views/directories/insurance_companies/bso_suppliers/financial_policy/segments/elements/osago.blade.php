    <div class="row">
        <div class="col-md-6">

            <div class="panel panel-default panel-outer">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label class="title segments-title" style="margin-top: 10px;">
                                        Страхователь
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="panel-body segments-padding">
                    <div class="col-lg-12">
                        <label class="pull-left">Тип страхователя</label>
                        <label class="pull-right">Любой
                            {{Form::checkbox('data[insurer_type_any]', 1, $data['insurer_type_any'])}}
                        </label>
                        {{Form::select('data[insurer_type_id]', [0 =>'Физ лицо', 1=>'Юр лицо'], $data['insurer_type_id'], ['class' => 'form-control insurer_type_any'])}}
                    </div>
                    <div class="col-lg-12 mt-15">
                        <label class="pull-left ">Адрес прописки собственника</label>
                        <label class="pull-right">Любой {{Form::checkbox('data[insurer_location_any]', 1, $data['insurer_location_any'])}}</label>
                        {{Form::select('data[location_id]', \App\Models\Settings\City::all()->pluck('title', 'id'), $data['location_id'], ['class' => 'form-control insurer_location_any'])}}
                    </div>
                    <div class="col-lg-12 mt-15">
                        <label class="pull-left">Коэффициент территории</label>
                        <label class="pull-right">Любой {{Form::checkbox('data[insurer_kt_any]', 1, $data['insurer_kt_any'])}}</label>
                        <input type="text" class="form-control sum insurer_kt_any" name="data[insurer_kt]"
                               value="{{$data['insurer_kt']}}">
                    </div>

                </div>

            </div>


        </div>
        <div class="col-md-6">


            <div class="panel panel-default panel-outer">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label class="title segments-title">
                                        Договор
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="panel-body segments-padding">

                    <div class="col-lg-12">
                        <label class="pull-left">Тип</label>
                        <label class="pull-right">Любой {{Form::checkbox('data[contract_type_any]', 1, $data['contract_type_any'])}}</label>
                        {{Form::select('data[contract_type_id]', collect([0 => 'Новый', 1 => 'Пролонгация']), $data['contract_type_id'], ['class' => 'form-control contract_type_any'])}}
                    </div>
                    <div class="col-lg-12 mt-15">
                        <label class="pull-left">Период использования</label>
                        <label class="pull-right">Любой {{Form::checkbox('data[period_any]', 1, $data['period_any'])}}</label>
                        {{Form::select('data[period]', [12 => 'Год', 11 => 'Менее года'], $data['period'], ['class' => 'form-control period_any'])}}
                    </div>
                    <div class="col-lg-12 mt-15">
                        <label class="pull-left">КБМ</label>
                        <label class="pull-right">Любой {{Form::checkbox('data[kbm_any]', 1, $data['kbm_any'])}}</label>
                        <input type="text" class="form-control sum kbm_any" name="data[kbm]" value="{{$data['kbm']}}">
                    </div>
                </div>

            </div>


        </div>
    </div>


    <div class="row">
        <div class="col-md-6">

            <div class="panel panel-default panel-outer">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label class="title segments-title">
                                        ТС
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="panel-body segments-padding">
                    <div class="row col-md-12">
                        <div class="col-lg-6">
                            <label class="pull-left">Категория ТС</label>
                            {{Form::select('data[vehicle_category_id]', \App\Models\Vehicle\VehicleCategories::all()->pluck('title', 'id'), $data['vehicle_category_id'], ['class' => 'form-control'])}}
                        </div>
                        <div class="col-lg-6 mt-15">
                            <label class="pull-left">Тип ТС</label>
                            <label class="pull-right">Любой {{Form::checkbox('data[vehicle_country_any]', 1, $data['vehicle_country_any'])}}</label>
                            {{Form::select('data[vehicle_country_id]', collect([1 => 'Иномарка', 2 => 'Отечественное']), $data['vehicle_country_id'], ['class' => 'form-control vehicle_country_any'])}}
                        </div>

                    </div>

                    <div class="row col-md-12" style="margin-top: 10px;">
                        <div class="col-md-4" style="min-height: 25px;">
                            <label class="pull-left">Мощность ТС
                                (Любая {{Form::checkbox('data[vehicle_power_any]', 1, $data['vehicle_power_any'])}})</label>
                        </div>
                        <div class="col-md-1 vehicle_power_any">
                            <label class="pull-left">от > </label>
                        </div>
                        <div class="col-md-3 vehicle_power_any">
                            <input type="text" class="form-control" value="{{$data['vehicle_power_from']}}"
                                   name="data[vehicle_power_from]">
                        </div>
                        <div class="col-md-1 vehicle_power_any">
                            <label class="pull-left">до</label>
                        </div>
                        <div class="col-md-3 vehicle_power_any">
                            <input type="text" class="form-control" value="{{$data['vehicle_power_to']}}"
                                   name="data[vehicle_power_to]">
                        </div>
                    </div>

                    <div class="row col-md-12" style="margin-top: 10px;">
                        <div class="col-lg-4 ">
                            <label class="pull-left">Прицеп (Не важно {{Form::checkbox('data[has_trailer_any]', 1, $data['has_trailer_any'])}})
                                <span class="has_trailer_any">Да {{Form::checkbox('data[has_trailer]', 1, $data['has_trailer'])}}</span></label>
                        </div>
                        <br><br>
                        <div class="col-md-4" style="min-height: 25px;">
                            <label class="pull-left">Возраст ТС (Любой {{Form::checkbox('data[vehicle_age_any]', 1, $data['vehicle_age_any'])}}
                                ) до < </label>
                        </div>
                        <div class="col-md-4 vehicle_age_any">
                            <input type="text" class="form-control" value="{{$data['vehicle_age']}}"
                                   name="data[vehicle_age]">
                        </div>
                    </div>

                </div>

            </div>


        </div>
        <div class="col-md-6">


            <div class="panel panel-default panel-outer">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-lg-8 col-sm-12 col-xs-12 col-md-12" style="padding-right: 8.7%;">
                            <div class="segments-title ">
                                <label class="title segments-color">
                                    Водители - неважно {{Form::checkbox('data[drivers_age_any]', 1, $data['drivers_age_any'])}}
                                </label>

                                <label class="title pull-right " style="margin-left: 20px;">

                                    <label class="segments-color">Мультидрайв</label>
                                    {{Form::checkbox('data[is_multi_drive_any]', 1, $data['is_multi_drive_any'])}}

                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">

                    <div class="col-lg-12 multi_driver_row">
                        <label class="pull-left">Минимальный возраст собственника</label>
                        <label class="pull-right"></label>
                        <input type="number" class="form-control owner_age_any" name="data[owner_age]"
                               value="{{$data['owner_age']}}">
                    </div>


                    <div class="col-lg-12 non_multi_driver_row">
                        <label class="pull-left">Минимальный возраст водителей</label>
                        <label class="pull-right">Любой {{Form::checkbox('data[drivers_age_any]', 1, $data['drivers_age_any'])}}</label>
                        <input type="number" class="form-control drivers_age_any" name="data[drivers_min_age]"
                               value="{{$data['drivers_min_age']}}">
                    </div>

                    <div class="col-lg-12 non_multi_driver_row mt-15">
                        <label class="pull-left">Минимальный стаж водителей</label>
                        <label class="pull-right">Любой {{Form::checkbox('data[drivers_exp_any]', 1, $data['drivers_exp_any'])}}</label>
                        <input type="number" class="form-control drivers_exp_any" name="data[drivers_min_exp]"
                               value="{{$data['drivers_min_exp']}}">
                    </div>

                </div>

            </div>


        </div>
    </div>
