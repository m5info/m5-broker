@extends('layouts.frame')


@section('title')

    Сегмент

@endsection

@section('content')

    @php

    @endphp


    {{ Form::open(['url' => url("/directories/insurance_companies/{$insurance_companies->id}/bso_suppliers/{$bso_supplier->id}/financial_policy/$financial_policy->id/segments/".(int)$segment->id."/"), 'method' => 'post',  'class' => 'form-horizontal', 'id' => 'formContract', 'files' => true]) }}


    @if(View::exists("directories.insurance_companies.bso_suppliers.financial_policy.segments.elements.{$financial_policy->product->slug}"))
        @include("directories.insurance_companies.bso_suppliers.financial_policy.segments.elements.{$financial_policy->product->slug}", ['data' => (array)$data])
    @else
        <p>Форма сегмент для продукта отсутствует</p>
    @endif



    {{Form::close()}}






@endsection


@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection


@section('js')
    <script>
        $(function () {

            $('input:checkbox').change(function () {
                checkViewControl($(this).is(':checked'), $(this).attr('name'));
            });

            startViewControl();

        });

        function startViewControl() {

            $('#formContract').find("input:checkbox").each(function () {
                checkViewControl($(this).is(':checked'), $(this).attr('name'));
            });

        }


        function checkViewControl(checked, obj_name) {

            obj_name = obj_name.substr(5);
            obj_name = obj_name.substr(0, obj_name.length - 1);

            console.log(checked, obj_name);
            if ('is_multi_drive_any' == obj_name) {
                if (checked) {
                    $('.non_multi_driver_row').addClass('hidden');
                    $('.multi_driver_row').removeClass('hidden');
                } else {
                    $('.non_multi_driver_row').removeClass('hidden');
                    $('.multi_driver_row').addClass('hidden');
                }
            } else {
                $('.' + obj_name).toggleClass('hidden', checked);
            }

        }


    </script>


    <style>


        .panel-body label {
            font-weight: normal;
        }


        .panel-heading label {
            color: #134C9F;
            font-weight: normal;


        }

        .panel-default > .panel-heading {
            background-color: #FFF;
            display: block;
            width: auto;
            padding: 0.4em 0 0.6em 0;
            font-size: 16px;

        }


    </style>

@append