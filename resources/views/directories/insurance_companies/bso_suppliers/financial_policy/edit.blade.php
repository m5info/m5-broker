@extends('layouts.app')

@section('content')

    {{ Form::open(['url' => url("/directories/insurance_companies/{$insurance_companies->id}/bso_suppliers/{$bso_supplier->id}/financial_policy/".(int)$financial_policy->id."/"), 'method' => 'post',  'class' => 'form-horizontal', 'id' => 'formContract', 'files' => true]) }}
    <div class="row col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="page-subheading">
            <h2>Финансовая политика
                <a href="{{url("/directories/insurance_companies/{$insurance_companies->id}/bso_suppliers/{$bso_supplier->id}/")}}">{{$bso_supplier->title}}</a>
            </h2>
        </div>
        <div class="block-main">
            <div class="block-sub" style="padding: 45px 0 20px 40px;">
                <div class="form-horizontal">

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Актуально</label>
                        <div class="col-sm-8">
                            {{ Form::checkbox('is_actual', 1, $financial_policy->is_actual) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Продукт</label>
                        <div class="col-sm-8">
                            {{ Form::select('product_id', \App\Models\Directories\Products::where('is_actual', '=', '1')->get()->pluck('title', 'id')->prepend('Все', 0), $financial_policy->product_id, ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-4 control-label">Дата действия</label>
                        <div class="col-sm-8">
                            {{ Form::text('date_active', setDateTimeFormatRu($financial_policy->date_active, 1), ['class' => 'form-control datepicker date']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Дата окончания</label>
                        <div class="col-sm-8">
                            {{ Form::text('date_end', setDateTimeFormatRu($financial_policy->date_end, 1), ['class' => 'form-control datepicker date']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Название</label>
                        <div class="col-sm-8">
                            {{ Form::text('title', $financial_policy->title, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Фиксированная стоимость</label>
                        <div class="col-sm-8">
                            {{ Form::select('fix_price', [0 => 'Нет', 1 => 'Да'], $financial_policy->fix_price !== null ? $financial_policy->fix_price : 0, ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>


                    <div class="form-group view_percent {{$financial_policy->fix_price !== 0 ? 'hidden' : ''}}">
                        <label class="col-sm-4 control-label">КВ Бордеро</label>
                        <div class="col-sm-8">
                            {{ Form::text('kv_bordereau', $financial_policy->kv_bordereau, ['class' => 'form-control sum']) }}
                        </div>
                    </div>
                    <div class="form-group view_percent {{$financial_policy->fix_price !== 0 ? 'hidden' : ''}}">
                        <label class="col-sm-4 control-label">КВ Двоу</label>
                        <div class="col-sm-8">
                            {{ Form::text('kv_dvou', $financial_policy->kv_dvou, ['class' => 'form-control sum']) }}
                        </div>
                    </div>
                    <div class="form-group view_percent {{$financial_policy->fix_price !== 0 ? 'hidden' : ''}}">
                        <label class="col-sm-4 control-label">КВ СК</label>
                        <div class="col-sm-8">
                            {{ Form::text('kv_sk', $financial_policy->kv_sk, ['class' => 'form-control sum', 'disabled']) }}
                        </div>
                    </div>

                    <div class="form-group view_fix_price  {{$financial_policy->fix_price == 0 ? 'hidden' : ''}}" id="fix_price_sum_block">
                        <label class="col-sm-4 control-label">Себестоимость</label>
                        <div class="col-sm-8">
                            {{ Form::text('fix_price_sum', $financial_policy->fix_price_sum, ['class' => 'form-control sum']) }}
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-sm-4 control-label" id="base_agent_title">Базовое КВ Агента / Менеджера</label>
                        <div class="col-sm-8">
                            {{ Form::text('kv_agent', $financial_policy->kv_agent, ['class' => 'form-control sum']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" id="base_parent_title">Базовое КВ Руководителя</label>
                        <div class="col-sm-8">
                            {{ Form::text('kv_parent', $financial_policy->kv_parent, ['class' => 'form-control sum']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Куратор</label>
                        <div class="col-sm-8">
                            {{ Form::text('partnership_reward_1', $financial_policy->partnership_reward_1, ['class' => 'form-control sum']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Рекомендатель 2</label>
                        <div class="col-sm-8">
                            {{ Form::text('partnership_reward_2', $financial_policy->partnership_reward_2, ['class' => 'form-control sum']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Рекомендатель 3</label>
                        <div class="col-sm-8">
                            {{ Form::text('partnership_reward_3', $financial_policy->partnership_reward_3, ['class' => 'form-control sum']) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <input class="pull-right btn btn-primary" style="margin-right: 7.4%;" type="submit"
                                   value="Сохранить"/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>




    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="page-subheading">
            <h2>Финансовые группы</h2>
        </div>
        <div class="block-main">
            <div class="block-sub" style="padding: 45px 45px 25px 45px;">
                <div class="form-horizontal">

                    <table class="tov-table">
                        <tr>
                            <th></th>
                            <th>Группа</th>
                            <th id="group_agent_title">КВ агента / менеджера</th>
                            <th>КВ руководителя</th>
                        </tr>
                        @foreach($financial_policy->availableGroups() as $financialPolicyGroup)
                            <tr>
                                <td class="text-center">
                                    {{ Form::checkbox("financialPolicyGroups[$financialPolicyGroup->id][is_actual]", 1, $financialPolicyGroup->is_actual, ['class' => 'is_actual']) }}
                                </td>
                                <td>{{ $financialPolicyGroup->title }}</td>
                                <td>
                                    {{ Form::text("financialPolicyGroups[$financialPolicyGroup->id][kv_agent]", titleFloatFormat($financialPolicyGroup->kv_agent), ['class' => 'form-control sum']) }}
                                </td>
                                <td>
                                    {{ Form::text("financialPolicyGroups[$financialPolicyGroup->id][kv_parent]", titleFloatFormat($financialPolicyGroup->kv_parent), ['class' => 'form-control sum']) }}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="form-group"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <input class="pull-right btn btn-primary" style="margin-right: 1.1%;" type="submit"
                                   value="Сохранить"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @if($financial_policy->id > 0 )

        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="page-subheading">
                <h2>Сегментация</h2>
                <a href="{{url("/directories/insurance_companies/{$insurance_companies->id}/bso_suppliers/{$bso_supplier->id}/financial_policy/{$financial_policy->id}/segments/0/")}}"
                   class="fancybox fancybox.iframe btn btn-primary pull-right">
                    {{ trans('form.buttons.add') }}
                </a>
            </div>
            <div class="block-main">
                <div class="block-sub">
                    @if(sizeof($financial_policy->segments))
                        <table class="tov-table">
                            @foreach($financial_policy->segments as $segments)
                                <tr class="clickable-row fancybox fancybox.iframe"
                                    href="{{url("/directories/insurance_companies/{$insurance_companies->id}/bso_suppliers/{$bso_supplier->id}/financial_policy/{$financial_policy->id}/segments/{$segments->id}/")}}">
                                    <td>{{$segments->title}}</td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        {{ trans('form.empty') }}
                    @endif
                    @else
                </div>
            </div>
        </div>


        @endif
        </div>


        {{Form::close()}}







@endsection


@section('js')
    <script>
        $(function () {

            viewBlok();
            $('[name="fix_price"]').change(function () {
                viewBlok();
            });

            $("input[name=kv_bordereau], input[name=kv_dvou]").change(function () {
                var val1 = +parseFloat(getFloatFormat($("input[name=kv_bordereau]").val())).toFixed(2);
                var val2 = +parseFloat(getFloatFormat($("input[name=kv_dvou]").val())).toFixed(2);

                var val3 = (val1 + val2).toFixed(2);
                $("input[name=kv_sk]").val(val3);
            });


        });


        function viewBlok()
        {
            res = $('[name="fix_price"]').val();
            if (res == 1) {
                $('#base_agent_title').html('Базовая продажа Агента / Менеджера');
                $('#group_agent_title').html('Продажа Агента / Менеджера');

                $('#base_parent_title').html('Базовое продажа Руководителя');

                $('.view_fix_price').removeClass('hidden');
                $('.view_percent').addClass('hidden');

            } else if (res == 0) {
                $('#base_agent_title').html('Базовое КВ Агента / Менеджера');
                $('#group_agent_title').html('КВ Агента / Менеджера');

                $('#base_parent_title').html('Базовое КВ Руководителя');

                $('.view_percent').removeClass('hidden');
                $('.view_fix_price').addClass('hidden');
            }
        }


    </script>
@append