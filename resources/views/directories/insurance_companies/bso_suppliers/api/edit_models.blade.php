@extends('layouts.app')

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h1>Марка {{$mark->title}}
                <a class="btn btn-inline btn-primary pull-right" href="{{url('directories/insurance_companies/'.$id.'/bso_suppliers/'.$bso_supplier->id.'/api/marks_and_models')}}" style="margin-bottom: 30px; margin-top: 20px;">Назад</a>
            </h1>
        </div>
        <div class="block-main">
            <div class="block-sub">
                {{ Form::open(['url' => url("/directories/insurance_companies/{$id}/bso_suppliers/{$bso_supplier->id}/api/mark/{$mark->id}/save"), 'method' => 'post']) }}


                <table class="tov-table" style="width:100%">
                    <tr>
                        <th>Название</th>
                        <th>Модель СК</th>
                    </tr>

                    {{--Dinamic--}}
                    @foreach($models as $model)
                        <tr>
                            <td>{{$model->title}}</td>
                            <td>
                                {{--<select name="model_sk[{{$model->id}}]" class="form-control select2-all">
                                    <option value="">Не выбрано</option>
                                    @foreach($models_sk as $model_sk)
                                        <option value="{{$model_sk->vehicle_models_sk_id}}" @if($model_sk->vehicle_models_id == $model->id) selected @endif >{{$model_sk->sk_title}}</option>
                                    @endforeach
                                </select>--}}

                                {{ Form::select("model_sk[$model->id]", $models_sk->pluck('sk_title', 'vehicle_models_sk_id')->prepend('Не выбрано', ''), $model->vehicle_models_sk_id ?: '', ['class' => 'form-control select2-all']) }}
                            </td>
                        </tr>
                    @endforeach
                </table>

                <div class="row">
                    <div class="col-sm-12">
                        <input class="pull-right btn btn-primary" type="submit" value="Сохранить" autocomplete="off">
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>




@endsection

