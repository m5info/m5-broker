@extends('layouts.app')

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h1>Марки и модели
                <span class="btn btn-success pull-right" style="width:50px;" onclick="updateSKVehicleMarks()"><i class="fa fa-refresh"></i></span>
            </h1>
        </div>
        <div class="block-main">
            <div class="block-sub">
                <table class="tov-table" style="width:100%">
                    <tr>
                        <th>Марка</th>
                        <th>СК</th>
                        <th>Моделей (Наших / СК)</th>
                        <th></th>
                    </tr>

                    @foreach($all_marks as $mark)

                        @php
                            $mark_sk = \App\Models\Vehicle\APISettings\VehicleMarksSK::where('insurance_companies_id', $id)->where('bso_supplier_id', $bso_supplier->id)->where('vehicle_marks_id', $mark->id)->get()->first();
                        @endphp

                        <tr>
                            <td><a class="fancybox fancybox.iframe" href="mark/{{$mark->id}}/edit">{{$mark->title}}</a></td>
                            <td>{{($mark_sk)?$mark_sk->sk_title:''}}</td>
                            <td>{{\App\Models\Vehicle\VehicleModels::where('mark_id', $mark->id)->count() }} / {{($mark_sk)?count($mark_sk->model):0}}</td>
                            <td><a href="mark/{{$mark->id}}" class="btn btn-primary" style="max-width: 150px;">Модели</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>


@endsection


@section('js')

    <script>

        function updateSKVehicleMarks() {

            loaderShow();
            $.get("{{url("/directories/insurance_companies/{$id}/bso_suppliers/{$bso_supplier->id}/api/marks_and_models/updata_mark")}}", {}, function (response) {
                loaderHide();

                if (response) {

                    if(response.state == true){
                        reload();
                    }else{
                        flashMessage('danger', response.msg);
                    }

                }


            }).done(function () {
                loaderShow();
            })
                .fail(function () {
                    loaderHide();
                })
                .always(function () {
                    loaderHide();
                });

        }

    </script>

@endsection