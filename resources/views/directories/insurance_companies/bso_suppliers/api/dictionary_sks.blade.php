@extends('layouts.app')

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h1>Страховые компании (пролонгация)
                <span class="btn btn-success pull-right" style="width:50px;" onclick="updateSKDictionarySks()"><i class="fa fa-refresh"></i></span>
            </h1>
        </div>
        <div class="block-main">
            <div class="block-sub">
                {{ Form::open(['url' => url("/directories/insurance_companies/{$id}/bso_suppliers/{$bso_supplier->id}/api/dictionary_sks/save"), 'method' => 'post']) }}


                <table class="tov-table" style="width:100%">
                    <tr>
                        <th>Справочник</th>
                        <th>Справочник СК</th>
                    </tr>

                    @foreach($all_sks as $sk)
                        <tr>
                            <td>{{$sk->title}}</td>
                            <td>
                                <select name="dictionary_sks_sk[{{$sk->id}}]" class="form-control">
                                    <option value="">Не выбрано</option>
                                    @foreach($all_sks_sk as $sk_sk)
                                        <option value="{{$sk_sk->dictionary_sk_sk_id}}" @if($sk_sk->dictionary_sk_id == $sk->id) selected @endif >{{$sk_sk->sk_title}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    @endforeach
                </table>

                <div class="row">
                    <div class="col-sm-12">
                        <input class="pull-right btn btn-primary" type="submit" value="Сохранить" autocomplete="off">
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>



@endsection

@section('js')

    <script>

        function updateSKDictionarySks() {

            loaderShow();
            $.get("{{url("/directories/insurance_companies/{$id}/bso_suppliers/{$bso_supplier->id}/api/dictionary_sks/updata")}}", {}, function (response) {
                loaderHide();

                if (response) {

                    if(response.state == true){
                        reload();
                    }else{
                        flashMessage('danger', response.msg);
                    }

                }


            }).done(function () {
                loaderShow();
            })
            .fail(function () {
                loaderHide();
            })
            .always(function () {
                loaderHide();
            });

        }

    </script>

@endsection