@extends('layouts.app')

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h1>Цель использования
                <span class="btn btn-success pull-right" style="width:50px;" onclick="updateSKVehiclePurpose()"><i class="fa fa-refresh"></i></span>
            </h1>
        </div>
        <div class="block-main">
            <div class="block-sub">
                {{ Form::open(['url' => url("/directories/insurance_companies/{$id}/bso_suppliers/{$bso_supplier->id}/api/vehicle_purpose_sk/save"), 'method' => 'post']) }}


                <table class="tov-table" style="width:100%">
                    <tr>
                        <th>Справочник</th>
                        <th>Справочник СК</th>
                    </tr>

                    @foreach($all_vehicle_purpose as $vehicle_purpose)
                        <tr>
                            <td>{{$vehicle_purpose->title}}</td>
                            <td>
                                <select name="purpose_sk[{{$vehicle_purpose->id}}]" class="form-control">
                                    <option value="">Не выбрано</option>
                                    @foreach($vehicle_purpose_sk as $purpose_sk)
                                        <option value="{{$purpose_sk->vehicle_purpose_sk_id}}" @if($purpose_sk->vehicle_purpose_id == $vehicle_purpose->id) selected @endif >{{$purpose_sk->sk_title}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    @endforeach
                </table>

                <div class="row">
                    <div class="col-sm-12">
                        <input class="pull-right btn btn-primary" type="submit" value="Сохранить" autocomplete="off">
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>



@endsection

@section('js')

    <script>

        function updateSKVehiclePurpose() {

            loaderShow();
            $.get("{{url("/directories/insurance_companies/{$id}/bso_suppliers/{$bso_supplier->id}/api/vehicle_purpose_sk/updata")}}", {}, function (response) {
                loaderHide();

                if (response) {

                    if(response.state == true){
                        reload();
                    }else{
                        flashMessage('danger', response.msg);
                    }

                }


            }).done(function () {
                loaderShow();
            })
            .fail(function () {
                loaderHide();
            })
            .always(function () {
                loaderHide();
            });

        }

    </script>

@endsection