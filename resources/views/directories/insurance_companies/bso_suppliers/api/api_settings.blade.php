@extends('layouts.app')

@section('content')
    {{ Form::open(['url' => url("/directories/insurance_companies/{$id}/bso_suppliers/{$bso_supplier->id}/api"), 'method' => 'post']) }}

    <div class="row col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="page-subheading">
            <h2>Настройки API</h2>
        </div>
        <div class="block-main">
            <div class="block-sub">
                <div class="form-horizontal">

                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Актуально</label>
                        <div class="col-sm-8">
                            {{ Form::checkbox('is_actual', 1, $api_setting->is_actual) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Другая реализация</label>
                        <div class="col-sm-8">
                            {{ Form::checkbox('custom_realization', 1, $api_setting->custom_realization, ['active' => $api_setting->custom_realization ? 1 : 0 ]) }}
                        </div>
                    </div>

                    <div class="form-group" id="custom_realization_hint" style="display: {{$api_setting->custom_realization ? 'block' : 'none'}};">
                        @foreach($online_products as $product)
                            <label class="col-sm-12" style="margin-left: 10px;">JS {{$product->title ? $product->title : '$title'}} (Для корректной работы, создайте свой скрипт по пути <code>'/js/sk/{{$api_setting->dir_name ? $api_setting->dir_name : '$dir_name'}}/{{$product->slug ? $product->slug : '$slug'}}/calc.js'</code>)</label>
                        @endforeach
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-4 control-label">СМС-подтверждение</label>
                        <div class="col-sm-8">
                            {{ Form::select('sms_confirmation', [0 => 'Нет', 1=> 'Да'], $api_setting->sms_confirmation, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Использовать версию</label>
                        <div class="col-sm-8">
                            {{ Form::select('work_type', [0 => 'Тестовая', 1=> 'Боевая'], $api_setting->work_type, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Путь</label>
                        <div class="col-sm-8">
                            {{ Form::select('dir_name', $dir_names->prepend('Не выбрано', ''), $api_setting->dir_name, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="divider"></div>

                    <h3>Доступ к бою</h3>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="battle_server_url">URL</label>
                            {{ Form::input('text', 'battle_server_url', $api_setting->battle_server_url, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            <label for="battle_server_login">Логин</label>
                            {{ Form::input('text', 'battle_server_login', $api_setting->battle_server_login, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            <label for="battle_server_password">Пароль</label>
                            {{ Form::input('text', 'battle_server_password', $api_setting->battle_server_password, ['class' => 'form-control']) }}
                        </div>
                    </div>


                    <h3>Доступ к тесту</h3>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="test_server_url">URL</label>
                            {{ Form::input('text', 'test_server_url', $api_setting->test_server_url, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            <label for="test_server_login">Логин</label>
                            {{ Form::input('text', 'test_server_login', $api_setting->test_server_login, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            <label for="test_server_password">Пароль</label>
                            {{ Form::input('text', 'test_server_password', $api_setting->test_server_password, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <input class="pull-right btn btn-primary" type="submit" value="Сохранить" autocomplete="off">
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>
    {{ Form::close() }}


    @if($api_setting->id > 0)

    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="page-subheading">
            <h2 class="inline-h1">Mapping API</h2>
        </div>
        <div class="block-main">
            <div class="block-sub">
                <div class="form-horizontal">

                    <h3>Авто</h3>

                    <div class="divider"></div>
                    <br/>

                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Цель использования</label>
                        <div class="col-sm-8" style="margin-top: 5px;">
                            <a href="{{url('directories/insurance_companies/'.$id.'/bso_suppliers/'.$bso_supplier->id.'/api/vehicle_purpose_sk')}}">Настроить</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Категории</label>
                        <div class="col-sm-8" style="margin-top: 5px;">
                            <a href="{{url('directories/insurance_companies/'.$id.'/bso_suppliers/'.$bso_supplier->id.'/api/vehicle_categories_sk')}}">Настроить</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Марки и модели</label>
                        <div class="col-sm-8" style="margin-top: 5px;">
                            <a href="{{url('directories/insurance_companies/'.$id.'/bso_suppliers/'.$bso_supplier->id.'/api/marks_and_models')}}">Настроить</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Противоугонные устройства</label>
                        <div class="col-sm-8" style="margin-top: 5px;">
                            <a href="{{url('directories/insurance_companies/'.$id.'/bso_suppliers/'.$bso_supplier->id.'/api/anti_theft_devices')}}">Настроить</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Цвета машин</label>
                        <div class="col-sm-8" style="margin-top: 5px;">
                            <a href="{{url('directories/insurance_companies/'.$id.'/bso_suppliers/'.$bso_supplier->id.'/api/vehicle_colors')}}">Настроить</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Страховые компании (пролонгация)</label>
                        <div class="col-sm-8" style="margin-top: 5px;">
                            <a href="{{url('directories/insurance_companies/'.$id.'/bso_suppliers/'.$bso_supplier->id.'/api/dictionary_sks')}}">Настроить</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @endif


@endsection

@section('js')
    <script>
        $(function(){
            $('[name="custom_realization"]').click(function(){
                active = $('[name="custom_realization"]').attr('active');

                if (active == 1){
                    $('#custom_realization_hint').css('display', 'none');
                    $('[name="custom_realization"]').attr('active', 0);
                } else{
                    $('#custom_realization_hint').css('display', 'block');
                    $('[name="custom_realization"]').attr('active', 1);
                }


            });
        })
    </script>
@endsection

