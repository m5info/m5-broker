@extends('layouts.frame')

@section('title')

    Марка {{$mark->title}}


@endsection

@section('content')

    {{ Form::open(['url' => url("/directories/insurance_companies/{$id}/bso_suppliers/{$bso_supplier->id}/api/mark/{$mark->id}/save_mark"), 'method' => 'post', 'class' => 'form-horizontal']) }}

    <div class="form-group">
        <label class="col-sm-4 ">Модель СК</label>
        <div class="col-sm-8">

            <select name="mark_sk" class="form-control select2-all">
                <option value="">Не выбрано</option>
                @foreach($mark_sk as $sk)
                    <option value="{{$sk->vehicle_marks_sk_id}}" @if($sk->vehicle_marks_id == $mark->id) selected @endif >{{$sk->sk_title}}</option>
                @endforeach
            </select>

        </div>
    </div>


    {{Form::close()}}

@endsection

@section('footer')


    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

