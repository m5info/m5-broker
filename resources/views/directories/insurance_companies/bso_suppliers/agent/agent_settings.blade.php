@extends('layouts.app')

@section('content')

<div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="page-subheading">
            <h2>Настройки Агента</h2>
        </div>
        <div class="block-main">
            <div class="block-sub">
                {{ Form::open(['url' => url("/directories/insurance_companies/{$id}/bso_suppliers/{$bso_supplier->id}/agent"), 'method' => 'post']) }}

                    <h3>Данные</h3>
                    <div class="col-lg-12">
                        <div>
                            <div class="form-group col-lg-4">
                                <label for="agent_id">Агентский договор</label>
                                {{ Form::input('text', 'agent_name', $agent->agent_name, ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="agent_id">Наиминование договора</label>
                                {{ Form::input('text', 'contract_name', $agent->contract_name, ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="agent_id">ID Агента</label>
                                {{ Form::input('text', 'agent_id', $agent->agent_id, ['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <label for="department_id">ID подразделения</label>
                            {{ Form::input('text', 'department_id', $agent->department_id, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group col-lg-12">
                            <label for="manager_id">ID менеджера</label>
                            {{ Form::input('text', 'manager_id', $agent->manager_id, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group col-lg-12">
                            <label for="signer_id">ID подписанта</label>
                            {{ Form::input('text', 'signer_id', $agent->signer_id, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group col-lg-12">
                            <label for="сhannel_sale_id">Канал продаж</label>
                            {{ Form::input('text', 'сhannel_sale_id', $agent->сhannel_sale_id, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <input class="pull-right btn btn-primary" type="submit" value="Сохранить" autocomplete="off">
                        </div>
                    </div>
                {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="page-subheading">
                <h2>Документы</h2>
            </div>
            <div class="block-main">
                <div class="block-sub">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">

                           @include('directories.insurance_companies.bso_suppliers.agent.agent_documents', ['bso_supplier' => $bso_supplier, 'agent' => $agent, 'insurance_company_id' => $id, 'bso_supplier_id' => $bso_supplier->id])

                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection

