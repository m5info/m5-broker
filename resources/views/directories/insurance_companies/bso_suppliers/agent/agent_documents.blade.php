<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <span class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="color: red;">Перечень документов</span>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @if($agent->documents->count())
            <table class="table orderStatusTable dataTable no-footer">
            <!--<thead>
            <tr>
                <th>{{ trans('users/users.edit.title') }}</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            </thead>-->
                <tbody>
                @foreach($agent->documents as $file)
                    <div class="col-lg-6 col-md-12">
                        <div class="upload-dot">
                            <div class="block-image">
                                @if (in_array($file->ext, ['jpg', 'jpeg', 'png', 'gif']))
                                    <a href="{{ url($file->url) }}" target="_blank">
                                        <img class="media-object preview-image" src="{{ url($file->preview) }}" onerror="this.onerror=null;this.src='/images/extensions/unknown.png';">
                                    </a>
                                @else
                                    <a href="{{ url($file->url) }}" target="_blank">
                                        <img class="media-object preview-icon" src="/images/extensions/{{$file->ext}}.png">
                                    </a>
                                @endif
                                <div class="upload-close">
                                    <div class="" style="float:right;color:red;">
                                        <a href="javascript:void(0);" onclick="removeFile('{{ $file->name }}')">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <tr>
                    <td>{{ $file->original_name }}</td>
                    <td>
                        <a href="{{ url($file->url) }}" class="btn btn-primary" target="_blank">
                            {{ trans('form.buttons.download') }}
                            </a>
                        </td>
                        <td>
                            <button class="btn btn-danger" type="button" onclick="removeFile('{{ $file->name }}')">
                            {{ trans('form.buttons.delete') }}
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <h3>{{ trans('form.empty') }}</h3>
        @endif
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        {!! Form::open(['url'=>"/directories/insurance_companies/{$id}/bso_suppliers/{$bso_supplier_id}/agent/document",'method' => 'post', 'class' => 'dropzone', 'id' => 'addManyDocForm']) !!}
        <div class="dz-message" data-dz-message>
            <p>Перетащите сюда файлы</p>
            <p class="dz-link">или выберите с диска</p>
        </div>
        {!! Form::close() !!}
    </div>
</div>


<script>

    function initManyDocuments() {
        /*
         $("#addManyDocForm").dropzone({
         //Dropzone.options.addOrgDocForm = {
         paramName: 'file',
         maxFilesize: 10,
         //acceptedFiles: "image/*",
         init: function () {
         this.on("complete", function () {
         if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
         reload();
         }

         });
         }
         });
         */

    }



</script>

<br/>

<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >

    @if(isset($hold_kv_product) && sizeof($hold_kv_product->documents))

        @foreach($hold_kv_product->documents as $key => $document)

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">

                <div class="row">
                    <span class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="color: red;padding-top:5px;">{{$document->file_title}}</span>

                    @if($agent->document($document->id))

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="upload-dot">
                                <div class="block-image">
                                    @if (in_array($agent->document($document->id)->file->ext, ['jpg', 'jpeg', 'png', 'gif']))
                                        <a href="{{ url($agent->document($document->id)->file->url) }}" target="_blank">
                                            <img class="media-object preview-image" src="{{ url($agent->document($document->id)->file->preview) }}" onerror="this.onerror=null;this.src='/images/extensions/unknown.png';">
                                        </a>
                                    @else
                                        <a href="{{ url($agent->document($document->id)->file->url) }}" target="_blank">
                                            <img class="media-object preview-icon" src="/images/extensions/{{$agent->document($document->id)->file->ext}}.png">
                                        </a>
                                    @endif
                                    <div class="upload-close">
                                        <div class="" style="float:right;color:red;">
                                            <a href="javascript:void(0);" onclick="removeDocument('{{ $agent->document($document->id)->file->name }}','{{$document->id}}')">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    @else
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h3>Документ отсутствует</h3>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {!! Form::open(['url'=>url("/directories/insurance_companies/{$id}/bso_suppliers/{$bso_supplier_id}/agent/document"),'method' => 'post', 'class' => 'dropzone', 'id' => '']) !!}
                            <div class="dz-message" data-dz-message>
                                <p>Перетащите сюда файл</p>
                                <p class="dz-link">или выберите с диска</p>
                            </div>
                            {!! Form::close() !!}
                        </div>

                    @endif
                </div>
            </div>


        @endforeach

    @endif

    <script>
        function process(operations = []){
            $.each(operations, function(k,operation){
                if(isFunction(operation)){
                    window[operation]()
                }
            })
        }

        function removeDocument(fileName, documentId) {
            newCustomConfirm(function (result) {
                if (result){
                    var filesUrl = '{{ url("/directories/insurance_companies/{$id}/bso_suppliers/{$bso_supplier_id}/agent/document/") }}';
                    var fileUrl = filesUrl + '/' + documentId;
                    $.post(fileUrl, {
                        _method: 'DELETE'
                    }, function () {
                        reload();
                    });
                }
            });
        }
    </script>

</div>
<script>
    function initDocuments() {
        initManyDocuments();
    }
</script>