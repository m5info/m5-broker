
<div class="form-group">
    <label class="col-sm-4 control-label">Тип</label>
    <div class="col-sm-8">
        {{ Form::select('hold_type_id', collect(\App\Models\Directories\HoldKv::HOLD_TYPE), old('hold_type_id'), ['class' => 'form-control', 'required']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">Продукт</label>
    <div class="col-sm-8">
        {{ Form::select('product_id', \App\Models\Directories\Products::where('is_actual', '=', '1')->get()->pluck('title', 'id'), old('product_id'), ['class' => 'form-control', 'required']) }}
    </div>
</div>

