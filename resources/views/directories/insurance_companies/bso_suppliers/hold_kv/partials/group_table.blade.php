<table class="tov-table">
    <thead>
    <tr>
        <th>Вид оплаты</th>
        <th>Доступен</th>
    </tr>
    </thead>
    @foreach(\App\Models\Finance\PayMethod::query()->where('is_actual', 1)->get() as $pay_method)
        <tr>
            <td class="text-left">{{ $pay_method->title }}</td>
            <td class="text-left">
                {{ Form::checkbox("method[]", $pay_method->id, $pay_methods->has($pay_method->id)) }}
            </td>
        </tr>
    @endforeach
    <tr>
        <td>
            Доступ БСО
        </td>
        <td>
            {{Form::select('access_bso', \App\Models\Finance\PayMethod::ACCESS_BSO, isset($access_bso->access_bso) ? $access_bso->access_bso : 0, ['class' => 'form-control'])}}
        </td>
    </tr>
</table>
<div class="form-group">
    <input type="submit" class="btn btn-primary btn-right" style="margin-right: 15px" value="Сохранить"/>
</div>