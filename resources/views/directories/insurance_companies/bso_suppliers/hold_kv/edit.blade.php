@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="page-subheading">
                <h2>Удержание КВ <a
                            href="{{url("/directories/insurance_companies/{$insurance_companies->id}/bso_suppliers/{$bso_supplier->id}")}}">{{$bso_supplier->title}}</a>
                </h2>

            </div>
            <div class="block-main">
                <div class="block-sub">

                    {{ Form::model($hold_kv, ['url' => url("/directories/insurance_companies/{$insurance_companies->id}/bso_suppliers/{$bso_supplier->id}/hold_kv/{$hold_kv->id}/"), 'method' => 'put', 'class' => 'form-horizontal']) }}


                    @include('directories.insurance_companies.bso_suppliers.hold_kv.form')


                    <div class="form-group">

                        <label class="col-sm-3 control-label">Много файлов</label>
                        <div class="col-sm-1">
                            {{ Form::checkbox('is_many_files', 1, $hold_kv->is_many_files, ['class' => 'form-control', 'id'=>"is_many_files", "onchange"=>"setManyFiles()"]) }}
                        </div>

                        <label class="col-sm-3 control-label">Проверять договор</label>
                        <div class="col-sm-1">
                            {{ Form::checkbox('is_check_policy', 1, $hold_kv->is_check_policy, ['class' => 'form-control']) }}
                        </div>
                        <div class="col-sm-12"></div>
                        <label class="col-sm-3 control-label">Е-ПОЛИС</label>
                        <div class="col-sm-1">
                            {{ Form::checkbox('is_epolicy', 1, $hold_kv->is_epolicy, ['class' => 'form-control']) }}
                        </div>

                        <label class="col-sm-3 control-label">Автоматическое списание БСО</label>
                        <div class="col-sm-1">
                            {{ Form::checkbox('is_auto_bso', 1, $hold_kv->is_auto_bso, ['class' => 'form-control']) }}
                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-sm-12">
                            {{ Form::text('many_text', $hold_kv->many_text, ['class' => 'form-control', 'id'=>'many_text', "placeholder"=>"Перечень документов"]) }}
                        </div>
                    </div>

                    <div class="row form-group">
                        <table id="tab_files" class="tov-table">
                            <tr>
                                <td>
                                    <center><strong>Документ</strong></center>
                                </td>
                                <td>
                                    <center><strong>Название для api</strong></center>
                                </td>
                                <td><span class="btn btn-success pull-right" onclick="addFormFiles()"><i
                                                class="fa fa-plus"></i>Добавить</span></td>
                            </tr>

                            @if(sizeof($hold_kv->documents))

                                @foreach($hold_kv->documents as $key => $document)
                                    <tr id='file_tr_{{$key}}'>
                                        <td><input id='file_title_{{$key}}' name='file_title[]'
                                                   value='{{$document->file_title}}' class='form-control' required
                                                   type='text'></td>
                                        <td><input id='file_name_{{$key}}' name='file_name[]'
                                                   value='{{$document->file_name}}' class='form-control' type='text'>
                                        </td>
                                        <td><span class='btn btn-primary pull-right' onclick='delFormFiles({{$key}})'><i
                                                        class='fa fa-minus'></i>Удалить</span></td>
                                    </tr>
                                @endforeach

                            @endif

                        </table>
                    </div>

                    <input type="submit" class="btn btn-primary" value="Сохранить"/>

                    {{Form::close()}}

                </div>
            </div>
        </div>


        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="page-subheading">
                <h2>Доступные виды оплаты</h2>
            </div>
            <div class="block-main">
                <div class="block-sub">
                    {{ Form::open(['url' => url("/directories/insurance_companies/{$hold_kv->insurance_companies_id}/bso_suppliers/{$hold_kv->bso_supplier_id}/products_settings/{$hold_kv->product_id}/"), 'method' => 'post', 'class' => 'form-horizontal']) }}
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="">Группа</label>
                        </div>
                        <div class="col-sm-6">
                            {{Form::select('group_id', App\Models\Settings\FinancialGroup::orderBy('title')->get()->pluck('title', 'id')->prepend('По умолчанию', 0), Session::has('group_id') ? Session::get('group_id') : 0, ['class'=>'form-control'])}}
                        </div>
                    </div>
                    <br>
                    <div class="_clear">

                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>


        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="display: none">
            <div class="page-subheading">
                <h2>Интеграции</h2>
            </div>

            <div class="block-main">
                <div class="block-sub">
                    <a href="javascript:void(0);" class="btn btn-success pull-right"
                       onclick="openFancyBoxFrame('/directories/insurance_companies/{{$hold_kv->insurance_companies_id}}/bso_suppliers/{{$hold_kv->bso_supplier_id}}/hold_kv/{{$hold_kv->id}}/supplier_form')">Добавить</a>

                    <table class="table">
                        <thead>
                        <tr>
                            <th>Тип</th>
                            <th>Версия</th>
                            <th>Дата изменения</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($formValues as $collection)

                            @php
                                $item = $collection->first();
                            @endphp

                            <tr>
                                <td>{{$item->version->integration->title}}</td>
                                <th>{{$item->version->title}}</th>
                                <td>{{$item['updated_at']}}</td>
                                <td>
                                    <span class="btn btn-primary pull-right"
                                          onclick="openFancyBoxFrame('/directories/insurance_companies/{{$hold_kv->insurance_companies_id}}/bso_suppliers/{{$hold_kv->bso_supplier_id}}/hold_kv/{{$hold_kv->id}}/supplier_form/{{$item->version_id}}')"
                                          style="height:20px;font-size:10px;padding:5px;"><i
                                                class='fa fa-edit'></i></span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>
            </div>
        </div>

    </div>


@endsection



@section('js')

    <script>

        $(function () {

            group_id = $('[name = "group_id"]').val();

            if (group_id){
                $.get('/directories/insurance_companies/1/bso_suppliers/{{$bso_supplier_id}}/hold_kv/{{$hold_kv->id}}/get_table_group?group_id='+group_id, function(res){
                    $('._clear').html(res);
                });
            } else{
                $.get('/directories/insurance_companies/1/bso_suppliers/{{$bso_supplier_id}}/hold_kv/{{$hold_kv->id}}/get_table_group?group_id=0', function(res){
                    $('._clear').html(res);
                });
            }



            setManyFiles()


        });

        function setManyFiles() {

            if ($("#is_many_files").is(':checked')) {
                $("#many_text").show();
            } else {
                $("#many_text").hide();
            }

        }

        var MY_COUNT_FILES = "{{ ($hold_kv->documents)?count($hold_kv->documents):0}}";

        function addFormFiles() {

            tr = "<tr id='file_tr_" + MY_COUNT_FILES + "'>" +
                "<td><input id='file_title_" + MY_COUNT_FILES + "' name='file_title[]' value='' class='form-control' required type='text'></td>" +
                "<td><input id='file_name_" + MY_COUNT_FILES + "' name='file_name[]' value='' class='form-control' type='text'></td>" +
                "<td><span class='btn btn-primary pull-right' onclick='delFormFiles(" + MY_COUNT_FILES + ")'><i class='fa fa-minus'></i>Удалить</span></td>" +
                "</tr>";
            $('#tab_files tr:last').after(tr);
            MY_COUNT_FILES = parseInt(MY_COUNT_FILES) + 1;
        }

        function delFormFiles(id) {
            $('#file_tr_' + id).remove();
        }

        $('[name = "group_id"]').change(function () {

            $.get('{{url("/directories/insurance_companies/{$id}/bso_suppliers/{$bso_supplier_id}/hold_kv/{$hold_kv->id}/get_table_group")}}', {
                group_id: $(this).val()
            }, function(res){
                $('._clear').html(res);
            });

        })


    </script>

@endsection