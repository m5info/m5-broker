@extends('layouts.frame')

@section('title')

    Алгоритм рассрочки

    @if($algorithm->id>0)
        <span class="btn btn-info pull-right" onclick="openLogEvents('{{$algorithm->id}}', 12, 0)"><i class="fa fa-history"></i> </span>
    @endif
@endsection

@section('content')



{{ Form::open(['url' => url("/directories/insurance_companies/{$insurance_companies->id}/installment_algorithms/".((int)$algorithm->id)."/"), 'method' => 'post', 'class' => 'form-horizontal']) }}

<div class="form-group">
    <label class="col-sm-4 control-label">Алгоритм</label>
    <div class="col-sm-8">
        {{ Form::select('algorithm_id', \App\Models\Settings\InstallmentAlgorithmsList::all()->pluck('title', 'id'), $algorithm->algorithm_id, ['class' => 'form-control select2-ws', 'required']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">Продукт</label>
    <div class="col-sm-8">

        {{ Form::select("product_id", \App\Models\Directories\Products::all()->pluck('title', 'id')->prepend('Все', 0), $algorithm->product_id, ['class' => 'form-control select2-ws']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">Ключ API</label>
    <div class="col-sm-8">

        {{ Form::text("sk_key", $algorithm->sk_key, ['class' => 'form-control']) }}
    </div>
</div>


{{Form::close()}}


@endsection

@section('footer')

    <button onclick="deleteAlghoritm()" type="submit" class="btn btn-danger pull-left">{{ trans('form.buttons.delete') }}</button>
    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>
    <script>
        function deleteAlghoritm() {
            if (!customConfirm()) return false;

            $.post('{{url("/directories/insurance_companies/{$insurance_companies->id}/installment_algorithms/".((int)$algorithm->id)."/")}}', {
                _method: 'DELETE'
            }, function () {
                parent_reload();
            });
        }
    </script>
@endsection