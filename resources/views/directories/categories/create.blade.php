@extends('layouts.frame')


@section('title')

    {{ trans('menu.categories') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/directories/categories'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('directories.categories.form')

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
