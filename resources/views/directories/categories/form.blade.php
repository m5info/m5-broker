
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/categories.title') }}</label>
    <div class="col-sm-8">
        {{ Form::text('title', old('title'), ['class' => 'form-control', 'required']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/categories.code') }}</label>
    <div class="col-sm-8">
        {{ Form::select('code', \App\Models\Vehicle\VehicleCategories::__CODES, isset($category->code) ? \App\Models\Vehicle\VehicleCategories::__CODES[$category->code] : old('code'), ['class' => 'form-control', 'required']) }}
    </div>
</div>


