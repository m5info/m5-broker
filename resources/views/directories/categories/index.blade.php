@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">{{ trans('menu.categories') }}</h1>
        <span class="btn btn-primary btn-right" onclick="openFancyBoxFrame('{{ url('/directories/categories/create')  }}')">
            {{ trans('form.buttons.create') }}
        </span>
    </div>

    @if(sizeof($categories))
        <table class="tov-table">
            <thead>
                <tr>
                    <th><a href="javascript:void(0);">{{ trans('settings/categories.title') }}</a></th>
                    <th><a href="javascript:void(0);">{{ trans('settings/categories.code') }}</a></th>
                </tr>
            </thead>
            @foreach($categories as $category)
                <tr onclick="openFancyBoxFrame('{{ url("/directories/categories/$category->id/edit") }}')">
                    <td>{{ $category->title }}</td>
                    <td>{{ $category->code }}</td>
                </tr>
            @endforeach

        </table>
    @else
        {{ trans('form.empty') }}
    @endif

@endsection
