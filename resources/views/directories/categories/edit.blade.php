@extends('layouts.frame')

@section('title')

    {{ trans('menu.categories') }}
    <span class="btn btn-info pull-right" onclick="openLogEvents('{{$category->id}}', 6, 0)"><i class="fa fa-history"></i> </span>


@endsection

@section('content')

    {{ Form::model($category, ['url' => url("/directories/categories/$category->id"), 'method' => 'put', 'class' => 'form-horizontal']) }}

    @include('directories.categories.form')

    {{Form::close()}}

@endsection

@section('footer')

    <button class="btn btn-danger pull-left" onclick="deleteItem('/directories/categories/', '{{ $category->id }}')">{{ trans('form.buttons.delete') }}</button>

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

