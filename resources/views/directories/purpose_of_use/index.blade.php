@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">{{ trans('menu.purpose_of_use') }}</h1>
        <span class="btn btn-primary btn-right" onclick="openFancyBoxFrame('{{ url('/directories/purpose_of_use/create')  }}')">
            {{ trans('form.buttons.create') }}
        </span>
    </div>

    @if(sizeof($purposes))
        <table class="tov-table">
            <thead>
                <tr>
                    <th><a href="javascript:void(0);">{{ trans('settings/categories.title') }}</a></th>
                </tr>
            </thead>
            @foreach($purposes as $purpose)
                <tr onclick="openFancyBoxFrame('{{ url("/directories/purpose_of_use/$purpose->id/edit") }}')">
                    <td>{{ $purpose->title }}</td>
                </tr>
            @endforeach

        </table>
    @else
        {{ trans('form.empty') }}
    @endif

@endsection
