@extends('layouts.frame')

@section('title')

    {{ trans('menu.purpose_of_use') }}
    <span class="btn btn-info pull-right" onclick="openLogEvents('{{$purpose->id}}', 6, 0)"><i class="fa fa-history"></i> </span>


@endsection

@section('content')

    {{ Form::model($purpose, ['url' => url("/directories/purpose_of_use/$purpose->id"), 'method' => 'put', 'class' => 'form-horizontal']) }}

    @include('directories.purpose_of_use.form')

    {{Form::close()}}

@endsection

@section('footer')

    <button class="btn btn-danger pull-left" onclick="deleteItem('/directories/purpose_of_use/', '{{ $purpose->id }}')">{{ trans('form.buttons.delete') }}</button>

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

