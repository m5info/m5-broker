<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/purpose_of_use.title') }}</label>
    <div class="col-sm-8">
        {{ Form::text('title', old('title'), ['class' => 'form-control', 'required']) }}
    </div>
</div>

