@extends('layouts.frame')


@section('title')

    {{ trans('menu.purpose_of_use') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/directories/purpose_of_use'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('directories.purpose_of_use.form')

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
