@extends('layouts.frame')


@section('title')

    {{ trans('menu.anti-theft devices') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/directories/anti_theft_devices'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('directories.anti_theft_devices.form')

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
