
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/anti-theft devices.title') }}</label>
    <div class="col-sm-8">
        {{ Form::text('title', old('title'), ['class' => 'form-control', 'required']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/anti-theft devices.row') }}</label>
    <div class="col-sm-8">
        {{ Form::input('number', 'row', old('row'), ['class' => 'form-control', 'required']) }}
    </div>
</div>


