@extends('layouts.frame')

@section('title')

    {{ trans('menu.anti-theft devices') }}
    <span class="btn btn-info pull-right" onclick="openLogEvents('{{$device->id}}', 6, 0)"><i class="fa fa-history"></i> </span>


@endsection

@section('content')

    {{ Form::model($device, ['url' => url("/directories/anti_theft_devices/$device->id"), 'method' => 'put', 'class' => 'form-horizontal']) }}

    @include('directories.anti_theft_devices.form')

    {{Form::close()}}

@endsection

@section('footer')

    <button class="btn btn-danger pull-left" onclick="deleteItem('/directories/anti_theft_devices/', '{{ $device->id }}')">{{ trans('form.buttons.delete') }}</button>

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

