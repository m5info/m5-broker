@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">{{ trans('menu.anti-theft devices') }}</h1>
        <span class="btn btn-primary btn-right" onclick="openFancyBoxFrame('{{ url('/directories/anti_theft_devices/create')  }}')">
            {{ trans('form.buttons.create') }}
        </span>
    </div>

    @if(sizeof($devices))
        <table class="tov-table">
            <thead>
                <tr>
                    <th><a href="javascript:void(0);">{{ trans('settings/anti-theft devices.title') }}</a></th>
                    <th><a href="javascript:void(0);">{{ trans('settings/anti-theft devices.row') }}</a></th>
                </tr>
            </thead>
            @foreach($devices as $device)
                <tr onclick="openFancyBoxFrame('{{ url("/directories/anti_theft_devices/$device->id/edit") }}')">
                    <td>{{ $device->title }}</td>
                    <td>{{ $device->row }}</td>
                </tr>
            @endforeach

        </table>
    @else
        {{ trans('form.empty') }}
    @endif

@endsection
