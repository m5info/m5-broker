@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">{{ trans('menu.vehicle_colors') }}</h1>
        <span class="btn btn-primary btn-right" onclick="openFancyBoxFrame('{{ url('/directories/vehicle_colors/create')  }}')">
            {{ trans('form.buttons.create') }}
        </span>
    </div>

    @if(sizeof($colors))
        <table class="tov-table">
            <thead>
                <tr>
                    <th><a href="javascript:void(0);">{{ trans('settings/vehicle_colors.title') }}</a></th>
                    <th><a href="javascript:void(0);">{{ trans('settings/vehicle_colors.is_actual') }}</a></th>
                </tr>
            </thead>
            @foreach($colors as $color)
                <tr onclick="openFancyBoxFrame('{{ url("/directories/vehicle_colors/$color->id/edit") }}')">
                    <td>{{ $color->title }}</td>
                    <td>{{ $color->is_actual ? 'Да' : 'Нет' }}</td>
                </tr>
            @endforeach

        </table>
    @else
        {{ trans('form.empty') }}
    @endif

@endsection
