
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/vehicle_colors.title') }}</label>
    <div class="col-sm-8">
        {{ Form::text('title', old('title'), ['class' => 'form-control', 'required']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/vehicle_colors.is_actual') }}</label>
    <div class="col-sm-8">
        {{ Form::checkbox('is_actual', old('is_actual'), old('is_actual'), ['required']) }}
    </div>
</div>


