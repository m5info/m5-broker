@extends('layouts.frame')


@section('title')

    {{ trans('menu.vehicle_colors') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/directories/vehicle_colors'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('directories.vehicle_colors.form')

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
