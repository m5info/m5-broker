@extends('layouts.frame')

@section('title')

    {{ trans('menu.vehicle_colors') }}
    <span class="btn btn-info pull-right" onclick="openLogEvents('{{$color->id}}', 6, 0)"><i class="fa fa-history"></i> </span>


@endsection

@section('content')

    {{ Form::model($color, ['url' => url("/directories/vehicle_colors/$color->id"), 'method' => 'put', 'class' => 'form-horizontal']) }}

    @include('directories.vehicle_colors.form')

    {{Form::close()}}

@endsection

@section('footer')

    <button class="btn btn-danger pull-left" onclick="deleteItem('/directories/vehicle_colors/', '{{ $color->id }}')">{{ trans('form.buttons.delete') }}</button>

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

