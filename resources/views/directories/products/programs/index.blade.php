@extends('layouts.frame')

@section('title')

    Программа


@endsection

@section('content')

    {{ Form::model($programs, ['url' => url("/directories/products/{$product}/edit/programs/".(int)$programs->id), 'method' => 'post', 'class' => 'form-horizontal']) }}


    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('settings/banks.is_actual') }}</label>
        <div class="col-sm-2">
            {{ Form::checkbox('is_actual', 1, old('is_actual')) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('settings/banks.title') }}</label>
        <div class="col-sm-8">
            {{ Form::text('title', old('title'), ['class' => 'form-control', 'required']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Ключ классов для калькулятора (EN)</label>
        <div class="col-sm-8" >
            {{ Form::select('slug', collect(\App\Models\Directories\ProductsPrograms::SLUG), old('slug'), ['class' => 'form-control', 'id' => 'slug', "onchange"=>"getAPiPrograms()"]) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Описание продукта</label>
        <div class="col-sm-8">
            {{ Form::textarea('description', old('description'),['rows' => 2,  'class' =>'form-control', 'style' => 'width:100%', 'id' => 'text_description']) }}
        </div>
    </div>

    <div class="form-group">
        <label  class="col-sm-4 control-label">СК</label>
        <div class="col-sm-8">
            {{ Form::select('insurance_company', \App\Models\Directories\InsuranceCompanies::all()->pluck('title', 'id')->prepend('Не выбрано', 0), $programs->insurance_companies_programs && $programs->insurance_companies_programs->insurance_company ? $programs->insurance_companies_programs->insurance_company->id : 0, ['class' => 'form-control ', 'id' => 'insurance_company']) }}
        </div>
    </div>

    <div class="form-group">
        <label  class="col-sm-4 control-label">Путь</label>
        <div class="col-sm-8">
            {{ Form::select('dir_name', $dir_names->prepend('Не выбрано', ''), old('dir_name'), ['class' => 'form-control ', 'id' => 'dir_name', "onchange"=>"getAPiPrograms()"]) }}
        </div>
    </div>

    <div class="form-group">
        <label  class="col-sm-4 control-label">Программа</label>
        <div class="col-sm-8">
            {{ Form::select('program_id', [], 0, ['class' => 'form-control select2-all', 'id' => 'program_id']) }}
        </div>
    </div>


    {{Form::close()}}

@endsection

@section('footer')

    @if((int)$programs->id > 0)
        <button class="btn btn-danger pull-left" onclick="deleteItem('/directories/products/{{ $product }}/edit/programs/', '{{ $programs->id }}')">{{ trans('form.buttons.delete') }}</button>
    @endif

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

@section('js')

    <script>

        var program_id = '{{(int)$programs->id > 0 ? (int)$programs->program_id : 0}}';

        $(function () {

            getAPiPrograms();



        });


        function getAPiPrograms() {

            $('#program_id').html('');
            slug = $("#slug").val();
            dir_name = $("#dir_name").val();
            if (slug == 'kasko_calculator'){
                $('#insurance_company').val(0);
                $('#insurance_company').attr('disabled', 'disabled');
            }else {
                $('#insurance_company').removeAttr('disabled');
            }
            if(slug.length > 0 && dir_name.length > 0){

                $.getJSON('{{url("/directories/products/{$product}/edit/programs/".(int)$programs->id."/get_api_programs")}}', {slug: slug, dir_name: dir_name}, function (response) {

                    var options = "<option value='0'>Не выбрано</option>";
                    response.map(function (item) {
                        options += "<option value='" + item.id + "'>" + item.title + "</option>";
                    });

                    $('#program_id').html(options).select2('val', program_id);


                });

            }
        }


    </script>


@endsection