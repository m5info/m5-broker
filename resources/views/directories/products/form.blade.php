<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('settings/banks.is_actual') }}</label>
        <div class="col-sm-2">
            {{ Form::checkbox('is_actual', 1, old('is_actual')) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">Отчеты ДВОУ</label>
        <div class="col-sm-2">
            {{ Form::checkbox('is_dvou', 1, old('is_dvou')) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">Осмотр</label>
        <div class="col-sm-8">
            <label class="control-label" style="padding: 0 0 0 10px">Нет</label>
            {{ Form::radio('for_inspections', 0, old('for_inspections')) }}
            <label class="control-label" style="padding: 0 0 0 10px">ДТП</label>
            {{ Form::radio('for_inspections', 1, old('for_inspections')) }}
            <label class="control-label" style="padding: 0 0 0 10px">ПСО</label>
            {{ Form::radio('for_inspections', 2, old('for_inspections')) }}
        </div>
    </div>
    @php($style = '')
    @if(isset($product) && $product->for_inspections == 0)
        @php($style = 'display:none')
    @endif
    <div class="form-group" id="inspection_temple_act" style="{{$style}}">
        <label class="col-sm-4 control-label">Акт осмотра</label>
        <div class="col-sm-8">
            {{ Form::select('inspection_temple_act', collect(\App\Models\Directories\Products::INSPECTION_TEMPLE_ACT), old('inspection_temple_act'), ['class' => 'form-control select2-ws']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('settings/banks.title') }}</label>
        <div class="col-sm-8">
            {{ Form::text('title', old('title'), ['class' => 'form-control', 'required']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Тип фин. политики</label>
        <div class="col-sm-8">
            {{ Form::select('financial_policy_type_id', collect(\App\Models\Directories\Products::FIN_TYPE), old('financial_policy_type_id'), ['class' => 'form-control select2-ws', 'required']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Категория</label>
        <div class="col-sm-8">
            {{ Form::select('category_id', \App\Models\Directories\ProductsCategory::orderBy('sort', 'asc')->get()->pluck('title', 'id'), old('category_id'), ['class' => 'form-control select2-ws', 'required']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Страховая сумма по умолчанию</label>
        <div class="col-sm-8">
            {{ Form::text('insurance_amount_default', old('insurance_amount_default'), ['class' => 'form-control sum']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Ставка для андеррайтера</label>
        <div class="col-sm-8">
            {{ Form::text('underwriter_rate', old('underwriter_rate'), ['class' => 'form-control']) }}
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-4 control-label">Доступ к КВ Оф.</label>
        <div class="col-sm-8">
            {{ Form::checkbox('kv_official_available', 1, old('kv_official_available')) }}
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-4 control-label">Доступ к КВ Неоф.</label>
        <div class="col-sm-8">
            {{ Form::checkbox('kv_informal_available', 1, old('kv_informal_available')) }}
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-4 control-label">Доступ к КВ Банк</label>
        <div class="col-sm-8">
            {{ Form::checkbox('kv_bank_available', 1, old('kv_bank_available')) }}
        </div>
    </div>

</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">


    @if(Auth::user()->role_id === 1)

        <div class="form-group">
            <label class="col-sm-4 control-label">Доступно оформление онлайн</label>
            <div class="col-sm-2">
                {{ Form::checkbox('is_online', 1, old('is_online')) }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label hint--bottom"  aria-label="Также, если активно, то при заведении в стандартной первичке, не подставляется фин. политика">Доступен в первичке - быстрой <i class="fa fa-info info-hint" aria-hidden="true"></i></label>
            <div class="col-sm-2">
                {{ Form::checkbox('available_in_primary_fast', 1, old('available_in_primary_fast')) }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">
                Шаблон <br/>
                @if(isset($product) && $product->id > 0)
                    @if($product->template)
                        <a href="{{ $product->template->getUrlAttribute() }}" target="_blank" style="float: left">{{ $product->template->original_name }}</a>
                    @endif
                @endif
            </label>
            <div class="col-sm-8">
                {{ Form::file('file', ['class' => 'file-input']) }}
            </div>
        </div>



        <div class="form-group">
            <label class="col-sm-4 control-label">
                Ключ классов<br/>
                @if(isset($product) && $product->id > 0)<a href="" >Спец настройки</a>@endif
            </label>
            <div class="col-sm-8" >
                {{ Form::select('slug', collect(\App\Models\Directories\Products::SLUG), old('slug'), ['class' => 'form-control select2-ws', 'id' => 'slug', "onchange"=>"selectTypeSlug()"]) }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">
                Описание продукта <br/>
                @if(isset($product) && $product->id > 0)<a href="{{url("/directories/products/{$product->id}/edit/info/")}}" >Инструкция</a>@endif
            </label>
            <div class="col-sm-8">
                {{ Form::textarea('description', old('description'),['rows' => 2,  'class' =>'form-control', 'style' => 'width:100%', 'id' => 'text_description']) }}
            </div>
        </div>

        @if(isset($product) && $product->id > 0)
        <div class="form-group">
            <label class="col-sm-12">Программы

                <span class="btn btn-success pull-right" style="width: 30px;height: 25px;font-size: 10px;" onclick="openFancyBoxFrame('{{ url("/directories/products/{$product->id}/edit/programs/0/") }}')">
                    <i class="fa fa-plus"></i>
                </span>
            </label>
            <div class="col-sm-12">

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>СК</th>
                        <th>Названиие</th>
                        <th>Актуально</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($product->programs as $programs)
                        <tr onclick="openFancyBoxFrame('{{ url("/directories/products/{$product->id}/edit/programs/{$programs->id}/") }}')">
                            <td>{{$programs->insurance_companies_programs && $programs->insurance_companies_programs->insurance_company ? $programs->insurance_companies_programs->insurance_company->title : '(По умолчанию для всех)'}}</td>
                            <td>{{ $programs->title }}</td>
                            <td>{{$programs->is_actual ? 'Да' : 'Нет'}}</td>
                        </tr>
                    @endforeach
                </table>


            </div>
        </div>
        @endif



    @endif


</div>



