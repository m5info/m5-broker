@extends('layouts.app')

@section('content')

    <style>
        .pb-5{
            padding-bottom: 3em;
        }
        .form-group{
            display: flex;
            align-items: center;
        }
    </style>

    <div class="page-heading">
        <h1 class="inline-h1">AvInfo BOT</h1>
    </div>

    @if(is_object($result))
    <div class="pb-5">
        <div><span>Остаток запросов: </span>{{ $result->leftClicks }}</div>
        <div><span>Доступ до: </span>{{ $result->accessToStr }}</div>
        <div><span>Доступно ДК: </span>{{ $result->leftDk }}</div>
        <div><span>Доступно отчетов : </span>{{ $result->leftReport }}</div>
    </div>
    @endif

    {{ Form::open(['url' => url('/directories/avinfobot/get_info/'), 'method' => 'post', 'class' => 'form-horizontal', 'onsubmit' => 'get_info(); return false;']) }}

    <div class="form-group">
        <label class="col-sm-4 control-label">Государственный регистрационный знак или VIN</label>
        <div class="col-sm-2">
            <select name="type" id="select_type" class="form-control" required>
                <option value="gosnomer">ГРЗ</option>
                <option value="vin">VIN</option>
                <option value="phone">Телефон</option>
            </select>
        </div>
        <div class="col-sm-2">
            <input type="text" name="request" id="gosnomer" class="form-control input" required placeholder="А001АА777" data-capslock autocomplete="off" maxlength="17">
        </div>
        <button type="submit" class="btn btn-primary">Запросить информацию</button>
    </div>


    {{Form::close()}}

    <div id="AvInfo"></div>


@endsection

@section('js')

    <script>

        /**
         *
         * Смена маски для input
         *
         */

        $('#gosnomer').attr('placeholder', 'А001АА777')
        $('#gosnomer').attr('length', 8)
        $('#gosnomer').mask('Z999ZZ999', {
            translation: {
                'Z': {
                    pattern: /[АВЕКМНОРСТУХавекмнорстуих]/, optional: false
                }
            }
        });

        $('#select_type').on('change', function (){
            if($( "#select_type option:selected" ).val() == 'gosnomer'){
                $('#gosnomer').attr('placeholder', 'А001АА777')
                $('#gosnomer').attr('length', 8)
                $('#gosnomer').mask('Z999ZZ999', {
                    translation: {
                        'Z': {
                            pattern: /[АВЕКМНОРСТУХавекмнорстуих]/, optional: false
                        }
                    }
                });
            }else if($( "#select_type option:selected" ).val() == 'vin'){
                $('#gosnomer').attr('placeholder', '1HGCM82633A004352')
                $('#gosnomer').attr('length', 17)
                $('#gosnomer').mask('ZZZZZZZZZZZZZZZZZ', {
                    translation: {
                        'Z': {
                            pattern: /[A-Za-z0-9]/, optional: false
                        }
                    }
                });
            }else if($( "#select_type option:selected" ).val() == 'phone'){
                $('#gosnomer').attr('placeholder', '79991112233')
                $('#gosnomer').attr('length', 11)
                $('#gosnomer').mask('99999999999');
            }
        })
        /** **/

        function get_info(){

            var success = true;
            //$('#contract').val($('#request').val());
            var form = $('.form-horizontal');

            form.find('input[required=required], select[required=required]').each(function () {
                var valid = $(this).val() != '';
                $(this).toggleClass('has-error', !valid);
                if (!valid) {
                    success = false;
                }
            });
            if(parseInt($('#gosnomer').val().length) < parseInt($('#gosnomer').attr('length'))){
                Swal.fire({
                    icon: 'error',
                    title: 'УПС...',
                    text: 'Хочешь потратить запрос в пустую??'
                })
                return
            }
            if(success){
                form = form.serialize();
                $.ajax({
                    type: "POST",
                    {{--url: "{{route('sk-partner-integration')}}",--}}
                    url: "{{route('get_info_avinfobot')}}",
                    data: form,
                    success: function (res){
                        if($( "#select_type option:selected" ).val() == 'phone'){
                            $('#AvInfo').html(res);
                            return;
                        }

                        res = JSON.parse(res);
                        if(res){
                            var dtp = 'ДТП:<br/>';
                            for(i = 0; i < res['dtp'].length; i++){
                                dtp = dtp + '<br/>Дата: ' + res['dtp'][i]['date'] + '<br/>';
                                dtp = dtp + 'Регион: ' + res['dtp'][i]['region'] + '<br/>';
                                dtp = dtp + 'Тип: ' + res['dtp'][i]['type'] + '<br/>';
                                (res['dtp'][i]['schemeUrl'] != '') ? (dtp = dtp + "<img src='" + res['dtp'][i]['schemeUrl'] + "'alt='Нет Схемы'>") : '';
                            }

                            var period_vladeniya = 'Владение:<br/><br/>';

                            for(i = 0; i < res['period_vladeniya'].length; i++){
                                period_vladeniya = period_vladeniya + 'Тип собственника: ' + res['period_vladeniya'][i]['OwnerType'] + '<br/>';
                                period_vladeniya = period_vladeniya + 'Дата начала владения: ' + res['period_vladeniya'][i]['DateStartOwnerShip'] + '<br/>';
                                (res['period_vladeniya'][i]['DateEndOwnerShip'] != '01.01.0001') ? (period_vladeniya = period_vladeniya + 'Дата окончания владения: ' + res['period_vladeniya'][i]['DateEndOwnerShip'] + '<br/>') : (period_vladeniya = period_vladeniya + "По настоящее время!<br/>");
                                period_vladeniya = period_vladeniya + 'Событие: ' + res['period_vladeniya'][i]['Oparetion'] + '<br/>';
                            }

                            $('#AvInfo').html(
                                'Фамилия: ' + res['family']+
                                '<br/>Имя: ' + res['name']+
                                '<br/>Отчество: ' + res['nametwo']+
                                '<br/>Телефон: ' + res['phone']+
                                '<br/>Авто: ' + res['car']+
                                '<br/>Марка: ' + res['mark']+
                                '<br/>Модель: ' + res['model']+
                                '<br/>Год: ' + res['year']+
                                '<br/>Мощность: ' + res['power']+
                                '<br/>Объём: ' + res['volume']+
                                '<br/>VIN: ' + res['vin']+
                                '<br/>ПТС: ' + res['docserie']+
                                '<br/>ПТС дата выдачи: ' + res['pts_date']+
                                '<br/>СТС: ' + res['sts']+
                                '<br/>СТС дата выдачи: ' + res['sts_date']+
                                '<br/>Диагностическая карта: ' + res['dk_number']+
                                '<br/>ДК Выдана: ' + res['dk_date_from']+
                                '<br/>ДК До: ' + res['dk_date']+

                                '<br/><br/>' + dtp +
                                '<br/>' + period_vladeniya
                            )
                        }else{
                            $('#AvInfo').html('Нет данных!');
                        }
                    }
                });
            }


        }
    </script>

@endsection




