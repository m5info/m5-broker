@extends('layouts.frame')

@section('title')

    {{ trans('menu.delivery') }}
    <span class="btn btn-info pull-right" onclick="openLogEvents('{{$cost->product}}', 2, 0)"><i class="fa fa-history"></i> </span>

@endsection

@section('content')

    {{ Form::model($cost, ['url' => url("/directories/delivery/update/$cost->id"), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @if($cost->product_id != 0)
        @include('directories.delivery.form')
    @else
        @include('directories.delivery.form_other_product')
    @endif

    {{Form::close()}}

@endsection

@section('footer')

    <button class="btn btn-danger pull-left" onclick="deleteItem_1('/directories/delivery/delete/', '{{ $cost->id }}')">{{ trans('form.buttons.delete') }}</button>

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection


@section('js')
    <script>

        function deleteItem_1(url, id){
            $.ajax({
                type: "POST",
                url: url+id,
                success: function (res){
                    parent_reload();
                }
            });
        }

    </script>

@endsection
