<div class="form-group">
    <label class="col-sm-4 control-label">Продукт</label>
    <div class="col-sm-8">
        {{ Form::text('product_title', $cost->product_title, ['class' => 'form-control', 'id' => 'product_title']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">Стоимость выезда в будни</label>
    <div class="col-sm-8">
        {{ Form::text('delivery_price', $cost->delivery_price, ['class' => 'form-control sum', 'required']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">Стоимость выезда в выходные</label>
    <div class="col-sm-8">
        {{ Form::text('weekend_price', $cost->weekend_price, ['class' => 'form-control sum', 'required']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/banks.is_actual') }}</label>
    <div class="col-sm-8">
        {{ Form::checkbox('is_actual', 1, old('is_actual')) }}
    </div>
</div>