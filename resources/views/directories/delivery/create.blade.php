@extends('layouts.frame')


@section('title')

    {{ trans('menu.delivery') }}

@endsection

@section('content')

    <style>
        .hide{
            display: none;
        }
    </style>


    {{ Form::open(['url' => url('/directories/delivery/create/'), 'method' => 'post', 'class' => 'form-horizontal']) }}


    <div class="form-group">
        <label class="col-sm-4 control-label">Продукт не из списка</label>
        <div class="col-sm-8">
            {{ Form::checkbox('cost_new_product', 1, 0, ['id' => 'cost_new_product']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Продукт</label>
        <div class="col-sm-8">
            {{ Form::select('product_id', $products->pluck('title', 'id'), -1, ['class'=>'form-control select2-all', 'id' => 'product_id']) }}
        </div>

        <div class="col-sm-8 hide">
            {{ Form::text('product_title', '', ['class' => 'form-control', 'id' => 'product_title', 'disabled']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Стоимость выезда в будни</label>
        <div class="col-sm-8">
            {{ Form::text('delivery_price', '', ['class' => 'form-control sum', 'required']) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">Стоимость выезда в выходные</label>
        <div class="col-sm-8">
            {{ Form::text('weekend_price', '', ['class' => 'form-control sum', 'required']) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('settings/banks.is_actual') }}</label>
        <div class="col-sm-8">
            {{ Form::checkbox('is_actual', 1, 1) }}
        </div>
    </div>

    {{Form::close()}}


@endsection

@section('js')
    <script>

        $('#cost_new_product').on('change', function (){
            if($("#cost_new_product").prop('checked')){
                $('#product_id').attr('disabled', 'disabled');
                ($('#product_id').parent()).addClass('hide');
                $('#product_title').removeAttr('disabled');
                ($('#product_title').parent()).removeClass('hide');
            }else{
                $('#product_title').attr('disabled', 'disabled');
                ($('#product_title').parent()).addClass('hide');
                $('#product_id').removeAttr('disabled');
                ($('#product_id').parent()).removeClass('hide');

            }
        })

    </script>

@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
