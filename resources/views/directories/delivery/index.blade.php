@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">Выезды</h1>
        <span class="btn btn-primary btn-right" onclick="openFancyBoxFrame('{{ url('/directories/delivery/create')  }}')">
            {{ trans('form.buttons.create') }}
        </span>
    </div>

    @if(sizeof($costs))
        <table class="tov-table">
            <thead>
            <tr>
                <th><a href="javascript:void(0);">Продукт</a></th>
                <th><a href="javascript:void(0);">Стоимость выезда в будни</a></th>
                <th><a href="javascript:void(0);">Стоимость выезда в выходные</a></th>
                <th><a href="javascript:void(0);">Актуально</a></th>
            </tr>
            </thead>
            @foreach($costs as $cost)
                <tr onclick="openFancyBoxFrame('{{url ("/directories/delivery/edit/$cost->id")}}')">
                    <td>{{ ($cost->product_id != 0) ? $cost->product->title : $cost->product_title }}</td>
                    <td>{{ titleFloatFormat($cost->delivery_price) }}</td>
                    <td>{{ titleFloatFormat($cost->weekend_price) }}</td>
                    <td>{{ $cost->is_actual == 1 ? 'Да' : 'Нет'}}</td>
                </tr>
            @endforeach
        </table>
    @else
        {{ trans('form.empty') }}
    @endif


@endsection

@section('js')


@endsection




