@extends('layouts.frame')

@section('title')

    Создание марки


@endsection

@section('content')

    {{ Form::open(['url' => url("/directories/marks_and_models/save"), 'method' => 'post', 'class' => 'form-horizontal']) }}


    <div class="form-group">
        <label class="col-sm-4 control-label">ID</label>
        <div class="col-sm-8">
            {{ Form::text('id', '', ['class'=>'form-control'])}}
        </div>

        <label class="col-sm-4 control-label">Название</label>
        <div class="col-sm-8">
            {{ Form::text('title', '', ['class'=>'form-control'])}}
        </div>

        <label class="col-sm-4 control-label">Категория</label>
        <div class="col-sm-8">
            {{ Form::select('category_id', \App\Models\Vehicle\VehicleCategories::all()->pluck('title', 'id')->prepend('Не указана', 0), '', ['class'=>'form-control'])}}
        </div>

        <label class="col-sm-4 control-label">Россия/Нет</label>
        <div class="col-sm-8">
            {{ Form::checkbox('is_rus', '', '' )}}
        </div>
    </div>

    {{Form::close()}}

@endsection

@section('footer')


    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

