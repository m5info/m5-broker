@extends('layouts.frame')

@section('title')

    Марка {{$mark->title}}


@endsection

@section('content')

    {{ Form::open(['url' => url("/directories/marks_and_models/mark/{$mark->id}/update_mark"), 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'update_form']) }}


    <div class="form-group">
        <label class="col-sm-4 control-label">Название</label>
        <div class="col-sm-8">
            {{ Form::text('title', $mark->title, ['class'=>'form-control'])}}
        </div>

        <label class="col-sm-4 control-label">Россия</label>
        <div class="col-sm-8">
            {{ Form::checkbox('is_rus', $mark->is_rus, $mark->is_rus )}}
        </div>
    </div>
    <input type="text" name="id" value="{{$mark->id}}" hidden>

    {{Form::close()}}

@endsection

@section('footer')

    <button onclick="deleteMark()" type="submit" class="btn btn-danger pull-left">{{ trans('form.buttons.delete') }}</button>
    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

@section('js')
    <script>
        function deleteMark() {

            let id = $('[name = "id"]').val();

            $.post('/directories/marks_and_models/delete', {id: id}, function (data) {
                window.top.location.reload();
            })
        }

    </script>
@endsection

