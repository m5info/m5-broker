@extends('layouts.frame')

@section('title')

    Марка {{$mark->title}} | Модель {{$model->title}}


@endsection

@section('content')

    {{ Form::open(['url' => url("/directories/marks_and_models/mark/{$mark->id}/model/{$model->id}/update_model"), 'method' => 'post', 'class' => 'form-horizontal']) }}


    <div class="form-group">
        <label class="col-sm-4 control-label">Название</label>
        <div class="col-sm-8">
            {{ Form::text('title', $model->title, ['class'=>'form-control'])}}
        </div>

        <label class="col-sm-4 control-label">Категория</label>
        <div class="col-sm-8">
            {{ Form::select('category_id', \App\Models\Vehicle\VehicleCategories::all()->pluck('title', 'id')->prepend('Не указана', 0), $model->category_id, ['class' => 'form-control'])}}
        </div>
    </div>
    <input type="text" name="id" hidden value="{{$model->id}}">

    {{Form::close()}}

@endsection

@section('footer')

    <button onclick="deleteModel()" type="submit" class="btn btn-danger pull-left">{{ trans('form.buttons.delete') }}</button>
    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

@section('js')
    <script>
        function deleteModel() {

            let id = $('[name = "id"]').val();

            $.post('/directories/marks_and_models/mark/'+ id +'/delete', {id: id}, function (data) {
                window.top.location.reload();
            })
        }

    </script>
@endsection

