@extends('layouts.app')

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h1>Марка {{$mark->title}}
                <a class="btn btn-inline btn-primary pull-right" href="{{url('/directories/marks_and_models/')}}"
                   style="margin-bottom: 30px; margin-top: 20px;">Назад</a>

                <a class="btn btn-inline btn-primary pull-right fancybox fancybox.iframe" href="{{url("/directories/marks_and_models/mark/{$mark->id}/create")}}"
                   style="margin-bottom: 30px; margin-top: 20px; margin-right: 15px;">Создать</a>
            </h1>
        </div>
        <div class="block-main">
            <div class="block-sub">
                {{ Form::open(['url' => url("/directories/insurance_companies//bso_suppliers/api/mark//save"), 'method' => 'post']) }}


                <table class="tov-table" style="width:100%">
                    <tr>
                        <th>Название</th>
                        <th>Категория</th>
                    </tr>


                    @foreach($models as $model)
                        <tr>
                            <td><a class="fancybox fancybox.iframe"
                                   href="/directories/marks_and_models/mark/{{$mark->id}}/model/{{$model->id}}/edit">{{$model->title}}</a>
                            </td>
                            @if(\App\Models\Vehicle\VehicleCategories::query()->where('id', $model->category_id)->pluck('title', 'id')->first())
                                <td>{{\App\Models\Vehicle\VehicleCategories::query()->where('id', $model->category_id)->pluck('title', 'id')->first()}}</td>
                            @else
                                <td>Не указана</td>
                            @endif

                        </tr>

                    @endforeach
                </table>

                <div class="row">
                    <div class="col-sm-12">
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>




@endsection

