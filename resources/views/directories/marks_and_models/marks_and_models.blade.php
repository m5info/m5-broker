@extends('layouts.app')

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h1>Марки и модели
                <a class="btn btn-inline btn-primary pull-right fancybox fancybox.iframe"
                   href="{{url("/directories/marks_and_models/create")}}"
                   style="margin-bottom: 30px; margin-top: 20px; margin-right: 15px;">Создать</a>
            </h1>

        </div>
        <div class="block-main">
            <div class="block-sub">
                <table class="tov-table" style="width:100%">
                    <tr>
                        <th>Марка</th>
                        <th>Россия\Нет</th>
                        <th></th>
                    </tr>

                    @foreach($all_marks as $mark)

                        <tr>
                            <td><a class="fancybox fancybox.iframe" href="/directories/marks_and_models/mark/{{$mark->id}}/edit">{{$mark->title}}</a>
                            </td>
                            <td>{{($mark->is_rus == 0) ? 'Нет' : 'Да' }}</td>
                            <td><a href="/directories/marks_and_models/mark/{{$mark->id}}" class="btn btn-primary"
                                   style="max-width: 150px;">Модели</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>


@endsection


@section('js')

@endsection