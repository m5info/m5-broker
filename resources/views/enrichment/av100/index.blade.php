@extends('layouts.app')

@section('content')


    <div class="page-heading">
        <h2>
            <span class="btn-left">
                Обогощениие XLS
            </span>
        </h2>
    </div>

    <div class="form-group">
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="control">
        </div>
    </div>

    <div class="form-group" style="margin-left: 0; margin-right: 0;">
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">



            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    {!! Form::open(['url'=>"/enrichment/av100/upload",'method' => 'post', 'class' => 'dropzone_', 'id' => 'addManyDocFormDownloadingContracts']) !!}
                    <div class="dz-message" data-dz-message>
                        <p>Перетащите сюда файлы</p>
                        <p class="dz-link">или выберите с диска</p>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">


            {{ Form::open(['url' => url('/enrichment/av100/load'), 'method' => 'post',  'class' => 'form-horizontal', 'id' => 'formContract', 'files' => true]) }}

            <input type="hidden" name="file" >


            <div class="block-view">
                <div class="row">
                    <div>
                        <div>

                            <div class="col-lg-6">
                                <div class="field form-col">
                                    <label class="control-label">VIN</label>
                                    <select class="form-control columns-select"  name="excel_columns[vin]"></select>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="field form-col">
                                    <label class="control-label">Рег номер</label>
                                    <select class="form-control columns-select"  name="excel_columns[reg_number]"></select>
                                </div>
                            </div>


                            <button type="submit" class="btn btn-w-m btn-success submit-btn" id="buttonSend">
                                Загрузить
                            </button>


                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
                        </div>
                    </div>
                </div>
            </div>



            {{Form::close()}}

            </div>

        </div>
    </div>




@endsection

@section('js')



    <script>





        $(function () {


            $("#buttonSend").hide();

            $("#addManyDocFormDownloadingContracts").dropzone({
                paramName: 'file',
                maxFilesize: 10,
                //acceptedFiles: "image/*",
                init: function () {
                    this.on("complete", function (response) {

                        $('.columns-row').removeClass('hidden');
                        var jsonResult = JSON.parse(response.xhr.response);
                        console.log(jsonResult);
                        var selectOptions = makeSelectOptions(prepareColumnsToMakeOptions(jsonResult.columns));
                        $('.columns-select').html(selectOptions);
                        $('.submit-btn').prop('disabled', false);
                        $('[name=file]').val(jsonResult.file);
                        setDefaultValues();
                        $("#buttonSend").show();

                    });
                }
            });


        });


        function prepareColumnsToMakeOptions(columns) {
            var result = $.map(columns, function (item) {
                return {
                    id: item,
                    title: item
                };
            });
            result.unshift({
                id: '',
                title: 'Не выбрано'
            });
            return result;
        }

        function setDefaultValues() {
            $('select[name="excel_columns[vin]"]').val('VIN');
            $('select[name="excel_columns[reg_number]"]').val('номер');

        }



    </script>


@endsection