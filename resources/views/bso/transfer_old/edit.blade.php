@extends('layouts.app')



@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h2>Создание приема передачи БСО</h2>
        </div>
        <div class="block-main">
            <div class="block-sub">

                {{ Form::open(['url' => url("/bso/transfer/"), 'method' => 'store', 'class' => 'form-horizontal']) }}

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Тип</label>
                        <div class="col-sm-3">
                            {{ Form::select('type_id', collect(\App\Models\BSO\BsoTransfer::TYPES), 0, ['class' => 'form-control type_id', 'id'=>'type_id', 'required']) }}
                        </div>
                    </div>
                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Агент-получатель</label>
                        <div class="col-sm-3">
                            {{ Form::select('user_id_to', collect(\App\Models\User::all()->pluck('name', 'id'))->prepend('Выберите агента получателя', 0), 0, ['class' => 'form-control', 'id'=>'user_id_to', 'required']) }}
                        </div>
                    </div>

                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Агент-отправитель</label>
                        <div class="col-sm-3">
                            {{ Form::select('user_id_from', collect(\App\Models\User::all()->pluck('name', 'id'))->prepend('Выберите агента отправителя', 0), 0, ['class' => 'form-control', 'id'=>'user_id_from', 'required']) }}
                        </div>
                    </div>

                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Точка продаж</label>
                        <div class="col-sm-3">
                            {{ Form::select('point_sale_id', collect(\App\Models\Settings\PointsSale::all()->pluck('title', 'id'))->prepend('Выберите точку продаж', 0), 0, ['class' => 'form-control', 'id'=>'point_sale_id', 'required']) }}
                        </div>
                    </div>

                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Сотрудник</label>
                        <div class="col-sm-3">
                            {{ Form::select('bso_manager_id', collect(\App\Models\User::all()->pluck('name', 'id'))->prepend('Выберите сотрудника', 0), 0, ['class' => 'form-control', 'id'=>'bso_manager_id', 'required']) }}
                        </div>
                    </div>
                    <div class="form-group toggled-setting" style="display: none;">
                        <label class="col-sm-3 control-label">Курьер</label>
                        <div class="col-sm-3">
                            {{ Form::select('courier_id', collect(\App\Models\User::all()->pluck('name', 'id'))->prepend('Выберите курьера', 0), 0, ['class' => 'form-control', 'id'=>'courier_id', 'required']) }}
                        </div>
                    </div>
                    <div id="user_info"></div>

                    <button type="submit" class="btn btn-primary" id="save_transfer">Сохранить</button>

                {{ Form::close() }}
            </div>
        </div>
    </div>




@endsection

@section('js')

    <script>

        $(function(){


            var visible_fields_types = {
                1: ['point_sale_id', 'user_id_to'],
                2: ['point_sale_id', 'bso_manager_id'],
                3: ['user_id_to', 'courier'],
                4: ['user_id_from', 'user_id_to', 'tp_change_selected'],
            };

            var settings = {
                dataFunc: {
                    type_id: function () {
                        return $('[name="type_id"]').val()
                    },
                    location_id: function () {
                        return $('[name="location_id"]').val()
                    },
                    user_id_to: function () {
                        return $('[name="user_id_to"]').val()
                    },
                    user_id_from: function () {
                        return $('[name="user_id_from"]').val()
                    },
                    point_sale_id: function () {
                        return $('[name="point_sale_id"]').val()
                    },
                    bso_manager_id: function () {
                        return $('[name="bso_manager_id"]').val()
                    },
                    courier_id: function () {
                        return $('[name="courier_id"]').val()
                    },
                    tp_change_selected: function () {
                        return $('[name="tp_change_selected"]').prop('checked')
                    },

                },
                getData: function () {
                    var data = {};
                    $.each(this.dataFunc, function (k, v) {
                        data[k] = v();
                    });
                    return data;
                },
                get: function(name){
                    return this.dataFunc[name]();
                }
            };

            toggleSettings();


            $(document).on('change', 'select[name="type_id"]', function(){
                toggleSettings();
                refreshUserInfo();
            });

            $(document).on('change', '[name="user_id_to"]', function(){
                refreshUserInfo();
            });

            function refreshUserInfo(){
                if($('[name="user_id_to"]:visible').length > 0){
                    var user_id = settings.get('user_id_to');
                    if(user_id>0){
                        $.get('{{url("/bso/transfer/get-user-info/")}}', {user_id:user_id}, function(res){
                            $('#user_info').html(res.html);
                        })
                    }else{
                        $('#user_info').html('');
                    }
                }else{
                    $('#user_info').html('');
                }
            }

            function toggleSettings(){
                var type_id = settings.get('type_id');
                $('.toggled-setting').hide();
                $.each(visible_fields_types[type_id], function(k,v){
                    $('[name="'+v+'"]').closest('.toggled-setting').show();
                });
            }
        });

    </script>


@endsection