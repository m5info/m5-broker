<div class="agent_info form-group">
    <div class="col-sm-6">
        <table class="tov-table">
            <tbody>
                <tr>
                    <td>БСО На руках</td>
                    <td>Из них старых (более 30 дней)</td>
                    <td>Из них старых (более 90 дней)</td>
                </tr>
                <tr>
                    <td class="right">
                        <a>{{ $table['on_hand'] }}</a>
                    </td>
                    <td class="right">
                        <a>{{ $table['older_30'] }}</a>
                    </td>
                    <td class="right">
                        <a>{{ $table['older_90'] }}</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>