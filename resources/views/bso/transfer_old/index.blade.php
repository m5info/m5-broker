@extends('layouts.app')

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h2>Прием передача БСО</h2>
            <a href="/bso/transfer/create" class="btn btn-primary pull-right">Добавить</a>
        </div>
        <div class="block-main">
            <div class="block-sub">
                @if(sizeof($bso_transfers))
                    <table class="tov-table">

                        <tr>
                            <th>ID</th>
                            <th>Тип</th>
                            <th>Статус</th>
                            <th>Агент получатель</th>
                            <th>Агент отправитель</th>
                            <th>Точка продаж</th>
                            <th>Менеджер</th>
                            <th>Курьер</th>
                            <th>Создан</th>
                            <th>Редактирован</th>
                        </tr>

                        @foreach($bso_transfers as $transfer)
                            <tr class="clickable-row" href="{{url ("/bso/transfer/process/$transfer->id/edit/")}}">
                                <td>{{ $transfer->id }}</td>
                                <td>{{ $transfer->type() }}</td>
                                <td>{{ $transfer->status() }}</td>
                                <td>{{ $transfer->user_to ? $transfer->user_to->name : '' }}</td>
                                <td>{{ $transfer->user_from ? $transfer->user_from->name : '' }}</td>
                                <td>{{ $transfer->point_sale ? $transfer->point_sale->title : '' }}</td>
                                <td>{{ $transfer->bso_manager ? $transfer->bso_manager->name : ''}}</td>
                                <td>{{ $transfer->courier ? $transfer->courier->name : ''}}</td>
                                <td>{{ $transfer->created_at }}</td>
                                <td>{{ $transfer->updated_at }}</td>
                            </tr>
                        @endforeach
                    </table>
                @else
                    Нет записей
                @endif

            </div>
        </div>
    </div>
@endsection

@section('js')

    <script>
        $(function(){
            $(document).on('click', '.clickable-row', function(){
                location.href = $(this).attr('href');
            })
        })

    </script>

@endsection