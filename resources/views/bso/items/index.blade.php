@extends('layouts.app')

@section('content')

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="easyui-accordion">
            <div title="БСО - {{$bso->type->title}} - {{$bso->bso_title}}">
                <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    @if((int)$bso->in_basket)
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <br>
                            В корзине
                        </div>
                    @else
                        @include('bso.items.info', ['bso' => $bso])
                    @endif
                </div>
            </div>


            @if($bso->bso_class_id !== 100)
                @if($bso->contract && count($bso->payments) > 0 && $bso->contract->statys_id > 0)

                    @php
                        $accept = "";
                        if($bso->contract && isset(App\Models\Contracts\Contracts::KIND_ACCEPTANCE[$bso->contract->kind_acceptance])){
                            $accept = "- ". App\Models\Contracts\Contracts::KIND_ACCEPTANCE[$bso->contract->kind_acceptance];
                        }
                    @endphp


                    <div title="Договор {{ $accept }}">
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            @include('bso.items.contract', ['contract' => $bso->contract, 'agents' => $agents, 'object_insurer' => $bso->contract->object_insurer])
                        </div>
                    </div>

                    @if($bso->payments)
                        <div title="Платежи">
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                @include('bso.items.peyments', ['payments' => $bso->payments, 'bso'=>$bso])
                            </div>
                        </div>
                    @endif

                @endif



            @endif




        </div>


    </div>
@endsection

@section('js')

    <script>

        $(function () {

            $('#contracts_logs').accordion({
                collapsible: true,
                active: false,
            }).children().children().click();

            $('.easyui-datalist').datalist({


            });

            @if($bso->contract)

                initInfo();
                initInsurer();

            @endif



        });


        function save_bso(key) {

            var agent_id = $('select[name="agent_id"]').val();
            var location_id = $('select[name="location_id"]').val();
            var state_id = $('select[name="state_id"]').val();
            var kind_acceptance = $('select[name="kind_acceptance"]').val();
            var comment = $('input[name="comment"]').val();

            if (res = myPostAjax('/bso/items/'+key+'/save_status', "location_id=" + location_id + "&state_id=" + state_id+ "&comment=" + comment+ "&kind_acceptance=" + kind_acceptance+ "&agent_id=" + agent_id)){
                flashMessage('success', "Данные успешно сохранены!");
            }
        }

        function delete_bso(key) {
            newCustomConfirm(function (result) {
                if (result){
                    res = myPostAjax('/bso/delete_bso_warehouse/' + key + '/delete_bso/', 'id=' + key);
                    if (res.error_state == 0) {
                        flashMessage('success', "Данные успешно удалены!");
                        location.href = "<?php echo url()->previous();?>";
                    }
                    else {
                        flashMessage('warning', res.error_title);
                    }
                }
            }, 'Подтвердите действие', 'Вы действительно хотите удаллить БСО?');
        }

        function sendContractFront(contract_id, type)
        {
            res = myPostAjax('/bso/items/{{$bso->id}}/'+type+'-contract-front/', "contract_id=" + contract_id);
            if (res == 200){
                flashMessage('success', "Данные успешно переданы!");
            }
        }


        @if($bso->contract && $bso->contract->statys_id > 0 && auth()->user()->hasPermission('reports', 'payment_delete'))
        function clearAllContract()
        {
            Swal.fire({
                title: 'Удалить договор и платежи?',
                text: "Вы действительно хотите удалить весь договор",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Да удалить!',
                cancelButtonText: 'Отмена'
            }).then((result) => {
                if (result.value) {
                    window.location = '{{url("/bso/items/{$bso->id}/clear-all-contract")}}';
                }
                });


        }
        @endif

        function process(param) {
            return true;
        }

    </script>

@endsection