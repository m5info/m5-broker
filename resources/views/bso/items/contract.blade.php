@php
    $type = in_array($contract->status_id, [3,4]) ? 'view' : 'edit';
@endphp

<div class="form-horizontal" id="main_container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="contracts_logs" style="margin-top: 15px;">
            <div title="Движение договоров">
                <div>
                    <table class="tov-table dataTable">
                        <tbody>
                            <tr>
                                <th>#</th>
                                <th>Статус</th>
                                <th>Дата действия</th>
                                <th>Пользователь</th>
                                <th>Описание</th>
                            </tr>
                        </tbody>
                        <tbody>
                            @if($contract->logs->count() > 0)
                                @foreach($contract->logs as $key => $log)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{isset(\App\Models\Contracts\Contracts::STATYS[$log->status_id]) ? \App\Models\Contracts\Contracts::STATYS[$log->status_id] : ''}}</td>
                                        <td>{{$log->created_at}}</td>
                                        <td>{{$log->user ? $log->user->name : ""}}</td>
                                        <td>{!! $log->description !!}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">Записи отсутствуют</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <!-- Условия договора -->
        @include("contracts.contract.{$type}.info", ['contract' => $contract])

        @if($type == 'edit')
            <span class="btn btn-primary btn-left" id="controlSaveButtonBox" onclick="saveContract(reload)">
                Сохранить
            </span>
            @if($contract->order_id > 0 && auth()->user()->hasPermission('contracts', 'available_to_front'))
                <span class="btn btn-success btn-left" onclick="sendContractFront('{{$contract->id}}', 'send')">
                    Передать в фронт
                </span>

                <span class="btn btn-info btn-right" onclick="sendContractFront('{{$contract->id}}', 'destroy')">
                    Удалить в фронт
                </span>
            @endif
        @endif
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <!-- Участники договора -->
        @include("contracts.contract.{$type}.insurer_info", ['contract' => $contract])
    </div>
</div>
