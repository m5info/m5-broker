@extends('layouts.frame')


@section('title')

    Настройка акцепта

@endsection

@section('content')


    {{ Form::open(['url' => url("/bso/items/{$bso->id}/edit_accept_status"), 'method' => 'post', 'class' => 'form-horizontal']) }}



    <div class="form-group">
        <div class="col-sm-12 col-lg-6">
            <label class="col-sm-12 control-label">Акцепт</label>
            {{Form::select('kind_acceptance', collect(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE), $bso->contract->kind_acceptance,  ['class' => 'form-control'])}}
        </div>
    </div>


    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
