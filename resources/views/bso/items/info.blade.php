<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <br/>

    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">{{$bso->type->title}}

                </label>
                <div class="col-sm-12">
                    @if(auth()->user()->hasPermission('bso', 'edit_bso_title'))
                        <a href="{{url("/bso/items/{$bso->id}/edit_bso_title")}}" class="fancybox fancybox.iframe underline">{{$bso->bso_title}}
                        </a>
                    @else
                        {{$bso->bso_title}}
                    @endif
                    @if(strlen($bso->bso_blank_title))
                        <br/>({{$bso->bso_blank_title}})
                    @endif
                </div>
            </div>
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Организация получатель</label>
                <div class="col-sm-12">
                    @if($bso->supplier_org)
                        @if(auth()->user()->hasPermission('bso', 'edit_supplier_org'))
                            <a href="{{url("/bso/items/{$bso->id}/edit_supplier_org")}}" class="fancybox fancybox.iframe underline">{{$bso->supplier_org->title}}</a>
                        @else
                            {{$bso->supplier_org->title}}
                        @endif
                    @endif
                </div>
            </div>
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Страховая компания</label>
                <div class="col-sm-12">
                    {{$bso->supplier ? $bso->supplier->title : ""}}
                </div>
            </div>
            <div class="col-sm-2">
                <label class="col-sm-12 control-label">Точка продаж</label>
                <div class="col-sm-12">
                    {{$bso->point_sale ? $bso->point_sale->title : ""}}
                </div>
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-3 col-lg-3">
                <label class="col-sm-12 control-label">Агент получивший полис</label>
                <div class="col-sm-12">
                    @if($bso->is_reserved == 1 && $bso->bso_cart_id > 0)
                        <a href="/bso/transfer/?bso_cart_id={{$bso->bso_cart_id}}" class="underline">Резерв: {{($bso->cars->user_to)?$bso->cars->user_to->name:''}}</a>
                    @else
                        {{($bso->agent)?$bso->agent->name:''}}
                    @endif

                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <label class="col-sm-12 control-label">У кого сейчас</label>
                <div class="col-sm-12">
                    {{($bso->user)?$bso->user->name:''}}
                </div>
            </div>
            @if($bso->contract_receipt && $bso->bso_class_id == 100)
                <div class="col-sm-2 col-lg-2">
                    <label class="col-sm-12 control-label">Договор</label>
                    <div class="col-sm-12">
                        <a href="{{url("/bso/items/{$bso->contract_receipt->bso->id}/")}}" target="_blank">{{$bso->contract_receipt->bso->bso_title}}</a>
                    </div>
                </div>
            @endif

            <div class="col-sm-4 col-lg-4">
                <div class="row col-sm-12">
                    <div class="col-sm-12 col-lg-6">
                        <label class="col-sm-12 control-label">Событие</label>
                        {{Form::select('location_id', \App\Models\BSO\BsoLocations::query()->where('can_be_set_manually', 1)->get()->pluck('title', 'id'), $bso->location_id,  ['class' => 'form-control'])}}
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <label class="col-sm-12 control-label">Статус</label>
                        {{Form::select('state_id', \App\Models\BSO\BsoState::all()->pluck('title', 'id'), $bso->state_id,  ['class' => 'form-control'])}}
                        @if($bso->state_id == 3)
                            @if($bso->file_id > 0)
                                <br/>
                                <a href="{{ url($bso->scan->url) }}" target="_blank">{{$bso->scan->original_name}}</a>
                            @endif
                        @endif
                    </div>
                    @if($bso->contract && $bso->contract->statys_id > 0)
                        @if(auth()->user()->hasPermission('bso', 'change_agent'))
                            <div class="col-sm-12 col-lg-6">
                                <label class="control-label">Передача БСО</label>
                                {{Form::select('agent_id', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id'), $bso->agent_id,  ['class' => 'form-control select2'])}}
                            </div>
                        @endif
                        @if(auth()->user()->hasPermission('contracts', 'contract_unacceptance'))
                            <div class="col-sm-12 col-lg-6">
                                <label class="control-label">Акцепт: </label>
                                <a href="{{url("/bso/items/{$bso->id}/edit_accept_status")}}" class="fancybox fancybox.iframe underline">{{ \App\Models\Contracts\Contracts::KIND_ACCEPTANCE[$bso->contract->kind_acceptance] }}
                                </a>


                            </div>
                        @else
                            {{ \App\Models\Contracts\Contracts::KIND_ACCEPTANCE[$bso->contract->kind_acceptance] }}
                        @endif

                    @endif
                </div>
            </div>
            @if(auth()->user()->hasPermission('bso', 'bso_change_status_save'))
                <div class="col-sm-1 col-lg-1">
                    <div class="col-sm-2 col-lg-2">
                        <span class="btn btn-success btn-left" style="margin-top: 22px;" id="save_bso" onclick="save_bso({{ $bso->id }})">Сохранить</span>
                    </div>
                </div>
            @endif
            @if(auth()->user()->hasPermission('bso', 'delete_bso_warehouse') && $bso->state_id == 0)
                <div class="col-sm-1 col-lg-1">
                    <div class="col-sm-2 col-lg-2">
                        <span class="btn btn-primary btn-left" style="margin-top: 22px;" id="delete_bso" onclick="delete_bso({{ $bso->id }})">Удалить БСО</span>
                    </div>
                </div>

            @endif
        </div>
    </div>
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-4 col-lg-4">
                @foreach($bso->comments as $comment)
                    <div class="row col-lg-12" style="padding: 14px 20px;margin-bottom: 18px;margin-left: 0px;list-style: none;background-color: #f5f5f5;border-radius: 2px;">
                        {{ $comment->comment }}
                    </div>
                @endforeach
                <label class="col-lg-12 control-label">Сообщение</label>
                    {{Form::text('comment', '',  ['class' => 'form-control'])}}
            </div>


            @if($bso->contract && $bso->contract->statys_id > 0 && auth()->user()->hasPermission('reports', 'payment_delete'))
                <div class="col-sm-8 col-lg-8">
                    <span class="btn btn-danger btn-right" onclick="clearAllContract()">Развязать договор</span>
                </div>
            @endif

        </div>
    </div>
    <div class="divider"></div>

    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Приема из СК</label>
                <div class="col-sm-12">
                    {{setDateTimeFormatRu($bso->time_target, 1)}}
                </div>
                <label class="col-sm-12 control-label">Создан</label>
                <div class="col-sm-12">
                    {{setDateTimeFormatRu($bso->time_create)}}
                </div>
            </div>
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Передача Агенту</label>
                <div class="col-sm-12">
                    {{setDateTimeFormatRu($bso->transfer_to_agent_time)}}
                </div>
            </div>
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Приема в организацию</label>
                <div class="col-sm-12">
                    {{setDateTimeFormatRu($bso->transfer_to_org_time)}}
                </div>
            </div>
            <div class="col-sm-3">
                <label class="col-sm-12 control-label">Передача в СК</label>
                <div class="col-sm-12">
                    {{setDateTimeFormatRu($bso->transfer_to_sk_time)}}
                </div>
            </div>
        </div>


    </div>



    <div class="divider"></div>


        <div class="form-horizontal">
            <table class="tov-table">
                <tr class="sort-row">
                    <th>Дата/время</th>
                    <th>Событие</th>
                    <th>Статус</th>
                    <th>Выдал</th>
                    <th>Получил</th>
                    <th>Оператор</th>
                    <th>Акт</th>
                </tr>
                <tbody>


                    @if(sizeof($bso->logs))

                        @foreach($bso->logs as $bso_logs)

                            <tr class="clickable-row">
                                <td>{{setDateTimeFormatRu($bso_logs->log_time)}}</td>
                                <td>{{$bso_logs->bso_location->title}}</td>
                                <td>{{$bso_logs->bso_state->title}}</td>
                                @if($bso_logs->act)
                                    <td>{{($bso_logs->act->user_from)?$bso_logs->act->user_from->name:''}}</td>
                                    <td>{{($bso_logs->act->user_to)?$bso_logs->act->user_to->name:''}}</td>
                                    <td>{{($bso_logs->act->bso_manager)?$bso_logs->act->bso_manager->name:''}}</td>
                                    <td>
                                        <a style="color:black !important;text-decoration:underline;" href="{{$bso_logs->act->get_link()}}">Акт № {{$bso_logs->act->act_number}}</a>
                                    </td>

                                @else
                                    <td></td>
                                    <td></td>
                                    <td>{{$bso_logs->user ? $bso_logs->user->name : ''}}</td>
                                    <td></td>
                                @endif

                            </tr>

                        @endforeach
                    @endif


                </tbody>
            </table>
        </div>


</div>