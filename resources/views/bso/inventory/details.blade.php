@extends('layouts.app')

@section('title')

@endsection

@section('content')
    <div class="page-heading">
        <h1 class="inline-h1">{{$title}}</h1>
        <span class="btn btn-success pull-right" id="bso_inventory_export_xls">Выгрузка в .xls</span>
    </div>

   {{-- <div class="block-inner">

    </div>--}}

    <div class="block-inner">
        <div id="table_full"></div>
    </div>

    @include('_chunks._pagination',['callback'=>'loadItems'])

@endsection

@section('js')
    <script type="text/javascript">


       var request = JSON.parse('{!! json_encode(request()->query()) !!}');
       $(function() {

           loadItems();

           $(document).on('click', '#bso_inventory_export_xls', function () {
               var data = { pageCount: -1,  PAGE: 1, };
               $.each(request, function(key, val){
                   data[key] = val;
               });
               var query = $.param({ method:  'BSO\\InventoryBsoController@get_details_table', param: data });
               location.href = '/exports/table2excel?'+query;
           });

       });




       function loadItems() {

           var data = {
               pageCount: $("#pageCount").val(),
               PAGE: PAGE,
           };

           $.each(request, function(key, val){
               data[key] = val;
           });


           $('#page_list').html('');
           $('#table_row').html('');
           $('#view_row').html(0);
           $('#max_row').html(0);

           loaderShow();

           $.post("/bso/inventory_bso/get_details_table", data, function (response) {
               $('#table_full').html(response.html);
               $('#view_row').html(response.view_row);
               $('#max_row').html(response.max_row);

               ajaxPaginationUpdate(response.page_max,loadItems);
               /*$('#page_list').pagination({
                   total:response.page_max,
                   pageSize:1,
                   pageNumber: PAGE,
                   layout:['first','prev','links','next','last'],
                   onSelectPage: function(pageNumber, pageSize){
                       setPage(pageNumber);
                   }
               });*/
               loaderHide();


           }).always(function() {
               loaderHide();
           });
       }

    </script>

    <style>

        .bso_table {
            /*font: 12px arial;*/
            border: 1px solid #777;
            border-collapse: collapse;
        }
        .bso_table td, th {
            border: 1px solid #777;
            padding: 5px;
           /* font: 12px arial;*/
        }

        .bso_table th {
            background-color: #EEE;
        }

        .bso_table td {
            background-color: #FFF;
        }

        input[type=button] {
            cursor: pointer;
        }

    </style>

@endsection

