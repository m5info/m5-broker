<table class="table table-bordered bso_items_table">
    <thead>
        <tr>
            <th>№ п/п</th>
            <th>{{ Form::checkbox('bso_ids_all', 0, 0, []) }}</th>
            <th>Организация</th>
            <th>Страховая компания</th>
            <th>Вид страхования</th>
            <th>№ полиса / квит. / сер.карт с</th>
            <th>Тип</th>
            <th>№ бланка</th>
            <th>Точка оборота БСО</th>
            <th>Событие</th>
            <th>Статус</th>
            <th>Агент</th>
            <th>Куратор</th>
            <th>Дата приема из СК</th>
            <th>Дней на складе</th>
            <th>Дата последней операции</th>
            <th>Дней у агента</th>
            <th>Акт приема передачи в СК</th>
            <th>Номер Отчета</th>
            <th>Полис квитанции</th>
            <th>История</th>
        </tr>
    </thead>

    <tbody>
    @if(sizeof($acts))
        @foreach($acts as $key => $bso)
            <tr class="@php if( $bso->TableTdColored) echo 'colored-red'; @endphp">
                <td>{{ $key+1 }}</td>
                <td>{{ Form::checkbox('bso_ids[]', $bso->id, false, []) }}</td>
                <td>{{ $bso->supplier_org ? $bso->supplier_org->title : "" }}</td>
                <td>{{ $bso->supplier ? $bso->supplier->title : "" }}</td>
                <td>{{ $bso->product ? $bso->product->title : "" }}</td>
                <td>{{ $bso->bso_title }}</td>
                <td>{{ $bso->bso_class_id == 100 ? "Квитанция" : "БСО"}}</td>
                <td>{{ $bso->bso_blank_title }}</td>
                <td>{{ $bso->point_sale ? $bso->point_sale->title : "" }}</td>
                <td>{{ $bso->bso_locations ? $bso->bso_locations->title : "" }}</td>
                <td>{{ $bso->bso_states ? $bso->bso_states->title : "" }}</td>
                <td>{{ $bso->user ? $bso->user->name : "" }}</td>
                <td>{{ $bso->user && $bso->user->curator ? $bso->user->curator->name : "" }}</td>
                <td>{{ setDateTimeFormatRu($bso->time_create, 1) }}</td>
                <td>{{ $bso->time_on_stock() }}</td>
                <td>{{ setDateTimeFormatRu($bso->last_operation_time, 1) }}</td>
                <td>{{ $bso->time_on_agent() }}</td>
                <td>
                    @if($bso->act_sk)
                        <a target="_blank" href="/bso_acts/acts_sk/{{$bso->bso_supplier_id}}/acts/{{$bso->act_sk->id}}/edit">{{$bso->act_sk->title}}</a>
                    @else
                        <a target="_blank" href="javascript:void(0);">---</a>
                    @endif
                </td>
                <td>
                    @if(count($bso->payments))
                        @foreach($bso->payments as $payment)
                            @if($payment->reports_order_id != 0 || !empty($payment->reports_order_id))
                                <a target="_blank" href="/reports/order/{{$payment->reports_order_id}}">{{$payment->reports_order_id}}</a>
                            @else
                                Отсутствует
                            @endif
                        @endforeach
                    @else
                        Отсутствует
                    @endif
                </td>
                <td>@if($bso->bso_class_id == 100 && $bso->contract_receipt && $bso->contract_receipt->bso)<a href="{{url("/bso/items/{$bso->contract_receipt->bso->id}/")}}" target="_blank">{{$bso->contract_receipt->bso->bso_title}}</a>@endif</td>
                <td><a href="{{url("/bso/items/{$bso['id']}/")}}" target="_blank">подробнее</a></td>
            </tr>
        @endforeach

    @else
        <tr>
            <td colspan="18" class="text-center">БСО отстутствует</td>
        </tr>
    @endif
    </tbody>
</table>