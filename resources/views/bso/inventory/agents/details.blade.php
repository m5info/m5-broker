@extends('layouts.app')

@section('title')

@endsection


@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">{{$title}}</h1>
        <span class="btn btn-success pull-right" id="bso_inventory_export_xls">Выгрузка в .xls</span>
        <span class="btn btn-primary pull-right" style="margin-right: 5px" onclick="refreshAgentDates()">Обнулить даты <i class="fa fa-undo"></i></span>
    </div>

    <div class="block-inner row">
        {{--<div class="col-sm-3 col-md-3">
            <div class="filter-group">
                <label for="location_id">Событие</label>
                {{Form::select('location_id', \App\Models\BSO\BsoLocations::all()->where('is_actual', 1)->pluck('title', 'id')->prepend('Все', -1), -1, ['class' => 'form-control select2-all', 'id'=>'location_id', 'onchange'=>'loadItems();'])}}
            </div>
        </div>--}}
        <div class="col-sm-3 col-md-3">
            <div class="filter-group">
                <label class="control-label">Организация</label>
                {{ Form::select('organization_id', \App\Models\Organizations\Organization::all()->pluck('title', 'id')->prepend('Все', 0), old('organization_id'), ['class' => 'form-control select2', 'id'=>'organization_id', 'onchange'=>'loadItems();']) }}
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="filter-group">
                <label for="state_id">Статус</label>
                {{Form::select('state_id', \App\Models\BSO\BsoState::all()->where('is_actual', 1)->pluck('title', 'id')->prepend('Не выбрано', -1), -1, ['class' => 'form-control select2-all', 'id'=>'state_id', 'onchange'=>'loadItems();'])}}
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="filter-group">
                <label for="date_from">C</label>
                {{Form::text('from', '', ['class' => 'form-control date datepicker', 'id'=>'from', 'onchange'=>'loadItems();'])}}
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="filter-group">
                <label for="date_to">По</label>
                {{Form::text('to', '', ['class' => 'form-control date datepicker', 'id'=>'to', 'onchange'=>'loadItems();'])}}
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <label class="col-sm-4 control-label">Тип</label>
            <div class="filter-group">
                {{ Form::select('bso_class_id', collect([-1 => 'Все', 1=>'БСО', 100=>'Квитанция']), '', ['class' => 'form-control', 'required', 'onchange'=>'loadItems();']) }}
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <label class="col-sm-4 control-label">Просроченные</label>
            <div class="filter-group">
                {{ Form::select('overdue', collect([0 => 'Нет', $limits[1] => $limits[1] .' дней', $limits[2] => $limits[2] .' дней']), request('overdue', 0), ['class' => 'form-control', 'required', 'onchange'=>'loadItems();']) }}
            </div>
        </div>
    </div>


    @include('_chunks._vue_table')

    @include('_chunks/_pagination',['callback'=>'loadItems'])



@endsection

@section('js')



    <script type="text/javascript">

        var request = JSON.parse('{!! json_encode(request()->query()) !!}');
        $(function() {

            loadItems();

            $(document).on('click', '#bso_inventory_export_xls', function () {
                var data = { page_count: -1,  PAGE: 1, };
                $.each(request, function(key, val){
                    data[key] = val;
                });
                var query = $.param({ method:  'BSO\\InventoryAgentsController@get_details_table', param: data });
                location.href = '/exports/table2excel?'+query;
            });

            $(document).on('change', '[name="bso_ids_all"]', function(){
                var checked = $(this).prop('checked');
                $('[name="bso_ids[]"]').prop('checked', checked);
            });

        });


        function loadItems() {

            var data = {
                page_count: $("#page_count").val(),
                PAGE: PAGE,
                state_id: $('[name = "state_id"]').val().toString(),
                from: $('[name = "from"]').val(),
                to: $('[name = "to"]').val(),
                organization_id: $('[name="organization_id"]').val(),
                bso_class_id: $('[name = "bso_class_id"]').val(),
                overdue: $('[name="overdue"]').val()
            };

            $.each(request, function(key, val){
                data[key] = val;
            });

            $('#page_list').html('');
            $('#table_row').html('');
            $('#view_row').html(0);
            $('#max_row').html(0);

            loaderShow();

            $.post("/bso/inventory_agents/get_details_table", data, function (response) {
                $('#table').html(response.html);
                $('#view_row').html(response.view_row);
                $('#max_row').html(response.max_row);

                ajaxPaginationUpdate(response.page_max,loadItems);

                loaderHide();

            }).always(function() {
                loaderHide();
            });
        }

        function refreshAgentDates() {

            var bso_ids = [];

            $.each($('[name="bso_ids[]"]:checked'), function(k,v){
                bso_ids.push($(v).val());
            });
            
            if (bso_ids.length === 0){
                Swal.fire(
                    'Предупреждение',
                    'Пожалуйста выберите необходимые БСО',
                    'warning'
                );
            }else{
                $.ajax({
                    url: '{{url('bso/inventory_agents/refresh_agent_date')}}',
                    type: 'POST',
                    async: false,
                    data: {bso_ids: bso_ids},
                    success: function(response) {
                        reload();
                    }
                });
            }
        }
    </script>

    <style>

        .bso_table {
            font: 12px arial;
            border: 1px solid #777;
            border-collapse: collapse;
        }


        .bso_table th {
            background-color: #EEE;
        }

        .bso_table td {
            background-color: #FFF;
        }
        .bso_table tr.colored-red td{
            background-color: #ffcccc;
        }
        input[type=button] {
            cursor: pointer;
        }

    </style>

@endsection

