<table class=" table table-bordered">
    <thead>
    <tr>
        <th>Агент</th>
        <th>Куратор</th>
        <th>Всего</th>
        <th>Из них старых (более {{$limits[1]}} дней) *</th>
        <th>Из них старых (более {{$limits[2]}} дней)</th>
    </tr>
    </thead>
    <tbody>
    @foreach($agents as $agent)
        @php($agent_bsos = isset($bso_items[$agent['id']]) ? $bso_items[$agent['id']] : collect([]))
        @if(count($agent_bsos) > 0)

            @php($agent_count_limit1 = $agent_bsos->filter(function($item){ $days=\App\Models\Characters\Agent::DAYS_LIMITS[1]; return strtotime($item->transfer_to_agent_time) < time() - 60*60*24*$days; })->count())
            @php($agent_count_limit2 = $agent_bsos->filter(function($item){ $days=\App\Models\Characters\Agent::DAYS_LIMITS[2]; return strtotime($item->transfer_to_agent_time) < time() - 60*60*24*$days; })->count())

            <tr
                    @if($agent_count_limit2 > 0)
                    style="background-color: #DDF;"
                    @elseif($agent_count_limit1 > 0)
                    style="background-color: #FDB;"
                    @endif
            >
                <td>{{ $agent->name }}</td>
                <td>{{ $agent->perent ? $agent->perent->name : "" }}</td>
                <td>
                    <a href="/bso/inventory_agents/details/?agent_id={{$agent->id}}&point_sale_id={{request()->get('point_sale_id') ?? '-1'}}&product_id={{request()->get('product_id') ?? '-1'}}&insurance_companies_id={{request()->get('insurance_companies_id') ?? '-1'}}&nop_id={{request()->get('nop_id') ?? '-1'}}&types=bso_in"
                       data-type="stock" data-type_bso="4" target="_blank">
                        {{ $agent_bsos->count() }}
                    </a>
                </td>
                <td>
                    <a href="/bso/inventory_agents/details/?agent_id={{$agent->id}}&point_sale_id={{request()->get('point_sale_id') ?? '-1'}}&product_id={{request()->get('product_id') ?? '-1'}}&insurance_companies_id={{request()->get('insurance_companies_id') ?? '-1'}}&nop_id={{request()->get('nop_id') ?? '-1'}}&overdue={{$limits[1]}}"
                       data-type="stock" data-type_bso="4" target="_blank">
                        {{ $agent_count_limit1 }}
                    </a>
                </td>
                <td>
                    <a href="/bso/inventory_agents/details/?agent_id={{$agent->id}}&point_sale_id={{request()->get('point_sale_id') ?? '-1'}}&product_id={{request()->get('product_id') ?? '-1'}}&insurance_companies_id={{request()->get('insurance_companies_id') ?? '-1'}}&nop_id={{request()->get('nop_id') ?? '-1'}}&overdue={{$limits[2]}}"
                       data-type="stock" data-type_bso="4" target="_blank">
                        {{ $agent_count_limit2 }}

                    </a>
                </td>
            </tr>
        @endif
    @endforeach

    </tbody>
</table>