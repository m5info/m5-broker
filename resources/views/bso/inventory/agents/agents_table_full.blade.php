<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>Номер БСО</th>
        <th>Вид страхования</th>
        <th>Страховая компания</th>
        <th>Событие</th>
        <th>Статус</th>
        <th>Дата приёма из СК</th>
        <th>Дней на складе</th>
        <th>Дата события</th>
        <th>Дней у агента</th>
        <th>Агент</th>
        <th>Руководитель</th>
        <th>Страхователь</th>
        <th>Отчёт в СК</th>
        <th>Номер договора</th>
        <th>Страховая премия</th>
        <th>Статус платежа</th>
    </tr>
    </thead>
    <tbody>
    @foreach($agents as $agent)

        @php($agent_bsos = isset($bso_items[$agent['id']]) ? $bso_items[$agent['id']] : collect([]))

        @if(count($agent_bsos) > 0)

            <tr>
                <td colspan="16" style='background-color:#f1f1f1'>
                    <b> {{ $agent->name }}</b>
                </td>
            </tr>

            @foreach($agent_bsos as $bso)
                <tr>
                    <td>{{ $bso->bso_title }}</td>       {{-- НОМЕР БСО --}}
                    <td>{{ $bso->product ? $bso->product->title : "" }}</td>       {{-- ВИД СТРАХОВАНИЯ --}}
                    <td>{{ $bso->supplier ? $bso->supplier->title : "" }}</td>       {{-- СТРАХОВАЯ КОМПАНИЯ --}}
                    <td>{{ $bso->bso_locations ? $bso->bso_locations->title : "" }}</td>       {{-- СОБЫТИЕ --}}
                    <td>{{ $bso->bso_states ? $bso->bso_states->title : "" }}</td>       {{-- СТАТУС --}}
                    <td>{{ setDateTimeFormatRu($bso->time_create, 1) }}</td>        {{-- Дата приема из СК --}}
                    <td>{{ $bso->time_on_stock() }}</td>        {{-- Дней на складе --}}
                    <td>{{ setDateTimeFormatRu($bso->last_operation_time, 1) }}</td>        {{-- Дата события --}}
                    <td>{{ $bso->time_on_agent() }}</td>        {{-- Дней у агента --}}
                    <td>{{ $agent->name }}</td>        {{-- Агент --}}
                    <td>{{ $agent->parent ? $agent->parent->name : ""}}</td>        {{-- Руководитель --}}
                    <td>
                        @if($bso->bso_class_id == 100 && isset($bso->contract_receipt) && isset($bso->contract_receipt->bso_title))
                            {{ $bso->contract_receipt && isset($bso->contract_receipt->insurer) ? $bso->contract_receipt->insurer->title : ""}}
                        @elseif($bso->contract)
                            {{ $bso->contract && isset($bso->contract->insurer) ? $bso->contract->insurer->title : ""}}
                        @endif
                    </td>           {{-- Страхователь --}}
                    <td>
                        @if($bso->act_sk)
                            {{$bso->act_sk->title}}
                        @endif
                    </td>       {{-- Отчет в страховую --}}
                    <td>
                    @if($bso->bso_class_id == 100 && isset($bso->contract_receipt) && isset($bso->contract_receipt->bso_title))
                        {{$bso->contract_receipt->bso->bso_title}}
                    @elseif($bso->contract)
                        {{$bso->bso_title}}
                    @endif
                    </td>           {{-- Номер договора --}}
                    <td>
                        @if($bso->bso_class_id == 100 && isset($bso->contract_receipt) && isset($bso->contract_receipt->payments) && isset($bso->contract_receipt->payments->last()->payment_total))
                            {{$bso->contract_receipt->payments->last()->payment_total}}
                        @elseif($bso->contract && isset($bso->contract->payments) && isset($bso->contract->payments->last()->payment_total))
                            {{$bso->contract->payments->last()->payment_total}}
                        @endif
                    </td>           {{-- Страховая премия --}}
                    <td>
                        @if($bso->bso_class_id == 100 && isset($bso->contract_receipt) && isset($bso->contract_receipt->statys_id) && isset($bso->contract_receipt->payments) && isset($bso->contract_receipt->payments->last()->statys_id))
                            {{App\Models\Contracts\Payments::STATUS[$bso->contract_receipt->payments->last()->statys_id]}}
                        @elseif($bso->contract && isset($bso->contract->payments) && isset($bso->contract->payments->last()->statys_id))
                            {{App\Models\Contracts\Payments::STATUS[$bso->contract->payments->last()->statys_id]}}
                        @endif
                    </td>           {{-- Статус платежа --}}
                </tr>
            @endforeach
            </tr>
        @endif
    @endforeach

    </tbody>
</table>