@extends('layouts.app')

@section('content')

<div class="page-heading">
    <h1 class="inline-h1">Инвентаризация по агентам</h1>
    <span class="btn btn-success btn-right" id="obj_export_xls" >Выгрузка в .xls</span>
    <span class="btn btn-success btn-right" id="full_obj_export_xls" >Полная выгрузка .xls</span>
</div>

<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="filter-group">

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    {{ Form::select('agent_id',\App\Models\User::getALLUserWhere()->get()->pluck('name', 'id')->prepend('Агент',-1), \Request::query('agent_id'), ['class' => 'form-control select2', 'id'=>'agent_id', 'onchange'=>'loadItems()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    {{ Form::select('nop_id', \App\Models\User::getALLUserWhere()->where('is_parent',1)->get()->pluck('name', 'id')->prepend('Руководитель', -1), \Request::query('nop_id'), ['class' => 'form-control select2', 'id'=>'nop_id', 'onchange'=>'loadItems()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    {{ Form::select('point_sale_id', \App\Models\Settings\PointsSale::where('is_actual', 1)->get()->pluck('title', 'id')->prepend('Точка продаж', -1), \Request::query('point_sale_id'), ['class' => 'form-control select2-all', 'id'=>'point_sale_id', 'onchange'=>'loadItems()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    {{ Form::select('product_id', \App\Models\Directories\Products::all()->pluck('title', 'id')->prepend('Продукт', -1), \Request::query('product_id'), ['class' => 'form-control select2-all', 'id'=>'product_id', 'onchange'=>'loadItems()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    {{ Form::select('insurance_companies_id', \App\Models\Directories\InsuranceCompanies::all()->pluck('title', 'id')->prepend('Страховая компания', -1), \Request::query('insurance_companies_id'), ['class' => 'form-control select2-all', 'id'=>'insurance_companies_id', 'onchange'=>'loadItems()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
                <br>
                <div class="btn-group col-xs-12">
                    <button id="reset"  class="btn btn-primary">
                        <span aria-hidden="true">&times; очистить фильтр</span>
                    </button>
                </div>


            </div>
        </div>
    </div>
</div>

<div class="block-inner sorting col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow: auto;">
    <div id="agents_table"></div>
</div>

@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        loadItems();

        $('#obj_export_xls').on('click', function(){
            var query = $.param({ method:  'BSO\\InventoryAgentsController@get_agents_table', param: getData() });
            location.href = '/exports/table2excel?'+query;
        })

        $('#full_obj_export_xls').on('click', function(){
            var query = $.param({ method:  'BSO\\InventoryAgentsController@get_agents_full_table', param: getData() });
            location.href = '/bso/inventory_agents/reports_table_export?'+query;
        })
    });

    /*КНОПКА ОЧИСТКИ ФИЛЬТТРА*/
    $('#reset').on('click',function () {
        let first = {};
        $('.filter-group select').each(function () {
            console.log($(this).children('option:first-child').val());
            $(this).select2('val', $(this).children('option:first-child').val(),'1');
        });
    }) ;


    function loadItems() {
        loaderShow();
        $.post("/bso/inventory_agents/get_agents_table", getData(), function (response) {
            $('#agents_table').html(response.html);
        }).always(function() {
            loaderHide();
        });
    }


    function getData(){
        return {
            agent_id:$("#agent_id").val(),
            nop_id:$("#nop_id").val(),
            point_sale_id:$("#point_sale_id").val(),
            product_id:$("#product_id").val(),
            insurance_companies_id:$("#insurance_companies_id").val(),
        }

    }

</script>
@endsection

