<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">


    @php $user_balances = $agent->getBalanceList(); @endphp

    <input type="hidden" value="0" id="limit_ban">
    <h3>Общая информация</h3>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th rowspan="2">БСО На руках</th>
            <th rowspan="2">Из них просроченных</th>
            <th colspan="3" class="center">Фин. долги</th>
            <th rowspan="2" class="center">Балансы</th>
        </tr>
        <tr>
            <th class="center">Нал</th>
            <th class="center">Безнал</th>
            <th class="center">Всего</th>
        </tr>
        </thead>
        <tbody>

        <tr {{--style="background-color: #FDD;"--}}>
            <td class="right"><a target="_blank"
                                 href="{{url("/bso/inventory_agents/details/?agent_id={$agent->id}&point_sale_id=-1&type_bso_id=-1&nop_id=-1&types=bso_in")}}">{{$agent->getUserLimitBSOToProduct(0, 1)}}</a>
            </td>
            <td class="right"><a target="_blank"
                                 href="{{url("/bso/inventory_agents/details/?agent_id={$agent->id}&point_sale_id=-1&type_bso_id=-1&nop_id=-1&types=bso_in")}}">{{$agent->getUserLimitBSOToProduct(0, 1, \App\Models\Characters\Agent::DAYS_LIMITS[1])}}</a>
            </td>

            <td class="right"><a target="_blank"
                                 href="{{url("/finance/debts/{$agent->id}/detail")}}">{{ isset($agent_summary['cash']) ? getPriceFormat($agent_summary['cash']) : "0,00" }}</a>
            </td>
            <td class="right"><a target="_blank"
                                 href="{{url("/finance/debts/{$agent->id}/detail")}}">{{ isset($agent_summary['sk']) ? getPriceFormat($agent_summary['sk']) : "0,00" }}</a>
            </td>
            <td class="right"><a target="_blank"
                                 href="{{url("/finance/debts/{$agent->id}/detail")}}">{{ isset($agent_summary['all']) ? getPriceFormat($agent_summary['all']) : "0,00" }}</a>
            </td>

            <td class="right">

                @php $balance_all = 0 @endphp
                @foreach($user_balances as $balance)

                    {{($balance->title)}}<br/>
                    @php $balance_all += $balance->balance @endphp

                @endforeach


                Итого: {{titleFloatFormat($balance_all)}}
            </td>

        </tr>
        </tbody>
    </table>
    <br>

    @if($parent)
        @php $parent_balances = $parent->getBalanceList(); @endphp

        <h3>Общая информация руководителя и группы</h3>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th rowspan="2">БСО На руках</th>
                <th rowspan="2">Из них просроченных</th>
                <th colspan="3" class="center">Фин. долги</th>
                <th rowspan="2" class="center">Балансы</th>
            </tr>
            <tr>
                <th class="center">Нал</th>
                <th class="center">Безнал</th>
                <th class="center">Всего</th>
            </tr>
            </thead>
            <tbody>

            <tr {{--style="background-color: #FDD;"--}}>
                <td class="right"><a target="_blank"
                                     href="{{url("/bso/inventory_agents/details/?agent_id={$parent->id}&point_sale_id=-1&type_bso_id=-1&nop_id=-1&types=bso_in")}}">{{$parent->getChildsLimitBSOToProduct(0, 1)}}</a>
                </td>
                <td class="right"><a target="_blank"
                                     href="{{url("/bso/inventory_agents/details/?agent_id={$parent->id}&point_sale_id=-1&type_bso_id=-1&nop_id=-1&types=bso_in")}}">{{$parent->getChildsLimitBSOToProduct(0, 1, \App\Models\Characters\Agent::DAYS_LIMITS[1])}}</a>
                </td>

                <td class="right"><a target="_blank"
                                     href="{{url("/finance/debts/{$parent->id}/detail")}}">{{ isset($parent_summaries['cash']) ? getPriceFormat($parent_summaries['cash']) : "0,00" }}</a>
                </td>
                <td class="right"><a target="_blank"
                                     href="{{url("/finance/debts/{$parent->id}/detail")}}">{{ isset($parent_summaries['sk']) ? getPriceFormat($parent_summaries['sk']) : "0,00" }}</a>
                </td>
                <td class="right"><a target="_blank"
                                     href="{{url("/finance/debts/{$parent->id}/detail")}}">{{ isset($parent_summaries['all']) ? getPriceFormat($parent_summaries['all']) : "0,00" }}</a>
                </td>

                <td class="right">

                    @php $parent_balance_all = 0 @endphp
                    @foreach($parent_balances as $balance)

                        {{($balance->title)}}<br/>
                        @php $parent_balance_all += $balance->balance @endphp

                    @endforeach


                    Итого: {{titleFloatFormat($parent_balance_all)}}
                </td>

            </tr>
            </tbody>
        </table>

    @endif
    <br>


    <h3 class="inline-block">Лимиты</h3>
    <span class="btn btn-primary inline-block" style="display: inline-block; float: right; margin-bottom: 10px;"
          onclick="openFancyBoxFrame('{{url("/users/limit/?user_id={$agent->id}&finish=info")}}')"
          style="width: 100%;">
        Редактировать лимиты
    </span>

    @if(sizeof($products))
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Тип</th>
                <th>Лимит</th>
                <th>У агента</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr {{--style="background-color: #FEE;"--}}>
                    <td>{{$product->title}}</td>
                    <td>{{$agent->getUserLimitBSOToProduct($product->id, 0)}}</td>
                    <td>{{$agent->getUserLimitBSOToProduct($product->id, 1)}}</td>
                </tr>


            @endforeach
            </tbody>
        </table>
    @else
        {{ trans('form.empty') }}
    @endif


    <br>
    <h3 class="inline-block">Агентские договоры</h3>
    @if(sizeof($organizations))
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Организация</th>
                <th>Номер договора</th>
                <th>Дата начала - окончания</th>
            </tr>
            </thead>
            <tbody>
            @foreach($organizations as $organization)
                @php
                    $agent_contract = $agent->getAgentContracts($organization->id);
                @endphp
                @if($agent_contract)
                    <tr class="bso_table_tr {{$agent_contract->end_date < date('Y-m-d') ? 'bso_table_td_date':'bso_table_td_simple'}}"
                        onclick="openFancyBoxFrame('{{url("/users/users/{$agent->id}/agent_contracts/{$agent_contract->id}/edit?finish=info")}}')">
                        <td>{{$organization->title}}</td>
                        <td>{{$agent_contract->agent_contract_title}}</td>
                        <td>{{setDateTimeFormatRu($agent_contract->begin_date, 1)}}
                            - {{setDateTimeFormatRu($agent_contract->end_date, 1)}}</td>
                    </tr>
                @else
                    <tr class="bso_table_tr bso_table_td_simple"
                        onclick="openFancyBoxFrame('{{url("/users/users/{$agent->id}/agent_contracts/0/edit?finish=info")}}')">
                        <td>{{$organization->title}}</td>
                        <td colspan="2" class="text-center">Отсутствует</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    @else
        <br>
        <p>{{ trans('form.empty') }}</p>
    @endif


</div>

<div class="clearfix">

</div>

<div class="col-12" style="padding: 0 15px;">
    <br>
    <h3 class="inline-block">Резервные акты</h3>
    @if(sizeof($reserve_acts))
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Акт</th>
                <th>Номер заявки</th>
                <th>Время создания</th>
                <th>Тип</th>
                <th>Передал</th>
                <th>Принял</th>
                <th>Сотрудник</th>
                <th>Статус</th>
                <th>Точка продаж</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reserve_acts as $reserve_act)

                <tr class="clickable-row-blank bso_table_tr reserve_acts"
                    data-href="/bso/transfer/?bso_cart_id={{$reserve_act['id']}}" style="cursor: pointer;">
                    <td>{{$reserve_act->id}}</td>
                    <td></td>
                    <td>{{setDateTimeFormatRu($reserve_act->time_create)}}</td>
                    <td>{{$reserve_act->type ? $reserve_act->type->title : ""}}</td>
                    <td>{{($reserve_act->user_from)?$reserve_act->user_from->name:''}} </td>
                    <td>{{($reserve_act->user_to)?$reserve_act->user_to->name:''}} </td>
                    <td>{{($reserve_act->bso_manager)?$reserve_act->bso_manager->name:''}} </td>
                    <td>{{\App\Models\BSO\BsoCarts::STATE_CAR[$reserve_act->cart_state_id]}}</td>
                    <td>{{$reserve_act->point_sale ? $reserve_act->point_sale->title : ""}}</td>
                </tr>

            @endforeach
            </tbody>
        </table>
    @else
        <br>
        <p>{{ trans('form.empty') }}</p>
    @endif

</div>