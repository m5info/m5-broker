@if($im->is('operator_bso'))
    @if(isset($agent_bsos_overdue) && $agent_bsos_overdue)
        У пользователя имеются просроченные БСО! - <a href="/bso/inventory_agents/details/?agent_id={{$agent->id}}&point_sale_id={{request()->get('point_sale_id') ?? '-1'}}&product_id={{request()->get('product_id') ?? '-1'}}&insurance_companies_id={{request()->get('insurance_companies_id') ?? '-1'}}&nop_id={{request()->get('nop_id') ?? '-1'}}&types=bso_in&overdue=90"
               data-type="stock" data-type_bso="4" target="_blank">
                {{ $agent_bsos_overdue }} шт.
            </a>
    @else
        <table class="" style="width: 100%;">
            <tr>
                <td style="width: 45%; vertical-align: top;">
                    <table class="bso_header">
                        <tr>
                            <td colspan="2">
                                <input type="checkbox" class="cb_left_column_style" />
                                <label style="margin-bottom: 10px;">Указывать диапазон БСО</label>
                            </td>
                        </tr><tr id="sk_selector">
                            <td style="width: 160px;">Страховая компания</td>
                            <td>
                                @php
                                    $bsoSuppliers = \App\Models\Directories\BsoSuppliers::where('is_actual', 1)->orderBy('title')->get()->pluck('title', 'id')->prepend('Выберите значение', 0);
                                    $disabled = '';
                                @endphp
                                <select class="form-control select2-all sk_user_id select2-offscreen" id="sk_user_id" name="sk_user_id" tabindex="-1" title="">
                                    @foreach($bsoSuppliers as $key => $title)
                                        @php
                                            $bsoSupplier = \App\Models\Directories\BsoSuppliers::find($key);
                                        @endphp
                                        @if(isset($bsoSupplier) && Auth::user()->role_id === 7 && Auth::user()->hasPermission('role_owns', 'is_agent'))
                                            @php
                                                $disabled = ($bsoSupplier->purpose_org->is_agent_contract == 1) ? 'disabled' : '';
                                            @endphp
                                        @endif
                                        <option value="{{ $key }}" {{ $disabled }}>{{ $title }}</option>
                                    @endforeach
                                </select>
                                <br/>
                                @if($bso_cart->bso_cart_type == 2 || $bso_cart->bso_cart_type == 3)
                                    <input type="button" value="Вывести все БСО" id="show_all_bsos"/>
                                @endif
                                <input type="button" style="cursor: pointer;display: none;" value="Добавить" class="button_sk_selector"/>
                            </td>
                        </tr>
                        <tr class="tr_type_selector">
                            <td style="width: 160px;">Тип</td>
                            <td><select id="bso_type_id"></select></td>
                        </tr>
                        <tr class="tr_type_selector">
                            <td style="width: 160px;">Статус</td>
                            <td>
                                {{ Form::select('bso_state_id', \App\Models\BSO\BsoState::all()->pluck('title','id'), 0, ['id'=> 'bso_state_id']) }}
                            </td>
                        </tr>
                    </table>
                    <div id="bsos">
                    </div>
                    <div id="rit_bsos" style="display: none;">
                    </div>
                    <input type="button" value="Поместить в корзину" style="cursor: pointer; display: none; margin-top: 10px;" class="save_transmit_bso btn btn-success btn-left"/></td>

                <td style="vertical-align: top;">


                    <a href="/bso/transfer/reserve_export?bso_cart_id={{$bso_cart->id}}" class="btn btn-primary btn-left doc_export_btn">Распечатать резервный акт</a>
                    <input type="button" class="btn  btn-success btn-left" id="b_transfer_bso" value="Передать БСО" />
                    <br/><br/><br/>


                    {{--

                    <a href="/bso/xls/act/bso_carts_group_reserve_act/?bso_cart_id={{$bso_cart->id}}" class="btn  btn-primary btn-left" target="_blank" id="print_reverve_act">Сгруппированный акт</a>

                    --}}



                    <label>
                        <input type="checkbox" class="cb_right_column_style" />
                        Группировка по типам
                    </label>

                    <div id="bso_cart">
                    </div>
                </td>
            </tr>
        </table>
    @endif
@else
    <div id="bso_cart">
    </div>
@endif