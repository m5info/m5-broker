@if($im->is('operator_bso'))

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div id="bso_cart">
        </div>
    </div>

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >



        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
            <div class="block-view">
                <div class="block-sub">
                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sk_selector">
                            <label class="control-label">Страховая компания</label>
                            {{ Form::select('sk_user_id', \App\Models\Directories\BsoSuppliers::where('is_actual', 1)->orderBy('title')->get()->pluck('title', 'id')->prepend('Выберите значение', 0), 0, ['id'=> 'sk_user_id', 'class'=>'form-control select2-all ']) }}
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tr_type_selector">
                            <label class="control-label">Тип</label>
                            {{ Form::select('bso_type_id', collect([]), 0, ['id'=> 'sk_user_id', 'class'=>'form-control ', 'id'=>'bso_type_id']) }}
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tr_type_selector">
                            <label class="control-label">Статус</label>
                            {{ Form::select('bso_state_id', \App\Models\BSO\BsoState::all()->pluck('title','id'), 0, ['class'=>'form-control ','id'=> 'bso_state_id']) }}
                        </div>


                        <input type="button" id="move_to_cart" class="btn btn-success pull-right" value="Добавить в корзину">

                    </div>
                </div>
            </div>

            <div id="bsos">
            </div>

        </div>

    </div>



</table>
@else
    <div id="bso_cart">
    </div>
@endif