<div style="background-color: #F3F3F3; padding: 15px; margin-bottom: 10px; margin-top: 10px;" data-id="{{$bso_supplier_id}}" class="supplier_block">
    <b>{{$bso_supplier_name}}</b>
    <span class="btn remove_sk_button btn-right" style="color: red;font-size: 18px; margin-right: 13px;"><i class="fa fa-close"></i></span>
    <table class="table table-bordered bso_items_table sk_user_id_{{$bso_supplier_id}}" id="table_sk_user_id_{{$bso_supplier_id}}" sk_user_id="{{$bso_supplier_id}}">
        <tr>
            <th>Тип</th>
            <th>Серия</th>
            <th>Кол-во</th>
            <th class="bso_number_td">№ полиса / квит. / сер.карт с</th>
            <th class="bso_number_td">№ по</th>
            <th>Удалить</th>
        </tr>


        <tr class="table_row row_1" data-index="1" completed="0" data-bso-supplier-id="{{$bso_supplier_id}}">
            <td>
                <input type="hidden" class="sk_user_id" value="{{$bso_supplier_id}}"/>
                {{ Form::select('type_selector', $bso_type->prepend('Выберите значение', 0), 0, ['class' => 'form-control type_selector']) }}
                <div class="error_div"></div>
            </td>
            <td>
                <select class="series_selector form-control " name="series_selector"></select>
            </td>
            <td>
                <input type="text" class="bso_qty intmask" onchange="selectBsoQty(this)" />
                <div class="error_div"></div>
            </td>
            <td>
                <input type="text" class="bso_number" name="bso_number" />
                <div class="error_div"></div>
            </td>
            <td><span class="bso_number_to">&nbsp;</span></td>

            <td style="text-align: center;">
                <span class="remove_string_button" style="color: red;font-size: 18px;"><i class="fa fa-close"></i></span>
            </td>
        </tr>

        <tr class="table_row row_2" data-index="2" completed="0" data-bso-supplier-id="{{$bso_supplier_id}}">
            <td>
                <input type="hidden" class="sk_user_id" value="{{$bso_supplier_id}}"/>
                {{ Form::select('type_selector', $bso_type->prepend('Выберите значение', 0), 0, ['class' => 'form-control type_selector']) }}
                <div class="error_div"></div>
            </td>
            <td>
                <select class="series_selector form-control " name="series_selector"></select>
            </td>
            <td>
                <input type="text" class="bso_qty intmask"  onchange="selectBsoQty(this)"/>
                <div class="error_div"></div>
            </td>
            <td>
                <input type="text" name="bso_number" class="bso_number" />
                <div class="error_div"></div>
            </td>
            <td><span class="bso_number_to">&nbsp;</span></td>

            <td style="text-align: center;">
                <span class="remove_string_button" style="color: red;font-size: 18px;"><i class="fa fa-close"></i></span>
            </td>
        </tr>


    </table>
    <div style="display: inline-block; width: 100%;">
        <input class="add_string_button btn btn-primary btn-right" id="add_string_button" type="button" value="Добавить строку" style="cursor: pointer;" sk_user_id="{{$bso_supplier_id}}"  data-sk_user_id="{{$bso_supplier_id}}" />
    </div>
</div>

<textarea class="new_tr" sk_user_id="{{$bso_supplier_id}}">
<td>
    <input type="hidden" class="sk_user_id" value="{{$bso_supplier_id}}"/>
    {{ Form::select('type_selector', $bso_type->prepend('Выберите значение', 0), 0, ['class' => 'form-control type_selector']) }}
    <div class="error_div"></div>
</td>
<td>
    <select class="series_selector form-control " name="series_selector"></select>
</td>
<td>
    <input type="text" class="bso_qty intmask"  onchange="selectBsoQty(this)"/>
    <div class="error_div"></div>
</td>
<td>
    <input type="text" class="bso_number" name="bso_number" />
    <div class="error_div"></div>
</td>
<td><span class="bso_number_to">&nbsp;</span></td>
<td style="text-align: center;">
    <span class="remove_string_button" style="color: red;font-size: 18px;"><i class="fa fa-close"></i></span>
</td>

</textarea>


<script>
    var bso_used = [];

    var current_select = '';

    var select = '';



    $(document).on('keyup', '.bso_number', function () {

        var tr = $(this).parents('tr')[0];
        var key = $(tr).data('index');
        var val = $('#rit_bsos .row_'+key+' .bso_number').val();

        //startContractFunctions(key);

    });

    function startContractFunctions_{{$bso_supplier_id}}(key){
        activSearchClearBso_{{$bso_supplier_id}}("bso_number", key, 1);
    }

    $(document).on('click', '.add_string_button[sk_user_id="{{$bso_supplier_id}}"]', function () {

        //Берем СК
        sk_user_id = $(this).data("sk_user_id");

        key = $('.table_row[data-bso-supplier-id="'+sk_user_id+'"]').length+1;

        // Создаем элемент
        var el = $('<tr>', {
            class: 'table_row row_'+key,
            'data-index': key,
            completed: '0',
            'data-bso-supplier-id': sk_user_id
        });

        el.html($('.new_tr[sk_user_id="'+sk_user_id+'"]').val());

        // Помещаем в таблицу
        $('#table_sk_user_id_'+sk_user_id).append(el);

        startContractFunctions_{{$bso_supplier_id}}(key);

        //table_row


    });


    startContractFunctions_{{$bso_supplier_id}}(1);
    startContractFunctions_{{$bso_supplier_id}}(2);





    function selectClearBso_{{$bso_supplier_id}}(object, key, suggestion) {

        var data = suggestion.data;
        var object = $('#rit_bsos .sk_user_id_{{$bso_supplier_id}} .row_'+key);

        object.find("[name*='type_selector']").val(data.type_bso_id);

        var options = "<option value='0'>Не выбрано</option>";
        data.select_bso_serie.map(function (item) {
            options += "<option value='" + item.id + "'>" + item.bso_serie + "</option>";
        });
        object.find("[name*='series_selector']").html(options);
        object.find("[name*='series_selector']").val(data.bso_serie_id);
        object.find("[name*='bso_number']").val(data.bso_number);

    }



    //ПОИСК ЧИСТЫХ БСО
    function activSearchClearBso_{{$bso_supplier_id}}(object_id, key, type) {

        var object = $('#rit_bsos .sk_user_id_{{$bso_supplier_id}} .row_'+key);
        object_bso = $('#rit_bsos .sk_user_id_{{$bso_supplier_id}} .row_'+key+' .'+object_id);

        object_bso.suggestions({
            serviceUrl: "/bso/actions/get_clear_bso/",
            type: "PARTY",
            params: {
                type_selector: $('#rit_bsos .sk_user_id_{{$bso_supplier_id}} .row_'+key).find("[name*='type_selector']").val(),
                series_selector: $('#rit_bsos .sk_user_id_{{$bso_supplier_id}} .row_'+key).find("[name*='series_selector']").val(),
                bso_number: $('#rit_bsos .sk_user_id_{{$bso_supplier_id}} .row_'+key).find("[name*='bso_number']").val(),
                bso_supplier_id: '{{$bso_supplier_id}}',
                bso_agent_id: -1,
                bso_cart_type: $('#bso_cart_type').val()
            },
            count: 10,
            minChars: 3,
            formatResult: function (e, t, n, i) {
                var s = this;

                var title = n.value;
                var bso_type = n.data.bso_type;
                var bso_sk = n.data.bso_sk;
                var agent_name = n.data.agent_name;

                var view_res = title;
                view_res += '<div class="' + s.classes.subtext + '">' + bso_type + "</div>";
                view_res += '<div class="' + s.classes.subtext + '">' + agent_name + "</div>";

                return view_res;
            },
            onSelect: function (suggestion) {

                selectClearBso_{{$bso_supplier_id}}(object, key, suggestion);
                return true;
            }
        });
    }



</script>