@extends('layouts.app')



@section('content')


    <div class="page-heading col-lg-12">
        <div class="" style="display: inline-block">
            <h1 style="display: inline-block">Испортить БСО</h1>
        </div>
    </div>

        <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-9 col-lg-10">
            Фильтры
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="filter-group" id="filters">
                        <div class="col-lg-12">
                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="like_title">Агент</label>
                                {{ Form::select('agent_id',\App\Models\User::getALLUserWhere()->get()->pluck('name', 'id')->prepend('Агент',-1), \Request::query('agent_id'), ['class' => 'form-control select2', 'id'=>'agent_id']) }}
                            </div>
                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="like_title">Наименование БСО</label>
                                {{ Form::text('bso_title', '', ['class'=>'form-control']) }}
                            </div>
                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3" style="margin-top: 22px;">
                                <span class="btn btn-primary" id="add_bso" onclick="checkValue()">Добавить</span>
                            </div>
{{--                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">--}}
{{--                                <label class="control-label" for="accept_status">Статус</label>--}}
{{--                                {{ Form::select('accept_status', collect(\App\Models\Orders\InspectionOrdersReports::STATE), -1, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

<div id="result_table" class="col-lg-12 no-padding" hidden >
    <div class="col-lg-12">
        <span class="btn btn-primary btn-right" id="execute_bso">Выполнить</span>
    </div>

    @include('_chunks._vue_table')


    <div class="col-lg-12">
        <span class="btn btn-primary btn-right" id="execute_bso">Выполнить</span>
    </div>

    {{--@include('_chunks._pagination', ['callback'=>'loadItems'])--}}
</div>
@endsection

@section('js')

    <script type="text/javascript">
        $(document).on("click", "#execute_bso", function () {
            var data = $('[name="bso_for_ruin[]"]').serialize();

            $.ajax({
                url: '/bso/ruin_bso/ruin',
                type: 'POST',
                data: data,
                success: function (response) {

                    window.parent.flashHeaderMessage('Выбранные БСО успешно испорчены!', 'success');
                    window.parent.jQuery.fancybox.close();

                    exportBsos(response);
                }
            });
        });

        function exportBsos(idsArray){
            Object.keys(idsArray).map(function(key, index) {

                var href = "{{url('bso/ruin_bso/export')}}/"+key;

                var arrStr = encodeURIComponent(idsArray[key]);
                href += '?bsos=' + arrStr;

                window.open(href, '_blank');

                // var link = document.createElement('a');
                //
                // link.setAttribute('href', href);
                // link.setAttribute('download', 'download');
                //
                // link.click();
            });

            reload();
        }

        $(document).on("click", "[name='select_all_bso']", function () {
                if ($(this).prop("checked")) {
                    $(".bso_for_ruin").prop("checked", true);
                }
                else {
                    $(".bso_for_ruin").prop("checked", false);
                }
            }
        );

        function getData() {

            return {
                agent_id: $('[name="agent_id"]').val(),
                bso_title: $('[name="bso_title"]').val(),
                like_title: $('[name="like_title"]').val(),
                found_bsos: $('[name="bso_for_ruin[]"]').serialize(),
                /*page: PAGE,*/
            }
        }

        function checkValue() {
            if($('[name="bso_title"]').val().length<1){
                showError('Введите БСО для поиска');
            } else {
                loadItems();
            }
        }

        function showError(mes) {
            $.ajax({
                url: '/bso/ruin_bso/show_error',
                type: 'POST',
                data: mes,
                success: function (response) {
                    $('body').append(response)
                }
            });
        }

        function loadItems() {

            loaderShow();

            $.get('{{url("/bso/ruin_bso/get_table")}}', getData(), function (table_res) {

                if(table_res.count_result < 1)
                {
                    showError('БСО не найден или его нельзя добавить! Испортить можно только чистый БСО, переданный агенту');
                }
                $('#table').html(table_res.html);

                if (table_res.count < 1) {
                    table_res.count = 1;
                    $('#result_table').attr("hidden","hidden");
                } else {
                    $('#result_table').removeAttr("hidden");
                }

               /* let maxpage = Math.ceil(table_res.count / table_res.perpage); //для пагинации

                $('#view_row').html(PAGE);
                $('#max_row').html(maxpage);*/

                /*ajaxPaginationUpdate(maxpage, loadItems);*/

                $(".clickable-row").click(function () {
                    if ($(this).attr('data-href')) {
                        window.location = $(this).attr('data-href');
                    }
                });

            }).always(function () {
                loaderHide();
            });

        }

        function process(operations = []){
            $.each(operations, function(k,operation){
                if(isFunction(operation)){
                    window[operation]()
                }
            })
        }
    </script>
@endsection
