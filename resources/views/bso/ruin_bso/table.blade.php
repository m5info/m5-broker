<table class="tov-table table table-bordered">
    <tr>
        <th>{{ Form::checkbox('select_all_bso', 1, true) }}</th>
        <th>СК</th>
        <th>Тип</th>
        <th>Событие</th>
        <th>Статус</th>

        <th>БСО</th>
        <th>Страхователь</th>

        <th>Агент</th>
        <th>Менеджер</th>
        <th>Руководитель</th>

        <th>Точка продаж</th>
        <th>Номер бланка</th>
        <th>Принят в организацию</th>
        <th>Дата последней операции</th>


        <th></th>
    </tr>

    <tbody>

    @foreach($bsos as $bso)
            <tr>
                <td>{{ Form::checkbox('bso_for_ruin[]', $bso->id, true, ['class' => 'bso_for_ruin']) }}</td>
                <td>{{(!empty($bso->supplier)) ? $bso->supplier->title : ''}}</td>
                <td>{{(!empty($bso->type->title)) ? $bso->type->title : ''}}</td>

                <td>{{$bso->location ? $bso->location->title : ""}}</td>
                <td>
                    @if($bso->is_reserved == 1)
                        Резерв
                    @else
                        {{$bso->state ? $bso->state->title : ""}}
                    @endif
                </td>

                <td>{{$bso->bso_title}}</td>
                <td>
                    @if(isset($bso->contract) && isset($bso->contract->id))
                        {{$bso->contract->getInsurer()}}
                    @endif
                </td>
                <td>
                    {{($bso->agent)?$bso->agent->name:''}}
                </td>

                <td>
                    @if(isset($bso->contract) && isset($bso->contract->manager))
                        {{($bso->contract->manager)?$bso->contract->manager->name:''}}
                    @endif

                </td>

                <td>
                    @if(isset($bso->contract) && $bso->contract->statys_id > 0 && $bso->contract->parent())
                        {{($bso->contract->parent()->name)}}
                    @endif
                </td>

                <td>{{$bso->point_sale ? $bso->point_sale->title : ""}}</td>
                <td>{{$bso->bso_blank_title ? $bso->bso_blank_title : ""}}</td>

                <td>{{setDateTimeFormatRu($bso->logs()->get()->first() ? $bso->logs()->get()->first()->log_time : '')}}</td>
                <td>{{setDateTimeFormatRu($bso->logs()->get()->last() ? $bso->logs()->get()->last()->log_time : '')}}</td>

                <td>
                    @if(auth()->user()->hasPermission('bso', 'items'))
                        <a href="/bso/items/{{$bso->id}}/" class="btn btn-success" target="_blank">Открыть</a>
                    @elseif(auth()->user()->is('underfiller'))
                        @if(isset($bso->contract->id))
                            <a href="/contracts/underfillers/edit/{{$bso->contract->id}}/"
                               class="btn btn-success" target="_blank">Открыть</a>
                        @endif
                    @endif
                </td>


            </tr>
    @endforeach
    </tbody>
</table>