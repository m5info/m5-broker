<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <span class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="color: red;">Выберите СК и загрузите файл или вставьте список в текстовое поле</span>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        {!! Form::open(['url'=>"/bso/bso_statuses_check/check", 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        <div class="col-lg-6">
            <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <label class="control-label" for="sk">Страховая компания</label>
                {{ Form::select('sk', \App\Models\Directories\InsuranceCompanies::all()->pluck('title','id'), 0, ['class' => 'form-control select2-ws']) }}
            </div>
            <div class="btn-group col-xs-12 col-sm-12 search_type-md-12 col-lg-12">
                <label class="control-label" for="user_id_from">Способ поиска</label>
                {{ Form::select('search_type', [1 => 'Частичное совпадение', 2 => 'Полное совпадение'], old('search_type'), ['class' => 'form-control select2-ws']) }}
            </div>
            <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <label class="control-label" for="bso_type">Вид БСО</label>
                {{ Form::select('bso_type', [1 => 'Все', 2 => 'БСО', 3 => 'Квитанция'], old('bso_type'), ['class' => 'form-control select2-ws']) }}
            </div>
            <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px;">
                <label for="check_method">Способ сверки (Текстовое поле/Загрузка файла): </label>
                {{ Form::checkbox('check_method', 1, 0) }}
            </div>
            <div class="textarea_method">
                <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px;">
                    @php
$placeholder = 'ККК 100008
MMM 10005
MMM 10077712
MMM 10077713';
                    @endphp
                    <label style="display: block" for="check_method">Поле для списка БСО</label>
                    {{ Form::textarea('bso_data', old('bso_data'), ['rows' => 20,  'class' =>'form-control', 'style' => 'width:100%', 'id' => 'text_description', 'placeholder' => $placeholder]) }}
                </div>
            </div>
            <div class="file_method" style="display: none">
                <div  style="margin-top: 20px;" class="btn-group col-xs-12 col-sm-12 col-md-6 col-lg-8">
                    {{ Form::file('file',['class' => '']) }}
                </div>
            </div>

        </div>
        <div class="col-lg-6">
            <div class="btn-group col-xs-12 col-sm-12 col-md-6 col-lg-2">
                {{ Form::submit('Сверка', ['class' => 'btn btn-success', 'style' => 'float:left']) }}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
{{--    @if($documents && $documents->count())--}}
{{--        <div class="col-lg-12" style="margin: 25px;">--}}
{{--            @foreach($documents as $file)--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-4 col-lg-2">--}}
{{--                        {!! $file->original_name !!}--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4 col-lg-2">--}}
{{--                        <a href="{{ url("files/$file->name") }}" class="btn btn-primary" target="_blank">--}}
{{--                            {!! trans('form.buttons.download') !!}--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4 col-lg-2">--}}
{{--                        <button class="btn btn-danger" type="button" onclick="removeFile('{!! $file->name !!}')">--}}
{{--                            {{ trans('form.buttons.delete') }}--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            @endforeach--}}
{{--        </div>--}}
{{--    @else--}}
{{--        <h3>{{ trans('form.empty') }}</h3>--}}
{{--    @endif--}}
</div>

@section('js')
    <script>

        @if(!empty(session('checked_file')))
            $(function () {

            var link = document.createElement('a');

            var href = '{{ url("files/".session('checked_file'))}}';

            link.setAttribute('href',href);
            link.setAttribute('download','download');

            link.click();
            });
        @endif

        $('[name="check_method"]').on('change', function () {
            if ($(this).prop('checked') == true){
                $('.file_method').show();
                $('.textarea_method').hide();
            } else{
                $('.file_method').hide();
                $('.textarea_method').show();
            }
        });
    </script>
@endsection