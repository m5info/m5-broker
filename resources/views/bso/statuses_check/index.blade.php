@extends('layouts.app')



@section('content')


    <div class="page-heading col-lg-12">
        <div class="" style="display: inline-block">
            <h1 style="display: inline-block">Сверка статусов БСО</h1>
            <a href="{{ url("examples/bso_check_statuses.xlsx") }}" style="display: inline-block; margin: 0 0 20px 30px;" class="btn btn-info">Пример</a>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h2>Документы</h2>
        </div>
        <div class="block-main">
            <div class="block-sub">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <div class="row form-horizontal">
                        @include('bso.statuses_check.documents', ['documents' => $documents])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

    <script type="text/javascript">
        function process(operations = []){
            $.each(operations, function(k,operation){
                if(isFunction(operation)){
                    window[operation]()
                }
            })
        }
    </script>
@endsection
