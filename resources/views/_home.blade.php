@extends('layouts.app')

@section('head')

@append

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="block-main-title">
                        Заявки
                    </div>
                    <div class="block-main">
                        <table class=" noScrollTable">
                            <thead>
                            <tr>
                                <th>Статус</th>
                                <th>Заявка</th>
                                <th>Клиент</th>
                                <th>Груз</th>
                                <th>Маршрут (км)</th>
                                <th>Объем (м3)</th>
                                <th>₽ за км</th>
                                <th>Доставки (₽)</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr class='clickable-row' data-href='{{url ("/manager/orders/{$order->id}/edit")}}'>
                                    <td><span class="circle {{ getStatusColor($order->state_id) }}">{{$order->state_title}}</span></td>
                                    <td>#{{$order->id}}</td>
                                    <td>{{ isset($order->client_sender)?$order->client_sender->name:'' }}</td>
                                    <td>{{ $order->types_trailers_title }}</td>
                                    <td>{{((isset($order->price))?titleFloatFormat($order->price->planned_km):0)}}</td>
                                    <td>{{ titleFloatFormat($order->load_volume) }}</td>
                                    <td>{{ titleFloatFormat($order->kv_km) }}</td>
                                    <td>{{ titleFloatFormat($order->total_price) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="block-main-title">
                        Транспорт
                    </div>
                    <div class="block-main">
                        <table class="table defaultTable">
                            <thead>
                            <tr>
                                <th>Статус</th>
                                <th>Машина</th>
                                <th>Тип</th>
                                <th>Номер</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cars as $car)
                                <tr class='clickable-row' data-href='{{ url("/vehicles/cars/$car->id/edit") }}'>
                                    <td>
                                        @if($car->order()->exists())
                                            <span class="circle {{ $car->order->state_color }}">{{ $car->order->state_title }}</span>
                                        @else
                                            <span class="circle green">Свободен</span>
                                        @endif
                                    </td>
                                    <td>{{ $car->title }}</td>
                                    <th>{{ $car->type_title }}</th>
                                    <td>{{ $car->registration_number }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                    <div class="block-main-title">
                        Водители
                    </div>
                    <div class="block-main">
                        <table class="table defaultTable">
                            <thead>
                            <tr>
                                <th>Статус</th>
                                <th>Водитель</th>
                                <th>В/У</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($drivers as $driver)
                                <tr class='clickable-row' data-href='{{ url("/users/users/$driver->id/edit") }}'>
                                    <td>
                                        @if($driver->cars()->exists() && $driver->cars->first() && $driver->cars->first()->order()->exists())
                                            <span class="circle {{ $driver->cars()->first()->order->state_color }}">{{ $driver->cars->first()->order->state_title }}</span>
                                        @else
                                            <span class="circle green">Свободен</span>
                                        @endif
                                    </td>
                                    <td>{{ $driver->name }}</td>
                                    <td>{{ $driver->driver_license_title }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <div class="block-main-title">
            </div>
            <div class="block-main">
                <div class="statistic">
                    <div class="block-sub">
                        <span class="title-large">Доход</span>
                        <span class="digit-large rur">9 780 960</span>
                    </div>
                    <div class="divider"></div>
                    <div class="block-sub">
                        <span class="title-large">Расход</span>
                        <span class="digit-large rur">6 340 589</span>
                    </div>
                    <div class="divider"></div>
                    <div class="block-sub">
                        <canvas id="myChart" width="200" height="200" class="graph-doughnut center-block"></canvas>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="graph">
                                    <span class="title"><span class="circle green"></span>Топливо</span>
                                    <span class="value rur">1 568 700</span></div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="graph">
                                    <span class="title"><span class="circle yellow"></span>Водители</span>
                                    <span class="value rur">659 300</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="graph">
                                    <span class="title"><span class="circle red"></span>Страховка</span>
                                    <span class="value rur">250 700</span>
                                    <a href="javascript:void(0);" class="link">Как сэкономить?</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="graph">
                                    <span class="title"><span class="circle blue"></span>Прочее</span>
                                    <span class="value rur">93 400</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="block-sub">
                        <span class="title-large">Прибыль</span>
                        <span class="digit-extra-large rur">3 780 960</span>
                    </div>
                    <div class="zigzag"></div>
                </div>
            </div>
        </div>
    </div>

    @if (session('login-success') && !count($errors))
        @include('partials.success-login')
    @endif

@endsection


