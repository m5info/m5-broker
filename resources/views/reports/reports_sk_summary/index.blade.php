@extends('layouts.app')

@section('content')


    <div class="page-heading">
        <h2>Финансовые потоки</h2>
    </div>
    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="filter-group" id="filters">

                    @php($select_params = ['class'=>'form-control select2-all select2-ws','onchange'=>'loadItems()'])

                    <div class="btn-group col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        <label class="control-label" for="user_id_from">Месяц</label>
                        {{ Form::select('report_month', getRuMonthes(), request()->has('report_month') ? request()->get('report_month') : (int)date("m"), $select_params) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-1 col-lg-1">
                        <label class="control-label" for="user_id_from">Год</label>
                        {{ Form::select('report_year', $report_year->pluck('report_year', 'report_year'), request()->has('report_year') ? request()->get('report_year') : date("Y"), $select_params) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label" for="user_id_from">СК</label>
                        {{ Form::select('insurance_id', $insurances_select->pluck('title', 'id')->prepend('Не выбрано', -1), request()->has('insurance_id') ? request()->get('insurance_id') : -1, $select_params) }}
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
        <div class="row">
            <div id="table"></div>
        </div>
    </div>





@endsection

@section('js')

    <script>


        $(function () {
            loadItems();
            $('.select2-ws').select2("destroy").select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true,
                minimumResultsForSearch: -1
            });
        });

        function getData(){
            return {
                insurance_id:$('[name="insurance_id"]').val(),
                report_month:$('[name="report_month"]').val(),
                report_year:$('[name="report_year"]').val(),
            }
        }

        function loadItems(){
            loaderShow();

            $.post('/reports/reports_sk_summary/get_table', getData(), function(table_res){

                loaderHide();

                $('#table').html(table_res.html);

                $(".clickable-row-blank").click(function () {
                    if ($(this).attr('data-href')) {
                        window.open($(this).attr('data-href'), '_blank');
                    }
                });


            }).always(function(){
                loaderHide();
            });

        }

    </script>

@endsection