@php
    $colspan = 15;
@endphp

<table class="table table-striped table-bordered">
    <thead>

        <tr>
            <th>ID</th>
            <th>Название</th>

            <th>Создан</th>
            <th>Акцептован</th>

            <th>Статус</th>
            <th>Месяц</th>
            <th>Год</th>
            <th>Тип</th>

            <th>СП</th>

            <th>Бордеро</th>
            <th>ДВОУ</th>
            <th>Общая</th>


            <th>КВ Агента</th>
            <th>КВ Рекомендателя</th>
            <th>Маржа</th>

        </tr>

    </thead>
    <tbody>

        @if(sizeof($insurances))

            @php
                $all_insur_payment_total = 0;
                $all_insur_bordereau_total = 0;
                $all_insur_dvoy_total = 0;
                $all_insur_amount_total = 0;
                $all_insur_kv_agent_total = 0;
                $all_insur_kv_recommender_total = 0;
                $all_insur_summary_total = 0;
            @endphp

            @foreach($insurances as $insurances)



                <tr class="clickable-row">
                    <td colspan="{{$colspan}}">
                        <b>{{ $insurances->title }}</b>
                    </td>
                </tr>

                @php
                    $group_id = 0;
                    $check_group_id = 0;
                    $all_payment_total = 0;
                    $all_bordereau_total = 0;
                    $all_dvoy_total = 0;
                    $all_amount_total = 0;
                    $all_kv_agent_total = 0;
                    $all_kv_recommender_total = 0;
                    $all_summary_total = 0;
                @endphp

                @foreach($insurances->getReportsSummary(request('report_year'), request('report_month')) as $report)

                    @php
                        $check_group_id = $report->bso_supplier->id;
                    @endphp

                    @if($group_id != $check_group_id)

                        <tr>
                            <td colspan="{{$colspan}}">
                                {{$report->bso_supplier->title}}
                            </td>
                        </tr>

                        @php
                            $group_id = $check_group_id;
                        @endphp
                    @endif

                    <tr class="clickable-row-blank" data-href="/reports/order/{{$report->id}}/">
                        <td>{{$report->id}}</td>
                        <td>{{$report->title}}</td>

                        <td>{{setDateTimeFormatRu($report->created_at, 1)}} {{$report->create_user->name}}</td>
                        <td>{{setDateTimeFormatRu($report->accepted_at, 1)}} {{($report->accept_user)?$report->accept_user->name:''}}</td>


                        <td>{{\App\Models\Reports\ReportOrders::STATE[$report->accept_status]}}</td>
                        <td>{{getRuMonthes()[$report->report_month]}}</td>
                        <td>{{$report->report_year}}</td>
                        <td>{{\App\Models\Reports\ReportOrders::TYPE[$report->type_id]}}</td>

                        <td class="nowrap">{{titleFloatFormat($report->payment_total)}}</td>
                        <td class="nowrap">{{titleFloatFormat($report->bordereau_total)}}</td>
                        <td class="nowrap">{{titleFloatFormat($report->dvoy_total)}}</td>
                        <td class="nowrap">{{titleFloatFormat($report->amount_total)}}</td>
                        <td class="nowrap">{{titleFloatFormat($report->kv_agent_total)}}</td>
                        <td class="nowrap">{{titleFloatFormat($report->kv_recommender_total)}}</td>
                        <td class="nowrap">{{titleFloatFormat($report->marjing_total)}}</td>
                    </tr>

                    @php
                        $all_payment_total += $report->payment_total;
                        $all_bordereau_total += $report->bordereau_total;
                        $all_dvoy_total += $report->dvoy_total;
                        $all_amount_total += $report->amount_total;
                        $all_kv_agent_total += $report->kv_agent_total;
                        $all_kv_recommender_total += $report->kv_recommender_total;
                        $all_summary_total += $report->marjing_total;
                    @endphp


                @endforeach


                @php
                    $all_insur_payment_total += $all_payment_total;
                    $all_insur_bordereau_total += $all_bordereau_total;
                    $all_insur_dvoy_total += $all_dvoy_total;
                    $all_insur_amount_total += $all_amount_total;
                    $all_insur_kv_agent_total += $all_kv_agent_total;
                    $all_insur_kv_recommender_total += $all_kv_recommender_total;
                    $all_insur_summary_total += $all_summary_total;
                @endphp

                <tr class="clickable-row">
                    <td colspan="{{$colspan-7}}"></td>
                    <td class="nowrap"><b>{{ titleFloatFormat($all_payment_total) }}</b></td>
                    <td class="nowrap"><b>{{ titleFloatFormat($all_bordereau_total) }}</b></td>
                    <td class="nowrap"><b>{{ titleFloatFormat($all_dvoy_total) }}</b></td>
                    <td class="nowrap"><b>{{ titleFloatFormat($all_amount_total) }}</b></td>
                    <td class="nowrap"><b>{{ titleFloatFormat($all_kv_agent_total) }}</b></td>
                    <td class="nowrap"><b>{{ titleFloatFormat($all_kv_recommender_total) }}</b></td>
                    <td class="nowrap"><b>{{ titleFloatFormat($all_summary_total) }}</b></td>
                </tr>


            @endforeach

            <tr>
                <td colspan="15">&nbsp;</td>
            </tr>

            <tr class="clickable-row">
                <td colspan="{{8}}"></td>
                <td class="nowrap"><b>{{ titleFloatFormat($all_insur_payment_total) }}</b></td>
                <td class="nowrap"><b>{{ titleFloatFormat($all_insur_bordereau_total) }}</b></td>
                <td class="nowrap"><b>{{ titleFloatFormat($all_insur_dvoy_total) }}</b></td>
                <td class="nowrap"><b>{{ titleFloatFormat($all_insur_amount_total) }}</b></td>
                <td class="nowrap"><b>{{ titleFloatFormat($all_insur_kv_agent_total) }}</b></td>
                <td class="nowrap"><b>{{ titleFloatFormat($all_insur_kv_recommender_total) }}</b></td>
                <td class="nowrap"><b>{{ titleFloatFormat($all_insur_summary_total) }}</b></td>
            </tr>
        @endif

    </tbody>
</table>