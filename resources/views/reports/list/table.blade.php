<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th rowspan="2">Все {{ Form::checkbox('all_in_list', 1) }}</th>
            <th rowspan="2">ID</th>
            <th rowspan="2">Название</th>
            <th rowspan="2">Статус</th>
            <th rowspan="2">Дата создания</th>
            <th rowspan="2">Месяц</th>
            <th rowspan="2">Год</th>
            <th rowspan="2">Тип</th>
            <th rowspan="2">Поток</th>

            <th rowspan="2">Риски</th>

            <th rowspan="2">СП</th>

            <th colspan="3">Комиссия</th>
            <th colspan="2">Суммы</th>

            <th colspan="5">Фактические данные</th>


            <th rowspan="2">Рекламник</th>
            <th rowspan="2">Комментарий</th>
            <th rowspan="2">Действие</th>


        </tr>

        <tr>
            <th>Бордеро</th>
            <th>ДВОУ</th>
            <th>Общая</th>

            <th style="white-space: nowrap;">К перечислению</th>
            <th style="white-space: nowrap;">К возврату</th>

            <th>Оплачено</th>
            <th>К оплате</th>
            <th>Получено</th>
            <th>Долг СК</th>

            <th>Предварительный платеж</th>

            {{--
            <th>Приход</th>
            <th>Сумма предв. оплаты</th>
            <th>Списание</th>
            <th>Долг</th>
            --}}
        </tr>

    </thead>
    <tbody>
        @php($ins_id = 0)

        @php($payment_total_all = 0)
        @php($bordereau_total_all = 0)
        @php($dvoy_total_all = 0)
        @php($amount_total_all = 0)
        @php($to_transfer_total_all = 0)
        @php($to_return_total_all = 0)
        @php($report_payment_sums_transfer_total_all = 0)
        @php($report_payment_sums_return_total_all = 0)
        @php($full_advance_payment = 0)
        @php($full_prepayment_summ = 0)
        @php($full_returnpayment_summ = 0)




        @if(sizeof($reports))
            @foreach($reports as $report)

                @php($report_payment_sums_transfer_total = $report->report_payment_sums->where('type_id', 0)->sum('amount'))
                @php($report_payment_sums_return_total = $report->report_payment_sums->where('type_id', 1)->sum('amount'))

                @php($bordereau_total = ($report->type_id == 0)?$report->bordereau_total:0)
                @php($dvoy_total = ($report->type_id == 1)?$report->dvoy_total:0)

                @php($payment_total_all += $report->payment_total)
                @php($bordereau_total_all += $bordereau_total)
                @php($dvoy_total_all += $dvoy_total)

                @php($amount_total_all += $bordereau_total+$dvoy_total)
                @php($to_transfer_total_all += $report->to_transfer_total)
                @php($to_return_total_all += $report->to_return_total)


                @php($report_payment_sums_transfer_total_all += $report_payment_sums_transfer_total)
                @php($report_payment_sums_return_total_all += $report_payment_sums_return_total)

                @php($prepayment_summ = $report->to_return_total-$report_payment_sums_return_total)
                @php($full_prepayment_summ += $prepayment_summ)
                @php($full_returnpayment_summ += $report->to_transfer_total-$report_payment_sums_transfer_total)


                @php($td_url = "class=clickable-row data-href=/reports/order/{$report->id}")

                @php($full_advance_payment += $report->advance_payment)

                <tr>
                    <td>{{ Form::checkbox("reports_in_list[]", $report->id, in_array($report->id, $reports_in_list) ? true : false, ['class' => 'reports_in_list']) }}</td>
                    <td {{ $td_url }}>{{$report->id}}</td>
                    <td {{ $td_url }}>{{$report->title}}</td>
                    <td {{ $td_url }}>{{\App\Models\Reports\ReportOrders::STATE[$report->accept_status]}}</td>

                    <td {{$td_url}}>{{$report->updated_at ? getDateFormatRu($report->updated_at) : ''}}</td>


                    <td {{ $td_url }}>{{getRuMonthes()[$report->report_month]}}</td>
                    <td {{ $td_url }}>{{$report->report_year}}</td>
                    <td {{ $td_url }}>{{\App\Models\Reports\ReportOrders::TYPE[$report->type_id]}}</td>
                    @if(isset($report->payment_flow_type))
                        <td {{ $td_url }}>{{ ($report->payment_flow_type == 1) ? 'СК' : 'Брокер' }}</td>
                    @else
                        <td {{ $td_url }}></td>
                    @endif

                    <td {{ $td_url }}>{{$report->getProductsTitleList()}}</td>

                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report->payment_total)}}</td>

                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($bordereau_total)}}</td>
                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($dvoy_total)}}</td>
                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report->amount_total)}}</td>

                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report->to_transfer_total)}}</td>
                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report->to_return_total)}}</td>

                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report_payment_sums_transfer_total)}}</td>
                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report->to_transfer_total-$report_payment_sums_transfer_total)}}</td>
                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report_payment_sums_return_total)}}</td>
                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report->to_return_total-$report_payment_sums_return_total)}}</td>
                    <td style="white-space: nowrap;" {{ $td_url }}>{{ titleFloatFormat($report->advance_payment) }}</td>





                    <td>
                        <a href="{{ url("reports/promotion/{$report->bso_supplier_id}/{$report->advertising_id}/reports") }}" target="_blank">{{ $report->advertising_id ? $report->advertising_id : '' }}</a>
                    </td>
                    <td {{ $td_url }}>{{$report->comments}}</td>
                    <td>
                        @if(!$report->is_recorded_to_foundations)
                            <span class="btn btn-danger btn-right" onclick="deleteReport(this, {{$report->id}})">Удалить</span>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr class="clickable-row">
                <td colspan="100" class="text-center">Нет результатов</td>
            </tr>
        @endif
    </tbody>
    <tfoot>

        <tr>

            <th colspan="11"></th>

            <th colspan="3" style="white-space: nowrap;">{{titleFloatFormat($amount_total_all)}}</th>
            <th style="white-space: nowrap;">{{titleFloatFormat($to_transfer_total_all)}}</th>
            <th style="white-space: nowrap;">{{titleFloatFormat($to_return_total_all)}}</th>



            <th style="white-space: nowrap;">{{titleFloatFormat($report_payment_sums_transfer_total_all)}}</th>
            <th style="white-space: nowrap;">{{titleFloatFormat($full_returnpayment_summ)}}</th>


            <th style="white-space: nowrap;">{{titleFloatFormat($report_payment_sums_return_total_all)}}</th>
            <th style="white-space: nowrap;">{{titleFloatFormat($full_prepayment_summ)}}</th>


            <th style="white-space: nowrap;">{{titleFloatFormat($full_advance_payment)}}</th>
            <th colspan="3"></th>
        </tr>

    </tfoot>
</table>