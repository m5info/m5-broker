@php($select_params = ['class'=>'form-control select2-all select2-ws','onchange'=>'getFilters(false)'])

<div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <label class="control-label" for="user_id_from">СК</label>
    {{ Form::select('insurance_id', $insurances->pluck('title', 'id')->prepend('Не выбрано', -1), session('report_sk.insurance_id', -1), $select_params) }}
</div>

<div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <label class="control-label" for="user_id_from">Организация</label>
    {{ Form::select('org_id', $organizations->pluck('title', 'id')->prepend('Не выбрано', -1), session('report_sk.org_id', -1), $select_params) }}
</div>

<div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <label class="control-label" for="user_id_from">Поставщик</label>
    {{ Form::select('supplier_id', $suppliers->pluck('title', 'id')->prepend('Не выбрано', -1), session('report_sk.supplier_id', -1), $select_params) }}
</div>

<div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <label class="control-label" for="user_id_from">Вид группировки</label>
    {{ Form::select('group_type', collect(['sk'=>'По СК', 'org'=>'По организациям']), session('report_sk.group_type', 'sk'), $select_params) }}
</div>

<div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <span onclick="loadItems()" class="btn btn-primary inline-block">Применить</span>
</div>