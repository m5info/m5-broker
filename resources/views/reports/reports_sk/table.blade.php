<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th rowspan="2">Поставщик</th>
            <th rowspan="2">Оборот</th>
            <th colspan="2">Бордеро</th>
            <th colspan="2">ДВОУ</th>
            <th rowspan="2">К перечислению</th>
            <th rowspan="2">Долг СК</th>
        </tr>

        <tr>
            <th>Кол-во</th>
            <th>Сумма</th>

            <th>Кол-во</th>
            <th>Сумма</th>

        </tr>

    </thead>
    <tbody>
        @php($group_id = 0)

        @if(sizeof($suppliers))
            @foreach($suppliers as $supplier)


                @php($supplier_payments = $supplier->getDebtBrokerToSk())


                @if(session('report_sk.group_type', 'sk') == 'sk')
                    @php($check_group_id = $supplier->insurance->id)
                    @php($group_title = $supplier->insurance->title)
                @else
                    @php($check_group_id = $supplier->purpose_org->id)
                    @php($group_title = $supplier->purpose_org->title)
                @endif



                @if($group_id != $check_group_id)
                    @php($insurance_payments = $supplier->insurance->getDebtBrokerToSk())
                    <tr>
                        <td>
                            <b>{{ $group_title }}</b>
                        </td>
                        <td class="text-center"><b>{{titleFloatFormat($supplier->insurance->getPaymentsTotal())}}</b></td>

                        <td class="text-center"><b>{{$supplier->insurance->getPaymentsCount(0)}}</b></td>
                        <td class="text-center"><b>{{titleFloatFormat($supplier->insurance->getPaymentsTotalKV(0))}}</b></td>

                        <td class="text-center"><b>{{$supplier->insurance->getPaymentsCount(1)}}</b></td>
                        <td class="text-center"><b>{{titleFloatFormat($supplier->insurance->getPaymentsTotalKV(1))}}</b></td>

                        <td>{{ titleFloatFormat($insurance_payments['to_transfer_total']) }}</td>
                        <td>{{ titleFloatFormat($insurance_payments['to_return_total']) }}</td>

                    </tr>
                    @php($group_id = $check_group_id)
                @endif

                <tr class="clickable-row">
                    <td>
                        <a href="{{url("/reports/reports_sk/{$supplier->id}/reports/")}}" target="_blank">
                            {{ $supplier->title }}
                        </a> (всего отчетов {{$supplier->reports_actual->count()}})
                    </td>
                    <td class="text-center">{{titleFloatFormat($supplier->getPaymentsTotal())}}</td>

                    <td class="text-center"><a href="{{url("/reports/reports_sk/{$supplier->id}/bordereau/")}}">{{$supplier->getPaymentsCount(0)}}</a></td>
                    <td class="text-center"><a href="{{url("/reports/reports_sk/{$supplier->id}/bordereau/")}}">{{titleFloatFormat($supplier->getPaymentsTotalKV(0))}}</a></td>

                    <td class="text-center"><a href="{{url("/reports/reports_sk/{$supplier->id}/dvoy/")}}">{{$supplier->getPaymentsCount(1)}}</a></td>
                    <td class="text-center"><a href="{{url("/reports/reports_sk/{$supplier->id}/dvoy/")}}">{{titleFloatFormat($supplier->getPaymentsTotalKV(1))}}</a></td>



                   <td>
                        <a href="/reports/reports_sk/{{$check_group_id}}/reports?motion=1">{{ titleFloatFormat($supplier_payments['to_transfer_total']) }}</a>
                    </td>
                    <td>
                        <a href="/reports/reports_sk/{{$check_group_id}}/reports?motion=2">{{ titleFloatFormat($supplier_payments['to_return_total']) }}</a>
                    </td>


                </tr>
            @endforeach
        @else
            <tr class="clickable-row">
                <td colspan="3" class="text-center">Нет результатов</td>
            </tr>
        @endif
    </tbody>
</table>