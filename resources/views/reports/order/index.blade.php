@extends('layouts.app')

@php
    $monthes = getRuMonthes();
    $years = getYearsRange(-5, +1);

@endphp

@section('content')

    <div class="block-main" style="margin-top: 5px;">
        <div class="block-sub">
            {{ Form::open(['url' => url("/reports/order/{$report->id}/"), 'method' => 'post', 'class' => 'form-horizontal']) }}

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

                <div class="form-group">
                    <label class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        Название ID {{$report->id}}
                        (Сформирован: {{setDateTimeFormatRu($report->created_at)}} {{$report->create_user ? "пользователем {$report->create_user->name}" : ""}}
                        )
                    </label>
                    <div class="col-sm-12">
                        {{ Form::text('title', $report->title, ['class' => 'form-control', 'required']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-6 ">Отчетный период</label>
                    <label class="col-sm-3 ">Договора с</label>
                    <label class="col-sm-3 ">Договора по</label>
                    <div class="col-sm-6">
                        {{ Form::select('report_month', $monthes, $report->report_month, ['class' => 'form-control', 'style' => 'width: 48%; display: inline']) }}
                        /
                        {{ Form::select('report_year', $years, $report->report_year, ['class' => 'form-control', 'style' => 'width: 48%; display: inline']) }}
                    </div>

                    <div class="col-sm-3">
                        {{ Form::text('report_date_start', setDateTimeFormatRu($report->report_date_start, 1), ['class' => 'form-control datepicker date']) }}

                    </div>

                    <div class="col-sm-3">
                        {{ Form::text('report_date_end', setDateTimeFormatRu($report->report_date_end, 1), ['class' => 'form-control datepicker date']) }}

                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-6 ">Подписант организации</label>
                    <label class="col-sm-6 ">Подписант поставщика</label>

                    <div class="col-sm-6">
                        {{ Form::text('signatory_org', $report->signatory_org, ['class' => 'form-control']) }}
                    </div>

                    <div class="col-sm-6">
                        {{ Form::text('signatory_sk_bso_supplier', $report->signatory_sk_bso_supplier, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">

                        <a class="btn btn-success doc_export_btn pull-left"
                           href="/reports/order/{{$report->id}}/form_report">Скачать отчет</a>
                        @if($report->type == 0 && !$report->checkDvou() && $report->getPayments()->get()->count())
                            <button type="button" class="btn btn-primary pull-right" id="also_dvou">Сформировать отчет ДВОУ</button>
                        @endif

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-12">Комментарий</label>
                    <div class="col-sm-12">
                        {{ Form::textarea('comments', $report->comments, ['class' => 'form-control']) }}
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-12 ">Статус
                        @if($report->accept_status==2)
                            <span class="pull-right">Акцепт: {{setDateTimeFormatRu($report->accepted_at)}} {{$report->accept_user ? "пользователем {$report->accept_user->name}" : ""}} </span>
                        @endif
                    </label>

                    <div class="col-sm-12">
                        {{ Form::select('accept_status', collect(\App\Models\Reports\ReportOrders::STATE), $report->accept_status, ['class' => 'form-control']) }}
                    </div>

                </div>




                <div class="form-group">
                    <div class="col-sm-6">
                        @if($report->report_base_payments()->count() >= 0)
                            <span class="btn btn-success pull-left" onclick="setEditBasePayments()">Применить изменения - {{$report->report_base_payments()->count()}}</span>
                        @endif
                    </div>
                    <div class="col-sm-6">

                        <input type="submit" class="btn btn-primary btn-right" value="Сохранить"/>

                    </div>
                </div>






            </div>




            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th rowspan="2">СП</th>
                        <th colspan="3">Комиссия</th>
                        <th colspan="2">Сумма</th>
                        <th rowspan="2">Предварительный платеж</th>
                    </tr>

                    <tr>
                        <th>Бордеро</th>
                        <th>ДВОУ</th>
                        <th>Общая</th>
                        <th>К перечислению</th>
                        <th>К возврату</th>
                    </tr>


                    </thead>
                    <tbody>

                    <tr>
                        <td>{{titleFloatFormat($report->payment_total)}}</td>
                        <td>{{titleFloatFormat($report->bordereau_total)}}</td>
                        <td>{{titleFloatFormat($report->dvoy_total)}}</td>
                        <td>{{titleFloatFormat($report->amount_total)}}</td>
                        <td>{{titleFloatFormat($report->to_transfer_total)}}</td>
                        <td>{{titleFloatFormat($report->to_return_total)}}</td>
                        <td>{{titleFloatFormat($report->advance_payment)}}</td>
                    </tr>

                    <tr>
                        <th colspan="4"><p style="text-align: right">Итог</p></th>
                        <th>{{titleFloatFormat($report->to_transfer_total - $report->report_payment_sums->where('type_id', 0)->sum('amount'))}}</th>
                        <th>{{titleFloatFormat($report->to_return_total - $report->report_payment_sums->where('type_id', 1)->sum('amount'))}}</th>
                        <th></th>
                    </tr>
                    </tbody>
                </table>

                <h2>Фактические данные
                    <span class="btn btn-success pull-right" style="width: 30px;height: 25px;font-size: 10px;"
                          onclick="openFancyBoxFrame('{{ url("/reports/order/{$report->id}/payment_sum/create") }}')">
                        <i class="fa fa-plus"></i>
                    </span>
                </h2>

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Комментарии</th>
                        <th>Списание</th>
                        <th>Приход</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($report->report_payment_sums as $payment_sum)
                        <tr onclick="openFancyBoxFrame('{{ url("/reports/order/{$report->id}/payment_sum/{$payment_sum->id}/edit") }}')">
                            <td style="white-space: nowrap;">{{ setDateTimeFormatRu($payment_sum->created_at) }}
                                <br/> {{ $payment_sum->user ? $payment_sum->user->name : ""}}</td>
                            <td>{{ $payment_sum->comments }}</td>
                            <td style="white-space: nowrap;">{{ $payment_sum->type_id == 0 ? titleFloatFormat($payment_sum->amount) : "" }}</td>
                            <td style="white-space: nowrap;">{{ $payment_sum->type_id == 1 ? titleFloatFormat($payment_sum->amount) : ""}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="2"><span class="pull-right">Итог</span></th>
                        <th>{{titleFloatFormat($report->report_payment_sums->where('type_id', 0)->sum('amount'))}}</th>
                        <th>{{titleFloatFormat($report->report_payment_sums->where('type_id', 1)->sum('amount'))}}</th>
                    </tr>
                    </tfoot>

                </table>
                {{Form::close()}}

                @include("reports.partials.documents", ['report' => $report])

                {{--@include('reports.calc.sum_calc', ['calc_sum'=>$report->payment_total, 'calc_procent'=>'23'])--}}


            </div>


        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="filter-group" id="filters">


                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3 pt20">
                    <label class="control-label" for="location_id">Тип</label>
                    {{ Form::select('type_id', collect(\App\Models\Contracts\Payments::TRANSACTION_TYPE), [], ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3 pt20">
                    <label class="control-label" for="location_id">Статус</label>
                    {{ Form::select('statys_id', collect(\App\Models\Contracts\Payments::STATUS), [], ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3 " style="bottom: -15px;">
                    <label class="control-label" for="product_id">
                        Продукт
                        <sup>
                            <a class="btn-xs btn-success" id="select_all_products">выбрать все</a>
                        </sup>
                    </label>
                    {{ Form::select('product_id', \App\Models\Directories\Products::all()->pluck('title', 'id'), -1, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                </div>
                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3 pt20">
                    <label class="control-label" for="location_id">Событие</label>
                    {{ Form::select('location_id', \App\Models\BSO\BsoLocations::all()->pluck('title', 'id'), []/*2,*/, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3 pt20">
                    <label class="control-label" for="location_id">Тип платежа</label>
                    {{ Form::select('payment_type', collect(\App\Models\Contracts\Payments::PAYMENT_TYPE), -1, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3 pt20">
                    <label class="control-label" for="location_id">Поток оплаты</label>
                    {{ Form::select('payment_flow', collect(\App\Models\Contracts\Payments::PAYMENT_FLOW), -1, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3 pt20">
                    <label class="control-label" for="location_id">Тип акцепта</label>
                    {{ Form::select('kind_acceptance', collect(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE), -1, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                </div>
            </div>


        </div>
    </div>



    <div id="action_table" style="display: none;">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th colspan="3">Маркер</th>
                <th colspan="3">Перерасчёт КВ</th>
                <th>Операции</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <select id="marker_color" class="form-control marker_color">
                        @foreach(\App\Models\Reports\ReportOrders::MARKER_COLORS as $key => $marker)
                            <option value="{{$key}}"
                                    style="background-color: {{$marker['color']}};">{{$marker['title']}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    {{ Form::text('marker_text', '', ['class' => 'form-control', 'id'=>'marker_text']) }}
                </td>
                <td class="text-center">
                    <span class="btn btn-success pull-left" onclick="markerPaymentOrder()">Установить</span>
                </td>
                <td>
                    <input type="checkbox" name="activate_kv">
                    {{ Form::text('kv_borderau', '', ['disabled' => 'disabled', 'id' => 'kv_borderau', 'class' => 'form-control kv_input']) }}
                    &nbsp; КВ Бордеро
                </td>
                <td>
                    <input type="checkbox" name="activate_kv">
                    {{ Form::text('kv_dvou', '', ['disabled' => 'disabled', 'id' => 'kv_dvou', 'class' => 'form-control kv_input']) }}
                    &nbsp; КВ ДВОУ
                </td>
                <td width="1%">
                    <span class="btn btn-success " id="recalc_kv" onclick="recalcKV()">Пересчитать КВ</span>
                </td>

                <td width="1%">
                    @if($report->accept_status != 2)
                        <span class="btn btn-danger btn-right" onclick="deletePaymentOrder()">Удалить выбранные</span>
                    @endif
                </td>

            </tr>
            </tbody>
        </table>
    </div>

   <div class="block-inner">

       {{--
            <div id="table_vue">
                {{--Обертка для клона шапки-- }}
                <div v-bind:style="{ top: head_offset + 'px', width: max_w + 'px'}"  class='cloned_head_wrapper'>
                    <table v-bind:style="{width: max_w + 'px'}" class='table  table-bordered bso_items_table'></table>
                </div>
                {{--Обычная таблица как везде, вставляем bind на высоту-- }}
                <div id="table" style="overflow-y: visible; }}"
                     v-bind:style="{ 'max-height' : table_height + 'px'}">
                    @include("payments.reports.payments", ["payments"=>$payments])
                </div>
            </div>
        --}}

        @include('_chunks._vue_table')

   </div>






@endsection



@section('js')

    <style>
        .kv_input {
            width: 100px !important;
            display: inline-block;
        }

        .payments_table td {
            white-space: nowrap;
        }

        .payments_table {
            /* overflow-x: scroll; */
        }


        .wrapper1, .wrapper2 {
            width: 100%;
            overflow-x: scroll;
            overflow-y: hidden;
        }

        .wrapper1 {
            height: 20px;
        }

        .wrapper2 {
        }

        .div1 {
            height: 20px;
        }

        .div2 {
            overflow: none;
        }

    </style>
    <script>
        var PAGE = 1;

        var report_id = {{$report->id}};

        $(function () {

            loadItems();

            $(document).on('submit', '#addDocument', function (e) {
                e.preventDefault();
            });

            $(document).on('change', '[name="activate_kv"]', function () {
                toggleKVFields();
            });

            $(document).on('change', '[name="payment[]"]', function () {
                var uncheckeds = $('[name="payment[]"]').length - $('[name="payment[]"]:checked').length;
                $('[name="all_payments"]').prop('checked', uncheckeds === 0);
                showActions();
            });

            $(document).on('change', '[name="all_payments"]', function () {
                var checked = $(this).prop('checked');
                $('[name="payment[]"]').prop('checked', checked);
                showActions();
            });

            $(document).on('click', '#also_dvou', function(){
                $('#also_dvou').hide();
                newCustomConfirm(function (result) {
                    if (result === true){
                        loaderShow();
                        $.post('/reports/order/{{$report->id}}/generate_dvou_report', {}, function(res){
                            if (Boolean(res.status) === true){
                                flashMessage('success', res.message);
                            } else if (Boolean(res.status) === false) {
                                flashMessage('danger', res.message);
                            } else{
                                flashMessage('danger', 'Что-то пошло не так.. Сформировать ДВОУ не удалось.');
                            }
                            loaderHide();
                        });
                    }else{
                        $('#also_dvou').show();
                    }
                }, 'Сформировать отчет ДВОУ?');
            })


        });


        $(function () {
            $('.wrapper1').on('scroll', function (e) {
                $('.wrapper2').scrollLeft($('.wrapper1').scrollLeft());
            });
            $('.wrapper2').on('scroll', function (e) {
                $('.wrapper1').scrollLeft($('.wrapper2').scrollLeft());
            });
        });

        $(window).on('load', function (e) {
            $('.div1').width($('.bso_items_table').width());
            $('.div2').width($('.bso_items_table').width());
        });


        $(document).on('click', '#select_all_products', function (event) {
            event.preventDefault();
            $.each($('[name="product_id"] option'), function (k, v) {
                $(v).prop('selected', true);
            });
            $('[name="product_id"]').change();
        });

        function getData() {
            return {
                report_id: report_id,

                type_id: $('[name="type_id"]').val(),
                statys_id: $('[name="statys_id"]').val(),
                product_id: $('[name="product_id"]').val(),
                location_id: $('[name="location_id"]').val(),

                payment_type: $('[name="payment_type"]').val(),
                payment_flow: $('[name="payment_flow"]').val(),
                kind_acceptance: $('[name="kind_acceptance"]').val(),
                page_count: $("#page_count").val(),
                page: PAGE,
            }
        }

        function loadItems() {
            loaderShow();

            $.post('/reports/order/' + report_id + '/get_table', getData(), function (table_res) {
                $('#table').html(table_res);
                showActions();
            }).always(function () {
                loaderHide();
            });

        }

        function toggleKVFields() {
            $.each($('[name="activate_kv"]'), function (k, v) {
                var check = $(this).prop('checked');
                var input = $(this).siblings('input');
                if (check) {
                    input.removeAttr('disabled')
                } else {
                    input.attr('disabled', 'disabled');
                }
            })
        }


        function showActions() {
            if ($('[name="payment[]"]:checked').length > 0) {
                $('#action_table').show();
            } else {
                $('#action_table').hide();
            }
        }


        function getEventData() {
            var event_data = {
                payment_ids: [],
                marker_color: $('#marker_color').val(),
                marker_text: $('#marker_text').val(),
            };

            var borderau_inp = $('#kv_borderau');
            if (!borderau_inp.attr('disabled')) {
                event_data['kv_borderau'] = borderau_inp.val()
            }

            var dvou_inp = $('#kv_dvou');
            if (!dvou_inp.attr('disabled')) {
                event_data['kv_dvou'] = dvou_inp.val()
            }


            $.each($('[name="payment[]"]:checked'), function (k, v) {
                event_data.payment_ids.push($(v).val());
            });
            return event_data;
        }

        function recalcKV() {
            $.post('{{url("/reports/order/{$report->id}/recalc_kv")}}', getEventData(), function (res) {
                resetCheckboxes();
                if (res.status === 'ok') {
                    location.reload()
                }
            })
        }

        function deletePaymentOrder() {
            loaderShow();

            $.post('{{url("/reports/order/{$report->id}/delete_payments")}}', getEventData(), function (res) {
                if (parseInt(res.status) === 1) {
                    $('#action_table').hide();
                    $('[name="payment[]"]').prop('checked', null);
                    $('[name="all_payments"]').prop('checked', null);
                    resetCheckboxes();
                    reload();
                }
            });
        }

        function markerPaymentOrder() {
            loaderShow();
            $.post('{{url("/reports/order/{$report->id}/marker_payments")}}', getEventData(), function (res) {
                if (parseInt(res.status) === 1) {
                    $('#action_table').hide();
                    $('#marker_color').val(0);
                    $('#marker_text').val('');
                    $('[name="payment[]"]').prop('checked', null);
                    $('[name="all_payments"]').prop('checked', null);
                    resetCheckboxes();
                    reload();
                }
            });
        }


        function resetCheckboxes() {
            $('[name="activate_kv"]').removeProp('checked');
            $('[name="payment[]"]').removeProp('checked');
            toggleKVFields();
        }


        function setEditBasePayments() {
            loaderShow();
            $.get('{{url("/reports/order/{$report->id}/set_edit_base_payments")}}', {}, function (res) {
                resetCheckboxes();
                reload();

            });
        }

    </script>
@endsection