<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-subheading">
        <h2>Документы</h2>
    </div>
    <div class="block-main">
        <div class="block-sub">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="row form-horizontal">
                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <span class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="color: red;"></span>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            @if($report->files->count())
                                <table class="table orderStatusTable dataTable no-footer">

                                    <tbody>
                                    @foreach($report->files as $file)
                                        <div class="col-lg-6 col-md-12">
                                            <div class="upload-dot">
                                                <div class="block-image">
                                                    @if (in_array($file->ext, ['jpg', 'jpeg', 'png', 'gif']))
                                                        <a href="{{ url($file->url) }}" target="_blank">
                                                            <img class="media-object preview-image"
                                                                 src="{{ url($file->preview) }}"
                                                                 onerror="this.onerror=null;this.src='/images/extensions/unknown.png';">
                                                        </a>
                                                    @else
                                                        <a href="{{ url($file->url) }}" target="_blank">
                                                            <img class="media-object preview-icon"
                                                                 src="/images/extensions/{{$file->ext}}.png">
                                                        </a>
                                                    @endif
                                                    <div class="upload-close">
                                                        <div class="" style="float:right;color:red;">
                                                            <a href="javascript:void(0);"
                                                               onclick="removeReportsFile('{{ $file->name }}')">
                                                                <i class="fa fa-times"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h3>{{ trans('form.empty') }}</h3>
                            @endif
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {!! Form::open(['url'=> "/reports/order/{$report->id}/upload_file",'method' => 'post', 'class' => 'dropzone', 'id' => 'addDocument']) !!}
                            <div class="dz-message" data-dz-message>
                                <p>Перетащите сюда файлы</p>
                                <p class="dz-link">или выберите с диска</p>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>


    function initManyDocuments() {
        /*
         $("#addManyDocForm").dropzone({
         //Dropzone.options.addOrgDocForm = {
         paramName: 'file',
         maxFilesize: 10,
         //acceptedFiles: "image/*",
         init: function () {
         this.on("complete", function () {
         if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
         reload();
         }

         });
         }
         });
         */

    }

    function initDocuments() {
        initManyDocuments();
    }


    function removeReportsFile(fileName) {
        newCustomConfirm(function (result) {
            if (result){
                var filesUrl = '/files';
                var fileUrl = filesUrl + '/' + fileName;
                $.post(fileUrl, {
                    _method: 'DELETE'
                }, function () {
                    reload();
                });
            }
        });
    }

</script>