@extends('layouts.app')

@section('content')

    <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container" style="margin-top: -5px;overflow: auto;">
        <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="filter-group" id="filters">

                        <div class="col-lg-9">
                            <div class="btn-group col-xs-12 col-sm-12 col-md-6 col-lg-4">
                                <label class="control-label" for="like_title">Название</label>
                                {{ Form::text('like_title', '', ['class'=>'form-control','onkeyup'=>'loadItems()']) }}
                            </div>

                            <div class="btn-group col-xs-12 col-sm-12 col-md-6 col-lg-4">
                                <label class="control-label" for="accept_status">Статус</label>
                                {{ Form::select('accept_status', collect(\App\Models\Reports\ReportOrders::STATE), '', ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('_chunks._vue_table')

@include('_chunks._pagination',['callback'=>'loadItems'])
@endsection

@section('js')

<script>

    function event_change(e){
        $('#add_to_ad').hide();
        $('#ad_create').hide();

        if (e.val == 1){
            $('#ad_create').show();
        } else if(e.val == 2) {
            $('#add_to_ad').show();
        }
    }

    $(document).on('click', '#execute', function () {
        var event = $('[name="event_id"]').val();
        var name = $('[name="advertising_name"]').val();
        var adverstising_id = $('[name="advertising_id"]').val();

        $.post('{{url("/reports/promotion/update_adverstising")}}', {event: event, supplier_id: '{{ request('supplier_id', '') }}', title: name, adverstising_id: adverstising_id, reports_list: reports_list}, function (res) {


        }).always(function () {
            loaderHide();
        });
    });

    Array.prototype.remove = function(value) {
        var idx = this.indexOf(value);
        if (idx != -1) {
            // Второй параметр - число элементов, которые необходимо удалить
            return this.splice(idx, 1);
        }
        return false;
    }

    $(function () {
      loadItems();
    });

    var reports_list = [];

    $(document).on('change', '.reports_in_list', function () {
        var checked = $(this).prop('checked');
        if (checked){
            reports_list.push($(this).val());
        } else{
            reports_list.remove($(this).val());
        }

        if (reports_list.length > 0){
            $('.advertising_edit').show();
        } else{
            $('.advertising_edit').hide();
        }
    });

    function getData(use_by_button) {

        var arr = [];

        $.each((reports_list), function(k, v){
            arr.push(v);
        });

        return {
            type_id: $('[name="type_id"]').val(),
            accept_status: $('[name="accept_status"]').val(),
            report_month: $('[name="report_month"]').val(),
            page_count: $("#page_count").val(),
            payment_flow_type: $('[name="payment_flow_type"]').val(),
            like_title: $('[name="like_title"]').val(),
            year: $('[name="year"]').val(),
            id: $('[name="id"]').val(),
            reports_in_list: arr,
            use_by_button: use_by_button,
            used_by_button: used_by_button,
            motion: $('[name="motion"]').val(),
            page: PAGE,
        }
    }

    $(document).on('change', '[name="all_in_list"]', function () {
        var checked = $(this).prop('checked');
        $('[name="reports_in_list[]"]').prop('checked', checked);
    });

    function loadItemByButton() {
        var use_by_button = 1;

        loadItems(use_by_button);
    }

    var used_by_button = 0;

    function loadItems(use_by_button = 0) {

        if (used_by_button == 1){
            use_by_button = 1;
        }
        loaderShow();

        $.post('{{url("/reports/promotion/{$supplier->id}/adverstisings/table")}}', getData(use_by_button), function (table_res) {

            $('#table').html(table_res.html);

            if (table_res.count < 1) {
                table_res.count = 1;
            }

            let maxpage = Math.ceil(table_res.count / table_res.perpage);

            $('#view_row').html(PAGE);
            $('#max_row').html(maxpage);

            ajaxPaginationUpdate(maxpage,loadItems);

            $(".clickable-row").click(function () {
                if ($(this).attr('data-href')) {
                    window.location = $(this).attr('data-href');
                }
            });

            if (table_res.used_by_button == 1){
                used_by_button = 1;
            }

        }).always(function () {
            loaderHide();
        });

    }

    function deleteReport(event, id) {

        if (confirm("Вы уверены что хотите отчет "+id)) {
            loaderShow();
            var url = '{{ url("/reports/order") }}/'+ id +'/delete_report_with_payments';
            $.post(url, '', function (res) {
                if (parseInt(res.status) === 1) {
                    var tr = $(event).parent().parent();
                    tr.hide();
                }
            }).always(function () {
                loaderHide();
            });
        }
    }

</script>

@endsection