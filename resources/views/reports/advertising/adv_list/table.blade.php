<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th rowspan="2">ID</th>
            <th rowspan="2">Название</th>
            <th rowspan="2">Создан</th>
            <th rowspan="2">Количество отчетов</th>
            <th rowspan="2">СП</th>
            <th rowspan="2">Бордеро</th>
            <th rowspan="2">ДВОУ</th>
            <th rowspan="2">Общая</th>
            <th rowspan="2">К перечислению</th>
            <th rowspan="2">К возврату</th>
            <th rowspan="2">Оплачено</th>
            <th rowspan="2">К оплате</th>
            <th rowspan="2">Получено</th>
            <th rowspan="2">Долг СК</th>
            <th rowspan="2">Итог</th>
        </tr>
    </thead>
    <tbody>
        @if(sizeof($adverstisings))
            @foreach($adverstisings as $adverstising)
                @php
                    $url = url("/reports/promotion/{$supplier_id}/{$adverstising->id}/reports");
                    $url = "class=clickable-row data-href=$url";
                @endphp
                <tr>
                    <td {{$url}}>{{$adverstising->id}}</td>
                    <td {{$url}}>{{$adverstising->title}}</td>
                    <td {{$url}}>{{setDateTimeFormatRu($adverstising->created_at, 1)}}</td>
                    <td {{$url}}>{{($adverstising->reports)? count($adverstising->reports):''}}</td>
                    <td>{{titleFloatFormat($adverstising->payment_total)}}</td>
                    <td>{{titleFloatFormat($adverstising->bordereau_total)}}</td>
                    <td>{{titleFloatFormat($adverstising->dvoy_total)}}</td>
                    <td>{{titleFloatFormat($adverstising->amount_total)}}</td>
                    <td>{{titleFloatFormat($adverstising->to_transfer_total)}}</td>
                    <td>{{titleFloatFormat($adverstising->to_return_total)}}</td>
                    <td>{{titleFloatFormat($adverstising->report_pay_outcome)}}</td>
                    <td>{{titleFloatFormat($adverstising->report_pay_need)}}</td>
                    <td>{{titleFloatFormat($adverstising->report_pay_income)}}</td>
                    <td>{{titleFloatFormat($adverstising->debt_sk)}}</td>
                    <td>{{titleFloatFormat($adverstising->report_pay_need - $adverstising->debt_sk)}}</td>
                </tr>
            @endforeach
        @else
            <tr class="clickable-row">
                <td colspan="100" class="text-center">Нет результатов</td>
            </tr>
        @endif
    </tbody>
</table>