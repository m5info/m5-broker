@extends('layouts.app')

@section('content')


    <div class="page-heading">
        <h2>Отчеты СК</h2>
    </div>
    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="filter-group" id="filters"></div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div id="table"></div>
        </div>
    </div>





@endsection

@section('js')

    <script>
        $(function () {
            loadItems()
        });


        function getData(){
            return {
                org_id:$('[name="org_id"]').val(),
                insurance_id:$('[name="insurance_id"]').val(),
                supplier_id:$('[name="supplier_id"]').val(),
                group_type:$('[name="group_type"]').val(),
            }
        }


        function loadItems(){
            loaderShow();

            $.post('/reports/promotion/get_filters', getData(), function(filter_res){
                $('#filters').html(filter_res);
                $.post('/reports/promotion/get_table', getData(), function(table_res){
                    $('#table').html(table_res.html);
                    $('.select2-ws').select2("destroy").select2({
                        width: '100%',
                        dropdownCssClass: "bigdrop",
                        dropdownAutoWidth: true,
                        minimumResultsForSearch: -1
                    });
                });

            }).always(function(){
                loaderHide();
            });

        }

    </script>

@endsection