@extends('layouts.frame')


@section('title')

    Рекламник

@endsection

@section('content')


    {{ Form::open(['url' => url("/reports/reports_advertising/create_adverstising"), 'method' => 'post', 'class' => 'form-horizontal']) }}


    <div class="form-group">
        <label class="col-sm-4 control-label">Название</label>
        <div class="col-sm-8">
            {{ Form::text('bso_title', '', ['class' => 'form-control', 'id'=>'title']) }}
        </div>
    </div>

    {{Form::close()}}


@endsection

@section('footer')

    <button type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection