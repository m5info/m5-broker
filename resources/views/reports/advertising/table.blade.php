<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th rowspan="2">Поставщик</th>
            <th rowspan="2">Рекламник</th>
        </tr>

    </thead>
    <tbody>
        @php($group_id = 0)

        @if(sizeof($suppliers))
            @foreach($suppliers as $supplier)

                @if(session('report_sk.group_type', 'sk') == 'sk')
                    @php($check_group_id = $supplier->insurance->id)
                    @php($group_title = $supplier->insurance->title)
                @else
                    @php($check_group_id = $supplier->purpose_org->id)
                    @php($group_title = $supplier->purpose_org->title)
                @endif


                @if($group_id != $check_group_id)
                    <tr>
                        <td>
                            <b>{{ $group_title }}</b>
                        </td>
                        <td class="text-center"><b>...</b></td>


                    </tr>
                    @php($group_id = $check_group_id)
                @endif

                <tr class="clickable-row">
                    <td>
                        <a href="{{url("/reports/promotion/{$supplier->id}/adverstisings/")}}" target="_blank">
                            {{ $supplier->title }}
                        </a> (всего рекламников {{$supplier->adverstisings->count()}})
                    </td>
                    <td class="text-center">...</td>


                </tr>
            @endforeach
        @else
            <tr class="clickable-row">
                <td colspan="3" class="text-center">Нет результатов</td>
            </tr>
        @endif
    </tbody>
</table>