@extends('layouts.app')

@section('content')

<div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container" style="margin-top: -5px;overflow: auto;">
    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="filter-group" id="filters">

                    <div class="col-lg-6">
                        {{ Form::open(['url' => url("/reports/promotion/update_adverstising/"), 'method' => 'post', 'class' => 'form-horizontal']) }}
                            {{ Form::hidden('event', 3) }}
                            {{ Form::hidden('adverstising_id', $adverstising->id) }}
                            <div class="form-group">
                                <label class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    Название ID {{$adverstising->id}}
                                    (Сформирован: {{setDateTimeFormatRu($adverstising->created_at)}} {{$adverstising->create_user ? "пользователем {$adverstising->create_user->name}" : ""}}
                                    )
                                </label>
                                <div class="col-sm-12">
                                    {{ Form::text('title', $adverstising->title, ['class' => 'form-control', 'required']) }}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-12">Комментарий</label>
                                <div class="col-sm-12">
                                    {{ Form::textarea('comment', $adverstising->comment, ['class' => 'form-control']) }}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-12 ">Статус</label>

                                <div class="col-sm-12">
                                    {{ Form::select('accept_status', collect(\App\Models\Reports\ReportOrders::STATE), $adverstising->accept_status, ['class' => 'form-control']) }}
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="submit" class="btn btn-primary btn-right" value="Сохранить"/>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>


                    <div class="col-lg-6">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">

                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>

                                    <th colspan="4"></th>
                                    <th colspan="2">Суммы</th>
                                    <th colspan="4">Фактические данные</th>
                                    <th colspan="1"></th>
                                </tr>

                                <tr>
                                    <th colspan="1">СП</th>
                                    <th colspan="1">Бордеро</th>
                                    <th colspan="1">ДВОУ</th>
                                    <th colspan="1">Общая</th>
                                    <th>К перечислению</th>
                                    <th>К возврату</th>
                                    <th>Оплачено</th>
                                    <th>К оплате</th>
                                    <th>Получено</th>
                                    <th>Долг СК</th>
                                    <th>Итог</th>
                                </tr>

                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{titleFloatFormat($adverstising->payment_total)}}</td>
                                    <td>{{titleFloatFormat($adverstising->bordereau_total)}}</td>
                                    <td>{{titleFloatFormat($adverstising->dvoy_total)}}</td>
                                    <td>{{titleFloatFormat($adverstising->amount_total)}}</td>
                                    <td>{{titleFloatFormat($adverstising->to_transfer_total)}}</td>
                                    <td>{{titleFloatFormat($adverstising->to_return_total)}}</td>
                                    <td>{{titleFloatFormat($adverstising->report_pay_outcome)}}</td>
                                    <td>{{titleFloatFormat($adverstising->report_pay_need)}}</td>
                                    <td>{{titleFloatFormat($adverstising->report_pay_income)}}</td>
                                    <td>{{titleFloatFormat($adverstising->debt_sk)}}</td>
                                    <td>{{titleFloatFormat($adverstising->report_pay_need - $adverstising->debt_sk)}}</td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="submit" id="recount_payments" class="btn btn-primary btn-left" value="Пересчитать"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12 advertising_edit" style="display: none">
    <button onclick="loadItemByButton()" type="submit" class="btn btn-success">Применить</button>
</div>
@include('_chunks._vue_table')
<div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <button onclick="loadItemByButton()" type="submit" class="btn btn-success">Применить</button>
</div>

@include('_chunks._pagination',['callback'=>'loadItems'])
@endsection

@section('js')

<script>

    function event_change(e){
        $('#add_to_ad').hide();
        $('#ad_create').hide();

        if (e.val == 1){
            $('#ad_create').show();
        } else if(e.val == 2) {
            $('#add_to_ad').show();
        }
    }

    $(document).on('click', '#execute', function () {
        var event = $('[name="event_id"]').val();
        var name = $('[name="advertising_name"]').val();
        var adverstising_id = $('[name="advertising_id"]').val();

        $.post('{{url("/reports/promotion/update_adverstising")}}', {event: event, supplier_id: '{{ request('supplier_id', '') }}', title: name, adverstising_id: adverstising_id, reports_list: reports_list}, function (res) {


        }).always(function () {
            loaderHide();
        });
    });

    Array.prototype.remove = function(value) {
        var idx = this.indexOf(value);
        if (idx != -1) {
            // Второй параметр - число элементов, которые необходимо удалить
            return this.splice(idx, 1);
        }
        return false;
    }

    $(function () {
      loadItems();
    });

    var reports_list = [];

    $(document).on('change', '.reports_in_list', function () {
        var checked = $(this).prop('checked');
        if (checked){
            reports_list.push($(this).val());
        } else{
            reports_list.remove($(this).val());
        }

        if (reports_list.length > 0){
            $('.advertising_edit').show();
        } else{
            $('.advertising_edit').hide();
        }
    });

    function process(operations = []){
        $.each(operations, function(k,operation){
            if(isFunction(operation)){
                window[operation]()
            }
        })
    }

    $(document).on('click', '#recount_payments', function () {

        loaderShow();
        var url = '{{ url("/reports/promotion/recount") }}';

        $.post(url, {adverstising_id: '{{$adverstising->id}}'}, function (res) {
            window.location.reload();
        }).always(function () {
            loaderHide();
        });

    });

    function getData(use_by_button) {

        var arr = [];

        $.each((reports_list), function(k, v){
            arr.push(v);
        });

        return {
            type_id: $('[name="type_id"]').val(),
            accept_status: $('[name="accept_status"]').val(),
            report_month: $('[name="report_month"]').val(),
            page_count: $("#page_count").val(),
            payment_flow_type: $('[name="payment_flow_type"]').val(),
            like_title: $('[name="like_title"]').val(),
            year: $('[name="year"]').val(),
            id: $('[name="id"]').val(),
            reports_in_list: arr,
            use_by_button: use_by_button,
            used_by_button: used_by_button,
            motion: $('[name="motion"]').val(),
            page: PAGE,
        }
    }

    $(document).on('change', '[name="all_in_list"]', function () {
        var checked = $(this).prop('checked');
        $('[name="reports_in_list[]"]').prop('checked', checked);
    });

    function loadItemByButton() {
        var use_by_button = 1;

        loadItems(use_by_button);
    }

    var used_by_button = 0;

    function loadItems(use_by_button = 0) {

        if (used_by_button == 1){
            use_by_button = 1;
        }
        loaderShow();

        $.post('{{url("/reports/promotion/{$supplier->id}/{$adverstising_id}/reports/table")}}', getData(use_by_button), function (table_res) {

            $('#table').html(table_res.html);

            if (table_res.count < 1) {
                table_res.count = 1;
            }

            let maxpage = Math.ceil(table_res.count / table_res.perpage);

            $('#view_row').html(PAGE);
            $('#max_row').html(maxpage);

            ajaxPaginationUpdate(maxpage,loadItems);

            $(".clickable-row").click(function () {
                if ($(this).attr('data-href')) {
                    window.location = $(this).attr('data-href');
                }
            });

            if (table_res.used_by_button == 1){
                used_by_button = 1;
            }

        }).always(function () {
            loaderHide();
        });

    }

    function deleteReportFromAd(event, id) {

        if (confirm("Вы уверены что хотите удалить отчет "+id)) {
            loaderShow();

            var adverstising_id = '{{$adverstising_id}}';

            $.post('{{url("/reports/promotion/update_adverstising")}}', {event: 4, report_id: id, adverstising_id: adverstising_id}, function (res) {

                window.location.reload();
            }).always(function () {
                loaderHide();
            });

        }
    }

</script>

@endsection