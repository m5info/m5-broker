<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th rowspan="2">Все {{ Form::checkbox('all_in_list', 1) }}</th>
            <th rowspan="2">ID</th>
            <th rowspan="2">Название</th>

            <th rowspan="2">Создан</th>
            <th rowspan="2">Акцептован</th>
            <th rowspan="2">Поток оплаты</th>
            <th rowspan="2">Статус</th>
            <th rowspan="2">Месяц</th>
            <th rowspan="2">Год</th>
            <th rowspan="2">Тип</th>

            <th rowspan="2">Риски</th>

            <th rowspan="2">СП</th>

            <th colspan="3">Комиссия</th>
            <th colspan="2">Суммы</th>

            <th colspan="5">Фактические данные</th>


            <th rowspan="2">Комментарий</th>
            <th rowspan="2">Действие</th>


        </tr>

        <tr>
            <th>Бордеро</th>
            <th>ДВОУ</th>
            <th>Общая</th>

            <th style="white-space: nowrap;">К перечислению</th>
            <th style="white-space: nowrap;">К возврату</th>

            <th>Оплачено</th>
            <th>К оплате</th>
            <th>Получено</th>
            <th>Долг СК</th>

            <th>Предварительный платеж</th>

            {{--
            <th>Приход</th>
            <th>Сумма предв. оплаты</th>
            <th>Списание</th>
            <th>Долг</th>
            --}}
        </tr>

    </thead>
    <tbody>
        @php($ins_id = 0)

        @php($payment_total_all = 0)
        @php($bordereau_total_all = 0)
        @php($dvoy_total_all = 0)
        @php($amount_total_all = 0)
        @php($to_transfer_total_all = 0)
        @php($to_return_total_all = 0)
        @php($report_payment_sums_transfer_total_all = 0)
        @php($report_payment_sums_return_total_all = 0)
        @php($full_advance_payment = 0)
        @php($full_prepayment_summ = 0)
        @php($full_returnpayment_summ = 0)




        @if(sizeof($reports))
            @foreach($reports as $report)

                @php($report_payment_sums_transfer_total = $report->report_payment_sums->where('type_id', 1)->sum('amount'))
                @php($report_payment_sums_return_total = $report->report_payment_sums->where('type_id', 0)->sum('amount'))

                @php($payment_total_all += $report->payment_total)
                @php($bordereau_total_all += $report->bordereau_total)
                @php($dvoy_total_all += $report->dvoy_total)
                @php($amount_total_all += $report->amount_total)
                @php($to_transfer_total_all += $report->to_transfer_total)
                @php($to_return_total_all += $report->to_return_total)


                @php($report_payment_sums_transfer_total_all += $report_payment_sums_transfer_total)
                @php($report_payment_sums_return_total_all += $report_payment_sums_return_total)

                @php($prepayment_summ = $report->to_transfer_total-$report_payment_sums_return_total)
                @php($full_prepayment_summ += $prepayment_summ)
                @php($full_returnpayment_summ += $report->to_return_total-$report_payment_sums_transfer_total)


                @php($td_url = "class=clickable-row data-href=/reports/order/{$report->id}")


                @php($advance_payment = $report->to_transfer_total - $report->to_return_total)
                @if($advance_payment < 0) @php($advance_payment = 0) @endif

                @php($full_advance_payment += $advance_payment)

                <tr>
                    <td>{{ Form::checkbox("reports_in_list[]", $report->id, in_array($report->id, $reports_in_list) ? true : false, ['class' => 'reports_in_list']) }}</td>
                    <td {{ $td_url }}>{{$report->id}}</td>
                    <td {{ $td_url }}>{{$report->title}}</td>

                    <td {{ $td_url }}>{{setDateTimeFormatRu($report->created_at, 1)}} {{$report->create_user->name}}</td>
                    <td {{ $td_url }}>{{setDateTimeFormatRu($report->accepted_at, 1)}} {{($report->accept_user)?$report->accept_user->name:''}}</td>


                    <td {{ $td_url }}>{{ collect(\App\Models\Reports\ReportOrders::PAYMENT_FLOW_TYPE)->has($report->payment_flow_type) ? \App\Models\Reports\ReportOrders::PAYMENT_FLOW_TYPE[$report->payment_flow_type] : 'Неизвестно' }}</td>
                    <td {{ $td_url }}>{{ \App\Models\Reports\ReportOrders::STATE[$report->accept_status]}}</td>
                    <td {{ $td_url }}>{{getRuMonthes()[$report->report_month]}}</td>
                    <td {{ $td_url }}>{{$report->report_year}}</td>
                    <td {{ $td_url }}>{{\App\Models\Reports\ReportOrders::TYPE[$report->type_id]}}</td>

                    <td {{ $td_url }}>{{$report->getProductsTitleList()}}</td>

                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report->payment_total)}}</td>

                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report->bordereau_total)}}</td>
                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report->dvoy_total)}}</td>
                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report->amount_total)}}</td>

                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report->to_transfer_total)}}</td>
                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report->to_return_total)}}</td>




                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report_payment_sums_return_total)}}</td>
                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report->to_transfer_total-$report_payment_sums_return_total)}}</td>
                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report_payment_sums_transfer_total)}}</td>
                    <td style="white-space: nowrap;" {{ $td_url }}>{{titleFloatFormat($report->to_return_total-$report_payment_sums_transfer_total)}}</td>
                    <td style="white-space: nowrap;" {{ $td_url }}>{{ titleFloatFormat($advance_payment) }}</td>





                    <td {{ $td_url }}>{{$report->comments}}</td>
                    <td>
                        @if(!$report->is_recorded_to_foundations)
                            <span class="btn btn-danger btn-right" onclick="deleteReport(this, {{$report->id}})">Удалить</span>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr class="clickable-row">
                <td colspan="100" class="text-center">Нет результатов</td>
            </tr>
        @endif
    </tbody>
    <tfoot>

        <tr>
            <th colspan="15"></th>

            <th style="white-space: nowrap;">{{titleFloatFormat($to_transfer_total_all)}}</th>
            <th style="white-space: nowrap;">{{titleFloatFormat($to_return_total_all)}}</th>


            <th style="white-space: nowrap;">{{titleFloatFormat($report_payment_sums_return_total_all)}}</th>
            <th style="white-space: nowrap;">{{titleFloatFormat($full_prepayment_summ)}}</th>
            <th style="white-space: nowrap;">{{titleFloatFormat($report_payment_sums_transfer_total_all)}}</th>
            <th style="white-space: nowrap;">{{titleFloatFormat($full_returnpayment_summ)}}</th>
            <th style="white-space: nowrap;">{{titleFloatFormat($full_advance_payment)}}</th>
            <th colspan="2"></th>
        </tr>

    </tfoot>
</table>