@extends('layouts.app')

@section('content')

    <div class="header_bab">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="pull-right"><a
                        onclick="openFancyBoxFrame('/account/table_setting/{{$table_key}}/edit/?controller=App\\Http\\Controllers\\Reports\\ReportsSK\\ReportsSKFormationController')"
                        class="btn btn-primary inline-block">Настроить отображение</a></div>
            <div id="tt" class="easyui-tabs">
                <div title="Реестр корзина" data-report_id="0"></div>
                <div title="Реестр текущий" data-report_id="-1"></div>
                <div title="Реестр будущий" data-report_id="-2"></div>
            </div>
        </div>
    </div>

    <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container"
         style="margin-top: -5px;overflow: auto;">
        <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="filter-group" id="filters">
                        <div class="col-sm-12 col-md-4 col-lg-4">
                            <h2>Период</h2>
                            <div class="col-sm-12 col-ls-12" style="margin-top: 25px;">
                                {{ Form::select('date_type', collect([1 => 'Дата оплаты', 2 => 'Дата заключения договора', 3 => 'Дата начала договора', 4 => 'Дата окончания договора']), 2, ['class'=>'form-control select2-all','onchange'=>'loadItems()']) }}
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                <label class="control-label">С</label>
                                {{ Form::text('date_from', ''/*date('d.m.Y', strtotime('-1 months'))*/, ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off', 'onchange' => 'loadItems()']) }}
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                <label class="control-label">По</label>
                                {{ Form::text('date_to', ''/*date('d.m.Y')*/, ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off', 'onchange' => 'loadItems()']) }}
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-8 col-lg-8">
                            <div class="col-sm-12 col-lg-12">
                                <h2>Детали</h2>
                            </div>
                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="location_id">Тип договора</label>
                                {{ Form::select('type_id', collect(\App\Models\Contracts\Contracts::TYPE), [0], ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                            </div>

                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="location_id">Статус</label>
                                {{ Form::select('statys_id', collect(\App\Models\Contracts\Payments::STATUS), null, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                            </div>

                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="product_id">
                                    Продукт
                                    <sup><a href="javascript:void(0);" class="btn-xs btn-success"
                                            id="select_all_products">выбрать все</a></sup>
                                </label>
                                {{ Form::select('product_id', \App\Models\Directories\Products::all()->pluck('title', 'id'), -1, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                            </div>
                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="location_id">Событие</label>
                                {{ Form::select('location_id', \App\Models\BSO\BsoLocations::all()->pluck('title', 'id'), null, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                            </div>


                            <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>

                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="location_id">Тип платежа</label>
                                {{ Form::select('payment_type', collect(\App\Models\Contracts\Payments::PAYMENT_TYPE), -1, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                            </div>

                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="location_id">Поток оплаты</label>
                                {{ Form::select('payment_flow', collect(\App\Models\Contracts\Payments::PAYMENT_FLOW), -1, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                            </div>

                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="location_id">Тип акцепта</label>
                                {{ Form::select('kind_acceptance', collect(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE), -1, ['class'=>'form-control select2-all','onchange'=>'loadItems()', 'multiple' => true]) }}
                            </div>
                            <div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
                            <div class="btn-group col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                <label class="control-label" for="bso_title">БСО</label>
                                {{ Form::text('bso_title', '', ['class' => 'form-control', 'id' => 'bso_title','onchange'=>'loadItems()']) }}
                            </div>

                            <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <label class="control-label" for="act_sk_id">Номер Акта</label>
                                {{ Form::text('act_sk_id', '', ['class' => 'form-control', 'id' => 'act_sk_id','onchange'=>'loadItems()']) }}
                            </div>

                            @if($report_prefix == 'dvoy')
                                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                    <label class="control-label" for="reports_order_id">Номер бордеро</label>
                                    {{ Form::text('reports_order_id', '', ['class' => 'form-control', 'id' => 'reports_order_id','onchange'=>'loadItems()']) }}
                                </div>
                            @elseif($report_prefix == 'bordereau')
                                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                    <label class="control-label" for="reports_dvou_id">Номер ДВОУ</label>
                                    {{ Form::text('reports_dvou_id', '', ['class' => 'form-control', 'id' => 'reports_dvou_id','onchange'=>'loadItems()']) }}
                                </div>
                            @endif
                            {{ Form::hidden('report_id', '0') }}
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div id="action_table"></div>
            </div>
        </div>



    </div>

    @include('_chunks._pagination',['callback'=>'loadItems','pagination__off'=> true,'count_pagination'=>[0=>'Все', 50=>'50', 500=>'500', 1000=>'1000', 150=>'5000'], 'b_class' => 'pull-right', 'statistic' => true])

    @include('_chunks._vue_table')

@endsection

@section('js')

    <script>
        $(function () {

            $('#tt').tabs({
                border: false, pill: false, plain: true,
                onSelect: function (title, index) {
                    return selectTab(index);
                }
            });


            $(document).on('change', '[name="payment[]"]', function () {
                var uncheckeds = $('[name="payment[]"]').length - $('[name="payment[]"]:checked').length;
                $('[name="all_payments"]').prop('checked', uncheckeds === 0);
                showActions();
            });


            $(document).on('click', '#select_all_products', function () {
                $.each($('[name="product_id"] option'), function (k, v) {
                    $(v).prop('selected', true);
                });
                $('[name="product_id"]').change();
            });


            $(document).on('change', '[name="all_payments"]', function () {
                var checked = $(this).prop('checked');
                $('[name="payment[]"]').prop('checked', checked);
                showActions();
            });


            $(document).on('click', '#execute', function () {
                var event_data = getEventData();

                loaderShow();
                $.post('/reports/reports_sk/{{$supplier->id}}/{{$report_prefix}}/execute', event_data, function (res) {
                    if (parseInt(res.status) === 1) {
                        flashMessage('success', 'Операция выполнена успешно');
                        loadItems();
                    }

                    loaderHide();
                })
            });

            loadItems();

        });


        function selectTab(index) {
            var report_id = $($('[data-report_id]')[index]).data('report_id');
            $('[name="report_id"]').val(report_id);
            loadItems();
        }


        function getData() {
            return {
                report_id: $('[name="report_id"]').val(),
                product_id: $('[name="product_id"]').val(),
                location_id: $('[name="location_id"]').val(),
                bso_title: $('[name="bso_title"]').val(),
                reports_dvou_id: $('[name="reports_dvou_id"]').val(),
                reports_order_id: $('[name="reports_order_id"]').val(),
                act_sk_id: $('[name="act_sk_id"]').val(),
                type_id: $('[name="type_id"]').val(),
                statys_id: $('[name="statys_id"]').val(),
                payment_type: $('[name="payment_type"]').val(),
                payment_flow: $('[name="payment_flow"]').val(),
                kind_acceptance: $('[name="kind_acceptance"]').val(),
                date_type: $('[name="date_type"]').val(),
                date_from: $('[name="date_from"]').val(),
                date_to: $('[name="date_to"]').val(),
                page_count: $('[name="page_count"]').val(),
            }
        }


        function getEventData() {
            var event_data = {
                payment_ids: [],
                report_name: $('[name="report_name"]').val(),
                and_dvou_report: 0,
                report_year: $('[name="report_year"]').val(),
                report_month: $('[name="report_month"]').val(),
                report_date_start: $('[name="report_date_start"]').val(),
                report_date_end: $('[name="report_date_end"]').val(),
                to_report_id: $('[name="to_report_id"]').val(),
                event_id: $('[name="event_id"]').val(),

            };
            $.each($('[name="payment[]"]:checked'), function (k, v) {
                event_data.payment_ids.push($(v).val());
            });
            if ($('[name="and_dvou_report"]').prop('checked')) {
                event_data.and_dvou_report = 1;
            }
            return event_data;
        }


        function loadItems() {
            loaderShow();

            $.post('/reports/reports_sk/{{$supplier->id}}/{{$report_prefix}}/get_table', getData(), function (table_res) {
                $('#table').html(table_res['table']);
                $('#statistic').html(table_res['statistic']);
                /*
                               for(let i = 0; i <50 ; i++)
                                    $('#table').append(table_res);*/

                $('.wrapper1').on('scroll', function (e) {
                    $('.wrapper2').scrollLeft($('.wrapper1').scrollLeft());
                });
                $('.wrapper2').on('scroll', function (e) {
                    $('.wrapper1').scrollLeft($('.wrapper2').scrollLeft());
                });

                $('.div1').width($('.bso_items_table').width());
                $('.div2').width($('.bso_items_table').width());

                ajaxPaginationUpdate('25', loadItems);

                if (vue_table)
                    vue_table.resetHead();
                showActions();

            }).always(function () {
                loaderHide();
            });

        }


        function showActions() {
            if ($('[name="payment[]"]:checked').length > 0) {

                $.post('/reports/reports_sk/{{$supplier->id}}/{{$report_prefix}}/get_action_table', getData(), function (actions_res) {
                    $('#action_table').html(actions_res)
                });

            } else {
                $('#action_table').html('')
            }

        }


    </script>

    <style>
        .wrapper1, .wrapper2 {
            width: 100%;
            overflow-x: scroll;
            overflow-y: hidden;
        }

        .wrapper1 {
            height: 20px;
        }

        .wrapper2 {
        }

        .div1 {
            height: 20px;
        }

        .div2 {
            overflow: none;
        }

    </style>

@endsection