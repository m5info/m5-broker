<table style="overflow:hidden" class="table table-bordered bso_items_table">

    <thead>
    <tr>
        @if($check_result == true)

            @foreach($user_columns as $column)

                @php
                    $current = \App\Http\Controllers\Reports\ReportsSK\ReportsSKFormationController::TABLE_KEY_VISIBILITY[$column['column_name']];
                    $check_visibility = 0;
                @endphp

                @foreach ($visibility as $display)

                    @if(stristr($current, $display->title) == true)
                        @php
                            $check_visibility = 1;
                        @endphp
                        @break
                    @endif

                @endforeach

                @if($check_visibility)


                    <th>
                        @if($column['column_name'] == '№')
                            <input type="checkbox" name="all_payments">
                            (<span class="total_count">{{$payments->count()}}</span>)
                        @elseif($column['column_name'] == 'Агент/Менеджер')
                            <p class="nowrap">{{$column['column_name']}}</p>
                        @else
                            @if($column['sorting'] == 1)
                                {{ $column['column_name'] }}
                            @else
                                {{ $column['column_name'] }}
                            @endif
                        @endif

                    </th>
                @endif
            @endforeach
        @else
            @if(isset($visibility['all']))
                @foreach(\App\Http\Controllers\Reports\ReportsSK\ReportsSKFormationController::DEFAULT_NOT_SELECTED_COLUMNS_ON as $col)
                    @if($col == '№')
                        <th>
                            <input type="checkbox" name="all_payments">
                            (<span class="total_count">{{$payments->count()}}</span>)
                        </th>
                    @else
                        <th>{{$col}}</th>
                    @endif
                @endforeach

            @endif
        @endif
    </tr>
    </thead>
    <tbody class="payments_table_tbody">
    @if(sizeof($payments))

        @if(!$check_result)

            @foreach($payments as $payment)

                @php
                    $base = $payment->base_payment;
                @endphp

                <tr @if(strlen($payment->marker_color) > 3) style="background-color: {{$payment->marker_color}}" @endif>

                    <td class="text-center">
                        <input class="payment_checkbox" type="checkbox" name="payment[]" value="{{$payment->id}}">
                    </td>
                    <td nowrap>@if($payment->acts_sk_id > 0)
                            <a target="_blank"
                               href="/bso_acts/acts_sk/{{$payment->bso->bso_supplier_id}}/acts/{{$payment->acts_sk_id}}/edit">{{$payment->act_sk->title}}
                                - {{$payment->act_sk->id}}</a>
                        @else
                            Не сформирован
                        @endif
                    </td>
                    <td nowrap>
                        @if(isset($report_id) && $report_id > 0)
                            <a href="#"
                               onclick="openFancyBoxFrame('{{url("/payment/{$payment->id}/?report_id=$report_id")}}')">{{ $payment->bso ? $payment->bso->bso_title : "" }}</a>
                        @else
                            <a href="#"
                               onclick="openFancyBoxFrame('{{url("/payment/{$payment->id}/")}}')">{{ $payment->bso ? $payment->bso->bso_title : "" }}</a>
                        @endif

                    </td>
                    <td nowrap>{{ $payment->bso && $payment->bso->product ? $payment->bso->product->title : ""}}</td>
                    <td nowrap>{{ $payment->Insurer }}</td>

                    <td nowrap>
                        {{($payment->contract && $payment->contract->sales_condition==0) ? ($payment->agent ? $payment->agent->name : '') : ($payment->manager ? $payment->manager->name : '')}}
                    </td>
                    <td>{{$payment->underwriter_name }}</td>
                    <td nowrap>{{($payment->parent_agent)?$payment->parent_agent->name:''}}</td>


                    <td>{{ ($payment->contract && $payment->contract->type_id>0)?\App\Models\Contracts\Contracts::TYPE[$payment->contract->type_id]:"" }}</td>

                    <td>{{ \App\Models\Contracts\Payments::PAYMENT_TYPE[$payment->payment_type] }}</td>
                    <td>{{ \App\Models\Contracts\Payments::PAYMENT_FLOW[$payment->payment_flow] }}</td>
                    <td>{{ $payment->bso_receipt ? $payment->bso_receipt : "" }}</td>


                    <td @if($payment->checkBaseData($base, 'payment_total') == false) style="color: red;"
                        @endif nowrap>{{ getPriceFormat($payment->payment_total) }}</td>
                    <td @if($payment->checkBaseData($base, 'official_discount') == false) style="color: red;"
                        @endif nowrap>{{ getPriceFormat($payment->official_discount_total) }}</td>
                    <td @if($payment->checkBaseData($base, 'official_discount') == false) style="color: red;" @endif>
                        {{ getPriceFormat($payment->official_discount) }}
                    </td>
                    <td @if($payment->checkBaseData($base, 'informal_discount') == false) style="color: red;"
                        @endif nowrap>{{ getPriceFormat($payment->informal_discount_total) }}</td>
                    <td @if($payment->checkBaseData($base, 'informal_discount') == false) style="color: red;" @endif>
                        {{ getPriceFormat($payment->informal_discount) }}
                    </td>

                    <td class="text-center">{{ getDateFormatRu($payment->payment_data) }}</td>

                    <td>{{ \App\Models\Contracts\Payments::STATUS[$payment->statys_id] }}</td>

                    <td>{{($payment->financial_policy_manually_set == 0)?
                ($payment->financial_policy)?$payment->financial_policy->title:''
                :''}}</td>

                    <td @if($payment->checkBaseData($base, 'financial_policy_kv_agent') == false) style="color: red;"
                        @endif nowrap>{{ getPriceFormat($payment->financial_policy_kv_agent_total) }}</td>

                    <td @if($payment->checkBaseData($base, 'financial_policy_kv_bordereau') == false) style="color: red;"
                        @endif class="text-center">{{ getPriceFormat($payment->financial_policy_kv_bordereau) }}</td>
                    <td @if($payment->checkBaseData($base, 'financial_policy_kv_bordereau') == false) style="color: red;"
                        @endif nowrap
                        class="text-center">{{ getPriceFormat($payment->financial_policy_kv_bordereau_total) }}</td>
                    <td>
                        @if($payment->reports_order_id > 0)
                            <a href="{{url("/reports/order/{$payment->reports_order_id}/")}}"
                               target="_blank">{{$payment->reports_border->title}}</a>
                        @endif
                    </td>
                    <td @if($payment->checkBaseData($base, 'financial_policy_kv_dvoy') == false) style="color: red;"
                        @endif class="text-center">{{ getPriceFormat($payment->financial_policy_kv_dvoy) }}</td>
                    <td @if($payment->checkBaseData($base, 'financial_policy_kv_dvoy') == false) style="color: red;"
                        @endif nowrap
                        class="text-center">{{ getPriceFormat($payment->financial_policy_kv_dvoy_total) }}</td>
                    <td nowrap>
                        @if($payment->reports_dvou_id > 0)
                            <a href="{{url("/reports/order/{$payment->reports_dvou_id}/")}}"
                               target="_blank">{{$payment->reports_dvoy->title}}</a>
                        @endif
                    </td>

                    <td class="text-center">{{ $payment->contract ? getDateFormatRu($payment->contract->sign_date) : "" }}</td>
                    <td class="text-center">{{ $payment->contract ? getDateFormatRu($payment->contract->begin_date) : "" }}</td>
                    <td class="text-center">{{ $payment->contract ? getDateFormatRu($payment->contract->end_date) : "" }}</td>


                    <td>{{ $payment->marker_text }}</td>

                    <td>{{ $payment->contract ? $payment->contract->kind_acceptance_ru('kind_acceptance') : ""}} </td>

                    <td>{{ $payment->payment_total_minus_bordero }}</td>

                    <td>{{isset(\App\Models\Contracts\Contracts::SALES_CONDITION[$payment->contracts_sales_condition]) ? \App\Models\Contracts\Contracts::SALES_CONDITION[$payment->contracts_sales_condition] : ""}}</td>

                </tr>
            @endforeach
        @else
            @foreach($payments as $key => $payment)

                @php
                    $base = $payment->base_payment;
                @endphp

                <tr>
                    @foreach($user_columns as $column)
                        @php
                            //dd($column);
                            $current = \App\Http\Controllers\Reports\ReportsSK\ReportsSKFormationController::TABLE_KEY_VISIBILITY[$column['column_name']];
                            $check_visibility = 0;
                        @endphp

                        @foreach ($visibility as $display)

                            @if(stristr($current, $display->title) == true)
                                @php
                                    $check_visibility = 1;
                                @endphp
                                @break
                            @endif

                        @endforeach
                        @php

                                @endphp
                        @if($check_visibility)
                            <td @if(strlen($payment->marker_color) > 3) style="background-color: {{$payment->marker_color}}" @endif>

                                @if($column['_key'] == 'payments_id')
                                    <input class="payment_checkbox" type="checkbox" name="payment[]"
                                           value="{{$payment->id}}">
                                @elseif($column['_key'] == 'bso_title')
                                    @if(isset($report_id) && $report_id > 0)
                                        <a class="nowrap" href="#"
                                           onclick="openFancyBoxFrame('{{url("/payment/{$payment->id}/?report_id=$report_id")}}')">{{ $payment->bso ? $payment->bso->bso_title : "" }}</a>
                                    @else
                                        <a class="nowrap" href="#"
                                           onclick="openFancyBoxFrame('{{url("/payment/{$payment->id}/")}}')">{{ $payment->bso ? $payment->bso->bso_title : "" }}</a>
                                    @endif
                                @elseif($column['_key'] == 'reports_act_id')

                                    @if($payment->acts_sk_id > 0)<a class="nowrap" target="_blank"
                                                                    href="/bso_acts/acts_sk/{{$payment->bso->bso_supplier_id}}/acts/{{$payment->acts_sk_id}}/edit">{{$payment->act_sk->title}}
                                        - {{$payment->act_sk->id}}</a>
                                    @else
                                        <p class="nowrap">Не сформирован</p>
                                    @endif

                                @elseif($column['_key'] == 'contracts_type_id')
                                    {{ App\Models\Contracts\Contracts::TYPE[$payment[$column['_key']]] }}
                                @elseif($column['_key'] == 'payments_type')
                                    {{ \App\Models\Contracts\Payments::PAYMENT_TYPE[$payment[$column['_key']]] }}
                                @elseif($column['_key'] == 'payments_flow')
                                    {{ \App\Models\Contracts\Payments::PAYMENT_FLOW[$payment[$column['_key']]] }}
                                @elseif($column['_key'] == 'payments_payment_total')
                                    <div @if($payment->checkBaseData($base, 'payment_total') == false) style="color: red;" @endif >
                                        <p class="nowrap">{{ getPriceFormat($payment->payment_total) }}</p>
                                    </div>
                                @elseif($column['_key'] == 'payments_official_discount_total')
                                    <div @if($payment->checkBaseData($base, 'official_discount') == false) style="color: red;" @endif >
                                        <p class="nowrap">{{ getPriceFormat($payment->payments_official_discount_total) }}</p>
                                    </div>
                                @elseif($column['_key'] == 'payments_official_discount')
                                    <div @if($payment->checkBaseData($base, 'official_discount') == false) style="color: red;" @endif >
                                        <p class="nowrap">{{ getPriceFormat($payment->payments_official_discount) }}</p>
                                    </div>
                                @elseif($column['_key'] == 'payments_informal_discount_total')
                                    <div @if($payment->checkBaseData($base, 'informal_discount') == false) style="color: red;" @endif >
                                        <p class="nowrap">{{ getPriceFormat($payment->payments_informal_discount_total) }}</p>
                                    </div>
                                @elseif($column['_key'] == 'payments_informal_discount')
                                    <div @if($payment->checkBaseData($base, 'informal_discount') == false) style="color: red;" @endif >
                                        <p class="nowrap">{{ getPriceFormat($payment->payments_informal_discount) }}</p>
                                    </div>
                                @elseif($column['_key'] == 'payments_financial_policy_kv_bordereau_total')
                                    <div @if($payment->checkBaseData($base, 'financial_policy_kv_bordereau') == false) style="color: red;" @endif >
                                        <p class="nowrap">{{ getPriceFormat($payment->payments_financial_policy_kv_bordereau_total) }}</p>
                                    </div>
                                @elseif($column['_key'] == 'payments_financial_policy_kv_bordereau')
                                    <div @if($payment->checkBaseData($base, 'financial_policy_kv_bordereau') == false) style="color: red;" @endif >
                                        <p class="nowrap">{{ getPriceFormat($payment->payments_financial_policy_kv_bordereau) }}</p>
                                    </div>
                                @elseif($column['_key'] == 'payments_financial_policy_kv_agent_total')
                                    <div @if($payment->checkBaseData($base, 'financial_policy_kv_agent') == false) style="color: red;" @endif >
                                        <p class="nowrap">{{ getPriceFormat($payment->payments_financial_policy_kv_agent_total) }}</p>
                                    </div>
                                @elseif($column['_key'] == 'payments_financial_policy_kv_dvoy_total')
                                    <div @if($payment->checkBaseData($base, 'financial_policy_kv_dvoy') == false) style="color: red;" @endif >
                                        <p class="nowrap">{{ getPriceFormat($payment->payments_financial_policy_kv_dvoy_total) }}</p>
                                    </div>
                                @elseif($column['_key'] == 'payments_financial_policy_kv_dvoy')
                                    <div @if($payment->checkBaseData($base, 'financial_policy_kv_dvoy') == false) style="color: red;" @endif >
                                        <p class="nowrap">{{ getPriceFormat($payment->payments_financial_policy_kv_dvoy) }}</p>
                                    </div>
                                @elseif($column['_key'] == 'contracts_sign_date' || $column['_key'] == 'contracts_begin_date' || $column['_key'] == 'payments_payment_data' || $column['_key'] == 'contracts_end_date')
                                    {{ getDateFormatRu($payment[$column['_key']]) }}
                                @elseif($column['_key'] == 'contracts_kind_acceptance')
                                    {{\App\Models\Contracts\Contracts::KIND_ACCEPTANCE[$payment[$column['_key']]]}}
                                @elseif($column['_key'] == 'payments_statys_id')
                                    {{\App\Models\Contracts\Payments::STATUS[$payment[$column['_key']]]}}
                                @elseif($column['_key'] == 'payments_bso_receipt')
                                    <p class="nowrap">{{$payment[$column['_key']]}}</p>
                                @elseif($column['_key'] == 'reports_bordero_title')
                                    @if($payment->reports_order_id > 0)
                                        <a href="{{url("/reports/order/{$payment->reports_order_id}/")}}"
                                           target="_blank">{{$payment->reports_bordero_title}}</a>
                                    @endif
                                @elseif($column['_key'] == 'reports_dvoy_title')
                                    @if($payment->reports_dvou_id > 0)
                                        <a href="{{url("/reports/order/{$payment->reports_dvou_id}/")}}"
                                           target="_blank">{{$payment->reports_dvoy_title}}</a>
                                    @endif
                                @elseif($column['_key'] == 'payments_marker_text')
                                    {{$payment->marker_text}}
                                @elseif($column['_key'] == 'contracts_sales_condition')
                                    {{\App\Models\Contracts\Contracts::SALES_CONDITION[$payment[$column['_key']]]}}
                                @else
                                    {{$payment[$column['_key']]}}
                                @endif
                            </td>
                        @endif
                    @endforeach
                </tr>
            @endforeach
        @endif
    @else
        <tr>
            <td colspan="{{ isset($is_reports) ? '31' : '25' }}" class="text-center">Нет платежей</td>
        </tr>
    @endif
    </tbody>
</table>

