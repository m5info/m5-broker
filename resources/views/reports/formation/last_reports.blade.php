@if(sizeof($reports))
    <label style="margin-bottom: 10px;">Последние отчеты:</label>
    <ul data-additional="1">
        @foreach($reports as $key => $report)
            @if($key < 5)
                <li style="margin-left: 20px;"> {{$report->title}} за период ({{getDateFormatRu($report->report_date_start)}}-{{getDateFormatRu($report->report_date_end)}})</li>
            @endif
        @endforeach
    </ul>
@else
    <label>Ни одного отчета еще не создано</label>
@endif