<div class="inline-block total_sum_block_sub">
    <span class="inline-block">
        <label for="">Сумма: </label>
        <span class="bold">{{titleFloatFormat($payments_sql->sum('payments.payment_total'))}} ₽</span>
    </span>
</div>

<div class="inline-block total_sum_block_sub">
    <span class="inline-block">
        <label for="">Оф. скидка: </label>
        <span class="bold">{{titleFloatFormat($payments_sql->sum('payments.official_discount_total'))}} ₽</span>
    </span>
</div>

<div class="inline-block total_sum_block_sub">
    <span class="inline-block">
        <label for="">Неоф. скидка: </label>
        <span class="bold">{{titleFloatFormat($payments_sql->sum('payments.informal_discount_total'))}} ₽</span>
    </span>
</div>

<div class="inline-block total_sum_block_sub">
    <span class="inline-block">
        <label for="">КВ Агента: </label>
        <span class="bold">{{titleFloatFormat($payments_sql->sum('payments.financial_policy_kv_agent_total'))}} ₽</span>
    </span>
</div>

<div class="inline-block total_sum_block_sub">
    <span class="inline-block">
        <label for="">Бордеро КВ %: </label>
        <span class="bold">{{titleFloatFormat($payments_sql->sum('payments.financial_policy_kv_bordereau_total'))}} ₽</span>
    </span>
</div>

<div class="inline-block total_sum_block_sub">
    <span class="inline-block">
        <label for="">ДВОУ КВ %: </label>
        <span class="bold">{{titleFloatFormat($payments_sql->sum('payments.financial_policy_kv_dvoy_total'))}} ₽</span>
    </span>
</div>