<h2>Калькулятор</h2>

<div class="form-group">
    <label class="col-sm-4 ">Сумма</label>
    <label class="col-sm-4 ">Процент</label>
    <label class="col-sm-4 ">Расчет</label>

    <div class="col-sm-4">
        {{ Form::text('calc_sum', titleFloatFormat($calc_sum), ['class' => 'form-control sum', 'id' => 'calc_sum']) }}
    </div>

    <div class="col-sm-4">
        {{ Form::text('calc_procent', titleFloatFormat($calc_procent), ['class' => 'form-control sum', 'id' => 'calc_procent']) }}
    </div>
    <div class="col-sm-4">
        <span class="btn btn-primary pull-left" style="width: 30px;height: 25px;font-size: 10px;" onclick="calcResult()"> = </span>
    </div>


</div>

<div class="row"></div>

<div class="form-group">
    <h2 class="col-sm-6" >Сумма процента</h2>
    <div class="col-sm-6">
        <h2 class="pull-right" id="calc_sum_procent">0</h2>
    </div>
    <div class="row"></div>
    <h2 class="col-sm-6" >Сумма - Процент</h2>
    <div class="col-sm-6">
        <h2 class="pull-right" id="calc_result">0</h2>
    </div>
</div>



<script>

    function calcResult()
    {
        calc_sum = StringToFloat($("#calc_sum").val());
        calc_procent = StringToFloat($("#calc_procent").val());

        if(calc_procent > 0 && calc_sum > 0){

            calc_sum_procent = calc_sum*(calc_procent / 100);
            calc_result = calc_sum - calc_sum_procent;

            $("#calc_sum_procent").html(CommaFormatted(calc_sum_procent));
            $("#calc_result").html(CommaFormatted(calc_result));

        }

    }


</script>