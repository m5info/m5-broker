@extends('layouts.app')

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h1>Экспресс тест</h1>
        </div>
        <div class="block-main">
            <div class="block-sub">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <select class="form-control" id="select1">
                            <option value="1">Чек</option>
                            <option value="2">Квитанция</option>
                            <option value="100">Без подтверждения</option>
                        </select>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label">
                                Текст
                            </label>
                            <input class="form-control text-input" type="text" value="" placeholder="Квитанция = 2"/>
                    </div>

                </div>
            </div>
        </div>


    </div>
@endsection

@section('js')

    <script>

        $(function(){

            $('#slecet1').on('select')

            alert($('#select1').val());

        });


    </script>


@endsection