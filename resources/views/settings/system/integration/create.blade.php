@extends('layouts.frame')

@section('title')

Интеграция

@endsection

@section('content')

{{ Form::model($integration ?? '', ['url' => url("/settings/system/integration"), 'method' => 'post', 'class' => 'form-horizontal']) }}

@include('settings.system.integration.form')

{{Form::close()}}

@endsection

@section('footer')


<button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.add') }}</button>

@endsection

