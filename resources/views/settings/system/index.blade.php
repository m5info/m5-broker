@extends('layouts.app')

@section('content')



<div class="page-heading">
    <h1>{{ trans('menu.system') }}</h1>

</div>

{{ Form::open(['url' => url('/settings/system'), 'method' => 'post', "autocomplete" =>"off", 'files' => true]) }}




<div class="block-main">
    <div class="block-sub">
        <div class="form-horizontal">

            <h4>Базовые настройки</h4>
            <br/>
            <div class="form-group">
                <label class="col-sm-3 control-label">Название</label>
                <div class="col-sm-9">
                    {{ Form::text('base[system_name]', \App\Models\Settings\SettingsSystem::getDataParam('base', 'system_name'), ['class' => 'form-control']) }}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Техподдержка</label>
                <div class="col-sm-9">
                    <div class="row form-horizontal">
                        <div class="col-sm-6">
                            {{ Form::text('base[phone]', \App\Models\Settings\SettingsSystem::getDataParam('base', 'phone'), ['class' => 'form-control phone', 'placeholder'=>'Телефо']) }}
                        </div>
                        <div class="col-sm-6">
                            {{ Form::text('base[email]', \App\Models\Settings\SettingsSystem::getDataParam('base', 'email'), ['class' => 'form-control', 'placeholder'=>'Email']) }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="divider"></div>
            <h4>Интеграция: AV100</h4>
            <br/>

            <div class="form-group">
                <label class="col-sm-3 control-label">URL</label>
                <div class="col-sm-9">
                    {{ Form::text('av100[url]', \App\Models\Settings\SettingsSystem::getDataParam('av100', 'url'), ['class' => 'form-control']) }}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Token/Ключ</label>
                <div class="col-sm-9">
                    {{ Form::textarea('av100[token]', \App\Models\Settings\SettingsSystem::getDataParam('av100', 'token'), ['class' => 'form-control']) }}
                </div>
            </div>

            <div class="divider"></div>
            <h4>Интеграция: M5 старая версия</h4>
            <br/>

            <div class="form-group">
                <label class="col-sm-3 control-label">URL</label>
                <div class="col-sm-9">
                    {{ Form::text('m5_back_old[url]', \App\Models\Settings\SettingsSystem::getDataParam('m5_back_old', 'url'), ['class' => 'form-control']) }}
                </div>
            </div>


            <!-- Интеграции -->


            <div class="divider"></div>

            <h4>Интеграция: Фронт-офис</h4>
            <br/>
            <div class="form-group">
                <label class="col-sm-3 control-label">Протокол</label>
                <div class="col-sm-9">
                    {{ Form::select('front[protocol]', collect(\App\Models\Settings\SettingsSystem::FRONT_PROTOCOL)->prepend('Не выбрано', 0), (int)(\App\Models\Settings\SettingsSystem::getDataParam('front', 'protocol')), ['class' => 'form-control select2-ws', 'required']) }}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">URL</label>
                <div class="col-sm-9">
                    {{ Form::text('front[url]', \App\Models\Settings\SettingsSystem::getDataParam('front', 'url'), ['class' => 'form-control']) }}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Авторизация</label>
                <div class="col-sm-9">
                    <div class="row form-horizontal">
                        <div class="col-sm-6">
                            {{ Form::text('front[login]', \App\Models\Settings\SettingsSystem::getDataParam('front', 'login'), ['class' => 'form-control', 'placeholder'=>'Логин']) }}
                        </div>
                        <div class="col-sm-6">
                            <input name="front[pass]" class="form-control" type="password" value="{{\App\Models\Settings\SettingsSystem::getDataParam('front', 'pass')}}" placeholder="Пароль">
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Token/Ключ</label>
                <div class="col-sm-9">
                    {{ Form::textarea('front[token]', \App\Models\Settings\SettingsSystem::getDataParam('front', 'token'), ['class' => 'form-control']) }}
                </div>
            </div>

            <!-- Интеграции -->


            <div class="divider"></div>
            <style>
                .integration-versions a[aria-expanded="true"]::before {
                    content: '';
                    display: block;
                    position: absolute;
                    right: 20px;
                    font-family: 'Glyphicons Halflings';
                    font-size: 14px;
                }
            </style>

            <h3>Список доступных интеграций</h3>
            <br/>

            @foreach($integrations as $n => $integration)
            <div class="row">
                <div class="col-sm-12">
                    <div class="block-view">
                        <span class="btn btn-primary pull-right" onclick="openFancyBoxFrame('/settings/system/integration/{{$integration->id}}')" style="position:absolute;right:16px;top:12px"><i class="fa fa-edit"></i></span>
                        <h4>{{$integration->title}}</h4>
                        <p>{{$integration->description}}</p>
                        <div style="padding-top:15px;padding-left:30px;padding-bottom:15px;">
                            <h3>Версии интеграции</h3>
                            <ul class="nav nav-tabs integration-versions">
                                @foreach($integration->versions as $i =>$version)
                                <li class="@if ($loop->first)active @endif">
                                    <a data-toggle="tab" href="#panel{{$i}}1">{{$version->title}}</a>

                                </li>
                                @endforeach
                            </ul>
                            <div class="tab-content">
                                @foreach($integration->versions as $i => $version)
                                <div id="panel{{$i}}1" class="tab-pane @if ($loop->first)active @endif" style="padding:15px;">
                                    <p>{{$version->description}}</p>
                                    <br>
                                    <span class="btn btn-primary pull-left" onclick="openFancyBoxFrame('/settings/system/integration/{{$integration->id}}/edit/{{$version->id}}')">Редактировать интеграцию</span>
                                    <span class="btn btn-primary pull-left" style="margin-left:15px;" onclick="openFancyBoxFrame('/settings/system/integration/{{$integration->id}}/edit/{{$version->id}}/main_form')">Редактировать системные данные интеграции</span>
                                </div>
                                @endforeach
                            </div>
                            <div class="form-group" style="padding-top:15px;">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <a href="javascript:void(0);" onclick="openFancyBoxFrame('/settings/system/integration/{{$integration->id}}/add_version')" class="btn btn-primary pull-right">Добавить версию</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="divider"></div>
            @endforeach

            <!-- end -->

            <div class="form-group" style="padding-top:15px;">
                <div class="col-sm-offset-3 col-sm-9">
                    <a href="javascript:void(0);" onclick="openFancyBoxFrame('/settings/system/integration')" class="btn btn-primary pull-right">Добавить интеграцию</a>
                </div>
            </div>


            <br/>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary pull-right">
                        Сохранить
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>


{{ Form::close() }}

@endsection

