@extends('layouts.frame')


@section('title')

    {{ trans('menu.pay_methods') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/settings/pay_methods'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('settings.pay_methods.form')

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
