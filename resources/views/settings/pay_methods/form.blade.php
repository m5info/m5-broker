
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/banks.title') }}</label>
    <div class="col-sm-8">
        {{ Form::text('title', old('banks'), ['class' => 'form-control', 'required']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/banks.is_actual') }}</label>
    <div class="col-sm-8">
        {{ Form::checkbox('is_actual', 1, old('is_actual')) }}
    </div>
</div>


<div class="form-group">
    <label class="col-sm-4 control-label">Тип оплаты</label>
    <div class="col-sm-8">
        {{ Form::select('payment_type', collect(\App\Models\Contracts\Payments::PAYMENT_TYPE), old('payment_type'), ['class' => 'form-control select2-ws']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">Поток оплаты</label>
    <div class="col-sm-8">
        {{ Form::select('payment_flow', collect(\App\Models\Contracts\Payments::PAYMENT_FLOW), old('payment_flow'), ['class' => 'form-control select2-ws']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">Интерфейс</label>
    <div class="col-sm-8">
        {{ Form::select('key_type', collect(\App\Models\Finance\PayMethod::KEY_TYPE), old('key_type'), ['class' => 'form-control select2-ws']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">Видимость в первичке</label>
    <div class="col-sm-8">
        {{ Form::checkbox('show_in_primary', 1, old('show_in_primary')) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">Комиссия</label>
    <div class="col-sm-8">
        {{ Form::text('acquiring', old('acquiring'), ['class' => 'form-control sum']) }}
    </div>
</div>