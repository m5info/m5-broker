@extends('layouts.frame')

@section('title')

    {{ trans('menu.pay_methods') }}
    <span class="btn btn-info pull-right" onclick="openLogEvents('{{$pay_method->id}}', 6, 0)"><i class="fa fa-history"></i> </span>


@endsection

@section('content')

    {{ Form::model($pay_method, ['url' => url("/settings/pay_methods/$pay_method->id"), 'method' => 'put', 'class' => 'form-horizontal']) }}

    @include('settings.pay_methods.form')

    {{Form::close()}}

@endsection

@section('footer')

    <button class="btn btn-danger pull-left" onclick="deleteItem('/settings/pay_methods/', '{{ $pay_method->id }}')">{{ trans('form.buttons.delete') }}</button>

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

