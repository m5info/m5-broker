<div class="notification_body">
    <p>
        <span class="col-lg-12">
            <span class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                @yield('notification')
            </span>

            <span class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                @if($notification->is_read == 0)
                    <span class="btn-xs btn-right fa fa-times-circle span-x-notif" data-notification="{{$notification->id}}"></span>
                @endif
            </span>

        </span>
    </p>
</div>
