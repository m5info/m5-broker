@extends('settings.notifications.layout')

@section('notification')
    Пользователь {{ $log->user ? $log->user->name : "" }} отказался от
    <a href="/orders/edit/{{$log->contract_id}}">заявки</a>
@endsection