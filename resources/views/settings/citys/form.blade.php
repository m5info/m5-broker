
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/banks.title') }}</label>
    <div class="col-sm-8">
        {{ Form::text('title', old('title'), ['class' => 'form-control', 'id' => 'city_address', 'required']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/banks.is_actual') }}</label>
    <div class="col-sm-8">
        {{ Form::checkbox('is_actual', 1, old('is_actual')) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">КЛАДР</label>
    <div class="col-sm-8">
        {{ Form::text('kladr', old('kladr'), ['class' => 'form-control', 'id' => 'address_kladr', 'required']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">Широта</label>
    <div class="col-sm-8">
        {{ Form::text('geo_lat', old('geo_lat'), ['class' => 'form-control', 'id' => 'geo_lat', 'required']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">Долгота</label>
    <div class="col-sm-8">
        {{ Form::text('geo_lon', old('geo_lon'), ['class' => 'form-control', 'id' => 'geo_lon', 'required']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">Тариф</label>
    <div class="col-sm-8">
        {{ Form::text('rate', old('rate'), ['class' => 'form-control sum', 'required']) }}
    </div>
</div>



