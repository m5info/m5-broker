@extends('layouts.frame')


@section('title')

    {{ trans('menu.citys') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/settings/citys'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('settings.citys.form')

    {{Form::close()}}


@endsection

@section('js')
    <script>

        $(function(){

            $('#city_address').suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "ADDRESS",
                bounds: "city",
                constraints: {
                    label: "",
                    locations: { city_type_full: "город" }
                },
                count: 5,
                onSelect: function (suggestion) {

                    key = $(this).data('key');
                    $('#city_address').val($(this).val());
                    $('#address_kladr').val(suggestion.data.city_kladr_id);
                    $('#geo_lat').val(suggestion.data.geo_lat);
                    $('#geo_lon').val(suggestion.data.geo_lon);
                }
            });
        })

    </script>

@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
