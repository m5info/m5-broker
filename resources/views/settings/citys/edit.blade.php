@extends('layouts.frame')

@section('title')

    {{ trans('menu.citys') }}
    <span class="btn btn-info pull-right" onclick="openLogEvents('{{$city->id}}', 2, 0)"><i class="fa fa-history"></i> </span>

@endsection

@section('content')

    {{ Form::model($city, ['url' => url("/settings/citys/$city->id"), 'method' => 'put', 'class' => 'form-horizontal']) }}

    @include('settings.citys.form')

    {{Form::close()}}

@endsection

@section('footer')

    <button class="btn btn-danger pull-left" onclick="deleteItem('/settings/citys/', '{{ $city->id }}')">{{ trans('form.buttons.delete') }}</button>

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection


@section('js')
    <script>

        $(function(){

            $('#city_address').suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "ADDRESS",
                bounds: "city",
                constraints: {
                    label: "",
                    locations: { city_type_full: "город" }
                },
                count: 5,
                onSelect: function (suggestion) {

                    key = $(this).data('key');
                    $('#city_address').val($(this).val());
                    $('#address_kladr').val(suggestion.data.city_kladr_id);
                    $('#geo_lat').val(suggestion.data.geo_lat);
                    $('#geo_lon').val(suggestion.data.geo_lon);
                }
            });
        })

    </script>

@endsection
