@extends('layouts.frame')

@section('title')

    {{ trans('menu.departures_courier_price') }}
    <span class="btn btn-info pull-right" onclick="openLogEvents('{{$departure->id}}', 6, 0)"><i class="fa fa-history"></i> </span>


@endsection

@section('content')

    {{ Form::model($departure, ['url' => url("/settings/departures_courier_price/$departure->id"), 'method' => 'put', 'class' => 'form-horizontal']) }}

    @include('settings.departures_courier_price.form')

    {{Form::close()}}

@endsection

@section('footer')

    <button class="btn btn-danger pull-left" onclick="deleteItem('/settings/departures_courier_price/', '{{ $departure->id }}')">{{ trans('form.buttons.delete') }}</button>

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

