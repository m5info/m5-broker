
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/banks.title') }}</label>
    <div class="col-sm-8">
        {{ Form::text('title', old('title'), ['class' => 'form-control', 'required']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/banks.is_actual') }}</label>
    <div class="col-sm-8">
        {{ Form::checkbox('is_actual', 1, old('is_actual')) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">Город</label>
    <div class="col-sm-8">
        {{Form::select('city_id', \App\Models\Settings\City::where('is_actual', '=', '1')->get()->pluck('title','id')->prepend('Все','0'), (isset($departure)?$departure->city_id:0), ['class' => 'form-control select2-ws'])}}

    </div>
</div>


<div class="form-group">
    <label class="col-sm-4 control-label">Цена</label>
    <div class="col-sm-8">
        {{ Form::text('price_total', (isset($departure)?titleFloatFormat($departure->price_total):''), ['class' => 'form-control sum']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">Выставить долг</label>
    <div class="col-sm-8">
        {{ Form::checkbox('is_debt', 1, old('is_debt')) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">Долг агенту</label>
    <div class="col-sm-8">
        {{ Form::text('debt_total', (isset($departure)?titleFloatFormat($departure->debt_total):''), ['class' => 'form-control sum']) }}
    </div>
</div>