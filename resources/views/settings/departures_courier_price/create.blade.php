@extends('layouts.frame')


@section('title')

    {{ trans('menu.departures_courier_price') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/settings/departures_courier_price'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('settings.departures_courier_price.form')

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
