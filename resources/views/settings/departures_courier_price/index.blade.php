@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">{{ trans('menu.departures_courier_price') }}</h1>
        <span class="btn btn-primary btn-right" onclick="openFancyBoxFrame('{{ url('/settings/departures_courier_price/create')  }}')">
            {{ trans('form.buttons.create') }}
        </span>
    </div>

    @if(sizeof($departures))
        <table class="tov-table">
            <thead>
                <tr>
                    <th><a href="javascript:void(0);">{{ trans('settings/banks.title') }}</a></th>
                    <th><a href="javascript:void(0);">{{ trans('settings/banks.is_actual') }}</a></th>
                    <th><a href="javascript:void(0);">Город</a></th>
                    <th><a href="javascript:void(0);">Цена</a></th>
                    <th><a href="javascript:void(0);">Выставить долг</a></th>
                    <th><a href="javascript:void(0);">Долг агенту</a></th>
                </tr>
            </thead>
            @foreach($departures as $departure)
                <tr onclick="openFancyBoxFrame('{{ url("/settings/departures_courier_price/$departure->id/edit") }}')">
                    <td>{{ $departure->title }}</td>
                    <td>{{ ($departure->is_actual==1)? trans('form.yes') :trans('form.no') }}</td>

                    <td>{{isset($departure->city)?$departure->city->title:''}}</td>
                    <td>{{ titleFloatFormat($departure->price_total) }}</td>

                    <td>{{ ($departure->is_debt==1)? trans('form.yes') :trans('form.no') }}</td>

                    <td>{{ titleFloatFormat($departure->debt_total) }}</td>

                </tr>
            @endforeach

        </table>
    @else
        {{ trans('form.empty') }}
    @endif

@endsection
