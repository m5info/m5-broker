@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">{{ trans('menu.installment_algorithms_list') }}</h1>
        <span class="btn btn-primary btn-right" onclick="openFancyBoxFrame('{{ url('/settings/installment_algorithms_list/create')  }}')">
            {{ trans('form.buttons.create') }}
        </span>
    </div>

    @if(sizeof($algoritms))
        <table class="tov-table">
            <thead>
                <tr>
                    <th><a href="javascript:void(0);">{{ trans('settings/installment_algorithms_list.title') }}</a></th>
                    <th><a href="javascript:void(0);">{{ trans('settings/installment_algorithms_list.quantity') }}</a></th>
                </tr>
            </thead>
            @foreach($algoritms as $algoritm)
                <tr onclick="openFancyBoxFrame('{{ url("/settings/installment_algorithms_list/$algoritm->id/edit") }}')">
                    <td>{{ $algoritm->title }}</td>
                    <td>{{ $algoritm->quantity }}</td>
                </tr>
            @endforeach

        </table>
    @else
        {{ trans('form.empty') }}
    @endif

@endsection
