@extends('layouts.frame')

@section('title')

    {{ trans('menu.installment_algorithms_list') }}
    <span class="btn btn-info pull-right" onclick="openLogEvents('{{$algoritm->id}}', 6, 0)"><i class="fa fa-history"></i> </span>


@endsection

@section('content')

    {{ Form::model($algoritm, ['url' => url("/settings/installment_algorithms_list/$algoritm->id"), 'method' => 'put', 'class' => 'form-horizontal']) }}

    @include('settings.installment_algorithms_list.form')

    {{Form::close()}}

@endsection

@section('footer')

    <button class="btn btn-danger pull-left" onclick="deleteItem('/settings/installment_algorithms_list/', '{{ $algoritm->id }}')">{{ trans('form.buttons.delete') }}</button>

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

