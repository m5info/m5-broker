@extends('layouts.frame')


@section('title')

    {{ trans('menu.installment_algorithms_list') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/settings/installment_algorithms_list'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('settings.installment_algorithms_list.form')

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
