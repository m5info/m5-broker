
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/installment_algorithms_list.title') }}</label>
    <div class="col-sm-8">
        {{ Form::text('title', old('installment_algorithms_list'), ['class' => 'form-control', 'required']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">Количество</label>
    <div class="col-sm-8">
        {{ Form::text("quantity", old('installment_algorithms_list'), ['class' => 'form-control']) }}
    </div>
</div>

