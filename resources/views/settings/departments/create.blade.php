@extends('layouts.frame')


@section('title')

    {{ trans('menu.departments') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/settings/departments'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('settings.departments.form')

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection


