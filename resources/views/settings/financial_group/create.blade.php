@extends('layouts.frame')


@section('title')

    {{ trans('menu.financial_policy') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/settings/financial_policy'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('settings.financial_group.form')

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
