@extends('layouts.frame')
@section('head')
    <style>
        .panel{
            border-color: #faebcc;
            border-width: 1px;
            border-style: solid;
            margin-bottom: 20px;
        }

        .panel>.panel-heading{
            color: #8a6d3b;
            background-color: #fcf8e3;
            border-color: #faebcc;
            padding: 20px;
        }

        .panel>.panel-body{
            padding: 20px;
        }

    </style>
@endsection

@section('title')

    {{ trans('menu.financial_policy') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/settings/financial_policy/copy'), 'method' => 'post', 'class' => 'form-horizontal']) }}


    <div class="panel panel-warning">
        <div class="panel-heading">
            Будьте внимательны!
        </div>
        <div class="panel-body">
            Данный функционал копирует в каждую страховую, в каждого поставщика БСО, в каждой финансовой политике одну группу в другую.
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Из какой</label>
        <div class="col-sm-8">
            {{ Form::select('from', $fp, 0, ['class' => 'form-control', 'select2']) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">В какую</label>
        <div class="col-sm-8">
            {{ Form::select('to', $fp, 0, ['class' => 'form-control', 'select2']) }}
        </div>
    </div>


    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="_submitForm()" type="submit" id="submit_button" class="btn btn-danger">Выполнить</button>

@endsection

@section('js')
    @parent
    <script>
        function _submitForm(){
            if ($('[name="to"]').val() == 0 || $('[name="from"]').val() == 0 ){

                alert('Заполните поля!');
            }else{
                $('#submit_button').attr('disabled', 'disabled');
                submitForm();
            }
        }
    </script>
@endsection
