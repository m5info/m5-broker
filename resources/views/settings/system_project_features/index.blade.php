@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">Системные фичи</h1>
    </div>
    <div class="block-main">
        <div class="block-sub">
            <div class="form-horizontal">
                <div class="row">
                    <div class="col-sm-3">

                        @if($localhost)
                            <label for="">Функционал не доступен на локальной версии</label>
                        @else
                            <label for="clear_cache">Пароль</label>
                            <input type="password" name="password" class="form-control">

                            <div class="form-group margin-side-0" style="margin-top: 35px;">
                                <label for="clear_cache">Очищает весь кэш в приложении</label>
                                <a class="btn btn-primary system_action pull-right" data-title="uc">Выполнить</a>
                            </div>

                            <div class="form-group margin-side-0">
                                <label for="clear_cache">Залить обновления и миграции</label>
                                <a class="btn btn-primary system_action pull-right" data-title="ud">Выполнить</a>
                            </div>

                            <div class="form-group margin-side-0">
                                <label for="clear_cache">Ветка <code>program</code> | Синхронизация</label>
                                <a class="btn btn-primary system_action pull-right" data-title="program">Выполнить</a>
                            </div>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h3>Последнее событие</h3>
    <div class="form-horizontal">
        <div class="row">
            <div class="col-sm-12">
                <pre id="log">Ожидается действие..</pre>
            </div>
        </div>
    </div>

    <div id="table"></div>

@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('.system_action').click(function (e) {
                $('.system_action').attr('disabled', 'disabled');
                var action = $(this).data('title');
                var password = $('[name = "password"]').val();
                $.post('/settings/system_project_features',
                    {action: action, password: password}).done(function (res) {
                    if (res.error) {
                        $('#log').html('<b>' + res.time + ' </b><code style="color:red;font-weight:600;">' + res.log + '</code>');
                    } else {
                        $('#log').html('<b>' + res.time + ' </b><code>' + res.log + '</code>');
                    }

                    $('.system_action').removeAttr('disabled');
                })
            });
        });
    </script>
@endsection

