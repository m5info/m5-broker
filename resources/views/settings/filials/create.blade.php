@extends('layouts.frame')


@section('title')

    {{ trans('menu.filials') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/settings/filials'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('settings.filials.form')

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection


