@extends('layouts.frame')


@section('title')

    {{ trans('menu.banks') }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/settings/banks'), 'method' => 'post', 'class' => 'form-horizontal']) }}

    @include('settings.banks.form')

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection
