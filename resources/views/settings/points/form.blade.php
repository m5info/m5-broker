
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/banks.title') }}</label>
    <div class="col-sm-8">
        {{ Form::text('title', old('title'), ['class' => 'form-control', 'required']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">Город</label>
    <div class="col-sm-8">
        {{ Form::select('city_id', \App\Models\Settings\City::where('is_actual', '=', '1')->get()->pluck('title', 'id'), old('city_id'), ['class' => 'form-control select2-ws', 'required']) }}

    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">{{ trans('settings/banks.is_actual') }}</label>
    <div class="col-sm-8">
        {{ Form::checkbox('is_actual', 1, old('is_actual')) }}
    </div>
</div>


