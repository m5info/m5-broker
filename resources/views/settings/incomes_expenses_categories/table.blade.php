<table class="tov-table">
    <thead>
        <tr>
            <th><a href="javascript:void(0);">{{ trans('settings/incomes_expenses_categories.title') }}</a></th>
            <th><a href="javascript:void(0);">{{ trans('settings/incomes_expenses_categories.is_actual') }}</a></th>
            <th><a href="javascript:void(0);">{{ trans('settings/incomes_expenses_categories.type') }}</a></th>
            <th><a href="javascript:void(0);">Учет в учредительских выплатах</a></th>
        </tr>
    </thead>
    @if(sizeof($incomes_expenses_categories))
        @foreach($incomes_expenses_categories as $income_expense_category)
            <tr onclick="openFancyBoxFrame('{{ url("/settings/incomes_expenses_categories/$income_expense_category->id/edit") }}')">
                <td>{{ $income_expense_category->title }}</td>
                <td>{{ ($income_expense_category->is_actual==1)? trans('form.yes') :trans('form.no') }}</td>
                <td>{{ $income_expense_category->type_ru('type') }}</td>
                <td>{{ ($income_expense_category->counting_in_foundation_payments==1) ? trans('form.yes') : trans('form.no') }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="3" class="text-center">Пока не добавлено ни одной категории</td>
        </tr>
    @endif
</table>
