@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">Действия</h1>
        <div style="float: right" >
            <label for="for_administrator">Для администратора</label>
            {{ Form::checkbox('for_administrator', 1, true, ['class' => '', 'onchange' => 'loadItems()']) }}
        </div>
    </div>

    <div class="form-horizontal col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="filters-wrapper">
            <div class="row">
                <div class="col-md-3">
                    {{Form::label('table', 'Таблица')}}
                    {{Form::select('table', collect($tables)->prepend('Не выбрано', 0), 0, ['class' => 'form-control select2-all select2-ws'/*, 'onchange' => 'loadContextId()'*/] )}}
                </div>
                <div class="col-md-3">
                    {{Form::label('table', 'Событие')}}
                    {{Form::select('event', collect(\App\Models\Log\CUDLogs::EVENT_NAME)->prepend('Не выбрано', 0), 0, ['class' => 'form-control select2-all select2-ws', 'onchange' => 'loadItems()'] )}}
                </div>
                <div class="col-md-3">
                    {{Form::label('table', 'Пользователь')}}
                    {{Form::select('user', collect(\App\Models\User::all())->pluck('name','id')->prepend('Не выбрано', 0), 0, ['class' => 'form-control select2-all select2-ws', 'onchange' => 'loadItems()'] )}}
                </div>
                <div class="col-md-3">
                    {{Form::label('table', 'Роль')}}
                    {{Form::select('role', collect(\App\Models\Users\Role::all())->pluck('title','id')->prepend('Не выбрано', 0), 0, ['class' => 'form-control select2-all select2-ws', 'onchange' => 'loadItems()'] )}}
                </div>
                <div class="col-md-3">
                    {{Form::label('table', 'Контекстное ID')}}
                    {{Form::text('context_id', 0, ['class' => 'form-control', 'onchange' => 'loadItems()'])}}
                </div>
                <div class="col-md-3" >
                    <label class="control-label">Дата от</label>
                    {{ Form::text('date_from', '', ['class' => 'form-control datepicker', 'onchange' => 'loadItems()']) }}
                </div>

                <div class="col-md-3">
                    <label class="control-label">Дата по</label>
                    {{ Form::text('date_to', '', ['class' => 'form-control datepicker', 'onchange' => 'loadItems()']) }}
                </div>

                <div class="col-md-3">
                    <label class="control-label">Лимит</label>
                    {{ Form::text('limit', 50, ['class' => 'form-control', 'onchange' => 'loadItems()']) }}
                </div>
                <div class="col-md-3 add-filter filter-contracts filter-bso_items" style="display: none">
                    <label class="control-label">БСО</label>
                    {{ Form::text('bso_title', '', ['class' => 'form-control', 'onchange' => 'loadItems()', 'id' => 'bso_title']) }}
                </div>
            </div>
        </div>
    </div>

    <div id="table-wrapper">

    </div>


@endsection

@section('js')

    <script>
        var bso_used = [];
        var table = '';

        $(document).ready(function(){
            // loaderShow();
            // loadItems();

            activSearchBsoSold("bso_title", '', 1, bso_used, [0, 1, 2, 3, 4, 5]);

            @if(request()->has('change_table'))
                $('[name="table"]').val('{{ request('change_table') }}').change();
            @endif
            @if(request()->has('change_id'))
                $('[name="context_id"]').val('{{ request('change_id') }}').change();
            @endif
        });

        function selectBso(object_id, key, type, suggestion)
        {
            var data = suggestion.data;
            var context_id = 0;

            if (table == 'contracts'){
                context_id = data.contract.id;
            }else if(table == 'bso_items'){
                context_id = data.bso_id;
            }

            $('[name="context_id"]').val(context_id).trigger('change');
        }

        $(document).on('change', '[name="table"]', function () {
            table = $(this).val();

            $('.add-filter').hide();
            $('.filter-'+table).show();

            $('[name="bso_title"]').val('').trigger('change');
        });

        function loadContextId()
        {
            loadItems();
            $('[name="context_id"]').html('');
            if($('[name="table"]').val() !== '0'){
                $.get('/logging/actions/get_context_id', getData(), function(res){
                    $('[name="context_id"]').append("<option value='0'>Не выбрано</option>")
                    $.each(res, function(key, model){
                        $('[name="context_id"]').append("<option value='"+model.context_id+"'>"+model.context_id+"</option>")
                    });
                    $('[name="context_id"]').removeAttr('disabled');
                });
            }else{
                $('[name="context_id"]')
                    .html("<option value='0'>Не выбрано</option>")
                    .change()
                    .attr('disabled', 'disabled');
                loadItems();
            }
        }

        function loadItems()
        {
            $.get('/logging/actions/get_table', getData(), function(data){
                $('#table-wrapper').html(data);
                loaderHide();
            })
        }

        function getData()
        {
            return {
                table: $('[name="table"]').val(),
                event: $('[name="event"]').val(),
                user: $('[name="user"]').val(),
                role: $('[name="role"]').val(),
                context_id: $('[name="context_id"]').val(),
                date_from: $('[name="date_from"]').val(),
                date_to: $('[name="date_to"]').val(),
                limit: $('[name="limit"]').val(),
                bso_title: $('[name="bso_title"]').val(),
                for_administrator: $('[name="for_administrator"]').prop('checked') ? 1 : 0,
            }
        }
    </script>

@endsection




