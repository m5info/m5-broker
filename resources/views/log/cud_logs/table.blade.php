<style>
    .span-event{
        padding: 10px 20px;
        border: 1px solid black;
        color: black;
        display: inline-block;
    }
    .log_item{
        padding: 1px;
    }

    .span-date{
        padding: 10px 20px !important;
        color: white;
        border-radius: 20px;
        background-color: black;
        font-weight: bolder;
    }
</style>
<table class="tov-table">
    <thead>
    <tr>
        <th><a href="javascript:void(0);">#</a></th>
{{--        <th><a href="javascript:void(0);">Таблица</a></th>--}}
        <th><a href="javascript:void(0);">Контекстное ID</a></th>
        <th><a href="javascript:void(0);">Пользователь</a></th>
        <th><a href="javascript:void(0);">Роль</a></th>
        <th style="text-align: center"><a href="javascript:void(0);">URL</a></th>
        <th style="text-align: center"><a href="javascript:void(0);">Событие</a></th>
        @if(!$for_administrator)
            <th style="text-align: center"><a href="javascript:void(0);">До обновления</a></th>
        @endif
        <th style="text-align: center"><a href="javascript:void(0);">Было обновлено</a></th>
        <th style="text-align: center"><a href="javascript:void(0);">Время изменения</a></th>
    </tr>
    </thead>
    <tbody>
        @if($logs->count() > 0)
            @foreach($logs as $log)
                <tr>
                    <td>{{ $log->id }}</td>
{{--                    <td>{{ $log->table }}</td>--}}
                    <td>{{ $log->context_id }}</td>
                    <td>{{ $log->user_name }}</td>
                    <td>{{ $log->role_name }}</td>
                    <td>
                        <p class="log_item">
                            <code>{{ $log->from_url }}</code>
                        </p>
                    </td>
                    <td>
                        <span class="span-event" style="background-color: {{\App\Models\Log\CUDLogs::EVENT_COLOR[$log->event]}}">
                            {{ isset(\App\Models\Log\CUDLogs::EVENT_NAME[$log->event]) ? \App\Models\Log\CUDLogs::EVENT_NAME[$log->event] : '' }}
                        </span>
                    </td>
                    @if(!$for_administrator)
                        <td>
                            @if(!empty($log->old_data))
                                @foreach(json_decode($log->old_data) as $column => $value)
                                    <p class="log_item">
                                        <code>{{$column}}</code>: <span>{{$value}}</span>
                                    </p>
                                @endforeach
                            @endif
                        </td>
                    @endif
                    <td>
                        @if(!empty($log->new_data))
                            @foreach(json_decode($log->new_data) as $column => $value)
                                <p class="log_item">
                                    @if($for_administrator)
                                        <code>{{\App\Models\Log\CUDLogs::rus_column($log->table, $column)}}</code>: <span>{{\App\Models\Log\CUDLogs::rus_value($log->table, $column, $value)}}</span>
                                    @else
                                        <code>{{$column}}</code>: <span>{{$value}}</span>
                                    @endif
                                </p>
                            @endforeach
                        @endif
                    </td>
                    <td nowrap>
                        <span class="span-date">
                            {{$log->created}}
                        </span>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td>
                    Записей не найдено
                </td>
            </tr>
        @endif
    </tbody>
</table>
