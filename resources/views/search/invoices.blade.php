@extends('layouts.app')

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h2>Результат поиска</h2>

        </div>
        <div class="block-main" style="border: none">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        @if(isset($errors))
                            {{ $errors }}
                        @else
                            @include('_chunks._vue_table')
                            <div id="tov-table">
                            <table class="tov-table table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Номер</th>
                                    <th>Юр. лицо</th>
                                    <th>Тип счёта</th>
                                    <th>Агент</th>
                                    <th>Дата создания</th>
                                    <th>Статус</th>
                                    <th>Кол-во платежей</th>
                                    <th>Общая сумма</th>
                                    <th></th>
                                </tr>
                                </thead>
                                @foreach($invoices as $invoice)
                                    <tr class="clickable-row" data-href="{{url("/cashbox/invoice/{$invoice->invoices_id}/edit")}}">
                                        <td>
                                            <div id="bso_tooltip_{{ $invoice->invoices_id }}" style="padding: 10px; background-color: #eeeeed;display:block; position: absolute;border: #c3c3c3 1px solid;display: none">
                                                @php
                                                    foreach($invoice->payments as $payment){

                                                        $bso_title = $payment->bso ? $payment->bso->bso_title : "";
                                                        $bso_product = $payment->bso && $payment->bso->product ? $payment->bso->product->title : "";

                                                        if(($payment->contract->kind_acceptance) == 1){
                                                            echo "<a href=".url('bso/items', $payment->bso->id)." target='_blank'>$bso_title</a> $bso_product<br>";
                                                        }else{
                                                            echo "<span title='Не безусловный'>$bso_title</span> $bso_product<br>";
                                                        }
                                                    }
                                                @endphp
                                            </div>
                                            {{ $invoice->invoices_id }}
                                        </td>
                                        <td>{{ $invoice->org_title }}</td>
                                        <td>{{ \App\Models\Finance\Invoice::TYPES[$invoice->invoice_type] }}</td>
                                        <td>{{ $invoice->agent_name }}</td>
                                        <td>{{ getDateFormatRu($invoice->invoices_created_at) }}</td>
                                        <td>{{ \App\Models\Finance\Invoice::STATUSES[$invoice->invoices_status_id]}}</td>
                                        <td>{{ $invoice->count_payments }}</td>
                                        <td>{{ getPriceFormat($invoice->total_payments_sum) }}</td>
                                        <td>
                                            <a href="{{url("/cashbox/invoice/{$invoice->invoices_id}/edit")}}" class="btn btn-success">Открыть</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            </div>
                        @endif
                    </div>
                </div>

        </div>


    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $('#table_vue').html('');

            var result_table = document.getElementById("tov-table").innerHTML;

            $('#table_vue').append(result_table);
            document.getElementById("tov-table").remove();

            $(".clickable-row").click( function(){
                if ($(this).attr('data-href')) {
                    location.href = $(this).attr('data-href')
                }
            });

        });
    </script>
@endsection