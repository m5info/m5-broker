@extends('layouts.app')

@section('content')

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-subheading">
            <h2>Результат поиска</h2>

        </div>
        <div class="block-main" style="border: none">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        @if(isset($errors))
                            {{ $errors }}
                        @else
                            @include('_chunks._vue_table')
                            <div id="tov-table">
                            <table class="tov-table table table-bordered">
                                <tr>
                                    <th>СК</th>
                                    <th>Тип</th>
                                    <th>Событие</th>
                                    <th>Статус</th>

                                    <th>БСО</th>
                                    <th>Страхователь</th>

                                    <th>Агент</th>
                                    <th>Менеджер</th>
                                    <th>Руководиитель</th>

                                    <th>Точка продаж</th>
                                    <th>Номер бланка</th>
                                    <th>Принят в организацию</th>
                                    <th>Дата последней операции</th>


                                    <th></th>
                                </tr>

                                <tbody>

                                @foreach($bsos as $bso)
                                    <tr>
                                        <td>{{(!empty($bso->supplier)) ? $bso->supplier->title : ''}}</td>
                                        <td>{{(!empty($bso->type->title)) ? $bso->type->title : ''}}</td>

                                        <td>{{$bso->location ? $bso->location->title : ""}}</td>
                                        <td>
                                            @if($bso->is_reserved == 1)
                                                Резерв
                                            @else
                                                {{$bso->state ? $bso->state->title : ""}}
                                            @endif
                                        </td>

                                        <td>{{$bso->bso_title}}</td>
                                        <td>
                                            @if(isset($bso->contract) && isset($bso->contract->id))
                                                {{$bso->contract->getInsurer()}}
                                            @endif
                                        </td>
                                        <td>
                                            {{($bso->agent)?$bso->agent->name:''}}
                                        </td>

                                        <td>
                                            @if(isset($bso->contract) && isset($bso->contract->manager))
                                                {{($bso->contract->manager)?$bso->contract->manager->name:''}}
                                            @endif

                                        </td>

                                        <td>
                                            @if(isset($bso->contract) && $bso->contract->statys_id > 0 && $bso->contract->parent())
                                                {{($bso->contract->parent()->name)}}
                                            @endif
                                        </td>

                                        <td>{{$bso->point_sale ? $bso->point_sale->title : ""}}</td>
                                        <td>{{$bso->bso_blank_title ? $bso->bso_blank_title : ""}}</td>

                                        <td>{{setDateTimeFormatRu($bso->logs()->get()->first() ? $bso->logs()->get()->first()->log_time : '')}}</td>
                                        <td>{{setDateTimeFormatRu($bso->logs()->get()->last() ? $bso->logs()->get()->last()->log_time : '')}}</td>

                                        <td>
                                            @if(auth()->user()->hasPermission('bso', 'items'))
                                                <a href="/bso/items/{{$bso->id}}/" class="btn btn-success">Открыть</a>
                                            @elseif(isset($bso->contract->id))
                                                @if(auth()->user()->is('underfiller'))
                                                    <a href="/contracts/underfillers/edit/{{$bso->contract->id}}/"
                                                       class="btn btn-success">Открыть</a>

                                                @else

                                                    <a href="/contracts/temp_contracts/contract/{{$bso->contract->id}}/edit/"
                                                       class="btn btn-success">Открыть</a>


                                                @endif
                                            @endif
                                        </td>


                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            </div>
                        @endif
                    </div>
                </div>

        </div>


    </div>

@endsection

@section('js')
<script>
    $(function () {
        $('#table_vue').html('');

        var result_table = document.getElementById("tov-table").innerHTML;

        $('#table_vue').append(result_table);
        document.getElementById("tov-table").remove();


        $(".clickable-row").click( function(){
            if ($(this).attr('data-href')) {
                location.href = $(this).attr('data-href')
            }
        });

    });
</script>
@endsection