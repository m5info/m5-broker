@extends('layouts.app')

@section('content')

    <h2 style="margin-bottom: 20px;">Создание новости</h2>
    <div class="row">
        <div class="col-sm-12">
            <form>
                @csrf
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" class="form-control" name="header">
                </div>
                <div class="form-group">
                    <label>Краткое описание</label>
                    <input type="text" class="form-control" name="short_desc">
                </div>
                <div class="form-group">
                    <label>Видимость</label>
                    {{Form::select('visibility', collect(\App\Models\Informing\News\News::VISIBILITY), 1, ['class' => 'form-control'] )}}
                </div>
                <div class="form-group">
                    <label>Включить уведомление?</label>
                    {{Form::checkbox('notify', 1, 1, ['class' => 'checkbox-close-inline'])}}
                </div>
                <div class="form-group">
                    <label>Содержимое</label><br>
                    <textarea id="content" type="text" class="form-control" name="content">
                    </textarea>
                </div>
                <button class="btn btn-success pull-right" id="save">Сохранить</button>
            </form>
        </div>
    </div>

@endsection

@section('js')

    <script src="/plugins/ckeditor/ckeditor.js"></script>
    <script>

        $(function () {
            CKEDITOR.replace('content')
        });

        $(document).on('click', '#save', function (e) {
            e.preventDefault();

            $.post({
                url: '/informing/edit-news/create',
                method: 'post',
                data: {
                    content: CKEDITOR.instances.content.getData(),
                    short_desc: $('[name = "short_desc"]').val(),
                    header: $('[name = "header"]').val(),
                    visibility: $('[name = "visibility"]').val(),
                    notify: $('[name = "notify"]').prop("checked") === true ? 1 : 0
                },
                success:function(res){
                    flashMessage('success', 'Новость создана');
                    setTimeout(function(){
                        location.href = '/informing/edit-news/';
                    }, 1500);
                }
            });
        });

    </script>


@endsection