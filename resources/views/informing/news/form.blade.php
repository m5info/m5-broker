@extends('layouts.app')

@section('content')

    <h2 style="margin-bottom: 20px;">Редактирование новости #{{$new->id}}
        <button class="btn btn-danger pull-right" onclick="delete_new({{$new->id}});">Удалить</button>
    </h2>
    <div class="row">
        <div class="col-sm-6">
            <form action="/news/{{$new->id}}/save">
                @csrf
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" class="form-control" name="header" value="{{$new->header}}">
                </div>
                <div class="form-group">
                    <label>Краткое описание</label>
                    <input type="text" class="form-control" name="short_desc" value="{{$new->short_desc}}">
                </div>
                <div class="form-group">
                    <label>Видимость</label>
                    {{Form::select('visibility', collect(\App\Models\Informing\News\News::VISIBILITY), $new->visibility ? $new->visibility : 1, ['class' => 'form-control'] )}}
                </div>
                <div class="form-group">
                    <label>Уведомить вновь?</label>
                    {{Form::checkbox('notify', 1, 1, ['class' => 'checkbox-close-inline'])}}
                </div>


                <div class="form-group">
                    <label>Содержимое</label><br>
                    <textarea id="content" type="text" class="form-control" name="content">
                        {{$new->content}}
                    </textarea>
                </div>
                <button class="btn btn-success pull-right" id="save">Сохранить</button>
            </form>
        </div>
        <div class="col-sm-6">
            @include('informing.news.files', [
                        'new' => $new
                    ])
        </div>
    </div>

@endsection

@section('js')

    <script src="/plugins/ckeditor/ckeditor.js"></script>
    <script>

        $(function () {
            CKEDITOR.replace('content')
        });



        function delete_new(id) {
            answer = confirm('Удалить новость ' + id);
            if (answer) {
                $.post({
                    url: '/informing/edit-news/{{$new->id}}/delete',
                    success: function (res) {
                        flashMessage('danger', 'Новость удалена');
                        setTimeout(function () {
                            location.href = '/informing/edit-news/'
                        }, 1500);
                    }
                })
            }
        }

        $(document).on('click', '#save', function (e) {
            e.preventDefault();


            $.post({
                url: '/informing/edit-news/{{$new->id}}/update',
                data: {
                    visibility: $('[name = "visibility"]').val(),
                    content: CKEDITOR.instances.content.getData(),
                    short_desc: $('[name = "short_desc"]').val(),
                    header: $('[name = "header"]').val(),
                    notify: $('[name = "notify"]').prop("checked") === true ? 1 : 0
                },
                success: function (res) {
                    flashMessage('success', 'Данные обновлены!');
                    setTimeout(function () {
                        //location.reload();
                    }, 1000);
                }
            });
        });


        console.log(text);
    </script>


@endsection