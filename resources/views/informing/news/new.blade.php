@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="_container_news">
                <h2 class="img-header">{{$new->header}} <span style="font-size: 15px; font-weight: 800;"><i
                                class="ico-time" style="height: 12px; width: 12px;"></i>{{setDateTimeFormatRu($new->created_at)}}</span>
                </h2>
                <div class="img-content" style="margin-top: 15px;">
                    {!! $new->content !!}
                </div>
                <hr>
                <div class="img-file" style="float: left;">
                    @if($new->docs)
                        @foreach($new->docs as $file)
                            @if (in_array($file->ext, ['jpg', 'jpeg', 'png', 'gif']))
                                <a href="{{ $file->url }}" target="_blank" style="display: inline-block; margin-right: 15px;">
                                    <img class="media-object preview-image"
                                         src="{{ $file->preview }}"
                                         onerror="this.onerror=null;this.src='/images/extensions/unknown.png';">
                                    <p style="text-align: center;">Открыть в новом окне</p>
                                </a>
                                @elseif(in_array($file->ext, ['docx', 'pdf', 'xls', 'xlsx', 'doc', 'tiff']))
                                <a href="{{ $file->url }}"
                                   target="_blank" style="display: inline-block; margin-right: 15px;">
                                    <img class="media-object preview-icon"
                                         src="/images/extensions/{{$file->ext}}.png">
                                    <p style="text-align: center;">Прикреплен файл</p>
                                </a>
                            @else
                                <a href="{{ $file->url }}"
                                   target="_blank" style="display: inline-block; margin-right: 15px;">
                                    <img class="media-object preview-icon"
                                         src="/images/extensions/unknown.png">
                                    <p style="text-align: center;">Прикреплен файл</p>
                                </a>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection