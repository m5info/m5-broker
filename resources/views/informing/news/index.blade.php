@extends('layouts.app')

@section('content')

    <h2 style="margin-bottom: 20px;">Последние новости</h2>
    <div class="row">
        <div class="col-sm-12">
            <table class="news-table" style="width: 100%;">
                <tbody>
                @if(!$news->isEmpty())
                    @foreach($news as $new)
                        <tr onclick="location.href = '/informing/news/{{$new->id}}'">
                            <th><p>{{$new->header}}</p></th>
                            <th><p>{{$new->short_desc}}</p></th>
                            <th><p>{{setDateTimeFormatRu($new->created_at)}}</p></th>
                        </tr>
                    @endforeach
                @else
                    <p>На данный момент новостей нет..</p>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection