<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <span class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="color: red;"></span>
    @if(Auth::user()->hasPermission('contracts', 'contract_download_all'))
        <span class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="color: red;"></span>
    @endif
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @if($new->docs->count())
            <table class="table orderStatusTable dataTable no-footer">
            <!--<thead>
            <tr>
                <th>{{ trans('users/users.edit.title') }}</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            </thead>-->
                <tbody>
                @foreach($new->docs as $file)
                    <div class="col-lg-6 col-md-12">
                        <div class="upload-dot">
                            <div class="block-image">
                                @if (in_array($file->ext, ['jpg', 'jpeg', 'png', 'gif']))
                                    <a href="{{ url($file->url) }}" target="_blank">
                                        <img class="media-object preview-image" src="{{ url($file->preview) }}"
                                             onerror="this.onerror=null;this.src='/images/extensions/unknown.png';">
                                    </a>
                                @elseif (in_array($file->ext, ['docx', 'pdf', 'xls', 'xlsx', 'doc', 'tiff']))
                                    <a href="{{ url($file->url) }}" target="_blank">
                                        <img class="media-object preview-icon"
                                             src="/images/extensions/{{$file->ext}}.png">
                                    </a>
                                    @else
                                    <a href="{{ url($file->url) }}" target="_blank">
                                        <img class="media-object preview-icon"
                                             src="/images/extensions/unknown.png">
                                    </a>
                                @endif
                                <div class="upload-close">
                                    <div class="" style="float:right;color:red;">
                                        <a href="javascript:void(0);" onclick="removeFileFromNews('{{ $file->name }}')">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<tr>
                    <td>{{ $file->original_name }}</td>
                    <td>
                        <a href="{{ url($file->url) }}" class="btn btn-primary" target="_blank">
                            {{ trans('form.buttons.download') }}
                            </a>
                        </td>
                        <td>
                            <button class="btn btn-danger" type="button" onclick="removeFile('{{ $file->name }}')">
                            {{ trans('form.buttons.delete') }}
                            </button>
                        </td>
                    </tr>-->
                @endforeach
                </tbody>
            </table>
        @else
            <h3>{{ trans('form.empty') }}</h3>
        @endif
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        {!! Form::open(['url'=>"/informing/edit-news/$new->id/update_file/",'method' => 'post', 'class' => 'dropzone', 'id' => 'addManyDocForm']) !!}
        <div class="dz-message" data-dz-message>
            <p>Перетащите сюда файлы</p>
            <p class="dz-link">или выберите с диска</p>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">

    @if(isset($new) && sizeof($new->docs))

        @foreach($new->docs as $key => $document)

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">

                <div class="row">
                    <span class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="color: red;padding-top:5px;">{{$new->file_title}} <span
                                class="required"></span></span>

                    @if($new->document($document->id))

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="upload-dot">
                                <div class="block-image">
                                    @if (in_array($new->document($document->id)->file->ext, ['jpg', 'jpeg', 'png', 'gif']))
                                        <a href="{{ url($new->document($document->id)->file->url) }}" target="_blank">
                                            <img class="media-object preview-image"
                                                 src="{{ url($new->document($document->id)->file->preview) }}"
                                                 onerror="this.onerror=null;this.src='/images/extensions/unknown.png';">
                                        </a>
                                    @else
                                        <a href="{{ url($contract->document($document->id)->file->url) }}"
                                           target="_blank">
                                            <img class="media-object preview-icon"
                                                 src="/images/extensions/{{$new->document($document->id)->file->ext}}.png">
                                        </a>
                                    @endif
                                    <div class="upload-close">
                                        <div class="" style="float:right;color:red;">
                                            <a href="javascript:void(0);"
                                               onclick="removeDocument('{{ $new->document($document->id)->file->name }}','{{$document->id}}')">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

        @endforeach

    @endif


    <script>

        function removeFileFromNews(id) {
            if (!customConfirm()) {
                return false;
            }

            var filesUrl = '{{url("/informing/edit-news/{$new->id}/delete_file")}}/' + id;
            $.post(filesUrl, {}, function (res) {
                location.reload();
            });

        }

    </script>

</div>