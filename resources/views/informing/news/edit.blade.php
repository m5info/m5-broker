@extends('layouts.app')

@section('content')

    <h2 style="margin-bottom: 20px;">Редактировать новости
        <button class="btn btn-success pull-right" onclick="location.href = '/informing/edit-news/create/'"
                style="margin-right: 30px;">Создать
        </button>
    </h2>
    <div class="row">
        <div class="col-sm-12">
            @if(!$news->isEmpty())
                <table class="news-table" style="width: 100%;">
                    <tbody>
                        @foreach($news as $new)
                        <tr onclick="location.href = '/informing/edit-news/{{$new->id}}/edit'">
                            <th><p>{{$new->header}}</p></th>
                            <th><p>{{$new->short_desc}}</p></th>
                            <th><p>{{setDateTimeFormatRu($new->created_at)}}</p></th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <p>На данный момент новостей нет..</p>
            @endif
        </div>
    </div>

@endsection