@extends('layouts.frame_new')

@section('content')
    @include('informing.guides.partials.guide_block',['guides' => $guides ?? []])
@endsection
