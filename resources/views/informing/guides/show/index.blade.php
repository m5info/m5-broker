@extends('layouts.app')

@section('content')
    @include('informing.guides.partials.guide_block',['guides' => $guides ?? []])
@endsection
