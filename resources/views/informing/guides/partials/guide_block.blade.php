@if(isset($guides) && count($guides) >0)
    @foreach($guides as $guide)
        @php
            $class = isset($smart) ? 'col-md-4 guid-block-sm' : 'col-xs-12';
            $id = $guide->id == '' ? 0 : $guide->id;
            $title = $guide->block_title  == '' ? 'Название блока' : $guide->block_title;
            $html = $guide->block_html == '' ? 'Контент' : $guide->block_html;
            $categories  = \App\Models\Menu::forSelect();
        @endphp
        <div id="guide{{ $id }}" data-id="{{ $id ?? '0'  }}"
             class="guide_block panel panel-default  {{ $class }} block-inner">
            <div class="col-xs-2 panel-heading">
                <h2>#{{ $guide->id }}</h2>
            </div>
            <div class="panel-heading  col-xs-10">
                <h2 class="block-title"> {{ $guide->block_title }}</h2>
            </div>
            <div id="guide_html{{ $id }}" class="panel-body block-sub col-xs-12">
                {!! $html !!}
            </div>
           {{-- {{ dd(\App\Models\Menu::forSelect()) }}--}}
            @if(isset($edit))
                <div class="row">
                    <div class="guide-buttons btn-group col-xs-12">

                        <div class="col-xs-3 guide-page-select">
                            {{ Form::select('page',$categories,$guide->group_id,['class'=>'form-control col-xs-12','id'=>'guide_group'.$guide->id,'onchange'=>'updateBlock('.$guide->id.')']) }}
                        </div>

                        <button onclick="openFancyBoxFrame_customHeight('{{url("informing/guide/show/".$guide->id )}}', 400)"  class="guide-preview col-xs-3 btn red btn-sm" onclick="">
                            Просмотр
                        </button>

                        <a href="{{ url('/informing/guide/edit/'.$guide->id) }}" target="_blank" role="button"
                           class="edit-guide col-xs-3 black btn red btn-sm btn-primary"> Редактировать отдельно</a>

                        <button onclick="removeGuideBlock($(this))" class="removeGuideBlock col-xs-3 black btn red btn-sm" data-id="{{ $guide->id }}">
                            удалить
                        </button>
                    </div>
                </div>
            @endif
        </div>

    @endforeach
@endif
