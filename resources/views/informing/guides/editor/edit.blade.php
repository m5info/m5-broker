@extends('layouts.app')

@section('content')
    <h1 class="col-xs-12">Редактирование руководства</h1>
    <div class="row">
        <div class="guide-buttons btn-group col-xs-12 ">
            <a href="{{ url('/informing/guide/edit/') }}" target="_blank" role="button"
               class="edit-guide col-xs-3 black btn red btn-sm btn-primary">Все блоки</a>
        </div>
    </div>

  @include('informing.guides.partials.guide_block',['edit'=>'true','guides' => $guides ?? []])

    <div class="col-xs-12">
        <button id="addGuideBlock" class="btn btn-sm btn-block btn-info ">Добавить блок</button>

    </div>

    @csrf


    <script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
    <script>CKFinder.config( { connectorPath: '/ckfinder/connector' } );</script>

    <script>
        var guides_counts = {!! $in_groups  !!};
    </script>

    <script src="/plugins/ckeditor/ckeditor.js"></script>
    <script src="/assets/js/guide.js"></script>
@endsection
