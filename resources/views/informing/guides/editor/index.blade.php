@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="guide-buttons btn-group col-xs-12 ">
            <a href="{{ url('/informing/guide/edit/') }}" target="_blank" role="button"
               class="edit-guide col-xs-3 black btn red btn-sm btn-primary">Перейти к редактированию</a>
        </div>
    </div>
    @include('informing.guides.partials.guide_block',['smart'=>'true','guides' => $guides ?? []])
@endsection
