<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    @if(!$kv->file)
        <h3 style="text-align: center;">Документ отсутствует</h3>
    @else
        <h3 style="text-align: center;">Документ добавлен</h3>
    @endif
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

    @if($kv->file)
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="upload-dot">
                <div class="block-image">
                    @if (in_array($kv->file->ext, ['jpg', 'jpeg', 'png', 'gif']))
                        <a href="{{ url($kv->file->url) }}" target="_blank">
                            <img class="media-object preview-image"
                                 src="{{ url($kv->file->preview) }}"
                                 onerror="this.onerror=null;this.src='/images/extensions/unknown.png';">
                        </a>
                    @else
                        <a href="{{ url($kv->file->url) }}" target="_blank">
                            <img class="media-object preview-icon"
                                 src="/images/extensions/{{$kv->file->ext}}.png">
                        </a>
                    @endif
                    <div class="upload-close">
                        <div class="" style="float:right;color:red;">
                            <a href="javascript:void(0);"
                               onclick="removeDocument()">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <form method="POST" action="/informing/edit-kv/{{$kv->id}}/update_file/" accept-charset="UTF-8"
              class="dropzone_ dz-clickable" id="addManyDocForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="dz-message" data-dz-message="">
                <p>Перетащите сюда файлы</p>
                <p class="dz-link">или выберите с диска</p>
            </div>
        </form>
    @endif
</div>

<script>
    function removeDocument() {
        if (!customConfirm()) {
            return false;
        }
        var filesUrl = '/informing/edit-kv/{{$kv->id}}/file/{{$kv->file ? $kv->file->id : ''}}';
        var fileUrl = filesUrl + '/';
        $.post(fileUrl, {}, function () {
            reload();
        });
    }
</script>

