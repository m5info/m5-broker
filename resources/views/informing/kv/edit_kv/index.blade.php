@extends('layouts.app')

@section('content')

    <h2 style="margin-bottom: 20px;">Редактирование КВ <a class="btn btn-success inline-block pull-right"
                                                                 id="create">Создать</a>
    </h2>
    <div class="row">
        <div class="col-sm-12">
            @if(!$kvs->isEmpty())
                <table class="news-table tov-table tov-table-with-last-buttons" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>
                            <p>#</p>
                        </th>
                        <th>
                            <p>Дата создания</p>
                        </th>
                        <th>
                            <p>Актуальность</p>
                        </th>
                        <th>
                            <p>Название файла</p>
                        </th>
                        <th>
                            <p>Актуально с</p>
                        </th>
                        <th>
                            <p style="float: right;">Действие</p>
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($kvs as $kv)
                        @if($kv->is_actual == 1)
                            @php
                                $color = 'background: #eaffe5;';
                            @endphp
                        @else
                            @php
                                $color = '';
                            @endphp
                        @endif
                        <tr class="{{$kv->is_actual === 1 ? 'success-tr' : ''}}">
                            <td class="td_kv" data-id="{{$kv->id}}">
                                #{{$kv->id}}
                            </td>
                            <td class="td_kv" data-id="{{$kv->id}}">
                                {{setDateTimeFormatRu($kv->created_at)}}
                            </td>
                            <td class="td_kv" data-id="{{$kv->id}}">
                                {{ $kv->is_actual ? 'Актуально' : 'Не актуально'  }}
                            </td>
                            @if($kv->file_id != 0)
                                <td class="td_kv" data-id="{{$kv->id}}">
                                    {{$kv->file->original_name}}
                                </td>
                            @else
                                <td class="td_kv" data-id="{{$kv->id}}">
                                    <p style="color: red;">Файл отсутствует</p>
                                </td>
                            @endif
                            <td>
                                @if($kv->is_actual == 1)
                                    <p>{{ $kv->actual_from ? setDateTimeFormatRu($kv->actual_from, 1) : ''}}</p>
                                @endif
                            </td>
                            <td>
                                @if($kv->file_id != 0)
                                    <a class="btn btn-success inline-block pull-right"
                                       href="{{url('files/'.$kv->file->name)}}">
                                        Скачать
                                    </a>
                                @else
                                    <a class="btn btn-danger inline-block pull-right td_kv" data-id="{{$kv->id}}">
                                        Файл не загружен
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    @else
                        <p>На данный момент КВ нет..</p>
                    </tbody>
                </table>
            @endif
        </div>
    </div>

@endsection

@section('js')

    <script>

        $(document).on('click', '.td_kv', function(){
            id = $(this).data('id');
            location.href = '/informing/edit-kv/'+id;
        });

        $(document).on('click', '#create', function () {
            answer = confirm('Создать новый КВ?');
            if (answer) {
                $.post({
                    url: '/informing/edit-kv/create',
                    success: function (res) {
                        if (res == 200) {
                            location.reload()
                        }
                    }
                })
            }
        });

    </script>

@endsection