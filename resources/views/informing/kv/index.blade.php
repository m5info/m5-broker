@extends('layouts.app')

@section('content')

    <h2 style="margin-bottom: 20px;">КВ</h2>
    <div class="row">
        <div class="col-sm-12">
            @if(!$kvs->isEmpty())
                <table class="news-table tov-table tov-table-with-last-buttons" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>
                            <p>#</p>
                        </th>
                        <th>
                            <p>Дата создания</p>
                        </th>
                        <th>
                            <p>Актуальность</p>
                        </th>
                        <th>
                            <p>Название файла</p>
                        </th>
                        <th>
                            <p>Актуально с</p>
                        </th>
                        <th>
                            <p style="float: right;">Действие</p>
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($kvs as $kv)
                        <tr class="{{$kv->is_actual === 1 ? 'success-tr' : ''}}">
                            <td>
                                <p>
                                    #{{$kv->id}}
                                </p>
                            </td>
                            <td>
                                <p>
                                    {{setDateTimeFormatRu($kv->created_at)}}
                                </p>
                            </td>
                            <td>
                                <p>{{ $kv->is_actual ? 'Актуально' : 'Не актуально'  }}</p>
                            </td>
                            @if($kv->file_id != 0)
                                <td>
                                    {{$kv->file->original_name}}
                                </td>
                            @else
                                <td>
                                    <p style="color: red;">Файл отсутствует</p>
                                </td>
                            @endif
                            <td>
                                @if($kv->is_actual == 1)
                                    <p>{{ $kv->actual_from ? setDateTimeFormatRu($kv->actual_from, 1) : ''}}</p>
                                @endif
                            </td>
                            <td>
                                @if($kv->file_id != 0)
                                    <a class="btn btn-success inline-block pull-right"
                                       href="{{url('files/'.$kv->file->name)}}">Скачать</a>
                                @else
                                    <a class="btn btn-danger inline-block pull-right">Файл не загружен</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    @else
                        <p>На данный момент КВ нет..</p>
                    </tbody>
                </table>
            @endif
        </div>
    </div>

@endsection

@section('js')

    <script>


    </script>

@endsection