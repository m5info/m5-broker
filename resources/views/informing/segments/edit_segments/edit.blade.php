@extends('layouts.app')

@section('content')

    <h2 style="margin-bottom: 20px;">Редактирование сегмента</h2>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="block-main">
                <div class="block-sub">
                    <div class="form-horizontal">
                        <form action="/informing/edit-segments/{{$segment->id}}/update/" id="form_" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Актуально?</label>
                                <div class="col-sm-8">
                                    {{ Form::checkbox('is_actual', $segment->is_actual, $segment->is_actual, ['required']) }}
                                </div>
                            </div>
                        </form>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Сегмент (XLS)</label>
                                <div class="col-sm-8">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        @include('informing.segments.edit_segments.file', [
                                        'segment' => $segment
                                        ])
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" style="margin-bottom: 20px;">
                                <button type="submit" id="delete_button" class="btn btn-danger pull-left">
                                    Удалить
                                </button>
                                <button type="submit" id="submit_button" class="btn btn-primary pull-right">
                                    Сохранить
                                </button>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>

        $('#delete_button').click(function(){
            res = confirm('Удалить сегмент?');
            if (res){
                $.post({
                    url: '/informing/edit-segments/{{$segment->id}}/delete_segment/',
                    success: function(res){
                        if (res == 200){
                            location.href = '/informing/edit-segments';
                        }
                    }
                });
            }
        });

        $('#submit_button').click(function(){
          $('#form_').submit();
        });

        function deleteGroupSegment(id) {
            answer = confirm('Удалить сегмент #' + id + '?');

            if (answer) {

                $.post({
                    url: '/informing/segments/delete_group',
                    data: {
                        id: id
                    },
                    success: function (res) {
                        if (res == 200) {
                            location.reload();
                        }
                    }
                })

            }
        }

        $('#addManyDocForm').dropzone({
            success: function(){
                reload();
            }
        });


    </script>

@endsection