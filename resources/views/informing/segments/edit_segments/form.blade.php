
<div class="form-group">
    <label class="col-sm-4 control-label">Актуально?</label>
    <div class="col-sm-8">
        {{ Form::checkbox('is_actual', old('is_actual'), old('is_actual'), ['required']) }}
    </div>
</div>


<div class="form-group">
    <label class="col-sm-4 control-label">Сегмент (XLS)</label>
    <div class="col-sm-8">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @include('informing.segments.edit_segments.file')
        </div>
    </div>
</div>


