@extends('layouts.app')

@section('content')

    <h2 style="margin-bottom: 20px;">Редактирование сегментов <a class="btn btn-success inline-block pull-right"
                                                                 id="create">Создать</a>
    </h2>
    <div class="row">
        <div class="col-sm-12">
            @if(!$segments->isEmpty())
                <table class="news-table tov-table tov-table-with-last-buttons" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>
                            <p>#</p>
                        </th>
                        <th>
                            <p>Дата создания</p>
                        </th>
                        <th>
                            <p>Актуальность</p>
                        </th>
                        <th>
                            <p>Название файла</p>
                        </th>
                        <th>
                            <p>Актуально с</p>
                        </th>
                        <th>
                            <p style="float: right;">Действие</p>
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($segments as $segment)
                        @if($segment->is_actual == 1)
                            @php
                                $color = 'background: #eaffe5;';
                            @endphp
                        @else
                            @php
                                $color = '';
                            @endphp
                        @endif
                        <tr class="{{$segment->is_actual === 1 ? 'success-tr' : ''}}">
                            <td class="td_segment" data-id="{{$segment->id}}">
                                #{{$segment->id}}
                            </td>
                            <td class="td_segment" data-id="{{$segment->id}}">
                                {{setDateTimeFormatRu($segment->created_at)}}
                            </td>
                            <td class="td_segment" data-id="{{$segment->id}}">
                                {{ $segment->is_actual ? 'Актуально' : 'Не актуально'  }}
                            </td>
                            @if($segment->file_id != 0)
                                <td class="td_segment" data-id="{{$segment->id}}">
                                    {{$segment->file->original_name}}
                                </td>
                            @else
                                <td class="td_segment" data-id="{{$segment->id}}">
                                    <p style="color: red;">Файл отсутствует</p>
                                </td>
                            @endif
                            <td class="td_segment" data-id="{{$segment->id}}">
                                @if($segment->is_actual == 1)
                                    <p>{{ $segment->actual_from ? setDateTimeFormatRu($segment->actual_from, 1) : ''}}</p>
                                @endif
                            </td>
                            <td>
                                @if($segment->file_id != 0)
                                    <a class="btn btn-success inline-block pull-right"
                                       href="{{url('files/'.$segment->file->name)}}">
                                        Скачать
                                    </a>
                                @else
                                    <a class="btn btn-danger inline-block pull-right td_segment" data-id="{{$segment->id}}">
                                        Файл не загружен
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    @else
                        <p>На данный момент групп сегментов нет..</p>
                    </tbody>
                </table>
            @endif
        </div>
    </div>

@endsection

@section('js')

    <script>

        $(document).on('click', '.td_segment', function () {
            id = $(this).data('id');
            location.href = '/informing/edit-segments/'+id;
        });

        $(document).on('click', '#create', function () {
            answer = confirm('Создать новый сегмент?');
            if (answer) {
                $.post({
                    url: '/informing/edit-segments/create',
                    success: function (res) {
                        if (res == 200) {
                            location.reload()
                        }
                    }
                })
            }
        });

        function deleteGroupSegment(id) {
            answer = confirm('Удалить сегмент #' + id + '?');

            if (answer) {

                $.post({
                    url: '/informing/segments/delete_group',
                    data: {
                        id: id
                    },
                    success: function (res) {
                        if (res == 200) {
                            location.reload();
                        }
                    }
                })

            }
        }


    </script>

@endsection