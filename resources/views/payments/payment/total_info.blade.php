<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
    <table class="bso_table table table-bordered">
        <tbody>

        @if(auth()->user()->hasPermission('contracts', 'set_financial_policy_manually'))

        <tr>
            <td><strong>ИТОГО БАЗОВОЕ КВ</strong></td>
            @if($payment->financial_policy_manually_set==1)
                <td>{{($payment->financial_policy_kv_bordereau+$payment->financial_policy_kv_dvoy)}}</td>
                <td>{{titleFloatFormat(getTotalSumToPrice($payment->payment_total, ($payment->financial_policy_kv_bordereau+$payment->financial_policy_kv_dvoy)))}}</td>
            @else
                @if($payment->financial_policy)
                    @if($payment->financial_policy->fix_price == 1)
                        <td>{{titleFloatFormat($payment->financial_policy_kv_bordereau)}}</td>
                        <td>{{titleFloatFormat(getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_bordereau))}}</td>
                    @else

                        <td>{{titleFloatFormat($payment->financial_policy->kv_sk)}}</td>
                        <td>{{titleFloatFormat(getTotalSumToPrice($payment->payment_total, $payment->financial_policy ? $payment->financial_policy->kv_sk : 0))}}</td>
                    @endif
                @else
                    <td></td>
                    <td></td>
                @endif
            @endif
        </tr>

        <tr>
            <td>Бордеро</td>
            @if($payment->financial_policy_manually_set==1)
                <td>{{($payment->financial_policy_kv_bordereauy)}}</td>
                <td>{{titleFloatFormat(getTotalSumToPrice($payment->payment_total, ($payment->financial_policy_kv_bordereau)))}}</td>
            @else
                @if($payment->financial_policy)
                    @if($payment->financial_policy->fix_price ==1)
                        <td>{{titleFloatFormat($payment->financial_policy_kv_bordereau)}}</td>
                        <td>{{titleFloatFormat(getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_bordereau))}}</td>
                    @else
                        <td>{{$payment->financial_policy ? $payment->financial_policy->kv_bordereau : ''}}</td>
                        <td>{{titleFloatFormat(getTotalSumToPrice($payment->payment_total, $payment->financial_policy ? $payment->financial_policy->kv_bordereau : 0))}}</td>

                    @endif
                @else
                    <td></td>
                    <td></td>
                @endif
            @endif
        </tr>

        <tr>
            <td>ДВОУ</td>
            @if($payment->financial_policy_manually_set==1)
                <td>{{($payment->financial_policy_kv_dvoy)}}</td>
                <td>{{titleFloatFormat(getTotalSumToPrice($payment->payment_total, ($payment->financial_policy_kv_dvoy)))}}</td>
            @else
                <td>{{$payment->financial_policy ? $payment->financial_policy->kv_dvou : ''}}</td>
                <td>{{titleFloatFormat(getTotalSumToPrice($payment->payment_total, $payment->financial_policy ? $payment->financial_policy->kv_dvou : 0))}}</td>
            @endif
        </tr>

        @endif

        <tr>
            <td>Банк</td>
            <td>{{($payment->bank_kv)}}</td>
            <td>{{($payment->bank_kv_total!=0.00)?titleFloatFormat($payment->bank_kv_total):''}}</td>
        </tr>

        <tr>
            <td>Эквайринг</td>
            <td>{{($payment->acquire_percent)}}</td>
            <td>{{($payment->acquire_total!=0.00)?titleFloatFormat($payment->acquire_total):''}}</td>
        </tr>

        @if(auth()->user()->hasPermission('contracts', 'set_financial_policy_manually'))
        <tr>
            <td><strong>ИТОГО КВ</strong></td>
            <td>{{($payment->financial_policy_kv_bordereau+$payment->financial_policy_kv_dvoy)}}</td>
            <td>{{titleFloatFormat($payment->financial_policy_kv_bordereau_total+$payment->financial_policy_kv_dvoy_total)}}</td>
        </tr>

        <tr>
            <td><strong>Маржа</strong></td>
            <td>{{($payment->financial_policy_marjing)}}</td>
            <td>{{($payment->financial_policy_marjing_total!=0.00)?titleFloatFormat($payment->financial_policy_marjing_total):''}}</td>
        </tr>
        @endif

        </tbody>
    </table>

    <br/>

    <table class="bso_table table table-bordered">
        <tbody>
        <tr>
            <td><strong>БАЗОВОЕ КВ Агента / Менеджера</strong></td>
            @if($payment->financial_policy_manually_set==1)
                <td>{{($payment->financial_policy_kv_agent)}}</td>
                <td>{{titleFloatFormat($payment->financial_policy_kv_agent_total)}}</td>
            @else

                @php
                    $kv_agent = $payment->financial_policy ? $payment->financial_policy->getGroupKV($payment)->kv_agent : 0;
                @endphp

                @if($payment->financial_policy)
                    @if($payment->financial_policy->fix_price == 1)
                        <td>{{titleFloatFormat($payment->financial_policy_kv_agent)}}</td>
                        <td>{{titleFloatFormat($payment->financial_policy_kv_agent_total)}}</td>
                    @else
                        <td>{{$kv_agent}}</td>
                        <td>{{titleFloatFormat(getTotalSumToPrice($payment->payment_total, $kv_agent))}}</td>
                    @endif
                @else
                    <td></td>
                    <td></td>
                @endif


            @endif
        </tr>

        <tr>
            <td><strong>КВ Агента / Менеджера</strong></td>
            <td>{{($payment->financial_policy_kv_agent)}}</td>
            <td>{{titleFloatFormat($payment->financial_policy_kv_agent_total)}}</td>
        </tr>

        <tr>
            <td><strong>КВ Рекомендодателя</strong></td>
            <td>{{($payment->referencer_kv_real)}}</td>
            <td>{{titleFloatFormat($payment->referencer_kv_total)}}</td>
        </tr>

        </tbody>
    </table>


</div>

<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >


    <table class="bso_table table table-bordered">
        <tbody>
        <tr>
            <td>Взнос</td>
            <td>{{($payment->payment_total!=0.00)?titleFloatFormat($payment->payment_total):''}}</td>
        </tr>

        <tr>
            <td>Взнос реальное</td>
            {{--@if((int)$payment->payment_flow == 0)
                @php
                    $invoice_payment_total = $payment->invoice_payment_total * ((100 - $payment->informal_discount)/100);
                @endphp
                <td>{{($payment->invoice_payment_total!=0.00)? titleFloatFormat($invoice_payment_total):''}}</td>
            @else--}}
                <td>{{($payment->invoice_payment_total!=0.00)?titleFloatFormat($payment->invoice_payment_total):''}}</td>
            {{--@endif--}}
        </tr>
        </tbody>
    </table>

    <br/>

    <table class="bso_table table table-bordered">
        <tbody>
            <tr>
                <td>Официальная</td>
                <td>{{($payment->official_discount)}}</td>
                <td>{{($payment->official_discount_total!=0.00)?titleFloatFormat($payment->official_discount_total):''}}</td>
            </tr>

            <tr>
                <td>Неофициальная</td>
                <td>{{($payment->informal_discount)}}</td>
                <td>{{($payment->informal_discount_total!=0.00)?titleFloatFormat($payment->informal_discount_total):''}}</td>
            </tr>



        </tbody>
    </table>
</div>


<style>
    .bso_table td, th {
        border: 1px solid #777;
    }
</style>