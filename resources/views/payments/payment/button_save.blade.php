
    @if((auth()->user()->hasPermission('reports', 'payment_edit')) || auth()->user()->hasPermission('reports', 'payment_pay_edit'))

        <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>
    @endif
