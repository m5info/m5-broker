<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>№</th>
        <th>Индекс</th>
        <th>Организация</th>
        <th>СК</th>
        <th>Страхователь</th>
        <th>Продукт</th>
        <th>Тип платежа</th>
        <th>№ договора</th>
        <th>Квитанция</th>
        <th>Сумма</th>
        <th>Оф.скидка</th>
        <th>Неоф.скидка</th>
        <th>КВ агента, %</th>
        <th>КВ агента, руб</th>
        <th>К оплате</th>
    </tr>
    </thead>
    <tbody>
    @php($total = 0)
    @php($total_kv_agent = 0)
    @php($total_sum = 0)
    @if(sizeof($invoice->payments))
        @foreach($invoice->payments as $key => $payment)
            @php($total += $payment->payment_total)
            @php($total_kv_agent += $payment->financial_policy_kv_agent_total)
            @php($total_sum += $payment->getPaymentAgentSum())
            <tr @if($payment->is_deleted == 1) style="background-color: #ffcccc;" @endif>



                <td>{{ $key+1 }}</td>
                <td>{{ $payment->id }}</td>
                <td>{{ $payment->bso->supplier_org ? $payment->bso->supplier_org->title : "" }}</td>
                <td>{{ $payment->bso && $payment->bso->insurance ? $payment->bso->insurance->title : "" }}</td>
                <td>{{ $payment->Insurer }}</td>
                <td>{{ $payment->bso && $payment->bso->product ? $payment->bso->product->title : ""}}</td>
                <td>{{ $payment->type_ru() }}</td>
                <td>{{ $payment->bso ? $payment->bso->bso_title : "" }}</td>
                <td>{{ $payment->bso_receipt ? : "" }}</td>
                <td nowrap>{{ getPriceFormat($payment->payment_total) }}</td>
                <td nowrap>{{ getPriceFormat($payment->official_discount_total) }}</td>
                <td nowrap>{{ getPriceFormat($payment->informal_discount_total) }}</td>
                <td>{{ $payment->financial_policy_kv_agent }}</td>
                <td nowrap>{{ getPriceFormat($payment->financial_policy_kv_agent_total) }}</td>
                <td nowrap>{{ getPriceFormat($payment->getPaymentAgentSum()) }}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan='8'>&nbsp</td>
            <td><strong class="itogo">ИТОГО:</strong></td>
            <td nowrap><strong>{{ getPriceFormat($total) }}</strong></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td nowrap><strong>{{ getPriceFormat($total_kv_agent) }}</strong></td>
            <td nowrap><strong>{{ getPriceFormat($total_sum) }}</strong></td>
        </tr>

    @endif
    </tbody>
</table>