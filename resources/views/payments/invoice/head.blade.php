<div class="page-heading">
    <h2>Счёт {{ $invoice->id }}
        @php
            $show_button = true;

            if(sizeof($invoice->payments)){
                $payment = $invoice->payments->first();
                if($payment->acts_to_underwriting_id){
                    $show_button = false;
                }
            }
        @endphp

        @if($invoice->status_id != 1 && $show_button)
            <span class="btn btn-success btn-right" onclick="create_act()">Передать в андеррайтинг</span>
        @endif

        <a href="/finance/invoice/invoices/{{$invoice->id}}/direction" target="_blank" id="direction" type="submit" class="btn btn-success btn-right">Направление</a>
        <a href="/finance/invoice/invoices/{{$invoice->id}}/act_export_under" type="submit" class="btn btn-success btn-right doc_export_btn">Акт сдачи в андеррайтинг</a>
    </h2>
    <br/>


</div>

<div class="divider"></div>

<br/>

<style>
    table.table tr{line-height: 30px;}
    .btn-sm{cursor: pointer;}
</style>
