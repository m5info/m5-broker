<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="block-view">
            <div class="block-sub">
                <div class="row">

                    @if(($invoice->status_id == 1 || $invoice->status_id == 3) && auth()->user()->hasPermission('cashbox', 'invoice'))

                        @include("payments.invoice.partials.set_payment", ["invoice" => $invoice, 'need_withdraw' => $need_withdraw])

                    @else

                        @include("payments.invoice.partials.get_payment", ["invoice" => $invoice])

                    @endif

                </div>
            </div>
        </div>

    </div>

    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="block-view">
            <div class="block-sub">
                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">Статус заказа</span>
                            <span class="view-value">{{$invoice->statuses_ru('status_id')}}</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">К оплате</span>
                            <span class="view-value">{{ titleFloatFormat($invoice_info->total_sum) }}</span>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">Тип платежа</span>
                            <span class="view-value">{{ $invoice->types_ru('type') }}</span>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">Дата выставления</span>
                            <span class="view-value">{{ setDateTimeFormatRu($invoice->created_at) }}</span>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">Организация</span>
                            <span class="view-value">{{ $invoice->org ? $invoice->org->title : "" }}</span>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">Агент</span>
                            <span class="view-value">{{$invoice->agent?$invoice->agent->name:''}}</span>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="view-field">
                            <span class="view-label">Агентское КВ</span>
                            <span class="view-value">{{ titleFloatFormat($invoice_info->total_kv_agent) }}</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                @if($invoice->status_id == 1)


                    <span class="btn btn-success btn-left" onclick="saveInvoiceTypePayment()">Сохранить</span>

                    <a href="javascript:void(0);" id="delete_invoice" class="btn btn-danger pull-right">Расформировать счёт</a>
                @endif
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="action_table" style="display: none">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Операции</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($invoice->status_id == 1)
                            <tr>
                                <td>
                                    <a href="javascript:void(0);" class="btn btn-danger btn-right" style="max-width: 140px"
                                       id="remove_from_invoice">Исключить</a>
                                </td>
                            </tr>
                        @else
                            <tr>
                                <td>
{{--                                    <span class="btn btn-success btn-right" onclick="create_act()">Передать в андеррайтинг</span>--}}
                                    Счёт оплачен. Нет доступных операций.
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>



<script>


    function initInvoce() {

        initPayment();

    }




</script>