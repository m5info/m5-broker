<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <label class="control-label">Вид оплаты</label>
    <div>
        {{ Form::select('type_invoice_payment', collect(\App\Models\Finance\Invoice::TYPE_INVOICE_PAYMENT), $invoice->getInvoceDefaultPaymentType(), ['class' => 'form-control select2-all', 'id'=>'type_invoice_payment', 'required', 'onchange' => 'loadPaymentContent()']) }}
    </div>

</div>

<div id="invoice_payment">

</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div>
        <input type="checkbox" value="1" name="withdraw_documents" {{ $need_withdraw ? "checked" : "" }}>
        <label class="control-label">Изъять документы</label>
    </div>
</div>


<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

    <span class="btn btn-primary btn-right" onclick="confirmInvoice()">Подтвердить</span>
    @if($invoice->status_id == 1)
        <input type="submit" onclick="inWaiting();return false;" class="btn btn-primary btn-left" value="В ожидание"/>
    @endif
</div>

<script>


    function initPayment() {

        loadPaymentContent();

    }

    function confirmInvoice() {
        newCustomConfirm(function (result) {
            if (result){
                $("#formInvoice").submit();
            }
        }, 'Подтвердите действие', 'Вы действительно подтверждаете счет?');
    }

    function inWaiting() {

        loaderShow();

        var checked = 0;

        if ($('[name="withdraw_documents"]').prop('checked')){
            checked = 1;
        }

        var data = {
            withdraw_documents:checked
        };

        $.ajax({
            type: "POST",
            url: "{{url("/cashbox/invoice/{$invoice->id}/in_waiting")}}",
            async: false,
            data: data,
            success: function (response) {
                reload();
            }
        });


    }

    function loadPaymentContent() {


        loaderShow();
        $.post("{{url("/cashbox/invoice/{$invoice->id}/data_invoice_payment")}}", {type_invoice_payment:$("#type_invoice_payment").val()}, function (response) {
            $('#invoice_payment').html(response);

            $('.sum')
                .change(function () {
                    $(this).val(CommaFormatted($(this).val()));
                })
                .blur(function () {
                    $(this).val(CommaFormatted($(this).val()));
                })
                .keyup(function () {
                    $(this).val(CommaFormatted($(this).val()));
                });

        }).fail(function(){
            $('#invoice_payment').html('');
        }).always(function() {
            loaderHide();
        });


    }


</script>