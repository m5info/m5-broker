@extends('layouts.frame')


@section('title')

    Чек {{$data_info->device_code}}

@endsection

@section('content')



            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">uuid</span>
                        <span class="view-value">{{$data_info->uuid}}</span>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">status</span>
                        <span class="view-value">{{$data_info->status}}</span>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">ecr_registration_number</span>
                        <span class="view-value">{{$data_info->payload->ecr_registration_number}}</span>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">fiscal_document_attribute</span>
                        <span class="view-value">{{$data_info->payload->fiscal_document_attribute}}</span>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">fiscal_document_number</span>
                        <span class="view-value">{{$data_info->payload->fiscal_document_number}}</span>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">fiscal_receipt_number</span>
                        <span class="view-value">{{$data_info->payload->fiscal_receipt_number}}</span>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">fn_number</span>
                        <span class="view-value">{{$data_info->payload->fn_number}}</span>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">receipt_datetime</span>
                        <span class="view-value">{{$data_info->payload->receipt_datetime}}</span>
                    </div>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">Сумма</span>
                        <span class="view-value">
                             {{titleFloatFormat($data_info->payload->total)}}
                        </span>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">Сайт</span>
                        <span class="view-value">www.nalog.ru</span>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <center><div id="qr-container"></div></center>
                </div>


            </div>




@endsection

@section('footer')

    <button type="submit" class="btn btn-primary">Печать</button>

@endsection


@section('js')

    <script src="/js/jquery-qrcode-0.14.0.js"></script>


    <script>

        $(function(){




            updateQrCode();


        });


        function updateQrCode() {
            var jq = window.jQuery;

            var options = {
                render: "div",
                ecLevel: "H",
                minVersion: parseInt(6, 10),

                fill: "#333333",
                background: "#ffffff",
                // fill: jq('#img-buffer')[0],

                text: "{{$qr_txt}}",
                size: parseInt(130, 10),
                radius: parseInt(20, 10) * 0.01,
                quiet: parseInt(1, 10),

            };

            jq('#qr-container').empty().qrcode(options);
        }


    </script>




@endsection