@php
    $is_reports = stristr(request()->route()->getPrefix(), 'reports/');
@endphp

<table style="overflow:hidden" class="table table-bordered bso_items_table">

    <thead>
    <tr>
        <th rowspan="2" class="text-center">
            <input type="checkbox" name="all_payments">
            (<span class="total_count">{{$payments->count()}}</span>)
        </th>

        @if($is_reports)
            <th rowspan="2">Акт в СК</th>
        @endif

        <th rowspan="2">№ договора</th>
        <th rowspan="2">Продукт</th>
        <th rowspan="2">Страхователь</th>
        <th rowspan="2">Условие продажи</th>
        <th rowspan="2">Агент / Менеджер</th>
        <th rowspan="2">Руководитель</th>
        <th rowspan="2">Тип договора</th>
        <th rowspan="2">Тип платежа</th>
        <th rowspan="2">Поток оплаты</th>
        <th rowspan="2">Квитанция</th>
        <th rowspan="2">Сумма</th>
        <th rowspan="2">Оф.скидка</th>
        <th rowspan="2">Оф.скидка(%)</th>
        <th rowspan="2">Неоф.скидка</th>
        <th rowspan="2">Неоф.скидка(%)</th>
        <th rowspan="2">Дата оплаты</th>
        <th rowspan="2">Статус</th>

        <th rowspan="2">Финансовая политика</th>

        @if($is_reports)
            <th colspan="2">Агент</th>
        @endif

        <th colspan="3">Бордеро</th>
        <th colspan="3">ДВОУ</th>
        <th colspan="2">Маржа</th>

        @if($is_reports)
            <th colspan="3">Даты договора</th>
        @endif


        <th rowspan="2">Маркер</th>

        @if($is_reports)
            <th rowspan="2">Тип акцепта</th>
        @endif
    </tr>
    <tr>

        @if($is_reports)
            <th class="text-center">КВ %</th>
            <th class="text-center">КВ Сумма</th>
        @endif

        <th class="text-center">КВ %</th>
        <th class="text-center">КВ Сумма</th>
        <th class="text-center">Отчет</th>

        <th class="text-center">КВ %</th>
        <th class="text-center">КВ Сумма</th>
        <th class="text-center">Отчет</th>

        <th class="text-center">%</th>
        <th class="text-center">Сумма</th>

        @if($is_reports)
            <th class="text-center">Заключения</th>
            <th class="text-center">Начало</th>
            <th class="text-center">Окончание</th>
        @endif




    </tr>
    </thead>
    <tbody class="payments_table_tbody">
    @if(sizeof($payments))

        @if(isset($payments_sql))
        <tr>
            <th colspan="12"></th>
            <th nowrap>{{titleFloatFormat($payments_sql->sum('payments.payment_total'))}}</th>
            <th nowrap colspan="2">{{titleFloatFormat($payments_sql->sum('payments.official_discount_total'))}}</th>
            <th nowrap colspan="2">{{titleFloatFormat($payments_sql->sum('payments.informal_discount_total'))}}</th>
            <th nowrap colspan="3"></th>
            @if($is_reports)
                <th nowrap>{{titleFloatFormat($payments_sql->sum('payments.financial_policy_kv_agent_total'))}}</th>
            @endif
            <th nowrap colspan="3">{{titleFloatFormat($payments_sql->sum('payments.financial_policy_kv_bordereau_total'))}}</th>
            <th nowrap colspan="3">{{titleFloatFormat($payments_sql->sum('payments.financial_policy_kv_dvoy_total'))}}</th>
            <th nowrap colspan="5"></th>
        </tr>
        @endif

        @foreach($payments as $payment)

            @php
                $base = $payment->base_payment;
            @endphp

            <tr class="child-vert-middle" @if(strlen($payment->marker_color) > 3) style="background-color: {{$payment->marker_color}}" @elseif($payment->financial_policy_manually_set == 1) style="background-color: #ffe6ff" @endif>
                <td class="text-center">
                    <input class="payment_checkbox" type="checkbox" name="payment[]" value="{{$payment->id}}">
                    <span>#{{$loop->iteration}}</span>
                </td>
                @if($is_reports)
                    <td nowrap>@if($payment->acts_sk_id > 0)
                            <a target="_blank" href="/bso_acts/acts_sk/{{$payment->bso->bso_supplier_id}}/acts/{{$payment->acts_sk_id}}/edit">{{$payment->act_sk->title}} - {{$payment->act_sk->id}}</a>
                        @else
                            Не сформирован
                        @endif
                    </td>
                @endif
                <td nowrap>
                    @if(isset($report_id) && $report_id > 0)
                        <a href="#" onclick="openFancyBoxFrame('{{url("/payment/{$payment->id}/?report_id=$report_id")}}')">{{ $payment->bso ? $payment->bso->bso_title : "" }}</a>
                    @else
                        <a href="#" onclick="openFancyBoxFrame('{{url("/payment/{$payment->id}/")}}')">{{ $payment->bso ? $payment->bso->bso_title : "" }}</a>
                    @endif

                </td>
                <td nowrap>{{ $payment->bso && $payment->bso->product ? $payment->bso->product->title : ""}}</td>
                <td>{{ $payment->Insurer}}</td>

                <td nowrap>
                    @if($payment->contract && isset(\App\Models\Contracts\Contracts::SALES_CONDITION[$payment->contract->sales_condition]))
                        {{\App\Models\Contracts\Contracts::SALES_CONDITION[$payment->contract->sales_condition]}}
                    @endif
                </td>

               <td nowrap>{{($payment->contract && $payment->contract->sales_condition==0)?
                ($payment->agent?$payment->agent->name:''):
                ($payment->manager?$payment->manager->name:'')
                }}</td>
               <td nowrap>{{($payment->parent_agent)?$payment->parent_agent->name:''}}</td>


                <td>{{ ($payment->contract && $payment->contract->type_id>0)?\App\Models\Contracts\Contracts::TYPE[$payment->contract->type_id]:"" }}</td>

               <td>{{ \App\Models\Contracts\Payments::PAYMENT_TYPE[$payment->payment_type] }}</td>
                <td>{{ \App\Models\Contracts\Payments::PAYMENT_FLOW[$payment->payment_flow] }}</td>
                <td>{{ $payment->bso_receipt ? : "" }}</td>


                <td @if($payment->checkBaseData($base, 'payment_total') == false) style="color: red;" @endif nowrap>{{ getPriceFormat($payment->payment_total) }}</td>
                <td @if($payment->checkBaseData($base, 'official_discount') == false) style="color: red;" @endif nowrap>{{ getPriceFormat($payment->official_discount_total) }}</td>
                <td @if($payment->checkBaseData($base, 'official_discount') == false) style="color: red;" @endif>{{ getPriceFormat($payment->official_discount) }}</td>
                <td @if($payment->checkBaseData($base, 'informal_discount') == false) style="color: red;" @endif nowrap>{{ getPriceFormat($payment->informal_discount_total) }}</td>
                <td @if($payment->checkBaseData($base, 'informal_discount') == false) style="color: red;" @endif>{{ getPriceFormat($payment->informal_discount) }}</td>

                <td class="text-center">{{ getDateFormatRu($payment->payment_data) }}</td>

                <td>{{ \App\Models\Contracts\Payments::STATUS[$payment->statys_id] }}</td>

                <td>{{($payment->financial_policy_manually_set == 0)?
                ($payment->financial_policy)?$payment->financial_policy->title:''
                :''}}</td>

                @if($is_reports)

                    <td @if($payment->checkBaseData($base, 'financial_policy_kv_agent') == false) style="color: red;" @endif nowrap>{{ getPriceFormat($payment->financial_policy_kv_agent) }}</td>

                    <td @if($payment->checkBaseData($base, 'financial_policy_kv_agent') == false) style="color: red;" @endif nowrap>{{ getPriceFormat($payment->financial_policy_kv_agent_total) }}</td>
                @endif

                <td @if($payment->checkBaseData($base, 'financial_policy_kv_bordereau') == false) style="color: red;" @endif class="text-center">{{ getPriceFormat($payment->financial_policy_kv_bordereau) }}</td>
                <td @if($payment->checkBaseData($base, 'financial_policy_kv_bordereau') == false) style="color: red;" @endif nowrap class="text-center">{{ getPriceFormat($payment->financial_policy_kv_bordereau_total) }}</td>
                <td>
                    @if($payment->reports_border)
                        <a href="{{url("/reports/order/{$payment->reports_order_id}/")}}"
                           target="_blank">{{$payment->reports_border->title}}</a>
                    @endif
                </td>
                <td @if($payment->checkBaseData($base, 'financial_policy_kv_dvoy') == false) style="color: red;" @endif class="text-center">{{ getPriceFormat($payment->financial_policy_kv_dvoy) }}</td>
                <td @if($payment->checkBaseData($base, 'financial_policy_kv_dvoy') == false) style="color: red;" @endif nowrap class="text-center">{{ getPriceFormat($payment->financial_policy_kv_dvoy_total) }}</td>
                <td nowrap>
                    @if($payment->reports_dvou_id > 0)
                        <a href="{{url("/reports/order/{$payment->reports_dvou_id}/")}}"
                           target="_blank">{{$payment->reports_dvoy->title}}</a>
                    @endif
                </td>

                <td @if($payment->checkBaseData($base, 'financial_policy_kv_bordereau') == false) style="color: red;" @endif class="text-center">{{ getPriceFormat($payment->financial_policy_marjing) }}</td>
                <td @if($payment->checkBaseData($base, 'financial_policy_kv_bordereau') == false) style="color: red;" @endif class="text-center">{{ getPriceFormat($payment->financial_policy_marjing_total) }}</td>


                @if($is_reports)

                    <td class="text-center">{{ $payment->contract ? getDateFormatRu($payment->contract->sign_date) : "" }}</td>
                    <td class="text-center">{{ $payment->contract ? getDateFormatRu($payment->contract->begin_date) : "" }}</td>
                    <td class="text-center">{{ $payment->contract ? getDateFormatRu($payment->contract->end_date) : "" }}</td>
                @endif




                <td>{{ $payment->marker_text }}</td>

                @if($is_reports)
                    <td>{{ $payment->contract ? $payment->contract->kind_acceptance_ru('kind_acceptance') : ""}} </td>
                @endif

            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="{{ $is_reports ? '35' : '20' }}" class="text-center">Нет платежей</td>
        </tr>
    @endif
    </tbody>
</table>

