@component('mail::layout')

    @slot('header')
        Ссылка на платежную страницу Полиса
    @endslot

    @component('mail::button', ['url' => $url])
        Перейти
    @endcomponent

    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }}.
        @endcomponent
    @endslot

@endcomponent