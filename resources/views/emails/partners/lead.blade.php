@component('mail::layout')
#Поступил лид

@slot('header')
@endslot

##Информация по лиду:

@component('mail::table')
|             |                                                                    |
|-------------|-------------------------------------------------------------------:|
|**Номер**    |{{$lead->id}}                                                       |
|**Имя**      |{{$lead->name}}                                                     |
|**Телефон**  |{{$lead->phone}}                                                    |
|**Авто**     |{{$lead->car_brand}} {{$lead->car_model}} ({{$lead->object_gosnum}})|
|**Источник** |{{$lead->user_ip}} / {{$lead->user_os}} / {{$lead->user_source}}    |
@endcomponent


@component('mail::promotion')
Скоро должно прийти уведомление о формировании контракта.

Если этого не произошло - значит, пользователь бросил форму на последнем этапе.

Попробуйте связаться с пользователем по предоставленному телефону.
@endcomponent

@component('mail::subcopy')
Обращаем ваше внимание, что телефон по умолчанию тянется из сервиса-партнера и может быть неактуален.
@endcomponent

@slot('footer')
@component('mail::footer')
{{config('app.name')}}©{{date('Y')}}.
@endcomponent
@endslot

@endcomponent