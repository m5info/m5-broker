@component('mail::layout')

    @slot('header')
        {{$title}}
    @endslot

    {{$message}}

    {{ date('d.m.Y') }}

    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }}.
        @endcomponent
    @endslot

@endcomponent