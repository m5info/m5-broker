@component('mail::layout')
#По лиду **{{$lead->id}}** оформлен контракт

@slot('header')
@endslot

@component('mail::button',['url'=>'http://m5back.eurogarant.ru/contracts/online/edit/'.$lead->contract->id])
Перейти в контракт
@endcomponent

##Информация по контракту:

@component('mail::table')
|                  |                                                                    |
|------------------|-------------------------------------------------------------------:|
|**Страхователь**  |{{$lead->contract->insurer->title}}                                 |
|**Телефон**       |{{$lead->contract->insurer->phone}}                                 |
|**Почта**         |{{$lead->contract->insurer->email}}                                 |
|**Авто**          |{{$lead->car_brand}} {{$lead->car_model}} ({{$lead->object_gosnum}})|
|**Требуется ДК**  |{{$lead->need_diagnostics_card ? 'Да' : 'Нет'}}                     |
@endcomponent

@component('mail::promotion')
Пользователь запустил предварительный расчет стоимости полиса от страховых компаний.

Скоро должно прийти уведомление о выборе страховой.

Если уведомления не пришло - значит предложений от страховых не поступило. Это может произойти в случае ошибок данных от сервиса-партнера, некорректной информации от пользователя или исключений по сегментам у страховых. Подробности ошибок можно посмотреть в контракте и отправить альтернативный расчет пользователю по почте.
@endcomponent

@slot('footer')
@component('mail::footer')
    {{config('app.name')}}©{{date('Y')}}.
@endcomponent
@endslot

@endcomponent