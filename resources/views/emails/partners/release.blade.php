@component('mail::layout')
#По лиду **{{$lead->id}}** выбрана страховая {{$lead->statys_id == 4 ? 'и получена ссылка на оплату' : ''}}

@slot('header')
@endslot

@component('mail::button',['url'=>'http://m5back.eurogarant.ru/contracts/online/edit/'.$lead->contract->id])
Перейти в контракт
@endcomponent

##Информация по контракту:

@component('mail::table')
|                  |                                                                    |
|------------------|-------------------------------------------------------------------:|
|**Страхователь**  |{{$lead->contract->insurer->title}}                                 |
|**Телефон**       |{{$lead->contract->insurer->phone}}                                 |
|**Почта**         |{{$lead->contract->insurer->email}}                                 |
|**Авто**          |{{$lead->car_brand}} {{$lead->car_model}} ({{$lead->object_gosnum}})|
|**Требуется ДК**  |{{$lead->need_diagnostics_card ? 'Да' : 'Нет'}}                     |
|**Страховая**     |{{$lead->contract->insurance_companies->title}}                     |
@endcomponent

@component('mail::promotion')

{{$lead->statys_id == 4 ? 'Пользователю осталось только оплатить полис.' : 'При формировании ссылки возникли проблемы, отправьте пользователю ссылку на оплату через систему.'}}

@endcomponent

@slot('footer')
@component('mail::footer')
    {{config('app.name')}}©{{date('Y')}}.
@endcomponent
@endslot

@endcomponent