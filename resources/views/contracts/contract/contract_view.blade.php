@extends('layouts.app')

@section('content')



<div class="page-heading">
    <h2>Выпущенный договор # {{$contract->id}}</h2>
</div>
<div class="row form-horizontal" id="main_container">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <!-- Условия договора -->
        @include('contracts.contract.view.info', ['contract' => $contract])
        <!-- Платежи -->
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="page-subheading">
                <h2 class="inline-h1">Платежи</h2>
                @if($contract->statys_id == 0)
                <span class="btn btn-success btn-right" style="font-size: 10px;" onclick="addTempPayment()">
                    <i class="fa fa-plus"></i>
                </span>
                @endif
            </div>
            <div class="block-view">
                <div class="block-sub">
                    <div class="divider"></div>

                    <div class="row">
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="div_payments">

                            @if(sizeof($contract->payments))
                            @foreach($contract->payments as $key => $payment)
                            @include('contracts.partials.view_payments', ['contract' => $contract, 'payment'=>$payment, 'key'=>$key])
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <!-- Участники договора -->
        @include('contracts.contract.view.insurer_info', ['contract' => $contract])
    </div>

</div>

@endsection
