<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-subheading">
        <h2>Участники договора</h2>
    </div>
    <div class="block-view">
        <div class="block-sub">
            <h3>Cтрахователь</h3>
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <div class="row form-horizontal">

                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                      <span class="view-label">Тип</span>
                      <span class="view-value">{{collect([0=>"ФЛ", 1=>'ЮЛ'])[$contract->insurer->type]}}</span>                                                                                                            
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                      <span class="view-label">@if($contract->insurer->type == 0) ФИО @elseif ($contract->insurer->type == 1) Название @else @endif</span>
                      <span class="view-value">{{$contract->insurer->title}}</span>                                                                                                            
                    </div>
                  </div>

                </div>
            </div>

            @if($contract->insurer->type == 0) 
            <div class="insurer_fl col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <h3>Паспорт</h3>
                <div class="row form-horizontal">
                    
                    
                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field" style="margin-left:15px;">
                      <span class="view-label">Серия</span>
                      <span class="view-value">{{$contract->insurer->doc_serie}}</span>                                                                                                            
                    </div>
                  </div>
                    
                                        
                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field" style="margin-left:15px;">
                      <span class="view-label">Номер</span>
                      <span class="view-value">{{$contract->insurer->doc_number}}</span>                                                                                                            
                    </div>
                  </div>

                </div>
            </div>
            @endif
            @if($contract->insurer->type == 1) 
            <div class="insurer_ul col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="row form-horizontal">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                      <span class="view-label">ИНН</span>
                      <span class="view-value">{{$contract->insurer->inn}}</span>                                                                                                            
                    </div>
                  </div>
                    
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                      <span class="view-label">КПП</span>
                      <span class="view-value">{{$contract->insurer->kpp}}</span>                                                                                                            
                    </div>
                  </div> 

                </div>
            </div>
            @endif


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <div class="row form-horizontal">

                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                      <span class="view-label">Телефон</span>
                      <span class="view-value">{{$contract->insurer->phone}}</span>
                    </div>
                  </div>
                    
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                      <span class="view-label">Email</span>
                      <span class="view-value">{{$contract->insurer->email}}</span>                                                                                                            
                    </div>
                  </div>  

                </div>
            </div>
        </div>

        <div class="block-sub">
            <h3>Объект страхования</h3>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @include('contracts.contract_templates.default.insurance_object.partials.view.'.$contract->bso->product->category->template, ['key' => 0, 'object' => $object_insurer])

            </div>
        </div>

        <div class="block-sub">

            <h3>Документы</h3>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <div class="row form-horizontal">

                    @include('contracts.partials.contract_document', ['contract' => $contract, 'hold_kv_product' => $contract->bso->supplier->hold_kv_product($contract->product_id)])

                </div>
            </div>
        </div>

        <h3>&nbsp;</h3>


    </div>
</div>