<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-subheading">
        <h2>Условия договора</h2>
    </div>
    <div class="block-view">
        <div class="block-sub">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">Андеррайтер</span>
                        <span class="view-value">{{ $contract->check_user ? $contract->check_user->name : ""}}</span>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">Номер договора</span>
                        <span class="view-value">{{$contract->bso->bso_title}}</span>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                      <span class="view-label">Заявка из фронт офиса</span>
                      <span class="view-value">{{$contract->order_title}}</span>                                                                                                            
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                      <span class="view-label">Дата заключения</span>
                      <span class="view-value">{{setDateTimeFormatRu($contract->sign_date, 1)}}</span>                                                                                                            
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                      <span class="view-label">Период действия</span>
                      <span class="view-value">{{setDateTimeFormatRu($contract->begin_date, 1)}} - {{setDateTimeFormatRu($contract->end_date, 1)}}</span>                                                                                                            
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                      <span class="view-label">Условие продажи</span>
                      <span class="view-value">{{collect(\App\Models\Contracts\Contracts::SALES_CONDITION)[$contract->sales_condition]}}</span>                                                                                                            
                    </div>
                </div>
                
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                      <span class="view-label">Менеджер/Агент</span>
                      <span class="view-value">
                          @if ($contract->manager_id)
                          {{$agents[$contract->manager_id]}} 
                           @if($contract->is_personal_sales == 1) 
                           (личная продажа)
                           @endif
                          @else
                          нет
                          @endif
                      </span>                                                                                                            
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                        <span class="view-label">Рекомендодатель</span>
                        <span class="view-value">
                          @if ($contract->referencer_id)
                                {{$referencers[$contract->referencer_id]}}
                          @else
                                нет
                          @endif
                      </span>
                    </div>
                </div>

                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                      <span class="view-label">Премия по договору</span>
                      <span class="view-value">
                      @if($contract->payment_total>0)
                           {{titleFloatFormat($contract->payment_total)}}
                      @else
                           отсутствует
                      @endif
                      </span>                                                                                                            
                    </div>
                </div>
                

                @if(auth()->user()->hasPermission('contracts', 'select_financial_policy'))
                  @if($contract->financial_policy_kv_bordereau || $contract->financial_policy_kv_dvoy || $contract->financial_policy_kv_agent || $contract->financial_policy_kv_parent)
                     <h3>Финансовая политика</h3>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                      
                       <div class="view-field">
                        <span class="view-label">Бордеро</span>
                        <span class="view-value">{{$contract->financial_policy_kv_bordereau}}</span>                                                                                                            
                       </div>
                        
                       <div class="view-field">
                        <span class="view-label">ДВОУ</span>
                        <span class="view-value">{{$contract->financial_policy_kv_dvoy}}</span>                                                                                                            
                       </div>
                        
                       <div class="view-field">
                        <span class="view-label">КВ Агента</span>
                        <span class="view-value">{{$contract->financial_policy_kv_agent}}</span>                                                                                                            
                       </div>
                       <div class="view-field">
                        <span class="view-label">КВ Руков.</span>
                        <span class="view-value">{{$contract->financial_policy_kv_parent}}</span>                                                                                                            
                       </div> 
            </div>
                  @endif
                @endif

                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="view-field">
                      <span class="view-label">Алгоритм рассрочки</span>
                      <span class="view-value">
                      @if($contract->installment_algorithms_id)
                          {{ \App\Models\Directories\InstallmentAlgorithms::find($contract->installment_algorithms_id)->algorithm->title }}
                      @else
                           отсутствует
                      @endif
                      </span>                                                                                                            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>