@extends('layouts.app')


@section('head')
    <style>
        .calendar-icon{
            margin-top: -25px;
            margin-right: 8px;
            float: right;
            color: #a3a3a3;
        }
        .form-error{border-color:red !important;}


    </style>

@endsection


@section('content')


    <div class="page-heading">
        <h2 class="inline-h1">
            @if($contract->statys_id == 4)
            Выпущенный договор
            @else
                Временный договор
            @endif
            # {{$contract->id}}

            @if($contract->statys_id == 0)
                <span class="btn btn-primary btn-right" id="controlSaveButtonBox" onclick="saveContract()">
                    Сохранить
                </span>
                <span class="btn btn-success btn-right" onclick="sendCheckContract()">
                    В проверку
                </span>

            @endif
        </h2>
        @if(auth()->user()->hasPermission('contracts', 'verification'))

            <div class="row">
                <span class="btn btn-success btn-right" onclick="acceptContract()" style="margin-right: 3%">Акцепт</span>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pull-right">
                    {{ Form::select(
                        'kind_acceptance',
                        collect(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE)->prepend('Не выбрано', -1),
                        $contract->kind_acceptance>=0 ? $contract->kind_acceptance : -1,
                        ['class' => 'form-control select2-all pull-right kind_acceptance']
                    ) }}
                </div>
            </div>

        @endif

    </div>


    <div class="row form-horizontal" id="main_container">

        @if(($contract->statys_id == 1 || $contract->statys_id == 3) && auth()->user()->hasPermission('contracts', 'verification'))
        <!-- Техандерайтинг -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="page-subheading">
                <h2>Техандерайтинг</h2>
            </div>
            <div class="block-main">
                <div class="block-sub">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label>Комментарий</label>
                            {{ Form::textarea('contract[0][error_accept]', $contract->error_accept, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <span class="btn btn-danger pull-left" onclick="errorContract()">Выставить ошибку</span>
                            <span class="btn btn-right btn-primary save_temp_contract">Сохранить</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if($contract->statys_id == 2)
            <!-- Техандерайтинг -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="page-subheading">
                    <h2>Техандерайтинг</h2>
                </div>
                <div class="block-main">
                    <div class="block-sub">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <label>Комментарий ({{$contract->check_user->name}})</label>
                                <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" style="color: red;">
                                {{$contract->error_accept}}
                                </div>
                            </div>
                        </div>
                       @if(!auth()->user()->hasPermission('contracts', 'verification'))
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <span class="btn btn-success pull-left" onclick="sendCheckContract()">Исправил</span>

                            </div>
                        </div>
                       @endif
                    </div>
                </div>
            </div>
        @endif



        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <!-- Условия договора -->
            @include('contracts.contract.edit.info', ['contract' => $contract])

            <!-- Платежи -->
            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="page-subheading">
                    <h2 class="inline-h1">Платежи</h2>
                    <span class="btn btn-success btn-right" style="font-size: 10px;" onclick="addPayment()">
                        <i class="fa fa-plus"></i>
                    </span>
                </div>
                <div class="block-main">
                    <div class="block-sub">
                        <div class="divider"></div>

                        <div class="row">
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="div_payments">
                                @if(sizeof($contract->temp_payments))
                                    @foreach($contract->temp_payments as $key => $payment)
                                        @include('contracts.partials.temp_payments', ['contract' => $contract, 'payment'=>$payment, 'key'=>$key])
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <!-- Участники договора -->
            @if($contract->is_online)
                @include('contracts.contract.edit.documents', ['contract' => $contract])
            @else
                @include('contracts.contract.edit.insurer_info', ['contract' => $contract])
            @endif
        </div>

        
        @if($contract->is_online)
            <div class="col-xs-12">
                <h2 class="inline-h1">Данные для оформленных онлайн</h2>
                @if (View::exists('contracts.online.forms.'.$contract->product->slug))
                    <form id="product_form">
                        <input type='hidden' id='calculation_id' value='{{isset($contract->calculation->id) ? $contract->calculation->id :''}}'>
                        @include('contracts.online.forms.'.$contract->product->slug, [])
                    </form>
                @else
                    <p>Оформление невозможно. Форма для продукта отсутствует</p>
                @endif

            </div>
        @endif
    </div>

    <div class="page-heading">
        <span class="btn btn-primary btn-left save_temp_contract">Сохранить</span>
        <span class="btn btn-success btn-right" onclick="acceptContract()" >Акцепт</span>
    </div>

@endsection



@section('js')

    @if($contract->is_online)
        <script src="/js/jquery.datetimepicker.full.min.js"></script>
        <link rel="stylesheet" href="/css/jquery.datetimepicker.min.css">
        <script src="/js/calculation.js"></script>
        <link rel="stylesheet" href="/css/calculation.css">
    @endif

<script>
    $(function () {

        $(document).on('click', '[data-create_receipt]', function () {
            var st = $(document).scrollTop();
            var key = $(this).data('create_receipt');
            var payment_id = $('[name="contract[0][payment][' + key + '][id]"]').val();
            openFancyBoxFrame('/contracts/temp_contracts/create_receipt_frame?payment_id=' + payment_id + '&key=' + key)
            setTimeout(function () {
                $(document).scrollTop(st)
            }, 0);
        });

        $(document).on('click', '.save_temp_contract', function () {
            saveContract(function (res) {
                flashHeaderMessage('Успешно сохранено!', 'success');
            })
        });

        $('.class_bso_receipt').each(function () {
            key = $(this).attr("id").split('_');
            activSearchBso("bso_receipt", "_" + key[2], 2);

        });


        initInfo();
        initInsurer();


        $("input, select").change(function () {
            $(this).removeClass('form-error');
        });

    });


    function selectBso(object_id, key, type, suggestion) {
        var data = suggestion.data;
        $("#bso_receipt_id" + key).val(data.bso_id);
    }




    function initDuplicates() {
        var items = [];
        $("input, select").each(function () {
            if (items[$(this).attr('name')]) {
                var duplicate = $("input[name='" + $(this).attr('name') + "']");
                if (duplicate[1]) {
                    $("input[name='" + $(this).attr('name') + "']").change(function () {
                        let old = $(this);
                        $(duplicate).each(function () {
                            $(this).val(old.val())
                        });
                    });
                }
            }
            items[$(this).attr('name')] = 1;
        });
    }

    document.addEventListener("DOMContentLoaded", function (event) {
        initDuplicates();
    });

    function sendCheckContract() {
        if (saveContract() == true) {
            var response = myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/send_check/")}}");
            if (response) {

                if (response.state == true) {
                    reload();
                } else {
                    $("#messages").html('');
                    $.each(response.msg, function (index, value) {
                        $('<div class="alert alert-danger"><button class="close" data-close="alert"></button>' + value + '</div>').appendTo("#messages");
                    });

                    $("#messages").fadeTo(2000, 500).slideUp(500, function () {
                        $("#messages").slideUp(500);
                        $("#messages").html();
                    });

                    if (response.errors) {
                        $.each(response.errors, function (index, value) {
                            $(value).addClass('form-error');
                        });
                    }

                }
            }
        }
    }

    @if(($contract->statys_id == 1 || $contract->statys_id == 3) && auth()->user()->hasPermission('contracts', 'verification'))

        function errorContract() {
            var text = $('[name*="error_accept"]').val();

            if (text.length > 5) {
                if (saveContract() == true) {
                    if (myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/error_contract/")}}")) {
                        reload();
                    }
                }
            } else {
                flashHeaderMessage('Введите комментарий к ошибке! (мин. 5 символов)', 'danger')
            }
        }


        function acceptContract() {

            var accept_select = $('[name="kind_acceptance"]');
            var accept_link = $('div.kind_acceptance a');

            if (parseInt(accept_select.val()) < 0) {

                var old = accept_link.css("border");
                accept_link.css("border-color", "red");
                setTimeout(function () {
                    accept_link.css("border", old);
                }, 2000);

                flashHeaderMessage('Необходимо выбрать тип акцепта', 'danger')

            } else {

                if (saveContract() == true) {

                    loaderShow();
                    var data_fields = $("#main_container").find("select, textarea, input");
                    data_fields.push($('[name="kind_acceptance"]')[0]);

                    $.ajax({
                        type: "POST",
                        url: "{{url("/contracts/temp_contracts/contract/{$contract->id}/accept_contract")}}",
                        async: false,
                        data: data_fields.serialize(),
                        success: function (response) {

                            $.each($('input, select'), function (index, value) {
                                $(value).removeClass('form-error');
                            });


                            if (response.state == true) {
                                //Перенаправляем пользователя обратно
                                window.location = "{{url("/contracts/verification/")}}";

                            } else {
                                $("#messages").html('');
                                $.each(response.msg, function (index, value) {
                                    $('<div class="alert alert-danger"><button class="close" data-close="alert"></button>' + value + '</div>').appendTo("#messages");
                                });

                                $("#messages").fadeTo(2000, 500).slideUp(500, function () {
                                    $("#messages").slideUp(500);
                                    $("#messages").html();
                                });

                                if (response.errors) {
                                    $.each(response.errors, function (index, value) {
                                        $(value).addClass('form-error');
                                    });
                                }
                            }
                        }
                    }).always(function () {
                        loaderHide();
                    });
                }
            }
        }

    @endif


    function addPayment() {
        if (saveContract() == true) {
            if (myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/add_pay/")}}")) {
                reload();
            }
        }
    }

    function deleteTempPayment(pay) {
        if (saveContract() == true) {
            if (myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/del_pay/?pay=")}}" + pay)) {
                reload();
            }
        }
    }


    function viewSetBsoNotReceipt(obj, key) {
        if ($(obj).is(':checked')) {
            $("#bso_receipt_" + key).attr('disabled', 'disabled');
            $("#bso_receipt_" + key).removeClass('valid_fast_accept');
        } else {
            $("#bso_receipt_" + key).removeAttr('disabled');
            $("#bso_receipt_" + key).addClass('valid_fast_accept');
        }
        $("#bso_receipt_" + key).css("border-color", "");
    }

    @if($contract->statys_id == 4)

        $('#main_container input').each(function () {
            if ($(this).is(":visible")) {
                if ($(this).is(':checkbox')) {
                    if ($(this).val()) {
                        $(this).replaceWith('<span style="padding:3px;">Да</span >');
                    } else {
                        $(this).replaceWith('<span  style="padding:3px;">Нет</span >');
                    }
                } else {
                    $(this).replaceWith('<p style="padding-top:5px;">' + $(this).val() + '</p>');
                }
            }
        });

        $('#main_container select').prop('disabled', true);


    @endif

</script>


@endsection