<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-subheading">
        <h2>Участники договора</h2>
    </div>
    <div class="block-main">
        <div class="block-sub">
            <h3>Cтрахователь</h3>
            <input type="hidden" name="contract[0][insurer][id]" value="{{$contract->insurer_id}}"/>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <div class="row form-horizontal">

                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" >
                        <label class="control-label">Тип</label>
                        {{ Form::select('contract[0][insurer][type]', collect([0=>"ФЛ", 1=>'ЮЛ']), ($contract->insurer)?$contract->insurer->type:0, ['class' => 'form-control select2-ws', 'id'=>'insurer_type_0', 'data-key'=>'0']) }}
                    </div>

                    <div class="insurer_fl col-xs-12 col-sm-12 col-md-9 col-lg-9" >
                        <label class="control-label">ФИО</label>
                        {{ Form::text('contract[0][insurer][fio]', ($contract->insurer)?$contract->insurer->title:'', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_fio_0', 'data-key'=>'0']) }}
                    </div>

                    <div class="insurer_ul col-xs-12 col-sm-12 col-md-9 col-lg-9" style="display: none">
                        <label class="control-label">Название</label>
                        {{ Form::text('contract[0][insurer][title]', ($contract->insurer)?$contract->insurer->title:'', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_title_0', 'data-key'=>'0']) }}
                    </div>

                </div>
            </div>

            <div class="insurer_fl col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <label class="control-label">Паспорт</label>
                <div class="row form-horizontal">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                        {{ Form::text('contract[0][insurer][doc_serie]', ($contract->insurer)?$contract->insurer->doc_serie:'', ['id' => 'insurer_doc_serie_0','class' => 'form-control', 'placeholder'=>'Серия']) }}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                        {{ Form::text('contract[0][insurer][doc_number]', ($contract->insurer)?$contract->insurer->doc_number:'', ['id' => 'insurer_doc_number_0','class' => 'form-control', 'placeholder'=>'Номер']) }}
                    </div>
                </div>
            </div>

            <div class="insurer_ul col-xs-12 col-sm-12 col-md-12 col-lg-12"  style="display: none">
                <div class="row form-horizontal">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                        <label class="control-label">ИНН</label>
                        {{ Form::text('contract[0][insurer][inn]', ($contract->insurer)?$contract->insurer->inn:'', ['class' => 'form-control', 'id'=>'insurer_inn_0', 'data-key'=>'0']) }}
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                        <label class="control-label">КПП</label>
                        {{ Form::text('contract[0][insurer][kpp]', ($contract->insurer)?$contract->insurer->kpp:'', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                    </div>

                </div>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <div class="row form-horizontal">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                        <label class="control-label">Телефон</label>
                        {{ Form::text('contract[0][insurer][phone]', ($contract->insurer)?$contract->insurer->phone:'', ['id' => 'insurer_phone_0','class' => 'form-control phone']) }}
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                        <label class="control-label">Email</label>
                        {{ Form::text('contract[0][insurer][email]', ($contract->insurer)?$contract->insurer->email:'', ['id' => 'insurer_email_0','class' => 'form-control',]) }}
                    </div>

                </div>
            </div>
        </div>

        <div class="block-sub">
            <h3>Объект страхования</h3>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if($object_insurer)
                @include('contracts.contract_templates.default.insurance_object.partials.edit.'.$contract->bso->product->category->template, ['key' => 0, 'object' => $object_insurer])
                @endif
            </div>
        </div>

        @php
        
        $hold_kv_product = $contract->bso->supplier->hold_kv_product($contract->product_id);
        
        @endphp
        @if($hold_kv_product)
        <div class="block-sub">

            <h3>Документы</h3>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <div class="row form-horizontal">

                    @include('contracts.partials.contract_document', ['contract' => $contract, 'hold_kv_product' => $contract->bso->supplier->hold_kv_product($contract->product_id)])

                </div>
            </div>
        </div>
        @endif
        <h3>&nbsp;</h3>
        

    </div>
</div>

<script>

    function initInsurer()
    {
        $('#insurer_type_0').change(function () {

            if(parseInt($(this).val()) == 0){
                $('.insurer_fl').show();
                $('.insurer_ul').hide();
            }else{
                $('.insurer_fl').hide();
                $('.insurer_ul').show();
            }

        });

        $('#insurer_type_0').change();

        $('#insurer_fio_'+0).suggestions({
            serviceUrl: '{{url("/custom_suggestions/suggest_physical/")}}',
            token: DADATA_TOKEN,
            type: "NAME",
            count: 5,
            onSelect: function (suggestion) {
                var data = suggestion.data;
                console.log(data);
                key = $(this).data('key');
                
                $('#insurer_title_'+key).val($(this).val());
                $('#insurer_doc_serie_'+key).val(data.doc_serie); 
                $('#insurer_doc_number_'+key).val(data.doc_number); 
                $('#insurer_phone_'+key).val(data.phone); 
                $('#insurer_email_'+key).val(data.email);
                
            }
        });

        $('#insurer_title_'+0+', #insurer_inn_'+0+', #insurer_kpp_'+0).suggestions({
            serviceUrl: '{{url("/custom_suggestions/suggest_jure/")}}',
            token: DADATA_TOKEN,
            type: "PARTY",
            count: 5,
            onSelect: function (suggestion) {
                var data = suggestion.data;
                console.log(data);
                key = $(this).data('key');

                $('#insurer_title_'+key).val(suggestion.value);
                $('#insurer_inn_'+key).val(data.inn);
                $('#insurer_kpp_'+key).val(data.kpp);
                $('#insurer_fio_'+key).val($('#insurer_title_'+key).val());
                $('#insurer_phone_'+key).val(data.phone); 
                $('#insurer_email_'+key).val(data.email);

            }
        });


        initDocuments();

    }

    function getModelsObjectInsurer(KEY, select_model_id)
    {

        $.getJSON('{{url("/contracts/actions/get_models")}}', {categoryId:$('#object_insurer_ts_category_'+KEY).val(),markId: $('#object_insurer_ts_mark_id_'+KEY).select2('val')}, function (response) {


            var options = "<option value='0'>Не выбрано</option>";
            response.map(function (item) {
                options += "<option value='" + item.id + "'>" + item.title + "</option>";
            });
            $('#object_insurer_ts_model_id_'+KEY).html(options).select2('val', select_model_id);


        });

    }



</script>