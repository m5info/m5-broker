<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-subheading">
        <h2>Документы</h2>
    </div>
    <div class="block-main">
        @php
            $hold_kv_product = $contract->bso->supplier->hold_kv_product($contract->product_id);
        @endphp

        @if($hold_kv_product)
            <div class="block-sub">
                <h3>Документы</h3>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <div class="row form-horizontal">
                        @include('contracts.partials.contract_document', ['contract' => $contract, 'hold_kv_product' => $contract->bso->supplier->hold_kv_product($contract->product_id)])

                    </div>
                </div>
            </div>
        @else
            <div class="block-sub">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <p>Для продукта {{$contract->product->title}} не требуются документы</p>
                </div>
            </div>
        @endif

    </div>
</div>

<script>
    function initInsurer() {
        initDocuments();
    }
</script>
