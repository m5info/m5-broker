<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 temp_contract" id="contract_[:KEY:]" data-key="[:KEY:]">
    <div class="trucks-item order-item">
        <div class="info">
            Договор <span class="number">[:NUM:]</span>

            <button onclick="deleteContract('[:KEY:]')" style="background-color: #FFF;font-size: 15px;margin-top: -5px;border: none;" class="pull-right view_but" type="button">
                <i class="fa fa-remove" style="color: red;"></i>
            </button>

        </div>
        <div class="divider"></div>
        <div class="info">

            <div class="form-group">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-4" >
                    <h3>Договор (полис)</h3>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >

                        <label class="control-label pull-left" >
                            Номер договора <span style="color:red">*</span>
                        </label>
                        <label class="control-label pull-right">
                            Изъять документы <input type="checkbox" value="1" checked name="contract[[:KEY:]][withdraw_documents]" />
                        </label>


                        {{ Form::text('contract[[:KEY:]][bso_title]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'bso_title_[:KEY:]', 'onchange'=>'setContractValue([:KEY:]);'])}}
                        <input type="hidden" name="contract[[:KEY:]][bso_id]" id="bso_id_[:KEY:]" />
                        <input type="hidden" name="contract[[:KEY:]][bso_supplier_id]" id="bso_supplier_id_[:KEY:]" />
                        <input type="hidden" name="contract[[:KEY:]][insurance_companies_id]" id="insurance_companies_id_[:KEY:]" />
                        <input type="hidden" name="contract[[:KEY:]][product_id]" id="product_id_[:KEY:]" />
                        <input type="hidden" name="contract[[:KEY:]][agent_id]" id="agent_id_[:KEY:]" />
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label">Заявка из фронт офиса</label>
                        {{ Form::text('contract[[:KEY:]][order_title]', '', ['class' => 'form-control', 'id'=>'order_title_[:KEY:]']) }}
                        <input type="hidden" name="contract[[:KEY:]][order_id]" id="order_id_[:KEY:]" />
                        <input type="hidden" name="contract[[:KEY:]][order_sort_id]" id="order_sort_id_[:KEY:]" />
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label">
                            Дата заключения <span style="color:red">*</span>
                        </label>
                        {{ Form::text('contract[[:KEY:]][sign_date]', '', ['class' => 'form-control datepicker date valid_fast_accept', 'id'=>'sign_date_[:KEY:]', 'onchange'=>'setPaymentDates([:KEY:]); refreshOptionFinancialPolicy(this, "[:KEY:]")']) }}
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label class="control-label">
                            Период действия <span style="color:red">*</span>
                        </label>
                        <div class="row form-horizontal">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                {{ Form::text('contract[[:KEY:]][begin_date]', '', ['class' => 'form-control date datepicker valid_fast_accept', 'id'=>'begin_date_[:KEY:]', 'placeholder'=>'Дата начала', 'onchange'=>'setEndDates([:KEY:])']) }}
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                {{ Form::text('contract[[:KEY:]][end_date]', '', ['class' => 'form-control date datepicker valid_fast_accept', 'placeholder'=>'Дата окончания', 'id'=>'end_date_[:KEY:]']) }}
                            </div>
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label">Условие продажи</label>
                        {{ Form::select('contract[[:KEY:]][sales_condition]', collect(\App\Models\Contracts\Contracts::SALES_CONDITION), 0, ['class' => 'form-control select2-ws', 'id'=>'sales_condition_[:KEY:]', 'data-key' => "[:KEY:]", 'onchange' => 'isPersonalSales([:KEY:])']) }}
                    </div>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="is_personal_sales_div_[:KEY:]" style="display: none;">
                        <label class="control-label pull-left" style="margin-top: 5px;">
                            Менеджер
                        </label>
                        <label class="control-label pull-right">Личная продажа
                            <input type="checkbox" value="1" id="is_personal_sales_[:KEY:]" name="contract[[:KEY:]][is_personal_sales]" data-key="[:KEY:]"/>
                        </label>
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right: 0px;" >
                            {{ Form::select('contract[[:KEY:]][manager_id]', $agents->prepend('Выберите значение', 0), 0, ['class' => 'form-control  select2-ws-min-3', 'id'=>'manager_id_[:KEY:]', 'onchange' => 'removeBorder("#s2id_manager_id_[:KEY:]")']) }}
                        </div>
                    </div>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-4" >
                    <h3>Порядок оплаты</h3>
                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label class="control-label">
                                    Премия по договору <span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][payment_total]', '', ['class' => 'form-control sum valid_fast_accept', 'id'=>'payment_total_[:KEY:]', 'onkeyup'=>'setPaymentQty([:KEY:])', 'onchange'=>'setPaymentQty_removeBorder([:KEY:])']) }}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label class="control-label">
                                    Дата оплаты <span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][payment][0][payment_data]', '', ['class' => 'form-control date datepicker valid_fast_accept', 'id'=>'payment_data_[:KEY:]']) }}

                            </div>
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label class="control-label">Алгоритм рассрочки</label>
                        {{ Form::select('contract[[:KEY:]][installment_algorithms_id]', collect([0=>'Укажите номер договора']), 0, ['class' => 'form-control select2-ws', 'id'=>'installment_algorithms_id_[:KEY:]', 'onchange' => 'setAlgos([:KEY:])']) }}
                    </div>

                    <div id='payment_algo_[:KEY:]' class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label class="control-label">
                                    Сумма платежа <span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][payment][0][payment_total]', '', ['class' => 'form-control sum valid_fast_accept', 'id'=>'payment_total_[:KEY:]']) }}
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <div class="row form-horizontal">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                        <label class="control-label">
                                            Взнос № <span style="color:red">*</span>
                                        </label>
                                        {{ Form::text('contract[[:KEY:]][payment][0][payment_number]', 1, ['class' => 'form-control valid_fast_accept', 'id'=>'payment_number_[:KEY:]']) }}
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                        <label class="control-label">Тип</label>
                                        {{ Form::select('contract[[:KEY:]][payment][0][type_id]', collect(\App\Models\Contracts\Payments::TRANSACTION_TYPE), 0, ['class' => 'form-control select2-ws']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                <label class="control-label">Офици. %</label>
                                {{ Form::text('contract[[:KEY:]][payment][0][official_discount]', '', ['class' => 'form-control sum percents_input_validation', 'id'=>'official_discount_[:KEY:]']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                <label class="control-label">Неофиц. %</label>
                                {{ Form::text('contract[[:KEY:]][payment][0][informal_discount]', '', ['class' => 'form-control sum percents_input_validation', 'id'=>'informal_discount_[:KEY:]']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                <label class="control-label">Банк %</label>
                                {{ Form::text('contract[[:KEY:]][payment][0][bank_kv]', '', ['class' => 'form-control sum percents_input_validation', 'id'=>'bank_kv_[:KEY:]', 'onkeyup' => 'setBankSelect([:KEY:])']) }}
                            </div>
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">
                            <div class="toggle-bank-[:KEY:] col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden">
                                <label class="control-label">Банк</label>
                                {{ Form::select('contract[[:KEY:]][bank_id]', collect(\App\Models\Settings\Bank::all())->pluck('title', 'id')->prepend('Не выбрано', 0), '', ['class' => 'form-control select2-all', 'id'=>'bank_id_0']) }}
                            </div>
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label pull-left" style="margin-top: 5px;" id="check_label_[:KEY:]">
                            Вид оплаты
                        </label>
                        {{ Form::select('contract[[:KEY:]][payment][0][pay_method_id]', \App\Models\Finance\PayMethod::query()->where('is_actual', 1)->pluck('title', 'id'), 0, ['class' => 'form-control valid_fast_accept', 'id'=>'check_[:KEY:]', 'data-key' => '[:KEY:]']) }}
                        <script>
                            var pay_methods_id2types = {
                                @foreach(\App\Models\Finance\PayMethod::query()->where('is_actual', 1)->get() as $method)
                                {!! "$method->id : $method->key_type," !!}
                                @endforeach
                            };
                        </script>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="receipt_block_[:KEY:]">
                        <label class="control-label pull-left" style="margin-top: 5px;" id="bso_receipt_label_[:KEY:]">
                            Квитанция<span style="color:red">*</span>
                            @if(auth()->user()->hasPermission('contracts', 'show_e_policy'))
                                <a href="javascript:void(0);" class="btn-xs btn-primary" data-create_receipt="[:KEY:]" style="display: inline">Е-квит</a>
                            @endif
                        </label>
                        <label class="control-label pull-right">Без квитанции
                            <input type="checkbox" value="100" id="check_[:KEY:]" name="contract[[:KEY:]][pay_method_id]" onclick="disableReceiptField([:KEY:])" data-key="[:KEY:]"/>
                        </label>
                        {{ Form::text('contract[[:KEY:]][payment][0][bso_receipt]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'bso_receipt_[:KEY:]', 'data-key' => '[:KEY:]']) }}
                        <input type="hidden" name="contract[[:KEY:]][payment][0][bso_receipt_id]" id="bso_receipt_id_[:KEY:]" />
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="check_block_[:KEY:]" style="display: none">
                        <div class="row form-horizontal">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label class="control-label">Телефон</label>
                                {{ Form::text('contract[[:KEY:]][check_phone]', '', ['class' => 'form-control phone', 'id'=>'check_phone_[:KEY:]',]) }}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label class="control-label">E-mail</label>
                                {{ Form::text('contract[[:KEY:]][check_email]', '', ['class' => 'form-control email', 'id'=>'check_email_[:KEY:]']) }}
                            </div>
                        </div>

                        <div class="row form-horizontal">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <label class="control-label">Отправить</label>
                                {{ Form::select('contract[[:KEY:]][when_to_send]', collect(\App\Models\Api\Atol\AtolCheck::WHEN_TO_SEND), 1, ['class' => 'form-control', 'id'=>'when_to_send_[:KEY:]',]) }}
                            </div>
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label pull-left" style="margin-top: 5px;">
                            Финансовая политика
                        </label>
                        <label class="control-label pull-right">Указать в ручную
                            <input type="checkbox" value="1" name="contract[[:KEY:]][financial_policy_manually_set]" onchange="viewSetFinancialPolicyManually(this, '[:KEY:]')"/>
                        </label>

                        <div class="wraper-inline-100">
                            {{ Form::select('contract[[:KEY:]][financial_policy_id]', collect([0=>'Укажите номер договора']), 0, ['class' => 'form-control select2-ws valid_fast_accept', 'id'=>'financial_policy_id_[:KEY:]', 'key' => '[:KEY:]']) }}
                        </div>
                    </div>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
                        <div class="row form-horizontal" id="financial_policy_manually_[:KEY:]" style="display: none;">

                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                {{ Form::text('contract[[:KEY:]][financial_policy_kv_bordereau]', '', ['class' => 'form-control sum', 'id'=>'financial_policy_kv_bordereau_[:KEY:]', 'placeholder'=>'Бордеро']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                {{ Form::text('contract[[:KEY:]][financial_policy_kv_dvoy]', '', ['class' => 'form-control sum', 'id'=>'financial_policy_kv_dvoy_[:KEY:]', 'placeholder'=>'ДВОУ']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                {{ Form::text('contract[[:KEY:]][financial_policy_kv_agent]', '', ['class' => 'form-control sum', 'id'=>'financial_policy_kv_agent_[:KEY:]', 'placeholder'=>'Агента']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                {{ Form::text('contract[[:KEY:]][financial_policy_kv_parent]', '', ['class' => 'form-control sum', 'id'=>'financial_policy_kv_parent_[:KEY:]', 'placeholder'=>'Руков.']) }}
                            </div>
                        </div>

                    </div>



                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-4" >
                    <h3>Cтрахователь</h3>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" >
                                <label class="control-label">Тип</label>
                                {{ Form::select('contract[[:KEY:]][insurer][type]', collect([0=>"ФЛ", 1=>'ЮЛ']), 0, ['class' => 'form-control select2-ws', 'id'=>'insurer_type_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                            <div class="insurer_fl col-xs-12 col-sm-12 col-md-9 col-lg-9" >
                                <label class="control-label">
                                    ФИО<span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][insurer][fio]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_fio_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                            <div class="insurer_ul col-xs-12 col-sm-12 col-md-9 col-lg-9" style="display: none">
                                <label class="control-label">
                                    Название<span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][insurer][title]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_title_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                        </div>
                    </div>

                    <div class="row insurer_fl col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label class="control-label">Паспорт</label>
                        <div class="row form-horizontal">
                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" >
                                {{ Form::text('contract[[:KEY:]][insurer][doc_serie]', '', ['class' => 'form-control', 'placeholder'=>'Серия']) }}
                            </div>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" >
                                {{ Form::text('contract[[:KEY:]][insurer][doc_number]', '', ['class' => 'form-control', 'placeholder'=>'Номер']) }}
                            </div>
                        </div>
                    </div>

                    <div class="row insurer_ul col-xs-12 col-sm-12 col-md-12 col-lg-12"  style="display: none">
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label class="control-label">ИНН</label>
                                {{ Form::text('contract[[:KEY:]][insurer][inn]', '', ['class' => 'form-control', 'id'=>'insurer_inn_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label class="control-label">КПП</label>
                                {{ Form::text('contract[[:KEY:]][insurer][kpp]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                        </div>
                    </div>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label class="control-label">Телефон</label>
                                {{ Form::text('contract[[:KEY:]][insurer][phone]', '', ['class' => 'form-control phone', 'id'=>'insurer_phone_[:KEY:]']) }}
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label class="control-label">Email</label>
                                {{ Form::text('contract[[:KEY:]][insurer][email]', '', ['class' => 'form-control', 'id'=>'insurer_email_[:KEY:]']) }}
                            </div>

                        </div>
                    </div>

                    <h3>Объект страхования</h3>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="contract_object_insurer_[:KEY:]">
                        <center><strong>Укажите номер договора</strong></center>
                    </div>

                    @if(auth()->user()->is('under'))
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <h3>Тип акцепта</h3>
                            <div class=" form-horizontal">
                            {{ Form::select('contract[[:KEY:]][kind_acceptance]', collect(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE)->prepend('Не выбрано', -1), -1, ['class' => 'form-control select2-ws pull-right valid_fast_accept', 'data-not_selected' => -1, 'onchange' => 'showCommentField([:KEY:])']) }}
                            </div>
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12 message_[:KEY:] hidden" >
                            <h3>Комментарий</h3>
                            <div class=" form-horizontal work-area-item">
                                {{ Form::textarea('contract[[:KEY:]][message]', '', ['class' => 'form-control', 'rows' => 3]) }}&nbsp;
                            </div>
                        </div>

                    @endif

                </div>

            </div>
        </div>
    </div>
</div>