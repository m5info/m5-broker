@extends('layouts.frame')


@section('title')
    Создать квитанцию
@endsection

@section('content')

    {{ Form::open(['url' => '/contracts/fast_accept/create_receipt/', 'method' => 'post', 'class' => 'form-horizontal']) }}

    {{ Form::hidden('bso_id', $bso_item->id) }}

    <div class="form-horizontal">

        <div class="form-group">
            <label class="col-sm-4 control-label">Серия</label>
            <div class="col-sm-8">
                {{ Form::select('series_id', collect($bso_series->pluck('bso_serie', 'id')), old('series_id'), ['class' => 'form-control', 'id'=>'bso_serie_id']) }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Номер квитанции</label>
            <div class="col-sm-8">
                {{ Form::text('receipt_number', old('rereceipt_number'), ['class' => 'form-control', 'id'=>'bso_title', 'maxlength' => 12, 'required']) }}
            </div>
        </div>
    </div>

    {{Form::close()}}


@endsection

@section('footer')
    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.create') }}</button>
@endsection


@section('js')
    <script>
        $(function () {



        })
    </script>
@append