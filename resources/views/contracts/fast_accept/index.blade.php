@extends('layouts.app')

@section('content')


    <div class="page-heading">
        <h2>
            <span class="btn-left">
                Быстрый акцепт
            </span>

        </h2>


    </div>



    {{ Form::open(['url' => url('/contracts/fast_accept/'), 'method' => 'post',  'class' => 'form-horizontal', 'id' => 'formContract']) }}


    <div class="form-group">
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="control">
        </div>
    </div>

    <div class="form-group" style="margin-left: 0; margin-right: 0;">
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <span class="btn btn-success btn-right" id="controlSaveButtonBox" onclick="saveFastAccept()">
                    Акцепт
                </span>
            <span class="btn btn-primary btn-left" onclick="addContractCongestion()">
                    Добавить
                </span>
        </div>
    </div>

    {{Form::close()}}

@endsection

@section('js')

    <script>
        formValid = false;

        function setPaymentDates(key) {
            sign_date = $("#sign_date_" + key).val();

            $("#payment_data_" + key).val(sign_date);
            removeBorderColor("#payment_data_" + key);
        }

        function setAlgos(key) {

            var element = $("#installment_algorithms_id_" + key);

            if(element.val() > 0){

                var algorithm_id = element.val();

                if (getInputsInstallmentAlgorithms("payment_algo_" + key, algorithm_id, key, 0)) {

                    $.ajax({
                        type: "POST",
                        url: "{{url("/settings/installment_algorithms_list/get_quantity")}}",
                        async: true,
                        data: {'algorithm_id': algorithm_id},
                        success: function (response) {

                            var str = $('[name="contract[' + key + '][payment_total]"]').val().replace(/\s/g, '');
                            var premium = parseInt(str);

                            if (parseInt($('[name="contract[' + key + '][payment_total]"]').val()) > 0) {

                                var payment_sum = premium / response;
                                $('[name="contract[' + key + '][payment][0][payment_total]"]').val(CommaFormatted(payment_sum));

                                var indexes = response + 1;

                                for (var i = 1; i < indexes; i++) {
                                    $('#algo_payment_sum_' + i).val(CommaFormatted(payment_sum));
                                }
                            }
                        }
                    });
                }

            } else {
                $("#payment_algo_" + key).html('');
            }
        }

        function removeBorder(el) {
            $(el).css('border', 'none');
        }

        function updateAlgos(key) {

            var element = $("#installment_algorithms_id_" + key);

            var pay_parts = $('.algo_payment_sum_'+key);
            var total_pay_parts_sum = 0.00;

            pay_parts.each(function(){
                total_pay_parts_sum += parseFloat($(this).val().replaceAll(' ', '').replace(',', '.'));
            });

            var contract_award = $('[name="contract[' + key + '][payment_total]"]').val().replaceAll(' ','').replace(',','.');

            var total = parseFloat(contract_award) - parseFloat(total_pay_parts_sum);

            $('[name="contract['+key+'][payment][0][payment_total]"]').val(CommaFormatted(total));

            if (element.val() > 0) {

                var algorithm_id = element.val();

            } else {
                $("#payment_algo_" + key).html('');
            }
        }

        function setContractValue(key){
            refreshOptionFinancialPolicy($('#sign_date_'+key)[0], key);
        }

        function refreshOptionFinancialPolicy(elem, key) {

            if ($('#bso_id_0').val()) {
                getOptionFinancialPolicy(
                    "financial_policy_id_" + key,
                    $('#insurance_companies_id_'+ key).val(),
                    $('#bso_supplier_id_'+ key).val(),
                    $('#product_id_'+ key).val(),
                    $('#financial_policy_id_'+ key).val(),
                    $(elem).val(),
                );
            }
        }

        $(function () {

            @if($autosave)
                getAutoSavedData();
            @else
                addContractCongestion();
            @endif

            //controlSaveButton();

            $(document).on('change', '[name*="sales_condition"]', function () {
                var key = $(this).attr('data-key');
                var val = parseInt($(this).val());

                if (val === 0) {
                    $('[name="contract[' + key + '][manager_id]"]').removeClass('valid_fast_accept')
                } else {
                    $('[name="contract[' + key + '][manager_id]"]').addClass('valid_fast_accept')
                }
            });


            $(document).on('click', '[data-create_receipt]', function () {
                var st = $(document).scrollTop();
                var key = $(this).data('create_receipt');
                var bso_id = $('#bso_id_' + key).val();
                if (bso_id === '') {
                    flashHeaderMessage('Необходимо выбрать договор', 'danger')
                } else {
                    openFancyBoxFrame('/contracts/fast_accept/create_receipt_frame?bso_id=' + bso_id + '&key=' + key)
                }
                setTimeout(function () {
                    $(document).scrollTop(st)
                }, 0);

            });


            $(document).on('change', '[name*="[is_personal_sales]"]', function () {
                var key = $(this).data('key');
                if ($(this).prop('checked')) {
                    $('#manager_id_' + key).val(0);
                    $('#manager_id_' + key).removeClass('valid_fast_accept').change()
                } else {
                    $('#manager_id_' + key).addClass('valid_fast_accept').change()
                }
            });

            $(document).on('change', '[name*="[pay_method_id]"]', function () {

                var key = $(this).data('key');
                var method_blocks = {
                    0: 'receipt_block_' + key,
                    1: 'check_block_' + key,
                    2: 'none'
                };

                var method = parseInt($(this).val());

                $.each(method_blocks, function (k, v) {
                    $("#" + v).hide()
                });

                $("#" + method_blocks[pay_methods_id2types[method]]).show();

                if (pay_methods_id2types[method] === 0) {
                    $('[name="contract[' + key + '][payment][0][bso_receipt]"]').addClass('valid_fast_accept');
                } else {
                    $('[name="contract[' + key + '][payment][0][bso_receipt]"]').removeClass('valid_fast_accept');
                }

            });

        });

        function autoSave() {
            if (!formValid){ // если не идет отправка данных
                $.ajax({
                    url: '{{ url('contracts/fast_accept/autosave') }}',
                    type: 'POST',
                    data: {
                        data: $('#formContract').serialize(),
                        type_id: 2,
                        count: $('#formContract .temp_contract').length,
                    },
                });
            }
        }

        setInterval(autoSave, 5000);

        var MY_COUNT_TEMP = 0;

        String.prototype.replaceAll = function (search, replace) {
            return this.split(search).join(replace);
        }

        function addContractCongestion() {


            myHtml = myGetAjax("{{url("/contracts/fast_accept/form_fast_acceptance/")}}");

            myHtml = myHtml.replaceAll('[:KEY:]', MY_COUNT_TEMP);
            myHtml = myHtml.replaceAll('[:NUM:]', (MY_COUNT_TEMP + 1));

            $('#control').append(myHtml);

            startContractFunctions(MY_COUNT_TEMP);

            MY_COUNT_TEMP = MY_COUNT_TEMP + 1;

            updateContractIndexes();

            controlSaveButton();

            toggleFlowOptions(MY_COUNT_TEMP - 1);

            $('.select2-ws').select2("destroy").select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true,
                minimumResultsForSearch: -1,
            });

        }

        function getAutoSavedData() {

            flashMessage('success', 'Загружены несохранённые данные');

            myHtml = myGetAjax("{{url("/contracts/fast_accept/get_autosave/")}}");

            $('#control').append(myHtml);

            @if($autosave)
                @for($i=0; $i < $autosave->count; $i++)

                    startContractFunctions({{$i}});

                @endfor
            @endif

            updateContractIndexes();
            controlSaveButton();

            $('.select2-ws').select2("destroy").select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true,
                minimumResultsForSearch: -1,
            });

            MY_COUNT_TEMP = {{ $autosave ? $autosave->count : 0 }};

        }


        function toggleFlowOptions(key) {

            // $.each($('[name*="[payment_type]"]'), function (k,v) {

            // var key = $(v).data('key');


            // if(parseInt($(this).val()) === 0){
            if (parseInt($('[name="contract[' + key + '][payment][0][payment_type]"]').val()) === 0) {

                $('[name="contract[' + key + '][payment][0][bso_not_receipt]"]').prop('checked', true).change();

                $('[name*="[payment_flow]"][data-key="' + key + '"]').html(
                    '<option value="0" selected="selected">Брокер</option>'
                )

                $('[name="contract[' + key + '][payment][0][payment_flow]"]').siblings('.select2-container').find('.select2-chosen').text('Брокер');

            } else if (parseInt($('[name="contract[' + key + '][payment][0][payment_type]"]').val()) === 3) {

                $('[name="contract[' + key + '][payment][0][bso_not_receipt]"]').prop('checked', true).change();


                $('[name*="[payment_flow]"][data-key="' + key + '"]').html(
                    '<option value="1" selected="selected">СК</option>'
                );

                $('[name="contract[' + key + '][payment][0][payment_flow]"]').siblings('.select2-container').find('.select2-chosen').text('СК');

            } else {
                $('[name="contract[' + key + '][payment][0][bso_not_receipt]"]').prop('checked', false).change();

                $('[name*="[payment_flow]"][data-key="' + key + '"]').html(
                    '<option value="0" selected="selected">Брокер</option><option value="1">СК</option>'
                )
            }

            // })

        }


        function deleteContract(id) {
            if (confirm('Удалить договор ?')) {
                $('#contract_' + parseInt(id)).remove();
                updateContractIndexes();
                controlSaveButton();
            }

        }

        function getContract() {
            return $('.temp_contract');
        }


        function updateContractIndexes() {
            var contracts = getContract();
            contracts.each(function (i, contract) {
                $(contract).find('.number').html(i + 1);
            });
        }


        function startContractFunctions(KEY) {
            var bso_used = [];

            $.each($('[name*="][bso_title]"]'), function (k, v) {
                bso_used.push($(v).val());
            });

            activSearchBso("bso_title", '_' + KEY, 1, bso_used);

            activSearchBso("bso_receipt", '_' + KEY, 2, bso_used);

            $('#manager_id_' + KEY).select2({
                width: '100%',
                minimumInputLength: 3,
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true
            });


            $('#insurer_type_' + KEY).change(function () {

                key = $(this).data('key');
                dataForm = $("#contract_" + key);

                type = $(this).val();

                if (parseInt(type) == 0) {
                    $(dataForm).find('.insurer_fl').show();
                    $(dataForm).find('.insurer_ul').hide();
                } else {
                    $(dataForm).find('.insurer_fl').hide();
                    $(dataForm).find('.insurer_ul').show();
                }

            });


            $('#insurer_fio_' + KEY).suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "NAME",
                count: 5,
                onSelect: function (suggestion) {
                    key = $(this).data('key');
                    $('#insurer_title_' + key).val($(this).val());

                }
            });

            $('#insurer_title_' + KEY + ', #insurer_inn_' + KEY + ', #insurer_kpp_' + KEY).suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "PARTY",
                count: 5,
                onSelect: function (suggestion) {
                    var data = suggestion.data;

                    key = $(this).data('key');


                    $('#insurer_title_' + key).val(suggestion.value);
                    $('#insurer_inn_' + key).val(data.inn);
                    $('#insurer_kpp_' + key).val(data.kpp);

                    $('#insurer_fio_' + key).val($('#insurer_title_' + key).val());

                }
            });

            $(document).on('click', '#formContract input[name="contract[' + KEY + '][pay_method_id]"]', function () {
                if ($(this).prop("checked")) {
                    $('#formContract input[name="contract[' + KEY + '][payment][0][bso_receipt]"]').css('border-color', '#e0e0e0');
                }
            });


            $('.sum')
                .change(function () {
                    $(this).val(CommaFormatted($(this).val()));
                })
                .blur(function () {
                    $(this).val(CommaFormatted($(this).val()));
                })
                .keyup(function () {
                    $(this).val(CommaFormatted($(this).val()));
                });

            $('.integer').change(function () {
                $(this).val($(this).val().replace(/\D+/g, ""))
            }).keyup(function () {
                $(this).val($(this).val().replace(/\D+/g, ""))
            });


            $('.phone').mask('+7 (999) 999-99-99');

        }


        function selectBso(object_id, key, type, suggestion) {

            var data = suggestion.data;

            if (parseInt(type) == 1) { // БСО
                $('#bso_id' + key).val(data.bso_id);
                $('#bso_supplier_id' + key).val(data.bso_supplier_id);
                $('#insurance_companies_id' + key).val(data.insurance_companies_id);
                $('#product_id' + key).val(data.product_id);
                $('#agent_id' + key).val(data.agent_id);

                getKvAccesses(key, data.product_id);

                getOptionInstallmentAlgorithms("installment_algorithms_id" + key, data.insurance_companies_id, 0);
                getOptionFinancialPolicy("financial_policy_id" + key, data.insurance_companies_id, data.bso_supplier_id, data.product_id, 0);

                intkey = key.replaceAll('_', '');
                object_insurer = myGetAjax('{{url("/bso/actions/get_html_mini_contract_object_insurer/")}}?product_id=' + data.product_id + '&key=' + intkey);

                //object_insurer = object_insurer.replaceAll('[:KEY:]', parseInt(intkey));

                $("#contract_object_insurer" + key).html(object_insurer);

                startContractObjectInsurerFunctions(parseInt(intkey));

                var receipt_used = [];
                $.each($('[name*="][bso_receipt]"]'), function (k, v) {
                    receipt_used.push($(v).val());
                });

                activSearchBso("bso_receipt", key, 2, receipt_used);

                activSearchOrdersToFront("order_title", key);

            }

            if (parseInt(type) == 2) { // Квитанция
                $('#bso_receipt_id' + key).val(data.bso_id);

            }

        }

        function disableReceiptField(key) {
            if ($('#formContract input[name="contract[' + key + '][pay_method_id]"]').prop('checked')) {
                $('#formContract input[name="contract[' + key + '][payment][0][bso_receipt]"]').attr('disabled', 'disabled');
            } else {
                $('#formContract input[name="contract[' + key + '][payment][0][bso_receipt]"]').removeAttr('disabled');
            }
        }

        function showCommentField(key) {
            if ($('#formContract select[name="contract[' + key + '][kind_acceptance]"]').val() == 0) {
                $('#formContract .message_' + key).removeClass('hidden');
            } else {
                $('#formContract .message_' + key).addClass('hidden');
            }
        }


        function setPaymentQty(key) {

            var con = $('input[name="contract[' + key + '][payment_total]"]').val();
            $('input[name="contract[' + key + '][payment][0][payment_total]"]').val(CommaFormatted(con));
            updateAlgos(key);

        }

        function updatePaymentQty(key) {
            updateAlgos(key);
        }

        function setBankSelect(key) {
            if ($('#bank_kv_' + key).val().length > 0) {
                $('#formContract .toggle-bank-'+key).removeClass('hidden');
            } else {
                $('#formContract .toggle-bank-'+key).addClass('hidden');
            }
        }

        function controlSaveButton() {

            $('.valid_fast_accept').change(function () {

                $(this).css("border-color", "");

                if ($(this)[0].localName === 'select') {
                    var container = $(this).prev('.select2-container');
                    container.css({'border': 'none'});
                }
            });

            if (getContract().length > 0) {
                $("#controlSaveButtonBox").show();
            } else {
                $("#controlSaveButtonBox").hide();
            }
        }

        function setPaymentQty_removeBorder(key){
            removeBorderColor('input[name="contract['+key+'][payment][0][payment_total]"]');
        }

        function removeBorderColor(el){
            $(el).css('borderColor', '#e0e0e0');
        }


        function saveFastAccept() {
            if (validFastAccept() == true) {
                loaderShow();
                formValid = true;
                $("#formContract").submit();
            }
        }

        function isPersonalSales(key) {
            if ($('#sales_condition_' + key).val() == 0) {
                $('#is_personal_sales_div_' + key).css('display', 'none');
                $('#s2id_manager_id_' + key).removeClass('valid_fast_accept');
            } else {
                $('#is_personal_sales_div_' + key).css('display', 'block');
                $('#s2id_manager_id_' + key).addClass('valid_fast_accept');
            }
        }

        function validFastAccept() {

            var first_err;
            var msg_arr = [];

            var i = 0;

            $('.valid_fast_accept').each(function () {

                if ($(this)[0].localName != 'div') {

                    var tag = $(this)[0].localName;

                    var val = '';
                    if (['select', 'input'].indexOf(tag) != -1) {
                        val = $(this).val();
                    } else {
                        return;
                    }


                    if (tag == 'select') {

                        var not_selected_val = 0;
                        if ($(this).attr('data-not_selected') !== undefined) {
                            not_selected_val = parseInt($(this).attr('data-not_selected'))
                        }

                        if (parseInt(val) == not_selected_val) {
                            if (i == 0 && $(this).parent('div').css('display') != 'none') {
                                msg_arr.push('Заполните все поля');
                                first_err = $(this);
                                i++;
                            }
                            msg_arr.push('Заполните все поля');
                            var container = $(this).prev('.select2-container');
                            container.css({
                                'height': '36px',
                                'border': '1px red solid'
                            });
                        }

                        // var key = $(this).attr('key');
                        //
                        // if ($(this).attr('name') == 'contract['+key+'][financial_policy_id]'){
                        //     if ($('contract['+key+'][financial_policy_manually_set]').prop("checked")){
                        //             console.log(123);
                        //
                        //     }
                        // }
                    }

                    if (new String(val).length <= 0 && $(this).parent('div').css('display') != 'none') {

                        if (i == 0) {
                            first_err = $(this);
                            i++;
                        }
                        msg_arr.push('Заполните все поля');

                        var elem = $(this);

                        elem.css("border-color", "red");

                    }
                }
            });


            $('.temp_contract').each(function (index, value) {

                var bso_key = $(this).data('key');

                var bso = $('input[name="contract[' + bso_key + '][bso_title]"]').val();

                $.post('check_correct_bso', {'bso': bso}, function (res) {
                    if (res == 'false') {

                        first_err = $('input[name="contract[' + bso_key + '][bso_title]"]');
                        i++;
                        msg_arr.push('Заполните все поля');

                        var elem = $('input[name="contract[' + bso_key + '][bso_title]"]');
                        elem.css("border-color", "red");
                    }
                });
            });

            if (msg_arr.length > 0) {
                $('html, body').animate({scrollTop: $(first_err).offset().top - 300}, 800);

                return false;
            }

            return true;
        }

        function get_end_dates(start_date) {
            var cur_date_tmp = start_date.split(".");
            var cur_date = new Date(cur_date_tmp[2], cur_date_tmp[1] - 1, cur_date_tmp[0]);
            var new_date = new Date(cur_date.setYear(cur_date.getFullYear() + 1));
            var new_date2 = new Date(new_date.setDate(new_date.getDate() - 1));
            return getFormattedDate(new_date2);
        }

        function setAllDates(key) {
            sign_date = $("#sign_date_" + key).val();
            $("#begin_date_" + key).val(sign_date);

            return setEndDates(key);
        }

        function setEndDates(key) {
            removeBorderColor("#end_date_" + key);
            begin_date = $("#begin_date_" + key).val();
            if ($('#end_date_' + key).val() == '') {
                $("#end_date_" + key).val(get_end_dates(begin_date));
            }
        }


        function viewSetFinancialPolicyManually(obj, key) {
            if ($(obj).is(':checked')) {
                $("#financial_policy_manually_" + key).show();
                $("#financial_policy_id_" + key).hide();
                $("select[name='contract[" + key + "][financial_policy_id]']").removeClass('valid_fast_accept');
                $("#s2id_financial_policy_id_" + key).css('border', 'unset');
            } else {
                $("#financial_policy_manually_" + key).hide();
                $("#financial_policy_id_" + key).show();
                $("select[name='contract[" + key + "][financial_policy_id]']").addClass('valid_fast_accept');
            }
        }

        function viewSetBsoNotReceipt(obj, key) {
            if ($(obj).is(':checked')) {
                $("#bso_receipt_" + key).attr('disabled', 'disabled');
                $("#bso_receipt_" + key).removeClass('valid_fast_accept');
                $('[name="contract[' + key + '][payment][0][bso_receipt]"]').val('');
                $('[name="contract[' + key + '][payment][0][bso_receipt_id]"]').val('');
            } else {
                $("#bso_receipt_" + key).removeAttr('disabled');
                $("#bso_receipt_" + key).addClass('valid_fast_accept');
            }

            $("#bso_receipt_" + key).css("border-color", "");
        }


        function startContractObjectInsurerFunctions(KEY) {
            $('#object_insurer_ts_mark_id_' + KEY).select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true
            });

            $('#object_insurer_ts_model_id_' + KEY).select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true
            });

        }

        function getModelsObjectInsurer(KEY, select_model_id) {

            $.getJSON('{{url("/contracts/actions/get_models")}}', {
                categoryId: $('#object_insurer_ts_category_' + KEY).val(),
                markId: $('#object_insurer_ts_mark_id_' + KEY).select2('val')
            }, function (response) {


                var options = "<option value='0'>Не выбрано</option>";
                response.map(function (item) {
                    options += "<option value='" + item.id + "'>" + item.title + "</option>";
                });
                $('#object_insurer_ts_model_id_' + KEY).html(options).select2('val', select_model_id);


            });

        }
        // инпуты КВ разблокируются по параметрам из продукта
        function getKvAccesses(key, product_id) {

            $.getJSON("/bso/actions/get_accesses_products/", {
                product_id: product_id
            }, function (response) {

                var official = $('#official_discount' + key);
                var informal = $('#informal_discount' + key);
                var bank = $('#bank_kv' + key);

                official.prop("disabled", true);
                informal.prop("disabled", true);
                bank.prop("disabled", true);

                official.removeClass('valid_fast_accept');
                informal.removeClass('valid_fast_accept');
                bank.removeClass('valid_fast_accept');

                if (response.kv_official_available == 1) {
                    official.prop("disabled", false);
                }
                if (response.kv_informal_available == 1) {
                    informal.prop("disabled", false);
                }
                if (response.kv_bank_available == 1) {
                    bank.prop("disabled", false);
                }
            });
        }


    </script>


@endsection