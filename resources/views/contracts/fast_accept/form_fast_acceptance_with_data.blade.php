@php
    $key = 0;
@endphp
@foreach($autosaved_data->contract as $contract)
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 temp_contract" id="contract_{{$key}}" data-key="{{$key}}">
        <div class="trucks-item order-item">
            <div class="info">
                Договор <span class="number">{{$key+1}}</span>

                <button onclick="deleteContract('{{$key}}')" style="background-color: #FFF;font-size: 15px;margin-top: -5px;border: none;" class="pull-right view_but" type="button">
                    <i class="fa fa-remove" style="color: red;"></i>
                </button>

            </div>
            <div class="divider"></div>
            <div class="info">

                <div class="form-group">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-4" >
                        <h3>Договор (полис)</h3>


                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >

                            <label class="control-label pull-left" >
                                Номер договора <span style="color:red">*</span>
                            </label>
                            <label class="control-label pull-right">
                                Изъять документы <input type="checkbox" value="1" {{ isset($contract->withdraw_documents) && $contract->withdraw_documents ? 'checked' : '' }} name="contract[{{$key}}][withdraw_documents]" />
                            </label>


                            {{ Form::text('contract['.$key.'][bso_title]', $contract->bso_title, ['class' => 'form-control valid_fast_accept', 'id'=>'bso_title_'.$key, 'onchange'=>'setContractValue('.$key.');'])}}
                            <input type="hidden" name="contract[{{$key}}][bso_id]" value="{{$contract->bso_id}}" id="bso_id_{{$key}}" />
                            <input type="hidden" name="contract[{{$key}}][bso_supplier_id]" value="{{$contract->bso_supplier_id}}" id="bso_supplier_id_{{$key}}" />
                            <input type="hidden" name="contract[{{$key}}][insurance_companies_id]" value="{{$contract->insurance_companies_id}}" id="insurance_companies_id_{{$key}}" />
                            <input type="hidden" name="contract[{{$key}}][product_id]" id="product_id_{{$key}}" value="{{$contract->product_id}}" />
                            <input type="hidden" name="contract[{{$key}}][agent_id]" id="agent_id_{{$key}}" value="{{$contract->agent_id}}" />
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label">Заявка из фронт офиса</label>
                            {{ Form::text('contract['.$key.'][order_title]', $contract->order_title, ['class' => 'form-control', 'id'=>'order_title_'.$key]) }}
                            <input type="hidden" name="contract[{{$key}}][order_id]" value="{{$contract->order_id}}"  id="order_id_{{$key}}" />
                            <input type="hidden" name="contract[{{$key}}][order_sort_id]" value="{{ $contract->order_sort_id }}" id="order_sort_id_{{$key}}" />
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label">
                                Дата заключения <span style="color:red">*</span>
                            </label>
                            {{ Form::text('contract['.$key.'][sign_date]', $contract->sign_date, ['class' => 'form-control datepicker date valid_fast_accept', 'id'=>'sign_date_'.$key, 'onchange'=>'setPaymentDates('.$key.'); refreshOptionFinancialPolicy(this, "'.$key.'")']) }}
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <label class="control-label">
                                Период действия <span style="color:red">*</span>
                            </label>
                            <div class="row form-horizontal">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                    {{ Form::text('contract['.$key.'][begin_date]', $contract->begin_date, ['class' => 'form-control date datepicker valid_fast_accept', 'id'=>'begin_date_'.$key, 'placeholder'=>'Дата начала', 'onchange'=>'setEndDates('.$key.')']) }}
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                    {{ Form::text('contract['.$key.'][end_date]', $contract->end_date, ['class' => 'form-control date datepicker valid_fast_accept', 'placeholder'=>'Дата окончания', 'id'=>'end_date_'.$key]) }}
                                </div>
                            </div>
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label">Условие продажи</label>
                            {{ Form::select('contract['.$key.'][sales_condition]', collect(\App\Models\Contracts\Contracts::SALES_CONDITION), $contract->sales_condition, ['class' => 'form-control select2-ws', 'id'=>'sales_condition_'.$key, 'data-key' => $key, 'onchange' => 'isPersonalSales('.$key.')']) }}
                        </div>


                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="is_personal_sales_div_{{$key}}" style="display: none;">
                            <label class="control-label pull-left" style="margin-top: 5px;">
                                Менеджер
                            </label>
                            <label class="control-label pull-right">Личная продажа
                                <input type="checkbox" value="1" {{isset($contract->is_personal_sales) && $contract->is_personal_sales ? 'checked' : ''}} id="is_personal_sales_{{$key}}" name="contract[{{$key}}][is_personal_sales]" data-key="{{$key}}"/>
                            </label>
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right: 0px;" >
                                {{ Form::select('contract['.$key.'][manager_id]', $agents->pluck('name', 'id')->prepend('Выберите значение', 0), $contract->manager_id, ['class' => 'form-control  select2-ws-min-3', 'id'=>'manager_id_'.$key, 'onchange' => 'removeBorder("#s2id_manager_id_'.$key.'")']) }}
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-4" >
                        <h3>Порядок оплаты</h3>
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <div class="row form-horizontal">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <label class="control-label">
                                        Премия по договору <span style="color:red">*</span>
                                    </label>
                                    {{ Form::text('contract['.$key.'][payment_total]', $contract->payment_total, ['class' => 'form-control sum valid_fast_accept', 'id'=>'payment_total_'.$key, 'onkeyup'=>'setPaymentQty('.$key.')', 'onchange'=>'setPaymentQty_removeBorder('.$key.')']) }}
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <label class="control-label">
                                        Дата оплаты <span style="color:red">*</span>
                                    </label>
                                    {{ Form::text('contract['.$key.'][payment][0][payment_data]', $contract->payment[0]->payment_data, ['class' => 'form-control date datepicker valid_fast_accept', 'id'=>'payment_data_'.$key.'']) }}

                                </div>
                            </div>
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <label class="control-label">Алгоритм рассрочки</label>
                            {{ Form::select('contract['.$key.'][installment_algorithms_id]', collect([0=>'Укажите номер договора']), $contract->installment_algorithms_id, ['class' => 'form-control select2-ws', 'id'=>'installment_algorithms_id_'.$key, 'onchange' => 'setAlgos('.$key.')']) }}
                        </div>

                        <div id="payment_algo_{{$key}}" class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            @if($contract->installment_algorithms_id && $contract->installment_algorithms_id != 0 && isset($contract->algo_payment))
                                @foreach($contract->algo_payment as $i => $algo_payment)
                                    <div class="row form-horizontal">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                            <label class="control-label">Дата следующего платежа</label>
                                            {{ Form::text("contract[{$key}][algo_payment][$i][date]", isset($algo_payment->date) ? setDateTimeFormatRu($algo_payment->date, 1) : '', ['class' => 'form-control datepicker date', 'id'=>"algo_payment_date_$i"]) }}
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                            <label class="control-label">Сумма следующего платежа</label>
                                            {{ Form::text("contract[{$key}][algo_payment][$i][sum]", isset($algo_payment->sum) && ($algo_payment->sum>0) ? titleFloatFormat($algo_payment->sum) : '', ['class' => 'form-control sum', 'id'=>"algo_payment_sum_$i", 'onchange'=>'setPaymentQty('.$key.')']) }}
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <div class="row form-horizontal">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <label class="control-label">
                                        Сумма платежа <span style="color:red">*</span>
                                    </label>
                                    {{ Form::text('contract['.$key.'][payment][0][payment_total]', $contract->payment[0]->payment_total, ['class' => 'form-control sum valid_fast_accept', 'id'=>'payment_total_'.$key.'']) }}
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <div class="row form-horizontal">
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                            <label class="control-label">
                                                Взнос № <span style="color:red">*</span>
                                            </label>
                                            {{ Form::text('contract['.$key.'][payment][0][payment_number]', $contract->payment[0]->payment_number, ['class' => 'form-control valid_fast_accept', 'id'=>'payment_number_'.$key]) }}
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                            <label class="control-label">Тип</label>
                                            {{ Form::select('contract['.$key.'][payment][0][type_id]', collect(\App\Models\Contracts\Payments::TRANSACTION_TYPE), $contract->payment[0]->type_id, ['class' => 'form-control select2-ws']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <div class="row form-horizontal">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                    <label class="control-label">Офици. %</label>
                                    {{ Form::text('contract['.$key.'][payment][0][official_discount]', $contract->payment[0]->official_discount, ['class' => 'form-control sum percents_input_validation', 'id'=>'official_discount_'.$key]) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                    <label class="control-label">Неофиц. %</label>
                                    {{ Form::text('contract['.$key.'][payment][0][informal_discount]', $contract->payment[0]->informal_discount, ['class' => 'form-control sum percents_input_validation', 'id'=>'informal_discount_'.$key]) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                    <label class="control-label">Банк %</label>
                                    {{ Form::text('contract['.$key.'][payment][0][bank_kv]', $contract->payment[0]->bank_kv, ['class' => 'form-control sum percents_input_validation', 'id'=>'bank_kv_'.$key, 'onkeyup' => 'setBankSelect('.$key.')']) }}
                                </div>
                            </div>
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <div class="row form-horizontal">
                                <div class="toggle-bank-{{$key}} col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden">
                                    <label class="control-label">Банк</label>
                                    {{ Form::select('contract['.$key.'][bank_id]', collect(\App\Models\Settings\Bank::all())->pluck('title', 'id')->prepend('Не выбрано', 0), $contract->bank_id ?? 0, ['class' => 'form-control select2-all', 'id'=>'bank_id_0']) }}
                                </div>
                            </div>
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label pull-left" style="margin-top: 5px;" id="check_label_{{$key}}">
                                Вид оплаты
                            </label>
                            {{ Form::select('contract['.$key.'][payment][0][pay_method_id]', \App\Models\Finance\PayMethod::query()->where('is_actual', 1)->pluck('title', 'id'), $contract->payment[0]->pay_method_id, ['class' => 'form-control valid_fast_accept', 'id'=>'check_'.$key, 'data-key' => ''.$key]) }}
                            <script>
                                var pay_methods_id2types = {
                                    @foreach(\App\Models\Finance\PayMethod::query()->where('is_actual', 1)->get() as $method)
                                    {!! "$method->id : $method->key_type," !!}
                                    @endforeach
                                };
                            </script>
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="receipt_block_{{$key}}">
                            <label class="control-label pull-left" style="margin-top: 5px;" id="bso_receipt_label_{{$key}}">
                                Квитанция<span style="color:red">*</span>
                                @if(auth()->user()->hasPermission('contracts', 'show_e_policy'))
                                    <a href="javascript:void(0);" class="btn-xs btn-primary" data-create_receipt="{{$key}}" style="display: inline">Е-квит</a>
                                @endif
                            </label>
                            <label class="control-label pull-right">Без квитанции
                                <input type="checkbox" value="100" id="check_{{$key}}" name="contract[{{$key}}][pay_method_id]" {{isset($contract->pay_method_id) && $contract->pay_method_id ? 'checked' : ''}} onclick="disableReceiptField({{$key}})" data-key="{{$key}}"/>
                            </label>
                            {{ Form::text('contract['.$key.'][payment][0][bso_receipt]', $contract->payment[0]->bso_receipt ?? '', ['class' => 'form-control valid_fast_accept', 'id'=>'bso_receipt_'.$key, 'data-key' => $key, isset($contract->pay_method_id) && $contract->pay_method_id ? 'disabled="disabled"' : '']) }}
                            <input type="hidden" name="contract[{{$key}}][payment][0][bso_receipt_id]" value="{{$contract->payment[0]->bso_receipt_id ?? ''}}" id="bso_receipt_id_{{$key}}" />
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="check_block_{{$key}}" style="display: none">
                            <div class="row form-horizontal">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <label class="control-label">Телефон</label>
                                    {{ Form::text('contract['.$key.'][check_phone]', $contract->check_phone, ['class' => 'form-control phone', 'id'=>'check_phone_'.$key,]) }}
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <label class="control-label">E-mail</label>
                                    {{ Form::text('contract['.$key.'][check_email]', $contract->check_email, ['class' => 'form-control email', 'id'=>'check_email_'.$key.'']) }}
                                </div>
                            </div>

                            <div class="row form-horizontal">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <label class="control-label">Отправить</label>
                                    {{ Form::select('contract['.$key.'][when_to_send]', collect(\App\Models\Api\Atol\AtolCheck::WHEN_TO_SEND), $contract->when_to_send, ['class' => 'form-control', 'id'=>'when_to_send_'.$key,]) }}
                                </div>
                            </div>
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label pull-left" style="margin-top: 5px;">
                                Финансовая политика
                            </label>
                            <label class="control-label pull-right">Указать в ручную
                                <input type="checkbox" value="1" {{ isset($contract->financial_policy_manually_set) && $contract->financial_policy_manually_set ? 'checked' : ''}} name="contract[{{$key}}][financial_policy_manually_set]" onchange="viewSetFinancialPolicyManually(this, {{$key}})"/>
                            </label>

                            <div class="wraper-inline-100">
                                {{ Form::select('contract['.$key.'][financial_policy_id]', collect([0=>'Укажите номер договора']), $contract->financial_policy_id, ['class' => 'form-control select2-ws valid_fast_accept', 'id'=>'financial_policy_id_'.$key, 'key' => $key]) }}
                            </div>
                        </div>


                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
                            <div class="row form-horizontal" id="financial_policy_manually_{{$key}}" style="display: none;">

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    {{ Form::text('contract['.$key.'][financial_policy_kv_bordereau]', $contract->financial_policy_kv_bordereau, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_bordereau_'.$key, 'placeholder'=>'Бордеро']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    {{ Form::text('contract['.$key.'][financial_policy_kv_dvoy]', $contract->financial_policy_kv_dvoy, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_dvoy_'.$key, 'placeholder'=>'ДВОУ']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    {{ Form::text('contract['.$key.'][financial_policy_kv_agent]', $contract->financial_policy_kv_agent, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_agent_'.$key.'', 'placeholder'=>'Агента']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    {{ Form::text('contract['.$key.'][financial_policy_kv_parent]', $contract->financial_policy_kv_parent, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_parent_'.$key, 'placeholder'=>'Руков.']) }}
                                </div>
                            </div>

                        </div>



                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-4" >
                        <h3>Cтрахователь</h3>


                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <div class="row form-horizontal">

                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" >
                                    <label class="control-label">Тип</label>
                                    {{ Form::select('contract['.$key.'][insurer][type]', collect([0=>"ФЛ", 1=>'ЮЛ']), $contract->insurer->type, ['class' => 'form-control select2-ws', 'id'=>'insurer_type_'.$key, 'data-key'=> $key]) }}
                                </div>

                                <div class="insurer_fl col-xs-12 col-sm-12 col-md-9 col-lg-9" >
                                    <label class="control-label">
                                        ФИО<span style="color:red">*</span>
                                    </label>
                                    {{ Form::text('contract['.$key.'][insurer][fio]', $contract->insurer->fio, ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_fio_'.$key, 'data-key'=> $key]) }}
                                </div>

                                <div class="insurer_ul col-xs-12 col-sm-12 col-md-9 col-lg-9" style="display: none">
                                    <label class="control-label">
                                        Название<span style="color:red">*</span>
                                    </label>
                                    {{ Form::text('contract['.$key.'][insurer][title]', $contract->insurer->title, ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_title_'.$key, 'data-key'=> $key]) }}
                                </div>

                            </div>
                        </div>

                        <div class="row insurer_fl col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <label class="control-label">Паспорт</label>
                            <div class="row form-horizontal">
                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" >
                                    {{ Form::text('contract['.$key.'][insurer][doc_serie]', $contract->insurer->doc_serie, ['class' => 'form-control', 'placeholder'=>'Серия']) }}
                                </div>
                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" >
                                    {{ Form::text('contract['.$key.'][insurer][doc_number]', $contract->insurer->doc_number, ['class' => 'form-control', 'placeholder'=>'Номер']) }}
                                </div>
                            </div>
                        </div>

                        <div class="row insurer_ul col-xs-12 col-sm-12 col-md-12 col-lg-12"  style="display: none">
                            <div class="row form-horizontal">

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <label class="control-label">ИНН</label>
                                    {{ Form::text('contract['.$key.'][insurer][inn]', $contract->insurer->inn, ['class' => 'form-control', 'id'=>'insurer_inn_'.$key, 'data-key'=> $key]) }}
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <label class="control-label">КПП</label>
                                    {{ Form::text('contract['.$key.'][insurer][kpp]', $contract->insurer->kpp, ['class' => 'form-control', 'id'=>'insurer_kpp_'.$key, 'data-key'=> $key]) }}
                                </div>

                            </div>
                        </div>


                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <div class="row form-horizontal">

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <label class="control-label">Телефон</label>
                                    {{ Form::text('contract['.$key.'][insurer][phone]', $contract->insurer->phone, ['class' => 'form-control phone', 'id'=>'insurer_phone_'.$key]) }}
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <label class="control-label">Email</label>
                                    {{ Form::text('contract['.$key.'][insurer][email]', $contract->insurer->email, ['class' => 'form-control', 'id'=>'insurer_email_'.$key]) }}
                                </div>

                            </div>
                        </div>

                        <h3>Объект страхования</h3>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="contract_object_insurer_{{$key}}">
                            <center><strong>Укажите номер договора</strong></center>
                        </div>

                        <script>
                            isPersonalSales({{$key}});
                            activSearchOrdersToFront("order_title", '_{{$key}}');
                            viewSetFinancialPolicyManually('[name="contract[{{$key}}][financial_policy_manually_set]"]', {{$key}});
                            setBankSelect({{$key}});
                            getKvAccesses({{$key}}, {{$contract->product_id}});
                            showCommentField({{$key}});

                            @if((int)$contract->bso_id > 0)
                                getOptionInstallmentAlgorithms("installment_algorithms_id_{{$key}}", {{$contract->insurance_companies_id}}, 0);
                                getOptionFinancialPolicy("financial_policy_id_{{$key}}", {{$contract->insurance_companies_id}}, {{$contract->bso_supplier_id}}, {{$contract->product_id}}, {{$contract->financial_policy_id}});
                                getKvAccesses({{$key}}, {{$contract->product_id}});

                                object_insurer = myGetAjax('{{url("/bso/actions/get_html_mini_contract_object_insurer_with_data/")}}?product_id={{$contract->product_id}}&key={{$key}}&type_id=2');
                                $("#contract_object_insurer_{{$key}}").html(object_insurer);

                                startContractObjectInsurerFunctions({{$key}});

                                var receipt_used = [];
                                $.each($('[name*="][bso_receipt]"]'), function (k, v) {
                                    receipt_used.push($(v).val());
                                });

                                activSearchBso("bso_receipt", '_{{$key}}', 2, receipt_used);

                                var method_blocks = {
                                    0: 'receipt_block_{{$key}}',
                                    1: 'check_block_{{$key}}',
                                    2: 'none'
                                };

                                var method = parseInt($('[name="contract[{{$key}}][payment][0][pay_method_id]"]').val());

                                $.each(method_blocks, function (k, v) {
                                    $("#" + v).hide()
                                });

                                $("#" + method_blocks[pay_methods_id2types[method]]).show();

                                if (method === 1) {
                                    $('[name="contract[{{$key}}][payment][0][bso_receipt]"]').addClass('valid_fast_accept');
                                } else {
                                    $('[name="contract[{{$key}}][payment][0][bso_receipt]"]').removeClass('valid_fast_accept');
                                }
                            @endif
                        </script>

                        @if(auth()->user()->is('under'))
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                                <h3>Тип акцепта</h3>
                                <div class=" form-horizontal">
                                {{ Form::select('contract['.$key.'][kind_acceptance]', collect(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE)->prepend('Не выбрано', -1), $contract->kind_acceptance, ['class' => 'form-control select2-ws pull-right valid_fast_accept', 'data-not_selected' => -1, 'onchange' => 'showCommentField('.$key.')']) }}
                                </div>
                            </div>

                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12 message_{{$key}} hidden" >
                                <h3>Комментарий</h3>
                                <div class=" form-horizontal work-area-item">
                                    {{ Form::textarea('contract['.$key.'][message]', $contract->message, ['class' => 'form-control', 'rows' => 3]) }}&nbsp;
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
        $key++;
    @endphp
@endforeach