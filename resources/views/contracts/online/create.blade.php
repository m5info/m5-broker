@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="/css/jquery.datetimepicker.min.css">


<div class="page-heading">
    <h2>{{$product->title}}, новый договор</h2>
</div>
<div class="row form-horizontal" id="main_container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            @if (View::exists('contracts.online.forms.'.$product->slug))
            <form id="product_form">
                @include('contracts.online.forms.'.$product->slug, [])
            </form>
            @else
            <p>Оформление невозможно. Форма для продукта отсутствует</p>
            @endif
        </div>
        @if (View::exists('contracts.online.forms.'.$product->slug))

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="calculation" class="col-lg-2 pull-right" style="padding: 5px 15px 5px 15px;">    
                <div class="product" style="margin:15px;">
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="calculate({{$product->id}});">Рассчитать</a>
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div id="offers">

        </div>


        @endif


    </div>



</div>
@endsection

@section('js')
<style>
    .calendar-icon{
        margin-top: -25px;
        margin-right: 8px;
        float: right;
        color: #a3a3a3;
    }
    .menu-fixed{
        margin-left:-10px !important;
    }
</style>
<script src="/js/jquery.datetimepicker.full.min.js"></script>
<link rel="stylesheet" href="/css/jquery.datetimepicker.min.css">
<script src="/js/online.js"></script>
<link rel="stylesheet" href="/css/online.css">
@endsection

