@extends('layouts.app')

@section('work-area')
    @include("contracts.online.workarea", ['products' => $products])
@endsection


@section('content')

    <div class="page-heading">
        <h2>Создание онлайн</h2>
    </div>
    <div class="row form-horizontal" id="main_container">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">
            <div class="policy-blocks">

                @if($products->get()->count())
                    <div class="policy-b">
                        <h3>Категория</h3>
                        <ul id="productCategoryMenuList" class="p-tabs">
                            @foreach($categories as $category)
                                @if($loop->first)
                                    @php
                                        $currrentCategory = 'current';
                                    @endphp

                                @else
                                    @php
                                        $currrentCategory = '';
                                    @endphp

                                @endif

                                <li class="category category-{{$category->id}}">
                                    <a onclick="selectCategory('{{$category->id}}');" href="#"
                                       class="{{$currrentCategory}}"
                                       data-category='{{$category->id}}'>{{$category->title}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="policy-b2">
                        <h3>Продукт</h3>
                        <div id="productMenuList" class="p-tabs-panes">

                            {{--@foreach($icp->keyBy('product_id') as $products)
                                <ul class="p-tabs2 product-category-{{$i}}" style="">
                                    @foreach($group as $product)
                                        <li>
                                            <a href="javascript:void(0);" product-group="[{{$product->id}}]" onclick="selectProduct({{$product->id}});return false;" data-product="{{$product->id}}" id="product_{{$product->id}}" data-slug="{{$product->slug}}" data-programs="{{$product->programs->count()}}" class="">{{$product->title}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endforeach--}}
                            @foreach($products->get()->groupBy('category_id') as $i => $group)
                                <ul class="p-tabs2 product-category-{{$i}}" style="">
                                    @foreach($group as $product)
                                        <li>
                                            <a href="javascript:void(0);" product-group="[{{$product->id}}]"
                                               onclick="selectProduct({{$product->id}});return false;"
                                               data-product="{{$product->id}}" id="product_{{$product->id}}"
                                               data-slug="{{$product->slug}}"
                                               data-programs="{{$product->programs->count()}}"
                                               class="">{{$product->title}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endforeach
                        </div>
                    </div>


                    <div class="policy-b2" id="view_programs">
                        <h3>Программы</h3>
                        <div id="programsMenuList" class="p-tabs3">
                            @php
                                $repit_company = null
                            @endphp
                            @foreach($icp as $program)
                                @if($program->programs_online_list_sort || $program->product_programs_slug != 'kasko')
                                    @if($loop->first)
                                        @php
                                            $selected = '';
                                            $currrentProgram = 'current';
                                        @endphp
                                    @else
                                        @php
                                            $selected = "hidden";
                                            $currrentProgram = '';
                                        @endphp
                                    @endif
                                    @if($repit_company !== $program->insurance_companies_title)
                                        <p style="padding: 10px 0;" class="program-description program-description-{{$program->products_id}}">{{$program->insurance_companies_title}}</p>
                                        @php
                                            $repit_company = $program->insurance_companies_title;
                                        @endphp
                                    @endif
                                    <div style="margin-right: -10px; width: auto;" class="program-description program-description-{{$program->products_id}} {{ $selected }}" id="program-{{$program->product_programs_id}}">
                                        <a href="javascript:void(0);" onclick="selectProgram({{$program->product_programs_id}});return false;" data-program="{{ $program->product_programs_id }}" class="{{ $currrentProgram }}">{{$program->product_programs_title}}</a>
                                    </div>
                                @endif
                            @endforeach

                            {{--@foreach($products->get() as $product)
                                @if($loop->first)
                                    @php($selected = "")
                                @else
                                    @php($selected = "hidden")
                                @endif
                                    @foreach($product->optional_programs() as $program)
                                        @if($loop->first)
                                            @php($currrentProgram = 'current')
                                        @else
                                            @php($currrentProgram = '')
                                        @endif
                                        <div class="program-description program-description-{{$product->id}} {{ $selected }}" id="program-{{$program->id}}" >
                                            <a href="javascript:void(0);" onclick="selectProgram({{$program->id}});return false;" data-program="{{ $program->id }}" class="{{ $currrentProgram }}">{{$program->title}}</a>
                                        </div>
                                    @endforeach
                            @endforeach--}}
                        </div>
                    </div>

                    <div id="productDescriptionMenuList" class="policy-b3">
                        <h3>Описание продукта</h3>
                        <div class="p-tabs-panes2">
                            @foreach($products->get() as $product)
                                @if($loop->first)
                                    @php($selected = "")
                                @else
                                    @php($selected = "hidden")
                                @endif
                                @isset($product->programs[0])
                                    @php($i = 0)
                                    @foreach($product->optional_programs() as $program)
{{--                                        @if($product->slug != 'kasko')--}}
{{--                                                {{ dd($product->optional_programs()) }}--}}
{{--                                            @endif--}}
                                        @php($i++)
                                        @if($i == 1)
                                            @php($currentProgram = "")
                                        @else
                                            @php($currentProgram = "hide")
                                        @endif
                                        <div class="program-description program-description-{{$product->id}} {{$selected}} {{$currentProgram}}"
                                             id="program-{{ $program->id }}">
                                            <p>{{$program->description ? $program->description : 'Отсутствует'}}</p>
                                            <a href="{{url('contracts/online/create/'.$product->id).'?program='.$program->id}}"
                                               class="btn btn-primary" data-toggle="tooltip"
                                               data-original-title="Начать новый расчет по продукту"
                                               onclick="getFormForProduct({{$product->id}}); return false;">Создать
                                                договор</a>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="product-description product-description-{{$product->id}} {{$selected}}">
                                        <p>{{$product->description ? $product->description : 'Отсутствует'}}</p>
                                        <a href="{{url('contracts/online/create/'.$product->id)}}"
                                           class="btn btn-primary" data-toggle="tooltip"
                                           data-original-title="Начать новый расчет по продукту"
                                           onclick="getFormForProduct({{$product->id}}); return false;">Создать
                                            договор</a>
                                    </div>
                                @endisset
                            @endforeach
                        </div>
                    </div>
                @else
                    <p>Продукты для оформления онлайн отсутствуют</p>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('head')
    <link rel="stylesheet" href="/css/online.css">
@append


@section('js')

    <script>

        $(function () {
            var kasko = $('#productMenuList a').data('product');
            if (kasko == 11) {
                if (kasko.hasClass('current')) {
                    initWorkArea();
                }
            }
            hideWorkAreButton();
        });

        function changeWorkArea(product_id) {
            if (product_id == 11) {
                initWorkArea();
            } else {
                hideWorkAreButton();
            }
        }

        @foreach($icp->keyBy('insurance_companies_id') as $sk_id => $data)
            @if($sk_id != '')
                $('#set_programs_list_{{$sk_id}}').sortable();
            @endif
        @endforeach

        function saveShowingPrograms() {

            $.ajax({
                type: "POST",
                url: "{{url("/contracts/actions/set_programs_list")}}",
                async: false,
                data: $("#set_sk_list").find("select, textarea, input").serialize(),
                success: function (response) {
                    reload();
                }
            });

            return true;
        }

        $(function () {
            $('document').ready(function () {
                $('[data-toggle="tooltip"]').tooltip({container: 'body'});
                selectCategory($('#productCategoryMenuList a:visible').eq(0).attr('data-category'));
                //selectProduct(($('#productMenuList a:visible').eq(0).attr('data-product')));
            });
        });

        function selectProgram(program_id) {
            $('.program-description a').removeClass('current');
            $('#program-' + program_id + ' a').addClass('current');

            $('#productDescriptionMenuList .program-description').addClass('hide');
            $('#productDescriptionMenuList #program-' + program_id).removeClass('hide');
        }

        function selectProduct(product_id) {

            var programms = $('#view_programs div.program-description-'+ product_id);

            changeWorkArea(product_id);

            $('#productDescriptionMenuList .product-description').addClass('hidden');
            $('#productDescriptionMenuList .product-description-' + product_id).removeClass('hidden');
            $('#productDescriptionMenuList .program-description').addClass('hidden');
            $('#productDescriptionMenuList .program-description-' + product_id).removeClass('hidden');
            $('#productMenuList a').removeClass('current');
            $('#productMenuList a[data-product="' + product_id + '"]').addClass('current');
            $('#programsMenuList .program-description').addClass('hidden');


            product = $("#product_" + product_id);
            if (product.data('slug').length > 0) {

            } else {
                $('#productDescriptionMenuList .product-description-' + product_id).addClass('hidden');
            }

            if (parseInt(product.data('programs')) > 0) {
                $("#view_programs").removeClass('hidden');
                $('#programsMenuList .program-description-' + product_id).removeClass('hidden');
                $('#productDescriptionMenuList .product-description-' + product_id).removeClass('hidden');

                // $("#programsMenuList").html('');

            } else {
                $("#view_programs").addClass('hidden');
                $('#programsMenuList .program-description-' + product_id).addClass('hidden');

                // $("#programsMenuList").html('');
            }


        }

        function selectCategory(category_id, product_id) {
            $('#productCategoryMenuList a').removeClass('current');
            $('#productCategoryMenuList .category-' + category_id + ' a').addClass('current');

            console.log('#productCategoryMenuList .category-' + category_id + ' a');

            $('#productMenuList ul').hide();
            $('#productMenuList .product-category-' + category_id).show();
            if (product_id === undefined) {
                var product_id = $('#productMenuList a:visible').eq(0).attr('data-product');
                selectProduct(product_id);
            } else {
                selectProduct(product_id);
            }
        }

    </script>

    <style>
        .cont-in {
            padding: 0 35px 35px 35px !important;
        }
    </style>
@endsection
