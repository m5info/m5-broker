<div class="work-area-group">
    <h3>Список программ:</h3>
    @php
        $icps = $icp;
        $array = [];
        foreach($icps as $icp){
            $array[$icp->insurance_companies_title][] = $icp;
        }
    @endphp
    <div class="work-area-item row" id="set_sk_list">
        @foreach($array as $sk_title => $icp)
            <span class="view-label sk" id="sk-{{$icp[0]['insurance_companies_id']}}">{{$sk_title}}</span><br>
            <div id="set_programs_list_{{$icp[0]['insurance_companies_id']}}">
            @foreach($icp as $program)
                @if($program['product_programs_slug'] == 'kasko')
                    @php($id = $program['product_programs_id'])
                    @php($sk_id = $program['insurance_companies_id'])
                    <div class="view-field programs col-lg-12" id="programs-{{$id}}">
                        <span class="view-label" style="background: unset">{{$program['product_programs_title']}}</span>
                        <span class="view-value" style="background: unset">{{ Form::checkbox("programs[sk][$sk_id][program][$id]", 1, $program['programs_online_list_sort'] ? 1 : 0) }}</span>
                    </div>
                @endif
            @endforeach
            </div>
        @endforeach
    </div>
    <div class="work-area-item">
        <span class="btn btn-success" onclick="saveShowingPrograms();">
            Сохранить
        </span>
    </div>
</div>

<style>
    #set_sk_list .sk{
        background: unset;
        padding: 10px 0 20px 10px;
    }
    #set_sk_list .programs{
        padding: 0 0 0 30px;
        cursor: pointer;
        background: unset;
        color: #505050;
    }
</style>