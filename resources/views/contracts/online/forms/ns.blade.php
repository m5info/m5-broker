
@php
$docTypes = ['Паспорт гражданина РФ',
'Водительское удостоверение РФ',
'Водительское удостоверение ГТН',
'Водительское удостоверение иностранного государства',
'Военный билет солдата (матроса, сержанта, старшины) РФ',
'Временное удостоверение личности гражданина РФ',
'Удостоверение личности офицера РФ',
'Иностранный паспорт',
'Загранпаспорт гражданина РФ',
'Свидетельство о государственной регистрации (запись в ЕГРЮЛ)',];

@endphp


@php

$calc = $contract->calculation ? $contract->calculation->json : null;

@endphp
<input type="hidden" name="contract[0][insurer][type]" value="{{$calc['contract'][0]['insurer']['type']  ?? '0'}}">
<input type="hidden" name="contract[0][insurer][title]" value="{{$calc['contract'][0]['insurer']['title']  ?? ''}}">
<input type="hidden" name="contract[0][object_insurer][title]" value="{{$calc['contract'][0]['object_insurer']['title']  ?? ''}}">
<input type="hidden" name="contract[0][object_insurer][type]"  value="{{$calc['contract'][0]['object_insurer']['type']  ?? ''}}"/>
<div class="block-view">
    <h3>Срок и период страхования</h3>
    <div class="row">
        <div class="col-lg-4">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Время начала <span class="required">*</span>                                                                            
                    </label>
                    <input placeholder="" name="contract[0][begin_time]" class="form-control format-time" value="{{$calc['contract'][0]['begin_time']  ?? '12:00'}}">
                    <span class="glyphicon glyphicon-time calendar-icon"></span>
                </div>
            </div>
        </div>          
        <div class="col-lg-4">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата начала <span class="required">*</span>                                                                            
                    </label>
                    <input placeholder="" name="contract[0][begin_date]" class="form-control format-date" id="begin_date_0" onchange="setEndDates(0)" value="{{ $calc['contract'][0]['begin_date']  ??  Carbon\Carbon::now()->format('d.m.Y')}}">
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата окончания <span class="required">*</span>                                                                            
                    </label>
                    <input placeholder="" name="contract[0][end_date]" class="form-control format-date end-date" id="end_date_0" value="{{$calc['contract'][0]['end_date']  ?? Carbon\Carbon::now()->addYear()->subDay(1)->format('d.m.Y')}}">
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="block-view">
    <h3>Участники договора</h3>
    <div class="row">
        <div class="col-lg-1">
            <h4>Страхователь</h4>
        </div>

        <div class="clear"></div>

        <div class="col-lg-4" >
            <label class="control-label">
                ФИО <span class="required">*</span>                                                                            
            </label>
            {{ Form::text('contract[0][insurer][fio]', $calc['contract'][0]['insurer']['fio']  ?? '', ['class' => 'form-control ', 'id'=>'insurer_fio_0', 'data-key'=>'0']) }}
        </div>


        <div class="col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата рождения <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][date]', $calc['contract'][0]['insurer']['date']  ?? '', ['class' => 'form-control format-date end-date', 'id' => 'insurer_birthdate_0']) }}
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>


        <div class="col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Пол <span class="required">*</span>                                                                            
                    </label>
                    {{Form::select('contract[0][insurer][sex]', collect([0=>"муж.", 1=>'жен.']), $calc['contract'][0]['insurer']['sex']  ?? 0, ['class' => 'form-control ', 'id' => 'insurer_sex_0','data-key'=>'0']) }}
                </div>
            </div>
        </div>

        <div class="col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Телефон <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][phone]', $calc['contract'][0]['insurer']['phone']  ?? '', ['class' => 'form-control']) }}
                </div>
            </div>
        </div>


        <div class="col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Email <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][email]', $calc['contract'][0]['insurer']['email']  ?? '', ['class' => 'form-control']) }}
                </div>
            </div>
        </div>

        <div class="clear"></div>
        <div class="col-lg-12" >
            <h4>Паспортные данные</h4>
        </div>
        <div class="col-lg-3" >
            <label class="control-label">Тип документа</label>
            {{Form::select("contract[0][object_insurer][doc_type]", $docTypes, $calc['contract'][0]['object_insurer']['doc_type']  ?? $docTypes[0], ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-lg-3" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Серия <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][doc_serie]', $calc['contract'][0]['insurer']['doc_serie']  ?? '', ['class' => 'form-control']) }}
                </div>
            </div>
        </div>


        <div class="col-lg-3" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Номер <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][doc_number]', $calc['contract'][0]['insurer']['doc_number']  ?? '', ['class' => 'form-control']) }}
                </div>
            </div>
        </div>


        <div class="col-lg-3" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата выдачи <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][docdate]', $calc['contract'][0]['insurer']['docdate']  ?? '', ['class' => 'form-control format-date end-date']) }}
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div class="col-lg-3" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Кем выдан <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][docinfo]', $calc['contract'][0]['insurer']['docinfo']  ?? '', ['class' => 'form-control']) }}
                </div>
            </div>
        </div>

        <div class="col-lg-3" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Номер подразделения <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][docoffice]', $calc['contract'][0]['insurer']['docoffice']  ?? '', ['class' => 'form-control']) }}
                </div>
            </div>
        </div>

        <div class="clear"></div>
        <div class="col-lg-12" >
            <h4>Адреса</h4>
        </div>

        <div class="col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Место рождения <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][address_born]', $calc['contract'][0]['insurer']['address_born']  ?? '', ['class' => 'form-control', 'id' => 'insurer_address_born']) }}
                    {{ Form::text('contract[0][insurer][address_born_kladr]', $calc['contract'][0]['insurer']['address_born_kladr']  ?? '', ['class' => 'hidden', 'id' => 'insurer_address_born_kladr']) }}
                </div>
            </div>
        </div>

        <div class="col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Адрес регистрации <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][address_register]', $calc['contract'][0]['insurer']['address_register']  ?? '', ['class' => 'form-control', 'id' => 'insurer_address_register']) }}
                    {{ Form::text('contract[0][insurer][address_register_kladr]', $calc['contract'][0]['insurer']['address_register_kladr']  ?? '', ['class' => 'hidden', 'id' => 'insurer_address_register_kladr']) }}
                </div>
            </div>
        </div>

        <div class="col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Адрес фактический <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][address_fact]', $calc['contract'][0]['insurer']['address_fact']  ?? '', ['class' => 'form-control', 'id' => 'insurer_address_fact']) }}
                    {{ Form::text('contract[0][insurer][address_fact_kladr]', $calc['contract'][0]['insurer']['address_fact_kladr']  ?? '', ['class' => 'hidden', 'id' => 'insurer_address_fact_kladr']) }}
                </div>
            </div>
        </div>



    </div>
</div>


<script>
    function init()
    {
        $('#insurer_fio_' + 0).suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "NAME",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#insurer_title_' + key).val($(this).val());

            }
        });

        $('#insurer_address_born').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#insurer_address_born').val($(this).val());
                $('#insurer_address_born_kladr').val(suggestion.data.city_kladr_id);
            }
        });



        $('#insurer_address_register').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#insurer_address_register').val($(this).val());
                $('#insurer_address_register_kladr').val(suggestion.data.city_kladr_id);
            }
        });



        $('#insurer_address_fact').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#insurer_address_fact').val($(this).val());
                $('#insurer_address_fact_kladr').val(suggestion.data.city_kladr_id);
            }
        });
    }

    document.addEventListener("DOMContentLoaded", function (event) {
        init();
    });



</script>

<div class="modal" id="issue"></div>