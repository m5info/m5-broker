
<div class="block-view">
    <h3>Срок и период страхования</h3>
    <div class="row">
        <div class="col-lg-4">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Время начала <span class="required">*</span>                                                                            
                    </label>
                    <input placeholder="" name="" class="form-control format-time" value="12:00">
                    <span class="glyphicon glyphicon-time calendar-icon"></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата начала <span class="required">*</span>                                                                            
                    </label>
                    <input placeholder="" name="" class="form-control format-date" id="begin_date_0" onchange="setEndDates(0)" value="{{Carbon\Carbon::now()->format('d.m.Y')}}">
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата окончания <span class="required">*</span>                                                                            
                    </label>
                    <input placeholder="" name="" class="form-control format-date end-date" id="end_date_0" value="{{Carbon\Carbon::now()->addYear()->subDay(1)->format('d.m.Y')}}">
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="block-view">
    <h3>Участники договора</h3>
    <div class="row">
        <div class="col-lg-1">
            <h4>Страхователь</h4>
        </div>

        <div class="col-lg-1">
            {{ Form::select('contract[0][insurer][type]', collect([0=>"ФЛ", 1=>'ЮЛ', 2=>'ИП']), 0, ['class' => 'form-control', 'id'=>'insurer_type_0', 'data-key'=>'0']) }}
        </div>
        <div class="clear"></div>

        <div class="col-lg-6" >
            <label  class="control-label" style="max-width:100%;">Страхователь (Собственник)</label>
            {{ Form::checkbox('contract[0][object_insurer][trailer]', 0, 0,['style' => 'width:18px;height:18px;margin-left:5px;position:absolute;', 'id' => 'insurer_type_owner']) }}
        </div>


        @php
        $docTypes = ['Паспорт гражданина РФ',
        'Водительское удостоверение РФ',
        'Водительское удостоверение ГТН',
        'Водительское удостоверение иностранного государства',
        'Военный билет солдата (матроса, сержанта, старшины) РФ',
        'Временное удостоверение личности гражданина РФ',
        'Удостоверение личности офицера РФ',
        'Иностранный паспорт',
        'Загранпаспорт гражданина РФ',
        'Свидетельство о государственной регистрации (запись в ЕГРЮЛ)',];

        @endphp


        {{--- Страхователь физлицо ---}}
        <div class="insurer_fl col-xs-12 col-sm-12 col-md-12 col-lg-12"  style="display: none;">
            <div class="row form-horizontal">
                <div class="col-lg-8" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                ФИО <span class="required">*</span>
                            </label>
                            {{ Form::text('contract[0][insurer][fio]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_fio_0', 'data-key'=>'0']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Дата рождения <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][date]', '', ['class' => 'form-control format-date end-date', 'id' => 'insurer_birthdate_0']) }}
                            <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Пол <span class="required">*</span>                                                                            
                            </label>
                            {{Form::select('contract[0][insurer][sex]', collect([0=>"муж.", 1=>'жен.']), 0, ['class' => 'form-control valid_fast_accept', 'id' => 'insurer_sex_0','data-key'=>'0']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Телефон <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][phone]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>


                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Email <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][email]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="clear"></div>
                <div class="col-lg-12" >
                    <h4>Паспортные данные</h4>
                </div>
                <div class="col-lg-3" >
                    <label class="control-label">Тип документа</label>
                    {{Form::select("contract[0][object_insurer][doc_type]", $docTypes, 0, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
                </div>

                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Серия <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][docserie]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>


                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Номер <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][docnumber]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>


                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Дата выдачи <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][docdate]', '', ['class' => 'form-control format-date end-date']) }}
                            <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>

                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Кем выдан <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][docinfo]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Номер подразделения <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][docoffice]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="clear"></div>
                <div class="col-lg-12" >
                    <h4>Адреса</h4>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Место рождения <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][address_born]', '', ['class' => 'form-control', 'id' => 'insurer_address_born']) }}
                            {{ Form::text('contract[0][insurer][address_born_kladr]', '', ['class' => 'hidden', 'id' => 'insurer_address_born_kladr']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Адрес регистрации <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][address_register]', '', ['class' => 'form-control', 'id' => 'insurer_address_register']) }}
                            {{ Form::text('contract[0][insurer][address_register_kladr]', '', ['class' => 'hidden', 'id' => 'insurer_address_register_kladr']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Адрес фактический <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][address_fact]', '', ['class' => 'form-control', 'id' => 'insurer_address_fact']) }}
                            {{ Form::text('contract[0][insurer][address_fact_kladr]', '', ['class' => 'hidden', 'id' => 'insurer_address_fact_kladr']) }}
                        </div>
                    </div>
                </div>

            </div>
        </div>
        {{--- Страхователь юр. илицо ---}}
        <div class="insurer_ul col-xs-12 col-sm-12 col-md-12 col-lg-12"  style="display: none;">
            <div class="row form-horizontal">

                <div class="col-lg-3" >
                    <label class="control-label">Название компании</label>
                    {{ Form::text('contract[0][insurer][name]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                </div>

                <div class="col-lg-3" >
                    <label class="control-label">ИНН</label>
                    {{ Form::text('contract[0][insurer][inn]', '', ['class' => 'form-control', 'id'=>'insurer_inn_0', 'data-key'=>'0']) }}
                </div>

                <div class="col-lg-3" >
                    <label class="control-label">КПП</label>
                    {{ Form::text('contract[0][insurer][kpp]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                </div>


                <div class="col-lg-3" >
                    <label class="control-label">Огрн</label>
                    {{ Form::text('contract[0][insurer][ogrn]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                </div>

                <div class="col-lg-3" >
                    <label class="control-label">Серия свидетельства о регистрации Ю.Л. <span class="required">*</span>  </label>
                    {{ Form::text('contract[0][insurer][name]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                </div>


                <div class="col-lg-3" >
                    <label class="control-label">Номер свидетельства о регистрации Ю.Л. <span class="required">*</span> </label>
                    {{ Form::text('contract[0][insurer][name]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                </div>


                <div class="col-lg-3" >
                    <label class="control-label">БИК</label>
                    {{ Form::text('contract[0][insurer][bik]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                </div>


                <div class="col-lg-3" >
                    <label class="control-label">E-mail</label>
                    {{ Form::text('contract[0][insurer][tel]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                </div>

                <div class="clear"></div>
                <div class="col-lg-12" >
                    <h4>Контактное лицо</h4>
                </div>
                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                ФИО <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][fio]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_contact', 'data-key'=>'0']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Должность <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][fio]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_contact', 'data-key'=>'0']) }}
                        </div>
                    </div>
                </div>


                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Дата рождения <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][date]', '', ['class' => 'form-control format-date end-date']) }}
                            <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Телефон <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][phone]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>


                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Email <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][email]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="clear"></div>
                <div class="col-lg-12" >
                    <h4>Юр. Адрес</h4>
                </div>
                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Улица <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][street]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Дом <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][town]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Корпус <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][korp]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Офис <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][insurer][apartment]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

            </div>
        </div>

        {{--- Страхователь И.П. ---}}
        <div class="insurer_ip col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none;">
            <div class="row form-horizontal">
                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">ФИО</label>
                            {{ Form::text('contract[0][owner][name]', '', ['class' => 'form-control', 'id'=>'insurer_inn_0', 'data-key'=>'0']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Дата рождения <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][date]', '', ['class' => 'form-control format-date end-date']) }}
                            <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">ИНН</label> <span class="required">*</span> 
                            {{ Form::text('contract[0][owner][inn]', '', ['class' => 'form-control', 'id'=>'insurer_inn_0', 'data-key'=>'0']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div> 
    <div class="row" style="padding-top:25px;">
        <div class="col-lg-1">
            <h4>Собственник</h4>
        </div>

        <div class="col-lg-1">
            {{ Form::select('contract[0][insurer][type]', collect([0=>"ФЛ", 1=>'ЮЛ', 2=>'ИП']), 0, ['class' => 'form-control', 'id'=>'owner_type_0', 'data-key'=>'0']) }}
        </div>

        <div class="clear"></div>


        <!--<div class="col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Регион регистрации <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][owner][region]', '', ['class' => 'form-control', 'id' => 'region']) }}
                </div>
            </div>
        </div>

        <div class="col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Город <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][owner][city]', '', ['class' => 'form-control', 'id' => 'city']) }}
                </div>
            </div>
        </div>-->

        <div class="clear"></div>


        {{--- Собственник физлицо ---}}
        <div class="owner_fl col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none;">
            <div class="row form-horizontal">

                <div class="col-lg-8" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                ФИО <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][fio]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'owner_fio_0', 'data-key'=>'0']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Дата рождения <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][date]', '', ['class' => 'form-control format-date end-date']) }}
                            <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                        </div>
                    </div>
                </div>



                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Пол <span class="required">*</span>                                                                            
                            </label>
                            {{Form::select('contract[0][owner][sex]', collect([0=>"муж.", 1=>'жен.']), 0, ['class' => 'form-control valid_fast_accept', 'data-key'=>'0']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Телефон <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][phone]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>


                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Email <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][email]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="clear"></div>
                <div class="col-lg-12" >
                    <h4>Паспортные данные</h4>
                </div>
                <div class="col-lg-3" >
                    <label class="control-label">Тип документа</label>
                    {{Form::select("contract[0][owner][doc_type]", $docTypes, 0, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
                </div>

                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Серия <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][docserie]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>


                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Номер <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][docnumber]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>


                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Дата выдачи <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][docdate]', '', ['class' => 'form-control format-date end-date']) }}
                            <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>

                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Кем выдан <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][docinfo]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Номер подразделения <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][docoffice]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="clear"></div>
                <div class="col-lg-12" >
                    <h4>Адреса</h4>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Место рождения <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][address_born]', '', ['class' => 'form-control', 'id' => 'owner_address_born']) }}
                            {{ Form::text('contract[0][owner][address_born_kladr]', '', ['class' => 'hidden', 'id' => 'owner_address_born_kladr']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Адрес регистрации <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][address_register]', '', ['class' => 'form-control', 'id' => 'owner_address_register']) }}
                            {{ Form::text('contract[0][owner][address_register_kladr]', '', ['class' => 'hidden', 'id' => 'owner_address_register_kladr']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Адрес фактический <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][address_fact]', '', ['class' => 'form-control', 'id' => 'owner_address_fact']) }}
                            {{ Form::text('contract[0][owner][address_fact_kladr]', '', ['class' => 'hidden', 'id' => 'owner_address_fact_kladr']) }}
                        </div>
                    </div>
                </div>


            </div>
        </div>
        {{--- Собственник юр.лицо ---}}
        <div class="owner_ul col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none;">
            <div class="row form-horizontal">

                <div class="col-lg-3" >
                    <label class="control-label">Название компании</label>
                    {{ Form::text('contract[0][owner][name]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                </div>

                <div class="col-lg-3" >
                    <label class="control-label">ИНН</label>
                    {{ Form::text('contract[0][owner][inn]', '', ['class' => 'form-control', 'id'=>'insurer_inn_0', 'data-key'=>'0']) }}
                </div>

                <div class="col-lg-3" >
                    <label class="control-label">КПП</label>
                    {{ Form::text('contract[0][owner][kpp]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                </div>


                <div class="col-lg-3" >
                    <label class="control-label">Огрн</label>
                    {{ Form::text('contract[0][owner][ogrn]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                </div>

                <div class="col-lg-3" >
                    <label class="control-label">Серия свидетельства о регистрации Ю.Л. <span class="required">*</span>  </label>
                    {{ Form::text('contract[0][owner][name]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                </div>


                <div class="col-lg-3" >
                    <label class="control-label">Номер свидетельства о регистрации Ю.Л. <span class="required">*</span> </label>
                    {{ Form::text('contract[0][owner][name]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                </div>


                <div class="col-lg-3" >
                    <label class="control-label">БИК</label>
                    {{ Form::text('contract[0][owner][bik]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                </div>


                <div class="col-lg-3" >
                    <label class="control-label">E-mail</label>
                    {{ Form::text('contract[0][owner][tel]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                </div>

                <div class="clear"></div>
                <div class="col-lg-12" >
                    <h4>Контактное лицо</h4>
                </div>
                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                ФИО <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][fio]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_contact', 'data-key'=>'0']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Должность <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][fio]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_contact', 'data-key'=>'0']) }}
                        </div>
                    </div>
                </div>


                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Дата рождения <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][date]', '', ['class' => 'form-control format-date end-date']) }}
                            <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Телефон <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][phone]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>


                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Email <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][email]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="clear"></div>
                <div class="col-lg-12" >
                    <h4>Юр. Адрес</h4>
                </div>
                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Улица <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][street]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Дом <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][town]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Корпус <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][korp]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-3" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Офис <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][apartment]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

            </div>
        </div>
        {{--- Собственник И.П. ---}}
        <div class="owner_ip col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none;">
            <div class="row form-horizontal">
                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">ФИО</label>
                            {{ Form::text('contract[0][owner][name]', '', ['class' => 'form-control', 'id'=>'insurer_inn_0', 'data-key'=>'0']) }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">
                                Дата рождения <span class="required">*</span>                                                                            
                            </label>
                            {{ Form::text('contract[0][owner][date]', '', ['class' => 'form-control format-date end-date']) }}
                            <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" >
                    <div class="field form-col">
                        <div>
                            <label class="control-label">ИНН</label> <span class="required">*</span> 
                            {{ Form::text('contract[0][owner][inn]', '', ['class' => 'form-control', 'id'=>'insurer_inn_0', 'data-key'=>'0']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>


</div>

<div class="block-view">
    <h3>Водители</h3>
    <div class="row">
        <div class="col-lg-3">
            {{ Form::select('contract[0][drivers][type]', collect([0=>"Ограниченный поименный список ЛДУ", 1=>'Неограниченный, список ЛДУ (мультидрайв)']), 0, ['class' => 'form-control', 'id'=>'driver_type_0', 'data-key'=>'0']) }}
        </div>

        @php
        $drivers = [];
        for($i = 1; $i<= 20; $i++){$drivers[$i] = $i;}
        @endphp
        <input name="driver_count" id="driver_count" type="hidden" value="0">
        <!--<div class="col-lg-3 driver_count">
            {{ Form::select('contract[0][drivers][count]', collect($drivers), 0, ['class' => 'form-control', 'id'=>'driver_count_0', 'data-key'=>'0']) }}
        </div>-->

        <div class="clear"></div>
        <div class="driver_limited col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none;">
            <div style="width:50px;padding-top:10px;">
                <a href="javascript:void(0);" class="btn btn-primary" title="Добавить водителя" onclick="addNewDriver();"><span class="glyphicon glyphicon-plus"></span></a>
            </div>
        </div>

        <div class="driver_unlimited col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none;">
            <div class="row form-horizontal">
                <div class="col-lg-12" style="padding-top:15px;">
                    <p>Договор будет заключен на условиях неограниченного списка лиц, допущенных к управлению</p>
                </div>
            </div>
        </div>


    </div>
</div>


<div class="block-view">
    <h3>Транспортное средство</h3>
    <div class="row">
        <div class="col-lg-3">
            <label class="control-label">Категория</label>
            {{Form::select("contract[0][object_insurer][ts_category]", \App\Models\Vehicle\VehicleCategories::query()->get()->pluck('title', 'id'), 0, ['class' => 'form-control', 'id'=>"object_insurer_ts_category_0", 'onchange'=>"getModelsObjectInsurer(0, 0);"])}}
        </div>

        <div class="col-lg-3" >
            <label class="control-label">Марка</label>
            {{Form::select("contract[0][object_insurer][mark_id]", \App\Models\Vehicle\VehicleMarks::orderBy('title')->get()->pluck('title', 'id')->prepend('Не выбрано', 0), 0, ['class' => 'mark-id mark_id select2', "id"=>"object_insurer_ts_mark_id_0", 'style'=>'width: 100%;', 'onchange'=>"getModelsObjectInsurer(0, 0);"])}}
        </div>

        <div class="col-lg-3" >
            <label class="control-label">Модель</label>
            {{Form::select("contract[0][object_insurer][model_id]", [], 0, ['class' => 'model_id model-id select2', "id"=>"object_insurer_ts_model_id_0", 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-lg-3" >
            <label class="control-label">Цель использования</label>
            {{Form::select("contract[0][object_insurer][reason]", ['Личная','Учебная езда','Такси','Дорожные и специальные ТС','Экстренные и коммунальные службы','Перевозка опасных и легко воспламеняющихся грузов','Регулярные пассажирские перевозки/перевозки пассажиров по заказам',], 0, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>



        <div class="clear"></div>

        <div class="col-lg-3" >
            <label class="control-label">VIN</label>
            {{ Form::text("contract[0][object_insurer][vin]", '', ['class' => 'form-control', "id"=>"object_insurer_ts_vin_0"]) }}
        </div>

        <div class="col-lg-3" >
            <label class="control-label">Номер кузова</label>
            {{ Form::text("contract[0][object_insurer][car_body_num]", '', ['class' => 'form-control']) }}
        </div>


        <div class="col-lg-3" >
            <label class="control-label">Страна регистрации</label>
            {{ Form::text("contract[0][object_insurer][car_body_num]", '', ['class' => 'form-control']) }}
        </div>

        <div class="col-lg-3" >
            <label class="control-label">Рег. номер</label>
            {{ Form::text("contract[0][object_insurer][reg_number]", '', ['class' => 'form-control ru_sumb', "id"=>"object_insurer_ts_reg_number_0"]) }}
        </div>

        <div class="clear"></div>

        <div class="col-lg-3" >
            <label class="control-label">Год выпуска</label>
            {{Form::text("contract[0][object_insurer][reason]",  Carbon\Carbon::now()->format('Y'), ['class' => 'form-control','id' => 'carYear', 'style'=>'width: 100%;'])}}
        </div>




        <div class="col-lg-3" >
            <label class="control-label">Мощность (л.с.)</label>
            {{ Form::text("contract[0][object_insurer][power]", '', ['class' => 'form-control sum', "id"=>"object_insurer_ts_power_0"]) }}
        </div>


        <div class="clear"></div>
        {{--- Доп условия для типов автомобилей ----}}

        <div class="col-lg-3" id="object_insurer_passengers_count_0">
            <label class="control-label">Кол-во мест</label>
            {{ Form::text("contract[0][object_insurer][passengers_count]", '', ['class' => 'form-control']) }}
        </div>

        <div class="col-lg-3" id="object_insurer_weight_0">
            <label class="control-label">Масса</label>
            {{ Form::text("contract[0][object_insurer][weight]", '', ['class' => 'form-control']) }}
        </div>


        <div class="col-lg-3" id="object_insurer_capacity_0">
            <label class="control-label">Грузоподъемность</label>
            {{ Form::text("contract[0][object_insurer][capacity]", '', ['class' => 'form-control']) }}
        </div>

        {{--- end ---}}
        <div class="clear"></div>

        <div class="col-lg-6" >
            <label  class="control-label" style="max-width:100%;">Автомобиль используется с прицепом</label>
            {{ Form::checkbox('contract[0][object_insurer][trailer]', 0, 0,['style' => 'width:18px;height:18px;margin-left:5px;position:absolute;']) }}
        </div>
        <div class="clear"></div>

        <div class="col-lg-12" >
            <h4>Документы ТС</h4>
        </div>

        <div class="col-lg-3" >
            <label class="control-label">Тип документа</label>
            {{Form::select("contract[0][object_insurer][doc_type]", ['Свидетельство о регистрации','Паспорт транспортного средства'], 0, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-lg-3" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Серия <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][docserie]', '', ['class' => 'form-control ru_sumb']) }}
                </div>
            </div>
        </div>


        <div class="col-lg-3" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Номер <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][docnumber]', '', ['class' => 'form-control ru_sumb']) }}
                </div>
            </div>
        </div>


        <div class="col-lg-3" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата выдачи <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][docdate]', '', ['class' => 'form-control format-date end-date']) }}
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>

        <div class="clear"></div>
        <div class="col-lg-12" >
            <h4>Документ ТО</h4>
        </div>


        <div class="col-lg-3" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Номер диагностической карты <span class="required">*</span>                                                                            
                    </label>
                    {{ Form::text('contract[0][insurer][docnumber]', '', ['class' => 'form-control']) }}
                </div>
            </div>
        </div>

        <div class="col-lg-3" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата очередного ТО <span class="required">*</span>
                    </label>
                    {{ Form::text('contract[0][insurer][docdate]', '', ['class' => 'form-control format-date end-date']) }}
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>

    </div>
</div>


<script>
    function init()
    {
        $('#insurer_type_0').change(function () {

            if ($("#insurer_type_owner").is(':checked')) {
                return;
            }


            if (parseInt($(this).val()) == 0) {
                $('.insurer_fl').show();
                $('.insurer_ul').hide();
                $('.insurer_ip').hide();
            } else if (parseInt($(this).val()) == 1) {
                $('.insurer_fl').hide();
                $('.insurer_ul').show();
                $('.insurer_ip').hide();
            } else if (parseInt($(this).val()) == 2) {
                $('.insurer_fl').hide();
                $('.insurer_ul').hide();
                $('.insurer_ip').show();
            }


        });


        $("#insurer_type_owner").change(function () {
            if (this.checked) {
                $('.insurer_fl').hide();
                $('.insurer_ul').hide();
                $('.insurer_ip').hide();
            } else {
                $('#insurer_type_0').trigger('change');
            }
        });


        $('#owner_type_0').change(function () {
            if (parseInt($(this).val()) == 0) {
                $('.owner_fl').show();
                $('.owner_ul').hide();
                $('.owner_ip').hide();
            } else if (parseInt($(this).val()) == 1) {
                $('.owner_fl').hide();
                $('.owner_ul').show();
                $('.owner_ip').hide();
            } else if (parseInt($(this).val()) == 2) {
                $('.owner_fl').hide();
                $('.owner_ul').hide();
                $('.owner_ip').show();
            }

        });

        $('#driver_type_0').change(function () {
            if (parseInt($(this).val()) == 0) {
                $('.driver_limited').show();
                $('.driver_count').show();
                $('.driver_unlimited').hide();
                $('.driver').show();

            } else if (parseInt($(this).val()) == 1) {
                $('.driver_limited').hide();
                $('.driver_count').hide();
                $('.driver_unlimited').show();
                $('.driver').hide();
            }

        });


        $('#insurer_type_0').change();

        $('#owner_type_0').change();
        $('#driver_type_0').change();
        $('#driver_count_0').change();
        /*$('#object_insurer_ts_mark_id_0').change();*/

        $('#insurer_fio_' + 0).suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "NAME",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#insurer_title_' + key).val($(this).val());

            }
        });


        $('#owner_fio_' + 0).suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "NAME",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#owner_title_' + key).val($(this).val());

            }
        });


        $('#insurer_address_born').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#insurer_address_born').val($(this).val());
                $('#insurer_address_born_kladr').val(suggestion.data.city_kladr_id);
            }
        });



        $('#insurer_address_register').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#insurer_address_register').val($(this).val());
                $('#insurer_address_register_kladr').val(suggestion.data.city_kladr_id);
            }
        });



        $('#insurer_address_fact').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#insurer_address_fact').val($(this).val());
                $('#insurer_address_fact_kladr').val(suggestion.data.city_kladr_id);
            }
        });



        $('#owner_address_register').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#owner_address_born').val($(this).val());
                $('#owner_address_born_kladr').val(suggestion.data.city_kladr_id);
            }
        });


        $('#owner_address_fact').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#owner_address_fact').val($(this).val());
                $('#owner_address_fact_kladr').val(suggestion.data.city_kladr_id);
            }
        });



        $('#owner_address_born').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#owner_address_born').val($(this).val());
                $('#owner_address_born_kladr').val(suggestion.data.city_kladr_id);
            }
        });


        $('#region').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#region').val($(this).val());

            },
            hint: false,
            bounds: "region-area",
        });

        $('#city').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#city').val($(this).val());

            },
            hint: false,
            bounds: "city-settlement",
            constraints: '#region',
            formatSelected: formatCity
        });


        var im = new Inputmask("9999", {"yearrange": {minyear: 1900, maxyear: 2099}});
        im.mask($('#carYear'));

    }


    function getModelsObjectInsurer(KEY, select_model_id)
    {

        $.getJSON('{{url("/contracts/actions/get_models")}}', {categoryId: $('#object_insurer_ts_category_' + KEY).val(), markId: $('#object_insurer_ts_mark_id_' + KEY).select2('val')}, function (response) {

            var options = "<option value='0'>Не выбрано</option>";
            response.map(function (item) {
                options += "<option value='" + item.id + "'>" + item.title + "</option>";
            });
            $('#object_insurer_ts_model_id_' + KEY).html(options).select2('val', select_model_id);


        });

        let current = parseInt($('#object_insurer_ts_category_' + KEY).val());

        if (current == 3) {
            $('#object_insurer_weight_' + KEY).show();
            $('#object_insurer_capacity_' + KEY).show();
            $('#object_insurer_passengers_count_' + KEY).hide();
        } else if (current == 4) {
            $('#object_insurer_weight_' + KEY).hide();
            $('#object_insurer_capacity_' + KEY).hide();
            $('#object_insurer_passengers_count_' + KEY).show();
        } else {
            $('#object_insurer_weight_' + KEY).hide();
            $('#object_insurer_capacity_' + KEY).hide();
            $('#object_insurer_passengers_count_' + KEY).hide();
        }




    }

    function removeDriver(i) {
        $('.driver-' + i).remove();
        $('.driver-title').each(function (key, item) {
            key += 1;
            $(item).html("Водитель " + key);
        });
    }



    function addNewDriver() {
        let driverCount = parseInt($('#driver_count').val());
        driverCount++;
        $('#driver_count').val(driverCount);

        $.ajax({
            type: "GET",
            url: "{{url('/contracts/online/driver_form/')}}" + '/' + driverCount,
            async: false,
            success: function (response) {
                $(response).insertBefore(".driver_limited");
                $('.driver-title').each(function (key, item) {
                    key += 1;
                    $(item).html("Водитель " + key);
                });
                formatTime();
                formatDate();
            }
        });

        return true;

    }

    function join(arr /*, separator */) {
        var separator = arguments.length > 1 ? arguments[1] : ", ";
        return arr.filter(function (n) {
            return n
        }).join(separator);
    }

    function formatCity(suggestion) {
        var address = suggestion.data;
        if (address.city_with_type === address.region_with_type) {
            return address.settlement_with_type || "";
        } else {
            return join([
                address.city_with_type,
                address.settlement_with_type]);
        }
    }




    document.addEventListener("DOMContentLoaded", function (event) {
        init();
        addNewDriver();
    });




</script>

