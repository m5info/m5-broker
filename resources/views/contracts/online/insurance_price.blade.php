<div class="col-lg-12"></div>
<div class="clear"></div>

@if(sizeof($supplier_offers))

    @foreach($supplier_offers as $offer)

        @php
            $company = App\Models\Directories\InsuranceCompanies::where('id', $offer['id'])->get()->first();
        @endphp


        @if(App\Models\Directories\InsuranceCompanies::where('id', $offer['id'])->get()->first() && !$offer['form_errors'])
            <div class="col-lg-3" style="padding-top:5px;margin-top: 10px;">
                <div id="calculation" class="sticky" style="float: none;margin: 0 auto;">
                    <div class="product" id="product14">
                        <h3>{{$company->title}}</h3>

                        @if(!$offer['errors'])
                            <div class="price-total">
                                {{titleFloatFormat($offer['sum'])}} <span>руб.</span>
                            </div>
                            <a class="btn btn-primary" href="javascript:void(0);" onclick="getIssueForm('{{$offer['product_id']}}', '{{$company->id}}')">Оформить</a>
                        @else
                            <div class="error">{{implode('<br>',$offer['errors'])}}</div>
                        @endif

                        @if($company->logo)
                            <div class="logo">
                                <img style="height:100px;" src="{{url($company->logo)}}">
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endif

    @endforeach

@endif

