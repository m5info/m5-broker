<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Выбрать бсо</h4>
        </div>
        <div class="modal-body" style='height:300px;'>
            <input class="form-control" id="bso_title" name="bso_title" type="text" value="" placeholde='bso_id'>
            <input type="hidden" name="bso_id" id="bso_id" />
            <input type="hidden" name="bso_supplier_id" id="bso_supplier_id" />
            <input type="hidden" name="insurance_companies_id" id="insurance_companies_id" />
            <input type="hidden" name="product_id" id="product_id" />
            <input type="hidden" name="agent_id" id="agent_id" />
            <input type="hidden" name="is_online" value = '1' />
        </div>
        <div class="modal-footer">
            <a class="btn btn-primary" href="javascript:void(0);" onclick="issue('{{$product_id}}', '{{$insurance_id}}')">Оформить</a>
        </div>
    </div>
</div>

<script>

    function activSearchBso(object_id, key, type)
    {

    $('#' + object_id + key).suggestions({
    serviceUrl: "/bso/actions/get_bso/",
            type: "PARTY",
            params:{type_bso:'1', product_id:'{{$product_id}}', bso_supplier_id: '{{$insurance_id}}', bso_agent_id:''},
                    count: 5,
                    minChars: 3,
                    formatResult: function (e, t, n, i) {
                    var s = this;
                    var title = n.value;
                    var bso_type = n.data.bso_type;
                    var bso_sk = n.data.bso_sk;
                    var agent_name = n.data.agent_name;
                    var view_res = title;
                    view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">СК</span>' + bso_sk + "</div>";
                    view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Тип</span>' + bso_type + "</div>";
                    view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Агент</span>' + agent_name + "</div>";
                    return view_res;
                    },
                    onSelect: function (suggestion) {


                    selectBso(object_id, key, type, suggestion);
                    }
            });
    }


    $(function () {
    activSearchBso("bso_title", '', 1);
    });
    function selectBso(object_id, key, type, suggestion)
    {
    var data = suggestion.data;
    if (parseInt(type) == 1){ // БСО
    $('#bso_id' + key).val(data.bso_id);
    $('#bso_supplier_id' + key).val(data.bso_supplier_id);
    $('#insurance_companies_id' + key).val(data.insurance_companies_id);
    $('#product_id' + key).val(data.product_id);
    $('#agent_id' + key).val(data.agent_id);
    $('#sk_title').val(data.bso_sk);
    $('#agent_title').val(data.agent_name);
    }
    }

</script>
