@extends('layouts.app')

@section('content')


    <div class="page-heading">
        <h2>
            <span class="btn-left">
                Загрузка договоров
            </span>
        </h2>
    </div>

    <div class="form-group">
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="control">
        </div>
    </div>

    <div class="form-group" style="margin-left: 0; margin-right: 0;">
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">



            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    {!! Form::open(['url'=>"/contracts/downloading_contracts/upload",'method' => 'post', 'class' => 'dropzone_', 'id' => 'addManyDocFormDownloadingContracts']) !!}
                    <div class="dz-message" data-dz-message>
                        <p>Перетащите сюда файлы</p>
                        <p class="dz-link">или выберите с диска</p>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

            {{ Form::open(['url' => url('/contracts/downloading_contracts/load'), 'method' => 'post',  'class' => 'form-horizontal', 'id' => 'formContract', 'files' => true]) }}

            <input type="hidden" name="file" >




                <div class="block-view">
                    <div class="row">
                        <div>
                            <div>

                                <div class="col-lg-12">
                                    <div class="field form-col">
                                        <label class="control-label">Страховая компания</label>
                                        {{ Form::select('bso_suppliers', $bso_suppliers->pluck('title', 'id'), 0, ['class' => 'form-control select2-all']) }}
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="field form-col">
                                        <label class="control-label">Продукт</label>
                                        <select class="form-control" id="product_id" name="product_id"></select>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="field form-col">
                                        <label class="control-label">Формат БСО</label>
                                        <select class="form-control par" required name="par">
                                            <option value=" ">XXX 1234567</option>
                                            <option value="-">XXX-1234567</option>
                                            <option value="">XXX1234567</option>
                                        </select>
                                    </div>
                                </div>


                                <button type="submit" class="btn btn-w-m btn-success submit-btn" id="buttonSend">
                                    Загрузить
                                </button>




                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
                            </div>
                        </div>
                    </div>
                </div>




            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="columns-row hidden">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th>
                                БСО
                            </th>
                            <td>
                                <select class="form-control columns-select"  name="excel_columns[bso_title]"></select>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Страхователь
                            </th>
                            <td>
                                <select class="form-control columns-select"  name="excel_columns[insurer_title]"></select>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Дата заключения
                            </th>
                            <td>
                                <select class="form-control columns-select"  name="excel_columns[contract_date]"></select>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Дата начала
                            </th>
                            <td>
                                <select class="form-control columns-select"  name="excel_columns[contract_begin]"></select>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Дата окончания
                            </th>
                            <td>
                                <select class="form-control columns-select"  name="excel_columns[contract_end]"></select>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Дата оплаты
                            </th>
                            <td>
                                <select class="form-control columns-select"  name="excel_columns[payment_date]"></select>
                            </td>
                        </tr>


                        <tr>
                            <th>
                                Сумма
                            </th>
                            <td>
                                <select class="form-control columns-select"  name="excel_columns[payment_total]"></select>
                            </td>
                        </tr>

                        <tr>
                            <th>
                                Указать признак оплаты
                            </th>
                            <td class="text-center">
                                <input class="only-paid" name="excel_columns[payment_mark]" type="checkbox" value="1">
                            </td>
                        </tr>
                        <tr class="paid hidden">
                            <th>
                                Признак оплаты - колонка
                            </th>
                            <td>
                                <select class="form-control columns-select" name="excel_columns[payment_column]"></select>
                            </td>
                        </tr>
                        <tr class="paid hidden">
                            <th>
                                Признак оплаты - текст
                            </th>
                            <td>
                                <input class="form-control" name="excel_columns[payment_text]" type="text">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>


            {{Form::close()}}

            </div>


        </div>
    </div>




@endsection

@section('js')



    <script>





        $(function () {

            $('.only-paid').change(function () {
                $('tr.paid').toggleClass('hidden', !$(this).is(':checked'));
            });

            $('[name=bso_suppliers]').change(function () {
                loadProducts();
            });

            loadProducts();
            $("#buttonSend").hide();

            $("#addManyDocFormDownloadingContracts").dropzone({
                paramName: 'file',
                maxFilesize: 10,
                //acceptedFiles: "image/*",
                init: function () {
                    this.on("complete", function (response) {

                        $('.columns-row').removeClass('hidden');
                        var jsonResult = JSON.parse(response.xhr.response);
                        console.log(jsonResult);
                        var selectOptions = makeSelectOptions(prepareColumnsToMakeOptions(jsonResult.columns));
                        $('.columns-select').html(selectOptions);
                        $('.submit-btn').prop('disabled', false);
                        $('[name=file]').val(jsonResult.file);
                        setDefaultValues();
                        $("#buttonSend").show();
                    });
                }
            });






        });


        function prepareColumnsToMakeOptions(columns) {
            var result = $.map(columns, function (item) {
                return {
                    id: item,
                    title: item
                };
            });
            result.unshift({
                id: '',
                title: 'Не выбрано'
            });
            return result;
        }

        function setDefaultValues() {
            $('select[name="excel_columns[bso_title]"]').val('Номер БСО');
            $('select[name="excel_columns[insurer_title]"]').val('Страхователь');
            $('select[name="excel_columns[contract_date]"]').val('Дата оформления');
            $('select[name="excel_columns[contract_begin]"]').val('Дата начала');
            $('select[name="excel_columns[contract_end]"]').val('Дата окончания');
            $('select[name="excel_columns[payment_date]"]').val('Дата оформления');
            $('select[name="excel_columns[payment_total]"]').val('Сумма премии в рублях');

        }


        function loadProducts() {
            var bso_suppliers = $('[name=bso_suppliers]').val();
            if (!bso_suppliers) {
                return false;
            }

            $.getJSON('{{url('/bso/actions/get_bso_product/')}}', {bso_supplier_id:bso_suppliers}, function (response) {

                var options = "<option value='0'>Не выбрано</option>";
                response.map(function (item) {
                    options += "<option value='" + item.product_id + "'>" + item.title + "</option>";
                });

                $("#product_id").html(options).val(0);

            });




        }

    </script>


@endsection