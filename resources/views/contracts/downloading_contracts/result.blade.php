@extends('layouts.app')

@section('content')


    <div class="page-heading">
        <h2>Загружено акцепт: Создано {{count($list)}} из {{$all_count}}</h2>


    </div>


    <table class="tov-table">
        <thead>
        <tr>
            <th><a href="javascript:void(0);">Договоры</a></th>
        </tr>
        </thead>
        @if(sizeof($list))
        @foreach($list as $r)
            <tr>
                <td>{{$r['bso_title']}}</td>
            </tr>
        @endforeach
        @else
            <tr>
                <td>{{ trans('form.empty') }}</td>
            </tr>

        @endif
    </table>



@endsection

@section('js')

    <script>




        $(function () {


        });



    </script>


@endsection