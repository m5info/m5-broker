@extends('layouts.app')

@section('content')

{{--@section('work-area')--}}
{{--    @include("contracts.orders.order.workarea", ['order', $order])--}}
{{--@append--}}

<div class="page-heading">
    <div class="col-lg-8">
        <h2>Заявка на оформление №{{ $order->id }} - {{ \App\Models\Contracts\Orders::STATUSES[$order->status_id] }}</h2>
    </div>
</div>
@if(auth()->user()->hasPermission('orders', 'order_statuses'))
    <div class="col-lg-2">
        <div class="work-area-item">
            {{ Form::select('order[0][status_to_change]', collect($select_visability), $order->status_id, ['class' => 'form-control select2-ws pull-right ']) }}
        </div>
    </div>
    <div class="col-lg-2">
        <span class="btn btn-success" onclick="saveOrder({{$order->status_id}});">
            Сохранить
        </span>
    </div>
@else
    <div class="col-lg-4">
        @php
            $status2down = [
                1 => ['new_status' => 0, 'title' => 'Вернуть во временные'],
                2 => ['new_status' => 1, 'title' => 'Вернуть в оформление'],
                3 => ['new_status' => 2, 'title' => 'Вернуть на печать'],
            ];
            $status2up = [
                0 => ['new_status' => 1, 'title' => 'В оформление'],
                1 => ['new_status' => 2, 'title' => 'На печать'],
                2 => ['new_status' => 3, 'title' => 'На доставку'],
                3 => ['new_status' => 4, 'title' => 'В Реализованные'],
            ];
        @endphp

        @if($order->status_id == 2 && ($auth_user->is('operator_bso') || $auth_user->is('formalizator') || $auth_user->role_id == 1))
            @if($order->status_id != 3)
                @if($order->status_id < 4)
                    @if(!($order->status_id == 1 && !($auth_user->is('formalizator'))) || $auth_user->role_id == 1)
                    <span class="btn btn-success btn-right" onclick="changeStatus({{$status2up[$order->status_id]['new_status']}});">
                        {{$status2up[$order->status_id]['title']}}
                    </span>
                    <input type="hidden" id="status_to_change" name="order[0][status_to_change]" value="{{$status2up[$order->status_id]['new_status']}}">
                    @endif
                @endif

                @if($order->status_id > 0 && $order->status_id < 4)
                    @if(!(Auth::user()->hasPermission('role_owns', 'is_agent') && $order->status_id == 1))
                        <span class="btn btn-primary btn-right" onclick="changeStatusDown({{$status2down[$order->status_id]['new_status']}});">
                            {{$status2down[$order->status_id]['title']}}
                        </span>
                        <input type="hidden" id="status_back" name="order[0][status_to_change]" value="{{$status2down[$order->status_id]['new_status']}}">
                    @endif
                @endif
            @endif
        @endif
    </div>
@endif
<div class="row form-horizontal" id="main_container">
    <div class="policy-blocks">


        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-top:15px;">
            <div class="block-view">
                <div class="block-main">
                    <div class="block-sub">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <div class="row form-horizontal">
                                <div class="view-field">
                                    <span class="view-label">Менеджер</span>
                                    @php
                                        $manager = \App\Models\User::find($order->manager_id ? $order->manager_id : $order->create_user_id)
                                    @endphp
                                    <span class="view-value">{{ "$manager->name $manager->email" }}</span>
                                    <input type="hidden" name="order[0][manager_id]" value="{{ $order->manager_id ? $order->manager_id : $order->create_user_id }}">
                                </div>
                                <div class="view-field">
                                    <span class="view-label">Срочно</span>
                                    <span class="view-value">{{ $order->urgency ? "Да" : "Нет" }}</span>
                                    <input type="hidden" name="order[0][urgency]" value="{{ $order->urgency ? 1 : 0 }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <h3>Информация по доставке</h3>
                <div class="row">
                    @if(isset($order->is_delivery) && $order->is_delivery == 1)
                        <div class="col-lg-12">
                            <div class="view-field">
                                <span class="view-label">Доставка</span>
                                <span class="view-value">Да</span>
                                <input type="hidden" name="order[0][delivery][is_delivery]" value="1">
                            </div>
                        </div>
                        @php
                            if($order->is_delivery == 1){
                                $delivery_show = '';
                            }else{
                                $delivery_show = 'style=display:none';
                            }
                        @endphp
                        <div class="is_delivery" {{ $delivery_show }}>
                            <div>
                                <div class="col-lg-6">
                                    <div class="field form-col">
                                        @php
                                            $date = setDateTimeFormatRu($order->delivery_date, 1);
                                            $time = $order->delivery_time;
                                        @endphp
                                        <div class="view-field">
                                            <span class="view-label">Дата</span>
                                            <span class="view-value">{{ $date }}</span>
                                            <input type="hidden" name="order[0][delivery][date]" value="{{ $date }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="field form-col">
                                        <div class="view-field">
                                            <span class="view-label">Время</span>
                                            <span class="view-value">{{ $time }}</span>
                                            <input type="hidden" name="order[0][delivery][time]" value="{{ $time }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="view-field">
                                    <span class="view-label">Город</span>
                                    <span class="view-value">{{ $order->delivery_city_id ? \App\Models\Settings\City::find($order->delivery_city_id)->title : '' }}</span>
                                    {{ Form::hidden('order[0][delivery][city_id]', $order->delivery_city_id, ['id' => 'city_id']) }}
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="view-field">
                                    <span class="view-label">Метро</span>
                                    <span class="view-value">{{ $order->delivery_metro }}</span>
                                    {{ Form::hidden('order[0][delivery][metro]', $order->delivery_metro, ['id' => 'metro']) }}
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="view-field">
                                    <span class="view-label">Адрес</span>
                                    <span class="view-value">{{ $order->address }}</span>
                                    <input type="hidden" name="order[0][delivery][address]" value="{{ $order->address }}">
                                    {{ Form::hidden('order[0][delivery][address_kladr]', $order->address_kladr, ['id' => 'address_kladr']) }}
                                    {{ Form::hidden('order[0][delivery][address_width]', $order->address_width, ['id' => 'address_width']) }}
                                    {{ Form::hidden('order[0][delivery][address_longitude]', $order->address_longitude, ['id' => 'address_longitude']) }}
                                </div>
                            </div>
                            @if($order->status_id >= 3)
                                <div class="col-lg-12">
                                    <div class="field form-col">
                                        <div class="view-field">
                                            <span class="view-label">Курьер</span>
                                            <span class="view-value">{{ $order->delivery_user_id ? \App\Models\User::find($order->delivery_user_id)->name : '' }}</span>
                                            <input type="hidden" name="order[0][delivery][courier]" value="{{ $order->delivery_user_id }}">
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="col-lg-12">
                                <div class="view-field">
                                    <span class="view-label">Комментарий к доствке</span>
                                    <span class="view-value">{{ $order->delivery_comment }}</span>
                                    <input type="hidden" name="order[0][delivery][comment]" value="{{ $order->delivery_comment }}">
                                </div>
                            </div>
{{--                            @if($order->delivery_user_id && $order->status_id >= 3)--}}
{{--                                <div class="col-lg-12">--}}
{{--                                    <span class="btn btn-success btn-right" onclick="RunForestRun({{$order->status_id}});">--}}
{{--                                        Выезд курьера--}}
{{--                                    </span>--}}
{{--                                </div>--}}
{{--                            @endif--}}
                        </div>
                    @else
                        <div class="col-lg-12">
                            <label  class="control-label" style="max-width:100%;">Доставка не нужна</label>
                            {{ Form::hidden('order[0][delivery][is_delivery]', 0) }}
                        </div>
                    @endif

                </div>



            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-top:15px;">
            <div class="block-view">
                <h3>Сообщения</h3>

                <div class="clear"></div>

                <div class="row">
                    <div class="col-lg-12">
                        <div>
                            @if(isset($order->comments))
                                @foreach($order->comments as $comment)
                                    <div class="col-lg-12" style="padding: 14px 20px;margin-bottom: 18px;background-color: #f5f5f5;border-radius: 2px;">
                                        @php
                                            if($author_id = collect($comment->only('proccesing_user_id', 'agent_id'))->max() != 0){
                                                 $author = \App\Models\User::find($author_id)->name;
                                            }else{
                                                $author = '';
                                            }
                                        @endphp
                                        {{ $author }} ({{$comment->created_at}}):<br><br>{!! $comment->comment !!}
                                    </div>
                                @endforeach
                            @endif
                            {{ Form::textarea('order[0][workarea_comment]', '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
            </div>
            @if(!(auth()->user()->hasPermission('orders', 'order_statuses')))
                <div class="col-lg-12">
                <span class="btn btn-success btn-right" onclick="saveOrder({{$order->status_id}});">
                    Сохранить
                </span>
                </div>
            @endif
        </div>

        @include("contracts.orders.order.partials.contracts_table", ['temp' => 'view'])

        @include("contracts.orders.order.partials.reserve_list", ['temp' => 'view'])

    </div>
</div>
@endsection

@section('js')

    @include('contracts.orders.order.js_inside')

@endsection