@extends('layouts.app')
@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">
@append

@section('work-area')
    @include("contracts.orders.order.created_contract.partials.workarea")
@append

@section('content')
<div id="main_container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <a href="{{ url("contracts/orders/edit/$order->id") }}">{{ "Заявка № $order->id" }}</a>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Условия договора -->
        @include("contracts.orders.order.created_contract.partials.terms")

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <!-- Страхователь -->
            @include("contracts.orders.order.created_contract.partials.insurer")

        <!-- Объект страхования -->
{{--        @if(!$permission)--}}
            @include("contracts.orders.order.created_contract.partials.insurance_object")
{{--        @endif--}}

        <!-- Документы -->
            @include("contracts.orders.order.created_contract.partials.documents")
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <!-- Платежи -->
        @include("contracts.orders.order.created_contract.partials.payments")

    </div>
</div>
@endsection
@section('js')
    <script>

        $('#insurer_fio_' + 0).suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "NAME",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#insurer_title_' + key).val($(this).val());

            }
        });

        function initInsurer()
        {
            $('#insurer_type_0').change(function () {

                if(parseInt($(this).val()) == 0){
                    $('.insurer_fl').show();
                    $('.insurer_ul').hide();
                }else{
                    $('.insurer_fl').hide();
                    $('.insurer_ul').show();
                }

            });

            $('#insurer_type_0').change();

            $('#insurer_fio_'+0).suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "NAME",
                count: 5,
                onSelect: function (suggestion) {
                    key = $(this).data('key');
                    $('#insurer_title_'+key).val($(this).val());

                }
            });

            $('#insurer_title_'+0+', #insurer_inn_'+0+', #insurer_kpp_'+0).suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "PARTY",
                count: 5,
                onSelect: function (suggestion) {
                    var data = suggestion.data;

                    key = $(this).data('key');


                    $('#insurer_title_'+key).val(suggestion.value);
                    $('#insurer_inn_'+key).val(data.inn);
                    $('#insurer_kpp_'+key).val(data.kpp);

                    $('#insurer_fio_'+key).val($('#insurer_title_'+key).val());

                }
            });


        }

        function getModelsObjectInsurer(KEY, select_model_id) {

            $.getJSON(
                '{{url("/contracts/actions/get_models")}}',
                {categoryId:$('#object_insurer_ts_category_'+KEY).val(),markId: $('#object_insurer_ts_mark_id_'+KEY).select2('val')},
                function (response) {

                    var options = "<option value='0'>Не выбрано</option>";
                    response.map(function (item) {
                        options += "<option value='" + item.id + "'>" + item.title + "</option>";
                    });

                    $('#object_insurer_ts_model_id_'+KEY).html(options).select2('val', select_model_id);

                });

        }

        function viewControllerSalesCondition() {
            if ($("#sales_condition_0").val() != 0) {
                $(".sales_condition_not_agent").show();
            } else {
                $(".sales_condition_not_agent").hide();
            }

        }

        function removeBorderColor(el){
            $(el).css('borderColor', '#e0e0e0');
        }

        function get_end_dates(start_date) {
            var cur_date_tmp = start_date.split(".");
            var cur_date = new Date(cur_date_tmp[2], cur_date_tmp[1] - 1, cur_date_tmp[0]);
            var new_date = new Date(cur_date.setYear(cur_date.getFullYear() + 1));
            var new_date2 = new Date(new_date.setDate(new_date.getDate() - 1));
            return getFormattedDate(new_date2);
        }

        function setAllDates(key) {
            sign_date = $("#sign_date_" + key).val();
            $("#begin_date_" + key).val(sign_date);

            return setEndDates(key);
        }

        function setEndDates(key) {
            begin_date = $("#begin_date_" + key).val();
            $("#end_date_" + key).val(get_end_dates(begin_date));
        }

        function activSearchBso(object_id, key, type)
        {

            $('#' + object_id + key).suggestions({
                serviceUrl: "/contracts/orders/get_bso_cart/",
                type: "PARTY",
                params:{type_bso:type, product_id:'{{ $contract->product_id }}', order_id: '{{ $contract->order->id }}', bso_supplier_id: '{{ $contract->bso_supplier_id }}', bso_agent_id:'{{ auth()->id() }}'},
                count: 5,
                minChars: 3,
                formatResult: function (e, t, n, i) {
                    var s = this;
                    var title = n.value;
                    var bso_type = n.data.bso_type;
                    var bso_sk = n.data.bso_sk;
                    var agent_name = n.data.agent_name;
                    var view_res = title;
                    view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">СК</span>' + bso_sk + "</div>";
                    view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Тип</span>' + bso_type + "</div>";
                    view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Агент</span>' + agent_name + "</div>";
                    return view_res;
                },
                onSelect: function (suggestion) {
                    selectBso(object_id, key, type, suggestion);
                }
            });
        }

        $(document).on('change', '[name*="[pay_method_id]"]', function(){
            toggle_pay_method();
        });


        function toggle_pay_method() {

            $.each($('[name*="pay_method_id"]'), function(k,v){

                if($(this).val() == 1)
                    $('#bso_receipt').attr('required','required');
                else
                    $('#bso_receipt').removeAttr('required');

                var key = $(v).attr('data-key');

                var method_blocks = {
                    0:'receipt_block_'+key,
                    1:'check_block_'+key,
                    2: 'none'
                };
                var method = parseInt($(v).val());

                $.each(method_blocks, function(k,v){
                    $("#" + v).hide()
                });
                $("#" + method_blocks[pay_methods_id2types[method]]).show()
            });


        }

        $(function () {

            activSearchBso("bso_title", '_0', 1);
            activSearchBso("bso_receipt", '_0', 2);
            initWorkArea();
            initInsurer();
            toggle_pay_method();
            initInfo();
            initInsurer();
        });

        function initInfo() {

            viewControllerSalesCondition();

            @if($contract->bso)
                getOptionFinancialPolicy("financial_policy_id_0", '{{$contract->bso->insurance_companies_id}}', '{{$contract->bso->bso_supplier_id}}', '{{$contract->product_id}}', '{{$contract->financial_policy_id}}');

                getOptionInstallmentAlgorithms("installment_algorithms_id_0", '{{$contract->bso->insurance_companies_id}}', '{{$contract->installment_algorithms_id}}');
            @endif

            viewSetFinancialPolicyManually($("#financial_policy_manually_set_0"), '0');

            activSearchOrdersToFront("order_title", "_0");

        }

        function selectBso(object_id, key, type, suggestion)
        {
            var data = suggestion.data;
            if (parseInt(type) == 1){ // БСО
                $('#bso_id' + key).val(data.bso_id);
                $('#bso_supplier_id' + key).val(data.bso_supplier_id);
                $('#insurance_companies_id' + key).val(data.insurance_companies_id);
                $('#product_id' + key).val(data.product_id);
                $('#agent_id' + key).val(data.agent_id);
                $('#sk_title').val(data.bso_sk);
                $('#agent_title').val(data.agent_name);
            }



            if (parseInt(type) == 2) { // Квитанциия
                $('#bso_receipt_id' + key).val(data.bso_id);
            }
            getKvAccesses(key, data.product_id);

            getOptionInstallmentAlgorithms("installment_algorithms_id" + key, data.insurance_companies_id, 0);
            getOptionFinancialPolicy("financial_policy_id" + key, data.insurance_companies_id, data.bso_supplier_id, data.product_id, 0);

            activSearchOrdersToFront("order_title",key);
        }

        function save_contract(){

            $.ajax({
                type: "POST",
                url: "{{url("/contracts/orders/contract/{$contract->id}/edit")}}",
                async: false,
                data: $("#main_container").find("select, textarea, input").serialize(),

                success: function (response) {
                    flashHeaderMessage('Успешно сохранено!', 'success');
                }
            });
        }


        function process(operations = []) {
            $.each(operations, function (k, operation) {
                if (isFunction(operation)) {
                    window[operation]()
                }
            })
        }


        function addPayment() {

            save_contract();

            setTimeout(function(){
                if (myGetAjax("{{url("/contracts/orders/contract/{$contract->id}/add_pay")}}")) {
                     reload();

                }
            }, 50);

        }


        function setAlgos(key){

            var element = $( "#installment_algorithms_id_"+ key );

            if(element.val() > 0){

                var algorithm_id = element.val();

                if (getInputsInstallmentAlgorithms("payment_algo_" + key, algorithm_id, key, {{$contract->id}})){

                    $.ajax({
                        type: "POST",
                        url: "{{url("/settings/installment_algorithms_list/get_quantity")}}",
                        async: true,
                        data: {'algorithm_id' : algorithm_id},
                        success: function (response) {

                            var str = $('[name="contract['+key+'][payment_total]"]').val().replace(/\s/g, '');
                            var premium = parseInt(str);

                            if(parseInt($('[name="contract['+key+'][payment_total]"]').val()) > 0){

                                var payment_sum = premium / response;
                                $('[name="contract['+key+'][payment][0][payment_total]"]').val(payment_sum);

                                var indexes = response + 1;

                                for (var i = 1; i < indexes; i++) {
                                    $('#algo_payment_sum_'+ i).val(payment_sum);
                                }
                            };
                        }
                    });
                }

            }else{
                $("#payment_algo_" + key).html('');
            }
        }

        function setPaymentQty(key) {

            var con = $('input[name="contract[' + key + '][payment_total]"]').val();
            $('input[name="contract[' + key + '][payment][0][payment_total]"]').val(CommaFormatted(con));
            updateAlgos(key);

        }

        function updatePaymentQty(key) {
            updateAlgos(key);
        }

        function updateAlgos(key) {

            var element = $("#installment_algorithms_id_" + key);

            var pay_parts = $('.algo_payment_sum_'+key);
            var total_pay_parts_sum = 0.00;

            pay_parts.each(function(){
                total_pay_parts_sum += parseFloat($(this).val().replaceAll(' ', '').replace(',', '.'));
            });

            var contract_award = $('[name="contract[' + key + '][payment_total]"]').val().replaceAll(' ','').replace(',','.');

            var total = parseFloat(contract_award) - parseFloat(total_pay_parts_sum);

            $('[name="contract['+key+'][payment][0][payment_total]"]').val(CommaFormatted(total));

            if (element.val() > 0) {

                var algorithm_id = element.val();

            } else {
                $("#payment_algo_" + key).html('');
            }
        }


        function viewControllerSalesCondition()
        {
            if($("#sales_condition_0").val() != 0)
            {
                $(".sales_condition_not_agent").show();
            }else
            {
                $(".sales_condition_not_agent").hide();
            }

        }

        function viewSetFinancialPolicyManually(obj, key)
        {
            if($(obj).is(':checked')){
                $("#financial_policy_manually_"+key).show();
                $("#financial_policy_id_"+key).hide();
            }else{
                $("#financial_policy_manually_"+key).hide();
                $("#financial_policy_id_"+key).show();
            }
        }


        function deleteTempPayment(pay) {
            save_contract();

            setTimeout(function(){
                if (myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/del_pay/?pay=")}}" + pay)) {
                    reload();
                }
            }, 50);
        }

    </script>
@endsection