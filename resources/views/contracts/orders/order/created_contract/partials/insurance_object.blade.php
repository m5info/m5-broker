<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-subheading">
        <h2>Объект страхования</h2>
    </div>
    <div class="block-main">
        <div class="block-sub">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @include("contracts.contract_templates.default.insurance_object.partials.edit.{$contract->product->category->template}", ['key' => 0, 'object' => $object_insurer ])
            </div>
        </div>
    </div>
</div>
