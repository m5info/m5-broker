<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-subheading">
        <h2>Cтрахователь</h2>
    </div>
    <div class="block-main">
        <div class="block-sub">
            <input type="hidden" name="contract[0][insurer][id]" value="{{$contract->insurer_id}}"/>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="row form-horizontal">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label">Тип</label>
                        {{ Form::select('contract[0][insurer][type]', collect([0=>"ФЛ", 1=>'ЮЛ']), ($contract->insurer)?$contract->insurer->type:0, ['class' => 'form-control select2-ws', 'id'=>'insurer_type_0', 'data-key'=>'0']) }}
                    </div>
                    <div class="insurer_fl col-xs-12 col-sm-12 col-md-9 col-lg-9">
                        <label class="control-label">ФИО</label>
                        {{ Form::text('contract[0][insurer][fio]', ($contract->insurer)?$contract->insurer->title:'', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_fio_0', 'data-key'=>'0']) }}
                    </div>
                    <div class="insurer_ul col-xs-12 col-sm-12 col-md-9 col-lg-9" style="display: none">
                        <label class="control-label">Название</label>
                        {{ Form::text('contract[0][insurer][title]', ($contract->insurer)?$contract->insurer->title:'', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_title_0', 'data-key'=>'0']) }}
                    </div>
                </div>
            </div>

            <div class="insurer_fl col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <label class="control-label">Паспорт</label>
                <div class="row form-horizontal">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        {{ Form::text('contract[0][insurer][doc_serie]', ($contract->insurer)?$contract->insurer->doc_serie:'', ['id' => 'insurer_doc_serie_0','class' => 'form-control', 'placeholder'=>'Серия']) }}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        {{ Form::text('contract[0][insurer][doc_number]', ($contract->insurer)?$contract->insurer->doc_number:'', ['id' => 'insurer_doc_number_0','class' => 'form-control', 'placeholder'=>'Номер']) }}
                    </div>
                </div>
            </div>
            <div class="insurer_ul col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none">
                <div class="row form-horizontal">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <label class="control-label">ИНН</label>
                        {{ Form::text('contract[0][insurer][inn]', ($contract->insurer)?$contract->insurer->inn:'', ['class' => 'form-control', 'id'=>'insurer_inn_0', 'data-key'=>'0']) }}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <label class="control-label">КПП</label>
                        {{ Form::text('contract[0][insurer][kpp]', ($contract->insurer)?$contract->insurer->kpp:'', ['class' => 'form-control', 'id'=>'insurer_kpp_0', 'data-key'=>'0']) }}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="row form-horizontal">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <label class="control-label">Телефон</label>
                        {{ Form::text('contract[0][insurer][phone]', ($contract->insurer)?$contract->insurer->phone:'', ['id' => 'insurer_phone_0','class' => 'form-control phone']) }}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <label class="control-label">Email</label>
                        {{ Form::text('contract[0][insurer][email]', ($contract->insurer)?$contract->insurer->email:'', ['id' => 'insurer_email_0','class' => 'form-control',]) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
