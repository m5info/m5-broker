<div id="contract_payments" class="row">
    <div class="page-subheading col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h2 class="inline-h1">Платежи</h2>
        @if($contract->kind_acceptance != 1)
            <span class="btn btn-success btn-right" onclick="addPayment()">
            <i class="fa fa-plus-circle"></i> Добавить платеж
        </span>

        @endif
    </div>
    @if(sizeof($contract->all_payments))
        @foreach($contract->all_payments as $key => $payment)
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="payment_block block-main">
                    <div class="block-sub">
                        <div class="row">
                            @php
                                $view_payment = $payment->invoice_id && $payment->statys_id == 1 ? 'paid_invoice' : 'edit';
                            @endphp

                            @include(
                                "contracts.contract_templates.default.payments.partials.{$view_payment}.payment",
                                ['contract' => $contract, 'payment' => $payment, 'key' => $key]
                            )
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>