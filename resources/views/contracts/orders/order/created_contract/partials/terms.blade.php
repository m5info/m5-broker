<div class="row col-xs-12 col-sm-6 col-md-6 col-lg-6">
    <div class="page-subheading">
        <h2>Условия договора</h2>
    </div>
    <div class="block-main">
        <div class="block-sub">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">Андеррайтер</label>
                    <br>
                    <br>
                    <p>{{$contract->check_user ? $contract->check_user->name : "Нет"}}</p>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">Номер договора</label>
                    @php
                        if($contract->order->status_id == 2 || auth()->user()->role->id == 5 || !$contract->bso){ // если на печати или юзер оформитель
                            $disabled = '';
                        }else{
                            $disabled = 'disabled';
                        }
                    @endphp
                    {{ Form::text('contract[0][bso_title]', $contract->bso ? $contract->bso->bso_title : '', ['class' => 'form-control', 'id'=>'bso_title_0' , $disabled]) }}
                    <input type="hidden" name="contract[0][bso_id]" id="bso_id_0" value="{{ $contract->bso ? $contract->bso->id : ''}}"/>
                    <input type="hidden" name="contract[0][bso_supplier_id]" id="bso_supplier_id_0"  value="{{ $contract->bso ? $contract->bso->bso_supplier_id : ''}}" />
                    <input type="hidden" name="contract[0][insurance_companies_id]" id="insurance_companies_id_0"  value="{{ $contract->bso ? $contract->bso->insurance_companies_id : ''}}" />
                    <input type="hidden" name="contract[0][product_id]" id="product_id_0"  value="{{ $contract->bso ? $contract->bso->product_id : ''}}" />
                    <input type="hidden" name="contract[0][agent_id]" id="agent_id_0"  value="{{ $contract->bso ? $contract->bso->agent_id : ''}}" />

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label>{{($contract->bso)?$contract->bso->insurance->title:''}} {{($contract->bso)?$contract->bso->supplier_org->title:''}}</label>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">ФИО Агента</label>
                    <br>
                    <br>
                    <p>{{$contract->agent ? $contract->agent->name : ""}}</p>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">Заявка из фронт офиса</label>
                    <input class="form-control" id="order_title_0" name="contract[0][order_title]" type="text" value="{{$contract->order_title}}" >
                    <input type="hidden" name="contract[0][order_id]" id="order_id_0" value="{{$contract->order_id}}">
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">Тип договора</label>
                    {{ Form::select('contract[0][type_id]', collect(\App\Models\Contracts\Contracts::TYPE), $contract->type_id, ['class' => 'form-control select2-ws', 'id'=>'type_id_0']) }}
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">Страховая сумма</label>
                    <input class="form-control sum" id="insurance_amount_0" name="contract[0][insurance_amount]" type="text" value="{{$contract->getInsuranceAmount()}}" onkeyup="$('#payment_contract_total_0').val($('#insurance_amount_0').val());">
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">Дата заключения</label>
                    <input class="form-control datepicker valid_fast_accept date" id="sign_date_0" onchange="setAllDates(0)" name="contract[0][sign_date]" type="text" value="{{setDateTimeFormatRu($contract->sign_date, 1)}}">
                </div>

                @if(!$contract->is_online)
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label">Период действия</label>
                        <div class="row form-horizontal">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <input class="form-control datepicker valid_fast_accept date" id="begin_date_0" placeholder="Дата начала" onchange="setEndDates(0)" name="contract[0][begin_date]" type="text" value="{{setDateTimeFormatRu($contract->begin_date, 1)}}">
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <input class="form-control datepicker valid_fast_accept date" placeholder="Дата окончания" id="end_date_0" name="contract[0][end_date]" type="text" value="{{setDateTimeFormatRu($contract->end_date, 1)}}">
                            </div>
                        </div>
                    </div>
                @endif


                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">Банк</label>
                    {{ Form::select('contract[0][bank_id]', collect(\App\Models\Settings\Bank::all())->pluck('title', 'id')->prepend('Не выбрано', 0), $contract->bank_id, ['class' => 'form-control select2-all', 'id'=>'bank_id_0']) }}
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label">Условие продажи</label>
                    {{ Form::select('contract[0][sales_condition]', collect(\App\Models\Contracts\Contracts::SALES_CONDITION), isset($contract->sales_condition) ? $contract->sales_condition : 0, ['class' => 'form-control select2-ws', 'id'=>'sales_condition_0', "onchange" =>"viewControllerSalesCondition()"]) }}
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sales_condition_not_agent">
                    <label class="control-label pull-left" style="margin-top: 5px;">
                        Менеджер/Агент
                    </label>
                    <label class="control-label pull-right">Личная продажа <input type="checkbox" value="1" id="is_personal_sales_0" name="contract[0][is_personal_sales]" @if($contract->is_personal_sales == 1) checked @endif/> </label>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sales_condition_not_agent manager_view_outer">
                    {{ Form::select('contract[0][manager_id]', $agents->prepend('Выберите значение', 0), $contract->manager_id ? $contract->manager_id : 0, ['class' => 'form-control select2', 'id'=>'manager_id_0']) }}
                    <br/>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <label class="control-label">Премия по договору</label>
                    {{ Form::text('contract[0][payment_total]', ($contract->payment_total>0)?titleFloatFormat($contract->payment_total):'', ['class' => 'form-control sum', 'id'=>'payment_contract_total_0', "onkeyup" => "$('#payment_total_0').val($('#payment_contract_total_0').val());"]) }}
                </div>



                @if(auth()->user()->hasPermission('contracts', 'select_financial_policy'))
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label class="control-label pull-left" style="margin-top: 5px;">
                            Финансовая политика
                        </label>
                        @if(auth()->user()->hasPermission('contracts', 'set_financial_policy_manually'))
                            <label class="control-label pull-right">Указать в ручную <input type="checkbox" value="1" @if($contract->financial_policy_manually_set==1) checked @endif name="contract[0][financial_policy_manually_set]" id="financial_policy_manually_set_0" onchange="viewSetFinancialPolicyManually(this, '0')"/> </label>
                        @endif
                        <div class="wraper-inline-100" id="financial_policy_id_block">
                            {{ Form::select( 'contract[0][financial_policy_id]', collect([0=>'Укажите номер договора']), 0, ['class' => 'form-control select2-all', 'id'=>'financial_policy_id_0'] ) }}
                        </div>
                    </div>


                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >

                        @if(auth()->user()->hasPermission('contracts', 'set_financial_policy_manually'))
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ></div>
                            <div id="financial_policy_manually_0" style="display: none;">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    <label class="control-label">Бордеро</label>
                                    {{ Form::text('contract[0][financial_policy_kv_bordereau]', $contract->financial_policy_kv_bordereau, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_bordereau_0']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    <label class="control-label">ДВОУ</label>
                                    {{ Form::text('contract[0][financial_policy_kv_dvoy]', $contract->financial_policy_kv_dvoy, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_dvoy_0']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    <label class="control-label">Агента</label>
                                    {{ Form::text('contract[0][financial_policy_kv_agent]', $contract->financial_policy_kv_agent, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_agent_0']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    <label class="control-label">Руков.</label>
                                    {{ Form::text('contract[0][financial_policy_kv_parent]', $contract->financial_policy_kv_parent, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_parent_0']) }}
                                </div>
                            </div>
                        @endif
                    </div>
                @endif

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <label class="control-label">Алгоритм рассрочки</label>
                    @php
                        if($contract->installment_algorithms_id && $contract->installment_algorithms_id != 0){
                            $title = \App\Models\Directories\InstallmentAlgorithms::find($contract->installment_algorithms_id)->algorithm->title;
                            $algo_prepend = collect([$contract->installment_algorithms_id=>$title]);
                            $no_algo_style = '';
                        }else{
                            $algo_prepend = collect([0=>'Не выбрано']);
                            $no_algo_style = 'style=display:none';
                        }
                    @endphp
                    {{ Form::select('contract[0][installment_algorithms_id]', $algo_prepend, $contract->installment_algorithms_id ? $contract->installment_algorithms_id : 0, ['class' => 'form-control select2-ws', 'id'=>'installment_algorithms_id_0', 'onchange' => 'setAlgos(0)']) }}
                </div>

                <div id="payment_algo_0" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    @if($contract->installment_algorithms_id && $contract->installment_algorithms_id != 0 && $contract->expected_payments)
                        @foreach($contract->expected_payments as $i => $expected_payment)
                            <input type="hidden" name="contract[0][algo_payment][{{$i}}][id]" value="{{ $expected_payment->id }}">
                            <div class="row form-horizontal">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <label class="control-label">Дата следующего платежа</label>
                                    {{ Form::text("contract[0][algo_payment][$i][date]", setDateTimeFormatRu($expected_payment->payment_date, 1) ?? '', ['class' => 'form-control datepicker date', 'id'=>"algo_payment_date_$i"]) }}
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <label class="control-label">Сумма следующего платежа</label>
                                    {{ Form::text("contract[0][algo_payment][$i][sum]", ($expected_payment->payment_sum>0)?titleFloatFormat($expected_payment->payment_sum):'', ['class' => 'form-control sum', 'id'=>"algo_payment_sum_$i", 'onchange'=>'setPaymentQty(0)']) }}
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>

                <div id='payment_algo' style="display: none;">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                        <label class="control-label">Дата следующего платежа</label>
                        {{ Form::text('contract[0][payment_next_date]', setDateTimeFormatRu($contract->payment_next_date, 1) ?? '', ['class' => 'form-control datepicker date', 'id'=>'payment_next_date_0']) }}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                        <label class="control-label">Сумма следующего платежа</label>
                        {{ Form::text('contract[0][payment_next_sum]', ($contract->payment_next_sum>0)?titleFloatFormat($contract->payment_next_sum):'', ['class' => 'form-control sum', 'id'=>'payment_next_sum_0', 'onchange'=>'setPaymentQty(0)']) }}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
