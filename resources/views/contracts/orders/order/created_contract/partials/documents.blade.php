<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-subheading">
        <h2>Документы</h2>
    </div>
    <div class="block-main">
        <div class="block-sub">
            @include('contracts.orders.order.documents',['order' => $order])
        </div>
    </div>
</div>
