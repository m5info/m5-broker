<style>
    .block-view{
        padding: 20px 35px 35px 35px;
    }
    .contract-block{
        display: block;
        padding: 15px 15px 15px 15px;
        border: 1px solid #e4e4e4;
        margin-bottom: 11px;
        margin-top: 10px;
        border-radius: 8px;
        background-color: #fff;
        box-shadow: 0 5px 20px rgba(0, 0, 0, .15);
    }
    .contract-block div{
        color: #7d7f81;
        font-family: "AgoraSansProRegular", "serif";
        font-size: 14px;
        font-weight: normal;
    }
    .contract-block:hover,
    .contract-block:visited,
    .contract-block:link,
    .contract-block:active
    {
        text-decoration: none;
    }
</style>

<script src="/js/jquery.datetimepicker.full.min.js"></script>
<script src="/js/bso/transfer.js"></script>
<link rel="stylesheet" href="/css/jquery.datetimepicker.min.css">

<script>

    // $(function(){
    //     initWorkArea();
    // });

    $("#cart_create").on('click', function () {
        var bso_cart_id = createCart();
        $('#bso_cart_id').val(bso_cart_id);
        saveOrder({{$order->status_id}});
    });

    function saveOrder(status_id){

        var data = $("#main_container").find("select, textarea, input").serialize();
        var message = $('[name="order[0][workarea_comment]"]').serialize();
        var status_to_change = status_id;

        $.ajax({
            type: "POST",
            url: "{{url("/contracts/orders/save/{$order->id}")}}",
            async: false,
            data: data + '&' +message+ '&order[0][status_to_change]=' +status_to_change,

            success: function (response) {
                flashHeaderMessage('Успешно сохранено!', 'success');
                reload();
            }
        });
    }


    function releaseOrder(status_id){

        var data = $("#main_container").find("select, textarea, input").serialize();
        var message = $('[name="order[0][workarea_comment]"]').serialize();
        var status_to_change = status_id;

        $.ajax({
            type: "POST",
            url: "{{url("/contracts/orders/save/{$order->id}")}}",
            async: false,
            data: data + '&' +message+ '&order[0][status_to_change]=' +status_to_change+ '&order[0][release_order]=1',

            success: function (response) {
                flashHeaderMessage('Успешно сохранено!', 'success');
                reload();
            }
        });
    }


    function RunForestRun(status_id){

        var data = $("#main_container").find("select, textarea, input").serialize();
        var message = $('[name="order[0][workarea_comment]"]').serialize();
        var status_to_change = status_id;

        $.ajax({
            type: "POST",
            url: "{{url("/contracts/orders/save/{$order->id}")}}",
            async: false,
            data: data + '&' +message+ '&order[0][status_to_change]=' +status_to_change+ '&order[0][courier_going]=1',

            success: function (response) {
                flashHeaderMessage('Успешно сохранено!', 'success');
                reload();
            }
        });
    }

    function createContracts(id){

        var data = $("#main_container").find("select, textarea, input").serialize();
        var message = $('[name="order[0][workarea_comment]"]').serialize();
        var status_to_change = $('[name="order[0][status_to_change]"]').serialize();

        $.ajax({
            type: "POST",
            url: "{{url("/contracts/orders/create_contracts/{$order->id}")}}",
            async: false,
            data: data + '&' +message+ '&' +status_to_change,

            success: function (response) {
                flashHeaderMessage('Договоры успешно созданы!', 'success');
                reload();
            }
        });
    }

    $( ".datepicker" ).datepicker({ dateFormat: 'dd.mm.yy' });


    function formatTime() {
        var configuration = {
            timepicker: true,
            datepicker: false,
            format: 'H:i',
            scrollInput: false
        };
        $.datetimepicker.setLocale('ru');
        $('input.format-time').datetimepicker(configuration).keyup(function (event) {
            if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 38 && event.keyCode != 40) {
                var pattern = new RegExp("[0-9:]{5}");
                if (pattern.test($(this).val())) {
                    $(this).datetimepicker('hide');
                    $(this).datetimepicker('show');
                }
            }
        });
        $('input.format-time').each(function () {
            var im = new Inputmask("99:99", {
                "oncomplete": function () {
                }
            });
            im.mask($(this));
        });
    }

    function init()
    {

        formatTime();

        $('#is_delivery').change( function () {
            if (this.checked) {
                $('.is_delivery').show();
            } else {
                $('.is_delivery').hide();
            }
        });

        $('#insurer_type_0').change(function () {

            if ($("#insurer_type_owner").is(':checked')) {
                return;
            }


            if (parseInt($(this).val()) == 0) {
                $('.insurer_fl').show();
                $('.insurer_ul').hide();
                $('.insurer_ip').hide();
            } else if (parseInt($(this).val()) == 1) {
                $('.insurer_fl').hide();
                $('.insurer_ul').show();
                $('.insurer_ip').hide();
            } else if (parseInt($(this).val()) == 2) {
                $('.insurer_fl').hide();
                $('.insurer_ul').hide();
                $('.insurer_ip').show();
            }


        });


        $("#insurer_type_owner").change(function () {
            if (this.checked) {
                $('.insurer_fl').hide();
                $('.insurer_ul').hide();
                $('.insurer_ip').hide();
            } else {
                $('#insurer_type_0').trigger('change');
            }
        });


        $('#owner_type_0').change(function () {
            if (parseInt($(this).val()) == 0) {
                $('.owner_fl').show();
                $('.owner_ul').hide();
                $('.owner_ip').hide();
            } else if (parseInt($(this).val()) == 1) {
                $('.owner_fl').hide();
                $('.owner_ul').show();
                $('.owner_ip').hide();
            } else if (parseInt($(this).val()) == 2) {
                $('.owner_fl').hide();
                $('.owner_ul').hide();
                $('.owner_ip').show();
            }

        });

        $('#driver_type_0').change(function () {
            if (parseInt($(this).val()) == 0) {
                $('.driver_limited').show();
                $('.driver_count').show();
                $('.driver_unlimited').hide();
                $('.driver').show();

            } else if (parseInt($(this).val()) == 1) {
                $('.driver_limited').hide();
                $('.driver_count').hide();
                $('.driver_unlimited').show();
                $('.driver').hide();
            }

        });


        $('#insurer_type_0').change();

        $('#owner_type_0').change();
        $('#driver_type_0').change();
        $('#driver_count_0').change();
        /*$('#object_insurer_ts_mark_id_0').change();*/

        $('#insurer_fio_' + 0).suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "NAME",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#insurer_title_' + key).val($(this).val());

            }
        });


        $('#delivery_address').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#address_kladr').val(suggestion.data.kladr_id);
                $('#address_width').val(suggestion.data.geo_lat);
                $('#address_longitude').val(suggestion.data.geo_lon);
            }
        });


        var im = new Inputmask("9999", {"yearrange": {minyear: 1900, maxyear: 2099}});
        im.mask($('#carYear'));

        // initManyDocuments();
    }


    function getModelsObjectInsurer(KEY, select_model_id)
    {

        $.getJSON('{{url("/contracts/actions/get_models")}}', {categoryId: $('#object_insurer_ts_category_' + KEY).val(), markId: $('#object_insurer_ts_mark_id_' + KEY).select2('val')}, function (response) {

            var options = "<option value='0'>Не выбрано</option>";
            response.map(function (item) {
                options += "<option value='" + item.id + "'>" + item.title + "</option>";
            });
            $('#object_insurer_ts_model_id_' + KEY).html(options).select2('val', select_model_id);


        });

        var current = parseInt($('#object_insurer_ts_category_' + KEY).val());

        if (current == 3) {
            $('#object_insurer_weight_' + KEY).show();
            $('#object_insurer_capacity_' + KEY).show();
            $('#object_insurer_passengers_count_' + KEY).hide();
        } else if (current == 4) {
            $('#object_insurer_weight_' + KEY).hide();
            $('#object_insurer_capacity_' + KEY).hide();
            $('#object_insurer_passengers_count_' + KEY).show();
        } else {
            $('#object_insurer_weight_' + KEY).hide();
            $('#object_insurer_capacity_' + KEY).hide();
            $('#object_insurer_passengers_count_' + KEY).hide();
        }
    }

    function join(arr /*, separator */) {
        var separator = arguments.length > 1 ? arguments[1] : ", ";
        return arr.filter(function (n) {
            return n
        }).join(separator);
    }

    function formatCity(suggestion) {
        var address = suggestion.data;
        if (address.city_with_type === address.region_with_type) {
            return address.settlement_with_type || "";
        } else {
            return join([
                address.city_with_type,
                address.settlement_with_type]);
        }
    }




    document.addEventListener("DOMContentLoaded", function (event) {
        init();
    });

    function changeStatus(status_id) {
        var data = $("#main_container").find("select, textarea, input").serialize();
        var message = $('[name="order[0][workarea_comment]"]').serialize();
        var status_to_change = $('#status_to_change').serialize();

        $.ajax({
            type: "POST",
            url: "{{url("/contracts/orders/save/{$order->id}")}}",
            async: false,
            data: data + '&' +message+ '&' +status_to_change,

            success: function (response) {
                flashHeaderMessage('Успешно сохранено!', 'success');
                reload();
            }
        });
    }

    function changeStatusDown(status_id) {
        var data = $("#main_container").find("select, textarea, input").serialize();
        var message = $('[name="order[0][workarea_comment]"]').serialize();
        var status_to_change = $('#status_back').serialize();

        $.ajax({
            type: "POST",
            url: "{{url("/contracts/orders/save/{$order->id}")}}",
            async: false,
            data: data + '&' +message+ '&' +status_to_change,

            success: function (response) {
                flashHeaderMessage('Успешно сохранено!', 'success');
                reload();
            }
        });
    }

    function createCart()
    {
        var user_id_from = $("#user_id_from").val();
        var user_id_to = '{{ auth()->id() }}';
        var tp_change_selected = $("#tp_change_selected").is(':checked') ? 1 : 0;
        var sk_id_to = $("#sk_id_to").val();
        var bso_cart_type = 1;
        var bso_state_id = $("#bso_state_id").val();
        var tp_id = '{{ auth()->user()->point_sale_id }}';
        var tp_new_id = $("#tp_new").val();
        var tp_bso_manager_id = $("#tp_bso_manager").val();
        var courier_id = $(".couriers").val();
        switch (bso_cart_type) {
            case '1':
                // Передача со склада агенту
                if (user_id_to == 0) {
                    alert('Укажите агента-получателя');
                    return false;
                }
                break;
            case '6':
                // Передача БСО курьеру
                if (user_id_to == 0) {
                    alert('Укажите агента-получателя');
                    return false;
                }
                break;
            case '2':
                // Передача от агента-агенту
                if (user_id_from == 0) {
                    alert('Укажите агента-отправителя');
                    return false;
                }
                if (user_id_to == 0) {
                    alert('Укажите агента-получателя');
                    return false;
                }
                break;
            case '3':
                // Прием БСО от агента
                if (user_id_from == 0) {
                    alert('Укажите агента-отправителя');
                    return false;
                }
                break;
        }
        var bso_cart_id = myGetAjax('/bso/transfer/create_bso_cart/?user_id_from=' + user_id_from + '&user_id_to=' + user_id_to + '&sk_id_to=' + sk_id_to + '&bso_cart_type=' + bso_cart_type + '&bso_state_id=' + bso_state_id + '&tp_id=' + tp_id+ '&tp_new_id=' + tp_new_id + '&tp_bso_manager_id=' + tp_bso_manager_id + '&courier_id=' + courier_id + '&tp_change_selected=' + tp_change_selected);

        // if(parseInt(bso_cart_id)0) window.location = '/bso/transfer/?bso_cart_id=' + bso_cart_id;
        return bso_cart_id;
    }
</script>