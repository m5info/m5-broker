@extends('layouts.app')

@section('content')

{{--@section('work-area')--}}
{{--    @include("contracts.orders.order.workarea", ['order', $order])--}}
{{--@append--}}

    <div class="page-heading">
        <div class="col-lg-6">
            <h2>Заявка на оформление №{{ $order->id }} - {{ \App\Models\Contracts\Orders::STATUSES[$order->status_id] }}</h2>
        </div>
        @if(auth()->user()->hasPermission('orders', 'order_statuses'))
            <div class="col-lg-2">
                @if($order->status_id == 4)
                    <span class="btn btn-success" onclick="releaseOrder({{$order->status_id}});">
                        Завершить заявку
                    </span>
                @endif
            </div>
            <div class="col-lg-2">
                <div class="work-area-item">
                    {{ Form::select('order[0][status_to_change]', collect($select_visability), $order->status_id, ['class' => 'form-control select2-ws pull-right', 'id' => 'status_to_change']) }}
                </div>
            </div>
            <div class="col-lg-2">
                <span class="btn btn-success" onclick="changeStatus({{$order->status_id}});">
                    Сохранить
                </span>
            </div>
        @else
            <div class="col-lg-2">
                @if($order->status_id == 4)
                    <span class="btn btn-success" onclick="releaseOrder({{$order->status_id}});">
                        Завершить заявку
                    </span>
                @endif
            </div>
            <div class="col-lg-4">
                @php
                    $status2down = [
                        1 => ['new_status' => 0, 'title' => 'Вернуть во временные'],
                        2 => ['new_status' => 1, 'title' => 'Вернуть в оформление'],
                        3 => ['new_status' => 2, 'title' => 'Вернуть на печать'],
                        4 => ['new_status' => 3, 'title' => 'Вернуть в доставку'],
                    ];
                    $status2up = [
                        0 => ['new_status' => 1, 'title' => 'В оформление'],
                        1 => ['new_status' => 2, 'title' => 'На печать'],
                        2 => ['new_status' => 3, 'title' => 'На доставку'],
                        3 => ['new_status' => 4, 'title' => 'В Реализованные'],
                    ];
                @endphp

                @if($order->status_id == 2 && ($auth_user->is('operator_bso') || $auth_user->is('formalizator') || $auth_user->role_id == 1))
                    @if($order->status_id < 4)
                        @if(!($order->status_id == 1 && !(auth()->user()->is('formalizator'))) || auth()->user()->role_id == 1)
                        <span class="btn btn-success btn-right" onclick="changeStatus({{$status2up[$order->status_id]['new_status']}});">
                            {{$status2up[$order->status_id]['title']}}
                        </span>
                        <input type="hidden" id="status_to_change" name="order[0][status_to_change]" value="{{$status2up[$order->status_id]['new_status']}}">
                        @endif
                    @endif

                    @if($order->status_id > 0)
                        @if(!(Auth::user()->hasPermission('role_owns', 'is_agent') && $order->status_id == 1))
                            <span class="btn btn-primary btn-right" onclick="changeStatusDown({{$status2down[$order->status_id]['new_status']}});">
                                {{$status2down[$order->status_id]['title']}}
                            </span>
                            <input type="hidden" id="status_back" name="order[0][status_to_change]" value="{{$status2down[$order->status_id]['new_status']}}">
                        @endif
                    @endif
                @endif
            </div>
        @endif
    </div>
    <div class="row form-horizontal" id="main_container">
        <input type="hidden" id="bso_cart_id" name="order[0][bso_cart_id]" value="{{ $order->bso_cart_id ? $order->bso_cart_id : '' }}">
        <div class="policy-blocks">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="col-lg-12">
                        <div class="block-main">
                            <div class="block-sub">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                                    <div class="row form-horizontal">
                                        @if(auth()->user()->hasEmployee || Auth::user()->role_id === 1)
                                            <div>
                                                <div style="float: left;padding-top: 9px;">Менеджер</div>
                                                <div class="col-lg-6" style="float: right">
                                                    {{ Form::select('order[0][manager_id]', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id'), $order->manager_id ? $order->manager_id : $order->create_user_id, ['class' => 'form-control select2']) }}
                                                </div>
                                            </div>
                                            <div>
                                                <div class="col-lg-6" style="float: left;padding-top: 9px;">Срочно</div>
                                                <div class="col-lg-6" style="float: right">
                                                    {{ Form::checkbox('order[0][urgency]', 1, $order->urgency ? 1 : 0) }}
                                                </div>
                                            </div>
                                        @else
                                            <div class="view-field">
                                                <span class="view-label">Менеджер</span>
                                                <span class="view-value">{{ \App\Models\User::find($order->manager_id ? $order->manager_id : $order->create_user_id)->name }}</span>
                                                <input type="hidden" name="order[0][manager_id]" value="{{ $order->manager_id ? $order->manager_id : $order->create_user_id }}">
                                                <input type="hidden" name="order[0][urgency]" value="{{ $order->urgency ? 1 : 0 }}">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="page-subheading">
                            <h2>
                                <div class="col-lg-8">
                                    Доставка
                                    {{ Form::checkbox('order[0][delivery][is_delivery]', 1, (isset($order->is_delivery) && $order->is_delivery == 1) ? 1 : 0,['style' => 'width:18px;height:18px;margin-left:5px;position:absolute;', 'id' => 'is_delivery']) }}
                                </div>
                                <div class="col-lg-4">
                                    <a href="/contracts/orders/edit/{{$order->id}}/act_export_delivery" type="submit" class="btn btn-info btn-left doc_export_btn">Акт сдачи в андеррайтинг</a>
                                </div>

                            </h2>
                        </div>

                        @php
                            if($order->is_delivery == 1){
                                $delivery_show = '';
                            }else{
                                $delivery_show = 'style=display:none';
                            }
                        @endphp
                        <div class="is_delivery" {{ $delivery_show }}>

                            <div class="block-main">
                                <div class="block-sub">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                                        <div class="row form-horizontal">
                                            <div>
                                                <div class="col-lg-6">
                                                    <div class="field form-col">
                                                        <label class="control-label">Дата</label>
                                                        @php
                                                            $date = setDateTimeFormatRu($order->delivery_date, 1);
                                                            $time = $order->delivery_time;
                                                        @endphp
                                                        {{ Form::text('order[0][delivery][date]', $date, ['class' => 'form-control format-date datepicker date']) }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="field form-col">
                                                        <label class="control-label">Время</label>
                                                        {{ Form::text('order[0][delivery][time]', ($time) ? $time : "12:00", ['class' => 'form-control format-time']) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <label class="control-label">Город</label>
                                                {{ Form::select('order[0][delivery][city_id]', \App\Models\Settings\City::where('is_actual', '=', 1)->get()->pluck('title', 'id'), $order->delivery_city_id, ['class' => 'form-control select2']) }}
                                            </div>
                                            <div class="col-lg-12">
                                                <label class="control-label">Метро</label>
                                                {{ Form::text('order[0][delivery][metro]', $order->delivery_metro, ['class' => 'form-control', 'id' => 'delivery_metro']) }}
                                            </div>
                                            <div class="col-lg-12">
                                                <label class="control-label">Адрес</label>
                                                {{ Form::text('order[0][delivery][address]', $order->address, ['class' => 'form-control', 'id' => 'delivery_address']) }}
                                                {{ Form::hidden('order[0][delivery][address_kladr]', $order->address_kladr, ['id' => 'address_kladr']) }}
                                                {{ Form::hidden('order[0][delivery][address_width]', $order->address_width, ['id' => 'address_width']) }}
                                                {{ Form::hidden('order[0][delivery][address_longitude]', $order->address_longitude, ['id' => 'address_longitude']) }}
                                            </div>
                                            @if($order->status_id >= 3)
                                                <div class="col-lg-12">
                                                    <div class="field form-col">
                                                        <label class="control-label">Курьер</label>
                                                        @include('partials.elements.users', ['title'=>'order[0][delivery][courier]', 'select'=> $order->delivery_user_id])
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-lg-12">
                                                <label class="control-label">Комментарий к доствке</label>
                                                {{ Form::textarea('order[0][delivery][comment]', $order->delivery_comment, ['class' => 'form-control']) }}
                                            </div>
                                            @if($order->delivery_user_id && $order->status_id >= 3)
                                                <div class="col-lg-12">
                                                    <span class="btn btn-success btn-right" onclick="RunForestRun({{$order->status_id}});">
                                                        Выезд курьера
                                                    </span>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="block-view">
                        <h3>Сообщения</h3>
                        <div class="clear"></div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div>
                                    @if(isset($order->comments))
                                        @foreach($order->comments as $comment)
                                            <div class="col-lg-12" style="padding: 14px 20px;margin-bottom: 18px;background-color: #f5f5f5;border-radius: 2px;">
                                                @php
                                                     $author = \App\Models\User::find($comment->proccesing_user_id)->name;
                                                @endphp
                                                {{ $author }} ({{$comment->created_at}}):<br><br>{!! $comment->comment !!}
                                            </div>
                                        @endforeach
                                    @endif
                                    {{ Form::textarea('order[0][workarea_comment]', '', ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(!(auth()->user()->hasPermission('orders', 'order_statuses')))
                    <div class="col-lg-6">
                        <span class="btn btn-success btn-right" onclick="saveOrder({{$order->status_id}});">
                            Сохранить
                        </span>
                    </div>
                @endif
            </div>

            @include("contracts.orders.order.partials.contracts_table", ['temp' => 'edit'])

            @include("contracts.orders.order.partials.reserve_list", ['temp' => 'edit'])
    </div>
</div>
@endsection



@section('js')

    @include('contracts.orders.order.js_inside')

@endsection