<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">
    <div class="col-lg-12">
        @if($temp != 'view')
            @if($order->status_id != 4)
                <span class="btn btn-success pull-right" onclick="openFancyBoxFrame('{{url("/contracts/orders/create_contracts/{$order->id}")}}');">
                    Создать договор
                </span>
            @endif
        @endif
        @if(count($order->contracts))
            <div class="page-subheading">
                <h2>Созданные договора</h2>
            </div>
            <div class="block-main">
                <div class="block-sub">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">
                            <table class="tov-table">
                                <tr>
                                    <th>ID</th>
                                    <th>Номер договора</th>
                                    <th>Продукт</th>
                                    <th>Страхователь</th>
                                    <th>Основной</th>
                                </tr>
                                @foreach($order->contracts as $contract)
                                    @php
                                        $link = "class=clickable-row data-href=" .url("/contracts/orders/created_contracts/edit/$contract->id");
                                    @endphp
                                    <tr>
                                        <td {{$link}}>{{ $contract->id }}</td>
                                        <td {{$link}}>{{ ($contract->bso)?$contract->bso->bso_title:'' }}</td>
                                        <td {{$link}}>{{ $contract->product->title }}</td>
                                        <td {{$link}}>{{ $contract->insurer->title }}</td>
                                        <td>{{ Form::radio('order[0][main_contract_id]', $contract->id, $order->main_contract_id == $contract->id) }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>