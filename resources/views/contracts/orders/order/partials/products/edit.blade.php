@foreach($available_products as $key => $available_product)
    <div class="col-lg-12">
        <div class="col-lg-3">
            <label  class="control-label" style="max-width:100%;">{{ $available_product['title'] }}</label>
            @php
                $available_id = $available_product['id'];
                $isset_product = (isset($products->$available_id->isset) && $products->$available_id->isset == 1) ? 1 : 0;
            @endphp
            {{ Form::checkbox("order[0][{$available_id}][isset]", 1, $isset_product,['style' => 'width:18px;height:18px;margin-left:5px;position:absolute;']) }}
        </div>
        <div class="col-lg-9">
            <div class="field form-col">
                <div class="col-lg-4">
                    <label class="control-label">Скидка оф. %</label>
                    {{ Form::text("order[0][{$available_id}][official_kv]", isset($products->{$available_id}) ? $products->{$available_id}->official_kv : '', ['class' => 'form-control']) }}
                </div>
                <div class="col-lg-4">
                    <label class="control-label">Скидка неоф. %</label>
                    {{ Form::text("order[0][{$available_id}][unofficial_kv]", isset($products->{$available_id}) ? $products->{$available_id}->unofficial_kv : '', ['class' => 'form-control']) }}
                </div>
                <div class="col-lg-4">
                    <label class="control-label">Желаемая сумма</label>
                    {{ Form::text("order[0][{$available_id}][total_amount]", isset($products->{$available_id}) ? $products->{$available_id}->total_amount : '', ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
    </div>

    <div class="clear"></div>
@endforeach