@foreach($available_products as $key => $available_product)
    @php
        $available_id = $available_product['id'];
        $isset_product = (isset($products->$available_id->isset) && $products->$available_id->isset == 1) ? 1 : 0;
    @endphp
    @if($isset_product == 1)
        <div class="col-lg-12">
            <div class="col-lg-3">
                 {{ $available_product['title'] }}
                 {{ Form::hidden("order[0][{$available_id}][isset]", $isset_product,['style' => 'width:18px;height:18px;margin-left:5px;position:absolute;']) }}
            </div>
            <div class="col-lg-9">
                <div class="field form-col">
                    <div class="col-lg-4">
                        {{ Form::hidden("order[0][{$available_id}][official_kv]", isset($products->{$available_id}) ? $products->{$available_id}->official_kv : '', ['class' => 'form-control']) }}
                        <div class="col-lg-6">Скидка оф. %</div>
                        <div class="col-lg-6">{{ isset($products->{$available_id}) ? $products->{$available_id}->official_kv : '0' }}</div>
                    </div>
                    <div class="col-lg-4">
                        {{ Form::hidden("order[0][{$available_id}][unofficial_kv]", isset($products->{$available_id}) ? $products->{$available_id}->unofficial_kv : '', ['class' => 'form-control']) }}
                        <div class="col-lg-6">Скидка неоф. %</div>
                        <div class="col-lg-6">{{ isset($products->{$available_id}) ? $products->{$available_id}->unofficial_kv : '0' }}</div>
                    </div>
                    <div class="col-lg-4">
                        {{ Form::hidden("order[0][{$available_id}][total_amount]", isset($products->{$available_id}) ? $products->{$available_id}->total_amount : '', ['class' => 'form-control']) }}
                        <div class="col-lg-6">Желаемая сумма</div>
                        <div class="col-lg-6">{{ isset($products->{$available_id}) ? $products->{$available_id}->total_amount : '0' }}</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clear"></div>
    @endif
@endforeach