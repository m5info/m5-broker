<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">
    <div class="col-lg-12">
        @if(auth()->user()->is('formalizator') && $temp != 'view')
            @if($order->status_id != 4)
                <span class="btn btn-success pull-right" id="cart_create">
                    Зарезервировать
                </span>
            @endif
        @endif
        @if($order->bso_cart_id)
            <div class="page-subheading">
                <h2>Резервированные</h2>
            </div>
            <div class="block-main" style="margin-bottom: 100px;">
                <div class="block-sub">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row form-horizontal">
                            @if(auth()->user()->is('formalizator'))
                                <input type="hidden" name="order[0][formalizator_id]" id="formalizator_id" value="{{auth()->id()}}">
                            @endif
                            @php
                                $formalizator = $order->formalizator_id ? \App\Models\User::find($order->formalizator_id) : false;
                            @endphp
                            {{--                                        <input type="hidden" name="" id="sk_user_id" value="">--}}
                            {{--                                        <input type="hidden" name="" id="bso_state_id" value="">--}}
                            <input type="hidden" name="user_id_from" id="user_id_from" value="0">
                            <input type="hidden" name="bso_cart_type" id="bso_cart_type" value="1">
                            {{ Form::select('tp', collect([auth()->user()->point_sale_id => '']), auth()->user()->point_sale_id, ['style' => 'display: none', 'class' => 'tp']) }}

                            @include("bso.transfer.edit_carts", ['bso_cart' => \App\Models\BSO\BsoCarts::find($order->bso_cart_id), 'im' => auth()->user()])
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>