<table class="tov-table table  table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>Страхователь</th>
        <th>Менеджер/Агент</th>
        <th>Курьер</th>
        <th>Дата доставки</th>
        <th>Действие</th>
    </tr>
    </thead>
    <tbody>
    @if(sizeof($orders))
        @foreach($orders as $order)
            <tr {{$order->urgency ? 'class=bg-red' : ''}}>
                <td>{{$order->id}}</td>
                <td>{{$order->main_contract_id > 0 ? $order->main_contract->insurer->title : ""}}</td>
                @php
                    $manager = \App\Models\User::find($order->manager_id ? $order->manager_id : "");
                @endphp
                <td>{{ ($manager)? "$manager->name $manager->email" : '' }}</td>
                <td>{{ $order->courier ? $order->courier->name : "" }}</td>
                <td>{{ setDateTimeFormatRu($order->delivery_date, 1) }}</td>
                <td>
                    @if($tabs_visibility[$order->status_id]['edit'])
                        @if($order->status_id == 0)
                            <a class="btn btn-danger btn-right" href="{{url("contracts/orders/edit/{$order->id}/delete")}}">
                                Удалить
                            </a>
                        @endif
                        <a class="btn btn-success btn-left" href="{{url("contracts/orders/edit/{$order->id}/")}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    @elseif($tabs_visibility[$order->status_id]['view'])
                        <a class="btn btn-success" href="{{url("contracts/orders/edit/{$order->id}/")}}">
                            <i class="fa fa-eye"></i>
                        </a>
                    @endif
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="13" class="text-center">Нет записей</td>
        </tr>
    @endif
    </tbody>
</table>