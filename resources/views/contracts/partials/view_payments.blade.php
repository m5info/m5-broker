
<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <br/>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="view-field">
        <span class="view-label">Взнос №</span>
        <span class="view-value">{{$payment->payment_number}}</span>                                                                                                            
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="view-field">
        <span class="view-label">Тип</span>
        <span class="view-value">{{collect(\App\Models\Contracts\Payments::TRANSACTION_TYPE)[$payment->type_id]}}</span>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="view-field">
        <span class="view-label">Сумма платежа</span>
        <span class="view-value">{{($payment->payment_total>0.00)?titleFloatFormat($payment->payment_total):''}}</span>
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="view-field">
        <span class="view-label">Дата оплаты</span>
        <span class="view-value">{{setDateTimeFormatRu($payment->payment_data, 1)}}</span>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="view-field">
        <span class="view-label">Офици. %</span>
        <span class="view-value">
            @if ($payment->official_discount != 0.00)
            {{titleFloatFormat($payment->official_discount)}}
            @endif
        </span>                                                                                                            
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="view-field">
        <span class="view-label">Неофиц. %</span>
        <span class="view-value">
            @if ($payment->informal_discount != 0.00)
            {{titleFloatFormat($payment->informal_discount)}}
            @endif
        </span>                                                                                                            
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="view-field">
        <span class="view-label">Банк %</span>
        <span class="view-value">
            @if ($payment->bank_kv != 0.00)
            {{titleFloatFormat($payment->bank_kv)}}
            @endif
        </span>                                                                                                            
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="view-field">
        <span class="view-label">Тип оплаты</span>
        <span class="view-value">
            {{ collect(\App\Models\Contracts\Payments::PAYMENT_TYPE)[$payment->payment_type]}}
        </span>                                                                                                            
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="view-field">
        <span class="view-label">Поток оплаты</span>
        <span class="view-value">
            {{ collect(\App\Models\Contracts\Payments::PAYMENT_FLOW)[$payment->payment_flow]}}
        </span>                                                                                                            
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="view-field">
        <span class="view-label">Квитанция</span>
        <span class="view-value">
            {{$payment->bso_receipt}}
        </span>                                                                                                            
    </div>
</div>
