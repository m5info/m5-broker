<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >

    @if(isset($hold_kv_product) && sizeof($hold_kv_product->documents))

    @foreach($hold_kv_product->documents as $key => $document)

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">

        <div class="row">
            <span class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="color: red;padding-top:5px;">{{$document->file_title}} <span class="required">*</span></span>

            @if($contract->document($document->id))

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="upload-dot">
                    <div class="block-image">
                        @if (in_array($contract->document($document->id)->file->ext, ['jpg', 'jpeg', 'png', 'gif']))                       
                        <a href="{{ url($contract->document($document->id)->file->url) }}" target="_blank">
                            <img class="media-object preview-image" src="{{ url($contract->document($document->id)->file->preview) }}" onerror="this.onerror=null;this.src='/images/extensions/unknown.png';">
                        </a>
                        @else
                        <a href="{{ url($contract->document($document->id)->file->url) }}" target="_blank">
                            <img class="media-object preview-icon" src="/images/extensions/{{mb_strtolower($contract->document($document->id)->file->ext)}}.png">
                        </a>
                        @endif
                        <div class="upload-close">
                            <div class="" style="float:right;color:red;">
                                <a href="javascript:void(0);" onclick="removeDocument('{{ $contract->document($document->id)->file->name }}','{{$document->id}}')">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            @else
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h3>Документ отсутствует</h3>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
                {!! Form::open(['url'=>url("/contracts/actions/contract/{$contract->id}/document/{$document->id}"),'method' => 'post', 'class' => 'dropzone_', 'id' => 'addManyDocForm']) !!}
                <div class="dz-message" data-dz-message>
                    <p>Перетащите сюда файл</p>
                    <p class="dz-link">или выберите с диска</p>
                </div>
                {!! Form::close() !!}
            </div>

            @endif
        </div>
    </div>


    @endforeach

    @endif

    <script>
        function removeDocument(fileName, documentId) {
            newCustomConfirm(function (result) {
                if (result){
                    var filesUrl = '{{ url("/contracts/actions/contract/{$contract->id}/document/") }}';
                    var fileUrl = filesUrl + '/' + documentId;
                    $.post(fileUrl, {
                        _method: 'DELETE'
                    }, function () {
                        reload();
                    });
                }
            });
        }
    </script>

</div>