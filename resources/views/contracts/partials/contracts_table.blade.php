
<table class="tov-table table  table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>Наименование полиса</th>
        <th class="nowrap">Дата страхования c</th>
        <th class="nowrap">Дата страхования по</th>
        <th>Год выпуска</th>
        <th>СК</th>
        <th>РИСК</th>
        <th>Страхователь</th>
        <th>Страховая премия</th>
        <th>Телефон</th>
        <th>Доп. телефон</th>
        <th>Адрес доставки</th>
        <th>Метро</th>
        <th>Примечание</th>
        <th>Менеджер</th>
        <th>Агент / Курьер</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @if(sizeof($contracts))
        @foreach($contracts as $contract)
            @php
                $fixed_status = false;
                $last_contract = $contract->logs->last();
                $temp_contracts = request()->route()->getPrefix() == 'contracts/temp_contracts';
                if($temp_contracts && $last_contract){
                    $fixed_status = $last_contract->status_id == 2 && $contract->statys_id == 1;
                }
            @endphp
            <tr>
                <td class="{{ $fixed_status ? 'bg-yellow' : '' }}">{{$contract->id}}</td>
                <td>
                    @if($contract->bso_id)
                        <a href="{{ url("bso/items/".$contract->bso_id) }}">Полис {{$contract->bso && $contract->bso->product ? $contract->bso->product->title : ""}} {{$contract->bso ? $contract->bso->bso_title : ""}}
                        </a>
                    @else
                        Полис {{$contract->bso && $contract->bso->product ? $contract->bso->product->title : ""}} {{$contract->bso ? $contract->bso->bso_title : ""}}
                    @endif
                </td>
                <td>{{setDateTimeFormatRu($contract->begin_date, 1)}}</td>
                <td>{{setDateTimeFormatRu($contract->end_date, 1)}}</td>
                <td>{{$contract->object_insurer_auto ? $contract->object_insurer_auto->car_year : ""}}</td>
                <td>{{$contract->insurance_companies ? $contract->insurance_companies->title : ""}}</td>
                <td>
                    @if($contract->bso)
                        {{$contract->bso && $contract->bso->product ? $contract->bso->product->title : ""}}
                    @else
                        {{$contract->product ? $contract->product->title : ""}}
                    @endif


                </td>
                <td>{{($contract->insurer)?$contract->insurer->title:''}}</td>
                <td>{{$contract->payment_total}}</td>
                <td>{{($contract->insurer)?$contract->insurer->phone:''}}</td>
                <td>{{($contract->insurer)?$contract->insurer->add_phone:''}}</td>
                <td>{{($contract->insurer)?$contract->insurer->delivery_address:''}}</td>
                <td>{{($contract->insurer)?$contract->insurer->underground:''}}</td>
                <td><span style="color: red;">{{$contract->errors->last() ? $contract->errors->last()->message : ""}}</span></td>
                @if($contract->manager)
                    <td>{{$contract->manager->name}}</td>
                @else
                    <td>{{$contract->agent ? $contract->agent->name : ''}}</td>
                @endif
                <td>{{$contract->courier() }}</td>
                <td>
                    @if($contract->is_online == 1)
                        <a target="_blank" class="btn btn-success" href="{{url("contracts/online/edit/{$contract->id}/")}}">
                            <i class="fa fa-eye"></i>
                        </a>
                    @else
                    @if($contract->statys_id == 4)
                        <a target="_blank" class="btn btn-success" href="{{url("/contracts/prolongation/{$contract->id}/")}}">
                            <i class="fa fa-eye"></i>
                        </a>
                    @else
                        <a target="_blank" class="btn btn-success" href="{{url("/contracts/temp_contracts/contract/{$contract->id}/edit")}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    @endif
                    @endif
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="17" class="text-center">Нет записей</td>
        </tr>
    @endif

    </tbody>
</table>
