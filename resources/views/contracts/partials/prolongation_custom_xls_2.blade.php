<table class="tov-table">
    <thead>
    <tr>
        {{--<th>#</th>--}}
        <th>Дата начала</th>
        <th>Дата окончания</th>
        <th>ФИО</th>
        <th>Риск</th>
        <th>№ Полиса</th>
        <th>Страховая компания</th>
        <th>А/М</th>
        <th>Год выпуска</th>
        <th>Банк (название банка, если машина кредитная)</th>
        <th>C/С по А/М</th>
        <th>Мощность</th>
        <th>Алгоритм рассрочки</th>
        <th>Премия по полису, руб</th>
        <th>Страховая сумма</th>
        <th>Контакты</th>
        <th>Доп. телефон</th>
        <th>Адрес доставки</th>
        <th>Метро</th>
        <th>Менеджер прошлый год</th>
        <th>Эксперт</th>
        <th>Примечание / номер направления с консалтинга</th>
    </tr>
    </thead>
    <tbody>
    @if(sizeof($contracts))
        @foreach($contracts as $contract)
            @php

                $fixed_status = false;
                $last_contract = $contract->logs->last();
                $temp_contracts = request()->route()->getPrefix() == 'contracts/temp_contracts';
                if($temp_contracts && $last_contract){
                    $fixed_status = $last_contract->status_id == 2 && $contract->statys_id == 1;
                }
            @endphp
            <tr>
                {{--<td>{{$contract->id}}</td>--}}
                <td>{{setDateTimeFormatRu($contract->begin_date, 1)}}</td>
                <td>{{setDateTimeFormatRu($contract->end_date, 1)}}</td>
                <td>{{($contract->insurer)?$contract->insurer->title:''}}</td>
                <td>{{$contract->bso && $contract->bso->product ? $contract->bso->product->title : ""}}</td>
                <td>{{$contract->bso ? $contract->bso->bso_title : ""}}</td>
                <td>{{$contract->insurance_companies ? $contract->insurance_companies->title : ""}}</td>
                <td>{{$contract->object_insurer_auto && $contract->object_insurer_auto->mark && $contract->object_insurer_auto->mark->title ? $contract->object_insurer_auto->mark->title : '' }} {{$contract->object_insurer_auto && $contract->object_insurer_auto->model && $contract->object_insurer_auto->model->title ? $contract->object_insurer_auto->model->title: ''}}</td>
                <td>{{$contract->object_insurer_auto && $contract->object_insurer_auto->car_year ? $contract->object_insurer_auto->car_year : ''}}</td>
                <td>{{$contract->bank && $contract->bank->title ? $contract->bank->title : ''}}</td>
                <td>{{$contract->insurance_amount ? getPriceFormat($contract->insurance_amount) : 'Не указана' }}</td>
                <td>{{ $contract->object_insurer_auto && $contract->object_insurer_auto->power ? $contract->object_insurer_auto->power : '' }}</td>
                <td>{{ $contract->installment_algorithm && $contract->installment_algorithm->algorithm && $contract->installment_algorithm->algorithm->title ? $contract->installment_algorithm->algorithm->title : '' }}</td>
                <td>{{$contract->payment_total ? getPriceFormat($contract->payment_total) : ''}}</td>
                <td>{{$contract->insurance_amount ? getPriceFormat($contract->insurance_amount) : 'Не указана' }}</td>
                <td>{{$contract->insurer && $contract->insurer->phone ? $contract->insurer->phone : 'Не указан'}}</td>
                <td>{{$contract->insurer && $contract->insurer->add_phone ? $contract->insurer->add_phone : 'Не указан'}}</td>
                <td>{{$contract->insurer && $contract->insurer->delivery_address ? $contract->insurer->delivery_address : 'Не указан'}}</td>
                <td>{{$contract->insurer && $contract->insurer->underground ? $contract->insurer->underground : 'Не указано'}}</td>
                <td>
                    @if($contract->sales_condition == 0)
                        {{$contract->agent && $contract->agent->name ? $contract->agent->name : ''}}
                    @elseif($contract->sales_condition == 1)
                        {{ $contract->manager && $contract->manager->name ? $contract->manager->name : ''}}
                    @endif
                </td>
                <td>{{$contract->bso && $contract->bso->agent && $contract->bso->agent->name ? $contract->bso->agent->name : ''}}</td>
                <td>{{$contract->order_title ? $contract->order_title : ''}}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="18" class="text-center">Нет записей</td>
        </tr>
    @endif

    </tbody>
</table>
