
<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <br/>
    <input type="hidden" name="contract[0][payment][{{$key}}][id]" value="{{$payment->id}}"/>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label class="control-label">Взнос №</label>
        {{ Form::text("contract[0][payment][$key][payment_number]", $payment->payment_number, ['class' => 'form-control valid_fast_accept', 'id'=>"payment_number_$key"]) }}
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label class="control-label">Тип</label>
        {{ Form::select("contract[0][payment][$key][type_id]", collect(\App\Models\Contracts\Payments::TRANSACTION_TYPE), $payment->type_id, ['class' => 'form-control select2-ws']) }}
    </div>
</div>

<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
        <label class="control-label">Сумма платежа</label>
        {{ Form::text("contract[0][payment][$key][payment_total]", ($payment->payment_total>0.00) ? titleFloatFormat($payment->payment_total):'', ['class' => 'form-control sum valid_fast_accept', 'id'=>"payment_total_$key"]) }}
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
        <label class="control-label">Дата оплаты</label>
        {{ Form::text("contract[0][payment][$key][payment_data]", setDateTimeFormatRu($payment->payment_data, 1), ['class' => 'form-control datepicker valid_fast_accept date', 'id'=>"payment_data_$key"]) }}
    </div>
</div>

<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
        <label class="control-label">Офици. %</label>
        {{ Form::text("contract[0][payment][$key][official_discount]", ($payment->official_discount!=0.00)?titleFloatFormat($payment->official_discount):'', ['class' => 'form-control sum', 'id'=>"official_discount_$key"]) }}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
        <label class="control-label">Неофиц. %</label>
        {{ Form::text("contract[0][payment][$key][informal_discount]", ($payment->informal_discount!=0.00)?titleFloatFormat($payment->informal_discount):'', ['class' => 'form-control sum', 'id'=>"informal_discount_$key"]) }}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
        <label class="control-label">Банк %</label>
        {{ Form::text("contract[0][payment][$key][bank_kv]", ($payment->bank_kv!=0.00)?titleFloatFormat($payment->bank_kv):'', ['class' => 'form-control sum', 'id'=>"bank_kv_$key"]) }}
    </div>
</div>
<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
        <label class="control-label">Тип оплаты</label>
        {{ Form::select("contract[0][payment][$key][payment_type]", collect(\App\Models\Contracts\Payments::PAYMENT_TYPE), $payment->payment_type, ['class' => 'form-control select2-ws']) }}
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
        <label class="control-label">Поток оплаты</label>
        {{ Form::select("contract[0][payment][$key][payment_flow]", collect(\App\Models\Contracts\Payments::PAYMENT_FLOW), $payment->payment_flow, ['class' => 'form-control select2-ws']) }}
    </div>
</div>

<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label class="control-label pull-left" style="margin-top: 5px;">
            Квитанция
            <a href="javascript:void(0);" class="btn-xs btn-primary" data-create_receipt="{{$key}}" style="display: inline">Е-квит</a>
        </label>

        {{ Form::text( "contract[0][payment][{$key}][bso_receipt]", $payment->bso_receipt, ['class' => 'form-control valid_accept class_bso_receipt', 'id'=>"bso_receipt_{$key}", ($payment->bso_not_receipt == 1) ? 'disabled' : '']) }}

        <input type="hidden" name="contract[0][payment][{{$key}}][bso_receipt_id]" id="bso_receipt_id_{{$key}}" value="{{$payment->bso_receipt_id}}"/>
    </div>
</div>

<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    @if($contract->statys_id == 0)
        <span class="btn btn-danger btn-right" style="margin-top: 15px;" onclick="deleteTempPayment('{{$payment->id}}')">Удалить</span>
    @endif
</div>