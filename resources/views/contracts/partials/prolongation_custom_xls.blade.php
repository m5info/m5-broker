<table class="tov-table">
    <thead>
    <tr>
        {{--<th>#</th>--}}
        <th>Дата начала</th>
        <th>Дата окончания</th>
        <th>ФИО</th>
        <th>Продукт</th>
        <th>№ Полиса</th>
        <th>Страховая компания</th>
        <th>Премия по полису, руб</th>
        <th>Менеджер прошлый год</th>
        <th>Руководитель</th>
    </tr>
    </thead>
    <tbody>
    @if(sizeof($contracts))
        @foreach($contracts as $contract)
            @php

                $fixed_status = false;
                $last_contract = $contract->logs->last();
                $temp_contracts = request()->route()->getPrefix() == 'contracts/temp_contracts';
                if($temp_contracts && $last_contract){
                    $fixed_status = $last_contract->status_id == 2 && $contract->statys_id == 1;
                }
            @endphp
            <tr>
                {{--<td>{{$contract->id}}</td>--}}
                <td>{{setDateTimeFormatRu($contract->begin_date, 1)}}</td>
                <td>{{setDateTimeFormatRu($contract->end_date, 1)}}</td>
                <td>{{($contract->insurer)?$contract->insurer->title:''}}</td>
                <td>{{$contract->bso && $contract->bso->product ? $contract->bso->product->title : ""}}</td>
                <td>{{$contract->bso ? $contract->bso->bso_title : ""}}</td>
                <td>{{$contract->insurance_companies ? $contract->insurance_companies->title : ""}}</td>
                <td>{{$contract->payment_total}}</td>
                <td>{{$contract->courier()}}</td>
                <td>
                @if($contract->sales_condition == 0)
                    {{$contract->agent && $contract->agent->name ? $contract->agent->name : ''}}
                @elseif($contract->sales_condition == 1)
                    {{ $contract->manager && $contract->manager->name ? $contract->manager->name : ''}}
                @endif
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="9" class="text-center">Нет записей</td>
        </tr>
    @endif

    </tbody>
</table>
