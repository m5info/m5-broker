<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">


    <span class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <span class="btn btn-success btn-left load_files_front" onclick="loadFilesFront({{$contract->id}})" style="display: @if((int)$contract->order_id > 0) block @else none @endif">
            Загрузить из фронт офиса
        </span>



        @if(Auth::user()->hasPermission('contracts', 'contract_download_all'))

            @if(auth()->user()->is('under'))
                <span class="btn btn-danger btn-right" onclick="removeCheckedFiles()">
                    Удалить выбранные
                </span>
                @if($contract->scans->count())
                    <div class="btn-group col-xs-12 col-sm-12 col-md-4 col-lg-3">
                        <label class="control-label">Выбрать все</label>
                        {{ Form::checkbox('check_all_files', 1, false, ['onchange' => 'checkAllFiles(event)']) }}
                    </div>
                @endif
            @endif
            <span class="btn btn-primary btn-right" onclick="downloadAll({{$contract->id}})">
                Скачать все
            </span>

        @endif

    </span>

    <br/><br/><br/>

    <span class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="color: red;">{{$hold_kv_product->many_text}}</span>


    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <table class="table orderStatusTable dataTable no-footer">
            <!--<thead>
            <tr>
                <th>{{ trans('users/users.edit.title') }}</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            </thead>-->
                <tbody id="document-box">
                @if($contract->scans->count())
                @foreach($contract->scans as $file)
                    <div class="col-lg-6 col-md-12" id="document-{{$file->id}}">
                        <div class="upload-dot">
                            <div class="block-image">
                                @if(auth()->user()->is('under'))
                                    {{ Form::checkbox('file_name[]', $file->id, old('file_name[]'), ['style' => 'position: absolute;top: 0px;left: 20px;', 'data-name' => $file->name]) }}
                                @endif
                                @if (in_array($file->ext, ['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG']))


                                    <div style="display: none; max-height: 550px" id="fs-galery{{$file->id}}">
                                        <a target="_blank" href="{{ url($file->url) }}">
                                        <img style="max-height: 550px;margin: 0 auto;" class="fs-galery"
                                             id="fs-galery{{$file->id}}" src="{{ url($file->url) }}"
                                             onerror="this.onerror=null;this.src='/images/extensions/unknown.png';">
                                        </a>
                                        <p class="file_name col-xs-12 mt-15 text-center">
                                            {{ $file->original_name }}
                                            <br>
                                            <a href="{{ url($file->url) }}" target="_blank">Открыть оригинал</a>
                                            <a href="{{ url($file->url) }}" download>Скачать</a>

                                            <a id="rotate_right" href="#"><img src="/images/rotate_right.png" alt=""></a>
                                            <a id="rotate_left" href="#"><img src="/images/rotate_left.png" alt=""></a>
                                        </p>
                                    </div>

                                    <a class="fancybox iframe" rel="group" href="#fs-galery{{$file->id}}" rel="group">
                                        <img class="media-object preview-image" src="{{ ($file->preview)?url($file->preview):url($file->url) }}">
                                    </a>
                                    <p class="file_name col-xs-12 mt-15">
                                        <a href="{{ url($file->url) }}" download>{{ $file->original_name }}</a>
                                    </p>

                                @elseif(in_array($file->ext, ['mp4', 'mkv', 'avi ','mov','avi','mpeg4','flv','3gpp']))

                                    <div style="display: none; max-height: 550px" id="fs-galery{{$file->id}}">
                                        <a target="_blank" href="{{ url($file->url) }}">
                                            <iframe style="width:640px; height:480px;" allowfullscreen src="{{ url($file->url) }}"></iframe>
                                        </a>
                                        <p class="file_name col-xs-12 mt-15 text-center">
                                            <a href="{{ url($file->url) }}" download>{{ $file->original_name }}</a>
                                        </p>
                                    </div>

                                    <a class="iframe" rel="group" href="#fs-galery{{$file->id}}" rel="group">
                                        <img class="media-object preview-image" src="/images/extensions/video.png">
                                    </a>
                                    <p class="file_name col-xs-12 mt-15">
                                        <a href="{{ url($file->url) }}" download>{{ $file->original_name }}</a>
                                    </p>

                                @else
                                    <div style="display: none; max-height: 550px" id="fs-galery{{$file->id}}">
                                        <a target="_blank"  href="{{ url($file->url) }}">
                                            <img style="max-height: 150px;margin: 0 auto;" class="fs-galery"
                                                 id="fs-galery{{$file->id}}" src="/images/extensions/{{mb_strtolower($file->ext)}}.png"
                                                 onerror="this.onerror=null;this.src='/images/extensions/unknown.png';">
                                        </a>
                                        <p class="file_name col-xs-12 mt-15 text-center">
                                            <a href="{{ url($file->url) }}" download>{{ $file->original_name }}</a>
                                        </p>
                                    </div>

                                    <a class="iframe" rel="group" href="#fs-galery{{$file->id}}" rel="group">
                                        <img class="media-object preview-icon"
                                             src="/images/extensions/{{mb_strtolower($file->ext)}}.png">
                                    </a>

                                    <p class="file_name col-xs-12 mt-15">
                                        <a href="{{ url($file->url) }}" download>{{ $file->original_name }}</a>
                                    </p>

                                @endif
                                <div class="upload-close">
                                    <div class="" style="float:right;color:red;">
                                        <a href="javascript:void(0);" onclick="removeFile('{{ $file->name }}')">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<tr>
                    <td>{{ $file->original_name }}</td>
                    <td>
                        <a href="{{ url($file->url) }}" class="btn btn-primary" target="_blank">
                            {{ trans('form.buttons.download') }}
                            </a>
                        </td>
                        <td>
                            <button class="btn btn-danger" type="button" onclick="removeFile('{{ $file->name }}')">
                            {{ trans('form.buttons.delete') }}
                            </button>
                        </td>
                    </tr>-->
                @endforeach

                @endif
                </tbody>
            </table>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        {!! Form::open(['url'=>"/contracts/actions/contract/$contract->id/scans",'method' => 'post', 'class' => 'dropzone_', 'id' => 'addManyDocForm']) !!}
        <div class="dz-message" data-dz-message>
            <p>Перетащите сюда файлы</p>
            <p class="dz-link">или выберите с диска</p>
        </div>
        {!! Form::close() !!}
    </div>
</div>


<script>


    function removeCheckedFiles() {
        if (!customConfirm()) {
            return false;
        }

        $('[name="file_name[]"]').each(function (key, item) {
            var elem = $(item);
            if (elem.prop('checked')){
                var fileName = elem.data('name');

                var filesUrl = '{{ url(\App\Models\File::URL) }}';
                var fileUrl = filesUrl + '/' + fileName;
                $.post(fileUrl, {
                    _method: 'DELETE'
                }, function () {

                });
            }
        });

        reload();
    }

    function checkAllFiles(event) {
        if ($(event.target).prop('checked')){
            $('[name="file_name[]"]').prop('checked', true);
        } else {
            $('[name="file_name[]"]').prop('checked', false);
        }
    }

    function downloadAll(id) {

        $.get('/contracts/actions/contract/' + id + '/download_all_scans', id, function (response) {
            // $.each(response, function (index, value) {
            //     var link = document.createElement('a');
            //
            //     link.setAttribute('href', value);
            //     link.setAttribute('download', 'download');
            //
            // });

            location.href = '/' + response;
            $.get('/contracts/actions/contract/' + id + '/delete_scans_archive', id, function (response) {

            });
        });
    }

    function loadFilesFront(id) {

        loaderShow();
        $.get('/contracts/actions/contract/' + id + '/load-files-front', {}, function (response) {

            reload();

        }).done(function () {
            loaderShow();
        }).fail(function () {
            loaderHide();
        }).always(function () {
            loaderHide();
        });


    }

    function initManyDocuments() {
        /*
         $("#addManyDocForm").dropzone({
         //Dropzone.options.addOrgDocForm = {
         paramName: 'file',
         maxFilesize: 10,
         //acceptedFiles: "image/*",
         init: function () {
         this.on("complete", function () {
         if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
         reload();
         }

         });
         }
         });
         */

    }


</script>
