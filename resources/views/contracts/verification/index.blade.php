@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">
@append

@section('content')


    <div class="page-heading">
        <h2 class="inline-h1">Проверка/Подтверждение</h2>

        <span id="export_btns">
            <span class="btn btn-success btn-right" data-export="in_check" data-export-id="2" style="display: none">Выгрузить .xlsx (Коррекция)</span>
        </span>

        <span>
            <span class="btn btn-success btn-right" id="export_act_of_disagreement">Выгрузить акт разногласий</span>
        </span>

        <span class="btn btn-primary btn-right" onclick="openFancyBoxFrame('{{url("/contracts/temp_contracts/add/paper/")}}')">
            <i class="fa fa-plus"></i> Бумажный договор
        </span>

        <span class="btn btn-success btn-right" onclick="openFancyBoxFrame('{{url("/contracts/temp_contracts/add/payment/")}}')">
            <i class="fa fa-plus-circle"></i>  Второй взнос
        </span>

    </div>

    <br>

    <div class="header_bab">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @if(sizeof($count_arr))
                <div id="tt" class="easyui-tabs" data-options="tools:'#tab-tools'">
                    @foreach($count_arr as $key => $count)
                        <div title="{{$count['title']}} {{($count['count']>0)?$count['count']:''}}" id="tab-{{$key}}" data-fact="{{$loop->index}}" data-view="{{$key}}"></div>
                    @endforeach
                </div>
            @else
                У вас нет доступных вкладок в этом разделе
            @endif
        </div>
    </div>


    <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container" style="margin-top: -5px;overflow: auto;">
        <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="filter-group">

                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="sk">СК</label>
                            {{ Form::select('sk', \App\Models\Directories\InsuranceCompanies::where('is_actual', 1)->pluck('title', 'id'), request('sk', 0), ['class' => 'form-control select2-all',  'onchange'=>'loadData()',"multiple" => true]) }}
                        </div>

                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="bso_title">БСО</label>
                            {{ Form::text('bso_title', request('bso_title', ''), ['class' => 'form-control',  'onkeyup'=>'loadData()']) }}
                        </div>

                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="product">Продукт</label>
                            {{ Form::select('product', $products->pluck('title', 'id')->prepend('Все', 0), request('product', 0), ['class' => 'form-control select2-ws',  'onchange'=>'loadData()']) }}
                        </div>



                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="insurer">Страхователь</label>
                            {{ Form::text('insurer', request('insurer', ''), ['class' => 'form-control',  'onkeyup'=>'loadData()']) }}
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>

                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="agent">Тип контракта</label>
                            {{ Form::select('contract_type', collect(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE)->prepend('Все', -1), request('contract_type') ?? -1, ['class' => 'form-control select2-ws',  'onchange'=>'loadData()']) }}
                        </div>

                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="agent">Агент / Курьер / Менеджер</label>
                            {{ Form::select('agent_id', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id')->prepend('Все', 0), request('agent', 0), ['class' => 'form-control select2',  'onchange'=>'loadData()']) }}
                        </div>



                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="conclusion_date_from">Дата заключения с</label>
                            {{ Form::text('conclusion_date_from', request('conclusion_date_from', ''), ['class' => 'form-control datepicker date',  'onchange'=>'loadData()']) }}
                        </div>

                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="conclusion_date_to">Дата заключения по</label>
                            {{ Form::text('conclusion_date_to', request('conclusion_date_to', ''), ['class' => 'form-control datepicker date',  'onchange'=>'loadData()']) }}
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="table" class="table"></div>
            </div>
        </div>
    </div>






@endsection

@section('js')

    <script>
        var map;
        var TAB_INDEX = 0;
        var current_tab = {{$set_tab ? $set_tab : -1}};

        @php
            /* Для переключения вкладки - в GET-параметр("set_tab") */
            /* значение взять из app/Models/Contracts/Contracts.php метода getVerificationCountArr() key из массива $result */
        @endphp

        $(function () {

            $(document).on('click', '[data-export]', function(){
                var data = getData();
                data.view = $(this).attr('data-export');
                var query = $.param({ method: 'Contracts\\Verification\\VerificationController@get_export_table', param: data});
                location.href = '/exports/table2excel?'+query;
            });

            $(document).on('click', '#check_all_contracts', function () {
                var checked = $(this).prop('checked');
                $('[name*="contract["]').prop('checked', checked).change();
            });

            $(document).on('click', '[name*="contract["]', function () {
                var all_checked = true;
                $.each($('[name*="contract["]'), function (k, v) {
                    all_checked = all_checked && $(v).prop('checked');
                });
                $('#check_all_contracts').prop('checked', all_checked).change();
            });

            if($('[data-view]').length > 0){

                $('#tt').tabs({
                    border:false,
                    pill: false,
                    plain: true,
                    onSelect: function(title, index){
                        return selectTab(index);
                    }
                });

                if (current_tab != -1){
                    tab_ = $('.panel-body-noborder[data-view='+current_tab+']');
                    index = $(tab_).attr('data-fact');

                    $('.tabs').children()[index].click();

                    selectTab(index);
                }else{
                    selectTab(0);
                }

            }


        });

        $('#export_act_of_disagreement').on('click', function(e){
            if(e.shiftKey) {
                location.href = '/contracts/verification/get_export_act_of_disagreement?documentation=1';
            }else{
                if ($('[name*="contract["]').serialize() === ''){
                    Swal.fire(
                        'Отказ',
                        'Пожалуйста выберите необходимые договора',
                        'warning'
                    );
                } else{
                    location.href = '/contracts/verification/get_export_act_of_disagreement?' + $('[name*="contract["]').serialize();
                }

            }
        });


        function selectTab(id) {
            TAB_INDEX = id;
            loaderShow();

            $('[data-export-id]').hide();
            //$('[data-export_tab="'+id+'"]').show();
            loadData();
            initTab();
            loaderHide();
            {{--$.get("{{url("/contracts/verification/get_tab")}}", getData(), function (response) {--}}
            {{--    $("#main_container").html(response);--}}
            {{--    loadData();--}}
            {{--    initTab();--}}
            {{--}).always(function() {--}}
            {{--    loaderHide();--}}
            {{--});--}}

        }


        function loadData(){
            $('#table').html('');
            $.get("{{url("/contracts/verification/get_table")}}", getData(), function(table_response){
                $('#table').html(table_response);
            });
        }

        function getData(){
            var tab = $('#tt').tabs('getSelected');
            var load = tab.data('view');

            if (load == 2){
                $('[data-export-id="'+load+'"]').show();
            }


            return {
                statys: load,
                sk: $('[name="sk"]').val(),
                bso_title: $('[name="bso_title"]').val(),
                product: $('[name="product"]').val(),
                insurer: $('[name="insurer"]').val(),
                agent: $('[name="agent_id"]').val(),
                conclusion_date_from: $('[name="conclusion_date_from"]').val(),
                conclusion_date_to: $('[name="conclusion_date_to"]').val(),
                contract_type: $('[name="contract_type"]').val(),
                courier: $('[name="courier"]').val()
            }
        }

        function initTab() {
            //startMainFunctions();
        }

    </script>

@endsection