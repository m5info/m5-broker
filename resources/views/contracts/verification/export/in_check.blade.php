<table class="tov-table">
    <thead>
    <tr>
        <th>№ п/п</th>
        <th>Дата договора</th>
        <th>Тип акцепта</th>
        <th>Вид страхования</th>
        <th>СК</th>
        <th>БСО</th>
        <th>ФИО Страхователя</th>
        <th>Андеррайтер (акцепт).</th>
        <th>Менеджер</th>
        <th>Агент</th>
        <th>Условие акцепта</th>
    </tr>
    </thead>
    <tbody>
        @if(sizeof($contracts))
            @foreach($contracts as $contract)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ setDateTimeFormatRu($contract->contract_sign_date, 1) }}</td>
                    <td>{{ isset(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE[$contract->contract_kind_acceptance]) ? \App\Models\Contracts\Contracts::KIND_ACCEPTANCE[$contract->contract_kind_acceptance] : '' }}</td>
                    <td>{{ $contract->product_title }}</td>
                    <td>{{ $contract->insurance_companies_title }}</td>
                    <td>{{ $contract->bso_items_bso_title }}</td>
                    <td>{{ $contract->insurer_title }}</td>
                    <td>
                        @php
                            $payments_accept = \App\Models\Actions\PaymentAccept::query()->where('contract_id', $contract->contract_id)->first();
                            if ($payments_accept){
                                $accept_user = \App\Models\User::query()->where('id', $payments_accept->accept_user_id)->first();
                            }
                        @endphp
                        {{ $accept_user ? $accept_user->name : ''}}
                    </td>
                    <td>{{ /*$contract->sales_condition == 1 && */$contract->manager_name }}</td>
                    <td>{{ /*$contract->sales_condition == 0 && */$contract->agent_name }}</td>
                    <td>
                        @php
                            $errors = \App\Models\Contracts\ContractMessage::query()
                                ->where('contract_id', '=', $contract->contract_id)
                                ->where('type_id', '=', 1)
                                ->get()
                        @endphp
                        {{ $errors->last() ? $errors->last()->message : '' }}
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="11" class="text-center">Нет записей</td>
            </tr>
        @endif
    </tbody>
</table>
