<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="filter-group">

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="sk">СК</label>
                    {{ Form::select('sk', \App\Models\Directories\InsuranceCompanies::where('is_actual', 1)->pluck('title', 'id')->prepend('Все', 0), request('sk', 0), ['class' => 'form-control select2-all',  'onchange'=>'loadData()',"multiple" => true]) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="bso_title">БСО</label>
                    {{ Form::text('bso_title', request('bso_title', ''), ['class' => 'form-control',  'onkeyup'=>'loadData()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="product">Продукт</label>
                    {{ Form::select('product', $products->pluck('title', 'id')->prepend('Все', 0), request('product', 0), ['class' => 'form-control select2-ws',  'onchange'=>'loadData()']) }}
                </div>



                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="insurer">Страхователь</label>
                    {{ Form::text('insurer', request('insurer', ''), ['class' => 'form-control',  'onkeyup'=>'loadData()']) }}
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="agent">Тип контракта</label>
                    {{ Form::select('contract_type', collect(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE)->prepend('Все', -1), request('contract_type') ?? -1, ['class' => 'form-control select2-ws',  'onchange'=>'loadData()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="agent">Агент / Курьер</label>
                    {{ Form::select('agent', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id')->prepend('Все', 0), request('agent', 0/*auth()->id()*/), ['class' => 'form-control select2',  'onchange'=>'loadData()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="agent">Менеджер</label>
                    {{ Form::select('manager', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id')->prepend('Все', 0), request('agent', 0/*auth()->id()*/), ['class' => 'form-control select2',  'onchange'=>'loadData()']) }}
                </div>


                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="conclusion_date_from">Дата заключения с</label>
                    {{ Form::text('conclusion_date_from', request('conclusion_date_from', ''), ['class' => 'form-control datepicker date',  'onchange'=>'loadData()']) }}
                </div>

                <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <label class="control-label" for="conclusion_date_to">Дата заключения по</label>
                    {{ Form::text('conclusion_date_to', request('conclusion_date_to', ''), ['class' => 'form-control datepicker date',  'onchange'=>'loadData()']) }}
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div id="table" class="table"></div>
    </div>
</div>