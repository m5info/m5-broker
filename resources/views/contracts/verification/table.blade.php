<table class="tov-table table  table-bordered">
    <thead>
    <tr>
        <th><input type="checkbox" id="check_all_contracts" autocomplete="off"></th>
        <th>#</th>
        <th>СК</th>
        <th>БСО</th>
        <th>Продукт</th>
        <th>Страхователь</th>
        <th>Дата заключения</th>
        <th>Дата акцепта</th>
        <th>Период</th>
        <th>Агент / Курьер</th>
        <th>Менеджер</th>
        <th>Андеррайтер</th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @if(sizeof($contracts))
        @foreach($contracts as $contract)
            @php
                $fixed_status = false;
                $logs = \App\Models\Contracts\ContractsLogs::query()->where('contract_id', '=', $contract->contract_id)->get();
                $last_contract = $logs->last();
                $temp_contracts = request()->route()->getPrefix() == 'contracts/temp_contracts';
                if($temp_contracts && $last_contract){
                    $fixed_status = $last_contract->status_id == 2 && $contract->statys_id == 1;
                }
            @endphp
            <tr class="
                {{ $fixed_status ? 'bg-yellow' : '' }}
                {{$contract->isOverdueConditionalAcceptance() && in_array($contract->contract_statys_id, [2, 7])  ? 'bg-red' : ''}}"
                @if ($contract->isOverdueConditionalAcceptance() && in_array($contract->contract_statys_id, [2, 7]))
                    title="С момента условного акцепта прошло {{$contract->daysFromDelay()}} дней" data-toggle="tooltip" data-placement="auto"
                @endif
            >
                <td><input type="checkbox" name="contract[{{$contract->contract_id}}]" value="{{$contract->contract_id}}"></td>
                <td>{{$contract->contract_id}}</td>
                <td>{{$contract->insurance_companies_title}}</td>
                <td>{{$contract->bso_items_bso_title}}</td>
                <td>{{$contract->product_title}}</td>
                <td>{{$contract->insurer_title}}</td>
                <td>{{setDateTimeFormatRu($contract->contract_sign_date, 1)}}</td>
                <td>{{setDateTimeFormatRu($contract->contract_check_date, 1)}}</td>
                <td>{{setDateTimeFormatRu($contract->contract_begin_date, 1)}} - {{setDateTimeFormatRu($contract->contract_end_date, 1)}}</td>
                <td>{{$contract->agent_name}}</td>
                <td>{{$contract->manager_name}}</td>
                <td>{{$contract->check_user_name}}</td>
                <td>
                    <span style="color: red;">
                        @php
                            $errors = \App\Models\Contracts\ContractMessage::query()
                                ->where('contract_id', '=', $contract->contract_id)
                                ->where('type_id', '=', 1)
                                ->get()
                        @endphp
                        {{ $errors->last() ? $errors->last()->message : '' }}
                    </span>
                </td>
                <td>
                    @if($contract->statys_id == 4)
                        <a class="btn btn-success" href="{{url("/contracts/prolongation/{$contract->contract_id}/")}}">
                            <i class="fa fa-eye"></i>
                        </a>
                    @else
                        <a class="btn btn-success" href="{{url("/contracts/verification/contract/{$contract->contract_id}/edit")}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    @endif
                </td>
                <td>
                    @if(Auth::user()->hasPermission('role_owns', 'is_underwriter'))
                        @if($contract->contract_statys_id == 2)
                            <a class="btn btn-danger" href="{{ url("/contracts/verification/contract/{$contract->contract_id}/return_to_check") }}" title="Вернуть в проверку">
                                <i class="fa fa-undo"></i>
                            </a>
                        @endif
                    @endif
                </td>

            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="15" class="text-center">Нет записей</td>
        </tr>
    @endif

    </tbody>
</table>

<script>
    $(document).ready(function () {
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    })
</script>
