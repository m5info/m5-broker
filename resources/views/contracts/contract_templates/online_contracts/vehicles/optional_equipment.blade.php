<div class="col-sm-6 optional-equipment-form" id="optional_equipment_part_{{$i}}" data-id="{{$i}}">
    <h4 class="">Оборудование {{$i}} <a href="javascript:void(0);" class="btn btn-danger pull-right"
                                        onclick="removeEquipment({{$i}});" id="remove_equipment_{{$i}}"
                                        style="position: relative; bottom: 15px;"
                                        title="Удалить дополнительное оборудование">
            <span class="glyphicon glyphicon-remove"></span>
        </a>
    </h4>
    <div class="clear"></div>
    <div class="col-md-8">
        <label>Название</label>{{Form::input('text', "contract[equipment][{$i}][title]", '', ['class'=> 'form-control'])}}
    </div>
    <div class="col-md-4">
        <label>Страховая
            сумма</label>{{Form::input('text', "contract[equipment][{$i}][insurance_amount]", '', ['class'=> 'form-control'])}}
    </div>
</div>
<div class="clear"></div>
