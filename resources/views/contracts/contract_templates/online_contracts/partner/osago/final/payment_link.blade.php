<div class="data-calc-response">
    <div class="company">
        <div class="logo"><img src="/calc/static/banks/{{ $data['calculation']->insurance_companies->logo->original_name }}" alt=""></div>
    </div>
    <div class="calculation" style="padding: 20px;">
        Оплатите полис по ссылке, и мы пришлем его на Вашу почту {{$data['contract']->send_email}}
    </div>
    <a class="button submit_btn" href="{{$data['link']}}">Оплатить</a>
</div>