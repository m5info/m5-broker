@if(isset($calc->sum) && $calc->sum)
    <div class="data-calc-response sum-response">
        <div class="company">
            <div class="logo"><img src="/calc/static/banks/{{ $calc->insurance_companies->logo->original_name }}" alt=""></div>
        </div>
        <div class="calculation">
            <div class="price">{{titleFloatFormat($calc->sum)}} ₽</div>
            <div class="date">с {{date('j', strtotime($calc->contract->begin_date))}} {{mb_strtolower(getRuMonthes('rod')[date('n', strtotime($calc->contract->begin_date))])}}</div>
        </div>
        <div class="button submit_btn">Выбрать</div>
    </div>
@else
    <div class="data-calc-response sum-response inactive">
        <div class="company">
            <div class="logo"><img src="/calc/static/banks/{{ $calc->insurance_companies->logo->original_name }}" alt=""></div>
        </div>
        <div class="calculation">
            <div class="info">Предложение от компании «{{ $calc->insurance_companies->title}}» не удалось получить в автоматическом режиме.</div>
        </div>
    </div>
@endif