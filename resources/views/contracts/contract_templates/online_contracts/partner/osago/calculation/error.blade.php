<div class="data-calc-response error-response error" id="confirm_fail_results">
    <div class="calculation">
        <div class="info">В автоматическом режиме не удалось расчитать точную стоимость полиса. Наш менеджер подготовит предложения и свяжется с вами в ближайшее время.</div>
    </div>
</div>