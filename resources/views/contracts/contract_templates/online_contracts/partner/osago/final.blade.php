@if($data['lead']->need_diagnostics_card)
    @include('contracts.contract_templates.online_contracts.partner.osago.final.need_dk', ['data' => $data])
@else
    @if(!$data['link'])
        @include('contracts.contract_templates.online_contracts.partner.osago.final.error', ['data' => $data])
    @else
        @include('contracts.contract_templates.online_contracts.partner.osago.final.payment_link', ['data' => $data])
    @endif
@endif