@extends('layouts.app')

@section('js')

    @include('contracts.contract_templates.online_contracts.js')

@endsection


@section('work-area')

    @if(($contract->statys_id == 1) && $contract->selected_calculation && ($contract->selected_calculation->statys_id == 4))
        @include("contracts.contract_templates.default.workarea.{$view_type}")
    @endif
@append

@section('content')

    <div class="row form-horizontal" id="main_container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                @if(View::exists("contracts.contract_templates.online_contracts.products.{$product->slug}.main.{$view_type}"))
                    <form id="product_form">
                        @include("contracts.contract_templates.online_contracts.products.{$product->slug}.main.{$view_type}", [])
                    </form>
                @else
                    <p>Оформление невозможно. Форма для продукта отсутствует</p>
                @endif
            </div>
        </div>
    </div>

@endsection


@if(($contract->statys_id == 1) && $contract->selected_calculation && ($contract->selected_calculation->statys_id == 4))
    <script src="/plugins/jquery/jquery.min.js"></script>
    <script>
        $(function(){
            initWorkArea();
        })

    </script>

    @include('contracts.contract_templates.default.js')
    @include('contracts.contract_templates.temp_contracts.js')

@endif





