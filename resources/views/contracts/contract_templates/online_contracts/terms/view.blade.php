<div class="block-view">
    <h3>Срок и период страхования</h3>
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Время начала</span>
                <span class="view-value">{{$contract->begin_date ? date('H:i', strtotime($contract->begin_date)) : '12:00'}}</span>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Дата начала</span>
                <span class="view-value">{{ $contract->begin_date  ? setDateTimeFormatRu($contract->begin_date, 1): Carbon\Carbon::now()->format('d.m.Y')}}</span>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Дата окончания</span>
                <span class="view-value">{{$contract->end_date  ? setDateTimeFormatRu($contract->end_date, 1) : Carbon\Carbon::now()->addYear()->subDay(1)->format('d.m.Y')}}</span>
            </div>
        </div>

    </div>
</div>
