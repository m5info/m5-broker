<div class="block-view">
    <h3>Условия договора</h3>
    <div class="row">

        <div class="col-md-6 col-lg-3" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата начала <span class="required">*</span>
                    </label>
                    {{ Form::text('contract[object][dk_date_from]', getDateFormatRu($object->dk_date_from), ['class' => 'form-control is_validate format-date end-date', 'placeholder' => '13.02.2018']) }}
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-lg-3" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата окончания <span class="required">*</span>
                    </label>
                    {{ Form::text('contract[object][dk_date]', getDateFormatRu($object->dk_date), ['class' => 'form-control is_validate format-date end-date', 'placeholder' => '13.02.2019']) }}
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>


        <div class="col-md-12 col-lg-6" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Комментарий
                    </label>
                    {{ Form::text('contract[object][dk_coments]', $object->dk_coments, ['class' => 'form-control', 'placeholder' => '']) }}
                </div>
            </div>
        </div>

        <div class="clear"></div>

        @php
            $subject = (isset($contract->insurer)?$contract->insurer:new \App\Models\Contracts\Subjects());
        @endphp





        <div class="col-md-6 col-lg-6" >
            <label class="control-label">Страхователь ФИО <span class="required">*</span></label>
            {{ Form::text("contract[insurer][fio]", $subject->title, ['class' => 'form-control is_validate', 'id'=>"insurer_fio", 'data-key'=>"insurer", 'placeholder' => 'Иванов Иван Иванович']) }}

            <input type="hidden" name="contract[insurer][type]" value="0"/>
            <input type="hidden" name="contract[insurer][email]" value=""/>
            <input type="hidden" name="contract[insurer][phone]" value=""/>
            <input type="hidden" name="contract[insurer][doc_serie]" value=""/>
            <input type="hidden" name="contract[insurer][doc_number]" value=""/>

        </div>

        <div class="col-md-6 col-lg-6" >
            <label class="control-label">Стоимость ДК <span class="required">*</span></label>
            {{ Form::text("contract[payment_total]", ($contract->payment_total>0)?titleFloatFormat($contract->payment_total):'', ['class' => 'form-control is_validate sum', 'placeholder' => '']) }}
        </div>

        <div class="clear"></div>


    </div>
</div>
