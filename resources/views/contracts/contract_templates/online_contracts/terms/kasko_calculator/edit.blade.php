<div class="block-view">
    <h3>Условия договора</h3>
    <div class="row">


        <div class="col-md-2 col-lg-1" >
            <label class="control-label">Страхователь</label>
            {{Form::select("contract[insurer][type]", collect([0=>"ФЛ", 1=>'ЮЛ']), ($contract->insurer)?$contract->insurer->type:0, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-md-2 col-lg-1">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Время <span class="required">*</span>
                    </label>
                    <input placeholder="" name="contract[begin_time]" class="form-control format-time" value="{{$contract->begin_time  ?? '12:00'}}">
                    <span class="glyphicon glyphicon-time calendar-icon"></span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-lg-2">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата начала <span class="required">*</span>
                    </label>
                    <input placeholder="" name="contract[begin_date]" class="form-control format-date" id="begin_date_0" onchange="setEndDates(0)" value="{{ $contract->begin_date  ? setDateTimeFormatRu($contract->begin_date, 1): Carbon\Carbon::now()->format('d.m.Y')}}">
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-lg-2">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата окончания <span class="required">*</span>
                    </label>
                    <input placeholder="" name="contract[end_date]" class="form-control format-date end-date" id="end_date_0" value="{{$contract->end_date  ? setDateTimeFormatRu($contract->end_date, 1) : Carbon\Carbon::now()->addYear()->subDay(1)->format('d.m.Y')}}">
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>


        <div class="col-md-3 col-lg-3" >
            <label class="control-label">Тип договора</label>
            {{Form::select("contract[is_prolongation]", collect([0=>"Первичный", 1=>'Пролонгация']), $contract->is_prolongation, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>


        <div class="ccol-md-3 col-lg-3" >
            <label class="control-label">Договор пролонгации</label>
            {{ Form::text("contract[prolongation_bso]", $terms->prolongation_bso, ['class' => 'form-control']) }}
        </div>



        <div class="clear"></div>

        <div class="col-md-3 col-lg-2" >
            <label class="control-label">Ущерб доп. оборудованию</label>
            {{ Form::text("contract[object][price_equipment]", ($object->price_equipment)?titleFloatFormat($object->price_equipment):'', ['class' => 'form-control sum']) }}
        </div>

        <div class="col-md-3 col-lg-2" >
            <label class="control-label">ТС куплено в кредит</label>
            {{ Form::select("contract[bank_id]", collect(\App\Models\Settings\Bank::all())->pluck('title', 'id')->prepend('Нет', 0), $contract->bank_id, ['class' => 'form-control select2-all', 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-md-3 col-lg-2" >
            <label class="control-label">Вид страховой суммы</label>
            {{Form::select("contract[type_sum_insured_id]", collect([0=>"Неагрегатная", 1=>'Агрегатная']), $terms->type_sum_insured_id, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-lg-3" >
            <label class="control-label">Вид франшизы</label>
            {{Form::select("contract[franchise_type_id]", collect([0=>"Нет", 1=>'При каждом случае', 2=>'Со второго случая']), $terms->franchise_type_id, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>
        <div class="col-lg-3" >
            <label class="control-label">Размер франшизы</label>
            {{ Form::text("contract[franchise]", ($terms->franchise)?titleFloatFormat($terms->franchise):'', ['class' => 'form-control sum']) }}
        </div>



    </div>
</div>


<script>

    function initTerms() {





    }

</script>