<div class="block-view">
    <h3>Условия договора</h3>
    <div class="row">



        <div class="col-md-2 col-lg-2">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Время <span class="required">*</span>
                    </label>
                    <input placeholder="" name="contract[begin_time]" class="form-control format-time" value="{{$contract->begin_time  ?? '12:00'}}">
                    <span class="glyphicon glyphicon-time calendar-icon"></span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-lg-2">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата начала <span class="required">*</span>
                    </label>
                    <input placeholder="" name="contract[begin_date]" class="form-control format-date" id="begin_date_0" onchange="setEndDates(0)" value="{{ $contract->begin_date  ? setDateTimeFormatRu($contract->begin_date, 1): Carbon\Carbon::now()->format('d.m.Y')}}">
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-lg-2">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата окончания <span class="required">*</span>
                    </label>
                    <input placeholder="" name="contract[end_date]" class="form-control format-date end-date" id="end_date_0" value="{{$contract->end_date  ? setDateTimeFormatRu($contract->end_date, 1) : Carbon\Carbon::now()->addYear()->subDay(1)->format('d.m.Y')}}">
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-lg-2" >
            <label class="control-label">Алгоритм рассрочки</label>
            {{ Form::select("contract[installment_algorithms_id]", $contract->insurance_companies ? $contract->insurance_companies->getAlgorithmsProductArray($contract->product_id) : [] , $contract->installment_algorithms_id, ['class' => 'form-control']) }}
        </div>

        <div class="col-md-3 col-lg-2" >
            <label class="control-label">Тип договора</label>
            {{Form::select("contract[is_prolongation]", collect([0=>"Первичный", 1=>'Пролонгация']), $contract->is_prolongation, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>


        <div class="ccol-md-3 col-lg-2" >
            <label class="control-label">Договор пролонгации</label>
            {{ Form::text("contract[prolongation_bso]", $terms->prolongation_bso, ['class' => 'form-control']) }}
        </div>




        <div class="clear"></div>





    </div>
</div>


<script>

    function initTerms() {





    }

</script>