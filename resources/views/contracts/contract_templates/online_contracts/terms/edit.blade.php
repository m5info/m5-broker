<div class="block-view">
    <h3>Срок и период страхования</h3>
    <div class="row">
        <div class="col-md-4 col-lg-4">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Время начала <span class="required">*</span>
                    </label>
                    <input placeholder="" name="contract[begin_time]" class="form-control format-time" value="{{$contract->begin_date ? date('H:i', strtotime($contract->begin_date)) : '00:00'}}">
                    <span class="glyphicon glyphicon-time calendar-icon"></span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата начала <span class="required">*</span>
                    </label>
                    <input placeholder="" name="contract[begin_date]" class="form-control format-date" id="begin_date_0" onchange="setEndDates(0)" value="{{ $contract->begin_date  ? setDateTimeFormatRu($contract->begin_date, 1): Carbon\Carbon::now()->format('d.m.Y')}}">
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата окончания <span class="required">*</span>
                    </label>
                    <input placeholder="" name="contract[end_date]" class="form-control format-date end-date" id="end_date_0" value="{{$contract->end_date  ? setDateTimeFormatRu($contract->end_date, 1) : Carbon\Carbon::now()->addYear()->subDay(1)->format('d.m.Y')}}">
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>
    </div>
</div>
