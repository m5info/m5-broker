

<div class="block-view">
    <h3>{{$subject_title}}
        @if($subject_name != 'insurer')
            {{ Form::checkbox("contract[$subject_name][is_insurer]", 1, $is_insurer,['onclick' => "viewSubjectData_{$subject_name}()", 'id'=>"{$subject_name}_is_insurer"]) }} <span style="font-size: 13px;">страхователь</span>
        @endif
        <div class="col-lg-1 pull-right">
            {{ Form::select("contract[$subject_name][type]", collect([0=>"ФЛ", 1=>'ЮЛ']), $subject->type, ['class' => 'form-control', 'id'=>"{$subject_name}_type", 'data-key'=>"{$subject_name}"]) }}
        </div>
    </h3>
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="control_form_{{$subject_name}}">
        </div>

    </div>
</div>



<script>
    function initSubject_{{$subject_name}}()
    {
        $('#{{$subject_name}}_type').change(function () {

            var forUnderfill = {{isset($forUnderfill) && $forUnderfill ? 1 : 0}};

            loaderShow();
            $.get("{{url("/contracts/online/action/".(int)$contract->id)}}/subject?name={{$subject_name}}&type="+parseInt($(this).val())+"&forUnderfill="+forUnderfill, {}, function (response)  {
                loaderHide();
                $("#control_form_{{$subject_name}}").html(response);
                initStartSubject();
            })
                .done(function() {
                    loaderShow();
                })
                .fail(function() {
                    loaderHide();
                })
                .always(function() {
                    loaderHide();
                });


        });

        $('#{{$subject_name}}_type').change();



    }


    document.addEventListener("DOMContentLoaded", function (event) {
        initSubject_{{$subject_name}}();
        @if($subject_name != 'insurer')
            setTimeout('viewSubjectData_{{$subject_name}}()', 1000);
        @endif
    });


    function viewSubjectData_{{$subject_name}}()
    {
        if($('#{{$subject_name}}_is_insurer').prop('checked')){
            $("#control_form_{{$subject_name}}").html('');
        }else{
            $('#{{$subject_name}}_type').change();
        }
    }


</script>

