{{--(isset($contract->insurer)?$contract->insurer:new \App\Models\Contracts\Subjects())--}}
<div class="block-view">

    <h3>{{$subject_title}}
    </h3>

        <div class="view-field">
            <span class="view-label">Тип владельца</span>
            @if($subject->type)
                <span class="view-value">ЮЛ</span>
            @else
                <span class="view-value">ФЛ</span>
            @endif
        </div>

        @include('contracts.contract_templates.online_contracts.subject.partials.edit.2', [
        'subject_title' => 'Страхователь',
        'subject_name' => 'insurer',
        'subject' => $contract->insurer,
        'subject_data' => $contract->insurer->get_info()
    ])


    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="control_form_{{$subject_name}}">
        </div>

    </div>
</div>


<script>
    function initSubject_{{$subject_name}}() {
        $('#{{$subject_name}}_type').change(function () {

            var forUnderfill = {{isset($forUnderfill) && $forUnderfill ? 1 : 0}};

            loaderShow();
            $.get("{{url("/contracts/online/action/".(int)$contract->id)}}/subject?name={{$subject_name}}&type=" + parseInt($(this).val())+"&forUnderfill="+forUnderfill, {}, function (response) {
                loaderHide();
                $("#control_form_{{$subject_name}}").html(response);
                initStartSubject();
            })
                .done(function () {
                    loaderShow();
                })
                .fail(function () {
                    loaderHide();
                })
                .always(function () {
                    loaderHide();
                });


        });

        $('#{{$subject_name}}_type').change();


    }


    document.addEventListener("DOMContentLoaded", function (event) {
        initSubject_{{$subject_name}}();
        @if($subject_name != 'insurer')
        setTimeout('viewSubjectData_{{$subject_name}}()', 1000);
        @endif
    });


    function viewSubjectData_{{$subject_name}}() {
        if ($('#{{$subject_name}}_is_insurer').prop('checked')) {
            $("#control_form_{{$subject_name}}").html('');
        } else {
            $('#{{$subject_name}}_type').change();
        }
    }


</script>

