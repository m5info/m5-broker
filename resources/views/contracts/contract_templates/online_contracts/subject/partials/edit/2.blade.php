<div class="row form-horizontal">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="view-field">
            <span class="view-label">ФИО</span>
            <span class="view-value">{{$subject_data->fio}}</span>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="view-field">
            <span class="view-label">Пол</span>
            <span class="view-value">@if((int)$subject_data->sex)
                    жен.
                @else
                    муж.
                @endif</span>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="view-field">
            <span class="view-label">Дата рождения</span>
            <span class="view-value">{{getDateFormatRu($subject_data->birthdate)}}</span>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="view-field">
            <span class="view-label">Телефон</span>
            <span class="view-value">{{$subject->phone}}</span>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="view-field">
            <span class="view-label">Email</span>
            <span class="view-value">{{$subject->email}}</span>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="view-field">
            <span class="view-label">Место рождения</span>
            <span class="view-value">{{$subject_data->address_born}}</span>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="view-field">
            <span class="view-label">Адрес регистрации</span>
            <span class="view-value">{{$subject_data->address_born}}</span>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="view-field">
            <span class="view-label">Адрес фактический</span>
            <span class="view-value">{{$subject_data->address_fact}}</span>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="view-field">
            <span class="view-label">Тип документа</span>
            <span class="view-value">Паспорт гражданина РФ</span>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="view-field">
            <span class="view-label">Серия </span>
            <span class="view-value">{{$subject_data->doc_serie}}</span>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="view-field">
            <span class="view-label">Номер </span>
            <span class="view-value">{{$subject_data->doc_number}}</span>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="view-field">
            <span class="view-label">Дата выдачи </span>
            <span class="view-value">{{setDateTimeFormatRu($subject_data->doc_date, 1)}}</span>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="view-field">
            <span class="view-label">Код подразделения </span>
            <span class="view-value">{{$subject_data->doc_office}}</span>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="view-field">
            <span class="view-label">Кем выдан </span>
            <span class="view-value">{{$subject_data->doc_info}}</span>
        </div>
    </div>
    @if(isset($forUnderfill) && $forUnderfill)
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Класс, КБМ </span>
                <span class="view-value">{{$subject_data->class_kmb}}</span>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Кол-во страх. случаев </span>
                <span class="view-value">{{$subject_data->count_insurance_cases}}</span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Серия В/У</span>
                <span class="view-value">{{$subject_data->license_serie}}</span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Номер В/У</span>
                <span class="view-value">{{$subject_data->license_number}}</span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Дата выдачи</span>
                <span class="view-value">{{$subject_data->license_date}}</span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Стаж с (дата)</span>
                <span class="view-value">{{$subject_data->exp_date}}</span>
            </div>
        </div>
    @endif

</div>