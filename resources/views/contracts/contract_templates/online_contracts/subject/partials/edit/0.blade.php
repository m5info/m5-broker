<div class="row form-horizontal">
    <div class="col-md-6 col-lg-4" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    ФИО <span class="required">*</span>
                </label>
                {{ Form::text("contract[{$subject_name}][fio]", $subject->get_info()->fio, ['class' => 'form-control valid_fast_accept', 'id'=>"{$subject_name}_fio", 'data-key'=>"{$subject_name}", 'placeholder' => 'Иванов Иван Иванович']) }}
                {{ Form::hidden("contract[{$subject_name}][first_name]", $subject->get_info()->first_name, ['id' => "{$subject_name}_first_name"]) }}
                {{ Form::hidden("contract[{$subject_name}][second_name]", $subject->get_info()->second_name, ['id' => "{$subject_name}_second_name"]) }}
                {{ Form::hidden("contract[{$subject_name}][last_name]", $subject->get_info()->last_name, ['id' => "{$subject_name}_last_name"]) }}
            </div>
        </div>
    </div>




    <div class="col-md-3 col-lg-2" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Пол <span class="required">*</span>
                </label>
                {{Form::select("contract[{$subject_name}][sex]", collect([0=>"муж.", 1=>'жен.']), $subject->get_info()->sex, ['class' => 'form-control valid_fast_accept', 'id' => "{$subject_name}_sex", 'data-key'=>"{$subject_name}"]) }}
            </div>
        </div>
    </div>



    <div class="col-md-3 col-lg-2" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Дата рождения <span class="required">*</span>
                </label>
                {{ Form::text("contract[{$subject_name}][birthdate]", setDateTimeFormatRu($subject->get_info()->birthdate, 1), ['class' => 'form-control format-date end-date', 'id'=>"{$subject_name}_birthdate", 'placeholder' => '18.05.1976']) }}
                <span class="glyphicon glyphicon-calendar calendar-icon"></span>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-lg-2" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Телефон
                </label>
                {{ Form::text("contract[{$subject_name}][phone]", $subject->phone, ['class' => 'form-control phone', 'placeholder' => '+7 (451) 653-13-54']) }}
            </div>
        </div>
    </div>


    <div class="col-md-6 col-lg-2" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Email
                </label>
                {{ Form::text("contract[{$subject_name}][email]", $subject->email, ['class' => 'form-control', 'placeholder' => 'test@mail.ru']) }}
            </div>
        </div>
    </div>

    <div class="clear"></div>


    <div class="col-lg-4" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Место рождения <span class="required">*</span>
                </label>
                {{ Form::text("contract[{$subject_name}][address_born]", $subject->get_info()->address_born, ['class' => 'form-control', 'id' => "{$subject_name}_address_born", 'placeholder' => 'Нижегородская обл, Лукояновский р-н, поселок Новая Москва']) }}
                {{ Form::text("contract[{$subject_name}][address_born_kladr]", $subject->get_info()->address_born_kladr, ['class' => 'hidden', 'id' => "{$subject_name}_address_born_kladr"]) }}
            </div>
        </div>
    </div>

    <div class="col-lg-4" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Адрес регистрации <span class="required">*</span>
                </label>
                {{ Form::text("contract[{$subject_name}][address_register]",  $subject->get_info()->address_register, ['class' => 'form-control', 'id' => "{$subject_name}_address_register", 'placeholder' => 'Нижегородская обл, Лукояновский р-н, поселок Новая Москва']) }}
                {{ Form::text("contract[{$subject_name}][address_register_kladr]",  $subject->get_info()->address_register_kladr, ['class' => 'hidden', 'id' => "{$subject_name}_address_register_kladr"]) }}
                <input name="contract[{{$subject_name}}][address_register_region]" id="{{$subject_name}}_address_register_region" value="{{$subject->get_info()->address_register_region}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_region_full]" id="{{$subject_name}}_address_register_region_full" value="{{$subject->get_info()->address_register_region_full}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_city]" id="{{$subject_name}}_address_register_city" value="{{$subject->get_info()->address_register_city}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_city_kladr_id]" id="{{$subject_name}}_address_register_city_kladr_id" value="{{$subject->get_info()->address_register_city_kladr_id}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_settlement]" id="{{$subject_name}}_address_register_settlement" value="{{$subject->get_info()->address_register_settlement}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_settlement_kladr_id]" id="{{$subject_name}}_address_register_settlement_kladr_id" value="{{$subject->get_info()->address_register_settlement_kladr_id}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_street]" id="{{$subject_name}}_address_register_street" value="{{$subject->get_info()->address_register_street}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_house]" id="{{$subject_name}}_address_register_house" value="{{$subject->get_info()->address_register_house}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_block]" id="{{$subject_name}}_address_register_block" value="{{$subject->get_info()->address_register_block}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_flat]" id="{{$subject_name}}_address_register_flat" value="{{$subject->get_info()->address_register_flat}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_zip]" id="{{$subject_name}}_address_register_zip" value="{{$subject->get_info()->address_register_zip}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_okato]" id="{{$subject_name}}_address_register_okato" value="{{$subject->get_info()->address_register_okato}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_city_fias_id]" id="{{$subject_name}}_address_register_city_fias_id" value="{{$subject->get_info()->address_register_city_fias_id}}" type="hidden"/>

            </div>
        </div>
    </div>

    <div class="col-lg-4" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Адрес фактический <span class="required">*</span>
                </label>
                {{ Form::text("contract[{$subject_name}][address_fact]", $subject->get_info()->address_fact, ['class' => 'form-control', 'id' => "{$subject_name}_address_fact", 'placeholder' => 'Нижегородская обл, Лукояновский р-н, поселок Новая Москва']) }}
                {{ Form::text("contract[{$subject_name}][address_fact_kladr]", $subject->get_info()->address_fact_kladr, ['class' => 'hidden', 'id' => "{$subject_name}_address_fact_kladr"]) }}
                <input name="contract[{{$subject_name}}][address_fact_region]" id="{{$subject_name}}_address_fact_region" value="{{$subject->get_info()->address_fact_region}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_region_full]" id="{{$subject_name}}_address_fact_region_full" value="{{$subject->get_info()->address_fact_region_full}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_city]" id="{{$subject_name}}_address_fact_city" value="{{$subject->get_info()->address_fact_city}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_city_kladr_id]" id="{{$subject_name}}_address_fact_city_kladr_id" value="{{$subject->get_info()->address_fact_city_kladr_id}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_settlement]" id="{{$subject_name}}_address_fact_settlement" value="{{$subject->get_info()->address_fact_settlement}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_settlement_kladr_id]" id="{{$subject_name}}_address_fact_settlement_kladr_id" value="{{$subject->get_info()->address_fact_settlement_kladr_id}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_street]" id="{{$subject_name}}_address_fact_street" value="{{$subject->get_info()->address_fact_street}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_house]" id="{{$subject_name}}_address_fact_house" value="{{$subject->get_info()->address_fact_house}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_block]" id="{{$subject_name}}_address_fact_block" value="{{$subject->get_info()->address_fact_block}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_flat]" id="{{$subject_name}}_address_fact_flat" value="{{$subject->get_info()->address_fact_flat}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_zip]" id="{{$subject_name}}_address_fact_zip" value="{{$subject->get_info()->address_fact_zip}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_okato]" id="{{$subject_name}}_address_fact_okato" value="{{$subject->get_info()->address_fact_okato}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_city_fias_id]" id="{{$subject_name}}_address_fact_city_fias_id" value="{{$subject->get_info()->address_fact_city_fias_id}}" type="hidden"/>
            </div>
        </div>
    </div>

    <div class="clear"></div>

    <div class="col-md-4 col-lg-4" >
        <label class="control-label">Тип документа</label>
        {{Form::select("contract[{$subject_name}][doc_type]", collect(\App\Models\Contracts\Subjects::DOC_TYPE), $subject->get_info()->doc_type, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
    </div>




    <div class="col-md-4 col-lg-2" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Серия <span class="required">*</span>
                </label>
                {{ Form::text("contract[{$subject_name}][doc_serie]", $subject->get_info()->doc_serie, ['class' => 'form-control', 'placeholder' => '1234']) }}
            </div>
        </div>
    </div>


    <div class="col-md-4 col-lg-2" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Номер <span class="required">*</span>
                </label>
                {{ Form::text("contract[{$subject_name}][doc_number]", $subject->get_info()->doc_number, ['class' => 'form-control', 'placeholder' => '567890']) }}
            </div>
        </div>
    </div>


    <div class="col-md-6 col-lg-2" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Дата выдачи <span class="required">*</span>
                </label>
                {{ Form::text("contract[{$subject_name}][doc_date]", setDateTimeFormatRu($subject->get_info()->doc_date, 1), ['class' => 'form-control format-date end-date', 'placeholder' => '12.05.2006']) }}
                <span class="glyphicon glyphicon-calendar calendar-icon"></span>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-lg-2" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Код подразделения <span class="required">*</span>
                </label>
                {{ Form::text("contract[{$subject_name}][doc_office]", $subject->get_info()->doc_office, ['class' => 'form-control', 'placeholder' => '567890', 'id' => "{$subject_name}_doc_office"]) }}
            </div>
        </div>
    </div>

    <div class="clear"></div>


    @if(isset($forUnderfill) && $forUnderfill)
        <div class="col-lg-9" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Кем выдан <span class="required">*</span>
                    </label>
                    {{ Form::text("contract[{$subject_name}][doc_info]", $subject->get_info()->doc_info, ['class' => 'form-control', 'placeholder' => 'РУВД Москвы', 'id' => "{$subject_name}_doc_info"]) }}
                </div>
            </div>
        </div>

        <div class="col-lg-1">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Класс, КБМ
                    </label>
                    {{ Form::select("contract[{$subject_name}][class_kmb]", \App\Models\Contracts\SubjectsFl::KBM_CLASS, $subject->get_info()->class_kmb, ['class' => 'form-control', 'id' => "{$subject_name}_class_kmb"]) }}
                </div>
            </div>
        </div>

        <div class="col-lg-2">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Кол-во страх. случаев
                    </label>
                    {{ Form::text("contract[{$subject_name}][count_insurance_cases]", $subject->get_info()->count_insurance_cases, ['class' => 'form-control', 'placeholder' => '0', 'id' => "{$subject_name}_count_insurance_cases"]) }}
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div class="col-lg-2" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Серия В/У
                    </label>
                    {{ Form::text("contract[{$subject_name}][license_serie]", $subject->get_info()->license_serie, ['class' => 'form-control', 'placeholder' => '1234', 'id' => "{$subject_name}_license_serie"]) }}
                </div>
            </div>
        </div>

        <div class="col-lg-2" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Номер В/У
                    </label>
                    {{ Form::text("contract[{$subject_name}][license_number]", $subject->get_info()->license_number, ['class' => 'form-control', 'placeholder' => '123456', 'id' => "{$subject_name}_license_number"]) }}
                </div>
            </div>
        </div>


        <div class="col-lg-2" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата выдачи
                    </label>
                    {{ Form::text("contract[{$subject_name}][license_date]", setDateTimeFormatRu($subject->get_info()->license_date, 1), ['class' => 'form-control format-date end-date', 'placeholder' => '12.05.2006']) }}
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>
        <div class="col-lg-2" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Стаж с (дата)
                    </label>
                    {{ Form::text("contract[{$subject_name}][exp_date]", setDateTimeFormatRu($subject->get_info()->exp_date, 1), ['class' => 'form-control format-date end-date', 'placeholder' => '12.05.2006']) }}
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>

    @else
        <div class="col-lg-12">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Кем выдан <span class="required">*</span>
                    </label>
                    {{ Form::text("contract[{$subject_name}][doc_info]", $subject->get_info()->doc_info, ['class' => 'form-control', 'placeholder' => 'РУВД Москвы', 'id' => "{$subject_name}_doc_info"]) }}
                </div>
            </div>
        </div>
    @endif

</div>


<script>



    function initStartSubject()
    {

        $('#{{$subject_name}}_fio').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "NAME",
            count: 5,
            onSelect: function (suggestion) {
                $('#{{$subject_name}}_first_name').val(suggestion.data.name);
                $('#{{$subject_name}}_second_name').val(suggestion.data.patronymic);
                $('#{{$subject_name}}_last_name').val(suggestion.data.surname);
            }
        });

        $('#{{$subject_name}}_doc_office').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "fms_unit",
            onSelect: function(suggestion){
                $('#{{$subject_name}}_doc_info').val(suggestion.data.name);
            },
            formatResult: function(value, currentValue, suggestion){
                suggestion.value = suggestion.data.code;
                return suggestion.data.code + " — " + suggestion.data.name;
            },
        });

        $('#{{$subject_name}}_address_born').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#{{$subject_name}}_address_born').val($(this).val());
                $('#{{$subject_name}}_address_born_kladr').val(suggestion.data.city_kladr_id);
            }
        });



        $('#{{$subject_name}}_address_register').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {

                key = $(this).data('key');
                $('#{{$subject_name}}_address_register').val($(this).val());
                $('#{{$subject_name}}_address_register_kladr').val(suggestion.data.kladr_id);
                $('#{{$subject_name}}_address_register_okato').val(suggestion.data.okato);
                $('#{{$subject_name}}_address_register_zip').val(suggestion.data.postal_code);
                $('#{{$subject_name}}_address_register_region').val(suggestion.data.region);
                $('#{{$subject_name}}_address_register_region_full').val(suggestion.data.region_with_type);
                $('#{{$subject_name}}_address_register_city').val(suggestion.data.city);
                $('#{{$subject_name}}_address_register_city_kladr_id').val(suggestion.data.city_kladr_id);
                $('#{{$subject_name}}_address_register_settlement').val(suggestion.data.settlement);
                $('#{{$subject_name}}_address_register_settlement_kladr_id').val(suggestion.data.settlement_kladr_id);
                $('#{{$subject_name}}_address_register_street').val(suggestion.data.street_with_type);
                $('#{{$subject_name}}_address_register_house').val(suggestion.data.house);
                $('#{{$subject_name}}_address_register_block').val(suggestion.data.block);
                $('#{{$subject_name}}_address_register_flat').val(suggestion.data.flat);
                $('#{{$subject_name}}_address_register_city_fias_id').val(suggestion.data.city_fias_id);

                $('#{{$subject_name}}_address_fact').val($(this).val());
                $('#{{$subject_name}}_address_fact_kladr').val(suggestion.data.kladr_id);
                $('#{{$subject_name}}_address_fact_region').val(suggestion.data.region);
                $('#{{$subject_name}}_address_fact_region_full').val(suggestion.data.region_with_type);
                $('#{{$subject_name}}_address_fact_okato').val(suggestion.data.okato);
                $('#{{$subject_name}}_address_fact_zip').val(suggestion.data.postal_code);
                $('#{{$subject_name}}_address_fact_city').val(suggestion.data.city);
                $('#{{$subject_name}}_address_fact_city_kladr_id').val(suggestion.data.city_kladr_id);
                $('#{{$subject_name}}_address_fact_settlement').val(suggestion.data.settlement);
                $('#{{$subject_name}}_address_fact_settlement_kladr_id').val(suggestion.data.settlement_kladr_id);
                $('#{{$subject_name}}_address_fact_street').val(suggestion.data.street_with_type);
                $('#{{$subject_name}}_address_fact_house').val(suggestion.data.house);
                $('#{{$subject_name}}_address_fact_block').val(suggestion.data.block);
                $('#{{$subject_name}}_address_fact_flat').val(suggestion.data.flat);
                $('#{{$subject_name}}_address_fact_city_fias_id').val(suggestion.data.city_fias_id);

            }
        });



        $('#{{$subject_name}}_address_fact').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#{{$subject_name}}_address_fact').val($(this).val());
                $('#{{$subject_name}}_address_fact_kladr').val(suggestion.data.kladr_id);
                $('#{{$subject_name}}_address_fact_region').val(suggestion.data.region);
                $('#{{$subject_name}}_address_fact_region_full').val(suggestion.data.region_with_type);
                $('#{{$subject_name}}_address_fact_city').val(suggestion.data.city);
                $('#{{$subject_name}}_address_fact_city_kladr_id').val(suggestion.data.city_kladr_id);
                $('#{{$subject_name}}_address_fact_settlement').val(suggestion.data.settlement);
                $('#{{$subject_name}}_address_fact_settlement_kladr_id').val(suggestion.data.settlement_kladr_id);
                $('#{{$subject_name}}_address_fact_street').val(suggestion.data.street_with_type);
                $('#{{$subject_name}}_address_fact_house').val(suggestion.data.house);
                $('#{{$subject_name}}_address_fact_block').val(suggestion.data.block);
                $('#{{$subject_name}}_address_fact_okato').val(suggestion.data.okato);
                $('#{{$subject_name}}_address_fact_zip').val(suggestion.data.postal_code);
                $('#{{$subject_name}}_address_fact_flat').val(suggestion.data.flat);
                $('#{{$subject_name}}_address_fact_city_fias_id').val(suggestion.data.city_fias_id);
            }
        });


        formatDate();
        $('.phone').mask('+7 (999) 999-99-99');

    }

    $(document).on('keyup', '#{{$subject_name}}_fio', function (e) {

        if(!hasDadataFio('{{$subject_name}}')){
            $('#{{$subject_name}}_first_name').val('');
            $('#{{$subject_name}}_second_name').val('');
            $('#{{$subject_name}}_last_name').val('');
        }
    });


    function hasDadataFio(subject_type) {
        var fio = $('#'+subject_type+'_fio').val();
        var first_name = $('#'+subject_type+'_first_name').val();
        var second_name = $('#'+subject_type+'_second_name').val();
        var last_name = $('#'+subject_type+'_last_name').val();

        var sumLenght = first_name.length + second_name.length + last_name.length + 2;

        if (fio.includes(first_name) && fio.includes(second_name) && fio.includes(last_name) && sumLenght === fio.length){
            return true;
        }

        return  false;
    }


    function {{$subject_name}}FioValidate() {

        var needValidate = true;
        
        if ('{{$subject_name}}' == 'insurer'){
            var subjectInMessage = 'страхователя';
        } else{
            var subjectInMessage = 'владельца';
            
            if ($('#owner_is_insurer').prop('checked')){
                needValidate = false;
            }
        }

        if (!needValidate){
            return true;
        }

        var first_name = $('#{{$subject_name}}_first_name').val().length > 0;
        var second_name = $('#{{$subject_name}}_second_name').val().length > 0;
        var last_name = $('#{{$subject_name}}_last_name').val().length > 0;

        if ((first_name && second_name && last_name)) {
            return true;
        }

        flashHeaderMessage('ФИО '+subjectInMessage+' заполнено некорректно! Выберите вариант ФИО из подсказок.', 'danger');

        return false;
    }

</script>

