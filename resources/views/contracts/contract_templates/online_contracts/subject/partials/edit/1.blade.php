<div class="row form-horizontal">


    <div class="col-lg-3" >
        <label class="control-label">Название компании</label>
        {{ Form::text("contract[{$subject_name}][title]", $subject->get_info()->title, ['class' => 'form-control', 'id'=>"{$subject_name}_title"]) }}
    </div>

    <div class="col-lg-3" >
        <label class="control-label">ИНН</label>
        {{ Form::text("contract[{$subject_name}][inn]", $subject->get_info()->inn, ['class' => 'form-control', 'id'=>"{$subject_name}_inn"]) }}
    </div>

    <div class="col-lg-3" >
        <label class="control-label">КПП</label>
        {{ Form::text("contract[{$subject_name}][kpp]", $subject->get_info()->kpp, ['class' => 'form-control', 'id'=>"{$subject_name}_kpp"]) }}
    </div>


    <div class="col-lg-3" >
        <label class="control-label">БИК</label>
        {{ Form::text("contract[{$subject_name}][bik]", $subject->get_info()->bik, ['class' => 'form-control', 'id'=>"{$subject_name}_bik"]) }}
    </div>

    <div class="clear"></div>

    <div class="col-lg-3" >
        <label class="control-label">Генеральный директор</label>
        {{ Form::text("contract[{$subject_name}][general_manager]", $subject->get_info()->general_manager, ['class' => 'form-control', 'id'=>"{$subject_name}_general_manager"]) }}
    </div>

    <div class="col-lg-3" >
        <label class="control-label">ОГРН</label>
        {{ Form::text("contract[{$subject_name}][ogrn]", $subject->get_info()->ogrn, ['class' => 'form-control', 'id'=>"{$subject_name}_ogrn"]) }}
    </div>

    <div class="col-lg-3">
        <label class="control-label">Свидетельства о регистрации <span class="required">*</span>  </label>
        {{ Form::text("contract[{$subject_name}][doc_serie]", $subject->get_info()->doc_serie, ['class' => 'form-control', 'id'=>"{$subject_name}_doc_serie", 'placeholder'=>'Серия']) }}
    </div>


    <div class="col-lg-3" >
        <label class="control-label">Свидетельства о регистрации<span class="required">*</span> </label>
        {{ Form::text("contract[{$subject_name}][doc_number]", $subject->get_info()->doc_number, ['class' => 'form-control', 'id'=>"{$subject_name}_doc_number", 'placeholder'=>'Номер']) }}
    </div>


    <div class="clear"></div>


    <div class="col-lg-6" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Адрес регистрации <span class="required">*</span>
                </label>
                {{ Form::text("contract[{$subject_name}][address_register]",  $subject->get_info()->address_register, ['class' => 'form-control', 'id' => "{$subject_name}_address_register"]) }}
                {{ Form::text("contract[{$subject_name}][address_register_kladr]",  $subject->get_info()->address_register_kladr, ['class' => 'hidden', 'id' => "{$subject_name}_address_register_kladr"]) }}
                <input name="contract[{{$subject_name}}][address_register_region]" id="{{$subject_name}}_address_register_region" value="{{$subject->get_info()->address_register_region}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_region_full]" id="{{$subject_name}}_address_register_region_full" value="{{$subject->get_info()->address_register_region_full}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_city]" id="{{$subject_name}}_address_register_city" value="{{$subject->get_info()->address_register_city}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_city_kladr_id]" id="{{$subject_name}}_address_register_city_kladr_id" value="{{$subject->get_info()->address_register_city_kladr_id}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_settlement]" id="{{$subject_name}}_address_register_settlement" value="{{$subject->get_info()->address_register_settlement}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_settlement_kladr_id]" id="{{$subject_name}}_address_register_settlement_kladr_id" value="{{$subject->get_info()->address_register_settlement_kladr_id}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_street]" id="{{$subject_name}}_address_register_street" value="{{$subject->get_info()->address_register_street}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_house]" id="{{$subject_name}}_address_register_house" value="{{$subject->get_info()->address_register_house}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_block]" id="{{$subject_name}}_address_register_block" value="{{$subject->get_info()->address_register_block}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_flat]" id="{{$subject_name}}_address_register_flat" value="{{$subject->get_info()->address_register_flat}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_zip]" id="{{$subject_name}}_address_register_zip" value="{{$subject->get_info()->address_register_zip}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_okato]" id="{{$subject_name}}_address_register_okato" value="{{$subject->get_info()->address_register_okato}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_register_city_fias_id]" id="{{$subject_name}}_address_register_city_fias_id" value="{{$subject->get_info()->address_register_city_fias_id}}" type="hidden"/>


            </div>
        </div>
    </div>

    <div class="col-lg-6" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Адрес фактический <span class="required">*</span>
                </label>
                {{ Form::text("contract[{$subject_name}][address_fact]", $subject->get_info()->address_fact, ['class' => 'form-control', 'id' => "{$subject_name}_address_fact"]) }}
                {{ Form::text("contract[{$subject_name}][address_fact_kladr]", $subject->get_info()->address_fact_kladr, ['class' => 'hidden', 'id' => "{$subject_name}_address_fact_kladr"]) }}

                <input name="contract[{{$subject_name}}][address_fact_region]" id="{{$subject_name}}_address_fact_region" value="{{$subject->get_info()->address_fact_region}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_region_full]" id="{{$subject_name}}_address_fact_region_full" value="{{$subject->get_info()->address_fact_region_full}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_city]" id="{{$subject_name}}_address_fact_city" value="{{$subject->get_info()->address_fact_city}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_city_kladr_id]" id="{{$subject_name}}_address_fact_city_kladr_id" value="{{$subject->get_info()->address_fact_city_kladr_id}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_settlement]" id="{{$subject_name}}_address_fact_settlement" value="{{$subject->get_info()->address_fact_settlement}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_settlement_kladr_id]" id="{{$subject_name}}_address_fact_settlement_kladr_id" value="{{$subject->get_info()->address_fact_settlement_kladr_id}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_street]" id="{{$subject_name}}_address_fact_street" value="{{$subject->get_info()->address_fact_street}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_house]" id="{{$subject_name}}_address_fact_house" value="{{$subject->get_info()->address_fact_house}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_block]" id="{{$subject_name}}_address_fact_block" value="{{$subject->get_info()->address_fact_block}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_flat]" id="{{$subject_name}}_address_fact_flat" value="{{$subject->get_info()->address_fact_flat}}" type="hidden"/>
                <input name="contract[{{$subject_name}}][address_fact_city_fias_id]" id="{{$subject_name}}_address_fact_city_fias_id" value="{{$subject->get_info()->address_register_city_fias_id}}" type="hidden"/>

            </div>
        </div>
    </div>



    <div class="clear"></div>
    <div class="col-lg-12" >
        <h4>Контактное лицо</h4>
    </div>

    <div class="col-lg-4" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    ФИО <span class="required">*</span>
                </label>
                {{ Form::text("contract[{$subject_name}][fio]", $subject->get_info()->fio, ['class' => 'form-control valid_fast_accept', 'id'=>"{$subject_name}_fio", 'data-key'=>"{$subject_name}"]) }}
                {{ Form::hidden("contract[{$subject_name}][first_name]", $subject->get_info()->first_name, ['id' => "{$subject_name}_first_name"]) }}
                {{ Form::hidden("contract[{$subject_name}][second_name]", $subject->get_info()->second_name, ['id' => "{$subject_name}_second_name"]) }}
                {{ Form::hidden("contract[{$subject_name}][last_name]", $subject->get_info()->last_name, ['id' => "{$subject_name}_last_name"]) }}
            </div>
        </div>
    </div>

    <div class="col-lg-2" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Дата рождения
                </label>
                {{ Form::text("contract[{$subject_name}][birthdate]", setDateTimeFormatRu($subject->get_info()->birthdate, 1), ['class' => 'form-control format-date end-date']) }}
                <span class="glyphicon glyphicon-calendar calendar-icon"></span>
            </div>
        </div>
    </div>

    <div class="col-lg-2" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Должность
                </label>
                {{Form::text("contract[{$subject_name}][position]", $subject->get_info()->position, ['class' => 'form-control valid_fast_accept']) }}
            </div>
        </div>
    </div>

    <div class="col-lg-2" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Телефон
                </label>
                {{ Form::text("contract[{$subject_name}][phone]", $subject->phone, ['class' => 'form-control phone']) }}
            </div>
        </div>
    </div>


    <div class="col-lg-2" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Email
                </label>
                {{ Form::text("contract[{$subject_name}][email]", $subject->email, ['class' => 'form-control']) }}
            </div>
        </div>
    </div>




</div>


<script>



    function initStartSubject()
    {

        $('#{{$subject_name}}_fio, #{{$subject_name}}_general_manager').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "NAME",
            count: 5,
            onSelect: function (suggestion) {
                $('#{{$subject_name}}_first_name').val(suggestion.data.name);
                $('#{{$subject_name}}_second_name').val(suggestion.data.patronymic);
                $('#{{$subject_name}}_last_name').val(suggestion.data.surname);
            }
        });


        $('#{{$subject_name}}_address_register').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#{{$subject_name}}_address_register').val($(this).val());
                $('#{{$subject_name}}_address_register_kladr').val(suggestion.data.kladr_id);
                $('#{{$subject_name}}_address_register_okato').val(suggestion.data.okato);
                $('#{{$subject_name}}_address_register_zip').val(suggestion.data.postal_code);
                $('#{{$subject_name}}_address_register_region').val(suggestion.data.region);
                $('#{{$subject_name}}_address_register_region_full').val(suggestion.data.region_with_type);
                $('#{{$subject_name}}_address_register_city').val(suggestion.data.city);
                $('#{{$subject_name}}_address_register_city_kladr_id').val(suggestion.data.city_kladr_id);
                $('#{{$subject_name}}_address_register_settlement').val(suggestion.data.settlement);
                $('#{{$subject_name}}_address_register_settlement_kladr_id').val(suggestion.data.settlement_kladr_id);
                $('#{{$subject_name}}_address_register_street').val(suggestion.data.street_with_type);
                $('#{{$subject_name}}_address_register_house').val(suggestion.data.house);
                $('#{{$subject_name}}_address_register_block').val(suggestion.data.block);
                $('#{{$subject_name}}_address_register_flat').val(suggestion.data.flat);
                $('#{{$subject_name}}_address_register_city_fias_id').val(suggestion.data.city_fias_id);


                $('#{{$subject_name}}_address_fact').val($(this).val());
                $('#{{$subject_name}}_address_fact_kladr').val(suggestion.data.kladr_id);
                $('#{{$subject_name}}_address_fact_okato').val(suggestion.data.okato);
                $('#{{$subject_name}}_address_fact_zip').val(suggestion.data.postal_code);
                $('#{{$subject_name}}_address_fact_region').val(suggestion.data.region);
                $('#{{$subject_name}}_address_fact_region_full').val(suggestion.data.region_with_type);
                $('#{{$subject_name}}_address_fact_city').val(suggestion.data.city);
                $('#{{$subject_name}}_address_fact_city_kladr_id').val(suggestion.data.city_kladr_id);
                $('#{{$subject_name}}_address_fact_settlement').val(suggestion.data.settlement);
                $('#{{$subject_name}}_address_fact_settlement_kladr_id').val(suggestion.data.settlement_kladr_id);
                $('#{{$subject_name}}_address_fact_street').val(suggestion.data.street_with_type);
                $('#{{$subject_name}}_address_fact_house').val(suggestion.data.house);
                $('#{{$subject_name}}_address_fact_block').val(suggestion.data.block);
                $('#{{$subject_name}}_address_fact_flat').val(suggestion.data.flat);
                $('#{{$subject_name}}_address_fact_city_fias_id').val(suggestion.data.city_fias_id);

            }
        });

        $('#{{$subject_name}}_address_fact').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "ADDRESS",
            count: 5,
            onSelect: function (suggestion) {
                key = $(this).data('key');
                $('#{{$subject_name}}_address_fact').val($(this).val());
                $('#{{$subject_name}}_address_fact_kladr').val(suggestion.data.kladr_id);
                $('#{{$subject_name}}_address_fact_region').val(suggestion.data.region);
                $('#{{$subject_name}}_address_fact_region_full').val(suggestion.data.region_with_type);
                $('#{{$subject_name}}_address_fact_city').val(suggestion.data.city);
                $('#{{$subject_name}}_address_fact_city_kladr_id').val(suggestion.data.city_kladr_id);
                $('#{{$subject_name}}_address_fact_settlement').val(suggestion.data.settlement);
                $('#{{$subject_name}}_address_fact_settlement_kladr_id').val(suggestion.data.settlement_kladr_id);
                $('#{{$subject_name}}_address_fact_street').val(suggestion.data.street_with_type);
                $('#{{$subject_name}}_address_fact_house').val(suggestion.data.house);
                $('#{{$subject_name}}_address_fact_block').val(suggestion.data.block);
                $('#{{$subject_name}}_address_fact_flat').val(suggestion.data.flat);
                $('#{{$subject_name}}_address_fact_okato').val(suggestion.data.okato);
                $('#{{$subject_name}}_address_fact_zip').val(suggestion.data.postal_code);
                $('#{{$subject_name}}_address_fact_city_fias_id').val(suggestion.data.city_fias_id);
            }
        });


        $('#{{$subject_name}}_title, #{{$subject_name}}_inn, #{{$subject_name}}_kpp').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "NAME",
            type: "PARTY",
            count: 5,
            onSelect: function (suggestion) {
                var data = suggestion.data;

                $('#{{$subject_name}}_title').val(suggestion.value);
                $('#{{$subject_name}}_inn').val(data.inn);
                $('#{{$subject_name}}_kpp').val(data.kpp);
                $('#{{$subject_name}}_ogrn').val(data.ogrn);

                // address fact
                $('#{{$subject_name}}_address_fact').val(suggestion.data.address.value);
                $('#{{$subject_name}}_address_fact_kladr').val(suggestion.data.address.data.kladr_id);
                $('#{{$subject_name}}_address_fact_region').val(suggestion.data.address.data.region);
                $('#{{$subject_name}}_address_fact_region_full').val(suggestion.data.address.data.region_with_type);
                $('#{{$subject_name}}_address_fact_city').val(suggestion.data.address.data.city);
                $('#{{$subject_name}}_address_fact_city_kladr_id').val(suggestion.data.address.data.city_kladr_id);
                $('#{{$subject_name}}_address_fact_settlement').val(suggestion.data.address.data.settlement);
                $('#{{$subject_name}}_address_fact_settlement_kladr_id').val(suggestion.data.address.data.settlement_kladr_id);
                $('#{{$subject_name}}_address_fact_street').val(suggestion.data.address.data.street_with_type);
                $('#{{$subject_name}}_address_fact_house').val(suggestion.data.address.data.house);
                $('#{{$subject_name}}_address_fact_block').val(suggestion.data.address.data.block);
                $('#{{$subject_name}}_address_fact_flat').val(suggestion.data.address.data.flat);
                $('#{{$subject_name}}_address_fact_okato').val(suggestion.data.address.data.okato);
                $('#{{$subject_name}}_address_fact_zip').val(suggestion.data.address.data.postal_code);
                $('#{{$subject_name}}_address_fact_city_fias_id').val(suggestion.data.address.data.city_fias_id);

                // address register
                $('#{{$subject_name}}_address_register').val(suggestion.data.address.value);
                $('#{{$subject_name}}_address_register_kladr').val(suggestion.data.address.data.kladr_id);
                $('#{{$subject_name}}_address_register_region').val(suggestion.data.address.data.region);
                $('#{{$subject_name}}_address_register_region_full').val(suggestion.data.address.data.region_with_type);
                $('#{{$subject_name}}_address_register_city').val(suggestion.data.address.data.city);
                $('#{{$subject_name}}_address_register_city_kladr_id').val(suggestion.data.address.data.city_kladr_id);
                $('#{{$subject_name}}_address_register_settlement').val(suggestion.data.address.data.settlement);
                $('#{{$subject_name}}_address_register_settlement_kladr_id').val(suggestion.data.address.data.settlement_kladr_id);
                $('#{{$subject_name}}_address_register_street').val(suggestion.data.address.data.street_with_type);
                $('#{{$subject_name}}_address_register_house').val(suggestion.data.address.data.house);
                $('#{{$subject_name}}_address_register_block').val(suggestion.data.address.data.block);
                $('#{{$subject_name}}_address_register_flat').val(suggestion.data.address.data.flat);
                $('#{{$subject_name}}_address_register_okato').val(suggestion.data.address.data.okato);
                $('#{{$subject_name}}_address_register_zip').val(suggestion.data.address.data.postal_code);
                $('#{{$subject_name}}_address_register_city_fias_id').val(suggestion.data.address.data.city_fias_id);

                if(data.management && data.management.name){
                    $('#{{$subject_name}}_general_manager').val(data.management.name);
                }

            }
        });


        formatDate();

        $('.phone').mask('+7 (999) 999-99-99');
    }

    $(document).on('keyup', '#{{$subject_name}}_fio', function (e) {

        if(!hasDadataFio('{{$subject_name}}')){
            $('#{{$subject_name}}_first_name').val('');
            $('#{{$subject_name}}_second_name').val('');
            $('#{{$subject_name}}_last_name').val('');
        }
    });


    function hasDadataFio(subject_type) {
        var fio = $('#'+subject_type+'_fio').val();
        var first_name = $('#'+subject_type+'_first_name').val();
        var second_name = $('#'+subject_type+'_second_name').val();
        var last_name = $('#'+subject_type+'_last_name').val();

        if (fio.includes(first_name) && fio.includes(second_name) && fio.includes(last_name)){
            return true;
        }

        return  false;
    }


    function {{$subject_name}}FioValidate() {

        var needValidate = true;

        if ('{{$subject_name}}' == 'insurer'){
            var subjectInMessage = 'страхователя';
        } else{
            var subjectInMessage = 'владельца';

            if ($('#owner_is_insurer').prop('checked')){
                needValidate = false;
            }
        }

        if (!needValidate){
            return true;
        }

        var first_name = $('#{{$subject_name}}_first_name').val().length > 0;
        var second_name = $('#{{$subject_name}}_second_name').val().length > 0;
        var last_name = $('#{{$subject_name}}_last_name').val().length > 0;

        if ((first_name && second_name && last_name)) {
            return true;
        }

        flashHeaderMessage('ФИО '+subjectInMessage+' заполнено некорректно! Выберите вариант ФИО из подсказок.', 'danger');

        return false;
    }

</script>

