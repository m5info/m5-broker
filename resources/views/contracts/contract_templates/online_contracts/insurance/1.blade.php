<div class="block-view">
    <h3>Страховая компания {{
    $contract->selected_calculation &&
    $contract->selected_calculation->bso_supplier &&
    $contract->selected_calculation->bso_supplier->insurance ?
    $contract->selected_calculation->bso_supplier->insurance->title : "" }}</h3>
    <div class="row">
        @if($contract->selected_calculation)

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Поставщик</span>
                <span class="view-value">
                    {{ $contract->selected_calculation &&
                    $contract->selected_calculation->bso_supplier ?
                    $contract->selected_calculation->bso_supplier->title : "" }}
                </span>
            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Страховая сумма</span>
                <span class="view-value">{{ $contract->selected_calculation->sum ? titleFloatFormat($contract->selected_calculation->sum) : "" }}</span>
            </div>
        </div>



        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Комиссионное вознаграждение</span>
                <span class="view-value">
                    {{titleFloatFormat($contract->financial_policy_kv_agent)}} % / {{titleFloatFormat(getTotalSumToPrice($contract->selected_calculation->sum, $contract->financial_policy_kv_agent))}}
                </span>
            </div>
        </div>



        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <span >
                {{ $contract->selected_calculation &&
                $contract->selected_calculation->bso_supplier ?
                $contract->selected_calculation->tariff : "" }}
            </span>
        </div>

        @endif

    </div>
</div>
