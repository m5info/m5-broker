<div class="block-view">
    <h3>Пролонгация</h3>
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Это пролонгация</span>
                <span class="view-value">{{$contract->is_prolongation ? 'Да' : 'Нет'}}</span>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Номер полиса</span>
                <span class="view-value">{{ $contract->prev_policy_number ? $contract->prev_policy_number : 'Отсутствует'}}</span>
            </div>
        </div>

    </div>
</div>
