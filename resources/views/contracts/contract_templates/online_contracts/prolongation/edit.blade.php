<div class="block-view">
    <h3>Пролонгация
        {{ Form::checkbox("contract[is_prolongation]", 1, $is_prolongation, ['onclick' => "loadProlongationInfoClick()", 'data-active' => $is_prolongation]) }} <span style="font-size: 13px;"></span>
    </h3>
    <div class="row" id="prolongation_inner">
        {{--resources/views/contracts/contract_templates/online_contracts/prolongation/partials.inner--}}
    </div>
</div>

<script>

    var is_prolongation = "{{$contract->is_prolongation}}";

    function loadProlongationInfo()
    {
        $.get('{{url("/contracts/online/action/".(int)$contract->id)}}/get-prolongation', {
            is_prolongation: is_prolongation
        }, function(res){
            $('#prolongation_inner').html(res);

            $('.select2-all').select2("destroy").select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true
            });
        });
    }

    function loadProlongationInfoClick()
    {
        var temp = $('[name="contract[is_prolongation]"]').data('active');

        if (temp == 1){
            $('[name="contract[is_prolongation]"]').data('active', 0);
        } else{
            $('[name="contract[is_prolongation]"]').data('active', 1);
        }

        is_prolongation = $('[name="contract[is_prolongation]"]').data('active');
        loadProlongationInfo();
    }

    $(function(){
        loadProlongationInfo();
    })

</script>




