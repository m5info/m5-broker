<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
    <label for="contract[prev_policy_serie]" class="control-label">Серия полиса</label>
    <input type="text" name="contract[prev_policy_serie]" class="form-control"  value="{{$prev_policy_serie}}">
</div>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
    <label for="contract[prev_policy_number]" class="control-label">Номер полиса</label>
    <input type="text" name="contract[prev_policy_number]" class="form-control"  value="{{$prev_policy_number}}">
</div>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
    <label class="control-label">СК</label>
    {{Form::select("contract[prev_sk_id]", \App\Models\Dictionaries\DictionarySk::all()->pluck('title', 'id')->prepend('Не выбрано', 0), $prev_sk_id, ['class' => 'prev_sk_id form-control select2-all', "id"=>"prev_sk_id_0", 'style'=>'width: 100%;'])}}
</div>