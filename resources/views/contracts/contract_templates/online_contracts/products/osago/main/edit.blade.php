<div class="row">
    <div class="col-md-4 col-lg-4">
        <label class="control-label pull-left">Электронный договор
            {{ Form::checkbox('contract[bso_is_epolicy]', 1, $contract->is_epolicy == 1 ? true : false, ['id' => 'bso_is_epolicy']) }}
        </label>
    </div>
</div>
{{--Срок и период страхования--}}
@include('contracts.contract_templates.online_contracts.terms.edit')

{{--Пролонгация--}}
@include('contracts.contract_templates.online_contracts.prolongation.edit', [
    'is_prolongation' => $contract->is_prolongation,
])

{{--Участники договора--}}
{{--Страхователь--}}
@include('contracts.contract_templates.online_contracts.subject.edit', [
    'subject_title' => 'Страхователь',
    'subject_name' => 'insurer',
    'subject' => (isset($contract->insurer)?$contract->insurer:new \App\Models\Contracts\Subjects())
])

{{--Собственник--}}
@include('contracts.contract_templates.online_contracts.subject.edit', [
   'subject_title' => 'Собственник',
   'subject_name' => 'owner',
   'is_insurer' => (isset($contract->owner) && isset($contract->insurer) && $contract->insurer == $contract->owner)?1:0,
   'subject' => (isset($contract->owner)?$contract->owner:new \App\Models\Contracts\Subjects())
])

{{--Водители--}}
@include('contracts.contract_templates.online_contracts.drivers.edit', [
    'drivers' => $contract->drivers,
    'contract' => $contract
])


{{--Транспортное средство--}}
@include('contracts.contract_templates.online_contracts.insurance_object.auto.osago.edit', [
    'object'=>(isset($contract->object_insurer_auto))?$contract->object_insurer_auto:new \App\Models\Contracts\ObjectInsurerAuto()
])


<script src="/plugins/jquery/jquery.min.js"></script>
@include('contracts.contract_templates.online_contracts.products.osago.js', [
    'contract_id' => isset($contract) && isset($contract->id) ? $contract->id : 0
])

<div class="block-view">
    <h3>
        Результат рассчета

        <span class="btn btn-success btn-left" onclick="saveContract({{$contract->id}});">Сохранить</span>

        <span class="btn btn-primary pull-right" onclick="get_calculate_sk({{$contract->id}});">Рассчитать</span>

    </h3>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="offers">
            @include('contracts.contract_templates.online_contracts.tariff.insurance_price', ['online_calculations'=>$contract->online_calculations])
        </div>

    </div>
</div>


