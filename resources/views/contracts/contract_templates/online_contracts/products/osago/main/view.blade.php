<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">


    {{--Поставщик, название страховой и страховая сумма--}}
    @include('contracts.contract_templates.online_contracts.insurance.1', [
        'contract' => $contract
    ])

    {{--Пролонгация--}}
    @include('contracts.contract_templates.online_contracts.prolongation.view')

    {{--Срок и период страхования--}}
    @include('contracts.contract_templates.online_contracts.terms.view')

    {{--Участники договора--}}
    {{--Страхователь--}}
    @include('contracts.contract_templates.online_contracts.subject.view', [
        'subject_title' => 'Страхователь',
        'subject_name' => 'insurer',
        'subject' => (isset($contract->insurer)?$contract->insurer:new \App\Models\Contracts\Subjects())
    ])

    {{--Собственник--}}
    @include('contracts.contract_templates.online_contracts.subject.view', [
       'subject_title' => 'Собственник',
       'subject_name' => 'owner',
       'is_insurer' => (isset($contract->owner) && isset($contract->insurer) && $contract->insurer == $contract->owner)?1:0,
       'subject' => (isset($contract->owner)?$contract->owner:new \App\Models\Contracts\Subjects())
    ])

    {{--Водители--}}
    @include('contracts.contract_templates.online_contracts.drivers.view', [
        'drivers' => $contract->drivers,
        'contract' => $contract
    ])


    {{--Транспортное средство--}}
    @include('contracts.contract_templates.online_contracts.insurance_object.auto.osago.view', [
        'object'=>(isset($contract->object_insurer_auto))?$contract->object_insurer_auto:new \App\Models\Contracts\ObjectInsurerAuto()
    ])
</div>


<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    <script src="/plugins/jquery/jquery.min.js"></script>

    @php
        $api_setting = $contract->bso_supplier->api_setting;
        $calculation = $contract->calculation;
    @endphp

    @if($api_setting->sms_confirmation == 1 && $calculation->sms_verify == 0)
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h3>СМС-подтверждение
                <button class="btn btn-primary pull-right" onclick="return_calc();"> Вернуться в расчет</button>
            </h3>
            <p>Для дальнейшего оформления полиса введите код, пришедший на номер страхователя</p>
            @if($api_setting->custom_realization)
                <p class="mt-15">Если не пришел СМС-код, отправьте повторно через <code id="counter_sms">10</code>
                    сек.
                    <button class="btn btn-success inline-block" id="repeat_sms_send"
                            onclick="confirmSMSCode('{{$api_setting->dir_name}}','{{$calculation->product->slug}}', {{$calculation->id}});"
                            disabled><i class="fa fa-refresh"></i></button>
                </p>
            @else
                <p class="mt-15">Если не пришел СМС-код, отправьте повторно через <code id="counter_sms">10</code>
                    сек.
                    <button class="btn btn-warning inline-block" id="repeat_sms_send"
                            onclick="confirmSMSCode({{$calculation->id}});" disabled><i class="fas fa-refresh"></i>
                    </button>

                </p>
            @endif
            <div class="row mt-15">
                <div class="col-lg-4">
                    <div id="sms_confirm">
                        @if($api_setting->custom_realization)
                            <button type="button" class="form-control-half inline-block btn btn-success" style="width: 120px;"
                                    onclick="confirmSMSCode('{{$api_setting->dir_name}}','{{$calculation->product->slug}}', {{$calculation->id}});">
                                Подтвердить
                            </button>
                        @else
                            <button type="button" class="form-control-half inline-block btn btn-success" style="width: 120px;"
                                    onclick="confirmSMSCode({{$contract->id}});">
                                Подтвердить
                            </button>

                            <script>
                                function confirmSMSCode(contract_id) {
                                    $.post('/contracts/online/'+contract_id+'/confirm_sms', {}, function (response) {
                                        if (response === false){
                                            flashMessage('danger', 'Ошибка валидации. Вернитесь в расчет и пересчитайте.');
                                        }else{
                                            $('#sms_verify').show();
                                            flashMessage('success', 'Запрос отправлен, ожидайте СМС с кодом');
                                        }
                                    });
                                }
                            </script>
                        @endif
                    </div>
                    <div id="sms_verify" style="display: none;">
                        {{Form::input('text','sms_code', '', ['class' => 'form-control-half inline-block text-center', 'placeholder' => '9647'])}}
                        <button type="button" class="form-control-half inline-block btn btn-success"
                                id="send_sms_code_button" onclick="sendSMSCode('{{$api_setting->dir_name}}','{{$calculation->product->slug}}', {{$calculation->id}}, {{$contract->id}});">
                            Отправить
                        </button>

                    </div>
                    <div class="loader_div inline-block hidden" id="loader_div_1"></div>
                </div>
            </div>
        </div>


        @php
            $api_setting = $contract->bso_supplier->api_setting;
            $calculation = $contract->selected_calculation;
        @endphp

        @if($api_setting->custom_realization)

            <script src="/js/autobuhn.min.0.8.3.js"></script>
            <script>
                pusher_uri = '{{config('app.pusher_integration_uri_outer')}}';
                socker_port = '{{config('app.integration_port')}}';
                protocol = '{{config('app.protocol_websocket')}}';
            </script>
            <script src="/js/sk/{{$api_setting->dir_name}}/{{$calculation->product->slug}}/calc.js"></script>

        @else
            <script>
                function sendSMSCode() {
                    $.post("/contracts/online/{{$contract->id}}/confirm_sms", {sms_code: $('[name="sms_code"]').val()}, function (res) {
                        if (res.state === true) {

                            flashMessage('success', 'Код подтвержден!');
                            setTimeout(function () {
                                location.reload()
                            }, 1000);

                        } else {
                            flashMessage('danger', res.error);
                        }
                    });
                }

            </script>
        @endif

        <script>
            function return_calc() {
                $.post('{{ url("/contracts/online/{$contract->id}/return_calculation/") }}')
            }
        </script>


    @endif

    @if($contract->selected_calculation && $contract->selected_calculation->statys_id == 2 && ( $contract->bso_supplier->api_setting->sms_confirmation == 0 || $contract->selected_calculation->sms_verify == 1 ))


        <h3>Подтверждение оплаты</h3>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row form-horizontal">

                {{ Form::open(['method' => 'post', 'class' => 'form-horizontal', 'id' => 'payment_confirm_form']) }}

                <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label pull-left">
                        БСО <span style="color:red">*</span>
                    </label>
                    @php
                        $bso_is_epolicy = false;
                        $bso_is_only_one_option = false;
                        $pay_methods_access_bso = \App\Models\Settings\BsoSupplierProductPayMethodAccessBso::query()
                        ->where('product_id', $contract->product_id)
                        ->where('bso_supplier_id', $contract->bso_supplier_id)->first();

                        if($pay_methods_access_bso){ // если настроено
                            if($access_bso = (int)$pay_methods_access_bso->access_bso){ // если не все
                                if($access_bso === 2){ // ешка
                                    $bso_is_epolicy = true;
                                }
                                $bso_is_only_one_option = true;
                            }else{
                                if($contract->is_epolicy){
                                    $bso_is_epolicy = true;
                                }
                            }
                        } else {
                            if($contract->is_epolicy){
                                $bso_is_epolicy = true;
                            }
                        }
                    @endphp
                    <label class="control-label pull-right {{$bso_is_only_one_option ? 'hidden' : ''}}">Электронный
                        {{ Form::checkbox('contract[bso_is_epolicy]', 1, $bso_is_epolicy, ['id' => 'bso_is_epolicy', 'onchange' => 'viewSetBsoIsPolicy()']) }}
                    </label>
                    @if($contract->sms_verify)
                        <script>
                            $('[name="contract[bso_is_epolicy]"]').change();
                        </script>
                    @endif
                    {{ Form::text('contract[bso_title]', '', ['class' => 'form-control', 'data-hidden' => 'bso_id', 'id'=>'bso_title', $contract->is_epolicy == 1 ? 'disabled' : '']) }}
                    <input type="hidden" name="contract[bso_id]" id="bso_id"/>
                </div>

                <!-- Метод оплаты -->
                @include("contracts.contract_templates.default.payment_method.edit")

                {{ Form::close() }}

            </div>
        </div>



        <script>

            $(function () {
                search_bso($('[name="contract[bso_title]"]'), 1);
                search_bso($('[name="contract[bso_receipt]"]'), 2);
                selectViewPaymentMethod();
                viewSetBsoIsPolicy();


                /*
                search_bso($('[name="contract[bso_receipt]"]'), 2);


                $(document).on('change', '#bso_id, #bso_receipt_id', function(){
                    if($('#bso_id').val() && $('#bso_receipt_id').val()){
                        $('#pay').show();
                    }else{
                        $('#pay').hide();
                    }
                });
                */


            });

            function viewSetBsoIsPolicy() {

                if ($("#bso_is_epolicy").is(':checked')) {
                    $("#bso_title").attr('disabled', 'disabled');
                } else {
                    $("#bso_title").removeAttr('disabled');
                }
            }


            function payment_confirm_submit() {
                var data = {};

                payment_method = $('#payment_method').val();
                key_type = $('.payment_method_type_' + payment_method).data("key_type");
                bso_is_epolicy = $("#bso_is_epolicy").is(':checked') ? 1 : 0;


                switch (parseInt(key_type)) {
                    case 0:
                        data = {
                            'payment_method': payment_method,
                            'bso_is_epolicy': bso_is_epolicy,
                            'bso_receipt_id': $('#bso_receipt_id').val(),
                            'bso_id': $('#bso_id').val(),
                        };
                        break;

                    case 1:
                        data = {
                            'payment_method': payment_method,
                            'bso_is_epolicy': bso_is_epolicy,
                            'send_email': $('#send_email_' + payment_method).val(),
                        };
                        break;

                    case 2:
                        data = {
                            'payment_method': payment_method,
                            'bso_is_epolicy': bso_is_epolicy,
                            'send_email': $('#send_email_' + payment_method).val(),
                            'bso_id': $('#bso_id').val(),
                        };
                        break;

                    case 3:
                        data = {
                            'payment_method': payment_method,
                            'bso_is_epolicy': bso_is_epolicy,
                            'bso_id': $('#bso_id').val(),
                        };
                        break;
                    case 4:
                        data = {
                            'payment_method': payment_method,
                            'bso_is_epolicy': bso_is_epolicy,
                            'send_email': $('#send_email_' + payment_method).val(),
                            'bso_id': $('#bso_id').val(),
                        };
                        break;

                }


                loaderShow();

                $.post('{{ url("/contracts/online/{$contract->id}/payment_confirm/") }}', data, function (response) {
                    loaderHide();
                    if (response.state == true) {
                        reload();
                    } else {
                        flashMessage('danger', response.error);
                    }

                }).done(function () {
                    loaderShow();
                })
                    .fail(function () {
                        loaderHide();
                    })
                    .always(function () {
                        loaderHide();
                    });


            }

            function return_calc() {
                $.post('{{ url("/contracts/online/{$contract->id}/return_calculation/") }}', {}, function (res) {
                    reload();
                    //location.href = "/contracts/online/edit/{{ $contract->id }}"
                })
            }

            function search_bso(input, type = null, class_bso) {

                var hid_id = input.attr('data-hidden');
                $('#' + hid_id).val('').change();

                input.suggestions({
                    serviceUrl: "/bso/actions/get_bso/",
                    type: "PARTY",
                    params: {
                        type_bso: type,
                        bso_supplier_id: parseInt('{{ $contract->selected_calculation ? $contract->selected_calculation->bso_supplier_id : 0 }}'),
                        bso_agent_id: parseInt('{{ $contract->agent_id }}'),
                        bso_used: []
                    },
                    count: 5,
                    minChars: 3,
                    formatResult: function (e, t, n, i) {
                        var s = this;
                        var title = n.value;
                        var bso_type = n.data.bso_type;
                        var bso_sk = n.data.bso_sk;
                        var agent_name = n.data.agent_name;

                        var view_res = title;
                        view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">СК</span>' + bso_sk + "</div>";
                        view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Тип</span>' + bso_type + "</div>";
                        view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Агент</span>' + agent_name + "</div>";

                        return view_res;
                    },
                    onSelect: function (suggestion) {
                        $('#' + hid_id).val(suggestion.data.bso_id).change()
                    }
                });

            }


            function selectViewPaymentMethod() {

                payment_method = $("#payment_method").val();
                $(".payment_method_type").hide();
                $(".payment_method_type_" + payment_method).show();

                //search_bso($('[name="contract[bso_receipt]"]'), 2);


            }


        </script>

    @elseif($contract->selected_calculation && $contract->selected_calculation->statys_id == 3)

    <!-- Договор на согласовании или на оплате -->
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h3>Ожидание оплаты</h3>
            <span class="btn btn-primary pull-left" onclick="check_status();">Проверить статус</span>
        </div>
        <script>
            function check_status() {
                loaderShow();

                $.post('{{ url("contracts/online/{$contract->id}/check_status") }}', {}, function (res) {
                    if (res.state){
                        reload();
                    } else{
                        flashHeaderMessage(res.error, 'danger')
                    }
                }).always(function () {
                    loaderHide();
                })
            }
        </script>


    @elseif($contract->selected_calculation && $contract->selected_calculation->statys_id == 4)



    <!-- Информация по договору -->
        @include("contracts.contract_templates.default.contract.view")

    <!-- Платежи -->
        @include("contracts.contract_templates.default.payments.view")

    <!-- Документы -->
        @include("contracts.contract_templates.default.documents.edit")

    @endif


</div>


