<script>

    var contract_id = '{{isset($contract_id) ? $contract_id : 0}}';
    $(function(){
        init();
    });


    function init(){

        $('#driver_type_checkbox').change(function(){
            if (parseInt($(this).attr('data-value')) === 0) {
                $('#driver_type_checkbox').attr('data-value', 1);
                $('#contract_driver_type').attr('value',1);
                $('.driver_limited').hide();
                $('.drivers_count').hide();
                $('.driver_unlimited').show();
                $('.driver').hide();
            }
            else if (parseInt($(this).attr('data-value')) === 1) {
                $('#driver_type_checkbox').attr('data-value', 0);
                $('#contract_driver_type').attr('value',0);
                $('.driver_limited').show();
                $('.drivers_count').show();
                $('.driver_unlimited').hide();
                $('.driver').show();

                if ($('#drivers_count').val() < 1){
                    addNewDriver();
                }

            }
        });

        /* Оставлено на случай, если где-то еще используется */

        $('#driver_type').change(function () {
            if (parseInt($(this).val()) == 0) {
                $('.driver_limited').show();
                $('.drivers_count').show();
                $('.driver_unlimited').hide();
                $('.driver').show();

            } else if (parseInt($(this).val()) == 1) {
                $('.driver_limited').hide();
                $('.drivers_count').hide();
                $('.driver_unlimited').show();
                $('.driver').hide();
            }

        });

        /*$('#driver_type_checkbox').change();
        $('#driver_type').change();
        $('#drivers_count').change();*/

        if (parseInt($('#driver_type_checkbox').attr('data-value')) === 0){
            $('.driver_limited').show();
        } else if(parseInt($('#driver_type_checkbox').attr('data-value')) === 1){
            $('.driver').hide();
            $('.driver_unlimited').show();
        }

        /*$('#object_ts_mark_id').change();*/




        var im = new Inputmask("9999", {"yearrange": {minyear: 1900, maxyear: 2099}});
        im.mask($('#carYear'));
        if($('.driver').length === 0 && parseInt($('#driver_type_checkbox').attr('data-value')) === 0){
            addNewDriver();
        }

        setTimeout(drivers_same_insurer, 200);
        setTimeout(initCar, 30);


        setTimeout(activValidDrivers, 300);

    }





    function getModelsObjectInsurer(KEY, select_model_id)
    {

        $.getJSON('{{url("/contracts/actions/get_models")}}', {categoryId: $('#object_ts_category_' + KEY).val(), markId: $('#object_ts_mark_id_' + KEY).select2('val')}, function (response) {

            var options = "<option value='0'>Не выбрано</option>";
            response.map(function (item) {
                options += "<option value='" + item.id + "'>" + item.title + "</option>";
            });
            $('#object_ts_model_id_' + KEY).html(options).select2('val', select_model_id);


        });

        let current = parseInt($('#object_ts_category_' + KEY).val());

        if (current == 3) {
            $('#object_weight_' + KEY).show();
            $('#object_capacity_' + KEY).show();
            $('#object_passengers_count_' + KEY).hide();
        } else if (current == 4) {
            $('#object_weight_' + KEY).hide();
            $('#object_capacity_' + KEY).hide();
            $('#object_passengers_count_' + KEY).show();
        } else {
            $('#object_weight_' + KEY).hide();
            $('#object_capacity_' + KEY).hide();
            $('#object_passengers_count_' + KEY).hide();
        }




    }

    function removeDriver(i) {

        driver_id = $('[name="contract[driver]['+i+'][id]"]').val();

        if(driver_id && parseInt(driver_id) > 0){
            $.post("/contracts/online/delete_driver", {driver_id: driver_id}, function (response) {

                delDtiver(i);

            });
        }else{
            delDtiver(i);
        }

    }

    function delDtiver(i) {
        $('.driver-' + i).remove();
        $('.driver-title').each(function (key, item) {
            key += 1;
            $(item).html("Водитель " + key);
        });
    }


    function addNewDriver() {
        let driverCount = parseInt($('#drivers_count').val());
        driverCount++;
        $('#drivers_count').val(driverCount);

        $.ajax({
            type: "GET",
            url: "{{url('/contracts/online/driver_form/')}}" + '/' + driverCount + '?view_type={{$view_type}}',
            async: false,
            success: function (response) {
                $(response).insertBefore(".driver_limited");
                $('.driver-title').each(function (key, item) {
                    key += 1;
                    $(item).html("Водитель " + key);
                });
                formatTime();
                formatDate();

                $('#driver_fio_' + driverCount).suggestions({
                    serviceUrl: DADATA_AUTOCOMPLETE_URL,
                    token: DADATA_TOKEN,
                    type: "NAME",
                    count: 5,
                    onSelect: function (suggestion) {
                        key = $(this).data('key');
                        $('#driver_fio_' + key).val($(this).val());
                        $('#driver_first_name_'+ key).val(suggestion.data.name);
                        $('#driver_second_name_'+ key).val(suggestion.data.patronymic);
                        $('#driver_last_name_'+ key).val(suggestion.data.surname);
                    }
                });

                activeKBMDriver(driverCount);



            }
        });

        return true;

    }

    $(document).on('keyup', '[id*="driver_fio_"]', function (e) {

        var driver_key = $(this).data('key');

        if(!driverHasDadataFio(driver_key)){
            $('#driver_first_name_'+ driver_key).val('');
            $('#driver_second_name_'+ driver_key).val('');
            $('#driver_last_name_'+ driver_key).val('');
        }
    });


    function driverHasDadataFio(driver_key) {
        var fio = $('#driver_fio_'+ driver_key).val();
        var first_name = $('#driver_first_name_'+ driver_key).val();
        var second_name = $('#driver_second_name_'+ driver_key).val();
        var last_name = $('#driver_last_name_'+ driver_key).val();

        var sumLenght = first_name.length + second_name.length + last_name.length + 2;

        if (fio.includes(first_name) && fio.includes(second_name) && fio.includes(last_name) && sumLenght === fio.length){
            return true;
        }

        return  false;
    }

    function join(arr /*, separator */) {
        var separator = arguments.length > 1 ? arguments[1] : ", ";
        return arr.filter(function (n) {
            return n
        }).join(separator);
    }

    function formatCity(suggestion) {
        var address = suggestion.data;
        if (address.city_with_type === address.region_with_type) {
            return address.settlement_with_type || "";
        } else {
            return join([
                address.city_with_type,
                address.settlement_with_type]);
        }
    }



    function drivers_same_insurer(){

        $('[id*="driver_fio"]').prop('disabled', false);
        $('[id*="driver_birthdate"]').prop('disabled', false);
        $('[id*="driver_sex"]').prop('disabled', false);

        if(event === undefined){
            $.each($('[name*="same_as_insurer"]'), function(k,v){
                toggle_driver_same_insurer(v)
            })
        }else{
            toggle_driver_same_insurer(event.target)
        }

    }

    function driversFioValidate() {

        var checkResults = [];

        $.each($('.driver-form'), function (k, v) {
            var driver_key = $(this).data('key');

            var fio = $('#driver_fio_'+driver_key).val();
            var first_name = $('#driver_first_name_'+driver_key).val();
            var second_name = $('#driver_second_name_'+driver_key).val();
            var last_name = $('#driver_last_name_'+driver_key).val();

            var sumLenght = first_name.length + second_name.length + last_name.length + 2;

            if (((fio.includes(first_name) && fio.includes(second_name) && fio.includes(last_name) && sumLenght === fio.length)) || $(this).css('display') === 'none') {
                checkResults.push(true);

            }else{
                checkResults.push(false);
                flashHeaderMessage('ФИО водителя №'+driver_key+' заполнено некорректно! Выберите вариант ФИО из подсказок.', 'danger');
            }
        });

        return $.inArray(false, checkResults) !== 1;
    }

    function toggleForeignDriverDocs(event){

        var target = event.target;
        var key = $(target).data('key');

        if ($(this).prop('checked')){
            $('#driver_doc_serie_' + key).removeClass('ru_sumb').removeClass('red_input').addClass('en_sumb');
            $('#driver_doc_num_' + key).removeClass('ru_sumb').removeClass('red_input').addClass('en_sumb');
        } else {
            $('#driver_doc_serie_' + key).removeClass('en_sumb').removeClass('red_input').addClass('ru_sumb');
            $('#driver_doc_num_' + key).removeClass('en_sumb').removeClass('red_input').addClass('ru_sumb');
        }

    }

    function toggle_driver_same_insurer(target){

        target = $(target);
        var key = target.attr('data-key');



        if(target.prop('checked')){
            $('[name*="same_as_insurer"]').prop('checked', false);
            target.prop('checked', true);

            $('#driver_fio_' + key).val($('#insurer_fio').val()).change();
            $('#driver_first_name_' + key).val($('#insurer_first_name').val()).change();
            $('#driver_second_name_' + key).val($('#insurer_second_name').val()).change();
            $('#driver_last_name_' + key).val($('#insurer_last_name').val()).change();
            $('#driver_birthdate_' + key).val($('#insurer_birthdate').val()).change();
            $('#driver_sex_' + key).val(parseInt($('#insurer_sex').val())).change();

            $('#driver_fio_' + key).prop('disabled', true);
            $('#driver_birthdate_' + key).prop('disabled', true);
            $('#driver_sex_' + key).prop('disabled', true);

            toggle_driver_same_insurer();

            saveContract(contract_id, true)
        }
    }


    function activeKBMDriver(i)
    {
        $('.driver-' + i).each(function() {
            $(this).change(function() {
                getKBMDriver(i);
            });
        });
    }


    function getKBMDriver(i){

        var data = {
            fio:$('[name="contract[driver]['+i+'][fio]"]').val(),
            birth_date:$('[name="contract[driver]['+i+'][birth_date]"]').val(),
            sex:$('[name="contract[driver]['+i+'][sex]"]').val(),
            doc_serie:$('[name="contract[driver]['+i+'][doc_serie]"]').val(),
            doc_num:$('[name="contract[driver]['+i+'][doc_num]"]').val(),
            doc_date:$('[name="contract[driver]['+i+'][doc_date]"]').val(),
            exp_date:$('[name="contract[driver]['+i+'][exp_date]"]').val(),
            foreign_docs:$('[name="contract[driver]['+i+'][foreign_docs]"]').val()
        };

        //Запрос на КБМ
        $.ajax({
            url: "/contracts/online/get_kbm",
            async: true,
            type: 'POST',
            data: {data: data},
            success: function (response) {
                $('[name="contract[driver][' + i + '][kbm]"]').val(response.kbm);
                $('[name="contract[driver][' + i + '][class_kbm]"]').val(response.class_kbm);
                $('[name="contract[driver][' + i + '][kbm_rsa_request_id]"]').val(response.rsa_request_id);
            }
        });

    }





</script>

