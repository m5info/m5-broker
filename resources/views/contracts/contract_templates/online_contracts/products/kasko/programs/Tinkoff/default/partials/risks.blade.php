<div class="block-view">
    <h3>Риски - {{$contract->getProductOrProgram()->title}}</h3>
    <div class="row">

        <div class="col-md-12 col-lg-12" >

            @include("contracts.contract_templates.online_contracts.products.kasko.programs.Tinkoff.default.partials.risks.{$contract->getProductOrProgram()->api_name}")

        </div>

    </div>

</div>

