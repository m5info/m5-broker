<script>

    var equipment = 1;

    $(function () {

        init();

    });


    function init() {


        $('#driver_type_checkbox').change(function () {
            if (parseInt($(this).val()) == 0) {
                $('#driver_type_checkbox').val(1);
                $('#contract_driver_type').attr('value', 1);
                $('.driver_limited').hide();
                $('.drivers_count').hide();
                $('.driver_unlimited').show();
                $('.driver').hide();
            } else if (parseInt($(this).val()) == 1) {
                $('#driver_type_checkbox').val(0);
                $('#contract_driver_type').attr('value', 0);
                $('.driver_limited').show();
                $('.drivers_count').show();
                $('.driver_unlimited').hide();
                $('.driver').show();
            }
        });

        $('#driver_type').change(function () {
            if (parseInt($(this).val()) == 0) {
                $('.driver_limited').show();
                $('.drivers_count').show();
                $('.driver_unlimited').hide();
                $('.driver').show();

            } else if (parseInt($(this).val()) == 1) {
                $('.driver_limited').hide();
                $('.drivers_count').hide();
                $('.driver_unlimited').show();
                $('.driver').hide();
            }

        });

        $('#driver_type_checkbox').change();
        $('#driver_type').change();
        $('#drivers_count').change();
        /*$('#object_ts_mark_id').change();*/


        var im = new Inputmask("9999", {"yearrange": {minyear: 1900, maxyear: 2099}});
        im.mask($('#carYear'));


        if ($('.driver').length === 0) {
            addNewDriver();
        }

        setTimeout(drivers_same_insurer, 20);
        setTimeout(initCar, 30);


        activValidDrivers();

        var object_insurer_id = $('#object_id').val();

        if (object_insurer_id){
            getAllEquipment(object_insurer_id);
        }

    }

    function getAllEquipment(object_insurer_id){
        res = myPostAjax('/contracts/online/optional_equipment/all', 'object_insurer_id='+object_insurer_id);
        $('#optional_equipment').html(res);
    }

    function addEquipment() {

        var object_insurer_id = $('#object_id').val();

        var index = 1;

        index = $('.optional-equipment-form').length + 1;

        if (index !== 1){
            equipment = index;
        }


        $.post('/contracts/online/optional_equipment/', {
            object_insurer_id: object_insurer_id,
            i: equipment
        }, function (res) {

            if ($('#optional_equipment').length !== 0){
                $('#optional_equipment').append(res);
            }else{
                $('#optional_equipment').html(res);
            }
            equipment++;
        })
    }

    function removeEquipment(i, id) {
        $('#optional_equipment_part_' + i).remove();
        equipment--;
    }


    function getModelsObjectInsurer(KEY, select_model_id) {

        $.getJSON('{{url("/contracts/actions/get_models")}}', {
            categoryId: $('#object_ts_category_' + KEY).val(),
            markId: $('#object_ts_mark_id_' + KEY).select2('val')
        }, function (response) {

            var options = "<option value='0'>Не выбрано</option>";
            response.map(function (item) {
                options += "<option value='" + item.id + "'>" + item.title + "</option>";
            });
            $('#object_ts_model_id_' + KEY).html(options).select2('val', select_model_id);


        });

        var current = parseInt($('#object_ts_category_' + KEY).val());

        if (current == 3) {
            $('#object_weight_' + KEY).show();
            $('#object_capacity_' + KEY).show();
            $('#object_passengers_count_' + KEY).hide();
        } else if (current == 4) {
            $('#object_weight_' + KEY).hide();
            $('#object_capacity_' + KEY).hide();
            $('#object_passengers_count_' + KEY).show();
        } else {
            $('#object_weight_' + KEY).hide();
            $('#object_capacity_' + KEY).hide();
            $('#object_passengers_count_' + KEY).hide();
        }


    }

    function getClassificationObjectInsurer(KEY, select_classification_id) {
        $.getJSON('{{url("/contracts/actions/classification")}}', {
            categoryId: $('#object_ts_category_' + KEY).val()
        }, function (response) {


            var options = "<option value='0'>Не выбрано</option>";
            response.map(function (item) {
                options += "<option value='" + item.id + "'>" + item.title + "</option>";
            });
            $('#classification_id_' + KEY).html(options).select2('val', select_classification_id);


        });
    }

    function removeDriver(i) {

        driver_id = $('[name="contract[driver][' + i + '][id]"]').val();

        if (driver_id && parseInt(driver_id) > 0) {
            $.post("/contracts/online/delete_driver/", {driver_id: driver_id}, function (response) {

                delDtiver(i);

            });
        } else {
            delDtiver(i);
        }

    }

    function delDtiver(i) {
        $('.driver-' + i).remove();
        $('.driver-title').each(function (key, item) {
            key += 1;
            $(item).html("Водитель " + key);
        });
    }


    function addNewDriver() {
        let driverCount = parseInt($('#drivers_count').val());
        driverCount++;
        $('#drivers_count').val(driverCount);

        $.ajax({
            type: "GET",
            url: "{{url('/contracts/online/driver_form/')}}" + '/' + driverCount + '?view_type={{$view_type}}',
            async: false,
            success: function (response) {
                $(response).insertBefore(".driver_limited");
                $('.driver-title').each(function (key, item) {
                    key += 1;
                    $(item).html("Водитель " + key);
                });
                formatTime();
                formatDate();

                $('#driver_fio_' + driverCount).suggestions({
                    serviceUrl: DADATA_AUTOCOMPLETE_URL,
                    token: DADATA_TOKEN,
                    type: "NAME",
                    count: 5,
                    onSelect: function (suggestion) {
                        key = $(this).data('key');
                        $('#driver_fio_' + key).val($(this).val());
                    }
                });

                activeKBMDriver(driverCount);


            }
        });

        return true;

    }

    function join(arr /*, separator */) {
        var separator = arguments.length > 1 ? arguments[1] : ", ";
        return arr.filter(function (n) {
            return n
        }).join(separator);
    }

    function formatCity(suggestion) {
        var address = suggestion.data;
        if (address.city_with_type === address.region_with_type) {
            return address.settlement_with_type || "";
        } else {
            return join([
                address.city_with_type,
                address.settlement_with_type]);
        }
    }


    function drivers_same_insurer() {

        $('[id*="driver_fio"]').prop('disabled', false);
        $('[id*="driver_birthdate"]').prop('disabled', false);
        $('[id*="driver_sex"]').prop('disabled', false);

        if (event === undefined) {
            $.each($('[name*="same_as_insurer"]'), function (k, v) {
                toggle_driver_same_insurer(v)
            })
        } else {
            toggle_driver_same_insurer(event.target)
        }

    }

    function toggle_driver_same_insurer(target) {

        target = $(target);
        var key = target.attr('data-key');


        if (target.prop('checked')) {
            $('[name*="same_as_insurer"]').prop('checked', false);
            target.prop('checked', true);

            $('#driver_fio_' + key).val($('#insurer_fio').val()).change();
            $('#driver_birthdate_' + key).val($('#insurer_birthdate').val()).change();
            $('#driver_sex_' + key).val(parseInt($('#insurer_sex').val())).change();

            $('#driver_fio_' + key).prop('disabled', true);
            $('#driver_birthdate_' + key).prop('disabled', true);
            $('#driver_sex_' + key).prop('disabled', true);
        }
    }


    function activeKBMDriver(i) {
        $('.driver-' + i).each(function () {
            $(this).change(function () {
                getKBMDriver(i);
            });
        });
    }


    function getKBMDriver(i) {

        var data = {
            fio: $('[name="contract[driver][' + i + '][fio]"]').val(),
            birth_date: $('[name="contract[driver][' + i + '][birth_date]"]').val(),
            sex: $('[name="contract[driver][' + i + '][sex]"]').val(),
            doc_serie: $('[name="contract[driver][' + i + '][doc_serie]"]').val(),
            doc_num: $('[name="contract[driver][' + i + '][doc_num]"]').val(),
            doc_date: $('[name="contract[driver][' + i + '][doc_date]"]').val(),
            exp_date: $('[name="contract[driver][' + i + '][exp_date]"]').val()
        };

        //Запрос на КБМ
        $.post("/contracts/online/get_kbm/", {data: data}, function (response) {

            $('[name="contract[driver][' + i + '][kbm]"]').val(response.kbm)

        });

    }


</script>

