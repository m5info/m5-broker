<p> -- defualt risk -- </p>
<div class="block-view">
    <h3>Риски</h3>
    <div class="form-group">

        <div class="col-md-3 col-lg-2">
            <label><b>Угон</b></label>
            {{Form::select("contract[risks][hijacking_id][id]", collect([0=>"Нет", 1=>'Да']), $terms->hijacking_id, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-md-2 risk_theft hidden">
            <label>Сумма (добавить)</label><br>
            {{Form::input('number','contract[risks][hijacking_id][sum]', $terms->hijacking_sum, ['class'=> 'form-control'])}}
        </div>
    </div>

    <div class="form-group">

        <div class="col-md-5 col-xs-12">
            <label for=""><b>Добровольное страхование гражданской ответственности</b></label><br>
            {{Form::select('contract[risks][voluntary_liability_insurance][status]', [0 => 'Нет', 1=> 'Есть'], $terms->voluntary_liability_insurance ? 1 : 0, ['class'=> 'form-control'])}}
        </div>
        <div class="col-md-4 risk_go hidden">
            <label>Сумма</label><br>
            {{Form::select('contract[risks][voluntary_liability_insurance][sum]', [0=> 'Не выбрано', 1 => 1000000, 2=> 1500000, 3=> 3000000], $terms->voluntary_liability_insurance, ['class'=> 'form-control'])}}
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-6">
            <label><b>Страхование пассажиров и водителя от несчастного случая (НС)</b></label><br>
            {{Form::select('contract[risks][passenger_and_driver_accident_insurance][status]', [0 => 'Нет', 1=> 'Есть'], $terms->passenger_and_driver_accident_insurance ? 1: 0, ['class'=> 'form-control'])}}
        </div>
        <div class="col-md-2  risk_ns hidden">
            <label>Сумма</label><br>
            {{Form::input('number','contract[risks][passenger_and_driver_accident_insurance][sum]', $terms->passenger_and_driver_accident_insurance_sum, ['class'=> 'form-control','min'=>'400000'])}}
        </div>
        <div class="col-md-2  risk_ns hidden">
            <label>Кол-во сидений</label><br>
            {{Form::input('number','contract[risks][passenger_and_driver_accident_insurance][seats]', $terms->passenger_and_driver_accident_insurance_seats, ['class'=> 'form-control','min'=>'0'])}}
        </div>
        <div class="col-md-2  risk_ns hidden">
            <label>Тип страховки</label><br>
            {{Form::select('contract[risks][passenger_and_driver_accident_insurance][type]',
            [0 => 'Паушальная система', 1=> 'по колличеству мест', 2=> 'по колличеству мест'],
            $terms->passenger_and_driver_accident_insurance_type ? 1: 0, ['class'=> 'form-control'])}}
        </div>
    </div>

    <div class="form-group" style="display: none">
        <div class="col-md-6">
            <label><b>Постоянная страховая сумма (GAP) </b></label><br>
            {{Form::select('contract[risks][permanent_sum_insured]', [0 => 'Нет', 1=> 'Есть'], $terms->permanent_sum_insured, ['class'=> 'form-control'])}}
        </div>

            <div class="col-md-3 col-lg-2">
                <label>Урегулирование без документов</label>
                {{Form::select("contract[risks][is_undocumented_settlement_competent_authorities]",  collect([0=>'Нет', 1=>'Неограниченно стекла и два раза повреждение детали', 1=>'Один раз стекла и один раз ЛКП']), $terms->is_undocumented_settlement_competent_authorities, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
            </div>

            <div class="col-md-3 col-lg-3">
                <label>Аварийный комиссар + сбор документов</label>
                {{Form::select("contract[risks][is_emergency_commissioner_select]", collect([0=>'Нет', 1=>'Сбор документов', 2=>'На место ДТП + сбор документов']), $terms->is_emergency_commissioner_select, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
            </div>

            <div class="col-md-3 col-lg-3">
                <label>Утрата товарной стоимости</label>
                {{Form::select("contract[risks][is_loss_commodity_value]", collect([0=>'Нет', 1=>'Есть']), $terms->is_loss_commodity_value, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
            </div>

    </div>

    <div class="form-group">
        <div class="col-xs-12">
            <h4>Другие риски</h4>
        </div>


        <div class="col-md-3 col-lg-2">
            <label>Самовозгорание</label>
            {{Form::select("contract[risks][damage_id]", collect([0=>"Нет", 1=>'Да']), $terms->damage_id, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>
    </div>

    <h3>Страховое возмещение</h3>
    <div class="row">
        <div class="col-md-6">
            <label>Для гарантийного ТС</label><br>
            @php
                if ($terms->guarantor_repair){
                    $checked = 'checked';
                }else{
                    $checked = '';
                }
            @endphp
            {{Form::input('checkbox', 'contract[risks][guarantor_repair]', '', [$checked => $checked] )}}
            <p class="inline-block label-r-g" data-name="contract[risks][guarantor][repair]"><b>Ремонт на СТО
                    официального диллера</b></p>
        </div>
        <div class="col-md-6">
            <label>Для негарантийного ТС</label><br>
            @php
                if ($terms->unguarantor_repair){
                    $checked = 'checked';
                }else{
                    $checked = '';
                }
            @endphp
            {{Form::input('checkbox', 'contract[risks][unguarantor_repair]', '', [$checked => $checked])}}
            <p class="inline-block label-r-g" data-name="contract[risks][unguarantor][repair]"><b>Ремонт на СТО
                    официального диллера</b></p>
        </div>
    </div>
</div>
<script src="/plugins/jquery/jquery.min.js"></script>
<script>
    var voluntary_liability_insurance =  $('[name="contract[risks][voluntary_liability_insurance][status]"]');
    var passenger_and_driver_accident_insurance =  $('[name="contract[risks][passenger_and_driver_accident_insurance][status]"]');
    var hijacking_id = $('[name="contract[risks][hijacking_id][id]"]');

    $(function () {



        switch($("[name='contract[program]']").val()) {
            case '50х50+':

                break;
            case 'Альфа-Все включено':
                $('[name="contract[risks][hijacking_id][id]"]').prop('selectedIndex',1);
                $('[name="contract[risks][hijacking_id][sum]"]').attr('min','1600000');
                $('[name="contract[risks][voluntary_liability_insurance][status]"]').val(1);
                //$('[name="contract[risks][voluntary_liability_insurance][status]"]').prop('selectedIndex',1);
                $('[name="contract[risks][passenger_and_driver_accident_insurance][status]"]').prop('selectedIndex',1);
                $('[name="contract[risks][passenger_and_driver_accident_insurance][sum]"]').attr('min','1600000');
                //showField();
                console.log(voluntary_liability_insurance);
                break;
            default:
                break;
        }

        $('input[type=number]').each(function () {
            if($(this).val() < $(this).attr('min') || $(this).val() == '')
                $(this).val($(this).attr('min'));
        });

        $('input[type=number]').on('change',function () {
            if($(this).attr('min'))
                if($(this).val() < $(this).attr('min') || $(this).val() == '')
                    $(this).val($(this).attr('min'));
        });

        showField();





        $('.label-r-g').click(function () {

            let name = $(this).attr('data-name');

            let status = $('[name = "' + name + '"]').attr('checked');

            if (status == undefined) {
                $('[name = "' + name + '"]').attr('checked', 'checked');
            } else if (status == 'checked') {
                $('[name = "' + name + '"]').removeAttr('checked');
            }

        });
    });




        function showField() {
            if (voluntary_liability_insurance.val() !== '' && voluntary_liability_insurance.val() !== '0') {
                $('.risk_go').removeClass('hidden');
            }

            if (passenger_and_driver_accident_insurance.val() !== '' && passenger_and_driver_accident_insurance.val() !== '0') {
                $('.risk_ns').removeClass('hidden');
            }
            if (hijacking_id.val() !== '' && hijacking_id.val() !== '0') {
                $('.risk_theft').removeClass('hidden');
            }

            $('[name = "contract[risks][voluntary_liability_insurance][status]"]').on('change', function () {
                $('.risk_go').toggleClass('hidden');
                $('.risk_go').children('select').prop('selectedIndex',0);
            });

            $('[name = "contract[risks][hijacking_id][id]"]').on('change', function () {
                status = $(this).val();
                if(status == 0)
                    $('.risk_theft').children('input').val('');
                $('.risk_theft').toggleClass('hidden');
            });

            $('[name = "contract[risks][passenger_and_driver_accident_insurance][status]"]').on('change', function () {
                // status = $('[name = "contract[risks][passenger_and_driver_accident_insurance][status]"]').val();
                $('.risk_ns').toggleClass('hidden');
                $('.risk_ns').children('input').val('');
                $('.risk_ns').children('select').prop('selectedIndex',0);
            });
        }

</script>