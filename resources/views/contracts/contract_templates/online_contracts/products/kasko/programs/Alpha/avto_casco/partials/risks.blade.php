<div class="block-view">
    <h3>Риски</h3>
    <div class="row">



        <div class="col-md-3 col-lg-2" >
            <label>Самовозгорание</label>
            {{Form::select("contract[risks][damage_id]", collect([0=>"Нет", 1=>'Да']), $terms->damage_id, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-md-3 col-lg-2" >
            <label>Угон</label>
            {{Form::select("contract[risks][hijacking_id]", collect([0=>"Нет", 1=>'Без ключей', 2=>'С ключами']), $terms->hijacking_id, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-md-3 col-lg-2" >
            <label>Урегулирование без документов</label>
            {{Form::select("contract[risks][is_undocumented_settlement_competent_authorities]",  collect([0=>'Нет', 1=>'Неограниченно стекла и два раза повреждение детали', 1=>'Один раз стекла и один раз ЛКП']), $terms->is_undocumented_settlement_competent_authorities, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-md-3 col-lg-3" >
            <label>Аварийный комиссар + сбор документов</label>
            {{Form::select("contract[risks][is_emergency_commissioner_select]", collect([0=>'Нет', 1=>'Сбор документов', 2=>'На место ДТП + сбор документов']), $terms->is_emergency_commissioner_select, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-md-3 col-lg-3" >
            <label>Утрата товарной стоимости</label>
            {{Form::select("contract[risks][is_loss_commodity_value]", collect([0=>'Нет', 1=>'Есть']), $terms->is_loss_commodity_value, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>

    </div>



    <h3>Страховое возмещение</h3>
    <div class="row">
        <div class="col-md-6">
            <label>Для гарантийного ТС</label><br>
            @php
                if ($terms->guarantor_repair){
                    $checked = 'checked';
                }else{
                    $checked = '';
                }
            @endphp
            {{Form::input('checkbox', 'contract[risks][guarantor_repair]', '', [$checked => $checked] )}}
            <p class="inline-block label-r-g" data-name="contract[risks][guarantor][repair]"><b>Ремонт на СТО официального диллера</b></p>
        </div>
        <div class="col-md-6">
            <label>Для негарантийного ТС</label><br>
            @php
                if ($terms->unguarantor_repair){
                    $checked = 'checked';
                }else{
                    $checked = '';
                }
            @endphp
            {{Form::input('checkbox', 'contract[risks][unguarantor_repair]', '', [$checked => $checked])}}
            <p class="inline-block label-r-g" data-name="contract[risks][unguarantor][repair]"><b>Ремонт на СТО официального диллера</b></p>
        </div>
    </div>
    <br>

    <h3>Дополнительные риски</h3>
    <div class="row">

        <div class="col-md-6">
            <label for=""><b>Добровольное страхование гражданской ответственности</b></label><br>
            {{Form::select('contract[risks][voluntary_liability_insurance][status]', [0 => 'Нет', 1=> 'Есть'], $terms->voluntary_liability_insurance ? 1 : 0, ['class'=> 'form-control'])}}
        </div>
        <div class="col-md-6 hide" id="dsgo_sum">
            <div class="col-md-6">
                <label>Сумма</label><br>
                {{Form::select('contract[risks][voluntary_liability_insurance][sum]', [0=> 'Не выбрано', 1 => 1000000, 2=> 1500000, 3=> 3000000], $terms->voluntary_liability_insurance, ['class'=> 'form-control'])}}
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <div class="clear"></div>
        <br>

        <div class="col-md-6">
            <label><b>Страхование пассажиров и водителя от несчастного случая (НС)</b></label><br>
            {{Form::select('contract[risks][passenger_and_driver_accident_insurance][status]', [0 => 'Нет', 1=> 'Есть'], $terms->passenger_and_driver_accident_insurance ? 1: 0, ['class'=> 'form-control'])}}
        </div>
        <div class="col-md-6 hide" id="spvns_sum">
            <div class="col-md-6">
                <label>Сумма</label><br>
                {{Form::input('text','contract[risks][passenger_and_driver_accident_insurance][sum]', $terms->passenger_and_driver_accident_insurance, ['class'=> 'form-control'])}}
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <div class="col-md-6 hide" id="seats_num">
            <div class="col-md-6">
                <label>Кол-во сидений</label><br>
                {{Form::input('text','contract[object][seats_num]', $terms->seats_num, ['class'=> 'form-control'])}}
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <div class="clear"></div>
        <br>

        <div class="col-md-6">
            <label><b>Постоянная страховая сумма (GAP) </b></label><br>
            {{Form::select('contract[risks][permanent_sum_insured]', [0 => 'Нет', 1=> 'Есть'], $terms->permanent_sum_insured, ['class'=> 'form-control'])}}
        </div>
        <div class="clear"></div>
    </div>


</div>

<script src="/plugins/jquery/jquery.min.js"></script>

<script>

    var voluntary_liability_insurance = '{{$terms->voluntary_liability_insurance}}';
    var passenger_and_driver_accident_insurance = '{{$terms->passenger_and_driver_accident_insurance}}';

    if (voluntary_liability_insurance !== '' && voluntary_liability_insurance !== '0'){
        $('#dsgo_sum').removeClass('hide');
    }

    if (passenger_and_driver_accident_insurance !== '' && passenger_and_driver_accident_insurance !== '0'){
        $('#spvns_sum').removeClass('hide');
        $('#seats_num').removeClass('hide');
    }

    $(document).on('change', $('[name = "contract[risks][voluntary_liability_insurance][status]"]'), function(){
        status = $('[name = "contract[risks][voluntary_liability_insurance][status]"]').val();
        if (status == 1){
            $('#dsgo_sum').removeClass('hide');
        } else{
            $('#dsgo_sum').addClass('hide');
        }
    });

    $(document).on('change', $('[name = "contract[risks][passenger_and_driver_accident_insurance][status]"]'), function(){
        status = $('[name = "contract[risks][passenger_and_driver_accident_insurance][status]"]').val();
        if (status == 1){
            $('#spvns_sum').removeClass('hide');
            $('#seats_num').removeClass('hide');
        } else{
            $('#spvns_sum').addClass('hide');
            $('#seats_num').addClass('hide');
        }
    });

/*    $('.label-r-g').click(function(){

        let name = $(this).attr('data-name');

        let status = $('[name = "'+name+'"]').attr('checked');

        if (status == undefined){
            $('[name = "'+name+'"]').attr('checked', 'checked');
        } else if(status == 'checked'){
            $('[name = "'+name+'"]').removeAttr('checked');
        }

    });*/


</script>