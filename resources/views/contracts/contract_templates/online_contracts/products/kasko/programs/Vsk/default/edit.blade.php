{{--Срок и период страхования--}}
@include('contracts.contract_templates.online_contracts.terms.kasko.edit', [
    'contract'=>$contract,
    'terms' => (isset($contract->terms))?$contract->terms:new \App\Models\Contracts\ContractTerms(),
])

{{--Участники договора--}}
{{--Страхователь--}}
@include('contracts.contract_templates.online_contracts.subject.edit', [
    'subject_title' => 'Страхователь',
    'subject_name' => 'insurer',
    'subject' => (isset($contract->insurer)?$contract->insurer:new \App\Models\Contracts\Subjects())
])

{{--Собственник--}}
@include('contracts.contract_templates.online_contracts.subject.edit', [
   'subject_title' => 'Собственник',
   'subject_name' => 'owner',
   'is_insurer' => (isset($contract->owner) && isset($contract->insurer) && $contract->insurer == $contract->owner)?1:0,
   'subject' => (isset($contract->owner)?$contract->owner:new \App\Models\Contracts\Subjects())
])


{{--Выгодоприобретатель--}}
@include('contracts.contract_templates.online_contracts.subject.edit', [
    'subject_title' => 'Выгодоприобретатель',
    'subject_name' => 'beneficiar',
    'is_insurer' => (isset($contract->beneficiar) && isset($contract->insurer) && $contract->beneficiar == $contract->owner)?1:0,
    'subject' => (isset($contract->beneficiar)?$contract->beneficiar:new \App\Models\Contracts\Subjects())
])

{{--Водители--}}
@include('contracts.contract_templates.online_contracts.drivers.edit', [
    'drivers' => $contract->drivers,
    'contract' => $contract
])


{{--Транспортное средство--}}
@include('contracts.contract_templates.online_contracts.insurance_object.auto.kasko.edit', [
    'object'=>(isset($contract->object_insurer_auto))?$contract->object_insurer_auto:new \App\Models\Contracts\ObjectInsurerAuto()
])


<script src="/plugins/jquery/jquery.min.js"></script>
@include('contracts.contract_templates.online_contracts.products.kasko.programs.Energogarant.js')



<div class="block-view">
    <h3>
        Результат рассчета
        <span style="width: 30px;height: 30px;font-size: 15px;cursor: pointer;color: orangered;" onclick="openFancyBoxFrame('{{url("/contracts/online/creare_new_calc/{$contract->id}/")}}')">
            <i class="fa fa-edit"></i>
        </span>

        <span class="btn btn-success btn-right" onclick="saveContract({{$contract->id}});">Сохранить как черновик</span>


    </h3>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="offers">
            @include('contracts.contract_templates.online_contracts.tariff.kasko.price', ['calc'=>$contract->calculation, 'contract' => $contract])
        </div>

    </div>
</div>

