<p>---default drivers---</p>
<div class="block-view">

    <h3>Водители <input type="number" id="contract_driver_type" hidden name="contract[drivers_type_id]" value="0"> {{ Form::checkbox('', 1, 0, ['id' => 'driver_type_checkbox', 'data-key'=>'0']) }} <label>Мультидрайв</label>
        <div class="col-lg-3 pull-right">


            {{--{{ Form::select('contract[drivers_type]', collect(\App\Models\Contracts\Contracts::DRIVERS_TYPE), $contract->drivers_type_id, ['class' => 'form-control', 'id'=>'driver_type', 'data-key'=>'0']) }}--}}
        </div>

    </h3>
    <div class="row" style="padding: 0 20px 0 20px;">

        <input name="contract[drivers_count]" id="drivers_count" type="hidden" value="{{ sizeof($drivers) }}">

        @if(sizeof($drivers))
            @foreach($drivers as $driver)
                @include('contracts.contract_templates.online_contracts.drivers.partials.driver.edit', [
                    'i' => $loop->iteration,
                    'driver' => $driver,
                ])
            @endforeach
        @endif


        <div class="driver_limited col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none;">
            <div style="width:50px;padding-top:10px;">
                <a href="javascript:void(0);" class="btn btn-primary inline-block" title="Добавить водителя"
                   onclick="addNewDriver();">
                    Добавить водителя</a>
            </div>
        </div>

        <div class="driver_unlimited col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none;">
            <div class="row form-horizontal">
                <div class="col-lg-12">
                    <p>Договор будет заключен на условиях неограниченного списка лиц, допущенных к управлению</p>
                </div>
            </div>
        </div>
    </div>

</div>

<script>

    function activValidDrivers() {

        @if(sizeof($drivers))
        @foreach($drivers as $driver)
        activeKBMDriver({{$loop->iteration}});

        @endforeach
        @endif

    }

</script>