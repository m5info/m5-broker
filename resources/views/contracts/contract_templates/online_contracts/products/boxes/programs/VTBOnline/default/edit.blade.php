
<script>
    Resolute = {};
    Resolute.frame = {};
    Resolute.scroll = function () {
        var scroll = 0;
        if (window.pageXOffset != null) {
            scroll = window.pageYOffset - $('#RESOLUTE_INSURANCE').offset().top + 5;
        } else {
            scroll = window.document.documentElement.scrollTop - $('#RESOLUTE_INSURANCE').offset().top + 5;
        }
        if (scroll > 0)Resolute.frame.window.postMessage('__resolute__{"scroll":' + scroll + '}', '*');
    };
    $(document).ready(function () {
        Resolute.frame = document.getElementById('RESOLUTE_INSURANCE');
        Resolute.frame.window = document.getElementById('RESOLUTE_INSURANCE').contentWindow;
        if (Resolute.frame) {
            Resolute.frame.onload = function () {
                $(window).scroll(Resolute.scroll);
                Resolute.scroll.call(window);
            };
        }
        $(window).on('message', function (e) {
            var data = e.originalEvent.data;
            if (/__resolute__/.test(data)) {
                data = JSON.parse(data.replace('__resolute__', ''));
                var _scroll = (Resolute.frame.offsetTop + data.scroll) + 'px';
                $('body,html').animate({"scrollTop": _scroll}, data.scrollp);
            }
        });
    });</script>
<div style="min-width: 910px; height: 700px;overflow: auto;position: static;">
    <iframe src='{!! $contract->calculation->get_product_service()->getFrame($product) !!}' height='6000px' width='100%' id='RESOLUTE_INSURANCE' scrolling='no' frameborder='0'>
        Ваш браузер не поддерживает плавающие фреймы!
    </iframe>
</div>
