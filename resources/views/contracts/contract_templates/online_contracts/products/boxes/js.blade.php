<script>



    function initDK(){

        var im = new Inputmask("9999", {"yearrange": {minyear: 1900, maxyear: 2099}});
        im.mask($('#carYear'));

        $('#insurer_fio').suggestions({
            serviceUrl: DADATA_AUTOCOMPLETE_URL,
            token: DADATA_TOKEN,
            type: "NAME",
            count: 5,
            onSelect: function (suggestion) {

            }
        });


        //initCar();
        setTimeout(initCar, 1000);

    }


    function getModelsObjectInsurer(KEY, select_model_id)
    {

        $.getJSON('{{url("/contracts/actions/get_models")}}', {categoryId: $('#object_ts_category_' + KEY).val(), markId: $('#object_ts_mark_id_' + KEY).select2('val')}, function (response) {

            var options = "<option value='0'>Не выбрано</option>";
            response.map(function (item) {
                options += "<option value='" + item.id + "'>" + item.title + "</option>";
            });
            $('#object_ts_model_id_' + KEY).html(options).select2('val', select_model_id);


        });

        getAjaxTsCategoryOKP();



    }

    function getTsCategoryOKP(current, select_id)
    {

        $.getJSON('{{url("/contracts/actions/get_category_okp")}}', {categoryId: current}, function (response) {

            var options = "<option value='0'>Не выбрано</option>";
            response.map(function (item) {
                options += "<option value='" + item.id + "'>" + item.title + "</option>";
            });
            $('#object_ts_category_okp_0').html(options).select2('val', select_id);


        });

    }


</script>

