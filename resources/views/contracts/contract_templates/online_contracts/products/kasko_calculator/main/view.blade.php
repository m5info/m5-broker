<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">


    {{--Поставщик, название страховой и страховая сумма--}}
    @include('contracts.contract_templates.online_contracts.insurance.1', [
        'contract' => $contract
    ])

    {{--Срок и период страхования--}}
    @include('contracts.contract_templates.online_contracts.terms.view')

    {{--Участники договора--}}
    {{--Страхователь--}}
    @include('contracts.contract_templates.online_contracts.subject.view', [
        'subject_title' => 'Страхователь',
        'subject_name' => 'insurer',
        'subject' => (isset($contract->insurer)?$contract->insurer:new \App\Models\Contracts\Subjects())
    ])

    {{--Собственник--}}
    @include('contracts.contract_templates.online_contracts.subject.view', [
       'subject_title' => 'Собственник',
       'subject_name' => 'owner',
       'is_insurer' => (isset($contract->owner) && isset($contract->insurer) && $contract->insurer == $contract->owner)?1:0,
       'subject' => (isset($contract->owner)?$contract->owner:new \App\Models\Contracts\Subjects())
    ])

    {{--Водители--}}
    @include('contracts.contract_templates.online_contracts.drivers.view', [
        'drivers' => $contract->drivers,
        'contract' => $contract
    ])


    {{--Транспортное средство--}}
    @include('contracts.contract_templates.online_contracts.insurance_object.auto.osago.view', [
        'object'=>(isset($contract->object_insurer_auto))?$contract->object_insurer_auto:new \App\Models\Contracts\ObjectInsurerAuto()
    ])
</div>


<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

    <script src="/plugins/jquery/jquery.min.js"></script>

    @if($contract->selected_calculation && $contract->selected_calculation->statys_id == 1)


        <h3>Подтверждение оплаты</h3>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row form-horizontal">

                {{ Form::open(['method' => 'post', 'class' => 'form-horizontal', 'id' => 'payment_confirm_form']) }}

                <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="control-label pull-left" >
                        БСО <span style="color:red">*</span>
                    </label>
                    <label class="control-label pull-right">Электронный
                        <input type="checkbox" value="1" name="contract[bso_is_epolicy]" onchange="viewSetBsoIsPolicy()" id="bso_is_epolicy"/>
                    </label>
                    {{ Form::text('contract[bso_title]', '', ['class' => 'form-control', 'data-hidden' => 'bso_id', 'id'=>'bso_title']) }}
                    <input type="hidden" name="contract[bso_id]" id="bso_id" />
                </div>

                <!-- Метод оплаты -->
                @include("contracts.contract_templates.default.payment_method.edit")

                {{ Form::close() }}

            </div>
        </div>



        <script>

            $(function(){
                search_bso($('[name="contract[bso_title]"]'), 1);
                selectViewPaymentMethod();
                viewSetBsoIsPolicy();


                /*
                search_bso($('[name="contract[bso_receipt]"]'), 2);


                $(document).on('change', '#bso_id, #bso_receipt_id', function(){
                    if($('#bso_id').val() && $('#bso_receipt_id').val()){
                        $('#pay').show();
                    }else{
                        $('#pay').hide();
                    }
                });
                */



            });


            function viewSetBsoIsPolicy() {

                if($("#bso_is_epolicy").is(':checked')){
                    $("#bso_title").attr('disabled','disabled');
                }else{
                    $("#bso_title").removeAttr('disabled');
                }
            }


            function payment_confirm_submit(){
                var data = {};

                payment_method = $('#payment_method').val();
                key_type = $('.payment_method_type_'+payment_method).data("key_type");
                bso_is_epolicy = $("#bso_is_epolicy").is(':checked')?1:0;




                switch(parseInt(key_type)) {
                    case 0:
                        data = {
                            'payment_method' : payment_method,
                            'bso_is_epolicy' : bso_is_epolicy,
                            'bso_receipt_id' : $('#bso_receipt_id').val(),
                            'bso_id' : $('#bso_id').val(),
                        };
                        break;

                    case 1:
                        data = {
                            'payment_method' : payment_method,
                            'bso_is_epolicy' : bso_is_epolicy,
                            'send_email' : $('#send_email_'+payment_method).val(),
                        };
                        break;

                    case 2:
                        data = {
                            'payment_method' : payment_method,
                            'bso_is_epolicy' : bso_is_epolicy,
                            'send_email' : $('#send_email_'+payment_method).val(),
                        };
                        break;

                    case 3:
                        data = {
                            'payment_method' : payment_method,
                            'bso_is_epolicy' : bso_is_epolicy,
                        };
                        break;


                }


                loaderShow();

                $.post('{{ url("/contracts/online/{$contract->id}/payment_confirm/") }}', data, function (response) {
                    loaderHide();
                    if(response.state == true)
                    {
                        reload();
                    }else{
                        flashMessage('danger', response.error);
                    }

                })  .done(function() {
                    loaderShow();
                })
                    .fail(function() {
                        loaderHide();
                    })
                    .always(function() {
                        loaderHide();
                    });



            }

            function return_calc(){
                $.post('{{ url("/contracts/online/{$contract->id}/return_calculation/") }}', {}, function(res){
                    location.href = "/contracts/online/edit/{{ $contract->id }}"
                })
            }
            function search_bso(input, type) {

                var hid_id = input.attr('data-hidden');
                $('#' + hid_id).val('').change();

                input.suggestions({
                    serviceUrl: "/bso/actions/get_bso/",
                    type: "PARTY",
                    params: {
                        type_bso: type,
                        bso_supplier_id: parseInt('{{ $contract->selected_calculation ? $contract->selected_calculation->bso_supplier_id : 0 }}'),
                        bso_agent_id: parseInt('{{ $contract->agent_id }}'),
                        bso_used: []
                    },
                    count: 5,
                    minChars: 3,
                    formatResult: function (e, t, n, i) {
                        var s = this;
                        var title = n.value;
                        var bso_type = n.data.bso_type;
                        var bso_sk = n.data.bso_sk;
                        var agent_name = n.data.agent_name;

                        var view_res = title;
                        view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">СК</span>' + bso_sk + "</div>";
                        view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Тип</span>' + bso_type + "</div>";
                        view_res += '<div class="' + s.classes.subtext + '"><span class="' + s.classes.subtext_inline + '">Агент</span>' + agent_name + "</div>";

                        return view_res;
                    },
                    onSelect: function (suggestion) {
                        $('#' + hid_id).val(suggestion.data.bso_id).change()
                    }
                });

            }


            function selectViewPaymentMethod() {

                payment_method = $("#payment_method").val();
                $(".payment_method_type").hide();
                $(".payment_method_type_"+payment_method).show();

                //search_bso($('[name="contract[bso_receipt]"]'), 2);


            }


        </script>

    @elseif($contract->selected_calculation && $contract->selected_calculation->statys_id == 3)

    <!-- Договор на согласовании или на оплате -->



    @elseif($contract->selected_calculation && $contract->selected_calculation->statys_id == 4)



    <!-- Информация по договору -->
        @include("contracts.contract_templates.default.contract.view")

    <!-- Платежи -->
        @include("contracts.contract_templates.default.payments.view")

    @endif

    <!-- Документы -->
        @include("contracts.contract_templates.default.documents.edit")


</div>


