{{--Условия договора--}}
@include('contracts.contract_templates.online_contracts.terms.kasko_calculator.edit', [
    'contract'=>$contract,
    'object'=>(isset($contract->object_insurer_auto))?$contract->object_insurer_auto:new \App\Models\Contracts\ObjectInsurerAuto(),
    'terms' => (isset($contract->terms))?$contract->terms:new \App\Models\Contracts\ContractTerms(),
])


{{--Риски--}}
@include('contracts.contract_templates.online_contracts.risks.kasko_calculator.edit', [
    'contract'=>$contract,
    'terms' => (isset($contract->terms))?$contract->terms:new \App\Models\Contracts\ContractTerms(),
])

{{--Транспортное средство--}}
@include('contracts.contract_templates.online_contracts.insurance_object.auto.kasko_calculator.edit', [
    'object'=>(isset($contract->object_insurer_auto))?$contract->object_insurer_auto:new \App\Models\Contracts\ObjectInsurerAuto()
])


{{--Водители--}}
@include('contracts.contract_templates.online_contracts.drivers.kasko_calculator.edit', [
    'drivers' => $contract->drivers,
    'contract' => $contract
])




<script src="/plugins/jquery/jquery.min.js"></script>
@include('contracts.contract_templates.online_contracts.products.kasko_calculator.js')



<div class="block-view">
    <h3>
        Результат рассчета

        <span class="btn btn-success btn-left" onclick="saveContract({{$contract->id}});">Сохранить</span>

        <span class="btn btn-primary pull-right" onclick="tempCalculate({{$contract->id}});">Рассчитать</span>

    </h3>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="offers">
            @include('contracts.contract_templates.online_contracts.tariff.insurance_price', ['online_calculations'=>$contract->online_calculations])
        </div>

    </div>
</div>


