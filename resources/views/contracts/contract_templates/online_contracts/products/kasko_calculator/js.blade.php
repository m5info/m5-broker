<script>

    $(function(){

        init();

    });


    function init(){

        $('#driver_type_checkbox').change(function(){
            if (parseInt($(this).val()) == 0) {
                $('#driver_type_checkbox').val(1);
                $('#contract_driver_type').attr('value',1);
                $('.driver_limited').hide();
                $('.drivers_count').hide();
                $('.driver_unlimited').show();
                $('.driver').hide();
            }
            else if (parseInt($(this).val()) == 1) {
                $('#driver_type_checkbox').val(0);
                $('#contract_driver_type').attr('value',0);
                $('.driver_limited').show();
                $('.drivers_count').show();
                $('.driver_unlimited').hide();
                $('.driver').show();
            }
        });

        $('#driver_type').change(function () {
            if (parseInt($(this).val()) == 0) {
                $('.driver_limited').show();
                $('.drivers_count').show();
                $('.driver_unlimited').hide();
                $('.driver').show();

            } else if (parseInt($(this).val()) == 1) {
                $('.driver_limited').hide();
                $('.drivers_count').hide();
                $('.driver_unlimited').show();
                $('.driver').hide();
            }

        });

        $('#driver_type_checkbox').change();

        $('#driver_type').change();
        $('#drivers_count').change();

        if($('.driver').length === 0){
            addNewDriver();
        }

        var im = new Inputmask("9999", {"yearrange": {minyear: 1900, maxyear: 2099}});
        im.mask($('#carYear'));

        setTimeout(initCar, 30);




    }


    function getModelsObjectInsurer(KEY, select_model_id)
    {

        $.getJSON('{{url("/contracts/actions/get_models")}}', {categoryId: $('#object_ts_category_' + KEY).val(), markId: $('#object_ts_mark_id_' + KEY).select2('val')}, function (response) {

            var options = "<option value='0'>Не выбрано</option>";
            response.map(function (item) {
                options += "<option value='" + item.id + "'>" + item.title + "</option>";
            });
            $('#object_ts_model_id_' + KEY).html(options).select2('val', select_model_id);


        });

        var current = parseInt($('#object_ts_category_' + KEY).val());

        if (current == 3) {
            $('#object_weight_' + KEY).show();
            $('#object_capacity_' + KEY).show();
            $('#object_passengers_count_' + KEY).hide();
        } else if (current == 4) {
            $('#object_weight_' + KEY).hide();
            $('#object_capacity_' + KEY).hide();
            $('#object_passengers_count_' + KEY).show();
        } else {
            $('#object_weight_' + KEY).hide();
            $('#object_capacity_' + KEY).hide();
            $('#object_passengers_count_' + KEY).hide();
        }




    }

    function join(arr /*, separator */) {
        var separator = arguments.length > 1 ? arguments[1] : ", ";
        return arr.filter(function (n) {
            return n
        }).join(separator);
    }

    function formatCity(suggestion) {
        var address = suggestion.data;
        if (address.city_with_type === address.region_with_type) {
            return address.settlement_with_type || "";
        } else {
            return join([
                address.city_with_type,
                address.settlement_with_type]);
        }
    }



    function removeDriver(i) {

        driver_id = $('[name="contract[driver]['+i+'][id]"]').val();

        if(driver_id && parseInt(driver_id) > 0){
            $.post("/contracts/online/delete_driver/", {driver_id: driver_id}, function (response) {

                delDtiver(i);

            });
        }else{
            delDtiver(i);
        }

    }

    function delDtiver(i) {
        $('.driver-' + i).remove();
        $('.driver-title').each(function (key, item) {
            key += 1;
            $(item).html("Водитель " + key);
        });
    }


    function addNewDriver() {
        var driverCount = parseInt($('#drivers_count').val());
        driverCount++;
        $('#drivers_count').val(driverCount);

        $.ajax({
            type: "GET",
            url: "{{url('/contracts/online/driver_form_calc/')}}" + '/' + driverCount + '?view_type={{$view_type}}',
            async: false,
            success: function (response) {
                $(response).insertBefore(".driver_limited");
                $('.driver-title').each(function (key, item) {
                    key += 1;
                    $(item).html("Водитель " + key);
                });


            }
        });

        return true;

    }




</script>

