<script>

    var equipment = 1;

    $(function () {

        init();

    });


    function init() {




        var im = new Inputmask("9999", {"yearrange": {minyear: 1900, maxyear: 2099}});
        im.mask($('#carYear'));

        var object_insurer_id = $('#object_id').val();

        if (object_insurer_id){
            getAllEquipment(object_insurer_id);
        }

        initStartObject();
        initStartRisks();

    }


    function join(arr /*, separator */) {
        var separator = arguments.length > 1 ? arguments[1] : ", ";
        return arr.filter(function (n) {
            return n
        }).join(separator);
    }

    function formatCity(suggestion) {
        var address = suggestion.data;
        if (address.city_with_type === address.region_with_type) {
            return address.settlement_with_type || "";
        } else {
            return join([
                address.city_with_type,
                address.settlement_with_type]);
        }
    }





</script>

