@if(isset($calculations_history) && sizeof($calculations_history))

    @foreach($calculations_history as $history)


        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">

            <div class="trucks-item order-item" style="cursor: pointer;" onclick="setSKCalc({{$history->id}})">

                <div class="order-summary">
                    <div class="discount">
                        <div class="title">Компания</div>
                        <div class="brand">{{ $history->insurance_companies->title }}</div>
                    </div>
                    <div class="total">
                        <div class="title">Продукт</div>
                        <div class="brand">{{($history->program)?$history->program->title:$history->product->title}}</div>
                    </div>
                </div>


                <div class="divider"></div>

                <div>
                    <div class="order-summary">
                        <div class="discount">
                            <div class="title">Коммиссия</div>
                            <span class="value">{{titleFloatFormat($history->agent_kv)}}</span>
                        </div>
                        <div class="total">
                            <div class="title">Сумма</div>
                            <span class="value">{{titleFloatFormat($history->sum)}}</span>
                        </div>
                    </div>
                </div>


                @if($history->statys_id == 1)
                    <div>
                        <div class="divider"></div>
                        <div class="info">
                            {{$history->tariff}}
                        </div>
                    </div>
                @elseif($history->statys_id == 0)
                    <div>
                        <div class="divider"></div>
                        <div class="info" style="color: red;">
                            {!! $history->messages !!}

                        </div>
                    </div>
                @endif
            </div>

        </div>

    @endforeach

@endif

<script>
    function setSKCalc(calc_id) {
        location.href = '/contracts/online/set_sk_calc/?calc_id=' + calc_id;
    }
</script>