@if(isset($calc))


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field-custom">
                <span class="view-label-custom">Страховая компаня</span>
                <span class="view-value-custom">{{ $calc->insurance_companies->title }}</span>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field-custom">
                <span class="view-label-custom">Программа</span>
                <span class="view-value-custom">{{$contract->getProductOrProgram()->title}}</span>
            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field-custom">
                <span class="view-label-custom">Страховая премия</span>
                <span class="view-value-custom">{{titleFloatFormat($calc->sum)}}</span>
            </div>
        </div>



        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field-custom">

                    @if($calc->statys_id == 1)
                        <span>{{$calc->tariff}}</span>
                    @elseif($calc->statys_id == 0)
                        <span style="color: red;">{!! $calc->messages !!}</span>
                    @endif

            </div>
        </div>


        @if(isset($is_set_calt) && $is_set_calt == true)

            {{-- <span class="btn btn-success btn-right" onclick="releaseCalculate({{$contract->id}});">Выпустить</span> --}}

            <span class="btn btn-success btn-right" onclick="alert('Выпуск договора на тесте запрещен!');">Выпустить</span>

        @else

        <span class="btn btn-primary btn-right" onclick="calculate({{$contract->id}});">Рассчитать</span>

        @endif

@endif

<h3>
    История рассчетов
</h3>

<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        @include('contracts.contract_templates.online_contracts.tariff.histoty_price', ['calculations_history'=>$contract->online_calculations_history])
    </div>

</div>

