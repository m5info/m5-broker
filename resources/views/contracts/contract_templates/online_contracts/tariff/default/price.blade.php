
    <div class="trucks-item order-item" style="cursor: pointer;min-height: 285px;" @if($calc->statys_id == 1 || $calc->statys_id == 2) onclick="setSKCalc({{$calc->id}})" @endif>

        @if(isset($calc->insurance_companies) && isset($calc->insurance_companies->logo))
            <center><img src="{{ $calc->insurance_companies->logo->url }}" alt="" class="img-responsive"></center>
            <div class="divider"></div>
        @endif

        <div class="order-summary">
            <div class="discount">
                <div class="title">Компания</div>
                <div class="brand">{{ ($calc->insurance_companies)?$calc->insurance_companies->title:'' }}</div>
            </div>
            <div class="total">
                <div class="title">Продукт</div>
                <div class="brand">{{($calc->getProductOrProgram())?$calc->getProductOrProgram()->title:''}}</div>
            </div>
        </div>


        <div class="divider"></div>

        <div>
            <div class="order-summary">
                <div class="discount">
                    <div class="title">Коммиссия</div>
                    <span class="value">{{titleFloatFormat($calc->agent_kv)}}</span>
                </div>
                <div class="total">
                    <div class="title">Сумма</div>
                    <span class="value">{{titleFloatFormat($calc->sum)}}</span>
                </div>
            </div>
        </div>


        @if($calc->statys_id == 1 || $calc->statys_id == 2)
            <div>
                <div class="divider"></div>
                <div class="info">
                    {!! $calc->tariff !!}
                </div>
            </div>
        @elseif($calc->statys_id == 0)
            <div>
                <div class="divider"></div>
                <div class="info" style="color: red;">

                    {!! $calc->messages !!}

                </div>
            </div>
        @endif
    </div>
