@if(isset($online_calculations) && sizeof($online_calculations))

    @foreach($online_calculations as $calc)

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">

            @include('contracts.contract_templates.online_contracts.tariff.default.price', ['calc'=>$calc])


        </div>

    @endforeach

@endif
