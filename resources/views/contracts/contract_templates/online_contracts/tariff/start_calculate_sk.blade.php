

<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3" id="calculation_{{$calculation_id}}">

    <div class="trucks-item order-item">

        @if(isset($calculation->insurance_companies) && isset($calculation->insurance_companies->logo))
            <center><img src="{{ $calculation->insurance_companies->logo->url }}" alt="" class="img-responsive"></center>
            <div class="divider"></div>
        @endif

        <div class="order-summary">
            <div class="discount">
                <div class="title">Компания</div>
                <div class="brand">{{ ($calculation->insurance_companies)?$calculation->insurance_companies->title:'' }}</div>
            </div>
            <div class="total">
                <div class="title">Продукт</div>
                <div class="brand">{{($calculation->getProductOrProgram())?$calculation->getProductOrProgram()->title:''}}</div>
            </div>
        </div>

        <div class='loader_div' id="loader_div_{{$calculation_id}}"></div>

        <div id="view_error_{{$calculation_id}}" class="hidden">
            <span class="btn btn-danger" onclick="refresh_calculate_id({{$calculation_id}})">Ошибка запроса</span>
        </div>

    </div>


</div>

@if($api_setting->custom_realization)
    <script src="/js/autobuhn.min.0.8.3.js"></script>
    <script>
        pusher_uri = '{{config('app.pusher_integration_uri_outer')}}';
        socker_port = '{{config('app.integration_port')}}';
        protocol = '{{config('app.protocol_websocket')}}';
    </script>
    <script src="/js/sk/{{$api_setting->dir_name}}/{{$calculation->product->slug}}/calc.js"></script>
    <script>
        refresh_calculate_id_{{$api_setting->dir_name}}_{{$calculation->product->slug}}('{{$api_setting->dir_name}}','{{$calculation->product->slug}}',{{$calculation_id}});
    </script>
@else
    <script>
        $(function () {

            refresh_calculate_id({{$calculation_id}});

            /*
            $.get('/contracts/online/calculate_sk/{{$calculation_id}}', {}, function (response) {

            $('#calculation_{{$calculation_id}}').html(response);

        }).always(function () {

            //Ошибка

            $(".loader_div").hide();
            $("#view_error").removeClass('hidden');

        });

        / *

        $.get("/contracts/online/calculate_sk/{{$calculation_id}}", function(html) {
            $('#calculation_{{$calculation_id}}').html(html);
        })


        / *
        $.ajax({
            url: '/contracts/online/calculate_sk/{{$calculation_id}}',
            type  : 'GET',
        }).done(function(response) {
            $('#calculation_{{$calculation_id}}').html(response);
        });

        / *

        */


        });

    </script>
@endif


