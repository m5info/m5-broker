<h3>Транспортное средство</h3>
<div class="row">
    <div class="col-md-4 col-lg-3">
        <label class="control-label">Категория</label>
        {{Form::select("contract[object][ts_category]", \App\Models\Vehicle\VehicleCategories::query()->get()->pluck('title', 'id'), 0, ['class' => 'form-control', 'id'=>"object_ts_category_0", 'onchange'=>"getModelsObjectInsurer(0, 0);"])}}
    </div>

    <div class="col-md-4 col-lg-3" >
        <label class="control-label">Марка</label>
        {{Form::select("contract[object][mark_id]", \App\Models\Vehicle\VehicleMarks::orderBy('title')->get()->pluck('title', 'id')->prepend('Не выбрано', 0), 0, ['class' => 'mark-id mark_id select2', "id"=>"object_ts_mark_id_0", 'style'=>'width: 100%;', 'onchange'=>"getModelsObjectInsurer(0, 0);"])}}
    </div>

    <div class="col-md-4 col-lg-3" >
        <label class="control-label">Модель</label>
        {{Form::select("contract[object][model_id]", [], 0, ['class' => 'model_id model-id select2', "id"=>"object_ts_model_id_0", 'style'=>'width: 100%;'])}}
    </div>

    <div class="col-lg-3" >
        <label class="control-label">Цель использования</label>
        {{Form::select("contract[object][reason]", ['Личная','Учебная езда','Такси','Дорожные и специальные ТС','Экстренные и коммунальные службы','Перевозка опасных и легко воспламеняющихся грузов','Регулярные пассажирские перевозки/перевозки пассажиров по заказам',], 0, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
    </div>


    <div class="clear"></div>

    <div class="col-lg-3" >
        <label class="control-label">VIN</label>
        {{ Form::text("contract[object][vin]", '', ['class' => 'form-control', "id"=>"object_ts_vin_0", 'placeholder' => 'KL1UF756E6B195928']) }}
    </div>

    <div class="col-lg-3" >
        <label class="control-label">Номер кузова</label>
        {{ Form::text("contract[object][car_body_num]", '', ['class' => 'form-control', 'placeholder' => '235347453234']) }}
    </div>


    <div class="col-lg-3" >
        <label class="control-label">Страна регистрации</label>
        {{ Form::text("contract[object][car_body_num]", '', ['class' => 'form-control', 'placeholder' => 'Россия']) }}
    </div>

    <div class="col-lg-3" >
        <label class="control-label">Рег. номер</label>
        {{ Form::text("contract[object][reg_number]", '', ['class' => 'form-control ru_sumb', "id"=>"object_ts_reg_number_0", 'placeholder' => 'е050КХ99']) }}
    </div>

    <div class="clear"></div>

    <div class="col-lg-3" >
        <label class="control-label">Год выпуска</label>
        {{Form::text("contract[object][reason]",  Carbon\Carbon::now()->format('Y'), ['class' => 'form-control','id' => 'carYear', 'style'=>'width: 100%;',  'placeholder' => '2017'])}}
    </div>

    <div class="col-lg-3" >
        <label class="control-label">Мощность (л.с.)</label>
        {{ Form::text("contract[object][power]", '', ['class' => 'form-control sum', "id"=>"object_ts_power_0", 'placeholder' => '145']) }}
    </div>


    <div class="clear"></div>
    {{--- Доп условия для типов автомобилей ----}}

    <div class="col-lg-3" id="object_passengers_count_0">
        <label class="control-label">Кол-во мест</label>
        {{ Form::text("contract[object][passengers_count]", '', ['class' => 'form-control', 'placeholder' => '5']) }}
    </div>

    <div class="col-lg-3" id="object_weight_0">
        <label class="control-label">Масса</label>
        {{ Form::text("contract[object][weight]", '', ['class' => 'form-control', 'placeholder' => '1500']) }}
    </div>


    <div class="col-lg-3" id="object_capacity_0">
        <label class="control-label">Грузоподъемность</label>
        {{ Form::text("contract[object][capacity]", '', ['class' => 'form-control', 'placeholder' => '900']) }}
    </div>

    {{--- end ---}}
    <div class="clear"></div>

    <div class="col-lg-6" >
        <label  class="control-label" style="max-width:100%;">Автомобиль используется с прицепом</label>
        {{ Form::checkbox('contract[object][trailer]', 0, 0,['style' => 'width:18px;height:18px;margin-left:5px;position:absolute;']) }}
    </div>
    <div class="clear"></div>

    <div class="col-lg-12" >
        <h4>Документы ТС</h4>
    </div>

    <div class="col-lg-3" >
        <label class="control-label">Тип документа</label>
        {{Form::select("contract[object][doc_type]", ['Свидетельство о регистрации','Паспорт транспортного средства'], 0, ['class' => 'form-control ru_sumb', 'style'=>'width: 100%;'])}}
    </div>

    <div class="col-lg-3" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Серия <span class="required">*</span>
                </label>
                {{ Form::text('contract[object][docserie]', '', ['class' => 'form-control ru_sumb', 'placeholder' => '38MB']) }}
            </div>
        </div>
    </div>


    <div class="col-lg-3" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Номер <span class="required">*</span>
                </label>
                {{ Form::text('contract[object][docnumber]', '', ['class' => 'form-control', 'placeholder' => '587123']) }}
            </div>
        </div>
    </div>


    <div class="col-lg-3" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Дата выдачи <span class="required">*</span>
                </label>
                {{ Form::text('contract[object][docdate]', '', ['class' => 'form-control format-date end-date', 'placeholder' => '18.04.2012']) }}
                <span class="glyphicon glyphicon-calendar calendar-icon"></span>
            </div>
        </div>
    </div>

    <div class="clear"></div>
    <div class="col-lg-12" >
        <h4>Документ ТО</h4>
    </div>


    <div class="col-lg-3" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Номер диагностической карты <span class="required">*</span>
                </label>
                {{ Form::text('contract[object][docnumber]', '', ['class' => 'form-control', 'placeholder' => '3456342']) }}
            </div>
        </div>
    </div>

    <div class="col-lg-3" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Дата очередного ТО <span class="required">*</span>
                </label>
                {{ Form::text('contract[object][docdate]', '', ['class' => 'form-control format-date end-date', 'placeholder' => '13.02.2018']) }}
                <span class="glyphicon glyphicon-calendar calendar-icon"></span>
            </div>
        </div>
    </div>  
</div>
