<div class="block-view">
    <div class="block-sub">
        <h3>Территория страхования</h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Адрес</span>
                    <span class="view-value">{{$object->address}}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Этажи</span>
                    <span class="view-value">{{$object->house_floor}} / {{$object->flat_floor}}</span>
                </div>
            </div>

        </div>
    </div>
</div>

