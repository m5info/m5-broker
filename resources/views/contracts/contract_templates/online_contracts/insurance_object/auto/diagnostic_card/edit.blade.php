<div class="block-view">
<h3>Транспортное средство</h3>
<div class="row">
    <div class="col-md-6 col-lg-3">
        <label class="control-label">Категория <span class="required">*</span></label>
        {{Form::select("contract[object][ts_category]", \App\Models\Vehicle\VehicleCategories::query()->get()->pluck('title', 'id'), ($object->ts_category)?$object->ts_category:2, ['class' => 'form-control select2-ws', 'id'=>"object_ts_category_0", 'onchange'=>"getModelsObjectInsurer(0, 0);"])}}
    </div>

    <div class="col-md-6 col-lg-3">
        <label class="control-label">Категория ОКП <span class="required">*</span></label>
        {{Form::select("contract[object][ts_category_okp]", \App\Models\Vehicle\VehicleCategoriesOKP::where('category_id', ($object->ts_category)?$object->ts_category:2)->orderBy('title', 'asc')->get()->pluck('title', 'id'), ($object->ts_category_okp)?$object->ts_category_okp:0, ['class' => 'form-control is_validate select2-ws', 'id'=>"object_ts_category_okp_0"])}}
    </div>

    <div class="col-md-6 col-lg-3" >
        <label class="control-label">Марка <span class="required">*</span></label>
        {{Form::select("contract[object][mark_id]", \App\Models\Vehicle\VehicleMarks::orderBy('title')->get()->pluck('title', 'id')->prepend('Не выбрано', 0), $object->mark_id, ['class' => 'is_validate mark-id mark_id select2-all', "id"=>"object_ts_mark_id_0", 'style'=>'width: 100%;', 'onchange'=>"getModelsObjectInsurer(0, 0);"])}}
    </div>

    <div class="col-md-6 col-lg-3" >
        <label class="control-label">Модель <span class="required">*</span></label>
        {{Form::select("contract[object][model_id]", [], $object->model_id, ['class' => 'is_validate model_id model-id select2-all', "id"=>"object_ts_model_id_0", 'style'=>'width: 100%;'])}}
    </div>





    <div class="clear"></div>


    <div class="col-md-4 col-lg-3" >
        <label class="control-label">VIN <span class="required">*</span></label>
        {{ Form::text("contract[object][vin]", $object->vin, ['class' => 'form-control is_validate', "id"=>"object_ts_vin_0", 'placeholder' => 'KL1UF756E6B195928']) }}
    </div>

    <div class="col-md-4 col-lg-3" >
        <label class="control-label">Номер кузова</label>
        {{ Form::text("contract[object][body_number]", $object->body_number, ['class' => 'form-control', 'placeholder' => '235347453234']) }}
    </div>

    <div class="col-md-4 col-lg-3" >
        <label class="control-label">Шасси (Рама)</label>
        {{ Form::text("contract[object][body_chassis]", $object->body_chassis, ['class' => 'form-control', 'placeholder' => 'KL1UF756E6B195928']) }}
    </div>


    <div class="col-md-4 col-lg-3" >
        <label class="control-label">Рег. номер</label>
        {{ Form::text("contract[object][reg_number]", $object->reg_number, ['class' => 'form-control ru_sumb', "id"=>"object_ts_reg_number_0", 'placeholder' => 'Е050КХ99']) }}
    </div>

    <div class="clear"></div>


    <div class="col-md-6 col-lg-3" >
        <label class="control-label">Год выпуска <span class="required">*</span></label>
        {{Form::text("contract[object][car_year]", $object->car_year ? $object->car_year : "", ['class' => 'form-control is_validate','id' => 'carYear', 'placeholder' => date("Y")])}}
    </div>

    <div class="col-md-6 col-lg-3">
        <label class="control-label">Тип топлива<span class="required">*</span></label>
        {{Form::select("contract[object][type_engine]", \App\Models\Contracts\ObjectInsurerAuto::TYPE_ENGINE, $object->type_engine, ['class' => 'form-control is_validate select2-ws'])}}
    </div>

    <div class="col-md-6 col-lg-3">
        <label class="control-label">Тип тормозной системы <span class="required">*</span></label>
        {{Form::select("contract[object][brake_system_id]",\App\Models\Contracts\ObjectInsurerAuto::BRAKE_SYSTEM, $object->brake_system_id, ['class' => 'form-control is_validate select2-ws'])}}
    </div>

    <div class="col-md-6 col-lg-3" >
        <label class="control-label">Марка шин <span class="required">*</span></label>
        {{ Form::text("contract[object][brand_tire]", $object->brand_tire, ['class' => 'form-control is_validate', 'placeholder' => '']) }}
    </div>


    <div class="clear"></div>

    <div class="col-md-6 col-lg-3" >
        <label class="control-label">Пробег <span class="required">*</span></label>
        {{Form::text("contract[object][mileage]", $object->mileage ? $object->mileage : "", ['class' => 'form-control is_validate sum'])}}
    </div>

    <div class="col-md-6 col-lg-3" >
        <label class="control-label">Масса без нагрузки <span class="required">*</span></label>
        {{ Form::text("contract[object][weight]", $object->weight, ['class' => 'form-control is_validate sum', 'placeholder' => '']) }}
    </div>

    <div class="col-md-6 col-lg-3" >
        <label class="control-label">Максимальная масса <span class="required">*</span></label>
        {{ Form::text("contract[object][max_weight]", $object->max_weight, ['class' => 'form-control is_validate sum', 'placeholder' => '']) }}
    </div>



    <div class="clear"></div>


    <div class="col-lg-12" >
        <h4>Документы ТС</h4>
    </div>

    <div class="col-lg-3" >
        <label class="control-label">Тип документа</label>
        {{Form::select("contract[object][doc_type]", [0=>'Паспорт транспортного средства', 1=>'Свидетельство о регистрации'], $object->doc_type, ['class' => 'form-control select2-ws', 'style'=>'width: 100%;'])}}
    </div>

    <div class="col-md-4 col-lg-3" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Серия <span class="required">*</span>
                </label>
                {{ Form::text('contract[object][docserie]', $object->docserie, ['class' => 'form-control is_validate', 'placeholder' => '38MB']) }}
            </div>
        </div>
    </div>


    <div class="col-md-4 col-lg-3" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Номер <span class="required">*</span>
                </label>
                {{ Form::text('contract[object][docnumber]', $object->docnumber, ['class' => 'form-control is_validate', 'placeholder' => '587123']) }}
            </div>
        </div>
    </div>


    <div class="col-md-4 col-lg-3" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Дата выдачи <span class="required">*</span>
                </label>
                {{ Form::text('contract[object][docdate]', getDateFormatRu($object->docdate), ['class' => 'form-control is_validate format-date end-date', 'placeholder' => '18.04.2012']) }}
                <span class="glyphicon glyphicon-calendar calendar-icon"></span>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-lg-12" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Кем выдан
                </label>
                {{ Form::text('contract[object][docissued]', $object->docissued, ['class' => 'form-control', 'placeholder' => 'ГАИ']) }}
            </div>
        </div>
    </div>


    <div class="clear"></div>



</div>
</div>

<script>

    function initCar() {

        @if($object->model_id > 0)
            getModelsObjectInsurer(0, {{$object->model_id}});
        @else
            getModelsObjectInsurer(0, 0);
        @endif


    }

    function getAjaxTsCategoryOKP()
    {
        getTsCategoryOKP($("#object_ts_category_0").val(), $("#object_ts_category_okp_0").val());
    }





</script>