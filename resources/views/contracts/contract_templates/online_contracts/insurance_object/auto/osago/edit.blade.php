<div class="block-view">
<h3>Транспортное средство</h3>
<div class="row">
    <div class="col-md-6 col-lg-3">
        <label class="control-label">Категория</label>
        {{Form::select("contract[object][ts_category]", \App\Models\Vehicle\VehicleCategories::query()->get()->pluck('title', 'id'), ($object->ts_category)?$object->ts_category:2, ['class' => 'form-control', 'id'=>"object_ts_category_0", 'onchange'=>"getModelsObjectInsurer(0, 0);"])}}
    </div>

    <div class="col-md-6 col-lg-3" >
        <label class="control-label">Марка</label>
        {{Form::select("contract[object][mark_id]", \App\Models\Vehicle\VehicleMarks::orderBy('title')->get()->pluck('title', 'id')->prepend('Не выбрано', 0), $object->mark_id, ['class' => 'mark-id mark_id select2-all', "id"=>"object_ts_mark_id_0", 'style'=>'width: 100%;', 'onchange'=>"getModelsObjectInsurer(0, 0);"])}}
    </div>

    <div class="col-md-6 col-lg-3" >
        <label class="control-label">Модель</label>
        {{Form::select("contract[object][model_id]", [], $object->model_id, ['class' => 'model_id model-id select2-all', "id"=>"object_ts_model_id_0", 'style'=>'width: 100%;'])}}
    </div>

    <div class="col-md-6 col-lg-3" >
        <label class="control-label">Год выпуска</label>
        {{Form::text("contract[object][car_year]", $object->car_year ? $object->car_year : "", ['class' => 'form-control','id' => 'carYear', 'style'=>'width: 100%;', 'placeholder' => '2018'])}}
    </div>



    <div class="clear"></div>

    <div class="col-lg-3" >
        <label class="control-label">Цель использования</label>
        {{Form::select("contract[object][purpose_id]", \App\Models\Vehicle\VehiclePurpose::all()->pluck('title', 'id'), $object->purpose_id, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
    </div>
    <div class="col-md-4 col-lg-3" >
        <label class="control-label">VIN</label>
        {{ Form::text("contract[object][vin]", $object->vin, ['class' => 'form-control vin_validation', "id"=>"object_ts_vin_0", 'placeholder' => 'KL1UF756E6B195928']) }}
    </div>

    <div class="col-md-4 col-lg-3" >
        <label class="control-label">Номер кузова</label>
        {{ Form::text("contract[object][body_number]", $object->body_number, ['class' => 'form-control', 'placeholder' => '235347453234']) }}
    </div>


    <div class="col-md-4 col-lg-3" >
        <label class="control-label">Рег. номер</label>
        @php
            $ruSumb = $object->foreign_reg_number ? '' : 'ru_sumb';
        @endphp
        {{ Form::text("contract[object][reg_number]", $object->reg_number, ['class' => "form-control {$ruSumb}", "id"=>"object_ts_reg_number_0", 'placeholder' => 'е050КХ99']) }}
        <label class="control-label" style="max-width:100%;">Иностранный номер</label>
        {{ Form::checkbox('contract[object][foreign_reg_number]', 1, $object->foreign_reg_number ? true : false, ['style' => 'width:18px;height:18px;margin-left:5px;position:absolute;']) }}
        <br>
        <label class="control-label" style="max-width:100%;">Транзитный номер</label>
        {{ Form::checkbox('contract[object][transit_number]', 1, $object->transit_number ? true : false, ['style' => 'width:18px;height:18px;margin-left:5px;position:absolute;']) }}
    </div>

    <div class="clear"></div>



    <div class="col-md-6 col-lg-3" >
        <label class="control-label">Мощность (л.с.)</label>
        {{ Form::text("contract[object][power]", $object->power, ['class' => 'form-control sum', "id"=>"object_ts_power_0", 'placeholder' => '145']) }}
    </div>


    {{--- Доп условия для типов автомобилей ----}}

    <div class="col-md-6 col-lg-3" id="object_passengers_count_0">
        <label class="control-label">Кол-во мест</label>
        {{ Form::text("contract[object][passengers_count]", $object->passengers_count, ['class' => 'form-control', 'placeholder' => '5']) }}
    </div>

    <div class="col-md-6 col-lg-3" id="object_weight_0">
        <label class="control-label">Масса</label>
        {{ Form::text("contract[object][weight]", $object->weight, ['class' => 'form-control', 'placeholder' => '1500']) }}
    </div>


    <div class="col-md-6 col-lg-3" id="object_capacity_0">
        <label class="control-label">Грузоподъемность</label>
        {{ Form::text("contract[object][capacity]", $object->capacity, ['class' => 'form-control', 'placeholder' => '900']) }}
    </div>

    {{--- end ---}}
    <div class="clear"></div>

    <div class="col-lg-6" >
        <label  class="control-label" style="max-width:100%;">Автомобиль используется с прицепом</label>
        {{ Form::checkbox('contract[object][is_trailer]', 1, $object->is_trailer,['style' => 'width:18px;height:18px;margin-left:5px;position:absolute;']) }}
    </div>

    <div class="clear"></div>

    <div class="col-lg-6" >
        <label  class="control-label" style="max-width:100%;">Автомобиль новый</label>
        {{ Form::checkbox('contract[object][is_new]', 1, $object->is_new, ['style' => 'width:18px;height:18px;margin-left:5px;position:absolute;']) }}
    </div>

    <div class="clear"></div>

    <div class="col-lg-12" >
        <h4>Документы ТС</h4>
    </div>

    <div class="col-lg-3" >
        <label class="control-label">Тип документа</label>
        {{Form::select("contract[object][doc_type]", [0=>'Паспорт транспортного средства', 1=>'Свидетельство о регистрации', 2 => 'Электронный ПТС'], $object->doc_type, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
    </div>

    <div class="col-md-4 col-lg-3" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Серия <span class="required">*</span>
                </label>
                {{ Form::text('contract[object][docserie]', $object->docserie, ['class' => 'form-control ru_sumb', 'placeholder' => '38MB']) }}
            </div>
        </div>
    </div>


    <div class="col-md-4 col-lg-3" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Номер <span class="required">*</span>
                </label>
                {{ Form::text('contract[object][docnumber]', $object->docnumber, ['class' => 'form-control ru_sumb', 'placeholder' => '587123']) }}
            </div>
        </div>
    </div>


    <div class="col-md-4 col-lg-3" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Дата выдачи <span class="required">*</span>
                </label>
                {{ Form::text('contract[object][docdate]', getDateFormatRu($object->docdate), ['class' => 'form-control format-date end-date', 'placeholder' => '18.04.2012']) }}
                <span class="glyphicon glyphicon-calendar calendar-icon"></span>
            </div>
        </div>
    </div>

    <div class="clear"></div>
    <div class="col-lg-12" >
        <h4>Документ ДК</h4>
    </div>


    <div class="col-md-6 col-lg-3" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Номер ДК
                </label>
                {{ Form::text('contract[object][dk_number]', $object->dk_number, ['class' => 'form-control', 'placeholder' => '3456342']) }}
            </div>
        </div>
    </div>

    <div class="col-md-6 col-lg-3" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Дата начала <span class="required">*</span>
                </label>
                {{ Form::text('contract[object][dk_date_from]', getDateFormatRu($object->dk_date_from), ['class' => 'form-control format-date end-date', 'placeholder' => '13.02.2018']) }}
                <span class="glyphicon glyphicon-calendar calendar-icon"></span>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-lg-3" >
        <div class="field form-col">
            <div>
                <label class="control-label">
                    Дата окончания <span class="required">*</span>
                </label>
                {{ Form::text('contract[object][dk_date]', getDateFormatRu($object->dk_date), ['class' => 'form-control format-date end-date', 'placeholder' => '13.02.2019']) }}
                <span class="glyphicon glyphicon-calendar calendar-icon"></span>
            </div>
        </div>
    </div>

    @if(isset($forUnderfill) && $forUnderfill)
        <div class="col-lg-12">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Примечание
                    </label>
                    {{ Form::text('contract[object][comment]', $object->comment, ['class' => 'form-control', 'placeholder' => 'Сообщение..']) }}
                </div>
            </div>
        </div>
    @endif
</div>
</div>

<script>

    function initCar() {

        @if($object->model_id > 0)
            getModelsObjectInsurer(0, {{$object->model_id}});
        @else
            getModelsObjectInsurer(0, 0);
        @endif

    }

</script>