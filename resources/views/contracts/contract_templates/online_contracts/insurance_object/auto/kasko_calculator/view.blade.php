<div class="block-view">
    <div class="block-sub">
        <h3>Транспортное средство</h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Категория</span>
                    <span class="view-value">{{ $object->category_auto ? $object->category_auto->title : "" }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Марка</span>
                    <span class="view-value">{{ $object->mark ? $object->mark->title : "" }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Модель</span>
                    <span class="view-value">{{ $object->model ? $object->model->title : "" }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Год выпуска</span>
                    <span class="view-value">{{ $object->car_year }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Цель использования</span>
                    <span class="view-value">{{ $object->target_use? $object->target_use->title : "" }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">VIN</span>
                    <span class="view-value">{{ $object->vin }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Номер кузова</span>
                    <span class="view-value">{{ $object->body_number }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Рег. номер</span>
                    <span class="view-value">{{ $object->reg_number }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Мощность (л.с.)</span>
                    <span class="view-value">{{ $object->power }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Кол-во мест</span>
                    <span class="view-value">{{ $object->passengers_count }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Масса</span>
                    <span class="view-value">{{ $object->weight }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Грузоподъемность</span>
                    <span class="view-value">{{ $object->capacity }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Автомобиль используется с прицепом</span>
                    <span class="view-value">{{ $object->is_trailer ? "Да" : "Нет"}}</span>
                </div>
            </div>


            <div class="col-lg-12">
                <h4>Документы ТС</h4>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Тип документа</span>
                    <span class="view-value">{{ $object->doc_type == 0 ? 'Паспорт транспортного средства' : 'Свидетельство о регистрации' }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Серия</span>
                    <span class="view-value">{{ $object->docserie }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Номер</span>
                    <span class="view-value">{{ $object->docnumber }}</span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label">Дата выдачи</span>
                    <span class="view-value">{{ getDateFormatRu($object->docdate) }}</span>
                </div>
            </div>

            <div class="col-lg-12" >
                <h4>Документ ТО</h4>
            </div>



            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label"> Номер диагностической карты </span>
                    <span class="view-value">{{ $object->dk_number }}</span>
                </div>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="view-field">
                    <span class="view-label"> Дата очередного ТО</span>
                    <span class="view-value">{{ getDateFormatRu($object->dk_date) }}</span>
                </div>
            </div>


        </div>
    </div>
</div>

