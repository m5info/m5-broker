<div class="block-view">
<h3>Транспортное средство</h3>
<div class="row">
    <div class="col-lg-3">
        <label class="control-label">Категория</label>
        {{Form::select("contract[object][ts_category]", \App\Models\Vehicle\VehicleCategories::query()->get()->pluck('title', 'id'), ($object->ts_category)?$object->ts_category:2, ['class' => 'form-control', 'id'=>"object_ts_category_0", 'onchange'=>"getModelsObjectInsurer(0, 0);"])}}
    </div>

    <div class="col-lg-3" >
        <label class="control-label">Марка</label>
        {{Form::select("contract[object][mark_id]", \App\Models\Vehicle\VehicleMarks::orderBy('title')->get()->pluck('title', 'id')->prepend('Не выбрано', 0), $object->mark_id, ['class' => 'mark-id mark_id select2-all', "id"=>"object_ts_mark_id_0", 'style'=>'width: 100%;', 'onchange'=>"getModelsObjectInsurer(0, 0);"])}}
    </div>

    <div class="col-lg-3" >
        <label class="control-label">Модель</label>
        {{Form::select("contract[object][model_id]", [], $object->model_id, ['class' => 'model_id model-id select2-all', "id"=>"object_ts_model_id_0", 'style'=>'width: 100%;'])}}
    </div>

    <div class="col-lg-3" >
        <label class="control-label">Год выпуска</label>
        {{Form::text("contract[object][car_year]", $object->car_year ? $object->car_year : date("Y"), ['class' => 'form-control','id' => 'carYear', 'style'=>'width: 100%;', 'placeholder' => '2018'])}}
    </div>



    <div class="clear"></div>

    <div class="col-lg-3" >
        <label class="control-label">Цель использования</label>
        {{Form::select("contract[object][purpose_id]", \App\Models\Vehicle\VehiclePurpose::all()->pluck('title', 'id'), $object->purpose_id, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
    </div>
    <div class="col-lg-3" >
        <label class="control-label">VIN</label>
        {{ Form::text("contract[object][vin]", $object->vin, ['class' => 'form-control', "id"=>"object_ts_vin_0", 'placeholder' => 'KL1UF756E6B195928']) }}
    </div>

    <div class="col-lg-3" >
        <label class="control-label">Номер кузова</label>
        {{ Form::text("contract[object][body_number]", $object->body_number, ['class' => 'form-control', 'placeholder' => '235347453234']) }}
    </div>


    <div class="col-lg-3" >
        <label class="control-label">Рег. номер</label>
        {{ Form::text("contract[object][reg_number]", $object->reg_number, ['class' => 'form-control ru_sumb', "id"=>"object_ts_reg_number_0", 'placeholder' => 'е050КХ99']) }}
    </div>




    <div class="clear"></div>



    <div class="col-lg-3">
        <label class="control-label">Стоимость ТС</label>
        {{ Form::text("contract[object][price]", ($object->price)?titleFloatFormat($object->price):'', ['class' => 'form-control sum']) }}
    </div>

    <div class="col-lg-3" >
        <label class="control-label">Противоугонное устройство</label>
        {{Form::select("contract[object][anti_theft_system_id]", \App\Models\Vehicle\VehicleAntiTheftSystem::query()->get()->pluck('title', 'id')->prepend('Не выбрано', 0), $object->anti_theft_system_id, ['class' => 'select2-all', 'style'=>'width: 100%;'])}}
    </div>


    <div class="col-lg-3">
        <label class="control-label">Цвет</label>
        {{Form::select("contract[object][color_id]", \App\Models\Vehicle\VehicleColor::query()->get()->pluck('title', 'id')->prepend('Не выбрано', 0), $object->color_id, ['class' => 'select2-all', 'style'=>'width: 100%;'])}}
    </div>


    <div class="col-lg-3" >
        <label class="control-label">Мощность (л.с.)</label>
        {{ Form::text("contract[object][power]", $object->power, ['class' => 'form-control sum', 'placeholder' => '145']) }}
    </div>


    <div class="clear"></div>


    <div class="col-lg-3" id="object_weight_0">
        <label class="control-label">Масса</label>
        {{ Form::text("contract[object][weight]", $object->weight, ['class' => 'form-control', 'placeholder' => '1500']) }}
    </div>


    <div class="col-lg-3" id="object_capacity_0">
        <label class="control-label">Грузоподъемность</label>
        {{ Form::text("contract[object][capacity]", $object->capacity, ['class' => 'form-control', 'placeholder' => '900']) }}
    </div>


    <div class="col-lg-3" id="object_passengers_count_0">
        <label class="control-label">Кол-во мест</label>
        {{ Form::text("contract[object][passengers_count]", $object->passengers_count, ['class' => 'form-control', 'placeholder' => '5']) }}
    </div>



</div>
</div>

<script>

    function initCar() {



        @if($object->model_id > 0)
            getModelsObjectInsurer(0, {{$object->model_id}});
        @else
            getModelsObjectInsurer(0, 0);
        @endif

    }

</script>