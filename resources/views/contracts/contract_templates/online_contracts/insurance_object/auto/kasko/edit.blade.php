<div class="block-view">
    <h3>Транспортное средство</h3>
    <div class="row">
        <input type="text" hidden id="object_id" value="{{$object->object_insurer_id}}">

        <input type="text" hidden id="count_object_id" value="{{$object->object_insurer_id}}">
        <div class="col-md-6 col-lg-3">
            <label class="control-label">Категория</label>
            {{Form::select("contract[object][ts_category]", \App\Models\Vehicle\VehicleCategories::query()->get()->pluck('title', 'id'), ($object->ts_category)?$object->ts_category:2, ['class' => 'form-control select2-ws', 'id'=>"object_ts_category_0", 'onchange'=>"getClassificationObjectInsurer(0, 0);getModelsObjectInsurer(0, 0);"])}}
        </div>

        <div class="col-md-6 col-lg-3">
            <label class="control-label">Марка</label>
            {{Form::select("contract[object][mark_id]", \App\Models\Vehicle\VehicleMarks::orderBy('title')->get()->pluck('title', 'id')->prepend('Не выбрано', 0), $object->mark_id, ['class' => 'mark-id mark_id select2-all', "id"=>"object_ts_mark_id_0", 'style'=>'width: 100%;', 'onchange'=>"getModelsObjectInsurer(0, 0);"])}}
        </div>

        <div class="col-md-6 col-lg-3">
            <label class="control-label">Модель</label>
            {{Form::select("contract[object][model_id]", [], $object->model_id, ['class' => 'model_id model-id select2-all', "id"=>"object_ts_model_id_0", 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-md-6 col-lg-3">
            <label class="control-label">Модификация</label>
            {{Form::text("contract[object][modification]", $object->modification, ['class' => 'form-control','id' => 'modification', 'style'=>'width: 100%;', 'placeholder' => ''])}}
        </div>


        <div class="clear"></div>

        <div class="col-lg-3">
            <label class="control-label">Классификация</label>
            {{Form::select("contract[object][classification_id]", [], $object->classification_id, ['class' => 'form-control select2-all', "id"=>"classification_id_0"])}}
        </div>


        <div class="col-lg-3">
            <label class="control-label">Тип двигателя</label>
            {{Form::select("contract[object][type_engine]", \App\Models\Contracts\ObjectInsurerAuto::TYPE_ENGINE, $object->type_engine, ['class' => 'form-control select2-ws'])}}
        </div>

        <div class="col-lg-3">
            <label class="control-label">Тип коробки</label>
            {{Form::select("contract[object][type_kpp]", \App\Models\Contracts\ObjectInsurerAuto::TYPE_KPP, $object->type_kpp, ['class' => 'form-control select2-ws'])}}
        </div>

        <div class="col-lg-3">
            <label class="control-label" style="max-width: 300px;">
                Год выпуска - Лет эксплуатации - Новое
                {{ Form::checkbox('contract[object][is_new]', 1, 0, ['style' => 'width:18px;height:18px;margin-left:5px;margin-top:-5px;position:absolute;']) }}
            </label><br>

            <div class="row col-md-12 col-lg-12">
                <div class="row col-md-6 col-lg-6">
                    {{Form::text("contract[object][car_year]", $object->car_year ? $object->car_year : "", ['class' => 'form-control','id' => 'carYear', 'onkeyup' => 'counting_years_of_exp(this)'])}}
                </div>
                <div class="col-md-6 col-lg-6">
                    {{Form::text("contract[object][car_year_exp]", $object->car_year_exp ? $object->car_year_exp : "", ['class' => 'form-control', 'id' => 'carYearExp'])}}
                </div>
            </div>

        </div>


        <div class="clear"></div>







        <div class="col-md-4 col-lg-3">
            <label class="control-label">
                Объем (СМЗ)
            </label>
            {{ Form::text('contract[object][volume]', titleFloatFormat($object->volume), ['class' => 'form-control sum']) }}
        </div>

        <div class="col-lg-3">
            <label class="control-label" style="max-width: 300px;">Мощность (Л.С. - КВТ)</label><br/>

            <div class="row col-md-12 col-lg-12">
                <div class="row col-md-6 col-lg-6">
                    {{ Form::text("contract[object][power]", (int)$object->power, ['class' => 'form-control', "id"=>"ts_power", "onkeyup"=>"var int = Math.round($('#ts_power').val()/1.36); if(!isNaN(int)) $('#ts_powerkw').val(int);"]) }}
                </div>
                <div class="col-md-6 col-lg-6">
                    {{ Form::text("contract[object][kv]", (int)$object->kv, ['class' => 'form-control', "id"=>"ts_powerkw", ]) }}
                </div>
            </div>
        </div>



        <div class="col-md-4 col-lg-3">
            <label class="control-label">
                Пробег
            </label>
            {{ Form::text('contract[object][mileage]', titleFloatFormat($object->mileage), ['class' => 'form-control sum']) }}
        </div>


        <div class="col-lg-3">
            <label class="control-label">Цель использования</label>
            {{Form::select("contract[object][purpose_id]", \App\Models\Vehicle\VehiclePurpose::all()->pluck('title', 'id'), $object->purpose_id, ['class' => 'form-control select2-ws'])}}
        </div>



        <div class="clear"></div>

        <div class="col-md-4 col-lg-3">
            <label class="control-label">VIN</label>
            {{ Form::text("contract[object][vin]", $object->vin, ['class' => 'form-control', "id"=>"object_ts_vin_0", 'placeholder' => 'KL1UF756E6B195928']) }}
        </div>

        <div class="col-md-4 col-lg-3">
            <label class="control-label">Номер кузова <a href="javascript:void(0);" class="btn-xs btn-primary copy_vin inline" data-input="contract[object][body_number]">Копировать VIN</a></label>
            {{ Form::text("contract[object][body_number]", $object->body_number, ['class' => 'form-control', 'placeholder' => '']) }}
        </div>



        <div class="col-md-4 col-lg-3">
            <label class="control-label">Номер шасси <a href="javascript:void(0);" class="btn-xs btn-primary copy_vin inline" data-input="contract[object][body_chassis]">Копировать
                    VIN</a></label>
            {{ Form::text("contract[object][body_chassis]", $object->body_chassis, ['class' => 'form-control', 'placeholder' => '']) }}
        </div>

        <div class="col-md-4 col-lg-3">
            <label class="control-label" style="max-width: 300px;">Номер двигателя</label>
            {{ Form::text("contract[object][body_engine]", $object->body_engine, ['class' => 'form-control', 'placeholder' => '']) }}
        </div>




        <div class="clear"></div>

        <div class="col-lg-3">
            <label class="control-label">Стоимость ТС</label>
            {{ Form::text("contract[object][price]", ($object->price)?titleFloatFormat($object->price):'', ['class' => 'form-control sum', 'id'=>'ts_price']) }}
        </div>

        <div class="col-lg-3">
            <label class="control-label">Противоугонное устройство</label>
            {{Form::select("contract[object][anti_theft_system_id]", \App\Models\Vehicle\VehicleAntiTheftSystem::query()->get()->pluck('title', 'id')->prepend('Не выбрано', 0), $object->anti_theft_system_id, ['class' => 'select2-all', 'style'=>'width: 100%;'])}}
        </div>



        <div class="col-lg-3">
            <label class="control-label">Цвет</label>
            {{Form::select("contract[object][color_id]", \App\Models\Vehicle\VehicleColor::query()->get()->pluck('title', 'id')->prepend('Не выбрано', 0), $object->color_id, ['class' => 'select2-all', 'style'=>'width: 100%;'])}}
        </div>


        <div class="col-md-3 col-lg-3">
            <div class="row col-md-6 col-lg-6">
                <label class="control-label">Дверей (шт.)</label>
                {{Form::text("contract[object][doors]", $object->doors, ['class' => 'form-control','id' => 'carYearDoors', 'style'=>'width: 100%;'])}}
            </div>
            <div class="col-md-6 col-lg-6">
                <label class="control-label">Ключей (шт.)</label>
                {{Form::text("contract[object][keys]", $object->keys, ['class' => 'form-control','id' => 'carYearKeys', 'style'=>'width: 100%;'])}}
            </div>
        </div>




        <div class="clear"></div>



        <div class="col-lg-3" id="object_weight_0">
            <label class="control-label">Масса</label>
            {{ Form::text("contract[object][weight]", $object->weight, ['class' => 'form-control', 'placeholder' => '1500']) }}
        </div>


        <div class="col-lg-3" id="object_capacity_0">
            <label class="control-label">Грузоподъемность</label>
            {{ Form::text("contract[object][capacity]", $object->capacity, ['class' => 'form-control', 'placeholder' => '900']) }}
        </div>


        <div class="col-lg-3" id="object_passengers_count_0">
            <label class="control-label">Кол-во мест</label>
            {{ Form::text("contract[object][passengers_count]", $object->passengers_count, ['class' => 'form-control', 'placeholder' => '5']) }}
        </div>






        <div class="clear"></div>

        <div class="col-lg-12">
            <h4>Документы ТС</h4>
            <input type="hidden" name="contract[object][doc_type]" value="0"/>
        </div>

        <div class="col-md-4 col-lg-3">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        СТС Серия
                    </label>
                    {{ Form::text('contract[object][sts_docserie]', $object->sts_docserie, ['class' => 'form-control', 'placeholder' => '38MB']) }}
                </div>
            </div>
        </div>


        <div class="col-md-4 col-lg-3">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        СТС Номер
                    </label>
                    {{ Form::text('contract[object][sts_docnumber]', $object->sts_docnumber, ['class' => 'form-control']) }}
                </div>
            </div>
        </div>


        <div class="col-md-4 col-lg-3">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        СТС Дата выдачи
                    </label>
                    {{ Form::text('contract[object][sts_docdate]', getDateFormatRu($object->sts_docdate), ['class' => 'form-control format-date end-date ru_sumb']) }}
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-lg-3">
            <label class="control-label" style="min-width: 300px;">Рег. номер - Иностранный номер
                {{ Form::checkbox('contract[object][foreign_reg_number]', 1, $object->foreign_reg_number ? true : false, ['style' => 'width:18px;height:18px;margin-left:5px;margin-top:-5px;position:absolute;']) }}
            </label>
            @php
                $ruSumb = $object->foreign_reg_number ? '' : 'ru_sumb';
            @endphp
            {{ Form::text("contract[object][reg_number]", $object->reg_number, ['class' => "form-control {$ruSumb}", "id"=>"object_ts_reg_number_0", 'placeholder' => 'е050КХ99']) }}

        </div>

        <div class="clear"></div>


        <div class="col-md-4 col-lg-3">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        ПТС Тип
                    </label>

                    {{Form::select("contract[object][doc_kind_pts]", collect(\App\Models\Contracts\ObjectInsurerAuto::DOC_KIND_PTS), $object->doc_kind_pts, ['class' => 'form-control select2-ws'])}}


                </div>
            </div>
        </div>

        <div class="col-md-4 col-lg-3">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        ПТС Серия
                    </label>
                    {{ Form::text('contract[object][docserie]', $object->docserie, ['class' => 'form-control', 'placeholder' => '38MB']) }}
                </div>
            </div>
        </div>


        <div class="col-md-4 col-lg-3">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        ПТС Номер
                    </label>
                    {{ Form::text('contract[object][docnumber]', $object->docnumber, ['class' => 'form-control', 'placeholder' => '587123']) }}
                </div>
            </div>
        </div>


        <div class="col-md-4 col-lg-3">
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        ПТС Дата выдачи
                    </label>
                    {{ Form::text('contract[object][docdate]', getDateFormatRu($object->docdate), ['class' => 'form-control format-date end-date']) }}
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>

        @if(isset($forUnderfill) && $forUnderfill)
            <div class="clear"></div>
            <div class="col-lg-12">
                <div class="field form-col">
                    <div>
                        <label class="control-label">
                            Примечание
                        </label>
                        {{ Form::text('contract[object][comment]', $object->comment, ['class' => 'form-control', 'placeholder' => 'Сообщение..']) }}
                    </div>
                </div>
            </div>
        @endif
        <div class="clear"></div>

        <div class="col-lg-12">
            <h4>Дополнительное оборудование</h4>
            <br>
            <div id="optional_equipment" style="padding: 0 20px 0 20px;">

            </div>
            <p></p>
            <div style="padding-top:10px;">
                <a href="javascript:void(0);" class="btn btn-primary inline-block" title="Добавить водителя"
                   onclick="addEquipment();">
                    Добавить</a>
            </div>

        </div>


    </div>
</div>

<script>


    function counting_years_of_exp() {
        year = $('#carYear').val();
        if (Number(year)) {
            result = new Date().getFullYear() - year;
            $('#carYearExp').val(result);
        }
    }


    function initCar() {

        @if($object->model_id > 0)
        getModelsObjectInsurer(0, {{$object->model_id}});
        getClassificationObjectInsurer(0, {{$object->classification_id}});
        @else
        getModelsObjectInsurer(0, 0);
        getClassificationObjectInsurer(0, 0);
        @endif

    }

</script>