@extends('layouts.frame')

@section('title')

    Выберите новую программу


@endsection

@section('content')


    @if(sizeof($programs))
        <table class="tov-table">

            @foreach($programs as $program)
                <tr style="cursor: pointer;" onclick="createNewProgramCalc('{{$program->id}}')">
                    <td>{{ $program->title }}</td>
                </tr>
            @endforeach

        </table>
    @else
        {{ trans('form.empty') }}
    @endif

    <script>

        function createNewProgramCalc(id) {
            loaderShow();

            $.post('{{url("/contracts/online/creare_new_calc/{$contract->id}/")}}', {program_id:id}, function (response) {

                if (Boolean(response.state) === true) {

                    window.parent_reload();

                } else {
                    flashHeaderMessage(response.msg, 'danger');
                }

            }).always(function () {
                loaderHide();
            });
        }

    </script>

@endsection

@section('footer')


@endsection

