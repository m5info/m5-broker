<div class="col-lg-12 driver driver-{{$i}}">
    {{ Form::hidden('contract[driver]['.$i.'][id]', $driver->id) }}
    <div class="row form-horizontal" style="border-bottom:1px dashed #c3c3c3;padding-top:5px;margin-top:5px;">
        <div class="col-md-12 col-lg-12">
            <h4 class="driver-title">Водитель {{$i}}</h4>
        </div>

        <div class="col-md-6 col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Возраст <span class="required">*</span>
                    </label>
                    {{ Form::text('contract[driver]['.$i.'][age]', $driver->age,
                    ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
        <div class="col-md-3 col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Стаж <span class="required">*</span>
                    </label>
                    {{ Form::text('contract[driver]['.$i.'][exp]', $driver->exp,
                    ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
        <div class="col-md-3 col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        &nbsp; <span class="required">&nbsp;</span>
                    </label>
                    <span class="btn btn-danger" style="width: 50px;" onclick="removeDriver({{$i}});" title="Удалить водителя">
                        <span class="glyphicon glyphicon-trash"></span>
                    </span>
                </div>
            </div>
        </div>

    </div>
</div>
