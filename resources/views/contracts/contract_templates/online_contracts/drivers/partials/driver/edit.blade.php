<div class="col-lg-12 driver driver-form driver-{{$i}}" data-key="{{$i}}">
    {{ Form::hidden('contract[driver]['.$i.'][id]', $driver->id) }}
    <div class="row form-horizontal" style="padding-top:5px;margin-top:5px;">
        <div class="col-md-6 col-lg-6">
            <h4 class="driver-title">Водитель {{$i}}</h4>
        </div>
        <div class="col-md-6 col-lg-6">
            <div style="width:50px;padding-top:10px;float:right;">
                <a href="javascript:void(0);" class="btn btn-danger" onclick="removeDriver({{$i}});" title="Удалить водителя">
                    <span class="glyphicon glyphicon-remove"></span>
                </a>
            </div>
        </div>
        <div class="col-md-12 col-lg-12">
            <div style="float: left">
                <div style="float: left">
                    <div style="float: left">
                        {{ Form::checkbox('contract[driver]['.$i.'][same_as_insurer]', 1, $driver->same_as_insurer,
                    ['style' => 'width:18px;height:18px;margin-left:5px;', 'id' => '#driver-insurer', 'onchange' => 'drivers_same_insurer()', 'data-key' => $i]) }}
                    </div>
                    <div style="padding-top: 7px;margin-left: 30px">
                        <span class="control-label" style="max-width:100%;">Водитель (Страхователь)</span>
                    </div>
                </div>
                <div style="float: right">
                    <div style="float: left">
                        {{ Form::checkbox('contract[driver]['.$i.'][foreign_docs]', 1, $driver->foreign_docs,
                    ['style' => 'width:18px;height:18px;margin-left:5px;', 'onchange' => 'toggleForeignDriverDocs(event)', 'id' => '#driver-foreign_docs', 'data-key' => $i]) }}
                    </div>
                    <div style="padding-top: 7px;margin-left: 30px">
                        <span class="control-label" style="max-width:100%;">Иностранные права</span>
                    </div>
                </div>
            </div>
            <div style="float: right">
                <div style="padding-top: 7px;margin-left: 30px">
                    <span class="control-label" style="max-width:100%;text-decoration: underline;cursor: pointer" id="previous_drivers_license_{{$i}}" data-key="{{$i}}">Информация по старым правам</span>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        ФИО <span class="required">*</span>
                    </label>
                    {{ Form::text('contract[driver]['.$i.'][fio]', $driver->fio,
                    ['class' => 'form-control', 'id'=>'driver_fio_'.$i, 'data-key'=>$i, 'placeholder' => 'Иванов Иван Иванович']) }}
                    {{ Form::hidden("contract[driver][$i][first_name]", $driver->first_name, ['id' => "driver_first_name_".$i]) }}
                    {{ Form::hidden("contract[driver][$i][second_name]", $driver->second_name, ['id' => "driver_second_name_".$i]) }}
                    {{ Form::hidden("contract[driver][$i][last_name]", $driver->last_name, ['id' => "driver_last_name_".$i]) }}
                </div>
            </div>
        </div>
        <div class="col-md-3 col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата рождения <span class="required">*</span>
                    </label>
                    {{ Form::text('contract[driver]['.$i.'][birth_date]', setDateTimeFormatRu($driver->birth_date),
                    ['class' => 'form-control format-date end-date','id' => 'driver_birthdate_'.$i, 'placeholder' => '14.06.1975']) }}
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Пол <span class="required">*</span>
                    </label>
                    {{Form::select('contract[driver]['.$i.'][sex]', collect([0=>"муж.", 1=>'жен.']), $driver->sex,
                    ['class' => 'form-control valid_fast_accept', 'id'=>'driver_sex_'.$i, 'data-key'=>$i]) }}
                </div>
            </div>
        </div>
        <div class="clear"></div>

        @php
            $lang_sumb_class = 'ru_sumb';

            if($driver->foreign_docs){
                $lang_sumb_class = 'en_sumb';
            }
        @endphp
        <div class="col-md-6 col-lg-2" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Серия В.У. <span class="required">*</span>
                    </label>
                    {{ Form::text('contract[driver]['.$i.'][doc_serie]', $driver->doc_serie,
                    ['class' => "form-control $lang_sumb_class", 'data-key'=>$i, 'id'=>'driver_doc_serie_'.$i, 'placeholder' => '1234']) }}
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-2" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Номер В.У. <span class="required">*</span>
                    </label>
                    {{ Form::text('contract[driver]['.$i.'][doc_num]', $driver->doc_num,
                    ['class' => "form-control $lang_sumb_class", 'data-key'=>$i, 'id'=>'driver_doc_num_'.$i, 'placeholder' => '567890']) }}
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        Дата выдачи В.У. <span class="required">*</span>
                    </label>
                    {{ Form::text('contract[driver]['.$i.'][doc_date]', setDateTimeFormatRu($driver->doc_date),
                    ['class' => 'form-control format-date', 'data-key'=>$i, 'id'=>'driver_doc_date_'.$i, 'placeholder' => '15.10.2001']) }}
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-lg-3" >
            <div class="field form-col">
                <div>
                    <label class="control-label" style="max-width: none;">
                        Начало водительского стажа <span class="required">*</span>
                    </label>
                    {{ Form::text('contract[driver]['.$i.'][exp_date]', setDateTimeFormatRu($driver->exp_date),
                    ['class' => 'form-control format-date', 'id'=>'driver_exp_date_'.$i, 'placeholder' => '14.06.1975']) }}
                    <span class="glyphicon glyphicon-calendar calendar-icon"></span>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-lg-1" >
            <div class="field form-col">
                <div>
                    <label class="control-label">
                        КБМ
                    </label>
                    {{ Form::text('contract[driver]['.$i.'][kbm]', $driver->kbm,
                    ['class' => 'form-control', 'readonly' => true]) }}

                    {{ Form::hidden('contract[driver]['.$i.'][class_kbm]', $driver->class_kbm,
                    ['class' => 'form-control']) }}

                    {{ Form::hidden('contract[driver]['.$i.'][kbm_rsa_request_id]', $driver->kbm_rsa_request_id,
                    ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('click', '#previous_drivers_license_{{$i}}', function () {
        var key = $(this).data('key');
        var driver_doc_num = $('#driver_doc_num_' + key).val() != '';
        var driver_doc_date = $('#driver_doc_date_' + key).val() != '';

        if (driver_doc_num && driver_doc_date){
            openFancyBoxFrame('{{url("contracts/online/driver_old_license_form/{$driver->id}")}}');
        } else{
            flashHeaderMessage('Заполните водителя №' + key + '!', 'danger');
        }
    });
</script>