@extends('layouts.frame')


@section('title')

    Предыдущие права/ФИО {{$driver->fio}}

@endsection

@section('content')


    {{ Form::open(['url' => url("contracts/online/driver_old_license_save/{$driver->id}"), 'id' => 'form_old_license', 'method' => 'post', 'class' => 'form-horizontal']) }}

        <div class="form-group">
            <label class="col-sm-3 control-label">Фамилия</label>
            <div class="col-sm-9">
                <input type="text" name="last_name" value="{{$driver_old_data ? $driver_old_data->last_name : $driver->last_name}}" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Имя</label>
            <div class="col-sm-9">
                <input type="text" name="first_name" value="{{$driver_old_data ? $driver_old_data->first_name : $driver->first_name}}" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Отчество</label>
            <div class="col-sm-9">
                <input type="text" name="middle_name" value="{{$driver_old_data ? $driver_old_data->middle_name : $driver->second_name}}" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Серия предыдущих прав</label>
            <div class="col-sm-9">
                <input type="text" name="doc_series" value="{{$driver_old_data ? $driver_old_data->doc_series : $driver->doc_serie}}" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Номер предыдущих прав</label>
            <div class="col-sm-9">
                <input type="text" name="doc_number" value="{{$driver_old_data ? $driver_old_data->doc_number : $driver->doc_num}}" class="form-control">
                <span id="doc_number_error" style="color: red"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Дата выдачи предыдущих прав</label>
            <div class="col-sm-9">
                <input type="text" name="doc_date_issue" value="{{$driver_old_data ? setDateTimeFormatRu($driver_old_data->doc_date_issue, 1) : setDateTimeFormatRu($driver->doc_date, 1)}}" class="form-control format-date">
                <span id="doc_date_issue_error" style="color: red"></span>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Дата окончания предыдущих прав</label>
            <div class="col-sm-9">
                <input type="text" name="doc_date_end" value="{{$driver_old_data ? setDateTimeFormatRu($driver_old_data->doc_date_end, 1) : ''}}" class="form-control format-date">
            </div>
        </div>

    {{Form::close()}}


@endsection

@section('footer')

    <span id="deleteOldData" type="button" class="btn btn-primary pull-left">{{ trans('form.buttons.delete') }}</span>
    <span id="updateOldData" type="button" class="btn btn-primary pull-right">{{ trans('form.buttons.save') }}</span>

@endsection

@section('js')
    <script src="/js/jquery.datetimepicker.full.min.js"></script>
    <link rel="stylesheet" href="/css/jquery.datetimepicker.min.css">
    <script>

        $(document).on('click', '#updateOldData', function(){
            if (validateInputs()){
                res = myPostAjax("/contracts/online/driver_old_license_save/{{$driver->id}}", $('#form_old_license').serialize());
                if (res.state == 1){
                    window.parent.jQuery.fancybox.close();
                    window.parent.flashHeaderMessage('Предыдущие права/ФИО сохранены!', 'success');
                }else{
                    window.parent.flashHeaderMessage('Есть ошибки в форме!', 'warning');
                }
            }
        });

        $(document).on('click', '#deleteOldData', function(){
            res = myPostAjax("/contracts/online/driver_old_license_delete/{{$driver->id}}", $('#form_old_license').serialize());
            window.parent.jQuery.fancybox.close();

        });

        $(function () {
            formatDate();
        });

        function validateInputs() {
            var doc_number = '{{$driver->doc_num}}';
            var doc_date_issue = Date.parse('{{$driver->doc_date}}');

            var previous_doc_number = $('[name="doc_number"]').val();
            var previous_doc_date_issue = Date.parse($('[name="doc_date_issue"]').val().split('.').reverse().join('-'));

            $('[name="doc_date_issue"]').keyup(function(){
                $('[name="doc_date_issue"]').css({'border-color' : ''})
                $('#doc_date_issue_error').text('');
            });
            $('[name="doc_number"]').keyup(function(){
                $('[name="doc_number"]').css({'border-color' : ''})
                $('#doc_number_error').text('');
            });
            var result = true;

            if (doc_number == previous_doc_number || previous_doc_number == ''){
                $('#doc_number_error').text('Укажите номер прав. Старый и новый номера прав не могут совпадать');
                $('[name="doc_number"]').css({'border-color' : 'red'});

                result = false;
            }

            if (isNaN(previous_doc_date_issue) || doc_date_issue <= previous_doc_date_issue){
                $('#doc_date_issue_error').text('Укажите дату. Старые права не могут быть с датой выдачи новых прав или новее старых');
                $('[name="doc_date_issue"]').css({'border-color' : 'red'});

                result = false;
            }
            return result;
        }
    </script>

@endsection
