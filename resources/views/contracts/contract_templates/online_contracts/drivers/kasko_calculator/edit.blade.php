<div class="block-view">

    <h3>Водители {{ Form::checkbox('contract[drivers_type]', 1, 0, ['id' => 'driver_type_checkbox', 'data-key'=>'0']) }} <label>Мультидрайв</label>
        <div class="col-lg-3 pull-right">

        </div>

    </h3>
    <div class="row">

        <input name="contract[drivers_count]" id="drivers_count" type="hidden" value="{{ sizeof($drivers) }}">

        @if(sizeof($drivers))
            @foreach($drivers as $driver)
                @include('contracts.contract_templates.online_contracts.drivers.partials.calc.edit', [
                    'i' => $loop->iteration,
                    'driver' => $driver,
                ])
            @endforeach
        @endif


        <div class="driver_limited col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none;">
            <div style="width:50px;padding-top:10px;">
                <a href="javascript:void(0);" class="btn btn-primary" title="Добавить водителя" onclick="addNewDriver();">
                    <span class="glyphicon glyphicon-plus"></span></a>
            </div>
        </div>




        <div class="driver_unlimited col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none;">
            <div class="row form-horizontal">
                <div class="col-lg-12">
                    <p>Договор будет заключен на условиях неограниченного списка лиц, допущенных к управлению</p>
                </div>
            </div>
        </div>
    </div>

</div>

<script>

    function activValidDrivers() {



    }

</script>