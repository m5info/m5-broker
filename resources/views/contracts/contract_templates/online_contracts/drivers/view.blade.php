<div class="block-view">

    <h3>Водители

    </h3>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="view-field">
                <span class="view-label">Тип водителя</span>
                <span class="view-value">{{collect(\App\Models\Contracts\Contracts::DRIVERS_TYPE)[$contract->drivers_type_id]}}</span>
            </div>
        </div>
    </div>
    <div class="row">

        <input name="contract[drivers_count]" id="drivers_count" type="hidden" readonly value="{{ sizeof($drivers) }}">

        @if(sizeof($drivers))
            @foreach($drivers as $driver)
                @include('contracts.contract_templates.online_contracts.subject.partials.edit.3', [
                    'i' => $loop->iteration,
                    'driver' => $driver,
                ])
            @endforeach
        @endif


        <div class="driver_limited col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none;">
            <div style="width:50px;padding-top:10px;">
                <a href="javascript:void(0);" class="btn btn-primary" title="Добавить водителя"
                   onclick="addNewDriver();">
                    <span class="glyphicon glyphicon-plus"></span></a>
            </div>
        </div>


        <div class="driver_unlimited col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none;">
            <div class="row form-horizontal">
                <div class="col-lg-12">
                    <p>Договор будет заключен на условиях неограниченного списка лиц, допущенных к управлению</p>
                </div>
            </div>
        </div>
    </div>

</div>