<style>
    .calendar-icon {
        margin-top: -25px;
        margin-right: 8px;
        float: right;
        color: #a3a3a3;
    }

    .menu-fixed {
        margin-left: -10px !important;
    }
</style>
<script src="/js/jquery.datetimepicker.full.min.js"></script>
<link rel="stylesheet" href="/css/jquery.datetimepicker.min.css">
<link rel="stylesheet" href="/css/online.css">


<script>

    $(function () {
        formatTime();
        formatDate();

        $(document).on('change', 'input, select', function () {
            $(this).removeClass('form-error');
        });

        $(document).on('keyup', 'input', function () {
            $(this).removeClass('form-error');
        });

        $(document).on('click', '.copy_vin', function () {
            vin = $('[name ="contract[object][vin]"]').val();
            el = $(this).data('input');
            $('[name = "' + el + '"]').val(vin);
        });

        $(document).on('keydown', '[name = "contract[object][power]"]', function () {
            var pre = $(this).val().toString();
            if (pre !== '0.00') {
                var int = Math.round(Number(pre) / 1.36);
                if (!isNaN(int)) {
                    $('[name = "contract[object][kv]"]').val(int);
                }
            }
        });
    });


    function get_end_dates(start_date) {
        var cur_date_tmp = start_date.split(".");
        var cur_date = new Date(cur_date_tmp[2], cur_date_tmp[1] - 1, cur_date_tmp[0]);
        var new_date = new Date(cur_date.setYear(cur_date.getFullYear() + 1));
        var new_date2 = new Date(new_date.setDate(new_date.getDate() - 1));
        return getFormattedDate(new_date2);
    }

    function setAllDates(key) {
        sign_date = $("#sign_date_" + key).val();
        $("#begin_date_" + key).val(sign_date);

        return setEndDates(key);
    }

    function setEndDates(key) {
        begin_date = $("#begin_date_" + key).val();
        $("#end_date_" + key).val(get_end_dates(begin_date));
    }


    function formatTime() {
        var configuration = {
            timepicker: true,
            datepicker: false,
            format: 'H:i',
            scrollInput: false
        };
        $.datetimepicker.setLocale('ru');
        $('input.format-time').datetimepicker(configuration).keyup(function (event) {
            if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 38 && event.keyCode != 40) {
                var pattern = new RegExp("[0-9:]{5}");
                if (pattern.test($(this).val())) {
                    $(this).datetimepicker('hide');
                    $(this).datetimepicker('show');
                }
            }
        });
        $('input.format-time').each(function () {
            var im = new Inputmask("99:99", {
                "oncomplete": function () {
                }
            });
            im.mask($(this));
        });
    }


    //ОСНОВНЫЕ ФУНКЦИИ

    //Сохраняем договор
    function saveContract(id, silence = false) {

        loaderShow();

        $.post('/contracts/online/save/' + id, $('#product_form').serialize(), function (response) {

            if (Boolean(response.state) === true) {
                if (silence === false){
                    flashMessage('success', "Данные успешно сохранены!");
                }
            }else {
                if(response.errors){
                    $.each(response.errors, function (index, value) {
                        if (silence === false) {
                            flashHeaderMessage(value, 'danger');
                        }
                        $('[name="' + index + '"]').addClass('form-error');
                    });
                }else{
                    if (silence === false){
                        flashHeaderMessage(response.msg, 'danger');
                    }
                }

            }

        }).always(function () {
            loaderHide();
        });

        return true;
    }

    function init_drivers_ids(id){

        $.post('/contracts/online/get_drivers/' + id, function (response) {

            $.each(response, function (index, value) {
                var i = index + 1;
                $('[name="contract[driver]['+ i +'][id]"]').val(value)
            });

        })

    }


    function recountKbms() {
        if ($('.driver').length > 0){
            $('.driver').each(function (i, k) {
                getKBMDriver(i+1);
            });
        }
    }


    function commonValidate() {

        if (!insurerFioValidate()) {
            return false;
        }
        if (!ownerFioValidate()) {
            return false;
        }
        if (!driversFioValidate()) {
            return false;
        }

        return true;
    }

    function get_calculate_sk(id) {

        if (!commonValidate()) {
            return;
        }

        recountKbms();

        $("#offers").html('');

        loaderShow();

        $.post('/contracts/online/save/' + id, $('#product_form').serialize(), function (response) {

            if (Boolean(response.state) === true) {

                calculations = myGetAjax('/contracts/online/get_calculate_list/' + id);
                for(i=0; i<calculations.length; i++){


                    $("#offers").append(myGetAjax('/contracts/online/get_calculate_sk/'+ calculations[i]));

                }

            } else {
                $.each(response.errors, function (index, value) {
                    flashHeaderMessage(value, 'danger');
                    $('[name="' + index + '"]').addClass('form-error');
                });
            }

        }).always(function () {
            loaderHide();
        });


        loaderShow();


        init_drivers_ids(id);


    }

    function refresh_calculate_id(calculation_id) {
        $.get('/contracts/online/calculate_sk/'+calculation_id, {}, function (response) {

            $('#calculation_'+calculation_id).html(response);

        }).always(function () {

            //Ошибка

            $("#loader_div_"+calculation_id).hide();
            $("#view_error_"+calculation_id).removeClass('hidden');

        });
    }


    function calculate(id) {

        loaderShow();

        $.post('/contracts/online/calculate/' + id, $('#product_form').serialize(), function (response) {

            var counter = 0;
            $.each(response.errors, function (index, value) {
                $('[name="' + index + '"]').addClass('form-error');

                if (counter < 2) {
                    flashHeaderMessage(value, 'danger');
                }
                counter++;
            });

            $('#offers').html(response.html);

            init_drivers_ids(id);

        }).always(function () {
            loaderHide();
        });


    }


    function tempCalculate(id) {
        loaderShow();

        $.post('/contracts/online/temp_calculate/' + id, $('#product_form').serialize(), function (response) {

            var counter = 0;
            $.each(response.errors, function (index, value) {
                $('[name="' + index + '"]').addClass('form-error');

                if (counter < 2) {
                    flashHeaderMessage(value, 'danger');
                }
                counter++;
            });

            $('#offers').html(response.html);

        }).always(function () {
            loaderHide();
        });
    }

    function releaseCalculate(id) {
        location.href = '/contracts/online/release_calculate/?id=' + id;
    }


    function setSKCalc(calc_id) {
        location.href = '/contracts/online/set_sk_calc/?calc_id=' + calc_id;
    }


</script>
