<div class="block-view">
    <h3>Риски</h3>
    <div class="row">



        <div class="col-md-3 col-lg-2" >
            <label>Самовозгорание</label>
            {{Form::select("contract[risks][damage_id]", collect([0=>"Нет", 1=>'Да']), $terms->damage_id, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-md-3 col-lg-2" >
            <label>Угон</label>
            {{Form::select("contract[risks][hijacking_id]", collect([0=>"Нет", 1=>'Без ключей', 2=>'С ключами']), $terms->hijacking_id, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-md-3 col-lg-2" >
            <label>Урегулирование без документов</label>
            {{Form::select("contract[risks][is_undocumented_settlement_competent_authorities]",  collect([0=>'Нет', 1=>'Неограниченно стекла и два раза повреждение детали', 1=>'Один раз стекла и один раз ЛКП']), $terms->is_undocumented_settlement_competent_authorities, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-md-3 col-lg-3" >
            <label>Аварийный комиссар + сбор документов</label>
            {{Form::select("contract[risks][is_emergency_commissioner_select]", collect([0=>'Нет', 1=>'Сбор документов', 2=>'На место ДТП + сбор документов']), $terms->is_emergency_commissioner_select, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>

        <div class="col-md-3 col-lg-3" >
            <label>Утрата товарной стоимости</label>
            {{Form::select("contract[risks][is_loss_commodity_value]", collect([0=>'Нет', 1=>'Есть']), $terms->is_loss_commodity_value, ['class' => 'form-control', 'style'=>'width: 100%;'])}}
        </div>





    </div>
</div>


<script>

    function initTerms() {





    }

</script>