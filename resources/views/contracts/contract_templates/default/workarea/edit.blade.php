@php
    $hide_on_0 = '';
    $hide_on_1 = '';
@endphp
@if($contract->kind_acceptance == 0)
    @php($hide_on_0 = 'style=display:none')
@elseif($contract->kind_acceptance == 1)
    @php($hide_on_1 = 'style=display:none')
@endif
<div class="work-area-group">
    <h3>Общие</h3>
    <div class="work-area-item">
        <span class="btn btn-primary" onclick="process(['save_contract', 'saveComment'])">Сохранить</span>
    </div>
</div>

@if($contract->statys_id == 0)
    <div class="work-area-item">
        <span class="btn btn-success" onclick="process(['save_contract_quiet', 'to_check'])">
            В проверку
        </span>
    </div>


    <div class="work-area-item">
        <span class="btn btn-danger" style="margin-top: 50px" onclick="deleteTempContract('{{$contract->id}}')">
            Удалить
        </span>
    </div>


@endif

@if(in_array($contract->statys_id, [1,7]) && auth()->user()->is('under'))
    <div class="work-area-group">
        <h3>Техандерайтинг</h3>
        <div class="work-area-item">
            {{ Form::select('kind_acceptance', collect(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE)->prepend('Не выбрано', -1)->except(3), $contract->kind_acceptance>=0 ? $contract->kind_acceptance : -1, ['class' => 'form-control select2-ws pull-right kind_acceptance', 'onclick' => 'process(["set_buttons"])'] ) }}
        </div>&nbsp;
        <div class="work-area-item hide_on_0" {{ $hide_on_0 }} style="margin-top: 80px">
            <span class="btn btn-success accept-margin" onclick="process(['save_contract', 'set_accept'])">Акцепт</span>
        </div>
    </div>
@endif

@include('contracts.contract_templates.default.workarea.partials.chat')

@if(in_array($contract->statys_id, [1,7]) && auth()->user()->is('under'))
    <div class="work-area-group hide_on_1" {{ $hide_on_1 }}>
        <div class="work-area-item">
            <span class="btn btn-danger" onclick="process(['save_contract', 'set_error'])">Ошибка</span>
        </div>
    </div>

    <div class="work-area-item">
        <span class="btn btn-danger" style="margin-top: 50px" onclick="deleteTempContract('{{$contract->id}}')">
            Удалить
        </span>
    </div>

@endif

@if($contract->statys_id==2 && auth()->user()->is('agent'))
    <div class="work-area-group hide_on_1" {{ $hide_on_1 }}>
        <span class="btn btn-success" onclick="process(['save_contract', 'to_fixed'])">
            Исправил
        </span>
    </div>
@endif