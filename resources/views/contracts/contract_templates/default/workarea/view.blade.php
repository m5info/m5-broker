@if($contract->statys_id == 0)
    <div class="work-area-item">
        <span class="btn btn-success" onclick="process(['to_check'])">
            В проверку
        </span>
    </div>
@endif

@if(in_array($contract->statys_id, [1,7]) && auth()->user()->is('under'))
    <div class="work-area-group">
        <h3>Техандерайтинг</h3>
        <div class="work-area-item">
            {{ Form::select( 'kind_acceptance', collect(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE)->prepend('Не выбрано', -1)->except(3), $contract->kind_acceptance>=0 ? $contract->kind_acceptance : -1, ['class' => 'form-control select2-ws pull-right kind_acceptance'] ) }}
        </div>&nbsp;
        <div class="work-area-item">
            <span class="btn btn-success" onclick="process(['set_accept'])">Акцепт</span>
        </div>
    </div>
@endif

@include('contracts.contract_templates.default.workarea.partials.chat')

@if(in_array($contract->statys_id, [1,7]) && auth()->user()->is('under'))
    <div class="work-area-group">
        <div class="work-area-item">
            <span class="btn btn-danger" onclick="process(['set_error'])">Ошибка</span>
        </div>
    </div>
@endif

@if($contract->statys_id==2 && auth()->user()->is('agent'))
    <div class="work-area-group">
        <span class="btn btn-success" onclick="process(['to_fixed'])">
            Исправил
        </span>
    </div>
@endif
