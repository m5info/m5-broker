@php
    $hide_on_0 = '';
    $hide_on_1 = '';
@endphp
@if($contract->kind_acceptance == 0)
    @php($hide_on_0 = 'style=display:none')
@elseif($contract->kind_acceptance == 0)
    @php($hide_on_1 = 'style=display:none')
@endif
<div class="work-area-group hide_on_1" {{ $hide_on_1 }} >
    <h3>Чат</h3>
    @if(sizeof($contract->messages))
        @foreach($contract->messages as $message)

            @php($message_class = \App\Models\Contracts\ContractMessage::MESSAGE_CLASS[$message->type_id])

            <div class="work-area-message">
                <div class="work-area-message-header">
                    {{ $message->created_at }}
                    <br>
                    {{ $message->user->name }}:
                </div>

                <div class="work-area-message-content {{$message_class}}">
                    {{ $message->message }}
                </div>

            </div>
        @endforeach
    @endif

    <div class="work-area-item">
        {{ Form::textarea('contract[message]', '', ['class' => 'form-control', 'rows' => 3]) }}&nbsp;
    </div>

    <div class="work-area-item hide_on_0" {{ $hide_on_0 }} >
        <span class="btn btn-primary" onclick="process(['save_contract_quiet', 'set_comment'])">Комментарий</span>
    </div>
</div>