@if(isset($payment->type_id) && $payment->type_id != 1)
    <div class="payment_block outer row col-xs-12 col-sm-12 col-md-12 col-lg-12" data-id="{{$key}}">
        <br/>
        <input type="hidden" name="contract[0][payment][{{$key}}][id]" value="{{$payment->id}}"/>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <label class="control-label">Взнос №</label>
            {{ Form::text("contract[0][payment][$key][payment_number]", $payment->payment_number, ['class' => 'form-control valid_fast_accept', 'id'=>"payment_number_$key"]) }}
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <label class="control-label">Тип</label>
            {{ Form::select("contract[0][payment][$key][type_id]", collect([0 => 'Взнос']), $payment->type_id, ['class' => 'form-control select2-ws']) }}
        </div>
    </div>
    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
            <label class="control-label">Сумма платежа</label>
            {{ Form::text("contract[0][payment][$key][payment_total]", ($payment->payment_total>0.00) ? titleFloatFormat($payment->payment_total):'', ['class' => 'form-control sum valid_fast_accept', 'id'=>"payment_total_$key", 'readonly']) }}
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
            <label class="control-label">Дата оплаты</label>
            {{ Form::text("contract[0][payment][$key][payment_data]", setDateTimeFormatRu($payment->payment_data, 1), ['class' => 'form-control datepicker valid_fast_accept date', 'id'=>"payment_data_$key"]) }}
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
            <label class="control-label">Дата выезда</label>
            {{ Form::text("contract[0][payment][$key][delivery_date]", setDateTimeFormatRu($payment->delivery_date, 1), ['class' => 'form-control datepicker valid_fast_accept date', 'id'=>"delivery_date_$key"]) }}
        </div>
    </div>

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
            <label class="control-label">Офици. %</label>
            {{ Form::text("contract[0][payment][$key][official_discount]", ($payment->official_discount!=0.00)?titleFloatFormat($payment->official_discount):'', ['class' => 'form-control sum percents_input_validation', 'id'=>"official_discount_$key"]) }}
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
            <label class="control-label">Неофиц. %</label>
            {{ Form::text("contract[0][payment][$key][informal_discount]", ($payment->informal_discount!=0.00)?titleFloatFormat($payment->informal_discount):'', ['class' => 'form-control sum percents_input_validation', 'id'=>"informal_discount_$key"]) }}
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
            <label class="control-label">Банк %</label>
            {{ Form::text("contract[0][payment][$key][bank_kv]", ($payment->bank_kv!=0.00)?titleFloatFormat($payment->bank_kv):'', ['class' => 'form-control sum percents_input_validation', 'id'=>"bank_kv_$key"]) }}
        </div>
        @if((int)$payment->reports_order_id > 0 || (int)$payment->reports_dvou_id > 0)
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 required">
                * Платеж добавлен в отчет. Пересчет КВ невозможен. Для пересчета необходимо обратиться к фин. директору.
            </div>
        @endif
    </div>

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label class="control-label pull-left" style="margin-top: 5px;">
                Вид оплаты
            </label>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            {{ Form::select("contract[0][payment][{$key}][pay_method_id]", \App\Models\Finance\PayMethod::query()->where('is_actual', 1)->pluck('title', 'id'), $payment->pay_method_id, ['class' => 'form-control select2-ws', 'data-key' => $key]) }}
        </div>
        <script>
            var pay_methods_id2types = {
                @foreach(\App\Models\Finance\PayMethod::query()->where('is_actual', 1)->get() as $method)
                {!! "$method->id : $method->key_type," !!}
                @endforeach
            };
        </script>
    </div>

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="receipt_block_{{$key}}">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label class="control-label pull-left" style="margin-top: 5px;">
                Квитанция
                <a href="javascript:void(0);" class="btn-xs btn-primary" data-create_receipt="{{$key}}" style="display: inline">Е-квит</a>
            </label>
            {{ Form::text( "contract[0][payment][{$key}][bso_receipt]", $payment->bso_receipt, ['class' => 'form-control valid_accept class_bso_receipt', 'id'=>"bso_receipt_{$key}"]) }}
            <input type="hidden" name="contract[0][payment][{{$key}}][bso_receipt_id]" id="bso_receipt_id_{{$key}}" value="{{$payment->bso_receipt_id}}"/>
        </div>
    </div>

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="check_block_{{$key}}" style="display: none">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label class="control-label">Телефон</label>
            {{ Form::text("contract[0][payment][{$key}][check_phone]", '', ['class' => 'form-control phone']) }}
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label class="control-label">E-mail</label>
            {{ Form::text("contract[0][payment][{$key}][check_email]", '', ['class' => 'form-control email']) }}
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label class="control-label">Отправить</label>
            {{ Form::select("contract[0][payment][{$key}][when_to_send]", collect(\App\Models\Api\Atol\AtolCheck::WHEN_TO_SEND), 1, ['class' => 'form-control', 'id'=>'when_to_send_[:KEY:]',]) }}
        </div>
    </div>
@else
    <div class="col-lg-12">
        <div class="view-field">
            <span class="view-label">Тип</span>
            <span class="view-value">Долг</span>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="view-field">
            <span class="view-label">Сумма платежа</span>
            <span class="view-value">{{ ($payment->payment_total>0.00) ? titleFloatFormat($payment->payment_total):'' }}</span>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="view-field">
            <span class="view-label">Комментарий</span>
            <span class="view-value">{{ $payment->comments }}</span>
        </div>
    </div>
@endif