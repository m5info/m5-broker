<table class="table table-bordered bso_items_table">
    <thead>
    <tr>
        <th>Тип</th>
        <th>Дата оплаты</th>
        <th>Вид оплаты</th>
       {{-- <th>Тип оплаты</th>--}}
       {{-- <th>Поток оплаты</th>--}}
        <th>Квитанция</th>
        <th>Сумма взноcа</th>
        {{--
        <th>Приход в кассу</th>
        <th>Сумма в кассу</th>
        --}}
    </tr>
    </thead>
    <tbody class="payments_table_tbody">

    @if(isset($payments) && sizeof($payments))
        @foreach($payments as $payment)
            <tr>
                <td>{{\App\Models\Contracts\Payments::TRANSACTION_TYPE[$payment->type_id]}} @if($payment->type_id == 0) {{$payment->payment_number}} @endif</td>
                <td>{{setDateTimeFormatRu($payment->payment_data, 1)}}</td>
                <td>


                    @if($payment->pay_mehtod)
                        {{$payment->pay_mehtod->title}}
                    @endif

                </td>
               {{-- <td>{{\App\Models\Contracts\Payments::PAYMENT_TYPE[$payment->payment_type]}}</td>--}}
               {{-- <td>{{\App\Models\Contracts\Payments::PAYMENT_FLOW[$payment->payment_flow]}}</td>--}}

                <td>{{$payment->bso_receipt ? : ""}}</td>
                <td>{{titleFloatFormat($payment->payment_total)}}</td>


            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="20" class="text-center">Нет платежей</td>
        </tr>
    @endif
    </tbody>
</table>