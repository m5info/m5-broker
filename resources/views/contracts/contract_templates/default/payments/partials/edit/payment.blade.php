@if(isset($payment->type_id) && $payment->type_id != 1)
    {{-- Фикс для пересохранения выплаты --}}
    @if($payment->type_id == 2 && $payment->statys_id == 1)
        <div class="col-lg-12">
            <div class="view-field">
                <span class="view-label">Тип</span>
                <span class="view-value">Выплата</span>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="view-field">
                <span class="view-label">Сумма платежа</span>
                <span class="view-value">{{ ($payment->payment_total>0.00) ? titleFloatFormat($payment->payment_total):'' }}</span>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="view-field">
                <span class="view-label">Комментарий</span>
                <span class="view-value">{{ $payment->comments }}</span>
            </div>
        </div>
    @else
        <div class="payment_block outer row col-xs-12 col-sm-12 col-md-12 col-lg-12" data-id="{{$key}}">
            <br/>
            <input type="hidden" name="contract[0][payment][{{$key}}][id]" value="{{$payment->id}}"/>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <label class="control-label">Взнос № <span class="required">*</span></label>
                {{ Form::text("contract[0][payment][$key][payment_number]", $payment->payment_number, ['class' => 'form-control default_contract_valid', 'data-error' => 'Заполните поле `Взнос №`', 'id'=>"payment_number_$key"]) }}
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <label class="control-label">Тип</label>
                @if(!$permission)
                    {{ Form::select("contract[0][payment][$key][type_id]", collect([0 => 'Взнос']), $payment->type_id, ['class' => 'form-control select2-ws']) }}

                @else
                    <span>{{\App\Models\Contracts\Payments::TRANSACTION_TYPE[$payment->type_id]}}</span>
                    <input type="hidden" name="contract[0][payment][{{$key}}][type_id]" value="{{$payment->type_id}}"/>
                @endif
            </div>
        </div>
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <label class="control-label">Сумма платежа <span class="required">*</span></label>
                {{ Form::text("contract[0][payment][$key][payment_total]", ($payment->payment_total>0.00) ? titleFloatFormat($payment->payment_total):'', ['class' => 'form-control sum copy_payment_total default_contract_valid', 'id'=>"payment_total_$key", 'data-error' => 'Заполните поле `Сумма платежа`', (int)$payment->contract->sales_condition ? 'readonly' : '']) }}
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <label class="control-label">Дата оплаты <span class="required">*</span></label>
                {{ Form::text("contract[0][payment][$key][payment_data]", setDateTimeFormatRu($payment->payment_data, 1), ['class' => 'form-control datepicker date default_contract_valid copy_payment_data', 'id'=>"payment_data_$key", 'data-error' => 'Заполните поле `Дата оплаты`']) }}
            </div>
        </div>

        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label class="control-label">Офиц. % <span class="required">*</span></label>
                {{ Form::text("contract[0][payment][$key][official_discount]", ($payment->official_discount!=0.00)?titleFloatFormat($payment->official_discount):0, ['class' => 'form-control sum default_contract_valid', 'id'=>"official_discount_$key", 'data-error' => 'Заполните поле `Офиц. %`', $product->kv_official_available ? '' : 'disabled'=>'disabled']) }}
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label class="control-label">Неофиц. % <span class="required">*</span></label>
                {{ Form::text("contract[0][payment][$key][informal_discount]", ($payment->informal_discount!=0.00)?titleFloatFormat($payment->informal_discount):0, ['class' => 'form-control sum default_contract_valid', 'id'=>"informal_discount_$key", 'data-error' => 'Заполните поле `Неофиц. %`', $product->kv_informal_available ? '' : 'disabled'=>'disabled']) }}
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label class="control-label">Банк % <span class="required">*</span></label>
                {{ Form::text("contract[0][payment][$key][bank_kv]", ($payment->bank_kv!=0.00)?titleFloatFormat($payment->bank_kv):0, ['class' => 'form-control sum default_contract_valid', 'id'=>"bank_kv_$key", 'data-error' => 'Заполните поле `Банк %`', $product->kv_bank_available ? '' : 'disabled'=>'disabled']) }}
            </div>
            @if((int)$payment->reports_order_id > 0 || (int)$payment->reports_dvou_id > 0)
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 required">
                    * Платеж добавлен в отчет. Пересчет КВ невозможен. Для пересчета необходимо обратиться к фин. директору.
                </div>
            @endif
        </div>

        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <label class="control-label pull-left" style="margin-top: 5px;">
                    Вид оплаты
                </label>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                {{ Form::select("contract[0][payment][{$key}][pay_method_id]", \App\Models\Finance\PayMethod::query()->where('is_actual', 1)->pluck('title', 'id'), $payment->pay_method_id, ['class' => 'form-control select2-ws', 'data-key' => $key]) }}
            </div>
            <script>
                var pay_methods_id2types = {
                    @foreach(\App\Models\Finance\PayMethod::query()->where('is_actual', 1)->get() as $method)
                    {!! "$method->id : $method->key_type," !!}
                    @endforeach
                };
            </script>
        </div>

        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="receipt_block_{{$key}}">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <label class="control-label pull-left" style="margin-top: 5px;">
                    Квитанция <span class="required">*</span>
                    <a href="javascript:void(0);" class="btn-xs btn-primary" data-create_receipt="{{$key}}"
                       style="display: inline">Е-квит</a>
                </label>
                {{ Form::text( "contract[0][payment][{$key}][bso_receipt]", $payment->bso_receipt, ['class' => 'form-control valid_accept class_bso_receipt default_contract_valid', 'id'=>"bso_receipt_{$key}", 'data-error' => 'Заполните поле `Е-квит`']) }}
                <input type="hidden" name="contract[0][payment][{{$key}}][bso_receipt_id]" id="bso_receipt_id_{{$key}}"
                       value="{{$payment->bso_receipt_id}}"/>
            </div>
        </div>

        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="check_block_{{$key}}" style="display: none">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <label class="control-label">Телефон</label>
                {{ Form::text("contract[0][payment][{$key}][check_phone]", '', ['class' => 'form-control phone']) }}
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <label class="control-label">E-mail</label>
                {{ Form::text("contract[0][payment][{$key}][check_email]", '', ['class' => 'form-control email']) }}
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <label class="control-label">Отправить</label>
                {{ Form::select("contract[0][payment][{$key}][when_to_send]", collect(\App\Models\Api\Atol\AtolCheck::WHEN_TO_SEND), 1, ['class' => 'form-control', 'id'=>'when_to_send_[:KEY:]',]) }}
            </div>
        </div>
        @if($payment->statys_id == -1)
            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="btn btn-danger btn-right" style="margin-top: 15px;"
                      onclick="deleteTempPayment('{{$payment->id}}')">Удалить</span>
            </div>
        @endif
    @endif
@else
    <div class="col-lg-12">
        <div class="view-field">
            <span class="view-label">Тип</span>
            <span class="view-value">Долг</span>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="view-field">
            <span class="view-label">Сумма платежа</span>
            <span class="view-value">{{ ($payment->payment_total>0.00) ? titleFloatFormat($payment->payment_total):'' }}</span>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="view-field">
            <span class="view-label">Комментарий</span>
            <span class="view-value">{{ $payment->comments }}</span>
        </div>
    </div>
@endif

{{--
@section('js')

    <script type="text/javascript">

        $(function () {

            initWorkArea();


            toggle_pay_method();

            $(document).on('change', '#payment_contract_total_0', function () {
                sum = $(this).val();
                var disabled_input = $('#payment_total_0').prop("disabled");

                if (!disabled_input) {
                    $('#payment_total_0').val(sum);
                }
            });

            $(document).on('change', '#insurance_amount_0', function () {

                payment = $(this).val();

                if (payment.search(/^[0-9 ,]+$/) != -1) {
                    $('[name = "contract[0][payment_total]"]').val(payment);
                }


            });

            $(document).on('click', '[data-create_receipt]', function () {
                var st = $(document).scrollTop();
                var key = $(this).data('create_receipt');
                var payment_id = $('[name="contract[0][payment][' + key + '][id]"]').val();
                openFancyBoxFrame('/contracts/temp_contracts/create_receipt_frame?payment_id=' + payment_id + '&key=' + key)
                setTimeout(function () {
                    $(document).scrollTop(st)
                }, 0);
            });


            $('.class_bso_receipt').each(function () {
                key = $(this).attr("id").split('_');
                activSearchBso("bso_receipt", "_" + key[2], 2);

            });


            initInfo();
            initInsurer();


            $("input, select").change(function () {
                $(this).removeClass('form-error');
            });


            $(document).on('change', '[name*="[pay_method_id]"]', function () {
                toggle_pay_method();
            });


            var available_kvs = {
                'kv_official_available': '{{$contract->product && $contract->product->kv_bank_available ? $contract->product->kv_bank_available : ''}}',
                'kv_informal_available': '{{$contract->product && $contract->product->kv_informal_available ? $contract->product->kv_informal_available : ''}}',
                'kv_bank_available': '{{$contract->product && $contract->product->kv_bank_available ? $contract->product->kv_bank_available : ''}}',
            };

            $('.payment_block.outer').each(function (index, value) {
                var key = $(this).attr('data-id');

                var official = $('#official_discount_' + key);
                var informal = $('#informal_discount_' + key);
                var bank = $('#bank_kv_' + key);

                official.prop("disabled", true);
                informal.prop("disabled", true);
                bank.prop("disabled", true);

                if (available_kvs['kv_official_available'] == 1) {
                    official.prop("disabled", false);
                }
                if (available_kvs['kv_informal_available'] == 1) {
                    informal.prop("disabled", false);
                }
                if (available_kvs['kv_bank_available'] == 1) {
                    bank.prop("disabled", false);
                }
            });

        });


        function toggle_pay_method() {

            $.each($('[name*="[pay_method_id]"]'), function (k, v) {

                var key = $(v).attr('data-key');

                var method_blocks = {
                    0: 'receipt_block_' + key,
                    1: 'check_block_' + key,
                    2: 'none'
                };
                var method = parseInt($(v).val());

                $.each(method_blocks, function (k, v) {
                    $("#" + v).hide()
                });
                $("#" + method_blocks[pay_methods_id2types[method]]).show()
            });


        }


        function process(operations = []) {
            $.each(operations, function (k, operation) {
                if (isFunction(operation)) {
                    window[operation]()
                }
            })
        }


        function set_buttons() {

            var select = $('.kind_acceptance').select2('val');


            if (select == 0) {
                $('.hide_on_1').show();
                $('.hide_on_0').hide();
            } else if (select == 1) {
                $('.hide_on_0').show();
                $('.hide_on_1').hide();
            } else {
                $('.hide_on_0').show();
                $('.hide_on_1').show();
            }

        }

        function validFastAccept() {

            var first_err;
            var msg_arr = [];

            var i = 0;

            var valid_message = false;
            var valid = true;


            $('.valid_fast_accept').each(function (_i, item) {

                if ($(this)[0].localName != 'div') {

                    var tag = $(this)[0].localName;

                    var val = '';
                    if (['select', 'input'].indexOf(tag) != -1) {
                        val = $(this).val();
                    } else {
                        return;
                    }


                    if (tag == 'select') {

                        var not_selected_val = 0;
                        if ($(this).attr('data-not_selected') !== undefined) {
                            not_selected_val = parseInt($(this).attr('data-not_selected'))
                        }

                        if (parseInt(val) == not_selected_val) {
                            if (i == 0 && $(this).parent('div').css('display') != 'none') {
                                first_err = $(this);
                                i++;
                            }
                            msg_arr.push('Заполните все поля');
                            var container = $(this).prev('.select2-container');
                            container.css({
                                'height': '36px',
                                'border': '1px red solid'
                            });
                        }
                    }

                    if (val.length < 1 && $(this).parent('div').css('display') != 'none' && $(this).parent('div').parent('div').css('display') != 'none') {

                        valid = false;


                        if (valid_message === false) {
                            flashMessage('danger', $(this).data('error'));
                            valid_message = true;
                        }


                        if (i == 0) {
                            first_err = $(this);
                            i++;
                        }
                        msg_arr.push('Заполните все поля');

                        var elem = $(this);

                        elem.css("border-color", "red");

                    }
                }
            });

            if (valid == true) {
                return true;
            } else {
                return false;
            }
        }

        function set_comment() {

            var message = $('[name="contract[message]"]').val();
            if (message.length > 0) {
                $.post("{{url("/contracts/temp_contracts/contract/{$contract->id}/set_comment/")}}", {'message': message}, function (res) {
                    reload();
                });
            } else {
                flashHeaderMessage('Введите комментарий!', 'danger')
            }

        }


        function save_contract() {


            $.ajax({
                type: "POST",
                url: "{{url("/contracts/temp_contracts/contract/{$contract->id}/edit")}}",
                async: false,
                data: $("#main_container").find("select, textarea, input").serialize(),

                success: function (response) {
                    flashHeaderMessage('Успешно сохранено!', 'success');
                }
            });

        }

        function save_contract_quiet() {
            $.ajax({
                type: "POST",
                url: "{{url("/contracts/temp_contracts/contract/{$contract->id}/edit")}}",
                async: false,
                data: $("#main_container").find("select, textarea, input").serialize(),
            });
        }


        /*function myvalid() {
            data = $("#main_container").find("select, textarea, input").serializeArray();

            validation = 1;
            type_of_pay = -1;

            data.forEach(function (elem) {

                if (elem.name == 'contract[0][insurer][fio]') {
                    if (elem.value == "") {
                        flashMessage('danger', 'Не заполнено "ФИО страхователя"');
                        validation = 0;
                    }
                }

                if (elem.name == 'contract[0][payment][0][pay_method_id]') {
                    if (elem.value == 1) {
                        type_of_pay = 1;
                    }
                }

                if (elem.name == 'contract[0][payment][0][bso_receipt]') {
                    if (elem.value == "" && type_of_pay == 1) {
                        flashMessage('danger', 'Не заполнено "Квитанция"');
                        validation = 0;
                    }
                }

            });

            return validation;

        }*/


        function to_check() {
            if (validFastAccept()) {

                var response = myPostAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/send_check/")}}")
                if (response) {
                    if (response.msg && response.msg.length) {
                        $.each(response.msg, function (index, value) {
                            //flashHeaderMessage(value, 'danger');
                        });
                    } else {
                        var module = '{{ auth()->user()->is('under') ? "verification" : "temp_contracts" }}';
                        location.href = '/contracts/' + module + '/';
                    }
                }

            }
        }

        function to_fixed() {
            var message = $('[name="contract[message]"]').val();
            $.post("{{url("/contracts/temp_contracts/contract/{$contract->id}/send_to_fixed/")}}", {'message': message}, function (response) {
                if (response.msg && response.msg.length) {
                    $.each(response.msg, function (index, value) {
                        flashHeaderMessage(value, 'danger');
                    });
                } else {
                    var module = '{{ auth()->user()->is('under') ? "verification" : "temp_contracts" }}';
                    location.href = '/contracts/' + module + '/';
                }
            });
        }

        function set_error() {
            var message = $('[name="contract[message]"]').val();
            if (message.length > 0) {
                var response = myPostAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/error_contract/")}}?message=" + message)
                if (response.msg && response.msg.length) {
                    $.each(response.msg, function (index, value) {
                        flashHeaderMessage(value, 'danger');
                    });
                } else {
                    var module = '{{ auth()->user()->is('under') ? "verification" : "temp_contracts" }}';
                    location.href = '/contracts/' + module + '/';
                }
            } else {
                flashHeaderMessage('Введите комментарий к ошибке!', 'danger')
            }
        }


        function set_accept() {

            var accept_select = $('[name="kind_acceptance"]');
            var accept_link = $('div.kind_acceptance a');

            if (parseInt(accept_select.val()) < 0) {
                var old = accept_link.css("border");
                accept_link.css("border-color", "red");
                setTimeout(function () {
                    accept_link.css("border", old);
                }, 2000);
                flashHeaderMessage('Необходимо выбрать тип акцепта', 'danger')

            } else {

                if (validateContract()) {

                    console.log('validateContract прошло');
                    var response = myPostAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/accept_contract")}}?kind_acceptance=" + accept_select.val());
                    if (response.msg && response.msg.length) {
                        $.each(response.msg, function (index, value) {
                            flashHeaderMessage(value, 'danger');
                        });
                    } else {
                        var module = '{{ auth()->user()->is('under') ? "verification" : "temp_contracts" }}';
                        location.href = '/contracts/' + module + '/';
                    }
                }
            }
        }

        function validateContract() {

            if ($('#financial_policy_id_0').val() > 0) {
                return true;
            } else {
                flashHeaderMessage('Необходимо указать фин.политику', 'danger')

                return false;
            }
        }


        function selectBso(object_id, key, type, suggestion) {
            var data = suggestion.data;
            $("#bso_receipt_id" + key).val(data.bso_id);
        }


        function initDuplicates() {
            var items = [];
            $("input, select").each(function () {
                if (items[$(this).attr('name')]) {
                    var duplicate = $("input[name='" + $(this).attr('name') + "']");
                    if (duplicate[1]) {
                        $("input[name='" + $(this).attr('name') + "']").change(function () {
                            let old = $(this);
                            $(duplicate).each(function () {
                                $(this).val(old.val())
                            });
                        });
                    }
                }
                items[$(this).attr('name')] = 1;
            });
        }

        document.addEventListener("DOMContentLoaded", function (event) {
            initDuplicates();
        });


        function addPayment() {


            save_contract();
            /*let last_block = $('#contract_payments .payment_block:last-child');

            $("html, body").animate({ scrollTop: last_block.offset().top-100 }, 200);*/
            setTimeout(function () {
                if (myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/add_pay/")}}")) {
                    reload();
                }
            }, 50);

        }

        function deleteTempPayment(pay) {

            if (confirm('Удалить платеж?')) {
                save_contract();

                setTimeout(function () {
                    if (myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/del_pay/?pay=")}}" + pay)) {
                        reload();
                    }
                }, 50);
            }
        }


        function viewSetBsoNotReceipt(obj, key) {
            if ($(obj).is(':checked')) {
                $("#bso_receipt_" + key).attr('disabled', 'disabled');
                $("#bso_receipt_" + key).removeClass('valid_fast_accept');
            } else {
                $("#bso_receipt_" + key).removeAttr('disabled');
                $("#bso_receipt_" + key).addClass('valid_fast_accept');
            }
            $("#bso_receipt_" + key).css("border-color", "");
        }

        @if($contract->statys_id == 4)

        $('#main_container input').each(function () {
            if ($(this).is(":visible")) {
                if ($(this).is(':checkbox')) {
                    if ($(this).val()) {
                        $(this).replaceWith('<span style="padding:3px;">Да</span >');
                    } else {
                        $(this).replaceWith('<span  style="padding:3px;">Нет</span >');
                    }
                } else {
                    $(this).replaceWith('<p style="padding-top:5px;">' + $(this).val() + '</p>');
                }
            }
        });

        $('#main_container select').prop('disabled', true);

        @endif


        function initInfo() {

            viewControllerSalesCondition();

            getOptionInstallmentAlgorithms("installment_algorithms_id_0", '{{$contract->insurance_companies_id}}', '{{$contract->installment_algorithms_id}}');

            @if(auth()->user()->hasPermission('contracts', 'select_financial_policy'))
            getOptionFinancialPolicy(
                "financial_policy_id_0",
                '{{$contract->insurance_companies_id}}',
                '{{$contract->bso_supplier_id}}',
                '{{$contract->product_id}}',
                '{{$contract->financial_policy_id}}'
            );
            @endif

            @if(auth()->user()->hasPermission('contracts', 'set_financial_policy_manually'))
            viewSetFinancialPolicyManually($("#financial_policy_manually_set_0"), '0');
            @endif

            $("#installment_algorithms_id_0").change(function () {
                if ($(this).val() > 0) {
                    $.ajax({
                        type: "POST",
                        url: "{{url("/settings/installment_algorithms_list/get_quantity")}}",
                        async: false,
                        data: {'algorithm_id': $(this).val()},
                        success: function (response) {
                            var str = $('#payment_contract_total_0').val().replace(/\s/g, '');
                            var sum = parseInt(str);
                            $('#payment_next_sum_0').val(sum / response);
                            $('#payment_algo').show();
                        }
                    });
                } else {
                    $('#payment_algo').hide();
                    $('#payment_next_date_0').val('');
                    $('#payment_next_sum_0').val('');
                }
            });

            activSearchOrdersToFront("order_title", "_0");
        }


        function get_end_dates(start_date) {
            var cur_date_tmp = start_date.split(".");
            var cur_date = new Date(cur_date_tmp[2], cur_date_tmp[1] - 1, cur_date_tmp[0]);
            var new_date = new Date(cur_date.setYear(cur_date.getFullYear() + 1));
            var new_date2 = new Date(new_date.setDate(new_date.getDate() - 1));
            return getFormattedDate(new_date2);
        }

        function setAllDates(key) {
            var sign_date = $("#sign_date_" + key).val();
            $("#begin_date_" + key).val(sign_date);

            return setEndDates(key);
        }

        function setEndDates(key) {
            var begin_date = $("#begin_date_" + key).val();
            $("#end_date_" + key).val(get_end_dates(begin_date));
        }


        function viewControllerSalesCondition() {
            if ($("#sales_condition_0").val() != 0) {
                $(".sales_condition_not_agent").show();
            } else {
                $(".sales_condition_not_agent").hide();
            }

        }

        function viewSetFinancialPolicyManually(obj, key) {
            if ($(obj).is(':checked')) {
                $("#financial_policy_manually_" + key).show();
                $("#financial_policy_id_block").hide();
            } else {
                $("#financial_policy_manually_" + key).hide();
                $("#financial_policy_id_block").show();
            }
        }


        function initInsurer() {

            $('#insurer_type_0').change(function () {

                if (parseInt($(this).val()) === 0) {
                    $('.insurer_fl').show();
                    $('.insurer_ul').hide();
                } else {
                    $('.insurer_fl').hide();
                    $('.insurer_ul').show();
                }

            });

            $('#insurer_type_0').change();

            $('#insurer_fio_' + 0).suggestions({
                serviceUrl: '{{url("/custom_suggestions/suggest_physical/")}}',
                token: DADATA_TOKEN,
                type: "NAME",
                count: 5,
                onSelect: function (suggestion) {
                    var data = suggestion.data;
                    console.log(data);
                    var key = $(this).data('key');

                    $('#insurer_title_' + key).val($(this).val());
                    $('#insurer_doc_serie_' + key).val(data.doc_serie);
                    $('#insurer_doc_number_' + key).val(data.doc_number);
                    $('#insurer_phone_' + key).val(data.phone);
                    $('#insurer_email_' + key).val(data.email);

                }
            });

            $('#insurer_title_' + 0 + ', #insurer_inn_' + 0 + ', #insurer_kpp_' + 0).suggestions({
                serviceUrl: '{{url("/custom_suggestions/suggest_jure/")}}',
                token: DADATA_TOKEN,
                type: "PARTY",
                count: 5,
                onSelect: function (suggestion) {
                    var data = suggestion.data;
                    var key = $(this).data('key');

                    $('#insurer_title_' + key).val(suggestion.value);
                    $('#insurer_inn_' + key).val(data.inn);
                    $('#insurer_kpp_' + key).val(data.kpp);
                    $('#insurer_fio_' + key).val($('#insurer_title_' + key).val());
                    $('#insurer_phone_' + key).val(data.phone);
                    $('#insurer_email_' + key).val(data.email);

                }
            });

            if (isFunction('initDocuments')) {
                initDocuments();
            }

        }

        function getModelsObjectInsurer(KEY, select_model_id) {

            $.getJSON(
                '{{url("/contracts/actions/get_models")}}',
                {
                    categoryId: $('#object_insurer_ts_category_' + KEY).val(),
                    markId: $('#object_insurer_ts_mark_id_' + KEY).select2('val')
                },
                function (response) {

                    var options = "<option value='0'>Не выбрано</option>";
                    response.map(function (item) {
                        options += "<option value='" + item.id + "'>" + item.title + "</option>";
                    });

                    $('#object_insurer_ts_model_id_' + KEY).html(options).select2('val', select_model_id);

                });

        }

        var bso_used = [];
        startContractFunctions({{$key}});

        function startContractFunctions(key) {

            activSearchBso("bso_receipt", key, 2, bso_used);

        }

        $('.payment_block.outer').each(function (index, value) {
            var key = $(this).attr('data-id');

            startContractFunctions(key);

        });

        //ПОИСК БСО
        function activSearchBso(object_id, key, type, bso_used = []) {

            var object = $('#bso_receipt_' + key);
            var bso_supplier_id = $('#bso_supplier_id_0').val();

            object.suggestions({
                serviceUrl: "/bso/actions/get_bso/",
                type: "PARTY",
                params: {
                    type_bso: type,
                    bso_supplier_id: bso_supplier_id,
                    bso_agent_id: -1,
                    bso_used: bso_used
                },
                count: 10,
                minChars: 3,
                formatResult: function (e, t, n, i) {
                    var s = this;
                    var title = n.value;
                    var bso_type = n.data.bso_type;
                    var bso_sk = n.data.bso_sk;
                    var agent_name = n.data.agent_name;

                    var view_res = ' &nbsp;' + title;
                    view_res += ' <div class="' + s.classes.subtext + '">' + bso_type + "</div> ";
                    view_res += ' <div class="' + s.classes.subtext + '">' + agent_name + "</div> ";

                    return view_res;
                },
                onSelect: function (suggestion) {

                    $.each($('#rit_bsos .bso_number'), function (k, v) {
                        bso_used.push($(v).val());
                    });

                    bso_used = bso_used;

                    selectBso(object, key, type, bso_used, suggestion);

                }
            });
        }

        function selectBso(object, key, type, bso_used, suggestion) {

            var data = suggestion.data;

            activSearchBso("bso_receipt", key, 2, bso_used);

            if (parseInt(type) == 1) {
                $('#bso_id' + key).val(data.bso_id);
                $('#bso_supplier_id' + key).val(data.bso_supplier_id);
                $('#insurance_companies_id' + key).val(data.insurance_companies_id);
                $('#product_id' + key).val(data.product_id);
                $('#agent_id' + key).val(data.agent_id);
            }

            if (parseInt(type) == 2) {
                $('#bso_receipt_id_' + key).val(data.bso_id);
            }

        }
    </script>
@endsection
--}}
