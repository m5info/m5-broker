<div id="contract_payments" class="row">
    <div class="page-subheading col-xs-12 col-sm-12 col-md-12 col-lg-12">

{{--        <span class="btn btn-success btn-right" onclick="addPayment()">--}}
{{--            <i class="fa fa-plus-circle"></i> Добавить платеж--}}
{{--        </span>--}}

        <div class="col-lg-12">
            <div class="col-lg-10">
                <h2 class="inline-h1">Платежи</h2>
            </div>

            @if(Auth::user()->hasPermission('role_owns', 'is_underwriter') && $contract->kind_acceptance != 1 && $contract->bso) {{--удалили !$permission, переданный из файла VerificationContractController ->  public function edit($id) --}}
                <div class="col-lg-1">
                    {{Form::select('type_id_id', collect(\App\Models\Contracts\Payments::TRANSACTION_TYPE), 0,  ['class' => 'form-control', 'id'=>'type_id_id'])}}
                </div>
                <div class="col-lg-1">
                    @php
                        $all_payments_first = $contract->all_payments->first();
                        $invoice_id = '';
                        if($all_payments_first && $all_payments_first->invoice_id>0) $invoice_id = $all_payments_first->invoice_id;
                    @endphp
                    <span class="btn btn-success pull-right" onclick="addTransaction({{ $invoice_id }})">Добавить</span>
                </div>
            @endif
        </div>

    </div>
    @if(sizeof($contract->all_payments))
        @foreach($contract->all_payments as $key => $payment)
            @if($payment->statys_id != -2)
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="payment_block block-main">
                        <div class="block-sub">
                            <div class="row">
                                @php
                                    $view_payment = ((int)$payment->statys_id == 1) ? 'paid_invoice' : 'edit';
                                    $view_payment = ((int)$payment->statys_id == 1 && $payment->type_id == 2) ? 'edit' : $view_payment;
                                @endphp


                                @include(
                                    "contracts.contract_templates.default.payments.partials.{$view_payment}.payment",
                                    ['contract' => $contract, 'payment' => $payment, 'key' => $key]
                                )
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    @endif
</div>