<div id="contract_payments" class="row">


    <div class="col-lg-12">
        <div class="col-lg-10">
            <span class="btn btn-success btn-left" onclick="addPayment()">
                <i class="fa fa-plus-circle"></i> Добавить платеж
            </span>
        </div>

    </div>

    @if(sizeof($contract->all_payments))
        @foreach($contract->all_payments as $key => $payment)
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="payment_block block-main">
                    <div class="block-sub">
                        <div class="row">
                            @php
                                $view_payment = $payment->invoice_id && $payment->statys_id == 1 ? 'paid_invoice' : 'edit';
                            @endphp

                            @include(
                                "contracts.contract_templates.default.payments.partials.{$view_payment}.payment",
                                ['contract' => $contract, 'payment' => $payment, 'key' => $key]
                            )
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>