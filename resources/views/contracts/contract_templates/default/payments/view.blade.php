@if(isset($view_type) && $view_type == 'all')

    <h3>Платежи</h3>
    <div class="block-view">
        <div class="row">
            <div class="form-horizontal">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="table table-bordered bso_items_table">
                        <thead>
                        <tr>
                            <th>Тип</th>
                            <th>Дата оплаты</th>
                            <th>Вид оплаты</th>
                            <th>Тип оплаты</th>
                            <th>Поток оплаты</th>
                            <th>Квитанция А7</th>
                            <th>Сумма взноcа</th>
                            {{--
                            <th>Приход в кассу</th>
                            <th>Сумма в кассу</th>
                            --}}
                        </tr>
                        </thead>
                        <tbody class="payments_table_tbody">

                        @if(isset($contract->all_payments) && sizeof($contract->all_payments))
                            @foreach($contract->all_payments as $payment)
                                <tr>
                                    <td>{{\App\Models\Contracts\Payments::TRANSACTION_TYPE[$payment->type_id]}} @if($payment->type_id == 0) {{$payment->payment_number}} @endif</td>
                                    <td>{{setDateTimeFormatRu($payment->payment_data, 1)}}</td>
                                    <td>
                                        @if($payment->pay_mehtod)
                                            {{$payment->pay_mehtod->title}}
                                        @endif
                                    </td>
                                    <td>{{\App\Models\Contracts\Payments::PAYMENT_TYPE[$payment->payment_type]}}</td>
                                    <td>{{\App\Models\Contracts\Payments::PAYMENT_FLOW[$payment->payment_flow]}}</td>

                                    <td>{{$payment->bso_receipt ? : ""}}</td>
                                    <td>{{titleFloatFormat($payment->payment_total)}}</td>

                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="20" class="text-center">Нет платежей</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@elseif(isset($view_type) && $view_type == 'second_payment')
    <h3>Платежи</h3>
    <div class="block-view">
        <div class="row">
            <div class="form-horizontal">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="table table-bordered bso_items_table">
                        <thead>
                        <tr>
                            <th>Тип</th>
                            <th>Дата оплаты</th>
                            <th>Вид оплаты</th>
                            <th>Тип оплаты</th>
                            <th>Поток оплаты</th>
                            <th>Квитанция А7</th>
                            <th>Сумма взноcа</th>
                            {{--
                            <th>Приход в кассу</th>
                            <th>Сумма в кассу</th>
                            --}}
                        </tr>
                        </thead>
                        <tbody class="payments_table_tbody">

                        @if(isset($contract->all_payments) && sizeof($contract->all_payments))
                            @foreach($contract->all_payments as $payment)
                                <tr>
                                    <td>{{\App\Models\Contracts\Payments::TRANSACTION_TYPE[$payment->type_id]}} @if($payment->type_id == 0) {{$payment->payment_number}} @endif</td>
                                    <td>{{setDateTimeFormatRu($payment->payment_data, 1)}}</td>
                                    <td>
                                        @if($payment->pay_mehtod)
                                            {{$payment->pay_mehtod->title}}
                                        @endif
                                    </td>
                                    <td>{{\App\Models\Contracts\Payments::PAYMENT_TYPE[$payment->payment_type]}}</td>
                                    <td>{{\App\Models\Contracts\Payments::PAYMENT_FLOW[$payment->payment_flow]}}</td>

                                    <td>{{$payment->bso_receipt ? : ""}}</td>
                                    <td>{{titleFloatFormat($payment->payment_total)}}</td>

                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="20" class="text-center">Нет платежей</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <br/>
                        <input type="hidden" name="contract[0][payment][{{$key}}][id]" value=""/>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label class="control-label">Взнос №</label>
                            {{ Form::text("contract[0][payment][$key][payment_number]", '', ['class' => 'form-control valid_fast_accept save_payment', 'id'=>"payment_number_$key"]) }}
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label class="control-label">Тип</label>
                            {{ Form::select("contract[0][payment][$key][type_id]", collect(\App\Models\Contracts\Payments::TRANSACTION_TYPE), '', ['class' => 'form-control select2-ws save_payment']) }}
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                            <label class="control-label">Сумма платежа</label>
                            {{ Form::text("contract[0][payment][$key][payment_total]", '', ['class' => 'form-control sum valid_fast_accept save_payment', 'id'=>"payment_total_$key"]) }}
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                            <label class="control-label">Дата оплаты</label>
                            {{ Form::text("contract[0][payment][$key][payment_data]", '', ['class' => 'form-control datepicker valid_fast_accept date save_payment', 'id'=>"payment_data_$key"]) }}
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                            <label class="control-label">Офици. %</label>
                            {{ Form::text("contract[0][payment][$key][official_discount]", '', ['class' => 'form-control sum save_payment', 'id'=>"official_discount_$key"]) }}
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                            <label class="control-label">Неофиц. %</label>
                            {{ Form::text("contract[0][payment][$key][informal_discount]", '', ['class' => 'form-control sum save_payment', 'id'=>"informal_discount_$key"]) }}
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                            <label class="control-label">Банк %</label>
                            {{ Form::text("contract[0][payment][$key][bank_kv]", '', ['class' => 'form-control sum save_payment', 'id'=>"bank_kv_$key"]) }}
                        </div>
                    </div>
                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                            <label class="control-label">Тип оплаты</label>
                            {{ Form::select("contract[0][payment][$key][payment_type]", collect(\App\Models\Contracts\Payments::PAYMENT_TYPE), '', ['class' => 'form-control select2-ws save_payment']) }}
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                            <label class="control-label">Поток оплаты</label>
                            {{ Form::select("contract[0][payment][$key][payment_flow]", collect(\App\Models\Contracts\Payments::PAYMENT_FLOW), '', ['class' => 'form-control select2-ws save_payment']) }}
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label pull-left" style="margin-top: 5px;">
                                Квитанция
                                <a href="javascript:void(0);" class="btn-xs btn-primary" data-create_receipt="{{$key}}" style="display: inline">Е-квит</a>
                            </label>

                            {{ Form::text( "contract[0][payment][{$key}][bso_receipt]", '', ['class' => 'form-control valid_accept class_bso_receipt save_payment', 'id'=>"bso_receipt_{$key}"]) }}

                            <input type="hidden" name="contract[0][payment][{{$key}}][bso_receipt_id]" id="bso_receipt_id_{{$key}}" value=""/>
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="work-area-item">
                            <span class="btn btn-primary btn-left" style="margin-top: 15px;" onclick="save_contract()">Сохранить</span>
                        </div>
                        <span class="btn btn-danger btn-right" style="margin-top: 15px;" onclick="RedirectBack()">Вернуться</span>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <script>
        function RedirectBack() {

            window.location.href = "/contracts/temp_contracts/";
        }

        function save_contract(){

            $.ajax({
                type: "POST",
                url: "{{url("/contracts/temp_contracts/contract/{$contract->id}/edit")}}",
                async: false,
                data: $('.save_payment').serialize(),

                success: function (response) {
                    flashHeaderMessage('Успешно сохранено!', 'success');
                }
            });
        }
    </script>
@else
    <h3>Платежи</h3>
    <div class="block-view">
        <div class="row">
            <div class="form-horizontal">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    @include('contracts.contract_templates.default.payments.partials.view.payment', ['payments' => $contract->payments])
                </div>
            </div>
        </div>
    </div>
@endif
