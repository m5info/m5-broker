
<div class="block-view">
    <div class="row">
        <div class="form-horizontal">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div class="view-field">
                    <span class="view-label">БСО ({{$contract->product->title}}) {{ collect(\App\Models\Contracts\Contracts::TYPE)[$contract->type_id] }}</span>
                    <span class="view-value">{{ $contract->bso ? $contract->bso->bso_title : '' }}</span>
                </div>

                @isset($contract->insurance_companies)
                    <div class="view-field">
                        <span class="view-label">Наименование СК</span>
                        <span class="view-value">{{ $contract->insurance_companies->title }}</span>
                    </div>
                @endisset


                @isset($contract->product->title)
                    <div class="view-field">
                        <span class="view-label">Продукт</span>
                        <span class="view-value">{{ $contract->product->title }}</span>
                    </div>
                @endisset

                <div class="view-field">
                    <span class="view-label">Срочно</span>
                    <span class="view-value">{{(isset($order->urgency) && $order->urgency == 1) ? "Да" : "Нет"}}</span>
                </div>

                <div class="view-field">
                    <span class="view-label">Без оформления</span>
                    <span class="view-value">{{(isset($order->without_forming) && $order->without_forming == 1) ? "Да" : "Нет"}}</span>
                </div>

                <div class="view-field">
                    <span class="view-label">Дата заключения</span>
                    <span class="view-value">{{setDateTimeFormatRu($contract->sign_date, 1)}}</span>
                </div>

                <div class="view-field">
                    <span class="view-label">Период действия</span>
                    <span class="view-value">{{setDateTimeFormatRu($contract->begin_date, 1)}} - {{setDateTimeFormatRu($contract->end_date, 1)}}</span>
                </div>

                <div class="view-field">
                    <span class="view-label">Банк</span>
                    <span class="view-value">{{$contract->bank_id ? $contract->bank->title : 'Нет'}}</span>
                </div>

                <div class="view-field">
                    <span class="view-label">Страховая премия</span>
                    <span class="view-value">{{titleFloatFormat($contract->payment_total)}}</span>
                </div>

                <div class="view-field">
                    <span class="view-label">Условие продажи</span>
                    <span class="view-value">{{collect(\App\Models\Contracts\Contracts::SALES_CONDITION)[$contract->sales_condition]}}</span>
                </div>

                @if($contract->sales_condition == 0)
                    <div class="view-field">
                        <span class="view-label">Агент</span>
                        <span class="view-value">{{ ($contract->agent) ? $contract->agent->name: '' }}</span>
                    </div>
                @elseif($contract->sales_condition == 1)
                    <div class="view-field">
                        <span class="view-label">Курьер</span>
                        <span class="view-value">{{ ($contract->agent) ? $contract->agent->name: '' }}</span>
                    </div>

                    <div class="view-field">
                        <span class="view-label">Менеджер</span>
                        <span class="view-value">{{ ($contract->manager) ? $contract->manager->name: '' }}</span>
                    </div>
                @elseif($contract->sales_condition == 2)
                    <div class="view-field">
                        <span class="view-label">Курьер</span>
                        <span class="view-value">{{ ($contract->agent) ? $contract->agent->name: '' }}</span>
                    </div>

                    <div class="view-field">
                        <span class="view-label">Агент</span>
                        <span class="view-value">{{ ($contract->manager) ? $contract->manager->name: '' }}</span>
                    </div>
                @endif



                <div class="view-field">
                    <span class="view-label">Заявка из фронт офиса</span>
                    <span class="view-value">{!! divide_by('<br>', 6, $contract->order_title) !!}</span>
                </div>



                @if($contract->masks)
                    <div class="view-field">
                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" style="font-size: 15px;">
                        @foreach($contract->masks as $mask)

                        <a href="{{url($mask->url)}}" target="_blank">{{$mask->original_name}}</a> /

                        @endforeach
                    </div>
                    </div>
                @endif



                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
                    @isset($contract->bank_id)
                        @if($contract->bank_id > 0)
                            <div class="view-field">
                                <span class="view-label">Банк</span>
                                <span class="view-value">{{ $contract->bank->title }}</span>
                            </div>
                        @endif
                    @endisset
                </div>
            </div>
        </div>
    </div>

</div>