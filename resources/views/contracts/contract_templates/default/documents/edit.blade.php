<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-subheading">
        <h2>Документы</h2>
    </div>
    <div class="block-main">
        <div class="block-sub">

            @if($contract->bso_supplier)
                @if($hold_kv_product = $contract->bso_supplier->hold_kv_product($contract->product_id))
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">
                            @include(
                                'contracts.partials.contract_document',
                                ['contract' => $contract, 'hold_kv_product' => $contract->bso_supplier->hold_kv_product($contract->product_id)]
                            )
                        </div>
                    </div>
                @endif
            @endif

        </div>
    </div>

    <script>
       $(document).ready(function() {
            $("a.iframe").fancybox();
        });
    </script>
</div>
