<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-subheading">
        <h2>Объект страхования</h2>
    </div>
    <div class="block-view">
        <div class="block-sub row form-horizontal">
                @include(
                    "contracts.contract_templates.default.insurance_object.partials.view.{$contract->product->category->template}",
                    ['key' => 0, 'object' => $contract->object_insurer]
                )
        </div>
        &nbsp;
    </div>
</div>

