<div class="row form-horizontal">
    <input type="hidden" name="contract[{{$key}}][object_insurer][id]"  value="{{$object->id}}"/>
    <input type="hidden" name="contract[{{$key}}][object_insurer][type]"  value="{{$object->type}}"/>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <label>Наименование</label>
        {{ Form::text("contract[$key][object_insurer][title]", $object->title, ['class' => 'form-control']) }}
    </div>
</div>