<div class="row form-horizontal">
    <input type="hidden" name="contract[{{$key}}][object_insurer][title]" id="object_insurer_title_{{$key}}" value="{{($object)?$object->title:''}}"/>
    <input type="hidden" name="contract[{{$key}}][object_insurer][id]"  value="{{($object)?$object->id:''}}"/>
    <input type="hidden" name="contract[{{$key}}][object_insurer][type]"  value="{{($object)?$object->type:''}}"/>
    <input type="hidden" name="contract[{{$key}}][object_insurer][car_year]" id="object_insurer_car_year_{{$key}}" value="{{($object)?$object->data()->car_year:''}}"/>

    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" >
        <label class="control-label">Категория</label>
        {{Form::select("contract[$key][object_insurer][ts_category]", \App\Models\Vehicle\VehicleCategories::query()->get()->pluck('title', 'id'), ($object && $object)?$object->ts_category: '', ['class' => 'form-control select2-ws', 'id'=>"object_insurer_ts_category_$key", 'onchange'=>"getModelsObjectInsurer($key, 0);"])}}

    </div>

    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" >
        <label class="control-label">Мощность (л.с.)</label>
        {{ Form::text("contract[$key][object_insurer][power]", ($object && $object)?$object->power:'', ['class' => 'form-control sum', "id"=>"object_insurer_ts_power_$key"]) }}
    </div>

    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5" >
        <label class="control-label">Рег. номер</label>
        {{ Form::text("contract[$key][object_insurer][reg_number]", ($object && $object)?$object->reg_number:'', ['class' => 'form-control ru_sumb', "id"=>"object_insurer_ts_reg_number_$key"]) }}
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ></div>

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
        <label class="control-label">Марка</label>
        {{Form::select("contract[$key][object_insurer][mark_id]", \App\Models\Vehicle\VehicleMarks::orderBy('title')->get()->pluck('title', 'id')->prepend('Не выбрано', 0), ($object && $object) ? $object->mark_id : 0, ['class' => 'mark-id mark_id select2-all', "id"=>"object_insurer_ts_mark_id_$key", 'style'=>'width: 100%;', 'onchange'=>"getModelsObjectInsurer($key, 0);"])}}
    </div>

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <label class="control-label">Модель</label>
        {{Form::select("contract[$key][object_insurer][model_id]", \App\Models\Vehicle\VehicleModels::where('mark_id', ($object && $object) ? $object->mark_id : 0)->orderBy('title')->pluck('title', 'id')->prepend('Не выбрано', 0), ($object && $object) ? $object->model_id: 0, ['class' => 'model_id model-id select2-all', "id"=>"object_insurer_ts_model_id_$key", 'style'=>'width: 100%;'])}}
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <label class="control-label">VIN</label>
        {{ Form::text("contract[$key][object_insurer][vin]", ($object && $object) ? $object->vin : '', ['class' => 'form-control', "id"=>"object_insurer_ts_vin_$key"]) }}
    </div>

</div>