<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-subheading">
        <h2>Cтрахователь</h2>
    </div>
    <div class="block-view">
        <div class="block-sub row form-horizontal">



            <div class="view-field">
                <span class="view-label">{{collect([0=>"ФЛ", 1=>'ЮЛ'])[($contract->insurer?$contract->insurer->type:0)]}}</span>
                <span class="view-value">{{$contract->insurer->title}}</span>
            </div>

            @if(!$permission)
            @if($contract->insurer->type == 0)
                <h3>Паспорт</h3>
                    <div class="view-field" >
                        <span class="view-label">Серия</span>
                        <span class="view-value">{{$contract->insurer->doc_serie}}</span>
                    </div>


                    <div class="view-field" >
                        <span class="view-label">Номер</span>
                        <span class="view-value">{{$contract->insurer->doc_number}}</span>
                    </div>
            @endif
            @if($contract->insurer->type == 1)

                <div class="view-field">
                    <span class="view-label">ИНН</span>
                    <span class="view-value">{{$contract->insurer->inn}}</span>
                </div>

                <div class="view-field">
                    <span class="view-label">КПП</span>
                    <span class="view-value">{{$contract->insurer->kpp}}</span>
                </div>


            @endif

            <hr/>

            <div class="view-field">
                <span class="view-label">Телефон</span>
                <span class="view-value">{{$contract->insurer->phone}}</span>
            </div>

            <div class="view-field">
                <span class="view-label">Доп. телефон</span>
                <span class="view-value">{{$contract->insurer->add_phone}}</span>
            </div>

            <div class="view-field">
                <span class="view-label">Email</span>
                <span class="view-value">{{$contract->insurer->email}}</span>
            </div>
            @endif

        </div>
        &nbsp;

    </div>
</div>