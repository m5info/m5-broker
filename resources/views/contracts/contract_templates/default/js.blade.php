<script>

    function addTransaction(invoice_id) {

        @if($contract->bso)
        if (invoice_id > 0){
            openFancyBoxFrame('{{url("/add_payment/{$contract->bso->id}/")}}/?type_id_id='+$("#type_id_id").val()+'&invoice_id='+invoice_id);
        } else {
            openFancyBoxFrame('{{url("/add_payment/{$contract->bso->id}/")}}/?type_id_id='+$("#type_id_id").val());
        }
        @endif

    }

    $(function () {

        initWorkArea();
        setTimeout(initWorkArea, 1000); // временный апгрейд)

        toggle_pay_method();

        $(document).on('click', '[data-create_receipt]', function () {
            var st = $(document).scrollTop();
            var key = $(this).data('create_receipt');
            var payment_id = $('[name="contract[0][payment][' + key + '][id]"]').val();
            openFancyBoxFrame('/contracts/temp_contracts/create_receipt_frame?payment_id=' + payment_id + '&key=' + key)
            setTimeout(function () {
                $(document).scrollTop(st)
            }, 0);
        });


        $('.class_bso_receipt').each(function () {
            key = $(this).attr("id").split('_');
            activSearchBso("bso_receipt", "_" + key[2], 2);

        });


        initInfo();
        initInsurer();


        $("input, select").change(function () {
            $(this).removeClass('form-error');
        });


        $(document).on('change', '[name*="[pay_method_id]"]', function(){
            toggle_pay_method();
        });

    });

    $(document).on('change', '.default_contract_valid', function(){
        $(this).css("border-color", "");

        if ($(this)[0].localName === 'select') {
            var container = $(this).prev('.select2-container');
            container.css({'border': 'none'});
        }
    });

    $(document).on('change', '[name = "contract[0][begin_date]"]',function(){
       $('[name ="contract[0][end_date]"]').change();
    });

    $(document).on('change', '[name = "contract[0][payment][0][pay_method_id]"]', function(){
        if ($(this).val() == 1 || $(this).val() == 7){
            $('.valid_accept').addClass('default_contract_valid');
        }else{
            $('.valid_accept').removeClass('default_contract_valid');
        }
    });


    $.fn.hasAttr = function(name) {
        return this.attr(name) !== undefined;
    };

    //type = 1 Float
    function setValToClassVal(obj, calssName, type)
    {
        setValue = $(obj).val();
        if(parseInt(type) == 1){
            setValue = CommaFormatted(StringToFloat(setValue));
        }

        if (!$("." + calssName).hasAttr('readonly')){
            $("." + calssName).val(setValue);
        }

    }


    function viewSetBsoNotReceipt(obj, key) {
        if ($(obj).is(':checked')) {
            $("#bso_receipt_" + key).attr('disabled', 'disabled');
            $("#bso_receipt_" + key).removeClass('valid_fast_accept');
            $('[name="contract[' + key + '][payment][0][bso_receipt]"]').val('');
            $('[name="contract[' + key + '][payment][0][bso_receipt_id]"]').val('');
        } else {
            $("#bso_receipt_" + key).removeAttr('disabled');
            $("#bso_receipt_" + key).addClass('valid_fast_accept');
        }

        $("#bso_receipt_" + key).css("border-color", "");
    }

    function valid() {

        var first_err;
        var msg_arr = [];

        var i = 0;

        $('.default_contract_valid').each(function () {

            if ($(this)[0].localName != 'div') {

                var tag = $(this)[0].localName;

                var val = '';
                if (['select', 'input'].indexOf(tag) != -1) {
                    val = $(this).val();
                } else {
                    return;
                }

                if (tag == 'select') {

                    var not_selected_val = 0;
                    if ($(this).attr('data-not_selected') !== undefined) {
                        not_selected_val = parseInt($(this).attr('data-not_selected'))
                    }

                    if (parseInt(val) == not_selected_val) {
                        if (i == 0 && $(this).parent('div').css('display') != 'none') {
                            first_err = $(this);
                            i++;
                        }
                        msg_arr.push('Заполните все поля');
                        var container = $(this).prev('.select2-container');
                        container.css({
                            'height': '36px',
                            'border': '1px red solid'
                        });
                    }
                }

                if (val.length < 1 && $(this).parent('div').css('display') != 'none' && $(this).parent('div').parent('div').css('display') != 'none') {

                    if(!$(this).is(':disabled')){
                        if (i == 0) {
                            first_err = $(this);
                            i++;
                        }
                        msg_arr.push('Заполните все поля');

                        var elem = $(this);

                        console.log(elem.parent('div'))
                        elem.css("border-color", "red");
                    }
                }
            }
        });


        $('.temp_contract').each(function (index, value) {
            var bso = $('input[name="contract[' + index + '][bso_title]"]').val();

            $.post('check_correct_bso', {'bso': bso}, function (res) {
                if (res == 'false') {

                    first_err = $('input[name="contract[' + index + '][bso_title]"]');
                    i++;
                    msg_arr.push('Заполните все поля');

                    var elem = $('input[name="contract[' + index + '][bso_title]"]');
                    elem.css("border-color", "red");
                }
            });
        });

        if (msg_arr.length > 0) {
            $('html, body').animate({scrollTop: $(first_err).offset().top - 300}, 800);

            return false;
        }

        return true;
    }


    function setAlgos(key){

        var element = $( "#installment_algorithms_id_"+ key );

        if(element.val() > 0){

            var algorithm_id = element.val();

            if (getInputsInstallmentAlgorithms("payment_algo_" + key, algorithm_id, key, {{$contract->id}})){

                $.ajax({
                    type: "POST",
                    url: "{{url("/settings/installment_algorithms_list/get_quantity")}}",
                    async: true,
                    data: {'algorithm_id' : algorithm_id},
                    success: function (response) {

                        var str = $('[name="contract['+key+'][payment_total]"]').val().replace(/\s/g, '');
                        var premium = parseInt(str);

                        if(parseInt($('[name="contract['+key+'][payment_total]"]').val()) > 0){

                            var payment_sum = premium / response;
                            $('[name="contract['+key+'][payment][0][payment_total]"]').val(payment_sum);

                            var indexes = response + 1;

                            for (var i = 1; i < indexes; i++) {
                                $('#algo_payment_sum_'+ i).val(payment_sum);
                            }
                        };
                    }
                });
            }

        }else{
            $("#payment_algo_" + key).html('');
        }
    }

    function toggle_pay_method() {

        $.each($('[name*="[pay_method_id]"]'), function(k,v){

            var key = $(v).attr('data-key');

            var method_blocks = {
                0:'receipt_block_'+key,
                1:'check_block_'+key,
                2: 'none'
            };

            var method = parseInt($(v).val());

            $.each(method_blocks, function(k,v){
                $("#" + v).hide()
            });
            $("#" + method_blocks[pay_methods_id2types[method]]).show()
        });


    }


    function process(operations = []){
        $.each(operations, function(k,operation){
            if(isFunction(operation)){
                window[operation]()
            }
        })
    }

    function set_buttons() {

        var select = $('.kind_acceptance').select2('val');


        if (select == 0){
            $('.hide_on_1').show();
            $('.hide_on_0').hide();
        }else if(select == 1){
            $('.hide_on_0').show();
            $('.hide_on_1').hide();
        }else{
            $('.hide_on_0').show();
            $('.hide_on_1').show();
        }

    }

    function set_comment(){

        var message = $('[name="contract[message]"]').val();
        if (message.length > 0) {
            $.post("{{url("/contracts/temp_contracts/contract/{$contract->id}/set_comment/")}}", {'message': message}, function(res){
                reload();
            });
        } else {
            flashHeaderMessage('Введите комментарий!', 'danger')
        }

    }


    function save_contract(){

        $.ajax({
            type: "POST",
            url: "{{url("/contracts/temp_contracts/contract/{$contract->id}/edit")}}",
            async: false,
            data: $("#main_container").find("select, textarea, input").serialize(),

            success: function (response) {
                flashHeaderMessage('Успешно сохранено!', 'success');
            }
        });
    }

    function save_contract_quiet() {
        $.ajax({
            type: "POST",
            url: "{{url("/contracts/temp_contracts/contract/{$contract->id}/edit")}}",
            async: false,
            data: $("#main_container").find("select, textarea, input").serialize(),
        });
    }
    
    function saveComment() {
        if ($('[name="contract[message]"]').val().length > 0){
            var message = $('[name="contract[message]"]').val();
            myPostAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/saveOnlyMessage/")}}?message="+message)
            $('[name="contract[message]"]').val('');
        }
    }

    function myvalid() {
        data = $("#main_container").find("select, textarea, input").serializeArray();

        validation = 1;
        type_of_pay = -1;

        data.forEach(function(elem){

            if (elem.name == 'contract[0][insurer][fio]') {
                if (elem.value == ""){
                    flashMessage('danger', 'Не заполнено "ФИО страхователя"');
                    validation = 0;
                }
            }

            if (elem.name == 'contract[0][payment][0][pay_method_id]'){
                if (elem.value == 1){
                    type_of_pay = 1;
                }
            }

            if (elem.name == 'contract[0][payment][0][bso_receipt]'){
                if (elem.value == "" && type_of_pay == 1){
                    flashMessage('danger', 'Не заполнено "Квитанция"');
                    validation = 0;
                }
            }

        });

        return validation;

    }

    function deleteTempContract()
    {
        Swal.fire({
            title: 'Удалить договор и платежи?',
            text: "Вы действительно хотите удалить весь договор",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Да удалить!',
            cancelButtonText: 'Отмена'
        }).then((result) => {
            if (result.value) {
                if (myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/delete_contract")}}")) {
                    window.location = '/contracts/temp_contracts/';
                }
            }
        });
    }


    function to_check(){
        if (valid() == 1){
            var response = myPostAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/send_check/")}}")
            if (response) {
                if(response.msg && response.msg.length){
                    $.each(response.msg, function (index, value) {
                        flashHeaderMessage(value, 'danger');
                    });
                }else{
                    var module = '{{ auth()->user()->is('under') ? "verification" : "temp_contracts" }}';
                    location.href = '/contracts/' + module + '/';
                }
            }
        }

    }

    function to_fixed(){
        var message = $('[name="contract[message]"]').val();
        $.post("{{url("/contracts/temp_contracts/contract/{$contract->id}/send_to_fixed/")}}", {'message': message}, function(response){
            if(response.msg && response.msg.length){
                $.each(response.msg, function (index, value) {
                    flashHeaderMessage(value, 'danger');
                });
            }else{
                var module = '{{ auth()->user()->is('under') ? "verification" : "temp_contracts" }}';
                location.href = '/contracts/' + module + '/';
            }
        });
    }

    function set_error(){
        if (validateContract()){

            var message = $('[name="contract[message]"]').val();

            if (message.length > 0) {
                var response = myPostAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/error_contract/")}}?message="+message)
                if(response.msg && response.msg.length){
                    $.each(response.msg, function (index, value) {
                        flashHeaderMessage(value, 'danger');
                    });
                }else{
                    var module = '{{ auth()->user()->is('under') ? "verification?set_tab=1" : "temp_contracts" }}';
                    location.href = '/contracts/' + module;
                }
            } else {
                flashHeaderMessage('Введите комментарий к ошибке!', 'danger')
            }
        }
    }


    function set_accept(){


        var accept_select = $('[name="kind_acceptance"]');
        var accept_link = $('div.kind_acceptance a');

        if (parseInt(accept_select.val()) < 0) {
            var old = accept_link.css("border");
            accept_link.css("border-color", "red");
            setTimeout(function () {
                accept_link.css("border", old);
            }, 2000);
            flashHeaderMessage('Необходимо выбрать тип акцепта', 'danger')

        } else {

            if (validateContract()){

                var response = myPostAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/accept_contract")}}?kind_acceptance="+accept_select.val());
                if(response.msg && response.msg.length){
                    $.each(response.msg, function (index, value) {
                        flashHeaderMessage(value, 'danger');
                    });
                }else{
                    var module = '{{ auth()->user()->is('under') ? "verification" : "temp_contracts" }}';
                    location.href = '/contracts/' + module + '?set_tab=1';
                }
            }
        }
    }

    function validateContract(){

        if ($('#financial_policy_id_0').val() > 0){
            return true;
        }else{
            flashHeaderMessage('Необходимо указать фин.политику', 'danger')

            return false;
        }
    }



    function selectBso(object_id, key, type, suggestion) {
        var data = suggestion.data;
        $("#bso_receipt_id" + key).val(data.bso_id);
    }




    function initDuplicates() {
        var items = [];
        $("input, select").each(function () {
            if (items[$(this).attr('name')]) {
                var duplicate = $("input[name='" + $(this).attr('name') + "']");
                if (duplicate[1]) {
                    $("input[name='" + $(this).attr('name') + "']").change(function () {
                        let old = $(this);
                        $(duplicate).each(function () {
                            $(this).val(old.val())
                        });
                    });
                }
            }
            items[$(this).attr('name')] = 1;
        });
    }

    document.addEventListener("DOMContentLoaded", function (event) {
        initDuplicates();
    });


    function addPayment() {

        save_contract();

        setTimeout(function(){
            if (myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/add_pay/")}}")) {
                reload();

            }
        }, 50);

    }

    function deleteTempPayment(pay) {
        save_contract();

        setTimeout(function(){
            if (myGetAjax("{{url("/contracts/temp_contracts/contract/{$contract->id}/del_pay/?pay=")}}" + pay)) {
                reload();
            }
        }, 50);
    }


    function viewSetBsoNotReceipt(obj, key) {
        if ($(obj).is(':checked')) {
            $("#bso_receipt_" + key).attr('disabled', 'disabled');
            $("#bso_receipt_" + key).removeClass('valid_fast_accept');
        } else {
            $("#bso_receipt_" + key).removeAttr('disabled');
            $("#bso_receipt_" + key).addClass('valid_fast_accept');
        }
        $("#bso_receipt_" + key).css("border-color", "");
    }

    @if($contract->statys_id == 4)

        $('#main_container input').each(function () {
            if ($(this).is(":visible")) {
                if ($(this).is(':checkbox')) {
                    if ($(this).val()) {
                        $(this).replaceWith('<span style="padding:3px;">Да</span >');
                    } else {
                        $(this).replaceWith('<span  style="padding:3px;">Нет</span >');
                    }
                } else {
                    $(this).replaceWith('<p style="padding-top:5px;">' + $(this).val() + '</p>');
                }
            }
        });

        $('#main_container select').prop('disabled', true);

    @endif


    function initInfo(){

        viewControllerSalesCondition();

        getOptionInstallmentAlgorithms("installment_algorithms_id_0", '{{$contract->insurance_companies_id}}', '{{$contract->installment_algorithms_id}}');

        @if(auth()->user()->hasPermission('contracts', 'select_financial_policy'))
            getOptionFinancialPolicy(
                "financial_policy_id_0",
                '{{$contract->insurance_companies_id}}',
                '{{$contract->bso_supplier_id}}',
                '{{$contract->product_id}}',
                '{{$contract->financial_policy_id}}',
                '{{$contract->sign_date}}',
            );
        @endif

        @if(auth()->user()->hasPermission('contracts', 'set_financial_policy_manually'))
            viewSetFinancialPolicyManually($("#financial_policy_manually_set_0"), '0');
        @endif

        @if((int)$contract->order_id > 0 && !auth()->user()->is('admin'))
        @else
        activSearchOrdersToFront("order_title", "_0");
        @endif
    }

    function refreshOptionFinancialPolicy(elem) {
        @if(auth()->user()->hasPermission('contracts', 'select_financial_policy'))

            getOptionFinancialPolicy(
                "financial_policy_id_0",
                '{{$contract->insurance_companies_id}}',
                '{{$contract->bso_supplier_id}}',
                '{{$contract->product_id}}',
                '{{$contract->financial_policy_id}}',
                $(elem).val(),
            );
        @endif
    }


    function get_end_dates(start_date) {
        var cur_date_tmp = start_date.split(".");
        var cur_date = new Date(cur_date_tmp[2], cur_date_tmp[1] - 1, cur_date_tmp[0]);
        var new_date = new Date(cur_date.setYear(cur_date.getFullYear() + 1));
        var new_date2 = new Date(new_date.setDate(new_date.getDate() - 1));
        return getFormattedDate(new_date2);
    }

    function setAllDates(key){
        var sign_date = $("#sign_date_"+key).val();
        $("#begin_date_"+key).val(sign_date);

        return setEndDates(key);
    }

    function setEndDates(key){
        var begin_date = $("#begin_date_"+key).val();
        $("#end_date_"+key).val(get_end_dates(begin_date));
    }


    function viewControllerSalesCondition() {
        if($("#sales_condition_0").val() != 0) {
            $(".sales_condition_not_agent").show();
        }else{
            $(".sales_condition_not_agent").hide();
        }

    }

    function viewSetFinancialPolicyManually(obj, key){
        if($(obj).is(':checked')){
            $("#financial_policy_manually_"+key).show();
            $("#financial_policy_id_block").hide();
        }else{
            $("#financial_policy_manually_"+key).hide();
            $("#financial_policy_id_block").show();
        }
    }


    function initInsurer() {

        $('#insurer_type_0').change(function () {

            if(parseInt($(this).val()) === 0){
                $('.insurer_fl').show();
                $('.insurer_ul').hide();
            }else{
                $('.insurer_fl').hide();
                $('.insurer_ul').show();
            }

        });

        $('#insurer_type_0').change();

        $('#insurer_fio_'+0).suggestions({
            serviceUrl: '{{url("/custom_suggestions/suggest_physical/")}}',
            token: DADATA_TOKEN,
            type: "NAME",
            count: 5,
            onSelect: function (suggestion) {
                var data = suggestion.data;
                console.log(data);
                var key = $(this).data('key');

                $('#insurer_title_'+key).val($(this).val());
                $('#insurer_doc_serie_'+key).val(data.doc_serie);
                $('#insurer_doc_number_'+key).val(data.doc_number);
                $('#insurer_phone_'+key).val(data.phone);
                $('#insurer_email_'+key).val(data.email);

            }
        });

        $('#insurer_title_'+0+', #insurer_inn_'+0+', #insurer_kpp_'+0).suggestions({
            serviceUrl: '{{url("/custom_suggestions/suggest_jure/")}}',
            token: DADATA_TOKEN,
            type: "PARTY",
            count: 5,
            onSelect: function (suggestion) {
                var data = suggestion.data;
                var key = $(this).data('key');

                $('#insurer_title_'+key).val(suggestion.value);
                $('#insurer_inn_'+key).val(data.inn);
                $('#insurer_kpp_'+key).val(data.kpp);
                $('#insurer_fio_'+key).val($('#insurer_title_'+key).val());
                $('#insurer_phone_'+key).val(data.phone);
                $('#insurer_email_'+key).val(data.email);

            }
        });

        if(isFunction('initDocuments')){
            initDocuments();
        }

    }

    function getModelsObjectInsurer(KEY, select_model_id) {

        $.getJSON(
            '{{url("/contracts/actions/get_models")}}',
            {categoryId:$('#object_insurer_ts_category_'+KEY).val(),markId: $('#object_insurer_ts_mark_id_'+KEY).select2('val')},
            function (response) {

                var options = "<option value='0'>Не выбрано</option>";
                response.map(function (item) {
                    options += "<option value='" + item.id + "'>" + item.title + "</option>";
                });

                $('#object_insurer_ts_model_id_'+KEY).html(options).select2('val', select_model_id);

        });

    }

</script>

