
{{--Условия договора--}}
@include('contracts.contract_templates.online_contracts.terms.diagnostic_card.edit', [
    'object'=>(isset($contract->object_insurer_auto))?$contract->object_insurer_auto:new \App\Models\Contracts\ObjectInsurerAuto(),
    'contract' => $contract
])

{{--Транспортное средство--}}
@include('contracts.contract_templates.online_contracts.insurance_object.auto.diagnostic_card.edit', [
    'object'=>(isset($contract->object_insurer_auto))?$contract->object_insurer_auto:new \App\Models\Contracts\ObjectInsurerAuto()
])


<div class="block-view">
    <h3>
        &nbsp;

        <span class="btn btn-success btn-left" onclick="saveContract({{$contract->id}});">Сохранить как черновик</span>

        <span class="btn btn-primary btn-right" onclick="releaseDK({{$contract->id}})">Выпустить</span>

    </h3>

    <div class="row">




        @if(auth()->user()->is_parent == 1)

            <div class="col-md-12 col-lg-4" >
                <label class="control-label">Агент <span class="required">*</span></label>
                {{ Form::select('contract[agent_id]', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id'), $contract->agent_id, ['class' => 'form-control select2 ']) }}
            </div>

            <div class="col-md-12 col-lg-4" >
                <label class="control-label">Условие продажи <span class="required">*</span></label>
                {{ Form::select('contract[sales_condition]', collect(\App\Models\Contracts\Contracts::SALES_CONDITION), $contract->sales_condition, ['class' => 'form-control select2-ws']) }}
            </div>

            <div class="col-md-12 col-lg-4" >
                <label class="control-label">Менеджер <span class="required">*</span></label>
                {{ Form::select('contract[manager_id]', \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id'), $contract->manager_id, ['class' => 'form-control select2 ']) }}
            </div>

        @else
            <input type="hidden" name="contract[agent_id]" value="{{auth()->id()}}"/>
            <input type="hidden" name="contract[sales_condition]" value="0"/>
            <input type="hidden" name="contract[manager_id]" value="{{auth()->id()}}"/>

        @endif


    </div>
</div>

<script src="/plugins/jquery/jquery.min.js"></script>
@include('contracts.contract_templates.online_contracts.products.boxes.js')


<script>

    $(function(){

        initDK();


    });

    function releaseDK(contract_id)
    {
        if(saveContract(contract_id) == true)
        {
            if(is_validate() == true)
            {
                $.getJSON('/contracts/online/release_boxes/'+contract_id, {}, function (response) {

                    if(response.state == false){
                        flashMessage('danger', response.msg);
                    }


                });

            }
        }
    }

    function is_validate() {

        validate = true;

        $('.is_validate').each(function() {
            val = ($(this).val())?$(this).val():$(this).html();

            /*
            if($(this).is("select")){

            }
            */

            if(val.length<1){
                validate = false;
                $(this).css("border-color","red");
            }else{
                $(this).css("border-color","");
            }

        });

        if(validate == false)
        {
            flashMessage('danger', 'Заполните все поля');
        }

        return validate;
    }


</script>