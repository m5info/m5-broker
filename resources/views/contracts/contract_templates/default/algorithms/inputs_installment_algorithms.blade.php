@for($i = 1; $i < $algos->quantity; $i++)
    <div class="row form-horizontal">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label class="control-label">Дата следующего платежа</label>
            {{ Form::text("contract[$key][algo_payment][$i][date]", '', ['class' => 'form-control datepicker date', 'id'=>"algo_payment_date_$i"]) }}
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label class="control-label">Сумма следующего платежа</label>
            {{ Form::text("contract[$key][algo_payment][$i][sum]", '', ['class' => "form-control sum algo_payment_sum_$key", 'id'=>"algo_payment_sum_$i", 'onchange'=>"updateAlgos($key)"]) }}
        </div>
    </div>
@endfor
