@if($contract
                ->bso_supplier
                ->hold_kv_product($contract->product_id) && $payment_method = $contract
                ->bso_supplier
                ->hold_kv_product($contract->product_id)
                ->pay_methods_group)

    @if(count($payment_method))
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label class="control-label pull-left">
                Тип оплаты
            </label>

            {{ Form::select('contract[payment][payment_method]', $payment_method->pluck('title', 'id'),
                             ($contract->get_payment_first()?$contract->get_payment_first()->pay_method_id:''),
                             [
                             'class' => 'form-control',
                             'id' => 'payment_method',
                             "onchange"=>"selectViewPaymentMethod()"
                             ]) }}
        </div>
    @else
        @php
            $hold_kv_id = $contract->bso_supplier && $contract->bso_supplier->hold_kv_product($contract->product_id) ? $contract->bso_supplier->hold_kv_product($contract->product_id)->id : false;
        @endphp
        @if($hold_kv_id !== false)
            <p>Доступных видов оплат нет. <a href="/directories/insurance_companies/{{$contract->insurance_companies_id}}/bso_suppliers/{{$contract->bso_supplier_id}}/hold_kv/{{$hold_kv_id}}/edit">Редактировать?</a></p>
        @else
            <p>Доступных видов оплат нет. Проверьте настройки поставщика</p>
        @endif
@endif

@foreach($payment_method as $method)

    @include('contracts.contract_templates.default.payment_method.partials.key_type_'.$method->key_type, [
        'contract' => $contract,
        'method_type_id' => $method->id,
        'method_title' => $method->title,
        'key_type' => $method->key_type,
    ])

@endforeach

<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <br><br>
    <span class="btn btn-success pull-left" id="pay"  {{-- style="display: none;" --}} onclick="payment_confirm_submit()">Подтвердить</span>
    <span class="btn btn-primary pull-right" onclick="return_calc()">Вернуть расчёт</span>
</div>

@else

    <h1 style="color: red;">Не настроен тип оплаты</h1>
    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <br><br>
        <span class="btn btn-primary pull-right" onclick="return_calc()">Вернуть расчёт</span>
    </div>

@endif