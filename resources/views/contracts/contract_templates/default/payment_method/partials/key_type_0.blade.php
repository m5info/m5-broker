
<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12 payment_method_type payment_method_type_{{$method_type_id}}" data-key_type="{{$key_type}}">
    <label class="control-label pull-left">
        {{$method_title}}
    </label>
    {{ Form::text('contract[bso_receipt]', '', ['class' => 'form-control class_bso_receipt', 'data-hidden' => 'bso_receipt_id']) }}
    <input type="hidden" name="contract[bso_receipt_id]" id="bso_receipt_id">
</div>
