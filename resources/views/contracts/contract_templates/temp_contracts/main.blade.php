@php
    $view_type = isset($view_type) ? $view_type : 'view';
@endphp

@extends('layouts.app')

@section('work-area')
    @include("contracts.contract_templates.default.workarea.{$view_type}")
@append

@section('content')

    <div class="page-heading">
        <h2 class="inline-h1">

            Договор
            # {{$contract->id}}
            ({{ \App\Models\Contracts\Contracts::STATYS[$contract->statys_id] }})
        </h2>
        <span class="btn btn-primary btn-right" style="margin-right: 78px" onclick="process(['save_contract'])">Сохранить</span>
    </div>


    <div class="row form-horizontal" id="main_container">

        @include("contracts.contract_templates.temp_contracts.main.{$view_type}")

    </div>

@endsection


@section('js')
    @include('contracts.contract_templates.default.js')
    @include('contracts.contract_templates.temp_contracts.js')

    @if($view_type == 'edit' && in_array($contract->statys_id, [1,7]) && auth()->user()->is('under'))

        <script>
        $(function () {

            process(["set_buttons"]);

        });
        </script>
    @endif

@endsection