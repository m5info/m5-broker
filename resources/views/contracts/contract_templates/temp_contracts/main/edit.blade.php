<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

    <!-- Условия договора -->
    @include("contracts.contract_templates.default.terms.edit")

    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <!-- Страхователь -->
        @include("contracts.contract_templates.default.insurer.edit")

        <!-- Объект страхования -->
        @if(!$permission)
            @include("contracts.contract_templates.default.insurance_object.edit")
        @endif

        <!-- Документы -->
        @include("contracts.contract_templates.default.documents.edit")
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <!-- Платежи -->
    @include("contracts.contract_templates.default.payments.edit")
</div>