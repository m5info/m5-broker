<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

    <!-- Данные договора -->
    @include("contracts.contract_templates.default.terms.view")

    <!-- Страхователь -->
    @include("contracts.contract_templates.default.insurer.view")

    @if(!$permission)
        <!-- Объект страхования -->
        @include("contracts.contract_templates.default.insurance_object.view")
    @endif
</div>

<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

    <!-- Информация по договору -->
    @include("contracts.contract_templates.default.contract.view")

    <!-- Платежи -->
    @include("contracts.contract_templates.default.payments.view", ['view_type'=>'all'])

    <!-- Документы -->
    @include("contracts.contract_templates.default.documents.edit")

</div>