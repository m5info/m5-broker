<div class="row form-horizontal">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div class="view-field">
            <span class="view-label">Категория</span>
            <span class="view-value">{{collect(\App\Models\Vehicle\VehicleCategories::query()->get()->pluck('title', 'id'))[$object->data()->ts_category]}}</span>                                                                                                            
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div class="view-field">
            <span class="view-label">Мощность (л.с.)</span>
            <span class="view-value">{{$object->data()->power}}</span>                                                                                                            
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div class="view-field">
            <span class="view-label">Рег. номер</span>
            <span class="view-value">{{$object->data()->reg_number}}</span>                                                                                                            
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div class="view-field">
            <span class="view-label">Марка</span>
            <span class="view-value">{{collect(\App\Models\Vehicle\VehicleMarks::orderBy('title')->get()->pluck('title', 'id')->prepend('Не выбрано', 0))[$object->data()->mark_id]}}</span>                                                                                                            
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div class="view-field">
            <span class="view-label">Модель</span>
            <span class="view-value">{{collect(\App\Models\Vehicle\VehicleModels::where('mark_id', $object->data()->mark_id)->orderBy('title')->pluck('title', 'id')->prepend('Не выбрано', 0))[$object->data()->model_id]}}</span>                                                                                                            
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div class="view-field">
            <span class="view-label">VIN</span>
            <span class="view-value">{{$object->data()->vin}}</span>                                                                                                            
        </div>
    </div>
</div>