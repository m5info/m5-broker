@extends('layouts.frame')


@section('title')

    {{ $title }}

@endsection

@section('content')


    {{ Form::open(['url' => url('/contracts/temp_contracts/add/payment'), 'method' => 'post', 'class' => 'form-horizontal','id'=>'save_sec_payment']) }}

    <div class="form-group">
        <label class="col-sm-4 control-label">Договор (полис)</label>
        <div class="col-sm-8">
            <input type="hidden" name="payment_id" id="payment_id" value="{{$payment->id}}" required/>
            @if($payment->bso_id > 0)
                <strong>{{$payment->bso->bso_title}}</strong>
                <input type="hidden" name="bso_title" id="bso_title" value="{{$payment->bso->bso_title}}"/>
                <input type="hidden" name="bso_id" id="bso_id" value="{{$payment->bso->id}}"/>
                <input type="hidden" name="bso_supplier_id" id="bso_supplier_id" value="{{$payment->bso->bso_supplier_id}}"/>
                <input type="hidden" name="insurance_companies_id" id="insurance_companies_id"  value="{{$payment->bso->insurance_companies_id}}" />
                <input type="hidden" name="product_id" id="product_id" value="{{$payment->bso->product_id}}"/>
                <input type="hidden" name="agent_id" id="agent_id" value="{{$payment->bso->agent_id}}"/>
                <input type="hidden" name="sk_title" id="sk_title" />
                <input type="hidden" name="agent_title" id="agent_title" />
                <input type="hidden" name="contract_id" id="contract_id" value="{{$payment->contract_id}}"/>
            @else
                {{ Form::text('bso_title', '', ['class' => 'form-control', 'id'=>'bso_title','required'=>'required']) }}

                <input type="hidden" name="bso_id" id="bso_id" />
                <input type="hidden" name="bso_supplier_id" id="bso_supplier_id" />
                <input type="hidden" name="insurance_companies_id" id="insurance_companies_id"  />
                <input type="hidden" name="product_id" id="product_id" />
                <input type="hidden" name="agent_id" id="agent_id" />
                <input type="hidden" name="sk_title" id="sk_title" />
                <input type="hidden" name="agent_title" id="agent_title" />
                <input type="hidden" name="contract_id" id="contract_id"/>
            @endif


        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Дата оплаты</label>
        <div class="col-sm-8">
            {{ Form::text("payment_data", setDateTimeFormatRu($payment->payment_data, 1), ['class' => 'form-control datepicker valid_accept date', 'id'=>"payment_data"]) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Сумма платежа</label>
        <div class="col-sm-8">
            {{ Form::text("payment_total", ($payment->payment_total)?titleFloatFormat($payment->payment_total):'', ['class' => 'form-control sum valid_accept', 'id'=>"payment_total",'required'=>'required']) }}
        </div>
    </div>

    @if(auth()->user()->hasPermission('contracts', 'select_financial_policy'))

        <div class="form-group">
            <label class="col-sm-4 control-label">Финансовая политика</label>
            <div class="col-sm-8">
                {{ Form::select( 'financial_policy_id', ($payment->bso_id > 0)?$payment->get_financial_policy()->pluck('title', 'id') : collect([0=>'Укажите номер договора']), $payment->financial_policy_id, ['class' => 'form-control', 'id'=>'financial_policy_id'] ) }}
            </div>
        </div>

    @endif

    <div class="form-group">
        <label class="col-sm-4 control-label">Офици. %</label>
        <div class="col-sm-8">
            {{ Form::text("official_discount", $payment->official_discount, ['class' => 'form-control sum valid_accept', 'id'=>"official_discount" ,'readonly']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Неофиц. %</label>
        <div class="col-sm-8">
            {{ Form::text("informal_discount", $payment->informal_discount, ['class' => 'form-control sum valid_accept', 'id'=>"informal_discount"]) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Банк %</label>
        <div class="col-sm-8">
            {{ Form::text("bank_kv", $payment->bank_kv, ['class' => 'form-control sum valid_accept', 'id'=>"bank_kv",'readonly']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label" >Вид оплаты</label>
        <div class="col-sm-8">
            {{ Form::select("pay_method_id", \App\Models\Finance\PayMethod::query()->where('is_actual', 1)->pluck('title', 'id'), 0, ['class' => 'form-control select2-ws', 'data-key' => 0, 'onchange'=>'toggle_pay_method()']) }}
            <script>
                var pay_methods_id2types = {
                    @foreach(\App\Models\Finance\PayMethod::query()->where('is_actual', 1)->get() as $method)
                    {!! "$method->id : $method->key_type," !!}
                    @endforeach
                };
            </script>
        </div>
    </div>


    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="receipt_block_0">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label class="control-label pull-left" style="margin-top: 5px;">
                Квитанция
                <a href="javascript:void(0);" class="btn-xs btn-primary" data-create_receipt="0" style="display: inline">Е-квит</a>
                <div id="receipt_error"  style="color: red"></div>
            </label>
            {{ Form::text( "bso_receipt", '', ['class' => 'form-control valid_accept class_bso_receipt', 'id'=>"bso_receipt",'required'=>'required']) }}
            <input type="hidden" name="bso_receipt_id" id="bso_receipt_id" value=""/>
        </div>
    </div>

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="check_block_0" style="display: none">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label class="control-label">Телефон</label>
            {{ Form::text("check_phone", '', ['class' => 'form-control phone']) }}
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label class="control-label">E-mail</label>
            {{ Form::text("check_email", '', ['class' => 'form-control email']) }}
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label class="control-label">Отправить</label>
            {{ Form::select("when_to_send", collect(\App\Models\Api\Atol\AtolCheck::WHEN_TO_SEND), 1, ['class' => 'form-control', 'id'=>'when_to_send_0',]) }}
        </div>
    </div>

    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection




@section('js')

    <script>




        $(function () {
            toggle_pay_method();

            $(document).on('click', '[data-create_receipt]', function () {
                var st = $(document).scrollTop();
                var bso_id = $('#bso_id').val();
                var bso_supplier_id = $('#bso_supplier_id').val();
                var insurance_companies_id = $('#insurance_companies_id').val();
                var agent_id = $('#agent_id').val();
                var contract_id = $('#contract_id').val();
                var bso_title = $('#bso_title').val();
                var payment_data = $('#payment_data').val();
                var payment_total = $('#payment_total').val();
                var financial_policy_id = $('#financial_policy_id').val();
                var official_discount = $('#official_discount').val();
                var informal_discount = $('#informal_discount').val();
                var bank_kv = $('#bank_kv').val();

                if (bso_id === '') {
                    $('#receipt_error').text('Заполните номер полиса!')
                } else {
                    window.$('.fancybox-close').click();
                    openFancyBoxFrame('/contracts/temp_contracts/add/receipt?bso_id=' + bso_id + '&bso_supplier_id='+ bso_supplier_id + '&insurance_companies_id='+ insurance_companies_id + '&agent_id='+ agent_id + '&contract_id='+ contract_id + '&bso_title='+ bso_title + '&payment_data='+ payment_data + '&payment_total='+ payment_total + '&financial_policy_id='+ financial_policy_id + '&official_discount='+ official_discount + '&informal_discount='+ informal_discount + '&bank_kv='+ bank_kv)
                }
                setTimeout(function () {
                    $(document).scrollTop(st)
                }, 0);

            });

            @if($payment->bso_id > 0)
                activSearchBso("bso_receipt", "", 2, null);
            @else
                activSearchBsoSold("bso_title", '', 1);
            @endif

            setParamsFromUrl();
        });

        function setParamsFromUrl() {
            @if(request()->getQueryString())
                @php(parse_str(request()->getQueryString(), $output))
                    @if(isset($output['add_params']) && $output['add_params'])
                    $('#bso_receipt').val('{{isset($output['bso_receipt']) ? $output['bso_receipt'] : ''}}');
                    $('#bso_receipt_id').val('{{isset($output['bso_receipt_id']) ? $output['bso_receipt_id'] : ''}}');
                    $('#bso_id').val('{{isset($output['bso_id']) ? $output['bso_id'] : ''}}');
                    $('#bso_supplier_id').val('{{isset($output['bso_supplier_id']) ? $output['bso_supplier_id'] : ''}}');
                    $('#insurance_companies_id').val('{{isset($output['insurance_companies_id']) ? $output['insurance_companies_id'] : ''}}');
                    $('#agent_id').val('{{isset($output['agent_id']) ? $output['agent_id'] : ''}}');
                    $('#contract_id').val('{{isset($output['contract_id']) ? $output['contract_id'] : ''}}');
                    $('#bso_title').val('{{isset($output['bso_title']) ? $output['bso_title'] : ''}}');
                    $('#payment_data').val('{{isset($output['payment_data']) ? $output['payment_data'] : ''}}');
                    $('#payment_total').val('{{isset($output['payment_total']) ? $output['payment_total'] : ''}}');
                    $('#financial_policy_id').val('{{isset($output['financial_policy_id']) ? $output['financial_policy_id'] : ''}}');
                    $('#official_discount').val('{{isset($output['official_discount']) ? $output['official_discount'] : ''}}');
                    $('#informal_discount').val('{{isset($output['informal_discount']) ? $output['informal_discount'] : ''}}');
                    $('#bank_kv').val('{{isset($output['bank_kv']) ? $output['bank_kv'] : ''}}');
                    $('#bso_not_receipt').prop('checked', false).change();
                    @endif
            @endif
        }

        function selectBso(object_id, key, type, suggestion)
        {

            var data = suggestion.data;

            if(parseInt(type) == 1){ // БСО
                $('#bso_id'+key).val(data.bso_id);
                $('#bso_supplier_id'+key).val(data.bso_supplier_id);
                $('#insurance_companies_id'+key).val(data.insurance_companies_id);
                //$('#product_id'+key).val(data.product_id);
                $('#agent_id'+key).val(data.agent_id);


                $('#sk_title').val(data.bso_sk);
                $('#agent_title').val(data.agent_name);


                getOptionFinancialPolicy("financial_policy_id", data.insurance_companies_id, data.bso_supplier_id, data.product_id, 0);

                activSearchBso("bso_receipt", "", 2, null);

                return getContractInfo(data.bso_id);

            }else{
                $('#bso_receipt_id').val(data.bso_id);
            }





        }


        function toggle_pay_method() {

            $.each($('[name*="pay_method_id"]'), function(k,v){

                if($(this).val() == 1)
                    $('#bso_receipt').attr('required','required');
                else
                    $('#bso_receipt').removeAttr('required');

                var key = $(v).attr('data-key');

                var method_blocks = {
                    0:'receipt_block_'+key,
                    1:'check_block_'+key,
                    2: 'none'
                };
                var method = parseInt($(v).val());

                $.each(method_blocks, function(k,v){
                    $("#" + v).hide()
                });
                $("#" + method_blocks[pay_methods_id2types[method]]).show()
            });


        }

        function getContractInfo(bso_id) {
            //55
            //alert(bso_id);

            $.getJSON("/contracts/actions/get_contract_info/", {bso_id: bso_id}, function (response) {

                $('#contract_id').val(response.contract_id);
                $('#payment_total').val(response.payment_total);
                $('#official_discount').val(response.official_discount);
                $('#informal_discount').val(response.informal_discount);
                $('#bank_kv').val(response.bank_kv);
                $('#pay_method_id').val(response.pay_method_id);
                $('#financial_policy_id').val(response.financial_policy_id);



            });

        }




        function saveSecPayment() {
           if(validAddPayment() == true)
            $("#save_sec_payment").submit();
        }

        function validAddPayment() {

             alert();
             return false;


        }

    </script>


@endsection

