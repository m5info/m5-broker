@extends('layouts.frame')


@section('title')
    Создать квитанцию
@endsection

@section('content')

    {{ Form::open(['url' => '/contracts/temp_contracts/add/receipt', 'method' => 'post', 'class' => 'form-horizontal']) }}

    {{ Form::hidden('bso_id', $bso_item->id) }}
    {{ Form::hidden('bso_supplier_id', request('bso_supplier_id')) }}
    {{ Form::hidden('insurance_companies_id', request('insurance_companies_id')) }}
    {{ Form::hidden('agent_id', request('agent_id')) }}
    {{ Form::hidden('contract_id', request('contract_id')) }}
    {{ Form::hidden('bso_title', request('bso_title')) }}
    {{ Form::hidden('payment_data', request('payment_data')) }}
    {{ Form::hidden('payment_total', request('payment_total')) }}
    {{ Form::hidden('financial_policy_id', request('financial_policy_id')) }}
    {{ Form::hidden('official_discount', request('official_discount')) }}
    {{ Form::hidden('informal_discount', request('informal_discount')) }}
    {{ Form::hidden('bank_kv', request('bank_kv')) }}

    <div class="form-horizontal">

        <div class="form-group">
            <label class="col-sm-4 control-label">Серия</label>
            <div class="col-sm-8">
                {{ Form::select('series_id', collect($bso_series->pluck('bso_serie', 'id')), old('series_id'), ['class' => 'form-control', 'id'=>'bso_serie_id']) }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Номер квитанции</label>
            <div class="col-sm-8">
                {{ Form::text('receipt_number', old('rereceipt_number'), ['class' => 'form-control', 'id'=>'bso_title', 'maxlength' => 12, 'required']) }}
            </div>
        </div>
    </div>

    {{Form::close()}}


@endsection

@section('footer')
    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.create') }}</button>
@endsection


@section('js')
    <script>
        $(function () {



        })
    </script>
@append