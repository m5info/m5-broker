@extends('layouts.frame')


@section('title')

    Электронный полис

@endsection

@section('content')


    {{ Form::open(['url' => url('/contracts/temp_contracts/add/electronic/'), 'method' => 'post', 'class' => 'form-horizontal']) }}


    <div class="form-group">
        <label class="col-sm-4 control-label">Агент</label>
        <div class="col-sm-8">
            {{ Form::select('agent_id', $agents->pluck('name', 'id'), auth()->id(), ['class' => 'form-control select2', 'id'=>'agent_id']) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">СК</label>
        <div class="col-sm-8">
            @if($is_fast_primary)
                {{ Form::select('bso_supplier_id', $bso_suppliers->pluck('title', 'id')->prepend('Выберите страховщика', 0), $bso_suppliers->first()->id, ['class' => 'form-control select2-all', 'id'=>'bso_supplier_id', "onchange"=>"getSuppliersProduct()"]) }}
            @else
                {{ Form::select('bso_supplier_id', $bso_suppliers->pluck('title', 'id')->prepend('Выберите страховщика', 0), 0, ['class' => 'form-control select2-all', 'id'=>'bso_supplier_id', "onchange"=>"getSuppliersProduct()"]) }}
            @endif
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label" >Продукт</label>
        <div class="col-sm-8">
            {{ Form::select('bso_type_id', collect([]), '', ['class' => 'form-control', 'id'=>'bso_type_id', "onchange"=>"getBsoSeries()"]) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Серия</label>
        <div class="col-sm-8">
            {{ Form::select('bso_serie_id', collect([]), '', ['class' => 'form-control', 'id'=>'bso_serie_id']) }}
        </div>
    </div>

    <div class="form-group">
        <span id="error_number" style="color: red"></span>
        <label class="col-sm-4 control-label">Номер договора</label>
        <div class="col-sm-8">
            {{ Form::text('bso_title', '', ['class' => 'form-control', 'id'=>'bso_title']) }}
        </div>
    </div>

    {{ Form::hidden('key', $key) }}
    {{ Form::hidden('from_url', $from_url) }}
    {{Form::close()}}


@endsection

@section('footer')

    <button onclick="createBsoAndContract()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection




@section('js')

    <script>




        $(function () {


            $('#bso_title').click(function () {
                controlValid("bso_title", 0);
            });

            $('#bso_type_id, #bso_serie_id').change(function () {
                controlValid($(this).attr("id"), 0);
            });



            getSuppliersProduct();

        });


        function getSuppliersProduct()
        {
            bso_supplier_id = $("#bso_supplier_id").val();

            if(parseInt(bso_supplier_id)>0){

                $.getJSON('{{url('/bso/actions/get_supplier_product/')}}', {bso_supplier_id:bso_supplier_id, e_product: 1, from_url: '{{request('from_url')}}'}, function (response) {

                    var options = "<option value='0'>Не выбрано</option>";
                    response.map(function (item) {
                        options += "<option value='" + item.id + "' {{ $is_fast_primary ? 'selected' : '' }} >" + item.title + "</option>";
                    });

                    $("#bso_type_id").html(options);
                    controlValid("bso_type_id", 0);
                    getBsoSeries();

                });

            }
        }

        function getBsoSeries(){
            bso_type_id = $("#bso_type_id").val();
            bso_supplier_id = $('#bso_supplier_id').val();

            if(parseInt(bso_type_id)>0){
                $.getJSON('{{url('/bso/actions/get_series/')}}', {bso_type_id: bso_type_id, bso_supplier_id:bso_supplier_id, is_electronic:1}, function (response) {

                    var options = "<option value='0'>Не выбрано</option>";
                    response.map(function (item) {
                        options += "<option value='" + item.id + "' {{ $is_fast_primary ? 'selected' : '' }} >" + item.bso_serie + "</option>";
                    });

                    $("#bso_serie_id").html(options);
                    controlValid("bso_serie_id", 0);

                });
            }
        }

        function checkExsistenceBso() {
            var bso_title = $('#bso_title').val();
            var bso_serie_id = $('#bso_serie_id').val();

            var result = false;

            $('#error_number').text('');

            $.ajax({
                url: '{{url("/bso/actions/check_exsistence_bso")}}',
                type: 'POST',
                async: false,
                data: {bso_title:bso_title, bso_serie_id:bso_serie_id},
                success: function (response) {
                    if (response.status === 1){
                        result = true;
                    }else{
                        $('#error_number').text('БСО с таким номером существует!');
                    }
                }
            });

            return result;
        }


        function createBsoAndContract()
        {
            bso_supplier_id = $('#bso_supplier_id').val();
            bso_type_id = $("#bso_type_id").val();
            bso_serie_id = $("#bso_serie_id").val();
            bso_title = $("#bso_title").val();
            agent_id = $("#agent_id").val();
            key = $("[name='key']").val();

            if(parseInt(bso_type_id) > 0){

                if(parseInt(bso_serie_id) > 0){

                    if(bso_title.length > 0 && checkExsistenceBso()){

                        $.ajax({
                            url: '{{url("/contracts/temp_contracts/add/electronic_for_primary/")}}',
                            type: 'POST',
                            data: {
                                bso_supplier_id:bso_supplier_id,
                                bso_type_id:bso_type_id,
                                bso_serie_id:bso_serie_id,
                                bso_title:bso_title,
                                agent_id:agent_id,
                                key:key
                            },
                            success: function (response) {
                                if (response['status'] === 1){
                                    var data = {};
                                    data['data'] = response['contract'];
                                    var key = response['key'];
                                    window.parent.jQuery('#bso_title_' + key).val(data['data'].bso_title);
                                    window.parent.selectBso("bso_title", '_' + key, 1, data, true);

                                    if (window.parent.jQuery('financial_policy_id_'+key)) {
                                        getOptionFinancialPolicyForParent("financial_policy_id_" + key, data['data'].insurance_companies_id, data['data'].bso_supplier_id, data['data'].product_id, data['data'].financial_policy_id);
                                    }
                                }
                                window.parent.flashHeaderMessage('Полис успешно создан', 'success');
                                window.parent.jQuery.fancybox.close();
                            }
                        });

                    }else{
                        controlValid("bso_title", 1);
                        return;
                    }
                }else{
                    controlValid("bso_serie_id", 1);
                    return;
                }
            }else{
                controlValid("bso_type_id", 1);
                return;
            }
        }


        function controlValid(element_id, state)
        {
            if(state == 1){
                $("#"+element_id).css("border-color","red");
            }else{
                $("#"+element_id).css("border-color","");
            }
        }




    </script>


@endsection

