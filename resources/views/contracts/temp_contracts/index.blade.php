@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">
@append

@section('content')


    <div class="page-heading">
        <h2>Временные договора

            <div class="pull-right">
                @if(auth()->user()->hasPermission('contracts', 'show_e_policy'))
                <span class="btn btn-primary btn-right"
                  onclick="openFancyBoxFrame('{{url("/contracts/temp_contracts/add/electronic/")}}')">
                    <i class="fa fa-plus"></i> E-Полис
                </span>
                @endif
                <span class="btn btn-primary btn-right"
                      onclick="openFancyBoxFrame('{{url("/contracts/temp_contracts/add/paper/")}}')">
                    <i class="fa fa-plus"></i> Бумажный договор
                </span>

                {{--<span class="btn btn-success btn-right"
                      onclick="openFancyBoxFrame('{{url("/contracts/temp_contracts/add/payment/")}}')">
                    <i class="fa fa-plus-circle"></i>  Второй взнос
                </span>--}}
            </div>

        </h2>
    </div>

    <div class="header_bab">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @if(sizeof($count_arr))
                <div id="tt" class="easyui-tabs" data-options="tools:'#tab-tools'">
                    @foreach($count_arr as $key => $count)
                        <div title="{{$count['title']}} {{($count['count']>0)?$count['count']:''}}" id="tab-{{$key}}"
                             data-view="{{$key}}"></div>
                    @endforeach
                </div>
            @else
                У вас нет доступных вкладок в этом разделе
            @endif
        </div>
    </div>
    <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container"
         style="margin-top: -5px;overflow: auto;">
        <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="filter-group">

                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="sk">СК</label>
                            {{ Form::select('sk', $sks->pluck('title', 'id')->prepend('Все', 0), request('sk', 0), ['class' => 'form-control select2-all',  'onchange'=>'loadData()',"multiple" => true]) }}
                        </div>

                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="bso_title">БСО</label>
                            {{ Form::text('bso_title', request('bso_title', ''), ['class' => 'form-control',  'onkeyup'=>'loadData()']) }}
                        </div>

                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="product">Продукт</label>
                            {{ Form::select('product', $products->pluck('title', 'id')->prepend('Все', 0), request('product', 0), ['class' => 'form-control select2-ws',  'onchange'=>'loadData()']) }}
                        </div>


                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label class="control-label" for="insurer">Страхователь</label>
                            {{ Form::text('insurer', request('insurer', ''), ['class' => 'form-control',  'onkeyup'=>'loadData()']) }}
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>

                        {{--                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">--}}
                        {{--                            <label class="control-label" for="agent">Тип контракта</label>--}}
                        {{--                            {{ Form::select('contract_type', collect(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE)->prepend('Все', -1), request('contract_type') ?? -1, ['class' => 'form-control select2-ws',  'onchange'=>'loadData()']) }}--}}
                        {{--                        </div>--}}

                        {{--                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">--}}
                        {{--                            <label class="control-label" for="agent">Агент</label>--}}
                        {{--                            {{ Form::select('agent', $agents->pluck('name', 'id')->prepend('Все', 0), request('agent', 0/*auth()->id()*/), ['class' => 'form-control select2',  'onchange'=>'loadData()']) }}--}}
                        {{--                        </div>--}}


                        {{--                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">--}}
                        {{--                            <label class="control-label" for="conclusion_date_from">Дата заключения с</label>--}}
                        {{--                            {{ Form::text('conclusion_date_from', request('conclusion_date_from', ''), ['class' => 'form-control datepicker date',  'onchange'=>'loadData()']) }}--}}
                        {{--                        </div>--}}

                        {{--                        <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">--}}
                        {{--                            <label class="control-label" for="conclusion_date_to">Дата заключения по</label>--}}
                        {{--                            {{ Form::text('conclusion_date_to', request('conclusion_date_to', ''), ['class' => 'form-control datepicker date',  'onchange'=>'loadData()']) }}--}}
                        {{--                        </div>--}}

                        {{--                        <div class="btn-group col-xs-12 col-sm-3 col-md-3 col-lg-3">--}}
                        {{--                            <label class="control-label">Менеджер</label>--}}
                        {{--                            {{ Form::select('manager', \App\Models\Characters\Agent::all()->pluck('name', 'id')->prepend('Все', -1), \Request::query('manager'), ['class' => 'form-control select2', 'id'=>'manager', 'onchange'=>'loadData()']) }}--}}
                        {{--                        </div>--}}

                        {{--                        <div class="btn-group col-xs-12 col-sm-3 col-md-3 col-lg-3">--}}
                        {{--                            <label class="control-label">Курьер</label>--}}
                        {{--                            {{ Form::select('courier', \App\Models\Characters\Courier::all()->pluck('name', 'id')->prepend('Все', -1), \Request::query('courier'), ['class' => 'form-control select2', 'id'=>'courier', 'onchange'=>'loadData()']) }}--}}
                        {{--                        </div>--}}


                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="table" class="table"></div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>

        var map;
        var TAB_INDEX = 0;

        $(function () {

            if ($('[data-view]').length > 0) {
                $('#tt').tabs({
                    border: false,
                    pill: false,
                    plain: true,
                    onSelect: function (title, index) {
                        return selectTab(index);
                    }
                });
                selectTab(0);
            }
        });


        function selectTab(id) {

            TAB_INDEX = id;

            loaderShow();
            loadData();
            initTab();
            loaderHide();

            {{--$.get("{{url("/contracts/temp_contracts/get_tab")}}", getData(), function (response) {--}}

            {{--    $("#main_container").html(response);--}}
            {{--    loadData();--}}
            {{--    initTab();--}}

            {{--}).always(function() {--}}
            {{--    loaderHide();--}}
            {{--});--}}

        }

        function loadData() {
            $.get("{{url("/contracts/temp_contracts/get_table")}}", getData(), function (table_response) {
                $('[name="agent"]').addClass('select2-ws');
                $('[name="product"]').addClass('select2-ws');
                $('[name="contract_type"]').addClass('select2-ws');
                $('#table').html(table_response);
                $('.select2-ws').select2("destroy").select2({
                    width: '100%',
                    dropdownCssClass: "bigdrop",
                    dropdownAutoWidth: true,
                    minimumResultsForSearch: -1
                });


            });
        }


        function getData() {

            var tab = $('#tt').tabs('getSelected');
            var load = tab.data('view');//$("#tab-"+id).data('view');

            return {
                statys: load,
                sk: $('[name="sk"]').val(),
                bso_title: $('[name="bso_title"]').val(),
                product: $('[name="product"]').val(),
                insurer: $('[name="insurer"]').val(),
                agent: $('[name="agent"]').val(),
                conclusion_date_from: $('[name="conclusion_date_from"]').val(),
                conclusion_date_to: $('[name="conclusion_date_to"]').val(),
                contract_type: $('[name="contract_type"]').val()
            }

        }

        function initTab() {
            startMainFunctions();

        }


    </script>


@endsection