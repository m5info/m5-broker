@extends('layouts.app')

@section('content')
    <div class="page-heading">
        <div class="pull-right" style="display: inline-block;">
            <button class="btn btn-success" id="obj_export_xls">ВЫГРУЗИТЬ В XLS</button>
        </div>
        <div>
            <h2 style="padding-bottom:20px; ">Пролонгация</h2>
        </div>
        <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container"
             style="margin-top: -5px;overflow: auto;">
            <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
                {{--{{dump($contracts)}}--}}
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <label class="control-label">Статус</label>
                    @php($status_select = collect(\App\Models\Contracts\Contracts::STATYS)->prepend('Все', -2))
                    {{ Form::select('status', $status_select, 4, ['class' => 'form-control select2-ws', 'id'=>'status', 'required', 'onchange' => 'loadContent()']) }}
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <label class="control-label">СК</label>
                    @php($all_companies = collect(\App\Models\Directories\InsuranceCompanies::get())->pluck('title', 'id')->prepend('Все', ''))
                    {{ Form::select('sk', $all_companies, '', ['class' => 'form-control select2-ws', 'id'=>'sk', 'required', 'onchange' => 'loadContent()']) }}
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <label class="control-label">БСО</label>
                    {{ Form::input('', 'bso_title', '', ['class' => 'form-control', 'id'=>'bso_title', 'placeholder'=>'например, MMM 214345', 'required', 'onkeyup' => 'loadContent()']) }}
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <label class="control-label">Продукт</label>
                    @php($products = collect(\App\Models\Directories\Products::get())->pluck('title', 'id'))
                    {{ Form::select('product', $products, '0', ['class' => 'form-control select2-ws', 'multiple'=>'multiple', 'id'=>'product', 'required', 'onchange' => 'loadContent()']) }}
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <label class="control-label">Страхователь</label>
                    {{ Form::text('insurer', request('insurer', ''), ['class' => 'form-control',  'onkeyup'=>'loadContent()']) }}
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <label class="control-label">Агент / Менеджер</label>
                    {{ Form::select('manager', \App\Models\Characters\Agent::all()->pluck('name', 'id')->prepend('Все', -1), \Request::query('manager'), ['class' => 'form-control select2', 'id'=>'manager', 'onchange'=>'loadItems()']) }}
                </div>

                <div class="clearfix"></div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <label class="control-label">Тип дат</label>
                    {{ Form::select('date_type', collect([0 => 'Диапазон дат', 1 => 'По месяцам']), \Request::query('date_type'), ['class' => 'form-control select2-ws', 'id'=>'date_type', 'onchange'=>'changeDateFiltres()']) }}
                </div>

                <div class="date-filter date-filter-range">
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <label class="control-label">С</label>
                        @php($date = \Carbon\Carbon::parse(\Carbon\Carbon::now()))
                        {{ Form::input('','begin_date', $date->subYears(1)->format('01.m.Y'), ['class' => 'form-control date datepicker', 'id'=>'end_date','placeholder'=>'XX.XX.XXXX', 'required', 'onchange' => 'loadContent()']) }}
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <label class="control-label">По</label>
                        {{ Form::input('', 'end_date', $date->endOfMonth()->format('d.m.Y'), ['class' => 'form-control date datepicker', 'id'=>'begin_date','placeholder'=>'XX.XX.XXXX', 'required', 'onchange' => 'loadContent()']) }}
                    </div>
                </div>
                <div class="date-filter date-filter-month" style="display: none">
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <label class="control-label">Годы</label>
                        @php($date = \Carbon\Carbon::parse(\Carbon\Carbon::now()))
                        @php($years = collect(getYearsArrey()))
                        @php($years->pop())
                        {{ Form::select('years', $years, 2019, ['class' => 'form-control select2-ws', 'multiple'=>'multiple', 'id'=>'years', 'onchange' => 'loadContent()']) }}
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <label class="control-label">Месяц</label>
                        {{ Form::select('month', collect(getRuMonthes()), $date->format('m'), ['class' => 'form-control', 'id'=>'month', 'onchange' => 'loadContent()']) }}
                    </div>
                </div>
            </div>
            <div class="dataTable">

            </div>

        </div>

    </div>

    @include('_chunks/_pagination',['callback'=>'loadItems'])

@endsection

@section('js')
    <script>

        $(document).ready(function () {

            $('#page_list').html('');
            $('#table_row').html('');
            $('#view_row').html(0);
            $('#max_row').html(0);

            $('#product').select2({
                placeholder: 'Продукт'
            });

            loadItems()

        });

        function initTab() {
            startMainFunctions();
            loadContent();
        }

        function changeDateFiltres() {
            var date_type = $('#date_type').val();

            if (date_type == 0){
                $('.date-filter-range').show();
                $('.date-filter-month').hide();
            } else if(date_type == 1){
                $('.date-filter-range').hide();
                $('.date-filter-month').show();

            }

            loadContent();
        }

        function loadItems() {
            loadContent();
        }


        function loadContent() {

            loaderShow();

            $.get("{{url("/contracts/prolongation/get_table")}}", getData(), function (response) {
                $(".dataTable").html(response.html);

                $('#view_row').html(response.view_row);
                $('#max_row').html(response.max_row);

                ajaxPaginationUpdate(response.page_max, loadItems);

            }).always(function () {
                loaderHide();
            });

        }


        $(document).on('click', '#obj_export_xls', function () {
            var query = $.param({
                method: 'Contracts\\Prolongation\\ProlongationController@get_table_custom',
                param: getData(),
                method_param: []
            });
            location.href = '/exports/table2excel?' + query;
        });

        function getData() {
            return {
                status: $('[name="status"]').val(),
                sk: $('[name="sk"]').val(),
                bso_title: $('[name="bso_title"]').val(),
                product: $('[name="product"]').val(),
                insurer: $('[name="insurer"]').val(),
                manager: $('[name="manager"]').val(),
                courier: $('[name="courier"]').val(),
                underwriter: $('[name="underwriter"]').val(),
                page_count: $("#page_count").val(),
                date_type: $("#date_type").val(),
                begin_date: $('[name="begin_date"]').val(),
                end_date: $('[name="end_date"]').val(),
                years: $('#years').val(),
                month: $('#month').val(),
                PAGE: PAGE,
            }
        }

    </script>
@endsection