@extends('layouts.app')

@section('content')
    <div class="page-heading">
        <div class="pull-right" style="display: inline-block;">
            <button class="btn btn-success" id="obj_export_xls">ВЫГРУЗИТЬ В XLS</button>
        </div>
        <div>
            <h2 style="padding-bottom:20px; ">Пролонгация</h2>
        </div>
        <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container"
             style="margin-top: -5px;overflow: auto;">
            <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="control-label">Статус</label>
                    @php($status_select = collect(\App\Models\Contracts\Contracts::STATYS)->prepend('Все', -2))
                    {{ Form::select('status', $status_select, 0, ['class' => 'form-control select2-ws', 'id'=>'status', 'required', 'onchange' => 'loadContent()']) }}
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="control-label">СК</label>
                    @php($all_companies = collect(\App\Models\Directories\InsuranceCompanies::get())->pluck('title', 'id')->prepend('Все', ''))
                    {{ Form::select('sk', $all_companies, '', ['class' => 'form-control select2-ws', 'id'=>'sk', 'required', 'onchange' => 'loadContent()']) }}
                </div>

                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="control-label">БСО</label>
                    {{ Form::input('', 'bso_title', '', ['class' => 'form-control', 'id'=>'bso_title', 'placeholder'=>'например, MMM 214345', 'required', 'onkeyup' => 'loadContent()']) }}
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="control-label">Риск</label>
                    @php($products = collect(\App\Models\Directories\Products::get())->pluck('title', 'id')->prepend('Все', '0'))
                    {{ Form::select('product', $products, '0', ['class' => 'form-control select2-ws', 'id'=>'product', 'required', 'onchange' => 'loadContent()']) }}
                </div>

                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="control-label">Страхователь</label>
                    @php($status_select = collect(\App\Models\Contracts\Subjects::select('id', 'title')->groupBy('title')->distinct()->get())->pluck('title', 'id')->prepend('Все', '-1'))
                    {{ Form::select('insurer', $status_select, -1, ['class' => 'form-control select2-all', 'id'=>'insurer', 'required', 'onchange' => 'loadContent()']) }}
                </div>

                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="control-label">С</label>
                    {{ Form::input('','begin_date', '', ['class' => 'form-control date datepicker', 'id'=>'end_date','placeholder'=>'XX.XX.XXXX', 'required', 'onchange' => 'loadContent()']) }}
                </div>

                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="control-label">По</label>
                    {{ Form::input('', 'end_date', '', ['class' => 'form-control date datepicker', 'id'=>'begin_date','placeholder'=>'XX.XX.XXXX', 'required', 'onchange' => 'loadContent()']) }}
                </div>

                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="control-label">Агент</label>
                    {{ Form::select('manager', \App\Models\Characters\Agent::all()->pluck('name', 'id')->prepend('Все', -1), \Request::query('manager'), ['class' => 'form-control select2-all', 'id'=>'manager', 'onchange'=>'loadItems()']) }}
                </div>

            </div>
            <div class="dataTable">

            </div>

        </div>

    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-left">
            <div class="filter-group">
                {{Form::select('page_count', collect([25=>'25', 50=>'50', 100=>'100', 150=>'150']), request()->has('page')?request()->session()->get('page'):25, ['class' => 'form-control select2-ws', 'id'=>'page_count', 'onchange'=>'loadItems()'])}}
            </div>
        </div>

        <center style="margin-top: 25px; margin-left: 34%; display: inline-block;">
            <span id="view_row"></span>/<span id="max_row"></span>
        </center>


        <div id="page_list" class="easyui-pagination pull-right"></div>
    </div>
@endsection

@section('js')
    <script>

        var PAGE = 1;

        $(document).ready(function () {

            $('#page_list').html('');
            $('#table_row').html('');
            $('#view_row').html(0);
            $('#max_row').html(0);

            loadItems()

        });

        function initTab() {
            startMainFunctions();
            loadContent();
        }

        function loadItems() {
            loadContent();
        }


        function loadContent() {

            loaderShow();

            $.get("{{url("/contracts/prolongation/get_table_custom")}}", getData(), function (response) {
                $(".dataTable").html(response.html);

                $('#view_row').html(response.view_row);
                $('#max_row').html(response.max_row);

                $('#page_list').pagination({
                    total: response.page_max,
                    pageSize: 1,
                    pageNumber: PAGE,
                    layout: ['first', 'prev', 'links', 'next', 'last'],
                    onSelectPage: function (pageNumber, pageSize) {
                        setPage(pageNumber);
                    }
                });

            }).always(function () {
                loaderHide();
            });

        }

        function setPage(field) {
            PAGE = field;
            loadItems();
        }

        $(document).on('click', '#obj_export_xls', function () {
            var query = $.param({
                method: 'Contracts\\Prolongation\\ProlongationController@get_table_custom',
                param: getData(),
                method_param: []
            });
            location.href = '/exports/table2excel?' + query;
        });

        function getData() {
            return {
                status: $('[name="status"]').val(),
                sk: $('[name="sk"]').val(),
                bso_title: $('[name="bso_title"]').val(),
                product: $('[name="product"]').val(),
                insurer: $('[name="insurer"]').val(),
                begin_date: $('[name="begin_date"]').val(),
                end_date: $('[name="end_date"]').val(),
                manager: $('[name="manager"]').val(),
                courier: $('[name="courier"]').val(),
                underwriter: $('[name="underwriter"]').val(),
                page_count: $("#page_count").val(),
                PAGE: PAGE,
            }
        }

    </script>
@endsection