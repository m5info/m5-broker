@php
    $key = 0;
@endphp
@foreach($autosaved_data->contract as $contract)
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 temp_contract" id="contract_{{$key}}" data-key="{{$key}}">
        <div class="trucks-item order-item">
            <div class="info">
                Договор <span class="number">{{$key+1}}</span>

                <button onclick="deleteContract('{{$key}}')" style="background-color: #FFF;font-size: 15px;margin-top: -5px;border: none;" class="pull-right view_but" type="button">
                    <i class="fa fa-remove" style="color: red;"></i>
                </button>

            </div>
            <div class="divider"></div>
            <div class="info">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-4">
                        <h3>Договор (полис)</h3>
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label pull-left">
                                Номер договора <span style="color:red">*</span>
                                @if(auth()->user()->hasPermission('contracts', 'show_e_policy'))
                                    <a href="javascript:void(0);" class="btn-xs btn-primary button-e-policy-{{$key}}" onclick="openFancyBoxFrame('{{url("/contracts/temp_contracts/add/electronic_for_primary/")}}?key={{$key}}&from_url={{request()->path()}}')" style="display: inline">E-Полис</a>
                                @endif
                            </label>
                            <label class="control-label pull-right">
                                Второй взнос
                                {{ Form::checkbox('contract['.$key.'][second_payment]', 1, isset($contract->second_payment) && $contract->second_payment ? 1 : 0, ['style' => "display: inline", 'data-key' => $key, 'onchange' => 'secondPaymentToggle('.$key.')'] ) }}
                            </label>
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class=" bso-title-block-{{$key}}">
                                {{ Form::text('contract['.$key.'][bso_title]', $contract->bso_title, ['class' => 'form-control valid_fast_accept', 'id'=>'bso_title_'.$key]) }}
                            </div>
                            <div class="row col-xs-6 col-sm-6 col-md-4 col-lg-4 second-payment-block-{{$key}}" style="display:none;margin-left: 45px;">
                                {{ Form::text('contract['.$key.'][payment][0][payment_number]', $contract->payment[0]->payment_number, ['class' => 'form-control ', 'id'=>'payment_number_'.$key]) }}
                            </div>
                        </div>

                        <input type="hidden" name="contract[{{$key}}][withdraw_documents]"  value="{{ $contract->withdraw_documents }}"/>
                        <input type="hidden" name="contract[{{$key}}][bso_id]" id="bso_id_{{$key}}" value="{{ $contract->bso_id }}"/>
                        <input type="hidden" name="contract[{{$key}}][bso_supplier_id]" id="bso_supplier_id_{{$key}}" value="{{ $contract->bso_supplier_id }}"/>
                        <input type="hidden" name="contract[{{$key}}][insurance_companies_id]" id="insurance_companies_id_{{$key}}" value="{{ $contract->insurance_companies_id }}"/>
                        <input type="hidden" name="contract[{{$key}}][product_id]" id="product_id_{{$key}}" value="{{ $contract->product_id }}"/>
                        <input type="hidden" name="contract[{{$key}}][agent_id]" id="agent_id_{{$key}}" value="{{ $contract->agent_id }}"/>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label">
                                Заявка из фронт офиса <span style="color:red">*</span>
                            </label>
                            {{ Form::text('contract['.$key.'][order_title]', $contract->order_title, ['class' => 'form-control orders-title valid_fast_accept', 'id'=>'order_title_'.$key, 'data-key' => $key, 'onkeyup' => 'checkOrderTitle('.$key.', event)']) }}
                            <input type="hidden" name="contract[{{$key}}][order_id]" id="order_id_{{$key}}" value="{{ $contract->order_id }}"/>
                            <input type="hidden" name="contract[{{$key}}][order_sort_id]" id="order_sort_id_{{$key}}" value="{{ $contract->order_sort_id }}"/>
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label">
                                Дата заключения <span style="color:red">*</span>
                            </label>
                            {{ Form::text('contract['.$key.'][sign_date]', $contract->sign_date, ['class' => 'form-control datepicker date valid_fast_accept', 'id'=>'sign_date_'.$key, 'onchange'=>'setAllDates('.$key.')']) }}
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <label class="control-label">
                                Период действия <span style="color:red">*</span>
                            </label>
                            <div class="row form-horizontal">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                    {{ Form::text('contract['.$key.'][begin_date]', $contract->begin_date, ['class' => 'form-control date datepicker valid_fast_accept', 'id'=>'begin_date_'.$key, 'placeholder'=>'Дата начала', 'onchange'=>'setEndDates('.$key.')']) }}
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                    {{ Form::text('contract['.$key.'][end_date]', $contract->end_date, ['class' => 'form-control date datepicker valid_fast_accept', 'placeholder'=>'Дата окончания', 'id'=>'end_date_'.$key]) }}
                                </div>
                            </div>
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden">
                            <label class="control-label">
                                Дата выезда
                            </label>
                            {{ Form::text('contract['.$key.'][delivery_date]', $contract->begin_date, ['class' => 'form-control datepicker date', 'id'=>'delivery_date_'.$key, 'onchange'=>'setAllDates('.$key.')']) }}
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label">Условие продажи</label>
                            <input type="hidden" name="contract[{{$key}}][sales_condition]" id="product_id_{{$key}}" value="{{ $contract->sales_condition ?: 1 }}"/>
                            {{ \App\Models\Contracts\Contracts::SALES_CONDITION[$contract->sales_condition ?: 1] }}
                        </div>


                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <label class="control-label pull-left" style="margin-top: 5px;">
                                Менеджер
                            </label>
                            <label class="control-label pull-right">
                                Личная продажа
                                <input type="checkbox" value="1" {{(int)$contract->manager_id ? 'checked' : ''}} id="is_personal_sales_{{$key}}" name="contract[{{$key}}][is_personal_sales]" data-key="{{$key}}"/>
                            </label>
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12 manager_view_outer" id="manager_view_outer_{{$key}}" style="padding-right: 0px;">
                                {{ Form::select('contract['.$key.'][manager_id]', auth()->user()->is('courier') ? $agents->pluck('name', 'id')->prepend('Не выбрано', 0) : $agents->pluck('name', 'id')->prepend($im->name, $im->id) , $contract->manager_id ?: $im->id, ['class' => 'form-control valid_fast_accept select2-all', 'id'=>'manager_id_'.$key]) }}
                            </div>
                        </div>
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <label class="control-label pull-left" style="margin-top: 5px;">
                                Рекомендодатель
                            </label>
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12 referencer_view_outer" id="referencer_view_outer_{{$key}}" style="padding-right: 0px;">
                                {{ Form::select('contract['.$key.'][referencer_id]', $referencers->pluck('name', 'id')->prepend('Не выбрано', 0), $contract->referencer_id ?: 0, ['class' => 'form-control  select2-all', 'id'=>'referencer_id_'.$key]) }}
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-4" >
                        <h3>Порядок оплаты</h3>
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <div class="row form-horizontal">
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" >
                                    <label class="control-label">
                                        Премия по договору <span style="color:red">*</span>
                                    </label>
                                    {{ Form::text('contract['.$key.'][payment_total]', $contract->payment_total, ['class' => 'form-control sum valid_fast_accept', 'id'=>'payment_total_'.$key, 'onkeyup'=>'setPaymentQty('.$key.')', 'onchange' => 'setPaymentQty_removeBorder('.$key.')']) }}
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" >
                                    <label class="control-label">Дата оплаты <span style="color:red">*</span></label>
                                    {{ Form::text('contract['.$key.'][payment][0][payment_data]', $contract->payment[0]->payment_data, ['class' => 'form-control date datepicker valid_fast_accept', 'id'=>'payment_data_'.$key]) }}

                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" >
                                    <label class="control-label">
                                        Сумма платежа <span style="color:red">*</span>
                                    </label>
                                    {{ Form::text('contract['.$key.'][payment][0][payment_total]', $contract->payment[0]->payment_total, ['class' => 'form-control sum valid_fast_accept', 'id'=>'payment_total_'.$key]) }}
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="contract[{{$key}}][installment_algorithms_id]" value="{{ $contract->installment_algorithms_id }}">
                        {{ Form::hidden('contract['.$key.'][payment][0][type_id]', $contract->payment[0]->type_id) }}
                        <input type="hidden" name="contract[{{$key}}][payment][0][order_source]" id="order_source_{{$key}}" value="{{ $contract->payment[0]->order_source }}"/>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <div class="row form-horizontal">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                                    <label class="control-label">
                                        Неофиц. % <span style="color:red">*</span>
                                    </label>
                                    {{ Form::text('contract['.$key.'][payment][0][informal_discount]', $contract->payment[0]->informal_discount, ['class' => 'form-control sum valid_fast_accept percents_input_validation', 'id'=>'informal_discount_'.$key]) }}
                                    {{ Form::hidden('contract['.$key.'][payment][0][official_discount]', $contract->payment[0]->official_discount, ['id' => 'official_discount'.$key]) }}
                                    {{ Form::hidden('contract['.$key.'][payment][0][bank_kv]', $contract->payment[0]->bank_kv, ['id' => 'bank_kv_'.$key]) }}
                                </div>
                            </div>
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label pull-left" style="margin-top: 5px;" id="check_label_{{$key}}">
                                Вид оплаты
                            </label>
                            {{ Form::select('contract['.$key.'][payment][0][pay_method_id]', \App\Models\Finance\PayMethod::query()->where('is_actual', 1)->where('show_in_primary', 1)->pluck('title', 'id'), $contract->payment[0]->pay_method_id, ['class' => 'form-control valid_fast_accept', 'id'=>'check_'.$key, 'data-key' => $key]) }}
                            <script>
                                var pay_methods_id2types = {
                                    @foreach(\App\Models\Finance\PayMethod::query()->where('show_in_primary', 1)->where('is_actual', 1)->get() as $method)
                                    {!! "$method->id : $method->key_type," !!}
                                    @endforeach
                                };
                            </script>
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="receipt_block_{{$key}}">
                            <label class="control-label pull-left" style="margin-top: 5px;" id="bso_receipt_label_{{$key}}">
                                Квитанция
                                @if(auth()->user()->hasPermission('contracts', 'show_e_policy'))
                                    <a href="javascript:void(0);" class="btn-xs btn-primary" data-create_receipt="{{$key}}" style="display: inline">Е-квит</a>
                                @endif
                            </label>
                            {{ Form::text('contract['.$key.'][payment][0][bso_receipt]', $contract->payment[0]->bso_receipt, ['class' => 'form-control valid_fast_accept temp_contract_receipt', 'id'=>'bso_receipt_'.$key, 'data-key' => $key]) }}
                            <input type="hidden" name="contract[{{$key}}][payment][0][bso_receipt_id]" value="{{$contract->payment[0]->bso_receipt_id}}" id="bso_receipt_id_{{$key}}" />
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="check_block_{{$key}}" style="display: none">
                            <div class="row form-horizontal">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <label class="control-label">Телефон</label>
                                    {{ Form::text('contract['.$key.'][check_phone]', $contract->check_phone, ['class' => 'form-control phone', 'id'=>'check_phone_'.$key,]) }}
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <label class="control-label">E-mail</label>
                                    {{ Form::text('contract['.$key.'][check_email]', $contract->check_email, ['class' => 'form-control email', 'id'=>'check_email_'.$key]) }}
                                </div>
                            </div>
                            <div class="row form-horizontal">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <label class="control-label">Отправить</label>
                                    {{ Form::select('contract['.$key.'][when_to_send]', collect(\App\Models\Api\Atol\AtolCheck::WHEN_TO_SEND), $contract->when_to_send, ['class' => 'form-control', 'id'=>'when_to_send_'.$key,]) }}
                                </div>
                            </div>
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none">
                            <label class="control-label pull-left" style="margin-top: 5px;">
                                Финансовая политика
                            </label>
                            <label class="control-label pull-right">Указать в ручную
                                <input type="checkbox" value="1" {{isset($contract->financial_policy_manually_set) && $contract->financial_policy_manually_set ? 'checked' : ''}} name="contract[{{$key}}][financial_policy_manually_set]" onchange="viewSetFinancialPolicyManually(this, '{{$key}}')"/> </label>
                            {{ Form::select('contract['.$key.'][financial_policy_id]', collect([0=>'Укажите номер договора']), $contract->financial_policy_id, ['class' => 'form-control select2-ws', 'id'=>'financial_policy_id_'.$key]) }}


                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" ></div>
                            <div class="row form-horizontal" id="financial_policy_manually_{{$key}}" style="display: none;">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    {{ Form::text('contract['.$key.'][financial_policy_kv_bordereau]', $contract->financial_policy_kv_bordereau, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_bordereau_'.$key, 'placeholder'=>'Бордеро']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    {{ Form::text('contract['.$key.'][financial_policy_kv_dvoy]', $contract->financial_policy_kv_dvoy, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_dvoy_'.$key, 'placeholder'=>'ДВОУ']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    {{ Form::text('contract['.$key.'][financial_policy_kv_agent]', $contract->financial_policy_kv_agent, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_agent_'.$key, 'placeholder'=>'Агента']) }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                    {{ Form::text('contract['.$key.'][financial_policy_kv_parent]', $contract->financial_policy_kv_parent, ['class' => 'form-control sum', 'id'=>'financial_policy_kv_parent_'.$key, 'placeholder'=>'Руков.']) }}
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-4" >
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <label class="control-label">Комментарий</label>
                            {{ Form::text('contract['.$key.'][comment]', $contract->comment, ['class' => 'form-control', 'id'=>'comment_'.$key]) }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-4" >
                        <h3>Cтрахователь</h3>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <div class="row form-horizontal">
                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" >
                                    <label class="control-label">Тип</label>
                                    {{ Form::select('contract['.$key.'][insurer][type]', collect([0=>"ФЛ", 1=>'ЮЛ']), $contract->insurer->type, ['class' => 'form-control select2-ws', 'id'=>'insurer_type_'.$key, 'data-key'=>$key]) }}
                                </div>
                                <div class="insurer_fl col-xs-12 col-sm-12 col-md-9 col-lg-9" >
                                    <label class="control-label">
                                        ФИО <span style="color:red">*</span>
                                    </label>
                                    {{ Form::text('contract['.$key.'][insurer][fio]', $contract->insurer->fio, ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_fio_'.$key, 'data-key'=>$key]) }}
                                </div>
                                <div class="insurer_ul col-xs-12 col-sm-12 col-md-9 col-lg-9" style="display: none">
                                    <label class="control-label">Название</label>
                                    {{ Form::text('contract['.$key.'][insurer][title]', $contract->insurer->title, ['class' => 'form-control', 'id'=>'insurer_title_'.$key, 'data-key'=>$key]) }}
                                </div>

                            </div>
                        </div>

                        <div class="row insurer_fl col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <label class="control-label">Паспорт</label>
                            <div class="row form-horizontal">
                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" >
                                    {{ Form::text('contract['.$key.'][insurer][doc_serie]', $contract->insurer->doc_serie, ['class' => 'form-control', 'placeholder'=>'Серия']) }}
                                </div>
                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" >
                                    {{ Form::text('contract['.$key.'][insurer][doc_number]', $contract->insurer->doc_number, ['class' => 'form-control', 'placeholder'=>'Номер']) }}
                                </div>
                            </div>
                        </div>

                        <div class="row insurer_ul col-xs-12 col-sm-12 col-md-12 col-lg-12"  style="display: none">
                            <div class="row form-horizontal">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <label class="control-label">ИНН</label>
                                    {{ Form::text('contract['.$key.'][insurer][inn]', $contract->insurer->inn, ['class' => 'form-control', 'id'=>'insurer_inn_'.$key, 'data-key'=>$key]) }}
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <label class="control-label">КПП</label>
                                    {{ Form::text('contract['.$key.'][insurer][kpp]', $contract->insurer->kpp, ['class' => 'form-control', 'id'=>'insurer_kpp_'.$key, 'data-key'=>$key]) }}
                                </div>
                            </div>
                        </div>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row form-horizontal">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <label class="control-label">Телефон <span style="color:red">*</span></label>
                                    {{ Form::text('contract['.$key.'][insurer][phone]', $contract->insurer->phone, ['class' => 'form-control phone valid_fast_accept', 'id'=>'insurer_phone_'.$key]) }}
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <label class="control-label">Доп. телефон</label>
                                    {{ Form::text('contract['.$key.'][insurer][add_phone]', $contract->insurer->add_phone, ['class' => 'form-control phone', 'id'=>'insurer_add_phone_'.$key]) }}
                                </div>
                            </div>

                            <div class="row form-horizontal">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <label class="control-label">Email</label>
                                    {{ Form::text('contract['.$key.'][insurer][email]', $contract->insurer->email, ['class' => 'form-control', 'id'=>'insurer_email_'.$key]) }}
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                    <label class="control-label">Метро</label>
                                    {{ Form::text('contract['.$key.'][insurer][underground]', $contract->insurer->underground, ['class' => 'form-control', 'id'=>'insurer_underground_'.$key]) }}
                                </div>
                            </div>
                            <div class="row form-horizontal">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <label class="control-label">Адрес доставки</label>
                                    {{ Form::text('contract['.$key.'][insurer][delivery_address]', $contract->insurer->delivery_address, ['class' => 'form-control', 'id'=>'delivery_address_'.$key]) }}
                                </div>
                            </div>
                        </div>

                        <h3>Объект страхования</h3>

                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="contract_object_insurer_{{$key}}">

                            <center><strong>Укажите номер договора</strong></center>

                        </div>

                        @if((int)$contract->bso_id > 0)
                            <script>
                                getOptionInstallmentAlgorithms("installment_algorithms_id_{{$key}}", {{$contract->insurance_companies_id}}, 0);
                                getOptionFinancialPolicy("financial_policy_id_{{$key}}", {{$contract->insurance_companies_id}}, {{$contract->bso_supplier_id}}, {{$contract->product_id}}, {{$contract->financial_policy_id}});
                                getKvAccesses({{$key}}, {{$contract->product_id}});

                                object_insurer = myGetAjax('{{url("/bso/actions/get_html_mini_contract_object_insurer_with_data/")}}?product_id={{$contract->product_id}}&key={{$key}}&type_id=1');
                                $("#contract_object_insurer_{{$key}}").html(object_insurer);

                                startContractObjectInsurerFunctions({{$key}});

                                var receipt_used = [];
                                $.each($('[name*="][bso_receipt]"]'), function (k, v) {
                                    receipt_used.push($(v).val());
                                });

                                @if (isset($contract->second_payment) && $contract->second_payment)
                                    activSearchBsoSold("bso_title", '_{{$key}}', 1, [], 2, true);

                                    $('.button-e-policy-{{$key}}').hide();
                                    $('.second-payment-block-{{$key}}').show();
                                    $('.bso-title-block-{{$key}}')
                                        .addClass('row')
                                        .addClass('col-md-8')
                                        .addClass('col-lg-8');
                                @else
                                    activSearchBso("bso_receipt", '_{{$key}}', 2, receipt_used);
                                @endif

                                activSearchOrdersToFront("order_title", '_{{$key}}');

                                var key = {{$key}};
                                var method_blocks = {
                                    0: 'receipt_block_' + key,
                                    1: 'check_block_' + key,
                                    2: 'none'
                                };

                                var method = parseInt($('[name="contract['+key+'][payment][0][pay_method_id]"]').val());

                                $.each(method_blocks, function (k, v) {
                                    $("#" + v).hide()
                                });

                                $("#" + method_blocks[pay_methods_id2types[method]]).show();

                                if (method === 1) {
                                    $('[name="contract[' + key + '][payment][0][bso_receipt]"]').addClass('valid_fast_accept');
                                } else {
                                    $('[name="contract[' + key + '][payment][0][bso_receipt]"]').removeClass('valid_fast_accept');
                                }
                            </script>
                        @else
                            <script>
                                @if (isset($contract->second_payment) && $contract->second_payment)
                                    activSearchBsoSold("bso_title", '_{{$key}}', 1, [], 2, true);
                                    $('.button-e-policy-{{$key}}').hide();
                                    $('.second-payment-block-{{$key}}').show();
                                    $('.bso-title-block-{{$key}}')
                                        .addClass('row')
                                        .addClass('col-md-8')
                                        .addClass('col-lg-8');
                                @else
                                    activSearchBso("bso_title", '_{{$key}}', 1, []);
                                @endif

                            </script>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
        $key++;
    @endphp
@endforeach
