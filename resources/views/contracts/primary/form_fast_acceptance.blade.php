<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 temp_contract" id="contract_[:KEY:]" data-key="[:KEY:]">
    <div class="trucks-item order-item">
        <div class="info">
            Договор <span class="number">[:NUM:]</span>

            <button onclick="deleteContract('[:KEY:]')" style="background-color: #FFF;font-size: 15px;margin-top: -5px;border: none;" class="pull-right view_but" type="button">
                <i class="fa fa-remove" style="color: red;"></i>
            </button>

        </div>
        <div class="divider"></div>
        <div class="info">
            <div class="form-group">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-4">
                    <h3>Договор (полис)</h3>
                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label pull-left">
                            Номер договора <span style="color:red">*</span>
                            @if(auth()->user()->hasPermission('contracts', 'show_e_policy'))
                                <a href="javascript:void(0);" class="btn-xs btn-primary button-e-policy-[:KEY:]" onclick="openFancyBoxFrame('{{url("/contracts/temp_contracts/add/electronic_for_primary/")}}?key=[:KEY:]&from_url={{request()->path()}}')" style="display: inline">E-Полис</a>
                            @endif
                        </label>
                        <label class="control-label pull-right">
                            Второй взнос
                            {{ Form::checkbox('contract[[:KEY:]][second_payment]', 1, 0, ['style' => "display: inline", 'data-key' => '[:KEY:]', 'onchange' => 'secondPaymentToggle([:KEY:])'] ) }}
                        </label>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class=" bso-title-block-[:KEY:]">
                            {{ Form::text('contract[[:KEY:]][bso_title]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'bso_title_[:KEY:]']) }}
                        </div>
                        <div class="row col-xs-6 col-sm-6 col-md-4 col-lg-4 second-payment-block-[:KEY:]" style="display:none;margin-left: 45px;">
                            {{ Form::text('contract[[:KEY:]][payment][0][payment_number]', 1, ['class' => 'form-control ', 'id'=>'payment_number_[:KEY:]']) }}
                        </div>
                    </div>


                    <input type="hidden" name="contract[[:KEY:]][withdraw_documents]"  value="0"/>
                    <input type="hidden" name="contract[[:KEY:]][bso_id]" id="bso_id_[:KEY:]" />
                    <input type="hidden" name="contract[[:KEY:]][bso_supplier_id]" id="bso_supplier_id_[:KEY:]" />
                    <input type="hidden" name="contract[[:KEY:]][insurance_companies_id]" id="insurance_companies_id_[:KEY:]" />
                    <input type="hidden" name="contract[[:KEY:]][product_id]" id="product_id_[:KEY:]" />
                    <input type="hidden" name="contract[[:KEY:]][agent_id]" id="agent_id_[:KEY:]" value="{{ auth()->id() }}"/>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label">
                            Заявка из фронт офиса <span style="color:red">*</span>
                        </label>
                        {{ Form::text('contract[[:KEY:]][order_title]', '', ['class' => 'form-control orders-title valid_fast_accept', 'id'=>'order_title_[:KEY:]', 'data-key' => '[:KEY:]', 'onkeyup' => 'checkOrderTitle([:KEY:], event)']) }}
                        <input type="hidden" name="contract[[:KEY:]][order_id]" id="order_id_[:KEY:]" />
                        <input type="hidden" name="contract[[:KEY:]][order_sort_id]" id="order_sort_id_[:KEY:]" />
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label">
                            Дата заключения <span style="color:red">*</span>
                        </label>
                        {{ Form::text('contract[[:KEY:]][sign_date]', '', ['class' => 'form-control datepicker date valid_fast_accept', 'id'=>'sign_date_[:KEY:]', 'onchange'=>'setAllDates([:KEY:])']) }}
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label class="control-label">
                            Период действия <span style="color:red">*</span>
                        </label>
                        <div class="row form-horizontal">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                {{ Form::text('contract[[:KEY:]][begin_date]', '', ['class' => 'form-control date datepicker valid_fast_accept', 'id'=>'begin_date_[:KEY:]', 'placeholder'=>'Дата начала', 'onchange'=>'setEndDates([:KEY:])']) }}
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                {{ Form::text('contract[[:KEY:]][end_date]', '', ['class' => 'form-control date datepicker valid_fast_accept', 'placeholder'=>'Дата окончания', 'id'=>'end_date_[:KEY:]']) }}
                            </div>
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden">
                        <label class="control-label">
                            Дата выезда
                        </label>
                        {{ Form::text('contract[[:KEY:]][delivery_date]', '', ['class' => 'form-control datepicker date', 'id'=>'delivery_date_[:KEY:]', 'onchange'=>'setAllDates([:KEY:])']) }}
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label">Условие продажи</label>
                        <input type="hidden" name="contract[[:KEY:]][sales_condition]" id="product_id_[:KEY:]" value="1"/>
                        {{ \App\Models\Contracts\Contracts::SALES_CONDITION[1] }}
                    </div>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label class="control-label pull-left" style="margin-top: 5px;">
                            Менеджер
                        </label>
                        <label class="control-label pull-right">
                            Личная продажа
                            <input type="checkbox" value="1" id="is_personal_sales_[:KEY:]" name="contract[[:KEY:]][is_personal_sales]" data-key="[:KEY:]"/>
                        </label>
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12 manager_view_outer" id="manager_view_outer_[:KEY:]" style="padding-right: 0px;">
                            {{ Form::select('contract[[:KEY:]][manager_id]', auth()->user()->is('courier') ? $agents->pluck('name', 'id')->prepend('Не выбрано', 0) : $agents->pluck('name', 'id')->prepend($im->name, $im->id) , $im->id, ['class' => 'form-control valid_fast_accept select2-all', 'id'=>'manager_id_[:KEY:]']) }}
                        </div>
                    </div>
                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label class="control-label pull-left" style="margin-top: 5px;">
                            Рекомендодатель
                        </label>
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12 referencer_view_outer" id="referencer_view_outer_[:KEY:]" style="padding-right: 0px;">
                            {{ Form::select('contract[[:KEY:]][referencer_id]', $referencers->pluck('name', 'id')->prepend('Не выбрано', 0), 0, ['class' => 'form-control select2-all', 'id'=>'referencer_id_[:KEY:]']) }}
                        </div>
                    </div>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-4" >
                    <h3>Порядок оплаты</h3>
                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" >
                                <label class="control-label">
                                    Премия по договору <span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][payment_total]', '', ['class' => 'form-control sum valid_fast_accept', 'id'=>'payment_total_[:KEY:]', 'onkeyup'=>'setPaymentQty([:KEY:])', 'onchange' => 'setPaymentQty_removeBorder([:KEY:])']) }}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" >
                                <label class="control-label">Дата оплаты <span style="color:red">*</span></label>
                                {{ Form::text('contract[[:KEY:]][payment][0][payment_data]', '', ['class' => 'form-control date datepicker valid_fast_accept', 'id'=>'payment_data_[:KEY:]']) }}

                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" >
                                <label class="control-label">
                                    Сумма платежа <span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][payment][0][payment_total]', '', ['class' => 'form-control sum valid_fast_accept', 'id'=>'payment_total_[:KEY:]']) }}
                            </div>
                        </div>
                    </div>


{{--                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >--}}
{{--                        <label class="control-label">Алгоритм рассрочки</label>--}}
{{--                        {{ Form::select('contract[[:KEY:]][installment_algorithms_id]', collect([0=>'Укажите номер договора']), 0, ['class' => 'form-control select2-ws', 'id'=>'installment_algorithms_id_[:KEY:]']) }}--}}
{{--                    </div>--}}

{{--                    <div id='payment_algo_[:KEY:]' class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">--}}
{{--                    </div>--}}

                    <input type="hidden" name="contract[[:KEY:]][installment_algorithms_id]" value="">
                    {{ Form::hidden('contract[[:KEY:]][payment][0][type_id]', 0) }}
                    <input type="hidden" name="contract[[:KEY:]][payment][0][order_source]" id="order_source_[:KEY:]" />

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                                <label class="control-label">
                                    Неофиц. % <span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][payment][0][informal_discount]', '', ['class' => 'form-control sum valid_fast_accept percents_input_validation', 'id'=>'informal_discount_[:KEY:]']) }}
                                {{ Form::hidden('contract[[:KEY:]][payment][0][official_discount]', 0, ['id' => 'official_discount_[:KEY:]']) }}
                                {{ Form::hidden('contract[[:KEY:]][payment][0][bank_kv]', 0, ['id' => 'bank_kv_[:KEY:]']) }}
                            </div>

                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label class="control-label pull-left" style="margin-top: 5px;" id="check_label_[:KEY:]">
                            Вид оплаты
                        </label>
                        {{ Form::select('contract[[:KEY:]][payment][0][pay_method_id]', \App\Models\Finance\PayMethod::query()->where('is_actual', 1)->where('show_in_primary', 1)->pluck('title', 'id'), 0, ['class' => 'form-control valid_fast_accept', 'id'=>'check_[:KEY:]', 'data-key' => '[:KEY:]']) }}
                        <script>
                            var pay_methods_id2types = {
                                @foreach(\App\Models\Finance\PayMethod::query()->where('show_in_primary', 1)->where('is_actual', 1)->get() as $method)
                                {!! "$method->id : $method->key_type," !!}
                                @endforeach
                            };
                        </script>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="receipt_block_[:KEY:]">
                        <label class="control-label pull-left" style="margin-top: 5px;" id="bso_receipt_label_[:KEY:]">
                            Квитанция
                            @if(auth()->user()->hasPermission('contracts', 'show_e_policy'))
                                <a href="javascript:void(0);" class="btn-xs btn-primary" data-create_receipt="[:KEY:]" style="display: inline">Е-квит</a>
                            @endif
                        </label>
                        {{ Form::text('contract[[:KEY:]][payment][0][bso_receipt]', '', ['class' => 'form-control valid_fast_accept temp_contract_receipt', 'id'=>'bso_receipt_[:KEY:]', 'data-key' => '[:KEY:]']) }}
                        <input type="hidden" name="contract[[:KEY:]][payment][0][bso_receipt_id]" id="bso_receipt_id_[:KEY:]" />
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="check_block_[:KEY:]" style="display: none">
                        <div class="row form-horizontal">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label class="control-label">Телефон</label>
                                {{ Form::text('contract[[:KEY:]][check_phone]', '', ['class' => 'form-control phone', 'id'=>'check_phone_[:KEY:]',]) }}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label class="control-label">E-mail</label>
                                {{ Form::text('contract[[:KEY:]][check_email]', '', ['class' => 'form-control email', 'id'=>'check_email_[:KEY:]']) }}
                            </div>
                        </div>

                        <div class="row form-horizontal">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <label class="control-label">Отправить</label>
                                {{ Form::select('contract[[:KEY:]][when_to_send]', collect(\App\Models\Api\Atol\AtolCheck::WHEN_TO_SEND), 1, ['class' => 'form-control', 'id'=>'when_to_send_[:KEY:]',]) }}
                            </div>
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none">
                        <label class="control-label pull-left" style="margin-top: 5px;">
                            Финансовая политика
                        </label>
                        <label class="control-label pull-right">Указать в ручную
                            <input type="checkbox" value="1" name="contract[[:KEY:]][financial_policy_manually_set]" onchange="viewSetFinancialPolicyManually(this, '[:KEY:]')"/> </label>
                        {{ Form::select('contract[[:KEY:]][financial_policy_id]', collect([0=>'Укажите номер договора']), 0, ['class' => 'form-control select2-ws', 'id'=>'financial_policy_id_[:KEY:]']) }}


                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" ></div>
                        <div class="row form-horizontal" id="financial_policy_manually_[:KEY:]" style="display: none;">

                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                {{ Form::text('contract[[:KEY:]][financial_policy_kv_bordereau]', '', ['class' => 'form-control sum', 'id'=>'financial_policy_kv_bordereau_[:KEY:]', 'placeholder'=>'Бордеро']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                {{ Form::text('contract[[:KEY:]][financial_policy_kv_dvoy]', '', ['class' => 'form-control sum', 'id'=>'financial_policy_kv_dvoy_[:KEY:]', 'placeholder'=>'ДВОУ']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                {{ Form::text('contract[[:KEY:]][financial_policy_kv_agent]', '', ['class' => 'form-control sum', 'id'=>'financial_policy_kv_agent_[:KEY:]', 'placeholder'=>'Агента']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3" >
                                {{ Form::text('contract[[:KEY:]][financial_policy_kv_parent]', '', ['class' => 'form-control sum', 'id'=>'financial_policy_kv_parent_[:KEY:]', 'placeholder'=>'Руков.']) }}
                            </div>
                        </div>

                    </div>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-4" >
                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label class="control-label">Комментарий</label>
                        {{ Form::text('contract[[:KEY:]][comment]', '', ['class' => 'form-control', 'id'=>'comment_[:KEY:]']) }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-4" >
                    <h3>Cтрахователь</h3>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" >
                                <label class="control-label">Тип</label>
                                {{ Form::select('contract[[:KEY:]][insurer][type]', collect([0=>"ФЛ", 1=>'ЮЛ']), 0, ['class' => 'form-control select2-ws', 'id'=>'insurer_type_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                            <div class="insurer_fl col-xs-12 col-sm-12 col-md-9 col-lg-9" >
                                <label class="control-label">
                                    ФИО <span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][insurer][fio]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_fio_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                            <div class="insurer_ul col-xs-12 col-sm-12 col-md-9 col-lg-9" style="display: none">
                                <label class="control-label">Название</label>
                                {{ Form::text('contract[[:KEY:]][insurer][title]', '', ['class' => 'form-control', 'id'=>'insurer_title_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                        </div>
                    </div>

                    <div class="row insurer_fl col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <label class="control-label">Паспорт</label>
                        <div class="row form-horizontal">
                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" >
                                {{ Form::text('contract[[:KEY:]][insurer][doc_serie]', '', ['class' => 'form-control', 'placeholder'=>'Серия']) }}
                            </div>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" >
                                {{ Form::text('contract[[:KEY:]][insurer][doc_number]', '', ['class' => 'form-control', 'placeholder'=>'Номер']) }}
                            </div>
                        </div>
                    </div>

                    <div class="row insurer_ul col-xs-12 col-sm-12 col-md-12 col-lg-12"  style="display: none">
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label class="control-label">ИНН</label>
                                {{ Form::text('contract[[:KEY:]][insurer][inn]', '', ['class' => 'form-control', 'id'=>'insurer_inn_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label class="control-label">КПП</label>
                                {{ Form::text('contract[[:KEY:]][insurer][kpp]', '', ['class' => 'form-control', 'id'=>'insurer_kpp_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                        </div>
                    </div>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row form-horizontal">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label class="control-label">Телефон <span style="color:red">*</span></label>
                                {{ Form::text('contract[[:KEY:]][insurer][phone]', '', ['class' => 'form-control phone valid_fast_accept', 'id'=>'insurer_phone_[:KEY:]']) }}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label class="control-label">Доп. телефон</label>
                                {{ Form::text('contract[[:KEY:]][insurer][add_phone]', '', ['class' => 'form-control phone', 'id'=>'insurer_add_phone_[:KEY:]']) }}
                            </div>
                        </div>

                        <div class="row form-horizontal">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label class="control-label">Email</label>
                                {{ Form::text('contract[[:KEY:]][insurer][email]', '', ['class' => 'form-control', 'id'=>'insurer_email_[:KEY:]']) }}
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label class="control-label">Метро</label>
                                {{ Form::text('contract[[:KEY:]][insurer][underground]', '', ['class' => 'form-control', 'id'=>'insurer_underground_[:KEY:]']) }}
                            </div>
                        </div>
                        <div class="row form-horizontal">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <label class="control-label">Адрес доставки</label>
                                {{ Form::text('contract[[:KEY:]][insurer][delivery_address]', '', ['class' => 'form-control', 'id'=>'delivery_address_[:KEY:]']) }}
                            </div>
                        </div>
                    </div>



                    <h3>Объект страхования</h3>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="contract_object_insurer_[:KEY:]">

                        <center><strong>Укажите номер договора</strong></center>

                    </div>


                </div>

            </div>
        </div>
    </div>
</div>