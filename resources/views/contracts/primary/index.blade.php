@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h2>
            <span class="btn-left">
                Первичные договора
            </span>
        </h2>
    </div>

    {{ Form::open(['url' => url('/contracts/primary/'), 'method' => 'post',  'class' => 'form-horizontal', 'id' => 'formContract']) }}

    <div class="form-group">
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="control">
        </div>
    </div>

    {{Form::close()}}

    <div class="form-group">
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <span class="btn btn-success btn-right" id="controlSaveButtonBox" onclick="saveFastAccept()">
                Сохранить
            </span>
            <span class="btn btn-primary btn-left" onclick="addContractCongestion()">
                Добавить
            </span>
        </div>
    </div>

@endsection

@section('js')

    <script>

        function setAlgos(key){

            var element = $( "#installment_algorithms_id_"+ key );

            if(element.val() > 0){

                var algorithm_id = element.val();

                if (getInputsInstallmentAlgorithms("payment_algo_" + key, algorithm_id, key, 0)){

                    $.ajax({
                        type: "POST",
                        url: "{{url("/settings/installment_algorithms_list/get_quantity")}}",
                        async: true,
                        data: {'algorithm_id' : algorithm_id},
                        success: function (response) {

                            var str = $('[name="contract['+key+'][payment_total]"]').val().replace(/\s/g, '');
                            var premium = parseInt(str);

                            if(parseInt($('[name="contract['+key+'][payment_total]"]').val()) > 0){

                                var payment_sum = premium / response;
                                $('[name="contract['+key+'][payment][0][payment_total]"]').val(payment_sum);

                                var indexes = response + 1;

                                for (var i = 1; i < indexes; i++) {
                                    $('#algo_payment_sum_'+ i).val(payment_sum);
                                }
                            };
                        }
                    });
                }

            }else{
                $("#payment_algo_" + key).html('');
            }
        }

        $(function () {
            @if($autosave)
                getAutoSavedData();
            @else
                addContractCongestion();
                activSearchOrdersToFront("order_title", "_0");
            @endif


            $(document).on('click', '[data-create_receipt]', function () {
                var st = $(document).scrollTop();
                var key = $(this).data('create_receipt');
                var bso_id = $('#bso_id_' + key).val();
                if (bso_id === '') {
                    flashHeaderMessage('Необходимо выбрать договор', 'danger')
                } else {
                    openFancyBoxFrame('/contracts/fast_accept/create_receipt_frame?bso_id=' + bso_id + '&key=' + key)
                }
                setTimeout(function () {
                    $(document).scrollTop(st)
                }, 0);

            });

            $(document).on('change', '[name*="[is_personal_sales]"]', function () {
                var key = $(this).data('key');
                if ($(this).prop('checked')) {
                    //$('#manager_id_' + key).val(0);
                    $('#manager_id_' + key).removeClass('valid_fast_accept').change()

                } else {
                    $('#manager_id_' + key).addClass('valid_fast_accept').change()

                }
            });

            $(document).on('change', '[name*="[pay_method_id]"]', function () {

                var key = $(this).data('key');
                var method_blocks = {
                    0: 'receipt_block_' + key,
                    1: 'check_block_' + key,
                    2: 'none'
                };

                var method = parseInt($(this).val());

                $.each(method_blocks, function (k, v) {
                    $("#" + v).hide()
                });

                $("#" + method_blocks[pay_methods_id2types[method]]).show();

                if (method === 1) {
                    $('[name="contract[' + key + '][payment][0][bso_receipt]"]').addClass('valid_fast_accept');
                } else {
                    $('[name="contract[' + key + '][payment][0][bso_receipt]"]').removeClass('valid_fast_accept');
                }

            });


        });

        function autoSave() {
            if (!saveFastAcceptInProgress){
                $.ajax({
                    url: '{{ url('contracts/primary/autosave') }}',
                    type: 'POST',
                    data: {
                        data: $('#formContract').serialize(),
                        type_id: 1,
                        count: $('#formContract .temp_contract').length,
                    },
                });
            }
        }

        setInterval(autoSave, 5000);

        var MY_COUNT_TEMP = 0;

        String.prototype.replaceAll = function (search, replace) {
            return this.split(search).join(replace);
        }

        function addContractCongestion() {

            myHtml = myGetAjax("{{url("/contracts/primary/form_fast_acceptance/")}}");

            myHtml = myHtml.replaceAll('[:KEY:]', MY_COUNT_TEMP);
            myHtml = myHtml.replaceAll('[:NUM:]', (MY_COUNT_TEMP + 1));

            $('#control').append(myHtml);

            startContractFunctions(MY_COUNT_TEMP);

            activSearchOrdersToFront("order_title", "_"+MY_COUNT_TEMP);

            MY_COUNT_TEMP = MY_COUNT_TEMP + 1;


            updateContractIndexes();

            controlSaveButton();

            toggleFlowOptions(MY_COUNT_TEMP - 1);

            $('.select2-ws').select2("destroy").select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true,
                minimumResultsForSearch: -1,
            });

        }


        function getAutoSavedData() {

            flashMessage('success', 'Загружены несохранённые данные');

            myHtml = myGetAjax("{{url("/contracts/primary/get_autosave/")}}");

            $('#control').append(myHtml);

            @if($autosave)
                @for($i=0; $i < $autosave->count; $i++)

                    startContractFunctions({{$i}}, true);
                    activSearchOrdersToFront("order_title", "_{{$i}}");
                    toggleFlowOptions({{$i}});

                @endfor
            @endif

            updateContractIndexes();
            controlSaveButton();

            $('.select2-ws').select2("destroy").select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true,
                minimumResultsForSearch: -1,
            });

            MY_COUNT_TEMP = {{ $autosave ? $autosave->count : 0 }};

        }


        function toggleFlowOptions(key) {

            // $.each($('[name*="[payment_type]"]'), function (k,v) {

            // var key = $(v).data('key');


            // if(parseInt($(this).val()) === 0){
            if (parseInt($('[name="contract[' + key + '][payment][0][payment_type]"]').val()) === 0) {

                $('[name="contract[' + key + '][payment][0][bso_not_receipt]"]').prop('checked', true).change();

                $('[name*="[payment_flow]"][data-key="' + key + '"]').html(
                    '<option value="0" selected="selected">Брокер</option>'
                );

                $('[name="contract['+key+'][payment][0][payment_flow]"]').siblings('.select2-container').find('.select2-chosen').text('Брокер');

            }else if (parseInt($('[name="contract['+key+'][payment][0][payment_type]"]').val()) === 3){

                $('[name="contract['+key+'][payment][0][bso_not_receipt]"]').prop('checked', true).change();


                $('[name*="[payment_flow]"][data-key="'+key+'"]').html(
                    '<option value="1" selected="selected">СК</option>'
                );

                $('[name="contract['+key+'][payment][0][payment_flow]"]').siblings('.select2-container').find('.select2-chosen').text('СК');

            } else {
                $('[name="contract[' + key + '][payment][0][bso_not_receipt]"]').prop('checked', false).change();

                $('[name*="[payment_flow]"][data-key="' + key + '"]').html(
                    '<option value="0" selected="selected">Брокер</option><option value="1">СК</option>'
                )
            }

            // })

        }

        function deleteContract(id) {

            if(confirm('Удалить данные оформления?')){
                $('#contract_' + parseInt(id)).remove();
                updateContractIndexes();
                controlSaveButton();
            }
        }

        function getContract() {
            return $('.temp_contract');
        }


        function updateContractIndexes() {
            var contracts = getContract();
            contracts.each(function (i, contract) {
                $(contract).find('.number').html(i + 1);
            });
        }


        function startContractFunctions(KEY, forAutosaved = false) {

            var bso_used = [];
            $.each($('[name*="][bso_title]"]'), function (k, v) {
                bso_used.push($(v).val());
            });

            if (!forAutosaved) {
                activSearchBso("bso_title", '_' + KEY, 1, bso_used);
            }

            activSearchBso("bso_receipt", '_' + KEY, 2, bso_used);


            $('#manager_id_' + KEY).select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true,
                minimumInputLength: 3
            });


            $('#insurer_type_' + KEY).change(function () {

                key = $(this).data('key');
                dataForm = $("#contract_" + key);

                type = $(this).val();

                $("#insurer_fio_" + key).removeClass('valid_fast_accept');
                $("#insurer_title_" + key).removeClass('valid_fast_accept');


                if (parseInt(type) == 0) {
                    $("#insurer_fio_" + key).addClass('valid_fast_accept');
                    $(dataForm).find('.insurer_fl').show();
                    $(dataForm).find('.insurer_ul').hide();

                } else {
                    $("#insurer_title_" + key).addClass('valid_fast_accept');
                    $(dataForm).find('.insurer_fl').hide();
                    $(dataForm).find('.insurer_ul').show();

                }

            });


            $('#insurer_fio_' + KEY).suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "NAME",
                count: 5,
                onSelect: function (suggestion) {
                    key = $(this).data('key');
                    $('#insurer_title_' + key).val($(this).val());

                }
            });

            $('#insurer_title_' + KEY + ', #insurer_inn_' + KEY + ', #insurer_kpp_' + KEY).suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "PARTY",
                count: 5,
                onSelect: function (suggestion) {
                    var data = suggestion.data;

                    key = $(this).data('key');


                    $('#insurer_title_' + key).val(suggestion.value);
                    $('#insurer_inn_' + key).val(data.inn);
                    $('#insurer_kpp_' + key).val(data.kpp);

                    $('#insurer_fio_' + key).val($('#insurer_title_' + key).val());

                }
            });


            $('.sum')
                .change(function () {
                    $(this).val(CommaFormatted($(this).val()));
                })
                .blur(function () {
                    $(this).val(CommaFormatted($(this).val()));
                })
                .keyup(function () {
                    $(this).val(CommaFormatted($(this).val()));
                });

            $('.integer').change(function () {
                $(this).val($(this).val().replace(/\D+/g, ""))
            }).keyup(function () {
                $(this).val($(this).val().replace(/\D+/g, ""))
            });

            $('.phone').mask('+7 (999) 999-99-99');

        }


        function selectBso(object_id, key, type, suggestion) {

            var data = suggestion.data;

            if (parseInt(type) == 1) { // БСО

                setNotNullValue('#bso_id' + key, data.bso_id);
                setNotNullValue('#bso_supplier_id' + key, data.bso_supplier_id);
                setNotNullValue('#insurance_companies_id' + key, data.insurance_companies_id);
                setNotNullValue('#product_id' + key, data.product_id);
                setNotNullValue('#agent_id' + key, data.agent_id);

                getOptionInstallmentAlgorithms("installment_algorithms_id" + key, data.insurance_companies_id, 0);

                financial_policy_id = 0;


                if (data.contract){

                    var contract = data.contract;

                    setNotNullValue('#sign_date' + key, contract.sign_date);
                    setNotNullValue('#begin_date' + key, contract.begin_date);
                    setNotNullValue('#end_date' + key, contract.end_date);

                    setNotNullValue('#payment_total' + key, contract.payment_total);

                    setNotNullValue('#payment_number' + key, contract.payment_number);

                    setNotNullValue('#manager_id' + key, contract.manager_id).change();

                    financial_policy_id = contract.financial_policy_id;


                    setNotNullValue('#informal_discount' + key, contract.informal_discount);
                    setNotNullValue('#official_discount' + key, contract.official_discount);
                    setNotNullValue('#bank_kv' + key, contract.bank_kv);

                    setNotNullValue('#insurer_type' + key, contract.insurer_type).change();
                    setNotNullValue('#insurer_title' + key, contract.insurer_title);
                    setNotNullValue('#insurer_fio' + key, contract.insurer_title);

                    setNotNullValue('#object_insurer_ts_category' + key, contract.object_insurer_ts_category).change();
                    setNotNullValue('#object_insurer_ts_power' + key, contract.object_insurer_ts_power);
                    setNotNullValue('#object_insurer_ts_reg_number' + key, contract.object_insurer_ts_reg_number);
                    setNotNullValue('#object_insurer_ts_mark_id' + key, contract.object_insurer_ts_mark_id).change();
                    setNotNullValue('#object_insurer_ts_model_id' + key, contract.object_insurer_ts_model_id).change();
                    setNotNullValue('#object_insurer_ts_vin' + key, contract.object_insurer_ts_vin);
                }



                getOptionFinancialPolicy("financial_policy_id" + key, data.insurance_companies_id, data.bso_supplier_id, data.product_id, financial_policy_id);



                getKvAccesses(key, data.product_id);

                object_insurer = myGetAjax('{{url("/bso/actions/get_html_mini_contract_object_insurer/")}}?product_id=' + data.product_id + '&key=' + key);

                //object_insurer = object_insurer.replaceAll('[:KEY:]', parseInt(intkey));

                $("#contract_object_insurer" + key).html(object_insurer);

                String.prototype.replaceAll = function (search, replace) {
                    return this.split(search).join(replace);
                };
                var intkey = key;
                intkey = new String(intkey).replaceAll('_', '');
                startContractObjectInsurerFunctions(parseInt(intkey));

                var receipt_used = [];
                $.each($('[name*="][bso_receipt]"]'), function (k, v) {
                    receipt_used.push($(v).val());
                });

                activSearchBso("bso_receipt", key, 2, receipt_used);

                activSearchOrdersToFront("order_title", key);

            }

            if (parseInt(type) == 2) { // Квитанция
                $('#bso_receipt_id' + key).val(data.bso_id);

            }


        }

        function checkOrderTitle(key, event) {
            var KeyID = event.keyCode;

            switch(KeyID)
            {
                case 8: // backspace
                    $('#order_id_'+key).val('');
                    break;
                case 46: // delete
                    $('#order_id_'+key).val('');
                    break;
                default:
                    break;
            }
        }

        function setPaymentQty(key) {
            $('input[name="contract['+key+'][payment][0][payment_total]"]').val(CommaFormatted($('input[name="contract['+key+'][payment_total]"]').val()));
        }

        function setPaymentQty_removeBorder(key){
            removeBorderColor('input[name="contract['+key+'][payment][0][payment_total]"]');
        }

        function controlSaveButton() {

            $('.valid_fast_accept').change(function () {

                $(this).css("border-color", "");

                if ($(this)[0].localName === 'select') {
                    var container = $(this).prev('.select2-container');
                    container.css({'border': 'none'});
                }
            });

            if (getContract().length > 0) {
                $("#controlSaveButtonBox").show();
            } else {
                $("#controlSaveButtonBox").hide();
            }
        }


        var saveFastAcceptInProgress = false;

        function saveFastAccept() {
            if (validFastAccept() == true && !saveFastAcceptInProgress) {
                saveFastAcceptInProgress = true;
                loaderShow();
                $("#formContract").submit();
            }
        }

        function validFastAccept() {

            var first_err;
            var msg_arr = [];
            var i = 0;

            $('.valid_fast_accept').each(function () {

                var tag = $(this)[0].localName;

                var val = '';
                if (['select', 'input'].indexOf(tag) !== -1) {
                    val = $(this).val();
                } else {
                    //val = $(this).html();
                    return;
                }


                if (tag === 'select' && parseInt(val) === 0) {

                    if(i == 0 && $(this).parent('div').css('display') != 'none'){
                        msg_arr.push('Заполните все поля');
                        first_err = $(this);
                        i++;
                    }
                    var container = $(this).prev('.select2-container');
                    container.css({
                        'height': '36px',
                        'border': '1px red solid'
                    });
                }

                if (new String(val).length < 1 && $(this).parent('div').css('display') != 'none') {
                    if(i == 0 && $(this).parent('div').css('display') != 'none'){
                        first_err = $(this);
                        i++;
                    }
                    msg_arr.push('Заполните все поля');

                    var elem = $(this);

                    elem.css("border-color", "red");


                }

            });

            $('.temp_contract').each(function (index, value) { // проверка на существование бсо

                var bso_key = $(this).data('key');

                var bso = $('input[name="contract['+bso_key+'][bso_title]"]').val();
                var bso_id = $('input[name="contract['+bso_key+'][bso_id]"]').val();

                $.ajax({
                    type: "POST",
                    url: "{{url('/contracts/fast_accept/check_correct_bso')}}",
                    async: false,
                    data: {'bso':bso,bso_id:bso_id},
                    success: function (res) {

                        if (res == 0){
                            first_err = $('input[name="contract['+bso_key+'][bso_title]"]');
                            i++;
                            msg_arr.push('Заполните все поля');

                            var elem = $('input[name="contract['+bso_key+'][bso_title]"]');
                            elem.css("border-color", "red");
                        }
                    }
                });
            });

            $('.orders-title').each(function () {

                if ($(this).val().length > 0){
                    var otKey = $(this).data('key');

                    if ($('#order_id_'+otKey).val() == ''){
                        first_err = $(this);
                        i++;
                        msg_arr.push('Заполните все поля');

                        var elem = $(this);
                        elem.css("border-color", "red");
                    }
                }

            });

            // проверка на существование квитанции
            $('.temp_contract_receipt').each(function (index, value) {
                var paymentKey = $(this).data('key');
                var bso = $('input[name="contract['+paymentKey+'][payment][0][bso_receipt]"]').val();

                $.ajax({
                    type: "POST",
                    url: "{{url('/contracts/fast_accept/check_correct_bso')}}",
                    async: false,
                    data: {'bso':bso},
                    success: function (res) {

                        if (res == 0 && $('input[name="contract['+paymentKey+'][payment][0][bso_receipt]"]').parent('div').css('display') != 'none'){
                            first_err =  $('input[name="contract['+paymentKey+'][payment][0][bso_receipt]"]');
                            i++;
                            msg_arr.push('Заполните все поля');

                            var elem =  $('input[name="contract['+paymentKey+'][payment][0][bso_receipt]"]');
                            elem.css("border-color", "red");
                        }
                    }
                });
            });

            if (msg_arr.length) {
                if ($(first_err).offset() !== undefined) {
                    $('html, body').animate({scrollTop: $(first_err).offset().top - 300}, 800);
                }

                return false;
            }


            return true;
        }

        function get_end_dates(start_date) {
            var cur_date_tmp = start_date.split(".");
            var cur_date = new Date(cur_date_tmp[2], cur_date_tmp[1] - 1, cur_date_tmp[0]);
            var new_date = new Date(cur_date.setYear(cur_date.getFullYear() + 1));
            var new_date2 = new Date(new_date.setDate(new_date.getDate() - 1));
            return getFormattedDate(new_date2);
        }

        function removeBorderColor(el){
            $(el).css('borderColor', '#e0e0e0');
        }

        function setAllDates(key) {
            sign_date = $("#sign_date_" + key).val();

            $("#begin_date_" + key).val(sign_date);
            $("#payment_data_" + key).val(sign_date);

            removeBorderColor("#begin_date_" + key);
            removeBorderColor("#payment_data_" + key);

            return setEndDates(key);
        }

        function setEndDates(key) {

            removeBorderColor("#end_date_" + key);
            begin_date = $("#begin_date_" + key).val();
            $("#end_date_" + key).val(get_end_dates(begin_date));

        }


        function viewSetFinancialPolicyManually(obj, key) {
            if ($(obj).is(':checked')) {
                $("#financial_policy_manually_" + key).show();
                $("#financial_policy_id_" + key).hide();
            } else {
                $("#financial_policy_manually_" + key).hide();
                $("#financial_policy_id_" + key).show();
            }
        }

        function viewSetBsoNotReceipt(obj, key) {
            if ($(obj).is(':checked')) {
                $("#bso_receipt_" + key).attr('disabled', 'disabled');
                $("#bso_receipt_" + key).removeClass('valid_fast_accept');

                $('[name="contract[' + key + '][payment][0][bso_receipt]"]').val('');
                $('[name="contract[' + key + '][payment][0][bso_receipt_id]"]').val('');

            } else {
                $("#bso_receipt_" + key).removeAttr('disabled');
                $("#bso_receipt_" + key).addClass('valid_fast_accept');
            }

            $("#bso_receipt_" + key).css("border-color", "");
        }


        function startContractObjectInsurerFunctions(KEY) {
            $('#object_insurer_ts_mark_id_' + KEY).select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true
            });
            $('#object_insurer_ts_model_id_' + KEY).select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true
            });


        }

        function getModelsObjectInsurer(KEY, select_model_id) {


            $.getJSON(
                '{{url("/contracts/actions/get_models")}}',
                {
                    categoryId: $('#object_insurer_ts_category_' + KEY).val(),
                    markId: $('#object_insurer_ts_mark_id_' + KEY).select2('val')
                },
                function (response) {

                    var options = "<option value='0'>Не выбрано</option>";

                    response.map(function (item) {
                        options += "<option value='" + item.id + "'>" + item.title + "</option>";
                    });

                    $('#object_insurer_ts_model_id_' + KEY).html(options).select2('val', select_model_id);

                });
        }




        function secondPaymentToggle(key, reinit = true) {

            if ($('[name="contract['+key+'][second_payment]"]').prop('checked')){
                if (reinit){
                    activSearchBsoSold("bso_title", "_"+key, 1, [], 2, true);

                    $('#bso_id_'+key).val('');
                    $('#bso_title_'+key).val('');
                }

                $.get('/bso/actions/get_payments_accepts', {'order_id': $('#order_id_'+key).val()}, function(res){
                    if (res !== ''){$('#payment_number_'+key).val(res);}
                });

                $('.button-e-policy-'+key).hide();
                $('.second-payment-block-'+key).show();
                $('.bso-title-block-'+key)
                    .addClass('row')
                    .addClass('col-md-8')
                    .addClass('col-lg-8');




            } else {
                if (reinit){
                    activSearchBso("bso_title", "_"+key, 1, null);

                    $('#bso_id_'+key).val('');
                    $('#bso_title_'+key).val('');
                }

                $('#payment_number_'+key).val('1');

                $('.button-e-policy-'+key).show();
                $('.second-payment-block-'+key).hide();
                $('.bso-title-block-'+key)
                    .removeClass('row')
                    .removeClass('col-md-8')
                    .removeClass('col-lg-8');
            }
        }



    </script>


@endsection