@extends('layouts.app')

@section('content')
<div class="page-heading">
    <h2 style="padding-bottom:10px;">Выгрузка по андеррайтерам</h2>
</div>

<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
            <div class="filter-group">
                <form id='filter'>
                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label">Андеррайтеры</label>
                        {{ Form::select('underwriter_id', $agents->pluck('name', 'accept_user_id')->prepend('Все', '0'), request('underwriter_id', 0), ['class' => 'form-control select2', 'id'=>'underwriter_id','onchange' => 'loadItems()']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label">Продукт</label>
                        {{ Form::select('product_id', \App\Models\Directories\Products::where('is_actual', '=','1')->get()->pluck('title','id')->prepend('Все', '0'), request('product_id') ?? '', ['class' => 'form-control select2', 'id' =>'product_id', 'onchange' => 'loadItems()']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label" for="from">C</label>
                        {{ Form::text('from', request('from', date('d.m.Y', time()-60*60*24*30)), ['class' => 'form-control datepicker date', 'id' =>'from', 'onchange' => 'loadItems()']) }}
                    </div>
                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label" for="to">По</label>
                        {{ Form::text('to', request('to', date('d.m.Y')), ['class' => 'form-control datepicker date',  'id' =>'to', 'onchange' => 'loadItems()']) }}
                    </div>

                </form>

            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
            <span class="btn btn-success pull-right doc_export_btn" id="export">Выгрузка в .xls</span>
        </div>

    </div>
</div>

<div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container" style="margin-top: -5px;overflow: auto;">
     {{--@include('contracts.partials.contracts_table', $contracts)--}}

</div>

@include('_chunks/_pagination',['callback'=>'loadItems'])

@endsection

@section('js')

<script>

     $(function () {
        loadItems();
    });

    function loadItems() {

        var data = {
            page_count: $("#page_count").val(),
            PAGE: PAGE,
            product_id: $('[name = "product_id"]').val(),
            underwriter_id: $('[name = "underwriter_id"]').val(),
            from: $('[name = "from"]').val(),
            to: $('[name = "to"]').val()
        };

        $('#page_list').html('');
        $('#table_row').html('');
        $('#view_row').html(0);
        $('#max_row').html(0);

        loaderShow();

        $.post("/contracts/contracts_techunder/get_table", data, function (response) {

            $('#main_container').html(response.html);
            $('#view_row').html(response.view_row);
            $('#max_row').html(response.max_row);

            ajaxPaginationUpdate(response.page_max,loadItems);

            loaderHide();

        }).always(function() {
            loaderHide();
        });

    }

     $(document).on('click', '#export', function () {

        var request = JSON.parse('{"underwriter_id":"' + $('#underwriter_id').val() + '", "product_id":"' + $('#product_id').val() + '", "from":"' + $('#from').val() + '", "to":"' + $('#to').val() + '"}');

        //var request = $('form#filter').serializeArray();

        var data = {pageCount: -1, PAGE: 1, };
        $.each(request, function (key, val) {
            data[key] = val;
        });
        var query = $.param({method: 'Contracts\\Techunder\\TechunderController@to_excel', param: data});
        location.href = '/exports/table2excel?' + query;
    });

</script>
<script src="/js/jquery.easyui.min.js"></script>

@endsection