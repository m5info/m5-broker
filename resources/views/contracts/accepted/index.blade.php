@extends('layouts.app')

@section('content')


    <div class="page-heading">
        <h1 class="inline-h1">Акцептованные договора</h1>
        <a class="btn btn-success btn-right" id="export_accepts">Выгрузить в .xls</a>
    </div>
    <div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="filter-group" id="filters">

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label">Номер БСО</label>
                        {{ Form::text('bso_title', '', ['class'=>'form-control', 'onkeyup' => 'loadItems(true)']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label">Организация</label>
                        {{ Form::select('org_id', \App\Models\Organizations\Organization::all()->pluck('title', 'id')->prepend('Все', 0), 0, ['class'=>'form-control select2-ws', 'onchange' => 'loadItems()']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label">СК</label>
                        {{ Form::select('sk', \App\Models\Directories\InsuranceCompanies::all()->pluck('title', 'id')->prepend('Все', -1), -1, ['class'=>'form-control select2-all', 'onchange' => 'loadItems()']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label">Поставщик</label>
                        {{ Form::select('bso_supplier_id', \App\Models\Directories\BsoSuppliers::all()->pluck('title', 'id')->prepend('Все', 0), 0, ['class'=>'form-control select2-ws', 'onchange' => 'loadItems()']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label">Тип</label>
                        {{ Form::select('type_id', collect([ 0 => 'Условный', 1 => 'Безусловный',2 => 'По служебке',])->prepend('Все', -1), 0, ['class'=>'form-control select2-all', 'onchange' => 'loadItems()']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label">Менеджер</label>
                        {{ Form::select('manager_id', \App\Models\User::all()->pluck('name', 'id')->prepend('Все', -1), -1, ['class'=>'form-control select2-all', 'onchange' => 'loadItems()']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label">Агент</label>
                        {{ Form::select('agent_id', \App\Models\User::all()->pluck('name', 'id')->prepend('Все', -1), -1, ['class'=>'form-control select2-all', 'onchange' => 'loadItems()']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label">Дата С</label>
                        {{ Form::text('date_from', '', ['class'=>'form-control datepicker date', 'onchange' => 'loadItems()']) }}
                    </div>

                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label">Дата ПО</label>
                        {{ Form::text('date_to', '', ['class'=>'form-control datepicker date', 'onchange' => 'loadItems()']) }}
                    </div>
                    <div class="btn-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="control-label">Подразделение</label>
                        {{ Form::select('department_ids[]', \App\Models\Settings\Department::all()->pluck('title', 'id'), 0, ['class' => 'form-control select2-all', 'multiple' => true, 'onchange' => 'loadItems()']) }}
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div id="table"></div>
        </div>
    </div>

    @include('_chunks/_pagination',['callback'=>'loadItems'])

@endsection

@section('js')

    <script>

        $(function () {

            loadItems();

            $(document).on('click', '#export_accepts', function () {
                var data = getData();
                var query = $.param({ method: 'Analitics\\Accepted\\AcceptedController@get_table', param: data});
                location.href = '/exports/table2excel?'+query;
            });
        });


        function getData(){
            return {
                bso_supplier_id:$('[name="bso_supplier_id"]').val(),
                manager_id:$('[name="manager_id"]').val(),
                agent_id:$('[name="agent_id"]').val(),
                department_ids:$('[name="department_ids[]"]').val(),
                bso_title:$('[name="bso_title"]').val(),
                date_from:$('[name="date_from"]').val(),
                date_to:$('[name="date_to"]').val(),
                type_id:$('[name="type_id"]').val(),
                org_id:$('[name="org_id"]').val(),
                sk:$('[name="sk"]').val(),

                page_count: $('[name="page_count"]').val(),
                PAGE: PAGE,
            }
        }


        function loadItems(){

            $('#page_list').html('');
            $('#table_row').html('');
            $('#view_row').html(0);
            $('#max_row').html(0);

            loaderShow();

            $.post('/analitics/accepted/get_table', getData(), function(table_res){

                $('#table').html(table_res.html);

                $('#view_row').html(table_res.view_row);
                $('#max_row').html(table_res.max_row);

                ajaxPaginationUpdate(table_res.page_max,loadItems);

            }).always(function(){
                loaderHide();
            });


            loaderHide();

        }

    </script>


@endsection