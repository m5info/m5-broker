<table class="table-bordered table">
    <thead>
    <tr>
        <th>Дата договора</th>
        <th>Дата акцепта</th>
        <th>Наименование БСО</th>
        <th>СК</th>
        <th>Агент</th>
        <th>Страхователь</th>
        <th>Подразделение</th>
        <th>Менеджер</th>
        <th>Андеррайтер</th>
        <th>Андеррайтер (редак.)</th>
        <th>Тип</th>
        <th>Условие</th>
    </tr>
    </thead>
    <tbody>

    @if(sizeof($accepts))
        @foreach($accepts as $accept)
            <tr class="{{$accept->contract->isOverdueConditionalAcceptance() && in_array($accept->contract->statys_id, [2, 7])  ? 'bg-red' : ''}}"
                @if ($accept->contract->isOverdueConditionalAcceptance() && in_array($accept->contract->statys_id, [2, 7]))
                    title="С момента условного акцепта прошло {{$accept->contract->daysFromDelay()}} дней" data-toggle="tooltip" data-placement="auto"
                @endif
            >
                <td>{{ $accept->contract ? getDateFormatRu($accept->contract->sign_date) : "" }}</td>
                <td>{{ $accept->contract ? getDateFormatRu($accept->contract->check_date) : "" }}</td>
                @php
                    $url = $accept->contract ? url("/bso/items/{$accept->contract->bso_id}") : "";
                @endphp
                <td>{!! $accept->contract ? "<a href=" .$url. ">" .$accept->contract->bso_title. "</a>" : "" !!}</td>
                <td>{{ $accept->contract && $accept->contract->insurance_companies ? $accept->contract->insurance_companies->title : ""}}</td>
                <td>{{ $accept->contract && $accept->contract->agent ? $accept->contract->agent->name : "" }}</td>
                <td>{{ $accept->contract && $accept->contract->insurer ? $accept->contract->insurer->title : "" }}</td>
                    @php
                        $user_department = ($accept->contract && $accept->contract->sales_condition == 0) ? 'agent' : 'manager';
                    @endphp
                <td>{{ $accept->contract ? $accept->contract->{$user_department}->department->title : "" }}</td>
                <td>{{ $accept->contract && $accept->contract->manager ? $accept->contract->manager->name : "" }}</td>
                <td>{{ $accept->accept_user ? $accept->accept_user->name : "" }}</td>
                <td>{{ $accept->contract && $accept->contract->check_user ? $accept->contract->check_user->name : "" }}</td>
                <td>{{ \App\Models\Contracts\Contracts::KIND_ACCEPTANCE[$accept->kind_acceptance] }}</td>
                <td>{{ $accept->contract && $accept->contract->errors->last() ? $accept->contract->errors->last()->message  : ""}}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="12" class="text-center">Нет записей</td>
        </tr>
    @endif


    </tbody>

</table>

<script>
    $(document).ready(function () {
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    })
</script>