@extends('layouts.app')
@section('content')
    <div class="page-heading">
        <h2>Акт передачи БСО в Андеррайтинг # {{$act->act_number}} от {{setDateTimeFormatRu($act->time_create)}}</h2>
    </div>
    <div class="divider"></div>

    <br/>
    <div class="row">

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

            <div class="form-horizontal">
                <div class="form-group">

                    <div class="col-sm-4">
                        <label class="col-sm-12 control-label">Тип акта</label>
                        <div class="col-sm-12">
                            {{$act->act_name}}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label class="col-sm-12 control-label">Создал</label>
                        <div class="col-sm-12">
                            {{ $act->user_from ? $act->user_from->name : ""}}
                        </div>
                    </div>

                    @if($act->under_accepted_id > 0)

                        <div class="col-sm-4">
                            <label class="col-sm-12 control-label">Принял</label>
                            <div class="col-sm-12">
                                {{ $act->under_accepted ? $act->under_accepted->name : ""}}
                            </div>
                        </div>

                    @endif

                    @if(auth()->user()->is('under') && $act->under_accepted_id == 0)
                        <div class="col-sm-4">
                            <a class="btn btn-success pull-right" href="/contracts/reception_contracts/details/{{$act->id}}/accept">
                                Принять
                            </a>
                        </div>
                    @endif
                </div>
            </div>


        </div>

        @if($act->under_accepted_id == 0)
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <span class="btn btn-danger pull-left" onclick="deleteAct()">Удалить акт</span>
            <hr>
            <div class="event_form" style="display:none;">
                @if($act->state_id == 0)

                    <span class="btn btn-danger pull-left" onclick="deleteItemsAct()">Исключить выбранные</span>
                @endif
            </div>
        </div>
        @endif
    </div>



    @include('contracts.reception_contracts.details.table', ['payments' => $payments])



@endsection

@section('js')

    <script>


        $(function () {


        });


        function show_checked_options() {
            show_actions();
        }


        function show_actions() {
            if ($('.contract_item_checkbox:checked').length > 0) {
                $('.event_form').show();
            } else {
                $('.event_form').hide();
            }

            $('.event_td').addClass('hidden');
            $('.event_' + $('#event_id').val()).removeClass('hidden');
            highlightSelected();
        }

        function highlightSelected() {
            $('.contract_item_checkbox').each(function(){
                $(this).closest('tr').toggleClass('green', $(this).is(':checked'));
            });
        }



        @if($act->state_id == 0)

            function deleteAct() {
                loaderShow();
                $.post("{{url("/contracts/reception_contracts/details/{$act->id}/delete_act")}}", {}, function (response) {
                    loaderHide();
                    window.location = '{{url("/contracts/reception_contracts/")}}';
                }).always(function() {
                    loaderHide();
                });

            }

            function deleteItemsAct() {
                var item_array = [];
                $('.contract_item_checkbox:checked').each(function () {
                    item_array.push($(this).val());
                });
                loaderShow();
                $.post("{{url("/contracts/reception_contracts/details/{$act->id}/delete_items")}}", {item_array: JSON.stringify(item_array)}, function (response) {
                    loaderHide();
                    reload();
                }).always(function() {
                    loaderHide();
                });


            }

        @endif

    </script>


@endsection