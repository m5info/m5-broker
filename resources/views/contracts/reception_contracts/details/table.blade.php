@if(sizeof($payments))

    <div class="form-horizontal">
        <table class="tov-table table table-bordered">
            <thead>
            <tr class="sort-row">
                <th>№ п\п</th>
                <th></th>
                <th>Агент</th>
                <th>СК</th>
                <th>Вид страхования</th>
                <th>Страхователь</th>
                <th>№ Бланка</th>
                <th>№ Квитанции</th>
                <th>Сумма</th>
                <th>Оф.скидка(%)</th>
                <th>Неоф.скидка(%)</th>
                <th class="nowrap">№ Счета</th>
                <th>Статус</th>
            </tr>
            </thead>
            <tbody>

            @foreach($payments as $key => $payment)


                <tr class="clickable-row">
                    <td>{{$key+1}}</td>
                    <td><input type='checkbox' value='{{$payment->id}}' class='contract_item_checkbox' onchange='show_checked_options()'></td>

                    <td>{{$payment->agent ?$payment->agent->name : ''}}</td>
                    <td>{{$payment->bso && $payment->bso->supplier ? $payment->bso->supplier->title : ''}}</td>
                    <td>{{$payment->bso && $payment->bso->type ? $payment->bso->type->title : ''}}</td>
                    <td>{{$payment->contract->insurer ? $payment->contract->insurer->title : ''}}</td>
                    <td>{{$payment->bso ? $payment->bso->bso_title : ''}}</td>
                    <td>{{$payment->bso_receipt}}</td>
                    <td class="nowrap">{{titleFloatFormat($payment->payment_total)}}</td>
                    <td>{{titleFloatFormat($payment->official_discount)}}</td>
                    <td>{{titleFloatFormat($payment->informal_discount)}}</td>
                    <td><a href="/finance/invoice/invoices/{{$payment->invoice_id}}/edit/">{{$payment->invoice_id}}</a></td>
                    <td>{{\App\Models\Contracts\Contracts::STATYS[$payment->contract->statys_id]}}</td>
                </tr>

            @endforeach


            </tbody>
        </table>
    </div>


@else

    <h1>Договоры отсутствуют</h1>

@endif

