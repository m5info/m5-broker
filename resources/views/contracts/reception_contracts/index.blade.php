@extends('layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="/css/themes/material-teal/easyui.css">
@append

@section('content')


    <div class="page-heading">
        <h2>Приём договоров из кассы</h2>
    </div>
    <div class="header_bab">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
            <div id="tt" class="easyui-tabs" data-options="tools:'#tab-tools'">

                <div title="Акты" id="tab-0" data-status="0"></div>
                <div title="Принятые" id="tab-1" data-status="1"></div>

            </div>
        </div>
    </div>


    <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="main_container" style="margin-top: -5px;overflow: auto;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="filter-group">
                    <div class="btn-group col-xs-12 col-sm-12 col-md-4 col-lg-3">
                        <label class="control-label" for="user_id_from">Создал</label>
                        {{ Form::select('user_from', \App\Models\Characters\Cashier::all()->pluck('name', 'id')->prepend('Все', 0), 0, ['class' => 'form-control select2-all', 'onchange'=>'loadContent()']) }}
                    </div>
                    <div class="btn-group col-xs-12 col-sm-12 col-md-4 col-lg-3">
                        <label class="control-label" for="user_id_from">Номер акта</label>
                        {{ Form::text('number', '', ['class' => 'form-control']) }}
                    </div>
                    <div class="btn-group col-xs-12 col-sm-12 col-md-4 col-lg-3">
                        <label class="control-label" for="user_id_from">Номер счета</label>
                        {{ Form::text('invoice_id', '', ['class' => 'form-control']) }}
                    </div>
                    <div style="margin-top: 15px" class="btn-group col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <span onclick="loadContent()" class="btn btn-success">Применить</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-horizontal block-inner" id="data_list"></div>

@endsection

@section('js')
    <script>

        var ACTIVE_TAB = 0;

        $(function(){
            loadContent();

            $('#tt').tabs({
                border:false,
                pill: false,
                plain: true,
                onSelect: function(title, index){
                    return selectTab(index);
                }
            });
            selectTab(ACTIVE_TAB);
        });

        function loadContent(){

            loaderShow();
            $.post("{{url("/contracts/reception_contracts/get_table/")}}", getData(), function (response){
                loaderHide();
                $("#data_list").html(response.html);

                $(".clickable-row").click( function(){
                    if ($(this).attr('data-href')) {
                        window.location = $(this).attr('data-href');
                    }
                });

            }).always(function() {
                loaderHide();
            });
        }

        function getData(){
            return {
                status: $('[id="tab-'+ACTIVE_TAB+'"]').attr('data-status'),
                user_from: $('[name="user_from"]').val(),
                number: $('[name="number"]').val(),
                invoice_id: $('[name="invoice_id"]').val()
            };
        }


        function selectTab(id) {
            ACTIVE_TAB = id;
            loadContent()
        }


        function initTab() {
            startMainFunctions();
        }

    </script>

@endsection