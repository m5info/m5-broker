<table class="tov-table table  table-bordered">
    <thead>
    <tr class="sort-row">
        <th>№ п\п</th>
        <th>№ счета</th>
        <th>№ акта</th>
        <th>Время создания</th>
        <th>Тип</th>
        <th>Создал</th>
        <th>Примечание</th>
    </tr>
    </thead>
    <tbody>
        @if(sizeof($acts))
            @foreach($acts as $act)
                <tr class="clickable-row" data-href="/contracts/reception_contracts/details/{{ $act->bso_acts_id }}">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $act->invoices_id }}</td>
                    <td>{{ $act->act_number }}</td>
                    <td>{{ setDateTimeFormatRu($act->time_create) }}</td>
                    <td>{!! $act->short_title !!} </td>
                    <td>{{ $act->creator }}</td>
                    <td>Срок передачи до: {{ getDateFormatRu($act->target_date) }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7" class="text-center">Пока нет актов</td>
            </tr>
        @endif
    </tbody>
</table>

