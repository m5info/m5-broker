@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">Лиды из внешних источников</h1>
    </div>

    <div class="block-inner">
        <div id="table_full"></div>
    </div>

    @include('_chunks._pagination',['callback'=>'loadItems'])

@endsection



@section('js')

    <script>

        var request = JSON.parse('{!! json_encode(request()->query()) !!}');
        $(function() {

            loadItems();

        });

        function loadItems() {

            var data = {
                page_count: $("#page_count").val(),
                PAGE: PAGE,
            };

            $.each(request, function(key, val){
                data[key] = val;
            });


            $('#page_list').html('');
            $('#table_row').html('');
            $('#view_row').html(0);
            $('#max_row').html(0);

            loaderShow();

            $.post("/contracts/partner-leads/leads_list", data, function (response) {
                $('#table_full').html(response.html);
                $('#view_row').html(response.view_row);
                $('#max_row').html(response.max_row);

                ajaxPaginationUpdate(response.page_max,loadItems);
                /*$('#page_list').pagination({
                    total:response.page_max,
                    pageSize:1,
                    pageNumber: PAGE,
                    layout:['first','prev','links','next','last'],
                    onSelectPage: function(pageNumber, pageSize){
                        setPage(pageNumber);
                    }
                });*/
                loaderHide();


            }).always(function() {
                loaderHide();
            });
        }

    </script>

    <style>
        .lead-table{
            width: 100%;
        }
    </style>
@endsection
