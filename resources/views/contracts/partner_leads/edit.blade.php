@extends('layouts.app')

@section('content')

    <div class="page-heading">
        <h1 class="inline-h1">Лид #{{$lead->id}} ({{$lead->status}})</h1>
    </div>

    <div class="row form-horizontal" id="main_container">
        {{--Данные по лиду--}}
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">


            <div class="page-subheading">
                <h2>Данные о клиенте</h2>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <label class="control-label">Имя</label>
                <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->name}}">

                <label class="control-label">Телефон</label>
                <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->phone}}">

                <label class="control-label">Город</label>
                <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->car_region}}">

            </div>


            <div class="page-subheading">
                <h2>Данные о переходе</h2>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <label class="control-label">IP</label>
                <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->user_ip}}">

                <label class="control-label">Операционная система</label>
                <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->user_os}}">

                <label class="control-label">Браузер</label>
                <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->user_browser}}">

                @if($lead->device_brand)
                    <label class="control-label">Модель устройства</label>
                    <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->device_brand}} {{$lead->device_model}}">
                @endif

                @if($lead->user_source)
                    <label class="control-label">Источник перехода</label>
                    <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->user_source}}">
                @endif

            </div>


            <div class="page-subheading">
                <h2>Данные об авто</h2>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <label class="control-label">Госномер</label>
                <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->object_gosnum}}">

                <label class="control-label">Марка</label>
                <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->car_brand}}">

                <label class="control-label">Модель</label>
                <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->car_model}}">

                <label class="control-label">Мощность</label>
                <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->object->power}}">

                <label class="control-label">Год выпуска</label>
                <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->object->car_year}}">
{{--
                <label class="control-label">Тип ТС</label>
                <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->user_browser}}">

                <label class="control-label">Цель использования</label>
                <input class="form-control" id="bso_title_0" disabled="" autocomplete="off" name="" type="text" value="{{$lead->user_browser}}">--}}

            </div>
        </div>
        {{--Данные из av100, контракты, выбранные страховые--}}
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">


            <div class="page-subheading">
                <h2 id="av100_spoiler">Изменить статус лида</h2>
            </div>

            {{ Form::open(['url' => url("/contracts/partner-leads/{$lead->id}/edit"), 'method' => 'post', 'class' => 'form-horizontal form-status']) }}



            <div class="form-group">
                <label class="col-sm-4 control-label">Статус</label>
                <div class="col-sm-8">
                    {{ Form::select('statys_id', collect(\App\Models\Partners\Leads::STATYS), $lead->statys_id, ['class' => 'form-control ', 'required']) }}
                </div>
            </div>

            <button type="submit" class="btn btn-success">{{ trans('form.buttons.save') }}</button>

            {{Form::close()}}


            <div class="page-subheading">
                <h2 id="av100_spoiler">Полная информация по авто от AV100</h2>
            </div>

            <div class="btn btn-success btn-left">
                <div id="av100_show" class="spoiler_toggle">
                    Показать полностью <i class="fa fa-chevron-down"></i>
                </div>
                <div id="av100_hide" class="spoiler_toggle">
                    Скрыть <i class="fa fa-chevron-up"></i>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="av100">
                    <pre>
                        {{$lead->av100}}
                    </pre>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>
        $(function() {

            $('.spoiler_toggle').click(function () {
                $('.spoiler_toggle, #av100').toggle();
            })

            $('.form-status').submit(function (e) {

                e.preventDefault();

                $.post($(this).prop('action'), $(this).serialize(), function (response) {
                    window.reload();
                });

            });

        });
    </script>

    <style>
        #av100, #av100_hide{
            display: none;
        }
        #av100{
            padding-top: 20px;
        }
    </style>
@endsection