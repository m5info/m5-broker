
    <table class="lead-table table table-bordered bso_items_table">
        <thead>
        <tr>
            <th>#</th>
            <th>Имя</th>
            <th>Телефон</th>
            <th>Требуется ДК</th>
            <th>Бренд авто</th>
            <th>Статус лида</th>
            <th>Контракт</th>
            <th>Подробно</th>
        </tr>
        </thead>
        @if(sizeof($lead_items))
            @foreach($lead_items as $lead)
                <tr>
                    <td>{{$lead->id}}</td>
                    <td>{{$lead->name}}</td>
                    <td>{{$lead->phone}}</td>
                    @if($lead->need_diagnostics_card)
                        <td>Да</td>
                    @else
                        <td></td>
                    @endif
                    <td>{{$lead->car_brand}}</td>
                    <td>{{$lead->status}}</td>
                    @if($lead->contract_id)
                        <td><a href="{{url("/contracts/online/edit/{$lead->contract_id}/")}}">{{$lead->contract_id}}</a></td>
                    @else
                        <td></td>
                    @endif
                    <td>
                        <a class="btn btn-success btn-left" href="{{url("/contracts/partner-leads/{$lead->id}/")}}">
                            <i class="fa fa-eye"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
