@extends('layouts.app')

@section('content')


    <div class="page-heading">
        <h2>Быстрый акцепт: Создано {{count($res)}} из {{$contracts_count}}</h2>


    </div>


    <table class="tov-table">
        <thead>
        <tr>
            <th><a href="javascript:void(0);">Договоры</a></th>
        </tr>
        </thead>
        @if(sizeof($res))
        @foreach($res as $r)
            <tr>
                <td><a href="{{url("/bso/items/{$r['bso_id']}/")}}" target="_blank">{{$r['bso_title']}}</a> </td>
            </tr>
        @endforeach
        @else
            <tr>
                <td>{{ trans('form.empty') }}</td>
            </tr>

        @endif
    </table>



@endsection

@section('js')

    <script>




        $(function () {


        });



    </script>


@endsection