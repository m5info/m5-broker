@extends('layouts.app')

@section('content')


    <div class="page-heading">
        <h2>
            <span class="btn-left">
                Первичные договора
            </span>
        </h2>



    </div>



    {{ Form::open(['url' => url('/contracts/primary-agent/'), 'method' => 'post',  'class' => 'form-horizontal', 'id' => 'formContract']) }}

    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="block-inner" style="overflow-x: visible;">


                <table class="table table-bordered bso_items_table">
                    <thead>
                    <tr>
                        <th nowrap>Номер договора <span style="color:red">*</span></th>
                        <th nowrap colspan="2">Cтрахователь <span style="color:red">*</span></th>
                        <th nowrap>Дата заключения <span style="color:red">*</span></th>
                        <th nowrap>Дата начала <span style="color:red">*</span></th>
                        <th nowrap>Дата окончания <span style="color:red">*</span></th>
                        <th nowrap>Финансовая политика <span style="color:red">*</span></th>
                        <th nowrap>Премия по договору <span style="color:red">*</span></th>
                        <th nowrap>Офиц %</th>
                        <th nowrap>Неоф %</th>
                        <th nowrap>Банк %</th>
                        <th nowrap>Дата оплаты <span style="color:red">*</span></th>
                        <th nowrap>Сумма платежа <span style="color:red">*</span></th>
                        <th nowrap colspan="2">Вид оплаты <span style="color:red">*</span></th>
                        <th nowrap></th>
                    </tr>
                    </thead>
                    <tbody id="control">
                    </tbody>
                </table>


        </div>

    </div>



    {{Form::close()}}

    <div class="form-group">
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right: 0px;">


            <span class="btn btn-primary btn-left" onclick="addContractCongestion()">
                Добавить
            </span>

            <span class="btn btn-success btn-right" id="controlSaveButtonBox" onclick="saveFastAccept()">
                Сохранить
            </span>


        </div>
    </div>



@endsection

@section('js')

    <script>

        function setAlgos(key) {

            var element = $("#installment_algorithms_id_" + key);

            if(element.val() > 0){

                var algorithm_id = element.val();

                if (getInputsInstallmentAlgorithms("payment_algo_" + key, algorithm_id, key, 0)) {

                    $.ajax({
                        type: "POST",
                        url: "{{url("/settings/installment_algorithms_list/get_quantity")}}",
                        async: true,
                        data: {'algorithm_id': algorithm_id},
                        success: function (response) {

                            var str = $('[name="contract[' + key + '][payment_total]"]').val().replace(/\s/g, '');
                            var premium = parseInt(str);

                            if (parseInt($('[name="contract[' + key + '][payment_total]"]').val()) > 0) {

                                var payment_sum = premium / response;
                                $('[name="contract[' + key + '][payment][0][payment_total]"]').val(CommaFormatted(payment_sum));

                                var indexes = response + 1;

                                for (var i = 1; i < indexes; i++) {
                                    $('#algo_payment_sum_' + i).val(CommaFormatted(payment_sum));
                                }
                            }
                        }
                    });
                }

            } else {
                $("#payment_algo_" + key).html('');
            }
        }

        function removeBorder(el) {
            $(el).css('border', 'none');
        }

        function updateAlgos(key) {

            var element = $("#installment_algorithms_id_" + key);

            var pay_parts = $('.algo_payment_sum_'+key);
            var total_pay_parts_sum = 0.00;

            pay_parts.each(function(){
                total_pay_parts_sum += parseFloat($(this).val().replaceAll(' ', '').replace(',', '.'));
            });

            var contract_award = $('[name="contract[' + key + '][payment_total]"]').val().replaceAll(' ','').replace(',','.');

            var total = parseFloat(contract_award) - parseFloat(total_pay_parts_sum);

            $('[name="contract['+key+'][payment][0][payment_total]"]').val(CommaFormatted(total));

            if (element.val() > 0) {

                var algorithm_id = element.val();

            } else {
                $("#payment_algo_" + key).html('');
            }
        }

        $(function () {
            addContractCongestion();
            activSearchOrdersToFront("order_title", "_0");




            $(document).on('change', '[name*="[pay_method_id]"]', function () {

                var key = $(this).data('key');
                var method_blocks = {
                    0: 'receipt_block_' + key,
                    1: 'check_block_' + key,
                    2: 'none'
                };

                var method = parseInt($(this).val());

                $.each(method_blocks, function (k, v) {
                    $("#" + v).hide()
                });

                $("#" + method_blocks[pay_methods_id2types[method]]).show();

                if (method === 1) {
                    $('[name="contract[' + key + '][payment][0][bso_receipt]"]').addClass('valid_fast_accept');
                } else {
                    $('[name="contract[' + key + '][payment][0][bso_receipt]"]').removeClass('valid_fast_accept');
                }

            });


        });


        var MY_COUNT_TEMP = 0;

        String.prototype.replaceAll = function (search, replace) {
            return this.split(search).join(replace);
        }

        function addContractCongestion() {

            myHtml = myGetAjax("{{url("/contracts/primary-agent/form_fast_acceptance/")}}");

            myHtml = myHtml.replaceAll('[:KEY:]', MY_COUNT_TEMP);
            myHtml = myHtml.replaceAll('[:NUM:]', (MY_COUNT_TEMP + 1));

            $('#control').append(myHtml);

            startContractFunctions(MY_COUNT_TEMP);

            activSearchOrdersToFront("order_title", "_"+MY_COUNT_TEMP);

            MY_COUNT_TEMP = MY_COUNT_TEMP + 1;


            updateContractIndexes();

            controlSaveButton();

            toggleFlowOptions(MY_COUNT_TEMP - 1);

            $('.select2-ws').select2("destroy").select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true,
                minimumResultsForSearch: -1,
            });

        }


        function toggleFlowOptions(key) {

            // $.each($('[name*="[payment_type]"]'), function (k,v) {

            // var key = $(v).data('key');


            // if(parseInt($(this).val()) === 0){
            if (parseInt($('[name="contract[' + key + '][payment][0][payment_type]"]').val()) === 0) {

                $('[name="contract[' + key + '][payment][0][bso_not_receipt]"]').prop('checked', true).change();

                $('[name*="[payment_flow]"][data-key="' + key + '"]').html(
                    '<option value="0" selected="selected">Брокер</option>'
                );

                $('[name="contract['+key+'][payment][0][payment_flow]"]').siblings('.select2-container').find('.select2-chosen').text('Брокер');

            }else if (parseInt($('[name="contract['+key+'][payment][0][payment_type]"]').val()) === 3){

                $('[name="contract['+key+'][payment][0][bso_not_receipt]"]').prop('checked', true).change();


                $('[name*="[payment_flow]"][data-key="'+key+'"]').html(
                    '<option value="1" selected="selected">СК</option>'
                );

                $('[name="contract['+key+'][payment][0][payment_flow]"]').siblings('.select2-container').find('.select2-chosen').text('СК');

            } else {
                $('[name="contract[' + key + '][payment][0][bso_not_receipt]"]').prop('checked', false).change();

                $('[name*="[payment_flow]"][data-key="' + key + '"]').html(
                    '<option value="0" selected="selected">Брокер</option><option value="1">СК</option>'
                )
            }

            // })

        }

        function deleteContract(id) {

            var result = confirm('Удалить данные оформления?');
            if(result){
                $('#contract_' + parseInt(id)).remove();
                updateContractIndexes();
                controlSaveButton();
            }


        }

        function getContract() {
            return $('.temp_contract');
        }


        function updateContractIndexes() {
            var contracts = getContract();
            contracts.each(function (i, contract) {
                $(contract).find('.number').html(i + 1);
            });
        }


        function startContractFunctions(KEY) {

            var bso_used = [];
            $.each($('[name*="][bso_title]"]'), function (k, v) {
                bso_used.push($(v).val());
            });

            activSearchBso("bso_title", '_' + KEY, 1, bso_used);

            activSearchBso("bso_receipt", '_' + KEY, 2, bso_used);


            $('#manager_id_' + KEY).select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true,
                minimumInputLength: 3
            });


            $('#insurer_type_' + KEY).change(function () {

                key = $(this).data('key');
                dataForm = $("#contract_" + key);

                type = $(this).val();

                $("#insurer_fio_" + key).removeClass('valid_fast_accept');
                $("#insurer_title_" + key).removeClass('valid_fast_accept');


                if (parseInt(type) == 0) {
                    $("#insurer_fio_" + key).addClass('valid_fast_accept');
                    $(dataForm).find('.insurer_fl').show();
                    $(dataForm).find('.insurer_ul').hide();

                } else {
                    $("#insurer_title_" + key).addClass('valid_fast_accept');
                    $(dataForm).find('.insurer_fl').hide();
                    $(dataForm).find('.insurer_ul').show();

                }

            });


            $('#insurer_fio_' + KEY).suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "NAME",
                count: 5,
                onSelect: function (suggestion) {
                    key = $(this).data('key');
                    $('#insurer_title_' + key).val($(this).val());

                }
            });

            $('#insurer_title_' + KEY + ', #insurer_inn_' + KEY + ', #insurer_kpp_' + KEY).suggestions({
                serviceUrl: DADATA_AUTOCOMPLETE_URL,
                token: DADATA_TOKEN,
                type: "PARTY",
                count: 5,
                onSelect: function (suggestion) {
                    var data = suggestion.data;

                    key = $(this).data('key');


                    $('#insurer_title_' + key).val(suggestion.value);
                    $('#insurer_inn_' + key).val(data.inn);
                    $('#insurer_kpp_' + key).val(data.kpp);

                    $('#insurer_fio_' + key).val($('#insurer_title_' + key).val());

                }
            });


            $('.sum')
                .change(function () {
                    $(this).val(CommaFormatted($(this).val()));
                })
                .blur(function () {
                    $(this).val(CommaFormatted($(this).val()));
                })
                .keyup(function () {
                    $(this).val(CommaFormatted($(this).val()));
                });

            $('.integer').change(function () {
                $(this).val($(this).val().replace(/\D+/g, ""))
            }).keyup(function () {
                $(this).val($(this).val().replace(/\D+/g, ""))
            });

            $('.phone').mask('+7 (999) 999-99-99');

        }


        function selectBso(object_id, key, type, suggestion) {

            var data = suggestion.data;

            if (parseInt(type) == 1) { // БСО
                $('#bso_id' + key).val(data.bso_id);
                $('#bso_supplier_id' + key).val(data.bso_supplier_id);
                $('#insurance_companies_id' + key).val(data.insurance_companies_id);
                $('#product_id' + key).val(data.product_id);
                $('#agent_id' + key).val(data.agent_id);


                getOptionInstallmentAlgorithms("installment_algorithms_id" + key, data.insurance_companies_id, 0);
                getOptionFinancialPolicy("financial_policy_id" + key, data.insurance_companies_id, data.bso_supplier_id, data.product_id, 0);


                getKvAccesses(key, data.product_id);

                intkey = key.replaceAll('_', '');


                var receipt_used = [];
                $.each($('[name*="][bso_receipt]"]'), function (k, v) {
                    receipt_used.push($(v).val());
                });

                activSearchBso("bso_receipt", key, 2, receipt_used);
            }

            if (parseInt(type) == 2) { // Квитанция
                $('#bso_receipt_id' + key).val(data.bso_id);

            }


        }

        function setPaymentQty(key) {
            $('input[name="contract['+key+'][payment][0][payment_total]"]').val(CommaFormatted($('input[name="contract['+key+'][payment_total]"]').val()));
        }

        function setPaymentQty_removeBorder(key){
            removeBorderColor('input[name="contract['+key+'][payment][0][payment_total]"]');
        }

        function controlSaveButton() {

            $('.valid_fast_accept').change(function () {

                $(this).css("border-color", "");

                if ($(this)[0].localName === 'select') {
                    var container = $(this).prev('.select2-container');
                    container.css({'border': 'none'});
                }
            });

            if (getContract().length > 0) {
                $("#controlSaveButtonBox").show();
            } else {
                $("#controlSaveButtonBox").hide();
            }
        }


        function saveFastAccept() {
            if (validFastAccept() == true) {
                $("#formContract").submit();
            }
        }

        function validFastAccept() {

            var first_err;
            var msg_arr = [];
            var i = 0;

            $('.valid_fast_accept').each(function () {

                var tag = $(this)[0].localName;

                var val = '';
                if (['select', 'input'].indexOf(tag) !== -1) {
                    val = $(this).val();
                } else {
                    val = $(this).html();
                }


                if (tag === 'select' && parseInt(val) === 0) {

                    if(i == 0 && $(this).parent('div').css('display') != 'none'){
                        msg_arr.push('Заполните все поля');
                        first_err = $(this);
                        i++;
                    }
                    var container = $(this).prev('.select2-container');
                    container.css({
                        'height': '36px',
                        'border': '1px red solid'
                    });
                }

                if (val.length < 1) {
                    if(i == 0 && $(this).parent('div').css('display') != 'none'){
                        first_err = $(this);
                        i++;
                    }
                    msg_arr.push('Заполните все поля');

                    var elem = $(this);

                    elem.css("border-color", "red");


                }

            });

            // проверка на существование бсо
            $('.temp_contract').each(function (index, value) {
                var bso = $('input[name="contract['+index+'][bso_title]"]').val();

                $.ajax({
                    type: "POST",
                    url: "{{url('/contracts/fast_accept/check_correct_bso')}}",
                    async: false,
                    data: {'bso':bso},
                    success: function (res) {

                        if (res == 0){
                            first_err = $('input[name="contract['+index+'][bso_title]"]');
                            i++;
                            msg_arr.push('Заполните все поля');

                            var elem = $('input[name="contract['+index+'][bso_title]"]');
                            elem.css("border-color", "red");
                        }
                    }
                });
            });

            // проверка на существование квитанции
            $('.temp_contract_receipt').each(function (index, value) {
                var bso = $('input[name="contract['+index+'][payment][0][bso_receipt]"]').val();

                $.ajax({
                    type: "POST",
                    url: "{{url('/contracts/fast_accept/check_correct_bso')}}",
                    async: false,
                    data: {'bso':bso},
                    success: function (res) {

                        if (res == 0 && $('input[name="contract['+index+'][payment][0][bso_receipt]"]').parent('div').css('display') != 'none'){
                            first_err =  $('input[name="contract['+index+'][payment][0][bso_receipt]"]');
                            i++;
                            msg_arr.push('Заполните все поля');

                            var elem =  $('input[name="contract['+index+'][payment][0][bso_receipt]"]');
                            elem.css("border-color", "red");
                        }
                    }
                });
            });

            if (msg_arr.length) {
                $('html, body').animate({scrollTop: $(first_err).offset().top - 300}, 800);

                return false;
            }


            return true;
        }

        function get_end_dates(start_date) {
            var cur_date_tmp = start_date.split(".");
            var cur_date = new Date(cur_date_tmp[2], cur_date_tmp[1] - 1, cur_date_tmp[0]);
            var new_date = new Date(cur_date.setYear(cur_date.getFullYear() + 1));
            var new_date2 = new Date(new_date.setDate(new_date.getDate() - 1));
            return getFormattedDate(new_date2);
        }

        function removeBorderColor(el){
            $(el).css('borderColor', '#e0e0e0');
        }

        function setAllDates(key) {
            sign_date = $("#sign_date_" + key).val();
            $("#begin_date_" + key).val(sign_date);


            removeBorderColor("#begin_date_" + key);

            return setEndDates(key);
        }

        function setEndDates(key) {

            removeBorderColor("#end_date_" + key);
            begin_date = $("#begin_date_" + key).val();
            $("#end_date_" + key).val(get_end_dates(begin_date));
        }


        function viewSetFinancialPolicyManually(obj, key) {
            if ($(obj).is(':checked')) {
                $("#financial_policy_manually_" + key).show();
                $("#financial_policy_id_" + key).hide();
            } else {
                $("#financial_policy_manually_" + key).hide();
                $("#financial_policy_id_" + key).show();
            }
        }

        function viewSetBsoNotReceipt(obj, key) {
            if ($(obj).is(':checked')) {
                $("#bso_receipt_" + key).attr('disabled', 'disabled');
                $("#bso_receipt_" + key).removeClass('valid_fast_accept');

                $('[name="contract[' + key + '][payment][0][bso_receipt]"]').val('');
                $('[name="contract[' + key + '][payment][0][bso_receipt_id]"]').val('');

            } else {
                $("#bso_receipt_" + key).removeAttr('disabled');
                $("#bso_receipt_" + key).addClass('valid_fast_accept');
            }

            $("#bso_receipt_" + key).css("border-color", "");
        }


        function startContractObjectInsurerFunctions(KEY) {
            $('#object_insurer_ts_mark_id_' + KEY).select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true
            });
            $('#object_insurer_ts_model_id_' + KEY).select2({
                width: '100%',
                dropdownCssClass: "bigdrop",
                dropdownAutoWidth: true
            });


        }

        function getModelsObjectInsurer(KEY, select_model_id) {


            $.getJSON(
                '{{url("/contracts/actions/get_models")}}',
                {
                    categoryId: $('#object_insurer_ts_category_' + KEY).val(),
                    markId: $('#object_insurer_ts_mark_id_' + KEY).select2('val')
                },
                function (response) {

                    var options = "<option value='0'>Не выбрано</option>";

                    response.map(function (item) {
                        options += "<option value='" + item.id + "'>" + item.title + "</option>";
                    });

                    $('#object_insurer_ts_model_id_' + KEY).html(options).select2('val', select_model_id);

                });
        }




    </script>


@endsection