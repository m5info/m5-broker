@php
    $key = 0;
@endphp
@if(isset($autosaved_data->contract))
    @foreach($autosaved_data->contract as $contract)
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 temp_contract" id="contract_{{$key}}" style="padding-right: 0px;" data-key="{{$key}}">
            <div class="trucks-item order-item">
                <div class="info">
                    Договор <span class="number">{{ $key+1}}</span>
                    <input type="hidden" value="0" name="contract[{{$key}}][is_personal_sales]" data-key="{{$key}}"/>
                    <button onclick="deleteContract('{{$key}}')" style="background-color: #FFF;font-size: 15px;margin-top: -5px;border: none;" class="pull-right view_but" type="button">
                        <i class="fa fa-remove" style="color: red;"></i>
                    </button>
                </div>
                <div class="divider"></div>
                <div class="info">
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" >
                            <h3>Договор (полис)</h3>
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row form-horizontal">
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
                                        <label class="control-label">
                                            Заключения<span style="color:red">*</span>
                                        </label>
                                        {{ Form::text('contract['.$key.'][sign_date]', $contract->sign_date, ['class' => 'form-control datepicker date valid_fast_accept', 'id'=>'sign_date_'.$key, 'onchange'=>'setAllDates('.$key.')']) }}
                                        <input type="hidden" name="contract[{{$key}}][begin_date]" value="{{$contract->begin_date}}" id="begin_date_{{$key}}" />
                                        <input type="hidden" name="contract[{{$key}}][end_date]" value="{{$contract->end_date}}" id="end_date_{{$key}}" />
                                    </div>
                                    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-6" >
                                        <label class="control-label pull-left">
                                            № договора <span style="color:red">*</span>
                                            @if(auth()->user()->hasPermission('contracts', 'show_e_policy'))
                                                <a href="javascript:void(0);" class="btn-xs btn-primary" onclick="openFancyBoxFrame('{{url("/contracts/temp_contracts/add/electronic_for_primary/")}}?key={{$key}}&from_url={{request()->path()}}')" style="display: inline">E-Полис</a>
                                            @endif
                                        </label>
                                        <input type="hidden" name="contract[{{$key}}][withdraw_documents]"  value="{{$contract->withdraw_documents}}"/>
                                        {{ Form::text('contract['.$key.'][bso_title]', $contract->bso_title, ['class' => 'form-control valid_fast_accept', 'id'=>'bso_title_'.$key]) }}
                                        <input type="hidden" name="contract[{{$key}}][bso_id]" id="bso_id_{{$key}}" value="{{$contract->bso_id}}"/>
                                        <input type="hidden" name="contract[{{$key}}][bso_supplier_id]" id="bso_supplier_id_{{$key}}" value="{{$contract->bso_supplier_id}}"/>
                                        <input type="hidden" name="contract[{{$key}}][insurance_companies_id]" id="insurance_companies_id_{{$key}}" value="{{$contract->insurance_companies_id}}"/>
                                        <input type="hidden" name="contract[{{$key}}][product_id]" id="product_id_{{$key}}" value="{{$contract->product_id}}"/>
                                        <input type="hidden" name="contract[{{$key}}][agent_id]" id="agent_id_{{$key}}" value="{{ auth()->id() }}"/>
                                        <input type="hidden" name="contract[{{$key}}][order_id]" id="order_id_{{$key}}" value="{{$contract->order_id}}"/>
                                        <input type="hidden" name="contract[{{$key}}][order_title]" id="order_title_{{$key}}" value="{{$contract->order_title}}"/>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
                                        <label class="control-label">
                                            Выезда
                                        </label>
                                        {{ Form::text('contract['.$key.'][delivery_date]', $contract->delivery_date, ['class' => 'form-control datepicker date', 'id'=>'delivery_date_'.$key, 'onchange'=>'setAllDates('.$key.')']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row form-horizontal">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                                        <label class="control-label">Условие продажи</label>
                                        {{ Form::select('contract['.$key.'][sales_condition]',
                                            collect([
                                                0 => \App\Models\Contracts\Contracts::SALES_CONDITION[0],
                                                1 => \App\Models\Contracts\Contracts::SALES_CONDITION[1],
                                                2 => \App\Models\Contracts\Contracts::SALES_CONDITION[2],
                                            ]), $contract->sales_condition,
                                            ['class' => 'form-control select2-ws', 'id'=>'sales_condition_'.$key, 'data-key' => $key,
                                            'onchange' => 'changeSalesVondition('.$key.')'])
                                        }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3" >
                            <h3>Cтрахователь</h3>
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row form-horizontal">
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4" >
                                        <label class="control-label">
                                            Тип
                                        </label>
                                        {{ Form::select('contract['.$key.'][insurer][type]', collect([0=>"ФЛ", 1=>'ЮЛ']), $contract->insurer->type, ['class' => 'form-control select2-ws', 'id'=>'insurer_type_'.$key, 'data-key'=>$key]) }}
                                    </div>
                                    <div class="insurer_fl col-xs-9 col-sm-9 col-md-9 col-lg-8" >
                                        <label class="control-label">
                                            ФИО <span style="color:red">*</span>
                                        </label>
                                        {{ Form::text('contract['.$key.'][insurer][fio]', $contract->insurer->fio, ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_fio_'.$key, 'data-key'=>$key]) }}
                                    </div>
                                    <div class="insurer_ul col-xs-9 col-sm-9 col-md-9 col-lg-8" style="display: none">
                                        <label class="control-label">Название</label>
                                        {{ Form::text('contract['.$key.'][insurer][title]', $contract->insurer->title, ['class' => 'form-control', 'id'=>'insurer_title_'.$key, 'data-key'=>$key]) }}
                                    </div>
                                    <input type="hidden" name="contract[{{$key}}][insurer][doc_serie]" value="{{$contract->insurer->doc_serie}}"/>
                                    <input type="hidden" name="contract[{{$key}}][insurer][doc_number]" value="{{$contract->insurer->doc_number}}"/>
                                    <input type="hidden" name="contract[{{$key}}][insurer][inn]" value="{{$contract->insurer->inn}}" id="insurer_inn_{{$key}}" data-key="{{$key}}"/>
                                    <input type="hidden" name="contract[{{$key}}][insurer][kpp]" value="{{$contract->insurer->kpp}}" id="insurer_kpp_{{$key}}" data-key="{{$key}}"/>
                                    <input type="hidden" name="contract[{{$key}}][insurer][phone]" value="{{$contract->insurer->phone}}"/>
                                    <input type="hidden" name="contract[{{$key}}][insurer][email]" value="{{$contract->insurer->email}}"/>
                                </div>
                            </div>
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row form-horizontal">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="view_form_manager_{{$key}}" style="display: none;" >
                                        <label class="control-label">Агент/Менеджер <span style="color:red">*</span></label>
                                        {{ Form::select('contract['.$key.'][manager_id]', $agents->pluck('name', 'id')->prepend('Выберите значение', 0), $contract->manager_id,['class' => 'form-control valid_fast_accept manager_id_'.$key])}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-2" >
                            <h3>Условия договора</h3>
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row form-horizontal">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <label class="control-label">
                                            Финансовая политика <span style="color:red">*</span>
                                        </label>
                                        {{ Form::select('contract['.$key.'][financial_policy_id]', collect([0=>'Укажите БСО']), $contract->financial_policy_id, ['class' => 'form-control valid_fast_accept select2-ws', 'id'=>'financial_policy_id_'.$key]) }}
                                        <input type="hidden" value="{{$contract->financial_policy_manually_set}}" name="contract[{{$key}}][financial_policy_manually_set]" />
                                    </div>
                                </div>
                            </div>
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row form-horizontal">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                        <label class="control-label">Офиц%</label>
                                        {{ Form::text('contract['.$key.'][payment][0][official_discount]', $contract->payment[0]->official_discount ?? 0, ['class' => 'form-control sum percents_input_validation', 'id'=>'official_discount_'.$key]) }}
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                        <label class="control-label">Неоф%</label>
                                        {{ Form::text('contract['.$key.'][payment][0][informal_discount]', $contract->payment[0]->informal_discount ?? 0, ['class' => 'form-control sum percents_input_validation', 'id'=>'informal_discount_'.$key]) }}
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                        <label class="control-label">Банк%</label>
                                        {{ Form::text('contract['.$key.'][payment][0][bank_kv]', $contract->payment[0]->bank_kv ?? 0, ['class' => 'form-control sum percents_input_validation', 'id'=>'bank_kv_'.$key, 'onkeyup' => 'setBankSelect('.$key.')']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3" >
                            <h3>Порядок оплаты</h3>
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row form-horizontal">
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        <label class="control-label">
                                            Оплаты <span style="color:red">*</span>
                                        </label>
                                        {{ Form::text('contract['.$key.'][payment][0][payment_data]', $contract->payment[0]->payment_data, ['class' => 'form-control date datepicker valid_fast_accept', 'id'=>'payment_data_'.$key]) }}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                        <label class="control-label">
                                            Сумма  <span style="color:red">*</span>
                                        </label>
                                        {{ Form::text('contract['.$key.'][payment_total]', $contract->payment_total, ['class' => 'form-control sum valid_fast_accept', 'id'=>'payment_total_'.$key, 'onkeyup'=>'setPaymentQty('.$key.')', 'onchange' => 'setPaymentQty_removeBorder('.$key.')']) }}
                                        <input type="hidden" name="contract[{{$key}}][payment][0][payment_total]" value="{{$contract->payment[0]->payment_total}}" id="payment_total_{{$key}}" />
                                        <input type="hidden" name="contract[{{$key}}][payment][0][payment_number]" value="{{$contract->payment[0]->payment_number}}" id="payment_number_{{$key}}" />
                                        <input type="hidden" name="contract[{{$key}}][payment][0][type_id]" value="{{$contract->payment[0]->type_id}}" />
                                    </div>
                                </div>
                            </div>
                            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row form-horizontal">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
                                        <label class="control-label" id="check_label_{{$key}}">
                                            Вид оплаты
                                        </label>
                                        {{ Form::select('contract['.$key.'][payment][0][pay_method_id]', \App\Models\Finance\PayMethod::query()->where('is_actual', 1)->pluck('title', 'id'), $contract->payment[0]->pay_method_id, ['class' => 'form-control valid_fast_accept select2-ws', 'id'=>'check_'.$key, 'data-key' => $key]) }}
                                        <script>
                                            var pay_methods_id2types = {
                                                @foreach(\App\Models\Finance\PayMethod::query()->where('is_actual', 1)->get() as $method)
                                                {!! "$method->id : $method->key_type," !!}
                                                @endforeach
                                            };
                                        </script>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" id="receipt_block_{{$key}}">
                                        <label class="control-label"  id="bso_receipt_label_{{$key}}">
                                            Квитанция
                                            @if(auth()->user()->hasPermission('contracts', 'show_e_policy'))
                                                <a href="javascript:void(0);" class="btn-xs btn-primary" data-create_receipt="{{$key}}" style="display: inline">Е-квит</a>
                                            @endif
                                        </label>
                                        {{ Form::text('contract['.$key.'][payment][0][bso_receipt]', $contract->payment[0]->bso_receipt, ['class' => 'form-control valid_fast_accept temp_contract_receipt', 'id'=>'bso_receipt_'.$key, 'data-key' => $key]) }}
                                        <input type="hidden" name="contract[{{$key}}][payment][0][bso_receipt_id]" value="{{$contract->payment[0]->bso_receipt_id}}" id="bso_receipt_id_{{$key}}" />
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="check_block_{{$key}}" style="display: none">
                                        <div class="row form-horizontal">
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                <label class="control-label">Телефон</label>
                                                {{ Form::text('contract['.$key.'][check_phone]', $contract->check_phone, ['class' => 'form-control phone', 'id'=>'check_phone_'.$key,]) }}
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                <label class="control-label">E-mail</label>
                                                {{ Form::text('contract['.$key.'][check_email]', $contract->check_email, ['class' => 'form-control email', 'id'=>'check_email_'.$key]) }}
                                            </div>
                                        </div>
                                        <div class="row form-horizontal">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <label class="control-label">Отправить</label>
                                                {{ Form::select('contract['.$key.'][when_to_send]', collect(\App\Models\Api\Atol\AtolCheck::WHEN_TO_SEND), $contract->when_to_send, ['class' => 'form-control', 'id'=>'when_to_send_'.$key]) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="display: none;">
                                <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                                    <label class="control-label">Алгоритм рассрочки</label>
                                    {{ Form::select('contract['.$key.'][installment_algorithms_id]', collect([0=>'Укажите номер договора']), $contract->installment_algorithms_id, ['class' => 'form-control select2-ws', 'id'=>'installment_algorithms_id_'.$key, 'onchange' => 'setAlgos('.$key.')']) }}
                                </div>
                                <div id='payment_algo_{{$key}}' class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @php
        $key++;
    @endphp
    @endforeach
@endif