<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 temp_contract" id="contract_[:KEY:]" style="padding-right: 0px;">
    <div class="trucks-item order-item">
        <div class="info">
            Договор <span class="number">[:NUM:]</span> - Условие продажи "{{ \App\Models\Contracts\Contracts::SALES_CONDITION[0] }}"
            <input type="hidden" name="contract[[:KEY:]][sales_condition]" id="product_id_[:KEY:]" value="0"/>
            <input type="hidden" value="0" name="contract[[:KEY:]][is_personal_sales]" data-key="[:KEY:]"/>
            <input type="hidden" name="contract[[:KEY:]][manager_id]" value="0"/>


            <button onclick="deleteContract('[:KEY:]')" style="background-color: #FFF;font-size: 15px;margin-top: -5px;border: none;" class="pull-right view_but" type="button">
                <i class="fa fa-remove" style="color: red;"></i>
            </button>

        </div>
        <div class="divider"></div>
        <div class="info">
            <div class="form-group">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3" >
                    <h3>Договор (полис)</h3>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >

                                <label class="control-label pull-left">
                                    Номер договора <span style="color:red">*</span>
                                </label>


                                <input type="hidden" name="contract[[:KEY:]][withdraw_documents]"  value="0"/>

                                {{ Form::text('contract[[:KEY:]][bso_title]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'bso_title_[:KEY:]']) }}
                                <input type="hidden" name="contract[[:KEY:]][bso_id]" id="bso_id_[:KEY:]" />
                                <input type="hidden" name="contract[[:KEY:]][bso_supplier_id]" id="bso_supplier_id_[:KEY:]" />
                                <input type="hidden" name="contract[[:KEY:]][insurance_companies_id]" id="insurance_companies_id_[:KEY:]" />
                                <input type="hidden" name="contract[[:KEY:]][product_id]" id="product_id_[:KEY:]" />
                                <input type="hidden" name="contract[[:KEY:]][agent_id]" id="agent_id_[:KEY:]" value="{{ auth()->id() }}"/>

                                <input type="hidden" name="contract[[:KEY:]][order_id]" id="order_id_[:KEY:]" />
                                <input type="hidden" name="contract[[:KEY:]][order_title]" id="order_title_[:KEY:]" />

                            </div>
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row form-horizontal">

                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4" >
                                <label class="control-label">
                                    Cтрахователь
                                </label>
                                {{ Form::select('contract[[:KEY:]][insurer][type]', collect([0=>"ФЛ", 1=>'ЮЛ']), 0, ['class' => 'form-control select2-ws', 'id'=>'insurer_type_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>


                            <div class="insurer_fl col-xs-9 col-sm-9 col-md-9 col-lg-8" >
                                <label class="control-label">
                                    ФИО <span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][insurer][fio]', '', ['class' => 'form-control valid_fast_accept', 'id'=>'insurer_fio_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                            <div class="insurer_ul col-xs-9 col-sm-9 col-md-9 col-lg-8" style="display: none">
                                <label class="control-label">Название</label>
                                {{ Form::text('contract[[:KEY:]][insurer][title]', '', ['class' => 'form-control', 'id'=>'insurer_title_[:KEY:]', 'data-key'=>'[:KEY:]']) }}
                            </div>

                            <input type="hidden" name="contract[[:KEY:]][insurer][doc_serie]" value=""/>
                            <input type="hidden" name="contract[[:KEY:]][insurer][doc_number]" value=""/>

                            <input type="hidden" name="contract[[:KEY:]][insurer][inn]" value="" id="insurer_inn_[:KEY:]" data-key="[:KEY:]"/>
                            <input type="hidden" name="contract[[:KEY:]][insurer][kpp]" value="" id="insurer_kpp_[:KEY:]" data-key="[:KEY:]"/>

                            <input type="hidden" name="contract[[:KEY:]][insurer][phone]" value=""/>
                            <input type="hidden" name="contract[[:KEY:]][insurer][email]" value=""/>


                        </div>
                    </div>






                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3" >
                    <h3>Даты договора</h3>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row form-horizontal">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <label class="control-label">
                                    Заключения <span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][sign_date]', '', ['class' => 'form-control datepicker date valid_fast_accept', 'id'=>'sign_date_[:KEY:]', 'onchange'=>'setAllDates([:KEY:])']) }}
                            </div>


                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <label class="control-label">
                                    Оплаты <span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][payment][0][payment_data]', '', ['class' => 'form-control date datepicker valid_fast_accept', 'id'=>'payment_data_[:KEY:]']) }}


                            </div>
                        </div>
                    </div>
                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <label class="control-label">
                                    Начала <span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][begin_date]', '', ['class' => 'form-control date datepicker valid_fast_accept', 'id'=>'begin_date_[:KEY:]',  'onchange'=>'setEndDates([:KEY:])']) }}
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <label class="control-label">Окончания <span style="color:red">*</span></label>
                                {{ Form::text('contract[[:KEY:]][end_date]', '', ['class' => 'form-control date datepicker valid_fast_accept', 'id'=>'end_date_[:KEY:]']) }}
                            </div>


                        </div>
                    </div>


                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3" >
                    <h3>Условия договора</h3>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <label class="control-label">
                                    ФП <span style="color:red">*</span>
                                </label>
                                {{ Form::select('contract[[:KEY:]][financial_policy_id]', collect([0=>'Укажите БСО']), 0, ['class' => 'form-control valid_fast_accept select2-ws', 'id'=>'financial_policy_id_[:KEY:]']) }}
                                <input type="hidden" value="0" name="contract[[:KEY:]][financial_policy_manually_set]" />
                            </div>


                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <label class="control-label">
                                    Премия <span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][payment_total]', '', ['class' => 'form-control sum valid_fast_accept', 'id'=>'payment_total_[:KEY:]', 'onkeyup'=>'setPaymentQty([:KEY:])', 'onchange' => 'setPaymentQty_removeBorder([:KEY:])']) }}
                            </div>

                        </div>
                    </div>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row form-horizontal">

                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                <label class="control-label">Офиц%</label>
                                {{ Form::text('contract[[:KEY:]][payment][0][official_discount]', '', ['class' => 'form-control sum', 'id'=>'official_discount_[:KEY:]']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                <label class="control-label">Неоф%</label>
                                {{ Form::text('contract[[:KEY:]][payment][0][informal_discount]', '', ['class' => 'form-control sum', 'id'=>'informal_discount_[:KEY:]']) }}
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
                                <label class="control-label">Банк%</label>
                                {{ Form::text('contract[[:KEY:]][payment][0][bank_kv]', '', ['class' => 'form-control sum', 'id'=>'bank_kv_[:KEY:]', 'onkeyup' => 'setBankSelect([:KEY:])']) }}
                            </div>

                        </div>
                    </div>



                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3" >
                    <h3>Порядок оплаты</h3>


                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row form-horizontal">

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                <label class="control-label">
                                    Сумма  <span style="color:red">*</span>
                                </label>
                                {{ Form::text('contract[[:KEY:]][payment][0][payment_total]', '', ['class' => 'form-control sum valid_fast_accept', 'id'=>'payment_total_[:KEY:]']) }}

                                <input type="hidden" name="contract[[:KEY:]][payment][0][payment_number]" value="1" id="payment_number_[:KEY:]" />
                                <input type="hidden" name="contract[[:KEY:]][payment][0][type_id]" value="0" />

                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label class="control-label" id="check_label_[:KEY:]">
                                    Вид оплаты
                                </label>
                                {{ Form::select('contract[[:KEY:]][payment][0][pay_method_id]', \App\Models\Finance\PayMethod::query()->where('is_actual', 1)->pluck('title', 'id'), 0, ['class' => 'form-control valid_fast_accept select2-ws', 'id'=>'check_[:KEY:]', 'data-key' => '[:KEY:]']) }}
                                <script>
                                    var pay_methods_id2types = {
                                        @foreach(\App\Models\Finance\PayMethod::query()->where('is_actual', 1)->get() as $method)
                                        {!! "$method->id : $method->key_type," !!}
                                        @endforeach
                                    };
                                </script>
                            </div>



                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row form-horizontal">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="receipt_block_[:KEY:]">
                                <label class="control-label"  id="bso_receipt_label_[:KEY:]">
                                    Квитанция
                                    <a href="javascript:void(0);" class="btn-xs btn-primary" data-create_receipt="[:KEY:]" style="display: inline">Е-квит</a>
                                </label>
                                {{ Form::text('contract[[:KEY:]][payment][0][bso_receipt]', '', ['class' => 'form-control valid_fast_accept temp_contract_receipt', 'id'=>'bso_receipt_[:KEY:]']) }}
                                <input type="hidden" name="contract[[:KEY:]][payment][0][bso_receipt_id]" id="bso_receipt_id_[:KEY:]" />
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="check_block_[:KEY:]" style="display: none">
                                <div class="row form-horizontal">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <label class="control-label">Телефон</label>
                                        {{ Form::text('contract[[:KEY:]][check_phone]', '', ['class' => 'form-control phone', 'id'=>'check_phone_[:KEY:]',]) }}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <label class="control-label">E-mail</label>
                                        {{ Form::text('contract[[:KEY:]][check_email]', '', ['class' => 'form-control email', 'id'=>'check_email_[:KEY:]']) }}
                                    </div>
                                </div>

                                <div class="row form-horizontal">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <label class="control-label">Отправить</label>
                                        {{ Form::select('contract[[:KEY:]][when_to_send]', collect(\App\Models\Api\Atol\AtolCheck::WHEN_TO_SEND), 1, ['class' => 'form-control', 'id'=>'when_to_send_[:KEY:]',]) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div style="display: none;">
                        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                            <label class="control-label">Алгоритм рассрочки</label>
                            {{ Form::select('contract[[:KEY:]][installment_algorithms_id]', collect([0=>'Укажите номер договора']), 0, ['class' => 'form-control select2-ws', 'id'=>'installment_algorithms_id_[:KEY:]', 'onchange' => 'setAlgos([:KEY:])']) }}
                        </div>
                        <div id='payment_algo_[:KEY:]' class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        </div>
                    </div>


                </div>




            </div>
        </div>
    </div>
</div>