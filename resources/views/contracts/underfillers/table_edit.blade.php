<table class="table-bordered table underfillers-table">
    <thead>
    <tr>
        <td>#</td>
        <td>Наименование полиса</td>
        <th>Дата приема</th>
        <th>Акцепт</th>

        <th>Страхователь</th>
        <th>Объект страхования</th>
        <th>Телефон</th>
        <th>Дата страхования (С)</th>
        <th>Дата страхования (По)</th>
        <th>Страховая премия</th>
        <th>Создатель акцепта</th>
        <th>Условие продажи</th>
        <th></th>
    </tr>
    </thead>
    <tbody>

    @if(sizeof($contracts))
        @foreach($contracts as $contract)
            <tr>
                <td>{{ $contract->id }}</td>
                <td>Полис {{$contract->product->title}} {{$contract->bso_title}}</td>
                <td>{{ setDateTimeFormatRu($contract->sign_date, 1)}}</td>
                <td>{{ \App\Models\Contracts\Contracts::KIND_ACCEPTANCE[$contract->kind_acceptance] }}</td>

                <th>{{ $contract->insurer->title }}</th>
                <th>{{ $contract->object_insurer->title }}</th>
                <th>{{ $contract->insurer->phone }}</th>
                <th>{{ setDateTimeFormatRu($contract->begin_date, 1)}}</th>
                <th>{{ setDateTimeFormatRu($contract->end_date, 1)}}</th>
                <th>{{ $contract->payment_total }}</th>
                <th>{{ $contract->check_user ? $contract->check_user->name : "" }}</th>
                <th>{{ \App\Models\Contracts\Contracts::SALES_CONDITION[$contract->sales_condition]}}</th>
                <th>
                    <a class="btn btn-success" href="/contracts/underfillers/edit/{{$contract->id}}">
                        <i class="fa fa-edit"></i>
                    </a>
                </th>
            </tr>

        @endforeach
    @else

    @endif


    </tbody>

</table>