@extends('layouts.app')


@section('js')

    @include('contracts.contract_templates.online_contracts.js')
    <script>

        $(function(){
            initWorkArea();
        });

        function closeContract(contract_id) {
            var res = myGetAjax("{{url("/contracts/underfillers/close_contract/")}}/"+contract_id);
            if(res.state == true){
                location.href = "{{url("/contracts/underfillers/")}}";
                return true;
            }
        }

        function getCalcData (id) {
            var data = {};

            $('.checked_risk:checked').each(function (index, item) {
                var key = $(this).val();
                var tr = $(this).closest('tr');
                data[key] = {};

                tr.find('input').each(function () {
                    data[key][$(this).data('name')] = $(this).val();
                })

            })
            data["calculate_insurance_sum"] = $('[name="calculate_insurance_sum"]').val();
            data["calculate_insurance_award"] = $('[name="calculate_insurance_award"]').val();

            return data;
        }

        function saveCalc(id) {

            var data = getCalcData(id);

            $.ajax({
                type: "POST",
                url: "{{url("/contracts/underfillers/save_calc/{$contract->id}")}}",
                async: false,
                data: data,
                success: function (response) {
                    // console.log(response)
                }
            });
        }

        //Сохраняем договор
        function saveContract(id) {

            loaderShow();
            saveCalc(id);
            if (isFunction('calcUnderfill')){
                calcUnderfill();
            }
            $.post('/contracts/online/underfiller_save/' + id, $('#product_form').serialize(), function (response) {

                if (Boolean(response.state) === true) {
                    flashMessage('success', "Данные успешно сохранены!");
                }else {
                    if(response.errors){
                        $.each(response.errors, function (index, value) {
                            flashHeaderMessage(value, 'danger');
                            $('[name="' + index + '"]').addClass('form-error');
                        });
                    }else{
                        flashHeaderMessage(response.msg, 'danger');
                    }

                }

            }).always(function () {
                loaderHide();
            });

            return true;
        }

        function closeAndSaveContract(id) {

            //Сохраняем договор
            loaderShow();
            saveCalc(id);
            if (isFunction('calcUnderfill')){
                calcUnderfill();
            }
            $.post('/contracts/online/underfiller_save/' + id, $('#product_form').serialize(), function (response) {

                if (Boolean(response.state) === true) {
                    flashMessage('success', "Данные успешно сохранены!");
                    var res = myGetAjax("{{url("/contracts/underfillers/close_contract/")}}/"+id);
                    if(res.state == true){
                        location.href = "{{url("/contracts/underfillers/")}}";
                        return true;
                    }
                }else {
                    if(response.errors){
                        $.each(response.errors, function (index, value) {
                            flashHeaderMessage(value, 'danger');
                            $('[name="' + index + '"]').addClass('form-error');
                        });
                    }else{
                        flashHeaderMessage(response.msg, 'danger');
                    }

                }

            }).always(function () {
                loaderHide();
            });

            return true;
        }

    </script>

@endsection



@section('work-area')
    @include("contracts.underfillers.partials.workarea", ['contract'=>$contract])
@endsection



@section('content')

    <div class="page-heading">
        <h1>Договор {{$contract->id}} (БСО: {{$contract->bso_title}})</h1>
    </div>



    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row form-horizontal" id="main_container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:15px;">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <!-- Информация по договору -->
                    @include("contracts.contract_templates.default.contract.view")
                    <!-- Платежи -->
                    @include("contracts.contract_templates.default.payments.view")

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <!-- Документы -->
                    @include("contracts.contract_templates.default.documents.edit")
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    @if(View::exists("contracts.underfillers.products.{$contract->product->slug}.main.edit"))
                        <form id="product_form">
                            @include("contracts.underfillers.products.{$contract->product->slug}.main.edit", ['view_type'=>'edit'])
                        </form>
                    @else
                        @include("contracts.underfillers.products.default.edit", [])
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection


