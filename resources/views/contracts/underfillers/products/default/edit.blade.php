<form name="contract_data" style="clear: both;" action="/contracts/underfillers/save/{{$contract->id}}" method="post">
    @csrf
    <div class="row">
{{--        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="block-view">
                <div>
                    <h2>Данные страхователя</h2>
                </div>
                <div class="form-group toggled-setting">
                    <div style="padding: 40px 20px;">
                        <div class="row">
                            <div class="col-sm-10">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Серия паспорта <span
                                                    class="required">*</span></label>
                                        @php
                                            if ($contract->insurer->doc_serie != '' && !old('serial_passport')){
                                                $fd = 'fake_disabled';
                                                $ro = 'readonly';
                                            }else{
                                                $fd = '';
                                                $ro = '';
                                            }
                                        @endphp
                                        {{Form::input('string', 'serial_passport', trim(old('serial_passport', $contract->insurer->doc_serie)) , ['class' => "form-control form-control-with-span $fd", $ro])}}
                                        @if(!empty($ro))
                                            <div style="padding-top: 5px; display: inline-block;">
                                                <span class="span-edit" data-name="serial_passport"
                                                      style="display: inline-block;"><i
                                                            class=" form-control-span-edit fa fa-pencil-square-o"
                                                            aria-hidden="true"></i></span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Номер <span class="required">*</span></label><br>
                                        @php
                                            if ($contract->insurer->doc_number != '' && !old('number_passport')){
                                                $fd = 'fake_disabled';
                                                $ro = 'readonly';
                                            }else{
                                                $fd = '';
                                                $ro = '';
                                            }
                                        @endphp
                                        {{Form::input('string', 'number_passport', trim(old('number_passport', $contract->insurer->doc_number)) , ['class' => "form-control form-control-with-span $fd", $ro])}}
                                        @if(!empty($ro))
                                            <div style="padding-top: 5px; display: inline-block;">
                                                <span class="span-edit" data-name="number_passport"
                                                      style="display: inline-block;"><i
                                                            class=" form-control-span-edit fa fa-pencil-square-o"
                                                            name="edit_number_passport" aria-hidden="true"></i></span>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">ИНН <span class="required">*</span></label><br>
                                        @php
                                            if ($contract->insurer->inn != '' && !old('inn')){
                                                $fd = 'fake_disabled';
                                                $ro = 'readonly';
                                            }else{
                                                $fd = '';
                                                $ro = '';
                                            }
                                        @endphp
                                        {{Form::input('string', 'inn', trim(old('inn', $contract->insurer->inn)), ['class' => "form-control form-control-with-span $fd", $ro])}}
                                        @if(!empty($ro))
                                            <div style="padding-top: 5px; display: inline-block;">
                                                <span class="span-edit" data-name="inn"
                                                      style="display: inline-block;"><i
                                                            class=" form-control-span-edit fa fa-pencil-square-o"
                                                            name="edit_inn" aria-hidden="true"></i></span>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">КПП <span class="required">*</span></label><br>
                                        @php
                                            if ($contract->insurer->kpp != '' && !old('kpp')){
                                                $fd = 'fake_disabled';
                                                $ro = 'readonly';
                                            }else{
                                                $fd = '';
                                                $ro = '';
                                            }
                                        @endphp
                                        {{Form::input('string', 'kpp', trim(old('kpp', $contract->insurer->kpp)) , ['class' => "form-control form-control-with-span $fd", $ro])}}
                                        @if(!empty($ro))
                                            <div style="padding-top: 5px; display: inline-block;">
                                                <span class="span-edit" data-name="kpp"
                                                      style="display: inline-block;"><i
                                                            class=" form-control-span-edit fa fa-pencil-square-o"
                                                            name="edit_kpp" aria-hidden="true"></i></span>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>--}}
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @include('contracts.contract_templates.online_contracts.subject.edit', [
                'subject_title' => 'Страхователь',
                'subject_name' => 'insurer',
                'subject' => (isset($contract->insurer)?$contract->insurer:new \App\Models\Contracts\Subjects()),
                'forUnderfill' => true
            ])
        </div>


        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="block-view">
                <div>
                    <h2>Объект страхования</h2>
                </div>
                <div class="form-group toggled-setting">
                    <div style="padding: 40px 20px;">
                        <div class="row">
                            <div class="col-sm-10">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Наиминование объекта</label>
                                        @php
                                            if ($contract->object_insurer && $contract->object_insurer->title != '' && !old('reg_number')){
                                                $fd = 'fake_disabled';
                                                $ro = 'readonly';
                                            }else{
                                                $fd = '';
                                                $ro = '';
                                            }
                                        @endphp
                                        {{Form::input('string', 'title', trim(old('reg_number', $contract->object_insurer ? $contract->object_insurer->title : '')) , ['class' => "form-control form-control-with-span $fd", $ro])}}
                                        @if(!empty($ro))
                                            <div style="padding-top: 5px; display: inline-block;">
                                                <span class="span-edit" data-name="title"
                                                      style="display: inline-block;"><i
                                                            class=" form-control-span-edit fa fa-pencil-square-o"
                                                            name="title" aria-hidden="true"></i></span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                {{--<div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Регистрационный номер <span
                                                    class="required">*</span></label>
                                        @php
                                            if ($contract->object_insurer_auto && $contract->object_insurer_auto->reg_number != '' && !old('reg_number')){
                                                $fd = 'fake_disabled';
                                                $ro = 'readonly';
                                            }else{
                                                $fd = '';
                                                $ro = '';
                                            }
                                        @endphp
                                        {{Form::input('string', 'reg_number', trim(old('reg_number', $contract->object_insurer_auto ? $contract->object_insurer_auto->reg_number : '')) , ['class' => "form-control form-control-with-span $fd", $ro])}}
                                        @if(!empty($ro))
                                            <div style="padding-top: 5px; display: inline-block;">
                                                <span class="span-edit" data-name="reg_number"
                                                      style="display: inline-block;"><i
                                                            class=" form-control-span-edit fa fa-pencil-square-o"
                                                            name="edit_inn" aria-hidden="true"></i></span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">VIN-номер</label>
                                        @php
                                            if ($contract->object_insurer_auto && $contract->object_insurer_auto->vin != '' && !old('vin')){
                                                $fd = 'fake_disabled';
                                                $ro = 'readonly';
                                            }else{
                                                $fd = '';
                                                $ro = '';
                                            }
                                        @endphp
                                        {{Form::input('string', 'vin', trim(old('vin', $contract->object_insurer_auto ? $contract->object_insurer_auto->vin : '')) , ['class' => "form-control form-control-with-span $fd", $ro])}}
                                        @if(!empty($ro))
                                            <div style="padding-top: 5px; display: inline-block;">
                                                <span class="span-edit" data-name="vin"
                                                      style="display: inline-block;"><i
                                                            class=" form-control-span-edit fa fa-pencil-square-o"
                                                            name="edit_inn" aria-hidden="true"></i></span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Год выпуска <span class="required">*</span></label>
                                        @php
                                            if ($contract->object_insurer_auto && $contract->object_insurer_auto->year_of_car != '' && !old('year_of_car')){
                                                $fd = 'fake_disabled';
                                                $ro = 'readonly';
                                            }else{
                                                $fd = '';
                                                $ro = '';
                                            }
                                        @endphp
                                        {{Form::input('integer', 'year_of_car', trim(old('year_of_car', $contract->object_insurer_auto ? $contract->object_insurer_auto->year_of_car : '')), ['class' => "form-control form-control-with-span $fd", $ro])}}
                                        @if(!empty($ro))
                                            <div style="padding-top: 5px; display: inline-block;">
                                                <span class="span-edit" data-name="year_of_car"
                                                      style="display: inline-block;"><i
                                                            class=" form-control-span-edit fa fa-pencil-square-o"
                                                            name="edit_inn" aria-hidden="true"></i></span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Марка авто <span class="required">*</span></label>
                                        @php
                                            if ($contract->object_insurer_auto && $contract->object_insurer_auto->mark_id != '0' && !old('mark_id')){
                                                $fd = 'fake_disabled';
                                                $ro = 'readonly';
                                            }else{
                                                $fd = '';
                                                $ro = '';
                                            }
                                        @endphp
                                        @if($contract->object_insurer_auto && $contract->object_insurer_auto->mark_id != '0' && !empty($contract->object_insurer_auto->mark_id))
                                            {{Form::select('mark_id', \App\Models\Vehicle\VehicleMarks::all()->pluck('title','id')->prepend('Не выбрано', ''), old('mark_id', $contract->object_insurer_auto->mark_id), ['class' => "form-control form-control-with-span $fd", 'onchange=loadModels()', $ro])}}
                                            @if(!empty($ro))
                                                <div style="padding-top: 5px; display: inline-block;">
                                                <span class="span-edit" data-name="mark_id"
                                                      style="display: inline-block;"><i
                                                            class=" form-control-span-edit fa fa-pencil-square-o"
                                                            name="edit_inn" aria-hidden="true"></i>
                                                </span>
                                                </div>
                                            @endif
                                        @else
                                            {{Form::select('mark_id', \App\Models\Vehicle\VehicleMarks::all()->pluck('title','id')->prepend('Не выбрано',''), old('mark_id', ''), ['class' => 'form-control form-control-with-span', 'onchange=loadModels()'])}}
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Модель</label><br>
                                        @php
                                            if ($contract->object_insurer_auto && $contract->object_insurer_auto->model_id != '0' && !old('model_id')){
                                                $fd = 'fake_disabled';
                                                $ro = 'readonly';
                                            }else{
                                                $fd = '';
                                                $ro = '';
                                            }
                                        @endphp
                                        @if($contract->object_insurer_auto && $contract->object_insurer_auto->model_id == '0' && $contract->object_insurer_auto->mark_id == '0')
                                            {{Form::select('model_id', \App\Models\Vehicle\VehicleModels::all()->pluck('title','id')->prepend('Не выбрано', 0), old('model_id', $contract->object_insurer_auto->model_id), ['class' => "form-control form-control-with-span $fd", $ro])}}
                                        @elseif($contract->object_insurer_auto && $contract->object_insurer_auto->model_id == '0' && $contract->object_insurer_auto->mark_id != '0')
                                            {{Form::select('model_id', \App\Models\Vehicle\VehicleModels::where('mark_id', '=', $contract->object_insurer_auto->mark_id)->pluck('title','id')->prepend('Не выбрано', 0), old('model_id', $contract->object_insurer_auto->model_id), ['class' => "form-control form-control-with-span $fd", $ro])}}
                                        @elseif($contract->object_insurer_auto && $contract->object_insurer_auto->model_id != '0' && $contract->object_insurer_auto->mark_id != '0')
                                            {{Form::select('model_id', \App\Models\Vehicle\VehicleModels::where('mark_id', '=', $contract->object_insurer_auto->mark_id)->pluck('title','id')->prepend('Не выбрано', 0), old('model_id', $contract->object_insurer_auto->model_id) , ['class' => "form-control form-control-with-span $fd", $ro])}}
                                            <div style="padding-top: 5px; display: inline-block;">
                                                <span class="span-edit" data-name="model_id"
                                                      style="display: inline-block;"><i
                                                            class=" form-control-span-edit fa fa-pencil-square-o"
                                                            name="edit_inn" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        @else
                                            {{Form::select('model_id', [0 => 'Не выбрано'], 0, ['class' => 'form-control form-control-with-span'])}}
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Мощность <span class="required">*</span></label>
                                        @php
                                            if ($contract->object_insurer_auto && $contract->object_insurer_auto->power != '' && $contract->object_insurer_auto->power != 0.00 && !old('year_of_car')){
                                                $fd = 'fake_disabled';
                                                $ro = 'readonly';
                                            }else{
                                                $fd = '';
                                                $ro = '';
                                            }
                                        @endphp
                                        {{Form::input('string', 'power', old('power', $contract->object_insurer_auto ? $contract->object_insurer_auto->power : ''), ['class' => "form-control form-control-with-span $fd", $ro])}}
                                        @if(!empty($ro))
                                            <div style="padding-top: 5px; display: inline-block;">
                                                <span class="span-edit" data-name="power"
                                                      style="display: inline-block;"><i
                                                            class=" form-control-span-edit fa fa-pencil-square-o"
                                                            name="edit_inn" aria-hidden="true"></i></span>
                                            </div>
                                        @endif
                                    </div>
                                </div>--}}

                                {{--<div class="col-sm-12">
                                    <div class="form-group">
                                        @if(!$contract->underfiller_check)
                                            <div class="form-group">
                                                <label class="control-label pull-left" style="margin-right: 10px;">Скрыть
                                                    из списка</label>
                                                {{ Form::checkbox('underfiller_check', 1, 1, ['class' => 'middle-checkbox']) }}
                                            </div>
                                        @endif
                                    </div>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <button type="submit" id="save" class="btn btn-success pull-right">Сохранить</button>
    </div>
</form>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div id="table"></div>
    </div>
</div>

<script>

    var PAGE = 1;

    function setPage(field) {
        PAGE = field;
        loadItems();
    }

    $(document).on("click", ".span-edit", function () {
        var dataName = $(this).attr('data-name');
        var state = $('[name = "' + dataName + '"]').attr('readonly');

        if (state) {
            $('[name = "' + dataName + '"]')
                .removeAttr('readonly')
                .removeClass('fake_disabled');
        } else {
            $('[name = "' + dataName + '"]')
                .attr('readonly', 'readonly')
                .addClass('fake_disabled');
        }
    });


    function loadModels() {

        var mark = $('[name = "mark_id"]').val();
        $.get("/contracts/underfillers/get_models", {'mark': mark}, function (req) {
            $('[name = "model_id"]').html('<option value="0" selected="selected">Не выбрано</option>');
            for (var model in req) {
                $('[name = "model_id"]').append('<option value=' + req[model].id + '>' + req[model].title + '</option>');
            }
        });
    }
</script>