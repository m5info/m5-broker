{{--Срок и период страхования--}}
@include('contracts.contract_templates.online_contracts.terms.edit')

{{--Участники договора--}}
{{--Страхователь--}}
@include('contracts.contract_templates.online_contracts.subject.edit', [
    'subject_title' => 'Страхователь',
    'subject_name' => 'insurer',
    'subject' => (isset($contract->insurer)?$contract->insurer:new \App\Models\Contracts\Subjects()),
   'forUnderfill' => true
])

{{--Собственник--}}
@include('contracts.contract_templates.online_contracts.subject.edit', [
   'subject_title' => 'Собственник',
   'subject_name' => 'owner',
   'is_insurer' => (isset($contract->owner) && isset($contract->insurer) && $contract->insurer == $contract->owner)?1:0,
   'subject' => (isset($contract->owner)?$contract->owner:new \App\Models\Contracts\Subjects()),
      'forUnderfill' => true
])

{{--Водители--}}
@include('contracts.contract_templates.online_contracts.drivers.edit', [
    'drivers' => $contract->drivers,
    'contract' => $contract
])


{{--Транспортное средство--}}
@include('contracts.contract_templates.online_contracts.insurance_object.auto.osago.edit', [
    'object'=>(isset($contract->object_insurer_auto))?$contract->object_insurer_auto:new \App\Models\Contracts\ObjectInsurerAuto()
])

@include('contracts.underfillers.partials.osago_calculate', ['contract' => $contract])



<script src="/plugins/jquery/jquery.min.js"></script>
@include('contracts.contract_templates.online_contracts.products.osago.js')
@include('contracts.underfillers.products.eosago.js')


