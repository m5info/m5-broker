@php
    $calcArr = (object)[];

    if($underfillers_calculation = $contract->underfillers_calculation){
        if(isset($underfillers_calculation->json_calculate)){
            $calcArr = json_decode($underfillers_calculation->json_calculate);
        }
    }
@endphp
<div class="block-view">

    <h3>Платежная информация</h3>
    <div class="row" style="padding: 0 20px 0 20px;">
        <div class="col-lg-12">
            <div class="row form-horizontal" style="padding-top:5px;margin-top:5px;">
            </div>

            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        <th>Стр. риски</th>
                        <th>Стр. сумма</th>
                        <th>Франшиза</th>
                        <th>Стр. премия</th>
                        <th>Страх. тариф</th>
                        <th>Расч. тариф</th>
                        <th>Расч. премия</th>
                        <th>Скидка <br> (%)</th>
                        <th>Сумма с <br> неоф. <br> скидкой</th>
                        <th>Неоф. <br> скидка <br> (%)</th>
                        <th>Примечание</th>
                    </tr>
                </thead>
                <tbody>
                @foreach(App\Models\Contracts\UnderfillersCalculations::KASKO_PRODUCTS as $key => $risk)
                    <tr>
                        <td>{{ Form::checkbox("calculate[$key]", $key, isset($calcArr->{$key}->checkbox) ? true : false, ['data-name' => 'checkbox','class' => 'checked_risk']) }}</td>
                        <td>{{$risk}}</td>
                        <td>{{ Form::text("calculate[$key][insurance_sum]", isset($calcArr->{$key}->insurance_sum) ? $calcArr->{$key}->insurance_sum : '',['data-name' => 'insurance_sum', 'class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text("calculate[$key][franchise]", isset($calcArr->{$key}->franchise) ? $calcArr->{$key}->franchise : '',['data-name' => 'franchise', 'class' => 'calc-koeff form-control']) }}</td>
                        @if(isset($calcArr->{$key}->insurance_award))
                            <td>{{ Form::text("calculate[$key][insurance_award]", $calcArr->{$key}->insurance_award,['data-name' => 'insurance_award', 'class' => 'calc-koeff form-control']) }}</td>
                        @else
                            <td>{{ Form::text("calculate[$key][insurance_award]", $key==1 ? $contract->payment_total : '',['data-name' => 'insurance_award', 'class' => 'calc-koeff form-control']) }}</td>
                        @endif
                        <td>{{ Form::text("calculate[$key][insurance_rate]", isset($calcArr->{$key}->insurance_rate) ? $calcArr->{$key}->insurance_rate :'',['data-name' => 'insurance_rate', 'class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text("calculate[$key][calc_rate]", isset($calcArr->{$key}->calc_rate) ? $calcArr->{$key}->calc_rate :'',['data-name' => 'calc_rate', 'class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text("calculate[$key][insurance_premium]", isset($calcArr->{$key}->insurance_rate) ? $calcArr->{$key}->insurance_rate :'',['data-name' => 'insurance_rate', 'class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text("calculate[$key][discount]", isset($calcArr->{$key}->discount) ? $calcArr->{$key}->discount :'',['data-name' => 'discount', 'class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text("calculate[$key][informal_discount_sum]", isset($calcArr->{$key}->informal_discount_sum) ? $calcArr->{$key}->informal_discount_sum :'',['data-name' => 'informal_discount_sum', 'class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text("calculate[$key][informal_discount]", isset($calcArr->{$key}->informal_discount) ? $calcArr->{$key}->informal_discount :'',['data-name' => 'informal_discount', 'class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text("calculate[$key][comment]", isset($calcArr->{$key}->comment) ? $calcArr->{$key}->comment :'',['data-name' => 'comment', 'class' => 'calc-koeff form-control']) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="row" style="padding: 0 20px 0 20px;">
                <label class="col-lg-6 control-label">Страховая сумма</label>
                <div class="col-lg-4">
                    {{ Form::text('calculate_insurance_sum', isset($calcArr->calculate_insurance_sum) ? $calcArr->calculate_insurance_sum :'',['class' => 'form-control']) }}
                </div>
            </div>
            <div class="row" style="padding: 0 20px 0 20px;">
                <label class="col-lg-6 control-label">Страховая премия по полису</label>
                <div class="col-lg-4">
                    {{ Form::text('calculate_insurance_award', isset($calcArr->calculate_insurance_award) ? $calcArr->calculate_insurance_award : $contract->payment_total,['class' => 'form-control']) }}
                </div>
            </div>
        </div>

    </div>
</div>
<style>
    table td, table th {
        text-align: center;
    }
</style>
