@php
    $calcArr = (object)[];

    if($underfillers_calculation = $contract->underfillers_calculation){
        if(isset($underfillers_calculation->json_calculate)){
            $calcArr = json_decode($underfillers_calculation->json_calculate);
        }
    }
@endphp
<div class="block-view">
    <h3>Платежная информация</h3>
    <div class="row" style="padding: 0 20px 0 20px;">
        <div class="col-lg-12">
            <div class="row form-horizontal" style="padding-top:5px;margin-top:5px;">
            </div>

            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Базовая ставка</th>
                        <th colspan="8">Коэффициент</th>
                        <th>Итого расчет</th>
                    </tr>
                    <tr>
                        <th>Базовая ставка</th>
                        <th>Территории преимущественного <br>использования ТС</th>
                        <th>Наличия/отсутствия страховых <br>выплат</th>
                        <th>Возраста и стажа водителя</th>
                        <th>Сезонного использования ТС</th>
                        <th>Краткосрочного страхования</th>
                        <th>Мощности легкового автомобиля</th>
                        <th>Применяемый при грубых нарушениях страхования</th>
                        <th>Коэфициент неограниченных водителей</th>
                        <th>Итого расчет</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ Form::text('base_rate', isset($calcArr->base_rate) ? $calcArr->base_rate : '',['class' => 'form-control']) }}</td>
                        <td>{{ Form::text('territory', isset($calcArr->territory) ? $calcArr->territory : '',['class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text('has_insurance_pays', isset($calcArr->has_insurance_pays) ? $calcArr->has_insurance_pays : '',['class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text('exp_age_driver', isset($calcArr->exp_age_driver) ? $calcArr->exp_age_driver : '',['class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text('season_using_ts', isset($calcArr->season_using_ts) ? $calcArr->season_using_ts : '',['class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text('short_time_insurance', isset($calcArr->short_time_insurance) ? $calcArr->short_time_insurance : '',['class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text('power_ts', isset($calcArr->power_ts) ? $calcArr->power_ts : '',['class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text('gross_violations', isset($calcArr->gross_violations) ? $calcArr->gross_violations : '',['class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text('multidrive', isset($calcArr->multidrive) ? $calcArr->multidrive : '',['class' => 'calc-koeff form-control']) }}</td>
                        <td>{{ Form::text('calc', isset($calcArr->calc) ? $calcArr->calc : '',['class' => 'form-control']) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div style="width:50px;padding-top:10px;">
                <a href="javascript:void(0);" class="btn btn-primary inline-block" title="Рассчитать"
                   onclick="calcUnderfill();">
                    Рассчитать</a>
            </div>
        </div>
    </div>
</div>
<style>
    table td, table th {
        text-align: center;
    }
</style>
<script>

    function calcUnderfill() {

        var base_rate = parseFloat($('[name="base_rate"]').val().replace(',', '.'));
        var saveData = {};
        saveData['base_rate'] = base_rate;

        $('.calc-koeff').each(function (index, item) {

            if ($(this).val().length > 0){
                base_rate *= parseFloat($(this).val().replace(',', '.'));

                saveData[$(this).attr('name')] = parseFloat($(this).val().replace(',', '.'));
            }else{
                base_rate *= 1;
            }
        });
        
        $('[name="calc"]').val(base_rate.toFixed(2));
        saveData['calc'] = base_rate.toFixed(2);

        saveCalcUnderfill(saveData);
    }

    function saveCalcUnderfill(data) {

        $.ajax({
            type: "POST",
            url: "{{url("/contracts/underfillers/save_calc/{$contract->id}")}}",
            async: false,
            data: data,
            success: function (response) {
                // console.log(response)
            }
        });
    }

</script>