<div class="work-area-group">
    <h3>Общие</h3>
    <div class="work-area-item">
        @if($contract->underfiller_check)
            <a href="/contracts/underfillers/complete_polices" class="btn btn-primary">Назад</a>
        @else
            <a href="/contracts/underfillers/" class="btn btn-primary">Назад</a>
        @endif
    </div>

    @if(in_array($contract->product->slug, ['osago', 'kasko', 'eosago']))
        <hr>
        <div class="work-area-item">
            <span class="btn btn-success" onclick="saveContract({{$contract->id}});">
                Сохранить
            </span>
        </div>
    @endif


    @if($contract->underfiller_check != 1)
        @if(in_array($contract->product->slug, ['osago', 'kasko', 'eosago']))
            <hr>
            <div class="work-area-item">
                <span class="btn btn-primary" onclick="closeAndSaveContract({{$contract->id}});">
                    Завершить
                </span>
            </div>
        @else
            <hr>
            <div class="work-area-item">
                <span class="btn btn-primary" onclick="closeContract({{$contract->id}});">
                    Завершить
                </span>
            </div>
        @endif
    @endif

</div>


