@extends('layouts.app')

@section('content')


    <div class="page-heading">
        @if(strpos(Request::url(), 'complete_polices'))
            <h1 class="inline-h1 pull-left">Дозаполнение полисов (завершенные)</h1>
            <a href="/contracts/underfillers/" class="btn btn-primary btn-inline pull-right"
               style="max-width: 200px;">Назад</a>
        @else
            <h1 class="inline-h1 pull-left">Дозаполнение полисов</h1>
            <a href="/contracts/underfillers/complete_polices" class="btn btn-primary btn-inline pull-right"
               style="max-width: 230px;">Завершенные полисы</a>
        @endif

    </div>

    <div class="block-main">
        <div class="block-sub">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="filter-group" id="filters">
                        <div class="col-sm-4">
                            <h2>Период</h2>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="control-label"></label>
                                    <h4>Дата приема</h4>
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">С</label>
                                    {{ Form::text('date_from', '', ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off', 'onchange'=>'loadItems()']) }}
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">По</label>
                                    {{ Form::text('date_to', '', ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off', 'onchange'=>'loadItems()']) }}
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-8">
                            <h2>Детали</h2>
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="col-sm-12">
                                        <label class="control-label"></label>
                                        <h4> &nbsp;</h4>
                                    </div>
                                    <label class="control-label" for="bso_title">БСО</label>
                                    {{ Form::text('bso_title', request('bso_title', ''), ['class' => 'form-control',  'onkeyup'=>'loadItems()']) }}
                                </div>

                            </div>
                        </div>
                        <div class="btn-group col-xs-12 col-sm-3 col-md-3 col-lg-3">

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 table-responsive">
        <div class="row">
            <div id="table"></div>
        </div>
    </div>

    @include('_chunks/_pagination',['callback'=>'loadItems'])

@endsection

@section('js')

    <script>


        $(function () {

            loadItems();

            $(document).on('click', '#export_accepts', function () {
                var data = getData();
                var query = $.param({method: 'Contracts\\Accepted\\AcceptedController@get_table', param: data});
                location.href = '/exports/table2excel?' + query;
            });
        });



        function goto() {
            location.href = $(this).attr('href');
        }


        function getData() {
            return {

                bso_title: $('[name="bso_title"]').val(),
                date_from: $('[name="date_from"]').val(),
                date_to: $('[name="date_to"]').val(),
                page_count: $('[name="page_count"]').val(),
                PAGE: PAGE,
            }
        }


        function loadItems() {



            $('#page_list').html('');
            $('#table_row').html('');
            $('#view_row').html(0);
            $('#max_row').html(0);

            loaderShow();

            var _path = '';

            if (location.href.includes('complete_polices') == false) {
                _path = '/contracts/underfillers/get_table';
            } else {
                _path = '/contracts/underfillers/get_complete_polices_table';
            }

            $.get(_path, getData(), function (table_res) {

                $('#table').html(table_res.html);

                $('#view_row').html(table_res.view_row);
                $('#max_row').html(table_res.max_row);

                ajaxPaginationUpdate(table_res.page_max,loadItems);


            }).always(function () {
                loaderHide();
            });


            loaderHide();

        }

    </script>


@endsection