{{ Html::script("https://js.pusher.com/5.0/pusher.min.js") }}

<script>


    //Pusher.logToConsole = true;

    var pusher = new Pusher('{{ config('broadcasting.connections.pusher.key') }}', {
        cluster: 'eu',
        forceTLS: true
    });



</script>


@yield('pusher')
