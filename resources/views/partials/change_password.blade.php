@extends('layouts.frame')
@section('title')
    <h2 class="text-center">Изменить пароль</h2>
@endsection
@section('content')

    <form action="/change_password" id="change_password_form" method="post">
        @csrf
        <div class="row">
            <div class="col-sm-3">
                <label for="" class="pull-right">Введите свой текущий пароль</label><br>
            </div>
            <div class="col-sm-9">
                <input type="password" required class="form-control" name="old_password">
            </div>
            <div class="col-sm-3">
                <label for="">Введите новый пароль</label><br>
            </div>
            <div class="col-sm-9">
                <input type="password" required class="form-control" name="new_password">
            </div>
            <div class="col-sm-3">
                <label for="">Повторите новый пароль</label><br>
            </div>
            <div class="col-sm-9">
                <input type="password" required class="form-control" name="new_password_repeat">
            </div>

            <div class="clearfix"></div>
            <br>
            <div>
                <br>
                <label id="_errors" style="float: left; margin-left: 30px;font-size: 20px;"></label>
                <button type="submit" id="submit" class="btn btn-success inline-block pull-right" style="margin-right: 15px;">
                    Подтвердить
                </button>
            </div>

        </div>
    </form>
@endsection
@section('js')
    <script>
        $(document).ready(function(){
            $(document).on('click', '#submit', function(e){
                e.preventDefault();

                data = $('#change_password_form').serialize();

                res = JSON.parse(myPostAjax('/change_password/', data));

                if (res){
                    $('#_errors').html(res.message);
                    if(res.error !== 0){
                        $('#_errors').css('color', 'red');
                    }else{
                        $('#_errors').css('color', 'green');

                        setTimeout(function(){
                            parent.reload()
                        }, 2000);
                    }
                }
            });
        });
    </script>
@endsection