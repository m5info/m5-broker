@if(!$transactions->isEmpty())
    <table class="tov-table">
        <thead>
        <tr>
            <th>#</th>
            <th>Тип</th>
            <th>Вид</th>
            <th>Сумма</th>
            <th>Сальдо</th>
            <th>Дата</th>
            <th>Описание</th>
        </tr>
        </thead>
        <tbody>


        @foreach($transactions as $trans)
            <tr>
                <td>{{$trans->id}}</td>
                <td>{{\App\Models\Users\UsersBalance::EVENT_TYPE_ID[$trans->event_type_id]}}</td>
                <td>{{\App\Models\Users\UsersBalance::TYPE_ID[$trans->type_id]}}</td>

                <td style="white-space: nowrap;">{{titleFloatFormat($trans->total_sum)}}</td>
                <td style="white-space: nowrap;">{{titleFloatFormat($trans->residue)}}</td>

                <td>{{setDateTimeFormatRu($trans->event_date)}}</td>
                <td>
                @if($trans->invoice_id)
                    @if(auth()->user()->is('cashier'))
                        @php
                            $url = url("cashbox/invoice/{$trans->invoice_id}/edit");
                        @endphp
                    @else
                        @php
                            $url = url("finance/invoice/invoices/{$trans->invoice_id}/edit");
                        @endphp
                    @endif
                    <a href="{{$url}}">{!! $trans->purpose_payment !!}</a>
                @else
                    {!! $trans->purpose_payment !!}
                @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <h4 style="margin-left: 15px; margin-top: 25px;">Не найдено ни одной транзации..</h4>
@endif