@php $userRolePermissions = auth()->user()->getAllPermissionArray() @endphp

<ul class="list-settings components">
    @foreach($menuItems as $groupTitle => $groupMenuItems)
        @if(is_array($groupMenuItems))
            @if(isset($userRolePermissions[$groupTitle]))
                <li>
                    <a href="#submenu-{{$groupTitle}}" class="list-settings-item" @if(is_array($menuItems[$groupTitle]['links'])) data-toggle="collapse" aria-expanded="false" @endif>
                        <span class="list-settings-item-ico {{$menuItems[$groupTitle]['ico']}}"></span>

                        {{ trans('menu.' . $menuItems[$groupTitle]['form_title']) }}
                    </a>
                    <ul class="collapse @if(auth()->user()->settings && auth()->user()->settings['menu_section'])in @endif list-unstyled" id="submenu-{{$groupTitle}}">
                    @foreach($menuItems[$groupTitle]['links'] as $menuItem => $menuItemTitle)
                        @php
                        $menuVisible = true;
                        if(auth()->user()->role->id == 3 && $menuItemTitle['name'] == 'temp_contracts'){
                         $menuVisible = false;
                        }
                        @endphp
                        @if(isset($userRolePermissions[$groupTitle][$menuItemTitle['name']]) && $menuVisible)
                            <li>
                                <a href="{{ url($groupTitle . "/" . $menuItemTitle['link']) }}/" class="list-settings-item sub-item">
                                    <span class="list-settings-item-ico {{$menuItemTitle['ico']}}"></span>
                                    {{ trans('menu.'.$menuItemTitle['name']) }}
                                </a>
                            </li>
                        @endif

                    @endforeach

                        @if($menuItems[$groupTitle]['form_button_link']!='' && FALSE && FALSE && FALSE)
                            <a href="{{url($menuItems[$groupTitle]['form_button_link'])}}" class="btn btn-primary list-settings-btn {{$menuItems[$groupTitle]['form_button_class']}}">

                                {{ trans($menuItems[$groupTitle]['form_button_name']) }}
                            </a>
                        @endif
                    </ul>


                </li>
            @endif
        @endif
    @endforeach
</ul>
