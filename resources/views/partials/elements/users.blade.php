@php
    $params = [
        'class' => 'form-control select2 '.(isset($class)?$class:''),
        'id'=>(isset($title)?$title:'agent_id'),
    ];

    if(isset($array)){
        $params = array_merge($params, $array);
    }
@endphp

{{ Form::select((isset($title)?$title:'agent_id'), \App\Models\User::getALLUserWhere()->pluck('name', 'id')->prepend('Нет', 0), (isset($select)?$select:auth()->id()), $params ) }}