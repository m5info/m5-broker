<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">

    <div class="row form-group">
        @if(auth()->user()->role->rolesVisibility(7)->visibility != 2)
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <label class="control-label">Агент</label>
                @php($agents_select = \App\Models\User::getALLUserWhere()->get()->pluck('name', 'id')->prepend('Все', 0))
                @php($agent_selected = request('agent_id') ? request()->query('agent_id') : session('finance.agent_id', auth()->id()))
                {{ Form::select('agent_id', $agents_select, $agent_selected, ['class' => 'form-control select2 ', 'id'=>'agent_id', 'required', 'onchange' => 'loadItems()']) }}
            </div>
        @else
            {{Form::hidden('agent_id', auth()->user()->id)}}
        @endif
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
            <label class="control-label">Тип</label>
            @php($type_select = collect(['cash'=>'Наличные', 'cashless'=>'Безналичные Брокер', 'card'=>'Безналичные Карта', 'sk' =>'СК'])->prepend('Все', ''))
            @php($type_selected = request('type') ? request()->query('type') : 0)
            {{ Form::select('type', $type_select, $type_selected, ['class' => 'form-control select2-all', 'id'=>'type', 'required', 'onchange' => 'loadItems()']) }}
        </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
            <label class="control-label">Статус</label>
            @php($status_select = collect(\App\Models\Finance\Invoice::STATUSES)->prepend('Все', ''))
            @php($status_selected = request('status_id') ? request()->query('status_id') : 1)
            {{ Form::select('status_id', $status_select, $status_selected, ['class' => 'form-control select2-all', 'id'=>'status_id', 'required', 'onchange' => 'loadItems()']) }}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
            <a class="btn btn-success btn-right" id="export_many" href="#">Печать</a>
        </div>
    </div>
</div>

<div id="table"></div>

@include('_chunks/_pagination',['class'=>'pull-right','callback'=>'loadItems'])


<script>

    $(function () {
        $(document).on('click', '[name="all_payments"]', function(){
            var checked = $(this).prop('checked');
            $('[name*="payment["]').prop('checked', checked).change();
        });

        $(document).on('click', '[name*="payment["]', function(){
            var all_checked = true;
            $.each($('[name*="payment["]'), function(k,v){
                all_checked = all_checked && $(v).prop('checked');
            });
            $('[name="all_payments"]').prop('checked', all_checked).change();
        });


        loadItems();
    });

    $(document).on('click', '#export_many', function () {
        var query = $.param(
            getData()
        );
        location.href = '/finance/invoice/invoices/export_many?' + query;
    });

    function loadItems(reset_page = false){
        loaderShow();

        var data = getData();

        $('#page_list').html('');
        $('#table_row').html('');
        $('#view_row').html(0);
        $('#max_row').html(0);

        $.post("{{url("/finance/invoice/invoices/get_invoices_table")}}", data, function (response) {

            $('#table').html(response.html);
            $('#view_row').html(response.view_row);
            $('#max_row').html(response.max_row);

            ajaxPaginationUpdate(response.page_max, loadItems);

            $(document).on('click', '.clickable-row', function(){
                location.href = $(this).data('href');
            })

        }).fail(function(){
            $('#table').html('');
        }).always(function() {
            loaderHide();
        });
    }

    function getData(){
        return {
            type: $('[name="type"]').val(),
            status_id: $('[name="status_id"]').val(),
            page_count: $('[name="page_count"]').val(),
            agent_id: $('[name="agent_id"]').val(),
            invoice_number: $('[name="invoice_number"]').val(),

            PAGE: PAGE,
        }
    }

</script>