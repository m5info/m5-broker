<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>№</th>
        <th>СК</th>
        <th>Страхователь</th>
        <th>Продукт</th>
        <th>Тип платежа форм.</th>
        <th>Тип платежа факт.</th>
        <th>Номер договора</th>
        <th>Квитанция</th>
        <th>Сумма</th>
        <th>КВ агента, %</th>
        <th>КВ агента, руб</th>
        <th>К оплате</th>
    </tr>
    </thead>
    <tbody>
    @php($all_total = 0)
    @if(sizeof($invoice->payments))
        @foreach($invoice->payments as $payment)
            @php($agent_total = $payment->getPaymentAgentSum())
            @php($all_total += $agent_total)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $payment->bso && $payment->bso->insurance ? $payment->bso->insurance->title : "" }}</td>
                <td>{{ $payment->Insurer}}</td>
                <td>{{ $payment->bso && $payment->bso->product ? $payment->bso->product->title : ""}}</td>
                <td>{{ $invoice->type_invoice_payment_id && collect(\App\Models\Finance\Invoice::TYPE_INVOICE_PAYMENT)->has($invoice->type_invoice_payment_id) ? \App\Models\Finance\Invoice::TYPE_INVOICE_PAYMENT[$invoice->type_invoice_payment_id] : '?'}}</td>
                <td>{{ $payment->type_ru() }}</td>
                <td>
                    @if(($payment->contract->kind_acceptance) == 1)
                        <a href="{{ url('bso/items', $payment->bso->id) }}"
                           target="_blank">{{ $payment->bso ? $payment->bso->bso_title : "" }}</a>
                    @else
                        <span title="Не безусловный">{{ $payment->bso ? $payment->bso->bso_title : "" }}</span>
                    @endif
                </td>

                <td>{{ $payment->bso_receipt ? : "" }}</td>
                <td>{{ getPriceFormat($payment->payment_total) }}</td>
                <td>{{ $payment->financial_policy_kv_agent }}</td>
                <td>{{ getPriceFormat($payment->financial_policy_kv_agent_total) }}</td>
                <td>{{ getPriceFormat($agent_total) }}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="10" class="text-right"></td>
            <td>Итог:</td>
            <td><b>{{getPriceFormat($all_total)}}</b></td>
            <td></td>
        </tr>
    @endif
    </tbody>
</table>