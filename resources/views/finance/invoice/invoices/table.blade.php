<div class="div_tooltip"></div>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Номер</th>
            <th>Создан</th>
            <th>Юр. лицо</th>
            <th>Тип счёта</th>
            <th>Поток</th>
            <th>Агент</th>
            <th>Статус</th>
            <th>Кол-во платежей</th>
            <th>Общая сумма</th>

        </tr>
    </thead>
    <tbody>
        @if(sizeof($invoices))
            @foreach($invoices as $invoice)
                <tr class="clickable-row invoice_row" id="{{ $invoice->id }}" data-href="{{url("/finance/invoice/invoices/{$invoice->id}/edit")}}">
                    <td> <div class="glyphicon glyphicon-new-window"></div>
                        <div id="bso_tooltip_{{ $invoice->id }}" class="bso_tooltip">
                            @php
                                foreach($invoice->payments as $payment){

                                    $bso_title = $payment->bso ? $payment->bso->bso_title : "";
                                    $bso_product = $payment->bso && $payment->bso->product ? $payment->bso->product->title : "";

                                    if(($payment->contract->kind_acceptance) == 1){
                                        echo "<a href=".url('bso/items', $payment->bso->id)." target='_blank'>$bso_title</a> $bso_product<br>";
                                    }else{
                                        echo "<span title='Не безусловный'>$bso_title</span> $bso_product<br>";
                                    }
                                }
                            @endphp
                        </div>
                        {{ $invoice->id }}
                    </td>
                    <td>{{ setDateTimeFormatRu($invoice->created_at)  }}</td>
                    <td>{{ $invoice->org ? $invoice->org->title : "" }}</td>
                    <td>{{ $invoice->types_ru('type')  }}</td>
                    <td>{{ $invoice->payments->first() ? $invoice->payments->first()->payment_flow_ru('payment_flow') : "" }}</td>
                    <td>{{ $invoice->agent ? $invoice->agent->name : "" }}</td>
                    <td>{{ $invoice->statuses_ru('status_id') }}</td>
                    <td>{{ $invoice->payments->count() }}</td>
                    <td>{{ getPriceFormat($invoice->payments->sum('payment_total')) }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9" class="text-center">Нет доступных счетов</td>
            </tr>
        @endif
    </tbody>
</table>

<script>
    var mouse = {};
    $('body').on( "mousemove", function( event ) {
        mouse.x = event.pageX ;
        mouse.y = event.pageY ;
        console.log();
    });
    $('tr.invoice_row td:first-child').on("mouseover", function() {

        var id = $(this).parent('tr').attr('id');
        console.log(id);

        if($('#bso_tooltip_'+ id).html() != '')
            $('.div_tooltip').html($('#bso_tooltip_'+ id).clone());
        else
            $('.div_tooltip').html('<span>Пусто</span>');

        $('.div_tooltip').show().css('top',mouse.y - $(document).scrollTop()-5).css('left',mouse.x-5);
    }).on("mouseout", function() {

        var id =  $(this).parent('tr').attr('id');
        $('.div_tooltip').hide();
    });
</script>