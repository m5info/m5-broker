@if($invoice->status_id == 1)
    <div class="work-area-group">
        <h3>Общее</h3>
        {{--
        <div class="work-area-item">
            <div class="work-area-item">
                <a href="javascript:void(0);" class="btn btn-primary" onclick="$('#invoice_form').submit()">{{ trans('form.buttons.save') }}</a>
            </div>
        </div>
        --}}
        <div class="work-area-item">
            <div class="work-area-item">
                <a href="javascript:void(0);" class="btn btn-danger" onclick="delete_invoice()">Расформировать счёт</a>
            </div>
        </div>
    </div>
@endif

<div class="work-area-group">
    <h3>Выгрузки</h3>

    @if(auth()->user()->is('courier') || auth()->user()->is('agent') || auth()->user()->is('manager'))
    <div class="work-area-item">
        <div class="work-area-item">
            <a class="btn btn-success doc_export_btn" href="/finance/invoice/invoices/{{$invoice->id}}/act_export">Акт сдачи в кассу</a>
        </div>
    </div>
    @endif

    @if(auth()->user()->is('under'))
        @if($invoice->type == 'cash' || $invoice->type == 'cashless_sk')
            <div class="work-area-item">
                <div class="work-area-item">
                    <a class="btn btn-success doc_export_btn" href="/finance/invoice/invoices/{{$invoice->id}}/act_for_pay_cash">Акт на опл. нал</a>
                </div>
            </div>
        @elseif($invoice->type == 'cashless' || $invoice->type == 'card')
            <div class="work-area-item">
                <div class="work-area-item">
                    <a class="btn btn-success doc_export_btn" href="/finance/invoice/invoices/{{$invoice->id}}/act_for_pay_cashless">Акт на опл. безнал</a>
                </div>
            </div>
        @elseif($invoice->type == 'sk')
            <div class="work-area-item">
                <div class="work-area-item">
                    <a class="btn btn-success doc_export_btn" href="/finance/invoice/invoices/{{$invoice->id}}/act_for_pay_cashless_card_broker">Акт на опл. (Карта Брокер)</a>
                </div>
            </div>
        @else
            <div class="work-area-item">
                <div class="work-area-item">
                    <a class="btn btn-success doc_export_btn" href="/finance/invoice/invoices/{{$invoice->id}}/act_for_pay_cash">Акт на опл. нал</a>
                </div>
            </div>
            <div class="work-area-item">
                <div class="work-area-item">
                    <a class="btn btn-success doc_export_btn" href="/finance/invoice/invoices/{{$invoice->id}}/act_for_pay_cashless">Акт на опл. безнал</a>
                </div>
            </div>
            <div class="work-area-item">
                <div class="work-area-item">
                    <a class="btn btn-success doc_export_btn" href="/finance/invoice/invoices/{{$invoice->id}}/act_for_pay_cashless_card_broker">Акт на опл. (Карта Брокер)</a>
                </div>
            </div>
        @endif
    @endif

</div>