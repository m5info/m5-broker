@extends('layouts.app')


@section('work-area')
    @include("finance.invoice.invoices.workarea")
@append

@section('content')

    <div class="page-heading" style="float: left">
        <h2 class="inline-h1">Счёт {{ $invoice->id }} &nbsp;</h2>

    </div>
    <div class="arrive-button" style="margin-top: -10px;">

        <a class="btn btn-success btn-left " href="/finance/invoice/invoices/{{$invoice->id}}/direction"
           target="_blank">Направление</a>

        @if($invoice->status_id == 1)
            <span class="btn btn-danger pull-right" onclick="delete_invoice()">Расформировать счёт</span>
        @endif

    </div>
    <div class="clear"></div>
    <br/>

    <style>


        table.table tr {
            line-height: 30px;
        }

        .btn-sm {
            cursor: pointer;
        }

        .arrive-button {
            margin-top: -7px;
        }
    </style>

    {{ Form::model($invoice, ['url' => url("/finance/invoice/invoices/{$invoice->id}/save"), 'method' => 'post', 'class' => 'form-horizontal', 'files' => true, 'id'=>'invoice_form']) }}

    <div class="row">

        {{-- Проверка на оплаченый счет
        @if ($invoice->status_id != '2')
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                <div class="block-view">
                    <div class="block-sub">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Тип</label>
                            <div class="col-sm-8">
                                @php($type_select = collect(\App\Models\Finance\Invoice::TYPES))
                                {{ Form::select('type', $type_select, $invoice->type, ['class' => 'form-control select2 select2-all', 'id'=>'type', 'required']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Юр лицо</label>
                            <div class="col-sm-8">
                                @php($type_select = collect(\App\Models\Organizations\Organization::whereIn('org_type_id',[1,2])->where('is_actual', '=', 1)->get()->pluck('title', 'id')))
                                {{ Form::select('org_id', $type_select, $invoice->org->id, ['class' => 'form-control select2 select2-all', 'id'=>'org_id', 'required']) }}
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        @endif
        --}}

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="block-view">
                <div class="block-sub">
                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="view-field">
                                <span class="view-label">К оплате</span>
                                <span class="view-value">{{ titleFloatFormat($invoice_info->total_sum) }}</span>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="view-field">
                                <span class="view-label">Тип платежа</span>
                                <span class="view-value">{{ $invoice->types_ru('type') }}</span>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="view-field">
                                <span class="view-label">Дата выставления</span>
                                <span class="view-value">{{ setDateTimeFormatRu($invoice->created_at) }}</span>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="view-field">
                                <span class="view-label">Организация</span>
                                <span class="view-value">{{ $invoice->org ? $invoice->org->title : "" }}</span>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="view-field">
                                <span class="view-label">Агент</span>
                                <span class="view-value">{{$invoice->agent?$invoice->agent->name:''}}</span>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="view-field">
                                <span class="view-label">Агентское КВ</span>
                                <span class="view-value">{{ titleFloatFormat($invoice_info->total_kv_agent) }}</span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="block-inner" style="padding: 0">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>№</th>
                <th>Индекс</th>
                <th>Организация</th>
                <th>СК</th>
                <th>Страхователь</th>
                <th>Продукт</th>
                <th>Тип платежа (формальный)</th>
                <th>Тип платежа (фактический)</th>
                <th>Номер договора</th>
                <th>Квитанция</th>
                <th>Менеджер</th>
                <th>Сумма</th>
                <th>КВ агента, %</th>
                <th>КВ агента, руб</th>
                <th>Неоф. скидка, %</th>
                <th>Оф. скидка %</th>
                <th>К оплате</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @php($all_total = 0)
            @if(sizeof($invoice->payments))
                @foreach($invoice->payments as $payment)
                    @php($agent_total = $payment->getPaymentAgentSum())
                    @php($all_total += $agent_total)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $payment->id}}</td>
                        <td>{{ $payment->bso->supplier_org ? \Illuminate\Support\Str::limit($payment->bso->supplier_org->title) : "" }}</td>
                        <td>{{ $payment->bso && $payment->bso->insurance ? $payment->bso->insurance->title : "" }}</td>
                        <td>{{ $payment->Insurer}}</td>
                        <td>{{ $payment->bso && $payment->bso->product ? $payment->bso->product->title : ""}}</td>
                        <td>{{\App\Models\Finance\Invoice::TYPE_INVOICE_PAYMENT[$invoice->type_invoice_payment_id] ? \App\Models\Finance\Invoice::TYPE_INVOICE_PAYMENT[$invoice->type_invoice_payment_id] : ''}}</td>
                        <td>{{ $payment->type_ru() }}</td>
                        <td class="nowrap">




                            @if($payment->type_id == 0 && $payment->contract->sales_condition == 1 && $payment->statys_id == 0)
                                <a href="javascript:void(0);" onclick="openFancyBoxFrame('{{url ("/finance/payment/$payment->id/edit")}}')">{{ $payment->bso ? $payment->bso->bso_title : "" }}</a>
                            @else

                                <a href="/contracts/temp_contracts/contract/{{$payment->contract_id}}/edit" target="_blank">
                                    {{ $payment->bso ? $payment->bso->bso_title : "" }}
                                </a>

                            @endif


                        </td>

                        <td>{{ $payment->bso_receipt ? : "" }}</td>
                        <td>{{ $payment->manager ? $payment->manager->name : "Не указан" }}</td>
                        <td class="nowrap">{{ getPriceFormat($payment->payment_total) }}</td>
                        <td>{{ $payment->financial_policy_kv_agent }}</td>
                        <td class="nowrap">{{ getPriceFormat($payment->financial_policy_kv_agent_total) }}</td>
                        <td class="nowrap">{{ getPriceFormat($payment->informal_discount) }}</td>
                        <td class="nowrap">{{ getPriceFormat($payment->official_discount) }}</td>
                        <td class="nowrap">{{ getPriceFormat($agent_total) }}</td>
                        <td>
                            @if($invoice->status_id == 1)
                                <span class="btn-sm btn-danger" data-delete_payment="{{ $payment->id }}">Удалить</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="16" class="text-right">Итого</td>
                    <td class="nowrap"><b>{{getPriceFormat($all_total)}}</b></td>
                    <td colspan="6"></td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>



    {{Form::close()}}

@endsection






@section('js')
    <script>
        $(function () {

            initWorkArea();

            $(document).on('click', '[data-delete_payment]', function () {
                var payment_id = $(this).data('delete_payment');
                if (confirm('Исключить платёж из счёта?')) {
                    $.post('/finance/invoice/invoices/{{$invoice->id}}/delete_payments/', {payment: [payment_id]}, function (res) {
                        if (res.type === 'invoice') {
                            location.href = '/finance/invoice'
                        } else if (res.type === 'payment') {
                            location.reload();
                        }
                    });
                }

            });


        });


        function delete_invoice() {
            if (confirm('Расформировать счёт?')) {
                $.post('/finance/invoice/invoices/{{$invoice->id}}/delete_invoice', {}, function (res) {
                    location.href = '/finance/invoice'
                });
            }
        }

    </script>

@endsection


