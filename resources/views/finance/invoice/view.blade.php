@extends('layouts.app')


@section('content')


    @include("payments.invoice.head", ["invoice" => $invoice])

    @include("payments.invoice.body", ["invoice" => $invoice])

    @include("payments.invoice.info", ["invoice" => $invoice])


@endsection




@section('js')
    <script>

        function create_act() {

            var payment_array = [
                @foreach($invoice->payments as $payment)
                {{ "{$payment->id}, "}}
                @endforeach
            ];


            var data = {
                agent_id: '{{$invoice->agent_id}}',
                payment_array: JSON.stringify(payment_array),
            };


            loaderShow();
            $.post("{{url("/cashbox/acts_to_underwriting/contract/create_get_acts/")}}", data, function (response) {
                loaderHide();
                location.href = '{{ url("/cashbox/invoice") }}';

            }).always(function() {
                loaderHide();
            });

        }

        $(function(){

            initInvoce();

        });




    </script>

@endsection


