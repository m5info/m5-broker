@if(Auth::user()->role_id === 7 && Auth::user()->hasPermission('role_owns', 'is_agent'))
    @php
        // если агент и стоит Платежи(выбрано все), то выбрать платежи не может
        $disabled_checked = (auth()->user()->hasPermission('finance', 'payments_check_available')) ? 'checked disabled' : '';
    @endphp
@else
    @php
        $disabled_checked = '';
    @endphp
@endif
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th><input type="checkbox" class="check_all_checkbox" name="all_payments" {{$disabled_checked}}></th>
            {{--<th>id</th>
            <th>Поставщик БСО</th>--}}
            <th>Тип</th>
            <th>Номер договора</th>
            <th>Страхователь</th>
            <th>СК</th>
            <th>Продукт</th>
            <th>Тип платежа</th>
            <th>Тип акцепта</th>
            <th>Поток оплаты</th>
            <th>Квитанция</th>
            <th>Сумма</th>
            <th>КВ агента, %</th>
            <th>КВ агента, руб</th>
            <th>К оплате</th>
        </tr>
    </thead>
    <tbody>
        @if(sizeof($payments))
            @foreach($payments as $payment)
                <tr>
                    <td><input type="checkbox" name="payment[{{$payment->id}}]" value="{{$payment->id}}" {{$disabled_checked}}></td>
                    {{--<td>{{$payment->id}}</td>
                    <td>{{$payment->bso && $payment->bso->supplier ? $payment->bso->supplier->title.'(id: => '.$payment->bso->supplier->id.')' : 'ПУсто'}}</td>--}}
                    <td>{{ \App\Models\Contracts\Payments::TRANSACTION_TYPE[$payment->type_id] }} {{ ($payment->type_id == 0)? $payment->payment_number : '' }}</td>
                    <td>{{ $payment->bso ? $payment->bso->bso_title : "" }}</td>
                    <td>
                        {{ ($payment->type_id == 0)? $payment->Insurer : $payment->comments }}
                    </td>
                     <td>{{ $payment->bso && $payment->bso->insurance ? $payment->bso->insurance->title : "" }} {{ $payment->bso && $payment->bso->supplier_org ? $payment->bso->supplier_org->title : "" }}</td>
                    <td>{{ $payment->bso && $payment->bso->product ? $payment->bso->product->title : ""}}</td>
                    <td>{{ \App\Models\Contracts\Payments::PAYMENT_TYPE[$payment->payment_type] }}</td>
                    <th>{{ ($payment->contract)?\App\Models\Contracts\Contracts::KIND_ACCEPTANCE[$payment->contract->kind_acceptance]:'' }}</th>
                    <td>{{ \App\Models\Contracts\Payments::PAYMENT_FLOW[$payment->payment_flow] }}</td>
                    <td>
                        @if($payment->atol_check_id)
                            <span class="btn-primary btn" onclick="openFancyBoxFrame('{{"/atol/{$payment->atol_check_id}/check"}}');">Чек онлайн</span>
                        @else
                            {{ $payment->bso_receipt }}
                        @endif
                    </td>
                    <td class="nowrap">{{ getPriceFormat($payment->payment_total) }}</td>
                    <td>{{ $payment->financial_policy_kv_agent }}</td>
                    <td>{{ titleFloatFormat($payment->financial_policy_kv_agent_total) }}</td>
                    <td class="nowrap">{{ getPriceFormat($payment->getPaymentAgentSum()) }}</td>
                </tr>
            @endforeach
        @else

            <tr>
                <td colspan="16" class="text-center">Нет доступных платежей</td>
            </tr>
        @endif
    </tbody>
</table>