<div class="form-horizontal block-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">

    <div class="row form-group">
        @if(auth()->user()->role->rolesVisibility(7)->visibility != 2)
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <label class="control-label">Агент</label>
                @php($agents_select = \App\Models\User::getALLUserWhere()->get()->pluck('name','id')) {{-- ->prepend('Все', 0) --}}
                @php($agent_selected = (request('agent_id') && request('agent_id') > 0) ? request('agent_id') : session('finance.agent_id', auth()->id()))
                @php($agent_selected = ($agent_selected == 0)?auth()->id():$agent_selected)
                {{ Form::select('agent_id', $agents_select, $agent_selected, ['class' => 'form-control select2 select2', 'id'=>'agent_id', 'required', 'onchange' => 'loadItems(event)']) }}
            </div>
        @else
            {{Form::hidden('agent_id', auth()->user()->id)}}
        @endif

        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
            <label class="control-label">Тип</label>
            @php($type_select = collect(['cash'=>'Наличные', 'cashless'=>'Безнал Брокер', 'card'=>'Безнал Карта', 'sk' =>'СК'])->prepend('Все', ''))
            @php($type_selected = request('type') ? request()->query('type') : 0)
            {{ Form::select('type', $type_select, $type_selected, ['class' => 'form-control select2 select2-all', 'id'=>'type', 'required', 'onchange' => 'loadItems(event)']) }}
        </div>

        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
            <label class="control-label">Тип акцепта</label>
            @php($type_select = collect(\App\Models\Contracts\Contracts::KIND_ACCEPTANCE)->prepend('Все', '-1'))
            @php($type_selected = request('type') ? request()->query('type') : -1)
            {{ Form::select('kind_acceptance', $type_select, $type_selected, ['class' => 'form-control select2 select2-all', 'id'=>'type', 'required', 'onchange' => 'loadItems(event)']) }}
        </div>

        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
            <label class="control-label">Страховая компания</label>
            @php($insurance_select = \App\Models\Directories\InsuranceCompanies::where('is_actual', 1)->get()->pluck('title', 'id')->prepend('Все', -1))
            @php($insurance_selected = request('insurance_companies_id') ? request()->query('insurance_companies_id') : '')
            {{ Form::select('insurance_companies_id', $insurance_select , $insurance_selected, ['class' => 'form-control select2-ws', 'id'=>'insurance_companies_id', 'onchange'=>'loadItems(event)']) }}
        </div>


    </div>
</div>

@if(Auth::user()->role_id === 7 && Auth::user()->hasPermission('role_owns', 'is_agent'))
    @if(auth()->user()->hasPermission('finance', 'payments_check_available'))
        <a href="#" class="btn btn-success btn-right add_to_invoice" id="add_to_invoice">Создать счёт</a>
        <a href="#" class="btn btn-primary btn-right invoice_update" id="invoice_update">Добавить к счету</a>
    @else
        <a href="#" class="btn btn-success btn-right add_to_invoice" id="add_to_invoice" style="display: none;">Создать счёт</a>
        <a href="#" class="btn btn-primary btn-right invoice_update" id="invoice_update" style="display: none;">Добавить к счету</a>
    @endif
@else
    <a href="#" class="btn btn-success btn-right add_to_invoice" id="add_to_invoice" style="display: none;">Создать счёт</a>
    <a href="#" class="btn btn-primary btn-right invoice_update" id="invoice_update" style="display: none;">Добавить к счету</a>
@endif

<div id="table">

</div>

@if(Auth::user()->role_id === 7 && Auth::user()->hasPermission('role_owns', 'is_agent'))
    @if(auth()->user()->hasPermission('finance', 'payments_check_available'))
        <a href="#" class="btn btn-success btn-right add_to_invoice" id="add_to_invoice">Создать счёт</a>
        <a href="#" class="btn btn-primary btn-right invoice_update" id="invoice_update">Добавить к счету</a>
    @else
        <a href="#" class="btn btn-success btn-right add_to_invoice" id="add_to_invoice" style="display: none;">Создать счёт</a>
        <a href="#" class="btn btn-primary btn-right invoice_update" id="invoice_update" style="display: none;">Добавить к счету</a>
    @endif
@else
    <a href="#" class="btn btn-success btn-right add_to_invoice" id="add_to_invoice" style="display: none;">Создать счёт</a>
    <a href="#" class="btn btn-primary btn-right invoice_update" id="invoice_update" style="display: none;">Добавить к счету</a>
@endif



@include('_chunks/_pagination',['class'=>'pull-right','callback'=>'loadItems'])

<style>
    .invoice_update {
        margin-bottom: 10px;
    }
</style>
<script>

    var PAGE = 1;


    $(function () {

        $(document).on('click', '[name="all_payments"]', function () {
            var checked = $(this).prop('checked');
            $('[name*="payment["]').prop('checked', checked).change();
        });

        $(document).on('click', '[name*="payment["]', function () {
            var all_checked = true;
            $.each($('[name*="payment["]'), function (k, v) {
                all_checked = all_checked && $(v).prop('checked');
            });
            $('[name="all_payments"]').prop('checked', all_checked).change();
        });

        $(document).on('click', '[name*="payment"]', function () {
            if ($('[name*="payment["]:checked').length > 0) {
                $('.add_to_invoice').show();
                $('.invoice_update').show();
            } else {
                $('.add_to_invoice').hide();
                $('.invoice_update').hide();
            }
        });

        $(document).on('click', '#add_to_invoice', function () {
            var payments = $('[name*="payment["]:checked');

            var data = {};
            $.each(payments, function (k, v) {
                data['payments[' + k + ']'] = $(v).val();
            });
            var query = build_query(data);
            $.get("/finance/invoice/invoices/create?" + query, {}, function (res) {
                if (res.status === 'ok') {
                    openFancyBoxFrame("/finance/invoice/invoices/create?" + query);
                } else {
                    flashMessage('danger', res.error)
                }
            }, 'json');
        });

        $(document).on('click', '#invoice_update', function () {
            var payments = $('[name*="payment["]:checked');
            var data = {};
            $.each(payments, function (k, v) {
                data['payments[' + k + ']'] = $(v).val();
            });
            var query = build_query(data);

            $.get("/finance/invoice/payments/update_invoice?" + query, {}, function (res) {
                if (res.status === 'ok') {
                    openFancyBoxFrame("/finance/invoice/payments/update_invoice?" + query);
                } else {
                    flashMessage('danger', res.error)
                }
            }, 'json');
        });

        loadItems();

    });


    function loadItems(event = false) {

        if(event){
            var target_id = event.target.id;
            PAGE = 1;
        }

        loaderShow();

        var data = getData(target_id);

        $('#page_list').html('');
        $('#table_row').html('');
        $('#view_row').html(0);
        $('#max_row').html(0);


        $.post("{{url("/finance/invoice/payments/get_payments_table")}}", data, function (response) {
            $('#table').html(response.html);
            $('#view_row').html(response.view_row);
            $('#max_row').html(response.max_row);

            ajaxPaginationUpdate(response.page_max, loadItems);


        }).fail(function () {
            $('#table').html('');
        }).always(function () {
            loaderHide();
        });

    }

    function getData(target_id = null) {

        return {
            agent_id: $('[name="agent_id"]').val(),
            type: $('[name="type"]').val(),
            kind_acceptance: $('[name="kind_acceptance"]').val(),
            page_count: $('[name="page_count"]').val(),
            PAGE: PAGE,
            insurance_companies_id: $('[name="insurance_companies_id"]').val(),
        }
    }


</script>