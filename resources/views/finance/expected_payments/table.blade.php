@php
    $analiticsVisibility = auth()->user()->role->visibility('analitics');
    /*Не доделаны значения Акцепт 	Дата по кассе 	Условие продажи 	Тип оплаты 	Тип платежа 	Личная продажа 	Маржа и тд*/

    $all_payment_total = 0;
    $all_invoice_payment_total = 0;
    $all_official_discount_total = 0;
    $all_informal_discount_total = 0;
    $all_kv_agent_total = 0;
    $all_kv_parent_total = 0;
    $all_kv_total = 0;
    $all_margin_total = 0;

@endphp
<div class="wrapper1">
    <div class="div1 row col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
    </div>
</div>
<div class="wrapper2">
    <div class="div2 row col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="container-for-table">
            <table class="table table-bordered payments_table huck">
                <tbody>
                @foreach($payments as $key => $payment)
                    @php
                        $all_payment_total += $payment->payment_total;
//                      Если поток оплаты = СК, то сумма в кассу = 0
                        if($payment->payment_flow == 0){
                            $all_invoice_payment_total += $payment->invoice_payment_total;
                        }else{
                            $all_invoice_payment_total += 0;
                        }
                        $all_official_discount_total += $payment->official_discount_total;
                        $all_informal_discount_total += $payment->informal_discount_total;
                        $all_kv_agent_total += $payment->financial_policy_kv_agent_total;
                        $all_kv_parent_total += $payment->financial_policy_kv_parent_total;
                        $all_kv_total += $payment->financial_policy_kv_bordereau_total + $payment->financial_policy_kv_dvoy_total;
                        $all_margin_total += $payment->getMarginAmount(1);
                    @endphp
                @endforeach
                <tr>
                    <th colspan="10"></th>
                    <th><b>{{getPriceFormat($all_payment_total)}}</b></th>
                    <th colspan="5"></th>
                    <th><b>{{getPriceFormat($all_kv_agent_total)}}</b></th>

                    @if(in_array($analiticsVisibility, [0,1,3]))

                        <th colspan="2"></th>
                        <th><b>{{getPriceFormat($all_kv_parent_total)}}</b></th>

                    @endif

                    @if(in_array($analiticsVisibility, [0,1]))

                        <th colspan="3"></th>
                        <th><b>{{getPriceFormat($all_kv_total)}}</b></th>

                        <th></th>
                        <th><b>{{getPriceFormat($all_margin_total)}}</b></th>

                    @endif

                </tr>
                <thead>
                <tr class="tr-sticky">
                    <th>№</th>
                    <th>Организация</th>
                    <th>СК</th>
                    <td>Точка продаж</td>
                    <th>Продукт</th>
                    <th>Полис №</th>
                    <th>Страхователь</th>
                    <th>Дата договора</th>
                    <th>Дата оплаты</th>
                    <th>Тип</th>
                    <th>Взнос</th>
                    <th colspan="2">Оф. скидка</th>
                    <th colspan="2">Неоф. скидка</th>
                    <th>КВ агента %</th>
                    <th colspan="2">Вознаграждение агента</th>

                    @if(in_array($analiticsVisibility, [0,1,3]))
                        <th>КВ Руководителя %</th>
                        <th>Вознаграждение руководитея</th>
                        <th>Агент</th>
                        <th>Менеджер</th>
                    @endif

                    @if(in_array($analiticsVisibility, [0,1]))

                        <th>КВ Входящая %</th>
                        <th>Вознаграждение брокера</th>

                        <th>Маржа %</th>
                        <th>Маржа сумма</th>

                    @endif

                </tr>
                </thead>
                @foreach($payments as $key => $payment)


                    <tr onclick="openFancyBoxFrame('/contracts/temp_contracts/add/payment?payment_id={{$payment->id}}')">
                        <td>{{$key+1}}</td>
                        <td>{{$payment->org ? $payment->org->title : ""}}</td>
                        <td>{{$payment->bso && $payment->bso->insurance ? $payment->bso->insurance->title : "" }}</td>
                        <td>{{$payment->bso->point_sale ? $payment->bso->point_sale->title : ''}}</td>
                        <td>{{ $payment->bso && $payment->bso->product ? $payment->bso->product->title : ""}}</td>
                        <td>{{$payment->bso->bso_title}}</td>
                        <td>{{ $payment->Insurer }}</td>
                        <td>{{ setDateTimeFormatRu($payment->contract->sign_date,1) }}</td>
                        <td>{{ setDateTimeFormatRu($payment->payment_data,1) }}</td>
                        <td>{{\App\Models\Contracts\Payments::TRANSACTION_TYPE[$payment->type_id]}} @if($payment->type_id == 0) {{$payment->payment_number}} @endif</td>
                        <td>{{titleFloatFormat($payment->payment_total)}}</td>
                        <td colspan="2">{{ getPriceFormat($payment->official_discount_total) }}</td>
                        <td colspan="2">{{ getPriceFormat($payment->informal_discount_total) }}</td>
                        <td>{{$payment->financial_policy_kv_agent}}</td>
                        <td colspan="2">{{ getPriceFormat($payment->financial_policy_kv_agent_total) }}</td>


                        @if(in_array($analiticsVisibility, [0,1,3]))

                            <td>{{$payment->financial_policy_kv_parent}}</td>
                            <td>{{getPriceFormat($payment->financial_policy_kv_parent_total)}}</td>

                            <td>{{$payment->agent->name}}</td>
                            <td>{{$payment->manager ? $payment->manager->name : "" }}</td>

                        @endif

                        @if(in_array($analiticsVisibility, [0,1]))



                            <td>{{$payment->financial_policy_kv_bordereau + $payment->financial_policy_kv_dvoy}}</td>
                            <td>{{getPriceFormat($payment->financial_policy_kv_bordereau_total + $payment->financial_policy_kv_dvoy_total)}}</td>

                            <td>{{($payment->getMarginAmount(0))}}</td>
                            <td>{{getPriceFormat($payment->getMarginAmount(1))}}</td>

                        @endif

                    </tr>
                @endforeach

                <tr>
                    <th colspan="10"></th>
                    <th><b>{{getPriceFormat($all_payment_total)}}</b></th>
                    <th colspan="5"></th>
                    <th><b>{{getPriceFormat($all_kv_agent_total)}}</b></th>

                    @if(in_array($analiticsVisibility, [0,1,3]))

                        <th colspan="2"></th>
                        <th><b>{{getPriceFormat($all_kv_parent_total)}}</b></th>

                    @endif

                    @if(in_array($analiticsVisibility, [0,1]))

                        <th colspan="3"></th>
                        <th><b>{{getPriceFormat($all_kv_total)}}</b></th>

                        <th></th>
                        <th><b>{{getPriceFormat($all_margin_total)}}</b></th>

                    @endif

                </tr>


                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $('.wrapper1').on('scroll', function (e) {
        $('.wrapper2').scrollLeft($('.wrapper1').scrollLeft());
    });
    $('.wrapper2').on('scroll', function (e) {
        $('.wrapper1').scrollLeft($('.wrapper2').scrollLeft());
    });

    $('.div1').width($('.payments_table').width());
    $('.div2').width($('.payments_table').width());
</script>

<style>

    .wrapper1, .wrapper2 { width: 100%; overflow-x: scroll; overflow-y: hidden; }
    .wrapper1 { height: 20px; }
    .wrapper2 {}
    .div1 { height: 20px; }
    .div2 { overflow: none; }


</style>