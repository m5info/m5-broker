@extends('layouts.app')

@section('head')


@append

@section('content')


    <div class="page-heading">
        <h2>Ожидаемые платежи</h2>
    </div>
    <div class="divider"></div>

    <form name="main_container">
    <div class="block-main">
        <div class="block-sub">
            <div class="form-horizontal">
                <div class="form-group">



                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6 col-lg-2">
                                <label class="control-label">Дата оплаты с</label>
                                {{ Form::text('date_from', '', ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off']) }}
                            </div>
                            <div class="col-sm-6 col-lg-2">
                                <label class="control-label">Дата оплаты по</label>
                                {{ Form::text('date_to', '', ['class' => 'form-control datepicker date inline', 'autocomplete' => 'off']) }}
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <label class="control-label">БСО</label>
                                {{ Form::text('bso_title', '', ['class' => 'form-control inline', 'autocomplete' => 'off']) }}
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="control-label">СК</label>
                                {{ Form::select('insurance_ids[]', $insurances->pluck('title', 'id'), 0, ['class' => 'form-control select2-all', 'multiple' => true, 'onchange' => 'loadItems()']) }}
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label">Организация</label>
                                {{ Form::select('org_ids[]', $organizations->pluck('title', 'id'), 0, ['class' => 'form-control select2-all', 'multiple' => true, 'onchange' => 'loadItems()']) }}
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label">Продукт</label>
                                {{ Form::select('product_id[]', $products->pluck('title', 'id'), 0, ['class' => 'form-control select2-all', 'multiple' => true, 'onchange' => 'loadItems()']) }}
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-12">
                        <div class="row">
                            @if(auth()->user()->hasEmployee || Auth::user()->role_id === 1)
                                <div class="col-sm-4">
                                    <label class="control-label">Руководитель</label>
                                    {{ Form::select('nop_id', \App\Models\User::getALLParent()->pluck('name', 'id')->prepend('Нет', 0), request('parent_agent_id') ? request()->query('parent_agent_id') : 0, ['class' => 'form-control select2 select2-all', 'id'=>'nop_id', 'required', 'onchange' => 'loadItems()']) }}
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label">Агент</label>
                                    {{ Form::select('agent_id', auth()->user()->visibleAgents('finance')->get()->pluck('name', 'id')->prepend('Нет', 0), request('agent_id') ? request()->query('agent_id') : 0, ['class' => 'form-control select2 select2-all', 'id'=>'agent_id', 'required', 'onchange' => 'loadItems()']) }}
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label">Менеджер</label>
                                    {{ Form::select('manager_id', auth()->user()->visibleAgents('finance')->get()->pluck('name', 'id')->prepend('Нет', 0), request('agent_id') ? request()->query('manager_id') : 0, ['class' => 'form-control select2 select2-all', 'id'=>'manager_id', 'required', 'onchange' => 'loadItems()']) }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <span class="btn btn-primary pull-left" onclick="loadItems()">Применить</span>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </form>
    <div class="block-inner sorting row col-xs-12 col-sm-12 col-md-12 col-lg-12" id="data_table" >

    </div>



@endsection

@section('js')

    <script>


        $(function () {
            loadItems();

        });



        function loadItems()
        {
            var data = $('form[name="main_container"]').serialize();

            $.get("{{url("/finance/expected_payments/get_payments_table")}}", data, function (res) {

                $('#data_table').html(res);

            });


        }



    </script>


@endsection