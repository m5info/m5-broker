@extends('layouts.frame')


@section('title')

    Взнос # {{$payment->payment_number}} - <a href="{{url("/contracts/temp_contracts/contract/{$contract->id}/edit/")}}" target="_blank" >{{$payment->bso->bso_title}}</a>

@endsection

@section('content')


    {{ Form::open(['url' => url("/finance/payment/{$payment->id}/edit"), 'method' => 'post', 'class' => 'form-horizontal']) }}

    <input type="hidden" name="payment[type_id]" value="{{$payment->type_id}}"/>
    <input type="hidden" name="payment[bso_id]" value="{{$payment->bso_id}}"/>
    <input type="hidden" name="payment[contract_id]" value="{{$payment->contract_id}}"/>


    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
            <label class="control-label pull-left" style="margin-top: 5px;">Cтрахователь</label>
            {{ Form::text("contract[insurer_title]", $contract->insurer->title, ['class' => 'form-control']) }}
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
            <label class="control-label pull-left" style="margin-top: 5px;">Заявка из фронт офиса</label>
            <input class="form-control" id="order_title_0" name="payment[order_title]" type="text" value="{{$payment->order_title}}" {{ (int)$payment->order_id > 0 && !auth()->user()->is('admin') == 1 ? 'readonly' : '' }}>
            <input type="hidden" name="payment[order_id]" id="order_id_0" value="{{$payment->order_id}}">
        </div>

    </div>

    {{--
    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
            <label class="control-label pull-left" style="margin-top: 5px;">Агент</label><br/><br/>
            {{ Form::select('payment[agent_id]', $agents->prepend('Выберите значение', 0), $payment->agent_id, ['class' => 'form-control select2']) }}
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
            <label class="control-label pull-left" style="margin-top: 5px;">Менеджер</label><br/><br/>
            {{ Form::select('payment[manager_id]', $agents->prepend('Выберите значение', 0), $payment->manager_id, ['class' => 'form-control select2', 'id' => 'manager_id']) }}
        </div>

    </div>
    --}}




    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
            <label class="control-label pull-left">Дата заключения</label>
            {{ Form::text("contract[sign_date]", setDateTimeFormatRu($contract->sign_date, 1), ['class' => 'form-control datepicker date', "id"=>"sign_date_0", 'onchange'=>"a_auto(this, 'begin_date')"]) }}
        </div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
            <label class="control-label pull-left">Дата начала</label>
            {{ Form::text("contract[begin_date]", setDateTimeFormatRu($contract->begin_date, 1), ['class' => 'form-control datepicker date', "id"=>"begin_date", 'onchange'=>"set_end_dates(this.value)"]) }}
        </div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
            <label class="control-label pull-left">Дата окончания</label>
            {{ Form::text("contract[end_date]", setDateTimeFormatRu($contract->end_date, 1), ['class' => 'form-control datepicker date datepicker_end', "id"=>"end_date"]) }}
        </div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
            <label class="control-label pull-left">Дата оплаты</label>
            {{ Form::text("payment[payment_data]", setDateTimeFormatRu($payment->payment_data, 1), ['class' => 'form-control datepicker date']) }}
        </div>
    </div>


    <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12" >

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
            <label class="control-label pull-left">Сумма</label>
            {{ Form::text("payment[payment_total]", ($payment->payment_total!=0.00)?titleFloatFormat($payment->payment_total):'', ['class' => 'form-control sum']) }}
        </div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
            <label class="control-label pull-left">Официальная %</label>
            {{ Form::text("payment[official_discount]", ($payment->official_discount!=0.00)?titleFloatFormat($payment->official_discount):'', ['class' => 'form-control sum']) }}
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
            <label class="control-label pull-left">Неофициальная %</label>
            {{ Form::text("payment[informal_discount]", ($payment->informal_discount!=0.00)?titleFloatFormat($payment->informal_discount):'', ['class' => 'form-control sum']) }}
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
            <label class="control-label pull-left">Банк %</label>
            {{ Form::text("payment[bank_kv]", ($payment->bank_kv!=0.00)?titleFloatFormat($payment->bank_kv):'', ['class' => 'form-control sum']) }}
        </div>
    </div>




    {{Form::close()}}


@endsection

@section('footer')

    <button class="btn btn-danger pull-left" onclick="deletePaymentTemp()">{{ trans('form.buttons.delete') }}</button>

    <button onclick="submitForm()" type="submit" class="btn btn-primary">{{ trans('form.buttons.save') }}</button>

@endsection

@section('js')

    <script>

        $(function () {






        });


        function deletePaymentTemp()
        {
            newCustomConfirm(function (result) {
                if (result){


                    loaderShow();
                    $.post('{{url("/finance/payment/{$payment->id}/delete")}}', {
                        _method: 'DELETE'
                    }, function (response) {


                        if(response.status == 1){
                            parent_reload();
                        }else{
                            alert(response.msg);
                        }

                        loaderHide();



                    }).done(function () {
                        loaderShow();
                    }).fail(function () {
                        loaderHide();
                    }).always(function () {
                        loaderHide();
                    });




                }
            });

        }




    </script>
@endsection