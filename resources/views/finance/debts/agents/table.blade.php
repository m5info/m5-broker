<table class="table table-bordered">
    <thead>
        <tr class="head-tr">
            <th class="center">Агент</th>
            <th class="center">Руководитель</th>
            <th class="center">Сумма долга нал</th>
            <th class="center">Сумма долга безнал</th>
            <th class="center">Сумма долга СК</th>
            <th class="center">Сумма долга</th>
            @foreach($user_balances as $balance)
                <th class="center">{{$balance->title}}</th>
            @endforeach
        </tr>
    </thead>
    @foreach($user_balances as $balance)
     @php ($total[$balance->id] = 0)
    @endforeach
    <tbody>






        @if(isset($debts))

            <tr>
                <td colspan="2" class="right">Итого</td>
                <td class="right">{{ titleFloatFormat($debts->sum('debt_nal')) }}</td>
                <td class="right">{{ titleFloatFormat($debts->sum('debt_bn')) }}</td>
                <td class="right">{{ titleFloatFormat($debts->sum('debt_bn_sk')) }}</td>
                <td class="right">{{ titleFloatFormat($debts->sum('debt_all')) }}</td>
                @foreach($user_balances as $balance)
                    <td class="right"></td>
                @endforeach
            </tr>

        @foreach($debts as $debt)
            <tr class="clickable-row" data-href="/finance/debts/{{$debt->agent_id}}/detail" style="background-color: {{ \App\Models\Contracts\Payments::getDebtPaymentsColor($debt->payment_data)  }};">


                <td class="right">{{ $debt->name }}</td>
                <td class="right">{{$debt->agent->perent ? $debt->agent->perent->name : ""}}</td>
                <td class="right">{{ titleFloatFormat($debt->debt_nal) }}</td>
                <td class="right">{{ titleFloatFormat($debt->debt_bn) }}</td>
                <td class="right">{{ titleFloatFormat($debt->debt_bn_sk) }}</td>
                <td class="right">{{ titleFloatFormat($debt->debt_all) }}</td>
                @foreach($user_balances as $balance)
                    @php($ab = $debt->agent->getBalance($balance->id)->balance)
                <td class="right"> {{titleFloatFormat($ab)}}</td>
                @php ($total[$balance->id] += $ab)
                @endforeach


            </tr>
        @endforeach
        @else
            <tr>
                <td colspan="56" style="text-align: center">Нет долгов</td>
            </tr>
        @endif

    </tbody>
</table>