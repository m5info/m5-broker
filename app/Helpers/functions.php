<?php

function memory()
{
    return round(memory_get_usage() / 1024 / 1024, 2) . 'MB';
}

function parentReload()
{
    echo <<<HTML
	<script>
		window.parent.location.reload();
	</script>
HTML;
    exit;

}

function parentRedirect($url)
{
    echo <<<HTML
	<script>
		window.parent.location = "$url";
	</script>
HTML;
    exit;

}

function parentReloadTab()
{
    echo <<<HTML
	<script>
		window.parent.reloadTab();
	</script>
HTML;
    exit;

}


function reload()
{
    echo <<<HTML
	<script>
		window.location.reload();
	</script>
HTML;
    exit;

}

function frameError($msg)
{
    echo <<<HTML
	<script>
        window.parent.flashHeaderMessage('{$msg}', 'danger');
		window.parent.jQuery.fancybox.close();
	</script>
HTML;
    exit;
}

function frameSuccess($msg)
{
    echo <<<HTML
	<script>
        window.parent.flashHeaderMessage('{$msg}', 'success');
		window.parent.jQuery.fancybox.close();
	</script>
HTML;
    exit;
}

function closeFancyBox()
{
    echo <<<HTML
	<script>
		window.parent.jQuery.fancybox.close();
	</script>
HTML;
    exit;
}

function closeFancyBoxTabsWithSetPage($seletor_tab,$page)
{
    echo <<<HTML
	<script>
		window.parent.jQuery.fancybox.close();
		window.parent.document.querySelector('$seletor_tab').children[0].children[2].children[0].children[$page].click();
	</script>
HTML;
    exit;
}


function parentGetAgetnInfo($agent_id)
{
    echo <<<HTML
	<script>
		window.parent.jQuery.fancybox.close();
		window.parent.getUserInfo({$agent_id});
	</script>
HTML;
    exit;

}

function getDateTime()
{
    return date("Y-m-d H:i:s");
}

function getDateTimeRu()
{
    return date("d.m.Y H:i:s");
}

function getDateRu()
{
    return date("d.m.Y");
}

function getDateFormat($date)
{
    $YY = substr($date, -4);
    $DD = substr($date, 0, 2);
    $MM = substr($date, 3, 2);

    return $YY . "-" . $MM . "-" . $DD;
}

function getDateFormatEn($date)
{
    if($date && strlen($date) > 4){
        return date('Y-m-d', strtotime($date));
    }
    return null;
}

function getDateFormatRu($date)
{
    if (strlen($date) > 0 && $date != '0000-00-00' && $date != '0000-00-00 00:00:00') {
        return date('d.m.Y', strtotime($date));
    } else {
        return '';
    }
}


function getDateFormatTimeRu($date)
{
    if (strlen($date) > 0 && $date != '0000-00-00' && $date != '0000-00-00 00:00:00') {
        return date('H:i', strtotime($date));
    } else {
        return '';
    }
}

function setDateTimeFormat($date)
{
    if (strlen($date) > 0 && $date != '1970-01-01 00:00:00' && $date != '1970-01-01' && $date != '01.01.1970') {
        return date('Y-m-d H:i:s', strtotime($date));
    } else {
        return null;
    }
}

function setDateTimeFormatRu($date, $type = 0)
{
    if (strlen($date) > 0 && $date != '1970-01-01 00:00:00' && $date != '1970-01-01' && $date != '01.01.1970' && $date != '0000-00-00') {
        if ($type == 0) return date('d.m.Y H:i:s', strtotime($date));
        else return date('d.m.Y', strtotime($date));
    } else {
        return '';
    }
}

function setDateTimePluss($date, $plus = '+1 year', $time = 'H:i:s')
{
    if (strlen($date) > 0 && $date != '1970-01-01 00:00:00' && $date != '1970-01-01' && $date != '01.01.1970' && $date != '0000-00-00') {
        return date("Y-m-d $time", strtotime("$plus " . $date));
    } else {
        return null;
    }
}

function setDateMyFormat($date, $format)
{
    if (strlen($date) > 0 && $date != '0000-00-00' && $date != '0000-00-00 00:00:00') {
        return date($format, strtotime($date));
    } else {
        return '';
    }
}

function getFloatFormat($str)
{
    $str = str_replace(' ', "", $str);
    $str = str_replace(',', ".", $str);

    return floatval($str);
}

function getFloatFormatHundredthFraction($str)
{
    $str = str_replace(' ', "", $str);
    $str = str_replace(',', ".", $str);
    $str = round($str, 2);

    return floatval($str);
}


function titleFloatFormat($num, $is_xls = 0)
{
    if (strlen($num) == 0) {
        $num = 0;
    }

    if($is_xls == 1) return number_format($num, 2, '.', '');

    return number_format($num, 2, ',', ' ');
}


function titleNumberFormat($num)
{
    if (strlen($num) == 0) {
        $num = 0;
    }

    return number_format($num, 0, '', ' ');
}

function twoNumbersEndFormat($num){

    $v = (string)$num;
    $length = strlen($v);
    $s_1 = substr($v, 0, $length - 2);
    $s_2 = substr($v, -2);
    $s_3 = (float)$s_1.'.'.$s_2;

    return $s_3;
}

function getPhoneFormat($str)
{

    $str = str_replace(' ', "", $str);
    $str = str_replace('(', "", $str);
    $str = str_replace(')', "", $str);
    $str = str_replace('-', "", $str);
    $str = str_replace(';', "", $str);
    $str = str_replace('+', "", $str);

    return $str;
}

function getStatusColor($order_state)
{
    $res = '';
    switch ($order_state) {
        case 0:
            $res = 'white';
            break;
        case 7:
        case 8:
            $res = 'purple';
            break;
        case 2:
        case 5:
            $res = 'yellow';
            break;
        case 3:
        case 4:
        case 6:
            $res = 'green';
            break;
    }

    return $res;
}

function getMonthById($id, $case = false)
{
    $months = getRuMonthes($case);
    return isset($months[$id]) ? $months[$id] : '';
}

function getRuMonthes($case = false)
{

    switch ($case) {

        case "rod":
            return [
                1 => 'Января',
                2 => 'Февраля',
                3 => 'Марта',
                4 => 'Апреля',
                5 => 'Мая',
                6 => 'Июня',
                7 => 'Июля',
                8 => 'Августа',
                9 => 'Сентября',
                10 => 'Октября',
                11 => 'Ноября',
                12 => 'Декабря',
            ];

            break;

        default :
            return [
                1 => 'Январь',
                2 => 'Февраль',
                3 => 'Март',
                4 => 'Апрель',
                5 => 'Май',
                6 => 'Июнь',
                7 => 'Июль',
                8 => 'Август',
                9 => 'Сентябрь',
                10 => 'Октябрь',
                11 => 'Ноябрь',
                12 => 'Декабрь',
            ];

    }

}

function getOneMonth($number_month, $case = false)
{
    $return = '';
    if ($case){
        switch ($number_month) {
            case 1:
                $return = 'Января';
                break;
            case 2:
                $return = 'Февраля';
                break;
            case 3:
                $return = 'Марта';
                break;
            case 4:
                $return = 'Апреля';
                break;
            case 5:
                $return = 'Мая';
                break;
            case 6:
                $return = 'Июня';
                break;
            case 7:
                $return = 'Июля';
                break;
            case 8:
                $return = 'Августа';
                break;
            case 9:
                $return = 'Сентября';
                break;
            case 10:
                $return = 'Октября';
                break;
            case 11:
                $return = 'Ноября';
                break;
            case 12:
                $return = 'Декабря';
                break;
        }
    }else{
        switch ($number_month) {
            case 1:
                $return = 'Январь';
                break;
            case 2:
                $return = 'Февраль';
                break;
            case 3:
                $return = 'Март';
                break;
            case 4:
                $return = 'Апрель';
                break;
            case 5:
                $return = 'Май';
                break;
            case 6:
                $return = 'Июнь';
                break;
            case 7:
                $return = 'Июль';
                break;
            case 8:
                $return = 'Август';
                break;
            case 9:
                $return = 'Сентябрь';
                break;
            case 10:
                $return = 'Октябрь';
                break;
            case 11:
                $return = 'Ноябрь';
                break;
            case 12:
                $return = 'Декабрь';
                break;
        }
    }
    return $return;
}


function getDateVerbose($date)
{
    if (is_numeric($date)) {
        $timestamp = $date;
    }else if(is_string($date)) {
        $date = new \DateTime($date);
        $timestamp = $date->getTimestamp();
    } else {
        return null;
    }

    $date_data = getdate($timestamp);

    $day = $date_data['mday'];
    $month = getMonthById($date_data['mon'], 'rod');
    $year = $date_data['year'];

    return "{$day} {$month} {$year} г.";
}


function getYearsRange($from, $to)
{
    $years = range(date('Y') + $from, date('Y') + $to);
    $years = array_combine($years, $years);
    return $years;
}

function getYearsArrey($year = 2019)
{
    $years = [];
    for ($i = $year; $i<=date("Y", strtotime("+ 1 year"));$i++){
        $years[$i] = $i;
    }
    return $years;
}

function getTypeCountTransportationsClientsPlanning($type_count_transportations)
{
    $type_count_transportations_title = '';
    switch ($type_count_transportations) {
        case 0:
            $type_count_transportations_title = "день";
            break;
        case 1:
            $type_count_transportations_title = "месяц";
            break;
        case 2:
            $type_count_transportations_title = "год";
            break;
    }
    return $type_count_transportations_title;
}

function getStatusPaymentTypeClientsPlanning($status_payment_type)
{
    $status_payment_type_title = '';
    switch ($status_payment_type) {
        case \App\Models\Subject\Type::STATUS_SENDER_PAYMENT_TUPE_DEFAULT:
            $status_payment_type_title = trans('clients/clients.edit.STATUS_SENDER_PAYMENT_TUPE_DEFAULT');
            break;
        case \App\Models\Subject\Type::STATUS_SENDER_PAYMENT_TUPE_CAUTION:
            $status_payment_type_title = trans('clients/clients.edit.STATUS_SENDER_PAYMENT_TUPE_CAUTION');
            break;
        case \App\Models\Subject\Type::STATUS_SENDER_PAYMENT_TUPE_TRUST:
            $status_payment_type_title = trans('clients/clients.edit.STATUS_SENDER_PAYMENT_TUPE_TRUST');
            break;
    }
    return $status_payment_type_title;
}

function getPlanningTimeHous($km)
{

    $hour = ceil($km / 70);
    $hour_rest = ceil($hour / 10) * 8;
    $hour = ($hour + $hour_rest);

    return $hour;
}

function getHTMLSecurityService($obj, $type)
{

    $myHTML = '';

    $myHTML = Form::select('status_security_service', \App\Models\Security\Security::STATUS_SECURITY_SERVICE, old('status_security_service'), ['class' => 'form-control status_security_service']);


    return ($myHTML);
}


function getTotalSumToPrice($price, $percent)
{
    $percent = getFloatFormat($percent);
    $price = getFloatFormat($price);
    $sum = ($price / 100) * $percent;
    return getFloatFormat(getPriceFormat($sum));
}


function getPriceFormat($price)
{
    return number_format($price, 2, ',', ' ');
}


function createPageNavigator($total, $pageNumber, $elementsPerPage, $path = '')
{

    $path = preg_replace('/(|&|\?)page=\d+/', '', $path);


    if (substr($path, -1) == '/') {
        $path .= '?';
    } else {
        $path .= '&';
    }

    $out = '';
    if (!$pageNumber) {
        $pageNumber = 1;
    }

    if ($total > $elementsPerPage) { //if all elements couldn't be placed on the page
        //[prev]
        if ($pageNumber > 1) {
            $out .= '<a  href="' . ($path . 'page=' . ($pageNumber - 1)) . '">&lt;&lt; пред.</a> &nbsp;&nbsp;';
        }

        //digital links
        $k = $pageNumber - 1;// $elementsPerPage;

        //not more than 4 links to the left
        $min = $k - 5;
        if ($min < 0) {
            $min = 0;
        } else {
            if ($min >= 1) { //link on the 1st page
                $out .= '<a  href="' . ($path . 'page=1') . '">1</a> &nbsp;&nbsp;';
                if ($min != 1) {
                    $out .= '... &nbsp;';
                }
            }
        }

        for ($i = $min; $i < $k; $i++) {
            $out .= '<a  href="' . ($path . 'page=' . ($i + 1)) . '">' . ($i + 1) . '</a> &nbsp;&nbsp;';
        }

        $out .= '<span class="active">' . ($k + 1) . '</span> &nbsp;&nbsp;';

        //not more than 5 links to the right
        $min = $k + 6;
        if ($min > ceil($total / $elementsPerPage)) {
            $min = ceil($total / $elementsPerPage);
        }

        for ($i = $k + 1; $i < $min; $i++) {
            $out .= '<a  href="' . ($path . 'page=' . ($i + 1)) . '">' . ($i + 1) . '</a> &nbsp;&nbsp;';
        }

        if ($min * $elementsPerPage < $total) { //the last link
            if ($min * $elementsPerPage < $total - $elementsPerPage) {
                $out .= ' ... &nbsp;&nbsp;';
            }

            if (!($total % $elementsPerPage == 0)) {
                $out .= '<a  href="' . ($path . 'page=' . (floor($total / $elementsPerPage) + 1)) . '">' . (floor($total / $elementsPerPage) + 1) . '</a> &nbsp;&nbsp;';
            } else { //$total is divided by $elementsPerPage
                $out .= '<a  href="' . ($path . 'page=' . (floor($total / $elementsPerPage))) . '">' . (floor($total / $elementsPerPage)) . '</a> &nbsp;&nbsp;';
            }

        }

        //[next]
        if ($pageNumber < $total / $elementsPerPage) {
            $out .= '<a href="' . ($path . 'page=' . ($pageNumber + 1)) . '">след. &gt;&gt;</a> ';
        }
    }

    return '<span class="paginator">' . $out . '</span>';
}


/**
 * Сумма прописью
 * @author runcore
 * @url rche.ru
 */
function num2str($inn, $stripkop = false)
{
    $nol = 'ноль';
    $str[100] = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
    $str[11] = array('', 'десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать', 'двадцать');
    $str[10] = array('', 'десять', 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
    $sex = array(
        array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),// m
        array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять') // f
    );
    $forms = array(
        array('копейка', 'копейки', 'копеек', 1), // 10^-2
        array('рубль', 'рубля', 'рублей', 0), // 10^ 0
        array('тысяча', 'тысячи', 'тысяч', 1), // 10^ 3
        array('миллион', 'миллиона', 'миллионов', 0), // 10^ 6
        array('миллиард', 'миллиарда', 'миллиардов', 0), // 10^ 9
        array('триллион', 'триллиона', 'триллионов', 0), // 10^12
    );
    $out = $tmp = array();
    // Поехали!
    $tmp = explode('.', str_replace(',', '.', $inn));
    $rub = number_format((float)$tmp[0], 0, '', '-');
    if ($rub == 0) $out[] = $nol;
    // нормализация копеек
    $kop = isset($tmp[1]) ? substr(str_pad($tmp[1], 2, '0', STR_PAD_RIGHT), 0, 2) : '00';
    $segments = explode('-', $rub);
    $offset = sizeof($segments);
    if ((int)$rub == 0) { // если 0 рублей
        $o[] = $nol;
        $o[] = morph(0, $forms[1][0], $forms[1][1], $forms[1][2]);
    } else {
        foreach ($segments as $k => $lev) {
            $sexi = (int)$forms[$offset][3]; // определяем род
            $ri = (int)$lev; // текущий сегмент
            if ($ri == 0 && $offset > 1) {// если сегмент==0 & не последний уровень(там Units)
                $offset--;
                continue;
            }
            // нормализация
            $ri = str_pad($ri, 3, '0', STR_PAD_LEFT);
            // получаем циферки для анализа
            $r1 = (int)substr($ri, 0, 1); //первая цифра
            $r2 = (int)substr($ri, 1, 1); //вторая
            $r3 = (int)substr($ri, 2, 1); //третья
            $r22 = (int)$r2 . $r3; //вторая и третья
            // разгребаем порядки
            if ($ri > 99) $o[] = $str[100][$r1]; // Сотни
            if ($r22 > 20) {// >20
                $o[] = $str[10][$r2];
                $o[] = $sex[$sexi][$r3];
            } else { // <=20
                if ($r22 > 9) $o[] = $str[11][$r22 - 9]; // 10-20
                elseif ($r22 > 0) $o[] = $sex[$sexi][$r3]; // 1-9
            }
            // Рубли
            $o[] = morph($ri, $forms[$offset][0], $forms[$offset][1], $forms[$offset][2]);
            $offset--;
        }
    }
    // Копейки
    if (!$stripkop) {
        $o[] = $kop;
        $o[] = morph($kop, $forms[0][0], $forms[0][1], $forms[0][2]);
    }
    return preg_replace("/\s{2,}/", ' ', implode(' ', $o));
}

/**
 * Склоняем словоформу
 */
function morph($n, $f1, $f2, $f5)
{
    $n = abs($n) % 100;
    $n1 = $n % 10;
    if ($n > 10 && $n < 20) return $f5;
    if ($n1 > 1 && $n1 < 5) return $f2;
    if ($n1 == 1) return $f1;
    return $f5;
}

function breadcrumb($array)
{
    $res = '<ul class="breadcrumb">';
    $url = "";
    foreach ($array as $item) {
        if (isset($item['label'])) {
            $res .= '<li>';
            if (isset($item['url'])) {
                $url .= '/' . $item['url'];
                $res .= '<a href="' . url($url) . '">' . $item['label'] . '</a>';
            } else {
                $res .= $item['label'];
            }
            $res .= '</li>';
        }
    }
    $res .= '</ul>';
    return $res;
}


function translitClass($str)
{
    $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
    $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
    return strtolower(preg_replace('/[^A-Za-z]+/', '', str_replace($rus, $lat, $str)));
}

function cyrillicDeobfuscation($str)
{
    $rus = array('А', 'В', 'Е', 'К', 'М', 'Н', 'О', 'Р', 'С', 'Т', 'У', 'Х');
    $lat = array('A', 'B', 'E', 'K', 'M', 'H', 'O', 'P', 'C', 'T', 'Y', 'X');
    return preg_replace('/[А-Яа-я]+/', '', str_ireplace($rus, $lat, $str));
}

function declension($str, $case)
{

}

function dynamicColors()
{
    $r = rand(1, 255);
    $g = rand(1, 255);
    $b = rand(1, 255);
    return "rgba(" . $r . "," . $g . "," . $b . ", 0.5)";
}


function divide_by($divider, $count_word, $str)
{

    $arr = explode(' ', $str);

    if (is_array($arr) && count($arr) > 0) {
        $arr = array_chunk($arr, $count_word);
        $arr2 = [];
        foreach ($arr as $v) {
            $arr2[] = implode(' ', $v);
        }

        $str = implode($divider, $arr2);
        return $str;
    }

    return $str;


}


function getLaravelSql($query)
{
    return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
        return is_numeric($binding) ? $binding : "'{$binding}'";
    })->toArray());
}


function parsePhoneNumber($phone)
{
    $phone = str_replace("+", '', $phone);
    $phone = str_replace("(", '', $phone);
    $phone = str_replace(")", '', $phone);
    $phone = str_replace("-", '', $phone);
    $phone = str_replace(" ", '', $phone);

    return $phone;
}

function setPhoneNumberFormat($phone, $format)// d = цифра
{
    $str = '';
    if(strlen($phone) == strlen($format)){
        $set_pos_phone = strlen($phone) - 1;

        for ($i = strlen($format) - 1; $i >= 0; $i--) {
            if ($format[$i] == 'd') {
                $str = (int)$phone[$set_pos_phone] . $str;
                $set_pos_phone--;

            } else $str = $format[$i] . $str;

        }
    }


    return $str;
}


function oldSetPhoneNumberFormat($phone, $format)// d = цифра
{
    $str = '';
    $set_pos_phone = strlen($phone) - 1;

    for ($i = strlen($format) - 1; $i >= 0; $i--) {
        if ($format[$i] == 'd') {
            $str = (int)$phone[$set_pos_phone] . $str;
            $set_pos_phone--;

        } else $str = $format[$i] . $str;

    }


    return $str;
}

function countDayToDates($date_s, $date_e)
{
    return $difference = intval(abs(
            strtotime($date_s) - strtotime($date_e)
        )) / (3600 * 24);
}

function isColored($date, $time)
{
    $colored = $date - $date;
    // if($colored)
    return true;
}

function getSqlSumRow($query, $coloms, $isDeleted = false)
{
    $sql = clone $query;
    if ($isDeleted !== false)
    {
        $sql->where('payments.is_deleted', 0);
    }
    $res = $sql->select(DB::raw("SUM($coloms) as result"))->first();

    return titleFloatFormat($res->result);
}

function isColoRedFoundation($fact, $total)
{
    if($fact > $total){
        return '#cc0000';
    }else{
        return '#408000';
    }

    return '#000';
}

function isColoRedGrin($sum, $not_green = 0)
{

    if($sum < 0){
        return '#cc0000';
    }elseif($not_green == 0)
    {
        return '#408000';
    }

    return '#000';
}


function str_split_by($string, $num, $lines, $current_line = 1){

    $current_line -= 1;
    $empty_arr = [];
    for($i = 0; $i < $lines; $i++){
        $empty_arr[$i] = '';
    }
    $new_arr_string = $empty_arr;
    if($string){
        $new_arr_string = mb_str_split($string, $num);
    }
    return isset($new_arr_string[$current_line]) ? $new_arr_string[$current_line] : $empty_arr[$current_line];
}

function responseStatus($msg, $status = true)
{
    $result = new \stdClass();
    $result->status = $status;
    $result->msg = $msg;
    return $result;
}

function getProcentToSum($payment_total, $price)
{
    return getFloatFormat($price)/(getFloatFormat($payment_total)/100);
}

function getProcentToSumFixFp($payment_total, $price)
{
    return ($price/($payment_total/100));
}

function getColonsExelFile($path)
{
    $xls = \PHPExcel_IOFactory::load($path);

// Устанавливаем индекс активного листа
    $xls->setActiveSheetIndex(0);
// Получаем активный лист
    $sheet = $xls->getActiveSheet();

    $myArr = array();


    for ($i = 0; $i <= 1; $i++) {
        $nColumn = \PHPExcel_Cell::columnIndexFromString(
            $sheet->getHighestColumn());

        for ($c = 0; $c <= $nColumn; $c++) {
            $val = $sheet->getCellByColumnAndRow($c, $i)->getValue();
            if(strlen($val)>0) array_push($myArr, $val);
        }


    }

    return $myArr; //возвращаем массив
}

function stringIsFloatFormar($string)
{
    $res = false;
    $myString = str_replace(' ', '', $string);
    $myString = str_replace(',', '.', $myString);

    $isFloat=preg_match('/^\s*([0-9]+(\.[0-9]+)?)\s*$/', $myString, $myNum);


    if($isFloat){
        if(strripos($string, ',') !== false){
            $res = true;
        }
        if(strripos($string, ',') !== false){
            $res = true;
        }
    }


   return $res;


}

/**
 * ИЗ: Направление ОСАГО №3288377 / 173643 от 12.05.2020 10:00:00 Гордеев Алеександр Васильевич
 * В: 173643
 * @param $order_title string
 * @return string|string[]|null
 */
function isolateOrderNumber($order_title)
{
    return (string)preg_replace('/([А-Яа-я \/]|№[0-9]+|[0-9]{1,}:[0-9]{2}:[0-9]{2}|[0-9]{2}.[0-9]{2}.[0-9]{4})/u', '', $order_title);
}

function parseOrderNumber($order_title)
{
    $title = [];
    preg_match_all('#№(.+?)от#su', $order_title, $title);

    if(isset($title) && isset($title[1]) && isset($title[1][0])){
        return $title[1][0];
    }

    return $order_title;
}

/**
 * Сокращаем входящую строку $str, до $length символов и добавляем ...
 * Пример: "Соркраща.."
 * @param $str
 * @param $length
 * @return string
 */
function cutToLength($str, $length)
{
    return (string)mb_strimwidth($str, 0, $length, '...');
}



function MSSQLQueryResult($sql, $get_one_row = false)
{
    $msSql = new \App\Services\Front\api\KansaltingRITSql();
    $result = $msSql->getQueryResult($sql, $get_one_row);
    $msSql->close();
    return $result;
}