<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentLink extends Mailable
{
    use Queueable, SerializesModels;

    public $payment_link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($payment_link)
    {
        $this->payment_link = $payment_link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.payment.new_payment_link', ['url' => $this->payment_link]);
    }
}
