<?php

namespace App\Mail\Partners;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Error extends Mailable
{
    use Queueable, SerializesModels;

    public $title, $msg;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title, $msg)
    {
        $this->title = $title;
        $this->msg = $msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.partners.error', ['title' => $this->title, 'message' => $this->msg]);
    }
}
