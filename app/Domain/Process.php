<?php

namespace App\Domain;

class Process{

    private static $process = null;

    public $param = [];
    public $errors = [];
    public $warnings = [];

    public static function get_process(){
        if (self::$process === null) {
            $process = new self();
            self::$process = &$process;
        }
        return self::$process;
    }

    /*
     * Params
     */
    public function has($key){
        return isset($this->param[$key]);
    }

    public function get($key){
        return $this->has($key) ? $this->param[$key] : false;
    }

    public function set($key, $val){
        $this->param[$key] = $val;
    }


    /*
     * Errors
     */
    public function has_error($key){
        return isset($this->errors[$key]);
    }

    public function get_error($key){
        return $this->has_error($key) ? $this->errors[$key] : false;
    }

    public function set_error($key, $val){
        $this->errors[$key] = $val;
    }


    /*
     * Warnings
     */
    public function has_warning($key){
        return isset($this->warnings[$key]);
    }

    public function get_warning($key){
        return $this->has_warning($key) ? $this->warnings[$key] : false;
    }

    public function set_warning($key, $val){
        $this->warnings[$key] = $val;
    }

}