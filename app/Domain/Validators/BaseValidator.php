<?php

namespace App\Domain\Validators;

use App\Domain\Process;
use Illuminate\Support\Facades\Validator;

abstract class BaseValidator{

    public $validator;

    public $data    = [];
    public $errors  = [];

    public $valid           = false; //валидация успешна
    public $add_to_process  = false; //добавить в процесс инфу о валидации


    public function __construct($data){
        $this->data = $data;
    }

    abstract public function rules() : array;
    abstract public function messages() : array;


    public function add_to_process(){

        $this->add_to_process = true;

        return $this;

    }


    public function pre_validate(){

        $errors = [];

        return $errors;

    }

    public function validate(){

        $this->validator = Validator::make($this->data, $this->rules(), $this->messages());

        $this->errors = $this->format_errors();

        $this->errors = array_merge($this->pre_validate(), $this->errors);

        if(count($this->errors) == 0){

            $this->valid = true;

            return true;

        }else{

            $this->valid = false;

            if($this->add_to_process){

                $process = Process::get_process();

                $process->errors = array_merge(
                    $process->errors,
                    $this->errors
                );

            }

        }

        return false;

    }


    public function get_errors(int $count = 0){

        if($count > 0){
            return array_slice($this->errors, 0, $count);
        }else{
            return $this->errors;
        }

    }


    /**
     * Приводит ошибки к названиям инпутов
     */
    private function format_errors(){

        $result = [];

        $error_packs = $this->validator->errors()->getMessages();

        if(is_array($error_packs) && count($error_packs)>0){

            foreach($error_packs as $field_key => $errors){

                $name_field = '';

                $field = explode('.', $field_key);

                $name_field .= array_shift($field);

                if(is_array($field) && count($field)>0){

                    foreach ($field as $field_nest){

                        $name_field .= "[{$field_nest}]";

                    }

                }

                $result[$name_field] = $errors;

            }

        }

        return $result;

    }


}
