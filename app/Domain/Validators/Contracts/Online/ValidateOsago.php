<?php

namespace App\Domain\Validators\Contracts\Online;

use App\Domain\Validators\BaseValidator;

class ValidateOsago extends BaseValidator {

    public function rules() : array {

        $rules = [

            'contract.begin_time' => 'required',
            'contract.begin_date' => 'required|date',
            'contract.end_date' => 'required|date',
            'contract.drivers_type' => 'required|integer',

            //объект страхования
            'contract.object.docserie' => 'required|string',
            'contract.object.docnumber' => 'required|string',
            'contract.object.docdate' => 'required|date',
            'contract.object.dk_number' => 'required',
            'contract.object.dk_date' => 'required|date',

        ];


        //если страхователь физ лицо
        if(request('contract.insurer.type') == 0){

            $rules['contract.insurer.fio'] = 'required|string';
            $rules['contract.insurer.sex'] = 'required|integer';
            $rules['contract.insurer.birthdate'] = 'required|date';
            $rules['contract.insurer.address_born'] = 'required|string';
            $rules['contract.insurer.address_register'] = 'required|string';
            $rules['contract.insurer.doc_serie'] = 'required|integer';
            $rules['contract.insurer.doc_number'] = 'required|integer';
            $rules['contract.insurer.doc_date'] = 'required|date';
            $rules['contract.insurer.doc_office'] = 'required';
            $rules['contract.insurer.doc_info'] = 'required|string';

        //если страхователь юр лицо
        }else{

            $rules['contract.insurer.doc_serie'] = 'required|integer';
            $rules['contract.insurer.doc_number'] = 'required|integer';
            $rules['contract.insurer.address_register'] = 'required|string';
            $rules['contract.insurer.address_fact'] = 'required|string';

        }


        //если собственник не страхователь
        if(request('contract.owner.is_insurer') == 0){

            //если физ лицо
            if(request('contract.owner.type') == 0){

                $rules['contract.owner.fio'] = 'required|string';
                $rules['contract.owner.birthdate'] = 'required|date';
                $rules['contract.owner.address_born'] = 'required|string';
                $rules['contract.owner.address_register'] = 'required|string';
                $rules['contract.owner.address_fact'] = 'required|string';
                $rules['contract.owner.doc_serie'] = 'required|integer';
                $rules['contract.owner.doc_number'] = 'required|integer';
                $rules['contract.owner.doc_date'] = 'required|date';
                $rules['contract.owner.doc_office'] = 'required';
                $rules['contract.owner.doc_info'] = 'required|string';

            //если юр лицо
            }else{

                $rules['contract.owner.doc_serie'] = 'required|integer';
                $rules['contract.owner.doc_number'] = 'required|integer';
                $rules['contract.owner.address_register'] = 'required|string';
                $rules['contract.owner.address_fact'] = 'required|string';
                $rules['contract.owner.fio'] = 'required|string';
            }


        }


        //водители если ограниченный список
        if(request('contract.drivers_type') == 0){

            //идём по водителям
            foreach(request('contract.driver', []) as $k => $driver){

                //если водитель как страхователь, то не валидируем фио, др, пол
                if(request("contract.driver.{$k}.same_as_insurer", 0) == 0){
                    $rules["contract.driver.{$k}.fio"] = 'required|string';
                    $rules["contract.driver.{$k}.birth_date"] = 'required|date';
                    $rules["contract.driver.{$k}.sex"] = 'required|digits_between:0,1';
                }

                $rules["contract.driver.{$k}.doc_date"] = 'required|date';
                $rules["contract.driver.{$k}.exp_date"] = 'required|date';
                $rules["contract.driver.{$k}.doc_serie"] = 'required';
                $rules["contract.driver.{$k}.doc_num"] = 'required';
            }



        }


        return $rules;

    }


    public function messages() : array {

        $messages = [

            // водители
            'contract.driver.*.fio.required' => 'Укажите ФИО водителя',
            'contract.driver.*.birth_date.required' => 'Укажите дату рождения водителя',
            'contract.driver.*.doc_date.required' => 'Укажите дату выдачи водительского удостоверения',
            'contract.driver.*.exp_date.required' => 'Укажите водительский стаж',
            'contract.driver.*.doc_num.required' => 'Укажите номер водительского удостоверения',
            'contract.driver.*.doc_serie.required' => 'Укажите серию водительского удостоверения',

            // собственник
            'contract.owner.fio.required' => 'Укажите ФИО собственника',
            'contract.owner.birthdate.required' => 'Укажите дату рождения собственника',
            'contract.owner.address_born.required' => 'Укажите мето рождения собственника',
            'contract.owner.address_register.required' => 'Укажите адрес регистрации собственника',
            'contract.owner.address_fact.required' => 'Укажите фактический адрес собственника',
            'contract.owner.doc_serie.required' => 'Укажите серию документа собственника',
            'contract.owner.docserie.required' => 'Укажите серию документа собственника',
            'contract.owner.doc_number.required' => 'Укажите номер документа собственника',
            'contract.owner.doc_date.required' => 'Укажите дату выдачи документа собственника',
            'contract.owner.doc_office.required' => 'Укажите кем выдан документ собственника',
            'contract.owner.doc_info.required' => 'Укажите кем выдан документ собственника',

            /* страхователь */
            'contract.insurer.fio.required' => 'Укажите ФИО страхователя',
            'contract.insurer.sex.required' => 'Укажите пол страхователя',
            'contract.insurer.birthdate.required' => 'Укажите дату рождения страхователя',
            'contract.insurer.address_born.required' => 'Укажите мето рождения страхователя',
            'contract.insurer.address_register.required' => 'Укажите адрес регистрации страхователя',
            'contract.insurer.doc_serie.required' => 'Укажите серию документа страхователя',
            'contract.insurer.doc_number.required' => 'Укажите номер документа страхователя',
            'contract.insurer.doc_date.required' => 'Укажите дату выдачи документа страхователя',
            'contract.insurer.doc_office.required' => 'Укажите код подразделения',
            'contract.insurer.doc_info.required' => 'Укажите кем выдан документ страхователя',
            'contract.insurer.address_fact.required' => 'Укажите фактический адрес страхователя',

            /* объект страхования */
            'contract.object.docserie.required' => 'Укажите серию документа ТС',
            'contract.object.docnumber.required' => 'Укажите номер документа ТС',
            'contract.object.docdate.required' => 'Укажите дату выдачи документа ТС',
            'contract.object.dk_number.required' => 'Укажите номер диагностической карты',
            'contract.object.dk_date.required' => 'Укажите дату очередного ТО',
            'contract.object.docdate.date' => 'Необходимо указать корректную дату выдачи документа ТС',
            'contract.object.dk_date.date' => 'Необходимо указать корректную дату очередного ТО',


            /* контракт */
            'contract.begin_time.required' => 'Укажите время начала страхования',
            'contract.begin_date.required' => 'Укажите дату начала страхования',
            'contract.end_date.required' => 'Укажите дату окончания страхования',
        ];


        return $messages;
    }


}