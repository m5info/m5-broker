<?php

namespace App\Domain\Validators\Contracts\TempContracts;


use App\Domain\Validators\BaseValidator;

class ValidatePayment extends BaseValidator {


    public function rules(): array {
        return [
            "id" => "integer",
            "payment_number" => "integer",
            "type_id" => "integer",
            "payment_total" => "required",
            "payment_data" => "date",
            "official_discount" => "numeric|between:0.00,99.99",
            "informal_discount" => "numeric|between:0.00,99.99",
            "bank_kv" => "numeric|between:0.00,99.99",
            "payment_type" => "integer",
            "payment_flow" => "integer",
            "pay_method_id" => "integer",
            "bso_receipt" => "string",
            "bso_receipt_id" => "integer",
            "check_phone" => "size:18",
            "check_email" => "email",
            "when_to_send" => "integer",
        ];
    }

    public function messages(): array {
        return [
            "payment_total.required" => "Введите сумма платежа",
            "check_phone.size" => "Введите корректный номер телефона",
        ];
    }


}