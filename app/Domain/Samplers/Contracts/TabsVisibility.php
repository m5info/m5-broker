<?php

namespace App\Domain\Samplers\Contracts;

use App\Models\Users\Permission;
use DB;

class TabsVisibility{

    public static function get_temp_contracts_tab_visibility(){

        $permission_id = Permission::query()->where('title', 'temp_contracts')->first()->id;

        $role_id = auth()->user()->role->id;

        $sql = "select * from subpermissions 
            join roles_subpermissions on subpermissions.id = roles_subpermissions.subpermission_id
            where roles_subpermissions.permission_id = {$permission_id} and roles_subpermissions.role_id = {$role_id}";

        $subpermissions = collect(DB::select($sql))->keyBy('title');


        $permission2status = [
            "temp" => 0,
            "in_check" => 1,
            "in_correction" => 2,
            "in_pay" => 3,
            "in_cashbox" => 5,
            "in_confirm" => 6,
            'in_close_correction' => 7,
        ];

        $list = [];

        foreach ($permission2status as $subp => $status_id){
            $perm = ['edit' => 0, 'view' => 0];
            if(isset($subpermissions[$subp])){
                $perm['edit'] = $subpermissions[$subp]->edit;
                $perm['view'] = $subpermissions[$subp]->view;
            }
            $list[$status_id] = $perm;
        }

        return $list;

    }

    public static function get_orders_tab_visibility(){

        $permission_id = Permission::query()->where('title', 'contract_orders')->first()->id;

        $role_id = auth()->user()->role->id;

        $sql = "select * from subpermissions 
            join roles_subpermissions on subpermissions.id = roles_subpermissions.subpermission_id
            where roles_subpermissions.permission_id = {$permission_id} and roles_subpermissions.role_id = {$role_id}";

        $subpermissions = collect(DB::select($sql))->keyBy('title');


        $permission2status = [
            "temp" => 0,
            "in_frame" => 1,
            "in_print" => 2,
            "in_delivery" => 3,
            "in_realized" => 4,
            "in_archive" => 5,
        ];

        $list = [];

        foreach ($permission2status as $subp => $status_id){
            $perm = ['edit' => 0, 'view' => 0];
            if(isset($subpermissions[$subp])){
                $perm['edit'] = $subpermissions[$subp]->edit;
                $perm['view'] = $subpermissions[$subp]->view;
            }
            $list[$status_id] = $perm;
        }

        return $list;
    }


    public static function get_orders_select_visibility(){

        $permission_id = Permission::query()->where('title', 'order_statuses')->first()->id;

        $role_id = auth()->user()->role->id;

        $sql = "select * from subpermissions 
            join roles_subpermissions on subpermissions.id = roles_subpermissions.subpermission_id
            where roles_subpermissions.permission_id = {$permission_id} and roles_subpermissions.role_id = {$role_id}";

        $subpermissions = collect(DB::select($sql))->keyBy('title');

        $list = [];

        $permission2status = [
            "temp" => 0,
            "in_frame" => 1,
            "in_print" => 2,
            "in_delivery" => 3,
            "in_realized" => 4,
        ];

        foreach ($subpermissions as $title => $status_id){
            $list[$permission2status[$title]] = \App\Models\Contracts\Orders::STATUSES[$permission2status[$title]];
        }

        return $list;
    }

    public static function get_inspection_orders_tab_visibility(){

        $permission_id = Permission::query()->where('title', 'inspections')->first()->id;

        $role_id = auth()->user()->role->id;

        $sql = "select * from subpermissions 
            join roles_subpermissions on subpermissions.id = roles_subpermissions.subpermission_id
            where roles_subpermissions.permission_id = {$permission_id} and roles_subpermissions.role_id = {$role_id}";

        $subpermissions = collect(DB::select($sql))->keyBy('title');


        $permission2status = [
            "temp_orders" => 0,
            "in_distribution" => 1,
            "in_work_orders" => 2,
            "in_check_orders" => 3,
            "agree_orders" => 4,
            "archive_orders" => 5,
        ];

        $list = [];

        foreach ($permission2status as $subp => $status_id){
            $perm = ['edit' => 0, 'view' => 0];
            if(isset($subpermissions[$subp])){
                $perm['edit'] = $subpermissions[$subp]->edit;
                $perm['view'] = $subpermissions[$subp]->view;
            }
            $list[$status_id] = $perm;
        }

        return $list;
    }


    public static function get_invoices_tab_visibility(){

        $perm = Permission::where('title', '=', 'invoices_type_view')->get()->first();

        $permission_id = $perm->id;
        $role_id = auth()->user()->role->id;

        $sql = "select * from subpermissions 
            join roles_subpermissions on subpermissions.id = roles_subpermissions.subpermission_id
            where roles_subpermissions.permission_id = {$permission_id} and roles_subpermissions.role_id = {$role_id}";

        $subpermissions = collect(DB::select($sql))->keyBy('title');


        $permission2status = [
            "all" => 0,
            "nalich" => 1,
            "beznalich" => 2,
            "sk" => 3,
        ];

        $list = [];

        foreach ($permission2status as $subp => $status_id){
            $perm = ['view' => 0];
            if(isset($subpermissions[$subp])){
                $perm['view'] = $subpermissions[$subp]->view;
            }
            $list[$status_id] = $perm;
        }

        return $list;
    }

    public static function get_analytics_visibility()
    {
        $permission_id = Permission::query()->where('title', 'analitics_common')->first()->id;
        $role_id = auth()->user()->role->id;

        $sql = "select * from subpermissions 
            join roles_subpermissions on subpermissions.id = roles_subpermissions.subpermission_id
            where roles_subpermissions.permission_id = {$permission_id} and roles_subpermissions.role_id = {$role_id}";

        $subpermissions = collect(DB::select($sql))->keyBy('title');

        return $subpermissions;
    }

    public static function get_verification_tab_visibility()
    {
        $permission_id = Permission::query()->where('title', 'verification')->first()->id;
        $role_id = auth()->user()->role->id;

        $sql = "select * from subpermissions 
            join roles_subpermissions on subpermissions.id = roles_subpermissions.subpermission_id
            where roles_subpermissions.permission_id = {$permission_id} and roles_subpermissions.role_id = {$role_id}";

        $subpermissions = collect(DB::select($sql))->keyBy('title');


        $permission2status = [
            "temp" => 0,
            "in_check" => 1,
            "in_correction" => 2,
            "in_pay" => 3,
            "in_cashbox" => 5,
            "in_confirm" => 6,
            'in_close_correction' => 7,

        ];

        $list = [];

        foreach ($permission2status as $subp => $status_id){
            $perm = ['edit' => 0, 'view' => 0];
            if(isset($subpermissions[$subp])){
                $perm['edit'] = $subpermissions[$subp]->edit;
                $perm['view'] = $subpermissions[$subp]->view;
            }
            $list[$status_id] = $perm;
        }

        return $list;

    }


}