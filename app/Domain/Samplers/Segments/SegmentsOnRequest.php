<?php

namespace App\Domain\Samplers\Segments;

use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\FinancialPolicySegment;

class SegmentsOnRequest{



    public static function get_suitable_product_segment($contract_id, $product_id){


        $fp = FinancialPolicy::query();

        $fp->where('is_actual', 1);
        $fp->where('product_id', $product_id);

        $fp->whereIn('bso_supplier_id', function($query)
        {
            $query->select(\DB::raw('api_settings.bso_supplier_id'))
                ->from('api_settings')
                ->where('api_settings.is_actual', 1);

        });


        $result = [];

        foreach ($fp->get() as $actually){

            if(sizeof($actually->segments)){

                foreach ($actually->segments as $segments)
                {
                    if($segments->checkSegmentContract($contract_id) == true)
                    {
                        $result[] = [
                            'id' => $actually->id,
                            'bso_supplier_id' => $actually->bso_supplier_id,
                            'insurance_companies_id' => $actually->insurance_companies_id,
                            'kv_sk' => $actually->kv_sk];
                    }
                }

            }else{
                $result[] = [
                    'id' => $actually->id,
                    'bso_supplier_id' => $actually->bso_supplier_id,
                    'insurance_companies_id' => $actually->insurance_companies_id,
                    'kv_sk' => $actually->kv_sk];
            }


        }


        if (count($result) > 0) {
            usort($result, function ($a, $b) {
                return ($b['kv_sk'] - $a['kv_sk']);
            });
        }


        $unique = collect($result)->unique('id');
        $unique = collect($unique)->unique(function ($item) {
            return $item['bso_supplier_id'].$item['insurance_companies_id'];
        });


        return $unique;
    }


    public static function get_suitable_companies_segment($calculation)
    {


        $fp = FinancialPolicy::query();

        $fp->where('is_actual', 1);
        $fp->where('product_id', $calculation->product_id);
        $fp->where('insurance_companies_id', $calculation->insurance_companies_id);

        $fp->whereIn('bso_supplier_id', function($query)
        {
            $query->select(\DB::raw('api_settings.bso_supplier_id'))
                ->from('api_settings')
                ->where('api_settings.is_actual', 1);

        });

        $result = [];

        foreach ($fp->get() as $actually){

            if(sizeof($actually->segments)){

                foreach ($actually->segments as $segments)
                {
                    if($segments->checkSegmentContract($contract_id) == true)
                    {
                        $result[] = [
                            'id' => $actually->id,
                            'bso_supplier_id' => $actually->bso_supplier_id,
                            'insurance_companies_id' => $actually->insurance_companies_id,
                            'kv_sk' => $actually->kv_sk];
                    }
                }

            }else{
                $result[] = [
                    'id' => $actually->id,
                    'bso_supplier_id' => $actually->bso_supplier_id,
                    'insurance_companies_id' => $actually->insurance_companies_id,
                    'kv_sk' => $actually->kv_sk];
            }


        }


        if (count($result) > 0) {
            usort($result, function ($a, $b) {
                return ($b['kv_sk'] - $a['kv_sk']);
            });
        }


        $unique = collect($result)->unique('id');
        $unique = collect($unique)->unique(function ($item) {
            return $item['bso_supplier_id'].$item['insurance_companies_id'];
        });


        return $unique;


    }

}