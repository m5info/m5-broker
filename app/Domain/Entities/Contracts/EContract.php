<?php

namespace App\Domain\Entities\Contracts;

use App\Domain\Entities\Entity;
use App\Helpers\EmptyObject;
use App\Models\Contracts\Contracts;
use App\Models\User;


class EContract extends Entity {

    public static $model = Contracts::class;


    /**
     * Создать или обновить договор из первички
     */
    public static function update_or_create_from_primary($contract, $data){

        $contract = EContract::get_first($contract);

        $agent = $data->agent_id ? User::find((int)$data->agent_id) : (object)[];

        $old_contract = $contract ? $contract : new EmptyObject();

        $update_data = [

            'type_id' => 1,
            'statys_id' => 0,
            'user_id' => auth()->id(),

            'agent_id' => isset($data->agent_id) ? $data->agent_id : $old_contract->agent_id,
            'parent_agent_id' => isset($agent->parent_id) ? $agent->parent_id : $old_contract->parent_agent_id,

            'bso_id' => isset($data->bso_id) ? $data->bso_id : $old_contract->bso_id,
            'bso_title' => isset($data->bso_title) ? $data->bso_title : $old_contract->bso_title,
            'product_id' => isset($data->product_id) ? $data->product_id : $old_contract->product_id,
            'program_title' => isset($data->program_title) ? $data->program_title : $old_contract->program_title,
            'bso_supplier_id' => isset($data->bso_supplier_id) ? $data->bso_supplier_id : $old_contract->bso_supplier_id,
            'insurance_companies_id' => isset($data->insurance_companies_id) ? $data->insurance_companies_id : $old_contract->insurance_companies_id,
            'installment_algorithms_id' => isset($data->installment_algorithms_id) && $data->installment_algorithms_id != '' ? $data->installment_algorithms_id : $old_contract->installment_algorithms_id,
            'bank_id' => isset($data->bank_id) ? $data->bank_id : $old_contract->bank_id,
            'order_title' => isset($data->order_title) ? $data->order_title : $old_contract->order_title,
            'order_id' => isset($data->order_id) ? $data->order_id : $old_contract->order_id,
            'order_sort_id' => isset($data->order_sort_id) ? $data->order_sort_id : $old_contract->order_sort_id,
            'sales_condition' => isset($data->sales_condition) ? $data->sales_condition : $old_contract->sales_condition,

            'sign_date' => isset($data->sign_date) ? setDateTimeFormat($data->sign_date) : $old_contract->sign_date,
            'begin_date' => isset($data->begin_date) ? setDateTimeFormat($data->begin_date) : $old_contract->begin_date,
            'end_date' => isset($data->end_date) ? setDateTimeFormat($data->end_date) : $old_contract->end_date,

            'payment_total' => isset($data->payment_total) ? getFloatFormat($data->payment_total) : $old_contract->payment_total,
            'insurance_amount' => isset($data->payment_total) ? getFloatFormat($data->payment_total) : $old_contract->payment_total,

            'comment' => isset($data->comment) ? $data->comment : $old_contract->comment,
        ];

        if(!$contract){
            $contract = Contracts::create($update_data);
        }elseif($contract->satatys_id != 4){
            $contract->update($update_data);
        }

        return $contract;

    }


    /**
     * Обновить временный договор
     */
    public static function update_from_temp_contract($contract, $data){

        $contract = EContract::get_first($contract);

        $update_data = [
            'bso_id' => isset($data->bso_id) ? $data->bso_id : $contract->bso_id,
            'program_title' => isset($data->program_title) ? $data->program_title : $contract->program_title,
            'order_title' => isset($data->order_title) ? $data->order_title : $contract->order_title,
            'order_id' => isset($data->order_id) ? $data->order_id : $contract->order_id,
            'order_sort_id' => isset($data->order_sort_id) ? $data->order_sort_id : $contract->order_sort_id,
            'sales_condition' => isset($data->sales_condition) ? $data->sales_condition : $contract->sales_condition,
            'sign_date' => isset($data->sign_date) ? setDateTimeFormat($data->sign_date) : $contract->sign_date,
            'begin_date' => isset($data->begin_date) ? setDateTimeFormat($data->begin_date) : $contract->begin_date,
            'end_date' => isset($data->end_date) ? setDateTimeFormat($data->end_date) : $contract->end_date,
            'payment_total' => isset($data->payment_total) ? getFloatFormat($data->payment_total) : $contract->payment_total,
            'installment_algorithms_id' => isset($data->installment_algorithms_id) ? $data->installment_algorithms_id : $contract->installment_algorithms_id,
            'type_id' => isset($data->type_id) ? $data->type_id : $contract->type_id,
            'insurance_amount' => isset($data->insurance_amount) ? getFloatFormat($data->insurance_amount) : $contract->insurance_amount,
            'bank_id' => isset($data->bank_id) ? $data->bank_id : $contract->bank_id,
        ];

        $contract->update($update_data);

        return $contract;

    }

}