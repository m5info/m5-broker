<?php

namespace App\Domain\Entities;

use Illuminate\Database\Eloquent\Builder;

class Entity{

    public static $model;

    public static function get_first($entity){

        $called_class = get_called_class();

        if($entity instanceof $called_class::$model){
            return $entity;
        }

        if($entity instanceof Builder){
            return $entity->first();
        }

        if($entity > 0){
            return $called_class::$model::find((int)$entity);
        }

        return false;
    }


    public static function get_all($entity){

        $called_class = get_called_class();

        if($entity instanceof $called_class::$model){
            return collect([$entity]);
        }

        if($entity instanceof Builder){
            return $entity->get();
        }

        if(is_countable($entity)){
            return $called_class::$model::whereIn('id', collect($entity));
        }

        return false;

    }


}