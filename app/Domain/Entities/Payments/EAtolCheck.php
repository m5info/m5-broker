<?php

namespace App\Domain\Entities\Payments;

use App\Domain\Entities\Entity;
use App\Domain\Integrations\Atol\Atol;
use App\Models\Api\Atol\AtolCheck;



class EAtolCheck extends Entity{

    public static $model = AtolCheck::class;

    /**
     * Создание чека АТОЛ
     */
    public static function create($data){

        $atol_client = new Atol();

        $required_data = collect([
            'product_price' => '',
            'client_email' => '',
            'client_phone' => '',
            'date' => date('Y-m-d H:i:s', time()),
        ]);

        $data = (object)$required_data->merge($data)->all();

        $atol_check = $atol_client->createcheck(
            "Полис",
            titleFloatFormat($data->product_price),
            date("Y-m-d H:i:s"),
            $data->client_email,
            $data->client_phone
        );

        return $atol_check;

    }


}