<?php

namespace App\Domain\Entities\Payments;

use App\Domain\Entities\Contracts\EContract;
use App\Domain\Entities\Entity;
use App\Helpers\EmptyObject;
use App\Models\Contracts\Payments;
use App\Models\Finance\PayMethod;
use App\Models\User;

class EPayment extends Entity{

    public static $model = Payments::class;

    /**
     * Обновление временного платежа
     */
    public static function update_temp($payment, $data){

        if($payment = EPayment::get_first($payment)){

            if(!$payment->invoice_id || $payment->invoice->status_id !=2){

                $payment_type = $payment->payment_type;
                $payment_flow = $payment->payment_flow;

                if(isset($data->pay_method_id)){
                    $method = PayMethod::find($data->pay_method_id);

                    $payment_type = $method->payment_type;
                    $payment_flow = $method->payment_flow;
                }

                $payment->update([

                    "bso_id" => isset($payment->contract) ? (int)$payment->contract->bso_id : 0,
                    "org_id" => isset($payment->contract) && isset($payment->contract->bso) ? (int)$payment->contract->bso->org_id : 0,

                    "payment_number" => isset($data->payment_number) ? (int)$data->payment_number : $payment->payment_number,
                    "type_id" => isset($data->type_id) ? (int)$data->type_id : $payment->type_id,
                    "order_source" => isset($data->order_source) ? $data->order_source : $payment->order_source,
                    "payment_total" => isset($data->payment_total) ? getFloatFormat($data->payment_total) : $payment->payment_total,
                    "payment_data" => isset($data->payment_data) ? getDateFormatEn($data->payment_data) : $payment->payment_data,
                    "delivery_date" => isset($data->delivery_date) ? getDateFormatEn($data->delivery_date) : $payment->delivery_date,
                    "official_discount" => isset($data->official_discount) ? getFloatFormat($data->official_discount) : $payment->official_discount,
                    "informal_discount" => isset($data->informal_discount) ? getFloatFormat($data->informal_discount) : $payment->informal_discount,
                    "bank_kv" => isset($data->bank_kv) ? getFloatFormat($data->bank_kv) : $payment->bank_kv,
                    "payment_type" => $payment_type,
                    "payment_flow" => $payment_flow,
                    "bso_receipt" => isset($data->bso_receipt) ? $data->bso_receipt : $payment->bso_receipt,
                    "bso_receipt_id" => isset($data->bso_receipt_id) ? (int)$data->bso_receipt_id : $payment->bso_receipt_id,
                    "pay_method_id" => isset($data->pay_method_id) ? (int)$data->pay_method_id : $payment->pay_method_id,
                ]);

            }else{
                if($payment->contract->sales_condition == 1){

                    //dd($data);
                    $payment->update([
                        "payment_number" => isset($data->payment_number) ? (int)$data->payment_number : $payment->payment_number,
                        "type_id" => isset($data->type_id) ? (int)$data->type_id : $payment->type_id,
                        "order_source" => isset($data->order_source) ? $data->order_source : $payment->order_source,
                        "payment_data" => isset($data->payment_data) ? getDateFormatEn($data->payment_data) : $payment->payment_data,
                        "delivery_date" => isset($data->delivery_date) ? getDateFormatEn($data->delivery_date) : $payment->delivery_date,
                        "official_discount" => isset($data->official_discount) ? getFloatFormat($data->official_discount) : $payment->official_discount,
                        "informal_discount" => isset($data->informal_discount) ? getFloatFormat($data->informal_discount) : $payment->informal_discount,
                        "bank_kv" => isset($data->bank_kv) ? getFloatFormat($data->bank_kv) : $payment->bank_kv,
                    ]);
                }
            }

            /*
            if(!$payment->invoice_id){

                $payment->update([
                    "payment_number" => isset($data->payment_number) ? (int)$data->payment_number : $payment->payment_number,
                    "type_id" => isset($data->type_id) ? (int)$data->type_id : $payment->type_id,
                    "payment_total" => isset($data->payment_total) ? getFloatFormat($data->payment_total) : $payment->payment_total,
                    "payment_data" => isset($data->payment_data) ? getDateFormatEn($data->payment_data) : $payment->payment_data,
                    "official_discount" => isset($data->official_discount) ? getFloatFormat($data->official_discount) : $payment->official_discount,
                    "informal_discount" => isset($data->informal_discount) ? getFloatFormat($data->informal_discount) : $payment->informal_discount,
                    "bank_kv" => isset($data->bank_kv) ? getFloatFormat($data->bank_kv) : $payment->bank_kv,
                    "payment_type" => isset($data->payment_type) ? (int)$data->payment_type : $payment->payment_type,
                    "payment_flow" => isset($data->payment_flow) ? (int)$data->payment_flow : $payment->payment_flow,
                    "bso_receipt" => isset($data->bso_receipt) ? $data->bso_receipt : $payment->bso_receipt,
                    "bso_receipt_id" => isset($data->bso_receipt_id) ? (int)$data->bso_receipt_id : $payment->bso_receipt_id,
                    "pay_method_id" => isset($data->pay_method_id) ? (int)$data->pay_method_id : $payment->pay_method_id,
                ]);

            }elseif ($payment->contract->sales_condition == 1){

                $payment->update([
                    "payment_number" => isset($data->payment_number) ? (int)$data->payment_number : $payment->payment_number,
                    "type_id" => isset($data->type_id) ? (int)$data->type_id : $payment->type_id,
                    "payment_data" => isset($data->payment_data) ? getDateFormatEn($data->payment_data) : $payment->payment_data,
                    "official_discount" => isset($data->official_discount) ? getFloatFormat($data->official_discount) : $payment->official_discount,
                    "informal_discount" => isset($data->informal_discount) ? getFloatFormat($data->informal_discount) : $payment->informal_discount,
                    "bank_kv" => isset($data->bank_kv) ? getFloatFormat($data->bank_kv) : $payment->bank_kv,
                ]);

            }
            */

        }

        return $payment;

    }


    /**
     * Создание временного платежа
     */
    public static function create_temp($contract){

        if($contract = EContract::get_first($contract)) {

            $updated_data = [
                'payment_number' => $contract->all_payments()->count() + 1,
                'bso_id' => $contract->bso_id,
                'statys_id' => -1,
                'contract_id' => $contract->id,
                'agent_id' => $contract->agent_id,
                'manager_id' => $contract->manager_id,
                'org_id' => $contract->bso ? $contract->bso->org_id : false,
                'accept_user_id' => auth()->id(),
                'accept_date' => date("Y-m-d H:i:s"),
                'order_id' => $contract->order_id,
                'order_title' => $contract->order_title,
            ];

            if(!empty($updated_data['bso_id'])){
                $payment = Payments::query()
                    ->where("payment_number", $updated_data['payment_number'])
                    ->where("bso_id", $updated_data['bso_id'])
                    ->first();
            }else{ // если у платежа нет бсо (контракт созданный из заявок)
                $payment = Payments::query()
                    ->where("payment_number", $updated_data['payment_number'])
                    ->where("contract_id", $contract->id)
                    ->first();
            }

            if(!$payment){

                $payment = Payments::create($updated_data);

                return $payment;
            }

        }

        return false;
    }


    /**
     * Создание платежа для первички
     */
    public static function create_primary($contract, $data){

        if($contract = EContract::get_first($contract)){
            $bso_id = isset($contract->bso_id) ? (int)$contract->bso_id : '';
            $payment_number = isset($data->payment_number) ? (int)$data->payment_number : '';

            $payment = Payments::query()
                ->where('bso_id', $bso_id)
                ->where('payment_number', $payment_number)
                ->where('is_deleted', 0)
                //->where('statys_id', '!=', -2)
                ->first();

            $old_payment = $payment ? $payment : new EmptyObject();

            $payment_type = $old_payment->payment_type;
            $payment_flow = $old_payment->payment_flow;

            if(isset($data->pay_method_id)){
                $method = PayMethod::find($data->pay_method_id);

                $payment_type = $method->payment_type;
                $payment_flow = $method->payment_flow;
            }

            $referencer_kv = 0;
            if (isset($data->referencer_id) && (int)$data->referencer_id) {
                if ($referencer = User::find($data->referencer_id)){
                    $referencer_kv = $referencer->referencer_kv;
                }
            } else {
                if ($referencer = User::find($contract->referencer_id)){
                    $referencer_kv = $referencer->referencer_kv;
                }
            }

            $updated_data = [

                "bso_id" => $bso_id,
                "payment_number" => $payment_number,

                "statys_id" => 0,
                "is_deleted" => 0,
                'accept_user_id' => auth()->id(),
                'accept_date' => date("Y-m-d H:i:s"),

                "type_id" => isset($data->type_id) ? (int)$data->type_id : $old_payment->type_id,
                "order_source" => isset($data->order_source) ? $data->order_source : $old_payment->order_source,
                "payment_total" => isset($data->payment_total) ? getFloatFormat($data->payment_total) : $old_payment->payment_total,
                'official_discount' => isset($data->official_discount) ? getFloatFormat($data->official_discount) : $old_payment->official_discount,
                'bank_kv' => isset($data->bank_kv) ? getFloatFormat($data->bank_kv) : $old_payment->bank_kv,
                "payment_data" => isset($data->payment_data) ? getDateFormatEn($data->payment_data) : $old_payment->payment_data,
                "informal_discount" => isset($data->informal_discount) ? getFloatFormat($data->informal_discount) : $old_payment->informal_discount,
                "pay_method_id" => isset($data->pay_method_id) ? (int)$data->pay_method_id : 0,
                "payment_type" => $payment_type,
                "payment_flow" => $payment_flow,
                "bso_receipt_id" => isset($data->bso_receipt_id) ? (int)$data->bso_receipt_id : $old_payment->bso_receipt_id,

                'parent_agent_id' => $contract->parent_agent_id,
                'point_sale_id' => $contract->bso->point_sale_id,
                'manager_id' => $contract->manager_id,
                'referencer_id' => isset($contract->referencer_id)? $contract->referencer_id :'',
                'referencer_kv' => $referencer_kv,
                'agent_id' => $contract->agent_id,
                'org_id' => $contract->bso->org_id,
                "contract_id" => $contract->id,
                'order_id' => $contract->order_id,
                'order_title' => $contract->order_title,
                'order_sort_id' => isset($contract->order_sort_id) ? $contract->order_sort_id : $old_payment->order_sort_id,

            ];


            if($payment){
                $payment->update($updated_data);
            }else{
                $payment = Payments::create($updated_data);
            }

            return $payment;

        }

        return false;

    }
}