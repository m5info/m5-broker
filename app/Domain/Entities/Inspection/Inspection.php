<?php

namespace App\Domain\Entities\Inspection;

use App\Domain\Entities\Entity;
use App\Helpers\EmptyObject;
use App\Models\Contracts\Contracts;
use App\Models\Orders\InspectionOrders;
use App\Models\User;


class Inspection extends Entity {

//    public static $model = InspectionOrders::class;
    public static $model = Contracts::class;

}