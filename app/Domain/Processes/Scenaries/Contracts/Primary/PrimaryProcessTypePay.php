<?php

namespace App\Domain\Processes\Scenaries\Contracts\Primary;

use App\Domain\Entities\Contracts\EContract;
use App\Domain\Processes\Operations\Contracts\ContractStatus;
use App\Domain\Processes\Operations\Payments\PaymentAtolCheck;


class PrimaryProcessTypePay {

    public static function handle($contract, $data){

        $required_data = collect([
            'pay_method_id' => false,
            'check_phone' => false,
            'check_email' => false,
            'when_to_send' => false,
        ]);

        $data = (object)$required_data->merge($data)->all();

        $contract = EContract::get_first($contract);

        if($data->pay_method_id == 1){ // Чек

            if($data->when_to_send == 0){ // Сейчас

                foreach($contract->all_payments as $payment){

                    PaymentAtolCheck::create_to_payment($payment, [
                        'product_price' => $payment->payment_total,
                        'client_email' => $data->check_email,
                        'client_phone' => $data->check_phone,
                    ]);
                }

            }else{ // Потом

                ContractStatus::set($contract, 6);

            }

        }


        return $contract;
    }


}