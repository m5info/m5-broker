<?php

namespace App\Domain\Processes\Scenaries\Contracts\Primary;

use App\Domain\Entities\Bso\EBsoItem;
use App\Domain\Entities\Contracts\EContract;
use App\Domain\Processes\Operations\Bso\BsoContract;
use App\Domain\Processes\Operations\Bso\BsoStateOperations;
use App\Domain\Processes\Operations\Contracts\ContractAccept;
use App\Domain\Processes\Operations\Contracts\ContractStatus;


class PrimaryAcceptContract {

    public static function handle($contract){

        $contract = EContract::get_first($contract);

        $bso_item = EBsoItem::get_first($contract->bso);

        //по умолчанию тип акцепта "проверка"
        $kind_acceptance = 3;

        if ($contract->statys_id != 4 || $contract->kind_acceptance != 1) {

            //если выбрано не проверять полис
            if($hold_kv = $contract->bso->supplier->hold_kv_product($contract->product_id)){
                $kind_acceptance = ($hold_kv->is_check_policy == 0) ? 1 : $kind_acceptance;
            }

            //завязываем бсо с контрактом в обе стороны
            BsoContract::attach($bso_item, $contract);

            //продаём бсо
            BsoStateOperations::set($bso_item, 2);

            //акцептуем
            ContractAccept::set($contract, $kind_acceptance);

            //отправляем контракт на оплату
            ContractStatus::set($contract, 3);

        }

        return $contract;
    }

}