<?php

namespace App\Domain\Processes\Scenaries\Contracts\Primary;

use App\Domain\Entities\Contracts\EContract;
use App\Domain\Entities\Payments\EPayment;
use App\Domain\Processes\Operations\Payments\ExpectedPayment;
use App\Domain\Processes\Operations\Payments\PaymentAcceptOperations;
use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Domain\Processes\Operations\Payments\PaymentMethodPay;
use App\Domain\Processes\Operations\Payments\PaymentReceipt;
use App\Models\Contracts\Payments;
use App\Models\User;


class PrimaryProcessPayments {

    public static function handle($contract, $data){

        $contract = EContract::get_first($contract);

        $data = (object) $data;

        if(isset($data->payment)){

            foreach($data->payment as $payment_data){

                $payment_data = (object)$payment_data;

                $payment = null;

                $temp_payment = Payments::where('contract_id', $contract->id)
                    ->where('payment_number', $payment_data->payment_number)
                    ->where('is_deleted', 0)
                    ->whereIn('statys_id', [0, 1, 2])
                    ->get()->first();



                if ($temp_payment){
                    continue;
                }

                if($payment = EPayment::create_primary($contract, $payment_data)){

                    //актуализируем фин политики в соответствии с контрактом
                    $payment = PaymentFinancialPolicy::actualize($payment);

                    if(isset($data->second_payment) && (int)$data->second_payment){

                        $agent = User::find($data->agent_id);
                        $parent = $agent->parent;

                        if($contract->sales_condition != 0 && isset($data->manager_id) && (int)$data->manager_id > 0){
                            $manager = User::find($data->manager_id);
                            $parent = $manager->parent;
                        }

                        //актуализируем фин политики в соответствии с контрактом
                        $payment = PaymentFinancialPolicy::actualizeByFirstPayment($payment);

                        $referencer_kv = 0;
                        if (isset($data->referencer_id) && (int)$data->referencer_id) {
                            if ($referencer = User::find($data->referencer_id)){
                                $referencer_kv = $referencer->referencer_kv;
                            }
                        } else {
                            if ($referencer = User::find($contract->referencer_id)){
                                $referencer_kv = $referencer->referencer_kv;
                            }
                        }
                        $payment->update([
                            'order_title' => isset($data->order_title) ? $data->order_title : '',
                            'order_id' => isset($data->order_id) ? $data->order_id : 0,
                            'order_sort_id' => isset($data->order_sort_id) ? $data->order_sort_id : '',
                            'agent_id' => isset($data->agent_id) ? $data->agent_id : $contract->agent_id,
                            'parent_agent_id' => isset($parent) ? $parent->id : $contract->parent_agent_id,
                            'manager_id' => isset($data->manager_id) ? $data->manager_id : $contract->manager_id,
                            'referencer_id' => isset($data->referencer_id) ? $data->referencer_id : $contract->referencer_id,
                            'referencer_kv' => $referencer_kv,
                            'statys_id' => 0
                        ]);
                    }

                    $payment->delivery_date = ( strlen($data->delivery_date) > 5 ) ? date("Y-m-d", strtotime($data->delivery_date))   : null;

                    $payment->point_sale_id = $contract->bso->point_sale_id;
                    $payment->save();
                    //устанавливаем метод оплаты
                    if(isset($payment_data->pay_method_id)){
                        $payment = PaymentMethodPay::set($payment, $payment_data->pay_method_id);
                    }

                    //пересчитываем скидки для платежа
                    $payment = PaymentDiscounts::recount($payment);

                    //цепляем квитанцию
                    $payment = PaymentReceipt::attach($payment, $payment_data);

                    //создаем или обновляем акцепты на платежи
                    $payment = PaymentAcceptOperations::update_or_create($payment);

                }

            }

        }

        return $contract;
    }


}