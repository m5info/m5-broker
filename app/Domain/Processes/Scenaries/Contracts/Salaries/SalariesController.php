<?php
namespace App\Domain\Processes\Scenaries\Contracts\Salaries;


use App\Models\Delivery\Delivery;
use App\Models\Delivery\DeliveryCost;

class SalariesController
{
    public static function setSalaries($payment){

        $user = $payment->agent;

        if($payment->delivery_date && strlen($payment->delivery_date) > 5 && $user->is('courier')){

            $salaries = Delivery::where('contract_id', $payment->contract_id)
                ->where('payment_id', $payment->id)
                ->get()->first();

            if(!$salaries){
                $price = DeliveryCost::where('product_id', $payment->contract->product_id)
                    ->where('is_actual', 1)
                    ->get()->first();

                if($price){
                    $day = (date('N', strtotime($payment->delivery_date)) >= 6) ? 1 : 0 ; // Узнаём выходной или нет ?
//dd($user->id);
                    Delivery::create([
                        'user_id'=>$user->id,
                        'parent_id'=>$user->parent_id,
                        'delivery_date'=>$payment->delivery_date,
                        'contract_id'=>$payment->contract_id,
                        'payment_id'=>$payment->id,
                        'delivery_cost'=>($day == 0) ? $price->delivery_price : $price->weekend_price,
                        'weekend'=>$day,
                    ]);

                    return true;
                }
            }
        }

        return false;
    }

    public static function destroySalaries($payment){

        $delivery = Delivery::where('contract_id', $payment->contract_id)->first();

        $delivery->update([
            'status' => 2
        ]);

        return true;
    }
}
?>