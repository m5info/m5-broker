<?php

namespace App\Domain\Processes\Scenaries\Contracts\TempContracts;

use App\Domain\Entities\Contracts\EContract;
use App\Domain\Entities\Payments\EPayment;
use App\Domain\Processes\Operations\Contracts\ContractObject;
use App\Domain\Processes\Operations\Contracts\ContractsFinancialPolicy;
use App\Domain\Processes\Operations\Contracts\ContractSubject;
use App\Domain\Processes\Operations\Contracts\ContractSupervising;
use App\Domain\Processes\Operations\Payments\ExpectedPayment;
use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;


class UpdateTempContract {



    public static function handle($contract_id, $data){

        $data = (object) $data;

        $contract = EContract::get_first($contract_id);

        $contract = EContract::update_from_temp_contract($contract, $data);

        $contract = ExpectedPayment::update_or_create($contract, $data);

        $contract = ContractSupervising::update($contract, $data);

        $contract = ContractsFinancialPolicy::update($contract, $data);

        if(isset($data->insurer)){
            $contract = ContractSubject::update_or_create($contract, (object)$data->insurer);
        }

        if(isset($data->object_insurer)){
            $contract = ContractObject::update_or_create($contract, (object)$data->object_insurer);
        }


        // когда нет данных из реквеста, но платежи есть
        if($contract->payments && $contract->payments->count()){
            foreach($contract->all_payments as $payment) {
                if ((int)$payment->order_id > 0) {
                    if(isset($data->order_source)){
                        $payment->update([
                            'order_source' => $data->order_source,
                        ]);
                    }

                } else {
                    $payment->update([
                        'order_id' => $contract->order_id,
                        'order_title' => $contract->order_title,
                        'manager_id' => $contract->manager_id,
                        'parent_agent_id' => $contract->parent_agent_id,
                    ]);
                }
            }
            $last_payment_for_referencer = $contract->payments()->where('type_id', '=', 0)->get()->last();
            //обновление данных по последнему платежу в договоре
            if ((int)$last_payment_for_referencer->order_id > 0){

            } else {
                if ((int)$contract->referencer_id && $referencer = $contract->referencer) {
                    $last_payment_for_referencer->update([
                        'referencer_id' => $referencer->id,
                        'referencer_kv' => $referencer->referencer_kv,
                    ]);
                } else {
                    $last_payment_for_referencer->update([
                        'referencer_id' => '',
                        'referencer_kv' => '',
                    ]);
                }
            }


        }

        if(isset($data->payment)) {

            $collect_data = collect($data->payment)->first();

            if($collect_data['id'] == ''){

                $new_payment = EPayment::create_temp($contract_id);

                $payment = EPayment::update_temp($new_payment->id, (object)$collect_data);

                PaymentFinancialPolicy::actualize($payment);

                PaymentDiscounts::recount($payment);

                return true;
            }

            foreach($data->payment as $pay_data){

                $pay_data = (object) $pay_data;
                if(auth()->user()->hasPermission('contracts', 'select_financial_policy')) {

                    $payment = EPayment::update_temp($pay_data->id, $pay_data);
                }
                $payment_bd = Payments::find($pay_data->id);

                if(isset($payment_bd->type_id) && $payment_bd->type_id == 0){ // только для взносов

                    PaymentFinancialPolicy::actualize($payment);
                    if(auth()->user()->hasPermission('contracts', 'select_financial_policy') &&
                    (int)$payment->reports_order_id <= 0 && (int)$payment->reports_dvou_id <= 0) {
                        PaymentDiscounts::recount($payment);
                    }
                }
            }

        }


        return true;

    }


}