<?php

namespace App\Domain\Processes\Scenaries\Contracts\FastAccept;

use App\Domain\Entities\Contracts\EContract;
use App\Domain\Entities\Payments\EPayment;
use App\Domain\Processes\Operations\Payments\PaymentAcceptOperations;
use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Domain\Processes\Operations\Payments\PaymentReceipt;
use App\Domain\Processes\Operations\Payments\PaymentSupervising;


class FastAcceptProcessPayments {

    public static function handle($contract, $data){

        $contract = EContract::get_first($contract);

        $data = (object) $data;

        if(isset($data->payment)) {

            foreach($data->payment as $payment_data){

                $payment_data = (object)$payment_data;

                if($payment = EPayment::create_primary($contract, $payment_data)){

                    $payment = PaymentFinancialPolicy::actualize($payment);

                    $payment = PaymentSupervising::actualize($payment);

                    $payment = PaymentDiscounts::recount($payment);

                    $payment = PaymentReceipt::attach($payment, $payment_data);

                    $payment = PaymentAcceptOperations::update_or_create($payment);

                }

            }


        }

        return $contract;
    }


}