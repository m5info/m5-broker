<?php

namespace App\Domain\Processes\Scenaries\Contracts\FastAccept;

use App\Domain\Entities\Contracts\EContract;
use App\Domain\Processes\Operations\Contracts\ContractObject;
use App\Domain\Processes\Operations\Contracts\ContractsFinancialPolicy;
use App\Domain\Processes\Operations\Contracts\ContractSubject;
use App\Domain\Processes\Operations\Contracts\ContractSupervising;

class FastAcceptProcessContract {

    public static function handle($contract, $data){

        $data = (object) $data;

        $contract = EContract::get_first($contract);

        $contract = EContract::update_or_create_from_primary($contract, $data);

        $contract = EContract::update_from_temp_contract($contract, $data);

        $contract = ContractSupervising::update($contract, $data);

        $contract = ContractsFinancialPolicy::update($contract, $data);

        if(isset($data->insurer)){
            $contract = ContractSubject::update_or_create($contract, (object)$data->insurer);
        }

        if(isset($data->object_insurer)){
            $contract = ContractObject::update_or_create($contract, (object)$data->object_insurer);
        }

        return $contract;

    }


}