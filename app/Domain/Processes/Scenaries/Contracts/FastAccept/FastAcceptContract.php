<?php

namespace App\Domain\Processes\Scenaries\Contracts\FastAccept;

use App\Domain\Entities\Bso\EBsoItem;
use App\Domain\Entities\Contracts\EContract;
use App\Domain\Process;
use App\Domain\Processes\Operations\Bso\BsoContract;
use App\Domain\Processes\Operations\Bso\BsoLocationOperations;
use App\Domain\Processes\Operations\Bso\BsoStateOperations;
use App\Domain\Processes\Operations\Contracts\ContractAccept;
use App\Domain\Processes\Operations\Contracts\ContractStatus;
use App\Models\Contracts\ContractMessage;
use App\Models\Contracts\Contracts;


class FastAcceptContract {

    public static function handle($contract, $data){

        $contract = EContract::get_first($contract);

        $bso_item = EBsoItem::get_first($contract->bso);

        //по умолчанию тип акцепта "проверка"
        $kind_acceptance = 3;

        //если пришёл какой то другой, который выбрали при быстром акцепте
        if (isset($data->kind_acceptance)) {
            if(isset(Contracts::KIND_ACCEPTANCE[(int)$data->kind_acceptance])){
            //if (in_array((int)$data->kind_acceptance, Contracts::KIND_ACCEPTANCE)) {
                $kind_acceptance = (int)$data->kind_acceptance;
            }
        }


        /*
        if($kind_acceptance == 1){
            $contract->statys_id = 4;
            $contract->save();
        }
        */

        //если поставили галку забрать документы
        if(isset($data->withdraw_documents) && $data->withdraw_documents == 1){

            Process::get_process()->set('withdraw_documents', 1);

            //ставим принят от агента
            BsoLocationOperations::set($bso_item, 4);

            // передаём юзеру
            $bso_item->update([
                'user_id' => auth()->id()
            ]);

        }

        //если не выпущен или не бузусловный акцепт
        if ($contract->statys_id != 4 || $contract->kind_acceptance != 1) {

            //завязываем бсо с контрактом в обе стороны
            BsoContract::attach($bso_item, $contract);

            //продаём бсо
            BsoStateOperations::set($bso_item, 2);

            //акцептуем
            ContractAccept::set($contract, $kind_acceptance);

            if((int)$kind_acceptance != 1){
                //отправляем контракт в Коррекция
                ContractStatus::set($contract, 2);

                //Если условный
                if((int)$kind_acceptance == 0) {
                    //Создаем ошибку менеджеру
                    ContractMessage::create([
                        'message' => $data->message,
                        'type_id' => 1,
                        'user_id' => auth()->id(),
                        'contract_id' => $contract->id,
                    ]);
                }
            }

        }

        return $contract;
    }

}