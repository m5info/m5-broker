<?php

namespace App\Domain\Processes\Scenaries\Contracts\Orders;

use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoCarts;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Orders;
use App\Models\User;
use Illuminate\Http\Request;

class BsoToCourier {

    // передача бсо на курьера и подтверждение акта
    public static function handle($order_id, $courier_id){

        $order = Orders::find($order_id);

        if($bso_cart = $order->bso_cart)
        {
            $bso_items = $bso_cart->bso_items;

            foreach($bso_items as $key => $bso_item){
                $bso_item->agent_id = $order->delivery_user_id;
                $bso_item->save();
            }

            $bso_cart->user_id_to = $order->delivery_user_id;
            $bso_cart->courier_id = $order->delivery_user_id;
            $bso_cart->save();

        }

        foreach($order->contracts as $contract){
            $contract->agent_id = $courier_id;
            $contract->save();
        }

        return true;
    }

}