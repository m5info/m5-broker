<?php

namespace App\Domain\Processes\Scenaries\Contracts\Online;

use App\Domain\Entities\Contracts\EContract;
use App\Domain\Processes\Operations\Contracts\ContractOnlineProduct;

class OnlineContractSave {

    public static function handle($contract, $data){

        $res = (object)['state'=> false, 'msg' => 'Не удалось сохранить договор.'];

        if($contract = EContract::get_first($contract)){

            //если нашли класс работы с продуктом
            if($online_product_class = ContractOnlineProduct::get_online_product_class($contract)){

                // если всё нормельно сохранилось
                if($online_product_class::save($contract, (object)$data)){
                    $res->state = true;
                    $res->msg = 'Данные успешно сохранены!';
                }

            }else{
                $res->msg = 'Класс продукта не найден!';

            }

        }

        return (array)$res;

    }




}