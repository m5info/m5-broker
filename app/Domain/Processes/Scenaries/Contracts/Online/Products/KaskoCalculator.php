<?php

namespace App\Domain\Processes\Scenaries\Contracts\Online\Products;


use App\Domain\Processes\Operations\Contracts\ContractObject;
use App\Domain\Processes\Operations\Contracts\ContractOnlineTerms;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Driver;
use App\Models\Contracts\Subjects;

class KaskoCalculator {

    public static function save(Contracts $contract, $all_data){

        $data = (object)$all_data->contract;


        $contract->sign_date = setDateTimeFormat(date("Y-m-d H:i:s"));
        $contract->begin_date = setDateTimeFormat($data->begin_date.' '.$data->begin_time);
        $contract->end_date = setDateTimeFormat($data->end_date);

        $contract->is_prolongation = $data->is_prolongation;
        $contract->bank_id = $data->bank_id;


        $object_insurer = (object)$data->object;
        if(isset($object_insurer->price)){
            $contract->insurance_amount = getFloatFormat($object_insurer->price);
        }


        if(isset($data->object)){
            $object = ContractObject::update_or_create_auto_kasko_calculator($contract->object_insurer_id, (object)$data->object);
            $contract->object_insurer_id = $object->id;
        }

        if(ContractOnlineTerms::update_or_create_kasko($contract, (object)$data) == true){

        }

        $contract->drivers()->delete();

        /*
        if(isset($data->drivers_type)){
            $contract->drivers_type_id = (int) $data->drivers_type;




            //если ограниченный список водителей
            if($data->drivers_type == 0){



            }


        }else{
            //если указаны водители
            if(isset($data->driver) && is_array($data->driver)){
                self::update_online_drivers($contract, $data->driver);

            }
        }
*/

        if(isset($data->driver) && is_array($data->driver)){
            $contract->drivers_type_id = 0;
            self::update_online_drivers($contract, $data->driver);
        }else{
            $contract->drivers_type_id = 1;
        }


        if($contract->save()){
            return true;
        }

        return false;

    }

    public static function update_online_drivers($contract, $data){


        foreach ($data as $driver){

            $driver = (object)$driver;

            $driver->subject_id = 0;


            $update_data = [
                "contract_id" => $contract->id,
                "age" => isset($driver->age) ? $driver->age : 0,
                "exp" => isset($driver->exp) ? $driver->exp : 0,
            ];



            Driver::create($update_data);

        }

    }



}