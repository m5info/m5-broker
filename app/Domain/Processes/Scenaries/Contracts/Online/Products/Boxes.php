<?php

namespace App\Domain\Processes\Scenaries\Contracts\Online\Products;


use App\Domain\Processes\Operations\Contracts\ContractObject;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Driver;
use App\Models\Contracts\Subjects;

class Boxes {

    public static function save(Contracts $contract, $all_data){

        $data = (object)$all_data->contract;

        if($contract->getProductOrProgram()->api_name == 'dk')//ДК
        {


            $contract->payment_total = getFloatFormat($data->payment_total);


            $contract->agent_id = $data->agent_id;
            $contract->sales_condition = $data->sales_condition;
            if($contract->sales_condition != 0){
                $contract->manager_id = $data->manager_id;
            }

            if(isset($data->object)){
                $temp_object = (object)$data->object;

                $contract->sign_date = setDateTimeFormat(date("Y-m-d H:i:s"));


                if(isset($temp_object->dk_date_from)){
                    $contract->begin_date = setDateTimeFormat($temp_object->dk_date_from);
                }

                if(isset($temp_object->dk_date)) {
                    $contract->end_date = setDateTimeFormat($temp_object->dk_date);
                }

                $object = ContractObject::update_or_create_auto_dk($contract->object_insurer_id, (object)$data->object);
                $contract->object_insurer_id = $object->id;

            }


            if(isset($data->insurer)){
                $contract->insurer_id = self::create_update_online_subject((object)$data->insurer, 'insurer', $contract->insurer_id);
            }

            /*

            if(isset($data->object)){

            }
            */


        }







        if($contract->save()){
            return true;
        }

        return false;

    }

    public static function create_update_online_subject($data, $type = 'insurer', $subject_id = null){
        //СДЕЛАТЬ ПОИСК КОНТРАГЕНТА
        $subject = Subjects::saveOrCreateOnlineSubject($data, $subject_id);
        return $subject->id;
    }

    public static function update_online_drivers($contract, $data){


        foreach ($data as $driver){

            $driver = (object)$driver;

            $driver->subject_id = 0;

            //если стоит галка "страхователь"
            if(isset($driver->same_as_insurer) && $driver->same_as_insurer == 1){
                $insurer_info = $contract->insurer->get_info();
                $driver->fio = $insurer_info->fio;
                $driver->sex = $insurer_info->sex;
                $driver->birth_date = $insurer_info->birthdate;
                $driver->subject_id = $contract->insurer_id;
            }

            $update_data = [
                "fio" => $driver->fio,
                "birth_date" => $driver->birth_date ? setDateTimeFormat($driver->birth_date) : "",
                "sex" => $driver->sex,
                "exp_date" => $driver->exp_date ? setDateTimeFormat($driver->exp_date) : "",
                "doc_num" => $driver->doc_num,
                "doc_serie" => $driver->doc_serie,
                "doc_date" => $driver->doc_date ? setDateTimeFormat($driver->doc_date) : "",
                "contract_id" => $contract->id,
                "same_as_insurer" => isset($driver->same_as_insurer) ? $driver->same_as_insurer : 0,
                "subject_id" => $driver->subject_id,
                "kbm" => $driver->kbm ? getFloatFormat($driver->kbm) : "",
                "class_kbm" => $driver->class_kbm ? getFloatFormat($driver->class_kbm) : "",
            ];



            $old_driver = $contract->drivers()
                ->where('doc_num', $driver->doc_num)->where('doc_num', '!=' , '')
                ->where('doc_serie', $driver->doc_serie)->where('doc_serie', '!=' , '')->where('doc_serie', '!=' , 0)
                ->first();

            if($old_driver){
                $old_driver->update($update_data);
            }else{
                if($driver->doc_serie != '' && $driver->doc_serie != 0 && $driver->doc_num != 0){
                    Driver::create($update_data);
                }
            }

        }

    }


}