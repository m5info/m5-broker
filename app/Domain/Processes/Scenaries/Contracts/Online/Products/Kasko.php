<?php

namespace App\Domain\Processes\Scenaries\Contracts\Online\Products;


use App\Domain\Processes\Operations\Contracts\ContractObject;
use App\Domain\Processes\Operations\Contracts\ContractOnlineTerms;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractTerms;
use App\Models\Contracts\Driver;
use App\Models\Contracts\ObjectInsurerAutoEquipment;
use App\Models\Contracts\Subjects;

class Kasko {

    public static function save(Contracts $contract, $all_data){

        $data = (object)$all_data->contract;

        $contract->sign_date = isset($contract->sign_date) && $contract->sign_date ? $contract->sign_date : setDateTimeFormat(date("Y-m-d H:i:s"));
        $contract->begin_date = setDateTimeFormat($data->begin_date.' '.$data->begin_time);
        $contract->end_date = setDateTimeFormat($data->end_date);

        $contract->is_prolongation = isset($data->is_prolongation) ? $data->is_prolongation : $contract->is_prolongation;
        $contract->bank_id = isset($data->bank_id) ? $data->bank_id : $contract->bank_id;
        $contract->insurance_amount = isset($data->insurance_amount) ? getFloatFormat($data->insurance_amount) : $contract->insurance_amount;

        $contract->installment_algorithms_id = isset($data->installment_algorithms_id) ? $data->installment_algorithms_id : $contract->installment_algorithms_id;


        if(ContractOnlineTerms::update_or_create_kasko($contract, (object)$data) == true){

        }

        if(isset($data->insurer)){
            $contract->insurer_id = self::create_update_online_subject((object)$data->insurer, 'insurer', $contract->insurer_id);
        }

        if(isset($data->owner)){
            if(isset($data->owner['is_insurer']) && (int)$data->owner['is_insurer'] == 1){
                $contract->temp_owner_id = $contract->owner_id;
                $contract->owner_id = $contract->insurer_id;
            }else{
                $contract->owner_id = self::create_update_online_subject((object)$data->owner, 'owner', $contract->temp_owner_id == 0 ? null : $contract->temp_owner_id);
            }
        }

        if(isset($data->beneficiar)){
            if(isset($data->beneficiar['is_insurer']) && (int)$data->beneficiar['is_insurer'] == 1){
                $contract->beneficiar_id = $contract->insurer_id;
            }else{
                $contract->beneficiar_id = self::create_update_online_subject((object)$data->beneficiar, 'beneficiar', $contract->beneficiar_id);
            }
        }

        if (isset($data->risks)){

            $current_risks = $data->risks;

            $terms = ContractTerms::where('contract_id', $contract->id)->first();
            if ($terms){
                $terms->damage_id = $current_risks['damage_id'];

                if(isset($current_risks['harms'])){
                    $terms->harms = $current_risks['harms']['status'];
                    $terms->harms_sum = $current_risks['harms']['sum'];
                }
                if(isset($current_risks['hijacking_id'])){
                    $terms->hijacking_id = isset($current_risks['hijacking_id']['id']) ?? $current_risks['hijacking_id']['id'];
                    $terms->hijacking_sum = isset($current_risks['hijacking_id']['sum']) ?? $current_risks['hijacking_id']['sum'];
                }

                $terms->is_undocumented_settlement_competent_authorities =
                    $current_risks['is_undocumented_settlement_competent_authorities'];
                $terms->is_emergency_commissioner_select = $current_risks['is_emergency_commissioner_select'];
                $terms->is_loss_commodity_value = $current_risks['is_loss_commodity_value'];
                if (isset($current_risks['guarantor_repair'])){
                    $terms->guarantor_repair = 1;
                }else{
                    $terms->guarantor_repair = 0;
                }

                if (isset($current_risks['unguarantor_repair'])){
                    $terms->unguarantor_repair = 1;
                }else{
                    $terms->unguarantor_repair = 0;
                }


                $terms->voluntary_liability_insurance = isset($current_risks['voluntary_liability_insurance']['sum']) ?? $current_risks['voluntary_liability_insurance']['sum'];
                $terms->passenger_and_driver_accident_insurance = isset($current_risks['passenger_and_driver_accident_insurance']['status']) ?? $current_risks['passenger_and_driver_accident_insurance']['status'];
                $terms->passenger_and_driver_accident_insurance_sum = isset($current_risks['passenger_and_driver_accident_insurance']['sum']) ?? $current_risks['passenger_and_driver_accident_insurance']['sum'];
                $terms->passenger_and_driver_accident_insurance_seats = isset($current_risks['passenger_and_driver_accident_insurance']['seats']) ?? $current_risks['passenger_and_driver_accident_insurance']['seats'];
                $terms->passenger_and_driver_accident_insurance_type = isset($current_risks['passenger_and_driver_accident_insurance']['type']) ?? $current_risks['passenger_and_driver_accident_insurance']['type'];



                $terms->permanent_sum_insured = $current_risks['permanent_sum_insured'];
                $terms->save();
            }else{

                $new_term = new ContractTerms();
                $new_term->contract_id = $contract->id;
                $new_term->damage_id = $current_risks['damage_id'];
                $new_term->hijacking_id = $current_risks['hijacking_id']['sum'];
                $new_term->is_undocumented_settlement_competent_authorities = $current_risks['is_undocumented_settlement_competent_authorities'];
                $new_term->is_emergency_commissioner_select = $current_risks['is_emergency_commissioner_select'];
                $new_term->is_loss_commodity_value = $current_risks['is_loss_commodity_value'];
                $new_term->voluntary_liability_insurance = $current_risks['voluntary_liability_insurance']['sum'];
                $new_term->passenger_and_driver_accident_insurance = $current_risks['passenger_and_driver_accident_insurance']['sum'];
                $new_term->permanent_sum_insured = $current_risks['permanent_sum_insured']['sum'];
                $new_term->save();
            }

        }

        if(isset($data->object)){
            $object = ContractObject::update_or_create_auto_kasko($contract->object_insurer_id, (object)$data->object);
            $contract->object_insurer_id = $object->id;

            ObjectInsurerAutoEquipment::where('object_insurer_id', $contract->object_insurer_id)->delete();

            if (isset($data->equipment)){
                foreach ($data->equipment as $equip){
                    $equipment = new ObjectInsurerAutoEquipment();
                    $equipment->object_insurer_id = $object->id;
                    $equipment->title = $equip['title'];
                    $equipment->insurance_amount = $equip['insurance_amount'];
                    if (!empty($equip['title']) && !empty($equip['insurance_amount'])){
                        $equipment->save();
                    }

                }
            }
        }



        if(isset($data->drivers_type)){
            $contract->drivers_type_id = (int) $data->drivers_type;

            //если ограниченный список водителей
            if($data->drivers_type == 0){

                //если указаны водители
                if(isset($data->driver) && is_array($data->driver)){

                    self::update_online_drivers($contract, $data->driver);

                }

            }

        }else{
            //если указаны водители
            if(isset($data->driver) && is_array($data->driver)){

                self::update_online_drivers($contract, $data->driver);

            }
        }


        if($contract->save()){
            return true;
        }

        return false;

    }

    public static function create_update_online_subject($data, $type = 'insurer', $subject_id = null){
        //СДЕЛАТЬ ПОИСК КОНТРАГЕНТА
        $subject = Subjects::saveOrCreateOnlineSubject($data, $subject_id);
        $subject->get_info()->updateInfo($data, $subject->id);
        return $subject->id;
    }

    public static function update_online_drivers($contract, $data){

        foreach ($data as $driver){

            $driver = (object)$driver;

            $driver->subject_id = 0;

            //если стоит галка "страхователь"
            if(isset($driver->same_as_insurer) && $driver->same_as_insurer == 1){
                $insurer_info = $contract->insurer->get_info();
                $driver->fio = $insurer_info->fio;
                $driver->sex = $insurer_info->sex;
                $driver->birth_date = $insurer_info->birthdate;
                $driver->subject_id = $contract->insurer_id;
            }

            $update_data = [
                "fio" => $driver->fio,
                "birth_date" => isset($driver->birth_date) && $driver->birth_date ? setDateTimeFormat($driver->birth_date) : "",
                "sex" => isset($driver->sex) ? $driver->sex : '',
                "exp_date" => isset($driver->exp_date) && $driver->exp_date ? setDateTimeFormat($driver->exp_date) : "",
                "doc_num" => isset($driver->doc_num) ? $driver->doc_num : "",
                "doc_serie" => isset($driver->doc_serie) ? $driver->doc_serie : "",
                "doc_date" => isset($driver->doc_date) && $driver->doc_date ? setDateTimeFormat($driver->doc_date) : "",
                "contract_id" => $contract->id,
                "same_as_insurer" => isset($driver->same_as_insurer) ? $driver->same_as_insurer : 0,
                "subject_id" => isset($driver->subject_id) ? $driver->subject_id : '',
                "kbm" => isset($driver->kbm) && $driver->kbm ? getFloatFormat($driver->kbm) : "",
                "class_kbm" => isset($driver->class_kbm) && $driver->class_kbm ? getFloatFormat($driver->class_kbm) : "",
            ];

            $old_driver = Driver::where('id', $driver->id)->first();

//            $old_driver = $contract->drivers()
//                ->where('doc_num', $driver->doc_num)
//                ->where('doc_serie', $driver->doc_serie)
//                ->first();

            if($old_driver){
                $old_driver->update($update_data);
            }else{
                Driver::create($update_data);
            }

        }

    }



}