<?php

namespace App\Domain\Processes\Scenaries\Contracts\Online\Products;


use App\Domain\Processes\Operations\Contracts\ContractObject;
use App\Domain\Processes\Operations\Contracts\ContractOnlineTerms;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractTerms;
use App\Models\Contracts\Driver;
use App\Models\Contracts\ObjectInsurerAutoEquipment;
use App\Models\Contracts\Subjects;

class Flats {

    public static function save(Contracts $contract, $all_data){

        $data = (object)$all_data->contract;

        $contract->sign_date = setDateTimeFormat(date("Y-m-d H:i:s"));
        $contract->begin_date = setDateTimeFormat($data->begin_date.' '.$data->begin_time);
        $contract->end_date = setDateTimeFormat($data->end_date);

        $contract->is_prolongation = $data->is_prolongation;
        $contract->installment_algorithms_id = isset($data->installment_algorithms_id) ? $data->installment_algorithms_id : '';



        if(isset($data->insurer)){
            $contract->insurer_id = self::create_update_online_subject((object)$data->insurer, 'insurer', $contract->insurer_id);
        }

        if(isset($data->beneficiar)){
            if(isset($data->beneficiar['is_insurer']) && (int)$data->beneficiar['is_insurer'] == 1){
                $contract->beneficiar_id = $contract->insurer_id;
            }else{
                $contract->beneficiar_id = self::create_update_online_subject((object)$data->beneficiar, 'beneficiar', $contract->beneficiar_id);
            }
        }

        if(isset($data->object)){
            $object = ContractObject::update_or_create_flats($contract->object_insurer_id, (object)$data->object);
            $contract->object_insurer_id = $object->id;
        }

        if (isset($data->risks)){



        }






        if($contract->save()){
            return true;
        }

        return false;

    }

    public static function create_update_online_subject($data, $type = 'insurer', $subject_id = null){
        //СДЕЛАТЬ ПОИСК КОНТРАГЕНТА
        $subject = Subjects::saveOrCreateOnlineSubject($data, $subject_id);
        $subject->get_info()->updateInfo($data, $subject->id);
        return $subject->id;
    }



}