<?php

namespace App\Domain\Processes\Scenaries\Contracts\Online\Products;


use App\Domain\Processes\Operations\Contracts\ContractObject;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Driver;
use App\Models\Contracts\Subjects;

class Osago {

    public static function save(Contracts $contract, $all_data){

        $data = (object)$all_data->contract;

        $contract->is_epolicy = isset($data->bso_is_epolicy) ? 1 : 0;
        $contract->sign_date = isset($contract->sign_date) && $contract->sign_date ? $contract->sign_date : setDateTimeFormat(date("Y-m-d H:i:s"));
        $contract->begin_date = setDateTimeFormat($data->begin_date.' '.$data->begin_time);
        $contract->end_date = setDateTimeFormat($data->end_date);
        $contract->is_prolongation = isset($data->is_prolongation) ? $data->is_prolongation : 0;
        $contract->prev_policy_serie = isset($data->prev_policy_serie) ? $data->prev_policy_serie : null;
        $contract->prev_policy_number = isset($data->prev_policy_number) ? $data->prev_policy_number : null;
        $contract->prev_sk_id = isset($data->prev_sk_id) ? $data->prev_sk_id : null;

        if(isset($data->insurer)){
            $contract->insurer_id = self::create_update_online_subject((object)$data->insurer, 'insurer', $contract->insurer_id);
        }

        if(isset($data->owner)){
            $owner = (object)$data->owner;
            if(isset($owner->is_insurer) && (int)$owner->is_insurer == 1){
                $contract->temp_owner_id = $contract->owner_id;
                $contract->owner_id = $contract->insurer_id;
            }else{
                $contract->owner_id = self::create_update_online_subject($owner, 'owner', $contract->temp_owner_id == 0 ? null : $contract->temp_owner_id);
            }
        }

        if(isset($data->object)){
            $object = ContractObject::update_or_create_auto_osago($contract->object_insurer_id, (object)$data->object);
            $contract->object_insurer_id = $object->id;
        }


        //если указаны водители
        if(isset($data->driver) && is_array($data->driver)){
            $contract->drivers_type_id = $data->drivers_type_id;
            self::update_online_drivers($contract, $data->driver);
        }else{
            $contract->drivers_type_id = $data->drivers_type_id;
        }

        //dd($contract->drivers);
        /*

        if(isset($data->drivers_type)){
            $contract->drivers_type_id = (int) $data->drivers_type;

            //если ограниченный список водителей
            if($data->drivers_type == 0){

                //если указаны водители
                if(isset($data->driver) && is_array($data->driver)){

                    self::update_online_drivers($contract, $data->driver);

                }

            }

        }else{
            //если указаны водители
            if(isset($data->driver) && is_array($data->driver)){

                self::update_online_drivers($contract, $data->driver);

            }
        }

        */

        if($contract->save()){
            return true;
        }

        return false;

    }

    public static function create_update_online_subject($data, $type = 'insurer', $subject_id = null){
        //СДЕЛАТЬ ПОИСК КОНТРАГЕНТА
        $subject = Subjects::saveOrCreateOnlineSubject($data, $subject_id);
        $subject->get_info()->updateInfo($data, $subject->id);
        return $subject->id;
    }

    public static function update_online_drivers($contract, $data){


        foreach ($data as $driver){

            $driver = (object)$driver;

            $driver->subject_id = 0;

            //если стоит галка "страхователь"
            if(isset($driver->same_as_insurer) && $driver->same_as_insurer == 1){
                $insurer_info = $contract->insurer->get_info();
                $driver->fio = $insurer_info->fio;
                $driver->sex = $insurer_info->sex;
                $driver->birth_date = $insurer_info->birthdate;
                $driver->subject_id = $contract->insurer_id;
            }

            $update_data = [
                "fio" => $driver->fio,
                "first_name" => $driver->first_name,
                "second_name" => $driver->second_name,
                "last_name" => $driver->last_name,
                "birth_date" => isset($driver->birth_date) && $driver->birth_date ? setDateTimeFormat($driver->birth_date) : "",
                "sex" => isset($driver->sex) ? $driver->sex : '',
                "exp_date" => isset($driver->exp_date) && $driver->exp_date ? setDateTimeFormat($driver->exp_date) : "",
                "doc_num" => isset($driver->doc_num) ? $driver->doc_num : "",
                "doc_serie" => isset($driver->doc_serie) ? $driver->doc_serie : "",
                "doc_date" => isset($driver->doc_date) && $driver->doc_date ? setDateTimeFormat($driver->doc_date) : "",
                "contract_id" => $contract->id,
                "same_as_insurer" => isset($driver->same_as_insurer) ? $driver->same_as_insurer : 0,
                "subject_id" => isset($driver->subject_id) ? $driver->subject_id : '',
                "kbm" => isset($driver->kbm) && $driver->kbm ? getFloatFormat($driver->kbm) : "",
                "class_kbm" => isset($driver->class_kbm) && $driver->class_kbm ? getFloatFormat($driver->class_kbm) : "",
                "foreign_docs" => isset($driver->foreign_docs) && $driver->foreign_docs ? 1 : 0,
                "kbm_rsa_request_id" => isset($driver->kbm_rsa_request_id) && $driver->kbm_rsa_request_id ? $driver->kbm_rsa_request_id : '',
            ];

            $old_driver = Driver::where('id', $driver->id)->first();

//            $old_driver = $contract->drivers()
//                ->where('doc_num', $driver->doc_num)->where('doc_num', '!=' , '')
//                ->where('doc_serie', $driver->doc_serie)->where('doc_serie', '!=' , '')->where('doc_serie', '!=' , 0)
//                ->first();

            if($old_driver){
                $old_driver->update($update_data);
            }else{
                Driver::create($update_data);
            }

        }

    }


}