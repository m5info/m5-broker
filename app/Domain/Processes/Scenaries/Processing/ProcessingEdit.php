<?php


namespace App\Domain\Processes\Scenaries\Processing;


use App\Domain\Entities\Payments\EPayment;
use App\Domain\Processes\Scenaries\Contracts\Orders\BsoToCourier;
use App\Models\BSO\BsoCarts;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ObjectInsurer;
use App\Models\Contracts\ObjectInsurerAuto;
use App\Models\Contracts\Orders;
use App\Models\Contracts\Payments;
use App\Models\Contracts\Subjects;
use App\Models\Directories\BsoSuppliers;
use App\Models\Orders\ProcessingOrdersLogs;

class ProcessingEdit{


    public static function create(){

        $order = new Orders();
        $order->status_id = 0;
        $order->create_user_id = auth()->id();
        $order->agent_id = auth()->id();
        $order->manager_id = auth()->id();
        $order->delivery_city_id = auth()->user()->city_id;
        $order->save();

        $object_insurer = new ObjectInsurer();
        $object_insurer->type = 1;
        $object_insurer->save();
        $auto = new ObjectInsurerAuto();
        $auto->object_insurer_id = $object_insurer->id;
        $auto->save();

        $insurer = new Subjects();
        $insurer->save();

        $contract = Contracts::create([
            'created_at' => date('Y-m-d'),
            'statys_id' => -2,
            'user_id' => auth()->id(),
            'agent_id' => auth()->id(),
            'manager_id' => auth()->id(),
            'sales_condition' => auth()->user()->is('is_manager')?1:2,
            'insurer_id' => $insurer->id,
            'object_insurer_id' => $object_insurer->id,
            'order_form_id' => $order->id,
        ]);

        EPayment::create_temp($contract);

        $order->main_contract_id = $contract->id;
        $order->save();

        return $order;
    }

    public static function updateContractBsoSupplierProduct($contract_id, $request)
    {
        $contract = Contracts::getContractId($contract_id);
        if($contract)
        {
            if($request->bso_id && (int)$request->bso_id > 0){

                $bso = BsoItem::getBsoId($request->bso_id);
                $contract->user_id = auth()->id();
                $contract->bso_id = $bso->id;
                $contract->bso_title = $bso->bso_title;
                $contract->insurance_companies_id = $bso->insurance_companies_id;
                $contract->bso_supplier_id = $bso->bso_supplier_id;
                $contract->product_id = $bso->product_id;


            }else{
                $bso_supplier = BsoSuppliers::findOrFail((int) $request->bso_supplier_id);
                $contract->bso_supplier_id = $bso_supplier->id;
                $contract->insurance_companies_id = $bso_supplier->insurance_companies_id;
                $contract->product_id = $request->product_id;

            }

            $contract->save();


            foreach ($contract->all_payments()->get() as $payment)
            {

                $payment->bso_id = $contract->bso_id;
                $payment->contract_id = $contract->id;
                $payment->statys_id = -1;
                $payment->agent_id = $contract->agent_id;
                $payment->manager_id = $contract->manager_id;
                $payment->org_id = $contract->bso ? $contract->bso->org_id : false;
                $payment->save();

            }


        }

        ProcessingOrdersLogs::setLogs($contract->id, 'Изменение поставщика');

        return $contract->order;
    }


    public static function updateOrders($order_id, $data)
    {
        $order = Orders::find($order_id);

        if(isset($data['delivery'])){
            $order->urgency = (isset($data['urgency'])) ? 1 : 0;
            $order->without_forming = (isset($data['without_forming'])) ? 1 : 0;
            $order->is_delivery = (isset($data['delivery']['is_delivery'])) ? 1 : 0;
            $order->delivery_city_id = $data['delivery']['city_id'];
            $order->delivery_metro = $data['delivery']['metro'];
            $order->address = $data['delivery']['address'];
            $order->address_width = $data['delivery']['address_width'];
            $order->address_longitude = $data['delivery']['address_longitude'];
            $order->address_kladr = $data['delivery']['address_kladr'];
            $order->delivery_date = date('Y-m-d', strtotime($data['delivery']['date']));
            $order->delivery_time = date('H:i', strtotime($data['delivery']['time']));
            $order->delivery_comment = $data['delivery']['comment'];
            $order->delivery_phone = $data['delivery']['delivery_phone'];
            $order->save();
        }

        $order->urgency = (isset($data['urgency'])) ? 1 : 0;
        $order->without_forming = (isset($data['without_forming'])) ? 1 : 0;

        if(isset($data['without_forming'])){
            if($order->contracts){
                foreach($order->contracts as $contract){
                    $contract->sign_date = $contract->created_at;
                    $contract->begin_date = isset($order->delivery_date) ? $order->delivery_date : $contract->end_date;
                    $contract->end_date = isset($order->delivery_date) ? $order->delivery_date : $contract->end_date;
                    $contract->payment_total = getFloatFormat(1);
                    $contract->save();
                }
            }
        }
        $order->save();

        return $order;
    }

    public static function statusChangeOrders($order_id, $status)
    {
        $order = Orders::find($order_id);
        $order->status_id = $status;
        $order->save();

        ProcessingOrdersLogs::setLogs($order->main_contract_id, 'Изменение статуса: '. Orders::STATUSES[$status]);

        if($status == 5 && $order->departures_courier_price_id > 0)
        {
            $order->setDeparturesCourierPrice();
        }

        return $order;
    }

    public static function deleteOrders($order_id)
    {
        $order = Orders::find($order_id);
        if($order->status_id == 0){
            Orders::destroy($order_id);
            return true;
        }

        return false;
    }


    public static function combineOrders($orders_active, $orders)
    {
        foreach ($orders as $order){
            if($order->id != $orders_active->id){

                if(!$orders_active->bso_cart_id && $order->bso_cart_id){
                    $orders_active->bso_cart_id = $order->bso_cart_id;
                }

                if($order->bso_cart_id && $orders_active->bso_cart_id != $order->bso_cart_id){
                    $bso_items = BsoItem::where('bso_cart_id', $order->bso_cart_id)->update(['bso_cart_id'=>$orders_active->bso_cart_id]);
                    BsoCarts::find($order->bso_cart_id)->delete();
                }

                Contracts::where('order_form_id', $order->id)->update(['order_form_id'=>$orders_active->id]);
                $order->delete();
            }
        }

        return $orders_active;
    }



    public static function assignOrders($orders, $courier_id, $departures_courier_price_id)
    {
        foreach ($orders as $order){
            ProcessingEdit::setDeliveryUser($order, $courier_id, $departures_courier_price_id);
        }

        return true;
    }


    public static function setDeliveryUser($order, $courier_id, $departures_courier_price_id)
    {
        $order->delivery_user_id = $courier_id;
        $order->status_id = ($courier_id)?4:3;
        $order->departures_courier_price_id = $departures_courier_price_id;
        $order->save();

        foreach ($order->contracts as $contract){
            $contract->agent_id = $courier_id;
            $contract->save();
            ProcessingOrdersLogs::setLogs($contract->id, 'Назначения курьера');
        }

        Contracts::where('order_form_id', $order->id)->update(['agent_id'=>$courier_id]);
        BsoToCourier::handle($order->id, $courier_id);
    }


    public static function updateDeliveryOrders($order_id, $data)
    {
        $order = Orders::find($order_id);

        $order->delivery_city_id = $data['delivery']['city_id'];
        $order->delivery_metro = $data['delivery']['metro'];
        $order->address = $data['delivery']['address'];
        $order->address_width = $data['delivery']['address_width'];
        $order->address_longitude = $data['delivery']['address_longitude'];
        $order->address_kladr = $data['delivery']['address_kladr'];
        $order->delivery_date = date('Y-m-d', strtotime($data['delivery']['date']));
        $order->delivery_time = date('H:i', strtotime($data['delivery']['time']));
        $order->delivery_comment = $data['delivery']['comment'];

        if(isset($data['delivery']['departures_courier_price_id'])){
            $order->departures_courier_price_id = $data['delivery']['departures_courier_price_id'];
        }
        $order->save();

        ProcessingEdit::setDeliveryUser($order, $data['delivery']['courier'], $order->departures_courier_price_id);
        return $order;
    }


    public static function sendContractToCheck($contract_id)
    {

        $contract = Contracts::getContractId($contract_id);
        if($contract && $contract->statys_id == -2){
            if($contract->bso){
                $contract->bso->update([
                    'state_id' => 2,
                    'location_id' => 1,
                ]);
                BsoLogs::setLogs($contract->bso_id, 2, 1);
                $contract->update([
                    'bso_title' => $contract->bso ? $contract->bso->bso_title : '',
                    'bso_supplier_id' => $contract->bso ? $contract->bso->bso_supplier_id : '',
                    'insurance_companies_id' => $contract->bso ? $contract->bso->insurance_companies_id : '',
                    'statys_id' => 1,
                ]);

                ProcessingOrdersLogs::setLogs($contract->id, 'В проверку');

                return true;
            }
        }

        return false;
    }


}