<?php


namespace App\Domain\Processes\Scenaries\Inspection;


use App\Domain\Entities\Inspection\Inspection;
use App\Domain\Processes\Operations\Contracts\ContractsFinancialPolicy;
use App\Domain\Processes\Operations\Inspection\InspectionChangeStatus;
use App\Domain\Processes\Operations\Inspection\InspectionSetExecutor;
use App\Models\Contracts\Subjects;
use App\Models\Orders\InspectionComments;
use App\Models\Orders\InspectionOrdersLogs;

class InspectionSave{

    // чтобы логи не дублировать
    public static $log_setted = false;


    public static function handle($order_id, $data){

        $order = Inspection::get_first($order_id);

        if(isset($data->inspection)){
            $order->begin_date = setDateTimeFormat($data->inspection->date.' '.$data->inspection->time);

            $order->inspection->update([
                'city_id' => $data->inspection->city_id,
                'position_type_id' => $data->inspection->position_type_id,
                'client_event_type_id' => $data->inspection->client_event_type_id,
                'bso_serie' => $data->inspection->bso_serie,
                'address' => isset($data->inspection->address) ? $data->inspection->address : $order->inspection->address,
                'geo_lon' => $data->inspection->geo_lon,
                'geo_lat' => $data->inspection->geo_lat,
            ]);
            $order->push();
        }

        // сохраняем или создаем страхователя
        if(isset($data->insurer)){
            if($data->insurer->id > 0){
                $insurer = $order->insurer;
                $insurer->type = isset($data->insurer->type) ? $data->insurer->type : 0;
                $insurer->title = $data->insurer->fio;
                $insurer->phone = $data->insurer->phone;
                $insurer->save();

            }else{
                $insurer = Subjects::create([
                    'phone' => $data->insurer->phone,
                    'type' => $data->insurer->type,
                    'title' => $data->insurer->fio,
                ]);

                $insurer->save();
            }
        }

        // обновляем инфу по выбранному акту в продукте
        $order->inspection->temple_selected = $order->product->inspection_temple_act;

        // сохраняем авто
        if(isset($data->object_insurer)){
            $object_insurer = $order->object_insurer;
            $auto = $object_insurer->auto;

            $auto->update([
                'reg_number' => $data->object_insurer->reg_number,
                'mark_id' => $data->object_insurer->mark_id,
                'model_id' => $data->object_insurer->model_id,
            ]);
            $auto->save();
        }

        $order->insurer_id = $insurer->id;
        $order->bso_title = $data->bso_title;
        $order->financial_policy_id = isset($data->financial_policy_id) ? $data->financial_policy_id : $order->financial_policy_id;
        $order->bso_supplier_id = isset($data->bso_supplier_id) ? $data->bso_supplier_id : $order->bso_supplier_id;
        $order->insurance_companies_id = isset($data->insurance_companies_id) ? $data->insurance_companies_id : $order->insurance_companies_id ;
        $order->product_id = $data->product_id;
        $order->manager_id = auth()->id();





        if($data->workarea_comment){
            InspectionComments::create([
                'contract_id' => $order->id,
                'comment' => $data->workarea_comment,
                'manager_id' => auth()->id()
            ]);
        }

        if(!self::$log_setted){
            if(isset($data->status_to_change) && $data->status_to_change > -1){
                InspectionChangeStatus::handle($order->id, $data->status_to_change);
            }
        }

        $order->push();

        return $order;
    }
}