<?php


namespace App\Domain\Processes\Scenaries\Inspection;


use App\Domain\Entities\Inspection\Inspection;
use App\Domain\Entities\Payments\EPayment;
use App\Domain\Processes\Operations\Bso\BsoStateOperations;
use App\Domain\Processes\Operations\Contracts\ContractAccept;
use App\Domain\Processes\Operations\Contracts\ContractsFinancialPolicy;
use App\Domain\Processes\Operations\Contracts\ContractStatus;
use App\Domain\Processes\Operations\Inspection\InspectionChangeStatus;
use App\Domain\Processes\Operations\Inspection\InspectionSetExecutor;
use App\Domain\Processes\Operations\Payments\PaymentAcceptOperations;
use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentExpectedPayments;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Domain\Processes\Operations\Payments\PaymentInvoice;
use App\Domain\Processes\Operations\Payments\PaymentStatus;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Subjects;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Finance\PayMethod;
use App\Models\Orders\InspectionComments;
use App\Models\Orders\InspectionOrdersLogs;
use App\Models\User;

class InspectionAccept{




    public static function handle($order_id){


        $order = Inspection::get_first($order_id);



        if($order->status_order_id == 3 || $order->status_order_id == 4){
            if(auth()->user()->is('under') && $order->kind_acceptance != 2){

                //Создаем БСО
                if(!$order->bso){
                    $order = InspectionAccept::createBSO($order);
                    return InspectionAccept::handle($order_id);
                }

                $order->inspection->accept_user_id = auth()->id();
                $order->inspection->accept_org_id = auth()->user()->organization_id;
                $order->push();

                InspectionChangeStatus::change_event($order_id, 7, auth()->id());

                $contract = $order;


                if($contract->bso){


                    $contract->payment_total = $contract->financial_policy->fix_price_sum;
                    $order->save();


                    $data_fp = new \stdClass();
                    $data_fp->financial_policy_id = $contract->financial_policy_id;

                    ContractsFinancialPolicy::update($contract, $data_fp);
                    $payment = $order->get_payment_first();


                    //акцептуем
                    $contract = ContractAccept::set($contract, 1);

                    //если акцепт не условный, то выпускаем договор
                    $contract = ContractStatus::set($contract, 4);

                    //продаём бсо
                    $bso = BsoStateOperations::set($contract->bso, 2);



                    if(!$payment){
                        $payment = EPayment::create_temp($order);
                    }


                    $payment->payment_total = $order->payment_total;
                    $payment->payment_data = getDateTime();
                    $payment->payment_type = 1;
                    $payment->payment_flow = 1;
                    $payment->bso_id = $order->bso_id;
                    $payment->bso_not_receipt = 1;

                    $method = PayMethod::searchMethod(1,1);
                    if($method) $payment->pay_method_id = $method->id;

                    $payment->save();




                    //обновляем
                    $payment = PaymentAcceptOperations::update_or_create($payment);

                    //если платёж временный, делаем его нормальным, неоплаченым
                    if($payment->statys_id == -1){

                        $parent_agent_id = 0;

                        if($contract->sales_condition != 0){
                            if($contract->manager){
                                $parent_agent_id = $contract->manager->parent_id;
                                $payment->manager_id = $contract->manager_id;
                            }
                            $payment->agent_id = $contract->agent_id;

                        }else{
                            if($contract->agent){
                                $payment->agent_id = $contract->agent_id;
                                $parent_agent_id = $contract->agent->parent_id;
                            }
                        }



                        $payment->parent_agent_id = $parent_agent_id;
                        $payment->point_sale_id = $contract->bso->point_sale_id;
                        $payment->save();


                        $payment = PaymentStatus::set($payment, 0);

                    }

                    if($payment = PaymentFinancialPolicy::actualize($payment)){
                        $payment = PaymentDiscounts::recount($payment);
                    }


                    $payment = PaymentExpectedPayments::expected($payment);

                    $hold_kv_product = $contract->bso->supplier->hold_kv_product($contract->product_id);
                    if($hold_kv_product->is_auto_bso == 1) {
                        PaymentInvoice::create_to_payment($payment, false);
                    }


                    InspectionChangeStatus::change_status($order_id, 5, auth()->id());
                    $order->inspection->financial_policy_kv_agent_total = $payment->financial_policy_kv_agent_total;
                    $order->push();




                    return responseStatus('Договор акцептован!');



                }
                else{
                    return responseStatus('Ошибка БСО!', false);
                }
            }else{
                return responseStatus('Вы не можете делать акцепт!', false);
            }


        }else{
            return responseStatus('Договор не в том статусе!', false);
        }

        return responseStatus('Ошибка акцепта!', false);
    }

    public static function createBSO($order)
    {

        $bso_serie = BsoSerie::where('insurance_companies_id', $order->insurance_companies_id)
            ->where('product_id', $order->product_id)->get()->last();

        if($bso_serie){

            $bso_title = $order->id;
            $bso = BsoItem::getElectronicBso($order->inspection->processing_user, $order->bso_supplier, $bso_serie->type_bso, $bso_serie, $bso_title);
            if($bso){
                $order->bso_id = $bso->id;
                $order->bso_title = $bso->bso_title;
                $order->insurance_companies_id = $bso->insurance_companies_id;
                $order->bso_supplier_id = $bso->bso_supplier_id;
                $order->product_id = $bso->product_id;
                $order->agent_id = $bso->agent_id;
                $order->save();
            }

        }
        return $order;
    }



}