<?php


namespace App\Domain\Processes\Scenaries\Enrichment\M5BackOld;


use App\Domain\Entities\Inspection\Inspection;
use App\Domain\Entities\Payments\EPayment;
use App\Domain\Processes\Operations\Bso\BsoStateOperations;
use App\Domain\Processes\Operations\Contracts\ContractAccept;
use App\Domain\Processes\Operations\Contracts\ContractsFinancialPolicy;
use App\Domain\Processes\Operations\Contracts\ContractStatus;
use App\Domain\Processes\Operations\Enrichment\M5BackOld\M5BackOldActsSK;
use App\Domain\Processes\Operations\Enrichment\M5BackOld\M5BackOldBso;
use App\Domain\Processes\Operations\Enrichment\M5BackOld\M5BackOldContracts;
use App\Domain\Processes\Operations\Enrichment\M5BackOld\M5BackOldInvoices;
use App\Domain\Processes\Operations\Enrichment\M5BackOld\M5BackOldReportsSK;
use App\Domain\Processes\Operations\Enrichment\M5BackOld\M5BackOldSettings;
use App\Domain\Processes\Operations\Enrichment\M5BackOld\M5BackOldUsers;
use App\Domain\Processes\Operations\Inspection\InspectionChangeStatus;
use App\Domain\Processes\Operations\Inspection\InspectionSetExecutor;
use App\Domain\Processes\Operations\Payments\PaymentAcceptOperations;
use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentExpectedPayments;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Domain\Processes\Operations\Payments\PaymentInvoice;
use App\Domain\Processes\Operations\Payments\PaymentStatus;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Subjects;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Finance\PayMethod;
use App\Models\Orders\InspectionComments;
use App\Models\Orders\InspectionOrdersLogs;
use App\Models\User;

class M5BackOldMain
{

    const TYPE = [
        'settings' => 'Настройки',
        'users' => 'Пользователи',
        'bso' => 'БСО',
        'contracts' => 'Договоры',
        'invoices' => 'Счета',
        'acts_sk' => 'Акты в СК',
        'reports_sk' => 'Отчеты',
    ];

    //'departments' => 'Подразделения',

    const SETTINGS = [
        'products'=>'Продукты',
        'points' => 'Точки',

        'finpolitics_groups' => 'Группы ФП',
        'organizations'=>'Организации',
        'insurance_companies' => 'СК и Поставщики',

    ];


    public static function getDataResult($type, $start, $counts, $count_all, $request, $state = 'info')
    {
        $res = new \stdClass();
        $res->state = 1;
        $res->msg = "Метод не найден";


        $_class = null;
        switch ($type) {
            case 'settings':
                $_class = new M5BackOldSettings();
                break;
            case 'users':
                $_class = new M5BackOldUsers();
                break;
            case 'bso':
                $_class = new M5BackOldBso();
                break;
            case 'contracts':
                $_class = new M5BackOldContracts();
                break;
            case 'invoices':
                $_class = new M5BackOldInvoices();
                break;
            case 'acts_sk':
                $_class = new M5BackOldActsSK();
                break;
            case 'reports_sk':
                $_class = new M5BackOldReportsSK();
                break;
        }

        if($_class){
            if($state == 'info'){
                $res = $_class->getDataInfo($start, $request);
            }
            if($state == 'updata'){
                $res = $_class->updateDataInfo($start, $counts, $request, $count_all);
            }
        }


        return $res;
    }



}