<?php


namespace App\Domain\Processes\Operations\Enrichment\M5BackOld;


use App\Models\BSO\BsoLogs;
use App\Models\BSO\BsoType;
use App\Models\Directories\BsoDopSerie;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\FinancialPolicyGroup;
use App\Models\Directories\HoldKv;
use App\Models\Directories\HoldKvDocuments;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\InsuranceCompaniesPrograms;
use App\Models\Directories\Products;
use App\Models\Directories\TypeBso;
use App\Models\Organizations\Organization;
use App\Models\Settings\Department;
use App\Models\Settings\FinancialGroup;
use App\Models\Settings\PointsSale;

class M5BackOldSettings
{




    public function getDataInfo($start, $request)
    {
        return M5BackOldSend::getDataInfo('settings', "view=info&type={$request->type_settings}");
    }

    public function updateDataInfo($start, $counts, $request, $count_all)
    {
        $res = new \stdClass();
        $res->state = 1;
        $res->msg = "M5 не настроен!";
        $res->start = 0;

        $response = M5BackOldSend::getDataInfo('settings', "view=updata&start={$start}&counts={$counts}&count_all={$count_all}&type={$request->type_settings}");

        if($response->result){
            $this->{"set_$request->type_settings"}($response->result);
            $res->state = $response->state;
            $res->msg = $response->msg;
            $res->progressbar = $response->progressbar;
        }

        return $res;
    }


    private function set_products($products)
    {
        Products::query()->truncate();
        \DB::table('product_programs')->delete();
        \DB::table('programs_online_list')->delete();

        foreach ($products as $product){
            \DB::table('products')->insert([
                'id'=>$product->id,
                'title'=>$product->title,
                'is_actual'=>$product->is_actual,
            ]);
        }
        return true;
    }

    private function set_organizations($organizations)
    {
        Organization::query()->truncate();
        \DB::table('org_bank_account')->delete();
        \DB::table('org_scans')->delete();
        \DB::table('points_departments')->delete();

        foreach ($organizations as $organization){
            \DB::table('organizations')->insert([
                'id' => $organization->id,
                'title' => $organization->title,
                'title_doc' => $organization->title,
                'is_actual' => $organization->is_active,
                'org_type_id' => 1,
            ]);
        }

        return true;
    }


    private function set_departments($departments)
    {
        Department::query()->truncate();
        foreach ($departments as $department){
            \DB::table('departments')->insert([
                'id' => $department->id,
                'title' => $department->title,
            ]);
        }

        return true;
    }

    private function set_points($points)
    {
        PointsSale::query()->truncate();
        foreach ($points as $point){
            \DB::table('points_sale')->insert([
                'id' => $point->id,
                'title' => $point->title,
                'is_actual' => $point->visible,
            ]);
        }

        return true;
    }

    private function set_finpolitics_groups($points)
    {
        FinancialGroup::query()->truncate();
        foreach ($points as $point){
            \DB::table('financial_group')->insert([
                'id' => $point->id,
                'title' => $point->title,
                'is_actual' => $point->is_actual,
            ]);
        }

        return true;
    }

    private function set_insurance_companies($insurance_companies)
    {


        \DB::table('financial_policies_groups_kv')->truncate();
        \DB::table('financial_policies')->truncate();
        \DB::table('hold_kv')->truncate();
        \DB::table('hold_kv_documents')->truncate();
        \DB::table('bso_suppliers')->truncate();


        \DB::table('type_bso')->truncate();
        \DB::table('bso_serie')->truncate();
        \DB::table('bso_dop_serie')->truncate();


        \DB::table('insurance_companies')->truncate();
        \DB::table('insurance_companies_programs')->truncate();

        \DB::table('incomes_expenses')->truncate();
        \DB::table('informing_segments')->truncate();
        \DB::table('installment_algorithms')->truncate();
        \DB::table('installment_algorithms_list')->truncate();

        \DB::table('bso_check_documents')->truncate();
        \DB::table('bso_suppliers_products_pay_methods')->truncate();
        \DB::table('bso_suppliers_products_pay_methods_access_bso')->truncate();
        \DB::table('bso_transfers')->truncate();

        \DB::table('financial_policies_segments')->truncate();





        foreach ($insurance_companies as $sk){


            \DB::table('insurance_companies')->insert([
                'id' => $sk->id,
                'title' => $sk->title,
                'is_actual' => $sk->is_actual,
            ]);


            foreach ($sk->bso_type as $bso_type){

                \DB::table('type_bso')->insert([
                    'id' => $bso_type->id,
                    'insurance_companies_id' => $sk->id,
                    'title' => $bso_type->title,
                    'is_actual' => $bso_type->is_actual,
                    'product_id' => $bso_type->bso_class,
                    'min_yellow' => $bso_type->min_yellow,
                    'min_red' => $bso_type->min_red,
                    'day_sk' => $bso_type->day_sk,
                    'day_agent' => $bso_type->day_agent,
                ]);

                $bso_class_id = 0;

                if(strlen($bso_type->title) > 2){
                    if(strripos($bso_type->title, 'E-') !== false){
                        $bso_class_id = 1;
                    }

                    if(strripos($bso_type->title, 'Е-') !== false){
                        $bso_class_id = 1;
                    }

                    if(strripos($bso_type->title, 'e-') !== false){
                        $bso_class_id = 1;
                    }

                    if(strripos($bso_type->title, 'Е-') !== false){
                        $bso_class_id = 1;
                    }

                    if(strripos($bso_type->title, 'е-') !== false){
                        $bso_class_id = 1;
                    }




                    if(strripos($bso_type->title, 'Кви') !== false){
                        $bso_class_id = 100;
                    }
                }


                foreach ($bso_type->bso_series as $bso_series){

                    $product = Products::query()->where('title', 'like', "%{$bso_type->title}%")->get()->last();
                    $product_id = $bso_type->bso_class;
                    if($product){
                        $product_id = $product->id;
                    }

                    \DB::table('bso_serie')->insert([
                        'id' => $bso_series->id,
                        'type_bso_id' => $bso_type->id,
                        'bso_serie' => $bso_series->title,
                        'product_id' => $product_id,
                        'insurance_companies_id' => $sk->id,
                        'bso_class_id' => $bso_class_id,
                    ]);


                    foreach ($bso_series->bso_series_dop as $bso_series_dop)
                    {
                        \DB::table('bso_dop_serie')->insert([
                            'id' => $bso_series_dop->id,
                            'type_bso_id' => $bso_type->id,
                            'bso_dop_serie' => $bso_series_dop->title,
                            'bso_serie_id' => $bso_series->id,
                            'insurance_companies_id' => $sk->id,
                        ]);
                    }


                }

            }


            foreach ($sk->bso_suppliers as $bso_suppliers){



                \DB::table('bso_suppliers')->insert([
                    'id' => $bso_suppliers->id,
                    'insurance_companies_id' => $bso_suppliers->sk_id,
                    'title' => $bso_suppliers->title,
                    'signer' => $bso_suppliers->signatory,
                    'source_org_id' => $bso_suppliers->org_id_source,
                    'purpose_org_id' => $bso_suppliers->org_id_dest,
                    'city_id' => 1,
                    'is_actual' => $bso_suppliers->is_active,
                ]);


                foreach ($bso_suppliers->hold_kv as $hold_kv){
                    \DB::table('hold_kv')->insert([
                        'id' => $hold_kv->id,
                        'insurance_companies_id' => $sk->id,
                        'bso_supplier_id' => $bso_suppliers->id,
                        'product_id' => $hold_kv->product_id,
                        'hold_type_id' => $hold_kv->type_id,
                        'is_check_policy' => 1,
                        'is_many_files' => $hold_kv->many_files,

                    ]);

                    foreach ($hold_kv->doc as $doc){

                        if(strlen($doc->title) > 3){
                            \DB::table('hold_kv_documents')->insert([
                                'hold_kv_id' => $hold_kv->id,
                                'file_title' => $doc->title,
                            ]);
                        }
                    }
                }


                foreach ($bso_suppliers->fin_politics as $fin_politics)
                {

                    \DB::table('financial_policies')->insert([
                        'id' => $fin_politics->id,
                        'bso_supplier_id' => $bso_suppliers->id,
                        'insurance_companies_id' => $sk->id,
                        'title' => $fin_politics->title,
                        'is_actual' => $fin_politics->is_actual,
                        'date_active' => $fin_politics->date_create,
                        'product_id' => $fin_politics->product_id,
                        'kv_bordereau' => $fin_politics->kv_bordereau,
                        'kv_dvou' => $fin_politics->kv_dvou,
                        'kv_sk' => $fin_politics->kv_sk,
                        'kv_agent' => $fin_politics->kv_agent,
                        'kv_parent' => 0,
                        'partnership_reward_1' => 0,
                        'partnership_reward_2' => 0,
                        'partnership_reward_3' => 0,

                    ]);

                    foreach ($fin_politics->finpolitics_groups as $finpolitics_groups){
                        \DB::table('financial_policies_groups_kv')->insert([
                            'financial_policy_id' => $fin_politics->id,
                            'financial_policies_group_id' => $finpolitics_groups->finpolitic_group_id,
                            'is_actual' => $finpolitics_groups->is_actual,
                            'kv_agent' => $finpolitics_groups->kv_agent_manager,
                            'kv_parent' => 0,
                        ]);
                    }

                }

            }

        }

        return true;
    }


}