<?php


namespace App\Domain\Processes\Operations\Enrichment\M5BackOld;


use App\Models\Acts\ReportAct;
use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoActsItems;
use App\Models\BSO\BsoCarts;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\BSO\BsoType;
use App\Models\Directories\BsoDopSerie;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\FinancialPolicyGroup;
use App\Models\Directories\HoldKv;
use App\Models\Directories\HoldKvDocuments;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\InsuranceCompaniesPrograms;
use App\Models\Directories\Products;
use App\Models\Directories\TypeBso;
use App\Models\Organizations\Organization;
use App\Models\Reports\ReportOrders;
use App\Models\Settings\FinancialGroup;
use App\Models\Settings\PointsSale;
use App\Models\Subject\Physical;
use App\Models\User;

class M5BackOldBso
{




    public function getDataInfo($start, $request)
    {
        if($request->bso_delete == 1){
            //БСО
            BsoLogs::query()->truncate();
            BsoCarts::query()->truncate();
            BsoActs::query()->truncate();
            BsoActsItems::query()->truncate();
            ReportOrders::query()->truncate();
            ReportAct::query()->truncate();
            BsoItem::query()->truncate();

            \DB::table('report_documents')->truncate();
            \DB::table('report_payment_sum')->truncate();
            \DB::table('reports_act_documents')->truncate();
            \DB::table('reports_advertising')->truncate();
            \DB::table('reports_orders_base_payments')->truncate();
            \DB::table('reservations')->truncate();

        }



        return M5BackOldSend::getDataInfo('bso', "view=info");
    }

    public function updateDataInfo($start, $counts, $request, $count_all)
    {
        $res = new \stdClass();
        $res->state = 1;
        $res->msg = "M5 не настроен!";

        $response = M5BackOldSend::getDataInfo('bso', "view=updata&start={$start}&counts={$counts}&count_all={$count_all}");

        if($response->result){
            $this->set_bso($response->result, $request);
            $res->state = $response->state;
            $res->msg = $response->msg;
            $res->progressbar = $response->progressbar;
            $res->start = $response->start;

        }

        return $res;
    }


    private function set_bso($bsos, $request)
    {

        \DB::raw('SET FOREIGN_KEY_CHECKS = 0;');

        foreach ($bsos as $bso){

            $user = User::find($bso->user_id);

            $check = BsoItem::where('id', $bso->id)->get()->last();
            $bso_serie = BsoSerie::where('id', $bso->bso_serie_id)->get()->last();

            //НЕ ПРАВИЛЬНЫЙ ПРОДУКТ
            //UPDATE bso_items SET  bso_items.product_id=(select type_bso.product_id from type_bso where type_bso.id = bso_items.type_bso_id)

            if(!$check && $bso_serie){

                \DB::table('bso_items')->insert([
                    'id' => $bso->id,
                    'bso_supplier_id' => $bso->sk_user_id,
                    'insurance_companies_id' => $bso->sk_id,
                    'point_sale_id' => $bso->tp_id,
                    'org_id' => $bso->org_id,
                    'bso_class_id' => $bso_serie->bso_class_id,
                    'product_id' => $bso_serie->product_id,
                    'type_bso_id' => $bso_serie->type_bso_id,
                    'state_id' => $bso->state_id,
                    'location_id' => $bso->location_id,
                    'user_id' => $bso->user_id,
                    'user_org_id' => ($user)?$user->organization_id:0,

                    'time_create' => $bso->time_create,
                    'time_target' => $bso->time_target,
                    'last_operation_time' => $bso->last_operation_time,
                    'transfer_to_agent_time' => $bso->transfer_to_agent_time,
                    'transfer_to_org_time' => $bso->transfer_to_org_time,
                    'transfer_to_sk_time' => $bso->transfer_to_sk_time,

                    'bso_serie_id' => $bso->bso_serie_id,
                    'bso_number' => $bso->bso_number,
                    'bso_dop_serie_id' => $bso->bso_dop_serie_id,
                    'bso_title' => $bso->bso_title,
                    'bso_blank_serie_id' => $bso->bso_blank_serie_id,
                    'bso_blank_number' => $bso->bso_blank_number,
                    'bso_blank_dop_serie_id' => $bso->bso_blank_dop_serie_id,
                    'bso_blank_title' => $bso->bso_blank_title,

                    'bso_comment' => $bso->bso_comment,
                    'bso_manager_id' => $bso->user_id,
                    'agent_id' => $bso->agent_id,
                    'contract_id' => $bso->contract_id,

                    'is_reserved' => $bso->is_reserved,
                    'bso_cart_id' => $bso->bso_cart_id,
                    'realized_act_id' => $bso->realized_act_id,

                    'acts_reserve_or_realized_id' => '',
                    'acts_implemented_id' => '',
                    'acts_sk_id' => $bso->reports_act_id,
                    'act_add_number' => '',

                    'bso_act_id' => '',
                    'transfer_id' => '',

                    'acts_to_underwriting_id' => '',
                ]);

            }
        }

        \DB::raw('SET FOREIGN_KEY_CHECKS = 1;');

        return true;
    }





}