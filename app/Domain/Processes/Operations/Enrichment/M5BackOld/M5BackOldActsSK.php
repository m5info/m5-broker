<?php


namespace App\Domain\Processes\Operations\Enrichment\M5BackOld;


use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Models\Acts\ReportAct;
use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoActsItems;
use App\Models\BSO\BsoCarts;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\BSO\BsoType;
use App\Models\Cashbox\Cashbox;
use App\Models\Cashbox\CashboxTransactions;
use App\Models\Contracts\ContractMessage;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsChat;
use App\Models\Contracts\ContractsDocuments;
use App\Models\Contracts\ContractsLogs;
use App\Models\Contracts\ObjectInsurer;
use App\Models\Contracts\Payments;
use App\Models\Contracts\Subjects;
use App\Models\Directories\BsoDopSerie;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\FinancialPolicyGroup;
use App\Models\Directories\HoldKv;
use App\Models\Directories\HoldKvDocuments;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\InsuranceCompaniesPrograms;
use App\Models\Directories\Products;
use App\Models\Directories\TypeBso;
use App\Models\File;
use App\Models\Finance\Invoice;
use App\Models\Finance\PayMethod;
use App\Models\Organizations\Organization;
use App\Models\Reports\ReportOrders;
use App\Models\Settings\FinancialGroup;
use App\Models\Settings\PointsSale;
use App\Models\Subject\Physical;
use App\Models\User;

class M5BackOldActsSK
{




    public function getDataInfo($start, $request)
    {

        if($request->acts_sk_delete == 1){

            ReportAct::query()->truncate();
        }



        return M5BackOldSend::getDataInfo('acts_sk', "view=info");
    }

    public function updateDataInfo($start, $counts, $request, $count_all)
    {
        $res = new \stdClass();
        $res->state = 1;
        $res->msg = "M5 не настроен!";


        $response = M5BackOldSend::getDataInfo('acts_sk', "view=updata&start={$start}&counts={$counts}&count_all={$count_all}");

        if($response->result){
            $this->set_acts_sk($response->result, $request);
            $res->state = $response->state;
            $res->msg = $response->msg;
            $res->progressbar = $response->progressbar;
            $res->start = $response->start;

        }

        return $res;
    }


    private function set_acts_sk($acts, $request)
    {

        foreach ($acts as $act)
        {


            \DB::table('reports_act')->insert([
                'id' => $act->id,
                'title' => $act->title,
                'signatory_org' => $act->signatory_org,
                'signatory_sk_bso_supplier' => $act->signatory_sk_bso_supplier,
                'bso_supplier_id' => $act->sk_user_id,
                'report_year' => $act->report_year,
                'report_month' => $act->report_month,
                'type_id' => (int)$act->type_act,
                'accept_status' => $act->accept_stat,
                'accept_user_id' => $act->create_user_id,
                'create_user_id' => $act->create_user_id,
                'report_date_start' => $act->report_date_start,
                'report_date_end' => $act->report_date_end,
                'accepted_at' => (strlen($act->accept_time)>5?$act->accept_time:date('Y-m-d')),
            ]);

        }


        return true;
    }


    public function checkPayments($invoice_id)
    {
        $pay = Payments::where('invoice_id', $invoice_id)->get();

        if($pay && isset($pay[0])){
            return true;
        }

        return false;
    }


}