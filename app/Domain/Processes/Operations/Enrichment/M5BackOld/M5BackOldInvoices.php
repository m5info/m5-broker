<?php


namespace App\Domain\Processes\Operations\Enrichment\M5BackOld;


use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Models\Acts\ReportAct;
use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoActsItems;
use App\Models\BSO\BsoCarts;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\BSO\BsoType;
use App\Models\Cashbox\Cashbox;
use App\Models\Cashbox\CashboxTransactions;
use App\Models\Contracts\ContractMessage;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsChat;
use App\Models\Contracts\ContractsDocuments;
use App\Models\Contracts\ContractsLogs;
use App\Models\Contracts\ObjectInsurer;
use App\Models\Contracts\Payments;
use App\Models\Contracts\Subjects;
use App\Models\Directories\BsoDopSerie;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\FinancialPolicyGroup;
use App\Models\Directories\HoldKv;
use App\Models\Directories\HoldKvDocuments;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\InsuranceCompaniesPrograms;
use App\Models\Directories\Products;
use App\Models\Directories\TypeBso;
use App\Models\File;
use App\Models\Finance\Invoice;
use App\Models\Finance\PayMethod;
use App\Models\Organizations\Organization;
use App\Models\Reports\ReportOrders;
use App\Models\Settings\FinancialGroup;
use App\Models\Settings\PointsSale;
use App\Models\Subject\Physical;
use App\Models\User;

class M5BackOldInvoices
{




    public function getDataInfo($start, $request)
    {
        if($request->invoices_delete == 1){
            //Договора
            Invoice::query()->truncate();

        }



        return M5BackOldSend::getDataInfo('invoices', "view=info");
    }

    public function updateDataInfo($start, $counts, $request, $count_all)
    {
        $res = new \stdClass();
        $res->state = 1;
        $res->msg = "M5 не настроен!";


        $response = M5BackOldSend::getDataInfo('invoices', "view=updata&start={$start}&counts={$counts}&count_all={$count_all}");

        if($response->result){
            $this->set_invoices($response->result, $request);
            $res->state = $response->state;
            $res->msg = $response->msg;
            $res->progressbar = $response->progressbar;
            $res->start = $response->start;

        }

        return $res;
    }


    private function set_invoices($invoices, $request)
    {

        foreach ($invoices as $info)
        {
            if($this->checkPayments($info->id))
            {
                $type = 'cashless';
                $type_invoice_payment_id = 3;

                if((int)$info->payment_type == 0){//0 БЕЗНАЛ
                    $type = 'cashless';
                    $type_invoice_payment_id = 3;
                }

                if((int)$info->payment_type == 1){//1 НАЛ

                    $type = 'cash';
                    $type_invoice_payment_id = 1;
                }

                if((int)$info->payment_type == 2){//2 СК
                    $type = 'cashless';
                    $type_invoice_payment_id = 4;
                }

                \DB::table('invoices')->insert([
                    'id' => $info->id,
                    'user_id' => $info->payment_user_id,
                    'status_id' => ($info->state_id+1),

                    'type' => $type,
                    'type_invoice_payment_id' => $type_invoice_payment_id,

                    'create_type' => 1,
                    'org_id' => $info->org_id,
                    'agent_id' => $info->agent_id,
                    'invoice_payment_user_id' => $info->user_id,
                    'invoice_payment_date' => $info->payment_time,
                ]);

                $data = Invoice::find($info->id);

                foreach ($data->payments as $payment)
                {
                    $payment->invoice_payment_total = $payment->getPaymentAgentSum();
                    $payment->invoice_payment_date = $info->payment_time;
                    $payment->statys_id = $info->state_id;
                    $payment->save();
                }

                $data->refreshInvoice();

            }
        }


        return true;
    }


    public function checkPayments($invoice_id)
    {
        $pay = Payments::where('invoice_id', $invoice_id)->get()->first();

        if($pay){
            return true;
        }

        return false;
    }


}