<?php


namespace App\Domain\Processes\Operations\Enrichment\M5BackOld;


use App\Models\Acts\ReportAct;
use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoActsItems;
use App\Models\BSO\BsoCarts;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\BSO\BsoType;
use App\Models\Cashbox\Cashbox;
use App\Models\Cashbox\CashboxTransactions;
use App\Models\Contracts\ContractMessage;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsChat;
use App\Models\Contracts\ContractsDocuments;
use App\Models\Contracts\ContractsLogs;
use App\Models\Directories\BsoDopSerie;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\FinancialPolicyGroup;
use App\Models\Directories\HoldKv;
use App\Models\Directories\HoldKvDocuments;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\InsuranceCompaniesPrograms;
use App\Models\Directories\Products;
use App\Models\Finance\Invoice;
use App\Models\Organizations\Organization;
use App\Models\Reports\ReportOrders;
use App\Models\Settings\Department;
use App\Models\Settings\FinancialGroup;
use App\Models\Settings\PointsSale;

class M5BackOldClean
{


    public static function clean()
    {
        //Настройки
        Products::query()->truncate();
        Organization::query()->truncate();
        //PointsSale::query()->truncate();
        //Department::query()->truncate();
        FinancialGroup::query()->truncate();
        \DB::table('incomes_expenses')->truncate();
        \DB::table('incomes_expenses_categories')->truncate();
        \DB::table('incomes_expenses_documents')->truncate();

        \DB::table('org_bank_account')->truncate();
        \DB::table('points_departments')->truncate();

        \DB::table('product_programs')->truncate();
        \DB::table('programs_online_list')->truncate();

        \DB::table('base_rate')->truncate();
        \DB::table('inspection_comments')->truncate();
        \DB::table('news')->truncate();
        \DB::table('inspection_comments')->truncate();





        //СК
        \DB::table('financial_policies_groups_kv')->truncate();
        \DB::table('financial_policies')->truncate();
        \DB::table('hold_kv')->truncate();
        \DB::table('hold_kv_documents')->truncate();
        \DB::table('bso_suppliers')->truncate();


        \DB::table('type_bso')->truncate();
        \DB::table('bso_serie')->truncate();
        \DB::table('bso_dop_serie')->truncate();


        \DB::table('insurance_companies')->truncate();
        \DB::table('insurance_companies_programs')->truncate();

        \DB::table('incomes_expenses')->truncate();
        \DB::table('informing_segments')->truncate();
        \DB::table('installment_algorithms')->truncate();
        \DB::table('installment_algorithms_list')->truncate();

        \DB::table('bso_check_documents')->truncate();
        \DB::table('bso_suppliers_products_pay_methods')->truncate();
        \DB::table('bso_suppliers_products_pay_methods_access_bso')->truncate();
        \DB::table('bso_transfers')->truncate();

        \DB::table('financial_policies_segments')->truncate();


        //Пользователи
        \DB::table('users')->where('id', '!=', auth()->id())->delete();
        \DB::table('users_balance')->truncate();
        \DB::table('users_balance_transactions')->truncate();
        \DB::table('users_limit_bso')->truncate();
        \DB::table('users_scans')->truncate();
        \DB::table('users2columns')->truncate();
        \DB::table('subjects')->truncate();
        \DB::table('subjects_fl')->truncate();
        \DB::table('subjects_juridical')->truncate();
        \DB::table('subjects_physical')->truncate();
        \DB::table('subjects_ul')->truncate();



        //БСО
        BsoLogs::query()->truncate();
        BsoCarts::query()->truncate();
        BsoActs::query()->truncate();
        BsoActsItems::query()->truncate();
        ReportOrders::query()->truncate();
        ReportAct::query()->truncate();
        BsoItem::query()->truncate();

        \DB::table('report_documents')->truncate();
        \DB::table('report_payment_sum')->truncate();
        \DB::table('reports_act_documents')->truncate();
        \DB::table('reports_advertising')->truncate();
        \DB::table('reports_orders_base_payments')->truncate();
        \DB::table('reservations')->truncate();
        \DB::table('notifications')->truncate();


        //Договора
        Invoice::query()->truncate();
        Cashbox::query()->truncate();
        CashboxTransactions::query()->truncate();
        ContractMessage::query()->truncate();
        Contracts::query()->truncate();
        ContractsChat::query()->truncate();
        ContractsDocuments::query()->truncate();
        ContractsLogs::query()->truncate();

        \DB::table('contracts_masks')->truncate();
        \DB::table('contracts_scans')->truncate();
        \DB::table('contracts_terms')->truncate();
        \DB::table('drivers')->truncate();
        \DB::table('expected_payments')->truncate();
        \DB::table('foundation_payments')->truncate();


        \DB::table('inspection_orders')->truncate();
        \DB::table('inspection_orders_logs')->truncate();
        \DB::table('inspection_orders_reports')->truncate();
        \DB::table('inspection_orders_reports_documents')->truncate();


        \DB::table('kbm_calculations')->truncate();
        \DB::table('log_events')->truncate();

        \DB::table('notifications')->truncate();

        \DB::table('object_insurer')->truncate();
        \DB::table('object_insurer_auto')->truncate();
        \DB::table('object_insurer_auto_equipment')->truncate();
        \DB::table('online_calculations')->truncate();
        \DB::table('optional_equipment')->truncate();
        \DB::table('order_comments')->truncate();
        \DB::table('order_documents')->truncate();

        \DB::table('orders')->truncate();
        \DB::table('orders_log_states')->truncate();
        \DB::table('org_scans')->truncate();

        \DB::table('files')->where('is_migrate', 1)->delete();

        \DB::table('payment_accepts')->truncate();
        \DB::table('payments')->truncate();









        return true;
    }



}