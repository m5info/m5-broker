<?php


namespace App\Domain\Processes\Operations\Enrichment\M5BackOld;


use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Models\Acts\ReportAct;
use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoActsItems;
use App\Models\BSO\BsoCarts;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\BSO\BsoType;
use App\Models\Cashbox\Cashbox;
use App\Models\Cashbox\CashboxTransactions;
use App\Models\Contracts\ContractMessage;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsChat;
use App\Models\Contracts\ContractsDocuments;
use App\Models\Contracts\ContractsLogs;
use App\Models\Contracts\ObjectInsurer;
use App\Models\Contracts\Payments;
use App\Models\Contracts\Subjects;
use App\Models\Directories\BsoDopSerie;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\FinancialPolicyGroup;
use App\Models\Directories\HoldKv;
use App\Models\Directories\HoldKvDocuments;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\InsuranceCompaniesPrograms;
use App\Models\Directories\Products;
use App\Models\Directories\TypeBso;
use App\Models\File;
use App\Models\Finance\Invoice;
use App\Models\Finance\PayMethod;
use App\Models\Organizations\Organization;
use App\Models\Reports\ReportOrders;
use App\Models\Settings\FinancialGroup;
use App\Models\Settings\PointsSale;
use App\Models\Subject\Physical;
use App\Models\User;

class M5BackOldReportsSK
{




    public function getDataInfo($start, $request)
    {

        if($request->reports_sk_delete == 1){

            ReportOrders::query()->truncate();
        }



        return M5BackOldSend::getDataInfo('reports_sk', "view=info");
    }

    public function updateDataInfo($start, $counts, $request, $count_all)
    {
        $res = new \stdClass();
        $res->state = 1;
        $res->msg = "M5 не настроен!";


        $response = M5BackOldSend::getDataInfo('reports_sk', "view=updata&start={$start}&counts={$counts}&count_all={$count_all}");

        if($response->result){
            $this->set_reports_sk($response->result, $request);
            $res->state = $response->state;
            $res->msg = $response->msg;
            $res->progressbar = $response->progressbar;
            $res->start = $response->start;

        }

        return $res;
    }


    private function set_reports_sk($reports, $request)
    {

        foreach ($reports as $info)
        {

            if($this->checkPayments($info->id)) {



                $accept_status = (int)$info->accept_stat;
                $accept_states_arr  = [0 => 1, 1 => 3, 2 => 6];


                \DB::table('reports_orders')->insert([
                    'id' => $info->id,
                    'bso_supplier_id' => $info->sk_user_id,
                    'type_id' => $info->is_dvou,
                    'create_user_id' => $info->create_user_id,
                    'created_at' => $info->create_time,
                    'title' => $info->title,
                    'signatory_org' => $info->signatory_org,
                    'signatory_sk_bso_supplier' => $info->signatory_sk_bso_supplier,
                    'report_year' => $info->report_year,
                    'report_month' => $info->report_month,
                    'report_date_start' => $info->report_date_start,
                    'report_date_end' => $info->report_date_end,
                    'accept_status' => $accept_states_arr[$accept_status],
                    'accepted_at' => $info->accept_time,
                    'accept_user_id' => $info->accept_user_id,
                    'comments' => (string)$info->comment,
                ]);

                $report = ReportOrders::find($info->id);
                $report->refreshSumOrder();

            }

        }


        return true;
    }


    public function checkPayments($invoice_id)
    {

        $pay = Payments::where('reports_order_id', $invoice_id)->orWhere('reports_dvou_id', $invoice_id)->get()->first();

        if($pay){
            return true;
        }

        return false;
    }


}