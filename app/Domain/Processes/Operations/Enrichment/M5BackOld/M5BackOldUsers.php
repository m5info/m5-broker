<?php


namespace App\Domain\Processes\Operations\Enrichment\M5BackOld;


use App\Models\BSO\BsoLogs;
use App\Models\BSO\BsoType;
use App\Models\Directories\BsoDopSerie;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\FinancialPolicyGroup;
use App\Models\Directories\HoldKv;
use App\Models\Directories\HoldKvDocuments;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\InsuranceCompaniesPrograms;
use App\Models\Directories\Products;
use App\Models\Directories\TypeBso;
use App\Models\Organizations\Organization;
use App\Models\Settings\FinancialGroup;
use App\Models\Settings\PointsSale;
use App\Models\Subject\Physical;
use App\Models\User;

class M5BackOldUsers
{




    public function getDataInfo($start, $request)
    {
        if($request->user_delete == 1){
            //Пользователи
            \DB::table('users')->where('id', '!=', auth()->id())->delete();
            \DB::table('users_balance')->truncate();
            \DB::table('users_balance_transactions')->truncate();
            \DB::table('users_limit_bso')->truncate();
            \DB::table('users_scans')->truncate();
            \DB::table('users2columns')->truncate();
            \DB::table('subjects')->truncate();
            \DB::table('subjects_fl')->truncate();
            \DB::table('subjects_juridical')->truncate();
            \DB::table('subjects_physical')->truncate();
            \DB::table('subjects_ul')->truncate();
        }



        return M5BackOldSend::getDataInfo('users', "view=info");
    }

    public function updateDataInfo($start, $counts, $request, $count_all)
    {
        $res = new \stdClass();
        $res->state = 1;
        $res->msg = "M5 не настроен!";

        $response = M5BackOldSend::getDataInfo('users', "view=updata&start={$start}&counts={$counts}&count_all={$count_all}");

        if($response->result){
            $this->set_users($response->result, $request);
            $res->state = $response->state;
            $res->msg = $response->msg;
            $res->progressbar = $response->progressbar;
            $res->start = $response->start;

        }

        return $res;
    }


    private function set_users($users, $request)
    {


        foreach ($users as $user){

            $check_user = User::where('id', $user->id_user)->get()->last();

            if($user->id_user != auth()->id() && !$check_user){

                $role_id = $request->user_agent;
                $organization_id = $request->user_org;

                $is_parent = ($user->leader)?$user->leader:0;
                if($is_parent > 0){
                    $role_id = $request->user_parent;
                }

                if($user->type_id == 1){
                    $role_id = $request->user_manager;
                }


                $password = bcrypt(trim($user->user_password));
                $subject = $this->getUserSubject($user);

                \DB::table('users')->insert([
                    'id' => $user->id_user,
                    'name' => $user->user_title,
                    'email' => $user->user_login,
                    'password' => $password,
                    'subject_type_id' => 1,
                    'subject_id' => $subject->id,
                    'role_id' => $role_id,
                    'organization_id' => $organization_id,
                    'is_parent' => $is_parent,
                    'parent_id' => $user->pid,
                    'financial_group_id' => 0,
                    'department_id' => $user->sale_channel_id,
                    'status_user_id' => ((int)$user->ban_level == 2?1:0),
                    'ban_level' => $user->ban_level,
                    'ban_reason' => $user->ban_reason,
                    'point_sale_id' => $user->filial_sale_id,
                    'settings' => '',
                ]);



            }

            $data_user = User::find($user->id_user);

            $this->updateUserBalance($data_user, 1, $user->balance);
            $this->updateUserBalance($data_user, 2, $user->balance_reserv);
            $this->updateUserBalance($data_user, 4, $user->balance_bn);
            $this->updateUserBalance($data_user, 3, $user->balance_sk);

        }

        return true;
    }


    public function getUserSubject($info)
    {
        return Physical::create([
            'first_name' => $info->firstname,
            'second_name' => $info->surname,
            'middle_name' => $info->middlename,
        ]);
    }


    public function updateUserBalance($user, $balance_id, $sum)
    {
        if($user && $sum){

            $balance = $user->getBalance($balance_id);
            if($balance && getFloatFormat($sum) > 0){
                $balance->setTransactions(
                    0, 0, getFloatFormat($sum), date('Y-m-d H:i:s'), "Перенос баланса из старой базы!"
                );
            }
        }

    }


}