<?php


namespace App\Domain\Processes\Operations\Enrichment\M5BackOld;


use App\Domain\Entities\Inspection\Inspection;
use App\Domain\Entities\Payments\EPayment;
use App\Domain\Processes\Operations\Bso\BsoStateOperations;
use App\Domain\Processes\Operations\Contracts\ContractAccept;
use App\Domain\Processes\Operations\Contracts\ContractsFinancialPolicy;
use App\Domain\Processes\Operations\Contracts\ContractStatus;
use App\Domain\Processes\Operations\Inspection\InspectionChangeStatus;
use App\Domain\Processes\Operations\Inspection\InspectionSetExecutor;
use App\Domain\Processes\Operations\Payments\PaymentAcceptOperations;
use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentExpectedPayments;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Domain\Processes\Operations\Payments\PaymentInvoice;
use App\Domain\Processes\Operations\Payments\PaymentStatus;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Subjects;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Finance\PayMethod;
use App\Models\Orders\InspectionComments;
use App\Models\Orders\InspectionOrdersLogs;
use App\Models\Settings\SettingsSystem;
use App\Models\User;

class M5BackOldSend
{


    public static function getDataInfo($method, $param)
    {

        $url = SettingsSystem::getDataParam('m5_back_old', 'url')."$method.php?$param";

        try {
            $res = file_get_contents($url);
            $res = \GuzzleHttp\json_decode($res);
        } catch (\Exception $e) {
            $res = new \stdClass();
            $res->state = 1;
            $res->msg = "M5 не настроен!";
        } finally {

        }
        return $res;
    }



}