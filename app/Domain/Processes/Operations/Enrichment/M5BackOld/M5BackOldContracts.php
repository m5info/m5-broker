<?php


namespace App\Domain\Processes\Operations\Enrichment\M5BackOld;


use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Domain\Processes\Operations\Payments\PaymentReceipt;
use App\Models\Acts\ReportAct;
use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoActsItems;
use App\Models\BSO\BsoCarts;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\BSO\BsoType;
use App\Models\Cashbox\Cashbox;
use App\Models\Cashbox\CashboxTransactions;
use App\Models\Contracts\ContractMessage;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsChat;
use App\Models\Contracts\ContractsDocuments;
use App\Models\Contracts\ContractsLogs;
use App\Models\Contracts\ObjectInsurer;
use App\Models\Contracts\Payments;
use App\Models\Contracts\Subjects;
use App\Models\Directories\BsoDopSerie;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\FinancialPolicyGroup;
use App\Models\Directories\HoldKv;
use App\Models\Directories\HoldKvDocuments;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\InsuranceCompaniesPrograms;
use App\Models\Directories\Products;
use App\Models\Directories\TypeBso;
use App\Models\File;
use App\Models\Finance\Invoice;
use App\Models\Finance\PayMethod;
use App\Models\Organizations\Organization;
use App\Models\Reports\ReportOrders;
use App\Models\Settings\FinancialGroup;
use App\Models\Settings\PointsSale;
use App\Models\Subject\Physical;
use App\Models\User;

class M5BackOldContracts
{




    public function getDataInfo($start, $request)
    {
        if($request->contracts_delete == 1){
            //Договора
            Invoice::query()->truncate();
            Cashbox::query()->truncate();
            CashboxTransactions::query()->truncate();
            ContractMessage::query()->truncate();
            Contracts::query()->truncate();
            ContractsChat::query()->truncate();
            ContractsDocuments::query()->truncate();
            ContractsLogs::query()->truncate();


            \DB::table('contracts_masks')->truncate();
            \DB::table('contracts_scans')->truncate();
            \DB::table('contracts_terms')->truncate();
            \DB::table('drivers')->truncate();
            \DB::table('expected_payments')->truncate();
            \DB::table('foundation_payments')->truncate();


            \DB::table('inspection_orders')->truncate();
            \DB::table('inspection_orders_logs')->truncate();
            \DB::table('inspection_orders_reports')->truncate();
            \DB::table('inspection_orders_reports_documents')->truncate();


            \DB::table('kbm_calculations')->truncate();
            \DB::table('log_events')->truncate();

            \DB::table('notifications')->truncate();

            \DB::table('object_insurer')->truncate();
            \DB::table('object_insurer_auto')->truncate();
            \DB::table('object_insurer_auto_equipment')->truncate();
            \DB::table('online_calculations')->truncate();
            \DB::table('optional_equipment')->truncate();
            \DB::table('order_comments')->truncate();
            \DB::table('order_documents')->truncate();

            \DB::table('orders')->truncate();
            \DB::table('orders_log_states')->truncate();
            \DB::table('org_scans')->truncate();


            \DB::table('payment_accepts')->truncate();
            \DB::table('payments')->truncate();
            \DB::table('files')->where('is_migrate', 1)->delete();



        }



        return M5BackOldSend::getDataInfo('contracts', "view=info");
    }

    public function updateDataInfo($start, $counts, $request, $count_all)
    {
        $res = new \stdClass();
        $res->state = 1;
        $res->msg = "M5 не настроен!";


        $response = M5BackOldSend::getDataInfo('contracts', "view=updata&start={$start}&counts={$counts}&count_all={$count_all}");

        if($response->result){
            $this->set_contracts($response->result, $request);
            $res->state = $response->state;
            $res->msg = $response->msg;
            $res->progressbar = $response->progressbar;
            $res->start = $response->start;

        }

        return $res;
    }


    private function set_contracts($contracts, $request)
    {


        foreach ($contracts as $info)
        {
            $bso = BsoItem::where('id', $info->bso_id)->get()->last();
            $contract = Contracts::where('id', $info->id)->get()->last();

            if($bso && !$contract){

                $sales_condition = $info->terms_sale;
                $object_insurer = ObjectInsurer::createDefaultProduct($bso->product);

                if($sales_condition == 0){
                    $agent = User::find($info->agent_id);
                }else{
                    $agent = User::find($info->manager_id);
                }


               \DB::table('contracts')->insert([
                    'id' => $info->id,

                    'created_at' => getDateTime(),
                    'updated_at' => getDateTime(),

                    'statys_id' => 4,
                    'user_id' => $info->techander_id_accept,
                    'check_user_id' => $info->techander_id_accept,

                    'agent_id' => $info->agent_id,
                    'manager_id' => $info->manager_id,

                    'bso_id' => $info->bso_id,
                    'bso_title' => $bso->bso_title,

                    'insurance_companies_id' => $bso->insurance_companies_id,
                    'bso_supplier_id' => $bso->bso_supplier_id,
                    'product_id' => $bso->product_id,

                    'parent_agent_id' => ($agent)?$agent->parent_id:0,
                    'sales_condition' => $sales_condition,

                    'sign_date' => $info->contract_date,
                    'begin_date' => $info->contract_begin,
                    'end_date' => $info->contract_end,

                    'object_insurer_id' => $object_insurer->id,
                    'payment_total' => $info->payment_total,
                    'kind_acceptance' => $info->kind_acceptance,
                    'insurer_id' => $this->getInsurer($info),

                ]);

                $contract = Contracts::find($info->id);

                foreach ($info->payments as $pay)
                {
                    //type2 - 0 Взнос 1 Долг 2 Выплата
                    $type_id = $pay->type2;

                    $bso = $contract->bso;

                    $payment_flow = ($pay->submit_receiver == 'sk')?1:0;
                    $payment_type = ($pay->submit_from_agent == 'cash')?0:1;
                    $pay_method_id = 0;
                    $pay_method = PayMethod::searchMethod($payment_type, $payment_flow);
                    if($pay_method) $pay_method_id = $pay_method->id;


                    \DB::table('payments')->insert([
                        'id' => $pay->id,
                        'statys_id' => 0,
                        'type_id' => $type_id,
                        'invoice_id' => $pay->cashbox_id,
                        'bso_id' => $pay->bso_id,
                        'contract_id' => $pay->contract_id,
                        'payment_number' => $pay->payment_number,
                        'payment_data' => $pay->payment_date,

                        'payment_type' => $payment_type,
                        'payment_flow' => $payment_flow,
                        'pay_method_id' => $pay_method_id,

                        'payment_total' => $pay->payment_value,

                        'financial_policy_id' => $pay->financial_policy,
                        'financial_policy_manually_set' => (int)$pay->financial_policy_manually_set,

                        'financial_policy_kv_bordereau' => $pay->financial_policy_kv_bordereau,
                        'financial_policy_kv_dvoy' => $pay->financial_policy_kv_dvoy,

                        'financial_policy_kv_agent' => $pay->financial_policy_kv_agent,
                        "financial_policy_kv_agent_total" => getTotalSumToPrice($pay->payment_value, $pay->financial_policy_kv_agent),

                        "financial_policy_kv_bordereau_total" => getTotalSumToPrice($pay->payment_value, $pay->financial_policy_kv_bordereau),
                        "financial_policy_kv_dvoy_total" => getTotalSumToPrice($pay->payment_value, $pay->financial_policy_kv_dvoy),

                        'official_discount' => getFloatFormat($pay->real_official_discount),
                        'informal_discount' => getFloatFormat($pay->real_informal_discount),

                        'bso_not_receipt' => ((int)$pay->backoffice_bso2_id > 0)?0:1,
                        'bso_receipt' => $pay->bso2,
                        'bso_receipt_id' => $pay->backoffice_bso2_id,

                        'agent_id' => $pay->agent_id,
                        'manager_id' => $pay->manager_id,
                        'parent_agent_id' => $pay->nop_id,

                        'invoice_payment_total' => 0,
                        'invoice_payment_date' => null,

                        'point_sale_id' => $bso->point_sale_id,
                        'set_balance' => $pay->to_balance,

                        'acts_sk_id' => $pay->reports_act_id,
                        'reports_order_id' => $pay->reports_order_id,
                        'reports_dvou_id' => $pay->reports_dvou_id,

                    ]);

                    $payment = Payments::find($pay->id);

                    if($type_id == 0){
                        //$payment = PaymentDiscounts::recount($payment);


                        if($contract->sales_condition == 1){

                            $marjing = ($payment->financial_policy_kv_bordereau+$payment->financial_policy_kv_dvoy)-(getFloatFormat($payment->informal_discount));

                        }else{

                            $marjing = ($payment->financial_policy_kv_bordereau+$payment->financial_policy_kv_dvoy)-(getFloatFormat($payment->financial_policy_kv_agent));

                        }

                        $payment->update([
                            "official_discount_total" => getTotalSumToPrice($payment->payment_total, $payment->official_discount),
                            "informal_discount_total" => getTotalSumToPrice($payment->payment_total, $payment->informal_discount),
                            "bank_kv_total" => getTotalSumToPrice($payment->payment_total, $payment->bank_kv),

                            "financial_policy_marjing" => $marjing,
                            "financial_policy_marjing_total" => getTotalSumToPrice($payment->payment_total, $marjing),
                        ]);

                        //Квитанция
                        if($payment->bso_receipt_id > 0){
                            $payment =PaymentReceipt::attach($payment, $payment);
                        }

                        if((int)$payment->payment_number == 1)
                        {
                            //Обновляем ФП у договора
                            $this->updateContractFp($contract, $payment);
                        }
                    }

                    if($type_id == 1){
                        $payment->financial_policy_marjing = 100;
                        $payment->financial_policy_marjing_total = $pay->payment_value;

                        $payment->financial_policy_kv_agent = -100;
                        $payment->financial_policy_kv_agent_total = $pay->payment_value * -1;

                    }

                    if($type_id == 2){
                        $payment->payment_total = $pay->real_payment_value;
                        $payment->financial_policy_marjing = -100;
                        $payment->financial_policy_marjing_total = $pay->real_payment_value * -1;

                        $payment->financial_policy_kv_agent = 100;
                        $payment->financial_policy_kv_agent_total = $pay->real_payment_value;

                    }

                    $payment->save();


                }

                foreach ($info->scans as $scan)
                {
                    //Сканы
                    $nameFullScanArr = explode('/', $scan->file_url);
                    $nameScanArr = $nameFullScanArr[(count($nameFullScanArr)-1)];
                    $nameArr = explode('.', $nameScanArr);
                    $folder = 'file_old'.str_replace($nameScanArr, '', $scan->file_url);
                    $ext = $nameArr[1];
                    $name = $nameArr[0];

                    $files = File::create([
                        'user_id' => auth()->id(),
                        'name' => $name,
                        'ext' => $ext,
                        'original_name' => $scan->file_name,
                        'folder' => $folder,
                        'host' => request()->getHttpHost(),
                        'is_migrate' => 1,
                    ]);

                    \DB::table('contracts_scans')->insert([
                        'contract_id' => $contract->id,
                        'file_id' => $files->id,
                        'created_at' => getDateTime(),
                        'updated_at' => getDateTime(),
                    ]);



                }



            }
        }


        return true;
    }


    public function getInsurer($data)
    {
        $updated_data = [
            'type' => isset($data->type_insurer) ? $data->type_insurer : 0,
            'title' => $data->insurer,
            'phone' => '',
            'email' => '',
            'doc_serie' => '',
            'doc_number' => '',
            'inn' => '',
            'kpp' =>  '',
        ];
        return Subjects::create($updated_data)->id;
    }

    public function updateContractFp($contract, $payment)
    {

        $contract->financial_policy_manually_set = $payment->financial_policy_manually_set;
        $contract->financial_policy_id = $payment->financial_policy_id;
        $contract->financial_policy_kv_bordereau = $payment->financial_policy_kv_bordereau;
        $contract->financial_policy_kv_dvoy = $payment->financial_policy_kv_dvoy;
        $contract->financial_policy_kv_agent = $payment->financial_policy_kv_agent;
        $contract->save();
        return true;
    }


}