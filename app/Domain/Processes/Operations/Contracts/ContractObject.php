<?php

namespace App\Domain\Processes\Operations\Contracts;


use App\Domain\Entities\Contracts\EContract;
use App\Helpers\EmptyObject;
use App\Models\Contracts\ObjectInsurer;
use App\Models\Contracts\ObjectInsurerAuto;
use App\Models\Contracts\ObjectInsurerFlats;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;

class ContractObject{


    public static function update_or_create($contract, $data){

        $contract = EContract::get_first($contract);

        if(isset($data->id) && (int)$data->id>0){
            $object = ObjectInsurer::find($data->id);
        }else{
            $object = ObjectInsurer::create([]);
        }

        $object->update([
            'type' => isset($data->type) ? $data->type : $object->type,
            'title' => isset($data->title) ? $data->title : $object->title,
        ]);

        $contract->update([
            'object_insurer_id' => $object->id
        ]);


        if(isset($data->type) && (int)$data->type == 1) {

            $auto = ObjectInsurerAuto::where('object_insurer_id', $object->id)->first();

            $old_auto = $auto ? $auto : new EmptyObject();

            $update_data = [
                'ts_category' => isset($data->ts_category) ? $data->ts_category : $old_auto->ts_category,
                'power' => isset($data->power) ? getFloatFormat($data->power) : $old_auto->power,
                'reg_number' => isset($data->reg_number) ? $data->reg_number : $old_auto->reg_number,
                'mark_id' => isset($data->mark_id) ? $data->mark_id : $old_auto->mark_id,
                'model_id' => isset($data->model_id) ? $data->model_id : $old_auto->model_id,
                'vin' => isset($data->vin) ? $data->vin : $old_auto->vin,
                'car_year' => isset($data->car_year) ? (int)$data->car_year : $old_auto->car_year,
                'object_insurer_id' => $object->id,
                'is_new' => isset($data->is_new) ? getFloatFormat($data->is_new) : $old_auto->is_new,
            ];

            if($auto){
                $auto->update($update_data);
            }else{
                ObjectInsurerAuto::create($update_data);
            }

        }

        return $contract;
    }

    public static function update_or_create_auto_osago($object_insurer_id, $data){

        $title = '';

        if(isset($data->mark_id) && $mark = VehicleMarks::where('id', $data->mark_id)->first()){
            $title .= "{$mark->title} ";
        }

        if(isset($data->model_id) && $model = VehicleModels::where('id', $data->model_id)->first()){
            $title .= "{$model->title} ";
        }

        if(isset($data->car_year)){
            $title .= "({$data->car_year})";
        }


        if(isset($object_insurer_id) && (int)$object_insurer_id>0){
            $object = ObjectInsurer::find($object_insurer_id);
        }else{
            $object = ObjectInsurer::create([]);
        }

        $object->update([
            'type' => 1,
            'title' => $title,
        ]);

        $auto = ObjectInsurerAuto::where('object_insurer_id', $object->id)->first();

        $old_auto = $auto ? $auto : new EmptyObject();

        $update_data = [
            'ts_category' => isset($data->ts_category) ? $data->ts_category : $old_auto->ts_category,
            'mark_id' => isset($data->mark_id) ? $data->mark_id : $old_auto->mark_id,
            'model_id' => isset($data->model_id) ? $data->model_id : $old_auto->model_id,
            'purpose_id' => isset($data->purpose_id) ? $data->purpose_id : $old_auto->purpose_id,
            'transit_number' => isset($data->transit_number) ? $data->transit_number : 0,

            'vin' => isset($data->vin) ? $data->vin : $old_auto->vin,
            'body_number' => isset($data->body_number) ? $data->body_number : $old_auto->body_number,
            'reg_number' => isset($data->reg_number) ? $data->reg_number : $old_auto->reg_number,
            'car_year' => isset($data->car_year) ? (int)$data->car_year : $old_auto->car_year,

            'power' => isset($data->power) ? getFloatFormat($data->power) : $old_auto->power,
            'passengers_count' => isset($data->passengers_count) ? (int)$data->passengers_count : $old_auto->passengers_count,
            'weight' => isset($data->weight) ? getFloatFormat($data->weight) : $old_auto->weight,
            'capacity' => isset($data->capacity) ? getFloatFormat($data->capacity) : $old_auto->capacity,

            'is_trailer' => isset($data->is_trailer) ? (int)$data->is_trailer : 0,
            'is_new' => isset($data->is_new) ? (int)$data->is_new : 0,
            'foreign_reg_number' => isset($data->foreign_reg_number) ? (int)$data->foreign_reg_number : 0,
            'doc_type' => isset($data->doc_type) ? (int)$data->doc_type : $old_auto->doc_type,
            'docserie' => isset($data->docserie) ? $data->docserie : $old_auto->docserie,
            'docnumber' => isset($data->docnumber) ? $data->docnumber : $old_auto->docnumber,
            'docdate' => isset($data->docdate) && $data->docdate !== '' ? getDateFormatEn($data->docdate) : $old_auto->docdate,

            'dk_number' => isset($data->dk_number) ? $data->dk_number : $old_auto->dk_number,
            'dk_date_from' => isset($data->dk_date_from) && $data->dk_date_from !== '' ? getDateFormatEn($data->dk_date_from) : $old_auto->dk_date_from,
            'dk_date' => isset($data->dk_date) && $data->dk_date !== '' ? getDateFormatEn($data->dk_date) : $old_auto->dk_date,
            'comment' => isset($data->comment) && $data->comment !== '' ? $data->comment : $old_auto->comment,

            'object_insurer_id' => $object->id
        ];

        if($auto){
            $auto->update($update_data);
        }else{
            ObjectInsurerAuto::create($update_data);
        }

        return $object;

    }


    public static function update_or_create_auto_kasko($object_insurer_id, $data){


        $title = '';

        if(isset($data->mark_id) && $mark = VehicleMarks::where('id', $data->mark_id)->first()){
            $title .= "{$mark->title} ";
        }

        if(isset($data->model_id) && $model = VehicleModels::where('id', $data->model_id)->first()){
            $title .= "{$model->title} ";
        }

        if(isset($data->car_year)){
            $title .= "({$data->car_year})";
        }


        if(isset($object_insurer_id) && (int)$object_insurer_id>0){
            $object = ObjectInsurer::find($object_insurer_id);
        }else{
            $object = ObjectInsurer::create([]);
        }

        $object->update([
            'type' => 1,
            'title' => $title,
        ]);

        $auto = ObjectInsurerAuto::where('object_insurer_id', $object->id)->first();

        $old_auto = $auto ? $auto : new EmptyObject();

        $update_data = [
            'ts_category' => isset($data->ts_category) ? $data->ts_category : $old_auto->ts_category,
            'mark_id' => isset($data->mark_id) ? $data->mark_id : $old_auto->mark_id,
            'model_id' => isset($data->model_id) ? $data->model_id : $old_auto->model_id,
            'purpose_id' => isset($data->purpose_id) ? $data->purpose_id : $old_auto->purpose_id,

            'vin' => isset($data->vin) ? $data->vin : $old_auto->vin,
            'body_number' => isset($data->body_number) ? $data->body_number : $old_auto->body_number,
            'reg_number' => isset($data->reg_number) ? $data->reg_number : $old_auto->reg_number,
            'car_year' => isset($data->car_year) ? (int)$data->car_year : $old_auto->car_year,

            'power' => isset($data->power) ? getFloatFormat($data->power) : $old_auto->power,
            'passengers_count' => isset($data->passengers_count) ? (int)$data->passengers_count : $old_auto->passengers_count,
            'weight' => isset($data->weight) ? getFloatFormat($data->weight) : $old_auto->weight,
            'capacity' => isset($data->capacity) ? getFloatFormat($data->capacity) : $old_auto->capacity,

            'mileage' => isset($data->mileage) ? getFloatFormat($data->mileage) : $old_auto->mileage,

            'classification_id' => isset($data->classification_id) ? (int)$data->classification_id : $old_auto->classification_id,
            'type_kpp' => isset($data->type_kpp) ? (int)$data->type_kpp : $old_auto->type_kpp,
            'foreign_reg_number' => isset($data->foreign_reg_number) ? (int)$data->foreign_reg_number : 0,

            'price' => isset($data->price) ? getFloatFormat($data->price) : $old_auto->price,


            'anti_theft_system_id' => isset($data->anti_theft_system_id) ? (int)$data->anti_theft_system_id : $old_auto->anti_theft_system_id,
            'color_id' => isset($data->color_id) ? (int)$data->color_id : $old_auto->color_id,



            'doc_type' => isset($data->doc_type) ? (int)$data->doc_type : $old_auto->doc_type,
            'doc_kind_pts' => isset($data->doc_kind_pts) ? (int)$data->doc_kind_pts : $old_auto->doc_kind_pts,
            'docserie' => isset($data->docserie) ? $data->docserie : $old_auto->docserie,
            'docnumber' => isset($data->docnumber) ? $data->docnumber : $old_auto->docnumber,
            'docdate' => isset($data->docdate) ? getDateFormatEn($data->docdate) : $old_auto->docdate,

            'modification' => isset($data->modification) ? $data->modification : $old_auto->modification,

            'body_engine' => isset($data->body_engine) ? $data->body_engine : $old_auto->body_engine,
            'body_chassis' => isset($data->body_chassis) ? $data->body_chassis : $old_auto->body_chassis,

            'type_engine' => isset($data->type_engine) ? (int)$data->type_engine : $old_auto->type_engine,
            'kv' => isset($data->kv) ?(int) $data->kv : $old_auto->kv,
            'car_year_exp' => isset($data->car_year_exp) ? (int)$data->car_year_exp : $old_auto->car_year_exp,
            'doors' => isset($data->doors) ? (int)$data->doors : $old_auto->doors,
            'keys' => isset($data->keys) ? (int)$data->keys : $old_auto->keys,

            'volume' => isset($data->volume) ? getFloatFormat($data->volume) : $old_auto->volume,

            'is_new' => isset($data->is_new) ? $data->is_new : 0,
            'sts_docserie' => isset($data->sts_docserie) ? $data->sts_docserie : $old_auto->sts_docserie,
            'sts_docnumber' => isset($data->sts_docnumber) ? $data->sts_docnumber : $old_auto->sts_docnumber,
            'sts_docdate' => isset($data->sts_docdate) ? getDateFormatEn($data->sts_docdate) : $old_auto->sts_docdate,
            'comment' => isset($data->comment) ? getDateFormatEn($data->comment) : $old_auto->comment,

            'object_insurer_id' => $object->id
        ];

        if($auto){

            $auto->update($update_data);
        }else{
            ObjectInsurerAuto::create($update_data);
        }

        return $object;

    }

    public static function update_or_create_auto_kasko_calculator($object_insurer_id, $data){

        $title = '';

        if(isset($data->mark_id) && $mark = VehicleMarks::where('id', $data->mark_id)->first()){
            $title .= "{$mark->title} ";
        }

        if(isset($data->model_id) && $model = VehicleModels::where('id', $data->model_id)->first()){
            $title .= "{$model->title} ";
        }

        if(isset($data->car_year)){
            $title .= "({$data->car_year})";
        }


        if(isset($object_insurer_id) && (int)$object_insurer_id>0){
            $object = ObjectInsurer::find($object_insurer_id);
        }else{
            $object = ObjectInsurer::create(['type' => 1, 'title' => $title]);
        }

        $object->update([
            'type' => 1,
            'title' => $title,
        ]);

        $auto = ObjectInsurerAuto::where('object_insurer_id', $object->id)->first();

        $old_auto = $auto ? $auto : new EmptyObject();

        $update_data = [
            'ts_category' => isset($data->ts_category) ? $data->ts_category : $old_auto->ts_category,
            'mark_id' => isset($data->mark_id) ? $data->mark_id : $old_auto->mark_id,
            'model_id' => isset($data->model_id) ? $data->model_id : $old_auto->model_id,
            'purpose_id' => isset($data->purpose_id) ? $data->purpose_id : $old_auto->purpose_id,

            'vin' => isset($data->vin) ? $data->vin : $old_auto->vin,
            'body_number' => isset($data->body_number) ? $data->body_number : $old_auto->body_number,
            'reg_number' => isset($data->reg_number) ? $data->reg_number : $old_auto->reg_number,
            'car_year' => isset($data->car_year) ? (int)$data->car_year : $old_auto->car_year,
            'is_new' => isset($data->is_new) ? getFloatFormat($data->is_new) : $old_auto->is_new,
            'foreign_reg_number' => isset($data->foreign_reg_number) ? (int)$data->foreign_reg_number : 0,

            'power' => isset($data->power) ? getFloatFormat($data->power) : $old_auto->power,
            'passengers_count' => isset($data->passengers_count) ? (int)$data->passengers_count : $old_auto->passengers_count,
            'weight' => isset($data->weight) ? getFloatFormat($data->weight) : $old_auto->weight,
            'capacity' => isset($data->capacity) ? getFloatFormat($data->capacity) : $old_auto->capacity,


            'price' => isset($data->price) ? getFloatFormat($data->price) : $old_auto->price,
            'price_equipment' => isset($data->price_equipment) ? getFloatFormat($data->price_equipment) : $old_auto->price_equipment,


            'anti_theft_system_id' => isset($data->anti_theft_system_id) ? (int)$data->anti_theft_system_id : $old_auto->anti_theft_system_id,
            'color_id' => isset($data->color_id) ? (int)$data->color_id : $old_auto->color_id,

            'object_insurer_id' => $object->id
        ];

        if($auto){
            $auto->update($update_data);
        }else{
            ObjectInsurerAuto::create($update_data);
        }

        return $object;

    }





    public static function update_or_create_auto_dk($object_insurer_id, $data){

        $title = '';

        if(isset($data->mark_id) && $mark = VehicleMarks::where('id', $data->mark_id)->first()){
            $title .= "{$mark->title} ";
        }

        if(isset($data->model_id) && $model = VehicleModels::where('id', $data->model_id)->first()){
            $title .= "{$model->title} ";
        }

        if(isset($data->car_year)){
            $title .= "({$data->car_year})";
        }


        if(isset($object_insurer_id) && (int)$object_insurer_id>0){
            $object = ObjectInsurer::find($object_insurer_id);
        }else{
            $object = ObjectInsurer::create([]);
        }

        $object->update([
            'type' => 1,
            'title' => $title,
        ]);

        $auto = ObjectInsurerAuto::where('object_insurer_id', $object->id)->first();

        $old_auto = $auto ? $auto : new EmptyObject();

        $update_data = [
            'ts_category' => isset($data->ts_category) ? $data->ts_category : $old_auto->ts_category,
            'ts_category_okp' => isset($data->ts_category_okp) ? $data->ts_category_okp : $old_auto->ts_category_okp,
            'mark_id' => isset($data->mark_id) ? $data->mark_id : $old_auto->mark_id,
            'model_id' => isset($data->model_id) ? $data->model_id : $old_auto->model_id,

            'vin' => isset($data->vin) ? $data->vin : $old_auto->vin,
            'body_number' => isset($data->body_number) ? $data->body_number : $old_auto->body_number,
            'body_chassis' => isset($data->body_chassis) ? $data->body_chassis : $old_auto->body_chassis,
            'reg_number' => isset($data->reg_number) ? $data->reg_number : $old_auto->reg_number,
            'car_year' => isset($data->car_year) ? (int)$data->car_year : $old_auto->car_year,
            'foreign_reg_number' => isset($data->foreign_reg_number) ? (int)$data->foreign_reg_number : 0,

            'type_engine' => isset($data->type_engine) ? (int)$data->type_engine : $old_auto->type_engine,
            'brake_system_id' => isset($data->brake_system_id) ? (int)$data->brake_system_id : $old_auto->brake_system_id,
            'brand_tire' => isset($data->brand_tire) ? $data->brand_tire : $old_auto->brand_tire,

            'mileage' => isset($data->mileage) ? getFloatFormat($data->mileage) : $old_auto->mileage,
            'weight' => isset($data->weight) ? getFloatFormat($data->weight) : $old_auto->weight,
            'max_weight' => isset($data->max_weight) ? getFloatFormat($data->max_weight) : $old_auto->max_weight,

            'doc_type' => isset($data->doc_type) ? (int)$data->doc_type : $old_auto->doc_type,
            'docserie' => isset($data->docserie) ? $data->docserie : $old_auto->docserie,
            'docnumber' => isset($data->docnumber) ? $data->docnumber : $old_auto->docnumber,
            'docdate' => isset($data->docdate) ? getDateFormatEn($data->docdate) : $old_auto->docdate,
            'docissued' => isset($data->docissued) ? $data->docissued : $old_auto->docissued,

            'dk_date_from' => isset($data->dk_date_from) ? getDateFormatEn($data->dk_date_from) : $old_auto->dk_date_from,
            'dk_date' => isset($data->dk_date) ? getDateFormatEn($data->dk_date) : $old_auto->dk_date,
            'dk_coments' => isset($data->dk_coments) ? $data->dk_coments : $old_auto->dk_coments,
            'is_new' => isset($data->is_new) ? getFloatFormat($data->is_new) : $old_auto->is_new,
            'object_insurer_id' => $object->id
        ];

        if($auto){
            $auto->update($update_data);
        }else{
            ObjectInsurerAuto::create($update_data);
        }

        return $object;

    }



    public static function update_or_create_flats($object_insurer_id, $data){


        $title = $data->address;

        if(isset($object_insurer_id) && (int)$object_insurer_id>0){
            $object = ObjectInsurer::find($object_insurer_id);
        }else{
            $object = ObjectInsurer::create([]);
        }

        $object->update([
            'type' => 2,
            'title' => $title,
        ]);

        $flats = ObjectInsurerFlats::where('object_insurer_id', $object->id)->first();

        $old_flats = $flats ? $flats : new EmptyObject();

        $update_data = [
            'address' => isset($data->address) ? $data->address : $old_flats->address,
            'address_kladr' => isset($data->address_kladr) ? $data->address_kladr : $old_flats->address_kladr,
            'address_region' => isset($data->address_region) ? $data->address_region : $old_flats->address_region,
            'address_city' => isset($data->address_city) ? $data->address_city : $old_flats->address_city,
            'address_city_kladr_id' => isset($data->address_city_kladr_id) ? $data->address_city_kladr_id : $old_flats->address_city_kladr_id,
            'address_street' => isset($data->address_street) ? $data->address_street : $old_flats->address_street,
            'address_latitude' => isset($data->address_latitude) ? $data->address_latitude : $old_flats->address_latitude,
            'address_longitude' => isset($data->address_longitude) ? $data->address_longitude : $old_flats->address_longitude,
            'address_house' => isset($data->address_house) ? $data->address_house : $old_flats->address_house,
            'address_block' => isset($data->address_block) ? $data->address_block : $old_flats->address_block,
            'address_flat' => isset($data->address_flat) ? $data->address_flat : $old_flats->address_flat,
            'house_floor' => isset($data->house_floor) ? $data->house_floor : $old_flats->house_floor,
            'flat_floor' => isset($data->flat_floor) ? $data->flat_floor : $old_flats->flat_floor,


            'object_insurer_id' => $object->id
        ];

        if($flats){

            $flats->update($update_data);
        }else{
            ObjectInsurerFlats::create($update_data);
        }

        return $object;

    }


}