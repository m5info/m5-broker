<?php

namespace App\Domain\Processes\Operations\Contracts;


use App\Domain\Entities\Contracts\EContract;
use App\Helpers\EmptyObject;
use App\Models\Contracts\ContractTerms;
use App\Models\Contracts\ObjectInsurer;
use App\Models\Contracts\ObjectInsurerAuto;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;

class ContractOnlineTerms{



    public static function update_or_create_kasko_calculator($contract, $data){

        $terms = $contract->terms;
        if(!$terms) $terms = ContractTerms::create(['contract_id'=>$contract->id]);

        $terms->prolongation_bso = $data->prolongation_bso;
        $terms->type_sum_insured_id = (int)$data->type_sum_insured_id;

        $terms->franchise_type_id = (int)$data->franchise_type_id;
        $terms->franchise = getFloatFormat($data->franchise);

        $terms_data = (object)$data->risks;

        $terms->damage_id = (int)$terms_data->damage_id;
        $terms->hijacking_id = (int)$terms_data->hijacking_id;
        $terms->is_undocumented_settlement_competent_authorities = (int)$terms_data->is_undocumented_settlement_competent_authorities;
        $terms->is_emergency_commissioner_select = (int)$terms_data->is_emergency_commissioner_select;
        $terms->is_loss_commodity_value = (int)$terms_data->is_loss_commodity_value;

        $terms->save();

        return true;
    }

    public static function update_or_create_kasko($contract, $data){


        $terms = $contract->terms;
        if(!$terms) $terms = ContractTerms::create(['contract_id'=>$contract->id]);

        $terms->prolongation_bso = isset($data->prolongation_bso) ? $data->prolongation_bso : '';
        $terms->type_sum_insured_id = isset($data->type_sum_insured_id) ? (int)$data->type_sum_insured_id : '';

        $terms->franchise_type_id = isset($data->franchise_type_id) ? (int)$data->franchise_type_id : '';
        $terms->franchise = isset($data->franchise) ? getFloatFormat($data->franchise) : '';

        if(isset($data->risks))
        {
            $terms_data = (object)$data->risks;

            $terms->damage_id = (int)$terms_data->damage_id;
            $terms->hijacking_id = (int)$terms_data->hijacking_id;
            $terms->is_undocumented_settlement_competent_authorities = (int)$terms_data->is_undocumented_settlement_competent_authorities;
            $terms->is_emergency_commissioner_select = (int)$terms_data->is_emergency_commissioner_select;
            $terms->is_loss_commodity_value = (int)$terms_data->is_loss_commodity_value;

        }


        $terms->save();

        return true;
    }


}