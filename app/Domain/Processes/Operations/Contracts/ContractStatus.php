<?php

namespace App\Domain\Processes\Operations\Contracts;

use App\Domain\Entities\Contracts\EContract;
use App\Models\Contracts\Contracts;

class ContractStatus{

    public static function set($contract, $status){

        $contract = EContract::get_first($contract);

        if($contract && isset(Contracts::STATYS[(int)$status])){

            $contract->update(['statys_id' => (int)$status]);

        }

        return $contract;
    }







}