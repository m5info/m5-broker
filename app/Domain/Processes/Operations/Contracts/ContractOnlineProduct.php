<?php

namespace App\Domain\Processes\Operations\Contracts;


use App\Domain\Entities\Contracts\EContract;
use Illuminate\Support\Str;

class ContractOnlineProduct{


    public static function get_online_product_class($contract){

        if($contract = EContract::get_first($contract)){

            $class_name = self::ucfirst($contract->getProductOrProgram()->slug);
            $online_product_class = "App\\Domain\\Processes\\Scenaries\\Contracts\\Online\\Products\\{$class_name}";
            if(class_exists($online_product_class)){

                return $online_product_class;
            }

        }

        return false;

    }


    public static function ucfirst($string)
    {
        $name = '';
        $strArr = explode('_', $string);
        foreach ($strArr as $str){
            $name .=Str::upper(Str::substr($str, 0, 1)).Str::substr($str, 1);
        }

        return $name;
    }

}