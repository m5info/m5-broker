<?php

namespace App\Domain\Processes\Operations\Contracts;

use App\Domain\Entities\Contracts\EContract;
use App\Helpers\EmptyObject;
use App\Models\Contracts\Subjects;

class ContractSubject{


    public static function update_or_create($contract, $data){

        $contract = EContract::get_first($contract);

        $subject = $contract->insurer;
        $old_subject = $subject ? $subject : new EmptyObject();

        $updated_data = [
            'type' => isset($data->type) ? $data->type : $old_subject->type,
            'phone' => isset($data->phone) ? $data->phone : $old_subject->phone,
            'add_phone' => isset($data->add_phone) ? $data->add_phone : $old_subject->add_phone,
            'email' => isset($data->email) ? $data->email : $old_subject->email,
            'delivery_address' => isset($data->delivery_address) ? $data->delivery_address : $old_subject->delivery_address,
            'underground' => isset($data->underground) ? $data->underground : $old_subject->underground,
            'doc_serie' => isset($data->doc_serie) ? $data->doc_serie : $old_subject->doc_serie,
            'doc_number' => isset($data->doc_number) ? $data->doc_number : $old_subject->doc_number,
            'inn' => isset($data->inn) ? $data->inn : $old_subject->inn,
            'kpp' =>  isset($data->kpp) ? $data->kpp : $old_subject->kpp,
        ];

        $updated_data_info = $updated_data;
        unset($updated_data_info['type']);
        unset($updated_data_info['phone']);
        unset($updated_data_info['email']);

        if(isset($data->type) && (int)$data->type == 0){
            $updated_data['title'] = isset($data->fio) ? $data->fio : $old_subject->title;

            $updated_data_info['fio'] = $updated_data['title'];
            unset($updated_data_info['title']);
            unset($updated_data_info['inn']);
            unset($updated_data_info['kpp']);


        }else{
            $updated_data['title'] = isset($data->title) ? $data->title : $old_subject->title;

            $updated_data_info['title'] = $updated_data['title'];
            unset($updated_data_info['doc_serie']);
            unset($updated_data_info['doc_number']);

        }

        if($subject){
            $subject->update($updated_data);
        }else{
            $subject = Subjects::create($updated_data);

        }

        $subject_info = $subject->get_info();
        $subject_info->save();
        $subject_info->update($updated_data_info);


        $contract->update([
            'insurer_id' => $subject->id
        ]);

        return $contract;
    }


}