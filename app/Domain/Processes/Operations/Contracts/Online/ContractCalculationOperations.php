<?php

namespace App\Domain\Processes\Operations\Contracts\Online;

use App\Domain\Entities\Contracts\EContract;
use App\Domain\Entities\Contracts\EContractCalculation;
use App\Domain\Processes\Operations\Contracts\ContractsFinancialPolicy;

class ContractCalculationOperations{

    public static function select_calc($contract, $calculation){

        if($contract = EContract::get_first($contract)){

            if($calculation = EContractCalculation::get_first($calculation)){

                $contract->update([
                    'selected_calculation_id' => $calculation->id,
                    'program_id' => $calculation->program_id,
                    'insurance_companies_id' => $calculation->insurance_companies_id,
                    'bso_supplier_id' => $calculation->bso_supplier_id,
                    'financial_policy_id' => $calculation->financial_policy_id,
                    'payment_total' => getFloatFormat($calculation->sum),
                ]);

                $calculation->statys_id = 2;
                $calculation->update();

                $contract = ContractsFinancialPolicy::update($contract, $calculation);

                return true;

            }


        }

        return false;

    }



}