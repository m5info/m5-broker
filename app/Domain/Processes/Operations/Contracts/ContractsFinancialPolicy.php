<?php

namespace App\Domain\Processes\Operations\Contracts;

use App\Domain\Entities\Contracts\EContract;
use App\Domain\Entities\Settings\EFinancialPolicy;
use App\Models\User;
use DB;
use stdClass;

class ContractsFinancialPolicy{


    public static function update($contract, $data){

        $contract = EContract::get_first($contract);

        if((int)$contract->statys_id == 4){
            return $contract;
        }

        //если передана существующая фп и не указано что фп вручную
        if(isset($data->financial_policy_id) && !isset($data->financial_policy_manually_set)){

            if($fp = EFinancialPolicy::get_first((int)$data->financial_policy_id)){

                $res = new stdClass();
                $res->kv_agent = $fp->kv_agent;
                $res->kv_parent = $fp->kv_parent;

                //если прямая продажа то группу берём по манагеру иначе по агенту
                $user_id = $contract->sales_condition == 1 ? $contract->manager_id : $contract->agent_id;

                if($user = User::find($user_id)){

                    $sql = "select * from financial_group 
                            inner join financial_policies_groups_kv on 
                            financial_group.id = financial_policies_groups_kv.financial_policies_group_id 
                            where financial_policies_groups_kv.financial_policy_id = {$fp->id} 
                            and financial_group.id = {$user->financial_group_id} 
                            and financial_policies_groups_kv.is_actual = 1
                            and financial_group.is_actual = 1";

                    $group = DB::selectOne($sql);

                    if($group){
                        $res->kv_agent = $group->kv_agent;
                        $res->kv_parent = $group->kv_parent;
                    }
                }

                if($fp->fix_price == 1)
                {
                    $payment_total = $contract->payment_total;
                    $fix_price_sum = $fp->fix_price_sum;

                    //СДЕЛАТЬ ПРАВИЛЬНЫЙ РАСЧЕТ
                    $data->financial_policy_kv_bordereau = getProcentToSum($payment_total, $fix_price_sum);
                    $data->financial_policy_kv_dvoy = 0;
                    $data->financial_policy_kv_parent = getProcentToSum($payment_total, $res->kv_parent);
                    $data->financial_policy_kv_agent = getProcentToSum($payment_total, $res->kv_agent);

                }else{
                    $data->financial_policy_kv_bordereau = $fp->kv_bordereau;
                    $data->financial_policy_kv_dvoy = $fp->kv_dvou;
                    $data->financial_policy_kv_parent = $res->kv_parent;
                    $data->financial_policy_kv_agent = $res->kv_agent;
                }

            }

        }

        if(auth()->user()->hasPermission('contracts', 'select_financial_policy')){
            $contract->update([
                'financial_policy_kv_bordereau' =>
                    isset($data->financial_policy_kv_bordereau) ? $data->financial_policy_kv_bordereau : $contract->financial_policy_kv_bordereau,

                'financial_policy_kv_parent' =>
                    isset($data->financial_policy_kv_parent) ? $data->financial_policy_kv_parent : $contract->financial_policy_kv_parent,

                'financial_policy_kv_agent' =>
                    isset($data->financial_policy_kv_agent) ? $data->financial_policy_kv_agent : $contract->financial_policy_kv_agent,

                'financial_policy_kv_dvoy' =>
                    isset($data->financial_policy_kv_dvoy) ? $data->financial_policy_kv_dvoy : $contract->financial_policy_kv_dvoy,

                'financial_policy_manually_set' =>
                    isset($data->financial_policy_manually_set) ? $data->financial_policy_manually_set : $contract->financial_policy_manually_set,

                'financial_policy_id' =>
                    isset($data->financial_policy_id) ? $data->financial_policy_id : $contract->financial_policy_id,
            ]);
        }

        return $contract;
    }


}