<?php

namespace App\Domain\Processes\Operations\Contracts;

use App\Domain\Entities\Contracts\EContract;
use App\Domain\Entities\Payments\EPayment;
use App\Domain\Processes\Operations\Bso\BsoStateOperations;
use App\Domain\Processes\Operations\Payments\PaymentAcceptOperations;
use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentExpectedPayments;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Domain\Processes\Operations\Payments\PaymentInvoice;
use App\Domain\Processes\Operations\Payments\PaymentReceipt;
use App\Domain\Processes\Operations\Payments\PaymentStatus;
use App\Models\Contracts\ContractMessage;
use App\Models\Contracts\Contracts;

class ContractAcceptDownloading{

    public static function accept(Contracts $contract, $payment_date, $payment_total){

        //акцептуем
        $contract = ContractAccept::set($contract, 1);

        //выпускаем договор
        $contract = ContractStatus::set($contract, 4);

        //продаём бсо
        $bso = BsoStateOperations::set($contract->bso, 2);


        //если есть платежи
        if(sizeof($contract->all_payments)){
            foreach ($contract->all_payments as $payment){

                //обновляем
                $payment = PaymentAcceptOperations::update_or_create($payment);

                //если платёж временный, делаем его нормальным, неоплаченым
                if($payment->statys_id == -1){

                    $payment->payment_data = $payment_date;
                    $payment->payment_total = $payment_total;


                    $parent_agent_id = 0;

                    if($contract->sales_condition != 0){
                        if($contract->manager){
                            $parent_agent_id = $contract->manager->parent_id;
                            $payment->manager_id = $contract->manager_id;
                        }
                        $payment->agent_id = $contract->agent_id;

                    }else{
                        if($contract->agent){
                            $payment->agent_id = $contract->agent_id;
                            $parent_agent_id = $contract->agent->parent_id;
                        }
                    }



                    $payment->parent_agent_id = $parent_agent_id;
                    $payment->point_sale_id = $contract->bso->point_sale_id;
                    $payment->save();


                    //Квитанция
                    if($payment->bso_receipt_id > 0){

                        PaymentReceipt::attach($payment, $payment);

                    }

                    $payment = PaymentStatus::set($payment, 0);

                }

                if($payment = PaymentFinancialPolicy::actualize($payment)){
                    $payment = PaymentDiscounts::recount($payment);
                }


                $payment = PaymentExpectedPayments::expected($payment);

                $hold_kv_product = $contract->bso->supplier->hold_kv_product($contract->product_id);
                if($hold_kv_product->is_auto_bso == 1) {
                    PaymentInvoice::create_to_payment($payment);
                }

            }




        }

        return true;
    }



}