<?php

namespace App\Domain\Processes\Operations\Contracts;

use App\Domain\Entities\Contracts\EContract;
use App\Domain\Entities\Payments\EPayment;
use App\Domain\Processes\Operations\Payments\TransactionReferencerOnAccept;
use App\Models\Contracts\ContractMessage;
use App\Models\Contracts\Contracts;
use App\Models\Users\Role;
use http\Client\Curl\User;
use Illuminate\Support\Carbon;

class ContractAccept{

    public static function set($contract, $accept_status){

        $contract = EContract::get_first($contract);

        if(isset(Contracts::KIND_ACCEPTANCE[(int)$accept_status])){

            $auth_id = auth()->id();

            if(auth()->user()->is('under')){
                if(!$contract->check_user_id){
                    $contract->update([
                        'check_user_id' => $auth_id,
                        'check_date' => date('Y-m-d')
                    ]);
                }

                $payments = $contract->payments()->where('type_id', '=', 0)->get();

                foreach($payments as $payment){
                    if(!(int)$payment->check_user_id){
                        $payment->update([
                            'check_user_id' => $auth_id,
                            'check_date' => date('Y-m-d')
                        ]);
                    }
                }
            }

            $contract->update([
                "kind_acceptance" => (int)$accept_status,
            ]);

            if($accept_status == 0) {
                $contract->update([
                    "delay_date" => date('Y-m-d H:i:s'),
                ]);
            }

            if($accept_status == 1) {

                $contract->sendDataToFront();

                TransactionReferencerOnAccept::set_transaction($contract);

                $contract->bso->setBsoLog(13, 0 , 2);

                if($hold_kv_product = $contract->bso->supplier->hold_kv_product($contract->product_id)){
                    if($hold_kv_product->is_auto_bso == 1){
                        $bso = $contract->bso;
                        $bso->location_id = 2;
                        $bso->save();
                    }
                }
            }
        }


        return $contract;
    }

    public static function set_error($contract, $message){

        $contract = EContract::get_first($contract);

        if ($contract->statys_id == 1 || $contract->statys_id == 7) {

            if(isset($contract->payment_accepts)){

                foreach($contract->payment_accepts as $payment_data){ // id андера ставится если только еще не проставлен

                    $auth_id = 0;

                    if($payment_data->accept_user_id > 0){
                        $auth_id = $payment_data->accept_user_id;
                    }else{
                        $auth_id = auth()->id();
                    }

                    $payment_data->update([
                        'accept_user_id' => $auth_id,
                        'accept_date' => date('Y-m-d H:i:s'),
                    ]);

                }
            }

            if(auth()->user()->is('under')) {
                if (!$contract->check_user_id) {
                    $contract->update([
                        'check_user_id' => auth()->id(),
                        'check_date' => date('Y-m-d')
                    ]);
                }

                $payments = $contract->payments()->where('type_id', '=', 0)->get();

                foreach($payments as $payment){
                    if(!(int)$payment->check_user_id){
                        $payment->update([
                            'check_user_id' => isset($auth_id) ? $auth_id : auth()->id(),
                            'check_date' => date('Y-m-d')
                        ]);
                    }
                }
            }

            $contract->update([
                'statys_id' => 2,
            ]);

            ContractMessage::create([
                'message' => $message,
                'type_id' => 1,
                'user_id' => auth()->id(),
                'contract_id' => $contract->id,
            ]);

            ContractAccept::set($contract, 0);

        }


        return $contract;

    }

}