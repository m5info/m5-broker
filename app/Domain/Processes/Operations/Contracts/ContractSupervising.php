<?php

namespace App\Domain\Processes\Operations\Contracts;


use App\Domain\Entities\Contracts\EContract;

class ContractSupervising{

    public static function update($contract, $data){

        $contract = EContract::get_first($contract);

        $parent_agent_id = $contract->agent ? $contract->agent->parent_id : 0;

        if (isset($data->sales_condition) && $data->sales_condition != 0) {

            $contract->update([
                'manager_id' => isset($data->manager_id) ? $data->manager_id : $contract->manager_id,
                'referencer_id' => isset($data->referencer_id) ? $data->referencer_id : $contract->referencer_id,
            ]);
            $parent_agent_id = ($contract->manager && $contract->manager->parent_id)  ? $contract->manager->parent_id : 0;

        }

        $contract->update([
            'parent_agent_id' => $parent_agent_id,
        ]);

        return $contract;
    }


}