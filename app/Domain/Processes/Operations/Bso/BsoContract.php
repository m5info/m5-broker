<?php

namespace App\Domain\Processes\Operations\Bso;

use App\Domain\Entities\Bso\EBsoItem;

use App\Domain\Entities\Contracts\EContract;


class BsoContract{

    public static function attach($bso_item, $contract){

        $bso_item = EBsoItem::get_first($bso_item);
        $contract = EContract::get_first($contract);

        if($bso_item && $contract){

            $bso_item->update(['contract_id' => $contract->id]);
            $contract->update(['bso_id' => $bso_item->id]);

        }

        return $bso_item;

    }







}