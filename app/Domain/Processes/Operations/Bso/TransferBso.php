<?php


namespace App\Domain\Processes\Operations\Bso;


use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoCarts;
use App\Models\BSO\BsoItem;
use App\Models\User;

class TransferBso{


    public static function handle($bso_cart_id)
    {
        if ( $bso_cart_id == 0 ) {
            return response();
        }

        $bso_items = BsoItem::where('bso_cart_id', $bso_cart_id)->get();

        if ( (int) $bso_items->count() == 0 ) {
            return response('empty cart');
        }

        $object = BsoCarts::find($bso_cart_id);

        $user_id_from      = $object->user_id_from;
        $user_id_to        = $object->user_id_to;
        $sk_id_to          = $object->sk_id_to;
        $bso_cart_type     = $object->bso_cart_type;
        $bso_state_id      = $object->bso_state_id;
        $curr_tp_id        = $object->curr_tp_id;
        $tp_id             = $object->tp_id;
        $tp_new_id         = $object->tp_new_id;
        $tp_bso_manager_id = $object->tp_bso_manager_id;
        $courier_id        = $object->courier_id;
        $tp_change_selected = $object->tp_change_selected;

        $bso_manager_id = (int) auth()->id();
        $log_time       = $time_create = date( 'Y-m-d H:i:s' );
        $ip_address     = $_SERVER['REMOTE_ADDR'];

        /*
        1	Передача со склада агенту
        2	Передача от агента-агенту
        3	Прием БСО от агента
        4	Передача БСО в СК
        5	Передача БСО на точку продаж
        6	Передача БСО курьеру
        7	Прием испорченного БСО от агента
        8	Передача со склада агенту/Точка продаж
        9	Передача БСО на точку продаж НОВАЯ

        */

        $bso_acts = BsoActs::select((\DB::raw('max(act_number_int) as max_act_number')))->get()->first();
        if($bso_acts->max_act_number){
            $act_number_int = (int)$bso_acts->max_act_number + 1;
        }else{
            $act_number_int = 1;
        }

        $bso_items = BsoItem::where('bso_cart_id', $bso_cart_id);

        $act_number = str_pad( $act_number_int, 6, '0', STR_PAD_LEFT );

        // Передача со склада агенту
        if ( $bso_cart_type == 1 || $bso_cart_type == 8) {
            $location_from = 0; // Склад БСО
            $location_to   = 1; // Агент
            $bso_state_id  = 0;
            $user_org_id = User::find($user_id_to)->organization_id;

            $curr_tp_id = $object->curr_tp_id;

            if($bso_cart_type == 8){
                // обновляем статусы БСО -> ТП
                $curr_tp_id = $tp_new_id;
            }

            $acts = BsoActs::create([
                'time_create' => $time_create,
                'type_id' => '1',
                'user_id_from' => $bso_manager_id,
                'user_id_to' => $user_id_to,
                'bso_manager_id' => $bso_manager_id,
                'location_from' => $location_from,
                'location_to' => $location_to,
                'bso_state_id' => $bso_state_id,
                'act_number' => $act_number,
                'act_number_int' => $act_number_int,
                'bso_cart_id' => $bso_cart_id,
                'curr_tp_id' => $curr_tp_id,
                'act_name' => 'Акт передачи БСО агенту'
            ]);

            $acts->setItemsCarts($bso_cart_id);

            $bso_act_id = $acts->id;

            // добавляем в лог передачу от сотрудника БСО -> агенту

            $sql = "insert into bso_logs (log_time, bso_id, bso_act_id, bso_state_id, bso_location_id, bso_user_from, bso_user_to, user_id, ip_address)
	SELECT '$log_time', id, '$bso_act_id', '$bso_state_id', '1', '$bso_manager_id', '$user_id_to', '$bso_manager_id', '$ip_address' from bso_items where bso_cart_id=$bso_cart_id ";
            \DB::insert($sql);

            // обновляем статусы БСО -> агенту
            $bso_items->update([
                'point_sale_id' => $curr_tp_id,
                'location_id' => $location_to,
                'state_id' => $bso_state_id,
                'user_id' => $user_id_to,
                'agent_id' => $user_id_to,
                'user_org_id' => $user_org_id,
                'bso_cart_id' => 0,
                'is_reserved' => 0,
                'last_operation_time' => $log_time,
                'transfer_to_agent_time' => $log_time
            ]);



        }

        // Прием БСО от агента и передача другому агенту
        if ( $bso_cart_type == 2 ) {
//            $location_from = 1; // Агент
//            $location_to   = 0; // Склад БСО
//            $bso_state_id  = 0;
//
//            $acts_from = BsoActs::create([
//                'time_create' => $time_create,
//                'type_id' => '3',
//                'user_id_from' => $user_id_from,
//                'user_id_to' => $bso_manager_id,
//                'bso_manager_id' => $bso_manager_id,
//                'location_from' => $location_from,
//                'location_to' => $location_to,
//                'bso_state_id' => $bso_state_id,
//                'act_number' => $act_number,
//                'act_number_int' => $act_number_int,
//                'bso_cart_id' => $bso_cart_id,
//                'curr_tp_id' => $curr_tp_id,
//                'act_name' => 'Акт приема БСО от агента'
//            ]);

//            $acts_from->setItemsCarts($bso_cart_id);

//            $bso_act_id_from = $acts_from->id;


            $act_number_int ++;
            $act_number = str_pad( $act_number_int, 6, '0', STR_PAD_LEFT );

            $location_from = 1; // Склад БСО
            $location_to   = 1; // Агент
            $bso_state_id  = 0;

            $acts_to = BsoActs::create([
                'time_create' => $time_create,
                'type_id' => '2',
                'user_id_from' => $user_id_from,
                'user_id_to' => $user_id_to,
                'bso_manager_id' => $bso_manager_id,
                'location_from' => $location_from,
                'location_to' => $location_to,
                'bso_state_id' => $bso_state_id,
                'act_number' => $act_number,
                'act_number_int' => $act_number_int,
                'bso_cart_id' => $bso_cart_id,
                'curr_tp_id' => $curr_tp_id,
                'act_name' => 'Акт передачи БСО от агента агенту'
            ]);

            $acts_to->setItemsCarts($bso_cart_id);

            $bso_act_id_to = $acts_to->id;


//            // добавляем в лог передачу от агента -> сотруднику БСО
//            $sql = "insert into bso_logs (log_time, bso_id, bso_act_id, bso_state_id, bso_location_id, bso_user_from, bso_user_to, user_id, ip_address)
//	SELECT '$log_time', id, '$bso_act_id_from', '$bso_state_id', '0', '$user_id_from', '$bso_manager_id', '$bso_manager_id', '$ip_address' from bso_items where bso_cart_id=$bso_cart_id ";
//            \DB::insert($sql);

            // добавляем в лог передачу от сотрудника БСО -> агенту
            $sql = "insert into bso_logs (log_time, bso_id, bso_act_id, bso_state_id, bso_location_id, bso_user_from, bso_user_to, user_id, ip_address)
	SELECT '$log_time', id, '$bso_act_id_to', '$bso_state_id', '1', '$bso_manager_id', '$user_id_to', '$bso_manager_id', '$ip_address' from bso_items where bso_cart_id=$bso_cart_id ";
            \DB::insert($sql);


            //менять точку продаж бсо по агенту
            if($tp_change_selected){
                $tp_id = User::find($user_id_to)->point_sale_id;
                $bso_items->update([
                    'point_sale_id' => $tp_id
                ]);
            }

            $user_org_id = User::find($user_id_to)->organization_id;
            // обновляем статусы БСО -> агенту
            $bso_items->update([
                'location_id' => $location_to,
                'state_id' => $bso_state_id,
                'user_id' => $user_id_to,
                'agent_id' => $user_id_to,
                'user_org_id' => $user_org_id,
                'bso_cart_id' => 0,
                'is_reserved' => 0,
                'last_operation_time' => $log_time,
                'transfer_to_agent_time' => $log_time
            ]);

        }


        // Прием БСО от агента
        if ( $bso_cart_type == 3 ) {
            $location_from = 1; // Агент
            $location_to   = 0; // Склад БСО


            $acts = BsoActs::create([
                'time_create' => $time_create,
                'type_id' => '3',
                'user_id_from' => $user_id_from,
                'user_id_to' => $bso_manager_id,
                'bso_manager_id' => $bso_manager_id,
                'location_from' => $location_from,
                'location_to' => $location_to,
                'bso_state_id' => $bso_state_id,
                'act_number' => $act_number,
                'act_number_int' => $act_number_int,
                'bso_cart_id' => $bso_cart_id,
                'curr_tp_id' => $curr_tp_id,
                'act_name' => 'Акт приема БСО от агента'
            ]);

            $acts->setItemsCarts($bso_cart_id);

            $bso_act_id = $acts->id;


            // добавляем в лог передачу от агента -> сотруднику БСО
            $sql = "insert into bso_logs (log_time, bso_id, bso_act_id, bso_state_id, bso_location_id, bso_user_from, bso_user_to, user_id, ip_address)
	SELECT '$log_time', id, '$bso_act_id', '$bso_state_id', '0', '$user_id_from', '$bso_manager_id', '$bso_manager_id', '$ip_address' from bso_items where bso_cart_id=$bso_cart_id ";
            \DB::insert($sql);

            // обновляем статусы агент -> склад
            $bso_items->update([
                'point_sale_id' => $curr_tp_id,
                'state_id' => $bso_state_id,
                'location_id' => 0,
                'user_id' => 0,
                'user_org_id' => 0,
                'agent_id' => 0,
                'bso_cart_id' => 0,
                'is_reserved' => 0,
                'last_operation_time' => $log_time,
                'transfer_to_agent_time' => $log_time
            ]);


        }


        // Передача БСО на точку продаж
        if ( $bso_cart_type == 5 ) {
            $location_from = 0; // Склад БСО
            $location_to   = 0; // ТП
            $bso_state_id  = 0;

            $tp_id = User::find($tp_bso_manager_id)->point_sale_id;

            $acts = BsoActs::create([
                'time_create' => $time_create,
                'type_id' => '5',
                'user_id_from' => $bso_manager_id,
                'user_id_to' => $tp_bso_manager_id,
                'bso_manager_id' => $bso_manager_id,
                'location_from' => $location_from,
                'location_to' => $location_to,
                'bso_state_id' => $bso_state_id,
                'act_number' => $act_number,
                'act_number_int' => $act_number_int,
                'bso_cart_id' => $bso_cart_id,
                'curr_tp_id' => $curr_tp_id,
                'tp_id' => $tp_id,
                'act_name' => 'Акт передачи БСО на точку продаж'
            ]);

            $acts->setItemsCarts($bso_cart_id);

            $bso_act_id = $acts->id;



            // добавляем в лог передачу от сотрудника БСО -> агенту
            $sql = "insert into bso_logs (log_time, bso_id, bso_act_id, bso_state_id, bso_location_id, bso_user_from, bso_user_to, user_id, ip_address)
	SELECT '$log_time', id, '$bso_act_id', '$bso_state_id', '0', '$bso_manager_id', '$tp_bso_manager_id', '$bso_manager_id', '$ip_address' from bso_items where bso_cart_id=$bso_cart_id ";
            \DB::insert($sql);

            // обновляем статусы БСО -> ТП
            $bso_items->update([
                'point_sale_id' => $tp_id,
                'location_id' => 0,
                'state_id' => $bso_state_id,
                'user_id' => 0,
                'user_org_id' => 0,
                'agent_id' => 0,
                'bso_cart_id' => 0,
                'is_reserved' => 0,
                'last_operation_time' => $log_time,
                'transfer_to_agent_time' => $log_time
            ]);

        }

        // Передача БСО на точку продаж НОВАЯ
        if ( $bso_cart_type == 9 ) {
            $location_from = 0; // Склад БСО
            $location_to   = 0; // ТП
            $bso_state_id  = 0;


            $acts = BsoActs::create([
                'time_create' => $time_create,
                'type_id' => '9',
                'user_id_from' => $bso_manager_id,
                'user_id_to' => $tp_bso_manager_id,
                'bso_manager_id' => $bso_manager_id,
                'location_from' => $location_from,
                'location_to' => $location_to,
                'bso_state_id' => $bso_state_id,
                'act_number' => $act_number,
                'act_number_int' => $act_number_int,
                'bso_cart_id' => $bso_cart_id,
                'curr_tp_id' => $curr_tp_id,
                'tp_id' => $tp_new_id,
                'act_name' => 'Акт передачи БСО на точку продаж'
            ]);

            $acts->setItemsCarts($bso_cart_id);

            $bso_act_id = $acts->id;


            // добавляем в лог передачу от сотрудника БСО -> агенту
            $sql = "insert into bso_logs (log_time, bso_id, bso_act_id, bso_state_id, bso_location_id, bso_user_from, bso_user_to, user_id, ip_address)
	SELECT '$log_time', id, '$bso_act_id', '$bso_state_id', '0', '$bso_manager_id', '$tp_bso_manager_id', '$bso_manager_id', '$ip_address' from bso_items where bso_cart_id=$bso_cart_id ";
            \DB::insert($sql);

            // обновляем статусы БСО -> ТП
            $bso_items->update([
                'point_sale_id' => $tp_new_id,
                'location_id' => 0,
                'user_id' => 0,
                'user_org_id' => 0,
                'agent_id' => 0,
                'bso_cart_id' => 0,
                'is_reserved' => 0,
                'last_operation_time' => $log_time,
                'transfer_to_agent_time' => $log_time
            ]);

        }


        // обновляем статус корзины
        $object->cart_state_id = 1;
        $object->save();

        return response($acts->id);


    }
}