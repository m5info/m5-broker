<?php

namespace App\Domain\Processes\Operations\Bso;

use App\Domain\Entities\Bso\EBsoItem;
use App\Domain\Entities\Bso\EBsoState;
use App\Models\BSO\BsoLogs;


class BsoLocationOperations{

    public static function set($bso_item, $location){

        $bso_item = EBsoItem::get_first($bso_item);
        $location = EBsoState::get_first((int)$location);

        if($bso_item && $location){
            $bso_item->update([
                'location_id' => $location->id
            ]);

            BsoLogs::setLogs($bso_item->id, $bso_item->state_id, $bso_item->location_id);
        }


        return $bso_item;
    }







}