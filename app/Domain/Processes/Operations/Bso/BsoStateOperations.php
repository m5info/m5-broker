<?php

namespace App\Domain\Processes\Operations\Bso;

use App\Domain\Entities\Bso\EBsoItem;
use App\Domain\Entities\Bso\EBsoState;
use App\Domain\Processes\Operations\Payments\PaymentReceiptStatus;
use App\Models\BSO\BsoLogs;


class BsoStateOperations{

    public static function set($bso_item, $status){

        $bso_item = EBsoItem::get_first($bso_item);
        $state = EBsoState::get_first((int)$status);


        if($bso_item && $state){
            $bso_item->update([
                'state_id' => $state->id
            ]);

            BsoLogs::setLogs($bso_item->id, $bso_item->state_id, $bso_item->location_id);
        }

        if ($contract = $bso_item->contract){
            PaymentReceiptStatus::refresh($contract);
        }

        return $bso_item;
    }







}