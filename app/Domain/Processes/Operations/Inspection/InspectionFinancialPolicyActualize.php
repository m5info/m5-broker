<?php


namespace App\Domain\Processes\Operations\Inspection;


use App\Domain\Entities\Inspection\Inspection;
use App\Models\Directories\FinancialPolicy;

class InspectionFinancialPolicyActualize{

    // устанавливаю фп взависимости от поставщика и продукта
    public static function set_default($order_id){

        $order = Inspection::get_first($order_id);
        $fp = FinancialPolicy::where('is_actual', 1)->where('bso_supplier_id', $order->bso_supplier_id)->where('product_id', $order->product_id)->get()->last();
        $order->financial_policy_id = $fp->id;
        $order->save();

        return $order;
    }

    public static function actualize($order_id){
        // выбираю ФП
        $order = Inspection::get_first($order_id);
        $fp = $order->financial_policies;

        // в payment_total себестоимость
        // financial_policy_kv_bordereau = 100
        // financial_policy_kv_bordereau_total = себестоимость (2000)
        // financial_policy_kv_dvoy = 0
        // financial_policy_kv_dvoy_total = 0
        // financial_policy_kv_agent = 100 / (financial_policy_kv_bordereau_total/financial_policy_kv_agent_total) getFloatFormat()
        // financial_policy_kv_agent_total = из фп беру (1000)

        $order->payment_total = $fp->fix_price_sum;
        $order->financial_policy_kv_bordereau = $fp->kv_bordereau;
        $order->financial_policy_kv_bordereau_total = $fp->fix_price_sum;
        $order->financial_policy_kv_dvoy = 0;
        $order->financial_policy_kv_dvoy_total = 0;
        $order->financial_policy_kv_agent = 100 / ($fp->fix_price_sum/$fp->kv_agent);
        $order->financial_policy_kv_agent_total = $fp->kv_agent;
        $order->inspection->financial_policy_kv_agent_total = $fp->kv_agent;

    }
}