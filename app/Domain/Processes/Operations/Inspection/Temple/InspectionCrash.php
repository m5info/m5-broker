<?php


namespace App\Domain\Processes\Operations\Inspection\Temple;


use App\Models\Contracts\Contracts;

class InspectionCrash
{

    /*
     'info' => [ //Общая информация
            'address' => 'Место ДТП',
            'dates' => 'Дата ДТП 10.01.2020 14:40',
            'numberDamaged' => 'Количество поврежденных ТС',
            'numberInjured' => 'Количество раненых',
            'numberDead' => 'Количество погибших',
            'examinationIntoxication' => 'Проводилось ли освидетельствование участников ДТП на состояние опьянения',
            'materialDamageVehicle' => 'Материальный ущерб, другим транспортным средствам',
            'materialDamageProperty' => 'Материальный ущерб, другому имуществу',
            'clearanceEmployee' => 'Проводилось ли оформление сотрудником ГИБДД',
            'numberEmployee' => 'номер нагрудного знака',
            'witnesses' => [
                [
                    'fullName' => 'ФИО',
                    'address' => 'адрес',
                    'phone' => 'телефон',
                ]
            ],
        ],


    'vehicle' => [ //Транспортное средство
            'markId' => 'Марка',
            'modelId' => 'Модель',
            'vin' => '(VIN) ТС',
            'regNumber' => 'Государственный регистрационный знак ТС',
            'stsSeries' => 'Свидетельство о регистрации ТС серия',
            'stsNumber' => 'Свидетельство о регистрации ТС номер',
            'owner' => 'Собственник ТС',
            'ownerAddress' => 'Собственник Адрес',
            'driver' => 'Водитель ТС',
            'driverDateBirth' => 'Водитель Дата рождения',
            'driverAddress' => 'Водитель Адрес',
            'driverPhone' => 'Водитель Телефон',
            'driverlicenseSeries' => 'Водительское удостоверение серия',
            'driverlicenseNumber' => 'Водительское удостоверение номер',
            'driverlicenseDate' => 'Водительское удостоверение дата выдачи',
            'category' => 'Категория ТС',
            'titleDeed' => 'Документ на право владения, пользования, распоряжения ТС',
            'sk' => 'Страховщик',
            'bsoSeries' => 'Страховой полис серия',
            'bsoNumber' => 'Страховой полис номер',
            'contractEndData' => 'Действителен до',
            'contractInsuredDamage' => 'ТС застраховано от ущерба',
            'infoDamage' => 'Характер и перечень видимых поврежденных деталей и элементов',
            'comment' => 'Замечания',
        ],

        'full_info' => [ //Детальная иинформайия
            'vehicle' => ['is_a' => false, 'is_b' => false],//Транспортное средство
            'detalization' => 'Обстоятельства ДТП',
            'driver' => ['owner' => false, 'other' => false],//ТС находилось под управлением
            'moreVehicle' => 'В случае, если в ДТП участвовало более',
            'damageOtherName' => 'Повреждения иного имущества, Наименование',
            'damageOtherOwner' => 'Повреждения иного имущества, Кому принадлежит',
            'canVehicleMove' => 'Может ли ТС передвигаться своим ходом? true',
            'whereVehicle' => 'где сейчас находится ТС',
            'comment' => 'Примечание, в том числе разногласия',
            'withAppAct' => 'С приложением false',
        ]


     */


    const DEFAULT_TEMPLE = [
        'info' => [ //Общая информация
            'address' => '',
            'dates' => '',
            'numberDamaged' => '',
            'numberInjured' => '0',
            'numberDead' => '0',
            'examinationIntoxication' => 'false',
            'materialDamageVehicle' => 'false',
            'materialDamageProperty' => 'false',
            'clearanceEmployee' => 'false',
            'numberEmployee' => '',
            'witnesses' => null,
        ],
        'vehicle_a' => [ //Транспортное средство "А"
            'markId' => '',
            'modelId' => '',
            'vin' => '',
            'regNumber' => '',
            'stsSeries' => '',
            'stsNumber' => '',
            'owner' => '',
            'ownerAddress' => '',
            'driver' => '',
            'driverDateBirth' => '',
            'driverAddress' => '',
            'driverPhone' => '',
            'driverlicenseSeries' => '',
            'driverlicenseNumber' => '',
            'driverlicenseDate' => '',
            'category' => '',
            'titleDeed' => '',
            'sk' => '',
            'bsoSeries' => '',
            'bsoNumber' => '',
            'contractEndData' => '',
            'contractInsuredDamage' => '',
            'infoDamage' => '',
            'comment' => '',
        ],
        'vehicle_b' => [ //Транспортное средство "В"
            'markId' => '',
            'modelId' => '',
            'vin' => '',
            'regNumber' => '',
            'stsSeries' => '',
            'stsNumber' => '',
            'owner' => '',
            'ownerAddress' => '',
            'driver' => '',
            'driverDateBirth' => '',
            'driverAddress' => '',
            'driverPhone' => '',
            'driverlicenseSeries' => '',
            'driverlicenseNumber' => '',
            'driverlicenseDate' => '',
            'category' => '',
            'titleDeed' => '',
            'sk' => '',
            'bsoSeries' => '',
            'bsoNumber' => '',
            'contractEndData' => '',
            'contractInsuredDamage' => '',
            'infoDamage' => '',
            'comment' => '',
        ],
        'inquirer' => [ //Обстоятельства ДТП
            'parking' => ['is_a' => false, 'is_b' => false],//ТС находилось на стоянке, парковке,обочине и т.п. в неподвижном состоянии
            'driverAbsent' => ['is_a' => false, 'is_b' => false],//Водитель отсутствовал на месте ДТП
            'movedParking' => ['is_a' => false, 'is_b' => false],//Двигался на стоянке
            'droveParking' => ['is_a' => false, 'is_b' => false],//Выезжал со стоянки, с места парковки,
            'comeParking' => ['is_a' => false, 'is_b' => false],//Заезжал на стоянку, парковку,
            'movedStraight' => ['is_a' => false, 'is_b' => false],//Двигался прямо
            'movedCrossroads' => ['is_a' => false, 'is_b' => false],//Двигался на перекрестке
            'comeCrossroads' => ['is_a' => false, 'is_b' => false],//Заезжал на перекресток
            'movedByCrossroads' => ['is_a' => false, 'is_b' => false],//Двигался по перекрестку
            'movedSameDirectionSameLane' => ['is_a' => false, 'is_b' => false],//двигавшимся в том же направлении по той же полосе
            'movedSameDirectionOtherLane' => ['is_a' => false, 'is_b' => false],//двигавшимся в том же направлении по другой полосе
            'changedLane' => ['is_a' => false, 'is_b' => false],//Менял полосу
            'outrun' => ['is_a' => false, 'is_b' => false],//Обгонял
            'turnedRight' => ['is_a' => false, 'is_b' => false],//Поворачивал направо
            'turnedLeft' => ['is_a' => false, 'is_b' => false],//Поворачивал налево
            'madeUTurn' => ['is_a' => false, 'is_b' => false],//Совершал разворот
            'backedUp' => ['is_a' => false, 'is_b' => false],//Двигался задним ходом
            'wentOncomingTraffic' => ['is_a' => false, 'is_b' => false],//Выехал на сторону дороги, предназначенную для встречного движения
            'secondVehicleWasLeft' => ['is_a' => false, 'is_b' => false],//Второе ТС находилось слева от меня
            'didNotComplyPriorityMarkRequirement' => ['is_a' => false, 'is_b' => false],//Не выполнил требование знака приоритета
            'madeRunOver' => ['is_a' => false, 'is_b' => false],//Совершил наезд
            'trafficLightInhibitStopped' => ['is_a' => false, 'is_b' => false],//Остановился на запрещающий сигнал светофора
            'other' => ['is_a' => false, 'is_b' => false],// Иное
            'otherCommentA' => "",// Иное  A
            'otherCommentB' => "",// Иное B
        ],
        'full_info' => [ //Детальная иинформайия
            'vehicle' => true,//Транспортное средство is_a = true is_b = false
            'detalization' => '',
            'driver' => true,//ТС находилось под управлением водителя
            'moreVehicle' => '',
            'damageOtherName' => '',
            'damageOtherOwner' => '',
            'canVehicleMove' => 'true',
            'whereVehicle' => '',
            'comment' => '',
            'withAppAct' => 'false',
        ]

    ];

    public static function getTempleCategory(Contracts $order, $category)
    {
        $temple_act = InspectionCrash::getTempleOrder($order);
        return $temple_act[$category];
    }

    public static function getTempleOrder(Contracts $order)
    {
        if($inspection = $order->inspection)
        {
            $temple_act = $inspection->temple_act;
            if(strlen($temple_act) < 5) $temple_act = InspectionCrash::setDefaultTemple($order, $inspection);
            $temple_act = \GuzzleHttp\json_decode($temple_act, true);
            return $temple_act;

        }
        return [];
    }

    public static function setDefaultTemple($order, $inspection)
    {
        $temple_act = InspectionCrash::DEFAULT_TEMPLE;

        //ДЕЛАЕМ АВТО ПОДСТАНОВКУ

        $inspection = InspectionCrash::saveInspectionData($inspection, $temple_act);
        return $inspection->temple_act;
    }

    public static function saveTempleOrder(Contracts $order, $category, $data)
    {
        $inspection = $order->inspection;
        $temple_act = InspectionCrash::getTempleOrder($order);

        $temple_act[$category] = \GuzzleHttp\json_decode($data);

        //Преобразования



        InspectionCrash::saveInspectionData($inspection, $temple_act);
        return true;
    }

    public static function saveInspectionData($inspection, $temple_act)
    {
        $inspection->temple_act = \GuzzleHttp\json_encode($temple_act);
        $inspection->save();
        return $inspection;
    }

}