<?php


namespace App\Domain\Processes\Operations\Inspection\Temple;


use App\Domain\Entities\Inspection\Inspection;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;

class InspectionCrashData{

    public static function get_data($id){

        $order = Inspection::get_first($id);

        if($templ_data = isset($order->inspection->temple_act) ? \GuzzleHttp\json_decode($order->inspection->temple_act) : null){
            $data = clone $templ_data;

            $data->vehicle_a->mark = isset($templ_data->vehicle_a->markId) && $templ_data->vehicle_a->markId ? VehicleMarks::find($templ_data->vehicle_a->markId)->title : '';
            $data->vehicle_a->model = isset($templ_data->vehicle_a->modelId) && $templ_data->vehicle_a->modelId ? VehicleModels::find($templ_data->vehicle_a->modelId)->title : '';

            $data->vehicle_b->mark = isset($templ_data->vehicle_a->markId) && $templ_data->vehicle_a->markId ? VehicleMarks::find($templ_data->vehicle_a->markId)->title : '';
            $data->vehicle_b->model = isset($templ_data->vehicle_a->modelId) && $templ_data->vehicle_a->modelId ? VehicleModels::find($templ_data->vehicle_a->modelId)->title : '';

        }else {
            $data = '';
        }

        return $data;
    }
}