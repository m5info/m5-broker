<?php


namespace App\Domain\Processes\Operations\Inspection;


use App\Models\Orders\Inspection;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ObjectInsurer;
use App\Models\Contracts\ObjectInsurerAuto;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\HoldKv;
use App\Models\Orders\InspectionOrders;
use App\Models\Orders\InspectionOrdersLogs;

class InspectionCreate{

    public static function handle(){

        if(auth()->user()->bso_supplier_id){
            $bso_supplier = BsoSuppliers::find(auth()->user()->bso_supplier_id);
        }else{
            $bso_supplier = BsoSuppliers::query()
                ->select('bso_suppliers.*')
                ->leftJoin('hold_kv', 'bso_suppliers.id', '=', 'hold_kv.bso_supplier_id')
                ->leftJoin('products', 'hold_kv.product_id', '=', 'products.id')
                ->where('products.category_id', '=', 6)->first();
        }

        $hold_kv_product = HoldKv::query()
            ->leftJoin('products', 'hold_kv.product_id', '=', 'products.id')
            ->where('hold_kv.bso_supplier_id', '=', $bso_supplier->id)
            ->where('products.category_id', '=', 6)->first();

        $object_insurer = new ObjectInsurer();
        $object_insurer->type = 1;
        $object_insurer->save();

        $auto = new ObjectInsurerAuto();
        $auto->object_insurer_id = $object_insurer->id;
        $auto->save();

        $inspection = new Inspection();
        $inspection->save();

        $order = new Contracts([
            'statys_id' => -4,
            'product_id' => $hold_kv_product->product_id,
            'insurance_companies_id' => $bso_supplier->insurance_companies_id,
            'bso_supplier_id' => $bso_supplier->id,
            'status_order_id' => 0,
            'manager_id' => auth()->id(),
            'user_id' => auth()->id(),
            'object_insurer_id' => $object_insurer->id,
            'inspection_id' => $inspection->id,
            'sign_date' => getDateTime(),
        ]);

        $order->save();

        if(auth()->user()->hasPermission('orders', 'order_supplier_and_fp')){
            InspectionFinancialPolicyActualize::set_default($order->id);
        }

        InspectionOrdersLogs::setLogs($order->id, InspectionOrders::STATUSES[0]);

        return $order;
    }
}