<?php


namespace App\Domain\Processes\Operations\Inspection;


use App\Domain\Entities\Inspection\Inspection;
use App\Models\Contracts\ContractsLogs;
use App\Models\Orders\InspectionOrdersLogs;
use App\Models\Organizations\PointsDepartments;
use App\Models\User;
use App\Services\FireBase\FireBaseAndroid;

class InspectionSetExecutor{

    // назначение чела
    public static function set_user($order_id, $procession_user_id){

        InspectionChangeStatus::change_event($order_id, 2, $procession_user_id);

        $order = Inspection::get_first($order_id);
        if(!(int)$order->inspection->position_type_id){
            $user = User::find($procession_user_id);
            $order->inspection->processing_user_id = $user->id;
            $order->inspection->processing_org_id = $user->organization_id;
            $order->agent_id = $user->id;
            $order->push();


            if($user->firebase && $user->is_work == 1){
                $fcm = new FireBaseAndroid();
                $fcm->push("Заявка", "Новая заявка #$order_id", $order_id, 1, $user->firebase);
            }
        }

        return true;
    }

    // назначение организации
    public static function set_org($order_id, $procession_org_id){

        $pd = PointsDepartments::find($procession_org_id);

        InspectionChangeStatus::change_event($order_id, 2, $procession_org_id);
        InspectionChangeStatus::change_status($order_id, 2, $procession_org_id);

        $order = Inspection::get_first($order_id);
        $order->inspection->processing_org_id = $pd->organization_id;
        $order->inspection->org_point_id = $procession_org_id;
        $order->push();

        return true;
    }

    // чел подтверждает заявку
    public static function processing_user($order_id, $user_id){

        $order = Inspection::get_first($order_id);

        $user = User::find($user_id);
        $order->inspection->processing_user_id = $user->id;
        $order->inspection->processing_org_id = $user->organization_id;
        $order->agent_id = $user->id;
        $order->push();

        // 4 - принял
        InspectionChangeStatus::change_event($order_id, 4, $user_id);

        // 2 - В работе
        InspectionChangeStatus::change_status($order_id, 2, $user_id);

        return $order;
    }

    // чел отклоняет заявку
    public static function decline_order($order_id, $user_id){


        ContractsLogs::create([
            'contract_id' => $order_id,
            'status_id' => -4,
            'user_id' => auth()->id(),
        ]);


        $order = Inspection::get_first($order_id);

        $order->agent_id = null;
        $order->inspection->org_point_id = null;
        $order->inspection->processing_org_id = null;
        $order->inspection->processing_user_id = null;
        $order->inspection->declined_user_id = $user_id;
        $order->push();

        // 3 - отклонил
        InspectionChangeStatus::change_event($order_id, 3, $user_id);

        return $order;
    }

    // чел приехал на заявку
    public static function arrived_order($order_id, $user_id){

        $order = Inspection::get_first($order_id);

        $user = User::find($user_id);
        $order->inspection->processing_user_id = $user->id;
        $order->inspection->processing_org_id = $user->organization_id;
        $order->agent_id = $user->id;
        $order->push();

        // 5 - Приехал
        InspectionChangeStatus::change_event($order_id, 5, $user_id);

        return $order;
    }

    // чел Закончил
    public static function finish_order($order_id, $user_id){

        $order = Inspection::get_first($order_id);
        $order->end_date = getDateTime();
        $order->push();

        // 6 - Закончил
        InspectionChangeStatus::change_event($order_id, 6, $user_id);
        InspectionChangeStatus::change_status($order_id, 3, $user_id);

        return $order;
    }
}