<?php


namespace App\Domain\Processes\Operations\Inspection;


use App\Domain\Entities\Inspection\Inspection;
use App\Models\Contracts\Contracts;
use App\Models\Orders\InspectionOrders;
use App\Models\Orders\InspectionOrdersLogs;

class InspectionChangeStatus{

    // $selected_executor - если назначается исполнитель
    public static function  change_event($order_id, $status_id, $selected_executor = null){

        $order = Inspection::get_first($order_id);

        InspectionOrdersLogs::setLogs($order->id, InspectionOrdersLogs::LOGS_STATUSES[$status_id], $selected_executor);
        $order->inspection->status_id = $status_id;
        $order->push();

        return $order;
    }

    public static function change_status($order_id, $status_id, $selected_executor = null){

        $order = Inspection::get_first($order_id);

        InspectionOrdersLogs::setLogs($order->id, InspectionOrders::STATUSES[$status_id], $selected_executor);
        $order->status_order_id = $status_id;
        $order->save();

        return $order;
    }

}