<?php

namespace App\Domain\Processes\Operations\Payments;

use App\Domain\Entities\Contracts\EContract;
use App\Domain\Entities\Payments\EPayment;

class PaymentSupervising{

    public static function actualize($payment){

        if($payment = EPayment::get_first($payment)){

            if($contract = EContract::get_first($payment->contract_id)){

                $payment->update([
                    'agent_id' => $contract->agent_id,
                    'manager_id' => $contract->manager_id,
                    'parent_agent_id' => $contract->parent_agent_id,
                ]);

            }

            return $payment;
        }

    }

}