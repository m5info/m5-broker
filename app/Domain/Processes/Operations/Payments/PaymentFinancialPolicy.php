<?php

namespace App\Domain\Processes\Operations\Payments;

use App\Domain\Entities\Contracts\EContract;
use App\Domain\Entities\Payments\EPayment;
use App\Models\Contracts\Payments;
use App\Models\Directories\FinancialPolicy;

class PaymentFinancialPolicy{

    /**
     * Обновить фин. политику платежа в соответствии с контрактом
     */
    public static function actualize($payment){

        // если не заполнена или есть доступ
        if(!(int)$payment->financial_policy_id || auth()->user()->hasPermission('contracts', 'select_financial_policy')){

            if($payment = EPayment::get_first($payment) ){

                if($contract = EContract::get_first($payment->contract_id)){

                    if((int)$payment->reports_order_id <= 0 && (int)$payment->reports_dvou_id <= 0 && $payment->type_id == 0){//перерасчет только для взносов

                        //только если прямая продажа или платёж не оплачен
                        if($contract->sales_condition == 1 || ($contract->sales_condition != 1 && $payment->statys_id != 1)){

                            $payment->financial_policy_id = (int) $contract->financial_policy_id;

                            $payment->financial_policy_kv_bordereau = getFloatFormat($contract->financial_policy_kv_bordereau);
                            $payment->financial_policy_kv_dvoy = getFloatFormat($contract->financial_policy_kv_dvoy);

                            if($payment->financial_policy_id > 0){

                                $fp = FinancialPolicy::findOrFail($payment->financial_policy_id);

                                $payment->financial_policy_kv_bordereau = getFloatFormat($fp->kv_bordereau);
                                $payment->financial_policy_kv_dvoy = getFloatFormat($fp->kv_dvou);


                                $group_kv = $fp->getGroupKV($payment);

                                if($fp->fix_price == 1)
                                {
                                    $payment_total = $payment->payment_total;


                                    //СДЕЛАТЬ ПРАВИЛЬНЫЙ РАСЧЕТ
                                    $fix_price_sum = $fp->fix_price_sum;
                                    $fix_price_sum_reward = getFloatFormat($fp->partnership_reward_1+$fp->partnership_reward_2+$fp->partnership_reward_3);

                                    $base_price = getProcentToSum($payment_total, $fix_price_sum);
                                    $base_price_reward = getProcentToSum($payment_total, $fix_price_sum_reward);

                                    $payment->financial_policy_kv_dvoy = 0;


                                    if((int)getFloatFormat($fp->kv_agent) > 0){

                                        $payment->financial_policy_kv_bordereau = 100 - $base_price;


                                        $kv_agent = $fp->kv_agent;
                                        $kv_parent = $fp->kv_parent;

                                        if($group_kv){
                                            $kv_agent = $group_kv->kv_agent;
                                            $kv_parent = $group_kv->kv_parent;
                                        }

                                        $kv_agent_total = $payment_total - $kv_agent;

                                        $payment->financial_policy_kv_agent = getProcentToSum($payment_total, $kv_agent_total);

                                        if($kv_parent > 0){
                                            $kv_parent_total = $payment_total - $kv_parent;
                                            $payment->financial_policy_kv_parent = getProcentToSum($payment_total, $kv_parent_total);
                                        }else{
                                            $payment->financial_policy_kv_parent = 0;
                                        }


                                    }else{

                                        $payment->financial_policy_kv_bordereau = 100;

                                        $payment->financial_policy_kv_agent = 100 - $base_price - $base_price_reward;
                                        if($payment->contract->sales_condition == 1){
                                            $payment->financial_policy_kv_bordereau = 100 - $payment->financial_policy_kv_agent;
                                        }

                                        if($group_kv){
                                            $payment->financial_policy_kv_parent = getProcentToSum($payment_total, $group_kv->kv_parent);
                                        }

                                    }


                                } else{

                                    $payment->financial_policy_kv_agent = getFloatFormat($fp->kv_agent);
                                    $payment->financial_policy_kv_parent = getFloatFormat($fp->kv_parent);

                                    if($group_kv){
                                        $payment->financial_policy_kv_agent = getFloatFormat($group_kv->kv_agent);
                                        $payment->financial_policy_kv_parent = getFloatFormat($group_kv->kv_parent);
                                    }
                                }

                            }else{
                                $payment->financial_policy_kv_agent = getFloatFormat($contract->financial_policy_kv_agent);
                                $payment->financial_policy_kv_parent = getFloatFormat($contract->financial_policy_kv_parent);
                            }


                            $payment->financial_policy_kv_bordereau_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_bordereau);
                            $payment->financial_policy_kv_dvoy_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_dvoy);
                            $payment->financial_policy_kv_agent_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_agent);
                            $payment->financial_policy_kv_parent_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_parent);
                            $payment->save();

                        }
                    }
                }

            }
        }

        return $payment;

    }


    /**
     * Обновить фин. политику платежа в соответствии с первым платежем
     */
    public static function actualizeByFirstPayment($payment)
    {
        // если не заполнена или есть доступ
        if($payment = EPayment::get_first($payment)){

            if($contract = EContract::get_first($payment->contract_id)){

                $first_payment = Payments::where('contract_id', $contract->id)
                    ->where('payment_number', '<', $payment->payment_number)
                    ->where('is_deleted', 0)
                    ->where('statys_id', '=', 1)
                    ->get()->first();

                if($first_payment){

                    $payment->financial_policy_id = (int) $first_payment->financial_policy_id;

                    $payment->financial_policy_kv_bordereau = getFloatFormat($first_payment->financial_policy_kv_bordereau);
                    $payment->financial_policy_kv_dvoy = getFloatFormat($first_payment->financial_policy_kv_dvoy);

                    if($payment->financial_policy_id > 0){

                        $fp = FinancialPolicy::findOrFail($payment->financial_policy_id);

                        $payment->financial_policy_kv_bordereau = getFloatFormat($fp->kv_bordereau);
                        $payment->financial_policy_kv_dvoy = getFloatFormat($fp->kv_dvou);

                        $group_kv = $fp->getGroupKV($payment);

                        if($fp->fix_price == 1)
                        {
                            $payment_total = $payment->payment_total;

                            //СДЕЛАТЬ ПРАВИЛЬНЫЙ РАСЧЕТ
                            $fix_price_sum = $fp->fix_price_sum;
                            $fix_price_sum_reward = getFloatFormat($fp->partnership_reward_1+$fp->partnership_reward_2+$fp->partnership_reward_3);

                            $base_price = getProcentToSum($payment_total, $fix_price_sum);
                            $base_price_reward = getProcentToSum($payment_total, $fix_price_sum_reward);

                            $payment->financial_policy_kv_dvoy = 0;


                            if((int)getFloatFormat($fp->kv_agent) > 0){

                                $payment->financial_policy_kv_bordereau = 100 - $base_price;


                                $kv_agent = $fp->kv_agent;
                                $kv_parent = $fp->kv_parent;

                                if($group_kv){
                                    $kv_agent = $group_kv->kv_agent;
                                    $kv_parent = $group_kv->kv_parent;
                                }

                                $kv_agent_total = $payment_total - $kv_agent;

                                $payment->financial_policy_kv_agent = getProcentToSum($payment_total, $kv_agent_total);

                                if($kv_parent > 0){
                                    $kv_parent_total = $payment_total - $kv_parent;
                                    $payment->financial_policy_kv_parent = getProcentToSum($payment_total, $kv_parent_total);
                                }else{
                                    $payment->financial_policy_kv_parent = 0;
                                }


                            }else{

                                $payment->financial_policy_kv_bordereau = 100;

                                $payment->financial_policy_kv_agent = 100 - $base_price - $base_price_reward;
                                if($payment->contract->sales_condition == 1){
                                    $payment->financial_policy_kv_bordereau = 100 - $payment->financial_policy_kv_agent;
                                }

                                if($group_kv){
                                    $payment->financial_policy_kv_parent = getProcentToSum($payment_total, $group_kv->kv_parent);
                                }

                            }


                        }
                        else{

                            $payment->financial_policy_kv_agent = getFloatFormat($fp->kv_agent);
                            $payment->financial_policy_kv_parent = getFloatFormat($fp->kv_parent);

                            if($group_kv){
                                $payment->financial_policy_kv_agent = getFloatFormat($group_kv->kv_agent);
                                $payment->financial_policy_kv_parent = getFloatFormat($group_kv->kv_parent);
                            }
                        }




                    }else{
                        $payment->financial_policy_kv_agent = getFloatFormat($first_payment->financial_policy_kv_agent);
                        $payment->financial_policy_kv_parent = getFloatFormat($first_payment->financial_policy_kv_parent);
                    }


                    $payment->financial_policy_kv_bordereau_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_bordereau);
                    $payment->financial_policy_kv_dvoy_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_dvoy);
                    $payment->financial_policy_kv_agent_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_agent);
                    $payment->financial_policy_kv_parent_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_parent);
                    $payment->save();

                }
            }
        }


        return $payment;

    }


}