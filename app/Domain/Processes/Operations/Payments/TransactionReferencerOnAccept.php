<?php


namespace App\Domain\Processes\Operations\Payments;


use App\Models\Settings\UserBalanceSettings;
use App\Models\User;
use App\Models\Users\UsersBalanceTransactions;

class TransactionReferencerOnAccept
{
    public static function set_transaction($contract)
    {
        if ($contract->sales_condition == 1){

            $payments = $contract->payments()->where('type_id', '=', 0)->get();

            foreach($payments as $payment){
                if((int)$payment->referencer_id && (int)$payment->referencer_kv_total){
                    if ((int)$payment->transaction_referencer_id){ // если уже закинули на баланс рекомендодателю

                    } else{
                        $referencer_kv_total = $payment->referencer_kv_total;
                        $_b = UserBalanceSettings::where('isset_for_kv', '=', 1)->where('is_actual', 1)->get()->first();

                        $referencer_balance = $payment->referencer->getBalance($_b->id);
                        $purpose_payment = "Безусловный акцеп договора {$contract->bso_title}, комиссия рекомедодателя";
                        $transaction = $referencer_balance->setTransactions(0, 0, getFloatFormat($referencer_kv_total), date("Y-m-d H:i:s"), $purpose_payment);
                        $payment->transaction_referencer_id = $transaction->id;
                        $payment->save();
                    }
                }
            }
        }
    }

    public static function store_transaction($contract)
    {
        if ($contract->sales_condition == 1){

            $payment = $contract->payments()->where('type_id', '=', 0)->get()->last();

            if((int)$payment->referencer_id && (int)$payment->referencer_kv_total){
                if ((int)$payment->transaction_referencer_id){ // если ранее закинули на баланс рекомендодателю
                    $transaction_log = UsersBalanceTransactions::find((int)$payment->transaction_referencer_id);
                    $referencer_kv_total = $transaction_log->total_sum;
                    $_b = UserBalanceSettings::where('isset_for_kv', '=', 1)->where('is_actual', 1)->get()->first();

                    $referencer_balance = $payment->referencer->getBalance($_b->id);
                    $purpose_payment = "Отмена - ".$transaction_log->purpose_payment ;
                    $referencer_balance->setTransactions(0, 1, getFloatFormat($referencer_kv_total), date("Y-m-d H:i:s"), $purpose_payment);
                    $payment->transaction_referencer_id = 0;
                    $payment->save();
                }
            }
        }
    }
    //при пересчете платежа после безусловного акцепта при изменении суммы кв рекомендодателя делаем перезачисления на баланс
    public static function check_balance($payment)
    {
        $transaction_log = UsersBalanceTransactions::find((int)$payment->transaction_referencer_id);
        $referencer_kv_total = $transaction_log->total_sum;

        if ($payment->referencer_kv_total != $referencer_kv_total){
            $_b = UserBalanceSettings::where('isset_for_kv', '=', 1)->where('is_actual', 1)->get()->first();

            $referencer_balance = $payment->referencer->getBalance($_b->id);

            $purpose_payment = "Отмена - ".$transaction_log->purpose_payment ;
            $referencer_balance->setTransactions(0, 1, getFloatFormat($referencer_kv_total), date("Y-m-d H:i:s"), $purpose_payment);

            $transaction = $referencer_balance->setTransactions(0, 0, getFloatFormat($payment->referencer_kv_total), date("Y-m-d H:i:s"), $transaction_log->purpose_payment);
            $payment->transaction_referencer_id = $transaction->id;
            $payment->save();
        }

        return $payment;
    }
}