<?php

namespace App\Domain\Processes\Operations\Payments;

use App\Domain\Entities\Payments\EPayment;
use App\Models\Actions\PaymentAccept;

class PaymentAcceptOperations{


    public static function update_or_create($payment){

        if($payment = EPayment::get_first($payment)){

            $auth_id = 0;

            $contract = $payment->contract;

            if($payment_accepts = $contract->payment_accepts){

                foreach($payment_accepts as $payment_accept){

                    if(auth()->user()->role->id != 7 && auth()->user()->role->id != 11){ // не Агент/Менеджер/Эксперт

                        if($payment_accept->accept_user_id > 0){
                            $auth_id = $payment_accept->accept_user_id;
                        }else{
                            $auth_id = auth()->id();
                        }

                    }
                }
            }

            $update_data = [
                'contract_id' => isset($payment->contract->id) ? $payment->contract->id : 0,
                'payment_id' => isset($payment->id) ? $payment->id : 0,
                'accept_user_id' => $auth_id,
                'parent_user_id' => auth()->user()->parent_id,
                'accept_date' => date('Y-m-d H:i:s'),
                'kind_acceptance' => $payment->contract->kind_acceptance,
            ];

            $accept = PaymentAccept::query()
                ->where('contract_id', $update_data['contract_id'])
                ->where('payment_id', $update_data['payment_id'])
                ->first();

            if($accept){
                $accept->update($update_data);
            }else{
                PaymentAccept::create($update_data);
            }

        }

        return $payment;

    }

}