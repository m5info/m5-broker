<?php

namespace App\Domain\Processes\Operations\Payments;

use App\Domain\Entities\Payments\EAtolCheck;
use App\Domain\Entities\Payments\EPayment;
use App\Models\Finance\Invoice;

class PaymentInvoice{

    public static function create_to_payment($payment, $setBalance = true){

        if($payment = EPayment::get_first($payment)){

            if($payment->payment_type == 1 && $payment->payment_flow == 1){

                $invoice = new Invoice();
                $invoice->status_id = 1;
                $invoice->user_id = $payment->agent_id;
                $invoice->type = 'sk';
                $invoice->create_type = 1;
                $invoice->org_id = $payment->bso->org_id;
                $invoice->agent_id = $payment->agent_id;
                $invoice->type_invoice_payment_id = 4;
                $invoice->save();
                $payment->invoice_id = $invoice->id;
                $payment->invoice_payment_total = 0;
                $payment->save();

                $invoice->setPaymentNonChash(0, $setBalance);

                return true;


            }
        }

        return false;
    }

}