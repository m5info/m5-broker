<?php

namespace App\Domain\Processes\Operations\Payments;

use App\Domain\Entities\Payments\EAtolCheck;
use App\Domain\Entities\Payments\EPayment;

class PaymentAtolCheck{

    public static function create_to_payment($payment, $data){

        if($payment = EPayment::get_first($payment)){

            if($atol_check = EAtolCheck::create($data)){

                $payment->update(['atol_check_id' => $atol_check->id]);

                return true;
            }

        }

        return false;
    }

}