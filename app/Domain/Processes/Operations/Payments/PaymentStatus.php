<?php

namespace App\Domain\Processes\Operations\Payments;

use App\Domain\Entities\Payments\EPayment;
use App\Models\Contracts\Payments;

class PaymentStatus{


    public static function set($payment, $status){

        if($payment = EPayment::get_first($payment)){

            if(isset(Payments::STATUS[(int)$status])){

                $payment->update(['statys_id' => (int)$status]);

            }

        }

        return $payment;
    }


}