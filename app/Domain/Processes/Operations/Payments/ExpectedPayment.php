<?php


namespace App\Domain\Processes\Operations\Payments;


use App\Models\Actions\ExpectedPayments;

class ExpectedPayment{

    public static function update_or_create($contract, $data){

        if(isset($data->algo_payment)){

//            $algo_payments_count = count($data->algo_payment); // количество платежей для ожидаемых
//            $algo_expected_payments = $contract->expected_payments->count(); // количество ожидаемых

            ExpectedPayments::query()
                ->where('contract_id', $contract->id)
                ->delete();

            foreach($data->algo_payment as $algo_payment){

                $update_data = [
                    'status_id' => 0,
                    'contract_id' => $contract->id,
                    'payment_date' => setDateTimeFormat($algo_payment['date']),
                    'payment_sum' => getFloatFormat($algo_payment['sum']),
                ];

                ExpectedPayments::create($update_data);

            }
        }

        return $contract;
    }
}