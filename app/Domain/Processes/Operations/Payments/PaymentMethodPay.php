<?php

namespace App\Domain\Processes\Operations\Payments;

use App\Domain\Entities\Payments\EPayment;
use App\Models\Contracts\Payments;
use App\Models\Finance\PayMethod;

class PaymentMethodPay{

    public static function set($payment, $method_id){

        if($payment = EPayment::get_first($payment)){

            if($method = PayMethod::query()->where('id', (int)$method_id)->first()){

                $payment->update(['pay_method_id' => $method->id]);

            }

        }

        return $payment;


    }

}