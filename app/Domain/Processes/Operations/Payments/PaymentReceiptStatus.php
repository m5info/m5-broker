<?php

namespace App\Domain\Processes\Operations\Payments;

use App\Domain\Entities\Bso\EBsoItem;
use App\Domain\Entities\Contracts\EContract;
use App\Domain\Entities\Payments\EPayment;
use App\Domain\Process;
use App\Domain\Processes\Operations\Bso\BsoLocationOperations;
use App\Models\BSO\BsoLogs;

class PaymentReceiptStatus{

    public static function refresh($contract){

        if($contract = EContract::get_first($contract)){

            if(isset($contract->payments) && $contract->payments && $contract->payments->count()){

                // берем только первый платеж
                $firstPayment = $contract->payments->first();

                //если метод оплаты - квитанция и есть бсо
                if ($firstPayment->bso_receipt_id && $contract->bso){

                    $bso = $contract->bso;

                    //и если она существует
                    if($bso_receipt = EBsoItem::get_first($firstPayment->bso_receipt_id)) {

                        $bso_receipt->update([
                            'state_id' => $bso->state_id,
                            'location_id' => $bso->location_id,
                        ]);

                        BsoLogs::setLogs($bso_receipt->id, $bso->state_id, $bso->location_id);
                    }
                }
            }
        }

        return $contract;
    }

}