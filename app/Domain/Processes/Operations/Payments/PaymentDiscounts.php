<?php

namespace App\Domain\Processes\Operations\Payments;

use App\Domain\Entities\Contracts\EContract;
use App\Domain\Entities\Payments\EAtolCheck;
use App\Domain\Entities\Payments\EPayment;
use App\Models\Directories\FinancialPolicy;

class PaymentDiscounts{

    public static function recount($payment){

        if($payment = EPayment::get_first($payment)){
            if($payment->type_id == 0) {//перерасчет только для взносов
                $payment->update([
                    "official_discount_total" => getTotalSumToPrice($payment->payment_total, $payment->official_discount),
                    "informal_discount_total" => getTotalSumToPrice($payment->payment_total, $payment->informal_discount),
                    "bank_kv_total" => getTotalSumToPrice($payment->payment_total, $payment->bank_kv),
                ]);



                $partnership_reward = 0;

                //БУДЕТ ТУТ
                // КВ БОРДЕРО - (official_discount + bank_kv)
                // КВ Агента/Менеджера - (official_discount + bank_kv)

                if($contract = EContract::get_first($payment->contract_id)) {

                    //только если прямая продажа или платёж не оплачен
                    if ($contract->sales_condition == 1 || ($contract->sales_condition != 1 && $payment->statys_id != 1)) {

                        if ($payment->financial_policy_manually_set == 0 && $payment->financial_policy_id > 0) {

                            //$financial_policy = FinancialPolicy::findOrFail($payment->financial_policy_id);


                            if ($payment->bank_kv > 0) {

                                $payment->financial_policy_kv_bordereau = $payment->financial_policy_kv_bordereau - $payment->bank_kv;
                                $payment->financial_policy_kv_agent = $payment->financial_policy_kv_agent - $payment->bank_kv;

                                $payment->financial_policy_kv_bordereau_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_bordereau);
                                $payment->financial_policy_kv_agent_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_agent);

                            }

                            if ($payment->official_discount > 0) {

                                $payment->financial_policy_kv_bordereau = $payment->financial_policy_kv_bordereau - $payment->official_discount;
                                $payment->financial_policy_kv_agent = $payment->financial_policy_kv_agent - $payment->official_discount;

                                $payment->financial_policy_kv_bordereau_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_bordereau);
                                $payment->financial_policy_kv_agent_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_agent);

                            }

                            if ($payment->informal_discount_total > 0 && $payment->contract->sales_condition != 0) {

                                $payment->financial_policy_kv_agent = $payment->financial_policy_kv_agent - $payment->informal_discount;

                            }

                            // если есть коммисия > 0
                            if ($pay_mehtod = $payment->pay_mehtod) {
                                if (isset($pay_mehtod->acquiring) && $pay_mehtod->acquiring > 0) {

                                    $payment->financial_policy_kv_agent = $payment->financial_policy_kv_agent - $pay_mehtod->acquiring;
                                    $payment->acquire_percent = $pay_mehtod->acquiring;
                                    $payment->acquire_total = getTotalSumToPrice($payment->payment_total, $pay_mehtod->acquiring);
                                }
                            }

                            $payment->financial_policy_kv_agent_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_agent);

                            $financial_policy = $payment->financial_policy;
                            if (isset($financial_policy->partnership_reward_1)) {

                                if ($financial_policy->fix_price == 1) {

                                    $partnership_reward = getProcentToSum($payment->payment_total, $financial_policy->partnership_reward_1);
                                    $partnership_reward += getProcentToSum($payment->payment_total, $financial_policy->partnership_reward_2);
                                    $partnership_reward += getProcentToSum($payment->payment_total, $financial_policy->partnership_reward_3);


                                } else {
                                    $partnership_reward = ($financial_policy->partnership_reward_1 + $financial_policy->partnership_reward_2 + $financial_policy->partnership_reward_3);
                                }


                            }
                            if ($payment->contract->sales_condition == 1) {
                                //расчет кв рекомендодателя в % от платежа в зависимости от доли кв рекомендодателя от входящего кв
                                if ($payment->referencer_kv > 0) {
                                    $payment->referencer_kv_real = $payment->financial_policy_kv_agent * $payment->referencer_kv / 100; //% входящего кв * % долю кв рекомендодателя / 100%
                                } else {
                                    $payment->referencer_kv_real = 0;
                                }
                                $payment->referencer_kv_total = getTotalSumToPrice($payment->payment_total, $payment->referencer_kv_real);

                                $payment->financial_policy_kv_agent -= $payment->referencer_kv_real;

                                if ($payment->financial_policy_kv_agent < 0) {
                                    $payment->financial_policy_kv_agent = 0;
                                }

                                $payment->financial_policy_kv_agent_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_agent);
                            }
                        }

                        if($payment->financial_policy_manually_set == 1 && $payment->contract->sales_condition == 1) { //фп вручную и продажа организации

                            if ($payment->referencer_kv > 0) {
                                $payment->referencer_kv_real = $payment->financial_policy_kv_agent * $payment->referencer_kv / (100 - $payment->referencer_kv); //% входящего кв * % долю кв рекомендодателя / 100%
                            } else {
                                $payment->referencer_kv_real = 0;
                            }
                            $payment->referencer_kv_total = getTotalSumToPrice($payment->payment_total, $payment->referencer_kv_real);

                        }


                        if ($payment->contract->sales_condition == 1) {

                            $marjing = ($payment->financial_policy_kv_bordereau + $payment->financial_policy_kv_dvoy) - (
                                    getFloatFormat($payment->informal_discount) +
                                    getFloatFormat($payment->referencer_kv_real) +
                                    getFloatFormat($payment->acquire_percent) +
                                    getFloatFormat($partnership_reward));


                        } else {
                            $marjing = ($payment->financial_policy_kv_bordereau + $payment->financial_policy_kv_dvoy) - (
                                    getFloatFormat($payment->financial_policy_kv_agent) +
                                    getFloatFormat($payment->acquire_percent) +
                                    getFloatFormat($partnership_reward));
                        }


                        $payment->financial_policy_marjing = $marjing;
                        $payment->financial_policy_marjing_total = getTotalSumToPrice($payment->payment_total, $marjing);

                        $payment->save();

                    }
                }

                if($payment->transaction_referencer_id){//если ранее были начисления на баланс по платежу, проверяем наличие изменений
                    $payment = TransactionReferencerOnAccept::check_balance($payment);
                }

                return $payment;
            }

        }


        return false;


    }

}