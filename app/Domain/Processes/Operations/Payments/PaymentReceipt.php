<?php

namespace App\Domain\Processes\Operations\Payments;

use App\Domain\Entities\Bso\EBsoItem;
use App\Domain\Entities\Payments\EPayment;
use App\Domain\Process;
use App\Domain\Processes\Operations\Bso\BsoLocationOperations;
use App\Models\BSO\BsoLogs;

class PaymentReceipt{


    public static function attach($payment, $data){

        if($payment = EPayment::get_first($payment)){

            //если метод оплаты - квитанция + её указали
            if(isset($data->bso_receipt_id) && $data->bso_receipt_id > 0){

                //и если она существует
                if($bso_receipt = EBsoItem::get_first($data->bso_receipt_id)){

                    $bso_receipt->update([
                        'contract_id' => $payment->contract->id,
                        'state_id' => 2,
                    ]);

                    //если пришло забрать доки
                    if(Process::get_process()->get('withdraw_documents') == 1){

                        $bso_receipt->update([
                            'location_id' => 4,
                            'user_id' => auth()->id()
                        ]);

                    }

                    BsoLogs::setLogs($bso_receipt->id, $bso_receipt->state_id, $bso_receipt->location_id);

                    $payment->update([
                        'bso_receipt' => $bso_receipt->bso_title,
                        'bso_receipt_id' => $bso_receipt->id,
                        'bso_not_receipt' => 0,
                    ]);

                }

            }else{

                //если без квитанции
                $payment->update([
                    'bso_receipt_id' => 0,
                    'bso_not_receipt' => 1,
                ]);

            }

        }

        return $payment;

    }

}