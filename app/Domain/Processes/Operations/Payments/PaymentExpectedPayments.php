<?php

namespace App\Domain\Processes\Operations\Payments;

use App\Domain\Entities\Payments\EPayment;
use App\Models\Contracts\Payments;

class PaymentExpectedPayments{


    public static function expected($payment){

        if($payment = EPayment::get_first($payment)){

            //$payment->update(['statys_id' => (int)$status]);

            $contract = $payment->contract;

            if((int)$contract->installment_algorithms_id > 0 && $contract->expected_payments){

                $ic_alghoritm = $contract->insurance_companies_algorithm;
                $alghoritm = $ic_alghoritm->algorithm;
                $alghoritm_payments_count = $alghoritm->quantity - 1; // сколько должно быть ожидаемых

                $new_pay_number = $contract->payments->count() + 1;
                foreach($contract->expected_payments as $expected_payment){

                    $created_payments_count = $contract->all_payments()->where('statys_id', '=', -2)->count(); // количество уже созданных ожидаемых

                    $payments_count_for_create = $alghoritm_payments_count - $created_payments_count; // сколько осталось создать

                    if($expected_payment->status_id != 1 && $payments_count_for_create > 0){
                        $new_expected_payment = $payment->replicate();
                        $new_expected_payment->payment_data = $expected_payment->payment_date;
                        $new_expected_payment->payment_total = $expected_payment->payment_sum;
                        $new_expected_payment->payment_number = $new_pay_number++;
                        $new_expected_payment->statys_id = -2;

                        $new_expected_payment->invoice_id = null;
                        $new_expected_payment->invoice_payment_total = null;
                        $new_expected_payment->invoice_payment_date = null;
                        $new_expected_payment->reports_order_id = 0;
                        $new_expected_payment->reports_dvou_id = 0;
                        $new_expected_payment->marker_color = null;
                        $new_expected_payment->marker_text = null;
                        $new_expected_payment->acts_sk_id = 0;
                        $new_expected_payment->atol_check_id = 0;
                        $new_expected_payment->accept_date = '';
                        $new_expected_payment->accept_user_id = 0;
                        $new_expected_payment->realized_act_id = 0;
                        $new_expected_payment->acts_to_underwriting_id = 0;
                        $new_expected_payment->check_user_id = 0;
                        $new_expected_payment->check_date = null;

                        $new_expected_payment->bso_receipt = null;
                        $new_expected_payment->bso_receipt_id = null;

                        $new_expected_payment->save();
                        $new_expected_payment = PaymentFinancialPolicy::actualize($new_expected_payment);
                        $new_expected_payment = PaymentDiscounts::recount($new_expected_payment);

                        $expected_payment->status_id = 1; // платеж из ожидаемого создан
                        $expected_payment->save();
                    }

                }

            }

        }

        return $payment;
    }


}