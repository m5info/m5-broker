<?php


namespace App\Domain\Integrations\Atol;


use App\Models\Api\Atol\AtolCheck;
use App\Models\Api\Atol\AtolError;
use App\Models\Api\Atol\AtolToken;

class Atol{

    private $URL = 'https://testonline.atol.ru/possystem/v4/';
    private $login = 'v4-online-atol-ru';
    private $password = 'iGFFuihss';
    private $group = "v4-online-atol-ru_4179";

    private $token = "";

    private $company_email = 'info@eurogarant.ru';
    private $sno = 'osn';
    private $inn = '5544332219';

    private $payment_address = 'https://v4.online.atol.ru';
    private $callback_url = "test";


    public function __construct(){
        $this->token = $this->auth();
    }

    private function send($data, $fun, $metod = 'POST', $get_data = ''){

        $curl = curl_init();

        $headers = [];
        //$headers[] = 'X-Request-ID: ' . uniqid();
        $headers[] = 'Content-type: application/json; charset=utf-8';
        $headers[] = 'Accept: application/json';

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->URL . $fun . $get_data,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $metod,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //dd($err);
            //Логируем ошибки
            //return null;
        }

        if ($metod == "POST") {
            //dd($response);
        }

        return \GuzzleHttp\json_decode($response);

    }


    private function auth(){

        $temp_token = '';
        $myToken = AtolToken::query()->get()->last();

        if (!$myToken || (time() - $myToken->token_timestamp) > 79200) {

            AtolToken::truncate();

            $res = $this->send(null, 'getToken?', "GET", "login={$this->login}&pass={$this->password}");

            if(isset($res->token)) {

                AtolToken::create([
                    //'token_timestamp' => time(),
                    'token' => $res->token,
                    //"token_datetime" => setDateTimeFormat($res->timestamp)
                ]);
                $temp_token = $res->token;

            } else AtolError::create([
                //'error_timestamp' => time(),
                'text' => $res->error->error_id . " " . $res->error->code . " " . $res->error->text,
                //"error_datetime" => date('Y-m-d H:i:s')
            ]);


        } else {
            $temp_token = $myToken->token_value;
        }

        return $temp_token;
    }

    //регистрируем чек
    public function createcheck($product_name, $product_price, $date, $client_email = '', $client_phone = ''){

        $token = md5($product_name . $product_price . $date . $client_email . $client_phone);

        $duble = AtolCheck::where('token', $token)->get()->first();
        if (!$duble) {
            $ext_id = "EV_API_" . uniqid() . "_" . date("YmdHis");

            $stat = 0;

            if ($product_price > 100000) {
                $stat = -1;
                $this->sendError($product_name . " | " . (titleFloatFormat($product_price)), "Превышение суммы по АТОЛ ОНЛАЙН");
            }

            $atol = AtolCheck::create([
                'check_datetime' => setDateTimeFormat($date),
                'check_sum' => $product_price,
                'check_ext_id' => $ext_id,
                'check_status' => '',
                'check_product' => $product_name,
                'check_client_email' => $client_email,
                'check_client_phone' => $client_phone,
                'stat' => $stat,
                'token' => $token,
            ]);
            if ($atol) {
                //отправляем чек
                if($this->sendcheck($atol)){
                    return $atol;
                }
            }

        } else {
            return false;
        }
    }

    //отправляем чек
    public function sendcheck($atol, $fun = 'sell')
    {
        $product_quantity = 1;
        $product_measurement_unit = '';//'Полис';
        $product_payment_method = 'full_payment';
        $product_payment_object = 'service';
        $product_name = $atol->check_product;
        $product_price = getFloatFormat($atol->check_sum);
        $client_email = $atol->check_client_email;
        $client_phone = $atol->check_client_phone;
        $date = $atol->check_datetime;
        $ext_id = $atol->check_ext_id;

        if ($fun != 'sell') {
            $ext_id = "EV_API_" . uniqid() . "_" . date("YmdHis");
        }

        $company = [
            "email" => $this->company_email,
            "sno" => $this->sno,
            "inn" => $this->inn,
            "payment_address" => $this->payment_address
        ];
        $items = [
            [
                "name" => $product_name,
                "price" => $product_price,
                "quantity" => $product_quantity,
                "sum" => $product_price,
                "measurement_unit" => $product_measurement_unit,
                "payment_method" => $product_payment_method,
                "payment_object" => $product_payment_object,
                "vat" => ["type" => "none"]
            ]
        ];
        $payments = [
            [
                "type" => 1,
                "sum" => $product_price
            ]
        ];

        if (strlen($client_email) > 3) {
            $client = ["email" => $client_email];
        } else {
            if (strlen($client_phone) > 0) {
                $client = ["phone" => $client_phone];
            }
        }


        $data = [
            "external_id" => $ext_id,
            "receipt" => [
                "client" => $client,
                "company" => $company,
                "items" => $items,
                "payments" => $payments,
                "total" => $product_price
            ],
            "service" => ["callback_url" => $this->callback_url],
            "timestamp" => $date
        ];



        $result = $this->send(\GuzzleHttp\json_encode($data), "{$this->group}/$fun?", "POST", "token={$this->token}");


        if ($result->error == null) {

            $atol->stat = 1;

            $atol->check_uuid = $result->uuid;
            $atol->save();

            $check = $this->check_atol($result->uuid);

            $atol->check_status = $check->status;
            $atol->data_info = \GuzzleHttp\json_encode($check);

            /*
            if($check->status == 'done'){
                //Записываем рузультат

            }else{
                //Логируем ошибку

            }


            if ($fun != 'sell') {
                $atol->check_status = 'destroy';
            }
            */

            $atol->save();

            return true;

        } else {
            //Отправка на почту
            $atol->stat = 2;
            $atol->save();
            AtolError::create([
                'atol_id' => $atol->id,
                'text' => $result->error->error_id . " " . $result->error->code . " " . $result->error->text,
            ]);


        }

        return false;

    }

    public function check_atol($uuid){
        sleep(2);
        $result = $this->send(null, "{$this->group}/report/", "GET", "$uuid?token={$this->token}");
        if ($result->status == 'wait') {
            return $this->check_atol($uuid);
        }

        return $result;
    }




}