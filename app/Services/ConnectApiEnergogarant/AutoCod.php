<?php

namespace App\Services\ConnectApiEnergogarant;


use Mockery\Exception;

class AutoCod
{
    protected $url = 'https://connect-api.energogarant.ru/api/get_data/';

    public function __construct()
    {

    }

    private function send($data, $metod = '', $type = 'POST')
    {
        $headers = [];
        $headers[] = 'Type: Auto';
        $headers[] = 'Service: AutoCod';
        $headers[] = 'Order: energogarant_osago_report';
        $headers[] = 'Content-type: application/json; charset=utf-8';
        $headers[] = 'Accept: application/json';


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);


        curl_close($curl);
        return $response;
    }

    //type = тип по сечу ищем vin или regNumber
    //value = значение
    public function getInfo($type, $value)
    {
        $json = \GuzzleHttp\json_encode(['vin'=>$value]);
        $response = $this->send($json);
        $result = null;

        $result = json_decode($response, false, 512, JSON_BIGINT_AS_STRING);

        return $result;
    }


}