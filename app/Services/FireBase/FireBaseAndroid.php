<?php

namespace App\Services\FireBase;

use GuzzleHttp\Client;

class FireBaseAndroid
{

    private $fcm = 'https://fcm.googleapis.com/fcm/send';


    public function register($token)
    {

        $client = new Client();

        $result = $client->post($this->fcm, [
            'headers' => [
                'Content-Type'  => 'application/json',
                'Authorization' => 'key=' . config('services.firebase.api_key'),
            ],
            'body'    => json_encode(['registration_ids' => [$token]])
        ]);

        return $result;

    }

    public function push($title, $message, $id, $event = 1, $token)
    {

        //$event 1 заявка 2 чат
        //'id' ID обьекта или Заявки

        $client = new Client();

        $push = [
            'data' => [
                'title'      => $title,
                'message'    => $message,
                'event'      => $event,
                'id'   => $id,
            ],
            'to'   => $token
        ];


        $result = $client->post($this->fcm, [
            'headers' => [
                'Content-Type'  => 'application/json',
                'Authorization' => 'key=' . config('services.firebase.api_key'),
            ],
            'body'    => json_encode($push)
        ]);

        return $result;

    }

}