<?php

namespace App\Services\FireBase;

use GuzzleHttp\Client;

class FireBaseIOS
{

    private $fcm = 'https://fcm.googleapis.com/fcm/send';

    public function register($token)
    {

        $client = new Client();

        $result = $client->post($this->fcm, [
            'headers' => [
                'Content-Type'  => 'application/json',
                'Authorization' => 'key=' . config('services.firebase.api_key'),
            ],
            'body'    => json_encode(['registration_ids' => [$token]])
        ]);

        return $result;

    }

    public function push($title, $message, $order_id, $token, $event = null, $product_id = null)
    {

        $client = new Client();

        $push = [
            'notification' => [
                'title'         => $title,
                'body'          => $message,
                'sound'          => 'default',
                'body_loc_args' => [
                    [
                        'order_id'   => intval($order_id),
                        'event'      => is_null($event) ? $event : intval($event),
                        'product_id' => is_null($product_id) ? $product_id : intval($product_id)
                    ]
                ]
            ],
            'to'           => $token
        ];

        \Log::info(json_encode($push));

        $result = $client->post($this->fcm, [
            'headers' => [
                'Content-Type'  => 'application/json',
                'Authorization' => 'key=' . config('services.firebase.api_key'),
            ],
            'body'    => json_encode($push)
        ]);

        \Log::info('FCM IOS');
        \Log::info($result->getBody()->getContents());

        return $result;

    }

}