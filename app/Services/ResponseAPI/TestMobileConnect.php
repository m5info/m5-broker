<?php

namespace App\Services\ResponseAPI;


use App\Models\Settings\SettingsSystem;
use App\Services\Front\api\KansaltingRIT_V2_1;
use GuzzleHttp\Client;
use Illuminate\Http\Response;

class TestMobileConnect
{


    public function __construct()
    {

    }



    public static function send($data, $metod = '', $type = 'POST')
    {

        $urls = 'http://127.0.0.1:8000/api/mobile/'.$metod;


        $headers = [];



        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $urls,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $type,
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            echo "EROR:".$err;
                //Логируем ошибки
            return null;
        }




        return $response;
    }




    public static function test($json, $metod)
    {
        return TestMobileConnect::send($json, $metod);
    }




}