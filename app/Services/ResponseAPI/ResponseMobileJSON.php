<?php

namespace App\Services\ResponseAPI;


use App\Models\Settings\SettingsSystem;
use App\Services\Front\api\KansaltingRIT_V2_1;
use GuzzleHttp\Client;
use Illuminate\Http\Response;

class ResponseMobileJSON
{


    public function __construct()
    {

    }



    public static function checkAuth($token) // Проверяем токен
    {
        if(isset($token) && strlen($token) > 10){
            return User::where('api_token', $token)->get()->last();
        }
        return false;
    }




    public static function responseJSON($data)
    {

        $result = new \stdClass();
        $result->status = 'success';
        $result->data = $data;

        return $result;
    }

    public static function responseErrorJSON($text) // Сообщение об ошибке //success
    {
        return ['status'=>'error', 'message'=>$text];
    }




}