<?php

namespace App\Services\BankiRu;


use App\Models\Settings\SettingsSystem;
use App\Services\Front\api\KansaltingRIT_V2_1;
use GuzzleHttp\Client;
use Illuminate\Http\Response;

class IntegrationBankiRu
{

    private $urls = "https://service.banki.ru/api/insurance/v1/leads";
    private $token = "7ca31f54f5bc16177de4df557716fddb4b25676041e32a8dfb102be736980e0ce8fcb9b18325d2fa";

    public function __construct()
    {

    }

    public function send($created_from, $created_to, $limit=5)
    {
        $headers = [];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->urls."?token={$this->token}&limit={$limit}&offset=10&created_from={$created_from}&created_to={$created_to}&type=0",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //Логируем ошибки
            return null;
        }
        return \GuzzleHttp\json_decode($response);
    }



    public function getOrders()
    {

        $res = $this->send("2019-04-09", '2019-04-09', 5);

        dd($res);

        foreach ($res as $order){
            dd($order);
            $this->setOrdersToRIT($order);
        }


       return true;
    }


    public function setOrdersToRIT($order)
    {
        $insurant = $order->extended_info->insurant;
        $car = $order->extended_info->car;
        $params = [];

        $params['FIO']              = $insurant->last_name.' '.$insurant->first_name.' '.$insurant->patronymic;
        $params['Phones']           = $order->phone;
        $params['Date']             = $order->extended_info->policy_start_date;
        $params['Email']            = $order->email;
        $params['Addres']           = $insurant->address_registration->city.' '.$insurant->address_registration->street.' '.$insurant->address_registration->house;
        $params['Source']           = 'banki.ru';
        $params['KindOfInsurance']  = "ОСАГО";
        $params['Marka']            = $car->brand;
        $params['Model']            = $car->model;
        $params['YearObject']       = $car->year;
        $params['HtmlForm']         = $this->creteHTMLTab($insurant, $car, $order->extended_info->drivers);

        $url = "http://192.168.2.18/k/Web/default.aspx";
        $ch = curl_init();                              // инициализируем сессию curl

        curl_setopt($ch, CURLOPT_URL,$url);             // указываем URL, куда отправлять POST-запрос
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);    // разрешаем перенаправление
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);     // указываем, что результат запроса следует передать в переменную, а не вывести на экран
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);           // таймаут соединения
        curl_setopt($ch, CURLOPT_POST, 1);              // указываем, что данные надо передать именно методом POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);  // добавляем данные POST-запроса
        $numOrder = curl_exec($ch);                     // выполняем запрос
        curl_close($ch);                                // завершаем сессию

        dd($numOrder);

    }

    public function creteHTMLTab($insurant, $car, $drivers)
    {

        $sex = ($insurant->sex == 'm')?"Мужской":"Женский";
        $issue_date = getDateFormatRu($insurant->passport->issue_date);
        $car_issue_date = getDateFormatRu($car->vehicle_document->issue_date);

        $car_type = ($car->vehicle_document->type == 'STS')?"СТС":"ПТС";

        $driver_html = '';

        if($drivers && count($drivers)>0){
            $driver_html = $this->creteHTMLTabDrivers($drivers);
        }


        $result = "
<table>
    <tr>
        <td>ФИО</td>
        <td>{$insurant->last_name} {$insurant->first_name} {$insurant->patronymic}</td>
    </tr>
    <tr>
        <td>Пол</td>
        <td>$sex</td>
    </tr>
    <tr>
        <td>Телефон</td>
        <td>{$insurant->phone}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{$insurant->email}</td>
    </tr>
    <tr>
        <td>Паспорт</td>
        <td>{$insurant->passport->series} {$insurant->passport->number}</td>
    </tr>
    <tr>
        <td>Выдан</td>
        <td>{$insurant->passport->issued_by}</td>
    </tr>
    <tr>
        <td>Дата выдачи</td>
        <td>{$issue_date}</td>
    </tr>
    <tr>
        <td>Адрес</td>
        <td>{$insurant->address_registration->city} {$insurant->address_registration->street} {$insurant->address_registration->house}</td>
    </tr>
    <tr>
        <td>Марка, модель</td>
        <td>{$car->brand} {$car->model} ({$car->modification})</td>
    </tr>
    <tr>
        <td>Год</td>
        <td>{$car->year}</td>
    </tr>
    <tr>
        <td>VIN</td>
        <td>{$car->vin}</td>
    </tr>
    <tr>
        <td>Рег номер</td>
        <td>{$car->license_plate}</td>
    </tr>
    <tr>
        <td>Документ {$car_type}</td>
        <td>{$car->vehicle_document->series} {$car->vehicle_document->number}</td>
    </tr>
    <tr>
        <td>Дата выдачи</td>
        <td>{$car_issue_date}</td>
    </tr>
    {$driver_html}
    
</table>";

        return $result;
    }


    public function creteHTMLTabDrivers($drivers)
    {
        $driver_html = '';
        foreach ($drivers as $key => $driver){
            $driver_html .= "
            <tr>
                <td>Водитель ".($key+1)."</td>
                <td>{$driver->last_name} {$driver->first_name} {$driver->patronymic}</td>
            </tr>
            <tr>
                <td>В/У</td>
                <td>{$driver->license->series} {$driver->license->number}</td>
            </tr>
            <tr>
                <td>Стаж</td>
                <td>".(getDateFormatRu($driver->license->first_issue_date))."</td>
            </tr>
            ";
        }


        return $driver_html;
    }


}