<?php

namespace App\Services\BankiRu;


use App\Models\Fronts\FrontLids;
use App\Models\Settings\SettingsSystem;
use App\Services\Front\api\KansaltingRIT_V2_1;
use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;

class IntegrationCubeGroup
{

    private $urls = "https://kasko-calculators.ru/api/public/insuranceleads";
    //private $urls = "http://calc.cubegroup.services/api/public/insuranceleads";
    private $username = "komandacube";

    private $password = "qweasd12345";
    private $token = '';


    public function __construct()
    {
        $this->token = base64_encode($this->username.':'.$this->password);
    }


    private function send($method, $data = null, $REQUEST = "POST", $file = null)
    {


        $headers = [];
        $headers[] = 'authorization: Basic '.$this->token;
        $headers[] = 'Content-type: application/json; charset=utf-8';
        $headers[] = 'Accept: application/json';

        /*
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->urls.'?pageNumber=3');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username.':'.$this->password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $REQUEST);

        $data = curl_exec($ch);
        curl_close($ch);

        dd($data);
        */


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->urls.$method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $REQUEST,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            echo "ERROR:";
            dd($err);
            //echo $err;
            //Логируем ошибки
            return null;
        }


        return \GuzzleHttp\json_decode($response);
    }



    public function getOrders()
    {

        $res = $this->send('?pageNumber=1&city_in=[1,16]&phone_valid=1', null, 'GET');

        foreach ($res as $order){

            $front = FrontLids::where('front_key', (int)$order->id)->get()->first();
            if(!$front) $this->setOrdersToRIT($order);
        }


       return true;
    }

    public function setOrdersToRIT($order)
    {
        $params = [];

        $car = explode(" ", $order->car->value);

        $car_brand = $car[0];
        $car_model = str_replace($car_brand.' ', '', $order->car->value);

        $getSourceAnndHTML = $this->creteHTMLTab((array)$order);


        $params['FIO']              = $order->fullName->value;
        $params['Phones']           = getPhoneFormat($order->phoneNumber->value);
        $params['Date']             = date("Y-m-d");
        $params['Email']            = '';
        $params['Addres']           = $order->city->value;
        $params['Source']           = 'CubeGroup';
        $params['KindOfInsurance']  = $getSourceAnndHTML->source;
        $params['Marka']            = $car_brand;
        $params['Model']            = $car_model;
        $params['YearObject']       = $order->year->value;
        $params['HtmlForm']         = $getSourceAnndHTML->html;


        $url = "http://192.168.2.18/k/Web/default.aspx";
        $ch = curl_init();                              // инициализируем сессию curl

        curl_setopt($ch, CURLOPT_URL,$url);             // указываем URL, куда отправлять POST-запрос
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);    // разрешаем перенаправление
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);     // указываем, что результат запроса следует передать в переменную, а не вывести на экран
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);           // таймаут соединения
        curl_setopt($ch, CURLOPT_POST, 1);              // указываем, что данные надо передать именно методом POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);  // добавляем данные POST-запроса
        $numOrder = curl_exec($ch);                     // выполняем запрос

        if ($numOrder === false){
            Mail::raw('Ошибка в кроне', function ($message) {
                $message->to('info@sst.cat')->subject('Ошибка крон!');
            });
        };



        curl_close($ch);                                // завершаем сессию

        $params['front_key'] = $order->id;
        FrontLids::create($params);

        return true;
    }


    public function creteHTMLTab($order)
    {
        $res = new \stdClass();
        $res->html = '';
        $res->source = "ОСАГО";

        $result = "<table>";

        foreach ($order as $key => $arr){

            if(isset($arr->value) && isset($key)){
                $label = (isset($arr->label))?$arr->label:'';

                if($key == 'interest') $label = 'СК';

                $my_val = '';
                $result .= "";



                if(is_array($arr->value)){

                    foreach ($arr->value as $val){
                        if(is_string($val)) {
                            $my_val .= (string)$val.",";


                            if($label == 'Что считаем?' && $val == 'КАСКО'){
                                $res->source = "КАСКО";
                            }

                        }else{
                           foreach ($val as $key_temp => $arr_val){
                               $my_val .= $this->getTegsTitle($key_temp).":".(string)$arr_val."; ";
                           }
                        }
                    }

                }else{

                    if (is_string($arr->value)){
                        $my_val = $arr->value;
                    }

                    if(is_object($arr->value)){
                        foreach ($arr->value as $temp){

                            $temp_value = '';

                            if(is_string($temp->value)) {
                                $temp_value = $temp->value;
                            }elseif(is_array($temp->value)) {
                                foreach ($temp->value as $temp_arr_val){
                                    $temp_value .= $temp_arr_val.";";
                                }
                            }

                            $result .= "<tr><td>{$temp->label}</td><td>{$temp_value}</td></tr>";
                        }
                    }
                    /*

                    if(is_object($arr->value)){
                        $my_val = $this->creteHTMLTab($arr->value);
                    }
                    */


                   // $my_val = var_export($arrVal, 1);
                }



                if((string)$key != 'project' && isset($label) && isset($my_val)){
                    $result .= "<tr><td>{$label}</td><td>{$my_val}</td></tr>";
                }

            }


        }

        $result .= "</table>";
        $res->html = $result;

        //dd($res);

        return $res;
    }


    public function getTegsTitle($teg)
    {
        $name = '';
        if($teg == 'age') $name = 'Возраст';
        if($teg == 'gender') $name = 'Пол';
        if($teg == 'experience') $name = 'Стаж';

        return $name;
    }

}