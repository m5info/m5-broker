<?php

namespace App\Services\FNS;


use App\Models\Contracts\Contracts;
use App\Models\Contracts\FNS;
use App\Models\Contracts\Payments;
use App\Models\Settings\SettingsSystem;
use App\Services\Front\api\KansaltingRIT_V2_1;
use Faker\Provider\Payment;
use GuzzleHttp\Client;
use Illuminate\Http\Response;

class FNSFerma
{

    private $Urls = "https://ferma.ofd.ru/api/";
    private $Login = "fermatest1";
    private $Password = "Hjsf3321klsadfAA";
    private $Token = "1c678291-d985-4807-8bea-3b296c54e986";

    public function __construct($organization)
    {

        //ОПРЕДЕЛЯЕМ НАСТРОЙКИ В ЗАВИСИМОСТИ ОТ ООО

        $this->Login = $organization->cashbox_api_login;
        $this->Password = $organization->cashbox_api_pass;

        //$this->Token = $organization->cashbox_api_token;

        //АВТТОРИЗОВЫВАЕМСЯ
        $this->auth();
    }




    public function send($method, $array, $type = "POST")
    {

        $json = \GuzzleHttp\json_encode($array);

        $headers = [];

        //$headers[] = 'Type: Auto';
        //$headers[] = 'Service: AutoCod';

        $headers[] = 'Content-type: application/json; charset=utf-8';
        $headers[] = 'Content-Length: '.strlen($json);



        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->Urls.$method.'?AuthToken='.$this->Token,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //Логируем ошибки
            return null;
        }



        return \GuzzleHttp\json_decode($response);
    }


    private function auth()
    {
        $array = ['Login' => $this->Login, 'Password' => $this->Password];
        $response = $this->send('/Authorization/CreateAuthToken', $array);
        if((string)$response->Status == 'Success'){
            $this->Token = (string)$response->Data->AuthToken;

            return true;
        }
        return false;
    }


    const PaymentTypeType = [
        0 => 'наличными',
        1 => 'безналичными',
        2 => 'предварительная оплата (аванс)',
        3 => 'предварительная оплата (кредит)',
        4 => 'иная форма оплаты',
    ];

    const PaymentMethodType = [
        1 => 'предоплата 100%',
        2 => 'предоплата',
        3 => 'аванс',
        4 => 'полный расчет',
        5 => 'частичный расчет',
        6 => 'передача в кредит',
        7 => 'оплата в кредит',
    ];

    const RequestType = [
        1 => 'Income', //получение денежных средств от покупателя;
        2 => 'IncomeReturn', //возврат денежных средств, полученных от покупателя
        3 => 'IncomePrepayment', //авансовый платеж от покупателя
        4 => 'IncomeReturnPrepayment', //возврат аванса
        5 => 'IncomeCorrection', //чек коррекции/приход
        6 => 'BuyCorrection', //чек коррекции/расход
        7 => 'Expense', //выдача денежных средств покупателю
        8 => 'ExpenseReturn', //возврат денежных средств, выданных покупателю
    ];



    const STATUS = [
        '' => 'Создан',
        'Success' => 'Выпущен',
        'Failed' => 'Ошибка',

    ];





    const NDSType = [
        1 => 'Vat10', //налог на добавленную стоимость (НДС) 10%;
        2 => 'Vat18', //НДС 18%
        3 => 'Vat20', //НДС 20%
        4 => 'Vat0', //НДС 0%
        5 => 'VatNo', //НДС не облагается
        6 => 'CalculatedVat10110', //вычисленный НДС 10% от 110% суммы
        7 => 'CalculatedVat18118', //вычисленный НДС 18% от 118% суммы
        8 => 'CalculatedVat20120', //вычисленный НДС 20% от 120% суммы
    ];

    const TaxationSystemType = [
        1 => 'Common', //общая система налогообложения
        2 => 'SimpleIn', //упрощенная система налогообложения (доход)
        3 => 'SimpleInOut', //упрощенная  система  налогообложения  (доход  минус расход)
        4 => 'Unified', //единый налог на вмененный доход
        5 => 'UnifiedAgricultural', //единый сельскохозяйственный налог
        6 => 'Patent', //патентная система налогообложения
    ];


    public function createContractKKT(Payments $payment)
    {

        $contract = $payment->contract;

        $Price = $payment->payment_total;//Цена товарной позиции, в рублях
        $TypeId = 1;//получение денежных средств от покупателя;
        $PaymentMethod = 1;//Признак способа расчета
        $PaymentType = $payment->payment_type;//Тип оплаты


        ///////////////////////////////
        ///
        /// Данные по ООО
        ///
        ///////////////////////////////
        $org = $contract->bso_supplier->purpose_org;

        $Inn = $org->inn;//ИНН лица, от имени которого генерируется кассовый документ (чек)
        $TaxationSystem = self::TaxationSystemType[1];//Система налогообложения
        $NDS = self::NDSType[5];//Вид вычисляемого НДС

        ///////////////////////////////
        ///
        /// Данные по Клиенту
        ///
        ///////////////////////////////
        $insurer = $contract->insurer;

        $InnInsurer = $insurer->inn;//ИНН лица, от имени которого генерируется кассовый документ (чек)
        $NameInsurer = $insurer->title;
        if($insurer->type == 0){
            $NameInsurer .= " {$insurer->doc_serie} {$insurer->doc_number}";
        }

        ///////////////////////////////

        $fns = FNS::create([
            'contract_id' => $contract->id,
            'payment_id' => $payment->id,
            'create_date' => getDateTime(),
        ]);

        $array = [
            'Request' => [
                "Inn" => $Inn, //ИНН лица, от имени которого генерируется кассовый документ (чек)
                "Type" => self::RequestType[$TypeId], //Тип формируемого документа (чек), RequestType
                "InvoiceId" => $payment->id, //Идентификатор счета, на основании которого генерируется чек
                "LocalDate" => date('Y-m-d\TH:i:s'), //Локальная дата и время чека
                "CustomerReceipt" => [ //Содержимое клиентского чека
                    "TaxationSystem" => $TaxationSystem, //Система налогообложения
                    "Email" => "narf-narf@yandex.ru", //Адрес электронной почты клиента
                    "Phone" => "",//Контактный телефон клиента
                    "PaymentType" => $PaymentType, //Признак предмета расчета для всего чека
                    "CustomUserProperty" => [ //Дополнительный реквизит пользователя с учетом особенностей сферы деятельности, в которой осуществляются расчеты, тег 1084
                        "Name" => "", //Наименование дополнительного реквизита пользователя с учетом особенностей сферы деятельности, в которой осуществляются расчеты, тег 1085
                        "Value" => "" //Значение дополнительного реквизита пользователя с учетом особенностей сферы деятельности, в которой осуществляются расчеты, тег 1086
                    ],
                ],
                "ClientInfo" => [ //Данные о покупателе
                    "Name" => $NameInsurer, //ФИО и паспортные данные покупателя или наименование организации, если клиент юр. лицо. Не более 256 символов в поле
                    "Inn" => $InnInsurer //ИНН покупателя. Длина 10-12 цифр
                ],
                "Items" => [ //Товарные позиции, приобретаемые клиентом
                    "Label" => "{$contract->product->title} {$contract->bso_title}", //Название товарной позиции
                    "Price" => $Price, //Цена товарной позиции, в рублях
                    "Quantity" => 1, //Количество товара в товарной позиции
                    "Amount" => $Price, //Общая стоимость товара в товарной позиции в рублях
                    "Vat" => $NDS, //Вид вычисляемого НДС

                ],
                "PaymentMethod" => $PaymentMethod, //Признак способа расчета
                "PaymentType" => $PaymentType, //Тип оплаты
                "PaymentItems" => [
                    "PaymentType" => $PaymentType,
                    "Sum" => $Price
                ]
            ]
        ];

        dd($array);

        $fns->request_json = \GuzzleHttp\json_encode($array);
        $fns->save();

        echo  \GuzzleHttp\json_encode($array);

        //dd($fns->request_json);

        $response = $this->send('/kkt/cloud/receipt', $array);
        $fns->status = (string)$response->Status;
        $fns->response_json = \GuzzleHttp\json_encode($response);
        $fns->save();

        dd($response);

    }



    public function sendDataKKT($ofd)
    {

        ///////////////////////////////
        ///
        /// Данные по ООО
        ///
        ///////////////////////////////
        $org = $ofd->organization;

        $Inn = $org->inn;//ИНН лица, от имени которого генерируется кассовый документ (чек)
        $TaxationSystem = self::TaxationSystemType[$org->taxation_system_type_id];//Система налогообложения
        $NDS = self::NDSType[$org->nds_type_id];//Вид вычисляемого НДС

        ///////////////////////////////
        ///
        /// Данные по Клиенту
        ///
        ///////////////////////////////

        $InnInsurer = $ofd->client_inn;//ИНН лица, от имени которого генерируется кассовый документ (чек)
        $NameInsurer = $ofd->client_title;
        if($ofd->client_type_id == 0){
            $NameInsurer .= " {$ofd->client_doc_serie} {$ofd->client_doc_number}";
        }

        ///////////////////////////////
        ///
        /// Данные по платежу
        ///
        ///////////////////////////////

        $Price = $ofd->payment_total;//Цена товарной позиции, в рублях
        $TypeId = $ofd->type_id;//получение денежных средств от покупателя;
        $PaymentMethod = $ofd->payment_method_id;//Признак способа расчета
        $PaymentType = $ofd->payment_type;//Тип оплаты


        $Items = new \stdClass();
        $Items->Label = "{$ofd->product_title}";
        $Items->Price = $Price;
        $Items->Quantity = 1;
        $Items->Amount = $Price;
        $Items->Vat = $NDS;
        $Items->PaymentMethod = $PaymentMethod;
        $Items->PaymentType = $PaymentType;


        /*
         [ //Товарные позиции, приобретаемые клиентом
                    "Label" => "{$ofd->product_title}", //Название товарной позиции
                    "Price" => $Price, //Цена товарной позиции, в рублях
                    "Quantity" => '1.00', //Количество товара в товарной позиции
                    "Amount" => $Price, //Общая стоимость товара в товарной позиции в рублях
                    "Vat" => $NDS, //Вид вычисляемого НДС
                    "PaymentMethod" => $PaymentMethod, //Признак способа расчета
                    "PaymentType" => $PaymentType //Тип оплаты
                ],


        $array = [
            'Request' => [
                "Inn" => $Inn, //ИНН лица, от имени которого генерируется кассовый документ (чек)
                "Type" => self::RequestType[$TypeId], //Тип формируемого документа (чек), RequestType
                "InvoiceId" => $ofd->id, //Идентификатор счета, на основании которого генерируется чек
                "LocalDate" => date('Y-m-d\TH:i:s', strtotime($ofd->create_date)), //Локальная дата и время чека
                "CustomerReceipt" => [ //Содержимое клиентского чека
                    "TaxationSystem" => $TaxationSystem, //Система налогообложения
                    "Email" => "narf-narf@yandex.ru", //Адрес электронной почты клиента
                    "Phone" => "",//Контактный телефон клиента
                    "PaymentType" => $PaymentType, //Признак предмета расчета для всего чека
                    "CustomUserProperty" => [ //Дополнительный реквизит пользователя с учетом особенностей сферы деятельности, в которой осуществляются расчеты, тег 1084
                        "Name" => "", //Наименование дополнительного реквизита пользователя с учетом особенностей сферы деятельности, в которой осуществляются расчеты, тег 1085
                        "Value" => "" //Значение дополнительного реквизита пользователя с учетом особенностей сферы деятельности, в которой осуществляются расчеты, тег 1086
                    ],
                ],
                "ClientInfo" => [ //Данные о покупателе
                    "Name" => $NameInsurer, //ФИО и паспортные данные покупателя или наименование организации, если клиент юр. лицо. Не более 256 символов в поле
                    "Inn" => $InnInsurer //ИНН покупателя. Длина 10-12 цифр
                ],
                "Items" => [$Items,
                    "PaymentMethod" => $PaymentMethod, //Признак способа расчета
                    "PaymentType" => $PaymentType //Тип оплаты
                ],
                "PaymentItems" => [
                    "PaymentType" => $PaymentType,
                    "Sum" => $Price
                ]
            ]
        ];

         "CustomUserProperty" => [ //Дополнительный реквизит пользователя с учетом особенностей сферы деятельности, в которой осуществляются расчеты, тег 1084
                        "Name" => "", //Наименование дополнительного реквизита пользователя с учетом особенностей сферы деятельности, в которой осуществляются расчеты, тег 1085
                        "Value" => "" //Значение дополнительного реквизита пользователя с учетом особенностей сферы деятельности, в которой осуществляются расчеты, тег 1086
                    ],
                    "ClientInfo" => [ //Данные о покупателе
                        "Name" => $NameInsurer, //ФИО и паспортные данные покупателя или наименование организации, если клиент юр. лицо. Не более 256 символов в поле
                        "Inn" => $InnInsurer //ИНН покупателя. Длина 10-12 цифр
                    ],
         */


        $array = [
            'Request' => [
                "Inn" => $Inn, //ИНН лица, от имени которого генерируется кассовый документ (чек)
                "Type" => self::RequestType[$TypeId], //Тип формируемого документа (чек), RequestType
                "InvoiceId" => $ofd->id, //Идентификатор счета, на основании которого генерируется чек
                "LocalDate" => date('Y-m-d\TH:i:s', strtotime($ofd->create_date)), //Локальная дата и время чека
                "CustomerReceipt" => [ //Содержимое клиентского чека
                    "TaxationSystem" => $TaxationSystem, //Система налогообложения
                    "Email" => $ofd->client_email, //Адрес электронной почты клиента
                    "Phone" => parsePhoneNumber($ofd->client_phone),//Контактный телефон клиента
                    "Items" => [$Items],

                ],
                "PaymentItems" => [
                    "PaymentType" => $PaymentType,
                    "Sum" => $Price
                ],


            ]
        ];



        $ofd->request_json = \GuzzleHttp\json_encode($array);
        $ofd->save();

       // echo  \GuzzleHttp\json_encode($array);

        //dd($fns->request_json);

        $response = $this->send('/kkt/cloud/receipt', $array);
        $ofd->status = (string)$response->Status;
        $ofd->response_json = \GuzzleHttp\json_encode($response);
        $ofd->save();

        //dd($response);

        if($ofd->status == 'Success'){

            $ofd->receipt_id = (string)$response->Data->ReceiptId;
            $ofd->save();

            $dataOFD = $this->checkDataKKT($ofd->receipt_id);
            $ofd->ofd_json = \GuzzleHttp\json_encode($dataOFD);
            $ofd->save();

        }

        return true;

    }


    public function checkDataKKT($ReceiptId)
    {

        $array = [
            'Request' => [
                "ReceiptId" => $ReceiptId,
            ]
        ];

        $response = $this->send('/kkt/cloud/list', $array);
        if(isset($response->Data[0]->StatusCode)){
            return $response;
        }

        return $this->checkDataKKT($ReceiptId);
    }

}