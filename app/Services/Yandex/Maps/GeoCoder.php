<?php

namespace App\Services\Yandex\Maps;

use Exception;
use GuzzleHttp\Client;

class GeoCoder
{
    protected $url = 'https://geocode-maps.yandex.ru/1.x?';

    protected $apikey = '6f560f00-8711-41e0-aac9-456e2609c317';


    public function getAddressCoordinates($address)
    {
        if (!$address) {
            throw new Exception('Некорректный адрес');
        }

        $response = $this->getResponse($address);

        return $this->formatResponse($response);

    }

    public function getResponse($address)
    {
        $params = [
            'geocode' => $address,
            'format'  => 'json',
            'apikey'  => $this->apikey,
            'results' => 1,
        ];

        $url = $this->url . http_build_query($params, '', '&');

        return (new Client)->get($url)->getBody()->getContents();

    }

    public function formatResponse($response)
    {
        $result = json_decode($response);

        dd($result);


        if (!$result->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found) {
            throw new Exception('Адрес не найден');
        }

        $coordinatesString = $result->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;

        $coordinatesArray = explode(' ', $coordinatesString);

        return [
            'longitude' => $coordinatesArray[0],
            'latitude'  => $coordinatesArray[1],
        ];

    }


    public function getCoordinatesAddress($latlong, $longlat)
    {
        $params = [
            'geocode' => "[$longlat, $latlong]",
            'format'  => 'json',
            'apikey'  => $this->apikey,
            'results' => 1,
        ];

        $url = $this->url . http_build_query($params, '', '&');

        $response = (new Client)->get($url)->getBody()->getContents();
        $result = json_decode($response);

        dd($result);



    }


}