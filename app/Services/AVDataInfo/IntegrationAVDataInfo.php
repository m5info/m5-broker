<?php

namespace App\Services\AVDataInfo;


use App\Models\Settings\SettingsSystem;
use App\Services\Front\api\KansaltingRIT_V2_1;
use GuzzleHttp\Client;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Response;

class IntegrationAVDataInfo
{

    private $urls = "https://data.av100.ru/";
    private $token = "";

    public function __construct()
    {
        $this->urls = SettingsSystem::getDataParam('av100', 'url');
        $this->token = SettingsSystem::getDataParam('av100', 'token');
    }

    public function sendGet($method, $data)
    {
        $url = "{$this->urls}{$method}.ashx?key={$this->token}&{$data}";
        return json_decode(file_get_contents($url));
    }

    public function getApiStats()
    {
        //$val = urlencode($val);
        $response = $this->sendGet('profile', "");
        return $response;
    }

    public function getApiPrice($mark, $model, $year)
    {
        //$val = urlencode($val);
        $response = $this->sendGet('carprices', "method=prices&marka={$mark}&model={$model}&year={$year}");
        return $response;
    }

    public function getApiGosNomerReporst($val)
    {
        $val = urlencode($val);
        $response = $this->sendGet('api', "gosnomer={$val}");
        return $response;
    }

    public function getApiVINReporst($val)
    {
        $val = urlencode($val);
        $response = $this->sendGet('api', "vin={$val}");
        return $response;
    }

    public function getParkonGosNomerReporst($val)
    {
        $val = urlencode($val);
        $response = $this->sendGet('parkon', "gosnumber={$val}");
        return $response;
    }

    public function getFullVINNumberReporst($val)
    {
        $response = $this->sendGet('vinbynumber', "number={$val}");
        dd($response);

    }


    public static function getPhoneStr($vin, $gosnumber)
    {
        $result = '';

        $av = new IntegrationAVDataInfo();

        if(strlen($vin)>3)
        {
            $report = $av->getApiVINReporst($vin);
        }elseif(strlen($gosnumber)>3){
            $report = $av->getApiGosNomerReporst($gosnumber);

        }

        if(isset($report->result) && isset($report->result->gibdd) && isset($report->result->gibdd[0])){
            $result = (string)$report->result->gibdd[0]->phone;
        }else{
            //dd($report);
        }




        return $result;
    }

    public static function getPhoneParkon($gosnumber)
    {
        $result = '';
        $av = new IntegrationAVDataInfo();
        if(strlen($gosnumber)>3){
            $report = $av->getParkonGosNomerReporst($gosnumber);
            if(isset($report->result) && isset($report->result[0]) && isset($report->result[0]->phone)){
                $result = (string)$report->result[0]->phone;
                if(isset($report->result[1]) && isset($report->result[1]->phone)){
                    $result .= ', '.$report->result[1]->phone;
                }
            }
        }

        return $result;
    }

}