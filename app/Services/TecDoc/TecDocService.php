<?php

namespace App\Services\TecDoc;


use App\Services\TecDoc\Service\TecDocSend;

class TecDocService{


    public function __construct(){

        //http://tecdoc3.api.abcp.ru/
        //adaptabilityManufacturers?
        //userlogin=api@id21137
        //&userpsw=423422a74e138fb64d0150221926a76c
        //&userkey=5460625&number=01089
        //&brandName=febi

    }

    //Поиск автомобиля по VIN
    public function getVehiclesByVIN($vin)
    {
        return TecDocSend::get('getVehiclesByVIN', ['vin'=>$vin]);
    }

    //Получение справочника производителей
    public function manufacturers($carType, $motorcyclesFilter)
    {
        //$carType = тип автомобилей (необязательный параметр): "1" - легковые, "2" - грузовые, "3" - Малотоннажные, "4" - Моторы;
        //$motorcyclesFilter = фильтрация по производителя: "0" - не фильтровать (по умолчанию), "1" - только производители авто, "2" - только мотоциклы;
        return TecDocSend::get('manufacturers', ['carType' => $carType, 'motorcyclesFilter' => $motorcyclesFilter]);
    }

    //Получение списка моделей
    public function models($manufacturerId, $carType = null)
    {
        //$carType = тип автомобилей (необязательный параметр, по умолчанию все): "1" - легковые, "2" - грузовые, "3" - Малотоннажные
        //$manufacturerId = идентификатор производителя
        $param = ['manufacturerId' => $manufacturerId];
        if($carType) $param['carType'] = $carType;

        return TecDocSend::get('models', $param);
    }

    //Получение списка модификаций
    public function modifications($manufacturerId, $modelId, $carType = null)
    {
        //$carType = тип автомобилей (необязательный параметр, по умолчанию все): "1" - легковые, "2" - грузовые
        //$manufacturerId = идентификатор производителя
        //$modelId = идентификатор модели

        $param = ['manufacturerId' => $manufacturerId, 'modelId' => $modelId];
        if($carType) $param['carType'] = $carType;

        return TecDocSend::get('modifications', $param);
    }

    //Получение модификации по идентификатору
    public function modification($modelVariant)
    {
        //modelVariant идентификатор модификации
        return TecDocSend::get('modification', ['modelVariant'=>$modelVariant]);
    }

    //Получение дерева категорий
    public function tree($manufacturerId, $carType = null)
    {
        //$carType = тип автомобилей (необязательный параметр): "1" - легковые (по умолчанию), "2" - грузовые, "3" - Малотоннажные, "4" - Моторы
        //$manufacturerId идентификатор модификации
        $param = ['manufacturerId' => $manufacturerId];
        if($carType) $param['carType'] = $carType;

        return TecDocSend::get('tree', $param);
    }

    //Получение списка деталей для категории
    public function articlesSimplified($categoryId, $modificationId)
    {
        //$categoryId = идентификатор категории
        //$modificationId = идентификатор модификации
        return TecDocSend::get('articlesSimplified', ['categoryId' => $categoryId, 'modificationId' => $modificationId]);
    }


    //Получение списка применимости проходит
    /*
        Получение списка применимости проходит в три этапа,
        первый этап получение производителей adaptabilityManufacturers
        затем adaptabilityModels
        и в завершении adaptabilityModifications операция получения модификаций.
     */


    public function adaptabilityManufacturers($brandName, $number)
    {
        return TecDocSend::get('adaptabilityManufacturers', ['brandName'=>$brandName, 'number'=>$number]);
    }

    public function adaptabilityModels($brandName, $number, $manufacturerName)
    {
        return TecDocSend::get('adaptabilityManufacturers', ['brandName'=>$brandName, 'number'=>$number, 'manufacturerName'=>$manufacturerName]);
    }

    public function adaptabilityModifications($brandName, $number, $manufacturerName, $modelName)
    {
        return TecDocSend::get('adaptabilityManufacturers', ['brandName'=>$brandName, 'number'=>$number, 'manufacturerName'=>$manufacturerName, 'modelName'=>$modelName]);
    }



    //Получение списка брендов
    public function brands()
    {
        return TecDocSend::get('brands');
    }


    //Получение списка настоящих номеров
    public function realNumberArticles($articleId)
    {
        return TecDocSend::get('realNumberArticles', ['articleId'=>$articleId]);
    }

    //Получение связанных атрибутов детали
    public function assignedArticleAttributes($manufacturerId, $modelId, $modificationId, $articleIdPairs)
    {
        //$articleIdPairs пары из идентификаторов детали и связанной детали в виде ассоциативного массива с ключами articleId и articleLinkId соответственно
    }

}