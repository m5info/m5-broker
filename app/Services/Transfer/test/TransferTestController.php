<?php

namespace App\Services\Transfer\test;

use App\Models\Directories\BsoSuppliers;

class TransferTestController{





    public function __construct()
    {


    }


    public static function test()
    {

        $client = new \SoapClient("http://192.168.2.57:8000/api/elt-poisk?wsdl", array(
            'cache_wsdl' => WSDL_CACHE_NONE,
            'trace' => 1,
            'exception' => 1,
        ));

        //dd($client->__getFunctions());

        $xml = <<<XML
<data>
  <AuthInfo>
    <Login>elt-poisk</Login>
    <Password>pass1234</Password>
    <SessionId>0</SessionId>
  </AuthInfo>
  <InsuranceCompany>EKAT_66</InsuranceCompany>
  <ProgramId/>
  <Params>
    <IsNew>1</IsNew>
    <UsageStart>2019-10-03T00:00:00</UsageStart>
    <VehicleYear>2019</VehicleYear>
    <Mark>AC</Mark>
    <Model>ACE</Model>
    <Modification>
      <Power>145</Power>
      <EngineType/>
      <EngineVolume>2000</EngineVolume>
      <KPPTypeId>1</KPPTypeId>
      <BodyType>0</BodyType>
      <DoorsCount/>
    </Modification>
    <Duration>12</Duration>
    <BankId>0</BankId>
    <Cost>1500000</Cost>
    <Franchise/>
    <SSType>1</SSType>
    <STOA>0</STOA>
    <GAP/>
    <OfficialDealer>1</OfficialDealer>
    <Region/>
    <DriversCount>1</DriversCount>
    <Risk>0</Risk>
    <PUUs/>
    <Drivers>
      <Driver>
        <Age>30</Age>
        <Experience>11</Experience>
        <Sex>0</Sex>
        <Childrens>0</Childrens>
        <MarriageStatus>4</MarriageStatus>
        <IsOwner>1</IsOwner>
      </Driver>
    </Drivers>
    <GO>
      <LimitSumId>0</LimitSumId>
      <GlobalLimitSum/>
    </GO>
    <NS/>
    <DO/>
    <Options>
      <Option>
        <Id>FRANCHISE_TYPE</Id>
        <Name>Тип франшизы</Name>
        <Values>
          <OptionValue>
            <Id/>
          </OptionValue>
        </Values>
      </Option>
      <Option>
        <Id>PAY_TYPE_ID</Id>
        <Name>Порядок оплаты</Name>
        <Values>
          <OptionValue>
            <Id>0</Id>
            <Name>Единовременно</Name>
          </OptionValue>
        </Values>
      </Option>
      <Option>
        <Id>ESCORT</Id>
        <Name>Программа сопровождения</Name>
        <Values>
          <OptionValue>
            <Id>0</Id>
          </OptionValue>
        </Values>
      </Option>
    </Options>
    <Commissar>0</Commissar>
    <Discount>0</Discount>
    <Discount2>0</Discount2>
    <CarDecoration>0</CarDecoration>
    <MachineryWear>0</MachineryWear>
  </Params>
</data>
XML;




        $soapBody = new \SoapVar($xml, XSD_ANYXML);


        try
        {
            $response = $client->PreliminaryKASKOCalculation($soapBody);
            //dump($client->__getLastResponse());

            dd($response);


        }
        catch(\SoapFault $e)
        {



            echo "ERROR|";
            dump($client->__getLastRequestHeaders());
            dump($client->__getLastRequest());

            //echo var_export( $e , 1);
            dd($e);
            return null;
        }

        dd($response);


    }


}