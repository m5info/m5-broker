<?php

namespace App\Services\Transfer\M5_v1\Users;


use App\Models\Contracts\Subjects;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Organizations\Organization;
use App\Models\Settings\PointsSale;
use App\Models\Subject\Juridical;
use App\Models\Subject\Physical;
use App\Models\User;
use App\Models\Users\UsersBalance;
use App\Services\Transfer\M5_v1\Send;

class M5Users{



    public function __construct()
    {


    }

    public function getUserLest()
    {
        return User::all()->last()->id;
    }


    public function update()
    {
        $response = Send::getInfo("SELECT * FROM users where id_user >".$this->getUserLest()." limit 100");

        if($response){
            foreach ($response as $info){


                $role_id = 7;
                $organization_id = 12;

                $is_parent = $info->leader;
                if($is_parent > 0){
                    $role_id = 4;
                }

                $password = bcrypt(trim($info->user_password));
                $subject = $this->getUserSubject($info);


                $data = User::create([
                    'id' => $info->id_user,
                    'name' => $info->user_title,
                    'email' => $info->user_login,
                    'password' => $password,
                    'subject_type_id' => 1,
                    'subject_id' => $subject->id,
                    'role_id' => $role_id,
                    'organization_id' => $organization_id,
                    'is_parent' => $is_parent,
                    'parent_id' => $info->pid,
                    'financial_group_id' => 0,
                    'department_id' => $info->filial_sale_id,
                    'ban_level' => $info->ban_level,
                    'ban_reason' => $info->ban_reason,
                    'point_sale_id' => $info->sale_channel_id,
                    'settings' => '',
                ]);

                $data->id = $info->id_user;
                $data->save();

                $this->updateUserBalance($data, 1, $info->balance);
                $this->updateUserBalance($data, 2, $info->balance_reserv);
                $this->updateUserBalance($data, 4, $info->balance_bn);
                $this->updateUserBalance($data, 3, $info->balance_sk);

                //dd($info, $data);

            }

            //dd($response);
            return $this->update();

        }else{
            return true;
        }
    }


    public function getUserSubject($info)
    {
        return Physical::create([
            'first_name' => $info->firstname,
            'second_name' => $info->surname,
            'middle_name' => $info->middlename,
        ]);
    }


    public function updateUserBalance($user, $balance_id, $sum)
    {

        if(getFloatFormat($sum) != 0){
            $balance = $user->getBalance($balance_id);
            $balance->setTransactions(
                0, 0, getFloatFormat($sum), date('Y-m-d H:i:s'), "Перенос баланса из старой базы!"
            );

        }

    }



}