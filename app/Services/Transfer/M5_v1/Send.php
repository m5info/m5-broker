<?php

namespace App\Services\Transfer\M5_v1;

use App\Models\Directories\BsoSuppliers;

class Send{



    public function __construct()
    {


    }


    public static function getInfo($sql)
    {
        $url = 'http://ph5b.in2007.ru/test/export_info.php';

        $curl = curl_init();
        $headers = [];

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => 'sql='.$sql,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);



        curl_close($curl);

        if ($err) {

            dd($err);

            //Логируем ошибки
            return null;
        }


        return \GuzzleHttp\json_decode($response);
    }




}