<?php

namespace App\Services\Transfer\M5_v1\Settings;


use App\Models\Organizations\Organization;
use App\Services\Transfer\M5_v1\Send;

class Organizations{



    public function __construct()
    {


    }


    public function updateOrganizations()
    {
        $response = Send::getInfo("SELECT * FROM organizations");

        if($response){
            foreach ($response as $org_info){
                Organization::create([
                    'id' => $org_info->id,
                    'title' => $org_info->title,
                    'is_actual' => $org_info->is_active,
                    'org_type_id' => 1,
                ]);
            }

            //dd($response);
            return true;

        }else{
            return true;
        }
    }




}