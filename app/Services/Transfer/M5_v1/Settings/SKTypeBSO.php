<?php

namespace App\Services\Transfer\M5_v1\Settings;


use App\Models\Directories\BsoDopSerie;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\TypeBso;
use App\Models\Organizations\Organization;
use App\Models\Settings\PointsSale;
use App\Services\Transfer\M5_v1\Send;

class SKTypeBSO{



    public function __construct()
    {


    }


    public function update()
    {

        //$this->updateType();
        //$this->updateSerie();
        $this->updateDopSerie();


        /*
        $sks = InsuranceCompanies::all();
        foreach ($sks as $sk){
            $this->updateType($sk);
            return true;
        }

        /*
        $response = Send::getInfo("SELECT * FROM sk");

        if($response){
            foreach ($response as $info){

                //dd($info);

                InsuranceCompanies::create([
                    'id' => $info->id,
                    'title' => $info->title,
                    'is_actual' => $info->is_active,
                ]);

            }

            //dd($response);
            return true;

        }else{

        }
        */
        return true;
    }


    public function updateType()
    {
        $response = Send::getInfo("SELECT * FROM bso_types");
        if($response){
            foreach ($response as $info){


                $bso_type = TypeBso::create([
                    'id' => $info->id,
                    'insurance_companies_id' => $info->id_sk,
                    'title' => $info->title,
                    'is_actual' => $info->is_actual,
                    'product_id' => $info->bso_class,
                    'min_yellow' => $info->min_yellow,
                    'min_red' => $info->min_red,
                    'day_sk' => $info->day_sk,
                    'day_agent' => $info->day_agent,
                ]);

                $bso_type->id = $info->id;
                $bso_type->save();

                //dd($info, $bso_type);

            }

            //dd($response);
            return true;

        }
        return true;
    }


    public function updateSerie()
    {
        $response = Send::getInfo("SELECT * FROM bso_series where pid = 0");
        if($response){
            foreach ($response as $info){

                $bso_type = TypeBso::find($info->bso_type_id);

                if(!$bso_type){
                    dd($info);
                }

                $bso_class_id = 0;
                //Бумажный 0
                //Электронный 1
                //Квитанция 100

                if(strlen($bso_type->title) > 2){
                    if(strripos($bso_type->title, 'Е-') !== false){
                        $bso_class_id = 1;
                    }

                    if(strripos($bso_type->title, 'Кви') !== false){
                        $bso_class_id = 100;
                    }
                }


                 $ser = BsoSerie::create([
                     'id' => $info->id,
                     'type_bso_id' => $info->bso_type_id,
                     'bso_serie' => $info->title,

                     'product_id' => $bso_type->product_id,
                     'insurance_companies_id' => $bso_type->insurance_companies_id,
                     'bso_class_id' => $bso_class_id,
                ]);

                $ser->id = $info->id;
                $ser->save();

                //dd($info, $bso_type);

            }

            //dd($response);
            return true;

        }

    }

    public function updateDopSerie(){

        $response = Send::getInfo("SELECT * FROM bso_series where pid > 0");
        if($response) {
            foreach ($response as $info) {

                $ser = BsoSerie::find($info->pid);
                if($ser){

                    //dd($info, $ser);

                    $dop = BsoDopSerie::create([
                        'id' => $info->id,
                        'type_bso_id' => $ser->type_bso_id,
                        'bso_dop_serie' => $info->title,
                        'bso_serie_id' => $ser->id,
                        'insurance_companies_id' => $ser->insurance_companies_id,
                    ]);

                    $dop->id = $info->id;
                    $dop->save();
                }


            }
        }
        return true;
    }

}