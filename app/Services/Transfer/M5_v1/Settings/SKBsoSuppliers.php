<?php

namespace App\Services\Transfer\M5_v1\Settings;


use App\Models\Directories\BsoDopSerie;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\FinancialPolicyGroup;
use App\Models\Directories\HoldKv;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\TypeBso;
use App\Models\Organizations\Organization;
use App\Models\Settings\PointsSale;
use App\Services\Transfer\M5_v1\Send;

class SKBsoSuppliers{



    public function __construct()
    {


    }


    public function update()
    {

        $this->finPolitics();



        return true;
    }

    public function bsoSuppliers()
    {


        $response = Send::getInfo("SELECT * FROM sk_bso_suppliers");

        if($response){
            foreach ($response as $info){

                //dd($info);

                $data = BsoSuppliers::create([
                    'id' => $info->id,
                    'insurance_companies_id' => $info->sk_id,
                    'title' => $info->title,
                    'signer' => $info->signatory,
                    'source_org_id' => $info->org_id_source,
                    'purpose_org_id' => $info->org_id_dest,
                    'city_id' => 1,
                    'is_actual' => $info->is_active,
                ]);

                $data->id = $info->id;
                $data->save();



            }


            return true;

        }
    }

    public function bsoSuppliersProduct()
    {
        $response = Send::getInfo("SELECT * FROM sk_bso_suppliers_deduction");

        if($response){
            foreach ($response as $info){

                $bso = BsoSuppliers::find($info->sk_bso_supplier_id);

                //dd($info, $bso);

                if($bso){
                    $data = HoldKv::create([
                        'id' => $info->id,
                        'insurance_companies_id' => $bso->insurance_companies_id,
                        'bso_supplier_id' => $info->sk_bso_supplier_id,
                        'product_id' => $info->product_id,
                        'hold_type_id' => $info->type_id,
                        'is_check_policy' => $info->check_policy,
                        'is_many_files' => $info->many_files,

                    ]);

                    $data->id = $info->id;
                    $data->save();
                }




            }


            return true;

        }
    }

    public function finPolitics()
    {

        $response = Send::getInfo("SELECT * FROM fin_politics");

        if($response){
            foreach ($response as $info){

                $bso = BsoSuppliers::find($info->sk_id);

                //dd($info);

                    $data = FinancialPolicy::create([
                        'id' => $info->id,
                        'bso_supplier_id' => $bso->id,
                        'insurance_companies_id' => $bso->insurance_companies_id,
                        'title' => $info->title,
                        'is_actual' => $info->is_actual,
                        'date_active' => $info->date_create,
                        'product_id' => $info->product_id,
                        'kv_bordereau' => $info->kv_bordereau,
                        'kv_dvou' => $info->kv_dvou,
                        'kv_sk' => $info->kv_sk,
                        'kv_agent' => $info->kv_agent,
                        'kv_parent' => 0,
                        'partnership_reward_1' => 0,
                        'partnership_reward_2' => 0,
                        'partnership_reward_3' => 0,

                    ]);

                    $data->id = $info->id;
                    $data->save();

                    $this->finPoliticsGroap($data, $info->kv_manager);
                    //dd($data);




            }


            return true;

        }

    }


    public function finPoliticsGroap($fp, $kv_manager)
    {
        FinancialPolicyGroup::where('financial_policy_id', $fp->id)->delete();

        FinancialPolicyGroup::create([
            'financial_policy_id' => $fp->id,
            'financial_policies_group_id' => 13,
            'is_actual' => $fp->is_actual,
            'kv_agent' => $kv_manager,
            'kv_parent' => 0,
        ]);

        $response = Send::getInfo("SELECT * FROM finpolitics_groups_relationship where fin_politic_id={$fp->id}");

        if($response) {
            foreach ($response as $info) {
                FinancialPolicyGroup::create([
                    'financial_policy_id' => $fp->id,
                    'financial_policies_group_id' => $info->finpolitic_group_id,
                    'is_actual' => $info->is_actual,
                    'kv_agent' => $info->kv_agent_manager,
                    'kv_parent' => 0,
                ]);

            }
        }



        return true;
    }


}