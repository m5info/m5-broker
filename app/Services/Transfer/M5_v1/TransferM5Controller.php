<?php

namespace App\Services\Transfer\M5_v1;

use App\Models\Directories\BsoSuppliers;

class TransferM5Controller{





    public function __construct()
    {


    }

    // Settings

    public static function getOrganizations()
    {
        $sett = new Settings\Organizations();
        return $sett->updateOrganizations();
    }

    public static function getPoints()
    {
        $sett = new Settings\Points();
        return $sett->update();
    }

    public static function getProducts()
    {
        $sett = new Settings\Products();
        return $sett->update();
    }

    public static function getSK()
    {
        $sett = new Settings\SK();
        return $sett->update();
    }

    public static function getSKTypeBSO()
    {
        $sett = new Settings\SKTypeBSO();
        return $sett->update();
    }

    public static function getSKBsoSuppliers()
    {
        $sett = new Settings\SKBsoSuppliers();
        return $sett->update();
    }



    //Users

    public static function getUsers()
    {
        $user = new Users\M5Users();
        return $user->update();
    }


    //BSO
    public static function getBSO()
    {
        $user = new BSO\BSOItems();
        return $user->update();
    }

    public static function getContracts()
    {
        $user = new BSO\BSOContracts();
        return $user->update();
    }

    public static function getPayments()
    {
        $user = new BSO\BSOPayments();
        return $user->update();
    }

    public static function getInvoices()
    {
        $user = new BSO\BSOInvoices();
        return $user->update();
    }


    //BSO ACT SK
    public static function getActsSk()
    {
        $user = new BSO\BSOActsSk();
        return $user->update();
    }


    //BSO ORDERS SK
    public static function getOrderSk()
    {
        $user = new BSO\BSOOrderSk();
        return $user->update();
    }


    public static function getPaymentsType2()
    {
        $user = new BSO\BSOPayments();
        return $user->update2();
    }

}