<?php

namespace App\Services\Transfer\M5_v1\BSO;


use App\Models\BSO\BsoItem;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Models\Contracts\Subjects;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Finance\Invoice;
use App\Models\Organizations\Organization;
use App\Models\Settings\PointsSale;
use App\Models\Subject\Juridical;
use App\Models\Subject\Physical;
use App\Models\User;
use App\Models\Users\UsersBalance;
use App\Services\Transfer\M5_v1\Send;

class BSOInvoices{



    public function __construct()
    {


    }


    public function checkPayments($invoice_id)
    {
        $pay = Payments::where('invoice_id', $invoice_id)->get();

        if($pay && isset($pay[0])){
            return true;
        }

        return false;
    }

    public function getBSOInvoice()
    {
        $bso = Invoice::orderBy('id', 'desc')->limit(1)->get();
        return ($bso && isset($bso[0]))?$bso[0]->id:0;
    }


    public function update()
    {

        $response = Send::getInfo("SELECT * FROM cashbox where id > ".$this->getBSOInvoice()." limit 1000");


        if($response){
            foreach ($response as $info){




                if($this->checkPayments($info->id)){

                    //dd($info);

                    /*
               const TYPE_INVOICE_PAYMENT = [
                   1 => 'Наличный',
                   2 => 'Баланс',
                   3 => 'Безналичный Брокер',
                   4 => 'Безналичный СК',
               ];

               const TYPES = [
                   'cash' => 'Наличный',
                   'cashless' => 'Безналичный',
                   'sk' => 'Страховая',
               ];


               payment_type




               */
                    $type = 'cashless';
                    $type_invoice_payment_id = 3;

                    if((int)$info->payment_type == 0){//0 БЕЗНАЛ
                        $type = 'cashless';
                        $type_invoice_payment_id = 3;
                    }

                    if((int)$info->payment_type == 1){//1 НАЛ

                        $type = 'cash';
                        $type_invoice_payment_id = 1;
                    }

                    if((int)$info->payment_type == 2){//2 СК
                        $type = 'cashless';
                        $type_invoice_payment_id = 4;
                    }



                    $data = Invoice::create([
                        'id' => $info->id,
                        'user_id' => $info->payment_user_id,
                        'status_id' => ($info->state_id+1),

                        'type' => $type,
                        'type_invoice_payment_id' => $type_invoice_payment_id,

                        'create_type' => 1,
                        'org_id' => $info->org_id,
                        'agent_id' => $info->agent_id,
                        'invoice_payment_user_id' => $info->user_id,
                        'invoice_payment_date' => $info->payment_time,
                    ]);

                    $data->id = $info->id;
                    $data->save();


                    foreach ($data->payments as $payment)
                    {
                        $payment->invoice_payment_total = $payment->getPaymentAgentSum();
                        $payment->invoice_payment_date = $info->payment_time;
                        $payment->statys_id = $info->state_id;
                        $payment->save();
                    }

                    $data->refreshInvoice();


                    //dd($info, $data);


                }





            }

            //dd($response);
            return $this->update();

        }else{
            return true;
        }
    }




}