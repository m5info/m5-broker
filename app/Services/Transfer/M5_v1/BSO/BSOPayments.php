<?php

namespace App\Services\Transfer\M5_v1\BSO;


use App\Models\BSO\BsoItem;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Models\Contracts\Subjects;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Organizations\Organization;
use App\Models\Settings\PointsSale;
use App\Models\Subject\Juridical;
use App\Models\Subject\Physical;
use App\Models\User;
use App\Models\Users\UsersBalance;
use App\Services\Transfer\M5_v1\Send;

class BSOPayments{



    public function __construct()
    {


    }


    public function getBSOPaymentsType2()
    {
        $bso = Payments::where('type_id', 2)->whereNull('payment_total')->limit(1)->get();
        return ($bso && isset($bso[0]))?$bso[0]->id:0;
    }


    public function update2()
    {

        $id = $this->getBSOPaymentsType2();
        if($id > 0){
            $response = Send::getInfo("SELECT * FROM payments where type2 = 2 and is_deleted = 0 and id >= ".$id." limit 100");
            if($response){
                foreach ($response as $pay){
                    $payment = Payments::find($pay->id);
                    if($payment){
                        $payment->payment_total = $pay->real_payment_value * -1;
                        $payment->financial_policy_marjing = -100;
                        $payment->financial_policy_marjing_total = $pay->real_payment_value * -1;
                        $payment->save();
                    }
                }
                return $this->update2();
            }

        }




        dd($id);
    }


    public function getBSOPayments()
    {
        $bso = Payments::orderBy('id', 'desc')->limit(1)->get();
        return ($bso && isset($bso[0]))?$bso[0]->id:0;
    }


    public function update()
    {

        $response = Send::getInfo("SELECT * FROM payments where bso_id > 0 and is_deleted = 0 and id > ".$this->getBSOPayments()." limit 1000");


        if($response){
            foreach ($response as $info){


                //dd($info);

                $bso = BsoItem::find($info->bso_id);
                $type_id = $info->type2;
                $payment_flow = ($info->submit_receiver == 'sk')?1:0;
                $payment_type = ($info->submit_from_agent == 'cash')?0:1;


                $data = Payments::create([
                    'id' => $info->id,
                    'statys_id' => 0,
                    'type_id' => $type_id,
                    'invoice_id' => $info->cashbox_id,
                    'bso_id' => $info->bso_id,
                    'contract_id' => $info->contract_id,
                    'payment_number' => $info->payment_number,
                    'payment_data' => $info->payment_date,

                    'payment_type' => $payment_type,
                    'payment_flow' => $payment_flow,

                    'payment_total' => $info->payment_value,

                    'financial_policy_id' => $info->financial_policy,
                    'financial_policy_manually_set' => $info->financial_policy_manually_set,
                    'financial_policy_kv_bordereau' => $info->financial_policy_kv_bordereau,
                    'financial_policy_kv_dvoy' => $info->financial_policy_kv_dvoy,
                    'financial_policy_kv_agent' => $info->financial_policy_kv_agent,

                    'bso_not_receipt' => ((int)$info->backoffice_bso2_id > 0)?0:1,
                    'bso_receipt' => $info->backoffice_bso2_id,
                    'bso_receipt_id' => $info->bso2,

                    'agent_id' => $info->agent_id,
                    'manager_id' => $info->manager_id,
                    'parent_agent_id' => $info->nop_id,

                    'invoice_payment_total' => '',
                    'invoice_payment_date' => '',

                    'point_sale_id' => $bso->point_sale_id,
                    'set_balance' => $info->to_balance,

                    'acts_sk_id' => $info->reports_act_id,
                    'reports_order_id' => $info->reports_order_id,
                    'reports_dvou_id' => $info->reports_dvou_id,

                ]);

                $data->id = $info->id;
                $data->save();

                if($type_id == 0){

                    $payment = $data;
                    $payment->financial_policy_kv_bordereau_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_bordereau);
                    $payment->financial_policy_kv_dvoy_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_dvoy);
                    $payment->financial_policy_kv_agent_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_agent);
                    $payment->financial_policy_kv_parent_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_parent);
                    $payment->save();


                    if((int)$info->payment_number == 1)
                    {
                        //Обновляем ФП у договора
                        $this->updateContractFp($payment->contract, $payment);


                    }
                }


                //dd($info, $data);

            }

            //dd($response);
            return $this->update();

        }else{
            return true;
        }
    }



    public function updateContractFp($contract, $payment)
    {

        $contract->financial_policy_manually_set = $payment->financial_policy_manually_set;
        $contract->financial_policy_id = $payment->financial_policy_id;
        $contract->financial_policy_kv_bordereau = $payment->financial_policy_kv_bordereau;
        $contract->financial_policy_kv_dvoy = $payment->financial_policy_kv_dvoy;
        $contract->financial_policy_kv_agent = $payment->financial_policy_kv_agent;
        $contract->save();
        return true;
    }



}