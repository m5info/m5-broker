<?php

namespace App\Services\Transfer\M5_v1\BSO;


use App\Models\BSO\BsoItem;
use App\Models\Contracts\Subjects;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Organizations\Organization;
use App\Models\Settings\PointsSale;
use App\Models\Subject\Juridical;
use App\Models\Subject\Physical;
use App\Models\User;
use App\Models\Users\UsersBalance;
use App\Services\Transfer\M5_v1\Send;

class BSOItems{



    public function __construct()
    {


    }

    public function getBSOLast()
    {
        $bso = BsoItem::orderBy('id', 'desc')->limit(1)->get()[0];
        return ($bso)?$bso->id:0;
    }


    public function update()
    {

        $response = Send::getInfo("SELECT * FROM bso_items where id >".$this->getBSOLast()." limit 1000");


        if($response){
            foreach ($response as $info){


                $user = User::find($info->user_id);


                $data = BsoItem::create([
                    'id' => $info->id,
                    'bso_supplier_id' => $info->sk_user_id,
                    'insurance_companies_id' => $info->sk_id,
                    'point_sale_id' => $info->tp_id,
                    'org_id' => $info->org_id,
                    'bso_class_id' => $info->bso_class_id,
                    'product_id' => $info->bso_class_id,
                    'type_bso_id' => $info->bso_type_id,
                    'state_id' => $info->state_id,
                    'location_id' => $info->location_id,
                    'user_id' => $info->user_id,
                    'user_org_id' => ($user)?$user->organization_id:0,

                    'time_create' => $info->time_create,
                    'time_target' => $info->time_target,
                    'last_operation_time' => $info->last_operation_time,
                    'transfer_to_agent_time' => $info->transfer_to_agent_time,
                    'transfer_to_org_time' => $info->transfer_to_org_time,
                    'transfer_to_sk_time' => $info->transfer_to_sk_time,

                    'bso_serie_id' => $info->bso_serie_id,
                    'bso_number' => $info->bso_number,
                    'bso_dop_serie_id' => $info->bso_dop_serie_id,
                    'bso_title' => $info->bso_title,
                    'bso_blank_serie_id' => $info->bso_blank_serie_id,
                    'bso_blank_number' => $info->bso_blank_number,
                    'bso_blank_dop_serie_id' => $info->bso_blank_dop_serie_id,
                    'bso_blank_title' => $info->bso_blank_title,


                    'bso_comment' => $info->bso_comment,
                    'bso_manager_id' => $info->user_id,
                    'agent_id' => $info->agent_id,
                    'contract_id' => $info->contract_id,

                    'is_reserved' => $info->is_reserved,
                    'bso_cart_id' => $info->bso_cart_id,
                    'realized_act_id' => $info->realized_act_id,


                    'acts_reserve_or_realized_id' => '',
                    'acts_implemented_id' => '',
                    'acts_sk_id' => '',
                    'act_add_number' => '',

                    'bso_act_id' => '',
                    'transfer_id' => '',

                    'acts_to_underwriting_id' => '',

                ]);

                $data->id = $info->id;
                $data->save();


                //dd($info, $data);

            }

            //dd($response);
            return $this->update();

        }else{
            return true;
        }
    }


    public function getUserSubject($info)
    {
        return Physical::create([
            'first_name' => $info->firstname,
            'second_name' => $info->surname,
            'middle_name' => $info->middlename,
        ]);
    }


    public function updateUserBalance($user, $balance_id, $sum)
    {

        if(getFloatFormat($sum) != 0){
            $balance = $user->getBalance($balance_id);
            $balance->setTransactions(
                0, 0, getFloatFormat($sum), date('Y-m-d H:i:s'), "Перенос баланса из старой базы!"
            );

        }

    }



}