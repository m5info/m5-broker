<?php

namespace App\Services\Transfer\M5_v1\BSO;


use App\Models\Acts\ReportAct;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Models\Contracts\Subjects;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Finance\Invoice;
use App\Models\Organizations\Organization;
use App\Models\Settings\PointsSale;
use App\Models\Subject\Juridical;
use App\Models\Subject\Physical;
use App\Models\User;
use App\Models\Users\UsersBalance;
use App\Services\Transfer\M5_v1\Send;

class BSOActsSk{



    public function __construct()
    {


    }


    public function checkPayments($invoice_id)
    {
        $pay = Payments::where('acts_sk_id', $invoice_id)->get();

        if($pay && isset($pay[0])){
            return true;
        }

        return false;
    }

    public function getBSOAkt()
    {
        $bso = ReportAct::orderBy('id', 'desc')->limit(1)->get();
        return ($bso && isset($bso[0]))?$bso[0]->id:0;
    }


    public function update()
    {

        $response = Send::getInfo("SELECT * FROM reports_act where id > ".$this->getBSOAkt()." and type_act = 1 limit 1000");


        if($response){
            foreach ($response as $info){




                if($this->checkPayments($info->id)){

                    //dd($info);






                    $data = ReportAct::create([
                        'id' => $info->id,
                        'title' => $info->title,
                        'signatory_org' => $info->signatory_org,
                        'signatory_sk_bso_supplier' => $info->signatory_sk_bso_supplier,
                        'bso_supplier_id' => $info->sk_user_id,
                        'report_year' => $info->report_year,
                        'report_month' => $info->report_month,
                        'type_id' => 1,
                        'accept_status' => $info->accept_stat,
                        'accept_user_id' => $info->create_user_id,
                        'create_user_id' => $info->create_user_id,
                        'report_date_start' => $info->report_date_start,
                        'report_date_end' => $info->report_date_end,
                        'accepted_at' => $info->accept_time,
                    ]);

                    $data->id = $info->id;
                    $data->save();


                    /*
                    foreach ($data->payments as $payment)
                    {
                        $payment->invoice_payment_total = $payment->getPaymentAgentSum();
                        $payment->invoice_payment_date = $info->payment_time;
                        $payment->statys_id = $info->state_id;
                        $payment->save();
                    }

                     $data->refreshInvoice();
                    */




                    //dd($info, $data);


                }





            }

            //dd($response);
            return $this->update();

        }else{
            return true;
        }
    }




}