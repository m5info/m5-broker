<?php

namespace App\Services\Transfer\M5_v1\BSO;


use App\Models\BSO\BsoItem;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ObjectInsurer;
use App\Models\Contracts\Subjects;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Organizations\Organization;
use App\Models\Settings\PointsSale;
use App\Models\Subject\Juridical;
use App\Models\Subject\Physical;
use App\Models\User;
use App\Models\Users\UsersBalance;
use App\Services\Transfer\M5_v1\Send;

class BSOContracts{



    public function __construct()
    {


    }

    public function getBSOContract()
    {
        $bso = Contracts::orderBy('id', 'desc')->limit(1)->get();
        return ($bso && isset($bso[0]))?$bso[0]->id:0;
    }


    public function update()
    {

        $response = Send::getInfo("SELECT * FROM contracts where bso_id>0 and id > ".$this->getBSOContract()." limit 1000");


        if($response){
            foreach ($response as $info){


                //dd($info);

                $bso = BsoItem::find($info->bso_id);

                $sales_condition = $info->terms_sale;

                if($sales_condition == 0){
                    $agent = User::find($info->agent_id);
                }else{
                    $agent = User::find($info->manager_id);
                }




                $data = Contracts::create([
                    'id' => $info->id,

                    'statys_id' => 4,
                    'user_id' => $info->techander_id_accept,
                    'agent_id' => $info->agent_id,
                    'manager_id' => $info->manager_id,

                    'bso_id' => $info->bso_id,
                    'bso_title' => $bso->bso_title,

                    'insurance_companies_id' => $bso->insurance_companies_id,
                    'bso_supplier_id' => $bso->bso_supplier_id,
                    'product_id' => $info->product_id,

                    'parent_agent_id' => ($agent)?$agent->parent_id:0,
                    'sales_condition' => $sales_condition,

                    'sign_date' => $info->contract_date,
                    'begin_date' => $info->contract_begin,
                    'end_date' => $info->contract_end,

                    'object_insurer_id' => 0,
                    'payment_total' => $info->payment_total,
                    'kind_acceptance' => $info->kind_acceptance,
                    'insurer_id' => $this->getInsurer($info),

                ]);

                $data->id = $info->id;
                $data->save();


                //dd($info, $data);

            }

            //dd($response);
            return $this->update();

        }else{
            return true;
        }
    }



    public function getInsurer($data)
    {
        $updated_data = [
            'type' => isset($data->type_insurer) ? $data->type_insurer : 0,
            'title' => $data->insurer,
            'phone' => '',
            'email' => '',
            'doc_serie' => '',
            'doc_number' => '',
            'inn' => '',
            'kpp' =>  '',
        ];
        return Subjects::create($updated_data)->id;
    }



}