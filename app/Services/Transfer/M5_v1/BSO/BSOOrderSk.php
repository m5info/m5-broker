<?php

namespace App\Services\Transfer\M5_v1\BSO;


use App\Models\Acts\ReportAct;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Models\Contracts\Subjects;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Finance\Invoice;
use App\Models\Organizations\Organization;
use App\Models\Reports\ReportOrders;
use App\Models\Settings\PointsSale;
use App\Models\Subject\Juridical;
use App\Models\Subject\Physical;
use App\Models\User;
use App\Models\Users\UsersBalance;
use App\Services\Transfer\M5_v1\Send;

class BSOOrderSk{



    public function __construct()
    {


    }


    public function checkPayments($invoice_id)
    {


        $pay = Payments::where('reports_order_id', $invoice_id)->orWhere('reports_dvou_id', $invoice_id)->get();

        if($pay && isset($pay[0])){
            return true;
        }

        return false;
    }

    public function getBSOOrder()
    {
        $bso = ReportOrders::orderBy('id', 'desc')->limit(1)->get();
        return ($bso && isset($bso[0]))?$bso[0]->id:0;
    }


    public function update()
    {

        $response = Send::getInfo("SELECT * FROM reports_order where id > ".$this->getBSOOrder()." and is_deleted = 0 limit 10");


        if($response){
            foreach ($response as $info){



                if($this->checkPayments($info->id)){

                   // dd($info);






                    $data = ReportOrders::create([
                        'id' => $info->id,
                        'bso_supplier_id' => $info->sk_user_id,
                        'type_id' => $info->is_dvou,
                        'create_user_id' => $info->create_user_id,
                        'created_at' => $info->create_time,
                        'title' => $info->title,
                        'signatory_org' => $info->signatory_org,
                        'signatory_sk_bso_supplier' => $info->signatory_sk_bso_supplier,
                        'report_year' => $info->report_year,
                        'report_month' => $info->report_month,
                        'report_date_start' => $info->report_date_start,
                        'report_date_end' => $info->report_date_end,
                        'accept_status' => 2,
                        'accepted_at' => $info->accept_time,
                        'accept_user_id' => $info->accept_user_id,
                        'comments' => (string)$info->comment,

                    ]);

                    $data->id = $info->id;
                    $data->save();

                    $data->refreshSumOrder();

                    /*
                    foreach ($data->payments as $payment)
                    {
                        $payment->invoice_payment_total = $payment->getPaymentAgentSum();
                        $payment->invoice_payment_date = $info->payment_time;
                        $payment->statys_id = $info->state_id;
                        $payment->save();
                    }

                     $data->refreshInvoice();
                    */




                    //dd($info, $data);


                }





            }

            //dd($response);
            return $this->update();

        }else{
            return true;
        }
    }




}