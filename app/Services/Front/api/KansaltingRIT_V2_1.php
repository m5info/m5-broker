<?php

namespace App\Services\Front\api;

use App\Models\Contracts\Contracts;
use App\Models\File;
use App\Models\Settings\SettingsSystem;
use App\Models\User;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;
use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Mockery\Exception;

class KansaltingRIT_V2_1
{

    private $url = '';

    public function __construct()
    {
        $this->url = SettingsSystem::getDataParam('front', 'url');
    }

    public function send($url, $data = null, $method = 'GET', $res_type = 0)
    {
        $headers = [];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "{$this->url}$url",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);


        $result = $response;


        if($res_type == 0){
            $result = \GuzzleHttp\json_decode($response);
        }


        return $result;
    }


    public function searchOrders($order, $count)
    {
        $order = (int)$order;
        $result = $this->send("get_data_orders_list.aspx?order={$order}");

        $res = null;

        foreach ($result as $res_data)
        {

            $order_title = "Направление ".(string)$res_data->type_order." №".(int)$res_data->seria_order." / ".(int)$res_data->number_order." от ".(string)$res_data->date_order.' '.(string)$res_data->client_name;


            $data = [];
            $data["value"] = $order_title;
            $data["unrestricted_value"] = $order_title;
            $data["data"] = [];
            $data["data"]['id'] = $res_data->id_node;
            $data["data"]['title'] = $order_title;
            $data["data"]['insurer'] = $res_data->client_name;
            $data["data"]['number_order'] = (int)$res_data->number_order;

            $res[] = $data;
        }

        return $res;

    }


    public function get_order_data($order_id)
    {


        $order_id = (int)$order_id;
        $result = $this->send("get_data_orders.aspx?order_id={$order_id}");

        $temp_auto = (array)\GuzzleHttp\json_decode((string)$result->object_insurer_auto);
        $temp_auto_str = $temp_auto["XmlФорма"];
        $temp_auto_str = str_replace('ctl00$ContentPlaceHolderBody$BaseOrder1$ctl01$','', $temp_auto_str);
        if($temp_auto_str) {
            $temp_auto_xml = new \SimpleXMLElement($temp_auto_str);
            $temp_auto_arr = [];
            foreach ($temp_auto_xml->KeyValueOfstringstring as $temp_KeyValue) {

                $temp_auto_arr[(string)$temp_KeyValue->Key] = (string)$temp_KeyValue->Value;
            }
        }
        //dd($result);


        $manager_id = 0;
        $manager_name = '';
        $manager = User::where("front_user_id", (int)$result->manager_id)->get()->first();
        if($manager){
            $manager_id = $manager->id;
            $manager_name = $manager->name. ' ' .$manager->email;
        }

        $model = (isset($temp_auto_arr['Модель'])?$temp_auto_arr['Модель']:'');
        $model = explode(' ', $model)[0];

        $phone = (string)$result->phone;
        //ЗАПРОС К КАНСАЛТИНГУ
        if(stristr($phone, '8 (')){
            $phone = str_replace('8 (', '+7 (', $phone);
        }else{
            $phone = parsePhoneNumber($phone);
            if(strlen($phone) > 5){
                $phone = setPhoneNumberFormat($phone, '+7 (ddd) ddd-dd-dd');
            }
        }

        $add_phone = (string)$result->dop_phone;
        //ЗАПРОС К КАНСАЛТИНГУ
        if(stristr($add_phone, '8 (')){
            $add_phone = str_replace('8 (', '+7 (', $add_phone);
        }else{
            $add_phone = parsePhoneNumber($add_phone);
            if(strlen($add_phone) > 5){
                $add_phone = setPhoneNumberFormat($add_phone, '+7 (ddd) ddd-dd-dd');
            }
        }


        $result = [
            'id'=>(int)$result->rit_order_id,
            'title'=>(string)$result->rit_order_title,
            'source'=>(string)$result->source_statement,
            'insurer'=>(string)$result->insurer,
            'insurer_type' => (((string)$result->insurer_type == 'ФизическоеЛицо')?0:1),
            'insurer_phone'=>$phone,
            'insurer_add_phone'=>$add_phone,
            'insurer_delivery_address'=>(string)$result->delivery,
            'insurer_underground'=>(string)$result->underground,
            'insurer_email'=>'',
            'payment_total'=>getFloatFormat((string)$result->summ_in_receipt),
            'payment_number'=>'1',
            'payment_data'=>setDateTimeFormatRu((string)$result->payment_date, 1),
            'delivery_date'=>setDateTimeFormatRu((string)$result->checkout_date, 1),
            'official_discount'=>'',
            'informal_discount'=>getFloatFormat((string)$result->discount),
            'bank_kv' => getFloatFormat((string)$result->kv_bank),
            'sign_date'=>'',
            'begin_date'=>'',
            'end_date'=>'',
            'is_personal_sales'=> (((string)$result->personal_selling == 'False')?0:1),
            'manager_id'=>$manager_id,
            'manager_name'=>$manager_name,
            'object_insurer_title'=>(string)$result->ts,
            'object_insurer_auto' => [
                'power'=>'',
                'reg_number'=>'',
                'mark'=>(isset($temp_auto_arr['Марка'])?$temp_auto_arr['Марка']:''),
                'mark_id'=>'0',
                'model'=> $model,
                'model_id'=>'0',
                'car_year'=>(string)$result->year_ts,
                'vin'=>'',
            ]

        ];

        //dd($result);

        if(isset($result['object_insurer_auto']) && isset($result['object_insurer_auto']['mark'])){
            $mark = VehicleMarks::where('title', 'like', $result['object_insurer_auto']['mark']."%")->get()->first();

            if($mark){
                $result['object_insurer_auto']['mark_id'] = $mark->id;
                $model = VehicleModels::where('mark_id', $mark->id)->where('title', 'like', "%".$result['object_insurer_auto']['model']."%")->get()->first();
                if($model){
                    $result['object_insurer_auto']['model_id'] = $model->id;
                }
            }

        }


        return $result;

    }

    public function searchUsers($name)
    {

        $name = explode(' (', $name)[0];

        $res = null;

        $result = $this->send("users.aspx", "user_name={$name}", "POST");
        foreach ($result as $res_data)
        {
            $data = [];
            $data["value"] = (string)$res_data->user_title." (".(string)$res_data->user_login.")";
            $data["unrestricted_value"] = (string)$res_data->user_title." (".(string)$res_data->user_login.")";
            $data["data"] = [];
            $data["data"]['id'] = (int)$res_data->user_id;
            $data["data"]['title'] = (string)$res_data->user_title;

            $res[] = $data;
        }

        return $res;


    }



    public function sendOrdersData($contract)
    {

        $payment = $contract->get_payment_first();

        if($payment){
            $kv_manager_base = $payment->financial_policy_kv_agent;

            if($payment->financial_policy_manually_set == 0 && $payment->financial_policy_id > 0){
                $financial_policy = $payment->financial_policy->getGroupKV($payment);
                $kv_manager_base = $financial_policy->kv_agent;
            }

            $kv_manager_sum = $payment->financial_policy_kv_agent_total;
            $payment_value = $payment->invoice_payment_total;
            $invoice_payment_total = $payment->payment_total;


            $DebtSum = $contract->get_payments_type(1)->sum('payment_total');
            $PaySum = $contract->get_payments_type(2)->sum('payment_total');

            if($DebtSum > 0){
                //$kv_manager_sum +=$DebtSum;
                $payment_value +=$DebtSum;
                //$invoice_payment_total +=$DebtSum;
            }

            if($PaySum > 0){
                //$kv_manager_sum -= $PaySum;
                $payment_value -= $PaySum;
                //$invoice_payment_total -= $PaySum;
            }

            $contract_date = $contract->sign_date?:getDateTime();
            if($payment->statys_id == 1){
                $contract_date = $payment->payment_data;
            }



            $params = [
                'order_id' => $contract->order_id,
                'state'  => (($contract->kind_acceptance==1)?1:0), // 0 Условный 1 БезУсловный
                'bso' => $contract->bso_title,
                'contract_date' => $contract_date,
                'payment_time' => $payment->invoice_payment_date,
                'submit_date' => $contract->sign_date?:getDateTime(),
                'sk_title' => $contract->insurance_companies->title,
                'insurer' => $contract->insurer->title,
                'payment_value' => $payment_value,
                'invoice_payment_total' => $invoice_payment_total,
                'discount' => $payment->informal_discount,
                'discount_sum' => $payment->informal_discount_total,
                'kv_bank' => $payment->bank_kv,
                'kv_manager_base' => $kv_manager_base,
                'kv_manager' => $payment->financial_policy_kv_agent,
                'kv_manager_sum' => $kv_manager_sum,
            ];



            $strParams = http_build_query($params, '', '&');
            $result = $this->send("set_orders.aspx", $strParams, 'POST', 1);
        }

        //Вторые взносы
        foreach ($contract->payments as $paymentSecond)
        {
            if($payment->id != $paymentSecond->id && $paymentSecond->type_id == 0){
                $this->sendOrdersDataPayment($contract, $paymentSecond);
            }
        }


        return true;
    }



    public function destroyOrdersData($contract)
    {

        $payment = $contract->get_payment_first();

        $params = [
            'order_id' => $contract->order_id,
            'state'  => 0, // 0 Условный 1 БезУсловный
            'bso' => '',
            'contract_date' => $contract->begin_date,
            'payment_time' => $payment->invoice_payment_date,
            'submit_date' => getDateTime(),
            'sk_title' => '',
            'insurer' => $contract->insurer->title,
            'payment_value' => 0,
            'invoice_payment_total' => 0,
            'discount' => 0,
            'discount_sum' => 0,
            'kv_bank' => 0,
            'kv_manager_base' => 0,
            'kv_manager' => 0,
            'kv_manager_sum' => 0,
        ];



        $strParams = http_build_query($params, '', '&');

        //dd($strParams);

        $result = $this->send("set_orders.aspx", $strParams, 'POST', 1);

        //dd($result);

        return true;

    }

    public function sendOrdersDataPayment($contract, $payment)
    {

        if($payment){
            $kv_manager_base = $payment->financial_policy_kv_agent;

            if($payment->financial_policy_manually_set == 0 && $payment->financial_policy_id > 0){
                $financial_policy = $payment->financial_policy->getGroupKV($payment);
                $kv_manager_base = $financial_policy->kv_agent;
            }

            $kv_manager_sum = $payment->financial_policy_kv_agent_total;
            $payment_value = $payment->invoice_payment_total;
            $invoice_payment_total = $payment->payment_total;


            $contract_date = $payment->payment_data?:getDateTime();

            $params = [
                'order_id' => $payment->order_id,
                'state'  => (($contract->kind_acceptance==1)?1:0), // 0 Условный 1 БезУсловный
                'bso' => $contract->bso_title,
                'contract_date' => $contract_date,
                'payment_time' => $payment->invoice_payment_date,
                'submit_date' => $contract->sign_date?:getDateTime(),
                'sk_title' => $contract->insurance_companies->title,
                'insurer' => $contract->insurer->title,
                'payment_value' => $payment_value,
                'invoice_payment_total' => $invoice_payment_total,
                'discount' => $payment->informal_discount,
                'discount_sum' => $payment->informal_discount_total,
                'kv_bank' => $payment->bank_kv,
                'kv_manager_base' => $kv_manager_base,
                'kv_manager' => $payment->financial_policy_kv_agent,
                'kv_manager_sum' => $kv_manager_sum,
            ];

            $strParams = http_build_query($params, '', '&');
            $result = $this->send("set_orders.aspx", $strParams, 'POST', 1);
        }

        return true;
    }

    public function destroyOrdersDataPayment($contract, $payment)
    {

        $params = [
            'order_id' => $payment->order_id,
            'state'  => 0, // 0 Условный 1 БезУсловный
            'bso' => '',
            'contract_date' => $contract->begin_date,
            'payment_time' => $payment->invoice_payment_date,
            'submit_date' => getDateTime(),
            'sk_title' => '',
            'insurer' => $contract->insurer->title,
            'payment_value' => 0,
            'invoice_payment_total' => 0,
            'discount' => 0,
            'discount_sum' => 0,
            'kv_bank' => 0,
            'kv_manager_base' => 0,
            'kv_manager' => 0,
            'kv_manager_sum' => 0,
        ];

        $strParams = http_build_query($params, '', '&');
        $result = $this->send("set_orders.aspx", $strParams, 'POST', 1);
        return true;
    }




    public function getOrderFile($contract)
    {
        if((int)$contract->order_id > 0){
            $result = $this->send("view_order_files.aspx?order_id={$contract->order_id}");

            $images = array();
            $i      = 0;
            foreach ( $result as $file ) {
                $name_file                 = trim( $file->name_file );
                $id_node                   = trim( $file->id_node );
                $images[ $i ]['name_file'] = $name_file;
                $images[ $i ]['id_node']   = $id_node;
                $images[ $i ]['img']       = 0;
                $images[ $i ]['type']      = '';
                preg_match_all( '/img|png|PNG|jpg|JPG|jpeg|JPEG/', $name_file, $matches );
                preg_match_all( '/pdf|PDF|xlsx|XLSX|xls|XLS|doc|DOC|docs|DOCS|msg|MSG/', $name_file, $matches_doc );
                if ( isset($matches) && isset($matches[0]) && isset($matches[0][0]) ) {
                    $images[ $i ]['type'] = $matches[0][0];
                    $images[ $i ]['img']  = 1;
                }
                if ( isset($matches_doc) && isset($matches_doc[0]) && isset($matches_doc[0][0]) ) {
                    $images[ $i ]['type'] = $matches_doc[0][0];
                    $images[ $i ]['img']  = 0;
                }

                $i ++;
            }

            $path = storage_path('app/'.Contracts::FILES_DOC . "/{$contract->id}/");
            if (!is_dir(($path))) {
                mkdir(($path), 0777, true);
            }

            foreach ( $images as $image ) {

                try {

                    $file_name = uniqid();

                    $file_contents = file_get_contents( $this->url."../File.ashx?t=read&id_node={$image['id_node']}" );

                    file_put_contents($path . $file_name.'.'.$image['type'], $file_contents);

                    $file = File::create([
                        'original_name' => $image['name_file'],
                        'ext'           => $image['type'],
                        'folder'        => Contracts::FILES_DOC . "/{$contract->id}/",
                        'name'          => $file_name,
                        'user_id'       => auth()->id()
                    ]);


                    $contract->scans()->save($file);

                } catch (Exception $e) {

                }





            }
        }



        return true;
    }



    public function createSecondPayment($payment)
    {
        $data_time_order = $payment->payment_data;
        $manager_id = $payment->manager->front_user_id;
        $base_order_id = $payment->contract->order_id;
        $contract = $payment->contract;


        $contract_payment_total = $contract->payment_total;
        $payment_total = $payment->payment_total;
        $payment_data = $payment->payment_data;

        $temp = $contract->expected_payments()->get()->last();
        if($temp){
            $payment_total = $temp->payment_sum;
            $payment_data = $temp->payment_date;
        }



        $object_insurer = '';
        if($contract->object_insurer_auto){
            $object_insurer = (($contract->object_insurer_auto->mark)?$contract->object_insurer_auto->mark->title:'')." ".(($contract->object_insurer_auto->model)?$contract->object_insurer_auto->model->title:'')." ({$contract->object_insurer_auto->year_of_car}) {$contract->object_insurer_auto->reg_number}";
        }



        $info = "<table><tbody><tr><th>ФИО</th><td>{$contract->insurer->title}</td></tr><tr><th>Дата договора</th><td>".getDateFormatRu($contract->sign_date)."</td></tr><tr><th>Дата платежа</th><td>".getDateFormatRu($payment_data)."</td></tr><tr><th>E-mail</th><td>{$contract->insurer->email}</td></tr><tr><th>Страховая компания</th><td>{$contract->insurance_companies->title} </td></tr><tr><th>Полис</th><td>Полис {$contract->bso->bso_title} </td></tr><tr><th>Вид страхования</th><td>{$contract->bso->product->title}</td></tr><tr><th>Агент</th><td>{$payment->agent->name}</td></tr><tr><th>Менеджер</th><td>{$payment->manager->name} (Кутепова ВН)</td></tr><tr><th>Объект страхования</th><td>{$object_insurer}</td></tr><tr><th>Общая премия (руб.)</th><td>".titleFloatFormat($contract_payment_total)."</td></tr><tr><th>Ожидаемый взнос №{$payment->payment_number} (руб.)</th><td>".titleFloatFormat($payment_total)."</td></tr></tbody></table>";


        //"<table><tr><th>TEST</th><td>1234</td></tr></table>"


        $discount_sum = getTotalSumToPrice($payment_total, $payment->informal_discount);

        $params = [
            'info' => $info,
            'contract_payment' => getFloatFormat($contract_payment_total),
            'payment_total' => getFloatFormat($payment_total),
            'discount' => getFloatFormat($payment->informal_discount),
            'discount_sum' => getFloatFormat($discount_sum),
            'client_payment_total' => getFloatFormat($payment_total-$discount_sum),
            'sk_title' => $contract->insurance_companies->title,
        ];


        $strParams = http_build_query($params, '', '&');


        $order_id = (int)$this->send("create_order_payment.aspx?order_id={$base_order_id}&user_id={$manager_id}&manager_id={$manager_id}&data_time_order={$data_time_order}", $strParams, "POST", 1);


        //$order_id = (int)$this->send("create_order_payment.aspx?order_id={$base_order_id}&user_id={$manager_id}&manager_id={$manager_id}&data_time_order={$data_time_order}", null, "GET", 1);

        if($order_id > 0){
            $result = $this->send("get_data_orders.aspx?order_id={$order_id}");

            $payment->order_id = $order_id;
            $payment->order_title = (string)$result->rit_order_title;
            $payment->order_sort_id = isolateOrderNumber((string)$result->rit_order_title);
            $payment->send_front = 1;
            $payment->save();
        }



        return $payment;
    }

    public static function getWorkedDayToRIT($mounth_from, $year_from){

        $date_from = getDateFormatEn("{$year_from}-{$mounth_from}-01");
        $number = cal_days_in_month(CAL_GREGORIAN, $mounth_from, $year_from);
        $date_to = getDateFormatEn("{$year_from}-{$mounth_from}-{$number}");

        $res = MSSQLQueryResult("
            SELECT 
                [IdUser] as user_rit_id,	
                sum(CASE 
                WHEN [СтатусДня] = 'Р' THEN 1 
                    ELSE 0
                END  ) as worked_day,
                YEAR([ДатаСтатистики]) as date_year,
                MONTH([ДатаСтатистики]) as date_month
            FROM [еврогарант].[dbo].[tblСтатистика]
            WHERE 
                [ДатаСтатистики] >= '{$date_from} 00:00:00' 
            AND [ДатаСтатистики] <= '{$date_to} 23:59:59'
            GROUP BY [IdUser], YEAR([ДатаСтатистики]), MONTH([ДатаСтатистики])");


        return $res;
    }



}