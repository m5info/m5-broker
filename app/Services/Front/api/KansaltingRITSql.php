<?php

namespace App\Services\Front\api;

use App\Models\Contracts\Contracts;
use App\Models\File;
use App\Models\Settings\SettingsSystem;
use App\Models\User;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;
use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Mockery\Exception;

class KansaltingRITSql
{

    private $connect;

    public function __construct()
    {


        $driver = "DRIVER=ODBC Driver 17 for SQL Server";
        $server = "192.168.2.18";
        $db_name = "еврогарант";
        $port = "1433";
        $user = "kdo";
        $password = "dmosk.ru135";
        $this->connect = odbc_connect("{$driver};Server={$server};Database={$db_name};Port={$port};String Types=Unicode", $user, $password);



        /*
        try {
            $this->connect = new \PDO('odbc:ODBC Driver 17 for SQL Server;SERVERNAME=sqlservername;DATABASE=еврогарант;','kdo','OutUserPas');

        } catch (\PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        */


    }


    public function query( $sql_query ) {

        return odbc_exec($this->connect, $sql_query);

        //$test = odbc_fetch_object($res);

        //return $this->connect->query($sql_query);

    }


    public function getQueryResult( $query, $get_one_row = false ) {


        $query_result = $this->query( $query );
        $result       = array();
        if ( $get_one_row ) {
            $curr = odbc_fetch_object( $query_result );
            if(count($curr) == 0) return null;
            return $curr;
        } else {
            while ( $curr = odbc_fetch_object( $query_result ) ) {
                $result[] = $curr;
            }
        }
        return (array) $result;

    }



    public function close()
    {
        odbc_close($this->connect);
        //odbc_close_all();
        return true;
    }



}