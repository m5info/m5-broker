<?php

namespace App\Services\Calc;

use App\Models\Contracts\ContractsCalculation;
use App\Models\Settings\BaseRate;
use App\Models\Settings\City;
use Exception;
use GuzzleHttp\Client;
use phpDocumentor\Reflection\Location;

class DefaultOSAGO
{


    const power = [
        0.6,
        1,
        1.1,
        1.2,
        1.4,
        1.6
    ];

    const age = [
        1.8,
        1.7,
        1.6,
        1
    ];

    private static function calculate_age($date) {
        $date_timestamp = strtotime($date);
        $age = date('Y') - date('Y', $date_timestamp);
        if (date('md', $date_timestamp) > date('md')) {
            $age--;
        }
        return $age;
    }

    public static function calc(ContractsCalculation $calc)
    {
        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = $calc->id;
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';



        $contract = $calc->contract;
        $kbm = 1;

        $driver = $contract->drivers()->orderBy('kbm', 'desc')->first();

        if($driver){
            $kbm = getFloatFormat($driver->kbm);
        }

        $year_birth = self::calculate_age($driver->birth_date);
        $year_exp = self::calculate_age($driver->exp_date);

        $power = 0;
        $age = 0;
        $p_power = (int)$contract->object_insurer_auto->power;
        $p_age = 0;

        if($p_power<= 50) $power = 0;
        else if($p_power>= 50 && $p_power<= 70) $power = 1;
        else if($p_power>= 70 && $p_power<= 100) $power = 2;
        else if($p_power>= 100 && $p_power<= 120) $power = 3;
        else if($p_power>= 120 && $p_power<= 150) $power = 4;
        else if($p_power>= 150) $power = 5;

        if($year_birth <= 22 && $year_exp < 3) $p_age = 0;
        else if($year_birth > 22 && $year_exp <= 3) $p_age = 1;
        else if($year_birth <= 22 && $year_exp > 3) $p_age = 2;
        else if($year_birth > 22 && $year_exp > 3) $p_age = 3;

        $address_register_city_kladr_id = $contract->owner->get_info()->address_register_city_kladr_id;
        if(strlen($address_register_city_kladr_id) > 3){

            $city = City::where('kladr', $address_register_city_kladr_id)->first();

            if($city){
                $base = BaseRate::where('city_id', $city->id)->where('company_id', $calc->insurance_companies_id)->first();
                if($base){
                    $response->state = true;
                    $response->payment_total = $base->rate * self::power[$power] * $kbm * $city->rate * self::age[$p_age];
                    $response->msg = "ТБ: {$base->rate}; КТ: {$city->rate}; КБМ: {$kbm}; КС: ".self::power[$power]."; КП: ".self::age[$p_age].";";
                    $response->statys_id = 2;

                }else $response->error = 'Адрес собственника не разрешен!';
            }else $response->error = 'Адрес собственника не разрешен!';


        }else $response->error = 'Не указан адрес собственника!';


        return $response;

    }

}