<?php

namespace App\Services\SK\Alpha;


use App\Services\SK\Alpha\Services\Dictionary\DictionaryApi;
use App\Services\SK\Alpha\Services\Dictionary\DictionaryService;
use App\Services\SK\Alpha\Services\Kasko\KaskoApi;
use App\Services\SK\Alpha\Services\Kasko\KaskoService;
use App\Services\SK\Alpha\Services\Osago\OsagoApi;
use App\Services\SK\Alpha\Services\Osago\OsagoService;
use App\Services\SK\BaseServiceController;

class ServiceController extends BaseServiceController {


    const SERVICES = [

        'dictionary' => [
            'service' => DictionaryService::class,  /// Можно проставить для удобства конечно им префиксы типа "AlphaDictionaryService::class"
            'api' => DictionaryApi::class,          /// но наверное необязательно
        ],
        'osago' => [
            'service' => OsagoService::class,
            'api' => OsagoApi::class,
        ],
        'kasko' => [
            'service' => KaskoService::class,
            'api' => KaskoApi::class,
        ],

    ];


    const PROGRAMS = [
        'kasko' => [
            ['id' => 1, 'name' => '50х50+', 'template' => 'default', 'title' => '50х50+'],
            ['id' => 2, 'name' => 'Голое КАСКО', 'template' => 'default', 'title' => 'Голое КАСКО'],
            ['id' => 3, 'name' => 'Альфа-Бизнес', 'template' => 'default', 'title' => 'Альфа-Бизнес'],
            ['id' => 4, 'name' => 'Альфа-Все включено', 'template' => 'default', 'title' => 'Альфа-Все включено'],
        ]

    ];
}