<?php


namespace App\Services\SK\Alpha\Services\Kasko;


class ProgramValidation
{

    public function generateDrivers($c)
    {

        $drivers_xml = '';
        $count = 0;

        foreach ($c['driver'] as $driver) {
            $d = $driver;
            if (isset($d['same_as_insurer']) && $d['same_as_insurer'] == 1) {
                $d['fio'] = $c['insurer']['fio'];
                $d['birth_date'] = $c['insurer']['birthdate'];
                $d['sex'] = $c['insurer']['sex'];
            } else
                $d['fio'] = explode(' ', $d['fio']);


            $birthDate = explode(".", $d['birth_date']);

            $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
                ? ((date("Y") - $birthDate[2]) - 1)
                : (date("Y") - $birthDate[2]));

            $exp_date = explode(".", $d['exp_date']);
            $exp = (date("md", date("U", mktime(0, 0, 0, $exp_date[0], $exp_date[1], $exp_date[2]))) > date("md")
                ? ((date("Y") - $exp_date[2]) - 1)
                : (date("Y") - $exp_date[2]));

            $d['birth_date'] = date('Y-m-d', strtotime($d['birth_date']));
            $d['exp_date'] = date('Y-m-d', strtotime($d['exp_date']));


            $drivers_xml .= '
            <AdmittedDriveList>
                  <age>' . $age . '</age>
                  <dateBirthDriver>' . $d['birth_date'] . 'T00:00:00</dateBirthDriver>
                  <dateExperiencedDriver>' . $d['exp_date'] . 'T00:00:00</dateExperiencedDriver>
                  <experience>' . $exp . '</experience>
                  <firstDateDriver>' . $d['exp_date'] . 'T00:00:00</firstDateDriver>
                  <gender>1</gender>
                  <name>' . $d["fio"][1] . '</name>
                  <patronymic>' . $d["fio"][0] . '</patronymic>
                  <surname>' . $d["fio"][2] . '</surname>
                  <driversLicenseSerialNumber>' . $d["doc_serie"] . '</driversLicenseSerialNumber>
                  <driversLicenseNumber>' . $d["doc_num"] . '</driversLicenseNumber>
                  <KBM/>
               </AdmittedDriveList>';
            $count++;
        }
        return $drivers = [
            'xml' => $drivers_xml,
            'count' => $count
        ];
    }

    public function ProgramXML($c)
    {

        return $xml = <<<XML
        <ns1:RTKVRequest>
            <DEBUG_MODE>true</DEBUG_MODE>
            <!--только по согласованию андеррайтера / Утрата стоимости-->
            <UTC xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <UTC xmlns="">
               <aggregateAmount>false</aggregateAmount>
               <baseRateSize>0</baseRateSize>
               <bonusValue>0</bonusValue>
               <bonusValueCalculation>false</bonusValueCalculation>
               <commissionMultiplier>0</commissionMultiplier>
               <currentKBM>0</currentKBM>
               <finalRate>0</finalRate>
               <insuaranceBonus>0</insuaranceBonus> <!--не заполняеться-->
               <KBM>0</KBM> <!--не заполняеться-->
               <risk>false</risk>
               <sumSize>0</sumSize>
               <underwritingDiscount>0</underwritingDiscount>
            </UTC>
         </UTC>
            <agent xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <agent xmlns="">
               <agentName>ООО ССТ</agentName>
               <exposition>0</exposition>
            </agent>
         </agent>
            <agentAgreement xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <agentAgreement xmlns="">
               <agentAgreementNumber>51/А/05</agentAgreementNumber>
            </agentAgreement>
         </agentAgreement>
            <beneficiary xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <beneficiary xmlns="">
               <beneficiaryList>
                  <requestBKI>false</requestBKI>
                  <scoringRating>0</scoringRating>
               </beneficiaryList>
            </beneficiary>
         </beneficiary>
            <bonus xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <bonus xmlns="">
               <addonSize>0</addonSize>
               <calculatedSize>0</calculatedSize>
               <earnedSize>0</earnedSize>
               <paidSize>0</paidSize>
               <totalSize>0</totalSize>
            </bonus>
         </bonus>
            <calculation xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <calculation xmlns="">
               <promoCode/>
               <checkResult/>
               <amountProportion>0</amountProportion>
               <contractState>0</contractState>
               <date>{$c['date_calc']}T11:57:31</date>
               <finalCalc>false</finalCalc>
               <loyaltyScore>0</loyaltyScore>
               <maxLoyaltyScore>0</maxLoyaltyScore>
               <purchaseAmount>0</purchaseAmount>
               <score>0</score>
               <success>true</success>
               <systemDescriptor>INTERPLAT</systemDescriptor>
               <userName>INTERPLAT</userName>
               <userRole/>
            </calculation>
         </calculation>
            <company xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <company xmlns="">
               <bankName>НЕТ</bankName>
               <companyName>ОАО «АльфаСтрахование»</companyName>
            </company>
         </company>
            <distribution xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <distribution xmlns="">
               <channel>5_Агенты и розничные брокеры</channel>
            </distribution>
         </distribution>
            <doo xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <doo xmlns="">
               <previousInsuaranceBonus>0</previousInsuaranceBonus> <!--не заполняеться-->
               <previousRegressLossesList>false</previousRegressLossesList>
               <lossesOnPreviousContractNumber>0</lossesOnPreviousContractNumber>
               <lossesOnPreviousContractSum>0</lossesOnPreviousContractSum>
               <aggregateAmount>false</aggregateAmount>
               <baseRatePercent>0</baseRatePercent>
               <bonusValue>0</bonusValue>
               <bonusValueCalculation>false</bonusValueCalculation>
               <c2c4>0</c2c4>
               <calculatedFranchise>{$c['franchise']}</calculatedFranchise>
               <category>0</category>
               <commissionMultiplier>0</commissionMultiplier>
               <currentKBM>0</currentKBM>
               <finalRate>0</finalRate>
               <franchiseSize>{$c['franchise']}</franchiseSize>
               <insuaranceBonus>0</insuaranceBonus> <!--не заполняеться-->
               <KBM>0</KBM> <!--не заполняеться-->
               <kpark>0</kpark>
               <parkLegalEntities>0</parkLegalEntities>
               <risk>{$this->getBool(isset($c['doo']['risk']))}</risk>
               <sumSize>{$c['doo']['total']}</sumSize>
               <underwritingDiscount>0</underwritingDiscount>
            </doo>
            </doo>
            <gap xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <gap xmlns="">
               <risk>false</risk>
               <baseRatePercent>1</baseRatePercent>
               <insuranceBonus>0</insuranceBonus>
            </gap>
         </gap>
         <!--Нельзя страховать ГДО-->
            <go xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <go xmlns="">
               <previousInsuaranceBonus>0</previousInsuaranceBonus> <!--не заполняеться-->
               <previousRegressLossesList>false</previousRegressLossesList>
               <lossesOnPreviousContractNumber>0</lossesOnPreviousContractNumber>
               <lossesOnPreviousContractSum>0</lossesOnPreviousContractSum>
               <aggregateAmount>false</aggregateAmount>
               <baseRateSize>0</baseRateSize>
               <bonusValue>0</bonusValue>
               <bonusValueCalculation>false</bonusValueCalculation>
               <c2c4>0</c2c4>
               <calculatedFranchise>0</calculatedFranchise>
               <commissionMultiplier>0</commissionMultiplier>
               <currentKBM>0</currentKBM>
               <finalRate>0</finalRate>
               <franchiseSize>0</franchiseSize>
               <insuaranceBonus>0</insuaranceBonus> <!--не заполняеться-->
               <KBM>0</KBM> <!--не заполняеться-->
               <kpark>0</kpark>
               <parkLegalEntities>0</parkLegalEntities>
               <risk>{$this->getBool('true')}</risk>
               <sumSize>0</sumSize>
               <underwritingDiscount>0</underwritingDiscount>
            </go>
            </go>
            <harm xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <harm xmlns="">
               <previousInsuaranceBonus>0</previousInsuaranceBonus>
               <previousRegressLossesList>false</previousRegressLossesList>
               <lossesOnPreviousContractNumber>0</lossesOnPreviousContractNumber>
               <lossesOnPreviousContractSum>0</lossesOnPreviousContractSum>
               <aggregateAmount>false</aggregateAmount>
               <avaragePayment>0</avaragePayment>
               <baseRatePercent>0</baseRatePercent>
               <bonusValue>0</bonusValue>
               <bonusValueCalculation>false</bonusValueCalculation>
               <c2c4>0</c2c4>
               <calculatedFranchise>{$c['franchise']}</calculatedFranchise>
               <commissionMultiplier>1</commissionMultiplier>
               <currentKBM>0</currentKBM>
               <finalRate>0</finalRate>
               <franchiseSize>{$c['franchise']}</franchiseSize>
               <frequency>0</frequency>
               <insuaranceBonus>0</insuaranceBonus>
               <KBM>0</KBM>
               <kpark>1</kpark>
               <parkLegalEntities>0</parkLegalEntities>
               <partialRisk>true</partialRisk>
               <risk>{$this->getBool(isset($c['risks']['harms']['status']))}</risk>
               <sumSize>{$c['risks']['harms']['sum']}</sumSize>
               <underwritingDiscount>1</underwritingDiscount>
            </harm>
         </harm>
              
           
            <insurance xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <insurance xmlns="">
             <insurancePeriodList>
                  <endDate>{$c['end_date']}T23:59:00</endDate>
                  <startDate>{$c['begin_date']}T11:57:31</startDate>
               </insurancePeriodList>
                <operationPeriodList>
                  <endDate>2020-10-24T10:57:31</endDate>
                  <startDate>2019-10-25T10:57:31</startDate>
               </operationPeriodList>
               <previousProgramm/>
               <previousComplementaryName/>
               <additionalAgreementType/>
               <agreementType>{$c['agreementType']}</agreementType>
               <expansionOfCoverageArea/>
               <amount_paid>0</amount_paid>
               <baseRateSize>0</baseRateSize>
               <claimSettlementOrder>Нет</claimSettlementOrder>
               <combackLimit>0</combackLimit>
               <complementaryName>Нет</complementaryName>
               <countries>0</countries><!--не заполняеться-->
               <currency>Рубль</currency>
               <currencyExchangeRate>0</currencyExchangeRate>
               <franchisingObligation>{$this->getBool($c['franchise_type_id'])}</franchisingObligation>
               <installmentPlan>Единовременно</installmentPlan>
               <lossesNumber>0</lossesNumber>
               <lossesSum>0</lossesSum>
               <orderNumber>0</orderNumber>
               <periodNumber>0</periodNumber>
               <previousContractPeriodNumber>0</previousContractPeriodNumber><!--не заполняеться-->
               <product>046</product>
               <programm>{$c['program']}</programm>
               <servicePackage>Пакет А+</servicePackage>
               <specialConditions>0</specialConditions>
               <groupInsurant/>
               <discountProgramm/>
               <rootAgreementType/>
               <specialProposalName>Нет</specialProposalName>
               <special_offer>false</special_offer>
               <visualInspection>false</visualInspection>
               <insuranceOutsidePublicRoads>false</insuranceOutsidePublicRoads>
               <spontaneousCombustionRisk>false</spontaneousCombustionRisk>
            </insurance>
         </insurance>
            <insurant xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <insurant xmlns="">
               <taxpayerIdentificationNumber/>
               <address>
                  <country>Россия</country>
                  <region>{$c['insurer']['address_register_city']}</region>
                  <district>{$c['insurer']['address_register_city']}</district>
                  <city>{$c['insurer']['address_register_city']}</city>
                  <place/>
                  <street>{$c['insurer']['address_register_street']}</street>
                  <building>{$c['insurer']['address_register_house']}</building>
               </address>
               <flagrantViolation>false</flagrantViolation> <!--не заполняеться-->
               <insurantDateBirth>{$c['insurer']['birthdate']}T00:00:00</insurantDateBirth>
               <insurantStatus/>
               <insurantName>{$c['insurer']['fio'][1]}</insurantName>
               <insurantsurname>{$c['insurer']['fio'][0]}</insurantsurname>
               <insurantType>Физическое лицо</insurantType>
               <passportNumber>{$c['insurer']['doc_number']}</passportNumber>
               <ID/>
               <scoreRateServiceName/>
               <passportSerialNumber>{$c['insurer']['doc_serie']}</passportSerialNumber>
               <registration/>
               <patronymic>{$c['insurer']['fio'][2]}</patronymic>
               <haveChildren/>
               <KBM/> <!--не заполняеться-->
               <married/>
               <requestBKI>false</requestBKI>
               <scoringRating>0</scoringRating>
               <surname>{$c['insurer']['fio'][0]}</surname>
               <driversLicenseSerialNumber/>
               <driversLicenseNumber/>
               <gender>{$c['insurer']['sex']}</gender>
            </insurant>
         </insurant>
            <kasko xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <kasko xmlns="">
               
              <operationPeriodList>
                  <endDate>2020-10-24T10:57:31</endDate>
                  <startDate>2019-10-25T10:57:31</startDate>
               </operationPeriodList>
               <operationPeriodNumber>1</operationPeriodNumber>
               <operationPeriodSize>12</operationPeriodSize>
            </kasko>
         </kasko>
            <ns xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <ns xmlns="">
               <previousInsuaranceBonus>0</previousInsuaranceBonus> <!--не заполняеться-->
               <previousRegressLossesList>false</previousRegressLossesList>
               <lossesOnPreviousContractNumber>0</lossesOnPreviousContractNumber>
               <lossesOnPreviousContractSum>0</lossesOnPreviousContractSum>
               <aggregateAmount>false</aggregateAmount>
               <baseRatePercent>0</baseRatePercent>
               <bonusValue>0</bonusValue>
               <bonusValueCalculation>false</bonusValueCalculation>
               <c2c4>0</c2c4>
               <calculatedFranchise>{$c['franchise']}</calculatedFranchise>
               <commissionMultiplier>1</commissionMultiplier>
               <currentKBM>0</currentKBM>
               <finalRate>1</finalRate>
               <franchiseSize>{$c['franchise']}</franchiseSize>
               <insuaranceBonus>0</insuaranceBonus> <!--не заполняеться-->
               <KBM>0</KBM> <!--не заполняеться-->
               <kpark>1</kpark>
               <parkLegalEntities/>
               <risk>true</risk>
               <sumSize>{$c['ns']['sum']}</sumSize>
               <typeNs>паушальная система</typeNs>
               <underwritingDiscount>1</underwritingDiscount>
            </ns>
         </ns>
            <owner xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <owner xmlns="">
               <taxpayerIdentificationNumber/>
               <address>
                  <region>{$c['owner']['address_register_city']}</region>
                  <district>{$c['owner']['address_register_city']}</district>
                  <city>{$c['owner']['address_register_city']}</city>
                  <place/>
                  <street>{$c['owner']['address_register_street']}</street>
                  <building>{$c['owner']['address_register_house']}</building>
               </address>
               <name>{$c['owner']['fio'][1]}</name>
               <ID/>
               <ownerDateBirth>{$c['owner']['birthdate']}T00:00:00</ownerDateBirth>
               <passportNumber>{$c['owner']['doc_number']}</passportNumber>
               <scoreRateServiceName/>
               <passportSerialNumber>{$c['owner']['doc_serie']}</passportSerialNumber>
               <patronymic>{$c['owner']['fio'][2]}</patronymic>
               <requestBKI>false</requestBKI>
               <scoringRating>0</scoringRating>
               <surname>{$c['owner']['fio'][0]}</surname>
               <driversLicenseSerialNumber/>
               <driversLicenseNumber/>
               <gender>{$c['owner']['sex']}</gender>
               <haveChildren/>
               <KBM/> <!--не заполняеться-->
               <married>false</married>
            </owner>
         </owner>
            <salesman xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <salesman xmlns="">
               <unit>0890 Территориальное агенство № 9</unit>
            </salesman>
         </salesman>
            <theft xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <theft xmlns="">
               <previousInsuaranceBonus>0</previousInsuaranceBonus>
               <previousRegressLossesList>false</previousRegressLossesList>
               <lossesOnPreviousContractNumber>0</lossesOnPreviousContractNumber>
               <lossesOnPreviousContractSum>0</lossesOnPreviousContractSum>
               <aggregateAmount>false</aggregateAmount>
               <avaragePayment>0</avaragePayment>
               <baseRatePercent>0</baseRatePercent>
               <bonusValue>0</bonusValue>
               <bonusValueCalculation>false</bonusValueCalculation>
               <c2c4>0</c2c4>
               <calculatedFranchise>{$c['franchise']}</calculatedFranchise>
               <commissionMultiplier>1</commissionMultiplier>
               <currentKBM>0</currentKBM>
               <finalRate>0</finalRate>
               <franchiseSize>{$c['franchise']}</franchiseSize>
               <frequency>0</frequency>
               <insuaranceBonus>0</insuaranceBonus>
               <KBM>0</KBM>
               <kpark>1</kpark>
               <parkLegalEntities>0</parkLegalEntities>
               <partialRisk>false</partialRisk>
               <risk>{$this->getBool(isset($c['risks']['hijacking_id']['id']))}</risk>
               <sumSize>{$c['risks']['hijacking_id']['sum']}</sumSize>
               <underwritingDiscount>1</underwritingDiscount>
            </theft>
         </theft>
         <ts xmlns="http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV/param">
            <ts xmlns="">
               <immobilizerName/>
               <immobilizerType/>
               <trailer/>
               <admittedToDriveNumber>{$c['drivers']['count']}</admittedToDriveNumber>
               <seriesSTS/>
               <numberSTS/>
               <brand>{$c['object']['brand']}</brand>
               <brand1>{$c['object']['brand']}</brand1>
                {$c['drivers']['xml']}
               <capacity>0</capacity>
               <yearOfCompletionOfModification>0</yearOfCompletionOfModification>
               <yearOfReleaseOfModification>0</yearOfReleaseOfModification>
               <credit>{$this->getBool(isset($c['bank_id']))}</credit>
               <creditPeriod>0</creditPeriod>
               <engineVolume>0</engineVolume>
               <firstSaleDate>2019-05-30T00:00:00</firstSaleDate>
               <followToRegistrationHome>false</followToRegistrationHome>
               <harmNumber>0</harmNumber>
               <instantNumberOfVehicles>0</instantNumberOfVehicles>
               <mileage>15</mileage>
               <model>{$c['object']['mark']}</model>
               <multidriveType/>
               <id>0</id>
               <newTs>true</newTs>
               <numberOfVehicleRetrospective>0</numberOfVehicleRetrospective>
               <numberPTS>801493</numberPTS>
               <operationDays>0</operationDays>
               <otherRisk>false</otherRisk>
               <ownerType>{$c['owner']['type']}</ownerType>
               <permissibleGrossWeight>0</permissibleGrossWeight>
               <power>{$c['object']['power']}</power>
               <priceOfModificationFrom>0</priceOfModificationFrom>
               <priceOfModificationTo>0</priceOfModificationTo>
               <priceOfNew>0</priceOfNew>
               <registrationDocuments>true</registrationDocuments>
               <seatsNumber>{$c['object']['seats_num']}</seatsNumber>
               <seriesPTS>{$c['object']['docserie']}</seriesPTS>
               <termInsuranceDays>0</termInsuranceDays>
               <termInsuranceMonth>{$c['insurance_month']}</termInsuranceMonth>
               <category/>
               <typeTs>АВИ | Автомобиль внедорожный иностранный</typeTs>
               <valueTs>{$c['object']['price']}</valueTs>
               <vin>{$c['object']['vin']}</vin>
               <warrantyStatus>false</warrantyStatus>
               <year>{$c['object']['car_year']}</year>
               <yearNumber>1</yearNumber>
               <rent>false</rent>
               <ownerDateBirth/>
               <tenure>01.01.1900T00:00:00</tenure>
               <numberOfDoors>{$c['object']['doors']}</numberOfDoors>
               <numberOfOwners>0</numberOfOwners>
            </ts>
         </ts>  
    </ns1:RTKVRequest>

XML;
    }

    protected function getBool($param){
        if($param =='1' || (bool)$param == true)
            return 'true';
        else
            return 'false';
    }
}
