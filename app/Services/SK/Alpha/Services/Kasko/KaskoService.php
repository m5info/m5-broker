<?php

namespace App\Services\SK\Alpha\Services\Kasko;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\File;
use App\Repositories\FilesRepository;

class KaskoService implements ProductServiceInterface
{

    public $api;

    public function __construct(KaskoApi $api)
    {
        $this->api = $api;
    }

    public function temp_calc(ContractsCalculation $calc)
    {

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';


        $result = $this->api->calc_kasko($calc);
        ///////

        $response->state = true;
        $response->error = '';
        $response->sk_key_id = '12312';
        $response->statys_id = 1;
        $response->payment_total = 5173.21;
        $response->msg = 'ТБ: 3998.00; КТ: 2.00; КБМ: 0.50; КВС: 0.96; КС: 1.00; КП: 1.00; КМ: 1.40; КПР: 1.00; КН: 1.00;';

        //////////


        return $response;

    }


    public function calc(ContractsCalculation $calc)
    {

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';


        $result = $this->api->calc_kasko($calc);

//        dd($result);
        //die();
        if (isset($result->calculation->calculation->checkResult) && $result->calculation->calculation->checkResult =='ok') {
            if ($result->calculation->calculation->commentList == '') {

                $response->state = true;
//dump($result->answer->refCalcId);
                $response->payment_total = (string)$result->bonus->bonus->totalSize;
                $response->sk_key_id = (string)$result->DecisionID;
                $response->msg = "1231test";

                $premiums = [];

                if($result->calculation->calculation->calcResultList)

                foreach ($result->calculation->calculation->calcResultList as $res) {
                    if($res->resultOrder == 1)
                        $premiums[$res->resultCode][$res->resultName] = $res->resultValue;
                }

                $response->payments_sizes = \GuzzleHttp\json_encode($premiums);
                $response->statys_id = 1;
            }else {
                $response->error = $result->calculation->calculation->commentList;
            }
        }

        return $response;
    }


    public function release(ContractsCalculation $calc)
    {


        // die('release');
        $contract = $calc->contract;

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->bso_id = $contract->bso_id;
        $response->statys_id = 0; //Статус договора

        $result = $this->api->release_kasko($calc);

        if ($result->state) {

            $answer = $result->answer;

            $response->state = true;
            $response->statys_id = 4;  //Выпущен
            $response->answer = $answer;

            $calc->update(['sk_key_id' => (string)$answer->contract_id]);

        } else {
            $response->error = $result->error;
        }

        return $response;

    }

    public function check_status(ContractsCalculation $calc)
    {
        // TODO: Implement check_status() method.
    }


    public function get_files(ContractsCalculation $calc)
    {

// ответ от GetPrintedFormsKasko
//        {#1631
//            +"PrintedForm": array:4 [
//            0 => {#2519
//            +"PrintedFormId": "688"
//            +"PrintedFormName": "Счёт КАСКО"
//    }
//    1 => {#2520
//            +"PrintedFormId": "238"
//            +"PrintedFormName": "Печатная форма полиса КАСКО"
//    }
//    2 => {#2521
//            +"PrintedFormId": "370"
//            +"PrintedFormName": "Квитанция А7"
//    }
//    3 => {#2522
//            +"PrintedFormId": "371"
//            +"PrintedFormName": "Квитанция А7 (копия)"
//    }
//  ]
//}

        $contract = $calc->contract;
        $payment = $contract->get_payment_first();

        $templates = [
            '688' => 'Счёт КАСКО',
            '238' => 'Печатная форма полиса КАСКО',
            '370' => 'Квитанция А7',
        ];

        $filesRepository = new FilesRepository();

        foreach ($templates as $form_id => $templ) {
            $answer = $this->api->get_forms_osago($calc, $form_id);

            if ($answer->state == true) {
                $document = $answer->form;

                if (isset($document) && isset($document->Content)) {
                    $File_n = 'pdf';
                    $file_contents = $document->Content;
                    $path = storage_path('app/' . Contracts::FILES_DOC . "/{$contract->id}/");

                    if (!is_dir(($path))) {
                        mkdir(($path), 0777, true);
                    }

                    $file_name = uniqid();

                    file_put_contents($path . $file_name . '.' . $File_n, $file_contents);

                    $file = File::create([
                        'original_name' => $templ . '.' . $File_n,
                        'ext' => $File_n,
                        'folder' => Contracts::FILES_DOC . "/{$contract->id}/",
                        'name' => $file_name,
                        'user_id' => auth()->id()
                    ]);

                    $contract->masks()->save($file);

                }
            }
        }

        return true;

    }

}