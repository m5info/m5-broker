<?php

namespace App\Services\SK\Alpha\Services\Osago;

use App\Models\Contracts\ContractsCalculation;
use App\Models\Contracts\KbmCalculation;
use App\Models\Contracts\OnlineCalculationLogs;
use App\Services\Kbm\Alfa\Calc;
use App\Services\SK\Alpha\Clients\DigestSoapClient;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Mockery\Exception;


class OsagoApi
{

    public $url;
    public $login;
    public $password;
    public $errors = [];


    public function __construct($url, $login, $password)
    {
        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
    }

    public function auth($method, $json, $type = "POST")
    {

        $curl = curl_init();
        $headers = [];

        $headers[] = 'Accept: */*';
        $headers[] = 'Content-Type: application/json';

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url . $method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_HEADER => 1,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_POSTFIELDS => $json ? \GuzzleHttp\json_encode($json) : null,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        $headers = explode("\n", $response);
        $nonce_www_auth = null;

        foreach ($headers as $header){
            $exploded_header = explode(':', $header);
            if (isset($exploded_header[0]) && $exploded_header[0] == 'WWW-Authenticate'){
                $nonce_www_auth = $header;
            }
        }

        if (!$nonce_www_auth){
            $nonce_www_auth = $headers[8];
        }


        // WWW-Authenticate
        $nonce = stristr($nonce_www_auth, 'nonce="');
        $nonce = str_replace('nonce="', '', $nonce);
        $nonce = trim(str_replace('"', '', $nonce));
        $qop = 'auth';

        $ha1 = md5("{$this->login}:alfastrah.ru:{$this->password}");
        $ha2 = md5("$type:$method");
        $cnonce = 'nU4eIL3w';
        $final_hash = md5("$ha1:$nonce:00000001:$cnonce:$qop:$ha2");
        curl_close($curl);

        $hashs_data = [
            'method' => $method,
            'json' => $json,
            'type' => $type,
            'final_hash' => $final_hash,
            'nonce' => $nonce,
            'cnonce' => $cnonce,
        ];

        return $hashs_data;
    }

    public function send($method, $params, $type = 'POST')
    {
        $auth = $this->auth($method, $params, $type);

        $curl = curl_init();
        $headers = [];
        $headers[] = "Authorization: Digest username=\"{$this->login}\", realm=\"alfastrah.ru\", nonce=\"{$auth['nonce']}\", uri=\"{$auth['method']}\", cnonce=\"{$auth['cnonce']}\", nc=\"00000001\", qop=\"auth\", response=\"{$auth['final_hash']}\"";
        $headers[] = "Accept: */*";
        $headers[] = "Content-type: application/json";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url . $auth['method'],
            CURLOPT_RETURNTRANSFER => 10,
//            CURLOPT_HEADER => 1,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $auth['type'],
            CURLOPT_POSTFIELDS => $auth['json'] ? \GuzzleHttp\json_encode($auth['json']) : null,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false
        ));

        $response = curl_exec($curl);
//        dump($this->url . $auth['method']);
//        dump($auth['json'] ? \GuzzleHttp\json_encode($auth['json']) : null);
//        dump($response);
        curl_close($curl);


        OnlineCalculationLogs::create([
            'contract_id' => 11229,
            'action' => $method,
            'data_send' => is_array($params) ? json_encode($params) : $params,
            'data_response' => is_array($response) ? json_encode($response) : $response,
        ]);


        return \GuzzleHttp\json_decode($response);

    }


    /* ************************************************************
     * API OPERATION METHODS
     **************************************************************/

    public function calc_osago(ContractsCalculation $calc)
    {
        $result = (object)['state' => false, 'answer' => [], 'error' => ''];

        $param = $this->getCalcParams($calc);

        if (!empty($this->errors)) {
            $result->error = implode(';', $this->errors);
            return $result;
        }

        try {
            $answer = $this->send('/wapi/osago/calculation', $param);

            if (isset($answer->info) && $answer->info){
                $this->errors[] = $answer->info;
                if (isset($answer->sub_errors) && $answer->sub_errors){
                    foreach ($answer->sub_errors as $sub_error){
                        $this->errors[] = $sub_error->message;
                    }
                }
            }else{
                $result->answer = $answer;
                $result->state = true;
            }

            if (isset($answer->calculation_details) && $details = $answer->calculation_details){
                if (!$details->success){
                    $result->state = false;
                    $this->errors[] = $details->popup_message;
                }
            }

            if (!empty($this->errors)) {
                $result->error = implode(';', $this->errors);
            }

        } catch (\Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }


    public function releaseEosago(ContractsCalculation $calc)
    {
        $result = (object)['state' => false, 'answer' => [], 'error' => ''];

        $param = $this->getEReleaseParams($calc);
        $osago_uuid = $calc->sk_key_id;
        $contract = $calc->contract;

        $answer = $this->send("/wapi/osago/contract/{$osago_uuid}/eosago", $param);
        $answerState = $this->send("/wapi/osago/contract/{$osago_uuid}/status", null, 'GET');

        if (isset($answer->info) && $answer->info){
            $result->error = $answer->info;
            if (isset($answer->sub_errors) && $answer->sub_errors){
                foreach ($answer->sub_errors as $sub_error){
                    $this->errors[] = $sub_error->message;
                }
                if (!empty($this->errors)) {
                    $result->error .= implode(';', $this->errors);
                }
            }
        }

        if (isset($answerState->info) && $answerState->info){
            $result->error .= $answerState->info;
        }
        if (isset($answerState->status) && $answerState->status == 'ERROR'){
            if (isset($answerState->comment_list) && $answerState->comment_list){
                foreach ($answerState->comment_list as $sub_error){
                    $this->errors[] = $sub_error;
                }
                if (!empty($this->errors)) {
                    $result->error .= implode(';', $this->errors);
                }
            }
        }

        $answerPay = $this->send("/wapi/osago/payment/{$osago_uuid}", ['email' => $contract->insurer->email]);

        if(isset($answerState->status) && $answerState->status == 'SUCCESS'){
            $result->state = true;
            $result->answer = [
                'answerState' => $answerState,
                'answerPay' => $answerPay
            ];
        }

        return $result;
    }

    public function releaseOsago(ContractsCalculation $calc)
    {
        $result = (object)['state' => false, 'answer' => [], 'error' => ''];

        $param = $this->getReleaseParams($calc);

        if (!empty($this->errors)) {
            $result->error = implode(';', $this->errors);
            return $result;
        }

        $osago_uuid = $calc->sk_key_id;

        $answerBso = $this->send("/wapi/osago/contract/{$osago_uuid}/bso", $param, 'POST');

        if (isset($answerBso->info) && $answerBso->info){
            $this->errors[] = $answerBso->info;
            if (isset($answerBso->sub_errors) && $answerBso->sub_errors){
                foreach ($answerBso->sub_errors as $sub_error){
                    $this->errors[] = $sub_error->message;
                }
            }
        }

        $answerState = $this->send("/wapi/osago/contract/{$osago_uuid}/status", null, 'GET');
        if (isset($answerState->status) && $answerState->status == 'ERROR'){
            if (isset($answerState->comment_list) && $answerState->comment_list){

                $this->errors[] = $answerState->comment_list[0];

                if (!empty($this->errors)) {
                    $result->error = implode(';', $this->errors);
                }
                return $result;
            }
        }

        $answer = null;
        if (isset($answerState->status) && $answerState->status == 'SUCCESS'){
            $answer = $this->send("/wapi/osago/contract/{$osago_uuid}", ['osago_uuid' => $osago_uuid], 'PATCH');
        }

        dump($answerBso);
        dump($answerState);
        dd($answer);

        return $result;
    }


    /* ************************************************************
     * COLLECT PARAM METHODS
     **************************************************************/


    public function getEReleaseParams(ContractsCalculation $calc)
    {
        $result = [];

        $auto = $calc->object_insurer_auto;
        $result['maintenance_card'] = [
            "card_number" => null,
            //            "card_series" => "213ABC",
            "card_type" => "Диагностическая карта",
            "upcoming_maintenance_date" => null
        ];

        if(isset($auto->dk_number) && $auto->dk_number){
            $result['maintenance_card'] = [
                "card_number" => $auto->dk_number,
//            "card_series" => "213ABC",
                "card_type" => "Диагностическая карта",
                "upcoming_maintenance_date" => $auto->dk_date
            ];
        }

        return $result;
    }


    public function getReleaseParams(ContractsCalculation $calc)
    {
        $contract = $calc->contract;

        $result = [];

        $auto = $calc->object_insurer_auto;
        $result['contract_series'] = $contract->bso ? $contract->bso->bso_serie->bso_serie : '';
        $result['contract_number'] = $contract->bso->bso_number;
        if(isset($auto->dk_number) && $auto->dk_number){
            $result['maintenance_card'] = [
                "card_number" => $auto->dk_number,
//            "card_series" => "213ABC",
                "card_type" => "Диагностическая карта",
                "upcoming_maintenance_date" => $auto->dk_date
            ];
        }

        return $result;
    }


    public function getCalcParams(ContractsCalculation $calc)
    {
        $contract = $calc->contract;
        $driversRestriction = $calc->contract->drivers_type_id == 1 ? false : true;

        $result = [];
        $result['agent'] = $this->get_agent($calc);
        $result['insurance_contract'] = [
            'begin_date' => date('Y-m-d H:i:s', strtotime($contract->begin_date)),
            'drivers_restriction' => $driversRestriction,
            'operation_periods' => $this->get_period_list($calc),
            'follow_to_registration' => false,
            'violations' => false,
            'is_rent' => false,
        ];
        if ($contract->is_prolongation){
            $result['insurance_contract']['previous_contract_series'] = $contract->prev_policy_serie;
            $result['insurance_contract']['previous_contract_number'] = $contract->prev_policy_number;
        }
        if ($driversRestriction){
            $result['drivers'] = $this->get_drivers($calc);
        }
        $result['transport_insurer'] = $this->get_subject('insurer', $calc);
        $result['transport_owner'] = $this->get_subject('owner', $calc);
        $result['vehicle'] = $this->get_transport($calc);

//        if ($sk_purpose = $calc->sk_purpose){
//            $result['insurance_contract']['purpose_of_use'] = $sk_purpose->sk_title;
//        }else{
//            $this->errors[] = 'Не синхронизирован справочник: Цели использования';
//        }
//dd($result);
        return $result;
    }


    public function get_period_list(ContractsCalculation $calc, $method = 'calc')
    {
        $contract = $calc->contract;

        if ($method == 'calc') {
            $period_list[] = [
                'start_date' => date('Y-m-d', strtotime($contract->begin_date)),
                'end_date' => date('Y-m-d', strtotime($contract->end_date))
            ];
        } elseif ($method == 'release') {
            $period_list[] = [
                'BeginDate' => date('Y-m-d', strtotime($contract->begin_date)),
                'EndDate' => date('Y-m-d', strtotime($contract->end_date))
            ];
        }

        return $period_list;
    }

    public function get_purpose(ContractsCalculation $calc)
    {

        $purpose_title = '';

        if ($purpose = $calc->sk_purpose) {
            $purpose_title = $purpose->sk_title;
        } else {
            $this->errors[] = 'Цель использования не найдена';
        }

        return $purpose_title;
    }

    public function get_transport(ContractsCalculation $calc)
    {

        $transport = [
            'registration_country' => 'Россия',
        ];

        if ($auto = $calc->object_insurer_auto) {

            $doc_type = '';
            switch ((int)$auto->doc_type){
                case 0;
                    $doc_type = 'Паспорт ТС';
                    break;
                case 1;
                    $doc_type = 'Свидетельство о регистрации ТС';
                    break;
                case 2;
                    $doc_type = 'Электронный паспорт ТС';
                    break;
            }

            $transport['power_hp'] = (float)$auto->power;
            $transport['use_with_trailer'] = $auto->is_trailer ? true : false;
            $transport['production_year'] = $auto->car_year;
            $transport['identity'] = [
                'body_number' => $auto->body_number ?: null,
                'licence_plate' => $auto->reg_number,
                'vin' => $auto->vin
            ];
            $transport['registration_documents'] = [
                [
                    'document_number' => $auto->docnumber,
                    'document_series' => $auto->docserie,
                    'document_type' => $doc_type,
                    'issue_date' => $auto->docdate,
                ]
            ];

        } else {
            $this->errors[] = 'Авто не найдено';
        }

        if ($sk_category = $calc->sk_category) {
            $transport['category'] = $sk_category->sk_title;
        } else {
            $this->errors[] = 'Не синхронизирован справочник: Категория';
        }

        if ($sk_mark = $calc->sk_mark) {
            $transport['mark'] = $sk_mark->sk_title;
        } else {
            $this->errors[] = 'Не синхронизирован справочник: Марки';
        }

        if ($sk_model = $calc->sk_model) {
            $transport['model'] = $sk_model->sk_title;
        } else {
            $this->errors[] = 'Не синхронизирован справочник: Модели';
        }

        return $transport;
    }

    public function get_drivers(ContractsCalculation $calc)
    {
        $drivers = [];

        if ($calc->drivers) {

            foreach ($calc->drivers as $driver) {

                $fio = $driver->get_fio();

                $driver_data = [
                    'birth_date' => $driver->birth_date,
                    'country' => 'Россия',
                    'driver_document' =>
                        [
                            'document_number' => $driver->doc_num,
                            'document_series' => $driver->doc_serie,
                            'document_type' => (int)$driver->foreign_docs ? 'Водительское удостоверение иностранного государства' : 'Водительское удостоверение РФ',
                            'driving_experience_date' => $driver->exp_date,
                            'issue_date' => $driver->doc_date,
                        ],
                    'first_name' => $fio['first_name'],
                    'last_name' => $fio['last_name'],
                    'middle_name' => $fio['middle_name'],
                    'sex' => $driver->sex ? 'FEMALE' : 'MALE',
                ];

                if($old_license_data = $driver->old_license){

                    $previous_driver_data = [
                        "birth_date" => $driver->birth_date,
                        "country" => "Россия",
                        "driver_document" => [
                            "document_series" => $old_license_data->doc_series,
                            "document_number" => $old_license_data->doc_number,
                            "document_type" => "Водительское удостоверение РФ",
                            "driving_experience_date" => $driver->exp_date,
                            "end_date" => $old_license_data->doc_date_end,
                            "issue_date" => $old_license_data->doc_date_issue
                        ],
                        "first_name" => $old_license_data->first_name,
                        "last_name" => $old_license_data->middle_name,
                        "middle_name" => $old_license_data->last_name
                    ];

                    if(!(int)$old_license_data->doc_date_end){
                        $this->errors[] = 'Добавьте дату окончания предыдущих прав!';
                    }

                    if(!(int)$old_license_data->doc_date_issue){
                        $this->errors[] = 'Добавьте дату выдачи предыдущих прав!';
                    }

                    $drivers[] = array_merge(['driver_data' => $driver_data], ['previous_driver_data' => $previous_driver_data]);

                }else{
                    $drivers[]['driver_data'] = $driver_data;
                }

            }

            if ($driver->doc_date == 0){
                $this->errors[] = 'Заполните дату выдачи в/у!';
            }

            if ($driver->exp_date == 0){
                $this->errors[] = 'Заполните дату опыта водителя!';
            }


        } else {
            $this->errors[] = 'Водител(ь/и) не найден(ы)';
        }

        return $drivers;
    }


    public function get_agent(ContractsCalculation $calc)
    {

        $contract = $calc->contract;

        $agent = [];

        if ($agent_info = $contract->getAgentSettings($calc)) {

            $agent = [
                'agent_contract_id' => $agent_info->agent_id,
                'channel_sale_id' => $agent_info->сhannel_sale_id,
                'department_id' => $agent_info->department_id,
                'manager_id' => $agent_info->manager_id,
                'signer_id' => $agent_info->signer_id,
            ];

        } else {
            $this->errors[] = 'Агент не найден';
        }

        return $agent;
    }

    public function get_subject($subject_name, ContractsCalculation $calc)
    {

        $contract = $calc->contract;
        $subject = [];

        if ($contract_subject = $contract->{$subject_name}) {

            $contract_subject_info = $contract_subject->get_info();
            // если физик
            if ($contract_subject->type == 0) {

                $fio = explode(' ', $contract_subject->title);
                $phone = setPhoneNumberFormat(parsePhoneNumber($contract_subject->phone), '8dddddddddd');

                $subject['person'] = [
                    'person_data' => [
                        'addresses' => [
                            [
                                'address_type' => 'Регистрации',
                                'apartment_number' => $contract_subject_info->address_register_flat ?: null,
                                'building' => $contract_subject_info->address_register_block ?: null,
                                'country' => 'Россия',
                                'house_number' => $contract_subject_info->address_register_house,
                                'location_place' => $contract_subject_info->address_register_city,
                                'region' => $contract_subject_info->address_register_region,
                                'street' => $contract_subject_info->address_register_street ?: null,
                                'zip' => $contract_subject_info->address_register_zip,
                                ],
                            [
                                'address_type' => 'Фактический',
                                'apartment_number' => $contract_subject_info->address_fact_flat ?: null,
                                'building' => $contract_subject_info->address_fact_block ?: null,
                                'country' => 'Россия',
                                'house_number' => $contract_subject_info->address_fact_house,
                                'location_place' => $contract_subject_info->address_fact_city,
                                'region' => $contract_subject_info->address_fact_region,
                                'street' => $contract_subject_info->address_fact_street ?: null,
                                'zip' => $contract_subject_info->address_fact_zip,
                                ]
                            ],
                        'birth_date' => $contract_subject_info->birthdate,
                        'country' => 'Россия',
                        'email' => $contract_subject->email,
                        'first_name' => isset($fio[1]) ? $fio[1] : $fio[0],
                        'last_name' => $fio[0],
                        'middle_name' => isset($fio[2]) ? $fio[2] : $fio[0],
                        'person_document' => [
                                'document_number' => $contract_subject->doc_number,
                                'document_series' => $contract_subject->doc_serie,
                                'document_type' => 'Паспорт гражданина РФ',
                                'issue_date' => $contract_subject_info->doc_date,
                            ],
                        'phones' => [
                                [
                                    'phone_number' => $phone,
                                    'phone_type' => 'Сотовый',
                                ],
                            ],
                        'sex' => $contract_subject_info->sex ? 'FEMALE' : 'MALE',
                    ]
                ];

                if ($contract_subject_info->doc_date == 0){
                    $this->errors[] = 'Заполните дату выдачи документа!';
                }

                if (!$contract_subject->email){
                    $this->errors[] = 'Заполните email!';
                }

            } else {

                $phone = setPhoneNumberFormat(parsePhoneNumber($contract_subject->phone), '8dddddddddd');

                $subject['organization'] = [
                    'addresses' => [
                        [
                            'address_type' => 'Регистрации',
                            'apartment_number' => $contract_subject_info->address_register_flat ?: null,
                            'building' => $contract_subject_info->address_register_block ?: null,
                            'country' => 'Россия',
                            'house_number' => $contract_subject_info->address_register_house,
                            'location_place' => $contract_subject_info->address_register_city,
                            'region' => $contract_subject_info->address_register_region,
                            'street' => $contract_subject_info->address_register_street,
                        ],
                        [
                            'address_type' => 'Фактический',
                            'apartment_number' => $contract_subject_info->address_fact_flat ?: null,
                            'building' => $contract_subject_info->address_fact_block ?: null,
                            'country' => 'Россия',
                            'house_number' => $contract_subject_info->address_fact_house,
                            'location_place' => $contract_subject_info->address_fact_city,
                            'region' => $contract_subject_info->address_fact_region,
                            'street' => $contract_subject_info->address_fact_street,
                        ]
                    ],
                    "country" => "Россия",
                    "email" => $contract_subject->email,
                    "full_name" => $contract_subject_info->title,
                    "inn" => $contract_subject_info->inn,
                    "kpp" => $contract_subject_info->kpp,
                    "ogrn" => $contract_subject_info->ogrn,
                    "phones" => [
                        [
                            "phone_number" => $phone,
                            "phone_type" => "Сотовый"
                        ]
                    ],
                    'registration_document' => [
                        "document_number" => $contract_subject_info->doc_number,
                        "document_series" => $contract_subject_info->doc_serie,
                        "document_type" => "Выписка из ЕГРЮЛ",
                    ],
                    "short_name" => $contract_subject_info->title
                ];
            }

        }

        return $subject;
    }

}
