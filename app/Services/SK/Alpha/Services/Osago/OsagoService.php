<?php

namespace App\Services\SK\Alpha\Services\Osago;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\File;
use App\Repositories\FilesRepository;
use stdClass;

class OsagoService implements ProductServiceInterface{

    public $api;

    public function __construct(OsagoApi $api){
        $this->api = $api;
    }

    public function temp_calc(ContractsCalculation $calc){
        // TODO: Implement temp_calc() method.
    }

    public function calc(ContractsCalculation $calc){

        $response = new stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';

        $result = $this->api->calc_osago($calc);

        if($result->state && isset($result->answer->coefficients)){

            $answer = $result->answer;
            $coeffs = $answer->coefficients;

            $response->state = true;
            $response->statys_id = 1;
            $response->answer = $answer;

            $response->payment_total = $answer->calculation_details ? (string)$answer->calculation_details->insurance_premium : '';
            $response->sk_key_id = (string)$answer->osago_uuid;

            $response->error = '';

//            unset($coeffs[0]);
//            unset($coeffs[1]);

            $response->msg = "";

            foreach($coeffs as $coeff){
                $response->msg .= "{$coeff->name}: {$coeff->value}; ";
            }

        }else{
            $response->error = $result->error;
        }

        return $response;
    }



    public function release(ContractsCalculation $calc){

        $contract = $calc->contract;

        $response = new stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';

        if($contract->is_epolicy == 1){ // Электронный
            $result = $this->api->releaseEosago($calc);
        }else{
            $result = $this->api->releaseOsago($calc);
            $response->bso_id = $contract->bso->id;
        }


        if($result->state){

            $response->state = true;
            $response->statys_id = 4; //Выпущен

            if($contract->is_epolicy == 1){
                if($result->state){

                    $bso = $contract->getElectronBSO((string)$result->answer['answerState']->insurance_contract->contract_series, (string)$result->answer['answerState']->insurance_contract->contract_number);

                    $bso->setBsoLog(0);
                    $response->answer = $result->answer['answerPay']->info ?? '';
                    $response->payment_link = $result->answer['answerPay']->payment_link ?? '';

                }

            }else{
                $bso = $contract->bso;
                $response->answer = $result->answer;
            }

            $response->bso_id = $bso->id;

        }else{
            $response->error = $result->error;
        }

        return $response;
    }


    public function check_status(ContractsCalculation $calc){

    }


    public function get_files(ContractsCalculation $calc){

        return true;
    }


}