<?php

namespace App\Services\SK\Alpha\Services\Dictionary;

use App\Interfaces\Services\SK\DictionaryServiceInterface;

class DictionaryService implements DictionaryServiceInterface {

    public $cache_json = __DIR__."/json.json";

    public $api;

    public function __construct(DictionaryApi $api){
        $this->api = $api;
    }

    public function get_api_purpose(){

        $response = [];
        $purposes = $this->api->get_purpose();
        if(isset($purposes)){
            foreach ($purposes as $key => $title){
                $response[] = [
                    'id'=> $key,
                    'title'=>(string)$title
                ];
            }
        }

        return $response;
    }




    public function get_api_categories(){

        $cats = $this->api->get_categories();
        $catsArray = [];
        if ($cats){
            foreach ($cats as $key => $cat){
                $catsArray[] = ['id' => $key, 'title' => $cat->category_name];
            }
        }

        return $catsArray;
    }




    public function get_marks_models(){

        set_time_limit(100);

        $marks = [];
        $models = [];


        if(!is_file($this->cache_json)) {

            $_marks = $this->api->get_marks();

            if ($_marks && count($_marks)){
                foreach ($_marks as $_mark){
                    $marks[] = [
                        'id' => (string) $_mark->mark_id,
                        'title' => (string) $_mark->mark,
                        'vehicle_categorie_sk_id' => (string) 'B - легковые',
                    ];

                    $_models = $this->api->get_models($_mark->mark_id);

                    if($_models && count($_models)){
                        foreach ($_models as $_model){
                            $models[] = [
                                'id' => (string) $_model->model_id,
                                'title' => (string) $_model->model,
                                'vehicle_mark_sk_id' => (string) $_mark->mark_id,
                                'vehicle_categorie_sk_id' => (string)'B - легковые',
                            ];
                        }
                    }
                }
            }

            $result = [
                'categories' => $this->api->get_categories(),
                'marks' => $marks,
                'models' => $models,
            ];

            file_put_contents($this->cache_json, \GuzzleHttp\json_encode($result));

            return $result;

        }else{

            return \GuzzleHttp\json_decode(file_get_contents($this->cache_json),true);
        }
    }
}