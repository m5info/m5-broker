<?php

namespace App\Services\SK\Alpha\Services\Dictionary;


use SoapClient;
use SoapHeader;
use SoapVar;

class DictionaryApi{


    public $url;
    public $login;
    public $password;

    public function __construct($url, $login, $password)
    {
        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
    }

    public function auth($method, $type = "GET")
    {
        set_time_limit(100);

        $curl = curl_init();
        $headers = [];

        $headers[] = 'Accept: */*';
        $headers[] = 'Content-Type: application/json';

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url . $method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_HEADER => 1,
            CURLOPT_TIMEOUT => 100,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        $headers = explode("\n", $response);

        // WWW-Authenticate
        $nonce_www_auth = stristr($headers[6], 'nonce="');
        $nonce = str_replace('nonce="', '', $nonce_www_auth);
        $nonce = trim(str_replace('"', '', $nonce));

        $qop = 'auth';

        $ha1 = md5("{$this->login}:alfastrah.ru:{$this->password}");
        $ha2 = md5("$type:$method");
        $cnonce = 'nU4eIL3w';
        $final_hash = md5("$ha1:$nonce:00001001:$cnonce:$qop:$ha2");
        curl_close($curl);

        $hashs_data = [
            'method' => $method,
            'type' => $type,
            'final_hash' => $final_hash,
            'nonce' => $nonce,
            'cnonce' => $cnonce,
        ];

        return $hashs_data;
    }

    public function send($method, $type = 'GET')
    {
        $auth = $this->auth($method, $type);

        $curl = curl_init();
        $headers = [];
        $headers[] = "Authorization: Digest username=\"{$this->login}\", realm=\"alfastrah.ru\", nonce=\"{$auth['nonce']}\", uri=\"{$auth['method']}\", cnonce=\"{$auth['cnonce']}\", nc=\"00001001\", qop=\"auth\", response=\"{$auth['final_hash']}\"";
        $headers[] = "Accept: */*";
        $headers[] = "Content-type: application/json";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url . $auth['method'],
            CURLOPT_RETURNTRANSFER => 10,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $auth['type'],
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return \GuzzleHttp\json_decode($response);
    }

    public function get_purpose(){
        return $this->send('/wapi/dictionary/purpose/names');
    }

    public function get_categories(){
        return $this->send('/wapi/dictionary/vehicle/category');
    }

    public function get_marks(){
        return $this->send('/wapi/dictionary/vehicle/mark');
    }

    public function get_models($markId){
        return $this->send("/wapi/dictionary/vehicle/mark/{$markId}/model");
    }



}