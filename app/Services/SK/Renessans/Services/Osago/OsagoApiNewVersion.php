<?php


namespace App\Services\SK\Renessans\Services\Osago;


use App\Models\Contracts\ContractsCalculation;

class OsagoApiNewVersion{

    public $url;
    public $login;
    public $password;
    public $subjects_keys = 2;
    public $driver_as_insurer_id = 0;
    public $owner_id = 0;
    public $insurer_id = 0;
    public $drivers_deals_array = [];
    public $methods_path = '/beta/link2api/eOsagoInsurance/1.0';
    public $errors = [];

    public function __construct($url, $login, $password){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

    }


    // калькуляция осаго
    public function calc_osago(ContractsCalculation $calc){

        $result = (object)['state' => false, 'answer' => [], 'error'=> ''];

        $params = $this->get_calc_params($calc);

        $calc = $this->send('/calculate', $params, 'POST');
dd($calc);
        if(isset($calc->isSuccess) && $calc->isSuccess == true){
            $result->state = true;
            $result->answer = $calc->model;
        }else{
            foreach($calc->messages as $message){
                if($message->path === 'autoEnginePowerCode'){
                    $result->error .= 'Мощность авто не соответствует справочнику автомобиля';
                }else {
                    $result->error .= $message->text;
                }
            }
        }

        if(!empty($this->errors)){ // ошибки со справочниками
            $result->error = implode(';', $this->errors);
        }

        return $result;
    }

    // выпуск осаго
    public function release_osago(ContractsCalculation $calc){

        $result = (object)['state' => false, 'answer' => [], 'error'=> ''];
        $contract = $calc->contract;

        $params = $this->get_release_params($calc);

        $calc_result = $this->send("/import?accountNumber={$calc->sk_key_id}", $params, 'POST');

        foreach($calc_result->messages as $message){
            $result->error .= $message->text;
        }

        if((isset($calc_result->isSuccess) && $calc_result->isSuccess == true) || $result->error === 'Невозможно выполнить действие в текущем состояние расчета.'){

            $payment_params = [
                'accountNumber' => $calc->sk_key_id,
                'successUrl' => "https://ya.ru",
                'failUrl' => "https://google.com",
            ];

            $payment_link = $this->send("/getPaymentLink", $payment_params, 'GET');

            if(isset($payment_link->isSuccess) && $payment_link->isSuccess == true){
                $result->state = true;
                $result->answer = $payment_link->paymentUrl;
                $result->statys_id = 3;

                $bso = $contract->getElectronBSO((string)$calc_result->model->deals[0]->policySeries, (string)$calc_result->model->deals[0]->policyNumber);
                $bso->setBsoLog(0);
            }
        }

        if(!empty($this->errors)){ // ошибки со справочниками
            $result->error = implode(';', $this->errors);
        }

        return $result;
    }

    public function check_status(ContractsCalculation $calc){
        $result = (object)['state' => false, 'files_list' => [], 'error'=> ''];

        $payment_params = [ 'accountNumber' => $calc->sk_key_id ];

        $check_status = $this->send("/getAccountStatus", $payment_params, 'GET');

        if(isset($check_status->isSuccess) && $check_status->isSuccess == true){

            $payment = $check_status->model->payments[0];
            if($payment->status == 'Paid'){
                $result->state = true;
                $result->files_list = $check_status->model->availableDocuments;
            }else {
                $result->error = 'Платеж не оплачен!';
            }

        }

        return $result;
    }


    public function get_file(ContractsCalculation $calc, $doc_code){

        $params = [
            'accountNumber' => $calc->sk_key_id,
            'printDocumentCode' => $doc_code,
        ];

        return $this->send("/printDocument", $params, 'GET');
    }

    public function getDealsParams(ContractsCalculation $calc)
    {
        $contract = $calc->contract;

//        $insurer = $contract->insurer;
//        $owner = $contract->owner;

        $result = [
            [
                'dealType' => 'eOSAGO',
                'dateStart' => date('d.m.Y', strtotime($contract->begin_date)),
                'isMultidrive' => ($contract->drivers_type_id == 1) ? true : false,
                'insurancePeriodTypeCode' => 'oneYear',
                'drivers' => $this->drivers_deals_array,
//                'insurant' => ['dealObjects' => 2],
                'insuranceObjects' => [
                    [
                        'auto' => ['dealObjects' => 1],
                        'owners' => [
                            ['dealObjects' => (int)$this->owner_id]
                        ]
                    ]
                ],
            ]
        ];

        return $result;
    }


    public function getDealsReleaseParams(ContractsCalculation $calc)
    {
        $contract = $calc->contract;

        $result = [
            [
                'dealType' => 'eOSAGO',
                'insurant' => ['dealObjects' => $contract->owner_id == $contract->insurer_id ? (int)$this->owner_id : (int)$this->insurer_id],
                'insuranceObjects' => [
                    [
                        'auto' => ['dealObjects' => 1],
                        'owners' => [
                            ['dealObjects' => (int)$this->owner_id]
                        ]
                    ]
                ],
            ]
        ];

        return $result;
    }

    /**
     * @param ContractsCalculation $calc
     * Получение subjects and insurer_object
     */
    public function getDealsObjectsParams(ContractsCalculation $calc)
    {
        $result = [];

        $result[] = $this->getVehicle($calc);
//        $result[] = $this->getSubjects($calc, 'insurer');
        $result[] = $this->getSubjects($calc, 'owner');

        $drivers_deals_array = [];
        if(!(int)$calc->contract->drivers_type_id){
            foreach ($calc->drivers as $driver){
                $drivers_deals_array[] = ['dealObjects' => $this->subjects_keys];

                $result[] = $this->getDrivers($driver);

                $this->subjects_keys++;
            }
        }

        $this->drivers_deals_array = $drivers_deals_array;

        return $result;
    }

    public function getDealsObjectsReleaseParams(ContractsCalculation $calc)
    {
        $result = [];
        $contract = $calc->contract;

        $result[] = $this->getReleaseVehicle($calc);
        $result[] = $this->getReleaseSubjects($calc, 'owner');
        if($contract->owner_id != $contract->insurer_id){
            $result[] = $this->getReleaseSubjects($calc, 'insurer');
        }

        // кажись водилы для выпуска не нужны
/*        $drivers_deals_array = [];
        if(!(int)$calc->contract->drivers_type_id){
            foreach ($calc->drivers as $driver){
                $drivers_deals_array[] = ['dealObjects' => $this->subjects_keys];

                $result[] = $this->getDrivers($driver);

                $this->subjects_keys++;
            }
        }

        $this->drivers_deals_array = $drivers_deals_array;*/

        return $result;
    }

    public function getReleaseVehicle(ContractsCalculation $calc){
        $autoInfo = $calc->object_insurer_auto;

        $result = [];
        $result['id'] = 1;
        $result['type'] = 'auto';
        $result['auto'] = [
            "diagnosticCard" => [
                'dateEnd' => date('d.m.Y', strtotime($autoInfo->dk_date)),
                'number' => $autoInfo->dk_number,
            ],
            "tsIdentityCard" => [
                'dateOfIssued' => date('d.m.Y', strtotime($autoInfo->docdate)),
                'number' => $autoInfo->docnumber,
                'series' => $autoInfo->docserie,
                'tsIdentityCardTypeCode' => (int)$autoInfo->doc_type ? 'sts' : 'pts', // еще есть epts
            ],
        ];

        return $result;
    }


    public function getVehicle(ContractsCalculation $calc)
    {
        $autoInfo = $calc->object_insurer_auto;

        $result = [];
        $result['id'] = 1;
        $result['type'] = 'auto';
        $result['auto'] = [
            "vin" => $autoInfo->vin,
            "registrationNumber" => $autoInfo->reg_number,
            "hasTrailer" => ($autoInfo->is_trailer == 1) ? true : false,
            "isTransitAuto" => false,
            "passengersQuantity" => 5,
            "autoYearOfIssueCode" => (string)$autoInfo->car_year,
            "autoEnginePowerCode" => (string)(int)$autoInfo->power,
        ];

        if($purpose = $calc->sk_purpose){
            $result['auto']['purposeOfUsingCode'] = $purpose->vehicle_purpose_sk_id;
        }else{
            $this->errors[] = 'Цель использования не найдена';
        }

        if($category = $calc->sk_category){
            $result['auto']['autoTypeCode'] = $category->vehicle_categorie_sk_id;
        }else{
            $this->errors[] = 'Не синхронизирован справочник: Категории';
        }

        if($mark = $calc->sk_mark){
            $result['auto']['autoBrandCode'] = $mark->vehicle_marks_sk_id;
        }else{
            $this->errors[] = 'Не синхронизирован справочник: Модель';
        }

        if($model = $calc->sk_model){
            $result['auto']['autoModelCode'] = $model->vehicle_models_sk_id;
        }else{
            $this->errors[] = 'Не синхронизирован справочник: Модель';
        }

        return $result;
    }


    public function getSubjects(ContractsCalculation $calc, $subjectType)
    {
        $result = [];
        $contract = $calc->contract;

        if($contract_subject = $contract->{$subjectType}){

            $id = (int)$this->driver_as_insurer_id ?: $this->subjects_keys++;

            if($subjectType == 'owner'){
                $this->owner_id = $id;
            }

            if($contract_subject->type == 0){// если физик
                $data_fl = $contract_subject->data_fl;

                $result['id'] = $id;
                $result['type'] = 'person';
                $result['person'] = [
                    "dateOfBirth" => date('d.m.Y', strtotime($data_fl->birthdate)),
                    "hasPersonalDataAgreement" => true,
//                    "registrationRegionCode" => "Moskva",
                    "addresses" => [
                        [
                            "country" => "Россия",
                            "region" => $data_fl->address_register_region_full,
                            "city" => $data_fl->address_register_city,
                            "street" => $data_fl->address_register_street,
                            "house" => $data_fl->address_register_house,
                            "zipCode" => $data_fl->address_register_zip,
                            "kladrId" => $data_fl->address_register_kladr,
//                            "fiasId" => "f64c75cd-a640-41ed-9893-c1aaef58e638",
//                            "streetFiasId" => "74779e0c-263e-45a0-b5dc-36ef11f45dba",
//                            "settlementFiasId" => "f64c75cd-a640-41ed-9893-c1aaef58e638", // поселок
//                            "regionFiasId" => "0c5b2444-70a0-4932-980c-b4dc0d3f02b5",
                        ]
                    ]
                ];
            }
        }

        return $result;
    }

    public function getReleaseSubjects(ContractsCalculation $calc, $subjectType)
    {
        $result = [];
        $contract = $calc->contract;

        if($contract_subject = $contract->{$subjectType}){

            $id = (int)$this->driver_as_insurer_id ?: $this->subjects_keys++;

            if($subjectType == 'owner'){
                $this->owner_id = $id;
            }elseif($subjectType == 'insurer'){
                $this->insurer_id = $id;
            }

            if($contract_subject->type == 0){// если физик
                $data_fl = $contract_subject->data_fl;

                $fio = explode(' ', $data_fl->fio);

                $result['id'] = $id;
                $result['type'] = 'person';
                $result['person'] = [
                    "dateOfBirth" => date('d.m.Y', strtotime($data_fl->birthdate)),
                    "lastName" => $fio[2],
                    "middleName" => $fio[1],
                    "name" => $fio[0],
                    "email1" => $contract_subject->email,
                    "phone1" => parsePhoneNumber($contract_subject->phone),
                    "identityDocument" => [
                        'dateIssue' => date('d.m.Y', strtotime($data_fl->doc_date)),
                        'issuedBy' => $data_fl->doc_info,
                        'number' => $data_fl->doc_number,
                        'series' => $data_fl->doc_serie,
                        'unitCode' => $data_fl->doc_office,
                    ],
                    "addresses" => [
                        [
                            "country" => "Россия",
                            "region" => $data_fl->address_register_region_full,
                            "city" => $data_fl->address_register_city,
                            "street" => $data_fl->address_register_street,
                            "house" => $data_fl->address_register_house,
                            "zipCode" => $data_fl->address_register_zip,
                            "kladrId" => $data_fl->address_register_kladr,
//                            "fiasId" => "f64c75cd-a640-41ed-9893-c1aaef58e638",
//                            "streetFiasId" => "74779e0c-263e-45a0-b5dc-36ef11f45dba",
//                            "settlementFiasId" => "f64c75cd-a640-41ed-9893-c1aaef58e638", // поселок
//                            "regionFiasId" => "0c5b2444-70a0-4932-980c-b4dc0d3f02b5",
                        ]
                    ]
                ];
            }
        }

        return $result;
    }


    public function getDrivers($driver)
    {
        $drivers = [];

            if((int)$driver->same_as_insurer){
                $this->driver_as_insurer_id = $this->subjects_keys;
            }

//            if(!(int)$driver->same_as_insurer){
                $fio = $driver->get_fio();

                $drivers['id'] = (int)$this->subjects_keys;
                $drivers['type'] = 'driver';
                $drivers['driver'] = [
                    "dateOfBirth" => date('d.m.Y', strtotime($driver->birth_date)),
                    "lastName" => $fio['last_name'],
                    "name" => $fio['first_name'],
                    "driverLicenseNumber" => $driver->doc_num,
                    "driverLicenseSeries" => $driver->doc_serie,
                    "drivingExperienceDateStart" => date('d.m.Y', strtotime($driver->exp_date)),
                ];
//            }


        return $drivers;
    }

    public function get_calc_params(ContractsCalculation $calc)
    {
        $params = [];
        $params['version'] = '1.0';
        $params['model'] = [
            'dealsObjects' => $this->getDealsObjectsParams($calc),
            'deals' => $this->getDealsParams($calc),
        ];

        return $params;
    }

    public function get_release_params(ContractsCalculation $calc)
    {
        $params = [];
        $params['version'] = '1.0';
        $params['model'] = [
            'onlinePaymentSuccessURL' => "https://ya.ru",
            'onlinePaymentFailURL' => "https://google.com",
            'dealsObjects' => $this->getDealsObjectsReleaseParams($calc),
            'deals' => $this->getDealsReleaseParams($calc),
        ];

        return $params;
    }

    public function auth(){

        $basic_token = base64_encode('ZHVffjf2r5BrVmwnUNL0UEwP7CIa:zTACZlXlc3LDyXCAs1W3WV7Qa6oa'); // действующая
//        $basic_token = base64_encode('2FrKvkioQOf5wDBggeaZuH9jBgka:5dm9D9efy5fg4YQZLxeCAog1pI4a'); // бета

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->url . '/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=password&username={$this->login}&password={$this->password}");

        $headers = array();
        $headers[] = "Authorization: Basic {$basic_token}";
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            dd(curl_error($ch));
        }
        curl_close($ch);

        return \GuzzleHttp\json_decode($result)->access_token;
    }

    public function send($method, $data, $type = 'GET')
    {
        $auth_token = $this->auth();

        $curl = curl_init();
        $headers = [];
        $headers[] = "Authorization: Bearer {$auth_token}";
        $headers[] = "Accept: */*";
        $headers[] = "Content-type: application/json";

        $json = '';
        if($data){
            $json = (string)\GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
            //            echo $json;
            //exit();
        }

        if($type == 'POST'){
            $url = $this->url.$this->methods_path.$method;
        }else {
            if($data){
                $url = $this->url.$this->methods_path.$method."?".http_build_query($data);
            }else{
                $url = $this->url.$this->methods_path.$method;
            }
            $json = null;
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            dd($err);
            //Логируем ошибки
            return null;
        }

        if($method === '/printDocument'){
            return $response;
        }
        return \GuzzleHttp\json_decode($response);
    }
}