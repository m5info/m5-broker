<?php


namespace App\Services\SK\Renessans\Services\Osago;


use App\Models\Contracts\ContractsCalculation;
use App\Models\Contracts\OnlineCalculationLogs;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class OsagoApi{

    public $url;
    public $login;
    public $password;
    public $calc_result = false;
    public $release_result = false;
    public $calc_tries = 0;
    public $release_tries = 0;
    public $errors = [];

    public function __construct($url, $login, $password){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

    }


    // калькуляция осаго
    public function calc_osago(ContractsCalculation $calc)
    {
        $result = (object)['state' => false, 'answer' => [], 'error'=> ''];

        $params = $this->get_calc_params($calc);

        if(!empty($this->errors)){ // ошибки со справочниками
            $result->error = implode(';', $this->errors);

            return $result;
        }

        $calc = $this->send('/calculate/?fullInformation=true', $params, 'POST');

        if(isset($calc->result) && $calc->result){

            while(!$this->calc_result && $this->calc_tries < 90){
                $this->get_calc_info($calc->data[0]->id);
            }

            $calc_result = $this->calc_result;

            if($calc_result->result){
                $result->state = true;
                $result->sk_key_id = $calc->data[0]->id;
                $result->answer = $calc_result->data;
            }

        }else{
            $result->error .= $calc->message ? $calc->message : 'Первичная ошибка расчета!';
        }

        return $result;
    }


    // выпуск осаго
    public function release_osago(ContractsCalculation $calc, $checkSegment = 0)
    {
        $result = (object)['state' => false, 'answer' => [], 'payment_link' => '', 'error'=> ''];

        $params = $this->get_release_params($calc, $checkSegment);

        if(!empty($this->errors)){ // ошибки со справочниками
            $result->error = implode(';', $this->errors);

            return $result;
        }

        $release = $this->send('/create/', $params, 'POST');

        if(isset($release->result) && ($release->result || $release->message == 'Договор с данным расчетом уже есть в системе')){

            if(isset($release->data->policyId)){
                $calc->update(['json' => \GuzzleHttp\json_encode(['policyId' => $release->data->policyId])]);
            }

            $policyId = \GuzzleHttp\json_decode($calc->json)->policyId;

            while(($this->release_result === false || $this->release_result === 'Ожидание проверки в РСА') && $this->release_tries < 90){
                $this->get_release_status($policyId);
            }

            $release_status = $this->release_result;

            if($release_status == false){
                $this->errors[] = 'Ошибка';
            } elseif($release_status == 'Ожидание проверки в РСА'){
                $this->errors[] = 'Идет процесс создания проекта. Повторите запрос!';
            } elseif(isset($release_status->result)){

                $payment_link = $this->get_payment_link($calc);
                $release_info = $this->send("/policy/{$policyId}/info/", ['key' => '6090f76b0fa711eb80eb005056a0400b']);

                if(
                    isset($release_info->data->return->Status) &&
                    ($release_info->data->return->Status == 'Сегментация пройдена' ||
                    ($release_info->data->return->Status == 'Согласован' || $release_info->data->return->StatusPay == 'Не оплачен'))
                ){
                    $result->state = true;
                    $result->payment_link = $payment_link;
                    $result->answer = $release_info->data;
                }
            }elseif(mb_stristr($release_status, 'ошибка')){
                $this->errors[] = $release_status;
            }

        }else{

            if(isset($release->message)){
                $result->error .= $release->message;
            }else {
                $result->error .= 'Ошибка создания договора!';
            }
        }

        if(!empty($this->errors)){ // ошибки со справочниками
            $result->error = implode(';', $this->errors);
        }

        return $result;
    }


    public function get_payment_link(ContractsCalculation $calc)
    {
        $policyId = \GuzzleHttp\json_decode($calc->json)->policyId;

        $payment_link = $this->send("/policy/{$policyId}/acquiring/renins/?key=6090f76b0fa711eb80eb005056a0400b", [
            'fail_url' => 'http://yandex.ru/',
            'success_url' => 'http://google.ru/'
        ], 'POST');

        return $payment_link->data->url;
    }

    public function get_calc_info($response_id)
    {
        sleep(1);

        $calc_info = $this->send("/calculate/{$response_id}", ['key' => '6090f76b0fa711eb80eb005056a0400b']);

        $this->calc_tries++;

        if(!$calc_info->result && stristr($calc_info->data, 'Расчет не окончен')){
            $this->calc_result = false;
        }else {
            $this->calc_result = $calc_info;
        }
    }


    public function check_payment_status(ContractsCalculation $calc)
    {
        $policyId = \GuzzleHttp\json_decode($calc->json)->policyId;

        return $this->send("/policy/{$policyId}/info/", ['key' => '6090f76b0fa711eb80eb005056a0400b']);
    }

    public function get_release_status($policyId)
    {
        sleep(1);
        $release_info = $this->send("/policy/{$policyId}/status/", ['key' => '6090f76b0fa711eb80eb005056a0400b']);

        $this->release_tries++;

        if(isset($release_info->data) && isset($release_info->data->return) && isset($release_info->data->return->Status) && $release_info->data->return->Status == 'stop'){
            $this->errors[] = $release_info->message;
            $this->release_result = 'stop';
        } elseif(!$release_info->result){
            $this->release_result = $release_info->message ?? false;
        } else {
            $this->release_result = $release_info;
        }
    }

    public function get_file(ContractsCalculation $calc, $doc_code)
    {
        $policyId = \GuzzleHttp\json_decode($calc->json)->policyId;

        $doc = $this->send("/policy/{$policyId}/{$doc_code}/", ['key' => '6090f76b0fa711eb80eb005056a0400b']);

        return $doc->data->return;
    }


    public function get_calc_params(ContractsCalculation $calc)
    {
        $contract = $calc->contract;
        $autoInfo = $calc->object_insurer_auto;
        $owner = $contract->owner;

        $owner_data = (int)$owner->type ? $owner->data_ul : $owner->data_fl;

//        $from  = Carbon::createFromFormat('Y-m-d H:s:i', $contract->begin_date);
//        $to  = Carbon::createFromFormat('Y-m-d H:s:i', $contract->end_date)->addDays(3);

        $result = [
            'key' => '6090f76b0fa711eb80eb005056a0400b',
            'dateStart' => date('Y-m-d', strtotime($contract->begin_date)),
            'period' => 12/*$to->diffInMonths($from)*/,
            'limitDrivers' => (int)$calc->contract->drivers_type_id ? 0 : 1,
            'trailer' => (int)$autoInfo->is_trailer,
            'isJuridical' => (int)$owner->type,
            'usagePeriod[0][dateStart]' => date('Y-m-d', strtotime($contract->begin_date)),
            'usagePeriod[0][dateEnd]' => date('Y-m-d', strtotime($contract->end_date)),
            'codeKladr' => $owner_data->address_register_kladr,
            'codeOkato' => $owner_data->address_register_okato,
        ];

        $prolongation = [];

        if ($contract->is_prolongation){
            $prolongation = [
                'prolongation' => 1,
                'prevSeria' => $contract->prev_policy_serie,
                'prevNumber' => $contract->prev_policy_number
            ];
        }

        $car = $this->get_car($calc);

        $drivers = $this->get_drivers($calc);

        $owner = $this->get_subject($calc, 'owner');

        return array_merge($result, $owner, $car, $drivers, $prolongation);
    }


    public function get_release_params(ContractsCalculation $calc, $checkSegment)
    {
        $contract = $calc->contract;
        $autoInfo = $calc->object_insurer_auto;
        $insurer = $contract->insurer;

        $result = [
            'CheckSegment' => $checkSegment,
            'calculationId' => $calc->sk_key_id,
            'cabinet[email]' => $insurer->email,
            'cabinet[password]' => '',
            'isInsurerJuridical' => (int)$insurer->type,
            'key' => '6090f76b0fa711eb80eb005056a0400b',
            'diagnostic[number]' => $autoInfo->dk_number,
            'diagnostic[validDate]' => $autoInfo->dk_date
        ];

        $car = $this->get_release_car($calc);

        $owner = $this->get_release_subject($calc, 'owner');
        $insurer = $this->get_release_subject($calc, 'insurer');

        return array_merge($result, $owner, $insurer, $car);
    }


    public function get_release_car(ContractsCalculation $calc)
    {
        $autoInfo = $calc->object_insurer_auto;

        $doc_type = '';
        switch ((int)$autoInfo->doc_type){
            case 0;
                $doc_type = 'pts';
                $docTypeId = 30;
                break;
            case 1;
                $doc_type = 'sts';
                $docTypeId = 31;
                break;
            case 2; // эптс
                $doc_type = 'sts';
                $docTypeId = 41;
                break;
        }

        $docType = [];

        if((int)$autoInfo->doc_type){ // если не птс, то появляется новый обязательный параметр типа дока
            $docType = ["car[sts][docType]" => $docTypeId];
        }

        $car = [
            "car[year]" => $autoInfo->car_year,
            "car[isNew]" => (int)$autoInfo->is_new,
            "car[{$doc_type}][serie]" => $autoInfo->docserie,
            "car[{$doc_type}][number]" => $autoInfo->docnumber,
            "car[{$doc_type}][dateIssue]" => $autoInfo->docdate,
        ];

        return array_merge($car, $docType);
    }

    public function get_car(ContractsCalculation $calc)
    {
        $autoInfo = $calc->object_insurer_auto;

        $car = [
            'car[power]' => (string)(int)$autoInfo->power,
            'car[documents][registrationNumber]' => $autoInfo->reg_number,
            'car[documents][chassisNumber]' => $autoInfo->body_chassis,
            'car[documents][carcaseNumber]' => $autoInfo->body_number,
            'car[documents][vin]' => $autoInfo->vin,
        ];


        if($category = $calc->sk_category){
            $car['car[category]'] = $category->vehicle_categorie_sk_id;
        }else{
            $this->errors[] = 'Не синхронизирован справочник: Категории';
        }

        if($mark = $calc->sk_mark){
            $car['car[make]'] = $mark->vehicle_marks_sk_id;
        }else{
            $this->errors[] = 'Не синхронизирован справочник: Модель';
        }

        if($model = $calc->sk_model){
            $car['car[model]'] = $model->vehicle_models_sk_id;
        }else{
            $this->errors[] = 'Не синхронизирован справочник: Модель';
        }

        return $car;
    }


    public function get_release_subject(ContractsCalculation $calc, $subject_name)
    {
        $contract = $calc->contract;
        $subject = [];

        if ($contract_subject = $contract->{$subject_name}) {

            $contract_subject_info = $contract_subject->get_info();

                $subject = [
                    "{$subject_name}[addressJuridical][country]" => 'Россия',
                    "{$subject_name}[addressJuridical][zip]" => $contract_subject_info->address_register_zip,
                    "{$subject_name}[addressJuridical][city]" => $contract_subject_info->address_register_city,
                    "{$subject_name}[addressJuridical][street]" => $contract_subject_info->address_register_street,
                    "{$subject_name}[addressJuridical][home]" => $contract_subject_info->address_register_house,
                    "{$subject_name}[addressJuridical][flat]" => $contract_subject_info->address_register_flat,
                    "{$subject_name}[addressJuridical][kladr]" => $contract_subject_info->address_register_kladr,
                    "{$subject_name}[addressJuridical][okato]" => $contract_subject_info->address_register_okato,
                    "{$subject_name}[addressFact][country]" => 'Россия',
                    "{$subject_name}[addressFact][zip]" => $contract_subject_info->address_fact_zip,
                    "{$subject_name}[addressFact][city]" => $contract_subject_info->address_fact_city,
                    "{$subject_name}[addressFact][street]" => $contract_subject_info->address_fact_street,
                    "{$subject_name}[addressFact][home]" => $contract_subject_info->address_fact_house,
                    "{$subject_name}[addressFact][flat]" => $contract_subject_info->address_fact_flat,
                    "{$subject_name}[addressFact][kladr]" => $contract_subject_info->address_fact_kladr,
                    "{$subject_name}[addressFact][okato]" => $contract_subject_info->address_fact_okato,
                    "{$subject_name}[email]" => $contract_subject->email,
                    "{$subject_name}[phone]" => $contract_subject->phone ? parsePhoneNumber($contract_subject->phone) : '',
                ];

                if ($contract_subject_info->doc_date == 0){
                    $this->errors[] = 'Заполните дату выдачи документа!';
                }

                if (isset($contract_subject->doc_info) && $contract_subject->doc_info == ''){
                    $this->errors[] = 'Заполните кем выдан документ!';
                }

            if($subject_name == 'insurer'){

                if ($contract_subject->type == 0) { // если физик

                    $fio = $contract_subject_info->get_fio();

                    $birthdate = date('Y-m-d', strtotime($contract_subject_info->birthdate));
                    $dateIssue = date('Y-m-d', strtotime($contract_subject_info->doc_date));

                    $insurer = [
                        'insurer[name]' => $fio['first_name'],
                        'insurer[lastname]' => $fio['last_name'],
                        'insurer[middlename]' => $fio['middle_name'],
                        'insurer[birthday]' => $birthdate,
                        'insurer[document][dateIssue]' => $dateIssue,
                        'insurer[document][issued]' => $contract_subject_info->doc_info,
                        'insurer[document][number]' => $contract_subject_info->doc_number,
                        'insurer[document][series]' => $contract_subject_info->doc_serie,
                    ];

                } else {

                    $insurer = [
                        'insurer[organization][name]' => $contract_subject_info->title,
                        'insurer[organization][inn]' => $contract_subject_info->inn,
                        'insurer[organization][kpp]' => $contract_subject_info->kpp
                    ];
                }

                $subject = array_merge($subject, $insurer);
            }
        }

        return $subject;
    }


    public function get_subject(ContractsCalculation $calc, $subject_name)
    {
        $contract = $calc->contract;
        $subject = [];

        if ($contract_subject = $contract->{$subject_name}) {

            $contract_subject_info = $contract_subject->get_info();

            if ($contract_subject->type == 0) { // если физик

                $fio = $contract_subject_info->get_fio();

                $subject = [
                    'owner[name]' => $fio['first_name'],
                    'owner[lastname]' => $fio['last_name'],
                    'owner[middlename]' => $fio['middle_name'],
                    'owner[birthday]' => date('Y-m-d', strtotime($contract_subject_info->birthdate)),
                    'owner[document][dateIssue]' => date('Y-m-d', strtotime($contract_subject_info->doc_date)),
                    'owner[document][issued]' => $contract_subject_info->doc_info,
                    'owner[document][number]' => $contract_subject_info->doc_number,
                    'owner[document][series]' => $contract_subject_info->doc_serie,
                ];

                if ($contract_subject_info->doc_date == 0){
                    $this->errors[] = 'Заполните дату выдачи документа!';
                }

                if (isset($contract_subject->doc_info) && $contract_subject->doc_info == ''){
                    $this->errors[] = 'Заполните кем выдан документ!';
                }

            } else {

                $subject = [
                    'owner[organization][name]' => $contract_subject_info->title,
                    'owner[organization][inn]' => $contract_subject_info->inn,
                    'owner[organization][kpp]' => $contract_subject_info->kpp
                ];
            }

        }

        return $subject;
    }


    public function get_drivers(ContractsCalculation $calc)
    {
        $drivers = [];

        if(!(int)$calc->contract->drivers_type_id){
            $key = 0;
            foreach ($calc->drivers as $driver){

                $fio = $driver->get_fio();

                $birth_date = date('Y-m-d', strtotime($driver->birth_date));
                $dateBeginDrive = date('Y-m-d', strtotime($driver->exp_date));
                $dateIssue = date('Y-m-d', strtotime($driver->doc_date));

                $driver = [
                    "drivers[{$key}][name]" => $fio['first_name'],
                    "drivers[{$key}][lastname]" => $fio['last_name'],
                    "drivers[{$key}][middlename]" => $fio['middle_name'],
                    "drivers[{$key}][birthday]" => $birth_date,
                    "drivers[{$key}][license][dateBeginDrive]" => $dateBeginDrive,
                    "drivers[{$key}][license][dateIssue]" => $dateIssue,
                    "drivers[{$key}][license][number]" => $driver->doc_num,
                    "drivers[{$key}][license][series]" => $driver->doc_serie
                ];

                $drivers = array_merge($drivers, $driver);

                $key++;
            }
        }

        return $drivers;
    }


    public function send($method, $data, $type = 'GET')
    {
        $curl = curl_init();
        $headers = [];

        if($type == 'POST'){
            $url = $this->url.$method;
            $action = $method;
        }else {
            if($data){
                $url = $this->url.$method."?".http_build_query($data);
                $action = $method."?".http_build_query($data);
            }else{
                $url = $this->url.$method;
                $action = $method;
            }
            $data = null;
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 240,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            dd($err);
            //Логируем ошибки
            return null;
        }

        OnlineCalculationLogs::create([
            'contract_id' => 11024,
            'action' => $action,
            'data_send' => is_array($data) ? json_encode($data) : $data,
            'data_response' => is_array($response) ? json_encode($response) : $response,
        ]);

        return \GuzzleHttp\json_decode($response);
    }
}