<?php

namespace App\Services\SK\Renessans\Services\Osago;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\File;
use App\Repositories\FilesRepository;
use stdClass;

class OsagoService implements ProductServiceInterface{

    public $api;

    public function __construct(OsagoApi $api){
        $this->api = $api;
    }

    public function calc(ContractsCalculation $calc){

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';

        $calculation = $this->api->calc_osago($calc); // расчитываем первично

        if($calculation->state == true){

            $calc->update(['sk_key_id' => $calculation->sk_key_id]);

            $segmentation = $this->api->release_osago($calc, 1); // проверяем сразу, проходим ли по сементации

            if($segmentation->state){

                $calculation = $this->api->calc_osago($calc); // расчитываем второй раз уже если все четко

                $coeffs = $calculation->answer->response->Details->Coefficient;

                $response->state = true;
                $response->error = '';
                $response->sk_key_id = $calculation->sk_key_id;
                $response->statys_id = 1;
                $response->payment_total = $calculation->answer->premium;
                $response->msg = "";

                foreach($coeffs as $coeff){
                    $response->msg .= "{$coeff->coefficient}: {$coeff->value}; ";
                }

            }else{
                $response->error = $segmentation->error;
            }

        }else{
            $response->error = $calculation->error;
        }

        return $response;
    }

    public function temp_calc(ContractsCalculation $calc){
        // TODO: Implement temp_calc() method.
    }

    public function release(ContractsCalculation $calc){

        $contract = $calc->contract;

        $response = new stdClass();
        $response->state = false;
        $response->error = '';
        $response->statys_id = '';
        $response->msg = '';

        $result = $this->api->release_osago($calc);

        if($result->state){

            $response->state = true;
            $response->statys_id = 3;
            $response->payment_link = $result->payment_link;
            $response->answer = $result->answer;

            $calc->update(['statys_id' => 3]);
            $contract->update(['statys_id' => 3]);

        }else{

            $response->error = $result->error;
        }

        $response->bso_id = (int)$contract->bso_id;

        return $response;
    }

    public function check_status(ContractsCalculation $calc)
    {
        $result = (object)['state' => false, 'error'=> ''];
        $contract = $calc->contract;

        $info = $this->api->check_payment_status($calc);

        if(isset($info->data->return->StatusPay) && $info->data->return->StatusPay == 'Оплачен'){
            if($this->save_files($calc)){ // получаем и сохраняем файлы
                $bso_serie = (string)$info->data->return->Seria;
                $bso_number = (string)$info->data->return->Number;
                $bso_number = str_replace($bso_serie, '', $bso_number);

                $bso = $contract->getElectronBSO($bso_serie, $bso_number);
                $contract->update(['statys_id' => 4, 'bso_id' => $bso->id]);
                $calc->update(['statys_id' => 4]);
                $bso->setBsoLog(0);

                $result->state = true;
            }

        }else{
            $result->error = 'Платеж не оплачен!';
        }

        return $result;
    }

    public function get_files(ContractsCalculation $calc)
    {
        return true;
    }

    public function save_files(ContractsCalculation $calc)
    {
        $documents = [
            'pdf' => 'Договор Е-ОСАГО',
            'application' => 'Заявлениe договора Е-ОСАГО'
        ];

        $contract = $calc->contract;

        $contract->masks()->detach();

        foreach($documents as $document_code => $document_name){

            $fileContent = $this->api->get_file($calc, $document_code);

            $File_n = 'pdf';
            $file_contents = base64_decode($fileContent);
            $path = storage_path('app/' . Contracts::FILES_DOC . "/{$contract->id}/");

            if(!is_dir(($path))){
                mkdir(($path), 0777, true);
            }

            $file_name = uniqid();

            file_put_contents($path . $file_name . '.' . $File_n, $file_contents);

            $file = File::create([
                'original_name' => $document_name,
                'ext' => $File_n,
                'folder' => Contracts::FILES_DOC . "/{$contract->id}/",
                'name' => $file_name,
                'user_id' => auth()->id()
            ]);

            $contract->masks()->save($file);
        }

        return true;
    }
}