<?php

namespace App\Services\SK\Renessans\Services\Dictionary;

use App\Interfaces\Services\SK\DictionaryServiceInterface;


class DictionaryServiceNewApi implements DictionaryServiceInterface {


    public $api;

    public function __construct(DictionaryApiNewApi $api){
        $this->api = $api;
    }

    public function get_api_purpose(){

        $answer = $this->api->get_purpose();

        $purposes = [];

        foreach($answer as $item){
            $purposes[] = [
                'id' => $item->code,
                'title' => $item->name,
            ];
        }

        return $purposes;
    }

    public function get_api_categories(){

        $answer = $this->api->get_categories();

        $categories = [];

        foreach($answer as $item){
            $categories[] = [
                'id' => $item->code,
                'title' => $item->name,
            ];
        }

        return $categories;
    }

    public function get_marks_models(){

        $answer = $this->api->get_marks_models();
        $answer_marks_models = $answer->autoModification;
        $marks = [];
        $models = [];

        foreach($answer_marks_models as $key => $answer_marks_models){

            $marks[] = [
                'id' => (string) $answer_marks_models->brand,
                'title' => (string) $answer_marks_models->brand,
                'vehicle_categorie_sk_id' => (string) 'B - легковые',
            ];

            if(isset($answer_marks_models->models) && count($answer_marks_models->models)){
                foreach ($answer_marks_models->models as $_model){
                    $models[] = [
                        'id' => (string) $_model->model,
                        'title' => (string) $_model->model,
                        'vehicle_mark_sk_id' => (string) $answer_marks_models->brand,
                        'vehicle_categorie_sk_id' => (string)'B - легковые',
                    ];
                }
            }
        }

        $result = [
            'categories' => $this->get_api_categories(),
            'marks' => $marks,
            'models' => $models,
        ];

        return $result;
    }


    /*{
        "dictionaryName": "autoModification",
        "description": "Общий справочник марок и моделей авто",
        "version": "5.5",
        "dateModify": "27.06.2020",
        "codes": null
    },
    {
        "dictionaryName": "AutoType",
        "description": "Тип ТС",
        "version": "5",
        "dateModify": "27.06.2020",
        "codes": null
      },
    {
        "dictionaryName": "AutoBrand",
        "description": "Марка авто",
        "version": "5",
        "dateModify": "27.06.2020",
        "codes": null
      },
    {
        "dictionaryName": "AutoModel",
        "description": "Модель авто",
        "version": "5",
        "dateModify": "27.06.2020",
        "codes": null
      },
    {
        "dictionaryName": "AutoYearOfIssue",
        "description": "Год выпуска",
        "version": "5",
        "dateModify": "27.06.2020",
        "codes": null
      },
    {
        "dictionaryName": "AutoEnginePower",
        "description": "Мощность двигателя",
        "version": "5",
        "dateModify": "27.06.2020",
        "codes": null
      },
    {
        "dictionaryName": "PurposeOfUsing",
        "description": "Цель использования",
        "version": "1",
        "dateModify": "01.01.2017",
        "codes": null
      },
    {
        "dictionaryName": "RegistrationRegion",
        "description": "Регион регистрации",
        "version": "1",
        "dateModify": "01.01.2017",
        "codes": null
      },
    {
        "dictionaryName": "IdentityDocumentType",
        "description": "Тип удостоверения",
        "version": "1",
        "dateModify": "01.01.2017",
        "codes": null
      },
    {
        "dictionaryName": "Citizenship",
        "description": "Гражданство",
        "version": "1",
        "dateModify": "01.01.2017",
        "codes": null
      },
    {
        "dictionaryName": "InsurancePeriodType",
        "description": "Тип периода использования",
        "version": "1",
        "dateModify": "01.01.2019",
        "codes": null
      },
    {
        "dictionaryName": "AddDocumentType",
        "description": "Тип указанного доп документа ВУ/неВУ",
        "version": "1",
        "dateModify": "01.01.2017",
        "codes": null
      },
    {
        "dictionaryName": "TsIdentityCardType",
        "description": "Тип паспорта ТС",
        "version": "3",
        "dateModify": "27.08.2019",
        "codes": null
      },
    {
        "dictionaryName": "DeliveryWay",
        "description": "Способ доставки документа",
        "version": "1",
        "dateModify": "01.01.2017",
        "codes": null
      }*/
}