<?php


namespace App\Services\SK\Renessans\Services\Dictionary;


class DictionaryApi{

    public $url;
    public $login;
    public $password;

    public function __construct($url, $login, $password){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

    }


    public function get_categories()
    {
        return $this->send('/categories', ['key' => '6090f76b0fa711eb80eb005056a0400b']);
    }


    public function get_marks_models()
    {
        return $this->send('/models', ['key' => '6090f76b0fa711eb80eb005056a0400b']);
    }


    public function send($method, $data, $type = 'GET')
    {
        $curl = curl_init();
        $headers = [];

        if($type == 'POST'){
            $url = $this->url.$method;
        }else {
            if($data){
                $url = $this->url.$method."?".http_build_query($data);
            }else{
                $url = $this->url.$method;
            }
            $data = null;
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            dd($err);
            //Логируем ошибки
            return null;
        }

        return \GuzzleHttp\json_decode($response);
    }
}