<?php

namespace App\Services\SK\Renessans\Services\Dictionary;

use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Facades\Excel;

class DictionaryApiNewApi{

    public $url;
    public $login;
    public $password;
    public $methods_path = '/link2api/eOsagoInsurance/1.0';

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

    }


    public function get_purpose(){
        //Дергаем справочник
//        dd($this->send('/dictionary/AutoEnginePower', null));

        return $this->send('/dictionary/PurposeOfUsing', null);
    }

    public function get_categories(){
        return $this->send('/dictionary/AutoType', null);
    }

    public function get_marks_models(){

        return $this->send('/dictionary/autoModification', null);
    }

    public function auth(){

        $basic_token = base64_encode('ZHVffjf2r5BrVmwnUNL0UEwP7CIa:zTACZlXlc3LDyXCAs1W3WV7Qa6oa');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->url . '/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=password&username={$this->login}&password={$this->password}");

        $headers = array();
        $headers[] = "Authorization: Basic {$basic_token}";
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            dd(curl_error($ch));
        }
        curl_close($ch);

        return \GuzzleHttp\json_decode($result)->access_token;
    }

    public function send($method, $data, $type = 'GET')
    {
        $auth_token = $this->auth();

        $curl = curl_init();
        $headers = [];
        $headers[] = "Authorization: Bearer {$auth_token}";
        $headers[] = "Accept: text/plain";

        $json = '';
        if($data){
            $json = 'params='.(string)\GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
            //            echo $json;
            //exit();
        }

        if($type == 'POST'){
            $url = $this->url.$this->methods_path.$method;
        }else {
            if($data){
                $url = $this->url.$this->methods_path.$method."?".http_build_query($data);
            }else{
                $url = $this->url.$this->methods_path.$method;
            }
            $json = null;
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);



        curl_close($curl);

        if ($err) {

            //            dd($err);

            //Логируем ошибки
            return null;
        }

        //dd($response);
        return \GuzzleHttp\json_decode($response);
    }

}