<?php


namespace App\Services\SK\Renessans\Services\Dictionary;


use App\Interfaces\Services\SK\DictionaryServiceInterface;

class DictionaryService implements DictionaryServiceInterface {

    public $api;

    public function __construct(DictionaryApi $api){
        $this->api = $api;
    }

    public function get_api_purpose()
    {
        return [
            ['id'=>'Личная',  'title'=>'Личная'],
            ['id'=>'Такси',  'title'=>'Такси'],
            ['id'=>'УчебнаяЕзда',  'title'=>'УчебнаяЕзда'],
            ['id'=>'ДорожныеИСпециальныеТС',  'title'=>'ДорожныеИСпециальныеТС'],
            ['id'=>'Прочее',  'title'=>'Прочее'],
            ['id'=>'ПассажирскиеПеревозки',  'title'=>'ПассажирскиеПеревозки'],
            ['id'=>'ОпасныйГруз',  'title'=>'ОпасныйГруз'],
            ['id'=>'СдачаВАренду',  'title'=>'СдачаВАренду'],
            ['id'=>'ЭкстренныеИКоммСлужбы',  'title'=>'ЭкстренныеИКоммСлужбы']
        ];
    }

    public function get_api_categories()
    {
        $answer = $this->api->get_categories();

        $categories = [];

        foreach($answer->data as $item){
            $categories[] = [
                'id' => $item->category,
                'title' => $item->name,
            ];
        }

        return $categories;
    }

    public function get_marks_models()
    {
        $answer = $this->api->get_marks_models();
        $answer_marks_models = $answer->data;
        $marks = [];
        $models = [];

        foreach($answer_marks_models as $key => $answer_mark_models){

            $marks[] = [
                'id' => (string) $answer_mark_models->make,
                'title' => (string) $answer_mark_models->make,
                'vehicle_categorie_sk_id' => (string) 'B - легковые',
            ];

            if(isset($answer_mark_models->models) && count($answer_mark_models->models)){
                foreach ($answer_mark_models->models as $_model){
                    $models[] = [
                        'id' => (string) $_model->model,
                        'title' => (string) $_model->model,
                        'vehicle_mark_sk_id' => (string) $answer_mark_models->make,
                        'vehicle_categorie_sk_id' => (string)'B - легковые',
                    ];
                }
            }
        }

        $result = [
            'categories' => $this->get_api_categories(),
            'marks' => $marks,
            'models' => $models,
        ];

        return $result;
    }

}