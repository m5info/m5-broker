<?php


namespace App\Services\SK\Renessans;


use App\Services\SK\BaseServiceController;
use App\Services\SK\Renessans\Services\Dictionary\DictionaryApi;
use App\Services\SK\Renessans\Services\Dictionary\DictionaryApiNewApi;
use App\Services\SK\Renessans\Services\Dictionary\DictionaryService;
use App\Services\SK\Renessans\Services\Dictionary\DictionaryServiceNewApi;
use App\Services\SK\Renessans\Services\Osago\OsagoApi;
use App\Services\SK\Renessans\Services\Osago\OsagoApiNewVersion;
use App\Services\SK\Renessans\Services\Osago\OsagoService;

class ServiceController extends BaseServiceController {

    const SERVICES = [

        'dictionary' => [
            'service' => DictionaryService::class,/// Можно проставить для удобства конечно им префиксы типа "AlphaDictionaryService::class"
            'api' => DictionaryApi::class,/// но наверное необязательно
        ],
        'osago' => [
            'service' => OsagoService::class,
            'api' => OsagoApi::class,
        ],
//        'kasko' => [
//            'service' => KaskoService::class,
//            'api' => KaskoApi::class,
//        ],

    ];
}