<?php

namespace App\Services\SK\DriveDK\Services\Boxes;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\ContractsCalculation;

class BoxesService implements ProductServiceInterface{


    public $api;

    public function __construct(BoxesApi $api){
        $this->api = $api;

    }

    public function temp_calc(ContractsCalculation $calc)
    {

        return null;
    }

    public function calc(ContractsCalculation $calc)
    {
        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';



        return $response;
    }


    public function release(ContractsCalculation $calc){

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';
        $response->dk_nomer = '';

        $result = $this->api->saveDK($calc);

        if(isset($result->id) && (int)$result->id > 0 && isset($result->nomer) && strlen($result->nomer) > 0) {

            $response->state = true;
            $response->sk_key_id = $result->id;
            $response->payment_total = $calc->contract->payment_total;
            $response->dk_nomer = $result->nomer;

        }else{
            $response->error = $result;
        }

        return $response;

    }


    public function check_status(ContractsCalculation $calc){

    }


    public function get_files(ContractsCalculation $calc){



        return true;
    }

    public function checkLimit()
    {


        return true;
    }


}