<?php

namespace App\Services\SK\DriveDK\Services\Boxes;


use App\Models\Contracts\ContractsCalculation;
use App\Services\SK\DriveDK\Services\Send\SendJSON;


class BoxesApi{


    public $url;
    public $login;
    public $password;
    public $errors = [];

    public $Send = null;

    public function __construct($url, $login, $password){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->Send = new SendJSON($url, $login, $password);
    }


    public function saveDK($calc)
    {
        //return $this->Send->save($this->getArrayData($calc));

        $result = new \stdClass();
        $result->id = 1;
        $result->nomer = '4444444';

        return $result;
    }

    const DOC_TYPE = [
        0=>'PTS',
        1=>'RegTalon'
    ];

    const CATEGORY_TYPE = [
        1=>'A',
        2=>'B',
        3=>'C',
        4=>'D',
    ];

    const CATEGORY_OKP_TYPE = [
        1=>'L',
        2=>'M1',
        3=>'N1',
        4=>'N2',
        5=>'N3',
        6=>'M2',
        7=>'M3',
        8=>'O1',
        9=>'O2',
        10=>'O3',
        11=>'O4',
    ];

    const TYPE_ENGINE = [
        0 => 'Petrol',
        1 => 'Diesel',
        2 => 'None',
        3 => 'None'
    ];

    const BRAKE_SYSTEM = [
        0 => 'Mechanical',
        1 => 'Hydraulic',
        2 => 'Pneumatic',
        3 => 'Combined',
        4 => 'None',
    ];

    public function getArrayData($calc)
    {

        $contract = $calc->contract;


        $name = explode(" ", $contract->insurer->title);
        $object = $contract->object_insurer_auto;

        $documentType = self::DOC_TYPE[$object->doc_type];
        $vechicleCategory = self::CATEGORY_TYPE[$object->ts_category];
        $vechicleCategory2 = self::CATEGORY_OKP_TYPE[$object->ts_category_okp];
        $fuel = self::TYPE_ENGINE[$object->type_engine];
        $brakingSystem = self::BRAKE_SYSTEM[$object->brake_system_id];

        $data = [
            'login'              => $this->login,
            'name'               => $name[1],
            'fName'              => $name[0],
            'mName'              => $name[2],
            'year'               => (int)$object->car_year,
            'make'               => $object->mark->title,
            'model'              => $object->model->title,

            'vin'                => $object->vin,
            'bodyNumber'         => $object->body_number,
            'frameNumber'        => $object->body_chassis,
            'registrationNumber' => $object->reg_number,

            'killometrage'       => getFloatFormat($object->mileage),
            'emptyMass'          => getFloatFormat($object->weight),
            'maxMass'            => getFloatFormat($object->max_weight),

            'tyres'              => $object->brand_tire,
            'isForeign'          => 'N',
            'documentDate'       => getDateFormatRu($contract->sign_date),
            'dateOfDiagnosis'    => getDateFormatRu($contract->begin_date),
            'validity'           => getDateFormatRu($contract->end_date),

            'series'             => $object->docserie,
            'number'             => $object->docnumber,
            'organization'       => $object->docissued,

            'note'               => $object->dk_coments,

            'draft'              => true,

            'documentType'       => $documentType,
            'vechicleCategory'   => $vechicleCategory,
            'vechicleCategory2'  => $vechicleCategory2,
            'fuel'               => $fuel,
            'brakingSystem'      => $brakingSystem,

            'id'                 => (int)$calc->sk_key_id,

        ];


        return $data;
    }


}