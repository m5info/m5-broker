<?php

namespace App\Services\SK\DriveDK\Services\Send;


use Doctrine\DBAL\Cache\CacheException;

class SendJSON{

    public $url;
    public $login;
    public $password;


    public function __construct($url, $login, $password){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

    }

    public function send($array, $method)
    {
        //dd($this->url.$method);

        try {
            $json = \GuzzleHttp\json_encode($array);

            $form_params = [
                'json' => $json,
                'hash' => hash('md5', $json . $this->password),
            ];

            //var_dump($form_params);

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Expect: '));
            curl_setopt($curl, CURLOPT_ENCODING, "");
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HEADER, 0); // отладка, получение заголовков
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 50);
            curl_setopt($curl, CURLOPT_TIMEOUT, 150);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // HTTPS
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0); // HTTPS
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $form_params);
            curl_setopt($curl, CURLOPT_URL, $this->url.$method);
            $result = curl_exec($curl);
            curl_close($curl);


        } catch (\Exception $exception) {

            $result = $exception->getMessage();

        }

        return $result;
    }


    public function save($array)
    {
        return \GuzzleHttp\json_decode($this->send($array, ''));
    }

}