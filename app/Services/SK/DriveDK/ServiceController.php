<?php

namespace App\Services\SK\DriveDK;

use App\Services\SK\BaseServiceController;
use App\Services\SK\DriveDK\Services\Boxes\BoxesService;
use App\Services\SK\DriveDK\Services\Boxes\BoxesApi;

class ServiceController extends BaseServiceController
{
    const SERVICES = [

        'boxes' => [
            'service' => BoxesService::class,
            'api' => BoxesApi::class,
        ],

    ];


    const PROGRAMS = [
        'boxes' => [
            ['id' => 1, 'name' => 'dk', 'template' => 'default', 'title' => 'Диагностическая карта'],
        ]

    ];

}
