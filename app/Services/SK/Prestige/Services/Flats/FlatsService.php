<?php

namespace App\Services\SK\Energogarant\Services\Flats;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\File;
use App\Repositories\FilesRepository;
use function GuzzleHttp\Psr7\str;

class FlatsService implements ProductServiceInterface{

    public $available_types = [
        0 => 'Бумажный',
        1 => 'Электронный',
    ];

    public $api;

    public function __construct(FlatsApi $api){
        $this->api = $api;
    }


    public function temp_calc(ContractsCalculation $calc)
    {


    }


    public function calc(ContractsCalculation $calc){



        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';




        /*//////

        $response->state = true;
        $response->error = '';
        $response->sk_key_id = '12312';
        $response->statys_id = 1;
        $response->payment_total = 5373.31;
        $response->msg = 'ТБ: 3998.00; КТ: 2.00; КБМ: 0.50; КВС: 0.96; КС: 1.00; КП: 1.00; КМ: 1.40; КПР: 1.00; КН: 1.00;';
        return $response;

        /////////*/






        return $response;
    }


    public function release(ContractsCalculation $calc)
    {

        $contract = $calc->contract;

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';



        return $response;
    }


    public function check_status(ContractsCalculation $calc)
    {


    }


    public function get_files(ContractsCalculation $calc)
    {


        return true;
    }

}