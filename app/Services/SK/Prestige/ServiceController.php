<?php

namespace App\Services\SK\Prestige;


use App\Services\SK\BaseServiceController;
use App\Services\SK\Energogarant\Services\Flats\FlatsApi;
use App\Services\SK\Energogarant\Services\Flats\FlatsService;

class ServiceController extends BaseServiceController {


    const SERVICES = [


        'flats' => [
            'service' => FlatsService::class,
            'api' => FlatsApi::class,
        ],


    ];

    const PROGRAMS = [
        'flats' => [
            ['id' => 1, 'name' => 'AgrPrestigeFlats', 'template' => 'prestige_flats', 'title' => 'Квартиры'],
        ],
    ];


}