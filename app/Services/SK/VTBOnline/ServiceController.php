<?php

namespace App\Services\SK\VTBOnline;


use App\Services\SK\BaseServiceController;
use App\Services\SK\VTBOnline\Services\Boxes\BoxesService;
use App\Services\SK\VTBOnline\Services\Boxes\BoxesApi;

class ServiceController extends BaseServiceController {

    const SERVICES = [

        'boxes' => [
            'service' => BoxesService::class,
            'api' => BoxesApi::class,
        ],

    ];

    const PROGRAMS = [
        'boxes' => [
                ['id' => 1, 'name' => 'voyage', 'template' => 'default', 'title' => 'Страхование выезжающих за рубеж'],
                ['id' => 2, 'name' => 'flat', 'template' => 'default', 'title' => 'Страхование квартиры'],
                ['id' => 3, 'name' => 'yourOwnDoctorB2B', 'template' => 'default', 'title' => 'Детский доктор для СИП'],
                ['id' => 4, 'name' => 'mite3', 'template' => 'default', 'title' => 'Клещевой энцефалит'],
                ['id' => 5, 'name' => 'accidente', 'template' => 'default', 'title' => 'Страхование от несчастного случая'],
                ['id' => 6, 'name' => 'steerYourHealthPlus', 'template' => 'default', 'title' => 'Страхование онкологии'],
            ]

    ];


}