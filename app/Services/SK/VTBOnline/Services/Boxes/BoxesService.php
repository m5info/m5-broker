<?php

namespace App\Services\SK\VTBOnline\Services\Boxes;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;

class BoxesService implements ProductServiceInterface{


    public $api;

    public function __construct(BoxesApi $api){
        $this->api = $api;

    }

    public function temp_calc(ContractsCalculation $calc)
    {

        return null;
    }

    public function calc(ContractsCalculation $calc)
    {

        return null;
    }


    public function release(ContractsCalculation $calc){

    }


    public function check_status(ContractsCalculation $calc){

    }


    public function get_files(ContractsCalculation $calc){

    }

    public function getFrame($product)
    {

        $loginFormatted = str_replace('-', '_', auth()->user()->email);
        $loginFormatted = str_replace('@', '_', $loginFormatted);

        $urls = "{$this->api->url}{$product->api_name}/index.php?p={$this->api->login}&client=" . $loginFormatted;


        return $urls;
    }


}