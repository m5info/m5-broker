<?php

namespace App\Services\SK\Verna;


use App\Services\SK\BaseServiceController;
use App\Services\SK\Verna\Services\Dictionary\DictionaryApi;
use App\Services\SK\Verna\Services\Dictionary\DictionaryService;
use App\Services\SK\Verna\Services\Kasko\Calculator\KaskoCalculatorApi;
use App\Services\SK\Verna\Services\Kasko\Calculator\KaskoCalculatorService;
use App\Services\SK\Verna\Services\Kasko\KaskoApi;
use App\Services\SK\Verna\Services\Kasko\KaskoService;
use App\Services\SK\Verna\Services\Osago\OsagoApi;
use App\Services\SK\Verna\Services\Osago\OsagoService;

class ServiceController extends BaseServiceController {


    const SERVICES = [

        'dictionary' => [
            'service' => DictionaryService::class,  /// Можно проставить для удобства конечно им префиксы типа "AlphaDictionaryService::class"
            'api' => DictionaryApi::class,          /// но наверное необязательно
        ],
        'osago' => [
            'service' => OsagoService::class,
            'api' => OsagoApi::class,
        ],
        'kasko' => [
            'service' => KaskoService::class,
            'api' => KaskoApi::class,
        ],
        'kasko_calculator' => [
            'service' => KaskoCalculatorService::class,
            'api' => KaskoCalculatorApi::class,
        ],

    ];

    const PROGRAMS = [
        'kasko' => [

            ]

    ];


}