<?php

namespace App\Services\SK\Verna\Services\Kasko;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;

class KaskoService implements ProductServiceInterface{


    public $api;

    public function __construct(KaskoApi $api){
        $this->api = $api;
    }

    public function temp_calc(ContractsCalculation $calc)
    {

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';


        $result = $this->api->getTempCalc($calc);

        if((int)$result->Error->Code == 0){

            $response->state = true;

            $response->payment_total = (string)$result->Answer->Calc->PremiumSum;
            $response->sk_key_id = null;
            $response->msg = (string)$result->Answer->Calc->CascoTarif;
            $response->statys_id = 1;


        }else{
            $response->error = (string)$result->Error->Text;
        }




        return $response;

    }

    public function calc(ContractsCalculation $calc)
    {

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';


        $result = $this->api->getTarif($calc);

        if((int)$result->Error->Code == 0){


            $response->state = true;

            $response->payment_total = (string)$result->Answer->Calc->PremiumSum;
            $response->sk_key_id = (string)$result->Answer->Contract->ContractId;
            $response->msg = (string)$result->Answer->Calc->CascoTarif;
            $response->statys_id = 1;


        }else{
            $response->error = (string)$result->Error->Text;
        }


        return $response;

    }


    public function release(ContractsCalculation $calc){

    }


    public function check_status(ContractsCalculation $calc){

    }


    public function get_files(ContractsCalculation $calc){

    }

}