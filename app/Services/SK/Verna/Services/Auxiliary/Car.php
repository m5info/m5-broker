<?php

namespace App\Services\SK\Verna\Services\Auxiliary;


use App\Models\Contracts\ContractsCalculation;

class Car{

    const MOTOR_TYPE = [
        0 => ['id'=>1, 'name'=>'Бензин', 'val'=>'P'],
        1 => ['id'=>2, 'name'=>'Дизель', 'val'=>'D'],
        2 => ['id'=>3, 'name'=>'Электро', 'val'=>'E'],
        3 => ['id'=>4, 'name'=>'Гибрид', 'val'=>'H'],
    ];


    public static function getCarOSAGO(ContractsCalculation $calc, $owner)
    {
        $contract = $calc->contract;
        $vehicle = $contract->object_insurer_auto;

        $DocDate = setDateTimeFormatRu($vehicle->docdate, 1);

        $period1_begin = date("d.m.Y", strtotime($contract->begin_date));
        $period1_end = date("d.m.Y", strtotime($contract->end_date));

        $dk_date = '';
        if($vehicle->dk_date != '1970-01-01 00:00:00') {
            $dk_date = date("d.m.Y", strtotime($vehicle->dk_date));
        }


        $response = new \stdClass();
        $response->Owner = $owner->result;
        $response->CountryCode = '643';//"ISO-код страны регистрации ТС (Тип: Целое число. Обязательный параметр)"

        $response->UseTrailer = $vehicle->is_trailer;

        $response->Rent = '0';
        $response->RightWheel = 'N';
        $response->UseSpecialSignal = 'N';
        $response->VIN = $vehicle->vin;
        $response->BodyNumber = $vehicle->body_number;
        $response->ChassisID = (string)$vehicle->body_chassis;
        $response->MaxMass = (int)$vehicle->weight;
        $response->LicensePlate = $vehicle->reg_number;


        $response->ReleaseDate = '01.01.'.$vehicle->car_year;

        $response->EngCap = (int)$vehicle->power;//
        $response->PeriodBeg1 = $period1_begin;
        $response->PeriodEnd1 = $period1_end;




        $response->CarDocument = new \stdClass();
        $response->CarDocument->Type = ($vehicle->doc_type=0)?220219:220220;//220219 Паспорт ТС 220220 Свидетельство о регистрации ТС
        $response->CarDocument->Serial = $vehicle->docserie;
        $response->CarDocument->Number = $vehicle->docnumber;
        $response->CarDocument->Date = $DocDate;


        $response->TicketCarDocument = new \stdClass();
//        $response->TicketCarDocument->Number = '';
//        $response->TicketCarDocument->DiagnosticNextDate = '';
//        $response->TicketCarDocument->DiagnosticDate = '';

        /*
         "TicketCarDocument": {
            "Type": "Код типа документа технического осмотра ТС (Тип: Целое число. Необязательный параметр)",
            "Serial": "Серия документа технического осмотра ТС (Тип: Строка. Необязательный параметр)",
            "Number": "Номер документа технического осмотра ТС (Тип: Строка. Необязательный параметр)",
            "DiagnosticDate": "Дата проведения технического осмотра ТС (Тип: Дата. Необязательный параметр)",
            "DiagnosticNextDate": "Дата проведения следующего технического осмотра ТС (Тип: Дата. Необязательный параметр)"
          },
         */


        $response->TicketCarDocument->Number = $vehicle->dk_number;
        $response->TicketCarDocument->DiagnosticDate = date('d.m.Y', strtotime($vehicle->dk_date_from));
        $response->TicketCarDocument->DiagnosticNextDate = date('d.m.Y', strtotime($vehicle->dk_date));



        if(!$UseForType = $calc->sk_purpose) return Car::response('Не синхронизирован справочник: Цель использования', false);
        if(!$TypeAvto = $calc->sk_category) return Car::response('Не синхронизирован справочник: Категория', false);
        if(!$ModelIsn = $calc->sk_model) return Car::response('Не синхронизирован справочник: Модель', false);

        $response->CatCar = $TypeAvto->vehicle_categorie_sk_id;//Категория ТС (Тип: Строка. Обязательный параметр)"
        $response->UseCarIsn = 4978;//$UseForType->vehicle_purpose_sk_id;//"Код признака использования ТС (Тип: Целое число. Обязательный параметр)"
        $response->MarkModelCarRSACode = $ModelIsn->vehicle_models_sk_id;//"РСА код марки(модели) (Тип: Целое число. Обязательный параметр)"



        return Car::response($response);






    }




    public static function response($result = '', $state = true)
    {
        return (object)['state' => $state, 'result' => $result];
    }


}