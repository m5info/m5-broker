<?php

namespace App\Services\SK\Verna\Services\Auxiliary;


use App\Models\Api\SK\ApiInsuranceAuxiliaryParameters;

class Subject{

    public $Send = null;


    public static function getSubject($subject, $insurance_companies_id, $Send)
    {
        return Subject::createOrUpdata($subject, 0, $Send);
    }





    private static function createOrUpdata($subject, $SubjectId, $Send)
    {
        $docs = '';
        if($subject->type == 0) {
            $sub = Subject::getSubjectFL($subject, $SubjectId);
        }else{
            $sub = "ЮЛ";//Subject::getXmlSubjectUL($subject, $SubjectId);
        }

        return Subject::response($sub);
    }





    private static function getSubjectFL($subject, $SubjectId)
    {
        $response = new \stdClass();

        $data = $subject->get_info();


        $Sex = ($data->sex == 0)?"М":"Ж";
        $name = explode(" ", $subject->title);
        $Birthday = date("d.m.Y", strtotime($data->birthdate));
        $Phone = oldSetPhoneNumberFormat(parsePhoneNumber($data->subject->phone), 'ddd-ddd-dd-dd');
        $Email = $data->subject->email;


        $PhysicalPerson =new \stdClass();
        //$PhysicalPerson->CountryCode = 'Страхователь: ISO-код страны регистрации (Тип: Целое число. Необязательный параметр)';
        $PhysicalPerson->Resident = '1';
        $PhysicalPerson->Juridical = '0';
        $PhysicalPerson->Sex = $Sex;

        $PhysicalPerson->Surname = $name[0];
        $PhysicalPerson->Name = $name[1];
        $PhysicalPerson->Patronymic = $name[2];


        $PhysicalPerson->BirthDate = $Birthday;
        $PhysicalPerson->Phone = $Phone;
        $PhysicalPerson->Email = $Email;

        $PhysicalPerson->PersonDocument = self::getFLDoc($subject);
        $PhysicalPerson->Address =  self::getAddress($subject);

        $response->PhysicalPerson = $PhysicalPerson;

        return $response;
    }




    private static function getFLDoc($subject)
    {
        $data = $subject->get_info();
        $DocDate = setDateTimeFormatRu($data->doc_date, 1);

        $response = new \stdClass();
        $response->Type = '1165';
        $response->Serial = $data->doc_serie;
        $response->Number = $data->doc_number;
        $response->Date = $DocDate;
        $response->IssuerStr = $data->doc_office;
        $response->IssuerCode = $data->doc_info;

        return $response;

    }






    private static function getAddress($subject)
    {
        $data = $subject->get_info();

        $response = new \stdClass();
        $response->StrValue = $data->address_register;
        $response->AreaKLADRCode = $data->address_register_city_kladr_id;
        $response->ApartmentAddress = new \stdClass();
        $response->ApartmentAddress->Street = $data->address_register_street;
        $response->ApartmentAddress->House = $data->address_register_house;
        $response->ApartmentAddress->Building = $data->address_register_block;
        $response->ApartmentAddress->Flat = $data->address_register_flat;

        return $response;

    }




    public static function getDrivers($drivers, $insurance_companies_id, $Send)
    {
        $res = new \stdClass();
        $res->Driver = [];

        foreach ($drivers as $driver){


            $Sex = ($driver->sex == 0)?"М":"Ж";
            $name = explode(" ", $driver->fio);
            $Birthday = setDateTimeFormatRu($driver->birth_date, 1);
            $DrivingDateBeg = setDateTimeFormatRu($driver->exp_date, 1);
            $DocumentDate = setDateTimeFormatRu($driver->doc_date, 1);

            $Driver = new \stdClass();

            //$Driver->CountryCode = '';
            $Driver->Resident = '1';
            $Driver->Juridical = '0';
            $Driver->Sex = $Sex;
            $Driver->Surname = $name[0];
            $Driver->Name = $name[1];
            $Driver->Patronymic = $name[2];
            $Driver->BirthDate = $Birthday;
            $Driver->DrivingDateBeg = $DrivingDateBeg;
            //$Driver->DriverKBM = '';
            $Driver->DriverDocument = new \stdClass();
            $Driver->DriverDocument->Serial = $driver->doc_serie;
            $Driver->DriverDocument->Number = $driver->doc_num;
            //$Driver->DriverDocument->CategoryDriverLicense = '';
            $Driver->DriverDocument->Date = $DocumentDate;

            $res->Driver[] = $Driver;

            /*
             "Driver": {
        "CountryCode": "Водитель: ISO-код страны регистрации (Тип: Целое число. Необязательный параметр)",
        "Resident": "Водитель: признак, является ли резидентом (Тип: Строка. Необязательный параметр. Макс. длина: 1 символов)",
        "Juridical": "Водитель: признак, является ли юридическим лицом (Тип: Строка. Необязательный параметр. Макс. длина: 1 символов)",
        "Sex": "Водитель: пол (Тип: Строка. Необязательный параметр. Макс. длина: 1 символов)",
        "Surname": "Водитель: фамилия (Тип: Строка. Необязательный параметр)",
        "Name": "Водитель: имя (Тип: Строка. Необязательный параметр)",
        "Patronymic": "Водитель: отчество (Тип: Строка. Необязательный параметр)",
        "BirthDate": "Водитель: дата рождения (Тип: Дата. Необязательный параметр)",
        "DrivingDateBeg": "Водитель: дата начала стажа по выбранной категории (Тип: Дата. Необязательный параметр)",
        "DriverKBM": "Водитель: коэффициент КБМ (класс) (Тип: Строка. Необязательный параметр)",
        "DriverDocument": {
          "Serial": "Водительское удостоверение: серия (Тип: Строка. Необязательный параметр)",
          "Number": "Водительское удостоверение: номер (Тип: Строка. Необязательный параметр)",
          "CategoryDriverLicense": "Водительское удостоверение: открытые категории (Тип: Строка. Необязательный параметр)",
          "Date": "Водительское удостоверение: дата выдачи (Тип: Дата. Необязательный параметр)"
        }
             */

        }

        return Subject::response($res);

    }























    private static function createOrUpdataDriver($driver, $SubjectId, $Send)
    {
        $subXml = Subject::getXmlDriverFL($driver, $SubjectId);

        //dd($subXml);

        $result = Subject::sendXML($subXml, $Send);


        if((int)$result->Error->Code == 0){

            if((int)$result->Answer->Subject->SubjectId > 0 && (int)$SubjectId == 0){
                return Subject::createOrUpdataDriver($driver, (int)$result->Answer->Subject->SubjectId, $Send);
            }

            return Subject::response($SubjectId);

        }else{
            return Subject::response((string)$result->Error->Text, false);
        }



    }



    private static function sendXML($subXml, $Send){

        $xml = <<<XML
    <WebRequest>
    <Info> 
        <Source>{$Send->Source}</Source> 
        <Operation>SubjEdit</Operation>
        <UserId>$Send->UserId</UserId> 
        <SessionId>$Send->SessionId</SessionId>
    </Info>
    <Body> 
        $subXml
    </Body>
    </WebRequest>
XML;

        //echo $xml;
        //dd($Send->send($xml));

        return $Send->send($xml);

    }




    private static function response($result = '', $state = true)
    {
        return (object)['state' => $state, 'result' => $result];
    }





    /******************************
     *
     *
     * NEW subject
     *
     *
     ******************************/

    private static function getXmlFLDoc($subject)
    {
        $data = $subject->get_info();

        $DocDate = date("d.m.Y", strtotime($data->doc_date));

        $docs = "<Doc>
                        <DocType>kdPassportRF</DocType>
                        <DocTypeId>1186</DocTypeId>
                        <DocName>Паспорт гражданина РФ</DocName>
                        <DocSeries>".$data->doc_serie."</DocSeries>
                        <DocNumber>".$data->doc_number."</DocNumber>
                        <DocDate>".$DocDate."</DocDate>
                        <DocEndDate></DocEndDate>
                        <DocIssuedBy>".$data->doc_office."</DocIssuedBy>
                        <DocIssuedCode>".$data->doc_info."</DocIssuedCode>
                    </Doc>";

        return $docs;

    }

    private static function getXmlDriverDoc($driver)
    {

        $DocDate = date("d.m.Y", strtotime($driver->doc_date));

        $docs = "<Doc>
                        <DocType>kdDriveLicen</DocType>
                        <DocTypeId>640254630</DocTypeId>
                        <DocName>Водительское удостоверение РФ</DocName>
                        <DocSeries>".$driver->doc_serie."</DocSeries>
                        <DocNumber>".$driver->doc_num."</DocNumber>
                        <DocDate>".$DocDate."</DocDate>
                        <DocEndDate></DocEndDate>
                        <DocIssuedBy></DocIssuedBy>
                        <DocIssuedCode></DocIssuedCode>
                    </Doc>";

        return $docs;

    }





    private static function getXmlDriverFL($driver, $SubjectId)
    {
        $Fullname = $driver->fio;
        $Birthday = date("d.m.Y", strtotime($driver->birth_date));

        if((int)$SubjectId == 0) $Fullname = uniqid();

        $SubjectType = "ФЛ";
        $Resident = "Y";
        $NameLat = '';

        $Age = (int)(((countDayToDates($Birthday, date("d.m.Y")))/365));
        $Sex = ($driver->sex == 0)?"М":"Ж";

        $Experience = date("d.m.Y", strtotime($driver->exp_date));
        $Experience_val = (int)(((countDayToDates($Experience, date("d.m.Y")))/365));
        $docs = Subject::getXmlDriverDoc($driver);

        return <<<XML
            <Subject>
                <SubjectId>$SubjectId</SubjectId>
                <SubjectType>$SubjectType</SubjectType>
                <Fullname>{$Fullname}</Fullname>
                <Resident>$Resident</Resident>
                <NameLat>$NameLat</NameLat>
                <Birthday>$Birthday</Birthday>
                <Age>$Age</Age>
                <Sex>$Sex</Sex>
                <Experience>$Experience</Experience>
                <Experience_val>$Experience_val</Experience_val>
                <Docs>$docs</Docs>
                <Phone></Phone>
                <Email></Email>
                <Addresses>
                    <Address>
                        <AddressType>adrReg</AddressType>
                        <AddressTypeId>640236940</AddressTypeId>
                        <AddressName>Регистрация</AddressName>
                        <Address></Address>
                        <AddressCountry>Росссия</AddressCountry>
                        <AddressKLADR></AddressKLADR>
                        <AddressStreet></AddressStreet>
                        <AddressHouse></AddressHouse>
                        <AddressBuilding></AddressBuilding>
                        <AddressFlat></AddressFlat>
                    </Address>
                </Addresses>
                <Addresses>
                    <Address>
                        <AddressType>adrFact</AddressType>
                        <AddressTypeId>8533</AddressTypeId>
                        <AddressName>Фактический</AddressName>
                        <Address></Address>
                        <AddressCountry>Росссия</AddressCountry>
                        <AddressKLADR></AddressKLADR>
                        <AddressStreet></AddressStreet>
                        <AddressHouse></AddressHouse>
                        <AddressBuilding></AddressBuilding>
                        <AddressFlat></AddressFlat>
                    </Address>
                </Addresses>
                <Addresses>
                    <Address>
                        <AddressType>adrPlBirth</AddressType>
                        <AddressTypeId>8272</AddressTypeId>
                        <AddressName>Место рождения</AddressName>
                        <Address></Address>
                        <AddressCountry>Росссия</AddressCountry>
                        <AddressKLADR></AddressKLADR>
                        <AddressStreet></AddressStreet>
                        <AddressHouse></AddressHouse>
                        <AddressBuilding></AddressBuilding>
                        <AddressFlat></AddressFlat>
                    </Address>
                </Addresses>
            </Subject>
XML;
    }














    private static function getXmlSubjectUL($all_data, $docs)
    {
        $data = (object)$all_data['ul'];

        $SubjectId = (int)$all_data["back_subject_id"];
        $SubjectType = "ЮЛ";
        $Resident = isset($data->is_resident)?((int)$data->is_resident == 1)?"Y":"N":$all_data["default_is_resident"];

        $docs_list = '';
        if($docs){
            for($i=0; $i<count($docs); $i++){

                if((isset($docs['doc_type'][$i]) || isset($docs['default_doc_type'][$i])) && strlen($docs['document_number'][$i]) > 0){

                    $doc_type_id =  $docs['doc_type'][$i];//isset($docs['doc_type'][$i])?(int)$docs['doc_type'][$i]:$docs['default_doc_type'][$i];
                    $doc_type = Dict::getDictVal('GeneralReferenceBook','DocTypes', $doc_type_id);

                    $docs_list .= "<Doc>
                        <DocType>".$doc_type->val."</DocType>
                        <DocTypeId>".$doc_type_id."</DocTypeId>
                        <DocName>".$doc_type->title."</DocName>
                        <DocSeries>".$docs['document_series'][$i]."</DocSeries>
                        <DocNumber>".$docs['document_number'][$i]."</DocNumber>
                        <DocDate>".$docs['doc_date'][$i]."</DocDate>
                        <DocEndDate></DocEndDate>
                        <DocIssuedBy>".$docs['doc_issue_org'][$i]."</DocIssuedBy>
                        <DocIssuedCode>".$docs['doc_code'][$i]."</DocIssuedCode>
                    </Doc>";

                }

            }
        }


        $AddressKLADR = @substr($data->subject_kladr, 0, 17);
        $AddressKLADRFact = @substr($data->fact_subject_kladr, 0, 17);

        //$AddressKLADR = $data->subject_kladr;
        //$AddressKLADRFact = $data->fact_subject_kladr;

        $Phone = parsePhoneNumber($data->phone);
        $Email = $data->email;

        return <<<XML
            <Subject>
                <SubjectId>$SubjectId</SubjectId>
                <SubjectType>$SubjectType</SubjectType>
                <Fullname>{$data->title}</Fullname>
                <Resident>$Resident</Resident>
                <INN>{$data->inn}</INN>
                <KPP>{$data->kpp}</KPP>
                <OGRN>{$data->ogrn}</OGRN>
                <OKVED>{$data->okved}</OKVED>
                <OKPO>{$data->okpo}</OKPO>
                <OPFTypeId>{$data->opf_id}</OPFTypeId>
                <EconomicId>{$data->economic_id}</EconomicId>
                <Docs>$docs_list</Docs>
                <Phone>{$Phone}</Phone>
                <Email>{$Email}</Email>
                <Addresses>
                    <Address>
                        <AddressType>adrLegal</AddressType>
                        <AddressTypeId>8531</AddressTypeId>
                        <AddressName>Юридический</AddressName>
                        <Address>{$data->address}</Address>
                        <AddressCountry>{$data->subject_country}</AddressCountry>
                        <AddressKLADR>{$AddressKLADR}</AddressKLADR>
                        <AddressStreet>{$data->subject_street}</AddressStreet>
                        <AddressHouse>{$data->subject_house}</AddressHouse>
                        <AddressBuilding>{$data->subject_block}</AddressBuilding>
                        <AddressFlat>{$data->subject_flat}</AddressFlat>
                    </Address>
                </Addresses>
                <Addresses>
                    <Address>
                        <AddressType>adrFact</AddressType>
                        <AddressTypeId>8533</AddressTypeId>
                        <AddressName>Фактический</AddressName>
                        <Address>{$data->fact_address}</Address>
                        <AddressCountry>{$data->fact_subject_country}</AddressCountry>
                        <AddressKLADR>{$AddressKLADRFact}</AddressKLADR>
                        <AddressStreet>{$data->fact_subject_street}</AddressStreet>
                        <AddressHouse>{$data->fact_subject_house}</AddressHouse>
                        <AddressBuilding>{$data->fact_subject_block}</AddressBuilding>
                        <AddressFlat>{$data->fact_subject_flat}</AddressFlat>
                    </Address>
                </Addresses>
        </Subject>
XML;
    }



    public static function getTempDrivers($drivers, $insurance_companies_id, $Send)
    {
        $res = '';
        foreach ($drivers as $driver){

            $res .= "<Driver><Age>{$driver->age}</Age><Experience_val>{$driver->exp}</Experience_val></Driver>";
        }

        return Subject::response($res);
    }




}