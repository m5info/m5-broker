<?php

namespace App\Services\SK\Verna\Services\Osago;


use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Services\Calc\DefaultOSAGO;
use App\Services\SK\Verna\Services\Auxiliary\Car;
use App\Services\SK\Verna\Services\Auxiliary\Subject;
use App\Services\SK\Verna\Services\SendAPI\Send;

class OsagoApi{


    public $url;
    public $login;
    public $password;
    public $errors = [];
    public $Send = null;
    public $SessionId = null;

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->Send = new Send($url, $login, $password, $api_setting);

    }



    public function setTariff(ContractsCalculation $calc)
    {
        $tariffParams = $this->getTariffParams($calc);

        $result = $this->Send->send('products/osago/tariff', $tariffParams, 'POST');

        if(isset($result->data->error)){
            $errors = '';
            if(is_array($result->data->error)){
                foreach($result->data->error as $error){
                    $errors .= $error->text;
                }
            }else{
                $errors = $result->data->error->row->text;
            }

            $this->responseError($errors);
        }else{
            $baseTariff = \GuzzleHttp\json_encode($result->data->result->baseTariff);

            $calc->json = $baseTariff;
            $calc->save();
        }
    }

    public function calc(ContractsCalculation $calc)
    {
        $this->setTariff($calc);

        if(count($this->errors)){
            return $this->responseError(implode('; ', $this->errors));
        }

        return $this->getMainJson($calc);
    }



    public function setPayment(ContractsCalculation $calc)
    {
        $result = (object)['state' => true, 'data' => new \stdClass()];
        $contract = $calc->contract;


        if($contract->is_epolicy){

            payment_url:

            if($contract->statys_id == 2){ // после расчета

                $paymentUrl = $this->getPaymentUrl($calc);

                $json = $calc->json ? \GuzzleHttp\json_decode($calc->json) : new \stdClass();
                $json->payment_url = $paymentUrl;
                $calc->json = json_encode($json);
                $calc->save();

                $contract->statys_id = 3;
                $contract->save();
            } else {
                $json = \GuzzleHttp\json_decode($calc->json);

                if(!isset($json->payment_url)){ // если ссылки на платеж не оказалось
                    $contract->statys_id = 2;
                    $contract->save();
                    goto payment_url;
                }

                return $this->getInvoice($calc);// получаем доки
            }

            $result->data->payment_url = $paymentUrl;

            return $result;
        }else {
            return $this->createOsagoAgreementWithBso($calc);
        }

        return $this->responseError("ERROR");
    }


    public function getPaymentUrl(ContractsCalculation $calc)
    {
        $contract = $calc->contract;

        $params = [
            'partner_failure_url' => url("contracts/online/edit/$contract->id"),
            'partner_success_url' => url("contracts/online/edit/$contract->id"),
            'postbank_url' => url("contracts/online/edit/$contract->id"),
            'isn' => $calc->sk_key_id,
        ];

        $paymentUrl = $this->Send->send('getInvoicePaymentUrl', $params, 'GET');

        if($paymentUrl){
            return urldecode($paymentUrl->data->result->url);
        }

        return $this->responseError("ERROR");
    }


    public function controlPaymentXml($xml, $rec = 0)
    {

        return $this->responseError("ERROR");
    }

    public function getInvoice(ContractsCalculation $calc)
    {
        $answer = $this->Send->send('invc/'.$calc->sk_key_id, [], 'GET');

        dd($answer);
    }


    public function createOsagoAgreementWithBso($calc)
    {
        $params = new \stdClass();
        $params->bso_number = '5015953042';
        $params->bso_series = 'МММ';
        $params->isn = $calc->sk_key_id;

        $response = $this->Send->send('products/osago/createOsagoAgreementWithBso', $params, 'POST');


        dd($response);
    }


    public function getMainJson($calc)
    {



        $contract = $calc->contract;
        $ContractId = strlen($calc->sk_key_id)>0?$calc->sk_key_id:0;

        //$response = $this->Send->send('products/osago/dicti?userConstName=ucCatalogDocumentClasses', null, 'GET');



        $Multidrive = ($contract->drivers_type_id == 1)?'N':"Y";

        $AgreementCalc = new \stdClass();

        $AgreementCalc->IsEpolicy = $contract->is_epolicy ? "Y" : "N";

        /*
        $AgreementCalc->BSO = new \stdClass();
        $AgreementCalc->BSO->Number = '';
        $AgreementCalc->BSO->Series = '';
        */


        $AgreementCalc->IsProlongation = '0';//"Признак пролонгации (Тип: Строка. Обязательный параметр. Макс. длина: 1 символов)"
        $AgreementCalc->IsAddendum = '0'; //"Признак дополнения (Тип: Строка. Обязательный параметр. Макс. длина: 1 символов)"
        $AgreementCalc->DriversRestriction = $Multidrive; //"Полис безлимитный (N), или с ограничением числа водителей по списку (Y) (Тип: Строка. Обязательный параметр. Макс. длина: 1 символов)"
        $AgreementCalc->PolicyKBM = '';//"Итоговый КБМ по полису (класс) (Тип: Строка. Необязательный параметр)"


        $sign_date = date("d.m.Y", strtotime($contract->sign_date));
        $begin_date = date("d.m.Y H:i", strtotime($contract->begin_date));
        $end_date = date("d.m.Y", strtotime($contract->end_date));


        $AgreementCalc->DateSign = $sign_date;
        $AgreementCalc->DateBeg = $begin_date;
        $AgreementCalc->DateEnd = $end_date;


        $AgreementCalc->InsurancePremium = '';
        $AgreementCalc->DateInsurancePremium = date("d.m.Y H:i:s");
        //$AgreementCalc->OSAGOKoef = ['TB'=>'','KT'=>'','KBM'=>'','KO'=>'','KVS'=>'','KM'=>'','KS'=>'','KN'=>''];



        $insurer = Subject::getSubject($contract->insurer, $calc->insurance_companies_id, $this->Send);
        if($insurer->state == false){
            return $this->responseError($insurer->result);
        }




        $owner = Subject::getSubject($contract->owner, $calc->insurance_companies_id, $this->Send);
        if($owner->state == false){
            return $this->responseError($owner->result);
        }


        $drivers = new \stdClass();
        $drivers->result = null;

        if(($contract->drivers && sizeof($contract->drivers)) && $contract->drivers_type_id == 0){
            $drivers = Subject::getDrivers($contract->drivers, $calc->insurance_companies_id, $this->Send);
            if($drivers->state == false){
                return $this->responseError($drivers->result);
            }
        }

        $AgreementCalc->PolicyOwner = $insurer->result;

        if($drivers->result && $drivers->result!=null){
            $AgreementCalc->DriversPart = $drivers->result;
        }




        $car = Car::getCarOSAGO($calc, $owner);

        if($car->state == false){
            return $this->responseError($car->result);
        }

        $AgreementCalc->CAR = $car->result;


        $contractJson = new \stdClass();
        $contractJson->AgreementCalc = $AgreementCalc;

        //dd($contractJson);

//        dump($contractJson);

        $response = $this->Send->send('products/osago/createcalc', $contractJson, 'POST');

        //dd($response);
        //Ошибка
        /*
         +"data": {#1734 ▼
        +"error": {#1702 ▼
          +"code": "105"
          +"parameter": "AgreementCalc-PolicyOwner-PhysicalPerson-Email"
          +"value": ""
          +"hash": "null"
          +"text": "не заполнен обязательный параметр"
        }

        Успех
        +"data": {#1724 ▼
    +"result": {#1696 ▼
      +"CalculatedPremium": "5914,11"
      +"CalculatedPremiumStr": "5 914,11 руб."
      +"TariffStr": "Тб=4 118.00; Кт=1.70; Кбм=0.80; Квс=0.96; Ко=1.00; Км=1.10; Кс=1.00; Кн=1.00; Прем=5 914.11"
      +"InvoiceIsn": "105753462"
    }
  }


         */

//        dd($response);


        //dd($response);

        //echo $xml;
        //dd("OK");

        //$response = $this->Send->send($xml);
        return $response;

    }


    public function getTariffParams(ContractsCalculation $calc)
    {
        $contract = $calc->contract;
        $owner = $contract->owner->get_info();
        $params = new \stdClass();

        if($calc->sk_purpose){
            $params->categoryIsn = $calc->sk_purpose->vehicle_purpose_sk_id;
            $params->kladrCode = $owner->address_fact_city_kladr_id;
            $params->dateSign = date('d.m.Y', strtotime($contract->sign_date));
        }else {
            $this->errors[] = 'Не синхронизирован справочник Целей использования!';
        }

        return $params;
    }


    public function getContractXML($calc)
    {
        $contract = $calc->contract;

        $sign_date = date("d.m.Y", strtotime($contract->sign_date));
        $begin_date = date("d.m.Y H:i", strtotime($contract->begin_date));
        $end_date = date("d.m.Y", strtotime($contract->end_date));

        $period1_begin = date("d.m.Y", strtotime($contract->begin_date));
        $period1_end = date("d.m.Y", strtotime($contract->end_date));

        $UseCaravan = $contract->object_insurer_auto->is_trailer;

        $Multidrive = ($contract->drivers && sizeof($contract->drivers))?'N':"Y";

        $xml = <<<XML
        
            <DateBeg>{$begin_date}</DateBeg> 
            <DateEnd>{$end_date}</DateEnd> 
            <PeriodBeg1>{$period1_begin}</PeriodBeg1>
            <PeriodEnd1>{$period1_end}</PeriodEnd1>
            <PeriodBeg2></PeriodBeg2>
            <PeriodEnd2></PeriodEnd2>
            <PeriodBeg3></PeriodBeg3>
            <PeriodEnd3></PeriodEnd3>
            <Remark></Remark>
            <Multidrive>{$Multidrive}</Multidrive>
            <Violations>N</Violations>
            <UseCaravan>{$UseCaravan}</UseCaravan>
        
XML;

        return $xml;
    }


    private function responseError($error)
    {
        $r = new \stdClass();
        $r->data = new \stdClass();
        $r->data->error = new \stdClass();
        $r->data->error->code = 10;
        $r->data->error->text = $error;
        return $r;
    }



}