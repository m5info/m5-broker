<?php

namespace App\Services\SK\Verna\Services\SendAPI;


use Doctrine\DBAL\Cache\CacheException;

class Send{


    public $url;
    public $login;
    public $password;

    public $api_setting = null;


    private $state_api_work = 0;


    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

        $this->api_setting = $api_setting;

    }

    public function send($method, $data, $type = 'POST')
    {
        $curl = curl_init();
        $headers = [];

        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $headers[] = 'Authorization: Bearer '.$this->password;

        //dd($headers);

        $json = '';
        if($data){
            $json = 'params='.(string)\GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
//            echo $json;
            //exit();
        }

        if($type == 'POST'){
            $url = $this->url.$method;
        }else {
            if($data){
                $url = $this->url.$method."?".http_build_query($data);
            }else{
                $url = $this->url.$method;
            }
            $json = null;
        }


        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);



        curl_close($curl);

        if ($err) {

//            dd($err);

            //Логируем ошибки
            return null;
        }

//dd($response);
       return \GuzzleHttp\json_decode($response);

    }




    public function responseError($txt)
    {
        $res = new \stdClass();
        $res->Error = new \stdClass();
        $res->Error->Code = 10;
        $res->Error->Text = $txt;
        return $res;
    }

}