<?php

namespace App\Services\SK\Verna\Services\Dictionary;

use App\Services\SK\Verna\Services\SendAPI\Send;

class DictionaryApi{


    public $url;
    public $login;
    public $password;

    public $Send = null;

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->Send = new Send($url, $login, $password, $api_setting);

    }

    public function query($param){
        //авторизация, или подготовка параметров авторизации для запроса, если нужно
        //собираем запрос для сервиса для конкретной ск
        //отправляем
    }



    public function get_purpose(){


        //Дергаем справочник
        $arr = [];
        $response = $this->Send->send('products/osago/categories', null, 'GET');

        if($response && isset($response->data) && isset($response->data->result) && isset($response->data->result->row))
        {
            foreach ($response->data->result->row as $row){
                $arr[] = ['id'=>$row->isn, 'title'=>$row->remark];

            }
        }


        return $arr;
    }

    public function get_categories(){
        //Дергаем справочник Категорий

        $arr = [];
        $response = $this->Send->send('products/osago/categories', null, 'GET');

        if($response && isset($response->data) && isset($response->data->result) && isset($response->data->result->row))
        {
            foreach ($response->data->result->row as $row){
                $arr[] = ['id'=>$row->isn, 'title'=>$row->remark];

            }
        }


        return $arr;
    }

    public function get_marks_models(){
        return $this->Send->send('products/osago/getrsacarmodels', null, 'GET');
    }




    public function get_modifications($mark_id, $model_id){
        //подготавливаем данные для запроса модификации
        //дёргаем свой query
        return [];

    }

}