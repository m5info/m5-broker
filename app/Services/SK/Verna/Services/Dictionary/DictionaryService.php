<?php

namespace App\Services\SK\Verna\Services\Dictionary;

use App\Interfaces\Services\SK\DictionaryServiceInterface;

class DictionaryService implements DictionaryServiceInterface {


    public $api;

    public $cache_json = __DIR__."/category_mark_model.json";

    public function __construct(DictionaryApi $api){
        $this->api = $api;
    }

    public function get_marks_models(){

        $categories = [];
        $marks = [];
        $models = [];

        if(!is_file($this->cache_json))
        {
            $response = $this->api->get_marks_models();

            if($response && isset($response->data) && isset($response->data->result) && isset($response->data->result->row))
            {

                $temp_categories = [];
                $temp_marks = [];
                $temp_models = [];

                foreach ($response->data->result->row as $row){

                    $temp_categories[$row->categoryId] = ['id' => $row->categoryId, 'title' => $row->categoryName];
                    $temp_marks[$row->markaId] = ['id' => $row->markaId, 'title' => $row->markaName, 'categorie_sk_id' => $row->categoryId];
                    $temp_models[$row->modelId] = ['id' => $row->modelId, 'rsa_id' => $row->RSACode, 'title' => $row->modelName,  'marka_id' => $row->markaId, 'categorie_sk_id' => $row->categoryId];
                }
            }

            foreach ($temp_categories as $sk_category){
                $categories[] = [
                    'id' => $sk_category['id'],
                    'title' => $sk_category['title'],
                ];
            }

            foreach ($temp_marks as $sk_mark){
                $marks[] = [
                    'id' => (string) $sk_mark['id'],
                    'title' => (string) $sk_mark['title'],
                    'vehicle_categorie_sk_id'=>(string) $sk_mark['categorie_sk_id'],
                ];
            }

            foreach ($temp_models as $sk_model){

                $models[] = [
                    'id' => (string) $sk_model['rsa_id'],
                    'title' => (string) $sk_model['title'],
                    'vehicle_mark_sk_id' => (string) $sk_model['marka_id'],
                    'vehicle_categorie_sk_id' =>  $sk_model['categorie_sk_id']
                ];
            }


            $result = [
                'categories' => $categories,
                'marks' => $marks,
                'models' => $models,
            ];

            file_put_contents($this->cache_json, \GuzzleHttp\json_encode($result));

            return $result;


        }else{

            return \GuzzleHttp\json_decode(file_get_contents($this->cache_json),true);
        }

    }


    public function get_api_purpose(){
        return $this->api->get_purpose();
    }

    public function get_api_categories(){

        return $this->get_marks_models()['categories'];

        //return $this->api->get_categories();
    }


}