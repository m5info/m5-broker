<?php

namespace App\Services\SK\TriadDK;

use App\Services\SK\BaseServiceController;
use App\Services\SK\TriadDK\Services\Boxes\BoxesService;
use App\Services\SK\TriadDK\Services\Boxes\BoxesApi;

class ServiceController extends BaseServiceController
{
    const SERVICES = [

        'boxes' => [
            'service' => BoxesService::class,
            'api' => BoxesApi::class,
        ],

    ];


    const PROGRAMS = [
        'boxes' => [
            ['id' => 1, 'name' => 'dk', 'template' => 'default', 'title' => 'Диагностическая карта'],
        ]

    ];

}
