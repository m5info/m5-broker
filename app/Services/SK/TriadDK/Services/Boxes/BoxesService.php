<?php

namespace App\Services\SK\TriadDK\Services\Boxes;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\ContractsCalculation;

class BoxesService implements ProductServiceInterface{


    public $api;

    public function __construct(BoxesApi $api){
        $this->api = $api;

    }

    public function temp_calc(ContractsCalculation $calc)
    {

        return null;
    }

    public function calc(ContractsCalculation $calc)
    {

        return null;
    }


    public function release(ContractsCalculation $calc){

    }


    public function check_status(ContractsCalculation $calc){

    }


    public function get_files(ContractsCalculation $calc){

    }

    public function checkLimit()
    {


        return true;
    }


}