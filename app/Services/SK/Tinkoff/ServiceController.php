<?php

namespace App\Services\SK\Tinkoff;

use App\Services\SK\BaseServiceController;
use App\Services\SK\Tinkoff\Services\Kasko\KaskoApi;
use App\Services\SK\Tinkoff\Services\Dictionary\DictionaryApi;
use App\Services\SK\Tinkoff\Services\Dictionary\DictionaryService;
use App\Services\SK\Tinkoff\Services\Kasko\KaskoService;
use App\Services\SK\Tinkoff\Services\Osago\OsagoApi;
use App\Services\SK\Tinkoff\Services\Osago\OsagoService;

class ServiceController extends BaseServiceController {


    const SERVICES = [

        'dictionary' => [
            'service' => DictionaryService::class,  /// Можно проставить для удобства конечно им префиксы типа "AlphaDictionaryService::class"
            'api' => DictionaryApi::class,          /// но наверное необязательно
        ],
        'osago' => [
            'service' => OsagoService::class,
            'api' => OsagoApi::class,
        ],
        'kasko' => [
            'service' => KaskoService::class,
            'api' => KaskoApi::class,
        ],


    ];


    const PROGRAMS = [
        'kasko' => [
            ['id' => 1, 'name' => 'KaskoDriveOptimum', 'template' => 'default', 'title' => 'КАСКО Драйв Optimum'],
            ['id' => 2, 'name' => 'KaskoDriveLiabilityMax', 'template' => 'default', 'title' => 'КАСКО Драйв LIABILITY MAX'],
            ['id' => 3, 'name' => 'KaskoDriveTheft', 'template' => 'default', 'title' => 'КАСКО Драйв THEFT'],
            ['id' => 4, 'name' => 'KaskoDriveMax', 'template' => 'default', 'title' => 'КАСКО Драйв MAX'],
        ]

    ];


}