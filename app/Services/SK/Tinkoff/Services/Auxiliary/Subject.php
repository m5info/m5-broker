<?php

namespace App\Services\SK\Tinkoff\Services\Auxiliary;


use App\Models\Api\SK\ApiInsuranceAuxiliaryParameters;
use App\Services\DData\DDataGetAdress;
use Illuminate\Support\Str;

class Subject{

    public $Send = null;


    public static function getSubject($subject, $insurance_companies_id, $num = 1)
    {


        $SK_XML_ID = "";

        $auxiliary = ApiInsuranceAuxiliaryParameters::where('insurance_companies_id', $insurance_companies_id)
            ->where('type_id', 0)
            ->where('local_id', $subject->id)->get()->first();



        if($auxiliary){

            if(strlen($auxiliary->sk_id) > 3){
                $SK_XML_ID = '<ns1:ID xmlns:ns1="http://toi.ru/partner/model/contact">'. $auxiliary->sk_id .'</ns1:ID>';//закомментировать на 1 расчет при переходе с тестового на боевой сервер тинькофф и обратно в OsagoApi get_calc_params() и в Subjects getSubject() и getDriver()
            }

        }else{

            ApiInsuranceAuxiliaryParameters::create([
                'insurance_companies_id' => $insurance_companies_id,
                'type_id' => 0,
                'local_id' => $subject->id,
                'sk_id' => $num,
                'auxiliary_parameters' => '',
            ]);
        }




        if($subject->type == 0) {
            $subXml = self::getXmlSubjectFL($subject);
        }else{
            $subXml = self::getXmlSubjectUL($subject);
        }

        $response = <<<XML
<ns0:subjectInfo>
    {$SK_XML_ID}
    <ns1:subjectNumber xmlns:ns1="http://toi.ru/partner/model/contact">{$num}</ns1:subjectNumber>
    <ns2:subjectDetails xmlns:ns2="http://toi.ru/partner/model/contact">
        
      {$subXml}
    </ns2:subjectDetails>
  </ns0:subjectInfo>
XML;


        return $response;

    }


    private static function getXmlSubjectFL($subject)
    {


        $data = $subject->get_info();

        $Sex = ($data->sex == 0)?"male":"female";
        $name = explode(" ", $subject->title);
        $Birthday = date("Y-m-d", strtotime($data->birthdate));
        $Phone = setPhoneNumberFormat(parsePhoneNumber($data->subject->phone), '7dddddddddd');
        $Email = $data->subject->email;


        $driver = $data->driver;
        $docs_driver_license = '';
        if($driver){

            $DocDateLicense = date("Y-m-d", strtotime($driver->doc_date));
            $docs_driver_license = "
             <ns2:document>
                <ns2:documentType>driver_license_russian_B</ns2:documentType>
                <ns2:series>{$driver->doc_serie}</ns2:series>
                <ns2:number>{$driver->doc_num}</ns2:number>
                <ns2:dateIssue>{$DocDateLicense}</ns2:dateIssue>
              </ns2:document>
            ";

            if($old_license_data = $driver->old_license){

                $old_doc_date_issue = date('Y-m-d', strtotime($old_license_data->doc_date_issue));

                $previous_driver_data = <<<XML
<ns2:document>
    <ns2:documentType>previous_driver_license</ns2:documentType>
    <ns2:series>$old_license_data->doc_series</ns2:series>
    <ns2:number>$old_license_data->doc_number</ns2:number>
    <ns2:dateIssue>$old_doc_date_issue</ns2:dateIssue>
    <ns2:lastName>$old_license_data->last_name</ns2:lastName>
    <ns2:firstName>$old_license_data->first_name</ns2:firstName>
    <ns2:middleName>$old_license_data->middle_name</ns2:middleName>
</ns2:document>
XML;

            }else{
                $previous_driver_data = '';
            }
        }



        $DocDate = date("Y-m-d", strtotime($data->doc_date));

        $dadata = new DDataGetAdress();
        $registerAdress = $dadata->getAddress($data->address_register)->suggestions[0]->data;



        $registerRegion = Str::substr($registerAdress->region_kladr_id, 0, 2);
        $registerKLADR6 = $registerAdress->house_kladr_id ?? $registerAdress->kladr_id;
        $registerKLADR5 = $registerAdress->street_kladr_id ?? $registerAdress->kladr_id;
        $registerKLADR3 = $registerAdress->city_kladr_id;
        $registerKLADR1 = $registerAdress->region_kladr_id;

        $factAdress = $dadata->getAddress($data->address_fact)->suggestions[0]->data;
        $factRegion = Str::substr($registerAdress->region_kladr_id, 0, 2);
        $factKLADR6 = $data->address_fact_kladr;
        $factKLADR5 = Str::substr($data->address_fact_kladr, 0, 15);
        $factKLADR3 = Str::substr($data->address_fact_kladr, 0, 13);
        $factKLADR1 = Str::substr($data->address_fact_kladr, 0, 13);


        return <<<XML
     <ns2:lastName>{$name[0]}</ns2:lastName>
      <ns2:firstName>{$name[1]}</ns2:firstName>
      <ns2:middleName>{$name[2]}</ns2:middleName>
      <ns2:birthdate>{$Birthday}</ns2:birthdate>
      <ns2:email>{$Email}</ns2:email>
      <ns2:gender>{$Sex}</ns2:gender>
      <ns2:citizenship>RU</ns2:citizenship>
      
      <ns2:address>
        <ns2:addressType>registration</ns2:addressType>
        <ns2:country>{$registerAdress->country_iso_code}</ns2:country>
        <ns2:postCode>{$registerAdress->postal_code}</ns2:postCode>
        <ns2:region>{$registerRegion}</ns2:region>
        <ns2:KLADR1>{$registerKLADR1}</ns2:KLADR1>
        <ns2:city>{$registerAdress->city_with_type}</ns2:city>
        <ns2:KLADR3>{$registerKLADR3}</ns2:KLADR3>
        <ns2:street>{$registerAdress->street_with_type}</ns2:street>
        <ns2:KLADR5>{$registerKLADR5}</ns2:KLADR5>
        <ns2:building>{$registerAdress->block}</ns2:building>
        <ns2:KLADR6>{$registerKLADR6}</ns2:KLADR6>
        <ns2:flat>{$registerAdress->flat}</ns2:flat>
      </ns2:address>
      
      <ns2:address>
       <ns2:addressType>home</ns2:addressType>
        <ns2:country>{$factAdress->country_iso_code}</ns2:country>
        <ns2:postCode>{$factAdress->postal_code}</ns2:postCode>
        <ns2:region>{$factRegion}</ns2:region>
        <ns2:KLADR1>{$factKLADR1}</ns2:KLADR1>
        <ns2:city>{$factAdress->city_with_type}</ns2:city>
        <ns2:KLADR3>{$factKLADR3}</ns2:KLADR3>
        <ns2:street>{$factAdress->street_with_type}</ns2:street>
        <ns2:KLADR5>{$factKLADR5}</ns2:KLADR5>
        <ns2:building>{$factAdress->block}</ns2:building>
        <ns2:KLADR6>{$factKLADR6}</ns2:KLADR6>
        <ns2:flat>{$factAdress->flat}</ns2:flat>
      </ns2:address>
      
      <ns2:document>
        <ns2:documentType>passport_russian</ns2:documentType>
        <ns2:series>{$data->doc_serie}</ns2:series>
        <ns2:number>{$data->doc_number}</ns2:number>
        <ns2:dateIssue>{$DocDate}</ns2:dateIssue>
        <ns2:subdivisionCode>{$data->doc_office}</ns2:subdivisionCode>
      </ns2:document>
      
      {$docs_driver_license}
      
      {$previous_driver_data}      
      
      <ns2:phone>
        <ns2:isPrimary>true</ns2:isPrimary>
        <ns2:typePhone>mobile</ns2:typePhone>
        <ns2:numberPhone>{$Phone}</ns2:numberPhone>
      </ns2:phone>
XML;
    }

    private static function getXmlSubjectUL($subject)
    {


        return <<<XML

XML;
    }




    public static function getDriver($subject, $insurance_companies_id, $num)
    {


        $Sex = ($subject->sex == 0)?"male":"female";
        $name = explode(" ", $subject->fio);
        $Birthday = date("Y-m-d", strtotime($subject->birth_date));

        $DocDateLicense = date("Y-m-d", strtotime($subject->doc_date));


        $SK_XML_ID = "";

        $auxiliary = ApiInsuranceAuxiliaryParameters::where('insurance_companies_id', $insurance_companies_id)
            ->where('type_id', 0)
            ->where('local_id', $subject->id)->get()->first();

        if($auxiliary){

            if(strlen($auxiliary->sk_id) > 3){
//                $SK_XML_ID = "<ns1:ID>{$auxiliary->sk_id}</ns0:ID>";
                  $SK_XML_ID = '<ns1:ID xmlns:ns1="http://toi.ru/partner/model/contact">'. $auxiliary->sk_id .'</ns1:ID>'; //закомментировать на 1 расчет при переходе с тестового на боевой сервер тинькофф и обратно в OsagoApi get_calc_params() и в Subjects getSubject() и getDriver()
            }

        }else{



            ApiInsuranceAuxiliaryParameters::create([
                'insurance_companies_id' => $insurance_companies_id,
                'type_id' => 0,
                'local_id' => $subject->id,
                'sk_id' => $num,
                'auxiliary_parameters' => '',
            ]);
        }

        if($old_license_data = $subject->old_license){

            $old_doc_date_issue = date('Y-m-d', strtotime($old_license_data->doc_date_issue));

            $previous_driver_data = <<<XML
<ns2:document>
    <ns2:documentType>previous_driver_license</ns2:documentType>
    <ns2:series>$old_license_data->doc_series</ns2:series>
    <ns2:number>$old_license_data->doc_number</ns2:number>
    <ns2:dateIssue>$old_doc_date_issue</ns2:dateIssue>
    <ns2:lastName>$old_license_data->last_name</ns2:lastName>
    <ns2:firstName>$old_license_data->first_name</ns2:firstName>
    <ns2:middleName>$old_license_data->middle_name</ns2:middleName>
</ns2:document>
XML;

        }else{
            $previous_driver_data = '';
        }

        return <<<XML
        
        <ns0:subjectInfo>
        {$SK_XML_ID}
    <ns1:subjectNumber xmlns:ns1="http://toi.ru/partner/model/contact">{$num}</ns1:subjectNumber>
    <ns2:subjectDetails xmlns:ns2="http://toi.ru/partner/model/contact">
      
      <ns2:lastName>{$name[0]}</ns2:lastName>
      <ns2:firstName>{$name[1]}</ns2:firstName>
      <ns2:middleName>{$name[2]}</ns2:middleName>
      <ns2:birthdate>{$Birthday}</ns2:birthdate>
      <ns2:gender>{$Sex}</ns2:gender>
      <ns2:citizenship>RU</ns2:citizenship>
      
      
      <ns2:document>
        <ns2:documentType>driver_license_russian_B</ns2:documentType>
        <ns2:series>{$subject->doc_serie}</ns2:series>
        <ns2:number>{$subject->doc_num}</ns2:number>
        <ns2:dateIssue>{$DocDateLicense}</ns2:dateIssue>
      </ns2:document>
      
        {$previous_driver_data}
      
    </ns2:subjectDetails>
  </ns0:subjectInfo>
        
      
XML;
    }


    public static function updateSubjects($contract, $insurance_companies_id, $result)
    {
        $subjectNum = 1;

        $subjectInfo = [];
        if(is_array($result->subjectInfo)){
            $subjectInfo = $result->subjectInfo;
        }else{
            $subjectInfo[] = $result->subjectInfo;
        }

        Subject::updateSubjectSkId($contract->insurer->id, $insurance_companies_id, Subject::getSkId($subjectInfo, $subjectNum), $result->setNumber);

        if($contract->insurer->id != $contract->owner->id){
            $subjectNum++;
            Subject::updateSubjectSkId($contract->owner->id, $insurance_companies_id, Subject::getSkId($subjectInfo, $subjectNum), $result->setNumber);
        }

        $ownerSubjectNum = $subjectNum;

        if((int)$contract->drivers_type_id != 1){

            if(($contract->drivers && sizeof($contract->drivers))){

                foreach ($contract->drivers as $driver){
                    if($contract->insurer->id == $driver->subject_id){
                        Subject::updateSubjectSkId($driver->subject_id, $insurance_companies_id, Subject::getSkId($subjectInfo, $ownerSubjectNum), $result->setNumber);
                    }else{
                        $subjectNum++;
                        Subject::updateSubjectSkId($driver->id, $insurance_companies_id, Subject::getSkId($subjectInfo, $subjectNum), $result->setNumber);
                    }
                }
            }
        }
        return true;
    }

    /**
     * @param $subject_id - subject_id в наше системе
     * @param $insurance_companies_id - тинькофф
     * @param $sk_id - идшник subject'а
     * @param $sk_key_id - идшник запроса
     * @return bool
     */
    public static function updateSubjectSkId($subject_id, $insurance_companies_id, $sk_id, $sk_key_id)
    {
        $auxiliary = ApiInsuranceAuxiliaryParameters::where('insurance_companies_id', $insurance_companies_id)
            ->where('type_id', 0)
            ->where('local_id', $subject_id)->get()->first();

        if(!$auxiliary){
            $auxiliary = ApiInsuranceAuxiliaryParameters::create([
                'insurance_companies_id' => $insurance_companies_id,
                'type_id' => 0,
                'local_id' => $subject_id,
                'auxiliary_parameters' => '',
            ]);
        }

        $auxiliary->auxiliary_parameters = $sk_key_id;
        $auxiliary->sk_id = $sk_id;
        $auxiliary->save();
        return true;
    }

    public static function getSkId($result, $num){
        foreach ($result as $subject){
            //dump($subject);

            if((int)$subject->subjectNumber == $num){
                return $subject->ID;
            }
        }

        return $num;
    }


}