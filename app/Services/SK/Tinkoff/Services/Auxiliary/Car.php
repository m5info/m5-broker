<?php

namespace App\Services\SK\Tinkoff\Services\Auxiliary;


use App\Models\Api\SK\ApiInsuranceAuxiliaryParameters;
use App\Models\Contracts\ContractsCalculation;
use App\Services\DData\DDataGetAdress;
use Illuminate\Support\Str;

class Car{



    public static function getCarOSAGO(ContractsCalculation $calc)
    {
        $contract = $calc->contract;
        $vehicle = $contract->object_insurer_auto;



        $DocType = ($vehicle->doc_type == 0)?"PTS":"STS";
        $DocDate = getDateFormatEn($vehicle->docdate, 1);
        $DocSeries = $vehicle->docserie;
        $DocNumber = $vehicle->docnumber;

        $DiagnosticCard = self::diagnosticCard($calc);

        if(!$UseForType = $calc->sk_purpose) return Car::response('Не синхронизирован справочник: Цель использования', false);
        if(!$TypeAvto = $calc->sk_category) return Car::response('Не синхронизирован справочник: Категория', false);
        if(!$BrandIsn = $calc->sk_mark) return Car::response('Не синхронизирован справочник: Марка', false);
        if(!$ModelIsn = $calc->sk_model) return Car::response('Не синхронизирован справочник: Модель', false);


        $Powerhp = (int)$vehicle->power;

        $isUsedWithTrailer = ((int)$vehicle->is_trailer == 1)?"true":"false";



        $xml = <<<XML
    
        <ns0:vehicleInfo>
    <ns0:vehicleDetails>
      <ns0:vehicleReferenceInfo>
      
        <ns0:vehicleReferenceDetails>
        
          <ns0:modelID>{$ModelIsn->vehicle_models_sk_id}</ns0:modelID>
          
          <ns0:engPwrHP>{$Powerhp}</ns0:engPwrHP>
          
          <ns0:caseTypeID>22</ns0:caseTypeID>
          
          <ns0:kppTypeID>7</ns0:kppTypeID>
          
        </ns0:vehicleReferenceDetails>
        
      </ns0:vehicleReferenceInfo>
      <ns0:isChangeNumAgg>false</ns0:isChangeNumAgg>
      <ns0:countryOfRegistration>
        <ns0:isNoCountryOfRegistration>false</ns0:isNoCountryOfRegistration>
        <ns0:countryOfRegistration>RU</ns0:countryOfRegistration>
      </ns0:countryOfRegistration>
      
      
      <ns0:vehicleDocument>
        <ns0:documentType>{$DocType}</ns0:documentType>
        <ns0:documentSeries>{$DocSeries}</ns0:documentSeries>
        <ns0:documentNumber>{$DocNumber}</ns0:documentNumber>
        <ns0:documentIssued>{$DocDate}</ns0:documentIssued>
      </ns0:vehicleDocument>
      
      
      
      <ns0:chassis>
        <ns0:isChassisMissing>false</ns0:isChassisMissing>
      </ns0:chassis>
      
      
      <ns0:credit>
        <ns0:isCredit>false</ns0:isCredit>
      </ns0:credit>
      
      
      <ns0:isKeyless>false</ns0:isKeyless>
      
      <ns0:isUsedWithTrailer>{$isUsedWithTrailer}</ns0:isUsedWithTrailer>
      
      
      <ns0:kuzovNumber>
        <ns0:isKuzovMissing>false</ns0:isKuzovMissing>
      </ns0:kuzovNumber>
      
      
      <ns0:mileage>1</ns0:mileage>
      
      
      <ns0:numberOfOwners>1</ns0:numberOfOwners>
      
      
      <ns0:registrationNumber>
        <ns0:isNoRegistrationNumber>false</ns0:isNoRegistrationNumber>
        <ns0:isRegNumNonStandart>false</ns0:isRegNumNonStandart>
        <ns0:registrationNumber>{$vehicle->reg_number}</ns0:registrationNumber>
      </ns0:registrationNumber>
      
      
      
      <ns0:sourceAcquisition>PURCHASED_IN_SALON</ns0:sourceAcquisition>
      <ns0:vehicleCost>1000000</ns0:vehicleCost>
      <ns0:vehicleUsage>personal</ns0:vehicleUsage>
      <ns0:vehicleUseRegion>77</ns0:vehicleUseRegion>
      
      <ns0:VIN>
        <ns0:isVINMissing>false</ns0:isVINMissing>
        <ns0:isIrregularVIN>false</ns0:isIrregularVIN>
        <ns0:VIN>{$vehicle->vin}</ns0:VIN>
      </ns0:VIN>
      
      
      <ns0:year>{$vehicle->car_year}</ns0:year>
      
      {$DiagnosticCard}
      
      <ns0:isRightHandDrive>false</ns0:isRightHandDrive>
      
      
      <ns0:numberOfKeys>
        <ns0:IsUnknownNumberOfKeys>true</ns0:IsUnknownNumberOfKeys>
      </ns0:numberOfKeys>
      
      
      <ns0:accordingInfoClient/>
    </ns0:vehicleDetails>
  </ns0:vehicleInfo>
       
    
XML;

        return Car::response($xml);



    }

    public static function getCarKasko(ContractsCalculation $calc)
    {
        $contract = $calc->contract;
        $vehicle = $contract->object_insurer_auto;





        $DocType = ($vehicle->doc_kind_pts == 0)?"PTS":"E_PTS";
        $DocDate = getDateFormatEn($vehicle->docdate, 1);
        $DocSeries = $vehicle->docserie;
        $DocNumber = $vehicle->docnumber;


        if(!$UseForType = $calc->sk_purpose) return Car::response('Не синхронизирован справочник: Цель использования', false);
        if(!$TypeAvto = $calc->sk_category) return Car::response('Не синхронизирован справочник: Категория', false);
        if(!$BrandIsn = $calc->sk_mark) return Car::response('Не синхронизирован справочник: Марка', false);
        if(!$ModelIsn = $calc->sk_model) return Car::response('Не синхронизирован справочник: Модель', false);


        $Powerhp = (int)$vehicle->power;
        $kppTypeID = ($vehicle->type_kpp == 0)?7:6;

        $isCredit = 'false';
        $bankInfo = '';

        if($contract->bank){
            return Car::response('Не синхронизирован справочник: Банк', false);
        }

        //Сделать справочник
        $caseTypeID = $vehicle->classification_id;

        $mileage = (int)$vehicle->mileage;

        $reg_number = $vehicle->reg_number;
        if(strlen($reg_number) < 3){
            //$reg_number = 'отсутствует';
        }


        $xml = <<<XML
        <ns0:vehicleInfo>
    <ns0:vehicleDetails>
      <ns0:vehicleReferenceInfo>
        <ns0:vehicleReferenceDetails>
        
        
          <ns0:modelID>{$ModelIsn->vehicle_models_sk_id}</ns0:modelID>
          <ns0:engPwrHP>{$Powerhp}</ns0:engPwrHP>
          <ns0:caseTypeID>{$caseTypeID}</ns0:caseTypeID>
          <ns0:kppTypeID>{$kppTypeID}</ns0:kppTypeID>
          
          
        </ns0:vehicleReferenceDetails>
      </ns0:vehicleReferenceInfo>
      <ns0:isChangeNumAgg>false</ns0:isChangeNumAgg>
      <ns0:countryOfRegistration>
        <ns0:isNoCountryOfRegistration>false</ns0:isNoCountryOfRegistration>
        <ns0:countryOfRegistration>RU</ns0:countryOfRegistration>
      </ns0:countryOfRegistration>
      <ns0:vehicleDocument>
        <ns0:documentType>{$DocType}</ns0:documentType>
        <ns0:documentSeries>{$DocSeries}</ns0:documentSeries>
        <ns0:documentNumber>{$DocNumber}</ns0:documentNumber>
        <ns0:documentIssued>{$DocDate}</ns0:documentIssued>
      </ns0:vehicleDocument>
      <ns0:chassis>
        <ns0:isChassisMissing>false</ns0:isChassisMissing>
      </ns0:chassis>
      
      <ns0:credit>
        <ns0:isCredit>{$isCredit}</ns0:isCredit>
        {$bankInfo}
      </ns0:credit>
      <ns0:isKeyless>false</ns0:isKeyless>
      <ns0:isUsedWithTrailer>false</ns0:isUsedWithTrailer>
      <ns0:kuzovNumber>
        <ns0:isKuzovMissing>false</ns0:isKuzovMissing>
      </ns0:kuzovNumber>
      <ns0:mileage>{$mileage}</ns0:mileage>
      
      
      <ns0:numberOfOwners>1</ns0:numberOfOwners>
      
      <ns0:registrationNumber>
        <ns0:isNoRegistrationNumber>false</ns0:isNoRegistrationNumber>
        <ns0:isRegNumNonStandart>false</ns0:isRegNumNonStandart>
        <ns0:registrationNumber>{$reg_number}</ns0:registrationNumber>
      </ns0:registrationNumber>
      
      <ns0:sourceAcquisition>PURCHASED_IN_SALON</ns0:sourceAcquisition>
      <ns0:vehicleCost>1000000</ns0:vehicleCost>
      <ns0:vehicleUsage>personal</ns0:vehicleUsage>
      <ns0:vehicleUseRegion>77</ns0:vehicleUseRegion>
      
      
      <ns0:VIN>
        <ns0:isVINMissing>false</ns0:isVINMissing>
        <ns0:isIrregularVIN>false</ns0:isIrregularVIN>
        <ns0:VIN>{$vehicle->vin}</ns0:VIN>
      </ns0:VIN>
      <ns0:year>{$vehicle->car_year}</ns0:year>
      <ns0:isRightHandDrive>false</ns0:isRightHandDrive>
      <ns0:numberOfKeys>
        <ns0:IsUnknownNumberOfKeys>false</ns0:IsUnknownNumberOfKeys>
        <ns0:numberOfKeys>{$vehicle->keys}</ns0:numberOfKeys>
      </ns0:numberOfKeys>
      <ns0:accordingInfoClient/>
    </ns0:vehicleDetails>
  </ns0:vehicleInfo>
       
    
XML;

        return Car::response($xml);



    }


        $from = $vehicle->dk_date_from;
        $exp = $vehicle->dk_date;

        $xml = <<<XML
    <ns0:techInspectionInfo>
        <ns0:sourceInfo>CUSTOMER</ns0:sourceInfo>
        <ns0:techDocumentType>DIAGNOSTIC_CARD</ns0:techDocumentType>
        <ns0:techInspExpirationDate>{$exp}</ns0:techInspExpirationDate>
        <ns0:techInspIssuedDate>{$from}</ns0:techInspIssuedDate>
        <ns0:techInspNumber>{$number}</ns0:techInspNumber>
     </ns0:techInspectionInfo>
XML;

        return $xml;
    }

    public static function response($result = '', $state = true)
    {
        return (object)['state' => $state, 'result' => $result];
    }

}