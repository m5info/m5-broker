<?php

namespace App\Services\SK\Tinkoff\Services\Kasko;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;

class KaskoService implements ProductServiceInterface{


    public $api;

    public function __construct(KaskoApi $api){
        $this->api = $api;
    }

    public function temp_calc(ContractsCalculation $calc)
    {


        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';


        $response = $this->api->temp_calc($calc);



        return $response;

    }

    public function calc(ContractsCalculation $calc)
    {

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';


        $response = $this->api->calculate($calc);


        return $response;

    }


    public function release(ContractsCalculation $calc){

    }


    public function check_status(ContractsCalculation $calc){

    }


    public function get_files(ContractsCalculation $calc){

    }

}