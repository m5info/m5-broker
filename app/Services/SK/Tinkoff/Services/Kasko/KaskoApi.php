<?php

namespace App\Services\SK\Tinkoff\Services\Kasko;


use App\Models\Contracts\ContractsCalculation;
use App\Services\SK\Tinkoff\Services\Auxiliary\Car;
use App\Services\SK\Tinkoff\Services\Auxiliary\Subject;
use App\Services\SK\Tinkoff\Services\Send\SendWSDL;


class KaskoApi{


    public $url;
    public $login;
    public $password;
    public $errors = [];


    public $Send = null;

    public function __construct($url, $login, $password){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->Send = new SendWSDL($url, $login, $password);
    }




    public function temp_calc(ContractsCalculation $calc)
    {

        $param = $this->get_calc_temp($calc);

        $client = $this->Send->getClient();
        $typedVar = new \SoapVar($param, XSD_ANYXML);

        try {


            $res = $client->calcPartnerQQuote($typedVar);

            dump($client->__getLastRequest());

            dd($res);

        } catch (\Exception $e) {
            return $this->responseError($e->getMessage());
        }

    }

    public function calculate(ContractsCalculation $calc)
    {
        $result = new \stdClass();
        $result->state = false;
        $result->error = '';
        $result->sk_key_id = '';
        $result->payment_total = '';
        $result->msg = '';
        $result->statys_id = 0;

        $param = $this->get_calc_params($calc);
        if(isset($param->state) && $param->state == false){
            return $this->responseError($param->error);
        }

        $client = $this->Send->getClient();
        $typedVar = new \SoapVar($param, XSD_ANYXML);

        try {


            $res = $client->calcPartnerFQuote($typedVar);

            if($res){
                if(isset($res->setNumber)){
                    $result->sk_key_id = (string)$res->setNumber;
                }

                if(isset($res->KASKOFQ) && isset($res->KASKOFQ->totalPremium) && getFloatFormat($res->KASKOFQ->totalPremium) > 0){
                    $result->payment_total = getFloatFormat($res->KASKOFQ->totalPremium);
                    $result->statys_id = 1;
                    $result->state = true;
                }

                if(isset($res->Header) && isset($res->Header->resultInfo) && isset($res->Header->resultInfo->status) && (string)$res->Header->resultInfo->status == 'ERROR'){
                    $result->error .= (string)$res->Header->resultInfo->errorInfo->descr.'; ';
                    $result->state = false;
                }

                if(isset($res->validInfo) && isset($res->validInfo->status) && (string)$res->validInfo->status == 'Error'){
                    $result->error .= (string)$res->validInfo->description.'; ';
                    $result->state = false;
                }




                return $result;
            }


            return $this->responseError("Ошибка сервиса!");



        } catch (\Exception $e) {


            return $this->responseError($e->getMessage());

        }

    }


    public function get_calc_params(ContractsCalculation $calc){



        $contract = $calc->contract;
        $producerCode = $calc->bso_supplier->agent->сhannel_sale_id;

        $begin_date = date("Y-m-d\TH:i", strtotime($contract->begin_date));
        $end_date = date("Y-m-d", strtotime($contract->end_date));

        $subjectNum = 1;

        $insurantNumber = 1;
        $ownerNumber = 1;

        $subjectInfo = Subject::getSubject($contract->insurer, $calc->insurance_companies_id, $subjectNum);

        if($contract->insurer->id != $contract->owner->id){
            $subjectNum++;
            $subjectInfo .= Subject::getSubject($contract->owner, $calc->insurance_companies_id, $subjectNum);
        }

        $car = Car::getCarKasko($calc);
        if($car->state == false){
            return $this->responseError($car->result);
        }


        return <<<XML
<ns0:calcPartnerFQuoteRequest xmlns:ns0="http://toi.ru/partner/model/quote/auto">
  <ns0:Header>
    <user>{$this->login}</user>
    <password>{$this->password}</password>
  </ns0:Header>
  
  {$subjectInfo}
  
  {$car->result}
  
  <ns0:KASKOFQ>
    <ns0:producerCode>{$producerCode}</ns0:producerCode>
    <ns0:termType>Annual</ns0:termType>
    <ns0:offeringType>{$calc->program->api_name}</ns0:offeringType>
    <ns0:effectiveDate>{$begin_date}:00+03:00</ns0:effectiveDate>
    <ns0:expirationDate>{$end_date}T23:59:59.999999+03:00</ns0:expirationDate>
    <ns0:insurant>
      <ns0:subjectNumber>{$insurantNumber}</ns0:subjectNumber>
    </ns0:insurant>
    <ns0:carOwner>
      <ns0:subjectNumber>{$ownerNumber}</ns0:subjectNumber>
    </ns0:carOwner>
    <ns0:driversList>
      <ns0:multidrive>
        <ns0:minAge>18</ns0:minAge>
        <ns0:minExperience>0</ns0:minExperience>
      </ns0:multidrive>
    </ns0:driversList>
    <ns0:coverages>
      <ns0:insuranceTerritory>Russia</ns0:insuranceTerritory>
    </ns0:coverages>
    <ns0:isRenewal>false</ns0:isRenewal>
  </ns0:KASKOFQ>
</ns0:calcPartnerFQuoteRequest>
XML;

    }

    public function get_calc_temp(ContractsCalculation $calc){

        $contract = $calc->contract;
        $producerCode = $calc->bso_supplier->agent->сhannel_sale_id;

        $begin_date = date("Y-m-d\TH:i", strtotime($contract->begin_date));
        $end_date = date("Y-m-d", strtotime($contract->end_date));



        //dd($car->result);

        return <<<XML
        
<ns0:calcPartnerQQuoteRequest xmlns:ns0="http://toi.ru/partner/model/quote/auto">
  <ns0:Header>
    <user>{$this->login}</user>
    <password>{$this->password}</password>
  </ns0:Header>
  <ns0:vehicleInfo>
    <ns0:vehicleUseRegion>77</ns0:vehicleUseRegion>
    <ns0:city>Москва</ns0:city>
    <ns0:year>2018</ns0:year>
    <ns0:modelID>415</ns0:modelID>
    <ns0:engPwrHP>204</ns0:engPwrHP>
    <ns0:caseTypeID>23</ns0:caseTypeID>
    <ns0:kppTypeID>7</ns0:kppTypeID>
    <ns0:vehicleCost>2000000</ns0:vehicleCost>
    <ns0:accordingInfoClient/>
  </ns0:vehicleInfo>
  <ns0:KASKOQQ1>
    <ns0:producerCode>{$producerCode}</ns0:producerCode>
    <ns0:termType>Annual</ns0:termType>
    <ns0:driversList>
      <ns0:unnamedDriverList>
        <ns0:unnamedDriver>
          <ns0:age>35</ns0:age>
          <ns0:experience>10</ns0:experience>
        </ns0:unnamedDriver>
      </ns0:unnamedDriverList>
    </ns0:driversList>
    <ns0:coverages>
      <ns0:insuranceTerritory>Russia</ns0:insuranceTerritory>
      <ns0:damage>
        <ns0:totalDamage>false</ns0:totalDamage>
        <ns0:fullDamage>true</ns0:fullDamage>
        <ns0:repairOptions>dealer</ns0:repairOptions>
        <ns0:deductible>
          <ns0:wholeVehicle>
            <ns0:amount>10000</ns0:amount>
            <ns0:fromSecondInsuredEvent>false</ns0:fromSecondInsuredEvent>
          </ns0:wholeVehicle>
        </ns0:deductible>
      </ns0:damage>
      <ns0:theft>
        <ns0:theft>true</ns0:theft>
      </ns0:theft>
      <ns0:GAP>
        <ns0:GAP>false</ns0:GAP>
      </ns0:GAP>
      <ns0:technicalAssistance>
        <ns0:technicalAssistance>false</ns0:technicalAssistance>
      </ns0:technicalAssistance>
      <ns0:courtesyCar>
        <ns0:courtesyCar>false</ns0:courtesyCar>
      </ns0:courtesyCar>
      <ns0:taxi>
        <ns0:taxi>false</ns0:taxi>
      </ns0:taxi>
      <ns0:substituteDriver>
        <ns0:substituteDriver>false</ns0:substituteDriver>
      </ns0:substituteDriver>
    </ns0:coverages>
    <ns0:additionalInfo/>
    <ns0:isRenewal>false</ns0:isRenewal>
  </ns0:KASKOQQ1>
</ns0:calcPartnerQQuoteRequest>
XML;

    }


    private function responseError($error)
    {
        $response = new \stdClass();
        $response->state = false;
        $response->error = $error;
        return $response;
    }

}