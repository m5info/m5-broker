<?php

namespace App\Services\SK\Tinkoff\Services\Dictionary;


use App\Services\SK\Tinkoff\Services\Send\SendJSON;
use SoapClient;
use SoapHeader;
use SoapVar;

class DictionaryApi{


    public $url;
    public $login;
    public $password;

    public $method = '/TOIESB_MarkModelProxy/CarReferenceServiceJSON';
    public $integrID = 'Eurogarant0002'; // боевые доступы

    public $Send = null;

    public function __construct($url, $login, $password, $api_setting){

        $agent_settings = $api_setting->bso_supplier->agent;

        $this->url = $url;
        $this->сhannel_sale_id = $agent_settings->сhannel_sale_id;
        $this->login = $login;
        $this->password = $password;
        $this->Send = new SendJSON($url, $login, $password, $api_setting);
    }


    public function get_purpose(){


        $purposes = [
            ['id'=>'640254020', 'title'=>'Личная'],
            ['id'=>'640254030', 'title'=>'Учебная езда'],
            ['id'=>'640254060', 'title'=>'Такси'],
            ['id'=>'640254070', 'title'=>'Дорожные и специальные ТС'],
            ['id'=>'10115', 'title'=>'Экстренные и коммунальные службы'],
            ['id'=>'10116', 'title'=>'Перевозка опасных и легко воспламеняющихся грузов'],
            ['id'=>'10117', 'title'=>'Регулярные пассажирские перевозки/перевозки пассажиров по заказам']
        ];


        return $purposes;

    }

    public function get_categories(){
        $request = [
            'listTypesRequest' => [
                'Header' => [
                    'integrID' => $this->integrID,
                ],
            ]

        ];


        $response = $this->Send->send($this->method, $request);
        $categories = [];

        foreach ($response->listTypesResponse->listTypes as $cat){
            $categories[] = ['id'=>$cat->id, 'title'=>$cat->name];
        }


        return $categories;

    }

    public function get_marks($category_id){


        $request = [
            'listMakersRequest' => [
                'Header' => [
                    'integrID' => $this->integrID,
                ],
                "vehicleType"=>"$category_id",
                "year"=>"2020"
            ]

        ];


        return $this->Send->send($this->method, $request);
    }


    public function get_models($mark_id, $category_id){

        $request = [
            'listModelsRequest' => [
                'Header' => [
                    'integrID' => $this->integrID,
                ],
                "vehicleType"=>"$category_id",
                "year"=>"2021",
                "maker"=>"$mark_id",
            ]

        ];


        return $this->Send->send($this->method, $request);

    }



}