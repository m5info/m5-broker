<?php

namespace App\Services\SK\Tinkoff\Services\Dictionary;

use App\Interfaces\Services\SK\DictionaryServiceInterface;

class DictionaryService implements DictionaryServiceInterface {

    public $cache_json = __DIR__."/dict_auto.json";

    public $api;

    public function __construct(DictionaryApi $api){
        $this->api = $api;
    }

    public function get_api_purpose(){
        return $this->api->get_purpose();
    }

    public function get_api_categories(){
        return $this->api->get_categories();
    }

    public function get_marks_models(){


        $categories = [];
        $marks = [];
        $models = [];

        if(!is_file($this->cache_json)) {
            //тянем категории
            if ($_categories = $this->api->get_categories()) {
                foreach ($_categories as $sk_category) {
                    $categories[] = [
                        'id' => $sk_category['id'],
                        'title' => $sk_category['title'],
                    ];

                    //тянем марки
                    if ((int)$sk_category['id'] > 0 && $_marks = $this->api->get_marks($sk_category['id'])) {
                        if ($_marks->listMakersResponse->Header->status == 'OK') {//ERROR

                            foreach ($_marks->listMakersResponse->listMakers as $sk_mark) {
                                $marks[] = [
                                    'id' => (string)$sk_mark->id,
                                    'title' => (string)$sk_mark->name,
                                    'vehicle_categorie_sk_id' => (string)$sk_category['id'],
                                ];

                                //тянем модели
                                if ($_models = $this->api->get_models($sk_mark->id, $sk_category['id'])) {

                                    foreach ($_models->listModelsResponse->listModels as $sk_model) {
                                        $models[] = [
                                            'id' => (string)$sk_model->id,
                                            'title' => (string)$sk_model->name,
                                            'vehicle_mark_sk_id' => (string)$sk_mark->id,
                                            'vehicle_categorie_sk_id' => $sk_category['id']
                                        ];
                                    }
                                }


                            }
                        }

                    }
                }
            }

            $result = [
                'categories' => $categories,
                'marks' => $marks,
                'models' => $models,
            ];

            file_put_contents($this->cache_json, \GuzzleHttp\json_encode($result));

            return $result;
        }else{
            return \GuzzleHttp\json_decode(file_get_contents($this->cache_json),true);
        }
    }
}