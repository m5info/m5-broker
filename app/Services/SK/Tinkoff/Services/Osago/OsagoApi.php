<?php

namespace App\Services\SK\Tinkoff\Services\Osago;

use App\Models\Api\SK\ApiInsuranceAuxiliaryParameters;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\File;
use App\Services\SK\Tinkoff\Services\Auxiliary\Car;
use App\Services\SK\Tinkoff\Services\Auxiliary\Subject;
use App\Services\SK\Tinkoff\Services\Send\SendWSDL;
use stdClass;

class OsagoApi{

    public $url;
    public $login;
    public $password;
    public $errors = [];
    public $work_type;

    public $Send = null;

    public function __construct($url, $login, $password, $settings){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->work_type = $settings->work_type;
        $this->Send = new SendWSDL($url, $login, $password);
    }


    /**
     * @param ContractsCalculation $calc
     * @return bool|stdClass
     * перевод в проект полиса
     */
    public function issueQuoteSetPartner(ContractsCalculation $calc)
    {
        $calc_json = \GuzzleHttp\json_decode($calc->json);

        if($calc_json && isset($calc_json->Header->integrationID)){

            $param = <<<XML
<ns1:issueQuoteSetPartnerRequest xmlns:ns1="http://toi.ru/partner/model/quote/set">
  <ns1:Header>
    <integrationID>{$calc_json->Header->integrationID}</integrationID>
    <user>$this->login</user>
    <password>$this->password</password>
  </ns1:Header>
  <ns1:setNumber>{$calc_json->setNumber}</ns1:setNumber>
  <ns1:issuerDetails>
    <ns1:userLastName>Петров</ns1:userLastName>
    <ns1:userFirstName>Иван</ns1:userFirstName>
    <ns1:userLogin>petrov@primer.ru</ns1:userLogin>
  </ns1:issuerDetails>
</ns1:issueQuoteSetPartnerRequest>
XML;

            $client = $this->Send->getClient();

            $typedVar = new \SoapVar($param, XSD_ANYXML);

            try{
                $client->issueQuoteSetPartner($typedVar);

            }catch(\SoapFault $e){
                return $this->responseError($e->getMessage());
            }

        } else{
            return false;
        }

        return true;
    }


    /**
     * @param ContractsCalculation $calc
     * @return stdClass
     * калькуляция
     */
    public function calc_osago(ContractsCalculation $calc){

        ini_set('max_execution_time', 600);
        ini_set('default_socket_timeout', 600);

        $result = new \stdClass();
        $result->state = false;
        $result->error = '';
        $result->sk_key_id = '';
        $result->payment_total = '';
        $result->msg = '';
        $result->statys_id = 2;


        $param = $this->get_calc_params($calc);
/*dd($param);*/
        if(isset($param->state) && $param->state == false){
            return $this->responseError($param->error);
        }

        $client = $this->Send->getClient();
        $typedVar = new \SoapVar($param, XSD_ANYXML);

        try {

            $res = $client->calcPartnerFQuote($typedVar);
/*dd($res);*/
            if(isset($res->OSAGOFQ->isTerminalG) && $res->OSAGOFQ->isTerminalG){ // заглушка от них
                return $this->responseError('Невозможно произвести расчет!');
            }

            $calc->json = \GuzzleHttp\json_encode($res);
            $calc->save();

            $result->state = true;
            $result->sk_key_id = isset($res->setNumber) ? $res->setNumber : '';

            if($res->Header->resultInfo->status == 'ERROR'){
                if(isset($res->validInfo)){
                    if(is_array($res->validInfo)){
                        foreach($res->validInfo as $err){
                            $result->error .= $err->description;
                        }
                    }else{
                        $result->error .= $res->validInfo->description;
                    }
                }else{
                    $result->error .= $res->validInfo->description ?? $res->Header->resultInfo->errorInfo->descr;
                }

                return $this->responseError($result->error);
            }

            if(isset($res->OSAGOFQ) && isset($res->OSAGOFQ->totalPremium)){
                $result->payment_total = getFloatFormat($res->OSAGOFQ->totalPremium);
                $coeffs = $res->OSAGOFQ->OSAGORatingFactors;

                $TB = $coeffs->TB ?? 'Нет данных';
                $KBM = $coeffs->KBM ?? 'Нет данных';
                $KM = $coeffs->KM ?? 'Нет данных';
                $KN = $coeffs->KN ?? 'Нет данных';
                $KO = $coeffs->KO ?? 'Нет данных';
                $KS = $coeffs->KS ?? 'Нет данных';
                $KT = $coeffs->KT ?? 'Нет данных';
                $KVS = $coeffs->KVS ?? 'Нет данных';

                $result->msg = "
                ТБ: {$TB}; 
                КБМ: {$KBM}; 
                КМ: {$KM}; 
                КН: {$KN};
                КО: {$KO}; 
                КС: {$KS}; 
                КТ: {$KT}; 
                КВС: {$KVS};";
            }

            Subject::updateSubjects($calc->contract, $calc->insurance_companies_id, $res);
        } catch (\SoapFault $e) {

            return $this->responseError($e->getMessage());
        }

        return $result;
    }

    public function cancel_osago(ContractsCalculation $calc)
    {
        $calc_json = \GuzzleHttp\json_decode($calc->json);

        if($calc_json && isset($calc_json->Header->integrationID)){

            $param = <<<XML
<ns1:cancelPolicyPartnerRequest>
  <ns1:Header>
    <integrationID>{$calc_json->Header->integrationID}</integrationID>
    <user>$this->login</user>
    <password>$this->password</password>
  </ns1:Header>
  <ns1:policyNumber>{$calc_json->OSAGOFQ->quoteNumber}</ns1:policyNumber>
</ns1:cancelPolicyPartnerRequest>
XML;

            $client = $this->Send->getClient();

            $typedVar = new \SoapVar($param, XSD_ANYXML);

            try{
                $res = $client->CancelPolicy($typedVar);

                                dump($client->__getLastRequest());
                                dump($client->__getLastResponse());
dd($res);
            }catch(\SoapFault $e){
                dump($client->__getLastRequest());

                dd($e);
                return $this->responseError($e->getMessage());
            }

        } else{
            return false;
        }

        return true;
    }

    /**
     * @param ContractsCalculation $calc
     * @return stdClass
     * получение печатных форм
     */
    public function getPrintedDocumentPartner(ContractsCalculation $calc)
    {
        $result = new \stdClass();
        $result->state = false;
        $result->error = '';

        $contract = $calc->contract;
        $jsonData = \GuzzleHttp\json_decode($calc->json);

        $param = <<<XML
<NS1:getPrintedDocumentPartnerRequest xmlns:NS1="http://toi.ru/partner/model/prntdoc">
  <NS1:Header>
    <integrationID>{$jsonData->Header->integrationID}</integrationID>
    <user>{$this->login}</user>
    <password>{$this->password}</password>
  </NS1:Header>
  <NS1:policyNumber>{$jsonData->OSAGOFQ->quoteNumber}</NS1:policyNumber>
  <NS1:docType>eosago_policy</NS1:docType>
</NS1:getPrintedDocumentPartnerRequest>
XML;

        $client = $this->Send->getClient();
        $typedVar = new \SoapVar($param, XSD_ANYXML);

        try {
            $res = $client->getPrintedDocumentPartner($typedVar);

            if($res->Header->resultInfo->status == 'OK'){

                $contract->masks()->detach();

                $document = $res->docInfo;
                $File_n = $document->dataExpression;
                $file_contents = ($document->dataDoc);
                $path = storage_path('app/' . Contracts::FILES_DOC . "/{$contract->id}/");

                if(!is_dir(($path))){
                    mkdir(($path), 0777, true);
                }

                $file_name = uniqid();

                file_put_contents($path . $file_name . '.' . $File_n, $file_contents);

                $file = File::create([
                    'original_name' => 'Е-ПОЛИС',
                    'ext' => $File_n,
                    'folder' => Contracts::FILES_DOC . "/{$contract->id}/",
                    'name' => $file_name,
                    'user_id' => auth()->id()
                ]);

                $contract->masks()->save($file);

                $bso_number = $this->parseBsoNumberFromPdf($path . $file_name . '.' . $File_n);

                $result->state = $this->setBsoContract($calc, $bso_number);

                return $result;
            }

        } catch (\Exception $e) {

            return $this->responseError($e->getMessage());
        }

        return $res;
    }

    public function setBsoContract(ContractsCalculation $calc, $bso_number)
    {
        $contract = $calc->contract;

        $bso = $contract->getElectronBSO((string)'ХХХ', (string)$bso_number);
        $bso->setBsoLog(0);
        $bso->setBsoLog(1);
        $bso->update([
            'state_id' => 2,
            'location_id' => 1,
        ]);

        $calc->update(['statys_id' => 4]);

        $contract->update([
            'statys_id' => 4,
            'bso_id' => $bso->id,
            'bso_title' => $bso->bso_title,
        ]);

        $contract->all_payments->first()->update([
            "bso_id" => $bso->id,
            'org_id' => $bso->org_id,
        ]);

        return true;
    }


    public function parseBsoNumberFromPdf($file)
    {
        $pdf = new \TonchikTm\PdfToHtml\Pdf($file, [
            'pdftohtml_path' => '/usr/bin/pdftohtml',
            'pdfinfo_path' => '/usr/bin/pdfinfo',
            'outputDir' => __DIR__ // output dir
        ]);

        $html = $pdf->getHtml()->getPage(1);

        $regex = '/ХХХ № (.+?)<\/p>/';

        preg_match($regex, $html, $res);

        return $res[1];
    }

    /**
     * @param ContractsCalculation $calc
     * @return stdClass
     * получение ссылки на опалату
     */
    public function getPaymentReferencePartner(ContractsCalculation $calc)
    {
        $jsonData = \GuzzleHttp\json_decode($calc->json);

        $param = <<<XML
<NS1:getPaymentReferencePartnerRequest xmlns:NS1="http://toi.ru/partner/model/payment">
<NS1:Header>
    <integrationID>{$jsonData->Header->integrationID}</integrationID>
    <user>{$this->login}</user>
    <password>{$this->password}</password>
</NS1:Header>
  <NS1:setNumber>{$jsonData->setNumber}</NS1:setNumber>
</NS1:getPaymentReferencePartnerRequest>
XML;

        $client = $this->Send->getClient();
        $typedVar = new \SoapVar($param, XSD_ANYXML);

        try {
            $res = $client->getPaymentReferencePartner($typedVar);

            if(isset($res->Header->resultInfo) && $res->Header->resultInfo->status == 'ERROR'){
                if(isset($res->Header->resultInfo->errorInfo->code) && $res->Header->resultInfo->errorInfo->code == 'ESB-002'){
                    // оплата уже была произведена
//                    $q = $this->sendPaymentNotificationPartner($calc);
                }else{
                    return $this->responseError($res->Header->resultInfo->errorInfo->descr);
                }
            }

            $calc->update(['statys_id' => 3]);

            $response = new stdClass();
            $response->state = true;
            $response->bso_id = 0;
            $response->statys_id = 3;
            $response->payment_link = isset($res->paymentURL) ? $res->paymentURL : '';
            $response->msg = isset($res->paymentURL) ? 'На почту отправлена ссылка на платежную страницу.' : 'Получение печатных форм.';

        } catch (\Exception $e) {
            return $this->responseError($e->getMessage());
        }

        return $response;
    }

    public function check_payment_status(ContractsCalculation $calc)
    {
        $jsonData = \GuzzleHttp\json_decode($calc->json);

        $param = <<<XML
<NS1:getPaymentReferencePartnerRequest xmlns:NS1="http://toi.ru/partner/model/payment">
<NS1:Header>
    <integrationID>{$jsonData->Header->integrationID}</integrationID>
    <user>{$this->login}</user>
    <password>{$this->password}</password>
</NS1:Header>
  <NS1:setNumber>{$jsonData->setNumber}</NS1:setNumber>
</NS1:getPaymentReferencePartnerRequest>
XML;

        $client = $this->Send->getClient();
        $typedVar = new \SoapVar($param, XSD_ANYXML);

        try {
            $res = $client->getPaymentReferencePartner($typedVar);

            if(isset($res->Header->resultInfo) && $res->Header->resultInfo->status == 'ERROR'){
                if($res->Header->resultInfo->errorInfo->code == 'ESB-002'){ // оплата уже была произведена
                    return true;
                }else{
                    return false;
                }
            }

        } catch (\Exception $e) {
            return false;
        }

        return false;
    }

    public function sendPaymentNotificationPartner(ContractsCalculation $calc)
    {
        $jsonData = \GuzzleHttp\json_decode($calc->json);

        $param = <<<XML
<NS1:sendPaymentNotificationPartnerRequest xmlns:NS1="http://toi.ru/partner/model/payment">
  <NS1:Header>
        <integrationID>{$jsonData->Header->integrationID}</integrationID>
  </NS1:Header>
  <NS1:setNumber>{$jsonData->setNumber}</NS1:setNumber>
  <NS1:policyNumber>{$jsonData->OSAGOFQ->quoteNumber}</NS1:policyNumber>
  <NS1:policyTerm>1</NS1:policyTerm>
  <NS1:productCode>OSAGO</NS1:productCode>
  <NS1:TermType>ANNUAL</NS1:TermType>
  <NS1:paymentStatus>CONFIRM</NS1:paymentStatus>
</NS1:sendPaymentNotificationPartnerRequest>
XML;

        $client = $this->Send->getClient();
        dd($client->__getFunctions());
        $typedVar = new \SoapVar($param, XSD_ANYXML);

        try {
            $res = $client->sendPaymentNotificationPartner($typedVar);
            dd($res);

        } catch (\Exception $e) {
            //dump($client->__getLastRequestHeaders());
            dump ($client->__getLastRequest());
            dump ($client->__getLastResponse());
            //            dump($res);

            dd($e);
            return $this->responseError($e->getMessage());
        }
        dd($res);
    }


    public function get_files_list(ContractsCalculation $calc)
    {
        $jsonData = \GuzzleHttp\json_decode($calc->json);
        $contract = $calc->contract;

        $param = <<<XML
      <ns1:getDocListByPolicyNumberPartnerRequest>
         <ns1:Header>
            <integrationID>{$jsonData->Header->integrationID}</integrationID>
            <user>{$this->login}</user>
            <password>{$this->password}</password>
         </ns1:Header>
         <ns1:policyNumber>{$jsonData->OSAGOFQ->quoteNumber}</ns1:policyNumber>
      </ns1:getDocListByPolicyNumberPartnerRequest>
XML;

        $client = $this->Send->getClient();
        $typedVar = new \SoapVar($param, XSD_ANYXML);

        try {
            $res = $client->getDocListByPolicyNumberPartner($typedVar);
dd($res);

        } catch (\Exception $e) {
            //dump($client->__getLastRequestHeaders());
            dump ($client->__getLastRequest());
            dump ($client->__getLastResponse());
//            dump($res);

            dd($e);
            return $this->responseError($e->getMessage());
        }
        dd($res);
    }




    public function get_calc_params(ContractsCalculation $calc){

        $contract = $calc->contract;

        $producerCode = $this->work_type ? $calc->bso_supplier->agent->сhannel_sale_id : "TestMotor-01";

        $begin_date = date("Y-m-d\TH:i", strtotime($contract->begin_date));
        $end_date = date("Y-m-d", strtotime($contract->end_date));

        $subjectNum = 1;

        $insurantNumber = 1;
        $ownerNumber = 1;

        $subjectInfo = '';
        $subjectInfo = Subject::getSubject($contract->insurer, $calc->insurance_companies_id, $subjectNum);

        $drivers = '';

        $insurer = $contract->insurer->get_info();
        if($insurer->driver){
            $insurerDocDateLicense = date("Y-m-d", strtotime($insurer->driver->doc_date));
            $isCalcKBMByPreviousDoc = $insurer->driver->old_license ? "true" : "false";
            $drivers = "
                <ns0:driver>
                  <ns0:subjectNumber>{$subjectNum}</ns0:subjectNumber>
                  <ns0:drivingLicenseIssueDateOriginal>{$insurerDocDateLicense}</ns0:drivingLicenseIssueDateOriginal>
                  <ns0:isCalcKBMByPreviousDoc>{$isCalcKBMByPreviousDoc}</ns0:isCalcKBMByPreviousDoc>
                </ns0:driver>
            ";
        }

        if($contract->insurer->id != $contract->owner->id){
            $subjectNum++;
            $subjectInfo .= Subject::getSubject($contract->owner, $calc->insurance_companies_id, $subjectNum);
            $ownerNumber = $subjectNum;
            $owner = $contract->owner->get_info();
            if($owner->driver){
                $ownerDocDateLicense = date("Y-m-d", strtotime($owner->driver->doc_date));
                $isCalcKBMByPreviousDoc = $owner->driver->old_license ? "true" : "false";
                $drivers .= "
                <ns0:driver>
                  <ns0:subjectNumber>{$subjectNum}</ns0:subjectNumber>
                  <ns0:drivingLicenseIssueDateOriginal>{$ownerDocDateLicense}</ns0:drivingLicenseIssueDateOriginal>
                  <ns0:isCalcKBMByPreviousDoc>{$isCalcKBMByPreviousDoc}</ns0:isCalcKBMByPreviousDoc>
                </ns0:driver>
            ";
            }
        }


        if((int)$contract->drivers_type_id == 1){

            $drivers = "<ns0:isMultidrive>true</ns0:isMultidrive>";

        }else{

            if(($contract->drivers && sizeof($contract->drivers))){

                foreach ($contract->drivers as $driver){
                    if(!(int)$driver->same_as_insurer){ // если водила это не страхователь
                        $subjectNum++;
                        $subjectInfo .= Subject::getDriver($driver, $calc->insurance_companies_id, $subjectNum);
                        $driverDocDateLicense = date("Y-m-d", strtotime($driver->exp_date));
                        $isCalcKBMByPreviousDoc = $driver->old_license ? "true" : "false";
                        $drivers .= "
                        <ns0:driver>
                          <ns0:subjectNumber>{$subjectNum}</ns0:subjectNumber>
                          <ns0:drivingLicenseIssueDateOriginal>{$driverDocDateLicense}</ns0:drivingLicenseIssueDateOriginal>
                          <ns0:isCalcKBMByPreviousDoc>{$isCalcKBMByPreviousDoc}</ns0:isCalcKBMByPreviousDoc>
                        </ns0:driver>
                    ";
                    }
                }

                $drivers = "<ns0:namedList>{$drivers}</ns0:namedList>";
            }

        }


        $car = Car::getCarOSAGO($calc);
        if($car->state == false){
            return $this->responseError($car->result);
        }

        $setNumberXML = '';
        $quoteNumberXML = '';

        if(strlen($calc->sk_key_id) > 3){
            $json = \GuzzleHttp\json_decode($calc->json);
            $setNumberXML = "<ns0:setNumber>{$calc->sk_key_id}</ns0:setNumber>";//закомментировать на 1 расчет при переходе с тестового на боевой сервер тинькофф и обратно в OsagoApi get_calc_params() и в Subjects getSubject() и getDriver()
            if(isset($json->setNumber) && isset($json->OSAGOFQ)){
                $subjectAuxiliary = ApiInsuranceAuxiliaryParameters::where('auxiliary_parameters', '=', $calc->sk_key_id)
                    ->whereRaw('LENGTH(sk_id) > 3')
                    ->count();

                if($subjectAuxiliary){ // тогда перерасчет
                    $setNumberXML = "<ns0:setNumber>{$json->setNumber}</ns0:setNumber>";
                    $quoteNumberXML = "<ns0:quoteNumber>{$json->OSAGOFQ->quoteNumber}</ns0:quoteNumber>";
                }
            }
        }

        //dd($car->result);

        return <<<XML
<ns0:calcPartnerFQuoteRequest xmlns:ns0="http://toi.ru/partner/model/quote/auto">
  <ns0:Header>
    <user>{$this->login}</user>
    <password>{$this->password}</password>
  </ns0:Header>
  {$setNumberXML}
  <ns0:producerCode>$producerCode</ns0:producerCode>
  {$subjectInfo}
  {$car->result}
  <ns0:OSAGOFQ>
    {$quoteNumberXML}
    <ns0:effectiveDate>{$begin_date}:00+03:00</ns0:effectiveDate>
    <ns0:expirationDate>{$end_date}T23:59:59.999999+03:00</ns0:expirationDate>
    <ns0:insurant>
      <ns0:subjectNumber>{$insurantNumber}</ns0:subjectNumber>
    </ns0:insurant>
    <ns0:carOwner>
      <ns0:subjectNumber>{$ownerNumber}</ns0:subjectNumber>
    </ns0:carOwner>
    <ns0:driversList>
        {$drivers}
    </ns0:driversList>
    <ns0:vehicleUsagePeriods/>
    <ns0:isEOSAGO>true</ns0:isEOSAGO>
  </ns0:OSAGOFQ>
</ns0:calcPartnerFQuoteRequest>
XML;

    }


    public function responseError($error)
    {
        $response = new \stdClass();
        $response->state = false;
        $response->error = $error;
        return $response;
    }


}