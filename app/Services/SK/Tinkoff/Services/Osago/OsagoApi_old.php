<?php

namespace App\Services\SK\Tinkoff\Services\Osago;

use App\Models\Contracts\ContractsCalculation;
use App\Services\Kbm\Alfa\Soap;
use App\Services\SK\Tinkoff\Clients\DigestSoapClient;
use App\Services\SK\Tinkoff\Services\Send\SendWSDL;

class OsagoApi{

    public $url;
    public $login;
    public $password;
    public $errors = [];



    public $Send = null;

    public function __construct($url, $login, $password){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

        $this->Send = new SendWSDL($url, $login, $password);
    }


    public function get_service_client($service_name){

        return new DigestSoapClient([
            'url' => $this->url,
            'service_name' => $service_name,
            'login' => $this->login,
            'password' => $this->password,
        ]);

    }


    // калькуляция
    public function calc_osago(ContractsCalculation $calc){



        /*


        $header = $this->Send->getHeader();

        $xml = <<<XML
<ns0:calcPartnerQQuoteRequest>
  {$header}
  <ns0:vehicleInfo>
    <ns0:vehicleUseRegion>50</ns0:vehicleUseRegion>
    <ns0:city>Московская область</ns0:city>
    <ns0:year>2017</ns0:year>
    <ns0:modelID>486</ns0:modelID>
    <ns0:engPwrHP>123</ns0:engPwrHP>
    <ns0:caseTypeID>21</ns0:caseTypeID>
    <ns0:kppTypeID>6</ns0:kppTypeID>
    <ns0:vehicleCost>697591</ns0:vehicleCost>
  </ns0:vehicleInfo>
  <ns0:KASKOQQ1>
    <ns0:producerCode>KaskoDriveOptimum</ns0:producerCode>
    <ns0:termType>Annual</ns0:termType>
    <ns0:offeringType>KaskoDriveLiabilityMax</ns0:offeringType>
    <ns0:driversList>
      <ns0:multidrive>
        <ns0:minAge>18</ns0:minAge>
        <ns0:minExperience>0</ns0:minExperience>
      </ns0:multidrive>
    </ns0:driversList>
    <ns0:coverages>
      <ns0:insuranceTerritory>Russia</ns0:insuranceTerritory>
    </ns0:coverages>
    <ns0:isRenewal>false</ns0:isRenewal>
  </ns0:KASKOQQ1>
</ns0:calcPartnerQQuoteRequest>
XML;




        $response = $this->Send->send('calcPartnerQQuote', $xml);

        dump ($response);

        dd("OK");
        */

        $client = new \SoapClient("https://tstrp.tinkoffinsurance.ru:23001/toi/partners/quickquote/v2.0?wsdl", array(
            'trace' => 1,
            'exception' => 1,
            "soap_version" => SOAP_1_1,
            'stream_context' => stream_context_create(
                array(
                    'ssl' => array(
                        'verify_peer'       => false,
                        'verify_peer_name'  => false,
                        'allow_self_signed' => true,
                    )
                )
            )));
        //$client->__setLocation('https://tstrp.tinkoffinsurance.ru:23001/toi/partners/quickquote/v2.0?WSDL');

        $param = $this->get_calc_params($calc);

        $typedVar = new \SoapVar($param, XSD_ANYXML);

        try {

            $res = $client->calcPartnerQQuote($typedVar);
            dump($client->__getLastRequest());
//           $service_client->__doRequest();
//            dd($service_client->__getLastRequest());
            $response = $client->__getLastResponse();
            dd($res);

        } catch (\Exception $e) {
            //
//            dd($res);
            dump($client->__getLastRequestHeaders());
            dump ($client->__getLastRequest());
            dd($e);
        }

        dd('123');


        dd("OK  ");


        $result = (object)['state' => false, 'answer' => [], 'error'=>''];
//        ini_set('default_socket_timeout', 10);
        $options = array(
            'cache_wsdl' => 0,
            'trace' => 1,
            "soap_version" => SOAP_1_2,
//            'location' => 'https://tstrp.tinkoffinsurance.ru:23001/TOIESB_MarkModelProxy/CarReferenceServiceJSON',
//            'stream_context' => stream_context_create(array(
//                'ssl' => array(
//                    'verify_peer' => false,
//                    'verify_peer_name' => true,
////                    'allow_self_signed' => true,
////                    'ciphers'=>"SHA1"
//                )
//            )),
            'authentication'    => SOAP_AUTHENTICATION_BASIC,
            'login' => $this->login,
            'password' => $this->password,
        );

        $url = 'https://tstrp.tinkoffinsurance.ru:23001/toi/partners/quickquote/v2.0?wsdl';
//
//        try {
//            $opts = array(
//                'http' => array(
//                    'user_agent' => 'PHPSoapClient'
//                )
//            );
//            $context = stream_context_create($opts);
//
//            $wsdlUrl = 'https://tstrp.tinkoffinsurance.ru:23001/toi/partners/quickquote/v2.0?wsdl';
//            $soapClientOptions = array(
//                'stream_context' => $context,
//                'cache_wsdl' => WSDL_CACHE_NONE
//            );
//
//            $client = new \SoapClient($wsdlUrl, $soapClientOptions);
//            dd($client);
//
//            $result = $client->checkVat($checkVatParameters);
//            print_r($result);
//        }
//        catch(Exception $e) {
//            echo $e->getMessage();
//        }

        $soapClient = new \SoapClient($url, $options);

        dd($soapClient);
        $service_client = $this->get_service_client('/toi/partners/quickquote/v2.0');

        dd($service_client);
        $auth_params = [
            'login' => $this->login,
            'password' => $this->password
        ];

        $auth = $this->auth('/TOIESB_MarkModelProxy/CarReferenceServiceJSON',$auth_params);
        dd($auth);
        // параметры из формы

        $result['error'] = $this->errors;

        dd($result);
    }


    public function get_calc_params(ContractsCalculation $calc){
        return <<<XML
<ns1:calcPartnerQQuoteRequest>
         <ns1:Header>
            <!--Optional:-->
            <integrationID>0</integrationID>
            <!--Optional:-->
            <corellationID>0</corellationID>
            <!--Optional:-->
            
            <!--Optional:-->
            <nativeID>0</nativeID>
            <!--Optional:-->
            <operationName>0</operationName>
            <!--Optional:-->
            <user>TestMotor</user>
            <!--Optional:-->
            
            <!--Optional:-->
            <sourceTS>2019-09-16T00:00:00.000+03:00</sourceTS>
            <!--Optional:-->
            <password>123qwe!E</password>
            <!--Optional:-->
            <resultInfo>
               <status>OK</status>
               <!--Optional:-->
               <errorInfo>
                  <!--Optional:-->
                  <code>0</code>
                  <!--Optional:-->
                  <descr>0</descr>
               </errorInfo>
               <!--Optional:-->
               <nativeErrorInfo>
                  <!--Optional:-->
                  <code>0</code>
                  <!--Optional:-->
                  <descr>0</descr>
               </nativeErrorInfo>
               <!--Optional:-->
               <businessError>
                  <!--Optional:-->
                  <code>0</code>
                  <!--Optional:-->
                  <descr>0</descr>
               </businessError>
            </resultInfo>
            <!--Zero or more repetitions:-->
            <targetSystem></targetSystem>
         </ns1:Header>
         <!--Optional:-->
         
         <ns1:vehicleInfo>
            <ns1:vehicleUseRegion>50</ns1:vehicleUseRegion>
            <!--Optional:-->
            <ns1:city>Московская область</ns1:city>
            <ns1:year>2017</ns1:year>
            <ns1:modelID>486</ns1:modelID>
            <ns1:engPwrHP>123</ns1:engPwrHP>
            <!--Optional:-->
            <ns1:caseTypeID>21</ns1:caseTypeID>
            <!--Optional:-->
            <ns1:kppTypeID>6</ns1:kppTypeID>
            <ns1:vehicleCost>418</ns1:vehicleCost>
            <!--Optional:-->
            <ns1:accordingInfoClient>
               <!--Optional:-->
               
               <!--Optional:-->
               <ns1:isCrashLastYear>false</ns1:isCrashLastYear>
               <!--Optional:-->
               
            </ns1:accordingInfoClient>
         </ns1:vehicleInfo>
         <!--Optional:-->
         <ns1:KASKOQQ1>
            <!--Optional:-->
            <ns1:producerCode>TestMotor-01</ns1:producerCode>
            <ns1:termType>Annual</ns1:termType>
            <!--Optional:-->
            <ns1:offeringType>KaskoDriveOptimum</ns1:offeringType>
            <ns1:driversList>
               <!--You have a CHOICE of the next 2 items at this level-->
               <ns1:unnamedDriverList>
                  <!--1 or more repetitions:-->
                  <ns1:unnamedDriver>
                     <ns1:age>30</ns1:age>
                     <ns1:experience>11</ns1:experience>
                  </ns1:unnamedDriver>
               </ns1:unnamedDriverList>
               
            </ns1:driversList>
            <ns1:coverages>
               <ns1:insuranceTerritory>Russia</ns1:insuranceTerritory>
               <!--Optional:-->
               <ns1:damage>
                  <!--Optional:-->
                  <ns1:totalDamage>true</ns1:totalDamage>
                  <!--Optional:-->
                  <ns1:fullDamage>true</ns1:fullDamage>
                  <!--Optional:-->
                  <ns1:repairOptions>dealer</ns1:repairOptions>
                  <!--Optional:-->
                  <ns1:deductible>
                     <!--Optional:-->
                     <ns1:wholeVehicle>
                        <!--Optional:-->
                        <ns1:amount>1200000</ns1:amount>
                        <!--Optional:-->
                        <ns1:fromSecondInsuredEvent>true</ns1:fromSecondInsuredEvent>
                     </ns1:wholeVehicle>
                  </ns1:deductible>
                  <!--Optional:-->
                  <ns1:paymentsWithoutDocuments>
                     <!--Optional:-->
                     <ns1:vehicleCostPercentage>10</ns1:vehicleCostPercentage>
                  </ns1:paymentsWithoutDocuments>
               </ns1:damage>
               <!--Optional:-->
               <ns1:theft>
                  <ns1:theft>true</ns1:theft>
               </ns1:theft>
               <!--Optional:-->
               <ns1:miniKASKO>false</ns1:miniKASKO>
               <!--Optional:-->
               <ns1:additionalEquipment>
                  <!--Optional:-->
                  <ns1:insuredSum>100000</ns1:insuredSum>
               </ns1:additionalEquipment>
               <!--Optional:-->
               <ns1:DAGO>
                  <ns1:insuredSum>2000000</ns1:insuredSum>
               </ns1:DAGO>
               <!--Optional:-->
               <ns1:personalAccident>
                  <!--Optional:-->
                  <ns1:carOption>
                     <ns1:insuredSum>1200000</ns1:insuredSum>
                  </ns1:carOption>
               </ns1:personalAccident>
               <!--Optional:-->
               <ns1:GAP>
                  <ns1:GAP>false</ns1:GAP>
               </ns1:GAP>
               <!--Optional:-->
               <ns1:technicalAssistance>
                  <ns1:technicalAssistance>true</ns1:technicalAssistance>
               </ns1:technicalAssistance>
               <!--Optional:-->
               <ns1:luggage>
                  <ns1:insuredSum>10000000</ns1:insuredSum>
               </ns1:luggage>
               <!--Optional:-->
               <ns1:animalsPersonalAccident>
                  <ns1:insuredSum>100000</ns1:insuredSum>
               </ns1:animalsPersonalAccident>
               <!--Optional:-->
               <ns1:courtesyCar>
                  <ns1:courtesyCar>true</ns1:courtesyCar>
               </ns1:courtesyCar>
               <!--Optional:-->
               <ns1:taxi>
                  <ns1:taxi>false</ns1:taxi>
               </ns1:taxi>
               <!--Optional:-->
               <ns1:substituteDriver>
                  <ns1:substituteDriver>true</ns1:substituteDriver>
               </ns1:substituteDriver>
               <!--Optional:-->
               
            </ns1:coverages>
            <!--Optional:-->
            <ns1:percentageDiscount>0</ns1:percentageDiscount>
            <!--Optional:-->
            <ns1:additionalInfo>
               <!--Optional:-->
               <ns1:mileageLimitations>
                  <!--Optional:-->
                  <ns1:limitations>0</ns1:limitations>
               </ns1:mileageLimitations>
               <!--Optional:-->
               
            </ns1:additionalInfo>
            <!--Optional:-->
            <ns1:isRenewal>true</ns1:isRenewal>
            <!--Optional:-->
            <ns1:numberOfCASCOLosses>0</ns1:numberOfCASCOLosses>
         </ns1:KASKOQQ1>
         <!--Optional:-->
         <ns1:OSAGOQQ1>
            <!--Optional:-->
            
            <ns1:driversList>
               <!--You have a CHOICE of the next 2 items at this level-->
               <ns1:unnamedDriverList>
                  <!--1 or more repetitions:-->
                  <ns1:unnamedDriver>
                     <ns1:age>30</ns1:age>
                     <ns1:experience>11</ns1:experience>
                     <!--Optional:-->
                     
                     <!--Optional:-->
                     <ns1:KBMManualValue>0.75</ns1:KBMManualValue><ns1:driverPersonalInfo>
                        <ns1:lastName/>
                        <ns1:firstName/>
                        <!--Optional:-->
                        
                        <ns1:birthdate>1989-05-31+03:00</ns1:birthdate>
                        <!--Optional:-->
                        
                        <!--Optional:-->
                        
                        <ns1:documentNumber/>
                     </ns1:driverPersonalInfo>
                  </ns1:unnamedDriver>
               </ns1:unnamedDriverList>
               
            </ns1:driversList>
         </ns1:OSAGOQQ1>
         <!--Optional:-->
         <ns1:insurantPhoneNumber>79859630544</ns1:insurantPhoneNumber>
      </ns1:calcPartnerQQuoteRequest>
XML;

    }
}