<?php

namespace App\Services\SK\Tinkoff\Services\Osago;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\ContractsCalculation;
use stdClass;

class OsagoService implements ProductServiceInterface{

    public $api;

    public function __construct(OsagoApi $api){
        $this->api = $api;
    }

    public function calc(ContractsCalculation $calc){

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';

        $response = $this->api->calc_osago($calc);

        return $response;
    }

    public function temp_calc(ContractsCalculation $calc){
        // TODO: Implement temp_calc() method.
    }

    public function release(ContractsCalculation $calc){

        $contract = $calc->contract;

        $response = new stdClass();
        $response->state = false;
        $response->error = 'Полис на оплате';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';

        if($contract->statys_id < 3){ // до оплаты
//            $this->api->cancel_osago($calc);
//            dd(1);
            if(!$confirm = $this->api->issueQuoteSetPartner($calc)){
                return $confirm;
            }

            return $this->api->getPaymentReferencePartner($calc);
        }

        return $response;
    }
    /*            [
      7 => "GetDocListByPolicyNumberPartnerResponse_Type getDocListByPolicyNumberPartner(GetDocListByPolicyNumberPartnerRequest_Type $parameters)"
      10 => "SendPolicyPaymentReferencePartnerResponse_Type sendPolicyPaymentReferencePartner(SendPolicyPaymentReferencePartnerRequest_Type $parameters)"
      11 => "ApproveConclusionPartnerResponse_Type approveConclusionPartner(ApproveConclusionPartnerRequest_Type $parameters)"
      12 => "GetPackDocumentsForPrintPartnerResponse_Type getPackDocumentsForPrintPartner(GetPackDocumentsForPrintPartnerRequest_Type $parameters)"
      13 => "GetAvailableSlotForPartnerResponse_Type getAvailableSlotForPartner(GetAvailableSlotForPartnerRequest_Type $parameters)"
      14 => "BookTimeSlotForPartnerResponse_Type bookTimeSlotForPartner(BookTimeSlotForPartnerRequest_Type $parameters)"
      15 => "CancelBookSlotForPartnerResponse_Type cancelBookSlotForPartner(CancelBookSlotForPartnerRequest_Type $parameters)"
      16 => "GetOrderIDBySetNumberPartnerResponse_Type getOrderIDBySetNumberPartner(GetOrderIDBySetNumberPartnerRequest_Type $parameters)"
      17 => "GetSubjectByIDPartnerResponse_Type getSubjectByIDPartner(GetSubjectByIDPartnerRequest_Type $parameters)"
      18 => "SendPaymentReferencePartnerResponse_Type sendPaymentReferencePartner(SendPaymentReferencePartnerRequest_Type $parameters)"
      19 => "SendLeadInfoPartnerResponse_Type sendLeadInfo(SendLeadInfoPartnerRequest_Type $parameters)"
      20 => "SendPayConfirmPolicyPartnerResponse_Type sendPayConfirmPolicyPartner(SendPayConfirmPolicyPartnerRequest_Type $parameters)"
      21 => "GeneratePolicyDocumentPartnerResponse_Type generatePolicyDocumentPartner(GeneratePolicyDocumentPartnerRequest_Type $parameters)"
      22 => "GetQuoteSetFullInfoBySetNumberPartnerResponse_Type getQuoteSetFullInfoBySetNumberPartner(GetQuoteSetFullInfoBySetNumberPartnerRequest_Type $parameters)"
      23 => "GetAvailableSlotForPartnerLightResponse_Type getAvailableSlotForPartnerLight(GetAvailableSlotForPartnerLightRequest_Type $parameters)"
      24 => "CheckLeadResponse_Type checkLead(CheckLeadRequest_Type $parameters)"
      25 => "GetInfoForRenewalPartnerResponse_Type getInfoForRenewalPartner(GetInfoForRenewalPartnerRequest_Type $parameters)"
      26 => "CancelPolicyPartnerResponse_Type cancelPolicy(CancelPolicyPartnerRequest_Type $parameters)"
      27 => "GetAppointmentInfoPartnerResponse_Type getAppointmentInfoPartner(GetAppointmentInfoPartnerRequest_Type $parameters)"
    ]*/
    public function check_status(ContractsCalculation $calc){

/*        if($this->api->check_payment_status($calc)){

            return $this->api->getPrintedDocumentPartner($calc);
        }*/

        if((int)$calc->payment_confirm){
            return $this->api->getPrintedDocumentPartner($calc);
        }

        return $this->api->responseError('Ссылка на оплату была отправлена на почту! После оплаты обновите страницу!');
    }

    // getDocListByPolicyNumberPartner :
    /*+"setDocInfo": {#1567
    +"setDocDetail": array:8 [
    0 => {#1568
    +"docType": "driver_license"
    +"description": "Водительское удостоверение (фото)"
    }
    1 => {#1569
    +"docType": "osago_policy_application_photo"
    +"description": "Фотография заявления о заключении договора страхования"
      }
      2 => {#1570
    +"docType": "osago_policy_photo"
    +"description": "Фотография полиса ОСАГО (Экземпляр клиента)"
      }
      3 => {#1571
    +"docType": "refusal_driver_license_photo"
    +"description": "Отказ в предоставлении документов (фото)"
      }
      4 => {#1572
    +"docType": "russian_passport"
    +"description": "Паспорт РФ (фото)"
      }
      5 => {#1573
    +"docType": "technical_inspection_photo"
    +"description": "Диагностическая карта (фото)"
      }
      6 => {#1574
    +"docType": "vehicle_passport_photo"
    +"description": "ПТС (фото)"
      }
      7 => {#1575
    +"docType": "vehicle_registartion_certificate_photo"
    +"description": "СТС (фото)"
      }
    ]
    }*/
    public function get_files(ContractsCalculation $calc){

        $this->api->get_files_list($calc);
    }


}