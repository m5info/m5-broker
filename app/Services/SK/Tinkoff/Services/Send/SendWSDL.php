<?php

namespace App\Services\SK\Tinkoff\Services\Send;


use Doctrine\DBAL\Cache\CacheException;

class SendWSDL{


    public $url;
    public $login;
    public $password;

    public $method_url = '/toi/partners/quickquote/v2.0?wsdl';

    public function __construct($url, $login, $password){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;


    }

    public function getClient()
    {

        $client = new \SoapClient($this->url."/toi/partners/quickquote/v2.0?wsdl", array(
            'trace' => 1,
            'exception' => 1,
            "soap_version" => SOAP_1_1,
            'connection_timeout' => 600,
            'stream_context' => stream_context_create(
                array(
                    'ssl' => array(
                        'verify_peer'       => false,
                        'verify_peer_name'  => false,
                        'allow_self_signed' => true,
                    )
                )
            )));


        return $client;
    }

}