<?php

namespace App\Services\SK\Tinkoff\Services\Send;


use Doctrine\DBAL\Cache\CacheException;

class SendJSON{

    public $url;
    public $login;
    public $password;

    public $api_setting = null;



    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

        $this->api_setting = $api_setting;

    }

    public function send($method, $array)
    {
        if($method == '/TOIESB_MarkModelProxy/CarReferenceServiceJSON')
        {
            //справочники доступны только по 23001 порту
            $this->url = str_replace('24001', '23001', $this->url);
        }
        $curl = curl_init();
        $headers = [];
        $headers[] = "Content-Type: application/json";


        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url.$method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => \GuzzleHttp\json_encode($array),
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //Логируем ошибки
            //dd($err);
        }


        return \GuzzleHttp\json_decode($response);


    }

}