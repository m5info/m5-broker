<?php

namespace App\Services\SK\Tinkoff\Clients;

use SoapClient;
use SoapHeader;
use SoapVar;

class DigestSoapClient extends SoapClient{

    public $login;
    public $password;


    public function __construct($params){

        $this->login = $params['login'];
        $this->password = $params['password'];

        $wsdl = "{$params['url']}{$params['service_name']}";
        parent::__construct($wsdl, ['trace' => true, 'exception' => true, 'encoding'=>'utf8']);

    }



}