<?php

namespace App\Services\SK\Alpha\Clients;

use SoapClient;
use SoapHeader;
use SoapVar;

class DigestSoapClient extends SoapClient{

    public $login;
    public $password;
    protected $soapClient;


    public function __construct($params)
    {
        $wsdl = "{$params['url']}{$params['service_name']}?wsdl";
        $this->login = $params['login'];
        $this->password = $params['password'];

        $this->soapClient          = new SoapClient($wsdl, array('trace' => 1, 'exception' => 0));
        $timestamp                 = gmdate('Y-m-d\TH:i:s\Z', time());
        $nonce                     = mt_rand();
        $nonceBase64               = base64_encode(pack('H*', $nonce));
        $passDigest                = base64_encode(pack('H*', sha1(pack('H*', $nonce) . pack('a*', $timestamp) . pack('a*', $this->password))));
        $auth                      = <<<XML
        <wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
            <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="UsernameToken-3">
                <wsse:Username>$this->login</wsse:Username>
                <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">$passDigest</wsse:Password>
                <wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">$nonceBase64</wsse:Nonce>
                <wsu:Created xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">$timestamp</wsu:Created>
            </wsse:UsernameToken>
        </wsse:Security>
XML;
        $authvalues                = new SoapVar($auth, XSD_ANYXML);
        $objHeader_Session_Outside = new SoapHeader("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", $authvalues, true);

        $this->soapClient->__setSoapHeaders(array($objHeader_Session_Outside));
    }

    public function getClient()
    {
        return $this->soapClient;
    }

    private function soapCall($functionName, $params = [])
    {
        try {
            return $this->soapClient->__call($functionName, $params)->return;
        } catch (\Exception $e) {
            //	\DevLogs::email($e->getMessage());
            throw new \Exception("soap failed");
        }
    }



































    /*


    public function __construct($params){

        $this->login = 'EVROGARANTS';
        $this->password = '03N7kQto';

        $wsdl = "https://b2b.alfastrah.ru/cxf/{$params['service_name']}?wsdl";
        parent::__construct($wsdl, ['trace' => true, 'exception' => false]);

    }

    public function __call($function_name, $arguments){

        $timestamp      = gmdate('Y-m-d\TH:i:s\Z', time());
        $nonce          = mt_rand();
        $nonceBase64    = base64_encode(pack('H*', $nonce));
        $passDigest     = base64_encode(pack('H*', sha1(pack('H*', $nonce) . pack('a*', $timestamp) . pack('a*', $this->password))));

        $auth           = <<<XML
        <wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
            <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="UsernameToken-3">
                <wsse:Username>{$this->login}</wsse:Username>
                <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">{$passDigest}</wsse:Password>
                <wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">{$nonceBase64}</wsse:Nonce>
                <wsu:Created xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">{$timestamp}</wsu:Created>
            </wsse:UsernameToken>
        </wsse:Security>
XML;
        $authvalues      = new SoapVar($auth, XSD_ANYXML);
        $security_header = new SoapHeader("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", $authvalues, true);

        $this->__setSoapHeaders([$security_header]);
        return parent::__call($function_name, $arguments);
    }


    public function getClient()
    {
        return $this->soapClient;
    }

    private function soapCall($functionName, $params = [])
    {
        try {
            return $this->soapClient->__call($functionName, $params)->return;
        } catch (\Exception $e) {
            //	\DevLogs::email($e->getMessage());
            throw new \Exception("soap failed");
        }
    }
    */
}