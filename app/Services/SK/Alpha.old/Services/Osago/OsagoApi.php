<?php

namespace App\Services\SK\Alpha\Services\Osago;

use App\Models\Contracts\ContractsCalculation;
use App\Models\Contracts\KbmCalculation;
use App\Services\Kbm\Alfa\Calc;
use App\Services\SK\Alpha\Clients\DigestSoapClient;
use Carbon\Carbon;
use Mockery\Exception;


class OsagoApi{

    public $url;
    public $login;
    public $password;
    public $errors = [];


    public function __construct($url, $login, $password){
        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
    }

    public function get_service_client($service_name){

        return new DigestSoapClient([
            'url' => $this->url,
            'service_name' => $service_name,
            'login' => $this->login,
            'password' => $this->password,
        ]);

    }





    /* ************************************************************
     * API OPERATION METHODS
     **************************************************************/

    public function calc_osago(ContractsCalculation $calc){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $param = $this->get_calc_params($calc);

        if(!empty($this->errors)){
            $result->error = implode(';', $this->errors);
            return $result;
        }

        try{
            $service_client = $this->get_service_client('CalcOsago')->getClient();
            $result->answer = $service_client->calcOsago($param);
            $result->state = true;

        }catch (\SoapFault $fault){
            $result->error = $fault->getMessage();
        }

        return $result;
    }


    public function release_osago(ContractsCalculation $calc){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];
        $contract = $calc->contract;

        $service_client = $this->get_service_client('OsagoAddContract')->getClient();

        // тут короче идет создание договора, если не добавил, то обновляет договор
        try{

            $result->answer = $service_client->addContract($this->get_release_params($calc, 1));
            $result->bso_id = $contract->bso_id;

            sleep(1); // без ожидания не отрабатывает (attack error)
            $service_client2 = $this->get_service_client('OsagoAddContract')->getClient();
            $result->answer = $service_client2->updateContract($this->get_release_params($calc, 5));
            $result->bso_id = $contract->bso_id;
            $result->state = true;

        }catch (\SoapFault $fault){
                $result->error = $fault->getMessage();
        }

        return $result;
    }


    public function get_forms_osago(ContractsCalculation $calc, $form_id){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

//        $params = $this->get_forms_params($calc);
        $param = $this->get_form_params($calc, $form_id);

        $service_client = $this->get_service_client('PrintedForms')->getClient();
//        dd($service_client->GetPrintedFormsOsago($params));

        try{

            $result->form = $service_client->GetPrintedFormByContractNumber($param);
//            $result->form = $service_client->GetPrintedFormByContractId($param);
            $result->state = true;

        }catch (\SoapFault $fault){
            $result->error = 'Не удалось получить формы для печати';
        }

        return $result;

    }


    /* ************************************************************
     * COLLECT PARAM METHODS
     **************************************************************/


    public function get_release_params(ContractsCalculation $calc, $status_id){

        $contract = $calc->contract;

        $kbm = null;
        if($contract->drivers_type_id == 0){
            foreach($contract->drivers as $driver){
                if(!isset($kbm)){
                    $kbm = $driver->class_kbm;
                }else{
                    if($kbm > $driver->class_kbm){
                        $kbm = $driver->class_kbm;
                    }
                }
            }
        }

        if($contract->owner->type == 0){
            // название параметра
            $ownerTransTypeTitle = 'TransportOwner';
            $ownerPolicyTypeTitle = 'PolicyOwner';

            $city = $calc->contract->insurer->data_fl->address_fact_city;
        }else{
            // название параметра
            $ownerTransTypeTitle = 'TransportOwnerJuridical';
            $ownerPolicyTypeTitle = 'PolicyOwnerJuridical';
        }

        $request_calc = $this->calc_osago($calc);

        $result = [];
        $result['Multidrive'] = ($contract->drivers_type_id == 0) ? 'No' : 'Yes';
        $result['BeginDate'] = date('Y-m-d\TH:i:s', strtotime($contract->begin_date));
        $result['EndDate'] = date('Y-m-d', strtotime($contract->end_date));
        $result['DateSign'] = date('Y-m-d', strtotime(Carbon::now()));
        $result["InsurancePremium"] = $contract->payment_total;
        $result["PolicyKBM"] = $kbm;
        $result["PolicySeria"] = $contract->bso ? $contract->bso->bso_serie->bso_serie : ""; // русские символы у них вроде
        $result["PolicyNumber"] = $contract->bso->bso_number;
        $result["PurposeOfUse"] = $this->get_purpose($calc);
        $result["ContractOptionId"] = "1"; // Первоначальный
        $result["ContractStatusTypeId"] = $status_id; // 1 - Статус договора - заявление
        $result["WithUseTrailer"] = ($contract->object_insurer_auto->is_trailer == 0) ? "false" : "true";
        $result[$ownerPolicyTypeTitle] = $this->get_insurer_release($calc); // страхователь
        $result[$ownerTransTypeTitle] = $this->get_owner_release($calc); // владелец авто
        $result['Transport'] = $this->release_get_transport($calc);
        $result['AgentBlock'] = $this->get_agent($calc);
        $result['Drivers'] = $this->get_drivers($calc, 'release');
        $result['Period'] = $this->get_period_list($calc, 'release');

        $coeff = $request_calc->answer->Coefficients;
        $result['Coefficients'] = [
            "tb" =>  $coeff->Tb,
            "kt" =>  $coeff->Kt,
            "kbm" => $coeff->Kbm,
            "kvs" => $coeff->Kvs,
            "ko" =>  $coeff->Ko,
            "km" =>  $coeff->Km,
            "ks" =>  $coeff->Ks,
            "kp" =>  $coeff->Ks,
            "kn" =>  $coeff->Kn,
            "kpr" => $coeff->Kpr
        ];
        $result["RSAKBM"] = [
            "IdRequestCalc" => $request_calc->answer->IdRequestCalc,
            "DateKBMReq" => date('Y-m-d\TH:i:s', strtotime(Carbon::now()))
        ];

        return $result;
    }


    public function get_calc_params(ContractsCalculation $calc){

        $contract = $calc->contract;

        $DriversRestriction = $calc->contract->drivers_type_id == 1 ? false : true;

        $city = null;

        if($contract->owner->type == 0){
            // название параметра
            $ownerTypeTitle = 'TransportOwner';
            $city = $calc->contract->insurer->data_fl->address_fact_city;
        }else{
            // название параметра
            $ownerTypeTitle = 'TransportOwnerJuridical';
            $city = $calc->contract->insurer->data_ul->address_fact_city;
        }

        $result = [
            "ContractType" => "Первоначальный",
            "PolicyBeginDate" => date('Y-m-d\TH:i:s', strtotime($contract->begin_date)),
            "InsuranceMonth" => "12",
            "RegionOfUse" => $city,
            "CityOfUse" => $city,
            // Ограничения на лиц допущенных к управлению
            "DriversRestriction" => $DriversRestriction,
            // Признак следования ТС к месту регистрации
            "FollowToRegistration" => false,
            // Наличие нарушений
            "FlagrantViolation" => false,
        ];

        $result['Transport'] = $this->get_transport($calc);
        $result[$ownerTypeTitle] = $this->get_subject('owner', $calc);
        $result['PeriodList'] = $this->get_period_list($calc);
        $result["Drivers"] = $this->get_drivers($calc);

        return $result;
    }


    public function get_period_list(ContractsCalculation $calc, $method = 'calc'){
        $contract = $calc->contract;

        if($method == 'calc'){
            $period_list[] = [
                'StartDate' => date('Y-m-d', strtotime($contract->begin_date)),
                'EndDate'   => date('Y-m-d', strtotime($contract->end_date))
            ];
        }elseif($method == 'release'){
            $period_list[] = [
                'BeginDate' => date('Y-m-d', strtotime($contract->begin_date)),
                'EndDate'   => date('Y-m-d', strtotime($contract->end_date))
            ];
        }

        return $period_list;
    }

    public function get_purpose(ContractsCalculation $calc){

        $purpose_title = '';

        if($purpose = $calc->sk_purpose){
            $purpose_title = $purpose->sk_title;
        }else{
            $this->errors[] = 'Цель использования не найдена';
        }

        return $purpose_title;
    }

    public function release_get_transport(ContractsCalculation $calc){

        $transport = [];

        if($calc->object_insurer_auto){

            $auto = $calc->object_insurer_auto;

            $transport['VIN'] = $auto->vin;
            $transport['ReleaseDate'] = date('Y-m-d', strtotime($auto->car_year));
            $transport['Power'] = $auto->power;
            $transport['PowerKwt'] = $auto->power * 0.7355;
            $transport['Passport'] = [
                "Seria" => $auto->docserie,
                "Number" => $auto->docnumber,
                "DateOfIssue" => $auto->docdate
            ];

            if($cat = $calc->sk_category){
                // тип авто
                $categories_types = [
                    'A - мотоциклы' => 'Мотоциклы и мотороллеры',
                    'B - легковые'  => 'Легковой а/м',
                    'C - грузовые'  => 'Грузовой а/м',
                    'D - автобусы'  => 'Автобусы',
                ];

                if(isset($categories_types[$cat->sk_title])){
                    $transport['TransportType'] = $categories_types[$cat->sk_title];
                }

            }else{
                $this->errors[] = 'Не синхронизирован справочник: Категория';
            }

            if($mark = $calc->sk_mark){
                $transport['BrandId'] = $mark->vehicle_marks_sk_id;
                $transport['BrandName'] = $mark->title;
            }else{
                $this->errors[] = 'Не синхронизирован справочник: Марки';
            }

            if($model = $calc->sk_model){
                $transport["ModelId"] = $model->vehicle_models_sk_id;
                $transport["ModelName"] = $model->sk_title;
            }else{
                $this->errors[] = 'Не синхронизирован справочник: Модели';
            }

        }else{
            $this->errors[] = 'Авто не найдено';
        }

        return $transport;

    }

    public function get_transport(ContractsCalculation $calc){

        $transport = [
            //будет true по умолчанию
            "RegistrationRussia" => true,
        ];
        if($auto = $calc->object_insurer_auto){

            if($auto->purpose_id == 3 && $is_taxi = false){ // если такси + созд. переменную
                $is_taxi = true;
            }else{
                $is_taxi = false;
            }

            $transport['Power'] = (float)$auto->power;
            $transport['IsTaxi'] = $is_taxi;
            $transport['VIN'] = $auto->vin;

        }else{
            $this->errors[] = 'Авто не найдено';
        }


        if($calc->sk_category){

            // категория авто
            $transport['Category'] = $calc->sk_category->sk_title;

            // тип авто
            $categories_types = [
                'A - мотоциклы' => 'Мотоциклы и мотороллеры',
                'B - легковые'  => 'Легковой а/м',
                'C - грузовые'  => 'Грузовой а/м',
                'D - автобусы'  => 'Автобусы',
            ];

            if(isset($categories_types[$calc->sk_category->sk_title])){
                $transport['Type'] = $categories_types[$calc->sk_category->sk_title];
            }

        }else{
            $this->errors[] = 'Не синхронизирован справочник: Категория';
        }

        return $transport;
    }


    public function get_drivers(ContractsCalculation $calc, $method = 'calc'){

        $drivers = [];

        if($calc->drivers){
            if($method == 'calc'){ // если для калькуляции

                foreach ($calc->drivers as $driver){

                    $fio = $driver->get_fio();

                    $drivers[] = [
                        'LastName'       => $fio['last_name'],
                        'FirstName'      => $fio['first_name'],
                        'MiddleName'     => $fio['middle_name'],
                        'BirthDate'      => $driver->birth_date,
                        'ExperienceDate' => $driver->exp_date,
                        'DriverDocument' => array(
                            'Number' => $driver->doc_num,
                            'Seria'  => $driver->doc_serie,
                        )
                    ];

                }

            }elseif($method == 'release'){ // если для выпуска

                foreach ($calc->drivers as $driver){

                    $fio = $driver->get_fio();

                    $drivers[] = [
                        'LastName'       => $fio['last_name'],
                        'FirstName'      => $fio['first_name'],
                        'MiddleName'     => $fio['middle_name'],
                        'Sex'            => ($driver->sex == 0) ? "Male" : "Female",
                        'BirthDate'      => $driver->birth_date,
                        'DriverLicense'  => array(
                            'Number' => $driver->doc_num,
                            'Seria'  => $driver->doc_serie,
                        ),
                        'IsResident' => "true",
                        'DriveBeginDate' => $driver->exp_date,
                        'DriverClass' => $driver->class_kbm
                    ];

                }

                return $drivers;

            }

        }else{
            $this->errors[] = 'Водител(ь/и) не найден(ы)';
        }

        return ['Driver' => $drivers];
    }

    /*
     * юзал для драйверов, ща наверно можно удалить
     */
    public function get_kbm_class($data){

        $result = new \stdClass();
        $result->kbm = '1';
        $result->class_kbm = '3';

        $fio = $data->get_fio();

        $kbmCalculation = KbmCalculation::create([
            'first_name'      => $fio['last_name'],
            'last_name'       => $fio['first_name'],
            'middle_name'     => $fio['middle_name'],
            'birth_date'      => date("Y-m-d 00:00:00", strtotime($data->birth_date)),
            'document_series' => $data->doc_serie,
            'document_number' => $data->doc_num,
        ]);

        $calc = new Calc();

        $calc->handle($kbmCalculation);

        $result->kbm = $kbmCalculation->kbm;
        $result->class_kbm = $kbmCalculation->class_kbm;

        return $result;
    }


    public function get_insurer_release(ContractsCalculation $calc, $subj_type = 'insurer'){

        $subject = [];

        $contract = $calc->contract;

        if($contract_subject = $contract->{$subj_type}){

            // если физик
            if($contract_subject->type == 0){

                $contract_owner_info = $contract_subject->get_info();

                $fio = explode(' ', $contract_subject->title);

                $subject['LastName']       = $fio[0];
                $subject['FirstName']      = isset($fio[1]) ? $fio[1] : $fio[0];
                $subject['MiddleName']     = isset($fio[2]) ? $fio[2] : $fio[0];
                $subject['Sex']            = ($contract_owner_info->sex == 0) ? "Male" : "Female";
                $subject['BirthDate']      = $contract_owner_info->birthdate;
                $subject['Passport']       = [
                    "Seria" => $contract_owner_info->doc_serie,
                    "Number" => $contract_owner_info->doc_number,
                    "DateOfIssue" => $contract_owner_info->doc_date
                ];
                $subject["Address"] = [
                    "Country" => "Россия",
                    "City" => isset($contract_owner_info->address_register_city) ? $contract_owner_info->address_register_city : "",
                    "Street" => isset($contract_owner_info->address_register_street) ? $contract_owner_info->address_register_street : "",
                    "HouseNumber" => isset($contract_owner_info->address_register_house) ? $contract_owner_info->address_register_house : "",
                    "ApartmentNumber" => isset($contract_owner_info->address_register_flat) ? $contract_owner_info->address_register_flat : "",
                    "KLADR" => isset($contract_owner_info->address_register_city_kladr_id) ? $contract_owner_info->address_register_city_kladr_id : "",
                ];
                $subject['Phone']          = isset($contract_subject->phone) ? $contract_subject->phone : "";
                $subject['IsResident']     = "true";

            }else{ // юрик

                $contract_owner_info = $contract_subject->get_info();

                $subject = [
                    'FullName' => $contract_owner_info->title,
                    'ShortName' => $contract_owner_info->title,
                    'INN' => $contract_owner_info->inn,
                    'KPP' => $contract_owner_info->kpp,
                    'Address' => [
                        "Country" => "Россия",
                        "Region" => $contract_owner_info->address_register_city,
                        "City" => $contract_owner_info->address_register_city,
                        "Street" => $contract_owner_info->address_register_street,
                        "HouseNumber" => $contract_owner_info->address_register_house,
                    ],
                    'Phone' => isset($contract_subject->phone) ? $contract_subject->phone : "",
                    'IsResident' => "true",
                ];

            }
        }

        return $subject;
    }

    public function get_agent(ContractsCalculation $calc){

        $contract = $calc->contract;

        if($agent_info = $contract->getAgentSettings($calc)){

            $agent = [
                "ChannelSaleCode" => $agent_info->сhannel_sale_id,
                "AgentContractNumber" => $agent_info->agent_name,
                "AgentName" => $agent_info->contract_name,
                "DepartmentId" => $agent_info->department_id,
                "SignerSubjectId" => $agent_info->signer_id,
                "ManagerSubjectId" => $agent_info->manager_id
            ];

        }else{
            $this->errors[] = 'Агент не найден';
        }

        return $agent;
    }

    public function get_owner_release(ContractsCalculation $calc){

        $contract = $calc->contract;

        // владелец это страхователь
        if($contract->owner->id == $contract->insurer->id){
            $subject = $this->get_insurer_release($calc);
        }else{
            $subject = $this->get_insurer_release($calc, 'owner');
        }

        return $subject;
    }

    public function get_subject($subject_name, ContractsCalculation $calc){

        $contract = $calc->contract;

        $subject = [];

        if($contract_subject = $contract->{$subject_name}){

            // если физик
            if($contract_subject->type == 0){

                $contract_owner_info = $contract_subject->get_info();

                $fio = explode(' ', $contract_subject->title);

                $subject["LastName"]       = $fio[0];
                $subject["FirstName"]      = isset($fio[1]) ? $fio[1] : $fio[0];
                $subject["MiddleName"]     = isset($fio[2]) ? $fio[2] : $fio[0];
                $subject["BirthDate"]      = $contract_owner_info->birthdate;

                $ExperienceDate = null;

                // если не мультидрайв
                if($calc->contract->drivers_type_id == 0){

                    foreach ($calc->drivers as $driver){
                        $ExperienceDate[] = $driver->exp_date;
                    }

                    // наименьший опыт берем
                    if($ExperienceDate){
                        usort($ExperienceDate, function($a, $b) {
                            $dateTimestamp1 = strtotime($a);
                            $dateTimestamp2 = strtotime($b);

                            return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
                        });
                    }else{
                        $this->errors[] = 'Укажите опыт вождения водителя';
                    }

                    $ExperienceDate = $ExperienceDate[0];
                }

                $subject['ExperienceDate'] = $ExperienceDate;

                $subject['PersonDocument'] = [
                    'Number' => $contract_subject->doc_number,
                    'Seria'  => $contract_subject->doc_serie,
                ];


            }else{

                $subject = [
                    'OrgName' => $contract_subject->title,
                    'INN'     => $contract_subject->inn,
                ];
            }

        }

        return $subject;
    }


    public function get_forms_params(ContractsCalculation $calc){

        $contract = $calc->contract;
// параметры для теста (для получения списка форм)
        $result = [];
        $result['ContractId'] = $calc->sk_key_id; // 1 - 80254474, 5 - 80463694 (для теста)
        $result["ContractSeria"] = $contract->bso ? $contract->bso->bso_serie->bso_serie : ""; // русские символы у них вроде
        $result["ContractNumber"] = $contract->bso->bso_number;

        return $result;
    }

    public function get_form_params(ContractsCalculation $calc, $form_id){
        $contract = $calc->contract;

        $result = [];
//        $result['ContractId'] = $calc->sk_key_id;
        $result['ContractSeria'] = $contract->bso ? $contract->bso->bso_serie->bso_serie : "";
        $result['ContractNumber'] = $contract->bso->bso_number;

        $result['PrintedFormId'] = $form_id;


        return $result;
    }

}
