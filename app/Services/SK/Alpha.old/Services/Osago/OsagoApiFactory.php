<?php


namespace App\Services\SK\Alpha\Services\Osago;


class OsagoApiFactory
{
    public $eosago_api;
    public $osago_api;


    public function __construct($url, $login, $password){

        $this->eosago_api = new EosagoApi($url, $login, $password);
        $this->osago_api = new OsagoApi($url, $login, $password);
    }

}