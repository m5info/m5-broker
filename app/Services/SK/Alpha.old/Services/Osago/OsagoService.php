<?php

namespace App\Services\SK\Alpha\Services\Osago;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\File;
use App\Repositories\FilesRepository;
use stdClass;

class OsagoService implements ProductServiceInterface{

    public $osago_api;
    public $eosago_api;

    public function __construct(OsagoApiFactory $api){
        $this->osago_api = $api->osago_api;
        $this->eosago_api = $api->eosago_api;
    }

    public function temp_calc(ContractsCalculation $calc){
        // TODO: Implement temp_calc() method.
    }

    public function calc(ContractsCalculation $calc){

        $contract = $calc->contract;
        $response = new stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';

        if($contract->is_epolicy == 1){ // Электронный

            $result = $this->eosago_api->calcEosago($calc);

            if($result->state && isset($result->answer->Coefficients)){
                $calcJson = \GuzzleHttp\json_decode($calc->json);
                $calcJson->RefCalcId = $result->answer->RefCalcId;
                $calcJson->IdRequestCalc = $result->answer->IdRequestCalc;
                $calcJson->UPID = $result->upid;
                $calc->json = \GuzzleHttp\json_encode($calcJson);
                $calc->save();
            }

        }else{
            $result = $this->osago_api->calc_osago($calc);
        }

        if($result->state && isset($result->answer->Coefficients)){

            $answer = $result->answer;
            $coeffs = $answer->Coefficients;

            $response->state = true;
            $response->statys_id = 1;
            $response->answer = $answer;

            $response->payment_total = (string)$answer->InsuranceBonus;
            $response->sk_key_id = (string)$answer->IdRequestCalc;

            $response->error = '';
            $response->msg = "
                ТБ: {$coeffs->Tb}; 
                КТ: {$coeffs->Kt}; 
                КБМ: {$coeffs->Kbm}; 
                КВС: {$coeffs->Kvs}; 
                КС: {$coeffs->Ks}; 
                КП: {$coeffs->Kp}; 
                КМ: {$coeffs->Km}; 
                КПР: {$coeffs->Kpr}; 
                КН: {$coeffs->Kn};
            ";

        }else{
            $response->error = $result->error;
        }

        return $response;
    }



    public function release(ContractsCalculation $calc){

        $contract = $calc->contract;

        $response = new stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';

        if($contract->is_epolicy == 1){ // Электронный
            $result = $this->eosago_api->releaseEosago($calc);
        }else{
            $result = $this->osago_api->release_osago($calc);
        }


        if($result->state){

            $answer = $result->answer;

            $response->state = true;
            $response->statys_id = 4;  //Выпущен
            $response->answer = $answer;

            $calc->update(['sk_key_id' => (string)$answer->contract_id]);

        }else{
            $response->error = $result->error;
        }

        $response->bso_id = $contract->bso->id;

        return $response;
    }


    public function check_status(ContractsCalculation $calc){

    }


    public function get_files(ContractsCalculation $calc){

// ответ от GetPrintedFormsOsago
//    0 => {#1029
//            +"PrintedFormId": "716"
//            +"PrintedFormName": "Заявление о заключении договора ОСАГО"
//    }
//    1 => {#1024
//            +"PrintedFormId": "754"
//            +"PrintedFormName": "Полис ОСАГО МММ"
//    }
//    2 => {#993
//            +"PrintedFormId": "755"
//            +"PrintedFormName": "Полис ОСАГО МММ копия"
//    }
//    3 => {#1023
//            +"PrintedFormId": "758"
//            +"PrintedFormName": "Полис ОСАГО МММ образец"
//    }
//    4 => {#1028
//            +"PrintedFormId": "370"
//            +"PrintedFormName": "Квитанция А7"
//    }
//    5 => {#1031
//            +"PrintedFormId": "371"
//            +"PrintedFormName": "Квитанция А7 (копия)"
//    }
//    6 => {#1032
//            +"PrintedFormId": "624"
//            +"PrintedFormName": "Счет"
//    }

        $contract = $calc->contract;
        $payment = $contract->get_payment_first();

        $templates = [
            '716' => 'Заявление о заключении договора ОСАГО',
            '758' => 'Полис ОСАГО МММ образец',
            '754' => 'Полис ОСАГО МММ',
            '370' => 'Квитанция А7',
            '624' => 'Счет',
        ];

        $filesRepository = new FilesRepository();

        foreach ($templates as $form_id => $templ){
            $answer = $this->osago_api->get_forms_osago($calc, $form_id);

            if($answer->state == true){
                $document = $answer->form;

                if(isset($document) && isset($document->Content)){
                    $File_n = 'pdf';
                    $file_contents = $document->Content;
                    $path = storage_path('app/' . Contracts::FILES_DOC . "/{$contract->id}/");

                    if(!is_dir(($path))){
                        mkdir(($path), 0777, true);
                    }

                    $file_name = uniqid();

                    file_put_contents($path . $file_name . '.' . $File_n, $file_contents);

                    $file = File::create([
                        'original_name' => $templ .'.'. $File_n,
                        'ext' => $File_n,
                        'folder' => Contracts::FILES_DOC . "/{$contract->id}/",
                        'name' => $file_name,
                        'user_id' => auth()->id()
                    ]);

                    $contract->masks()->save($file);

                }
            }
        }

        return true;

    }


}