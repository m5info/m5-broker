<?php

namespace App\Services\SK\Alpha\Services\Osago;

use App\Models\Contracts\ContractsCalculation;
use App\Models\Contracts\KbmCalculation;
use App\Services\Kbm\Alfa\Calc;
use App\Services\SK\Alpha\Clients\DigestSoapClient;
use Carbon\Carbon;
use Mockery\Exception;
use SoapVar;


class EosagoApi{

    public $url;
    public $login;
    public $password;
    public $errors = [];


    public function __construct($url, $login, $password){
        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
    }

    public function getServiceClient($service_name){

        return new DigestSoapClient([
            'url' => $this->url,
            'service_name' => $service_name,
            'login' => $this->login,
            'password' => $this->password,
        ]);

    }

    /* ************************************************************
     * API OPERATION METHODS
     **************************************************************/

    public function calcEosago(ContractsCalculation $calc){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $upid = $this->getUpid($calc);

        $this->checkDrivers($calc); // проверяем водителей
        $this->checkInsurerOwner($calc); // проверяем страх. и владельца
        $this->checkVehicle($calc); // проверяем авто

        $param = $this->getCalcParams($calc, $upid->UPID);
        $param = new SoapVar($param, XSD_ANYXML);

        if(!empty($this->errors)){
            $result->error = implode(';', $this->errors);
            return $result;
        }

        try{
            $service_client = $this->getServiceClient('partner/OsagoCalcRelaunch')->getClient();

            $result->answer = $service_client->CalcEOsago($param);
            $result->upid = $upid->UPID;

            if ($result->answer->InsuranceBonus){
                $result->state = true;
            }else{
                $result->error = $result->answer->commentList;
            }
        }catch (\SoapFault $fault){
            dump($service_client->__getLastRequest());
            dd($service_client->__getLastResponse());
            dd($fault->getMessage());
            $result->error = $fault->getMessage();
        }

        return $result;
    }

    public function releaseEosago(ContractsCalculation $calc){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];
        $contract = $calc->contract;

        $serviceClient = $this->getServiceClient('partner/EOsagoRelaunch')->getClient();

        $params = $this->getReleaseParams($calc);

        $params = new SoapVar($params, XSD_ANYXML);

        try{
            dd($serviceClient->loadContractEosago($params));
            $result->answer = $serviceClient->loadContractEosago($params);
            $result->bso_id = $contract->bso_id;
            $result->state = true;

        }catch (\SoapFault $fault){
            dump($serviceClient->__getLastRequest());
            dd($serviceClient->__getLastResponse());
            dd($fault->getMessage());
            $result->error = $fault->getMessage();
        }
dd($result);
        return $result;
    }

    public function getUpid(ContractsCalculation $calc)
    {
        $contract = $calc->contract;
        $agentInfo = $contract->getAgentSettings($calc);
        $partnerServiceClient = $this->getServiceClient('partner/PartnersInteraction')->getClient();

        $partnerParams = [ 'callerCode' => $agentInfo->agent_id ];

        return $partnerServiceClient->getUPID($partnerParams);
    }


    public function checkDrivers(ContractsCalculation $calc)
    {
        if($calc->drivers) {

            $driversArr = [];

            foreach ($calc->drivers as $driver) {

                $serviceClient = $this->getServiceClient('partner/EOsagoRelaunch')->getClient();

                $fio = $driver->get_fio();
                $last_name = $fio['last_name'];
                $first_name = $fio['first_name'];
                $middle_name = $fio['middle_name'];
                $birth_date = date('Y-m-d', strtotime($driver->birth_date));
                $doc_date = date('Y-m-d', strtotime($driver->doc_date));
                $now = date('Y-m-d\TH:i:s', strtotime(Carbon::now()));

                $driverParams = <<<XML
                <ns1:DriverRequest>
                    <DriverRequestValue>
                        <DriverInfoRequest>
                            <Surname>$middle_name</Surname>
                            <Name>$first_name</Name>
                            <Patronymic>$last_name</Patronymic>
                            <BirthDate>$birth_date</BirthDate>
                            <DriverDocument>
                                <Serial>$driver->doc_serie</Serial>
                                <Number>$driver->doc_num</Number>
                            </DriverDocument>
                            <DriverDocDate>$doc_date</DriverDocDate>
                        </DriverInfoRequest>
                        <DateRequest>$now</DateRequest>
                    </DriverRequestValue>
                </ns1:DriverRequest>
XML;

                $param = new SoapVar($driverParams, XSD_ANYXML);

                $loadDrivers = $serviceClient->loadDriver($param);

                $driversArr[$driver->id] = $loadDrivers->IDCheckDriver;
            }

            $calcJson = (object)[];
            $calcJson->IDCheckDriver = $driversArr;
            $calc->json = \GuzzleHttp\json_encode($calcJson);
            $calc->save();
        }
    }

    public function checkInsurerOwner(ContractsCalculation $calc)
    {
        $serviceClient = $this->getServiceClient('partner/EOsagoRelaunch')->getClient();
        $serviceClient2 = $this->getServiceClient('partner/EOsagoRelaunch')->getClient();

        $ownerParams = $this->getSubjectForCheck($calc, 'owner');
        $insurerParams = $this->getSubjectForCheck($calc, 'insurer');

        $ownerParams = new SoapVar($ownerParams, XSD_ANYXML);
        $insurerParams = new SoapVar($insurerParams, XSD_ANYXML);

        $loadOwner = $serviceClient->loadInsurerOwner($ownerParams);
        sleep(1);
        $loadInsurer = $serviceClient2->loadInsurerOwner($insurerParams);

        $calcJson = \GuzzleHttp\json_decode($calc->json);
        $calcJson->IDCheckInsurerOwner = [
            'owner' => $loadOwner->IDCheckInsurerOwner,
            'insurer' => $loadInsurer->IDCheckInsurerOwner
        ];

        $calc->json = \GuzzleHttp\json_encode($calcJson);
        $calc->save();
    }

    public function checkVehicle(ContractsCalculation $calc)
    {
        $serviceClient = $this->getServiceClient('partner/EOsagoRelaunch')->getClient();

        $vehicleParams = $this->getVehicleForCheck($calc);

        $vehicleParams = new SoapVar($vehicleParams, XSD_ANYXML);

        $loadVehicle = $serviceClient->loadVehicle($vehicleParams);

        $calcJson = \GuzzleHttp\json_decode($calc->json);
        $calcJson->IDCheckTS = $loadVehicle->IDCheckTS;

        $calc->json = \GuzzleHttp\json_encode($calcJson);
        $calc->save();
    }

    public function getVehicleForCheck(ContractsCalculation $calc){

        if($auto = $calc->object_insurer_auto){

            if($mark = $calc->sk_mark){
                $brandName = $mark->sk_title;
            }else{
                $this->errors[] = 'Не синхронизирован справочник: Марки';
            }

            if($model = $calc->sk_model){
                $modelName = $model->sk_title;
            }else{
                $this->errors[] = 'Не синхронизирован справочник: Модели';
            }

            if($calc->sk_category){

                // категория авто
                $transport_category = $calc->sk_category->sk_title;

                // тип авто
                $categories_types = [
                    'A - мотоциклы' => 'Мотоциклы и мотороллеры',
                    'B - легковые'  => 'Легковой а/м',
                    'C - грузовые'  => 'Грузовой а/м',
                    'D - автобусы'  => 'Автобусы',
                ];

                if(isset($categories_types[$calc->sk_category->sk_title])){
                    $transport_type = $categories_types[$calc->sk_category->sk_title];
                }

            }else{
                $this->errors[] = 'Не синхронизирован справочник: Категория';
            }

            $power = (int) $auto->power;
            $docType = $auto->doc_type ? 31 : 30;
            $now = date('Y-m-d\TH:i:s', strtotime(Carbon::now()));

            $vehicle = <<<XML
                <ns1:TSRequest>
                    <TSRequestValue>
                        <TSInfoRequest>
                            <CountryCar>1</CountryCar>
                            <CarIdent>
                                <LicensePlate>$auto->reg_number</LicensePlate>
                                <VIN>$auto->vin</VIN>
                            </CarIdent>
                            <Mark>$brandName</Mark>
                            <Model>$modelName</Model>
                            <YearIssue>$auto->car_year</YearIssue>
                            <DocumentCar>$docType</DocumentCar>
                            <DocCarSerial>$auto->docserie</DocCarSerial>
                            <DocCarNumber>$auto->docnumber</DocCarNumber>
                            <DocumentCarDate>$auto->docdate</DocumentCarDate>
                            <EngCap>$power</EngCap>
                        </TSInfoRequest>
                        <DateRequest>$now</DateRequest>
                    </TSRequestValue>
                </ns1:TSRequest>
XML;

        }else{
            $this->errors[] = 'Авто не найдено';
        }


        return $vehicle;
    }

    /* ************************************************************
     * COLLECT PARAM METHODS
     **************************************************************/

    public function getSubjectForCheck(ContractsCalculation $calc, $subj_type)
    {

        $contract = $calc->contract;
        $contract_subject = $contract->{$subj_type};

        if($contract_subject->type == 0){

            $contract_subject_info = $contract_subject->get_info();
            $fio = explode(' ', $contract_subject_info->fio);
            $last_name = $fio[0];
            $first_name = $fio[1];
            $middle_name = $fio[2];
            $birthDate = date('Y-m-d', strtotime($contract_subject_info->birthdate));
            $seria = $contract_subject_info->doc_serie;
            $number = $contract_subject_info->doc_number;
            $now = date('Y-m-d\TH:i:s', strtotime(Carbon::now()));

            $subjectCall = $subj_type == 'insurer' ? 'Insurer' : 'Owner';

            $subjectParams = <<<XML
<ns1:InsurerOwnerRequest>
			<InsurerOwnerRequestValue>
				<CheckedPerson>$subjectCall</CheckedPerson>
				<PhysicalPersonInfoRequest>
					<Country>643</Country>
<!--					<Zip>309201</Zip>-->
					<State>$contract_subject_info->address_register_region</State>
					<City>$contract_subject_info->address_register_city</City>
					<Surname>$last_name</Surname>
					<Name>$first_name</Name>
					<Patronymic>$middle_name</Patronymic>
					<BirthDate>$birthDate</BirthDate>
					<PersonDocument>
						<DocPerson>12</DocPerson>
						<Serial>$seria</Serial>
						<Number>$number</Number>
					</PersonDocument>
				</PhysicalPersonInfoRequest>
				<DateRequest>$now</DateRequest>
			</InsurerOwnerRequestValue>
		</ns1:InsurerOwnerRequest>
XML;

        }else{ // юрик

            $info = $contract_subject->get_info();
            $phone = isset($contract_subject->phone) ? setPhoneNumberFormat(parsePhoneNumber($contract_subject->phone), 'dddddddddd') : "";
            $mail = isset($contract_subject->email) ? $contract_subject->email : '';


            $subjectParams = <<<XML

XML;
        }

        return $subjectParams;
    }

    public function getCalcParams(ContractsCalculation $calc, $upid){

        $contract = $calc->contract;
        $begin_date = date('Y-m-d\T00:00:00', strtotime($contract->begin_date));
        $owner_info = $contract->owner->get_info();
        $use_region = $owner_info->address_register_region;
        $use_city = $owner_info->address_register_city;
        $multidrive = $contract->drivers_type_id ?  'false' : 'true';
        $transport = $this->get_calc_transport($calc);
        $drivers = $this->get_calc_drivers($calc);
        $owner = $this->get_calc_owner($calc);

        $result = <<<XML
    <ns1:CalcEOsago>
        <ns1:PartnerInfo>
            <ns1:UPID>$upid</ns1:UPID>
        </ns1:PartnerInfo>		
        <ns1:ContractType>Первоначальный</ns1:ContractType>
        <ns1:PolicyBeginDate>$begin_date</ns1:PolicyBeginDate>
        <ns1:InsuranceMonth>12</ns1:InsuranceMonth>
        <ns1:RegionOfUse>$use_region</ns1:RegionOfUse>
        <ns1:CityOfUse>$use_city</ns1:CityOfUse>
        <ns1:DriversRestriction>$multidrive</ns1:DriversRestriction>
        <ns1:FollowToRegistration>false</ns1:FollowToRegistration>
        $transport
        $drivers
        $owner
        <ns1:UnwantedClient>false</ns1:UnwantedClient>
    </ns1:CalcEOsago>
XML;


        return $result;
    }

    public function getReleaseParams(ContractsCalculation $calc){

        $contract = $calc->contract;
        $calcJson = \GuzzleHttp\json_decode($calc->json);
        $auto = $calc->object_insurer_auto;

        $insurer = $this->getReleaseSubject($calc, 'insurer');
        $owner = $this->getReleaseSubject($calc, 'owner');
        $agent = $this->getAgent($calc);
        $drivers = $this->getReleaseDrivers($calc);
        $car = $this->getReleaseCar($calc);
        $has_trailer = $auto->is_trailer == 1 ? '1' : '0';
        $now = date('Y-m-d\TH:i:s', strtotime(Carbon::now()));
        $beginDate = date('Y-m-d\TH:i:s', strtotime($contract->begin_date));
        $endDate = date('Y-m-d\TH:i:s', strtotime($contract->end_date));
        $multidrive = ($contract->drivers_type_id == 0) ? 'true' : 'false';

        $result = <<<XML
        <ns1:LoadContractEosago>
			<ns1:PartnerInfo>
				<ns1:UPID>$calcJson->UPID</ns1:UPID>
			</ns1:PartnerInfo>
            <ns1:LoadContractRequest>
                <ns1:CalcRef>$calcJson->RefCalcId</ns1:CalcRef>
                <ns1:DateRevision>$now</ns1:DateRevision>
                <ns1:DateActionBeg>$beginDate</ns1:DateActionBeg>
                <ns1:DateActionEnd>$endDate</ns1:DateActionEnd>
                <ns1:DriversRestriction>$multidrive</ns1:DriversRestriction>
                <ns1:IsTransCar>false</ns1:IsTransCar>
                <ns1:IsTrailerAllowed>$has_trailer</ns1:IsTrailerAllowed>
                $car
                <ns1:Insurer>
                    <ns1:PhysicalPerson>
                        $insurer
                    </ns1:PhysicalPerson>
                </ns1:Insurer>
                <ns1:Owner>
                    <ns1:PhysicalPerson>
                        $owner
                    </ns1:PhysicalPerson>
                </ns1:Owner>
                $drivers
                $agent
            </ns1:LoadContractRequest>
        </ns1:LoadContractEosago>
XML;

//        dd($result);
        return $result;
    }

    public function get_calc_transport(ContractsCalculation $calc){

        if($auto = $calc->object_insurer_auto){

            if($mark = $calc->sk_mark){
                $brandName = $mark->sk_title;
            }else{
                $brandName = '';
                $this->errors[] = 'Не синхронизирован справочник: Марки';
            }

            if($model = $calc->sk_model){
                $modelName = $model->sk_title;
            }else{
                $modelName = '';
                $this->errors[] = 'Не синхронизирован справочник: Модели';
            }

            if($calc->sk_category){

                // категория авто
                $transport_category = $calc->sk_category->sk_title;

                // тип авто
                $categories_types = [
                    'A - мотоциклы' => 'Мотоциклы и мотороллеры',
                    'B - легковые'  => 'Легковой а/м',
                    'C - грузовые'  => 'Грузовой а/м',
                    'D - автобусы'  => 'Автобусы',
                ];

                if(isset($categories_types[$calc->sk_category->sk_title])){
                    $transport_type = $categories_types[$calc->sk_category->sk_title];
                }

            }else{
                $this->errors[] = 'Не синхронизирован справочник: Категория';
            }

            $has_trailer = $auto->is_trailer == 1 ? 'true' : 'false';

            $transport = <<<XML
        <ns1:Transport>
            <ns1:Mark>$brandName</ns1:Mark>
            <ns1:Model>$modelName</ns1:Model>
            <ns1:Category>$transport_category</ns1:Category>
            <ns1:Type>$transport_type</ns1:Type>
            <ns1:RegistrationRussia>true</ns1:RegistrationRussia>
            <ns1:Power>$auto->power</ns1:Power>
            <ns1:Purpose>Личные</ns1:Purpose>
            <ns1:LicensePlate>$auto->reg_number</ns1:LicensePlate>
            <ns1:VIN>$auto->vin</ns1:VIN>
            <ns1:WithUseTrailer>$has_trailer</ns1:WithUseTrailer>
            <ns1:YearIssue>$auto->car_year</ns1:YearIssue>
        </ns1:Transport>
XML;

        }else{
            $this->errors[] = 'Авто не найдено';
        }


        return $transport;
    }

    public function get_calc_drivers(ContractsCalculation $calc){

        if($calc->drivers){

            $drivers = '';

            foreach ($calc->drivers as $driver){

                $fio = $driver->get_fio();
                $last_name = $fio['last_name'];
                $first_name = $fio['first_name'];
                $middle_name = $fio['middle_name'];
                $birth_date = date('Y-m-d', strtotime($driver->birth_date));
                $exp_date = date('Y-m-d', strtotime($driver->exp_date));

                $drivers .= <<<XML
            <ns1:Driver>
                <ns1:LastName>$last_name</ns1:LastName>
                <ns1:FirstName>$first_name</ns1:FirstName>
                <ns1:MiddleName>$middle_name</ns1:MiddleName>
                <ns1:BirthDate>$birth_date</ns1:BirthDate>
                <ns1:ExperienceDate>$exp_date</ns1:ExperienceDate>
                <ns1:DriverDocument>
                    <ns1:Number>$driver->doc_num</ns1:Number>
                    <ns1:Seria>$driver->doc_serie</ns1:Seria>
                </ns1:DriverDocument>
            </ns1:Driver>
XML;
            }

            $drivers = <<<XML
        <ns1:Drivers>
            $drivers
        </ns1:Drivers>
XML;

        }else{
            $this->errors[] = 'Водител(ь/и) не найден(ы)';
        }

        return $drivers;
    }

    public function get_calc_owner(ContractsCalculation $calc){

        $contract = $calc->contract;
        $contract_subject = $contract->owner;

        if($contract_subject->type == 0){

            $contract_subject_info = $contract_subject->get_info();
            $fio = explode(' ', $contract_subject_info->fio);
            $last_name = $fio[0];
            $first_name = $fio[1];
            $middle_name = $fio[2];
            $birthDate = date('Y-m-d', strtotime($contract_subject_info->birthdate));
            $seria = $contract_subject_info->doc_serie;
            $number = $contract_subject_info->doc_number;


            $owner = <<<XML
        <ns1:TransportOwner>
            <ns1:LastName>$last_name</ns1:LastName>
            <ns1:FirstName>$first_name</ns1:FirstName>
            <ns1:MiddleName>$middle_name</ns1:MiddleName>
            <ns1:BirthDate>$birthDate</ns1:BirthDate>
            <ns1:PersonDocument>
                <ns1:Number>$number</ns1:Number>
                <ns1:Seria>$seria</ns1:Seria>
            </ns1:PersonDocument>
        </ns1:TransportOwner>
XML;


        }else{ // юрик

            $info = $contract_subject->get_info();
            $phone = isset($contract_subject->phone) ? setPhoneNumberFormat(parsePhoneNumber($contract_subject->phone), 'dddddddddd') : "";
            $mail = isset($contract_subject->email) ? $contract_subject->email : '';


            $owner = <<<XML
        <ns1:TransportOwner>

        </ns1:TransportOwner>
XML;

        }


        return $owner;
    }

    public function getReleaseSubject(ContractsCalculation $calc, $subj_type){

        $contract = $calc->contract;
        $contract_subject = $contract->{$subj_type};
        $calcJson = \GuzzleHttp\json_decode($calc->json);

        if($contract_subject->type == 0){

            $contract_subject_info = $contract_subject->get_info();
            $fio = explode(' ', $contract_subject_info->fio);
            $last_name = $fio[0];
            $first_name = $fio[1];
            $middle_name = $fio[2];
            $birthDate = date('Y-m-d', strtotime($contract_subject_info->birthdate));
            $seria = $contract_subject_info->doc_serie;
            $number = $contract_subject_info->doc_number;
            $idCheckInsurerOwner = $calcJson->IDCheckInsurerOwner->{$subj_type};
            $sex = $contract_subject_info->sex ? 'Female' : 'Male';

            $owner = <<<XML
<ns1:IDCheckInsurerOwner>$idCheckInsurerOwner</ns1:IDCheckInsurerOwner>
<ns1:Surname>$last_name</ns1:Surname>
<ns1:Name>$first_name</ns1:Name>
<ns1:Patronymic>$middle_name</ns1:Patronymic>
<ns1:BirthDate>$birthDate</ns1:BirthDate>
<ns1:Sex>$sex</ns1:Sex>
<ns1:Phone>+7(111)1112233</ns1:Phone>
<ns1:Email>drachevdv@alfastrah.ru</ns1:Email>
<ns1:PersonDocument>
    <ns1:DocPerson>12</ns1:DocPerson>
    <ns1:Serial>$seria</ns1:Serial>
    <ns1:Number>$number</ns1:Number>
</ns1:PersonDocument>
<ns1:CountryCode>643</ns1:CountryCode>
<ns1:CountryName>Россия</ns1:CountryName>
<!--<ns1:Zip>400120</ns1:Zip>-->
<ns1:State>$contract_subject_info->address_register_region</ns1:State>
<ns1:City>$contract_subject_info->address_register_city</ns1:City>
<ns1:Street>$contract_subject_info->address_register_street</ns1:Street>
<ns1:House>$contract_subject_info->address_register_house</ns1:House>
<ns1:Apartment>$contract_subject_info->address_register_flat</ns1:Apartment>
XML;


        }else{ // юрик

            $info = $contract_subject->get_info();
            $phone = isset($contract_subject->phone) ? setPhoneNumberFormat(parsePhoneNumber($contract_subject->phone), 'dddddddddd') : "";
            $mail = isset($contract_subject->email) ? $contract_subject->email : '';


            $owner = <<<XML

XML;
        }

        return $owner;
    }


    public function getAgent(ContractsCalculation $calc){
        $contract = $calc->contract;

        if($agent_info = $contract->getAgentSettings($calc)){

            $result = <<<XML
<ns1:AgentBlock>
    <ns1:AgentContractId>$agent_info->agent_id</ns1:AgentContractId>
    <ns1:ManagerId>$agent_info->manager_id</ns1:ManagerId>
    <ns1:DepartmentId>$agent_info->department_id</ns1:DepartmentId>
    <ns1:ChannelSaleId>$agent_info->сhannel_sale_id</ns1:ChannelSaleId>
</ns1:AgentBlock>
XML;
        }

        return $result;
    }


    public function getReleaseDrivers(ContractsCalculation $calc){

        $calcJson = \GuzzleHttp\json_decode($calc->json);

        if($calc->drivers){

            $drivers = '';

            foreach ($calc->drivers as $driver){

                $fio = $driver->get_fio();
                $last_name = $fio['last_name'];
                $first_name = $fio['first_name'];
                $middle_name = $fio['middle_name'];
                $birth_date = date('Y-m-d', strtotime($driver->birth_date));
                $exp_date = date('Y-m-d', strtotime($driver->exp_date));
                $sex = $driver->sex ? 'Female' : 'Male';
                $idCheckDriver = $calcJson->IDCheckDriver->{$driver->id};

                $drivers .= <<<XML
                    <ns1:Driver>
                        <ns1:IDCheckDriver>$idCheckDriver</ns1:IDCheckDriver>
                        <ns1:CountryCode>643</ns1:CountryCode>
                        <ns1:CountryName>Россия</ns1:CountryName>
                        <ns1:DriverDocument>
                            <ns1:Serial>$driver->doc_serie</ns1:Serial>
                            <ns1:Number>$driver->doc_num</ns1:Number>
                            <ns1:DateIssue>$driver->doc_date</ns1:DateIssue>
                        </ns1:DriverDocument>
                        <ns1:CategoriesDriverLicense>
                            <ns1:CatDriverLicense>2531</ns1:CatDriverLicense>
                        </ns1:CategoriesDriverLicense>
                        <ns1:DriverDocDate>$exp_date</ns1:DriverDocDate>
                        <ns1:Surname>$last_name</ns1:Surname>
                        <ns1:Name>$first_name</ns1:Name>
                        <ns1:Patronymic>$middle_name</ns1:Patronymic>
                        <ns1:BirthDate>$birth_date</ns1:BirthDate>
                        <ns1:Sex>$sex</ns1:Sex>
                    </ns1:Driver>
XML;
            }

            $drivers = <<<XML
                <ns1:Drivers>
                    $drivers
                </ns1:Drivers>
XML;

        }else{
            $this->errors[] = 'Водител(ь/и) не найден(ы)';
        }

        return $drivers;
    }

    public function getReleaseCar(ContractsCalculation $calc){

        if($auto = $calc->object_insurer_auto){

            if($mark = $calc->sk_mark){
                $brandName = $mark->sk_title;
            }else{
                $this->errors[] = 'Не синхронизирован справочник: Марки';
            }

            if($model = $calc->sk_model){
                $modelName = $model->sk_title;
            }else{
                $this->errors[] = 'Не синхронизирован справочник: Модели';
            }

            if($calc->sk_category){

                // категория авто
                $transport_category = $calc->sk_category->sk_title;

                // тип авто
                $categories_types = [
                    'A - мотоциклы' => 'Мотоциклы и мотороллеры',
                    'B - легковые'  => 'Легковой а/м',
                    'C - грузовые'  => 'Грузовой а/м',
                    'D - автобусы'  => 'Автобусы',
                ];

                if(isset($categories_types[$calc->sk_category->sk_title])){
                    $transport_type = $categories_types[$calc->sk_category->sk_title];
                }

            }else{
                $this->errors[] = 'Не синхронизирован справочник: Категория';
            }

            $power = (int) $auto->power;
            $dk_date = date('Y-m-d', strtotime($auto->dk_date));
            $dkYear = date('Y', strtotime($auto->dk_date));
            $dkMonth = date('m', strtotime($auto->dk_date));
            $calcJson = \GuzzleHttp\json_decode($calc->json);
            $docType = $auto->doc_type ? 31 : 30;

            $transport = <<<XML
                <ns1:Car>
                    <ns1:IDCheckTS>$calcJson->IDCheckTS</ns1:IDCheckTS>
                    <ns1:CountryCar>1</ns1:CountryCar>
                    <ns1:CarIdent>
                        <ns1:VIN>$auto->vin</ns1:VIN>
                    </ns1:CarIdent>
                    <ns1:LicensePlate>$auto->reg_number</ns1:LicensePlate>
                    <ns1:Mark>$brandName</ns1:Mark>
                    <ns1:Model>$modelName</ns1:Model>
                    <ns1:YearIssue>$auto->car_year</ns1:YearIssue>
                    <ns1:DocumentCar>$docType</ns1:DocumentCar>
                    <ns1:DocCarSerial>$auto->docserie</ns1:DocCarSerial>
                    <ns1:DocCarNumber>$auto->docnumber</ns1:DocCarNumber>
                    <ns1:DocumentCarDate>$auto->docdate</ns1:DocumentCarDate>
                    <ns1:TicketCar>53</ns1:TicketCar>
<!--                    <ns1:TicketCarSerial></ns1:TicketCarSerial>-->
                    <ns1:TicketCarNumber>$auto->dk_number</ns1:TicketCarNumber>
                    <ns1:TicketCarYear>$dkYear</ns1:TicketCarYear>
                    <ns1:TicketCarMonth>$dkMonth</ns1:TicketCarMonth>
                    <ns1:TicketDiagnosticDate>$dk_date</ns1:TicketDiagnosticDate>
                    <ns1:EngCap>$power</ns1:EngCap>
                    <ns1:Purpose>PERSONAL</ns1:Purpose>
                </ns1:Car>
XML;

        }else{
            $this->errors[] = 'Авто не найдено';
        }


        return $transport;
    }

}
