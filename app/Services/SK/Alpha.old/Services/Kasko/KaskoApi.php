<?php

namespace App\Services\SK\Alpha\Services\Kasko;


use App\Models\Contracts\ContractsCalculation;
use App\Models\Contracts\KbmCalculation;
use App\Models\Vehicle\VehicleModels;
use App\Services\Kbm\Alfa\Calc;
use App\Services\SK\Alpha\Clients\DigestSoapClient;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Mockery\Exception;
use App\Models\Vehicle\VehicleMarks;

class KaskoApi extends ProgramValidation
{


    public $url;
    public $login;
    public $password;
    public $errors = [];


    public function __construct($url, $login, $password)
    {
        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
    }


    public function get_service_client($service_name)
    {

        return new DigestSoapClient([
            'url' => $this->url,
            'service_name' => $service_name,
            'login' => $this->login,
            'password' => $this->password,
        ]);

    }


    public function release_kasko(ContractsCalculation $calc)
    {


        $result = (object)['state' => false, 'answer' => [], 'error' => ''];

        $param = $this->get_release_params($calc);

     //   dd($param);
        $service_client = $this->get_service_client('KaskoSaving')->getClient();
//        dd($service_client->__getFunctions());
//dd($service_client->addContract($param));
        try {
            //dd($service_client->addContract($param));
            $result->answer = $service_client->addContract($param);
//            dump($service_client->__getLastRequest());
            $result->state = true;
        } catch (\SoapFault $fault) {
            $result->error = $fault->getMessage();
        }

        return $result;

    }

    public function get_release_params(ContractsCalculation $calc)
    {

        $contract = $calc->contract;

        $kbm = null;
        if ($contract->drivers_type_id == 0) {
            foreach ($contract->drivers as $driver) {
                if (!isset($kbm)) {
                    $kbm = $driver->class_kbm;
                } else {
                    if ($kbm > $driver->class_kbm) {
                        $kbm = $driver->class_kbm;
                    }
                }
            }
        }

        $ownerTransTypeTitle = '';
        $ownerPolicyTypeTitle = '';
        if ($contract->owner->type == 0) {
            // название параметра
            $ownerTransTypeTitle = 'TransportOwner';
            $ownerPolicyTypeTitle = 'PolicyOwner';

            $city = $calc->contract->insurer->data_fl->address_fact_city;
        } else {
            // название параметра
            $ownerTransTypeTitle = 'TransportOwnerJuridical';
            $ownerPolicyTypeTitle = 'PolicyOwnerJuridical';
        }

        $result = [];
        $result['CalcId'] = $calc->sk_key_id;
        $result['Multidrive'] = ($contract->drivers_type_id == 0) ? 'No' : 'Yes';
        $result['BeginDate'] = date('Y-m-d\TH:i:s', strtotime($contract->begin_date));
        $result['EndDate'] = date('Y-m-d', strtotime($contract->end_date));
        $result['DateSign'] = date('Y-m-d', strtotime(Carbon::now()));

        $result["PolicyKBM"] = $kbm;

       // dd($contract->payment_total);
        if ($contract->is_epolicy != 1) { // Бумажный

        } elseif ($contract->is_epolicy == 1) { // Электронный
            // хз что делаем
        }

        $result["PurposeOfUse"] = $this->get_purpose($calc);
        $result["ContractOptionId"] = "1"; // Первоначальный
        $result["ContractStatusTypeId"] = 5; // 1 - Статус договора - заявление
        $result["WithUseTrailer"] = ($contract->object_insurer_auto->is_trailer == 0) ? "false" : "true";
        $result[$ownerPolicyTypeTitle] = $this->get_insurer_release($calc); // страхователь
        $result[$ownerTransTypeTitle] = $this->get_owner_release($calc); // владелец авто
        $result['Transport'] = $this->release_get_transport($calc);
        $result['AgentBlock'] = $this->get_agent($calc);
        $result['Drivers'] = $this->get_drivers($calc, 'release');
        $result['Period'] = $this->get_period_list($calc, 'release');
        $result['StatementDate']= '2019-11-11T18:55:06';
        $result['PaymentBeginDate']= '2019-11-11T18:55:06';
        $result['Additionals'] = [
            'ProductName' => 'Альфа-Бизнес',
            'PaymentType' => 'Единовременно',
            'ActionRegisterGibdd' => false,
            'SumInsuredRisk' => false,
            'RepairInsurerSTOA' => false,
            'RepairChoiceSTOA' => false,
            'RepairDealerSTOA' => false,
            'PaymentIndependent' => false,
            'SpecProgram' => "Нет",
            'SpecOffer' => false,
            'AddProgram' => "Нет",
            'BeginRiskTheft' => "Нет",
        ];

        $result['Beneficiaries'] = [
            ['Physical' => $this->get_insurer_release($calc)]
        ];



        // Заполнение рисков для вывода отдельно, каждый
     //   {"THEFT":{"Premium":"0.0","Deductible":"0.0","InsSum":"1"},"DAMAGE":{"Premium":"158940.0","Deductible":"0.0","InsSum":"1800000"},"NS":{"Premium":"5250.0","InsSum":"1500000.0"},"DO":{"Premium":"0.0","InsSum":"0.0"},"GO":{"Premium":"0.0","InsSum":"0.0"}}
         $premiums = \GuzzleHttp\json_decode($calc->payments_sizes);
//        dd($premiums);
        $result['InsuranceRisks'] = [
            'Kasko' => [
                'InsuranceAmount' => $premiums->DAMAGE->InsSum,
                'InsurancePremium' => $premiums->DAMAGE->Premium,
                'FullOrPartial' => true,
                'Franchise' => 0.0,
            ],
            'NS' => [
                'InsuranceAmount' => $premiums->NS->InsSum,
                'InsurancePremium' => $premiums->NS->Premium,
                'SystemLimp' => 1,
            ]
        ];


/*        $result['InsuranceRisks'] = [
            'Kasko' => [
                'InsuranceAmount' => $contract->insurance_amount,
                'InsurancePremium' => 942000,
                'FullOrPartial' => true,
                'Franchise' => 0.0,
            ]
        ];*/
//
        $result["ContractOptionId"] = "1"; // Первоначальный
        $result["ContractStatusTypeId"] = 5; // 1 - Статус договора - заявление
        $result["WithUseTrailer"] = ($contract->object_insurer_auto->is_trailer == 0) ? "false" : "true";

        $result['Coefficients'] = [
            "tb" => 0,
            "kt" => 0,
            "kbm" => 0,
            "kvs" => 0,
            "ko" => 0,
            "km" => 0,
            "ks" => 0,
            "kp" => 0,
            "kn" => 0,
            "kpr" => 0,
        ];
        return $result;
    }

    /* ************************************************************
     * API OPERATION METHODS
     **************************************************************/

    public function calc_kasko(ContractsCalculation $calc)
    {

        $result = (object)['state' => false, 'answer' => [], 'error' => ''];

        $service_client = $this->get_service_client('CalcKaskoCommon')->getClient();

        $param = $this->get_calc_params($calc);

        $typedVar = new \SoapVar($param, XSD_ANYXML);//, '', 'rtkv', 'Envelope', 'http://www.ibm.com/rules/decisionservice/RTKVapp/RTKV');

       // dd($service_client);
        try {
            $result = $service_client->CalcKasko($typedVar);

//           $service_client->__doRequest();
//            dd($service_client->__getLastRequest());
//            $response = $service_client->__getLastResponse();
 ;

        } catch (\SoapFault $e) {
             dump($service_client->__getLastRequestHeaders());
            dump($service_client->__getLastRequest());
            dump($service_client->__getLastResponseHeaders());
            dump($service_client->__getLastResponse());
            //  dd($e);
        }


//        $result = $service_client->addContract($typedVar);

     /*   dd($result);*/
        return $result;

    }

    public function get_calc_params(ContractsCalculation $calc)
    {

        $c = request()->contract;

        //dd($c['object']['is_new']);
        $bdate = strtotime($c['begin_date']);
        $edate = strtotime($c['end_date']);
        $c['insurance_month'] = ((date('Y', $edate) - date('Y', $bdate)) * 12) + (date('m', $edate) - date('m', $bdate));
        $c['end_date'] = date('Y-m-d', strtotime($c['end_date']));
        $c['begin_date'] = date('Y-m-d', strtotime($c['begin_date']));

        $c['franchise'] = (int)$c['franchise'];

        $c['insurer']['fio'] = explode(' ', $c['insurer']['fio']);
        $c['insurer']['sex'] = $c['insurer']['sex'] == 1 ? 'female' : 'male';
        $c['insurer']['type'] = $c['insurer']['type'] == 0 ? 'Физическое лицо' : 'Юридическое лицо';
        $c['agreementType'] = $c['is_prolongation'] == '1' ? 'Пролонгированный' : 'Первоначальный';
        //dd($c);

        if (isset($c['owner']['is_insurer']) && $c['owner']['is_insurer'] == 1)
            $c['owner'] = $c['insurer'];
        else {
            $c['owner']['fio'] = explode(' ', $c['owner']['fio']);
            $c['owner']['type'] = $c['owner']['type'] == 0 ? 'Физическая лицо' : 'Юридическое лицо';
            $c['owner']['sex'] = $c['owner']['sex'] == 1 ? 'female' : 'male';
        }

//dd($c['object']);
        $c['object']['brand'] =
            VehicleMarks::where('id', $c['object']['mark_id'])->get()->pluck('title')->first();


        $c['object']['mark'] =
            VehicleModels::where('id', $c['object']['model_id'])->get()->pluck('title')->first();


        $c['ns'] = $c['risks']['passenger_and_driver_accident_insurance'];
        if (isset($c['ns']['status']))
            $c['ns']['status'] = (boolean)$c['ns']['status'];


        $c['doo']['risk'] = isset($c['equipment']);
        $c['doo']['total'] = 0;

        if ($c['doo']['risk'])
            foreach ($c['equipment'] as $d)
                $c['doo']['total'] += $d['insurance_amount'];


        $c['object']['seats_num'] = isset($c['object']['seats_num']) ? $c['object']['seats_num'] : 0;
        $c['date'] = date('Y-m-d', time());

        $c['drivers'] = $this->generateDrivers($c);

        $c['insurer']['birthdate'] = date('Y-m-d', strtotime($c['insurer']['birthdate']));
        $c['owner']['birthdate'] = date('Y-m-d', strtotime($c['owner']['birthdate']));


        $c['risks']['harms']['status'] = (bool)$c['risks']['harms']['status'];
        $c['risks']['voluntary_liability_insurance']['status'] =
            (bool)$c['risks']['voluntary_liability_insurance']['status'];

        $c['date_calc'] = date('Y-m-d', time());

        $cat = $calc->sk_model->vehicle_models_sk_id;
        $request = ['GetKaskoTranportTypeRequest' => ['modelID' => $cat]];
        $soap = $this->get_service_client('WsDictionary')->getClient();
        $soap->__call('getKaskoTransportType', $request);
        $c['object']['ts_type'] = $soap->__getLastResponse();
        $c['franchise_type_id'] = (bool)$c['franchise_type_id'];
       // dd($c);
        $xml = $this->ProgramXML($c);
       //  dd($xml);

        return $xml;
    }

    public function get_period_list(ContractsCalculation $calc, $method = 'calc')
    {
        $contract = $calc->contract;

        if ($method == 'calc') {
            $period_list[] = [
                'StartDate' => date('Y-m-d', strtotime($contract->begin_date)),
                'EndDate' => date('Y-m-d', strtotime($contract->end_date))
            ];
        } elseif ($method == 'release') {
            $period_list[] = [
                'BeginDate' => date('Y-m-d', strtotime($contract->begin_date)),
                'EndDate' => date('Y-m-d', strtotime($contract->end_date))
            ];
        }

        return $period_list;
    }

    public function get_purpose(ContractsCalculation $calc)
    {

        $purpose_title = '';

        if ($purpose = $calc->sk_purpose) {
            $purpose_title = $purpose->sk_title;
        } else {
            $this->errors[] = 'Цель использования не найдена';
        }

        return $purpose_title;
    }

    public function release_get_transport(ContractsCalculation $calc)
    {

        $transport = [];


        if ($calc->object_insurer_auto) {

            $auto = $calc->object_insurer_auto;

            $transport['VIN'] = $auto->vin;
            $transport['Power'] = $auto->power;
            $transport['PowerKwt'] = $auto->power * 0.7355;
            $transport['ReleaseYear'] = $auto->car_year;
            $transport['Tenure'] = '2019-05-20';
            $transport['RiskInsured'] = '100.1';
            $transport['IsNewTS'] = $auto->is_new;
            $transport['newTS'] = $auto->is_new;
            $transport['LicensePlate'] = $auto->reg_number;
            $transport['Passport'] = [
                "Seria" => $auto->docserie,
                "Number" => $auto->docnumber,
                "DateOfIssue" => $auto->docdate
            ];


            if ($cat = $calc->sk_category) {
                // тип авто
                    $categories_types = [
                    'A - мотоциклы' => 'Мотоциклы и мотороллеры',
                    'B - легковые' => 'Легковой а/м',
                    'C - грузовые' => 'Грузовой а/м',
                    'D - автобусы' => 'Автобусы',
                ];

                if (isset($categories_types[$cat->sk_title])) {
                    $transport['TransportType'] = 'АЛИ | Легковой автомобиль иностранный';
                }

            } else {
                $this->errors[] = 'Не синхронизирован справочник: Категория';
            }

            if ($mark = $calc->sk_mark) {
                $transport['BrandId'] = $mark->vehicle_marks_sk_id;
                $transport['BrandName'] = $mark->title;
            } else {
                $this->errors[] = 'Не синхронизирован справочник: Марки';
            }

            if ($model = $calc->sk_model) {
                $transport["ModelId"] = $model->vehicle_models_sk_id;
                $transport["ModelName"] = $model->sk_title;
            } else {
                $this->errors[] = 'Не синхронизирован справочник: Модели';
            }

        } else {
            $this->errors[] = 'Авто не найдено';
        }
//dd($transport);
        return $transport;

    }

    public function get_drivers(ContractsCalculation $calc, $method = 'calc')
    {

        $drivers = [];

        if ($calc->drivers) {
            if ($method == 'calc') { // если для калькуляции

                foreach ($calc->drivers as $driver) {

                    $fio = $driver->get_fio();

                    $drivers[] = [
                        'LastName' => $fio['last_name'],
                        'FirstName' => $fio['first_name'],
                        'MiddleName' => $fio['middle_name'],
                        'BirthDate' => $driver->birth_date,
                        'ExperienceDate' => $driver->exp_date,
                        'DriverDocument' => array(
                            'Number' => $driver->doc_num,
                            'Seria' => $driver->doc_serie,
                        )
                    ];

                }

            } elseif ($method == 'release') { // если для выпуска

                foreach ($calc->drivers as $driver) {

                    $fio = $driver->get_fio();

                    $drivers[] = [
                        'LastName' => $fio['last_name'],
                        'FirstName' => $fio['first_name'],
                        'MiddleName' => $fio['middle_name'],
                        'Sex' => ($driver->sex == 0) ? "Male" : "Female",
                        'BirthDate' => $driver->birth_date,
                        'DriverLicense' => array(
                            'Number' => $driver->doc_num,
                            'Seria' => $driver->doc_serie,
                        ),
                        'IsResident' => "true",
                        'BeginDriveDate' => $driver->exp_date,
                        'DriveBeginDate' => $driver->exp_date,
                        'DriverClass' => $driver->class_kbm
                    ];

                }

                return $drivers;

            }

        } else {
            $this->errors[] = 'Водител(ь/и) не найден(ы)';
        }

        return ['Driver' => $drivers];
    }

    /*
     * юзал для драйверов, ща наверно можно удалить
     */

    public function get_insurer_release(ContractsCalculation $calc, $subj_type = 'insurer')
    {

        $subject = [];

        $contract = $calc->contract;

        if ($contract_subject = $contract->{$subj_type}) {

            // если физик
            if ($contract_subject->type == 0) {

                $contract_owner_info = $contract_subject->get_info();

                $fio = explode(' ', $contract_subject->title);

                $subject['LastName'] = $fio[0];
                $subject['FirstName'] = isset($fio[1]) ? $fio[1] : $fio[0];
                $subject['MiddleName'] = isset($fio[2]) ? $fio[2] : $fio[0];
                $subject['Sex'] = ($contract_owner_info->sex == 0) ? "Male" : "Female";
                $subject['BirthDate'] = $contract_owner_info->birthdate;
                $subject['Passport'] = [
                    "Seria" => $contract_owner_info->doc_serie,
                    "Number" => $contract_owner_info->doc_number,
                    "DateOfIssue" => $contract_owner_info->doc_date
                ];
                $subject["Address"] = [
                    "Country" => "Россия",
                    "City" => isset($contract_owner_info->address_register_city) ? $contract_owner_info->address_register_city : "",
                    "Street" => isset($contract_owner_info->address_register_street) ? $contract_owner_info->address_register_street : "",
                    "HouseNumber" => isset($contract_owner_info->address_register_house) ? $contract_owner_info->address_register_house : "",
                    "ApartmentNumber" => isset($contract_owner_info->address_register_flat) ? $contract_owner_info->address_register_flat : "",
                    "KLADR" => isset($contract_owner_info->address_register_city_kladr_id) ? $contract_owner_info->address_register_city_kladr_id : "",
                ];
                $subject['Phone'] = isset($contract_subject->phone) ? $contract_subject->phone : "";
                $subject['IsResident'] = "true";

            } else { // юрик

                $contract_owner_info = $contract_subject->get_info();

                $subject = [
                    'FullName' => $contract_owner_info->title,
                    'ShortName' => $contract_owner_info->title,
                    'INN' => $contract_owner_info->inn,
                    'KPP' => $contract_owner_info->kpp,
                    'Address' => [
                        "Country" => "Россия",
                        "Region" => $contract_owner_info->address_register_city,
                        "City" => $contract_owner_info->address_register_city,
                        "Street" => $contract_owner_info->address_register_street,
                        "HouseNumber" => $contract_owner_info->address_register_house,
                    ],
                    'Phone' => isset($contract_subject->phone) ? $contract_subject->phone : "",
                    'IsResident' => "true",
                ];

            }
        }

        return $subject;
    }

    public function get_agent(ContractsCalculation $calc)
    {


        $contract = $calc->contract;

        if ($agent_info = $contract->getAgentSettings($calc)) {

            $agent = [
                "ChannelSaleCode" => $agent_info->сhannel_sale_id,
                "AgentContractNumber" => $agent_info->agent_name,
                "AgentName" => $agent_info->contract_name,
                "DepartmentId" => $agent_info->department_id,
                "SignerSubjectId" => $agent_info->signer_id,
                "ManagerSubjectId" => $agent_info->manager_id
            ];

        } else {
            return 'Агент не найден';
        }

        return $agent;
    }

    public function get_owner_release(ContractsCalculation $calc)
    {

        $contract = $calc->contract;

        // владелец это страхователь
        if ($contract->owner->id == $contract->insurer->id) {
            $subject = $this->get_insurer_release($calc);
        } else {
            $subject = $this->get_insurer_release($calc, 'owner');
        }

        return $subject;
    }

    public function get_forms_osago(ContractsCalculation $calc, $form_id){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $param = $this->get_form_params($calc, $form_id);

        $service_client = $this->get_service_client('PrintedForms')->getClient();

        try{

            $result->form = $service_client->GetPrintedFormByContractId($param);
            $result->state = true;

        }catch (\SoapFault $fault){
            $result->error = 'Не удалось получить формы для печати';
        }

        return $result;

    }

    public function get_form_params(ContractsCalculation $calc, $form_id){

        $params = [
            'ContractId' => $calc->sk_key_id, //$calc->sk_key_id
            'PrintedFormId' => $form_id
        ];

        return $params;
    }

}