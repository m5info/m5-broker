<?php

namespace App\Services\SK\Alpha\Services\Dictionary;


use SoapClient;
use SoapHeader;
use SoapVar;

class DictionaryApi{


    public $url;
    public $login;
    public $password;

    public $Send = null;

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

        $this->soapClient          = new SoapClient( $this->url.'WsDictionary?wsdl', ['trace' => 1, 'exception' => 0]);
        $timestamp                 = gmdate('Y-m-d\TH:i:s\Z', time());
        $nonce                     = mt_rand();
        $nonceBase64               = base64_encode(pack('H*', $nonce));
        $passDigest                = base64_encode(pack('H*', sha1(pack('H*', $nonce) . pack('a*', $timestamp) . pack('a*', $this->password))));
        $auth                      = <<<XML
        <wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
            <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="UsernameToken-3">
                <wsse:Username>{$this->login}</wsse:Username>
                <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">{$passDigest}</wsse:Password>
                <wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">{$nonceBase64}</wsse:Nonce>
                <wsu:Created xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">{$timestamp}</wsu:Created>
            </wsse:UsernameToken>
        </wsse:Security>
XML;
        $authvalues                = new SoapVar($auth, XSD_ANYXML);
        $objHeader_Session_Outside = new SoapHeader("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", $authvalues, true);

        $this->soapClient->__setSoapHeaders(array($objHeader_Session_Outside));

    }


    public function get_purpose(){

       /* $request = [
            'getDictionary'=> [
                'Code' => 'AS_SPEC_PROG'
            ]

        ];

        dd($this->soapClient->__call('getDictionary', $request));;*/
      return $this->soapClient->__call('getTransportUse', []);
    }

    public function get_categories(){
        //Дергаем справочник Категорий
        //Если у СК это не реализовано возвращаем константу
        $categories = [
            ['id'=>'A - мотоциклы', 'title'=>'A - мотоциклы'],
            ['id'=>'B - легковые', 'title'=>'B - легковые'],
            ['id'=>'C - грузовые', 'title'=>'C - грузовые'],
            ['id'=>'D - автобусы', 'title'=>'D - автобусы'],
            ['id'=>'E - тракторы', 'title'=>'E - тракторы'],
            ['id'=>'Прицеп', 'title'=>'Прицеп'],
        ];

        return $categories;
    }

    public function get_marks(){

        return $this->soapClient->__call('getTransportMarka', []);
    }


    public function get_models(){

        return $this->soapClient->__call('getTransportModel', []);

    }



}