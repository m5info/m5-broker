<?php

namespace App\Services\SK\Alpha\Services\Dictionary;

use App\Interfaces\Services\SK\DictionaryServiceInterface;

class DictionaryService implements DictionaryServiceInterface {

    public $cache_json = __DIR__."/json.json";

    public $api;

    public function __construct(DictionaryApi $api){
        $this->api = $api;
    }

    public function get_api_purpose(){

        $response = [];
        $purposes = $this->api->get_purpose();
        if(isset($purposes->Item)){
            foreach ($purposes->Item as $row){
                $response[] = [
                    'id'=>(string)$row->DictID,
                    'title'=>(string)$row->DictText
                ];
            }
        }

        return $response;
    }




    public function get_api_categories(){

        return $this->api->get_categories();
    }




    public function get_marks_models(){

        $marks = [];
        $models = [];


        if(!is_file($this->cache_json)) {

            $_marks = $this->api->get_marks();
            $_models = $this->api->get_models();

            if(isset($_marks->Item)){

                foreach ($_marks->Item as $row){

                    $marks[] = [
                        'id' => (string) $row->DictID,
                        'title' => (string) $row->DictText,
                        'vehicle_categorie_sk_id' => (string) 'B - легковые',
                    ];

                }
            }



            if(isset($_models->Item)){

                foreach ($_models->Item as $row){

                    $models[] = [
                        'id' => (string) $row->ModelID,
                        'title' => (string) $row->ModelText,
                        'vehicle_mark_sk_id' => (string) $row->MarkaID,
                        'vehicle_categorie_sk_id' => (string)'B - легковые',
                    ];

                }

            }


            $result = [
                'categories' => $this->api->get_categories(),
                'marks' => $marks,
                'models' => $models,
            ];

            file_put_contents($this->cache_json, \GuzzleHttp\json_encode($result));

            return $result;

        }else{

            return \GuzzleHttp\json_decode(file_get_contents($this->cache_json),true);
        }
    }
}