<?php

namespace App\Services\SK\RESO;


use App\Services\SK\BaseServiceController;
use App\Services\SK\RESO\Services\Dictionary\DictionaryApi;
use App\Services\SK\RESO\Services\Dictionary\DictionaryService;
use App\Services\SK\RESO\Services\Osago\OsagoApi;
use App\Services\SK\RESO\Services\Osago\OsagoService;

class ServiceController extends BaseServiceController {


    const SERVICES = [

        'dictionary' => [
            'service' => DictionaryService::class,  /// Можно проставить для удобства конечно им префиксы типа "AlphaDictionaryService::class"
            'api' => DictionaryApi::class,          /// но наверное необязательно
        ],
        'osago' => [
            'service' => OsagoService::class,
            'api' => OsagoApi::class,
        ],

    ];



}