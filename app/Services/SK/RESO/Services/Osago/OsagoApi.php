<?php

namespace App\Services\SK\RESO\Services\Osago;


use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Services\SK\RESO\Services\SendAPI\Send;
use Carbon\Carbon;

class OsagoApi{


    public $url;
    public $login;
    public $password;
    public $Send = null;
    public $SessionId = null;
    public $errors = [];

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->Send = new Send($url, $login, $password, $api_setting);

    }


    public function calc_osago($calc)
    {
        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $xml = $this->get_calc_params($calc);

        $response_calc = $this->Send->send($xml, 'Calculate');

        if(isset($response_calc->answer->return->AutoCalcResult) && empty($response_calc->answer->return->AutoCalcResult->Error)){

            $answer = $response_calc->answer->return->AutoCalcResult;

            $calc->sk_key_id = (string)$answer->CalcID;
            $calc->save();

            $xml = $this->get_save_policy_params($calc);

            $response_save = $this->Send->send($xml, 'savePolicy');

//            $response_release = $this->release_osago($calc);
//            dd(1);
            $result->answer = $response_save->answer->return->Message_;

            if($response_save->answer->return->Step != 0){
                $result->answer = $response_save->answer->return;
                $result->state = true;
            }
        }else{
            $result->answer = $response_calc->answer->return->AutoCalcResult->Error;
        }

        if(!empty($this->errors)){ // ошибки со справочниками
            $result->answer = implode(';', $this->errors);
        }

        return $result;

    }

    public function release_osago($calc){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $xml = $this->get_issue_params($calc);

        try{
            $response_issue = $this->Send->send($xml, 'issuePolicy');

            if($response_issue->answer->return->Step == 4){
                $result->answer = $response_issue->answer->return;
                $result->state = true;
            }else{
                $result->error = $response_issue->answer->return->Message_;
            }

        }catch (\SoapFault $fault){
            $result->error = $fault->getMessage();
        }

        return $result;
    }


    public function get_forms_osago(ContractsCalculation $calc, $form_id){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $xml = $this->get_form_params($calc, $form_id);
        try{

            $response_issue = $this->Send->send($xml, 'printDoc');
            $result->answer = $response_issue->answer->return;
            $result->state = true;

        }catch (\SoapFault $fault){
            $result->error = 'Не удалось получить формы для печати';
        }

        return $result;

    }

    public function get_calc_params($calc){

        $contract = $calc->contract;
        $insurant = $this->get_subject($calc, 'insurer');
        $owner = $this->get_subject($calc, 'owner');
        $drivers = $this->get_drivers($calc);
        $agent = $this->get_agent_info($calc);
        $auto = $this->get_object_insurer_auto($calc);
        $fromDate = date('Y-m-d\T00:00:00', strtotime($contract->begin_date));
        $toDate = date('Y-m-d\T23:59:59', strtotime($contract->end_date));
        $now = Carbon::now()->format('Y-m-d\TH:i:s');

        $xml = <<<XML
<ns3:Calculate xmlns:ns2="http://autostrahovka.reso.ru/Schemas" xmlns:ns3="http://wsauto.webservice.reso.ru/"> 
    <parameter> 
        $agent 
        <FromDate>$fromDate+03:00</FromDate>
        <PDate>$now+03:00</PDate>
        <ToDate>$toDate+03:00</ToDate> 
        $insurant
        $owner
        $drivers
        $auto 
        <NotApplySpecProgDiscount>true</NotApplySpecProgDiscount>
        <Osago>true</Osago> 
    </parameter> 
</ns3:Calculate>
XML;

        return $xml;
    }

    public function get_save_policy_params($calc){

        $policy = $this->get_policy_for_save($calc);
        $insurer = $this->get_subject_for_save($calc, 'insurer');
        $owner = $this->get_subject_for_save($calc, 'owner');
        $drivers = $this->get_drivers_for_save($calc);
        $auto = $this->get_object_for_save($calc);

        $xml = <<<XML
<ns1:savePolicy>
    <parameter>
        $policy
        <PHOLDER>
            $insurer
        </PHOLDER>
        <OWNERS>
            <OWNERType>
                $owner
            </OWNERType>
        </OWNERS>
        <DRIVERS>
            $drivers
        </DRIVERS>
        <OBJECT_>
            $auto
        </OBJECT_>
    </parameter>
</ns1:savePolicy>
XML;

        return $xml;
    }

    public function get_issue_params($calc){

        $xml = <<<XML
<ns1:issuePolicy>
      <parameter>
        <CalcId>$calc->sk_key_id</CalcId>
        <AgentID>666666</AgentID>
        <UserJuristical>false</UserJuristical>
        <UserIsSubagent>false</UserIsSubagent>
        <b2c>true</b2c>
        <UserName>Тестов Тест Тестович</UserName>
      </parameter>
</ns1:issuePolicy>
XML;

        return $xml;
    }


    public function get_form_params($calc, $form_id){

        $xml = <<<XML
<ns2:printDoc xmlns:ns2="http://wsauto.webservice.reso.ru/" xmlns:ns1="http://autostrahovka.reso.ru/Schemas">
  <parameter>
    <ns1:CalcId>$calc->sk_key_id</ns1:CalcId>
    <ns1:PrintDocCode>$form_id</ns1:PrintDocCode>
  </parameter>
</ns2:printDoc>
XML;


        return $xml;
    }

    public function get_subject($calc, $subject_type){

        $subject = [];

        $contract = $calc->contract;

        if($contract_subject = $contract->{$subject_type}){

            if($subject_type == 'insurer'){
                $tag_name = 'Insurant';
            }elseif($subject_type == 'owner'){
                $tag_name = 'CarOwner';
            }

            // если физик
            if($contract_subject->type == 0){

                $contract_subject_info = $contract_subject->get_info();

                $birthDate = date('Y-m-d\TH:i:s', strtotime($contract_subject_info->birthdate));
                $seria = $contract_subject_info->doc_serie;
                $number = $contract_subject_info->doc_number;
                $phone = isset($contract_subject->phone) ? $contract_subject->phone : "";

                $subject = <<<XML
<{$tag_name}ID>0</{$tag_name}ID> 
<{$tag_name}Type>1</{$tag_name}Type> 
<{$tag_name}FullName>$contract_subject->title</{$tag_name}FullName> 
<{$tag_name}BirthDate>$birthDate+04:00</{$tag_name}BirthDate> 
<{$tag_name}DocType>35</{$tag_name}DocType> 
<{$tag_name}DocSeria>$seria</{$tag_name}DocSeria> 
<{$tag_name}DocNumber>$number</{$tag_name}DocNumber>
<{$tag_name}AddrKLADR>$contract_subject_info->address_fact_kladr</{$tag_name}AddrKLADR>
XML;

            }else{ // юрик

                $info = $contract_subject->get_info();

                $subject = <<<XML
<{$tag_name}ID>0</{$tag_name}ID> 
XML;

            }
        }

        return $subject;

    }

    public function get_drivers($calc){

        if($calc->drivers){

            $drivers = '';

            foreach ($calc->drivers as $driver){

                $fio = implode(' ', $driver->get_fio());
                $birth_date = date('Y-m-d\TH:i:s', strtotime($driver->birth_date));
                $sex = ($driver->sex == 1) ? 'false' : 'true' ;

                if($driver->exp_date != "0000-00-00"){
                    $stage = (new \DateTime(Carbon::now()->format('d-m-Y')))->diff(new \DateTime($driver->exp_date));
                    $exp_date = $stage->y;
                }else{
                    $exp_date = '';
                }

                $drivers .= <<<XML
        <Driver CascoReqNum="0" Sex="$sex" BirthDate="$birth_date+04:00" Experience="$exp_date" FullName="$fio" LicenseSeria="$driver->doc_serie" LicenseNumber="$driver->doc_num"> 
        </Driver>
XML;
            }

            $limit = $calc->contract->drivers_type_id == 1 ? 'UNLIMIT' : 'LIMIT';

            $drivers = <<<XML
            <DriverList Kind="$limit"> 
                <Drivers>
                    $drivers
                </Drivers>
            </DriverList>
XML;


        }else{
            $this->errors[] = 'Водител(ь/и) не найден(ы)';
        }

        return $drivers;
    }

    public function get_object_insurer_auto($calc){

        if($calc->object_insurer_auto){

            $auto = $calc->object_insurer_auto;

            $contract = $calc->contract;

            $releaseDate = date('Y', strtotime($auto->sts_docdate));

            if($category = $calc->sk_category){
                $categoryName = $category->vehicle_categorie_sk_id;
            }else{
                $categoryName = '';
                $this->errors[] = 'Не синхронизирован справочник: Категория';
            }

            if($mark = $calc->sk_mark){
                $brandName = $mark->sk_title;
            }else{
                $brandName = '';
                $this->errors[] = 'Не синхронизирован справочник: Марки';
            }

            if($model = $calc->sk_model){
                $modelName = $model->sk_title;
            }else{
                $modelName = '';
                $this->errors[] = 'Не синхронизирован справочник: Модели';
            }

            if($purpose = $calc->sk_purpose){
                $purpose_id = $purpose->vehicle_purpose_sk_id;
            }else{
                $purpose_id = '';
                $this->errors[] = 'Не синхронизирован справочник: Модели';
            }
            $power = (int)$auto->power;

            $owner_info = $contract->owner->get_info();
            $has_trailer = $auto->is_trailer == 1 ? 'true' : 'false';

            $use_kladr = $owner_info->address_fact_kladr;

            $transport = <<<XML
        <CarBrandName>$brandName</CarBrandName>
        <CarModelName>$modelName</CarModelName>
        <CarCategory>$categoryName</CarCategory>
        <PeriodUse>10</PeriodUse>
        <CarUseRegionKLADR>$use_kladr</CarUseRegionKLADR>
        <CarVIN>$auto->vin</CarVIN>
        <CarYear>$auto->car_year</CarYear>
        <PowerAuto>$power</PowerAuto>
        <CarUsedTrailer>$has_trailer</CarUsedTrailer>
XML;


        }else{
            $this->errors[] = 'Авто не найдено';
        }

        return $transport;
    }

    public function get_agent_info(ContractsCalculation $calc){

        $contract = $calc->contract;
        $company = 'TEST'; // ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ «ЛОГЧЭНДЖ»
        $agent_id = '666666'; // 34809359
        $manager_fio = 'Тестов Тест Тестович';
        $manager_office = 'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ «ЛОГЧЭНДЖ»';
        $manager_phone = '8(985) 305-60-51';

        $agent = <<<XML
<AgencyID>$agent_id</AgencyID> 
<AgentID>$agent_id</AgentID> 
<TargetCompany>$company</TargetCompany> 
<ManagerOffice>$manager_office</ManagerOffice> 
<ManagerFIO>$manager_fio</ManagerFIO> 
<ManagerPhone>$manager_phone</ManagerPhone>
<UserJuristical>false</UserJuristical>
<UserIsSubagent>false</UserIsSubagent>
<b2c>true</b2c>
XML;

        $agent = <<<XML
<AgentID>$agent_id</AgentID>
<TargetCompany>$company</TargetCompany>
<UserJuristical>false</UserJuristical>
<UserIsSubagent>false</UserIsSubagent>
<b2c>true</b2c>
XML;

        return $agent;
    }

    public function get_policy_for_save(ContractsCalculation $calc){

        $contract = $calc->contract;
        $company = 'TEST';
        $agent_id = '666666'; // 34809359
        $fromDate = date('Y-m-d\T00:00:00', strtotime($contract->begin_date));
        $toDate = date('Y-m-d\T23:59:59', strtotime($contract->end_date));
        $now = Carbon::now()->format('Y-m-d\TH:i:s');
        $driverLimit = $contract->drivers_type_id == 1 ? 'UNLIMIT' : 'LIMIT';

        $policy = <<<XML
<POLICYDATA>
    <IntegratorCompany>$company</IntegratorCompany>
    <CalcID>$calc->sk_key_id</CalcID>
    <UserJuristical>false</UserJuristical>
    <UserIsSubagent>false</UserIsSubagent>
    <UserDateOfBirth>1986-09-06T04:00:00+04:00</UserDateOfBirth>
    <UserGender>М</UserGender>
    <UserPhone>79853056051</UserPhone>
    <UserMail>sb@sst.cat</UserMail>
    <UserINN>666666666666</UserINN>
    <UserAddressKLADR>7700000000015780151</UserAddressKLADR>
    <UserAddress>Г Москва, ул. Краснобогатырская, 6с2</UserAddress>
    <b2c>true</b2c>
    <UserName>Тестов Тест Тестович</UserName>
    <AgentID>$agent_id</AgentID>
    <PDate>$now+03:00</PDate>
    <FromDate>$fromDate+03:00</FromDate>
    <ToDate>$toDate+03:00</ToDate>
    <DriverUnlimitedKind>$driverLimit</DriverUnlimitedKind>
    <IsEOSAGO>true</IsEOSAGO>
    <Currency>1</Currency>
    <InsPlace>online</InsPlace>
    <Login>$company</Login>
</POLICYDATA>
XML;

        return $policy;
    }


    public function get_subject_for_save($calc, $subject_type){

        $subject = [];

        $contract = $calc->contract;

        if($contract_subject = $contract->{$subject_type}){

            // если физик
            if($contract_subject->type == 0){

                $contract_subject_info = $contract_subject->get_info();

                $birthDate = date('Y-m-d\TH:i:s', strtotime($contract_subject_info->birthdate));
                $seria = $contract_subject_info->doc_serie;
                $number = $contract_subject_info->doc_number;
                $doc_date = date('Y-m-d\TH:i:s', strtotime($contract_subject_info->doc_date));
                $phone = isset($contract_subject->phone) ? setPhoneNumberFormat(parsePhoneNumber($contract_subject->phone), 'dddddddddd') : "";
                $mail = isset($contract_subject->email) ? $contract_subject->email : '';
                $sex = $contract_subject_info->sex == 1 ? 'Ж' : 'М';
                $street_fact_kladr = $contract_subject_info->address_fact_kladr;
                $region = $contract_subject_info->address_fact_region == 'Московская' ? 'Москва' : $contract_subject_info->address_fact_region;


                $subject = <<<XML
<PARTNERDATA>
    <type_>1</type_>
    <name_>$contract_subject_info->fio</name_>
    <birthdate>$birthDate+04:00</birthdate>
    <sex>$sex</sex>
    <Resident>true</Resident>
    <DOCUMENTS>
        <DOCUMENTType>
            <type_>35</type_>
            <seria>$seria</seria>
            <number>$number</number>
            <date>$doc_date+04:00</date>
        </DOCUMENTType>
    </DOCUMENTS>
    <ADDRESSES>
        <ADDRESSType>
            <type_>1</type_>
            <country>179</country>
            <streetKLADR>$street_fact_kladr</streetKLADR>
            <regionKLADR>$contract_subject_info->address_fact_city_kladr_id</regionKLADR>
            <region>$region</region>
            <city>$contract_subject_info->address_fact_city</city>
            <street>$contract_subject_info->address_fact_street</street>
            <housenum>$contract_subject_info->address_fact_house</housenum>
            <korpusnum>$contract_subject_info->address_fact_block</korpusnum>
            <flatnum>$contract_subject_info->address_fact_flat</flatnum>
            <cityKLADR>$contract_subject_info->address_fact_city_kladr_id</cityKLADR>
        </ADDRESSType>
    </ADDRESSES>
    <CONTACTS>
        <CONTACTType>
            <type_>4</type_>
            <value>$phone</value>
        </CONTACTType>
        <CONTACTType>
            <type_>5</type_>
            <value>$mail</value>
        </CONTACTType>
    </CONTACTS>
</PARTNERDATA>
XML;


            }else{ // юрик

                $info = $contract_subject->get_info();
                $phone = isset($contract_subject->phone) ? setPhoneNumberFormat(parsePhoneNumber($contract_subject->phone), 'dddddddddd') : "";
                $mail = isset($contract_subject->email) ? $contract_subject->email : '';

                $subject = <<<XML
<PARTNERDATA>
    <type_>2</type_>
        <CONTACTS>
        <CONTACTType>
            <type_>4</type_>
            <value>$phone</value>
        </CONTACTType>
        <CONTACTType>
            <type_>5</type_>
            <value>$mail</value>
        </CONTACTType>
    </CONTACTS>
</PARTNERDATA>
XML;

            }
        }

        return $subject;

    }

    public function get_drivers_for_save($calc){

        $drivers = '';

        foreach ($calc->drivers as $driver){

            $fio = implode(' ', $driver->get_fio());
            $birth_date = date('Y-m-d\TH:i:s', strtotime($driver->birth_date));
            $sex = ($driver->sex == 1) ? 'Ж' : 'М' ;
            $stage = (new \DateTime(Carbon::now()->format('d-m-Y')))->diff(new \DateTime($driver->exp_date));
            $licenceDate = date('Y-m-d\TH:i:s', strtotime($driver->doc_date));

            $drivers .= <<<XML
            <DRIVERType>
                <name_>$fio</name_>
                <birthdate>$birth_date+04:00</birthdate>
                <LicenceDate>$licenceDate+03:00</LicenceDate>
                <LicenceNumber>$driver->doc_num</LicenceNumber>
                <licenceSeria>$driver->doc_serie</licenceSeria>
                <Stage>$stage->y</Stage>
                <sex>$sex</sex>
            </DRIVERType>
XML;
        }

        return $drivers;
    }

    public function get_object_for_save($calc){

        $contract = $calc->contract;
        $auto = $calc->object_insurer_auto;

        $releaseDate = date('Y', strtotime($auto->sts_docdate));

        if($mark = $calc->sk_mark){
            $brandName = $mark->sk_title;
        }else{
            $brandName = '';
            $this->errors[] = 'Не синхронизирован справочник: Марки';
        }

        if($model = $calc->sk_model){
            $modelName = $model->sk_title;
        }else{
            $modelName = '';
            $this->errors[] = 'Не синхронизирован справочник: Модели';
        }

        if($purpose = $calc->sk_purpose){
            $purpose_code = $purpose->vehicle_purpose_sk_id;
        }else{
            $purpose_code = '';
            $this->errors[] = 'Цель использования не найдена';
        }

        if($category = $calc->sk_category){
            $category_id = $category->vehicle_categorie_sk_id;
        }else{
            $category_id = '';
            $this->errors[] = 'Не синхронизирован справочник: Категории';
        }

        $docdate = date('Y-m-d\TH:i:s', strtotime($auto->docdate));
        $owner = $contract->owner->get_info();
        $has_trailer = $auto->is_trailer == 1 ? 'true' : 'false';
        $weight = isset($auto->weight) ? (int)$auto->weight : '';
        $power = (int)$auto->power;
        $dk_date_from = date('Y-m-d\TH:i:s', strtotime($auto->dk_date_from));
        $dk_date_to = date('Y-m-d\TH:i:s', strtotime($auto->dk_date));

        if($auto->doc_type == 0){ // pts

            $pts_or_sts = <<<XML
<CarPTSDate>$docdate+00:00</CarPTSDate>
<CarPTSNumber>$auto->docnumber</CarPTSNumber>
<CarPTSSeria>$auto->docserie</CarPTSSeria>
XML;

        }elseif($auto->doc_type == 1){ // sts
            $pts_or_sts = <<<XML
<CarREGNum>$auto->docnumber</CarREGNum>
<CarREGSeria>$auto->docserie</CarREGSeria>
XML;
        }

        $transport = <<<XML
          <CarBrand>$brandName</CarBrand>
          <CarVersion>$modelName</CarVersion>
          <CarVersionPTS>$brandName $modelName</CarVersionPTS>
          <CarOwnerType>С</CarOwnerType>
          $pts_or_sts
          <CarGOSNumber>$auto->reg_number</CarGOSNumber>
          <Used_Trailer>$has_trailer</Used_Trailer>
          <CarVIN>$auto->vin</CarVIN>
          <CarYear>$auto->car_year</CarYear>
          <PS>$power</PS>
          <CarUseRegionKLADR>$owner->address_fact_kladr</CarUseRegionKLADR>
          <CarUsePeriod>12</CarUsePeriod>
          <osagoDocType>2</osagoDocType>
          <osagoDocNum>$auto->dk_number</osagoDocNum>
          <osagoDocDateFromDK>$dk_date_from+00:00</osagoDocDateFromDK>
          <osagoDocDatetoTO>$dk_date_to+00:00</osagoDocDatetoTO>

XML;

        return $transport;
    }
}
