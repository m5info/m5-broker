<?php

namespace App\Services\SK\RESO\Services\Osago;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\File;
use App\Repositories\FilesRepository;
use stdClass;

class OsagoService implements ProductServiceInterface{

    public $available_types = [
        0 => 'Бумажный',
        1 => 'Электронный',
    ];

    public $api;

    public function __construct(OsagoApi $api){
        $this->api = $api;
    }


    public function temp_calc(ContractsCalculation $calc)
    {


    }


    public function calc(ContractsCalculation $calc){

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';

        $result = $this->api->calc_osago($calc);

        if($result->state == true){

            $coeffs = $result->answer->Coeffs;

            $response->state = true;
            $response->payment_total = $result->answer->PremiumOsago;
            $response->sk_key_id = (string)$result->answer->CalcId;
            $response->msg = 'ТБ: 4118.00; КТ: 2.00; КБМ: 0.80; КВС: 0.96; КС: 1.00; КП: 1.00; КМ: 1.40; КПР: 1.00; КН: 1.00;';
            $response->statys_id = 2;

        }else{
            $response->error = $result->answer;
        }

        return $response;
    }


    public function release(ContractsCalculation $calc)
    {

        $contract = $calc->contract;

        $response = new stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->bso_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';

        $result = $this->api->release_osago($calc);

        if($result->state){

            $answer = $result->answer;

            $response->state = true;
            $response->statys_id = 4;  //Выпущен
            $response->answer = $answer;
            $response->payment_link = $answer->PaymentLink;

        }else{
            $response->error = $result->error;
        }

        //ЕСЛИ ДОГОВОР ЭЛЕКТРОННЫЙ Получаем БСО
        if($contract->is_epolicy == 1){
            $bso = $contract->getElectronBSO('MMM', '000000002');

            $bso->setBsoLog(0);

        }else{
            $bso = $contract->bso;
        }

        $response->bso_id = $bso->id;

        return $response;
    }


    public function check_status(ContractsCalculation $calc)
    {


    }


    public function get_files(ContractsCalculation $calc)
    {


        // доступные формы для печати
//        +"AvailableDocs": {#1189
//        +"TAvailableDoc": array:3 [
//            0 => {#1193
//            +"DocType": 0
//            +"Code": 26137610
//            +"Name": "Заявление на заключение ОСАГО"
//            +"Note": "Форма заявления с 25.05.2017"
//          }
//          1 => {#1195
//            +"DocType": 0
//            +"Code": 4844568
//            +"Name": "Справка об убыточности клиента"
//            +"Note": "ОСАГО"
//          }
//          2 => {#1196
//            +"DocType": 0
//            +"Code": 13241468
//            +"Name": "Письмо об отказе в выдаче справки"
//            +"Note": "Письмо об отказе в выдаче сведений о страховании ОСАГО"
//          }
//        ]
//      }

        $contract = $calc->contract;
        $payment = $contract->get_payment_first();

        $templates = [
            '26137610' => 'Заявление на заключение ОСАГО',
        ];

        $filesRepository = new FilesRepository();

        foreach ($templates as $form_id => $templ){

            $answer = $this->api->get_forms_osago($calc, $form_id);

            if($answer->state == true){
                $document = $answer->answer->Attach;

                if(isset($document) && isset($document->EncodeB64Str)){
                    $File_n = $document->DocExt;
                    $file_contents = base64_decode($document->EncodeB64Str);

                    $path = storage_path('app/' . Contracts::FILES_DOC . "/{$contract->id}/");

                    if(!is_dir(($path))){
                        mkdir(($path), 0777, true);
                    }

                    $file_name = uniqid();

                    file_put_contents($path . $file_name . $File_n, $file_contents);

                    $file = File::create([
                        'original_name' => $templ . $File_n,
                        'ext' => str_replace('.', '', $File_n),
                        'folder' => Contracts::FILES_DOC . "/{$contract->id}/",
                        'name' => $file_name,
                        'user_id' => auth()->id()
                    ]);

                    $contract->masks()->save($file);

                }
            }
        }

        return true;
    }

}