<?php

namespace App\Services\SK\RESO\Services\Dictionary;

use App\Interfaces\Services\SK\DictionaryServiceInterface;

class DictionaryService implements DictionaryServiceInterface {


    public $api;
    public $sheet_name = "Справочник";
    public $cache_json = __DIR__."/json.json";

    public function __construct(DictionaryApi $api){
        $this->api = $api;
    }

    public function get_marks_models(){

        $marks = [];
        $models = [];

        if(!is_file($this->cache_json)) {

            $marks_models = iconv("cp1251", "utf-8", $this->api->get_marks_models());

            $array = array_map("str_getcsv", explode("\n", $marks_models));

            $array_m_m = [];

            if(isset($array)){
                $model_id = 0;
                $mark_id = 0;
                foreach ($array as $row){

                    if($row[0] != null){

                        $mark_model = explode(';', $row[0]);
                        $array_m_m[$mark_model[0]][$model_id++] = $mark_model[1];
                    }
                }

                $marks2models = [];

                foreach($array_m_m as $mark_title => $row){
                    $mark_id++;
                    $marks2models[$mark_id] = [
                        'mark_title' => $mark_title,
                        'models' => $row,
                    ];
                }
            }

            if(isset($marks2models)){

                foreach ($marks2models as $mark_id => $mark_models){

                    $marks[] = [
                        'id' => $mark_id,
                        'title' => (string) str_replace('"', '', $mark_models['mark_title']),
                        'vehicle_categorie_sk_id' => (string) 'Легковые автомобили (Категория \'B\')',
                    ];

                    foreach($mark_models['models'] as $model_id => $model_title){

                        $models[] = [
                            'id' => $model_id,
                            'title' => (string) str_replace('"', '', $model_title),
                            'vehicle_mark_sk_id' => $mark_id,
                            'vehicle_categorie_sk_id' => (string)'Легковые автомобили (Категория \'B\')',
                        ];
                    }
                }
            }

            $result = [
                'categories' => $this->api->get_categories(),
                'marks' => $marks,
                'models' => $models,
            ];

            file_put_contents($this->cache_json, \GuzzleHttp\json_encode($result));

            return \GuzzleHttp\json_decode(file_get_contents($this->cache_json),true);

        }else{

            return \GuzzleHttp\json_decode(file_get_contents($this->cache_json),true);
        }
    }


    public function get_api_purpose(){

        $purposes_list = [];
        $answer = mb_convert_encoding($this->api->get_purpose(), "utf-8", "windows-1251");

        $purposes = explode(PHP_EOL, $answer);

        foreach($purposes as $purpose){
            $purpose = str_replace('"', '', trim($purpose));
            $purpose = explode(';', $purpose);

            if($purpose[0] != ''){

                $purposes_list[] = [
                    'id' => $purpose[0],
                    'title' => $purpose[1],
                ];
            }
        }

        return $purposes_list;
    }

    public function get_api_categories(){


//        $categories_list = [];
//        $answer = mb_convert_encoding($this->api->get_categories(), "utf-8", "windows-1251");
//
//        $categories = explode(PHP_EOL, $answer);
//
//        foreach($categories as $category){
//            $category = str_replace('"', '', trim($category));
//            $category = explode(';', $category);
//
//            if($category[0] != ''){
//
//                $categories_list[] = [
//                    'id' => $category[0],
//                    'title' => $category[1],
//                ];
//            }
//        }
//
//        return $categories_list;

        return $this->api->get_categories();
    }


}