<?php

namespace App\Services\SK\RESO\Services\Dictionary;

use App\Models\Contracts\Contracts;
use App\Models\Contracts\Orders;
use App\Models\File;
use App\Repositories\FilesRepository;
use App\Services\SK\RESO\Services\SendAPI\Send;

class DictionaryApi{

    public $cache_json = __DIR__."/json.json";

    public $url;
    public $login;
    public $password;

    public $Send = null;

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->Send = new Send($url, $login, $password, $api_setting);

    }

    public function query($param){
        //авторизация, или подготовка параметров авторизации для запроса, если нужно
        //собираем запрос для сервиса для конкретной ск
        //отправляем
    }



    public function get_purpose(){
        //Дергаем справочник

        $xml = <<<XML
  <ns3:getDictionary xmlns:ns2="http://autostrahovka.reso.ru/Schemas" xmlns:ns3="http://wsauto.webservice.reso.ru/">          
    <agent>666666</agent>        
    <dictCode>2</dictCode>
  </ns3:getDictionary>
XML;
        $xml = new \SoapVar($xml, XSD_ANYXML);

        $answer = $this->Send->send($xml, 'getDictionary');

        $purposes = base64_decode($answer->answer->return->EncodeB64Str);

        return $purposes;
    }

    public function get_categories(){
        //Дергаем справочник Категорий

//        $xml = <<<XML
//  <ns3:getDictionary xmlns:ns2="http://autostrahovka.reso.ru/Schemas" xmlns:ns3="http://wsauto.webservice.reso.ru/">
//    <agent>666666</agent>
//    <dictCode>4</dictCode>
//  </ns3:getDictionary>
//XML;
//        $xml = new \SoapVar($xml, XSD_ANYXML);
//
//        $answer = $this->Send->send($xml, 'getDictionary');
//
//        $categories = base64_decode($answer->answer->return->EncodeB64Str);


        $categories = [
            ['id'=>'0', 'title'=>'Мотоциклы и мото роллеры (Категория \'А\')'],
            ['id'=>'1', 'title'=>'Легковые автомобили (Категория \'B\')'],
            ['id'=>'2', 'title'=>'Легковые автомобили, используемые в качестве такси (Категория \'B\')'],
            ['id'=>'3', 'title'=>'Прицепы к легковым автомобилям, мотоциклам, мотороллерам'],
            ['id'=>'4', 'title'=>'Грузовые автомобили (не более 16т) (Категория \'С\')'],
            ['id'=>'5', 'title'=>'Грузовые автомобили (более 16т) (Категория \'С\')'],
            ['id'=>'6', 'title'=>'Прицепы к грузовым автомобилям, полуприцепы, прицепы-роспуски'],
            ['id'=>'7', 'title'=>'Автобусы (не более 16 пассажиромест) (Категория \'D\')'],
            ['id'=>'8', 'title'=>'Автобусы (более 16 пассажиромест) (Категория \'D\')'],
            ['id'=>'9', 'title'=>'Автобусы, используемые в качестве такси (Категория \'D\')'],
            ['id'=>'10', 'title'=>'Троллейбусы'],
            ['id'=>'11', 'title'=>'Трамваи'],
            ['id'=>'12', 'title'=>'Тракторы, самоходные дорожно-строительные машины и пр
С'],
            ['id'=>'13', 'title'=>'Прицепы к тракторам, самоходным дорожно-строительным машинам и пр 
ПС'],

        ];

        return $categories;
    }

    public function get_marks_models(){

        $xml = <<<XML
  <ns3:getDictionary xmlns:ns2="http://autostrahovka.reso.ru/Schemas" xmlns:ns3="http://wsauto.webservice.reso.ru/">          
    <agent>666666</agent>
    <dictCode>1</dictCode>
  </ns3:getDictionary>
XML;

        $answer = $this->Send->send($xml, 'getDictionary');

        $marks_and_model_list = base64_decode($answer->answer->return->EncodeB64Str);

        return  $marks_and_model_list;
    }


    public function get_modifications($mark_id, $model_id){
        //подготавливаем данные для запроса модификации
        //дёргаем свой query
        return [];

    }

}