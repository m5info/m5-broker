<?php

namespace App\Services\SK\RESO\Services\SendAPI;


use Doctrine\DBAL\Cache\CacheException;
use SoapClient;

class Send{

    public $url;
    public $login;
    public $password;

    public $SessionId;
    public $UserId;

    public $api_setting = null;

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

        $this->api_setting = $api_setting;

    }

    public function send($xml, $method)
    {

        $result = (object)['debug' => (object)[]];

        $wsdl = "{$this->url}/WebServiceAuto/WSAutoService?wsdl";

//        ini_set('default_socket_timeout', 600);

        $soap_stream_context = stream_context_create(array(
            'http'=>array(
                'method'=>"POST",
                'header'=> "Content-Type: application/soap+xml; charset=utf-8\r\n"
            )
        ));

        $soapClient  = new SoapClient($wsdl, array(
            'soap_version' => SOAP_1_1,
//            'stream_context' => $soap_stream_context,
            'trace' => 1,
            'keep_alive' => 1,
            'exception' => 0,
            'cache_wsdl' => WSDL_CACHE_NONE,
//            "connection_timeout" => 300
        ));

        $xml = new \SoapVar($xml, XSD_ANYXML);
//dd($soapClient->__getFunctions());
        try {
            $result->answer = $soapClient->{$method}($xml);

//            dd($soapClient->__getFunctions());
//            $result->debug->request = $soapClient->__getLastRequest();
//            $result->debug->response = $soapClient->__getLastResponse();
            dump($soapClient->__getLastRequest());
//            dump($result->answer);
//            dump($soapClient->__getLastRequestHeaders());
            dump($soapClient->__getLastResponse());
        }catch (\SoapFault $fault){
//            dd($soapClient->__getFunctions());

            //            dd(1);
//            dd($soapClient->__getLastResponse());
            dd($fault->getMessage());
        }

        return $result;
    }

}


