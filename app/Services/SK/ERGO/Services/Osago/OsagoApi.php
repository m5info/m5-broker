<?php

namespace App\Services\SK\ERGO\Services\Osago;

use App\Models\Contracts\ContractsCalculation;

use Carbon\Carbon;

class OsagoApi{

    public $url;
    public $login;
    public $password;
    public $errors = [];

    public function __construct($url, $login, $password){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

    }

    /**
     * @param $method
     * @return $auth
     * Метод возвращает Correlation_id для дальнейших действий с методами РГС
     */
    public function auth($method){

        $auth_params = [
            'username' => $this->login,
            'password' => $this->password
        ];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "{$this->url}{$method}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $auth_params,
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return (object)\GuzzleHttp\json_decode($response, true);
    }


    // калькуляция осаго
    public function calcOsago(ContractsCalculation $calc){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $auth = $this->auth('authorization');

        $params = $this->getCalcParams($calc, $auth);

        $calc = $this->send('calculate', $params);

        if(isset($calc->result) && $calc->result == true){

            $calc_info = $this->send("calculate/{$calc->data[0]->id}", $params, "key={$auth->data}");

            if(isset($calc_info->result) && $calc_info->result == true){
                $result->state = true;
                $result->answer = $calc_info->data;
                $result->sk_key_id = $calc->data[0]->id;
            }else{
                $result->answer = $calc->message;
            }

        }else{
            $result->answer = $calc->message;
        }

        if(!empty($this->errors)){ // ошибки со справочниками
            $result->answer = implode(';', $this->errors);
        }

        return $result;
    }


    public function releaseOsago(ContractsCalculation $calc){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $auth = $this->auth('authorization');

        $params = $this->getReleaseParams($calc, $auth);

        $release = $this->send('create', $params, "key={$auth->data}");

        if($release->result){
            $calc->sk_key_id = $release->data->policyId;
            $calc->save();
        }

        $release_info = $this->sendGet("policy/{$calc->sk_key_id}/info/?key={$auth->data}");

        if($release_info->result){

            $register = $this->sendGet("policy/{$calc->sk_key_id}/register/?key={$auth->data}");

            if($register->result){
                $result->answer = $release_info;
                $result->authKey = $auth->data;
                $result->state = true;
            }else{
                $result->error = 'Не удалось зарегистрировать полис';
            }

        }else{
            $result->error = 'Не удалось создать полис';
        }

        return $result;
    }


    public function send($method, $params, $key = 'fullInformation=true'){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "{$this->url}{$method}/?{$key}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $params,
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return \GuzzleHttp\json_decode($response);
    }

    public function sendGet($method){

        $curl = curl_init();
dump("{$this->url}{$method}");
        curl_setopt_array($curl, array(
            CURLOPT_URL => "{$this->url}{$method}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return \GuzzleHttp\json_decode($response);
    }


    public function getCalcParams(ContractsCalculation $calc, $auth, $calculation_id = null){

        $is_pre_calculation = ($calculation_id) ? false : true;
        $contract = $calc->contract;
        $agent_info = ($contract->getAgentSettings($calc)) ? $contract->getAgentSettings($calc) : null;
        $autoInfo = $calc->object_insurer_auto;
        $owner = $contract->owner;
        $owner_data_fl = $owner->data_fl;
        $owner_fio = explode(' ', $owner_data_fl->fio);

        $result = array(
            'key' => $auth->data,
            'dateStart' =>  date('Y-m-d', strtotime($contract->begin_date)),
            'period' => '12',
            'limitDrivers' => ($contract->drivers_type_id == 1) ? '0' : '1',
            'trailer' => ($autoInfo->is_trailer == 1) ? '1' : '0',
            'isJuridical' => $owner->type ? '1' : '0',
            'codeKladr' => '7600000000000',
            'codeOkato' => '45000000000',
            'owner[name]' => $owner_fio[0],
            'owner[lastname]' => $owner_fio[2],
            'owner[middlename]' => $owner_fio[1],
            'owner[birthday]' => date('Y-m-d', strtotime($owner_data_fl->birthdate)),
            'owner[document][dateIssue]' => date('Y-m-d', strtotime($owner_data_fl->doc_date)),
            'owner[document][issued]' => $owner_data_fl->doc_info,
            'owner[document][number]' => $owner_data_fl->doc_number,
            'owner[document][series]' => $owner_data_fl->doc_serie,
//            'owner[organization][name]' => 'ООО Ромашка',
//            'owner[organization][inn]' => '12345678',
//            'owner[organization][kpp]' => '87654321',
            'usagePeriod[0][dateStart]' => date('Y-m-d', strtotime($contract->begin_date)),
            'usagePeriod[0][dateEnd]' => date('Y-m-d', strtotime($contract->end_date)),

        );

        $result = array_merge($result, $this->getAuto($calc));
        $result = array_merge($result, $this->getDrivers($calc));

        return $result;
    }


    public function getReleaseParams(ContractsCalculation $calc, $auth){

        $result = array(
            'calculationId' => $calc->sk_key_id,
            'key' => $auth->data,
            'cabinet[email]' => 'mvg198412345@inbox.ru',
            'cabinet[password]' => '12345678',
        );

        $result = array_merge($result, $this->getSubject($calc, 'insurer'));
        $result = array_merge($result, $this->getSubject($calc, 'owner'));
        $result = array_merge($result, $this->getReleaseAuto($calc));

        return $result;
    }


    public function getSubject(ContractsCalculation $calc, $subj_type){

        $contract = $calc->contract;

        $subject = [];

        if($contract_subject = $contract->{$subj_type}){

            if($contract_subject->type == 0){// если физик

                $data_fl = $contract_subject->data_fl;

                $phone = ($contract_subject->phone) ? setPhoneNumberFormat(parsePhoneNumber($contract_subject->phone), '7dddddddddd') : null;

                $fio = explode(' ', $data_fl->fio);

                if($subj_type == 'insurer'){ // эти параметры только для страхователя
                    $subject['isInsurerJuridical'] = $contract_subject->type ? '1' : '0';
                    $subject["{$subj_type}[name]"] = $fio[1];
                    $subject["{$subj_type}[lastname]"] = $fio[0];
                    $subject["{$subj_type}[middlename]"] = isset($fio[2]) ? $fio[2] : '';
                    $subject["{$subj_type}[document][dateIssue]"] = $data_fl->doc_date;
                    $subject["{$subj_type}[document][issued]"] = $data_fl->doc_info;
                    $subject["{$subj_type}[document][number]"] = $data_fl->doc_number;
                    $subject["{$subj_type}[document][series]"] = $data_fl->doc_serie;
                }

                $subject["{$subj_type}[addressJuridical][country]"] = 'Россия';
                $subject["{$subj_type}[addressJuridical][zip]"] = $data_fl->address_register_zip;
                $subject["{$subj_type}[addressJuridical][city]"] = $data_fl->address_register_city;
                $subject["{$subj_type}[addressJuridical][street]"] = $data_fl->address_register_street;
                $subject["{$subj_type}[addressJuridical][home]"] = $data_fl->address_register_house;
                $subject["{$subj_type}[addressJuridical][flat]"] = $data_fl->address_register_flat;
                $subject["{$subj_type}[addressJuridical][kladr]"] = $data_fl->address_register_kladr;
                $subject["{$subj_type}[addressJuridical][okato]"] = $data_fl->address_register_okato;
                $subject["{$subj_type}[addressFact][country]"] = 'Россия';
                $subject["{$subj_type}[addressFact][zip]"] = $data_fl->address_fact_zip;
                $subject["{$subj_type}[addressFact][city]"] = $data_fl->address_fact_city;
                $subject["{$subj_type}[addressFact][street]"] = $data_fl->address_fact_street;
                $subject["{$subj_type}[addressFact][home]"] = $data_fl->address_fact_house;
                $subject["{$subj_type}[addressFact][flat]"] = $data_fl->address_fact_flat;
                $subject["{$subj_type}[addressFact][kladr]"] = $data_fl->address_fact_kladr;
                $subject["{$subj_type}[addressFact][okato]"] = $data_fl->address_fact_okato;
                $subject["{$subj_type}[birthday]"] = $data_fl->birthdate;
                $subject["{$subj_type}[email]"] = $contract_subject->email;
                $subject["{$subj_type}[phone]"] = $phone;

            }else{// юрик

                $contract_subject_info = $contract_subject->get_info();
                $data_ul = $contract_subject->data_ul;

                $subject["{$subj_type}[organization][name]"] = 'ООО Ромашка';
                $subject["{$subj_type}[organization][inn]"] = '12345678';
                $subject["{$subj_type}[organization][kpp]"] = '87654321';
                $subject["{$subj_type}[addressJuridical][country]"] = 'Россия';
                $subject["{$subj_type}[addressJuridical][zip]"] = '123181';
                $subject["{$subj_type}[addressJuridical][city]"] = 'Москва';
                $subject["{$subj_type}[addressJuridical][street]"] = 'ул Исаковского';
                $subject["{$subj_type}[addressJuridical][home]"] = '29 к 3';
                $subject["{$subj_type}[addressJuridical][flat]"] = '104';
                $subject["{$subj_type}[addressJuridical][kladr]"] = '7600000000013990066';
                $subject["{$subj_type}[addressJuridical][okato]"] = '45283577000';
                $subject["{$subj_type}[addressFact][country]"] = 'Россия';
                $subject["{$subj_type}[addressFact][zip]"] = '123181';
                $subject["{$subj_type}[addressFact][city]"] = 'Москва';
                $subject["{$subj_type}[addressFact][street]"] = 'ул Исаковского';
                $subject["{$subj_type}[addressFact][home]"] = '29 к 3';
                $subject["{$subj_type}[addressFact][flat]"] = '104';
                $subject["{$subj_type}[addressFact][kladr]"] = '7600000000013990066';
                $subject["{$subj_type}[addressFact][okato]"] = '45283577000';
                $subject["{$subj_type}[email]"] = 'test@test.ru';
                $subject["{$subj_type}[phone]"] = '7654321';
            }

        }else{
            $this->errors[] = 'Нет данных по Субъекту';
        }

        return $subject;
    }


    public function getAuto(ContractsCalculation $calc){

        $contract = $calc->contract;
        $autoInfo = $calc->object_insurer_auto;
        $auto = [];

        if($autoInfo){

            $auto = [
                'car[power]' => $autoInfo->power,
                'car[documents][registrationNumber]' => $autoInfo->reg_number,
                'car[documents][chassisNumber]' => '',
                'car[documents][carcaseNumber]' => '',
                'car[documents][vin]' => $autoInfo->vin,
            ];

            if($calc->sk_category){
                $auto['car[category]'] = $calc->sk_category->vehicle_categorie_sk_id;
            }else{
                $this->errors[] = 'Не синхронизирован справочник: Категория';
            }

            if($calc->sk_mark){
                $auto['car[make]'] = $calc->sk_mark->sk_title;
            }else{
                $this->errors[] = 'Не синхронизирован справочник: Марка';
            }

            if($calc->sk_model){
                $auto['car[model]'] = $calc->sk_model->sk_title;
            }else{
                $this->errors[] = 'Не синхронизирован справочник: Модель';
            }

        }else{
            $this->errors[] = 'Авто не найдено';
        }

        return $auto;
    }

    public function getReleaseAuto(ContractsCalculation $calc){

        $autoInfo = $calc->object_insurer_auto;
        $auto = [];

        if($autoInfo){

            $auto['diagnostic[number]'] = $autoInfo->dk_number ?: '';
            $auto['diagnostic[validDate]'] = $autoInfo->dk_date ? date('Y-m-d', strtotime($autoInfo->dk_date)) : '';
            $auto['car[year]'] = $autoInfo->car_year;
            $auto['car[isNew]'] = $autoInfo->car_year == Carbon::now()->format('Y') ? '1' : '0';


            if(isset($autoInfo->doc_type) && $autoInfo->doc_type == 0){
                $auto['car[pts][serie]'] = $autoInfo->docserie;
                $auto['car[pts][number]'] = $autoInfo->docnumber;
                $auto['car[pts][dateIssue]'] = date('Y-m-d', strtotime($autoInfo->docdate));
            }elseif($autoInfo->doc_type == 1){
                $auto['car[sts][serie]'] = $autoInfo->docserie;
                $auto['car[sts][number]'] = $autoInfo->docnumber;
                $auto['car[sts][dateIssue]'] = date('Y-m-d', strtotime($autoInfo->docdate));
            }

        }else{
            $this->errors[] = 'Авто не найдено';
        }

        return $auto;
    }


    public function getDrivers(ContractsCalculation $calc){

        $drivers = [];

        if($calc->drivers){

            $id = 0;
            foreach ($calc->drivers as $driver){

                $fio = $driver->get_fio();

                $last_name = $fio['last_name'];
                $first_name = $fio['first_name'];
                $middle_name = $fio['middle_name'];

                $drivers[] = [
                    "drivers[{$id}][name]" => $first_name,
                    "drivers[{$id}][lastname]" => $last_name,
                    "drivers[{$id}][middlename]" => $middle_name,
                    "drivers[{$id}][birthday]" => $driver->birth_date,
                    "drivers[{$id}][license][dateBeginDrive]" => $driver->exp_date,
                    "drivers[{$id}][license][dateIssue]" => $driver->doc_date,
                    "drivers[{$id}][license][number]" => $driver->doc_num,
                    "drivers[{$id}][license][series]" => $driver->doc_serie,
                ];

                $id++;
            }

            $result = [];

            foreach($drivers as $subArr){
                foreach($subArr as $key => $val){
                    $result[$key] = $val;
                }
            }

        }else{
            $this->errors[] = 'Не указан водитель';
        }

        return $result;
    }

    public function get_purpose(ContractsCalculation $calc){

        $purpose_id = '';

        if($purpose = $calc->sk_purpose){
            $purpose_id = $purpose->vehicle_purpose_sk_id;
        }else{
            $this->errors[] = 'Цель использования не найдена';
        }

        return $purpose_id;
    }

    public function get_agent(ContractsCalculation $calc){

        $agent = [];
        $contract = $calc->contract;

        if($contract->user->organization->inn){

            $agent = [
                "INN" => $contract->user->organization->inn, // 7710353606 из примера
                "SubjectTypeId" => "2",
            ];

        }else{
            $this->errors[] = 'Агент не найден';
        }

        return $agent;
    }


    public function get_periods_of_use(ContractsCalculation $calc){

        $periods_of_use = [];

        $contract = $calc->contract;

        $periods_of_use[] = [
            "Id" => 1,
            "StartDate" => date('Y-m-d\TH:i:s', strtotime($contract->begin_date)),
            "EndDate" => date('Y-m-d\TH:i:s', strtotime($contract->end_date)),
            "IsUsagePeriodAdded" => false,
            "IsUsagePeriodRemoved" => false
        ];

        return $periods_of_use;

    }

}