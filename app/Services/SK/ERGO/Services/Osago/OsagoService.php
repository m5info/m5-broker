<?php

namespace App\Services\SK\ERGO\Services\Osago;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\File;
use App\Repositories\FilesRepository;
use stdClass;

class OsagoService implements ProductServiceInterface{

    public $api;

    public function __construct(OsagoApi $api){
        $this->api = $api;
    }

    public function calc(ContractsCalculation $calc){

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';


        $calc = $this->api->calcOsago($calc);

        if($calc->state == true){

            $koeff = $calc->answer->response->Details->Coefficient;

            $response->state = true;
            $response->error = '';
            $response->sk_key_id = $calc->sk_key_id;
            $response->statys_id = 1;
            $response->payment_total = $calc->answer->premium;
            $response->msg = "ТБ: {$koeff[0]->value}; КТ: {$koeff[1]->value}; КБМ: {$koeff[2]->value}; КВС: {$koeff[8]->value}; КС: {$koeff[6]->value}; КП: {$koeff[5]->value}; КМ: {$koeff[7]->value}; КПР: {$koeff[9]->value}; КН: {$koeff[4]->value};";

        }else{
            $response->error = $calc->answer;
        }

        return $response;
    }

    public function temp_calc(ContractsCalculation $calc){
        // TODO: Implement temp_calc() method.
    }

    public function release(ContractsCalculation $calc){

        $contract = $calc->contract;

        $response = new stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';

        $result = $this->api->releaseOsago($calc);

        if($result->state){

            $answer = $result->answer;

            $addBso = $this->api->sendGet("policy/{$calc->sk_key_id}/apply_bso/?key={$result->authKey}&serie=XXX&number=12345678");

dd($addBso);
            $response->state = true;
            $response->statys_id = 4;  //Выпущен
            $response->answer = $answer;

            $this->save_files($calc, $answer->Data);

        }else{

            $response->error = $result->error;
        }

        $response->bso_id = $contract->bso->id;

        return $response;
    }

    public function check_status(ContractsCalculation $calc){

    }

    public function get_files(ContractsCalculation $calc){

        return true;
    }

    public function save_files(ContractsCalculation $calc, $documents){

        $contract = $calc->contract;

        if(isset($documents) && $documents != ''){

            foreach($documents as $document){

                $File_n = 'pdf';
                $file_contents = base64_decode($document->Document);
                $path = storage_path('app/' . Contracts::FILES_DOC . "/{$contract->id}/");

                if(!is_dir(($path))){
                    mkdir(($path), 0777, true);
                }

                $file_name = uniqid();

                file_put_contents($path . $file_name . '.' . $File_n, $file_contents);

                $file = File::create([
                    'original_name' => $document->Name,
                    'ext' => $File_n,
                    'folder' => Contracts::FILES_DOC . "/{$contract->id}/",
                    'name' => $file_name,
                    'user_id' => auth()->id()
                ]);

                $contract->masks()->save($file);
            }

        }

        return true;
    }
}