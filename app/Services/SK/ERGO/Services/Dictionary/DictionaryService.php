<?php

namespace App\Services\SK\ERGO\Services\Dictionary;

use App\Interfaces\Services\SK\DictionaryServiceInterface;


class DictionaryService implements DictionaryServiceInterface {

    public $api;
    public $cache_json = __DIR__."/json.json";


    public function __construct(DictionaryApi $api){
        $this->api = $api;
    }

    public function get_api_purpose(){

        return $this->api->getPurpose();

    }


    public function get_api_categories(){

        return $this->api->getCategories();

    }

    public function get_marks_models(){

        if(!is_file($this->cache_json)) {

            $marksModels = $this->api->getMarksModels();

            $result = [
                'categories' => $this->api->getCategories(),
                'marks' => $marksModels['marks'],
                'models' => $marksModels['models'],
            ];

            file_put_contents($this->cache_json, \GuzzleHttp\json_encode($result));

            return \GuzzleHttp\json_decode(file_get_contents($this->cache_json),true);

        }else{

            return \GuzzleHttp\json_decode(file_get_contents($this->cache_json),true);
        }

        return $result;
    }
}