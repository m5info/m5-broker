<?php

namespace App\Services\SK\ERGO\Services\Dictionary;

use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Facades\Excel;

class DictionaryApi{

    public $url;
    public $login;
    public $password;
    public $cache_json = __DIR__."/lists/json.json";

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

    }


    public function getPurpose(){
        //Дергаем справочник

        //Если у СК это не реализовано возвращаем константу
        $purposes = [
            ['id'=>'0',  'title' => 'Личная'],
            ['id'=>'1',  'title' => 'Такси'],
            ['id'=>'2',  'title' => 'Учебная Езда'],
            ['id'=>'3',  'title' => 'Дорожные И Специальные ТС'],
            ['id'=>'4',  'title' => 'Прочее'],
            ['id'=>'5',  'title' => 'Пассажирские Перевозки'],
            ['id'=>'6',  'title' => 'Опасный Груз'],
            ['id'=>'7',  'title' => 'Сдача В Аренду'],
            ['id'=>'8',  'title' => 'Экстренные И Комм Службы'],
        ];

        return $purposes;
    }

    public function getCategories(){

        $categories = [];

        $id = 0;
        foreach($this->send('categories') as $row){

            $row_category_name = $row->name;
            $row_category_id = $row->category;

            $checks = [
                !empty(trim($row_category_name)),
                !empty(trim($row_category_id)),
            ];

            if(!in_array(false, $checks,true)){

                $categories[$row_category_id] = [
                    'id' => $row_category_id,
                    'title' => $row_category_name,
                ];

            }
        }

        sort($categories);

        return $categories;

    }

    public function getMarksModels(){

        $marksModels = [
            'marks' => [],
            'models' => [],
        ];

        $mark_id = 0;
        $model_id = 0;
        foreach($this->send('models') as $row){

            $mark_name = $row->make;

            $marksModels['marks'][$mark_id] = [
                'id' => $mark_id,
                'title' => $mark_name,
                'vehicle_categorie_sk_id' => '',
            ];

            foreach($row->models as $model){
                $marksModels['models'][$model_id] = [
                    'id' => $model_id,
                    'title' => $model->model,
                    'vehicle_mark_sk_id' => $mark_id,
                    'vehicle_categorie_sk_id' => $model->category,
                ];
                $model_id++;
            }
            $mark_id++;
        }

        sort($marksModels['marks']);

        return $marksModels;
    }


    public function send($method){

        $list = [];

        $auth = $this->auth('authorization');

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "{$this->url}/{$method}/?key={$auth->data}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = \GuzzleHttp\json_decode(curl_exec($curl));

        return $response->data;
    }

    public function auth($method){

        $auth_params = [
            'username' => $this->login,
            'password' => $this->password
        ];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "{$this->url}{$method}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $auth_params,
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return (object)\GuzzleHttp\json_decode($response, true);
    }
}