<?php

namespace App\Services\SK\ERGO;


use App\Services\SK\BaseServiceController;
use App\Services\SK\ERGO\Services\Dictionary\DictionaryApi;
use App\Services\SK\ERGO\Services\Dictionary\DictionaryService;
use App\Services\SK\ERGO\Services\Osago\OsagoApi;
use App\Services\SK\ERGO\Services\Osago\OsagoService;

class ServiceController extends BaseServiceController {

    const SERVICES = [

        'dictionary' => [
            'service' => DictionaryService::class,  /// Можно проставить для удобства конечно им префиксы типа "AlphaDictionaryService::class"
            'api' => DictionaryApi::class,          /// но наверное необязательно
        ],
        'osago' => [
            'service' => OsagoService::class,
            'api' => OsagoApi::class,
        ],

    ];

}