<?php

namespace App\Services\SK\Ingos\Services\Dictionary;

use App\Services\SK\Ingos\Services\SendAPI\Send;

class DictionaryApi{


    public $url;
    public $login;
    public $password;

    public $Send = null;

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->Send = new Send($url, $login, $password, $api_setting);

    }

    public function query($param){
        //авторизация, или подготовка параметров авторизации для запроса, если нужно
        //собираем запрос для сервиса для конкретной ск
        //отправляем
    }


    private function getDicti(){

        $response = $this->Send->send('GetDicti', [
            'SessionToken'     => $this->Send->token,
            'Product' => 753518300,
        ]);

        if($response->ResponseData){
            return new \SimpleXMLElement($response->ResponseData->any);
        }

    }


    public function get_purpose(){

        $purposes = [
            ['id'=>'Personal', 'title'=>'Личная'],
            ['id'=>'Taxi', 'title'=>'Такси'],
            ['id'=>'RidingTraining', 'title'=>'Учебная езда'],
            ['id'=>'RoadVehicles', 'title'=>'Дорожные и специальные транспортные средства'],
            ['id'=>'Others', 'title'=>'Прочее'],
            ['id'=>'PassengerService', 'title'=>'Регулярные пассажирские перевозки / перевозки пассажиров по заказам'],
            ['id'=>'DangerousCargo', 'title'=>'Перевозка опасных и легковоспламеняющихся грузов'],
            ['id'=>'Rent', 'title'=>'Прокат / краткосрочная аренда'],
            ['id'=>'Emergency', 'title'=>'Экстренные и коммунальные службы'],
        ];


        return $purposes;


        //$dict = $this->GetDicti();
    }

    public function get_categories(){
        //Дергаем справочник Категорий

        $dict = $this->getDicti();
        $arr = [];
        foreach ($dict->ModelList->Model->Model as $models){
            $temp_arr = (array)$models;

            $arr[] = ['id'=>$temp_arr['@attributes']['ISN'], 'title'=>$temp_arr['@attributes']['Name']];
        }

        return $arr;
    }

    public function get_marks_models(){
        $dict = $this->getDicti();
        return $dict->ModelList->Model;
    }




    public function get_modifications($mark_id, $model_id){
        //подготавливаем данные для запроса модификации
        //дёргаем свой query
        return [];

    }

}