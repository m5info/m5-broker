<?php

namespace App\Services\SK\Ingos\Services\Dictionary;

use App\Interfaces\Services\SK\DictionaryServiceInterface;

class DictionaryService implements DictionaryServiceInterface {


    public $api;
    public $cache_json = __DIR__."/dict_auto.json";

    public function __construct(DictionaryApi $api){
        $this->api = $api;
    }

    public function get_marks_models(){

        $categories = [];
        $marks = [];
        $models = [];

        if(!is_file($this->cache_json)) {

            $_categories = $this->api->get_marks_models();

            foreach ($_categories->Model as $_cat){
                $temp_arr_cat = (array)$_cat;

                $categories[] = [
                    'id' => $temp_arr_cat['@attributes']['ISN'],
                    'title' => $temp_arr_cat['@attributes']['Name'],
                ];

                foreach ($_cat->Model as $_mark){
                    $temp_arr_mark = (array)$_mark;

                    $marks[] = [
                        'id' => $temp_arr_mark['@attributes']['ISN'],
                        'title' => $temp_arr_mark['@attributes']['Name'],
                        'vehicle_categorie_sk_id' => (string)$temp_arr_cat['@attributes']['ISN'],
                    ];

                    foreach ($_mark->Model as $_model){
                        $temp_arr_model = (array)$_model;
                        $models[] = [
                            'id' => $temp_arr_model['@attributes']['ISN'],
                            'title' => $temp_arr_model['@attributes']['ISN'],
                            'vehicle_mark_sk_id' => $temp_arr_mark['@attributes']['ISN'],
                            'vehicle_categorie_sk_id' => $temp_arr_cat['@attributes']['ISN']
                        ];
                    }

                }


            }

            $result = [
                'categories' => $categories,
                'marks' => $marks,
                'models' => $models,
            ];

            file_put_contents($this->cache_json, \GuzzleHttp\json_encode($result));

            return $result;
        }else{
            return \GuzzleHttp\json_decode(file_get_contents($this->cache_json),true);
        }
    }


    public function get_api_purpose(){
        return $this->api->get_purpose();
    }

    public function get_api_categories(){
        return $this->api->get_categories();
    }


}