<?php

namespace App\Services\SK\Ingos\Services\SendAPI;


use Doctrine\DBAL\Cache\CacheException;

class Send{


    public $url;
    public $login;
    public $password;

    public $token = '';
    protected $soapClient;

    public $api_setting = null;


    private $state_api_work = 0;


    public function __construct($url, $login, $password, $api_setting){


        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->api_setting = $api_setting;

        $this->soapClient = $this->getSoapClient();

        $this->setToken();

    }


    private function getSoapClient()
    {
        try {
            return new \SoapClient(
                $this->url, array(
                    'cache_wsdl' => WSDL_CACHE_NONE,
                    "exceptions" => true,
                    'trace'      => true,
                )
            );
        } catch (\SoapFault $e) {
            throw $e;
        }
    }

    protected function requestParams($params)
    {
        $res = null;
        if (is_array($params)) {
            $res = new \SoapParam($this->soapify($params, SOAP_ENC_OBJECT), 'Data');
        }
        $res = new \SoapParam(new \SoapVar($params, XSD_ANYXML), 'Data');

        //var_dump($res);

        return $res;

    }

    protected function soapify($data, $encoding)
    {
        foreach ($data as $key => &$value) {
            if (is_array($value)) {
                $value = $this->soapify($value, $encoding);
            }

        }
        return new \SoapVar($data, $encoding);
    }


    private function setToken()
    {
        $response = $this->send('Login', [
            'User'     => $this->login,
            'Password' => $this->password,
        ]);

        if(isset($response->ResponseData)){
            $this->token = $response->ResponseData->SessionToken;
        }

    }


    public function send($functionName, $arguments, array $options = []){

        //var_dump($arguments);

        try {
            $response = $this->soapClient->__soapCall($functionName, array($arguments), $options);
            //echo "<pre>" . print_r($this->soapClient->__getLastRequest(), 1) . "</pre>";

            if((int)$response->ResponseStatus->ErrorCode!=0){
                return $this->responseError($response->ResponseStatus->ErrorMessage);
            }

            return $response;
        } catch (\SoapFault $e) {
            return $this->responseError($e);
        }
    }

    public function responseError($txt)
    {
        $res = new \stdClass();
        $res->Error = new \stdClass();
        $res->Error->Code = 10;
        $res->Error->Text = $txt;
        return $res;
    }

}