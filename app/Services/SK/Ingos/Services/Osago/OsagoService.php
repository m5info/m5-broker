<?php

namespace App\Services\SK\Ingos\Services\Osago;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\File;
use App\Repositories\FilesRepository;
use function GuzzleHttp\Psr7\str;

class OsagoService implements ProductServiceInterface{

    public $available_types = [
        0 => 'Бумажный',
        1 => 'Электронный',
    ];

    public $api;

    public function __construct(OsagoApi $api){
        $this->api = $api;
    }


    public function temp_calc(ContractsCalculation $calc)
    {


    }


    public function calc(ContractsCalculation $calc){



        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';





        /*//////

        $response->state = true;
        $response->error = '';
        $response->sk_key_id = '12312';
        $response->statys_id = 1;
        $response->payment_total = 5373.31;
        $response->msg = 'ТБ: 3998.00; КТ: 2.00; КБМ: 0.50; КВС: 0.96; КС: 1.00; КП: 1.00; КМ: 1.40; КПР: 1.00; КН: 1.00;';
        return $response;

        /////////*/

        return $this->api->getTarif($calc);


        /*

        $result = $this->api->getTarif($calc);


        dd($result);

        if($result && isset($result->data) && isset($result->data->error)){

            $array = is_array($result->data->error)?$result->data->error:array($result->data->error);

            foreach ($array as $error){

                if(isset($error->parameter)){
                    $response->error .= "{$error->parameter}: {$error->text};";
                }

                if(isset($error->row)){
                    $response->error .= "{$error->row->text};";
                }

            }



        }

        dd($result);



        if((int)$result->Error->Code == 0){

            $response->state = true;

            $response->payment_total = (string)$result->Answer->Calc->OsagoPrem;
            $response->sk_key_id = (string)$result->Answer->Contract->ContractId;
            $response->msg = (string)$result->Answer->Calc->OsagoTarif;
            $response->statys_id = 2;


        }else{
            $response->error = (string)$result->Error->Text;
        }


        return $response;
        */
    }


    public function release(ContractsCalculation $calc)
    {

        $contract = $calc->contract;

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';

        $response->bso_id = $contract->bso_id;
        $response->statys_id = 0; //Статус договора

        $result = $this->api->setPayment($calc);

        if((int)$result->Error->Code == 0){

            $response->state = true;
            $response->statys_id = 4; //Выпущен

            //ЕСЛИ ДОГОВОР ЭЛЕКТРОННЫЙ Получаем БСО
            if($contract->is_epolicy == 1){
                $bso = $contract->getElectronBSO((string)$result->Answer->BSOSeries, (string)$result->Answer->BSONumber);

                $bso->setBsoLog(0);

            }else{
                $bso = $contract->bso;
            }


            $response->bso_id = $bso->id;


        }else{
            $response->error = (string)$result->Error->Text;
        }


        return $response;
    }


    public function check_status(ContractsCalculation $calc)
    {


    }


    public function get_files(ContractsCalculation $calc)
    {

        $tempals = [
            'PrintPolis'=>'Полис',
            'PrintDeclaration'=>'Заявление',
        ];


        $contract = $calc->contract;
        $payment = $contract->get_payment_first();

        if ($payment->pay_mehtod->key_type == 3) { // Счет
            $tempals['PrintInvoice'] = 'Счет клиенту Сбербанк';
        }elseif($payment->pay_mehtod->key_type == 0) { // Квитанция
            $tempals['PrintTicket'] = 'Квитанция';
        }

        $filesRepository = new FilesRepository();

        foreach ($tempals as $key => $templ){

            $document = $this->api->getDocument($calc, $key);
            if(isset($document) && isset($document->FileData))
            {
                $File_n = explode('.', $document->FileName);
                $file_contents = base64_decode($document->FileData);
                $path = storage_path('app/'.Contracts::FILES_DOC . "/{$contract->id}/");

                if (!is_dir(($path))) {
                    mkdir(($path), 0777, true);
                }

                $file_name = uniqid();

                file_put_contents($path . $file_name.'.'.$File_n[1], $file_contents);


                $file = File::create([
                    'original_name' => $templ,
                    'ext'           => $File_n[1],
                    'folder'        => Contracts::FILES_DOC . "/{$contract->id}/",
                    'name'          => $file_name,
                    'user_id'       => auth()->id()
                ]);


                $contract->masks()->save($file);

            }


        }

        return true;
    }

}