<?php

namespace App\Services\SK\Ingos\Services\Osago;


use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Services\Calc\DefaultOSAGO;
use App\Services\SK\Verna\Services\Auxiliary\Car;
use App\Services\SK\Verna\Services\Auxiliary\Subject;
use App\Services\SK\Verna\Services\SendAPI\Send;

class OsagoApi{


    public $url;
    public $login;
    public $password;
    public $Send = null;
    public $SessionId = null;

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->Send = new Send($url, $login, $password, $api_setting);

    }



    public function getTarif(ContractsCalculation $calc)
    {
        return $this->calc($calc);
    }

    public function calc(ContractsCalculation $calc)
    {
        return DefaultOSAGO::calc($calc);
    }



    public function setPayment(ContractsCalculation $calc)
    {
        return $this->responseError("ERROR");
    }


    public function controlPaymentXml($xml, $rec = 0)
    {

        return $this->responseError("ERROR");
    }



    public function getMainJson($calc)
    {

        $contract = $calc->contract;
        $ContractId = strlen($calc->sk_key_id)>0?$calc->sk_key_id:0;

        //$response = $this->Send->send('products/osago/dicti?userConstName=ucCatalogDocumentClasses', null, 'GET');




        $Multidrive = ($contract->drivers_type_id == 1)?'N':"Y";

        $AgreementCalc = new \stdClass();

        $AgreementCalc->IsProlongation = '0';//"Признак пролонгации (Тип: Строка. Обязательный параметр. Макс. длина: 1 символов)"
        $AgreementCalc->IsAddendum = '0'; //"Признак дополнения (Тип: Строка. Обязательный параметр. Макс. длина: 1 символов)"
        $AgreementCalc->DriversRestriction = $Multidrive; //"Полис безлимитный (N), или с ограничением числа водителей по списку (Y) (Тип: Строка. Обязательный параметр. Макс. длина: 1 символов)"
        $AgreementCalc->PolicyKBM = '';//"Итоговый КБМ по полису (класс) (Тип: Строка. Необязательный параметр)"


        $sign_date = date("d.m.Y", strtotime($contract->sign_date));
        $begin_date = date("d.m.Y H:i", strtotime($contract->begin_date));
        $end_date = date("d.m.Y", strtotime($contract->end_date));




        $AgreementCalc->DateSign = $sign_date;
        $AgreementCalc->DateBeg = $begin_date;
        $AgreementCalc->DateEnd = $end_date;

        $AgreementCalc->AddDateSign = '';
        $AgreementCalc->AddDateBeg = '';
        $AgreementCalc->AddPremiumSum = '';



        $AgreementCalc->InsurancePremium = '';
        $AgreementCalc->DateInsurancePremium = date("d.m.Y H:i:s");
        $AgreementCalc->OSAGOKoef = ['TB'=>'','KT'=>'','KBM'=>'','KO'=>'','KVS'=>'','KM'=>'','KS'=>'','KN'=>''];



        $insurer = Subject::getSubject($contract->insurer, $calc->insurance_companies_id, $this->Send);
        if($insurer->state == false){
            return $this->responseError($insurer->result);
        }




        $owner = Subject::getSubject($contract->owner, $calc->insurance_companies_id, $this->Send);
        if($owner->state == false){
            return $this->responseError($owner->result);
        }


        $drivers = new \stdClass();
        $drivers->result = null;

        if(($contract->drivers && sizeof($contract->drivers)) && $contract->drivers_type_id == 0){
            $drivers = Subject::getDrivers($contract->drivers, $calc->insurance_companies_id, $this->Send);
            if($drivers->state == false){
                return $this->responseError($drivers->result);
            }
        }

        $AgreementCalc->PolicyOwner = $insurer->result;

        if($drivers->result && $drivers->result!=null){
            $AgreementCalc->DriversPart = $drivers->result;
        }




        $car = Car::getCarOSAGO($calc, $owner);

        if($car->state == false){
            return $this->responseError($car->result);
        }

        $AgreementCalc->CAR = $car->result;


        $contractJson = new \stdClass();
        $contractJson->AggregatorId ='';
        $contractJson->Pwd ='';
        $contractJson->AgreementCalc = $AgreementCalc;



        $response = $this->Send->send('products/osago/createcalc', $contractJson, 'POST');




        dd($response);

        //echo $xml;
        //dd("OK");

        //$response = $this->Send->send($xml);
        return $response;

    }









    public function getContractXML($calc)
    {
        $contract = $calc->contract;

        $sign_date = date("d.m.Y", strtotime($contract->sign_date));
        $begin_date = date("d.m.Y H:i", strtotime($contract->begin_date));
        $end_date = date("d.m.Y", strtotime($contract->end_date));

        $period1_begin = date("d.m.Y", strtotime($contract->begin_date));
        $period1_end = date("d.m.Y", strtotime($contract->end_date));

        $UseCaravan = $contract->object_insurer_auto->is_trailer;

        $Multidrive = ($contract->drivers && sizeof($contract->drivers))?'N':"Y";

        $xml = <<<XML
        
            <DateBeg>{$begin_date}</DateBeg> 
            <DateEnd>{$end_date}</DateEnd> 
            <PeriodBeg1>{$period1_begin}</PeriodBeg1>
            <PeriodEnd1>{$period1_end}</PeriodEnd1>
            <PeriodBeg2></PeriodBeg2>
            <PeriodEnd2></PeriodEnd2>
            <PeriodBeg3></PeriodBeg3>
            <PeriodEnd3></PeriodEnd3>
            <Remark></Remark>
            <Multidrive>{$Multidrive}</Multidrive>
            <Violations>N</Violations>
            <UseCaravan>{$UseCaravan}</UseCaravan>
        
XML;

        return $xml;
    }




    private function responseError($error)
    {
        $r = new \stdClass();
        $r->Error = new \stdClass();
        $r->Error->Code = 10;
        $r->Error->Text = $error;
        return $r;
    }



}