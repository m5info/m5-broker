<?php

namespace App\Services\SK\Ingos;


use App\Services\SK\BaseServiceController;
use App\Services\SK\Ingos\Services\Dictionary\DictionaryApi;
use App\Services\SK\Ingos\Services\Dictionary\DictionaryService;
use App\Services\SK\Ingos\Services\Osago\OsagoApi;
use App\Services\SK\Ingos\Services\Osago\OsagoService;

class ServiceController extends BaseServiceController {


    const SERVICES = [

        'dictionary' => [
            'service' => DictionaryService::class,  /// Можно проставить для удобства конечно им префиксы типа "AlphaDictionaryService::class"
            'api' => DictionaryApi::class,          /// но наверное необязательно
        ],
        'osago' => [
            'service' => OsagoService::class,
            'api' => OsagoApi::class,
        ],


    ];

    const PROGRAMS = [
        'kasko' => [

            ]

    ];


}