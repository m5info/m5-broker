<?php

namespace App\Services\SK\Soglasie;

use App\Services\SK\BaseServiceController;
use App\Services\SK\Soglasie\Services\Dictionary\DictionaryApi;
use App\Services\SK\Soglasie\Services\Dictionary\DictionaryService;
use App\Services\SK\Soglasie\Services\Osago\OsagoApi;
use App\Services\SK\Soglasie\Services\Osago\OsagoService;

class ServiceController extends BaseServiceController {


    const SERVICES = [

        'dictionary' => [
            'service' => DictionaryService::class,
            'api' => DictionaryApi::class
        ],
        'osago' => [
            'service' => OsagoService::class,
            'api' => OsagoApi::class,
        ],

    ];



}