<?php

namespace App\Services\SK\Soglasie\Services\Dictionary;

class DictionaryApi extends DictionaryApiParser
{
    public $url;
    public $login;
    public $password;

    const PATHS = [
        'purposesOfUse' => 'https://b2b.soglasie.ru/CCMC/catalog.jsp?calc=10698&catalog=1718',
        'categories' => 'https://b2b.soglasie.ru/CCMC/catalog.jsp?calc=10698&catalog=1716',
        'marksAndModels' => 'https://b2b.soglasie.ru/CCMC/catalog.jsp?product=93&catalog=1436'
    ];

    public function __construct($url, $login, $password, $api_setting)
    {
        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
    }

    public function get_purpose()
    {
        return $this->parseTable('purposesOfUse');
    }

    public function get_categories()
    {
        return $this->parseTable('categories');
    }

    public function get_marks_and_models()
    {
        return $this->parseTableMarksAndModels('marksAndModels');
    }

}