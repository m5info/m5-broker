<?php

namespace App\Services\SK\Soglasie\Services\Dictionary;
use Symfony\Component\DomCrawler\Crawler;

class DictionaryApiParser
{
    /**
     * Парсинг HTML - справочника (простая таблица)
     * @param $path - Ссылка на HTML-источник справочника
     * @return array
     */
    protected function parseTable($path)
    {
        $endpoint = DictionaryApi::PATHS[$path];
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $endpoint);
        $dom = new Crawler((string)$response->getBody());
        $parsedData = $dom->filter('table')->children()->each(function(Crawler $tr){
            return $tr->children()->each(function(Crawler $td){
                return $td->text();
            });
        });

        unset($parsedData[0]);
        $resultData = [];

        foreach ($parsedData as $i => $str){
            $resultData[$i]['id'] = $str[0];
            $resultData[$i]['title'] = $str[1];
        }

        return $resultData;
    }

    /**
     * Кастомный парсинг HTML - справочника по маркам и моделям
     * @param $path
     * @return array
     */
    protected function parseTableMarksAndModels($path)
    {
        $endpoint = DictionaryApi::PATHS[$path];
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $endpoint);
        $dom = new Crawler((string)$response->getBody());
        $parsedData = $dom->filter('table')->children()->each(function(Crawler $tr){
            return $tr->children()->each(function(Crawler $td){
                return $td->text();
            });
        });

        unset($parsedData[0]);
        $resultData = array();
        $resultData['categories'] = $this->parseTable('categories');
        $resultData['marks'] = $this->getMarks($parsedData);
        $resultData['models'] = $this->getModels($parsedData);

        return $resultData;
    }

    private function getMarks($parsedData)
    {
        $prepareData = $this->prepareMarksData($parsedData);

        $marks = [];

        $i = 1;
        foreach ($prepareData as $id => $title)
        {
            $marks[$i]['id'] = $i;
            $marks[$i]['title'] = $title;

            #Заглушка (легковые автомобили)
            $marks[$i]['vehicle_categorie_sk_id'] = 2;
            $i++;
        }

        return $marks;
    }

    private function prepareMarksData($parsedData)
    {
        $prepareMarks = [];
        foreach ($parsedData as $key => $str){
            $prepareData = explode('; ', $str[1]);
            $prepareMarks[$key] = $prepareData[0];
        }

        $prepareData = collect($prepareMarks)->unique();

        $i = 1;
        foreach ($prepareData as $key => $title)
        {
            $prepareMarks[$i] = $title;
            $i++;
        }

        return $prepareMarks;
    }

    private function getModels($parsedData)
    {
        $prepareMarks = array_flip($this->prepareMarksData($parsedData));
        $models = [];

        $i = 1;
        foreach ($parsedData as $str)
        {
            $explodedData = explode('; ', $str[1]);

            $models[$i]['id'] = $str[0];
            $models[$i]['title'] = $explodedData[1];

            #Заглушка под легковые авто
            $models[$i]['vehicle_categorie_sk_id'] = 2;
            $models[$i]['vehicle_mark_sk_id'] = $prepareMarks[$explodedData[0]];
            $i++;
        }

        return $models;
    }

}