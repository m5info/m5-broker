<?php

namespace App\Services\SK\Soglasie\Services\Dictionary;

use App\Interfaces\Services\SK\DictionaryServiceInterface;


class DictionaryService implements DictionaryServiceInterface
{
    public $api;

    public function __construct(DictionaryApi $api)
    {
        $this->api = $api;
    }

    public function get_api_purpose()
    {
        return $this->api->get_purpose();
    }

    public function get_api_categories()
    {
        return $this->api->get_categories();
    }

    public function get_marks_models()
    {
        return $this->api->get_marks_and_models();
    }
}