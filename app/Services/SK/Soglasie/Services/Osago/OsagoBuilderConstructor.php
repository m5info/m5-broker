<?php

namespace App\Services\SK\Soglasie\Services\Osago;

use App\Services\SK\Soglasie\Services\Osago\Builders\CalculateBuilder;
use App\Services\SK\Soglasie\Services\Osago\Builders\CheckConfirmBuilder;
use App\Services\SK\Soglasie\Services\Osago\Builders\ConfirmBuilder;
use App\Services\SK\Soglasie\Services\Osago\Builders\GetPDFBuilder;
use App\Services\SK\Soglasie\Services\Osago\Builders\ReturnPayLinkBuilder;
use App\Services\SK\Soglasie\Services\Osago\Builders\ScoringBuilder;

class OsagoBuilderConstructor
{
    public $calculateBuilder;
    public $scoringBuilder;
    public $confirmBuilder;
    public $checkConfirmBuilder;
    public $returnPayLinkBuilder;
    public $getPDF;

    /**
     * 1. Скоринг
     * 2. Калькуляция
     * 3. Подтверждение (загрузка) полиса
     * 4. Проверка статуса полиса, после его загрузки
     * 5. Получение ссылки на оплату
     *
     * 7. Получение PDF
     */
    public function __construct()
    {
        $this->scoringBuilder = new ScoringBuilder();
        $this->calculateBuilder = new CalculateBuilder();
        $this->confirmBuilder = new ConfirmBuilder();
        $this->checkConfirmBuilder = new CheckConfirmBuilder();
        $this->returnPayLinkBuilder = new ReturnPayLinkBuilder();
        $this->getPDF = new GetPDFBuilder();
    }
}