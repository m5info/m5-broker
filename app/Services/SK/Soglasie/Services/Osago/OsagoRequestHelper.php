<?php

namespace App\Services\SK\Soglasie\Services\Osago;

use App\Models\Contracts\OnlineCalculationLogs;

class OsagoRequestHelper
{
    /**
     * ID Сроков страхования
     */
    const INSURANCE_TERMS = [
        '1 год' => 8,
        '2 год' => 81,
        '3 год' => 82,
        '4 год' => 83,
        '5 год' => 84,
        '5 дн' => 1,
        '15 дн' => 20,
        '20 дн' => 15,
        '1 мес' => 2,
        '2 мес' => 3,
        '3 мес' => 4,
        '4 мес' => 5,
        '5 мес' => 6,
        '6 мес' => 7,
        '7 мес' => 12,
        '8 мес' => 13,
        '9 мес' => 14,
        '10 мес' => 16,
        '11 мес' => 17,
        '13 мес' => 21,
        'ПоследДеньГода' => 101
    ];

    /**
     * Документ на авто
     * Справочник: https://wiki.soglasie.ru/partners/integration/services/onlineservices/catalogue?s[]=%D0%B2%D0%B8%D0%B4&s[]=%D0%B4%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0&s[]=%D1%82%D1%81
     */
    const TS_DOC_TYPE = [
        'Техпаспорт' => 1,
        'Паспорт ТС' => 2,
        'Свидетельство о регистрации ТС' => 3,
        'Технический талон' => 4,
        'Паспорт самоходной машины' => 5,
        'Техпаспорт иностранного государства' => 6,
        'Паспорт ТС иностранного государства' => 7,
        'Свидетельство о регистрации ТС иностранного государства' => 8,
        'Технический талон иностранного государства' => 9,
        'Паспорт самоходной машины иностранного государства' => 10,
        'Талон государственного технического осмотра' => 11,
        'Талон техосмотра' => 12,
        'Международный сертификат технического осмотра' => 13,
        'Диагностическая карта' => 14,
        'Электронный паспорт транспортного средства, электронный паспорт шасси, электронный паспорт самоходной машины' => 31,
        'Аналогичные документы на ТС' => 34
    ];

    /**
     * Коды стран (надеюсь не понадобится)
     * Справочник: https://b2b.soglasie.ru/CCMC/catalog.jsp?product=93&catalog=1024
     */
    const COUNTY_CODE = [
        'Российская Федерация' => 643
    ];

    const OWNER_TYPE = [
        'Банк' => 1004,
        'Физическое лицо' => 1001,
        'Юридическое лицо' => 1002
    ];

    /**
     * [Type]
     * Справочник: https://wiki.soglasie.ru/partners/integration/services/onlineservices/catalogue?s[]=%D0%B2%D0%B8%D0%B4&s[]=%D0%B4%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0&s[]=%D1%82%D1%81
     * Тип документа страхователя/собственника
     */
    const SUBJECT_DOC_TYPE = [
        'Свидетельство о рождении' => 16,
        'Удостоверение личности офицера' => 2,
        'Справка об освобождении из места лишения свободы' => 12,
        'Паспорт Минморфлота' => 17,
        'Военный билет солдата (матроса, сержанта, старшины)' => 4,
        'Дипломатический паспорт гражданина РФ' => 5,
        'Иностранный паспорт' => 3,
        'Свидетельство о регистрации ходатайства иммигранта о признании его беженцем' => 18,
        'Вид на жительство' => 19,
        'Удостоверение беженца в РФ' => 20,
        'Временное удостоверение личности гражданина РФ' => 21,
        'Паспорт гражданина РФ' => 6,
        'Загранпаспорт гражданина РФ' => 22,
        'Свидетельство о рождении, выданное уполномоченным органом иностранного государства' => 23,
        'Паспорт моряка' => 24,
        'Военный билет офицера запаса' => 25,
        'Иные документы, выдаваемые органами МВД' => 26,
        'Паспорт гражданина СССР' => 1,
        'Загранпаспорт гражданина СССР' => 61,
        'Водительское удостоверение РФ' => 15,
        'Свидетельство о смерти' => 82,
        'Водительское удостоверение иностранного государства' => 35,
        'Другие документы' => 40
    ];

    /**
     * [TypeRSA]
     * Тип документа страхователя/собственника
     * Справочник: https://wiki.soglasie.ru/partners/integration/services/onlineservices/catalogue?s[]=typersa
     */
    const SUBJECT_DOC_TYPE_OLD = [
        "Свидетельство о рождении" => 1,
        "Удостоверение личности офицера" => 2,
        "Справка об освобождении из места лишения свободы" => 3,
        "Паспорт Минморфлота" => 4,
        "Военный билет солдата (матроса, сержанта, старшины)" => 5,
        "Дипломатический паспорт гражданина РФ" => 6,
        "Иностранный паспорт" => 7,
        "Свидетельство о регистрации ходатайства иммигранта о признании его беженцем" => 8,
        "Вид на жительство" => 9,
        "Удостоверение беженца в РФ" => 10,
        "Временное удостоверение личности гражданина РФ" => 11,
        "Паспорт гражданина РФ" => 12,
        "Загранпаспорт гражданина РФ" => 13,
        "Свидетельство о рождении, выданное уполномоченным органом иностранного государства" => 14,
        "Паспорт моряка" => 15,
        "Военный билет офицера запаса" => 16,
        "Иные документы, выдаваемые органами МВД" => 17,
        "Паспорт гражданина СССР" => 18,
        "Загранпаспорт гражданина СССР" => 19,
        "Водительское удостоверение РФ" => 20,
        "Свидетельство о смерти" => 21,
        "Водительское удостоверение иностранного государства" => 22,
        "Другие документы" => 23
    ];

    /**
     * Получаем удостоверения водителей в формате: "Серия+номер;Серия+номер"
     * Пример: 1111222222; 55556666666
     * @param $calc
     * @return string
     */
    protected function getCalcDriverLicense($calc)
    {
        $countDrivers = count($calc->contract->drivers);
        $license = '';
        $xml = '';
        if ($calc->contract->drivers_type_id != 1){
            foreach ($calc->contract->drivers as $iteration => $driver) {
                if ($countDrivers == $iteration + 1) {
                    $license .= $driver->doc_serie . $driver->doc_num;
                } else {
                    $license .= $driver->doc_serie . $driver->doc_num . ';';
                }
            }
            $xml = <<<XML
<param>
<brief>ВУ</brief>
 <val>{$license}</val>
</param>
XML;
        }

        return $xml;
    }

    protected function getCalcDriverAge($calc)
    {
        $driversAgeStr = '';
        $drivers_age = '';
        $countDrivers = count($calc->contract->drivers);

        if ($calc->contract->drivers_type_id == 0)
        {
            foreach ($calc->contract->drivers as $iteration => $driver) {
                $currentYear = date('Y');
                $driverAge = new \DateTime($driver->birth_date);
                $driverAge = $driverAge->format('Y');
                $driverAge = $currentYear - $driverAge;

                if ($countDrivers == $iteration + 1) {
                    $driversAgeStr .= "<val>{$driverAge}</val>";
                } else {
                    $driversAgeStr .= "<val>{$driverAge}</val>" . PHP_EOL;
                }

            }

            $drivers_age = <<<XML
<param>
<brief>Возраст</brief>
$driversAgeStr
</param>
XML;
        }

        return $drivers_age;
    }

    /**
     * Получаем имена водителей в формате: "Иванов Иван Иванович;Сидоров Сидр Сидорович"
     * @param $calc
     * @return string
     */
    protected function getCalcDriverNames($calc)
    {
        $countDrivers = count($calc->contract->drivers);
        $driverNames = '';
        $xml = '';

        if ($calc->contract->drivers_type_id == 0)
        {

        foreach ($calc->contract->drivers as $iteration => $driver) {

            $fio = explode(' ', $driver->fio);
            if ($countDrivers == $iteration + 1) {
                $driverNames .= $fio[0] . ' ' . $fio[1] . ' ' . $fio[2];
            } else {
                $driverNames .= $fio[0] . ' ' . $fio[1] . ' ' . $fio[2] . ';';
            }
        }

        $xml = <<<XML
<param>
    <brief>СтрокаФИОВодителей</brief>
    <val>{$driverNames}</val>
</param>
XML;
        }

        return $xml;
    }

    /**
     * Получаем дату в формате: 1997-07-08T00:00:00
     * @param $dateVal
     * @param $time
     * @param $stringTime
     * @return string
     * @throws \Exception
     */
    protected function getCalcDate($dateVal, $time = false, $stringTime = "00:00:00")
    {
        $date = new \DateTime($dateVal);
        if ($time == false){
            $prepare = $date->format('Y-m-d');
            $datetime = $prepare . 'T' . $stringTime;
        }else{
            $datetime = $date->format('Y-m-d\TH:i:s');
        }
        return $datetime;
    }

    /**
     * Период использования автомобиля в месяцах
     * @param $calc
     * @return int
     */
    /*protected function getCalcCarPeriodOdUse($calc)
    {
        $currentYear = date('Y');
        $currentMonth = date('m');

        $carYear = $calc->contract->object_insurer_auto->car_year;

        $periodOfUse = ($currentYear - $carYear) * 12 + $currentMonth;

        return $periodOfUse;
    }*/

    protected function getInsuranceTerm($calc)
    {
        return $this->prepareInsuranceTerm($calc);
    }

    private function prepareInsuranceTerm($calc)
    {
        $diffDates = $this->getDiffFromDates($calc->contract->begin_date, $calc->contract->end_date);

        $response = false;

        if ($diffDates['days'] == "5" && $diffDates['months'] == "00" && $diffDates['years'] == "00") {
            $response = self::INSURANCE_TERMS['5 дн'];
        }

        if ($diffDates['days'] == "15") {
            $response = self::INSURANCE_TERMS['15 дн'];
        }

        if ($diffDates['days'] == "20") {
            $response = self::INSURANCE_TERMS['20 дн'];
        }

        if ($diffDates['TotalDays'] == "364") {
            $response = self::INSURANCE_TERMS['1 год'];
        }

        if ($diffDates['TotalDays'] == "729") {
            $response = self::INSURANCE_TERMS['2 год'];
        }

        if ($diffDates['TotalDays'] == "1094") {
            $response = self::INSURANCE_TERMS['3 год'];
        }

        if ($diffDates['TotalDays'] == "1459") {
            $response = self::INSURANCE_TERMS['4 год'];
        }

        if ($diffDates['TotalDays'] == "1824") {
            $response = self::INSURANCE_TERMS['5 год'];
        }

        if ($diffDates['months'] == "1" && $diffDates['years'] == 0  && $diffDates['days'] == "0") {
            $response = self::INSURANCE_TERMS['1 мес'];
        }

        if ($diffDates['months'] == "2" && $diffDates['years'] == 0  && $diffDates['days'] == "0") {
            $response = self::INSURANCE_TERMS['2 мес'];
        }
        if ($diffDates['months'] == "3" && $diffDates['years'] == 0  && $diffDates['days'] == "0") {
            $response = self::INSURANCE_TERMS['3 мес'];
        }
        if ($diffDates['months'] == "4" && $diffDates['years'] == 0  && $diffDates['days'] == "0") {
            $response = self::INSURANCE_TERMS['4 мес'];
        }
        if ($diffDates['months'] == "5" && $diffDates['years'] == 0  && $diffDates['days'] == "0") {
            $response = self::INSURANCE_TERMS['5 мес'];
        }

        if ($diffDates['months'] == "6" && $diffDates['years'] == 0  && $diffDates['days'] == "0") {
            $response = self::INSURANCE_TERMS['6 мес'];
        }
        if ($diffDates['months'] == "7" && $diffDates['years'] == 0  && $diffDates['days'] == "0") {
            $response = self::INSURANCE_TERMS['7 мес'];
        }

        if ($diffDates['months'] == "8" && $diffDates['years'] == 0  && $diffDates['days'] == "0") {
            $response = self::INSURANCE_TERMS['8 мес'];
        }

        if ($diffDates['months'] == "9" && $diffDates['years'] == 0  && $diffDates['days'] == "0") {
            $response = self::INSURANCE_TERMS['9 мес'];
        }

        if ($diffDates['months'] == "10" && $diffDates['years'] == 0  && $diffDates['days'] == "0") {
            $response = self::INSURANCE_TERMS['10 мес'];
        }

        if ($diffDates['months'] == "11" && $diffDates['years'] == 0 && $diffDates['days'] == "0") {
            $response = self::INSURANCE_TERMS['11 мес'];
        }

        if ($diffDates['months'] == "12" && $diffDates['years'] == 0 && $diffDates['days'] == "0") {
            $response = self::INSURANCE_TERMS['12 мес'];
        }

        return $response;
    }

    private function getDiffFromDates($from, $to)
    {
        $fromDate = new \DateTime($from);
        $toDate = new \DateTime($to);

        $diffDate = $fromDate->diff($toDate);

        $data = [];
        $data['days'] = $diffDate->format('%d');
        $data['TotalDays'] = $diffDate->format('%a');
        $data['months'] = $diffDate->format('%m');
        $data['years'] = $diffDate->format('%Y');

        return $data;
    }

    protected function getCalcDriverKBM($calc)
    {
        $KBM = '1.00';
        if ($calc->contract->driver_type_id == 0) {
            if (count($calc->contract->drivers) == 0){
                foreach ($calc->contract->drivers as $driver) {
                    if ($driver->kbm < $KBM) {
                        $KBM = (string)$driver->kbm;
                    }
                }
            }
        }

        return (string)$KBM;
    }

    protected function getCalcOwnerType($calc)
    {
        if ($calc->contract->owner->type == 1) {
            $ownerType = self::OWNER_TYPE['Юридическое лицо'];
        } else {
            $ownerType = self::OWNER_TYPE['Физическое лицо'];
        }
        return $ownerType;
    }


    /**
     * Справочник видов удостоверения: https://b2b.soglasie.ru/diasoft-schema/graphql/entity?brief=ВидУдостоверения
     * Дефолтное значение: 6 (Паспорт гражданина РФ)
     *
     * @param $calc
     * @return int
     */
    protected function getIdentityType($calc)
    {
        return 6;
    }

    protected function getCalcDriverExperience($calc)
    {
        $driversExpStr = '';
        $driversExp = '';
        $countDrivers = count($calc->contract->drivers);

        /* Если не мультидрайв */
        if ($calc->contract->drivers_type_id == 0){
            foreach ($calc->contract->drivers as $iteration => $driver) {
                $currentYear = date('Y');
                $driverAge = new \DateTime($driver->doc_date);
                $driverAge = $driverAge->format('Y');
                $driverAge = $currentYear - $driverAge;

                if ($countDrivers == $iteration + 1) {
                    $driversExpStr .= "<val>{$driverAge}</val>";
                } else {
                    $driversExpStr .= "<val>{$driverAge}</val>" . PHP_EOL;
                }

            }
            $driversExp = <<<XML
<param>
<brief>Стаж</brief>
$driversExpStr
</param>
XML;
        }

        return $driversExp;
    }

    protected function getCutAddress($kladrAddress)
    {
        return substr($kladrAddress, 0, 13);
    }

    protected function getCalcPrevPolicy($calc)
    {
        $xml = '';
        if ($calc->contract->is_prolongation == 1)
        {
            $prevPolicy = $calc->contract->prev_policy_serie . $calc->contract->prev_policy_number;
            $xml = <<<XML
<param>
<brief>ИсходныйДоговорНомер</brief>
<val>$prevPolicy</val>
</param>
XML;
        }

        return $xml;
    }

    /**
     * Получаем объект ФИО из строки
     * Из: (string) Иванов Иван Иванович
     * В: (stdClass)
     *      first_name = Иван
     *      second_name = Иванов
     *      middle_name = Иванович
     * @param $fio
     * @return \stdClass
     */
    protected function getExplodedFio($fio)
    {
        $fioData = explode(' ', $fio);

        $preparedFio = new \stdClass();
        $preparedFio->first_name = $fioData[1];
        $preparedFio->second_name = $fioData[0];
        $preparedFio->middle_name = $fioData[2];

        return $preparedFio;
    }

    protected function getSex($sexData)
    {
        if ($sexData == '0'){
            $sex = 'М';
        }else{
            $sex = 'Ж';
        }
        return $sex;
    }

    protected function getDateYMD($dateVal)
    {
        $date = new \DateTime($dateVal);
        return $date->format('Y-m-d');
    }

    protected function getBool($data)
    {
        return (bool)$data ? "true" : "false";
    }

    protected function getUppercase($data)
    {
        return strtoupper($data);
    }

    protected function getStringSex($sex)
    {
        if ($sex == 0)
        {
            return 'male';
        }else{
            return 'female';
        }
    }

    protected function getCarDocument($calc)
    {
        $object_insurer_auto = $calc->contract->object_insurer_auto;
        if ($object_insurer_auto->doc_type == 0){
            return self::TS_DOC_TYPE['Паспорт ТС'];
        }elseif($object_insurer_auto->doc_type == 1){
            return self::TS_DOC_TYPE['Свидетельство о регистрации ТС'];
        }else{
            return self::TS_DOC_TYPE['Паспорт ТС'];
        }
    }

    protected function getSubjectDocumentType($docType)
    {
        if ($docType == 0){
            return self::SUBJECT_DOC_TYPE['Паспорт гражданина РФ'];
        }elseif($docType == 1){
            return self::SUBJECT_DOC_TYPE['Иностранный паспорт'];
        }elseif($docType == 2){
            return self::SUBJECT_DOC_TYPE['Вид на жительство'];
        }elseif ($docType == 99){
            return self::SUBJECT_DOC_TYPE['Другие документы'];
        }else{
            return self::SUBJECT_DOC_TYPE['Другие документы'];
        }
    }

    protected function getTypeRSADriverDocument()
    {
        return self::SUBJECT_DOC_TYPE_OLD['Водительское удостоверение РФ'];
    }


    protected function getCountryCode($countryName)
    {
        return self::COUNTY_CODE[$countryName];
    }

    protected function getConfirmReceipt($calc)
    {
        $payment = count($calc->contract->all_payments)>0 ? $calc->contract->all_payments->first() : false;
        $receipt = $payment && $payment->receipt ? $payment->receipt : false;
        $receiptSerie = $receipt->bso_serie ?? false;
        $paymentMethod = $payment->pay_mehtod;

        if ($receipt !== false && $paymentMethod->key_type == 0){
            $receiptPart = "<CashPaymentOption>true</CashPaymentOption>";
            $receiptSerie = "<SerialA7>$receiptSerie->bso_serie</SerialA7>";
            $receiptNumber = "<NumberA7>$receipt->bso_number</NumberA7>";
        }else{
            $receiptPart = "<CashPaymentOption>false</CashPaymentOption>";
            $receiptSerie = "";
            $receiptNumber = "";
        }

        $xml = <<<XML
$receiptPart
$receiptSerie
$receiptNumber
XML;

        return $xml;
    }

    /**
     * Блок диагностической карты в ConfirmBuilder
     * @param $calc
     * @return string
     */
    protected function getConfirmTO($calc)
    {
        $object_insurer_auto = $calc->contract->object_insurer_auto;

        $dk_number = $object_insurer_auto->dk_number;
        $dk_from = $object_insurer_auto->dk_date_from;
        $dk_to = $object_insurer_auto->dk_date;

        $dk_type = self::TS_DOC_TYPE['Диагностическая карта'];
        $next_to_year = $this->nextDay($dk_to, 'Y');
        $next_to_month = $this->nextDay($dk_to, 'm');

        $xml = '';

        if ($dk_number != '' && $dk_from != '' && $dk_to)
        {
            $xml = <<<XML
<TicketCar>
    <Type>{$dk_type}</Type>
    <Number>{$dk_number}</Number>
    <Date>{$dk_from}</Date>
    <Exit/>
    <Info/>
    <IsPrimary>true</IsPrimary>
</TicketCar>
<TicketCarYear>{$next_to_year}</TicketCarYear>
<TicketCarMonth>{$next_to_month}</TicketCarMonth>
<TicketDiagnosticDate>{$dk_from}</TicketDiagnosticDate>
XML;

        }

        //dd($object_insurer_auto);
        return $xml;
    }

    protected function nextDay($dateVal, $format)
    {
        $date = new \DateTime($dateVal);
        $date->modify('+1 day');
        return $date->format($format);
    }

    /**
     * Блок водителей в ConfirmBuilder
     * @param $calc
     * @return string
     */
    protected function getConfirmDrivers($calc)
    {
        $xml = '';
        $drivers = $calc->drivers;

        if (count($drivers) > 0 && $calc->contract->drivers_type_id == 0){
            $driversData = '';
            foreach ($drivers as $driver)
            {
                $driversData .= $this->getConfirmDriver($driver);
            }
            $xml = <<<XML
<Drivers>$driversData</Drivers>
XML;
        }
        return $xml;
    }

    private function getConfirmDriver($driver)
    {
        $fio = explode(' ', $driver->fio);
        $sex = $this->getStringSex($driver->sex);
        $xml = <<<XML
<Driver>
    <Face>
        <Resident>true</Resident>
        <Surname>{$fio[0]}</Surname>
        <Name>{$fio[1]}</Name>
        <Patronymic>{$fio[2]}</Patronymic>	
        <BirthDate>{$driver->birth_date}</BirthDate>
        <Sex>{$sex}</Sex>
        <Documents>
            <Document>
                <TypeRSA>{$this->getTypeRSADriverDocument()}</TypeRSA>
                <Serial>{$driver->doc_serie}</Serial>
                <Number>{$driver->doc_num}</Number>
                <Date>{$driver->doc_date}</Date>
            </Document>
        </Documents>
    </Face>
    <DrivingExpDate>{$driver->exp_date}</DrivingExpDate>
    <KBM>{$driver->kbm}</KBM>
</Driver>
XML;
        return $xml;
    }

    /**
     * В зависимости от ФЛ\ЮЛ собираем XML
     * Используется только в CalculateBuilder
     * @param $calc
     * @return string
     */
    protected function getCalcInsurer($calc)
    {
        $insurer = new \stdClass();
        $insurerInfo = $calc->contract->insurer;
        $insurer->insurer_type = $this->getCalcOwnerType($calc);

        if ($insurerInfo->type == 1){
            $insurerInfoInDetail = $insurerInfo->data_ul;
            $insurer->inn = $insurerInfo->inn;
            $xml = <<<XML
<param>
    <brief>СтраховательИНН</brief>
    <val>{$insurer->inn}</val>
</param>
<param>
    <brief>ТипСобственникаТС</brief>
    <val>{$insurer->insurer_type}</val>
</param>
XML;

        }else{
            $insurerInfoInDetail = $insurerInfo->data_fl;
            $insurer->fio = $this->getExplodedFio($insurerInfoInDetail->fio);
            $insurer->pass_serie = $insurerInfo->doc_serie;
            $insurer->birthdate = $insurerInfoInDetail->birthdate;
            $xml = <<<XML
<param>
    <brief>СтраховательДатаРождения</brief>
    <val>{$insurer->birthdate}</val>
</param>
<param>
    <brief>СтраховательИмя</brief>
    <val>{$insurer->fio->first_name}</val>
</param>
<param>
    <brief>СтраховательОтчество</brief>
    <val>{$insurer->fio->middle_name}</val>
</param>
<param>
    <brief>СтраховательФамилия</brief>
    <val>{$insurer->fio->second_name}</val>
</param>
<param>
    <brief>СерияПаспСтрах</brief>
    <val>{$insurer->pass_serie}</val>
</param>
<param>
    <brief>ТипСобственникаТС</brief>
    <val>{$insurer->insurer_type}</val>
</param>
XML;
        }

        return $xml;
    }

    protected function getExplodedTitleUL($title)
    {
        $titleData = explode(' ', $title);

        $preparedOrg = new \stdClass();
        $preparedOrg->OPF = $titleData[0];
        $preparedOrg->name = $titleData[1];
        $preparedOrg->name = str_replace($titleData[0].' ', '', $title);

        return $preparedOrg;
    }

    protected function shortFio($fio)
    {
        $fio = explode(' ', $fio);

        $letter1 = mb_substr($fio[1], 0, 1);
        $letter2 = mb_substr($fio[2], 0, 1);
        $response = ucfirst($fio[0]).' '.$letter1.'. '.$letter2.'.';
        $response = mb_convert_case($response, MB_CASE_TITLE, 'UTF-8');


        return $response;
    }

    protected function writeData($calc, $xml, $action)
    {

        $log = OnlineCalculationLogs::query()
            ->where('action', $action)
            ->where('contract_id', $calc->contract_id)
            ->where('online_calculation_id', $calc->id)
            ->first();

        if (!$log){
            $log = new OnlineCalculationLogs();
            $log->contract_id = $calc->contract_id;
            $log->online_calculation_id = $calc->id;
            $log->action = $action;
            $log->data_send = $xml;
        }else{
            $log->data_send = $xml;
        }

        $log->save();
    }
}