<?php

namespace App\Services\SK\Soglasie\Services\Osago\Builders;

use App\Services\SK\Soglasie\Services\Osago\OsagoRequestHelper;

class ScoringBuilder extends OsagoRequestHelper
{
    public function handle($calc)
    {
        $owner = $this->scoringOwnerParams($calc);

        $xml = <<<XML
		<getScoringId xmlns="http://scoring.b2b.soglasie.ru/">
			<request xmlns="">
				<private>
					<lastname>{$owner->fio->second_name}</lastname>
					<firstname>{$owner->fio->first_name}</firstname>
					<middlename>{$owner->fio->middle_name}</middlename>
					<birthday>{$owner->birthdate}</birthday>
					<birthplace />
					<documents>
						<document>
							<doctype>{$owner->doc_type}</doctype>
							<docseria>{$owner->doc_serie}</docseria>
							<docnumber>{$owner->doc_number}</docnumber>
							<docdatebegin>{$owner->doc_date}</docdatebegin>
							<docplace />
						</document>
					</documents>
					<addresses>
						<address>
							<type>Registered</type>
							<address>{$owner->address_full}</address>
							<city>{$owner->city}</city>
							<street>{$owner->street}</street>
							<building>{$owner->building}</building>
							<flat>{$owner->flat}</flat>
						</address>
					</addresses>
				</private>
			</request>
		</getScoringId>
XML;

        $this->writeData($calc, $xml, 'Скоринг');
        //dd($xml);
        return $xml;
    }

    private function scoringOwnerParams($calc)
    {
        $owner = new \stdClass();
        $ownerInfo = $calc->contract->owner;
        $ownerInfoInDetail = $ownerInfo->data_fl;

        $owner->sex = $ownerInfoInDetail->sex;
        $owner->fio = $this->getExplodedFio($ownerInfoInDetail->fio);
        $owner->birthdate = $ownerInfoInDetail->birthdate;
        $owner->doc_type = $this->getIdentityType($calc);
        $owner->doc_serie = $ownerInfo->doc_serie;
        $owner->doc_number = $ownerInfo->doc_number;
        $owner->doc_date = $ownerInfoInDetail->doc_date;
        $owner->kladr_code = $ownerInfoInDetail->address_register_kladr;
        $owner->street = $ownerInfoInDetail->address_register_street;
        $owner->building = $ownerInfoInDetail->address_register_block;
        $owner->flat = $ownerInfoInDetail->address_register_flat;
        $owner->city = $ownerInfoInDetail->address_register_city;
        $owner->address_full = $ownerInfoInDetail->address_register;

        return $owner;
    }
}