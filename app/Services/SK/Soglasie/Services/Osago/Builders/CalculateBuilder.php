<?php

namespace App\Services\SK\Soglasie\Services\Osago\Builders;

use App\Services\SK\Soglasie\Services\Osago\OsagoRequestHelper;

/**
 * Метод калькуляции
 **/
class CalculateBuilder extends OsagoRequestHelper
{
    public function handle($calc, $scoringId)
    {
        $car = $this->calcCarParams($calc);
        $common = $this->calcCommonParams($calc);
        $owner = $this->calcOwnerParams($calc);
        $insurerBlock = $this->calcInsurerParams($calc);
        $drivers = $this->calcDriversParams($calc);

        $xml = <<<XML
      <ns1:calcProduct>
         <data>
		   <subuser>subuser</subuser>
		   <product>
			  <brief>ОСАГО</brief>
		   </product>
		   <contract>
			  <datebeg>{$common->begin_date}</datebeg>
			  <dateend>{$common->end_date}</dateend>
			  {$drivers->driver_license}
             {$drivers->driver_ages}
			  <param>
				 <brief>ГодПостройки</brief>
				 <val>{$car->car_year}</val>
			  </param>
			  <param>
				 <brief>ГосРегТС</brief>
				 <val>{$car->car_reg_number}</val>
			  </param>
			  <param>
				 <brief>ДопускБезОграничений</brief>
				 <val>{$common->multidrive}</val>
			  </param>
			  <param>
				 <brief>ИДЗапросаСкоринг</brief>
				 <val>{$scoringId}</val>
			  </param>
			  <param>
				 <brief>VIN</brief>
				 <val>{$car->car_vin}</val>
			  </param>
			  <param>
				 <brief>МодельТС</brief>
				 <val>{$car->model_id}</val>
			  </param>
			  <param>
				 <brief>Мощность</brief>
				 <val>{$car->car_power}</val>
			  </param>
			  <param>
				 <brief>НаличиеГрубыхНарушений</brief>
				 <val>0</val>
			  </param>
			  <param>
				 <brief>НаличиеДС</brief>
				 <val>0</val>
			  </param>
			  <param>
				 <brief>ПериодИсп</brief>
				 <val>{$car->car_period_of_use}</val>
			  </param>
			  <param>
				 <brief>ПолСобственника</brief>
				 <val>{$owner->sex}</val>
			  </param>
			  <param>
				 <brief>ПризнСтрахПрицеп</brief>
				 <val>0</val>
			  </param>
			  <param>
				 <brief>Пролонгация</brief>
				 <val>{$common->prolongation}</val>
			  </param>
			  {$common->prolongation_contract}
			  <param>
				 <brief>СобственникИмя</brief>
				 <val>{$owner->fio->first_name}</val>
			  </param>
			  <param>
				 <brief>СобственникОтчество</brief>
				 <val>{$owner->fio->middle_name}</val>
			  </param>
			  <param>
				 <brief>СобственникФамилия</brief>
				 <val>{$owner->fio->second_name}</val>
			  </param>
			  <param>
				 <brief>СрокСтрах</brief>
				 <val>{$common->insurance_term}</val>
			  </param>
              {$drivers->driver_experience}
			  <param>
				 <brief>СтрахованиеПрицепаОСАГО</brief>
				 <val>0</val>
			  </param>
			  {$insurerBlock}
			  {$drivers->driver_names}
			  <param>
				 <brief>ТаксиПрокатТестДрайв</brief>
				 <val>{$common->purpose_of_use}</val>
			  </param>
			  <param>
				 <brief>ТерриторияИспользования</brief>
				 <val>{$owner->territory_of_use}</val>
			  </param>
			  <param>
				 <brief>ТипТСОСАГО</brief>
				 <val>{$car->category_id}</val>
			  </param>
			  <param>
				 <brief>ТранзитныйНомер</brief>
				 <val>{$car->transit_number}</val>
			  </param>
			  <param>
				 <brief>ТСИностранное</brief>
				 <val>{$car->is_foreign}</val>
			  </param>
			  <coeff>
				 <brief>Кбм</brief>
				 <val>{$drivers->driver_kbm}</val>
			  </coeff>
			  <datecalc>{$common->current_date}</datecalc>
		   </contract>
         </data>
      </ns1:calcProduct>
XML;
        $this->writeData($calc, $xml, 'Калькуляция');
        //dd($xml);
        return $xml;
    }


    private function calcCommonParams($calc)
    {
        $contract = $calc->contract;

        $common = new \stdClass();
        $common->multidrive = $contract->drivers_type_id;
        $common->begin_date = $this->getCalcDate($contract->begin_date);
        $common->end_date = $this->getCalcDate($contract->end_date, false, "23:59:59");
        $common->current_date = $this->getCalcDate(date('Y-m-d'), false, date('H:i:s'));
        $common->insurance_term = $this->getInsuranceTerm($calc);
        $common->purpose_of_use = $calc->sk_purpose->vehicle_purpose_sk_id;
        $common->prolongation = $contract->is_prolongation;
        $common->prolongation_contract = $this->getCalcPrevPolicy($calc);

        return $common;
    }

    private function calcDriversParams($calc)
    {
        $drivers = new \stdClass();
        $drivers->driver_license = $this->getCalcDriverLicense($calc);
        $drivers->driver_ages = $this->getCalcDriverAge($calc);
        $drivers->driver_names = $this->getCalcDriverNames($calc);
        $drivers->driver_kbm = $this->getCalcDriverKBM($calc);
        $drivers->driver_experience = $this->getCalcDriverExperience($calc);

        return $drivers;
    }

    private function calcCarParams($calc)
    {
        $car = new \stdClass();
        $object_insurer_auto = $calc->contract->object_insurer_auto;

        $car->car_year = $object_insurer_auto->car_year;
        $car->car_reg_number = $object_insurer_auto->reg_number;
        $car->car_vin = $object_insurer_auto->vin;
        $car->car_power = $object_insurer_auto->power;
        $car->car_period_of_use = 12;//$this->getCalcCarPeriodOdUse($calc);
        $car->is_foreign = $object_insurer_auto->foreign_reg_number;
        $car->model_id = $calc->sk_model->vehicle_models_sk_id;
        $car->category_id = $calc->contract->object_insurer_auto->ts_category;
        $car->transit_number = $calc->contract->object_insurer_auto->transit_number;

        return $car;
    }

    private function calcOwnerParams($calc)
    {
        $owner = new \stdClass();
        $ownerInfo = $calc->contract->owner;
        $ownerInfoInDetail = $ownerInfo->data_fl;

        $owner->sex = $this->getSex($ownerInfoInDetail->sex);
        $owner->fio = $this->getExplodedFio($ownerInfoInDetail->fio);
        $owner->territory_of_use = $this->getCutAddress($ownerInfoInDetail->address_register_kladr);

        return $owner;
    }

    private function calcInsurerParams($calc)
    {
        $insurer = $this->getCalcInsurer($calc);
        return $insurer;
    }
}