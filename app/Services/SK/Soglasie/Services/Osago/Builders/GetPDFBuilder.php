<?php

namespace App\Services\SK\Soglasie\Services\Osago\Builders;

use App\Services\SK\Soglasie\Services\Osago\OsagoRequestHelper;

class GetPDFBuilder extends OsagoRequestHelper
{
    public function handle($calc, $entity)
    {
        $params = $this->getParams($calc);


        $xml = <<<XML
<ns1:getReport>
    <id>{$entity}</id>
    <param>
        <name>ID_POLICY</name>
        <value>{$params->object_id}</value>
    </param>
    <param>
        <name>FIO_AND_PROCURACY_INSHURER</name>
        <value>{$params->insurer_fio}</value>
    </param>
    <param>
        <name>FIO_AND_PROCURACY_ASSURER</name>
        <value>{$params->owner_fio}</value>
    </param>
    <param>
        <name>FIO_ASSURER</name>
        <value>{$params->owner_fio}</value>
    </param>
    <param>
        <name>PROCURACY_ASSURER</name>
        <value>№ 2905-Д/14 от 01.09.2014</value>
    </param>
    <param>
        <name>OFFSET_X</name>
        <value>0</value>
    </param>
    <param>
        <name>OFFSET_Y</name>
        <value>0</value>
    </param>
    <param>
        <name>OUT_TYPE</name>
        <value>pdf</value>
    </param>
    <param>
        <name>ISBSOPRINT</name>
        <value>{$params->a7}</value>
    </param>
</ns1:getReport>
XML;
        $this->writeData($calc, $xml, 'Получить PDF');
        //dd($xml);
        return $xml;
    }

    private function getParams($calc)
    {
        $params = new \stdClass();
        $json = json_decode($calc->json);
        $params->entity_id = $json->EntityID;
        $params->object_id = $json->ObjectID;

        $params->insurer_fio = $calc->contract->insurer->get_info()->fio;
        $params->owner_fio = $this->shortFio($calc->contract->owner->get_info()->fio);

        // A7
        $payment = count($calc->contract->all_payments)>0 ? $calc->contract->all_payments->first() : false;
        $receipt = $payment && $payment->receipt ? $payment->receipt : false;
        $paymentMethod = $payment->pay_mehtod;

        if ($receipt !== false && $paymentMethod->key_type == 0){
            $params->a7 = 1;
        }else{
            $params->a7 = 0;
        }

        return $params;
    }
}