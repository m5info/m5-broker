<?php

namespace App\Services\SK\Soglasie\Services\Osago\Builders;

use App\Services\SK\Soglasie\Services\Osago\OsagoRequestHelper;

class ConfirmBuilder extends OsagoRequestHelper
{
    public function handle($calc)
    {
        $common = $this->commonParams($calc);
        $car = $this->carParams($calc);
        $insurerBlock = $this->insurerParams($calc);
        $owner = $this->ownerParams($calc);
        $temp = $this->tempParams($calc);
        $drivers = $this->driversParams($calc);

        $xml = <<<XML
      <ns1:LoadObject>
        <test>true</test>
        <subuser>SUBUSER</subuser>
        <package timeout="500000">
          <TemplateID>42</TemplateID>
          <Data>
            <Policy>
              <CodeInsurant>   </CodeInsurant>
              <Department> </Department>
              <Office> </Office>
              <BSOSeries>{$common->bso->serie}</BSOSeries>
              <BSONumber>{$common->bso->number}</BSONumber>
              <DeclarationDate>{$common->declaration_date}</DeclarationDate>
              <BeginDate>{$common->begin_date}</BeginDate>
              <EndDate>{$common->end_date}</EndDate>
              <Period1Begin>{$common->period_start}</Period1Begin>
              <Period1End>{$common->period_end}</Period1End>
              <InsPrem>{$common->sum}</InsPrem>
              <IsTransCar>{$car->is_foreign}</IsTransCar>
              <IsInsureTrailer>{$car->transit_number}</IsInsureTrailer>
              <OwnerKBM>{$temp->{'Кбм'}}</OwnerKBM>
              <IsGrossViolation>false</IsGrossViolation>
              <BaseTariff>{$temp->{'БазовыйТариф'}}</BaseTariff>
              <KT>{$temp->{'Кт'}}</KT>
              <KBM>{$temp->{'Кбм'}}</KBM>
              <KVS>{$temp->{'Квс'}}</KVS>
              <KO>{$temp->{'Ко'}}</KO>
              <KM>{$temp->{'Км'}}</KM>
              <KS>{$temp->{'Кс'}}</KS>
              <KP>{$temp->{'Кп'}}</KP>
              <KN></KN>
              <KPr></KPr>
              <CarInfo>
                <VIN>{$car->vin}</VIN>
                <BodyNumber/>
                <ChassisNumber/>
                <LicensePlate>{$car->reg_number}</LicensePlate>
                <MarkModelCarCode>{$car->model_id}</MarkModelCarCode>
                <MarkPTS>{$car->mark_title}</MarkPTS>
                <ModelPTS>{$car->model_title}</ModelPTS>
                <YearIssue>{$car->year}</YearIssue>
                <DocumentCar>
                  <Type>{$car->document_type}</Type>
                  <Serial>{$car->doc_serie}</Serial>
                  <Number>{$car->doc_number}</Number>
                  <Date>{$car->doc_date}</Date>
                  <Exit/>
                  <Info/>
                  <IsPrimary>true</IsPrimary>
                </DocumentCar>
                {$common->to}
                <EngCap>{$car->power}</EngCap>
                <Rented>false</Rented>
              </CarInfo>
              <Insurer>
                {$insurerBlock}
              </Insurer>
              <CarOwner>
                <Phisical>
                  <Resident>true</Resident>
                  <PBOUL>false</PBOUL>
                  <Surname>{$owner->fio->second_name}</Surname>
                  <Name>{$owner->fio->first_name}</Name>
                  <Patronymic>{$owner->fio->middle_name}</Patronymic>
                  <BirthDate>{$owner->birthdate}</BirthDate>
                  <Sex>{$owner->sex}</Sex>
                  <Documents>
                    <Document>
                      <Type>{$owner->doc_type}</Type>
                      <Serial>{$owner->pass_serie}</Serial>
                      <Number>{$owner->pass_number}</Number>
                      <Date>{$owner->pass_date_of_issue}</Date>
                      <Exit>{$owner->doc_issued}</Exit>
                      <IsPrimary>true</IsPrimary>
                    </Document>
                  </Documents>
                  <Addresses>
                    <Address>
                      <Type>Registered</Type>
                      <Country>{$owner->country_code}</Country>
                      <AddressCode>{$owner->address_code}</AddressCode>
                      <Street>{$owner->street}</Street>
                      <Hous>{$owner->house}</Hous>
                      <Flat>{$owner->flat}</Flat>
                      <Index>{$owner->zip}</Index>
                      <AddressString>{$owner->zip} {$owner->address_full}</AddressString>
                      <IsPrimary>true</IsPrimary>
                    </Address>
                  </Addresses>
                  <PhoneMobile>{$owner->phone}</PhoneMobile>
                </Phisical>
              </CarOwner>
              {$drivers->drivers_block}
              <ReqScorID>{$temp->scoringId}</ReqScorID>
              <IKP1> </IKP1>
              <CommissIKP1>0</CommissIKP1>
              {$common->receipt}
            </Policy>
          </Data>
        </package>
      </ns1:LoadObject>
XML;

        $this->writeData($calc, $xml, 'Загрузка договора');
        //dd($xml);
        return $xml;
    }

    private function commonParams($calc)
    {
        $params = new \stdClass();
        $bso = $calc->contract->bso;

        $params->bso = new \stdClass();
        $params->bso->number = $bso->bso_number;
        $params->bso->serie = $bso->bso_serie->bso_serie;
        $params->declaration_date = $this->getCalcDate(date('Y-m-d H:i:s'), true);
        $params->period_start = $this->getDateYMD($calc->contract->begin_date);
        $params->period_end = $this->getDateYMD($calc->contract->end_date);
        $params->begin_date = $this->getCalcDate($calc->contract->begin_date);
        $params->end_date = $this->getCalcDate($calc->contract->end_date, false, "23:59:59");
        $params->sum = $calc->sum;
        $params->receipt = $this->getConfirmReceipt($calc);
        $params->to = $this->getConfirmTO($calc);

        return $params;
    }

    private function carParams($calc)
    {
        $car = new \stdClass();
        $object_insurer_auto = $calc->contract->object_insurer_auto;

        $car->is_foreign = $this->getBool($object_insurer_auto->foreign_reg_number);
        $car->transit_number = $this->getBool($calc->contract->object_insurer_auto->transit_number);
        $car->vin = $object_insurer_auto->vin;
        $car->reg_number = $object_insurer_auto->reg_number;
        $car->model_id = $calc->sk_model->vehicle_models_sk_id;
        $car->year = $object_insurer_auto->car_year;
        $car->document_type = $this->getCarDocument($calc);
        $car->power = $object_insurer_auto->power;

        $car->doc_serie = $object_insurer_auto->docserie;
        $car->doc_number = $object_insurer_auto->docnumber;
        $car->doc_date = $object_insurer_auto->docdate;

        $car->mark_title = $this->getUppercase($object_insurer_auto->mark->title);
        $car->model_title = $this->getUppercase($object_insurer_auto->model->title);

//        dd($car);
        return $car;
    }

    private function insurerParams($calc)
    {
        $insurer = new \stdClass();
        $insurerInfo = $calc->contract->insurer;

        if($insurerInfo->type == 0){

            $insurerInfoInDetail = $insurerInfo->data_fl;

            $insurer->fio = $this->getExplodedFio($insurerInfoInDetail->fio);
            $insurer->insurer_type = $this->getCalcOwnerType($calc);
            $insurer->pass_serie = $insurerInfo->doc_serie;
            $insurer->birthdate = $insurerInfoInDetail->birthdate;
            $insurer->sex = $this->getStringSex($insurerInfoInDetail->sex);
            $insurer->pass_serie = $insurerInfoInDetail->doc_serie;
            $insurer->pass_number = $insurerInfoInDetail->doc_number;
            $insurer->pass_date_of_issue = $insurerInfoInDetail->doc_date;
            $insurer->doc_type = $this->getSubjectDocumentType($insurerInfoInDetail->doc_type);
            $insurer->doc_issued = $insurerInfoInDetail->doc_info;
            $insurer->address_code = $insurerInfoInDetail->address_register_city_kladr_id;
            $insurer->phone = $insurerInfo->phone;
            $insurer->country_code = $this->getCountryCode('Российская Федерация');

            $insurer->region = $insurerInfoInDetail->address_register_region_full;
            $insurer->city = $insurerInfoInDetail->address_register_city;
            $insurer->street = $insurerInfoInDetail->address_register_street;
            $insurer->house = $insurerInfoInDetail->address_register_house;
            $insurer->building = $insurerInfoInDetail->address_register_block;
            $insurer->flat = $insurerInfoInDetail->address_register_flat;
            $insurer->address_full = $insurerInfoInDetail->address_register;
            $insurer->zip = $insurerInfoInDetail->address_register_zip;

        $xml = <<<XML
<Phisical>
  <Resident>true</Resident>
  <PBOUL>false</PBOUL>
  <Surname>{$insurer->fio->second_name}</Surname>
  <Name>{$insurer->fio->first_name}</Name>
  <Patronymic>{$insurer->fio->middle_name}</Patronymic>
  <BirthDate>{$insurer->birthdate}</BirthDate>
  <Sex>{$insurer->sex}</Sex>
  <Documents>
    <Document>
      <Type>{$insurer->doc_type}</Type>
      <Serial>{$insurer->pass_serie}</Serial>
      <Number>{$insurer->pass_number}</Number>
      <Date>{$insurer->pass_date_of_issue}</Date>
      <Exit>{$insurer->doc_issued}</Exit>
      <IsPrimary>true</IsPrimary>
    </Document>
  </Documents>
  <Addresses>
    <Address>
      <Type>Registered</Type>
      <Country>{$insurer->country_code}</Country>
      <AddressCode>{$insurer->address_code}</AddressCode>
      <Street>{$insurer->street}</Street>
      <Hous>{$insurer->house}</Hous>
      <Flat>{$insurer->flat}</Flat>
      <Index>{$insurer->zip}</Index>
      <AddressString>{$insurer->zip} {$insurer->address_full}</AddressString>
      <IsPrimary>true</IsPrimary>
    </Address>
  </Addresses>
  <PhoneMobile>{$insurer->phone}</PhoneMobile>
</Phisical>
XML;

        }else{

            $insurerInfoInDetail = $insurerInfo->data_ul;
            $org = $this->getExplodedTitleUL($insurerInfo->title);
            $addressCode = $this->getCutAddress($insurerInfoInDetail->address_register_city_kladr_id);

            $xml = <<<XML
<Juridical>
<Resident>true</Resident>
<FullName>{$org->name}</FullName>
<OPF>{$org->OPF}</OPF>
<INN>{$insurerInfo->inn}</INN>
<Addresses>
  <Address>
    <Type>Actual</Type>
    <Country>643</Country>
    <AddressCode>{$addressCode}</AddressCode>
  </Address>
</Addresses>
</Juridical>
XML;
        }

        return $xml;
    }

    private function ownerParams($calc)
    {
        $owner = new \stdClass();
        $ownerInfo = $calc->contract->owner;
        $ownerInfoInDetail = $ownerInfo->data_fl;

        $owner->sex = $this->getStringSex($ownerInfoInDetail->sex);
        $owner->fio = $this->getExplodedFio($ownerInfoInDetail->fio);
        $owner->birthdate = $ownerInfoInDetail->birthdate;
        $owner->pass_serie = $ownerInfoInDetail->doc_serie;
        $owner->pass_number = $ownerInfoInDetail->doc_number;
        $owner->pass_date_of_issue = $ownerInfoInDetail->doc_date;
        $owner->doc_type = $this->getSubjectDocumentType($ownerInfoInDetail->doc_type);
        $owner->doc_issued = $ownerInfoInDetail->doc_info;
        $owner->address_code = $ownerInfoInDetail->address_register_city_kladr_id;
        $owner->phone = $ownerInfo->phone;
        $owner->country_code = $this->getCountryCode('Российская Федерация');

        $owner->region = $ownerInfoInDetail->address_register_region_full ?? "";
        $owner->city = $ownerInfoInDetail->address_register_city;
        $owner->street = $ownerInfoInDetail->address_register_street;
        $owner->house = $ownerInfoInDetail->address_register_house;
        $owner->building = $ownerInfoInDetail->address_register_block ?? "";
        $owner->flat = $ownerInfoInDetail->address_register_flat;
        $owner->address_full = $ownerInfoInDetail->address_register;
        $owner->zip = $ownerInfoInDetail->address_register_zip;
        //dd($owner);

        return $owner;
    }

    private function tempParams($calc)
    {
        $temp = $calc->json == null ? new \stdClass() : json_decode($calc->json);
        return $temp;
    }

    private function driversParams($calc)
    {
        $drivers = new \stdClass();
        $drivers->drivers_block = $this->getConfirmDrivers($calc);
        return $drivers;
    }
}