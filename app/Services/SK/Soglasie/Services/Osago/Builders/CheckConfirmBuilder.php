<?php

namespace App\Services\SK\Soglasie\Services\Osago\Builders;

use App\Services\SK\Soglasie\Services\Osago\OsagoRequestHelper;

class CheckConfirmBuilder extends OsagoRequestHelper
{
    public function handle($calc)
    {
        $tempParams = $this->tempParams($calc);

        $xml = <<<XML
<ns1:GetStatusObject>
    <status>
        <TemplateID>42</TemplateID>
        <PackageID>{$tempParams->package_id}</PackageID>
    </status>
</ns1:GetStatusObject>
XML;
        $this->writeData($calc, $xml, 'Проверка загрузки договора');
        //dd($xml);
        return $xml;
    }

    private function tempParams($calc)
    {
        $params = new \stdClass();
        $json = json_decode($calc->json);
        $params->package_id = $json->PackageID;

        return $params;
    }
}