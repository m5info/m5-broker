<?php

namespace App\Services\SK\Soglasie\Services\Osago\Builders;

use App\Services\SK\Soglasie\Services\Osago\OsagoRequestHelper;

class ReturnPayLinkBuilder extends OsagoRequestHelper
{
    public function handle($calc)
    {
        $tempParams = $this->tempParams($calc);

        $xml = <<<XML
<ns1:getPayInfoExtended>
    <PayDocNumber>9000000001843{$tempParams->object_id}</PayDocNumber>
</ns1:getPayInfoExtended>
XML;

        $this->writeData($calc, $xml, 'Получение ссылки на оплату');

        return $xml;
    }

    private function tempParams($calc)
    {
        $params = new \stdClass();
        $json = json_decode($calc->json);
        $params->entity_id = $json->EntityID;
        $params->object_id = $json->ObjectID;

        return $params;
    }
}