<?php

namespace App\Services\SK\Soglasie\Services\Osago;

use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\ContractsCalculation;

class OsagoService implements ProductServiceInterface{

    private $api;

    public function __construct(OsagoApi $api){
        $this->api = $api;
    }


    public function temp_calc(ContractsCalculation $calc)
    {

    }


    public function calc(ContractsCalculation $calc)
    {
        $calcResult = $this->api->calc($calc);
        $response = $this->prepareAnswer();

        if ($calcResult->state == true){
            $response->payment_total = $calcResult->price;
            $response->state = true;
            $response->sk_key_id = $calc->sk_key_id;
            $response->msg = $calcResult->coeff;
            $response->statys_id = 1;
            $response->tariff = $calcResult->coeff;
            $calc->messages = $calcResult->coeff;
            $calc->save();
        }else{
            $response->state = false;
            $response->error = $calcResult->error;
        }

        return $response;
    }


    public function release(ContractsCalculation $calc)
    {
        $releaseResult = $this->api->release($calc);
        $response = $this->prepareAnswer();

        if ($releaseResult->state == true){
            $response->statys_id = 4;
            $response->state = true;
            $response->bso_id = $releaseResult->bso_id;
        }else{
            $response->state = false;
            $response->error = $releaseResult->error;
        }
        return $response;
    }


    public function check_status(ContractsCalculation $calc)
    {

    }


    public function get_files(ContractsCalculation $calc)
    {
        $getFilesResult = $this->api->get_files($calc);
        $response = $this->prepareAnswer();
        return $response;
    }

    /**
     * Генерим объект ответа
     * @return \stdClass
     */
    private function prepareAnswer()
    {
        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';

        return $response;
    }

}