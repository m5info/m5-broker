<?php

namespace App\Services\SK\Soglasie\Services\Osago;

trait OsagoValidator
{
    /**
     * Общий валидор на все методы
     * @param $calc
     */
    public function validate($calc)
    {
        $ownerInfo = $calc->contract->owner;
        $this->validateCommonParams($calc);
        $this->validateInsurerParams($calc);
        $this->validateOwnerParams($calc);
        $this->validateCarParams($calc);

        if ($ownerInfo->type == 1){
            $this->errors[] = 'Владелец авто не может быть юридическим лицом';
        }
    }

    public function returnErrors()
    {
        $response = new \stdClass();
        $response->state = false;
        $response->error = '';

        foreach ($this->errors as $key => $err)
        {
            $key += 1;
            $response->error .= $key.'. '.$err.'<br>';
        }
        return $response;
    }

    private function validateCommonParams($calc)
    {
        $contract = $calc->contract;
        if (empty($contract->begin_date))
        {
            $this->errors[] = 'Нет даты начала договора';
        }else{

            $bd = new \DateTime($contract->begin_date);
            $bd = $bd->format('Y-m-d');
            $cd = date('Y-m-d');
            if ($cd > $bd){
                $this->errors[] = 'Дата начала договора не может быть задана задним числом';
            }
        }

        if (empty($contract->end_date))
        {
            $this->errors[] = 'Нет даты окончания договора';
        }

        if ($contract->is_prolongation)
        {
            if (empty($contract->prev_policy_number)){
                $this->errors[] = 'Нет номера договора пролонгации';
            }
            if (empty($contract->prev_policy_serie)){
                $this->errors[] = 'Нет серии договора пролонгации';
            }
        }

        if ($contract->drivers_type_id == 0){

            if (count($contract->drivers) != 0){
                foreach ($contract->drivers as $key => $driver)
                {
                    $this->validateDriver($driver, $key+1);
                }
            }else{
                $this->errors[] = 'Не может быть не мультидрайв и без водителей';
            }

        }
    }

    private function validateInsurerParams($calc)
    {
        $insurerInfo = $calc->contract->insurer;
        if ($insurerInfo->type == 0){
            $this->validateSubjectFL($insurerInfo, $insurerInfo->data_fl, 'страхователя');
        }else{
            $this->validateSubjectUL($insurerInfo, $insurerInfo->data_ul, 'страхователя');
        }
    }

    private function validateSubjectFL($subjectInfo, $subjectInfoInDetail, $subjectName)
    {
        try{
            $this->getExplodedFio($subjectInfoInDetail->fio);
        }catch(\Exception $e){
            $this->errors[] = 'Имя '.$subjectName.' должно быть вида: "Иван Иванов Иванович"';
        }

        if (empty($subjectInfoInDetail->birthdate))
        {
            $this->errors[] = 'Нет даты рождения '.$subjectName;
        }

        if (empty($subjectInfo->phone))
        {
            $this->errors[] = 'Нет телефона '.$subjectName;
        }

        if (strlen($subjectInfo->phone) !== 18){
            $this->errors[] = 'Телефон '.$subjectName.' должен быть формата: +7 (999) 999-99-99';
        }

        if (empty($subjectInfoInDetail->doc_serie)){
            $this->errors[] = 'Нет серии документа '.$subjectName;
        }

        if (empty($subjectInfoInDetail->doc_number)){
            $this->errors[] = 'Нет номера документа '.$subjectName;
        }

        if(empty($subjectInfoInDetail->doc_date) || $subjectInfoInDetail->doc_date == '0000-00-00'){
            $this->errors[] = 'Нет даты выдачи документа '.$subjectName;
        }

        if(empty($subjectInfoInDetail->doc_info)){
            $this->errors[] = 'Нет данных кем выдан документ '.$subjectName;
        }

        if (empty($subjectInfoInDetail->address_register_city_kladr_id)){
            $this->errors[] = 'Нет города регистрации '.$subjectName;
        }

        if (empty($subjectInfoInDetail->address_register_street)){
            $this->errors[] = 'Нет улицы регистрации '.$subjectName;
        }

        if (empty($subjectInfoInDetail->address_register_house)){
            $this->errors[] = 'Нет дома регистрации '.$subjectName;
        }
        //+7 (920) 028-74-93


    }

    protected function validateSubjectUL($subjectInfo, $subjectInfoInDetail, $subjectName)
    {
        if (empty($subjectInfo->inn))
        {
            $this->errors[] = 'Нет ИНН '.$subjectName;
        }

        if (empty($subjectInfo->title))
        {
            $this->errors[] = 'Нет названия компании '.$subjectName;
        }

        try{
            $this->getExplodedTitleUL($subjectInfo->title);
        }catch(\Exception $e){
            $this->errors[] = 'Название компании '.$subjectName.' указано неверно. Пример: "000 Еврогарант"';
        }

        if (empty($subjectInfoInDetail->address_register_city_kladr_id)){
            $this->errors[] = 'Укажите корректный адрес регистрации '.$subjectName;
        }

    }

    protected function getExplodedFio($fio)
    {
        $fioData = explode(' ', $fio);

        $preparedFio = new \stdClass();
        $preparedFio->first_name = $fioData[1];
        $preparedFio->second_name = $fioData[0];
        $preparedFio->middle_name = $fioData[2];

        return $preparedFio;
    }

    protected function validateOwnerParams($calc)
    {
        $ownerInfo = $calc->contract->owner;
        if ($ownerInfo->type == 0){
            $this->validateSubjectFL($ownerInfo, $ownerInfo->data_fl, 'собственника');
        }else{
            $this->validateSubjectUL($ownerInfo, $ownerInfo->data_ul, 'собственника');
        }
    }

    protected function validateDriver($driver, $i)
    {
        try{
            $this->getExplodedFio($driver->fio);
        }catch(\Exception $e){
            $this->errors[] = 'Имя водителя #'.$i.' должно быть вида: "Иван Иванов Иванович"';
        }

        if ($driver->birth_date == '0000-00-00'){
            $this->errors[] = 'Нет даты рождения водителя #'.$i;
        }

        if ($driver->doc_date == '0000-00-00'){
            $this->errors[] = 'Нет даты выдачи В.У. водителя #'.$i;
        }

        if (empty($driver->doc_serie)){
            $this->errors[] = 'Нет серии В.У. водителя #'.$i;
        }

        if (empty($driver->doc_num)){
            $this->errors[] = 'Нет номера В.У. водителя #'.$i;
        }

        if ($driver->exp_date == '0000-00-00'){
            $this->errors[] = 'Нет даты начала водительского стажа водителя #'.$i;
        }

        if ($driver->kbm == '0.00'){
            $this->errors[] = 'Проверьте корректность данных водителя #'.$i.'. КМБ не неизвестен из-за некорректных данных.';
        }
    }

    protected function validateCarParams($calc)
    {
        $object_insurer_auto = $calc->contract->object_insurer_auto;

        if ($object_insurer_auto->power == '0.00')
        {
            $this->errors[] = 'Мощность Т.С. не может быть "0.00"';
        }

        if ($object_insurer_auto->car_year == 0){
            $this->errors[] = 'Не указан год выпуска Т.С.';
        }

        if (empty($object_insurer_auto->vin)){
            $this->errors[] = 'VIN-номер Т.С. не может быть пустым';
        }

        if ($calc->sk_model == null){
            $this->errors[] = 'Укажите марку и модель Т.С. Если вы указали, то проверьте соответствие данной марки/модели в справочнике.';
        }

        if (empty($object_insurer_auto->reg_number)){
            $this->errors[] = 'Рег. номер Т.С. не указан';
        }
    }

    protected function getExplodedTitleUL($title)
    {
        $titleData = explode(' ', $title);

        $preparedOrg = new \stdClass();
        $preparedOrg->OPF = $titleData[0];
        $preparedOrg->name = $titleData[1];
        $preparedOrg->name = str_replace($titleData[0], '', $title);

        return $preparedOrg;
    }
}