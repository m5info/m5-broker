<?php

namespace App\Services\SK\Soglasie\Services\Osago;

use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\Contracts\OnlineCalculationLogs;
use App\Models\File;
use App\Services\SK\Soglasie\Services\SendAPI\Send;

class OsagoApi
{
    use OsagoValidator;
    private $url;
    private $login;
    private $password;
    private $sender;
    private $errors = [];
    private $builder;
    private $result;
    private $tempCalc;

    const METHODS_DEV = [
        'calc' => 'CCM/CCMPort.wsdl',
        'scoring' => 'scoring/scoring?wsdl',
        'uploadContract' => 'schema/upload-test/upload.wsdl',
        'returnPayLink' => 'diasoft/acquiring?wsdl',
        'getPDF' => 'upload-test/services/report?wsdl'
    ];

    const METHODS_PROD = [
        'calc' => 'CCM/CCMPort.wsdl',
        'scoring' => 'scoring/scoring?wsdl',
        'uploadContract' => 'schema/upload2/upload.wsdl',
        'returnPayLink' => 'diasoft/acquiring?wsdl',
        'getPDF' => 'schema/upload2/report.wsdl'
    ];

    private function getMethodsConst($calc)
    {
        $env = $calc->bso_supplier->api_setting->work_type;
        if ($env == 0){
            return self::METHODS_DEV;
        }else{
            return self::METHODS_PROD;
        }
    }

    public function __construct($url, $login, $password, $api_setting)
    {
        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->sender = new Send($url, $login, $password, $api_setting);
        $this->builder = new OsagoBuilderConstructor();
        ini_set('max_execution_time', 600);
    }

    /**
     * Предварительный расчет
     * Метод SOAP: calcProduct - https://wiki.soglasie.ru/partners/integration/services/calcservice/calc/osagocalc
     * @param ContractsCalculation $calc
     * @return \stdClass
     */
    public function calc(ContractsCalculation $calc)
    {
        $this->validate($calc);
        if (count($this->errors)>0){
            return $this->returnErrors();
        }

        $calc->json = '';
        $calc->save();
        $client = $this->sender->getClient($this->getMethodsConst($calc)['scoring'], $calc, 'Скоринг');
        $params = new \SoapVar($this->builder->scoringBuilder->handle($calc), XSD_ANYXML);
        try{

            $responseData = $client->getScoringId($params);
            $this->writeDataLog($calc, $responseData, 'Скоринг');

            $scoringId = $responseData->response->scoringid;
            $this->editTempData($calc,'scoringId', $scoringId);
        }catch(\SoapFault $e)
        {
            dd("Ошибка получения Скоринга", $e);
        }

        $params = new \SoapVar($this->builder->calculateBuilder->handle($calc, $scoringId), XSD_ANYXML);
        //dd($params);
        $client = $this->sender->getClient($this->getMethodsConst($calc)['calc'], $calc, 'Калькуляция');
        $response = new \stdClass();

        try{
            $result = $client->calcProduct($params);
            $this->writeDataLog($calc, $result, 'Калькуляция');
            //dd($result);
            $this->result = $result;
            $response->price = $result->data->contract->result;
            $response->coeff = '';
            $response->state = true;

            foreach ($result->data->contract->coeff as $coeff)
            {
                if (isset($coeff->result)){
                    $response->coeff .= $coeff->name.': '.$coeff->result.' ';
                    $this->editTempData($calc, $coeff->brief, $coeff->result);
                }
            }
            $this->tempCalc->save();

        }catch(\Exception $e){
            $response = $this->prepareErrorResponse();
        }
        return $response;
    }

    /**
     * Подтверждение полиса
     * Метод SOAP: LoadObject - https://wiki.soglasie.ru/partners/integration/services/loadservice/start
     * Шаблон для ОСАГО: https://wiki.soglasie.ru/partners/integration/services/loadservice/templates/42_osagotemplate/start
     * Шаблон для E-ОСАГО: https://wiki.soglasie.ru/partners/integration/services/loadservice/templates/eosagotemplate/start
     * @param ContractsCalculation $calc
     * @return \stdClass
     */
    public function release(ContractsCalculation $calc)
    {
        try{

            $json = json_decode($calc->json);
            $client = $this->sender->getClient($this->getMethodsConst($calc)['uploadContract'], $calc, 'Проверка загрузки договора');

            if (!isset($json->PackageID)){
                $this->downloadPackage($calc);
            }

            $params = new \SoapVar($this->builder->checkConfirmBuilder->handle($calc), XSD_ANYXML);
            $result = $client->GetStatusObject($params);
            $this->writeDataLog($calc, $result, 'Проверка загрузки договора');

            if (isset($result->response->Error)){
                $this->downloadPackage($calc);
                $result = $client->GetStatusObject($params);
                $this->writeDataLog($calc, $result, 'Проверка загрузки договора');
            }

            $this->editTempData($calc, 'EntityID', $result->response->List->Object->EntityID);
            $this->editTempData($calc, 'ObjectID', $result->response->List->Object->ObjectID);

            // Получение ссылки

                /*$client = $this->sender->getClient($this->getMethodsConst($calc)['returnPayLink'], $calc, 'Получить PDF');
                $params = new \SoapVar($this->builder->returnPayLinkBuilder->handle($calc), XSD_ANYXML);
                $result = $client->getPayInfoExtended($params);
                $this->writeDataLog($calc, $result, 'Получение ссылки на оплату');*/


            $response = new \stdClass();
            $response->state = true;
            $response->bso_id = $calc->contract->bso_id;

            $this->get_files($calc);

        }catch(\Exception $e)
        {
            dd($e);
            $response = new \stdClass();
            $response->state = false;
            $response->error = 'Ошибка: '.$e->getMessage().'. Что-то пошло не так. Вернитесь в расчет';
        }

        dd(1);

        return $response;
    }

    public function get_files($calc)
    {
        $calc->contract->masks()->detach();
        $this->get_file($calc, 'osagopolicy');
        $this->get_file($calc, 'osagopolicyback');
        $this->get_file($calc, 'osagonotice');
    }

    private function prepareErrorResponse()
    {
        $response = new \stdClass();
        $errors = $this->result->data->contract->error;
        $response->error = '';

        if (count($errors) > 1){
            $i = 1;
            foreach ($errors as $error)
            {
                if ($i !== 1){
                    $response->error .= $error->{'_'}." ";
                }
                $i++;
            }
        }
        $response->state = false;
        return $response;
    }

    /**
     * Обновляем темповые данные в калькуляции
     * @param $calc
     * @param $name
     * @param $value
     * @param bool $save
     * @return mixed
     */
    private function editTempData($calc, $name, $value, $save = true)
    {
        $json = $calc->json == null ? new \stdClass() : json_decode($calc->json);
        $json->{$name} = $value;
        $calc->json = json_encode($json, JSON_UNESCAPED_UNICODE);
        if ($save === true){
            $calc->save();
        }
        $this->tempCalc = $calc;
        return $calc;
    }

    private function downloadPackage($calc)
    {
        $params = new \SoapVar($this->builder->confirmBuilder->handle($calc), XSD_ANYXML);

        $client = $this->sender->getClient($this->getMethodsConst($calc)['uploadContract'], $calc, 'Загрузка договора');
        $result = $client->LoadObject($params);

        $this->writeDataLog($calc, $result, 'Загрузка договора');
        try{
            $this->editTempData($calc, 'PackageID', $result->response->PackageID);
        }catch(\Exception $e)
        {
            dd($e);
        }
        return $result;
    }

    private function get_file($calc, $entity)
    {
        $client = $this->sender->getClient($this->getMethodsConst($calc)['getPDF'], $calc, 'Получить PDF');
        $params = new \SoapVar($this->builder->getPDF->handle($calc, $entity), XSD_ANYXML);

        $responseData = $client->getReport($params);
        $this->writeDataLog($calc, $responseData, 'Получить PDF');
        $resultGetFiles = $responseData->report->data;


        $file_content = $resultGetFiles;
        $path = storage_path('app/' . Contracts::FILES_DOC . "/{$calc->contract->id}/");

        if (!is_dir(($path))){
            mkdir(($path), 0777, true);
        }

        if ($entity == 'osagopolicy'){
            $file_name = 'Осаго (передняя сторона)';
        }elseif ($entity == 'osagopolicyback'){
            $file_name = 'Осаго (задняя сторона)';
        }elseif ($entity == 'osagonotice'){
            $file_name = 'Заявление';
        }

        //$file_name = uniqid();

        file_put_contents($path .''. $file_name . '.pdf', $file_content);

        $file = File::create([
            'original_name' => $file_name.'.pdf',
            'ext' => 'pdf',
            'folder' => Contracts::FILES_DOC . "/{$calc->contract->id}/",
            'name' => $file_name,
            'user_id' => auth()->id()
        ]);

        $calc->contract->masks()->save($file);
    }

    /**
     * Пишем лог для дебага
     * @param $calc
     * @param $data_response
     * @param $action
     */
    private function writeDataLog($calc, $data_response, $action)
    {
        $log = OnlineCalculationLogs::query()
            ->where('action', $action)
            ->where('contract_id', $calc->contract_id)
            ->first();

        if ($log){
            $log->data_response = json_encode($data_response);
        }else{
            $log = new OnlineCalculationLogs();
            $log->contract_id = $calc->contract_id;
            $log->online_calculation_id = $calc->id;
            $log->action = $action;
        }

        $log->save();
    }
}
