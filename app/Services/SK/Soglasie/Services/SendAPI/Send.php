<?php

namespace App\Services\SK\Soglasie\Services\SendAPI;

use App\Models\Contracts\OnlineCalculationLogs;

class Send{

    public $url;
    public $login;
    public $password;
    public $SessionId;
    public $UserId;
    public $api_setting;

    public function __construct($url, $login, $password, $api_setting)
    {
        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->api_setting = $api_setting;
    }

    public function getClient($method, $calc, $action)
    {
        $sendUrl = "{$this->url}/{$method}";
        $this->writeDataLog($calc, $sendUrl, $action);

        $client = new \SoapClient($sendUrl, array(
            'trace' => 1,
            'exception' => 1,
            "soap_version" => SOAP_1_1,
            'encoding'=> 'UTF-8',
            'cache_wsdl' => WSDL_CACHE_NONE,
            'login' => $this->login,
            'password' => $this->password,
            'stream_context' => stream_context_create(
                array(
                    'ssl' => array(
                        'verify_peer'       => false,
                        'verify_peer_name'  => false,
                        'allow_self_signed' => true,
                    ),
                    /*'header' => array(
                        "Authorization: ".base64_encode($this->login.":".$this->password),
                    )*/
                )
            )));

        //$client = new \SoapClient($sendUrl, array());

        /*$headers[] = new \SoapHeader('http://ccm.b2b.soglasie.ru/', 'Content-Type', 'application/xml');
        $headers[] = new \SoapHeader('http://ccm.b2b.soglasie.ru/', 'Authorization', 'Basic '.base64_encode($this->login.':'.$this->password));
        $client->__setSoapHeaders($headers);*/

        return $client;
    }

    private function writeDataLog($calc, $link, $action)
    {
        $log = OnlineCalculationLogs::query()
            ->where('action', $action)
            ->where('contract_id', $calc->contract_id)
            ->where('online_calculation_id', $calc->id)
            ->first();

        if (!$log){
            $log = new OnlineCalculationLogs();
            $log->contract_id = $calc->contract_id;
            $log->online_calculation_id = $calc->id;
            $log->action = $action;
        }

        $log->link = $link;
        $log->save();
    }

}


