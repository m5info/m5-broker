<?php

namespace App\Services\SK\Energogarant\Services\Auxiliary;


use App\Models\Contracts\ContractsCalculation;

class Car{

    const MOTOR_TYPE = [
        0 => ['id'=>1, 'name'=>'Бензин', 'val'=>'P'],
        1 => ['id'=>2, 'name'=>'Дизель', 'val'=>'D'],
        2 => ['id'=>3, 'name'=>'Электро', 'val'=>'E'],
        3 => ['id'=>4, 'name'=>'Гибрид', 'val'=>'H'],
    ];


    public static function getCarOSAGO(ContractsCalculation $calc)
    {
        $contract = $calc->contract;
        $vehicle = $contract->object_insurer_auto;

        $PtsDate = '';
        $PtsSeries = '';
        $PtsNumber = '';

        $RegCertDate = '';
        $RegCertSeries = '';
        $RegCertNumber = '';

        $DocDate = setDateTimeFormatRu($vehicle->docdate, 1);

        if($vehicle->doc_type == 0){
            $PtsDate = $DocDate;
            $PtsSeries = $vehicle->docserie;
            $PtsNumber = $vehicle->docnumber;
        }else{
            $RegCertDate = $DocDate;
            $RegCertSeries = $vehicle->docserie;
            $RegCertNumber = $vehicle->docnumber;
        }


        $dk_date = '';
        if($vehicle->dk_date != '1970-01-01 00:00:00') {
            $dk_date = date("d.m.Y", strtotime($vehicle->dk_date));
        }


        if($vehicle->capacity > 0){
            $MaxLoad = str_replace('.', ',', $vehicle->capacity);
        }else{
            $MaxLoad = '';
        }

        if($vehicle->weight > 0){
            $MaxMass = str_replace('.', ',', $vehicle->weight);
        }else{
            $MaxMass = '';
        }

        $Powerhp = (int)$vehicle->power;
        $Powerkw = (int)round($Powerhp/1.36);

        $MaxLoad = str_replace(',00', '', $MaxLoad);
        $MaxMass = str_replace(',00', '', $MaxMass);

        if(!$UseForType = $calc->sk_purpose) return Car::response('Не синхронизирован справочник: Цель использования', false);
        if(!$TypeAvto = $calc->sk_category) return Car::response('Не синхронизирован справочник: Категория', false);
        if(!$BrandIsn = $calc->sk_mark) return Car::response('Не синхронизирован справочник: Марка', false);
        if(!$ModelIsn = $calc->sk_model) return Car::response('Не синхронизирован справочник: Модель', false);


        $xml = <<<XML
    
        <UseForType>{$UseForType->vehicle_purpose_sk_id}</UseForType>
        <TypeAvto>{$TypeAvto->vehicle_categorie_sk_id}</TypeAvto>
        <BrandIsn>{$BrandIsn->vehicle_marks_sk_id}</BrandIsn>
        <ModelIsn>{$ModelIsn->vehicle_models_sk_id}</ModelIsn>
        <Modification></Modification>
        
        <Vin>{$vehicle->vin}</Vin>
        <ChassisId></ChassisId>
        <BodyId>{$vehicle->body_number}</BodyId>
        <RegNumber>{$vehicle->reg_number}</RegNumber>
       
        <PtsType>5302</PtsType>
        <PtsDate>$PtsDate</PtsDate>
        <PtsSeries>{$PtsSeries}</PtsSeries>
        <PtsNumber>{$PtsNumber}</PtsNumber>
        
        <RegCountry>2570</RegCountry>
        <RegCertSeries>{$RegCertSeries}</RegCertSeries>
        <RegCertNumber>{$RegCertNumber}</RegCertNumber>
        <RegCertIssueDate>{$RegCertDate}</RegCertIssueDate>
        
        <Color></Color>
        
        <MotorType></MotorType>
        <MotorId></MotorId>
        <MotorVolume></MotorVolume>
        
        <Doors></Doors>
        <Seats>{$vehicle->passengers_count}</Seats>
        
        <MaxLoad>$MaxLoad</MaxLoad>
        <MaxMass>$MaxMass</MaxMass>
        
        <Powerhp>$Powerhp</Powerhp>
        <Powerkw>$Powerkw</Powerkw>
        
        
        <CarIssuedYear>{$vehicle->car_year}</CarIssuedYear>
        <GtoType>2</GtoType>
        <GtoSeries></GtoSeries>
        <GtoNumber>{$vehicle->dk_number}</GtoNumber>
        <GtoDate>$dk_date</GtoDate>
        <SeasonUse>0</SeasonUse>
    
XML;

        return Car::response($xml);
    }


    public static function getCarCalc(ContractsCalculation $calc)
    {
        $contract = $calc->contract;
        $vehicle = $contract->object_insurer_auto;

        $PtsDate = '';
        $PtsSeries = '';
        $PtsNumber = '';

        $RegCertDate = '';
        $RegCertSeries = '';
        $RegCertNumber = '';

        $DocDate = setDateTimeFormatRu($vehicle->docdate, 1);

        if($vehicle->doc_type == 0){
            $PtsDate = $DocDate;
            $PtsSeries = $vehicle->docserie;
            $PtsNumber = $vehicle->docnumber;
        }else{
            $RegCertDate = $DocDate;
            $RegCertSeries = $vehicle->docserie;
            $RegCertNumber = $vehicle->docnumber;
        }


        $dk_date = '';
        if($vehicle->dk_date != '1970-01-01 00:00:00') {
            $dk_date = date("d.m.Y", strtotime($vehicle->dk_date));
        }


        if($vehicle->capacity > 0){
            $MaxLoad = str_replace('.', ',', $vehicle->capacity);
        }else{
            $MaxLoad = '';
        }

        if($vehicle->weight > 0){
            $MaxMass = str_replace('.', ',', $vehicle->weight);
        }else{
            $MaxMass = '';
        }

        $Powerhp = (int)$vehicle->power;
        $Powerkw = (int)round($Powerhp/1.36);

        $MaxLoad = str_replace(',00', '', $MaxLoad);
        $MaxMass = str_replace(',00', '', $MaxMass);

        if(!$UseForType = $calc->sk_purpose) return Car::response('Не синхронизирован справочник: Цель использования', false);
        if(!$TypeAvto = $calc->sk_category) return Car::response('Не синхронизирован справочник: Категория', false);
        if(!$BrandIsn = $calc->sk_mark) return Car::response('Не синхронизирован справочник: Марка', false);
        if(!$ModelIsn = $calc->sk_model) return Car::response('Не синхронизирован справочник: Модель', false);


        $xml = <<<XML
    
        <UseForType>{$UseForType->vehicle_purpose_sk_id}</UseForType>
        <TypeAvto>{$TypeAvto->vehicle_categorie_sk_id}</TypeAvto>
        <BrandIsn>{$BrandIsn->vehicle_marks_sk_id}</BrandIsn>
        <ModelIsn>{$ModelIsn->vehicle_models_sk_id}</ModelIsn>
        <Modification></Modification>
        
        <Vin>{$vehicle->vin}</Vin>
        <ChassisId></ChassisId>
        <BodyId>{$vehicle->body_number}</BodyId>
        <RegNumber>{$vehicle->reg_number}</RegNumber>
       
        <PtsType>5302</PtsType>
        <PtsDate>$PtsDate</PtsDate>
        <PtsSeries>{$PtsSeries}</PtsSeries>
        <PtsNumber>{$PtsNumber}</PtsNumber>
        
        <RegCountry>2570</RegCountry>
        <RegCertSeries>{$RegCertSeries}</RegCertSeries>
        <RegCertNumber>{$RegCertNumber}</RegCertNumber>
        <RegCertIssueDate>{$RegCertDate}</RegCertIssueDate>
        
        <Color></Color>
        
        <MotorType></MotorType>
        <MotorId></MotorId>
        <MotorVolume></MotorVolume>
        
        <Doors></Doors>
        <Seats>{$vehicle->passengers_count}</Seats>
        
        <MaxLoad>$MaxLoad</MaxLoad>
        <MaxMass>$MaxMass</MaxMass>
        
        <Powerhp>$Powerhp</Powerhp>
        <Powerkw>$Powerkw</Powerkw>
        
        
        <CarIssuedYear>{$vehicle->car_year}</CarIssuedYear>
        <GtoType>2</GtoType>
        <GtoSeries></GtoSeries>
        <GtoNumber>{$vehicle->dk_number}</GtoNumber>
        <GtoDate>$dk_date</GtoDate>
        <SeasonUse>0</SeasonUse>
    
XML;

        return Car::response($xml);
    }

    public static function getCarKasko(ContractsCalculation $calc)
    {
        $contract = $calc->contract;
        $vehicle = $contract->object_insurer_auto;


        $PtsDate = setDateTimeFormatRu($vehicle->docdate, 1);
        $PtsSeries = $vehicle->docserie;
        $PtsNumber = $vehicle->docnumber;

        $RegCertDate = setDateTimeFormatRu($vehicle->sts_docdate, 1);;
        $RegCertSeries = $vehicle->sts_docserie;
        $RegCertNumber = $vehicle->sts_docnumber;


        if($vehicle->capacity > 0){
            $MaxLoad = str_replace('.', ',', $vehicle->capacity);
        }else{
            $MaxLoad = '';
        }

        if($vehicle->weight > 0){
            $MaxMass = str_replace('.', ',', $vehicle->weight);
        }else{
            $MaxMass = '';
        }

        $Powerhp = (int)$vehicle->power;
        $Powerkw = (int)round($Powerhp/1.36);

        $MaxLoad = str_replace(',00', '', $MaxLoad);
        $MaxMass = str_replace(',00', '', $MaxMass);

        if(!$UseForType = $calc->sk_purpose) return Car::response('Не синхронизирован справочник: Цель использования', false);
        if(!$TypeAvto = $calc->sk_category) return Car::response('Не синхронизирован справочник: Категория', false);
        if(!$BrandIsn = $calc->sk_mark) return Car::response('Не синхронизирован справочник: Марка', false);
        if(!$ModelIsn = $calc->sk_model) return Car::response('Не синхронизирован справочник: Модель', false);
        if(!$ColorIsn = $calc->sk_color) return Car::response('Не синхронизирован справочник: Цвета', false);
        if(!$AntiTheftSystemIsn = $calc->sk_anti_theft_system) return Car::response('Не синхронизирован справочник: Противоугонное устройство', false);


        $IsZalog = ($contract->bank_id > 0)?'Y':'N';


        $MotorVolume = (int)$vehicle->volume;
        $MotorType = Car::MOTOR_TYPE[(int)$vehicle->type_engine]['val'];

        $Price = '';
        if($vehicle->price > 0){
            $Price = str_replace('.', ',', $vehicle->price);
        }

        $UseYear = $vehicle->car_year_exp-1;

        $xml = <<<XML
    
        <TypeAvto>{$TypeAvto->vehicle_categorie_sk_id}</TypeAvto>
        <Classific></Classific>
        <BrandIsn>{$BrandIsn->vehicle_marks_sk_id}</BrandIsn>
        <ModelIsn>{$ModelIsn->vehicle_models_sk_id}</ModelIsn>
        <Modification>{$vehicle->modification}</Modification>
        
        <CarIssuedYear>{$vehicle->car_year}</CarIssuedYear>
        <UseYear>$UseYear</UseYear>
        
        
        
        <Vin>{$vehicle->vin}</Vin>
        <BodyId>{$vehicle->body_number}</BodyId>
        <ChassisId>{$vehicle->body_chassis}</ChassisId>
        <RegNumber>{$vehicle->reg_number}</RegNumber>
        
        
        <PtsType>5302</PtsType>
        <PtsDate>$PtsDate</PtsDate>
        <PtsSeries>{$PtsSeries}</PtsSeries>
        <PtsNumber>{$PtsNumber}</PtsNumber>
        
        <RegCountry>2570</RegCountry>
        <RegCertSeries>{$RegCertSeries}</RegCertSeries>
        <RegCertNumber>{$RegCertNumber}</RegCertNumber>
        <RegCertIssueDate>{$RegCertDate}</RegCertIssueDate>
        
        
        <Color>{$ColorIsn->color_sk_id}</Color>
        
        
        <Antijack>{$AntiTheftSystemIsn->anti_theft_system_sk_id}</Antijack>
        
        <IsZalog>$IsZalog</IsZalog>
        
        <MotorType>{$MotorType}</MotorType>
        <MotorId>{$vehicle->body_engine}</MotorId>
        <MotorVolume>{$MotorVolume}</MotorVolume>
        
        
        <Doors>{$vehicle->doors}</Doors>
        <Keys>{$vehicle->keys}</Keys>
        <Seats>{$vehicle->passengers_count}</Seats>
        <MaxLoad>$MaxLoad</MaxLoad>
        <MaxMass>$MaxMass</MaxMass>
        
        <Powerhp>$Powerhp</Powerhp>
        <Powerkw>$Powerkw</Powerkw>
        
        <Country>2570</Country>
            
            
        <Price>$Price</Price>
        
        
        <UseForType>{$UseForType->vehicle_purpose_sk_id}</UseForType>
        
        
        <GtoType>2</GtoType>
        <GtoSeries></GtoSeries>
        <GtoNumber>{$vehicle->dk_number}</GtoNumber>
        <GtoDate></GtoDate>
       
    
XML;

        return Car::response($xml);
    }


    public static function response($result = '', $state = true)
    {
        return (object)['state' => $state, 'result' => $result];
    }


}