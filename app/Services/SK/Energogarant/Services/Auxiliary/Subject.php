<?php

namespace App\Services\SK\Energogarant\Services\Auxiliary;


use App\Models\Api\SK\ApiInsuranceAuxiliaryParameters;
use Illuminate\Support\Str;

class Subject{

    public $Send = null;


    public static function getSubject($subject, $insurance_companies_id, $Send)
    {
        $response = null;

        $hesh = \GuzzleHttp\json_encode($subject->get_info());
        $hesh = base64_encode($hesh);


        //Поиск в спомогательной таблице ИЩЕМ ID контрагента, если нет создаем
        $auxiliary = ApiInsuranceAuxiliaryParameters::where('insurance_companies_id', $insurance_companies_id)
            ->where('type_id', 0)
            ->where('local_id', $subject->id)->get()->first();


        if($auxiliary){
            //Проверка были ли изменения если нет то возвращаем преведущий ID
            if($auxiliary->auxiliary_parameters == $hesh){
                return Subject::response($auxiliary->sk_id);
            }else{
                $auxiliary->auxiliary_parameters = $hesh;
                $auxiliary->save();
                return Subject::createOrUpdata($subject, $auxiliary->sk_id, $Send);
            }

        }else{

            $response = Subject::createOrUpdata($subject, 0, $Send);

            if($response->state == true){

                if((int)$response->result > 0){
                    ApiInsuranceAuxiliaryParameters::create([
                        'insurance_companies_id' => $insurance_companies_id,
                        'type_id' => 0,
                        'local_id' => $subject->id,
                        'sk_id' => $response->result,
                        'auxiliary_parameters' => $hesh,
                    ]);
                }
            }
        }

        return $response;

    }


    public static function getTempDrivers($drivers, $insurance_companies_id, $Send)
    {
        $res = '';
        foreach ($drivers as $driver){

            $res .= "<Driver><Age>{$driver->age}</Age><Experience_val>{$driver->exp}</Experience_val></Driver>";
        }

        return Subject::response($res);
    }

    public static function getDrivers($drivers, $insurance_companies_id, $Send)
    {
        $res = '';

        foreach ($drivers as $driver){



            if($driver->subject_id > 0){
                $auxiliary = ApiInsuranceAuxiliaryParameters::where('insurance_companies_id', $insurance_companies_id)
                    ->where('type_id', 0)
                    ->where('local_id', $driver->subject_id)->get()->first();
                if($auxiliary){
                    $res .= "<Driver><SubjectId>".$auxiliary->sk_id."</SubjectId></Driver>";
                }
            }else{

                $hesh = \GuzzleHttp\json_encode($driver);
                $hesh = base64_encode($hesh);

                $auxiliary = ApiInsuranceAuxiliaryParameters::where('insurance_companies_id', $insurance_companies_id)
                    ->where('type_id', 1)
                    ->where('local_id', $driver->id)->get()->first();

                if($auxiliary){

                    if($auxiliary->auxiliary_parameters == $hesh){
                        $res .= "<Driver><SubjectId>".$auxiliary->sk_id."</SubjectId></Driver>";
                    }else{

                        $response = Subject::createOrUpdataDriver($driver, $auxiliary->sk_id, $Send);
                        if($response->state == true){

                            $auxiliary->auxiliary_parameters = $hesh;
                            $auxiliary->save();

                            $res .= "<Driver><SubjectId>".$response->result."</SubjectId></Driver>";

                        }else{
                            return Subject::response($response->result, false);
                        }

                        return Subject::createOrUpdataDriver($driver, $auxiliary->sk_id, $Send);
                    }

                }else{
                    $response = Subject::createOrUpdataDriver($driver, 0, $Send);
                    if($response->state == true){

                        if((int)$response->result > 0){
                            ApiInsuranceAuxiliaryParameters::create([
                                'insurance_companies_id' => $insurance_companies_id,
                                'type_id' => 1,
                                'local_id' => $driver->id,
                                'sk_id' => $response->result,
                                'auxiliary_parameters' => $hesh,
                            ]);
                        }
                        $res .= "<Driver><SubjectId>".$response->result."</SubjectId></Driver>";

                    }else{
                        return Subject::response($response->result, false);
                    }
                }
            }

        }

        return Subject::response($res);

    }



    private static function createOrUpdata($subject, $SubjectId, $Send)
    {
        $docs = '';


        if($subject->type == 0) {
            $subXml = Subject::getXmlSubjectFL($subject, $SubjectId);
        }else{
            $subXml = Subject::getXmlSubjectUL($subject, $SubjectId);
        }



        $result = Subject::sendXML($subXml, $Send);
        if((int)$result->Error->Code == 0){

            if((int)$result->Answer->Subject->SubjectId > 0 && (int)$SubjectId == 0){
                return Subject::createOrUpdata($subject, (int)$result->Answer->Subject->SubjectId, $Send);
            }

            return Subject::response($SubjectId);

        }else{

            return Subject::response((string)$result->Error->Text, false);
        }
    }


    private static function createOrUpdataDriver($driver, $SubjectId, $Send)
    {
        $subXml = Subject::getXmlDriverFL($driver, $SubjectId);

        //dd($subXml);

        $result = Subject::sendXML($subXml, $Send);


        if((int)$result->Error->Code == 0){

            if((int)$result->Answer->Subject->SubjectId > 0 && (int)$SubjectId == 0){
                return Subject::createOrUpdataDriver($driver, (int)$result->Answer->Subject->SubjectId, $Send);
            }

            return Subject::response($SubjectId);

        }else{
            return Subject::response((string)$result->Error->Text, false);
        }



    }



    private static function sendXML($subXml, $Send){

        $xml = <<<XML
    <WebRequest>
    <Info> 
        <Source>{$Send->Source}</Source> 
        <Operation>SubjEdit</Operation>
        <UserId>$Send->UserId</UserId> 
        <SessionId>$Send->SessionId</SessionId>
    </Info>
    <Body> 
        $subXml
    </Body>
    </WebRequest>
XML;

        //echo $xml;
        //dd($Send->send($xml));

        return $Send->send($xml);

    }




    private static function response($result = '', $state = true)
    {
        return (object)['state' => $state, 'result' => $result];
    }





    /******************************
     *
     *
     * NEW subject
     *
     *
     ******************************/

    private static function getXmlFLDoc($subject)
    {
        $data = $subject->get_info();

        $DocDate = date("d.m.Y", strtotime($data->doc_date));

        $docs = "<Doc>
                        <DocType>kdPassportRF</DocType>
                        <DocTypeId>1186</DocTypeId>
                        <DocName>Паспорт гражданина РФ</DocName>
                        <DocSeries>".$data->doc_serie."</DocSeries>
                        <DocNumber>".$data->doc_number."</DocNumber>
                        <DocDate>".$DocDate."</DocDate>
                        <DocEndDate></DocEndDate>
                        <DocIssuedBy>".$data->doc_info."</DocIssuedBy>
                        <DocIssuedCode>".$data->doc_office."</DocIssuedCode>
                    </Doc>";

        return $docs;

    }

    private static function getXmlDriverDoc($driver)
    {

        $DocDate = date("d.m.Y", strtotime($driver->doc_date));

        $docs = "<Doc>
                        <DocType>kdDriveLicen</DocType>
                        <DocTypeId>640254630</DocTypeId>
                        <DocName>Водительское удостоверение РФ</DocName>
                        <DocSeries>".$driver->doc_serie."</DocSeries>
                        <DocNumber>".$driver->doc_num."</DocNumber>
                        <DocDate>".$DocDate."</DocDate>
                        <DocEndDate></DocEndDate>
                        <DocIssuedBy></DocIssuedBy>
                        <DocIssuedCode></DocIssuedCode>
                    </Doc>";

        return $docs;

    }


    private static function getXmlSubjectFL($subject, $SubjectId)
    {
        $data = $subject->get_info();

        $Fullname = $subject->title;
        $Birthday = date("d.m.Y", strtotime($data->birthdate));




        if((int)$SubjectId == 0) $Fullname = uniqid();

        $SubjectType = "ФЛ";
        $Resident = "Y";
        $NameLat = '';

        $Age = (int)(((countDayToDates($Birthday, date("d.m.Y")))/365));
        $Sex = ($data->sex == 0)?"М":"Ж";

        $AddressKLADR = Str::substr($data->address_register_kladr, 0, 17);
        $AddressKLADRBirth = Str::substr($data->address_born_kladr, 0, 17);
        $AddressKLADRFact = Str::substr($data->address_fact_kladr, 0, 17);

        $docs = Subject::getXmlFLDoc($subject);

        $Experience = '';
        $Experience_val = '';

        $driver = $data->driver;
        if($driver){

            $Experience = date("d.m.Y", strtotime($driver->exp_date));
            $Experience_val = (int)(((countDayToDates($Experience, date("d.m.Y")))/365));
            $docs .= Subject::getXmlDriverDoc($driver);

        }


        $Phone = parsePhoneNumber($subject->phone);
        $Email = $subject->email;

        return <<<XML
            <Subject>
                <SubjectId>$SubjectId</SubjectId>
                <SubjectType>$SubjectType</SubjectType>
                <Fullname>{$Fullname}</Fullname>
                <Resident>$Resident</Resident>
                <NameLat>$NameLat</NameLat>
                <Birthday>$Birthday</Birthday>
                <Age>$Age</Age>
                <Sex>$Sex</Sex>
                <Experience>$Experience</Experience>
                <Experience_val>$Experience_val</Experience_val>
                <Docs>$docs</Docs>
                <Phone>{$Phone}</Phone>
                <Email>{$Email}</Email>
                <Addresses>
                    <Address>
                        <AddressType>adrReg</AddressType>
                        <AddressTypeId>640236940</AddressTypeId>
                        <AddressName>Регистрация</AddressName>
                        <Address>{$data->address_register}</Address>
                        <AddressCountry>Росссия</AddressCountry>
                        <AddressKLADR>{$AddressKLADR}</AddressKLADR>
                        <AddressStreet>{$data->address_register_street}</AddressStreet>
                        <AddressHouse>{$data->address_register_house}</AddressHouse>
                        <AddressBuilding>{$data->address_register_block}</AddressBuilding>
                        <AddressFlat>{$data->address_register_flat}</AddressFlat>
                    </Address>
                </Addresses>
                <Addresses>
                    <Address>
                        <AddressType>adrFact</AddressType>
                        <AddressTypeId>8533</AddressTypeId>
                        <AddressName>Фактический</AddressName>
                        <Address>{$data->address_fact}</Address>
                        <AddressCountry>Росссия</AddressCountry>
                        <AddressKLADR>{$AddressKLADRFact}</AddressKLADR>
                        <AddressStreet>{$data->address_fact_street}</AddressStreet>
                        <AddressHouse>{$data->address_fact_house}</AddressHouse>
                        <AddressBuilding>{$data->address_fact_block}</AddressBuilding>
                        <AddressFlat>{$data->address_fact_flat}</AddressFlat>
                    </Address>
                </Addresses>
                <Addresses>
                    <Address>
                        <AddressType>adrPlBirth</AddressType>
                        <AddressTypeId>8272</AddressTypeId>
                        <AddressName>Место рождения</AddressName>
                        <Address>{$data->address_born}</Address>
                        <AddressCountry>Росссия</AddressCountry>
                        <AddressKLADR>{$AddressKLADRBirth}</AddressKLADR>
                        <AddressStreet></AddressStreet>
                        <AddressHouse></AddressHouse>
                        <AddressBuilding></AddressBuilding>
                        <AddressFlat></AddressFlat>
                    </Address>
                </Addresses>
            </Subject>
XML;
    }




    private static function getXmlDriverFL($driver, $SubjectId)
    {
        $Fullname = $driver->fio;
        $Birthday = date("d.m.Y", strtotime($driver->birth_date));

        if((int)$SubjectId == 0) $Fullname = uniqid();

        $SubjectType = "ФЛ";
        $Resident = "Y";
        $NameLat = '';

        $Age = (int)(((countDayToDates($Birthday, date("d.m.Y")))/365));
        $Sex = ($driver->sex == 0)?"М":"Ж";

        $Experience = date("d.m.Y", strtotime($driver->exp_date));
        $Experience_val = (int)(((countDayToDates($Experience, date("d.m.Y")))/365));
        $docs = Subject::getXmlDriverDoc($driver);

        return <<<XML
            <Subject>
                <SubjectId>$SubjectId</SubjectId>
                <SubjectType>$SubjectType</SubjectType>
                <Fullname>{$Fullname}</Fullname>
                <Resident>$Resident</Resident>
                <NameLat>$NameLat</NameLat>
                <Birthday>$Birthday</Birthday>
                <Age>$Age</Age>
                <Sex>$Sex</Sex>
                <Experience>$Experience</Experience>
                <Experience_val>$Experience_val</Experience_val>
                <Docs>$docs</Docs>
                <Phone></Phone>
                <Email></Email>
                <Addresses>
                    <Address>
                        <AddressType>adrReg</AddressType>
                        <AddressTypeId>640236940</AddressTypeId>
                        <AddressName>Регистрация</AddressName>
                        <Address></Address>
                        <AddressCountry>Росссия</AddressCountry>
                        <AddressKLADR></AddressKLADR>
                        <AddressStreet></AddressStreet>
                        <AddressHouse></AddressHouse>
                        <AddressBuilding></AddressBuilding>
                        <AddressFlat></AddressFlat>
                    </Address>
                </Addresses>
                <Addresses>
                    <Address>
                        <AddressType>adrFact</AddressType>
                        <AddressTypeId>8533</AddressTypeId>
                        <AddressName>Фактический</AddressName>
                        <Address></Address>
                        <AddressCountry>Росссия</AddressCountry>
                        <AddressKLADR></AddressKLADR>
                        <AddressStreet></AddressStreet>
                        <AddressHouse></AddressHouse>
                        <AddressBuilding></AddressBuilding>
                        <AddressFlat></AddressFlat>
                    </Address>
                </Addresses>
                <Addresses>
                    <Address>
                        <AddressType>adrPlBirth</AddressType>
                        <AddressTypeId>8272</AddressTypeId>
                        <AddressName>Место рождения</AddressName>
                        <Address></Address>
                        <AddressCountry>Росссия</AddressCountry>
                        <AddressKLADR></AddressKLADR>
                        <AddressStreet></AddressStreet>
                        <AddressHouse></AddressHouse>
                        <AddressBuilding></AddressBuilding>
                        <AddressFlat></AddressFlat>
                    </Address>
                </Addresses>
            </Subject>
XML;
    }














    private static function getXmlSubjectUL($all_data, $docs)
    {
        $data = (object)$all_data['ul'];

        $SubjectId = (int)$all_data["back_subject_id"];
        $SubjectType = "ЮЛ";
        $Resident = isset($data->is_resident)?((int)$data->is_resident == 1)?"Y":"N":$all_data["default_is_resident"];

        $docs_list = '';
        if($docs){
            for($i=0; $i<count($docs); $i++){

                if((isset($docs['doc_type'][$i]) || isset($docs['default_doc_type'][$i])) && strlen($docs['document_number'][$i]) > 0){

                    $doc_type_id =  $docs['doc_type'][$i];//isset($docs['doc_type'][$i])?(int)$docs['doc_type'][$i]:$docs['default_doc_type'][$i];
                    $doc_type = Dict::getDictVal('GeneralReferenceBook','DocTypes', $doc_type_id);

                    $docs_list .= "<Doc>
                        <DocType>".$doc_type->val."</DocType>
                        <DocTypeId>".$doc_type_id."</DocTypeId>
                        <DocName>".$doc_type->title."</DocName>
                        <DocSeries>".$docs['document_series'][$i]."</DocSeries>
                        <DocNumber>".$docs['document_number'][$i]."</DocNumber>
                        <DocDate>".$docs['doc_date'][$i]."</DocDate>
                        <DocEndDate></DocEndDate>
                        <DocIssuedBy>".$docs['doc_issue_org'][$i]."</DocIssuedBy>
                        <DocIssuedCode>".$docs['doc_code'][$i]."</DocIssuedCode>
                    </Doc>";

                }

            }
        }


        $AddressKLADR = Str::substr($data->subject_kladr, 0, 17);
        $AddressKLADRFact = Str::substr($data->fact_subject_kladr, 0, 17);

        //$AddressKLADR = $data->subject_kladr;
        //$AddressKLADRFact = $data->fact_subject_kladr;

        $Phone = parsePhoneNumber($data->phone);
        $Email = $data->email;

        return <<<XML
            <Subject>
                <SubjectId>$SubjectId</SubjectId>
                <SubjectType>$SubjectType</SubjectType>
                <Fullname>{$data->title}</Fullname>
                <Resident>$Resident</Resident>
                <INN>{$data->inn}</INN>
                <KPP>{$data->kpp}</KPP>
                <OGRN>{$data->ogrn}</OGRN>
                <OKVED>{$data->okved}</OKVED>
                <OKPO>{$data->okpo}</OKPO>
                <OPFTypeId>{$data->opf_id}</OPFTypeId>
                <EconomicId>{$data->economic_id}</EconomicId>
                <Docs>$docs_list</Docs>
                <Phone>{$Phone}</Phone>
                <Email>{$Email}</Email>
                <Addresses>
                    <Address>
                        <AddressType>adrLegal</AddressType>
                        <AddressTypeId>8531</AddressTypeId>
                        <AddressName>Юридический</AddressName>
                        <Address>{$data->address}</Address>
                        <AddressCountry>{$data->subject_country}</AddressCountry>
                        <AddressKLADR>{$AddressKLADR}</AddressKLADR>
                        <AddressStreet>{$data->subject_street}</AddressStreet>
                        <AddressHouse>{$data->subject_house}</AddressHouse>
                        <AddressBuilding>{$data->subject_block}</AddressBuilding>
                        <AddressFlat>{$data->subject_flat}</AddressFlat>
                    </Address>
                </Addresses>
                <Addresses>
                    <Address>
                        <AddressType>adrFact</AddressType>
                        <AddressTypeId>8533</AddressTypeId>
                        <AddressName>Фактический</AddressName>
                        <Address>{$data->fact_address}</Address>
                        <AddressCountry>{$data->fact_subject_country}</AddressCountry>
                        <AddressKLADR>{$AddressKLADRFact}</AddressKLADR>
                        <AddressStreet>{$data->fact_subject_street}</AddressStreet>
                        <AddressHouse>{$data->fact_subject_house}</AddressHouse>
                        <AddressBuilding>{$data->fact_subject_block}</AddressBuilding>
                        <AddressFlat>{$data->fact_subject_flat}</AddressFlat>
                    </Address>
                </Addresses>
        </Subject>
XML;
    }




}