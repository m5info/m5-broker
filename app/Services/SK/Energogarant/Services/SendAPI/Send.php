<?php

namespace App\Services\SK\Energogarant\Services\SendAPI;


use Doctrine\DBAL\Cache\CacheException;

class Send{

    public $Source = 'b2b';

    public $url;
    public $login;
    public $password;

    public $SessionId;
    public $UserId;

    public $api_setting = null;


    private $state_api_work = 0;


    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

        $this->api_setting = $api_setting;

    }

    public function send($xml)
    {
        $curl = curl_init();
        $headers = [];


        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "requestXML=$xml",
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //Логируем ошибки
            return null;
        }


        try {
            $this->state_api_work = 1;
            return new \SimpleXMLElement($response);
        } catch (\Exception $e) {
            $this->state_api_work = 0;
            return $this->responseError($response);
        }


    }


    public function getSessionId()
    {


        $api_setting = $this->api_setting;

        if($api_setting->auxiliary_date <= date("Y-m-d H:i:s")){

            $xml = <<<XML
<WebRequest> 
    <Info> 
        <Source>{$this->Source}</Source>
        <Operation>Login</Operation>
    </Info> 
    <Body> 
        <User>{$this->login}</User> 
        <Psw>{$this->password}</Psw> 
    </Body> 
</WebRequest> 
XML;

            $response = $this->send($xml);

            if($this->state_api_work == 1){
                $this->SessionId = (string)$response->Answer->SessionId;
                $this->UserId = (string)$response->Answer->UserId;
                $api_setting->auxiliary_parameters = \GuzzleHttp\json_encode(['SessionId'=>$this->SessionId, 'UserId'=>$this->UserId]);
                $api_setting->auxiliary_date = date('Y-m-d H:i:s', strtotime("+10 hours"));
                $api_setting->save();
            }else{
                return false;
            }

        }else{
            $auxiliary_parameters = (object)\GuzzleHttp\json_decode($api_setting->auxiliary_parameters);
            $this->SessionId = (string)$auxiliary_parameters->SessionId;
            $this->UserId = (string)$auxiliary_parameters->UserId;
        }


        return true;
    }


    public function responseError($txt)
    {
        $res = new \stdClass();
        $res->Error = new \stdClass();
        $res->Error->Code = 10;
        $res->Error->Text = $txt;
        return $res;
    }

}