<?php

namespace App\Services\SK\Energogarant\Services\Dictionary;

use App\Services\SK\Energogarant\Services\SendAPI\Send;

class DictionaryApi{


    public $url;
    public $login;
    public $password;

    public $Send = null;

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->Send = new Send($url, $login, $password, $api_setting);
        $this->Send->getSessionId();

    }

    public function query($param){
        //авторизация, или подготовка параметров авторизации для запроса, если нужно
        //собираем запрос для сервиса для конкретной ск
        //отправляем
    }



    public function get_purpose(){
        //Дергаем справочник

        //Если у СК это не реализовано возвращаем константу

        $purposes = [
            ['id'=>'640254020', 'title'=>'Личная'],
            ['id'=>'640254030', 'title'=>'Учебная езда'],
            ['id'=>'640254060', 'title'=>'Такси'],
            ['id'=>'640254070', 'title'=>'Дорожные и специальные ТС'],
            ['id'=>'10115', 'title'=>'Экстренные и коммунальные службы'],
            ['id'=>'10116', 'title'=>'Перевозка опасных и легко воспламеняющихся грузов'],
            ['id'=>'10117', 'title'=>'Регулярные пассажирские перевозки/перевозки пассажиров по заказам']
        ];


        return $purposes;
    }

    public function get_categories(){
        //Дергаем справочник Категорий


        //Если у СК это не реализовано возвращаем константу

        $categories = [
            ['id'=>'640210140', 'title'=>'МОТОЦИКЛЫ'],
            ['id'=>'640189240', 'title'=>'ЛЕГКОВЫЕ'],
            ['id'=>'640183000', 'title'=>'ГРУЗОВЫЕ'],
            ['id'=>'640180230', 'title'=>'АВТОБУСЫ'],
            ['id'=>'640212210', 'title'=>'СПЕЦТЕХНИКА'],
            ['id'=>'640243660', 'title'=>'ТРОЛЛЕЙБУСЫ'],
            ['id'=>'640243650', 'title'=>'ТРАМВАИ']
        ];


        return $categories;
    }

    public function get_marks($categorie_id){

        $xml = <<<XML
<WebRequest> 
    <Info> 
        <Source>{$this->Send->Source}</Source> 
        <Operation>SpravInfo</Operation>
        <Product>AgrOsago</Product>
        <SpravId>{$categorie_id}</SpravId>
        <UserId>{$this->Send->UserId}</UserId>
        <SessionId>{$this->Send->SessionId}</SessionId>
    </Info>
</WebRequest> 
XML;

        return $this->Send->send($xml);
    }


    public function get_models($mark_id){
        $xml = <<<XML
<WebRequest> 
    <Info> 
        <Source>{$this->Send->Source}</Source> 
        <Operation>SpravInfo</Operation>
        <Product>AgrOsago</Product>
        <SpravId>{$mark_id}</SpravId>
        <UserId>{$this->Send->UserId}</UserId>
        <SessionId>{$this->Send->SessionId}</SessionId>
    </Info>
</WebRequest> 
XML;

        return $this->Send->send($xml);

    }


    public function get_modifications($mark_id, $model_id){
        //подготавливаем данные для запроса модификации
        //дёргаем свой query
        return [];

    }

}