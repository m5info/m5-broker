<?php

namespace App\Services\SK\Energogarant\Services\Dictionary;

use App\Interfaces\Services\SK\DictionaryServiceInterface;

class DictionaryService implements DictionaryServiceInterface {


    public $api;

    public $cache_json = __DIR__."/json.json";

    public function __construct(DictionaryApi $api){
        $this->api = $api;
    }

    public function get_marks_models(){

        $categories = [];
        $marks = [];
        $models = [];


        if(!is_file($this->cache_json)){

            //тянем категории
            if($_categories = $this->api->get_categories()){

                foreach($_categories as $sk_category){

                    $categories[] = [
                        'id' => $sk_category['id'],
                        'title' => $sk_category['title'],
                    ];

                    //тянем марки
                    if($_marks = $this->api->get_marks($sk_category['id'])){

                        foreach ($_marks->Answer->Row as $sk_mark){

                            $marks[] = [
                                'id' => (string) $sk_mark->Id,
                                'title' => (string) $sk_mark->Name,
                                'vehicle_categorie_sk_id'=>(string) $sk_category['id'],
                            ];

                            //тянем модели
                            if($_models = $this->api->get_models($sk_mark->Id)){

                                foreach ($_models->Answer->Row as $sk_model){

                                    $models[] = [
                                        'id' => (string) $sk_model->Id,
                                        'title' => (string) $sk_model->Name,
                                        'vehicle_mark_sk_id' => (string) $sk_mark->Id,
                                        'vehicle_categorie_sk_id' =>  $sk_category['id']
                                    ];

                                }

                            }

                        }

                    }

                }

            }

            $result = [
                'categories' => $categories,
                'marks' => $marks,
                'models' => $models,
            ];

            file_put_contents($this->cache_json, \GuzzleHttp\json_encode($result));

            return $result;


        }else{

            return \GuzzleHttp\json_decode(file_get_contents($this->cache_json),true);
        }

    }


    public function get_api_purpose(){
        return $this->api->get_purpose();
    }

    public function get_api_categories(){
        return $this->api->get_categories();
    }


}