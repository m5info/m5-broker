<?php

namespace App\Services\SK\Energogarant\Services\Kasko;


use App\Models\Contracts\ContractsCalculation;
use App\Services\SK\Energogarant\Services\Auxiliary\Car;
use App\Services\SK\Energogarant\Services\Auxiliary\Subject;
use App\Services\SK\Energogarant\Services\SendAPI\Send;


class KaskoApi{


    public $url;
    public $login;
    public $password;
    public $Send = null;
    public $SessionId = null;

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->Send = new Send($url, $login, $password, $api_setting);
        $this->Send->getSessionId();

    }

    public function query($param){
        //авторизация, или подготовка параметров авторизации для запроса, если нужно
        //собираем запрос для сервиса для конкретной ск
        //отправляем
    }


    public function getTarif(ContractsCalculation $calc)
    {
        return $this->getMainXML($calc);
    }


    public function getMainXML($calc)
    {

        $contract = $calc->contract;
        $ContractId = strlen($calc->sk_key_id) > 0 ? $calc->sk_key_id : 0;

        $programa = $contract->getProductOrProgram();

        $ContractXML = $this->getContractXMLDefault($calc);

        $insurer = Subject::getSubject($contract->insurer, $calc->insurance_companies_id, $this->Send);
        if($insurer->state == false){
            return $this->responseError($insurer->result);
        }

        $owner = Subject::getSubject($contract->owner, $calc->insurance_companies_id, $this->Send);
        if($owner->state == false){
            return $this->responseError($owner->result);
        }

        $beneficiar = Subject::getSubject($contract->beneficiar, $calc->insurance_companies_id, $this->Send);
        if($beneficiar->state == false){
            return $this->responseError($beneficiar->result);
        }

        $drivers = new \stdClass();
        $drivers->result = '';
        if(($contract->drivers && sizeof($contract->drivers))){
            $drivers = Subject::getDrivers($contract->drivers, $calc->insurance_companies_id, $this->Send);
            if($drivers->state == false){
                return $this->responseError($drivers->result);
            }
        }



        $car = Car::getCarKasko($calc);
        if($car->state == false){
            return $this->responseError($car->result);
        }



        $xml = <<<XML
<WebRequest> 
    <Info> 
        <Product>{$programa->api_name}</Product>
        <Operation>AgrCalc</Operation>
        <ContractId>{$ContractId}</ContractId>
        <Source>{$this->Send->Source}</Source>
        <UserId>{$this->Send->UserId}</UserId>
        <SessionId>{$this->Send->SessionId}</SessionId>
    </Info>
    <Body> 
        <Insurer><SubjectId>{$insurer->result}</SubjectId></Insurer>
        <Owner><SubjectId>{$owner->result}</SubjectId></Owner>
        <Beneficiary><SubjectId>{$beneficiar->result}</SubjectId></Beneficiary>
        <Drivers>
            {$drivers->result}
        </Drivers>
        <Contract>
            {$ContractXML}
        </Contract>
        <Car>
            {$car->result}
        </Car>
        <Equipments></Equipments>
    </Body> 
</WebRequest> 
XML;


        //dd($xml);

        $response = $this->Send->send($xml);
        return $response;
    }



    public function getContractXMLDefault($calc)
    {
        $contract = $calc->contract;

        $sign_date = date("d.m.Y", strtotime($contract->sign_date));
        $begin_date = date("d.m.Y H:i", strtotime($contract->begin_date));
        $end_date = date("d.m.Y", strtotime($contract->end_date));


        $Multidrive = ($contract->drivers && sizeof($contract->drivers))?'N':"Y";
        $MultidriveId = '';//($contract->drivers && sizeof($contract->drivers))?9885:9886;


        $program_data = "<FranchiseType/>
            <Franchise/>
            <GetSprav/>
            <GAP/>";


        $Variant = '';
        /*
        if((int)$this->compensation_variant>0){
            $Variant .= '<Variant>'.$this->compensation_variant.'</Variant>';
        }
        if((int)$this->compensation_variant_2>0){
            $Variant .= '<Variant>'.$this->compensation_variant_2.'</Variant>';
        }
        */

        $LimitSum = str_replace('.', ',', $contract->insurance_amount);
        $DGOLimitSum = '';
        $DGOProgramm = '';
        $NSLimitSum = '';

        $is_loss = "N";
        /*
         * Компенсация  разницы между стоимостью автомобиля, не бывшего в ДТП, и автомобиля, восстановленного после ДТП
        if($this->is_loss_commodity_value == 1){
            $is_loss = "Y";
        }
        */

        $KindSumisn = [0=>640273680, 1=>640273670][$contract->terms->type_sum_insured_id];//Вид страховой суммы
        $OplataType = (int)$contract->insurance_companies_algorithm->sk_key; //Единовременно до начала действия договора



        $Carout = "N";
        $CaroutDoc = "N";
        $Carfire = "N";
        $NoSprav = '';
        $UpTariff = '';

        $xml = <<<XML
        
            <DateSign>$sign_date</DateSign> 
            <DateBeg>{$begin_date}</DateBeg> 
            <DateEnd>{$end_date}</DateEnd> 
            
            <Multidrive>{$Multidrive}</Multidrive>
            <MultidriveId>{$MultidriveId}</MultidriveId>
            
            
            <Currency>35</Currency>
          
            <PrevContractId></PrevContractId>
            <PrevContractName></PrevContractName>
            
            <LimitSum>$LimitSum</LimitSum>
            <DGOLimitSum>$DGOLimitSum</DGOLimitSum>
            <DGOProgramm>$DGOProgramm</DGOProgramm>
            <NSLimitSum>$NSLimitSum</NSLimitSum>
            
            
            <Compensation>{$Variant}</Compensation>
            
            
            $program_data
            
            <UTS>{$is_loss}</UTS>
            
            
            <KindSumisn>$KindSumisn</KindSumisn>
            <OplataType>$OplataType</OplataType>
            
            <Carout>$Carout</Carout>
            <CaroutDoc>$CaroutDoc</CaroutDoc>
            <Carfire>$Carfire</Carfire>
            <NoSprav>$NoSprav</NoSprav>
            <UpTariff>$UpTariff</UpTariff>
            
            
            <DiscountOfKV></DiscountOfKV>
            
            <ExtTerrAdd></ExtTerrAdd>
            <ExtTerrPeriod></ExtTerrPeriod>
            <ExtTerrOther></ExtTerrOther>
            <ExtTerrBeg></ExtTerrBeg>
            <ExtTerrEnd></ExtTerrEnd>
            
            <PaymentOrder>10316</PaymentOrder>
            
            <CountAvto>1</CountAvto>
            
            <PlaceBuy/>
            <PlaceReg/>
            <Route/>
            
            <OsagoSeries/>
            <OsagoNumber/>
            <OsagoCompany/>
            
            <AnotherKaskoNumber/>
            <AnotherKaskoCompany/>
            
            <PrevKaskoNumber/>
            <PrevKaskoCompany/>
            
            <AdditionalTerms1/>
            <AdditionalTerms2/>
            
            <KvSeries/>
            <KvNumber/>
            
XML;

        return $xml;
    }



    public function getTempCalc(ContractsCalculation $calc)
    {

        $contract = $calc->contract;
        $ContractId = strlen($calc->sk_key_id) > 0 ? $calc->sk_key_id : 0;
        $vehicle = $contract->object_insurer_auto;

        if(!$TypeAvto = $calc->sk_category) return $this->responseError('Не синхронизирован справочник: Категория', false);
        if(!$BrandIsn = $calc->sk_mark) return $this->responseError('Не синхронизирован справочник: Марка', false);
        if(!$ModelIsn = $calc->sk_model) return $this->responseError('Не синхронизирован справочник: Модель', false);
        if(!$ColorIsn = $calc->sk_color) return $this->responseError('Не синхронизирован справочник: Цвета', false);
        if(!$AntiTheftSystemIsn = $calc->sk_anti_theft_system) return $this->responseError('Не синхронизирован справочник: Противоугонное устройство', false);

        $Price = '';
        if($vehicle->price > 0){
            $Price = str_replace('.', ',', $vehicle->price);
        }


        $UseYear = date("Y")-(int)$vehicle->car_year;


        if($vehicle->capacity > 0){
            $MaxLoad = str_replace('.', ',', $vehicle->capacity);
        }else{
            $MaxLoad = '';
        }

        if($vehicle->weight > 0){
            $MaxMass = str_replace('.', ',', $vehicle->weight);
        }else{
            $MaxMass = '';
        }

        $MaxLoad = str_replace(',00', '', $MaxLoad);
        $MaxMass = str_replace(',00', '', $MaxMass);


        $KindSumisn = [0=>640273680, 1=>640273670][$contract->terms->type_sum_insured_id];//Вид страховой суммы
        $OplataType = (int)640258100; //Единовременно до начала действия договора


        $drivers = new \stdClass();
        $drivers->result = '';


        if(($contract->drivers && sizeof($contract->drivers))){
            $drivers = Subject::getTempDrivers($contract->drivers, $calc->insurance_companies_id, $this->Send);
            if($drivers->state == false){
                return $this->responseError($drivers->result);
            }
        }


        $xml = <<<XML
<WebRequest> 
    <Info> 
        <Product>{$calc->program->api_name}</Product>
        <Operation>Calc</Operation>
        <Source>{$this->Send->Source}</Source>
        <UserId>{$this->Send->UserId}</UserId>
        <SessionId>{$this->Send->SessionId}</SessionId>
    </Info>
    <Body> 
    
<Car>
    <TypeAvto>{$TypeAvto->vehicle_categorie_sk_id}</TypeAvto>
    <Classific></Classific>
    <BrandIsn>{$BrandIsn->vehicle_marks_sk_id}</BrandIsn>
    <ModelIsn>{$ModelIsn->vehicle_models_sk_id}</ModelIsn>
    <Country>2570</Country>
    <UseYear>$UseYear</UseYear>
    <Color>{$ColorIsn->color_sk_id}</Color>
    <Antijack>{$AntiTheftSystemIsn->anti_theft_system_sk_id}</Antijack>
    <MaxLoad>$MaxLoad</MaxLoad>
    <MaxMass>$MaxMass</MaxMass>
    <Vin>{$vehicle->vin}</Vin>
    <Price>{$Price}</Price>
</Car>

<Contract>
    <PeriodAgr>12</PeriodAgr>
    <LimitSum>{$Price}</LimitSum>
    <KindSumisn>{$KindSumisn}</KindSumisn>
    <TypeInsurer>0</TypeInsurer>
    <CountAvto>10065</CountAvto>
    <OplataType>{$OplataType}</OplataType>
    
    
    
    <Carout>Y</Carout>
    <CaroutDoc>N</CaroutDoc>
    <Carfire>N</Carfire>
    
    <Compensation>
        <Variant>10211</Variant><Variant>10212</Variant>
    </Compensation>
    <NoSprav>9871</NoSprav>
    <GAP>N</GAP>
    <PrevKaskoNumber></PrevKaskoNumber>
    <FranchiseType></FranchiseType>
    <Franchise></Franchise>
    <GetSprav>0</GetSprav>
    <UTS>N</UTS>
    <ExtTerrAdd>10131</ExtTerrAdd>
    <ExtTerrPeriod>10132</ExtTerrPeriod>
    <DGOLimitSum></DGOLimitSum>
    <DGOProgramm></DGOProgramm>
    <NSLimitSum></NSLimitSum>
    <Multidrive>N</Multidrive>
    
    <UpTariff>1,00</UpTariff>
    <DiscountOfKV>0,00</DiscountOfKV>
</Contract>
<Insurer><SubjectType>ФЛ</SubjectType></Insurer>
<Drivers>
    {$drivers->result}
</Drivers>
<Equipments>
    <Equipment>
        <Name>доп.оборудование (общее)</Name>
        <LimitSum>0</LimitSum>
    </Equipment>
</Equipments>

    </Body> 
</WebRequest> 
XML;


        $response = $this->Send->send($xml);

        return $response;
    }


    private function responseError($error)
    {
        $r = new \stdClass();
        $r->Error = new \stdClass();
        $r->Error->Code = 10;
        $r->Error->Text = $error;
        return $r;
    }

}