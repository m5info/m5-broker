<?php

namespace App\Services\SK\Energogarant\Services\Osago;


use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Services\SK\Energogarant\Services\Auxiliary\Car;
use App\Services\SK\Energogarant\Services\Auxiliary\Subject;
use App\Services\SK\Energogarant\Services\SendAPI\Send;

class OsagoApi{


    public $url;
    public $login;
    public $password;
    public $Send = null;
    public $SessionId = null;

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->Send = new Send($url, $login, $password, $api_setting);
        $this->Send->getSessionId();

    }



    public function getTarif(ContractsCalculation $calc)
    {
        return $this->getMainXML($calc);
    }


    public function setPayment(ContractsCalculation $calc)
    {
        return $this->controlPaymentXml($this->getPaymentXML($calc));
    }


    public function controlPaymentXml($xml, $rec = 0)
    {

        $response = $this->Send->send($xml);

        if($response->Error->Code == 0){

            return $response;


        }elseif($response->Error->Code != 30){

            return $this->responseError($response->Error->Text);

        }elseif($response->Error->Code == 30){
            if($rec<=5){
                sleep(2);
                return $this->controlPaymentXml($xml, $rec++);
            }
        }

        return $this->responseError("ERROR");
    }



    public function getMainXML($calc)
    {

        $contract = $calc->contract;
        $ContractId = strlen($calc->sk_key_id)>0?$calc->sk_key_id:0;

        $ContractXML = $this->getContractXML($calc);



        $insurer = Subject::getSubject($contract->insurer, $calc->insurance_companies_id, $this->Send);
        if($insurer->state == false){
            return $this->responseError($insurer->result);
        }

        $owner = Subject::getSubject($contract->owner, $calc->insurance_companies_id, $this->Send);
        if($owner->state == false){
            return $this->responseError($owner->result);
        }


        $drivers = new \stdClass();
        $drivers->result = '';
        if(($contract->drivers && sizeof($contract->drivers))){
            $drivers = Subject::getDrivers($contract->drivers, $calc->insurance_companies_id, $this->Send);
            if($drivers->state == false){
                return $this->responseError($drivers->result);
            }
        }

        $car = Car::getCarOSAGO($calc);
        if($car->state == false){
            return $this->responseError($car->result);
        }





        $xml = <<<XML
<WebRequest> 
    <Info> 
        <Product>AgrOsago</Product>
        <Operation>AgrCalc</Operation>
        <ContractId>{$ContractId}</ContractId>
        <Source>{$this->Send->Source}</Source>
        <UserId>{$this->Send->UserId}</UserId>
        <SessionId>{$this->Send->SessionId}</SessionId>
    </Info>
    <Body> 
        <Insurer><SubjectId>{$insurer->result}</SubjectId></Insurer>
        <Owner><SubjectId>{$owner->result}</SubjectId></Owner>
        <Drivers>
            {$drivers->result}
        </Drivers>
        <Contract>{$ContractXML}</Contract>
        <Car>{$car->result}</Car>
    </Body> 
</WebRequest> 
XML;

        //echo $xml;
        //dd("OK");

        $response = $this->Send->send($xml);
        return $response;

    }
    public function getContractXML($calc)
    {
        $contract = $calc->contract;

        $sign_date = date("d.m.Y", strtotime($contract->sign_date));
        $begin_date = date("d.m.Y H:i", strtotime($contract->begin_date));
        $end_date = date("d.m.Y", strtotime($contract->end_date));

        $period1_begin = date("d.m.Y", strtotime($contract->begin_date));
        $period1_end = date("d.m.Y", strtotime($contract->end_date));

        $UseCaravan = $contract->object_insurer_auto->is_trailer;

        $Multidrive = ($contract->drivers && sizeof($contract->drivers))?'N':"Y";

        $xml = <<<XML
        
            <DateBeg>{$begin_date}</DateBeg> 
            <DateEnd>{$end_date}</DateEnd> 
            <PeriodBeg1>{$period1_begin}</PeriodBeg1>
            <PeriodEnd1>{$period1_end}</PeriodEnd1>
            <PeriodBeg2></PeriodBeg2>
            <PeriodEnd2></PeriodEnd2>
            <PeriodBeg3></PeriodBeg3>
            <PeriodEnd3></PeriodEnd3>
            <Remark></Remark>
            <Multidrive>{$Multidrive}</Multidrive>
            <Violations>N</Violations>
            <UseCaravan>{$UseCaravan}</UseCaravan>
        
XML;

        return $xml;
    }


    public function getPaymentXML($calc)
    {
        $ContractId = $calc->sk_key_id;

        $contract = $calc->contract;
        $payment = $contract->get_payment_first();


        $TypePolis = '';
        $TypePay = '';
        $BSO = '';
        $Ticket = '';

        if($contract->is_epolicy != 1) { // Бумажный
            $TypePolis = 'cTypePolisBSO';
            $BSO = "<BSOSeries>{$contract->bso->type->serie->title}</BSOSeries><BSONumber>{$contract->bso->bso_number}</BSONumber>";

        }elseif($contract->is_epolicy == 1){ // Электронный
            $TypePolis = 'cTypePolisEPolis';
        }

        if ($payment->pay_mehtod->key_type == 5) { // Без подтверждения
            $TypePay = 'cTypePayNotVerification';
        }elseif ($payment->pay_mehtod->key_type == 3) { // Счет
            $TypePay = 'cTypePayInvoice';
        }elseif($payment->pay_mehtod->key_type == 0) { // Квитанция
            $TypePay = 'cTypePayTicket';
            $Ticket = "<TicketSeries>{$payment->receipt->bso->type->serie->title}</TicketSeries><TicketNumber>{$payment->receipt->bso->bso_number}</TicketNumber>";
        }


        $xml = <<<XML
<WebRequest>
  <Info>
    <Product>AgrOsago</Product>
    <Operation>AgrConfirm</Operation>
    <ContractId>{$ContractId}</ContractId>
    <Source>{$this->Send->Source}</Source>
    <UserId>{$this->Send->UserId}</UserId>
    <SessionId>{$this->Send->SessionId}</SessionId>
  </Info>
  <Body>
    <Contract>
      <TypePolis>$TypePolis</TypePolis>
      <TypePay>$TypePay</TypePay>
      $BSO
      $Ticket
    </Contract>
  </Body>
</WebRequest>
XML;

        return $xml;

    }



    public function getTemplate($calc)
    {

        $ContractId = $calc->sk_key_id;

        $xml = <<<XML
<WebRequest> 
    <Info> 
        <Product>AgrOsago</Product>
        <Operation>AgrPrint</Operation>
        <ContractId>{$ContractId}</ContractId>
        <Source>{$this->Send->Source}</Source>
        <UserId>{$this->Send->UserId}</UserId>
        <SessionId>{$this->Send->SessionId}</SessionId>
    </Info>
</WebRequest>
XML;

        return $this->Send->send($xml);

    }


    public function getDocument($calc, $print_template, $rec = 0)
    {
        $ContractId = $calc->sk_key_id;

        $xml = <<<XML
<WebRequest> 
    <Info> 
        <Product>AgrOsago</Product>
        <Operation>{$print_template}</Operation>
        <ContractId>{$ContractId}</ContractId>
        <Source>{$this->Send->Source}</Source>
        <UserId>{$this->Send->UserId}</UserId>
        <SessionId>{$this->Send->SessionId}</SessionId>
    </Info>
</WebRequest>
XML;

        $response = $this->Send->send($xml);

        if((int)$response->Error->Code == 0){
            return $response->Answer;

        }else if((int)$response->Error->Code == 30 && $rec <= 6){
            sleep(5);
            return $this->getDocument($calc, $print_template, ($rec+1));
        }

        return $response;
    }



    private function responseError($error)
    {
        $r = new \stdClass();
        $r->Error = new \stdClass();
        $r->Error->Code = 10;
        $r->Error->Text = $error;
        return $r;
    }



}