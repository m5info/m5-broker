<?php


namespace App\Services\SK\Vsk\Services\Dictionary;


use App\Interfaces\Services\SK\DictionaryServiceInterface;

class DictionaryService implements DictionaryServiceInterface {

    private $api;

    public $cache_json = __DIR__."/json.json";

    public function __construct(DictionaryApi $api)
    {
        $this->api = $api;
    }

    public function get_api_purpose(){

        $purposes = $this->api->get_purpose();

        $result = [];

        foreach($purposes->Results as $purpose){
            $result[] = [
                'id' => $purpose->ID,
                'title' => $purpose->Name,
            ];
        }

        return $result;
    }

    public function get_api_sks(){

        $sks = $this->api->get_sks();

        $result = [];

        foreach($sks->Results as $sk){
            $result[] = [
                'id' => $sk->ID,
                'title' => $sk->Name,
            ];
        }

/*        $arr = [];
        foreach($result as $sK){
            $title = $sK['title'];
            $inn = explode(':', $sK['title']);

            $pos=strpos($title, "ИНН/БИК");
            $title=substr($title, 0, $pos);
            $title = trim($title);

            $arr[] = [
                'title' => $title,
                'inn' => isset($inn[1]) ? $inn[1] : ''
            ];
        }
        echo \GuzzleHttp\json_encode($arr, JSON_UNESCAPED_SLASHES);
die();*/
        return $result;
    }

    public function get_api_categories(){

        $categories = $this->api->get_categories();

        $result = [];

        foreach($categories->Results as $category){
            $result[] = [
                'id' => $category->ID,
                'title' => $category->Name,
            ];
        }

        return $result;
    }

    public function get_marks_models()
    {

        $marks = [];
        $models = [];


        if(!is_file($this->cache_json)) {

            $_marks = $this->api->get_marks();

            foreach($_marks as $key => $mark){

                $marks[] = [
                    'id' => (string) $mark->ID,
                    'title' => (string) $mark->Name,
                    'vehicle_categorie_sk_id' => (string) 'B - легковые',
                ];
            }

            $_models = $this->api->get_models();

            if($_models){
                if(is_array($_models)){

                    foreach ($_models as $model_key => $model){

                        $models[] = [
                            'id' => (string) $model->ID,
                            'title' => (string) $model->Name,
                            'vehicle_mark_sk_id' => (string) $model->ParentItemID,
                            'vehicle_categorie_sk_id' => (string)'B - легковые',
                        ];
                    }
                }else{

                    $models[] = [
                        'id' => (string) $_models->ID,
                        'title' => (string) $_models->Name,
                        'vehicle_mark_sk_id' => (string) $_models->ParentItemID,
                        'vehicle_categorie_sk_id' => (string)'B - легковые',
                    ];
                }
            }

            $result = [
                'categories' => $this->get_api_categories(),
                'marks' => $marks,
                'models' => $models,
            ];

            file_put_contents($this->cache_json, \GuzzleHttp\json_encode($result));

            return $result;

        }else{

            return \GuzzleHttp\json_decode(file_get_contents($this->cache_json),true);
        }
    }

    public function get_api_manufacturers()
    {
        $manufacturers = $this->api->get_manufacturers();

/*     0 => {#1699
        +"ID": "a878d6cb-59e0-4ea5-8d05-59e55b2f56be"
        +"ParentItemID": null
        +"Name": "иностранные"
        +"DisplayName": "иностранные"
        +"Description": "иностранные"
        +"ItemOrder": 0
        +"IsActive": true
      }
      1 => {#1702
        +"ID": "e05fffb1-d675-4eac-b474-615e3e5fa009"
        +"ParentItemID": null
        +"Name": "отечественные"
        +"DisplayName": "отечественные"
        +"Description": "отечественные"
        +"ItemOrder": 0
        +"IsActive": true
      }*/
    }

    // список Типов документа ТС
//    ^ {#1704
//    +"GetClassifierItemResult": {#1691
//    +"ClassifierItemInfo": array:12 [
//      0 => {#1699
//    +"ID": "ea47b4d0-5071-4149-8ab4-d493e9d303da"
//    +"ParentItemID": null
//    +"Code": "DTVHS"
//    +"Name": "ПТС"
//    +"DisplayName": "ПТС"
//    +"Description": "ПАССПОРТ ТРАНСПОРТНОГО СРЕДСТВА"
//    +"ItemOrder": 1
//    +"Value9": "30"
//    +"IsActive": true
//    }
//      1 => {#1702
//    +"ID": "643138bc-e602-4774-830a-8791e80fb2c9"
//    +"ParentItemID": null
//    +"Code": "DTVHREG"
//    +"Name": "СВИДЕТЕЛЬСТВО О ГОСРЕГИСТРАЦИИ"
//    +"DisplayName": "СВИДЕТЕЛЬСТВО О ГОСРЕГИСТРАЦИИ"
//    +"Description": "СВИДЕТЕЛЬСТВО О ГОСУДАРСТВЕННОЙ РЕГИСТРАЦИИ ТРАНСПОРТНОГО СРЕДСТВА"
//    +"ItemOrder": 2
//    +"Value9": "31"
//    +"IsActive": true
//      }
//      2 => {#1705
//    +"ID": "0c727b23-4315-428d-bd61-5bc4fedbc644"
//    +"ParentItemID": null
//    +"Code": "VHDTREGDOC"
//    +"Name": "технический паспорт"
//    +"DisplayName": "технический паспорт"
//    +"Description": "технический паспорт"
//    +"ItemOrder": 3
//    +"Value9": "33"
//    +"IsActive": true
//      }
//      3 => {#1706
//    +"ID": "3bdb82ff-a077-4dc1-a5ad-5dbd8f50e4d4"
//    +"ParentItemID": null
//    +"Code": "VHDTREGCARD"
//    +"Name": "технический талон"
//    +"DisplayName": "технический талон"
//    +"Description": "технический талон"
//    +"ItemOrder": 4
//    +"Value9": "34"
//    +"IsActive": true
//      }
//      4 => {#1709
//    +"ID": "633a998b-1b20-4eca-8a0f-eb96d959067d"
//    +"ParentItemID": null
//    +"Code": "VHDTOTH"
//    +"Name": "иной документ"
//    +"DisplayName": "иной документ"
//    +"Description": "иной документ"
//    +"ItemOrder": 5
//    +"Value9": "40"
//    +"IsActive": true
//      }
//      5 => {#1710
//    +"ID": "32c3a07c-d79a-43fa-91c1-389d00e306d7"
//    +"ParentItemID": null
//    +"Name": "Паспорт самоходной машины"
//    +"DisplayName": "ПСМ"
//    +"Description": "Паспорт самоходной машины"
//    +"ItemOrder": 6
//    +"Value9": "32"
//    +"IsActive": true
//      }
//      6 => {#1711
//    +"ID": "3d86bf1b-5eba-4c92-b9a7-71bdfa2c3512"
//    +"ParentItemID": null
//    +"Code": "EDTVHS"
//    +"Name": "Электронный ПТС/ПСМ "
//    +"DisplayName": "Электронный ПТС/ПСМ "
//    +"Description": "Электронный ПАСПОРТ ТРАНСПОРТНОГО СРЕДСТВА"
//    +"ItemOrder": 7
//    +"Value9": "41"
//    +"IsActive": true
//      }
//      7 => {#1712
//    +"ID": "6882f7b5-ef27-46da-8025-64fc7cb2402c"
//    +"ParentItemID": null
//    +"Name": "Техпаспорт иностранного государства"
//    +"DisplayName": "Техпаспорт иностранного государства"
//    +"Description": "Техпаспорт иностранного государства"
//    +"ItemOrder": 8
//    +"Value9": "35"
//    +"IsActive": true
//      }
//      8 => {#1713
//    +"ID": "69888298-efc8-4f7e-98fe-3aa70ee2c95d"
//    +"ParentItemID": null
//    +"Name": "Паспорт ТС иностранного государства"
//    +"DisplayName": "Паспорт ТС иностранного государства"
//    +"Description": "Паспорт ТС иностранного государства"
//    +"ItemOrder": 9
//    +"Value9": "36"
//    +"IsActive": true
//      }
//      9 => {#1714
//    +"ID": "554240ad-6bcb-4b25-8f67-7f9ce5f4f735"
//    +"ParentItemID": null
//    +"Name": "Свидетельство о регистрации ТС иностранного государства"
//    +"DisplayName": "Свидетельство о регистрации ТС иностранного государства"
//    +"Description": "Свидетельство о регистрации ТС иностранного государства"
//    +"ItemOrder": 10
//    +"Value9": "37"
//    +"IsActive": true
//      }
//      10 => {#1715
//    +"ID": "d1721242-ddbb-4467-8a9a-ab7d38d8ea1a"
//    +"ParentItemID": null
//    +"Name": "Технический талон иностранного государства"
//    +"DisplayName": "Технический талон иностранного государства"
//    +"Description": "Технический талон иностранного государства"
//    +"ItemOrder": 11
//    +"Value9": "38"
//    +"IsActive": true
//      }
//      11 => {#1716
//    +"ID": "eded7fc5-29af-485f-b4b8-a6d79a57031f"
//    +"ParentItemID": null
//    +"Name": "Паспорт самоходной машины иностранного государства"
//    +"DisplayName": "Паспорт самоходной машины иностранного государства"
//    +"Description": "Паспорт самоходной машины иностранного государства"
//    +"ItemOrder": 12
//    +"Value9": "39"
//    +"IsActive": true
//      }
//    ]
//  }





    /*0 => {#1693
    +"ID": "B84EB620-5C3B-46DC-8FE4-83B6767EE95E"
    +"Name": "до 21 года включительно со стажем вождения до 2 лет включительно"
    }
    1 => {#1701
    +"ID": "2DC093EF-210E-4499-A80B-B822A10F509B"
    +"Name": "до 21 года включительно со стажем вождения от 3 лет включительно до 6 лет включительно"
    }
    2 => {#1704
    +"ID": "DAF1D4B5-275C-4163-9A12-EA70A5667891"
    +"Name": "от 22 лет включительно до 24 лет включительно со стажем вождения до 2 лет включительно"
    }
    3 => {#1707
    +"ID": "2013960F-75AD-4621-B4AA-340941EEFE40"
    +"Name": "от 22 лет включительно до 24 лет включительно со стажем вождения от 3 лет включительно до 9 лет включительно"
    }
    4 => {#1708
    +"ID": "5AEF154C-A493-4227-AD8C-0D5110FF1EA0"
    +"Name": "от 25 лет включительно до 29 лет включительно со стажем вождения 0 лет"
    }
    5 => {#1711
    +"ID": "8375ADB8-DD23-45E4-AEFA-B4BF0E2B1A82"
    +"Name": "от 25 лет включительно до 29 лет включительно со стажем вождения 1 год"
    }
    6 => {#1712
    +"ID": "85847CCC-AD41-46B2-8A2B-61FE1E27B963"
    +"Name": "от 25 лет включительно до 29 лет включительно со стажем вождения 2 года"
    }
    7 => {#1713
    +"ID": "449A2A4C-4596-4C1E-831F-EF36D35CFC3C"
    +"Name": "от 25 лет включительно до 29 лет включительно со стажем вождения от 3 лет включительно до 9 лет включительно"
    }
    8 => {#1714
    +"ID": "243F450B-092D-43CB-AF8C-9A4D3C3F5326"
    +"Name": "от 25 лет включительно до 29 лет включительно со стажем вождения от 10 лет включительно до 14 лет включительно"
    }
    9 => {#1715
    +"ID": "71FCA284-C431-4610-98F3-CA48A7460E60"
    +"Name": "от 30 лет включительно до 59 лет включительно со стажем вождения до 2 лет включительно"
    }
    10 => {#1716
    +"ID": "15655BBF-B765-4B1A-9431-77EC85E69757"
    +"Name": "от 30 лет включительно до 34 лет включительно со стажем вождения от 3 лет включительно до 6 лет включительно"
    }
    11 => {#1717
    +"ID": "61EBA0E1-AAEE-4DD1-B60B-E3FBA2BA81DF"
    +"Name": "от 30 лет включительно до 34 лет включительно со стажем вождения от 7 лет включительно до 9 лет включительно"
    }
    12 => {#1718
    +"ID": "D6F8DBA6-D2CB-481C-80FE-D26AA0C94A6B"
    +"Name": "от 30 лет включительно до 34 лет включительно со стажем вождения от 10 лет включительно"
    }
    13 => {#1719
    +"ID": "95C71B97-D464-4625-B25F-CEA9C5E45061"
    +"Name": "от 35 лет включительно до 39 лет включительно со стажем вождения от 3 лет включительно до 4 лет включительно"
    }
    14 => {#1720
    +"ID": "A4D8AA95-43F3-4514-A9AB-82D202E2D633"
    +"Name": "от 40 лет включительно до 59 лет включительно со стажем вождения от 3 лет включительно"
    }
    15 => {#1721
    +"ID": "394D5CD9-BC4B-49AE-B987-75B7E5E11FEA"
    +"Name": "от 35 лет включительно до 39 лет включительно со стажем вождения от 5 лет включительно"
    }
    16 => {#1722
    +"ID": "4831F455-E776-419F-9649-CA942554F145"
    +"Name": "от 59 лет включительно со стажем вождения до 2 лет включительно"
    }
    17 => {#1723
    +"ID": "570C6F23-D81F-4B14-8D49-D78445757A74"
    +"Name": "от 59 лет включительно со стажем вождения от 3 лет включительно"
    }*/
}