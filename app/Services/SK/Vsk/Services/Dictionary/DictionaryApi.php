<?php


namespace App\Services\SK\Vsk\Services\Dictionary;


use App\Services\SK\Vsk\Services\Clients\VskSoapClient;
use SoapVar;

class DictionaryApi{

    public $url;
    public $login;
    public $password;

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
    }

    public function get_service_client($service_name){

        return new VskSoapClient([
            'url' => $this->url,
            'service_name' => $service_name,
            'login' => $this->login,
            'password' => $this->password,
        ]);
    }


    public function get_purpose(){

        $service_client = $this->get_service_client('Companies/vsk/services/osagointegration.asmx');

        $param = <<<XML
    <GetPurposeUse xmlns="http://tempuri.org/vsk/osagointegration" />
XML;

        $param = new SoapVar($param, XSD_ANYXML);

        return $service_client->GetPurposeUse($param);
    }


    public function get_sks(){

        $service_client = $this->get_service_client('Companies/vsk/services/osagointegration.asmx');

        $param = <<<XML
    <GetInsuranceCompany xmlns="http://tempuri.org/vsk/osagointegration">
      <UserLogin>$this->login</UserLogin>
      <UserPass>$this->password</UserPass>
    </GetInsuranceCompany>
XML;

        $param = new SoapVar($param, XSD_ANYXML);

        return $service_client->GetInsuranceCompany($param);
    }


    public function get_categories(){

        $service_client = $this->get_service_client('Companies/vsk/services/osagointegration.asmx');

        $param = <<<XML
    <GetVehicleType xmlns="http://tempuri.org/vsk/osagointegration" />
XML;

        $param = new SoapVar($param, XSD_ANYXML);

        return $service_client->GetVehicleType($param);
    }


    public function get_ages_experiences(){

        $service_client = $this->get_service_client('Companies/vsk/services/osagointegration.asmx');

        $param = <<<XML
    <GetLduAgeExpirienceType xmlns="http://tempuri.org/vsk/osagointegration" />
XML;

        $param = new SoapVar($param, XSD_ANYXML);

        return $service_client->GetLduAgeExpirienceType($param);
    }


    public function get_marks(){

        $service_client = $this->get_service_client('/companies/vsk/services/agentservice.asmx');

        $clsId = 'ECEBF857-C8D5-4E54-A23E-621BEC28072B'; // ид справочника марок
        $prodId = '548A36FB-543E-461D-A339-BCFE719FC31E'; // ид осаго

        $param = <<<XML
    <GetClassifierItem xmlns="http://tempuri.org/vsk/AgentService/">
      <user>
        <UserName>dealer2</UserName>
        <UserPwd>dealer2</UserPwd>
        <IsPassClearText>true</IsPassClearText>
      </user>
      <clsId>$clsId</clsId>
      <prodId>$prodId</prodId>
    </GetClassifierItem>
XML;

        $param = new SoapVar($param, XSD_ANYXML);

        $answer = $service_client->GetClassifierItem($param);

        return isset($answer->GetClassifierItemResult) && isset($answer->GetClassifierItemResult->ClassifierItemInfo) ? $answer->GetClassifierItemResult->ClassifierItemInfo : null;
    }


    public function get_models(){

        $service_client = $this->get_service_client('/companies/vsk/services/agentservice.asmx');

        $clsId = '38D0CD83-981B-4163-A95D-73E32DE983F8'; // ид справочника моделей
        $prodId = '548A36FB-543E-461D-A339-BCFE719FC31E'; // ид осаго

        $param = <<<XML
    <GetClassifierItem xmlns="http://tempuri.org/vsk/AgentService/">
      <user>
        <UserName>dealer2</UserName>
        <UserPwd>dealer2</UserPwd>
        <IsPassClearText>true</IsPassClearText>
      </user>
      <clsId>$clsId</clsId>
      <prodId>$prodId</prodId>
    </GetClassifierItem>
XML;

        $param = new SoapVar($param, XSD_ANYXML);

        $answer = $service_client->GetClassifierItem($param);

        return isset($answer->GetClassifierItemResult) && isset($answer->GetClassifierItemResult->ClassifierItemInfo) ? $answer->GetClassifierItemResult->ClassifierItemInfo : null;
    }


    public function get_manufacturers(){

        $service_client = $this->get_service_client('/companies/vsk/services/agentservice.asmx');

        $clsId = '9555D0D0-C116-47DF-AD82-780564F58BFD'; // ид справочника производства
        $prodId = '548A36FB-543E-461D-A339-BCFE719FC31E'; // ид осаго

        $param = <<<XML
    <GetClassifierItem xmlns="http://tempuri.org/vsk/AgentService/">
      <user>
        <UserName>dealer2</UserName>
        <UserPwd>dealer2</UserPwd>
        <IsPassClearText>true</IsPassClearText>
      </user>
      <clsId>$clsId</clsId>
      <prodId>$prodId</prodId>
    </GetClassifierItem>
XML;

        $param = new SoapVar($param, XSD_ANYXML);

        $answer = $service_client->GetClassifierItem($param);
dd($answer);
        return isset($answer->GetClassifierItemResult) && isset($answer->GetClassifierItemResult->ClassifierItemInfo) ? $answer->GetClassifierItemResult->ClassifierItemInfo : null;
    }


    public static function getDuration($begin_date, $end_date){
        $begin_date = new \DateTime($begin_date);
        $end_date = new \DateTime($end_date);
        $interval = $begin_date->diff($end_date);

        $m = $interval->m;
        $d = $interval->days;

        $durationKey = '';

        if($d >= 5 && $d <= 15){
            $durationKey = '28B6BC73-0125-49AE-B38E-3A6D5979B8E5';
        } else if($d >= 16 && $m < 1){
            $durationKey = '605D1624-E2F6-4980-B16B-BE1E58A42384';
        } else if($m == 2){
            $durationKey = '9AE4DED1-4E1E-4FAE-8EB7-54606F88D264';
        } else if($m == 3){
            $durationKey = '13AACCAE-4B7E-4465-8304-CBD6F199B1B5';
        } else if($m == 4){
            $durationKey = '7A3B2767-B9D1-4F77-8807-2771AF785D1E';
        } else if($m == 5){
            $durationKey = 'B6F3981B-3555-46B3-B49C-AB2986A1557C';
        } else if($m == 6){
            $durationKey = 'B7225D89-DB7B-4359-BBD6-6C98EABE8E5D';
        } else if($m == 7){
            $durationKey = 'CB6E121C-1894-47EF-936D-52D86B3CB54F';
        } else if($m == 8){
            $durationKey = 'D021C2CB-ABF3-4AC8-B480-09304401E9DB';
        } else if($m == 9){
            $durationKey = '6542D3AB-F91C-4BA7-8E02-AD42DC161522';
        } else if($m >= 10){
            $durationKey = 'EA9C5195-D092-433F-9240-EF861048CB6D';
        }

        return $durationKey;
    }

    public static function getVehicleDuration($vehicleDocDate){

        $vehicleDocDate = new \DateTime($vehicleDocDate);
        $now = new \DateTime();
        $interval = $vehicleDocDate->diff($now);

        $m = $interval->m;

        $vehicleDurationKey = '';

        if($m <= 3){
            $vehicleDurationKey = '00E7BD55-8742-459F-9024-935C8F7E9691';
        } elseif($m == 3){
            $vehicleDurationKey = '6D303BDC-D29C-4AEA-B580-162B403E1854';
        } elseif($m == 4){
            $vehicleDurationKey = 'F230BE1C-041D-4066-8C6A-79E06DC1DB33';
        } elseif($m == 5){
            $vehicleDurationKey = 'D9A39F16-9B2D-4CA4-9B96-95EA79D991D6';
        } elseif($m == 6){
            $vehicleDurationKey = '4BA3EA5B-8316-4D2D-AB60-E3415F2647E7';
        } elseif($m == 7){
            $vehicleDurationKey = '9B6EECD2-5951-4DFC-A901-518078455B8C';
        } elseif($m == 8){
            $vehicleDurationKey = '5700A4DA-1EA4-451C-B9A4-EED0B3ECCC92';
        } elseif($m >= 9){
            $vehicleDurationKey = 'B9442886-6AC3-4D7F-9C5F-B8E420A42016';
        }

        return $vehicleDurationKey;
    }


    public static function getPower($power){

        $powerKey = '';

        if($power <= 50){
            $powerKey = 'AB87E2FF-B72E-4258-A2F8-B4E0247B3C0B';
        } elseif($power > 50 && $power <= 70){
            $powerKey = 'C179C977-5EDB-4C86-BABC-BC3D3F74D910';
        } elseif($power > 70 && $power <= 100){
            $powerKey = '57D3682F-63F9-426F-893D-3D9AEC9282CD';
        } elseif($power > 100 && $power <= 120){
            $powerKey = '18F909BD-5336-42E9-9B44-3A89B775B136';
        } elseif($power > 120 && $power <= 150){
            $powerKey = '8DDACB75-C6C4-40E0-A3D1-244B91F52719';
        } elseif($power > 150){
            $powerKey = '3E15565F-DC1D-48A9-BEB2-C7A38AF489C5';
        }

        return $powerKey;
    }


    public static function getKbm($kbm_class = 3){

        $kbmKey = '0A8BDCC4-D4FF-447C-963D-2EF76010143F';

        if($kbm_class == 0){
            $kbmKey = '12B98CE7-9BA0-4451-9081-08410495DA3F';
        } elseif($kbm_class == 1){
            $kbmKey = '20297731-A8D5-45EA-884C-E4DAF3FB643A';
        } elseif($kbm_class == 2){
            $kbmKey = '0C166F26-32EE-4ABE-941C-3C464D1C4694';
        } elseif($kbm_class == 3){
            $kbmKey = '0A8BDCC4-D4FF-447C-963D-2EF76010143F';
        } elseif($kbm_class == 4){
            $kbmKey = 'B55180A6-0B8D-4656-9ED9-AB2843BA3FDA';
        } elseif($kbm_class == 5){
            $kbmKey = '834ACCB3-091D-4739-8683-068826F58B15';
        } elseif($kbm_class == 6){
            $kbmKey = '6F8A768D-95A4-4E6E-8C44-D5507341BF50';
        } elseif($kbm_class == 7){
            $kbmKey = 'BB22C8B4-BF53-4AD2-9231-ED3E13F666BD';
        } elseif($kbm_class == 8){
            $kbmKey = '060183B8-9FCE-4277-B4DC-2F2378A9C8EA';
        } elseif($kbm_class == 9){
            $kbmKey = '104CB8EB-35C1-4781-A0EA-AC5E3B70C4FC';
        } elseif($kbm_class == 10){
            $kbmKey = 'B1F9E8FE-3CC3-41E3-B317-247D384564C6';
        } elseif($kbm_class == 11){
            $kbmKey = '82B52C01-1221-48D3-80FD-82482FB40DA5';
        } elseif($kbm_class == 12){
            $kbmKey = 'AEB0C49D-CA17-458C-9AF5-7A15E4718963';
        } elseif($kbm_class == 13){
            $kbmKey = '637ABE1F-A987-4481-83BB-6095D4B7532B';
        }

        return $kbmKey;
    }


    public static function getAgeExpirienceType($type = 1){

        $typeKey = 'B84EB620-5C3B-46DC-8FE4-83B6767EE95E';

        if($type == 1){
            $typeKey = 'B84EB620-5C3B-46DC-8FE4-83B6767EE95E';
        } elseif($type == 2){
            $typeKey = '2DC093EF-210E-4499-A80B-B822A10F509B';
        } elseif($type == 3){
            $typeKey = 'DAF1D4B5-275C-4163-9A12-EA70A5667891';
        } elseif($type == 4){
            $typeKey = '2013960F-75AD-4621-B4AA-340941EEFE40';
        } elseif($type == 5){
            $typeKey = '5AEF154C-A493-4227-AD8C-0D5110FF1EA0';
        } elseif($type == 6){
            $typeKey = '8375ADB8-DD23-45E4-AEFA-B4BF0E2B1A82';
        } elseif($type == 7){
            $typeKey = '85847CCC-AD41-46B2-8A2B-61FE1E27B963';
        } elseif($type == 8){
            $typeKey = '449A2A4C-4596-4C1E-831F-EF36D35CFC3C';
        } elseif($type == 9){
            $typeKey = '243F450B-092D-43CB-AF8C-9A4D3C3F5326';
        } elseif($type == 10){
            $typeKey = '71FCA284-C431-4610-98F3-CA48A7460E60';
        } elseif($type == 11){
            $typeKey = '15655BBF-B765-4B1A-9431-77EC85E69757';
        } elseif($type == 12){
            $typeKey = '61EBA0E1-AAEE-4DD1-B60B-E3FBA2BA81DF';
        } elseif($type == 13){
            $typeKey = 'D6F8DBA6-D2CB-481C-80FE-D26AA0C94A6B';
        } elseif($type == 14){
            $typeKey = '95C71B97-D464-4625-B25F-CEA9C5E45061';
        } elseif($type == 15){
            $typeKey = 'A4D8AA95-43F3-4514-A9AB-82D202E2D633';
        } elseif($type == 16){
            $typeKey = '394D5CD9-BC4B-49AE-B987-75B7E5E11FEA';
        } elseif($type == 17){
            $typeKey = '4831F455-E776-419F-9649-CA942554F145';
        } elseif($type == 18){
            $typeKey = '570C6F23-D81F-4B14-8D49-D78445757A74';
        }

        return $typeKey;
    }

}

