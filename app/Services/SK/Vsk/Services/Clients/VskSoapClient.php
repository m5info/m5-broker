<?php


namespace App\Services\SK\Vsk\Services\Clients;


use SoapHeader;
use SoapVar;

class VskSoapClient extends \SoapClient{

    public $login;
    public $password;
    protected $soapClient;


    public function __construct($params)
    {
        ini_set("soap.wsdl_cache_enabled", "0");
        ini_set("default_socket_timeout", 240);
        set_time_limit(240);

        $wsdl = "{$params['url']}/{$params['service_name']}?WSDL";
        parent::__construct($wsdl, [
            'trace' => true,
            'exception' => false,
            'encoding'=>'utf8',
            'soap_version' => SOAP_1_2,
            'connection_timeout' => 240,
            'keep_alive' => 0,
        ]);
    }

    public function __call($function_name, $arguments){

//        $auth           = <<<XML
//<o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:s="s">
//  <o:UsernameToken>
//  <o:Username>{$this->login}</o:Username>
//  <o:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd#PasswordString">{$this->password}</o:Password>
//  </o:UsernameToken>
//</o:Security>
//XML;
//        $authvalues      = new SoapVar($auth, XSD_ANYXML);
//
//        $security_header = new SoapHeader("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", $authvalues, true);
//
//        $this->__setSoapHeaders([$security_header]);

        return parent::__call($function_name, $arguments);
    }

}