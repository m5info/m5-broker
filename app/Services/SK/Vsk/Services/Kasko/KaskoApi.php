<?php
namespace App\Services\SK\Vsk\Services\Kasko;

/**
 * Class KaskoApi
 * Страхования компания - ВСК
 * @package App\Services\SK\Vsk\Services\Kasko
 */
class KaskoApi
{

    CONST CALCV2_WSDL_URL = 'http://ws-test.vsk.ru:8501/cxf/partners/policy2?wsdl';

    public function calcV2()
    {
        $client = new \SoapClient(self::CALCV2_WSDL_URL, array(
            'trace' => 1,
            'exception' => 0,
            'login' => 'Agentis',
            'password' => 'Agentis'
        ));
        $functions = $client->__getFunctions();
        print '<pre>';
        print_r($functions);
        print '</pre>';

        $params = [
            'PRODUCT' => '',
            'PROGRAMTYPE' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => 'Link_Programm'
            ],
            'DURATION' => '12',
            'DURATIONTYPE' => 'M',
            'STOA' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => 'Link_STOA'
            ],
            'INSTALLMENT' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => 'Link_Installment'
            ],
            'KFAV' => '1',
            'LDUTYPE' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => 'Link_Ldu_Type'
            ],
            'INSUREDTYPE' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => 'Link_Person_Type'
            ],
            'OWNERTYPE' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => 'Link_Person_Type'
            ],
            'PROLONGATION' => 'false',
            'INSUREDCLASS' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => 'Link_Driver_Clas'
            ],
            'MODEOFUSE' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => 'Link_ModeOfUse'
            ],
            'REGINOTARIFF' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => 'Link_Region'
            ],
            'ENCUMBRANCETYPE' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => 'Link_Encumbrance'
            ],
            'EFFECTIVEDATE' => '',
            'INSURER_PERSON_NAME1' => '',
            'INSURER_PERSON_NAME2' => '',
            'INSURER_PERSON_NAME3' => '',
            'INSURER_BIRTHDAY' => '',
            'BENEFICIARY_TYPE' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => 'Link_Person_Type'
            ],
            'OWNER_PERSON_NAME1' => '',
            'OWNER_PERSON_NAME2' => '',
            'OWNER_PERSON_NAME3' => '',
            'OWNER_BIRTHDAY' => '',
            'VEHICLELIST' => [
                [
                    'CARMODEL' => [
                        'Id' => '',
                        'SysId' => 'B2B',
                        'Type' => 'Link_VEHICLE_MOD'
                    ],
                    'CONSTRUCTED' => '',
                    'DATERUN' => '',
                    'COST' => '',
                    'KILOMETERS' => '',
                    'ENGINEPOWER' => '',
                    'MOTORTYPE' => [
                        'Id' => '',
                        'SysId' => 'B2B',
                        'Type' => 'Link_VEHICLE_Motor'
                    ],
                    'MOTORVOLUME' => '',
                    'KPPTYPE' => [
                        'Id' => '',
                        'SysId' => 'B2B',
                        'Type' => 'Link_VEHICLE_KPP'
                    ],
                    'BODYTYPE' => [
                        'Id' => '',
                        'SysId' => 'B2B',
                        'Type' => 'Link_VEHICLE_BodyType'
                    ],
                    'FRANCHTYPE' => [
                        'Id' => '',
                        'SysId' => 'B2B',
                        'Type' => 'Link_FranchType'
                    ],
                    'FRANCHSUM' => [
                        'Id' => '',
                        'SysId' => 'B2B',
                        'Type' => 'Link_FranchSum'
                    ],
                    'FullINSURANCE' => [
                        'Id' => '',
                        'SysId' => 'B2B',
                        'Type' => 'Link_InsType'
                    ],
                    'CASCOSUM' => '',
                    'DISCOUNTKV' => '',
                    'ISDAMAGE' => '',
                    'ISCASCO' => '',
                    'VIN' => '',
                    'REG_NUMBER' => '',
                    'PTS' => '',
                    'PTS_DATE' => '',
                    'DRIVERLIST' => [
                        [
                            'DRIVERAGE' => '',
                            'DRIVINGEXPIRIENCE' => ''
                        ]
                    ],
                    'ATSLIST' => [
                        [
                            'Id' => '',
                            'SysId' => 'B2B',
                            'Type' => 'Link_Model_PSS'
                        ]
                    ],
                    'DOLIST' => [
                        [
                            'NAMEDO' => '',
                            'SUMDO' => ''
                        ]
                    ],
                    'OWNERS_COUNT' => '',
                    'CRASH_COUNT' => '',
                    'PRODUCER_COUNTRY' => [
                        'Id' => '',
                        'SysId' => 'B2B',
                        'Type' => 'PRODUCER_COUNTRY'
                    ],
                    'PTS_ORGANISATION' => [
                        'Id' => '',
                        'SysId' => 'B2B',
                        'Type' => 'PTS_ORGANISATION'
                    ],
                    'VIN_STATE' => [
                        'Id' => '',
                        'SysId' => 'B2B',
                        'Type' => 'VIN_STATE'
                    ]
                ]
            ],
            'INSURER_DOCS' => [
                [
                    'DOC_TYPE' => [
                        'Id' => '',
                        'SysId' => 'B2B',
                        'Type' => 'Link_DOC_TYPE_NP'
                    ],
                    'DOC_SERIA' => '',
                    'DOC_NUMBER' => '',
                    'DOC_DATE' => ''
                ]
            ],
            'INSURER_ADDRESS' => [
                [
                    'ADDRESS_TYPE' => [
                        'Id' => '',
                        'SysId' => 'B2B',
                        'Type' => 'Link_DOC_TYPE_NP'
                    ],
                    'APARTMENT' => '',
                    'KLADR' => '',
                    'BUILDING' => '',
                    'STREET' => '',
                    'CITY' => '',
                    'FULL_ADDRESS' => '',
                    'INDEX' => '',
                    'SUBJECT' => '',
                    'HOUSE' => ''
                ]
            ],
            'INSURER_BIRTHCITY' => '',
            'INSURER_BIRTHCOUNTRY' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => ''
            ],
            'INSURER_CITIZENSHIP' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => ''
            ],
            'PARTNERSDISCOUNT' => '',
            'OWNER_DOCS' => [
                [
                    'DOC_TYPE' => [
                        'Id' => '',
                        'SysId' => 'B2B',
                        'Type' => 'Link_DOC_TYPE_NP'
                    ],
                    'DOC_SERIA' => '',
                    'DOC_NUMBER' => '',
                    'DOC_DATE' => ''
                ]
            ],
            'OWNER_ADDRESS' => [
                [
                    'ADDRESS_TYPE' => [
                        'Id' => '',
                        'SysId' => 'B2B',
                        'Type' => 'Link_DOC_TYPE_NP'
                    ],
                    'APARTMENT' => '',
                    'KLADR' => '',
                    'BUILDING' => '',
                    'STREET' => '',
                    'CITY' => '',
                    'FULL_ADDRESS' => '',
                    'INDEX' => '',
                    'SUBJECT' => '',
                    'HOUSE' => ''
                ]
            ],
            'OWNER_BIRTHCOUNTRY' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => ''
            ],
            'OWNER_BIRTHCITY' => '',
            'OWNER_CITIZENSHIP' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => ''
            ],
            'INSURER_SEX' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => 'Link_Gender'
            ],
            'OWNER_SEX' => [
                'Id' => '',
                'SysId' => 'B2B',
                'Type' => 'Link_Gender'
            ]
        ];

        $calcResponse = $client->CalcV2($params);

        print '<pre>';
        print_r($calcResponse);
        print '</pre>';
    }

}
