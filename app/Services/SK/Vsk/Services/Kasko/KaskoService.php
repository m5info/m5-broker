<?php
namespace App\Services\SK\Vsk\Services\Kasko;

use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\ContractsCalculation;

class KaskoService implements ProductServiceInterface
{

    public $api;

    public function __construct(KaskoApi $api){
        $this->api = $api;
    }

    public function calc(ContractsCalculation $calc){

    }

    public function temp_calc(ContractsCalculation $calc){

    }

    public function release(ContractsCalculation $calc){

    }

    public function check_status(ContractsCalculation $calc){

    }

    public function get_files(ContractsCalculation $calc){

    }

}
