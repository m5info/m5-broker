<?php

namespace App\Services\SK\Vsk\Services\SendAPI;

class Send{

    public $Source = 'b2b';

    public $url;
    public $login;
    public $password;

    public $SessionId;
    public $UserId;

    public $api_setting = null;


    private $state_api_work = 0;


    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

        $this->api_setting = $api_setting;

    }

    public function send($xml, $method, $receiver_api, $correlationId)
    {

        $curl = curl_init();

        $reply_to = 'http://'.config('app.output_ip_address').'/api/integration_receiver/vsk/response/'.$receiver_api;

        $headers = [
            /*'ReplyTo: http://192.168.217.33:8080/callback/answer',*/
            'ReplyTo: ' . $reply_to,
            'Content-type: application/xml',
            'X-VSK-CorrelationId: ' . $correlationId,
        ];

        //dump($headers);
//        dump($this->url.$method);

        try{
            curl_setopt_array($curl, array(
                CURLOPT_URL => $this->url.$method,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 20,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $xml,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYPEER => true,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSLCERT => dirname(__FILE__).'/Cert/cert.pem',
                CURLOPT_CAINFO => dirname(__FILE__).'/Cert/chain[all-hosts].pem',
                CURLOPT_CAPATH => dirname(__FILE__).'/Cert/',
                CURLINFO_HEADER_OUT => true,
            ));
        }catch(\Exception $e){
            echo $e->getMessage();
        }

//        dump($xml);

        $response = curl_exec($curl);

        //dump(curl_getinfo($curl));

        $err = curl_error($curl);

        //dump($err);

//        dd('response:'. $response);

        curl_close($curl);


        if ($err) {
//            dd('error CURL: '. $err);
            //Логируем ошибки
            return null;
        }

        //dd($response);
        return $response;



    }


    public function responseError($txt)
    {
        $res = new \stdClass();
        $res->Error = new \stdClass();
        $res->Error->Code = 10;
        $res->Error->Text = $txt;
        return $res;
    }

}