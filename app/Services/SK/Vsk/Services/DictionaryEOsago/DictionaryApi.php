<?php

namespace App\Services\SK\Vsk\Services\DictionaryEOsago;

use App\Models\Vehicle\VehicleCategories;
use App\Models\Vehicle\VehiclePurpose;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Facades\Excel;

class DictionaryApi
{

    public $csv_file = __DIR__ . "\lists\models_03_07.csv";


    public function get_categories()
    {
        return [
           ['id' => 'A', 'title' => 'Мотоциклы, мопеды и легкие квадрициклы (транспортные средства категории «А», «М»)'],
           ['id' => 'B', 'title' => 'Транспортные средства категории «B», «BE»'],
           ['id' => 'С15', 'title' => 'Транспортные средства категорий «С» и «СЕ» с разрешенной максимальной массой 16 тонн и менее'],
           ['id' => 'С16', 'title' => 'Транспортные средства категорий «С» и «СЕ» с разрешенной максимальной массой более 16 тонн'],
           ['id' => 'D15', 'title' => 'Транспортные средства категорий «D» и «DЕ» с числом пассажирских мест до 16 включительно'],
           ['id' => 'D16', 'title' => 'Транспортные средства категорий «D» и «DЕ» с числом пассажирских мест более 16'],
           ['id' => 'TB', 'title' => 'Троллейбусы (транспортные средства категории «Tb»)'],
           ['id' => 'TM', 'title' => 'Трамваи (транспортные средства категории «Tm»)'],
           ['id' => 'T', 'title' => 'Тракторы, самоходные дорожно-строительные и иные машины, за исключением транспортных средств, не имеющих колесных движителей'],
        ];
    }

    public function get_models()
    {
        $marks = $this->get_marks(true);

        foreach ($marks as $key => $mark){

        }

        $models = [];
    }

    public function get_marks($temp = false)
    {
        $list = [];
        if(is_file($this->csv_file)){
            $handle = fopen($this->csv_file, "r");
            if ($handle) {
                $current_name = '';
                $counter = 1;
                while (($buffer = fgets($handle, 4096)) !== false) {
                    $temp_arr = explode(';', $buffer);
                    $temp_arr[1] = str_replace("\r\n", "", $temp_arr[1]);

                    if ($current_name != $temp_arr[0]){
                        if ($temp){
                            $list[(string)$counter] = (string)$temp_arr[0];
                        }else{
                            $mark = new \stdClass();
                            $mark->mark_id = (string)$counter;
                            $mark->mark = (string)$temp_arr[0];
                            $list[] = $mark;
                        }

                        $current_name = $temp_arr[0];
                        $counter++;
                    }


                }
                if (!feof($handle)) {
                    echo "Ошибка: fgets() неожиданно потерпел неудачу\n";
                }
                fclose($handle);
            }
        }

        return $list;
    }

    public function get_purpose(){

        return [
            ['id' => 'PERSONAL', 'title' => 'Личная'],
            ['id' => 'TAXI', 'title' => 'Такси'],
            ['id' => 'TRAINING', 'title' => 'Учебная езда'],
            ['id' => 'SPECIAL', 'title' => 'Дорожные и специальные транспортные средства'],
            ['id' => 'OTHER', 'title' => 'Прочее'],
            ['id' => 'PASSENGER_TRANSPORT', 'title' => 'Регулярные пассажирские перевозки/перевозки пассажиров по заказам'],
            ['id' => 'DANGEROUS_TRANSPORTATION', 'title' => 'Перевозка опасных и легко воспламеняющихся грузов'],
            ['id' => 'RENTAL', 'title' => 'Прокат/краткосрочная аренда'],
            ['id' => 'EMEDGENCY', 'title' => 'Экстренные и коммунальные службы'],
        ];
    }

    private function get_temporary_list_marks()
    {



    }

}
