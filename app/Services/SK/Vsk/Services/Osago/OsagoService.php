<?php

namespace App\Services\SK\Vsk\Services\Osago;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\File;
use App\Models\IntegrationOnline\VskApiOnline;
use App\Repositories\FilesRepository;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use stdClass;

class OsagoService implements ProductServiceInterface{

    public $available_types = [
        0 => 'Бумажный',
        1 => 'Электронный',
    ];

    public $osago_api;
    public $eosago_api;

    public function __construct(OsagoApiFactory $api){
        $this->osago_api = $api->osago_api;
        $this->eosago_api = $api->eosago_api;
    }


    public function temp_calc(ContractsCalculation $calc)
    {


    }


    public function calc(ContractsCalculation $calc)
    {
        $contract = $calc->contract;

        $response = $this->getStartAnswerObject();

        if($contract->is_epolicy){

            $response->error = 'Е-ОСАГО у данной СК недоступно!';
            $response->state = false;
            $response->statys_id = 1;
            $response->aswer = '';
            $response->payment_total = 0;
            $response->msg = '';

            return $response;

            $vsk_api_online = VskApiOnline::query()->where('id_correlation', $calc->id)->first();

            if (!$vsk_api_online){

                $vsk_api_online = new VskApiOnline();
                $vsk_api_online->created_at = date('Y-m-d H:i:s');
                $vsk_api_online->step = 'auth';
                $vsk_api_online->id_correlation = $calc->id;
                $vsk_api_online->save();

            }

            if (!empty($vsk_api_online->id_bp) && $vsk_api_online->step !== 'end_point') {
                $vsk_api_online->step = 'calc';
                $vsk_api_online->save();
            }

            $result = $this->eosago_api->{$vsk_api_online->step}($calc, $vsk_api_online);

            if ($result->state){
                $response->error = '';
                $response->state = true;
                $response->statys_id = 1;
                $response->aswer = $result->answer;
                $response->payment_total = $result->payment_total;
                $response->msg = '';

            }else{
                $response->error = $result->error;
            }

        } else {
            $result = $this->osago_api->calc_osago($calc);

            if($result->state && isset($result->answer)){
                $answer = $result->answer;

                $response->state = true;
                $response->statys_id = 1;
                $response->answer = $answer;
                $response->payment_total = (string)$answer->PremiumSum;

                if (isset($answer->CalcInfo)) {
                    foreach($answer->CalcInfo as $coeff){
                        $response->msg .= "$coeff->ItemName: $coeff->Value; ";
                    }
                }

            }else{
                $response->error = $result->error;
            }
        }

        return $response;
    }

    public function savePolicy(ContractsCalculation $calc)
    {
        $response = $this->getStartAnswerObject();

        $vsk_api_online = VskApiOnline::query()->where('id_correlation', $calc->id)->first();
        $result = $this->api->{$vsk_api_online->step}($calc, $vsk_api_online, 'save_policy_response');

        if ($result->state){
            $response->error = '';
            $response->state = true;
            $response->statys_id = 1;
            $response->aswer = $result->answer;
            $response->payment_total = $result->payment_total;
            $response->msg = '';

        }else{
            $response->error = $result->error;
        }

        return $response;
    }

    public function sign(ContractsCalculation $calc, Request $request)
    {
        $response = $this->getStartAnswerObject();

        $vsk_api_online = VskApiOnline::query()->where('id_correlation', $calc->id)->first();

        if ($vsk_api_online->step == 'auth'){
            $method = 'sign';
        }elseif($vsk_api_online->step == 'sign'){
            $method = 'end_point';
        }

        dump($method);

        $vsk_api_online->save();

        $result = $this->api->{$method}($calc, $vsk_api_online, $request);

        if ($result->state){
            $response->error = '';
            $response->state = true;
            $response->statys_id = 1;
            $response->aswer = $result->answer;
            $response->payment_total = $result->payment_total;
            $response->msg = '';

        }else{
            $response->error = $result->error;
        }

        return $response;
    }

    public function buyPolicy(ContractsCalculation $calc, Request $request)
    {

    }


    public function release(ContractsCalculation $calc)
    {
        $contract = $calc->contract;

        $response = $this->getStartAnswerObject();

        if($contract->is_epolicy){

            $vsk_api_online = VskApiOnline::query()->where('id_correlation', $calc->id)->first();

            if ($vsk_api_online->step == 'auth'){
                $method = 'buyPolicy';
            }elseif($vsk_api_online->step == 'buyPolicy'){
                $method = 'end_point';
            }

            dump($method);

            $vsk_api_online->save();

            $result = $this->api->{$method}($calc, $vsk_api_online);

            if ($result->state){
                $response->error = '';
                $response->state = true;
                $response->statys_id = 1;
                $response->aswer = $result->answer;
                $response->payment_total = $result->payment_total;
                $response->msg = '';

            }else{
                $response->error = $result->error;
            }

        } else{

            $result = $this->osago_api->release_osago($calc);

            $response->bso_id = $contract->bso->id;

            if($result->state){

                $calc->update([
                    'sk_key_id' => $result->answer->CalcInfo[0]->Value,
                ]);

                $new_sum = $this->osago_api->change_contract_state($calc); // меняем статус и обновляем премию

                $response->state = true;
                $response->statys_id = 4; //Выпущен

                $calc->update([
                    'sum' => $new_sum,
                ]);

            }else{
                $response->error = $result->error;
            }

        }

        return $response;
    }


    public function check_status(ContractsCalculation $calc)
    {


    }


    public function get_files(ContractsCalculation $calc)
    {
        $files_list = $this->osago_api->get_files_list($calc);

        $contract = $calc->contract;

        foreach ($files_list->CalcInfo as $key => $templ){

            $document = $this->osago_api->getDocument($calc, $templ->Value);

            if(isset($document->CalcInfo) && isset($document->CalcInfo->Value))
            {
                $fileData = $document->CalcInfo->Value;
                $file_contents = base64_decode($fileData);
                $path = storage_path('app/'.Contracts::FILES_DOC . "/{$contract->id}/");

                if (!is_dir(($path))) {
                    mkdir(($path), 0777, true);
                }

                $file_name = uniqid();

                file_put_contents($path . $file_name.'.pdf', $file_contents);


                $file = File::create([
                    'original_name' => $templ->ItemName,
                    'ext'           => 'pdf',
                    'folder'        => Contracts::FILES_DOC . "/{$contract->id}/",
                    'name'          => $file_name,
                    'user_id'       => auth()->id()
                ]);


                $contract->masks()->save($file);

            }


        }

        return true;
    }

    private function getStartAnswerObject()
    {
        $response = new stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';

        return $response;
    }

}