<?php

namespace App\Services\SK\Vsk\Services\Osago;


use App\Models\Contracts\ContractsCalculation;
use App\Services\SK\Vsk\Services\SendAPI\Send;
use Carbon\Carbon;
use stdClass;

class EOsagoApi{


    public $url;
    public $login;
    public $password;
    public $Send = null;
    public $SessionId = null;
    public $errors = [];

    private $xml_generator = null;
    private $validator = null;

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->Send = new Send($url, $login, $password, $api_setting);
        $this->xml_generator = new OsagoXML();
        $this->validator = new OsagoValidator();

    }

    public function auth(ContractsCalculation $calc, $vsk_api_online, $receiver_api = 'calc_response')
    {
        $method = 'v2/osago/Auth/Login';

        $this->errors = $this->validator->calc($calc);

        /*if ($this->checkError()){
            return $this->responseError();
        };*/

        $xml = $this->xml_generator->authXML($calc);

        return $this->Send->send($xml, $method, $receiver_api, $calc->id);
    }



    public function sign(ContractsCalculation $calc, $vsk_api_online, $request)
    {
        $method = 'v2/osago/Policy/SignPolicy';
        $receiver_api = 'sign_response';

        $this->errors = $this->validator->sign($calc, $vsk_api_online, $request);

        if ($this->checkError()){
            return $this->responseError();
        };

        $xml = $this->xml_generator->signXML($calc, $vsk_api_online, $request);

        return $this->Send->send($xml, $method, $receiver_api, $calc->id);
    }

    public function calc(ContractsCalculation $calc, $vsk_api_online)
    {
        $method = 'v2/osago/Policy/CalculatePolicy';
        $receiver_api = 'calc_response';

        $this->errors = $this->validator->calc($calc);

        /*if ($this->checkError()){
            return $this->responseError();
        };*/

        $params = $this->xml_generator->calcXML($calc, $vsk_api_online);
        //$params = $this->xml_generator->getCalcParamsTest($calc, $vsk_api_online);

        return $this->Send->send($params, $method, $receiver_api, $calc->id);
    }

    public function savePolicy(ContractsCalculation $calc, $vsk_api_online)
    {
        $method = 'v2/osago/Policy/SavePolicy';
        $receiver_api = 'save_policy_response';

        $this->errors = $this->validator->calc($calc);

        if ($this->checkError()){
            return $this->responseError();
        };

        $xml = $this->xml_generator->savePolicyXML($calc, $vsk_api_online);

        return $this->Send->send($xml, $method, $receiver_api, $calc->id);
    }

    public function buyPolicy(ContractsCalculation $calc, $vsk_api_online, $request = null)
    {
        $method = 'v2/osago/Policy/BuyPolicy';
        $receiver_api = 'buy_policy_response';

        $xml = $this->xml_generator->buyPolicyXML($calc, $vsk_api_online);

        return $this->Send->send($xml, $method, $receiver_api, $calc->id);
    }

    public function end_point(ContractsCalculation $calc, $vsk_api_online, $request = null)
    {

        $result = new stdClass();
        $result->state = true;
        $result->answer = 'Завершена очередь запросов';
        $result->payment_total = $vsk_api_online->temp_amount;
        $vsk_api_online->step = 'auth';
        $vsk_api_online->save();

        return $result;
    }

    private function responseError()
    {
        $r = new \stdClass();

        $r->error = 'Ошибки валидации :<br>';
        foreach ($this->errors as $error_key => $error_value){
            $error_key = $error_key+1;
            $r->error = (string)$r->error.'<br>'.$error_key.'. '.$error_value;
        }

        $r->state = false;
        return $r;
    }

    private function checkError()
    {
        if (isset($this->errors[0])){
            return true;
        }else{
            return false;
        }
    }

}