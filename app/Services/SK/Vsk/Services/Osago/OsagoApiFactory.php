<?php


namespace App\Services\SK\Vsk\Services\Osago;


class OsagoApiFactory
{
    public $eosago_api;
    public $osago_api;


    public function __construct($url, $login, $password){

        $this->eosago_api = new EOsagoApi($url, $login, $password, 123);
        $this->osago_api = new OsagoApi($url, $login, $password);
    }

}