<?php

namespace App\Services\SK\Vsk\Services\Osago;

use App\Models\Contracts\ContractsCalculation;

class OsagoValidator
{

    public $errors = [];

    public function auth(ContractsCalculation $calc, $vsk_api_online)
    {
        if (!$calc->contract->insurer->phone){
            $this->errors[] = 'Не указан номер страхователя';
        }

        return $this->errors;
    }

    public function sign(ContractsCalculation $calc, $vsk_api_online, $request)
    {
        if (isset($request->sms_code) && !$request->sms_code){

            $this->errors[0] = 'СМС-код не может быть пустым';

        }else{

        }

        return $this->errors;
    }

    public function calc(ContractsCalculation $calc)
    {
        $contract = $calc->contract;
        $owner = $contract->owner;
        $insurer = $contract->insurer;
        $car_data = $calc->object_insurer_auto;
        $power = $car_data->power;

        $owner_info = $owner->get_info();
        $owner_fio_parts = explode(' ', $owner_info->fio);

        $insurer_info = $insurer->get_info();
        $insurer_fio_parts = explode(' ', $insurer_info->fio);

        $sk_car_mark = $calc->sk_mark;
        $sk_car_model = $calc->sk_model;

        $object_insurer_auto = $contract->object_insurer_auto;

        if (countDayToDates($contract->begin_date, date('d.m.Y')) < 4){
            $this->errors[] = 'Дата начала договора должна быть на 3 дня позже даты оформления (сегодняшнего дня)';
        }

        if (!$insurer->phone || !$insurer->email){
            $this->errors[] = 'Не указан номер или email страхователя';
        }elseif ($insurer->email && !filter_var($insurer->email, FILTER_VALIDATE_EMAIL)){
            $this->errors[] = "Неверный формат email страхователя";
        }

        if (!$insurer->doc_serie || !$insurer->doc_number  || $insurer_info->doc_date == '0000-00-00' || (!isset($insurer_fio_parts[1]) || !isset($insurer_fio_parts[0]) || !isset($insurer_fio_parts[2]))){
            $this->errors[] = 'Не указаны ФИО, серия, номер или дата выдачи паспорта страхователя';
        }

        if (!$insurer_info->birthdate || $insurer_info->birthdate == '' || $insurer_info->birthdate == '0000-00-00'){
            $this->errors[] = 'Не указана дата рождения страхователя';
        }

        if (!$owner->doc_serie || !$owner->doc_number || $owner_info->doc_date == '0000-00-00' || (!isset($owner_fio_parts[1]) || !isset($owner_fio_parts[0]) || !isset($owner_fio_parts[2]))){
            $this->errors[] = 'Не указаны ФИО, серия, номер или дата выдачи паспорта собственника';
        }

        if (!$owner_info->birthdate || $owner_info->birthdate == '' || $owner_info->birthdate == '0000-00-00'){
            $this->errors[] = 'Не указана дата рождения собственника';
        }

        if ($power <= 0){
            $this->errors[] = 'Мощность автомобиля не может быть "0.00"';
        }

        if (!isset($sk_car_mark->sk_title)){
            $this->errors[] = 'Не указана марка авто';
        }

        if (isset($sk_car_model->vehicle_models_id) && $sk_car_model->vehicle_models_id <= 0){
            $this->errors[] = 'Не указана модель авто или не сопоставлена со справочником';
        }

        if(!$car_data->docserie || !$car_data->docnumber || $car_data->docdate == '0000-00-00'){
            $this->errors[] = "Не указана серия, номер или дата выдачи документа ТС";
        }

        /*if (!$object_insurer_auto->dk_number || $object_insurer_auto->dk_date == '0000-00-00' || $object_insurer_auto->dk_date_from == '0000-00-00'){
            $this->errors[] = "Не указан номер, дата начала или дата окончания диагностической карты";
        }*/

        if (!$object_insurer_auto->vin || !$object_insurer_auto->reg_number || !$car_data->car_year){
            $this->errors[] = "Не указан VIN, рег. номер или год выпуска авто";
        }

        if (!isset($sk_car_model->vehicle_marks_sk_id) || !isset($sk_car_model->vehicle_models_sk_id)){
            $this->errors[] = "Не найден автомобиль. Обновите данные в справочнике по данной марке\модели `Марок и Моделей`";
        }

        if (!$owner_info->address_register_kladr || !$insurer_info->address_register_kladr){
            $this->errors[] = "Не указан адрес регистрации страховщика или собственника";
        }



        return $this->errors;

    }

}