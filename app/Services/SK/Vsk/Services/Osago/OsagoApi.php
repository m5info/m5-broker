<?php


namespace App\Services\SK\Vsk\Services\Osago;


use App\Models\Contracts\ContractsCalculation;
use App\Services\SK\Vsk\Services\Clients\VskSoapClient;
use App\Services\SK\Vsk\Services\Dictionary\DictionaryApi;
use SoapVar;

class OsagoApi{

    public $url;
    public $login;
    public $password;
    public $product_key = '548A36FB-543E-461D-A339-BCFE719FC31E'; // ОСАГО
    public $errors = [];


    public function __construct($url, $login, $password){
        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
    }

    public function get_service_client($service_name){

        return new VskSoapClient([
            'url' => $this->url,
            'service_name' => $service_name,
            'login' => $this->login,
            'password' => $this->password,
        ]);

    }


    public function calc_osago(ContractsCalculation $calc)
    {
        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $service_client = $this->get_service_client('Companies/vsk/services/osagointegration.asmx');

        $param = $this->get_calc_params($calc);

        if(!empty($this->errors)){
            $result->error = implode(';', $this->errors);
            return $result;
        }

        $param = new SoapVar($param, XSD_ANYXML);

        try{
            $answer = $service_client->CALC_DATAv2($param);

            if($answer->Results->PremiumSum == 0){
                $result->error = isset($answer->Results->Message) ? $answer->Results->Message : 'Ошибка расчета';
            } else {
                $result->state = true;
                $result->answer = $answer->Results;
            }

        }catch(\SoapFault $fault){
//            dump($service_client->__getLastRequest());
//            dump($service_client->__getLastResponseHeaders());
//            dd($fault->getMessage());
            $result->error = $fault->getMessage();
        }

        return $result;
    }

    public function release_osago(ContractsCalculation $calc){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $service_client = $this->get_service_client('Companies/vsk/services/osagointegration.asmx');

        $param = $this->get_release_params($calc);

        if(!empty($this->errors)){
            $result->error = implode(';', $this->errors);
            return $result;
        }

        $param = new SoapVar($param, XSD_ANYXML);

        try{
            $answer = $service_client->CreatePolicy2016($param);

            if ((int)$answer->Results->PremiumSum == 0){
                $result->error = $answer->Results->Message;
            } else {
                $result->state = true;
                $result->answer = $answer->Results;
            }

        }catch(\SoapFault $fault){
//            dump($service_client->__getLastRequest());
//            dump($service_client->__getLastResponse());
//            dd($service_client->__getLastResponseHeaders());
            $result->error = $fault->getMessage();
        }

        return $result;
    }


    public function change_contract_state(ContractsCalculation $calc)
    {
        $service_client = $this->get_service_client('Companies/vsk/services/osagointegration.asmx');

        $params = <<<XML
    <SetPolicyStatus2016 xmlns="http://tempuri.org/vsk/osagointegration">
      <UserLogin>$this->login</UserLogin>
      <UserPass>$this->password</UserPass>
      <policyID>$calc->sk_key_id</policyID>
      <newStatus>0C9468DD-F53C-4962-8B0B-93BF28ABA6A9</newStatus>
    </SetPolicyStatus2016>
XML;

        $params = new SoapVar($params, XSD_ANYXML);

        $answer = $service_client->SetPolicyStatus2016($params);

        return $answer->Results->PremiumSum;
    }


    public function get_files_list(ContractsCalculation $calc){

        $service_client = $this->get_service_client('Companies/vsk/services/osagointegration.asmx');

        $params = <<<XML
    <GetListPrintDocuments xmlns="http://tempuri.org/vsk/osagointegration">
      <UserLogin>$this->login</UserLogin>
      <UserPass>$this->password</UserPass>
      <policyID>$calc->sk_key_id</policyID>
    </GetListPrintDocuments>
XML;

        $params = new SoapVar($params, XSD_ANYXML);

        $answer = $service_client->GetListPrintDocuments($params);

        return $answer->Results;
    }


    public function getDocument(ContractsCalculation $calc, $documentType){

        $service_client = $this->get_service_client('Companies/vsk/services/osagointegration.asmx');

        $params = <<<XML
    <GetPolicyPrintDocumentPdf xmlns="http://tempuri.org/vsk/osagointegration">
      <UserLogin>$this->login</UserLogin>
      <UserPass>$this->password</UserPass>
      <policyID>$calc->sk_key_id</policyID>
      <documentType>$documentType</documentType>
    </GetPolicyPrintDocumentPdf>
XML;

        $params = new SoapVar($params, XSD_ANYXML);

        $answer = $service_client->GetPolicyPrintDocumentPdf($params);

        return $answer->Results;
    }


    public function get_calc_params(ContractsCalculation $calc){

        $contract = $calc->contract;
        $autoInfo = $calc->object_insurer_auto;

        if($purpose = $calc->sk_purpose){
            $purpose_id = $purpose->vehicle_purpose_sk_id;
        } else {
            $this->errors[] = 'Не синхронизирован справочник Целей использования!';
        }

        if($vehicleType = $calc->sk_category){
            $vehicleType_id = $vehicleType->vehicle_categorie_sk_id;
        }

        $duration = DictionaryApi::getDuration($contract->begin_date, $contract->end_date);
        $vehicleDuration = DictionaryApi::getVehicleDuration($autoInfo->docdate);
        $enginePower = DictionaryApi::getPower($autoInfo->power);

        $trailer = $autoInfo->is_trailer ? 'true' : 'false';
        $multidrive = (int)$contract->drivers_type_id ? 'true' : 'false';
        $lduAgeExpirienceType = (int)$contract->drivers_type_id ? '' : $this->getMinimalAgeExperience($calc);

        $insurer = $contract->insurer;
        $insurerData = $insurer->get_info();
        $owner = $contract->owner;
        $ownerData = $owner->get_info();

        $insurerType = (int)$insurer->type ? 'true' : 'false';
        $ownerType = (int)$owner->type ? 'true' : 'false';

        $owner_birthdate = date('d.m.Y', strtotime($ownerData->birthdate));

        $maxKbm = $this->getMaximalKbm($calc);
//        $kbmClassKey = DictionaryApi::getKbm(3);

        $prev_policy_xml = '';
        if ((int)$contract->is_prolongation){

            $sk_id = '';
            $sk_inn = '';
            $sk_kpp = '';
            $sk_title = '';

            if($sk_sk = $calc->sk_sk){
                $sk_id = $sk_sk->dictionary_sk_sk_id;
                $sk_inn = $sk_sk->sk->inn;
                $sk_kpp = $sk_sk->kpp;
                $sk_title = addcslashes($sk_sk->official_title, '\'');
            } else {
                $this->errors[] = 'Не синхронизирован справочник СК для пролонгации!';
            }

            $prev_policy_num_serie = $contract->prev_policy_serie . $contract->prev_policy_number;

            $prev_policy_xml = <<<XML
<Param n="PRIOR_CONTRACT_NUMBER" v="{$prev_policy_num_serie}" />
<Param n="PRIOR_INSURER_ID" v="{$sk_id}" />
<Param n="PRIOR_INSURER_INN" v="{$sk_inn}" />
<Param n="PRIOR_INSURER_BANK_KPP" v="{$sk_kpp}" />
<Param n="PRIOR_INSURER_NAME" v='{$sk_title}' />
XML;

        }

        $fias = $insurerData->address_fact_city_fias_id;

        $is_insurer = (int)$owner->is_insurer;

        if($sk_mark = $calc->sk_mark){
            $sk_mark_id = $sk_mark->vehicle_marks_sk_id;
        }else {
            $sk_mark_id = '';
            $this->errors[] = 'Не синхронизирован справочник марок!';
        }

        $result = <<<XML
    <CALC_DATAv2 xmlns="http://tempuri.org/vsk/osagointegration">
      <UserLogin>$this->login</UserLogin>
      <UserPass>$this->password</UserPass>
      <Product>$this->product_key</Product>
      <Duration>$duration</Duration>
      <VehicleDuration>$vehicleDuration</VehicleDuration>
      <InsuredJuridical>$insurerType</InsuredJuridical>
      <OwnerJuridical>$ownerType</OwnerJuridical>
      <OutragePolicyProvisions>false</OutragePolicyProvisions>
      <KbmCoeff>$maxKbm</KbmCoeff>
      <InsuredAccidentCount>0</InsuredAccidentCount>
      <PurposeUse>$purpose_id</PurposeUse>
      <VehicleType>$vehicleType_id</VehicleType>
      <EnginePower>$enginePower</EnginePower>
      <LduLimit>$multidrive</LduLimit>
      <LduAgeExpirienceType>$lduAgeExpirienceType</LduAgeExpirienceType>
      <Trailer>$trailer</Trailer>
      <Fias>$fias</Fias>
      <contractDataOsago>
        {$prev_policy_xml}
        <Param n="INSURANCE_OBJECT_VIN" v="{$autoInfo->vin}" />
        <Param n="INSURED_IS_OWNER" v="{$is_insurer}" />
        <Param n="OWNER_BIRTH_DATE" v="{$owner_birthdate}" />
        <Param n="OWNER_DOC_SERIA" v="{$ownerData->doc_serie}" />
        <Param n="OWNER_PERSON_DOC_TYPE" v="629F2ACE-0066-4FF8-9217-03E18E975628" />
        <Param n="INSURANCE_OBJECT_MARK" v="{$sk_mark_id}" />
        <Param n="INSURANCE_OBJECT_HORSE_ENGINE_POWER" v="{$autoInfo->power}" />
      </contractDataOsago>
    </CALC_DATAv2>
XML;

        return $result;
    }


    public function get_release_params(ContractsCalculation $calc){

        $contract = $calc->contract;
        $autoInfo = $calc->object_insurer_auto;

        if($purpose = $calc->sk_purpose){
            $purpose_id = $purpose->vehicle_purpose_sk_id;
        }

        if($vehicleType = $calc->sk_category){
            $vehicleType_id = $vehicleType->vehicle_categorie_sk_id;
        }

        $duration = DictionaryApi::getDuration($contract->begin_date, $contract->end_date);
        $vehicleDuration = DictionaryApi::getVehicleDuration($autoInfo->docdate);
        $enginePower = DictionaryApi::getPower($autoInfo->power);

        $trailer = $autoInfo->is_trailer ? 'true' : 'false';
        $multidrive = (int)$contract->drivers_type_id ? 'true' : 'false';
        $lduAgeExpirienceType = (int)$contract->drivers_type_id ? '' : $this->getMinimalAgeExperience($calc);

        $insurer = $contract->insurer;
        $insurerData = $insurer->get_info();
        $owner = $contract->owner;
        $ownerData = $owner->get_info();

        $insurerType = (int)$insurer->type ? 'true' : 'false';
        $ownerType = (int)$owner->type ? 'true' : 'false';

        $maxKbm = $this->getMaximalKbm($calc);
//        $kbmClassKey = DictionaryApi::getKbm(3);

        $is_insurer = (int)$owner->is_insurer;

        $date_sign = date('d.m.Y');
        $date_begin = date('d.m.Y', strtotime($contract->begin_date));
        $date_end = date('d.m.Y', strtotime($contract->end_date));

        $policySerial = $contract->bso ? $contract->bso->bso_serie->bso_serie : '';
        $policyNumber = $contract->bso->bso_number;

        $drivers_xml = $this->get_drivers_xml($calc);
        $vehicle_xml = $this->get_vehicle_xml($calc);
        $insurer_xml = $this->get_subject_xml($calc, 'insurer');
        $owner_xml = $this->get_subject_xml($calc, 'owner');

        if($agentInfo = $contract->getAgentSettings($calc)){
            $insurer_representative = $agentInfo->agent_name;
            $tochka_prodazh = $agentInfo->сhannel_sale_id;
            $seller_fio = $agentInfo->manager_id;
        }else {
            $insurer_representative = '';
            $tochka_prodazh = '';
            $seller_fio = '';
            $this->errors[] = 'Не настроены данные по агентскому договору!';
        }

        $prev_policy_xml = '';
        if ((int)$contract->is_prolongation){

            $sk_id = '';
            $sk_inn = '';
            $sk_kpp = '';
            $sk_title = '';

            if($sk_sk = $calc->sk_sk){
                $sk_id = $sk_sk->dictionary_sk_sk_id;
                $sk_inn = $sk_sk->sk->inn;
                $sk_kpp = $sk_sk->kpp;
                $sk_title = addcslashes($sk_sk->official_title, '\'');
            } else {
                $this->errors[] = 'Не синхронизирован справочник СК для пролонгации!';
            }

            $prev_policy_num_serie = $contract->prev_policy_serie . $contract->prev_policy_number;

            $prev_policy_xml = <<<XML
<Param n="PRIOR_CONTRACT_NUMBER" v="{$prev_policy_num_serie}" />
<Param n="PRIOR_INSURER_ID" v="{$sk_id}" />
<Param n="PRIOR_INSURER_INN" v="{$sk_inn}" />
<Param n="PRIOR_INSURER_BANK_KPP" v="{$sk_kpp}" />
<Param n="PRIOR_INSURER_NAME" v='{$sk_title}' />
XML;

        }

        $result = <<<XML
    <CreatePolicy2016 xmlns="http://tempuri.org/vsk/osagointegration">
      <UserLogin>$this->login</UserLogin>
      <UserPass>$this->password</UserPass>
      <Product>$this->product_key</Product>
      <Duration>$duration</Duration>
      <VehicleDuration>$vehicleDuration</VehicleDuration>
      <InsuredJuridical>$insurerType</InsuredJuridical>
      <OwnerJuridical>$ownerType</OwnerJuridical>
      <LeaseholderJuridical>false</LeaseholderJuridical>
      <OutragePolicyProvisions>false</OutragePolicyProvisions>
      <KbmCoeff>$maxKbm</KbmCoeff>
      <InsuredAccidentCount>0</InsuredAccidentCount>
      <PurposeUse>$purpose_id</PurposeUse>
      <VehicleType>$vehicleType_id</VehicleType>
      <EnginePower>$enginePower</EnginePower>
      <LduLimit>$multidrive</LduLimit>
      <LduAgeExpirienceType>$lduAgeExpirienceType</LduAgeExpirienceType>
      <Trailer>$trailer</Trailer>
      <PolicySerial>$policySerial</PolicySerial>
      <PolicyNumber>$policyNumber</PolicyNumber>
      <contractDataOsago>
        {$prev_policy_xml}
        {$drivers_xml}
        {$vehicle_xml}
        <Param n="CONTRACT_SIGN_DATE" v="$date_sign" />
        <Param n="OTHER_INSURER_REPRESENTATIVE" v="{$insurer_representative}" />
        <Param n="TOCHKA_PRODAZH" v="{$tochka_prodazh}" />
        <Param n="FIO_SELLER" v="{$seller_fio}" />
        <Param n="CONTRACT_BEGIN_DATE" v="$date_begin" />
        <Param n="USED_BEGIN_DATE1" v="$date_begin" />
        <Param n="USED_END_DATE1" v="$date_end" />
        <Param n="ISSUE_DATE" v="$date_sign" />
        {$insurer_xml}
        {$owner_xml}
        <Param n="INSURED_IS_OWNER" v="{$is_insurer}" />
        <Param n="INSURED_PERSONAL_DATA" v="false" />
      </contractDataOsago>
    </CreatePolicy2016>
XML;

        return $result;
    }

    public function getMaximalKbm(ContractsCalculation $calc){

        $contract = $calc->contract;
        $result = '1.00';

        if($calc->drivers && !(int)$contract->drivers_type_id){

            foreach($calc->drivers as $key => $driver){
                if($key == 0){
                    $result = $driver->kbm;
                }else{
                    $result = $driver->kbm > $result ? $driver->kbm : $result;
                }
            }
        }

        return $result;
    }


    public function getMinimalAgeExperience(ContractsCalculation $calc){

        $now = new \DateTime();

        $types = [];

        if($calc->drivers){
            foreach($calc->drivers as $key => $driver){
                $b = new \DateTime($driver->birth_date);
                $e = new \DateTime($driver->exp_date);

                $years = $b->diff($now)->y;
                $exp = $e->diff($now)->y;

                if($years < 21 && $exp < 2){ // до 21 года включительно со стажем вождения до 2 лет включительно
                    $types[] = 1;
                } elseif($years < 21 && $exp >= 3 && $exp < 6){ // до 21 года включительно со стажем вождения от 3 лет включительно до 6 лет включительно
                    $types[] = 2;
                } elseif($years >= 22 && $years < 24 && $exp < 2){ // от 22 лет включительно до 24 лет включительно со стажем вождения до 2 лет включительно
                    $types[] = 3;
                } elseif($years >= 22 && $years < 24 && $exp >= 3 && $exp < 9){ // от 22 лет включительно до 24 лет включительно со стажем вождения от 3 лет включительно до 9 лет включительно
                    $types[] = 4;
                } elseif($years >= 25 && $years < 29 && $exp === 0){ // от 25 лет включительно до 29 лет включительно со стажем вождения 0 лет
                    $types[] = 5;
                } elseif($years >= 25 && $years < 29 && $exp >= 1){ // от 25 лет включительно до 29 лет включительно со стажем вождения 1 год
                    $types[] = 6;
                } elseif($years >= 25 && $years < 29 && $exp >= 2){ // от 25 лет включительно до 29 лет включительно со стажем вождения 2 года
                    $types[] = 7;
                } elseif($years >= 25 && $years < 29 && $exp >= 3 && $exp < 9){ // от 25 лет включительно до 29 лет включительно со стажем вождения от 3 лет включительно до 9 лет включительно
                    $types[] = 8;
                } elseif($years >= 25 && $years < 29 && $exp >= 10 && $exp < 14){ // от 25 лет включительно до 29 лет включительно со стажем вождения от 10 лет включительно до 14 лет включительно
                    $types[] = 9;
                } elseif($years >= 30 && $years < 59 && $exp < 2){ // от 30 лет включительно до 59 лет включительно со стажем вождения до 2 лет включительно
                    $types[] = 10;
                } elseif($years >= 30 && $years < 34 && $exp >= 3 && $exp < 6){ // от 30 лет включительно до 34 лет включительно со стажем вождения от 3 лет включительно до 6 лет включительно
                    $types[] = 11;
                } elseif($years >= 30 && $years < 34 && $exp >= 7 && $exp < 9){ // от 30 лет включительно до 34 лет включительно со стажем вождения от 7 лет включительно до 9 лет включительно
                    $types[] = 12;
                } elseif($years >= 30 && $years < 34 && $exp >= 10){ // от 30 лет включительно до 34 лет включительно со стажем вождения от 10 лет включительно
                    $types[] = 13;
                } elseif($years >= 35 && $years < 39 && $exp == 3){ // от 35 лет включительно до 39 лет включительно со стажем вождения от 3 лет включительно до 4 лет включительно
                    $types[] = 14;
                } elseif($years >= 40 && $years < 59 && $exp >= 3){ // от 40 лет включительно до 59 лет включительно со стажем вождения от 3 лет включительно
                    $types[] = 15;
                } elseif($years >= 35 && $years < 39 && $exp >= 5){ // от 35 лет включительно до 39 лет включительно со стажем вождения от 5 лет включительно
                    $types[] = 16;
                } elseif($years >= 59 && $exp <= 2){ // от 59 лет включительно со стажем вождения до 2 лет включительно
                    $types[] = 17;
                } elseif($years >= 59 && $exp >= 3){ // от 59 лет включительно со стажем вождения от 3 лет включительно
                    $types[] = 18;
                }
            }
        }

        $type = max($types);

        return DictionaryApi::getAgeExpirienceType($type);
    }

    public function get_drivers_xml(ContractsCalculation $calc)
    {
        $contract = $calc->contract;
        $result = '';

        if(!(int)$contract->drivers_type_id){
            if($calc->drivers){

                foreach($calc->drivers as $key => $driver){

                    $fio = $driver->get_fio();
                    $bd = date('d.m.Y', strtotime($driver->birth_date));
                    $exp_date = date('d.m.Y', strtotime($driver->exp_date));
                    $doc_date = date('d.m.Y', strtotime($driver->doc_date));
                    $gender = $driver->sex ? 'F' : 'M';

                    $doc_type = (int)$driver->foreign_docs ? '80C749E0-697A-4490-8D3F-74E30CE6B6DA' : 'E8EB69A1-FD6C-44E0-A970-598763CAE9F5';

                    $kbm_rsa_request_id = $driver->kbm_rsa_request_id == '' ? 111111 : $driver->kbm_rsa_request_id; // 111111 они временно попросили для теста

                    $result .= <<<XML
    <Param n="USER_{$key}_KBM_REQUEST_ID" v="{$kbm_rsa_request_id}" />
    <Param n="USER_{$key}_LAST_NAME" v="{$fio['last_name']}" />
    <Param n="USER_{$key}_FIRST_NAME" v="{$fio['first_name']}" />
    <Param n="USER_{$key}_MIDDLE_NAME" v="{$fio['middle_name']}" />
    <Param n="USER_{$key}_BIRTH_DATE" v="{$bd}" />
    <Param n="USER_{$key}_SEX" v="{$gender}" />
    <Param n="USER_{$key}_EXP_DATE" v="{$exp_date}" />
    <Param n="USER_{$key}_DOC_TYPE" v="{$doc_type}" />
    <Param n="USER_{$key}_DOC_SERIA" v="{$driver->doc_serie}" />
    <Param n="USER_{$key}_DOC_NUMBER" v="{$driver->doc_num}" />
    <Param n="USER_{$key}_DOC_DATE" v="{$doc_date}" />
    <Param n="USER_{$key}_INSURED_ACCIDENT_COUNT" v="" />
    <Param n="USER_{$key}_FAMILY_STATE" v="1" />
    <Param n="USER_{$key}_KBM_VALUE" v="{$driver->kbm}" />

XML;
                }
            }
        }

        return $result;
    }


    public function get_vehicle_xml(ContractsCalculation $calc)
    {
        $contract = $calc->contract;
        $result = '';

        if($auto = $calc->object_insurer_auto){

            if($sk_mark = $calc->sk_mark){
                $sk_mark_id = $sk_mark->vehicle_marks_sk_id;
            }else {
                $sk_mark_id = '';
                $this->errors[] = 'Не синхронизирован справочник марок!';
            }

            if($sk_model = $calc->sk_model){
                $sk_model_id = $sk_model->vehicle_models_sk_id;
            }else {
                $sk_model_id = '';
                $this->errors[] = 'Не синхронизирован справочник моделей!';
            }

            if($purpose = $calc->sk_purpose){
                $purpose_id = $purpose->vehicle_purpose_sk_id;
            } else {
                $purpose_id = '';
                $this->errors[] = 'Не синхронизирован справочник Целей использования!';
            }

//              производители
//            a878d6cb-59e0-4ea5-8d05-59e55b2f56be - иностранные
//            e05fffb1-d675-4eac-b474-615e3e5fa009 - отечественные

            $doc_type = '';
            switch ((int)$auto->doc_type){
                case 0;
                    $doc_type = 'ea47b4d0-5071-4149-8ab4-d493e9d303da';
                    break;
                case 1;
                    $doc_type = '643138bc-e602-4774-830a-8791e80fb2c9';
                    break;
                case 2;
                    $doc_type = '3d86bf1b-5eba-4c92-b9a7-71bdfa2c3512';
                    break;
            }

            $doc_date = date('d.m.Y', strtotime($auto->docdate));

            $dk_number = $auto->dk_number;
            $dk_date_from = date('d.m.Y', strtotime($auto->dk_date_from));
            $dk_date = date('m.Y', strtotime($auto->dk_date));

            $result = <<<XML
    <Param n="INSURANCE_OBJECT_MANUFACTURE" v="a878d6cb-59e0-4ea5-8d05-59e55b2f56be" />
    <Param n="INSURANCE_OBJECT_MARK" v="{$sk_mark_id}" />
    <Param n="INSURANCE_OBJECT_MODEL" v="{$sk_model_id}" />
    <Param n="INSURANCE_OBJECT_DOCUMENT_TYPE" v="{$doc_type}" />
    <Param n="INSURANCE_OBJECT_HORSE_ENGINE_POWER" v="{$auto->power}" />
    <Param n="INSURANCE_OBJECT_DOC_PTS_SERIA" v="{$auto->docserie}" />
    <Param n="INSURANCE_OBJECT_DOC_PTS_NUMBER" v="{$auto->docnumber}" />
    <Param n="INSURANCE_OBJECT_DOC_PTS_DATE" v="{$doc_date}" />
    <Param n="INSURANCE_OBJECT_VIN" v="{$auto->vin}" />
    <Param n="INSURANCE_OBJECT_VIN_NOT_PRESENT" v="0" />
    <Param n="INSURANCE_OBJECT_REG_NUMBER" v="{$auto->reg_number}" />
    <Param n="INSURANCE_OBJECT_MADE_YEAR" v="{$auto->car_year}" />
    <Param n="INSURANCE_OBJECT_PURPOSE_USE" v="{$purpose_id}" />
    <Param n="INSURANCE_OBJECT_LEASE" v="0" />
    <Param n="INSURANCE_OBJECT_DOC_DK_NUMBER" v="{$dk_number}" />
    <Param n="INSURANCE_OBJECT_DOC_TO_FACT_DATE" v="{$dk_date_from}" />
    <Param n="INSURANCE_OBJECT_DOC_TO_DATE" v="{$dk_date}" />
XML;
        }

        return $result;
    }

    public function get_subject_xml(ContractsCalculation $calc, $subject_name){


        $contract = $calc->contract;
        $subject = [];

        if ($contract_subject = $contract->{$subject_name}) {

            $contract_subject_info = $contract_subject->get_info();

            if ($contract_subject->type == 0) { // если физик

                $param_name_prefix = $subject_name == 'insurer' ? 'INSURED_PERSON' : 'OWNER';
                $add_param_name_prefix = $subject_name == 'insurer' ? 'INSURED_PERSON' : 'OWNER_PERSON';

                $fio = explode(' ', $contract_subject->title);
                $birthdate = date('d.m.Y', strtotime($contract_subject_info->birthdate));
                $phone = oldSetPhoneNumberFormat(parsePhoneNumber($contract_subject->phone), 'dddddddddd');
                $gender = $contract_subject_info->sex ? 'F' : 'M';
                $doc_date = date('d.m.Y', strtotime($contract_subject->doc_date));
                $kladr = substr($contract_subject_info->address_register_city_kladr_id,0, 11);

                $subject = <<<XML
        <Param n="{$param_name_prefix}_BIRTH_DATE" v="$birthdate" />
        <Param n="{$param_name_prefix}_LAST_NAME" v="{$fio[0]}" />
        <Param n="{$param_name_prefix}_FIRST_NAME" v="{$fio[1]}" />
        <Param n="{$param_name_prefix}_MIDDLE_NAME" v="{$fio[2]}" />
        <Param n="{$param_name_prefix}_SEX" v="$gender" />
        <Param n="{$param_name_prefix}_FAMILY_STATE" v="1" />
        <Param n="{$param_name_prefix}_CLADR" v="{$kladr}" />
        <Param n="{$param_name_prefix}_INDEX" v="{$contract_subject_info->address_fact_zip}" />
        <Param n="{$param_name_prefix}_STREET" v="{$contract_subject_info->address_fact_street}" />
        <Param n="{$param_name_prefix}_HOUSENUM" v="{$contract_subject_info->address_fact_house}" />
        <Param n="{$param_name_prefix}_KORPUSNUM" v="{$contract_subject_info->address_fact_block}" />
        <Param n="{$param_name_prefix}_FLATNUM" v="{$contract_subject_info->address_fact_flat}" />
        <Param n="{$param_name_prefix}_PHONE_NUMBER" v="$phone" />
        <Param n="{$param_name_prefix}_MAIL" v="$contract_subject->email" />
        <Param n="{$param_name_prefix}_DOC_SERIA" v="{$contract_subject->doc_serie}" />
        <Param n="{$param_name_prefix}_DOC_NUMBER" v="{$contract_subject->doc_number}" />
        <Param n="{$param_name_prefix}_DOC_ISSUED" v="{$contract_subject_info->doc_info}" />
        <Param n="{$param_name_prefix}_DOC_DATE" v="$doc_date" />
        <Param n="{$add_param_name_prefix}_DOC_TYPE" v="629F2ACE-0066-4FF8-9217-03E18E975628" />
XML;

                if ($contract_subject_info->doc_date == 0){
                    $this->errors[] = 'Заполните дату выдачи документа!';
                }

                if (!$contract_subject->email){
                    $this->errors[] = 'Заполните email!';
                }

            } else {

                $param_name_prefix = $subject_name == 'insurer' ? 'INSURED' : 'OWNER';

                $kladr = substr($contract_subject_info->address_register_city_kladr_id,0, 11);
                $title = htmlspecialchars($contract_subject_info->title);

                $subject = <<<XML
        <Param n="{$param_name_prefix}_JP_NAME" v="$title" />
        <Param n="{$param_name_prefix}_JP_INN" v="$contract_subject_info->inn" />
        <Param n="{$param_name_prefix}_JP_BANK_KPP" v="$contract_subject_info->kpp" />
        <Param n="{$param_name_prefix}_JP_CLADR" v="$kladr" />
        <Param n="{$param_name_prefix}_JP_DOC_TYPE" v="82c8936d-292b-4771-9a11-f0ccdb756b88" />
        <Param n="{$param_name_prefix}_JP_DOC_SERIA" v="$contract_subject_info->doc_serie" />
        <Param n="{$param_name_prefix}_JP_DOC_NUMBER" v="$contract_subject_info->doc_number" />
XML;
            }
        }

        return $subject;
    }
}