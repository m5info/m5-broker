<?php

namespace App\Services\SK\Vsk\Services\Osago;

use App\Models\Contracts\ContractsCalculation;
use Carbon\Carbon;

class OsagoXML
{
    public function authXML(ContractsCalculation $calc)
    {

        $phone = $calc->contract->insurer->phone;
        $phone = str_replace('(', '', $phone);
        $phone = str_replace(')', '', $phone);
        $phone = str_replace('-', '', $phone);
        $phone = str_replace('+', '', $phone);
        $phone = str_replace(' ', '', $phone);

        $messageId = uniqid() /*uniqid()*/; // For example: 5dc3f56134ad1
        $xml = <<<XML
<ns2:loginRequest xmlns="http://www.vsk.ru/schema/partners/common" xmlns:ns2="http://www.vsk.ru/schema/partners/auth" xmlns:ns3="http://www.vsk.ru/schema/partners/model/calc" xmlns:ns4="http://www.vsk.ru/schema/partners/policy" xmlns:ns5="http://www.vsk.ru/schema/partners/user" xmlns:ns6="http://www.vsk.ru/schema/partners/model">
    <messageId>{$messageId}</messageId>
    <ns2:partnerClientId>{$phone}</ns2:partnerClientId>
</ns2:loginRequest>
XML;



        return $xml;
    }

    public function calcXML(ContractsCalculation $calc, $vsk_api_online)
    {

        $sk_car_mark = $calc->sk_mark;

        $vehicle_models_sk_id = '';
        $vehicle_marks_sk_id = '';

        $contract = $calc->contract;

        $begin_date = date('Y-m-d\T00:00:00', strtotime($contract->begin_date)).'+03:00';
        $end_date = date('Y-m-d\T23:59:59', strtotime($contract->end_date)).'+03:00';
        $now = Carbon::now()->format('Y-m-d\T').'00:00:00+03:00';



        $owner = $calc->contract->owner;



        $owner_info = $owner->get_info();
        $owner_fio_parts = explode(' ', $owner_info->fio);

        $owner_doc_date = date('Y-m-d', strtotime($owner_info->doc_date)).'+03:00'; // 2000-01-01

        $car_data = $calc->object_insurer_auto;



        $object_insurer_auto = $contract->object_insurer_auto;

        $period_start_1 = date('Y-m-d', strtotime($contract->begin_date)).'+03:00';
        $period_end_1 = date('Y-m-d', strtotime($contract->end_date)).'+03:00';




        $owner_city_kladr = mb_strimwidth($owner_info->address_register_kladr, 0, 11);



        if ($owner_info->sex == 0){
            $owner_sex = 1;
        }elseif($owner_info->sex == 1){
            $owner_sex = 2;
        }

        if ($car_data->is_trailer == 0){
            $is_trailer = 'false';
        }else{
            $is_trailer = 'true';
        }


        $sk_car_model = $calc->sk_model;

        if (isset($sk_car_model->vehicle_models_sk_id) and isset($sk_car_model->vehicle_marks_sk_id)){
            $vehicle_models_sk_id = $sk_car_model->vehicle_models_sk_id;
            $vehicle_marks_sk_id = $sk_car_mark->vehicle_marks_sk_id;
        }

        $insurer_ = $contract->insurer;
        $insurer = $insurer_->get_info();

        $insurer_->phone = str_replace('(', '', $insurer_->phone);
        $insurer_->phone = str_replace(')', '', $insurer_->phone);
        $insurer_->phone = str_replace('-', '', $insurer_->phone);
        $insurer_->phone = str_replace('(', '', $insurer_->phone);
        $insurer_->phone = str_replace('+', '', $insurer_->phone);
        $insurer_->phone = str_replace(' ', '', $insurer_->phone);

        $insurer_city_kladr = mb_strimwidth($insurer->address_register_kladr, 0, 11);

        if ($insurer->sex == 0){
            $owner_sex = 1;
        }elseif($insurer->sex == 1){
            $owner_sex = 2;
        }

        $drivers_xml = $this->get_drivers_data_calc_xml($calc);

        $insurer_fio_info = explode(' ', $insurer->fio);

        $vehicleDateIssue = $car_data->docdate.'+03:00';
        $diagnosticCardDateIssue = $object_insurer_auto->dk_date_from.'+03:00';
        $diagnosticCardValidThru = $object_insurer_auto->dk_date.'+03:00';
        $ownerBirthdate = $owner_info->birthdate.'+03:00';
        $insurerPassportRFDateIssue = $insurer->doc_date.'+03:00';
        $usedSince = $car_data->docdate.'+03:00';
        $insurerBirthdate = $insurer->birthdate.'+03:00';

        /*
         * <ns2:docs>
                    <ns2:objectDocType>
                        <ns2:objectDocTypeCode>DIAGNOSTIC_CARD</ns2:objectDocTypeCode>
                    </ns2:objectDocType>
                    <ns2:series></ns2:series>
                    <ns2:number>{$object_insurer_auto->dk_number}</ns2:number>
                    <ns2:dateIssue>{$diagnosticCardDateIssue}</ns2:dateIssue>
                    <ns2:validThru>{$diagnosticCardValidThru}</ns2:validThru>
                </ns2:docs>
        */

        $xml = <<<XML
<ns3:calculatePolicyRequest xmlns="http://www.vsk.ru/schema/partners/common" xmlns:ns6="http://www.vsk.ru/schema/partners/user" xmlns:ns5="http://www.vsk.ru/schema/partners/auth" xmlns:ns2="http://www.vsk.ru/schema/partners/model/calc" xmlns:ns4="http://www.vsk.ru/schema/partners/model" xmlns:ns3="http://www.vsk.ru/schema/partners/policy">
    <messageId>{$vsk_api_online->id_message}</messageId>
    <ns3:policy>
        <ns2:product>
            <ns2:productCode>OSAGO</ns2:productCode>
        </ns2:product>
        <ns2:dateCreate>{$now}</ns2:dateCreate>
        <ns2:dateStart>{$begin_date}</ns2:dateStart>
        <ns2:dateEnd>{$end_date}</ns2:dateEnd>
        <ns2:previousPolicyNumber>{$contract->prev_policy_number}</ns2:previousPolicyNumber>
        <ns2:policyObjects>
            <ns2:object xsi:type="ns2:VehicleXT" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <ns2:objectType>
                    <ns2:code>VEHICLE</ns2:code>
                </ns2:objectType>
                <ns2:docs>
                    <ns2:objectDocType>
                        <ns2:objectDocTypeCode>VEHICLE_REGISTRATION_CERTIFICATE</ns2:objectDocTypeCode>
                    </ns2:objectDocType>
                    <ns2:series>{$car_data->docserie}</ns2:series>
                    <ns2:number>{$car_data->docnumber}</ns2:number>
                    <ns2:dateIssue>{$vehicleDateIssue}</ns2:dateIssue>
                </ns2:docs>
                
                <ns2:model>
                    <ns2:vehicleModelCode>{$vehicle_models_sk_id}</ns2:vehicleModelCode>
                    <ns2:mark>
                        <ns2:vehicleMarkCode>{$vehicle_marks_sk_id}</ns2:vehicleMarkCode>
                        <ns2:name>{$sk_car_mark->sk_title}</ns2:name>
                    </ns2:mark>
                    <ns2:manufacture>
                        <ns2:vehicleManufactureCode>RUS</ns2:vehicleManufactureCode>
                    </ns2:manufacture>
                    <ns2:type>
                        <ns2:vehicleTypeCode>B</ns2:vehicleTypeCode>
                    </ns2:type>
                    <ns2:name>{$sk_car_model->sk_title}</ns2:name>
                </ns2:model>
                <ns2:vin>{$object_insurer_auto->vin}</ns2:vin>
                <ns2:bodyNumber></ns2:bodyNumber>
                <ns2:chassisNumber></ns2:chassisNumber>
                <ns2:licensePlate>{$object_insurer_auto->reg_number}</ns2:licensePlate>
                <ns2:purpose>
                    <ns2:vehiclePurposeCode>PERSONAL</ns2:vehiclePurposeCode>
                </ns2:purpose>
                <ns2:country>
                    <ns2:countryCode>Россия</ns2:countryCode>
                </ns2:country>
                <ns2:owner>
                    <ns2:objectType>
                        <ns2:code>CONTRACTOR</ns2:code>
                    </ns2:objectType>
                    <ns2:name>{$owner->title}</ns2:name>
                    <ns2:docs>
                        <ns2:objectDocType>
                            <ns2:objectDocTypeCode>PASSPORT_RF</ns2:objectDocTypeCode>
                        </ns2:objectDocType>
                        <ns2:series>{$owner->doc_serie}</ns2:series>
                        <ns2:number>{$owner->doc_number}</ns2:number>
                        <ns2:dateIssue>{$owner_doc_date}</ns2:dateIssue>
                    </ns2:docs>
                    <ns2:contractorType>
                        <ns2:contractorTypeCode>INDIVIDUAL</ns2:contractorTypeCode>
                    </ns2:contractorType>
                    <ns2:isResident>true</ns2:isResident>
                    <ns2:contacts>
                        <ns2:contactType>
                            <ns2:contactTypeCode>EMAIL</ns2:contactTypeCode>
                        </ns2:contactType>
                        <ns2:contact>{$owner->email}</ns2:contact>
                        <ns2:contactPerson>{$owner->title}</ns2:contactPerson>
                    </ns2:contacts>
                    <ns2:addresses>
                        <ns2:country>
                            <ns2:countryCode>Россия</ns2:countryCode>
                        </ns2:country>
                        <ns2:addressType>
                            <ns2:addressTypeCode>REGISTRATION</ns2:addressTypeCode>
                        </ns2:addressType>
                        <ns2:region>{$owner_info->address_register_region}</ns2:region>
                        <ns2:district></ns2:district>
                        <ns2:city>{$owner_info->address_register_city}</ns2:city>
                        <ns2:locality>{$owner_info->address_register_settlement}</ns2:locality>
                        <ns2:street>{$owner_info->address_register_street}</ns2:street>
                        <ns2:house>{$owner_info->address_register_house}</ns2:house>
                        <ns2:building></ns2:building>
                        <ns2:flat>{$owner_info->address_register_flat}</ns2:flat>
                        <ns2:okato></ns2:okato>
                        <ns2:kladr>{$owner_city_kladr}</ns2:kladr>
                    </ns2:addresses>
                    <ns2:firstName>{$owner_fio_parts[1]}</ns2:firstName>
                    <ns2:surname>{$owner_fio_parts[0]}</ns2:surname>
                    <ns2:secondName>{$owner_fio_parts[2]}</ns2:secondName>
                    <ns2:birthDate>{$ownerBirthdate}</ns2:birthDate>
                    <ns2:gender>{$owner_sex}</ns2:gender>
                    <ns2:fullName>{$owner_info->fio}</ns2:fullName>
                    <ns2:latName></ns2:latName>
                </ns2:owner>
                <ns2:ownershipType>
                    <ns2:ownershipTypeCode>INDIVIDUAL-OWNER</ns2:ownershipTypeCode>
                </ns2:ownershipType>
                <ns2:trailer>{$is_trailer}</ns2:trailer>
                <ns2:transit>false</ns2:transit>
                <ns2:lease>false</ns2:lease>
                <ns2:cost>0.00</ns2:cost>
                <ns2:power>{$car_data->power}</ns2:power>
                <ns2:maxWeight>0</ns2:maxWeight>
                <ns2:unladenWeight>0</ns2:unladenWeight>
                <ns2:passengers>0</ns2:passengers>
                <ns2:usedSince>{$usedSince}</ns2:usedSince>
                <ns2:mileage>0</ns2:mileage>
                <ns2:engineNumber></ns2:engineNumber>
                <ns2:keysCount>0</ns2:keysCount>
                <ns2:yearIssue>{$car_data->car_year}</ns2:yearIssue>
            </ns2:object>
            <ns2:objectType>
                <ns2:code>VEHICLE</ns2:code>
            </ns2:objectType>
            <ns2:orderNo>1</ns2:orderNo>
            <ns2:period1BeginDate>{$period_start_1}</ns2:period1BeginDate>
            <ns2:period1EndDate>{$period_end_1}</ns2:period1EndDate>
        </ns2:policyObjects>
        {$drivers_xml}
        <ns2:participant>
            <ns2:contractor xsi:type="ns2:IndividualsXT" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <ns2:objectType>
                    <ns2:code>CONTRACTOR</ns2:code>
                </ns2:objectType>
                <ns2:docs>
                    <ns2:objectDocType>
                        <ns2:objectDocTypeCode>PASSPORT_RF</ns2:objectDocTypeCode>
                    </ns2:objectDocType>
                    <ns2:series>{$insurer->doc_serie}</ns2:series>
                    <ns2:number>{$insurer->doc_number}</ns2:number>
                    <ns2:dateIssue>{$insurerPassportRFDateIssue}</ns2:dateIssue>
                    <ns2:whom>{$insurer->doc_info}</ns2:whom>
                    <ns2:subdivisionCode>{$insurer->doc_office}</ns2:subdivisionCode>
                </ns2:docs>
                <ns2:contractorType>
                    <ns2:contractorTypeCode>INDIVIDUAL</ns2:contractorTypeCode>
                </ns2:contractorType>
                <ns2:contacts>
                    <ns2:contactType>
                        <ns2:contactTypeCode>EMAIL</ns2:contactTypeCode>
                    </ns2:contactType>
                    <ns2:contact>{$insurer_->email}</ns2:contact>
                    <ns2:contactPerson>{$insurer->fio}</ns2:contactPerson>
                </ns2:contacts>
                <ns2:contacts>
                    <ns2:contactType>
                        <ns2:contactTypeCode>PHONE</ns2:contactTypeCode>
                    </ns2:contactType>
                    <ns2:contact>{$insurer_->phone}</ns2:contact>
                    <ns2:contactPerson>{$insurer->fio}</ns2:contactPerson>
                </ns2:contacts>
                <ns2:addresses>
                    <ns2:country>
                        <ns2:countryCode>Россия</ns2:countryCode>
                    </ns2:country>
                    <ns2:addressType>
                        <ns2:addressTypeCode>REGISTRATION</ns2:addressTypeCode>
                    </ns2:addressType>
                    <ns2:region>{$insurer->address_register_region}</ns2:region>
                    <ns2:district></ns2:district>
                    <ns2:city>{$insurer->address_register_city}</ns2:city>
                    <ns2:locality>{$insurer->address_register_settlement}</ns2:locality>
                    <ns2:street>{$insurer->address_register_street}</ns2:street>
                    <ns2:house>{$insurer->address_register_house}</ns2:house>
                    <ns2:building></ns2:building>
                    <ns2:flat>{$insurer->address_register_flat}</ns2:flat>
                    <ns2:kladr>{$insurer_city_kladr}</ns2:kladr>
                </ns2:addresses>
                <ns2:firstName>{$insurer_fio_info[1]}</ns2:firstName>
                <ns2:surname>{$insurer_fio_info[0]}</ns2:surname>
                <ns2:secondName>{$insurer_fio_info[2]}</ns2:secondName>
                <ns2:birthDate>{$insurerBirthdate}</ns2:birthDate>
                <ns2:gender>{$owner_sex}</ns2:gender>
                <ns2:fullName>{$insurer->fio}</ns2:fullName>
            </ns2:contractor>
            <ns2:participantType>
                <ns2:participantTypeCode>INSURANT</ns2:participantTypeCode>
            </ns2:participantType>
        </ns2:participant>
    </ns3:policy>
</ns3:calculatePolicyRequest>
XML;

        return $xml;

    }

    public function savePolicyXML(ContractsCalculation $calc, $vsk_api_online)
    {

        $contract = $calc->contract;

        $begin_date = date('Y-m-d\T00:00:00', strtotime($contract->begin_date)).'+03:00';
        $end_date = date('Y-m-d\T23:59:59', strtotime($contract->end_date)).'+03:00';
        $now = Carbon::now()->format('Y-m-d\TH:i:s').'+03:00';

        $car_data = $this->get_car_data_xml($calc);
        $driver_data = $this->get_drivers_data_xml($calc);
        $insurer_data = $this->get_insurer_data_xml($calc);

        $insurer = $contract->insurer->get_info();
        $classifierValue = mb_strimwidth($insurer->address_register_city_kladr_id, 0, 2);

        $xml = <<<XML
<policy:savePolicyRequest xmlns:common="http://www.vsk.ru/schema/partners/common" xmlns="http://www.vsk.ru/schema/partners/model" xmlns:policy="http://www.vsk.ru/schema/partners/policy" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <common:messageId>{$vsk_api_online->id_message}</common:messageId>
    <common:bpId>{$vsk_api_online->id_bp}</common:bpId>
    <common:sessionId>{$vsk_api_online->id_session}</common:sessionId>
    <policy:policy>
        <product>
            <productCode>OSAGO</productCode>
        </product>
        <dateCreate>{$now}</dateCreate>
        <dateStart>{$begin_date}</dateStart><!-- Сегодняшняя дата + 3 дня -->
        <dateEnd>{$end_date}</dateEnd><!-- Сегодняшняя дата + 2 дня -1 год -->
        <policyObjects>
            {$car_data}
        </policyObjects>
            {$driver_data}
        <participant>
            {$insurer_data}
        </participant>
        <classifiers>
            <classifierType>
                <classifierTypeCode>DEAL_REGION</classifierTypeCode>
            </classifierType>
            <classifierValue>{$classifierValue}</classifierValue>
        </classifiers>
    </policy:policy>
</policy:savePolicyRequest>
XML;


        return $xml;
    }

    public function signXML(ContractsCalculation $calc, $vsk_api_online, $request)
    {
        $phone = $calc->contract->insurer->phone;
        $phone = str_replace('(', '', $phone);
        $phone = str_replace(')', '', $phone);
        $phone = str_replace('-', '', $phone);
        $phone = str_replace('+', '', $phone);
        $phone = str_replace(' ', '', $phone);

        $xml = <<<XML
<policy:signPolicyRequest xmlns:common="http://www.vsk.ru/schema/partners/common" xmlns:policy="http://www.vsk.ru/schema/partners/policy">
    <common:messageId>{$vsk_api_online->id_message}</common:messageId>
    <common:bpId>{$vsk_api_online->id_bp}</common:bpId>
    <common:sessionId>{$vsk_api_online->id_session}</common:sessionId>
    <policy:partnerClientId>{$phone}</policy:partnerClientId>
    <policy:code>{$request->sms_code}</policy:code>
</policy:signPolicyRequest>
XML;
        return $xml;
    }

    public function buyPolicyXML(ContractsCalculation $calc, $vsk_api_online)
    {
        $amount = str_replace('.', '', (string)$vsk_api_online->temp_amount);
        $xml = <<<XML
<policy:buyPolicyRequest xmlns:common="http://www.vsk.ru/schema/partners/common" xmlns:policy="http://www.vsk.ru/schema/partners/policy">
    <common:messageId>{$vsk_api_online->id_message}</common:messageId>
    <common:bpId>{$vsk_api_online->id_bp}</common:bpId>
    <common:sessionId>{$vsk_api_online->id_session}</common:sessionId>
    <policy:policyNumber>{$vsk_api_online->policy_number}</policy:policyNumber>
    <policy:returnUrl>http://192.168.2.171</policy:returnUrl>
    <policy:failUrl>http://192.168.2.171</policy:failUrl>
    <policy:amount>{$amount}</policy:amount>
</policy:buyPolicyRequest>
XML;
        return $xml;
    }

    public function getCalcParamsTest(ContractsCalculation $calc, $vsk_api_online)
    {
        $xml = <<<XML
<ns3:calculatePolicyRequest xmlns="http://www.vsk.ru/schema/partners/common" xmlns:ns6="http://www.vsk.ru/schema/partners/user" xmlns:ns5="http://www.vsk.ru/schema/partners/auth" xmlns:ns2="http://www.vsk.ru/schema/partners/model/calc" xmlns:ns4="http://www.vsk.ru/schema/partners/model" xmlns:ns3="http://www.vsk.ru/schema/partners/policy">
    <messageId>12334554</messageId>
    <ns3:policy>
        <ns2:product>
            <ns2:productCode>OSAGO</ns2:productCode>
        </ns2:product>
        <ns2:dateCreate>2020-04-10T11:15:26.237318+03:00</ns2:dateCreate>
        <ns2:dateStart>2020-04-18T00:00:00+03:00</ns2:dateStart>
        <ns2:dateEnd>2021-04-17T23:59:59+03:00</ns2:dateEnd>
        <ns2:dateIssue>2020-04-10+03:00</ns2:dateIssue>
        <ns2:previousPolicyNumber>XXX 0087796749</ns2:previousPolicyNumber>
        <ns2:policyObjects>
            <ns2:object xsi:type="ns2:VehicleXT" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <ns2:objectType>
                    <ns2:code>VEHICLE</ns2:code>
                </ns2:objectType>
                <ns2:docs>
                    <ns2:objectDocType>
                        <ns2:objectDocTypeCode>VEHICLE_REGISTRATION_CERTIFICATE</ns2:objectDocTypeCode>
                    </ns2:objectDocType>
                    <ns2:series>5960</ns2:series>
                    <ns2:number>530289</ns2:number>
                    <ns2:dateIssue>2018-04-16+03:00</ns2:dateIssue>
                </ns2:docs>
                <ns2:model>
                    <ns2:vehicleModelCode>507</ns2:vehicleModelCode>
                    <ns2:mark>
                        <ns2:vehicleMarkCode>63</ns2:vehicleMarkCode>
                        <ns2:name>DATSUN</ns2:name>
                    </ns2:mark>
                    <ns2:type>
                        <ns2:vehicleTypeCode>B</ns2:vehicleTypeCode>
                    </ns2:type>
                    <ns2:name>ON-DO</ns2:name>
                </ns2:model>
                <ns2:vin>Z8NBAABD0K0083666</ns2:vin>
                <ns2:bodyNumber></ns2:bodyNumber>
                <ns2:chassisNumber></ns2:chassisNumber>
                <ns2:licensePlate>М903ВА159</ns2:licensePlate>
                <ns2:purpose>
                    <ns2:vehiclePurposeCode>PERSONAL</ns2:vehiclePurposeCode>
                </ns2:purpose>
                <ns2:country>
                    <ns2:countryCode>Россия</ns2:countryCode>
                </ns2:country>
                <ns2:owner>
                    <ns2:objectType>
                        <ns2:code>CONTRACTOR</ns2:code>
                    </ns2:objectType>
                    <ns2:name>СОКОЛОВ АЛЕКСАНДР СЕРГЕЕВИЧ</ns2:name>
                    <ns2:docs>
                        <ns2:objectDocType>
                            <ns2:objectDocTypeCode>PASSPORT_RF</ns2:objectDocTypeCode>
                        </ns2:objectDocType>
                        <ns2:series>5709</ns2:series>
                        <ns2:number>594029</ns2:number>
                        <ns2:dateIssue>2010-06-26+04:00</ns2:dateIssue>
                        <ns2:whom>КРАЙ ПЕРМСКИЙ, Р-Н КУНГУРСКИЙ, Д ПОПОВКА, УЛ ГЕОЛОГОВ, ДОМ 2</ns2:whom>
                    </ns2:docs>
                    <ns2:contractorType>
                        <ns2:contractorTypeCode>INDIVIDUAL</ns2:contractorTypeCode>
                    </ns2:contractorType>
                    <ns2:isResident>true</ns2:isResident>
                    <ns2:contacts>
                        <ns2:contactType>
                            <ns2:contactTypeCode>PHONE</ns2:contactTypeCode>
                        </ns2:contactType>
                        <ns2:contact>79519339777</ns2:contact>
                        <ns2:contactPerson>СОКОЛОВ АЛЕКСАНДР СЕРГЕЕВИЧ</ns2:contactPerson>
                    </ns2:contacts>
                    <ns2:contacts>
                        <ns2:contactType>
                            <ns2:contactTypeCode>EMAIL</ns2:contactTypeCode>
                        </ns2:contactType>
                        <ns2:contact>osago4cb646f9c72a@elmail.ru</ns2:contact>
                        <ns2:contactPerson>СОКОЛОВ АЛЕКСАНДР СЕРГЕЕВИЧ</ns2:contactPerson>
                    </ns2:contacts>
                    <ns2:addresses>
                        <ns2:country>
                            <ns2:countryCode>Россия</ns2:countryCode>
                        </ns2:country>
                        <ns2:addressType>
                            <ns2:addressTypeCode>REGISTRATION</ns2:addressTypeCode>
                        </ns2:addressType>
                        <ns2:region>Пермский</ns2:region>
                        <ns2:district>Кунгурский р-н</ns2:district>
                        <ns2:city></ns2:city>
                        <ns2:locality>Поповка</ns2:locality>
                        <ns2:street>Геологов</ns2:street>
                        <ns2:house>2</ns2:house>
                        <ns2:flat></ns2:flat>
                        <ns2:kladr>59012000150</ns2:kladr>
                    </ns2:addresses>
                    <ns2:firstName>АЛЕКСАНДР</ns2:firstName>
                    <ns2:surname>СОКОЛОВ</ns2:surname>
                    <ns2:secondName>СЕРГЕЕВИЧ</ns2:secondName>
                    <ns2:birthDate>1963-11-17+03:00</ns2:birthDate>
                    <ns2:gender>1</ns2:gender>
                    <ns2:fullName>СОКОЛОВ АЛЕКСАНДР СЕРГЕЕВИЧ</ns2:fullName>
                </ns2:owner>
                <ns2:ownershipType>
                    <ns2:ownershipTypeCode>INDIVIDUAL-OWNER</ns2:ownershipTypeCode>
                </ns2:ownershipType>
                <ns2:trailer>false</ns2:trailer>
                <ns2:transit>false</ns2:transit>
                <ns2:lease>false</ns2:lease>
                <ns2:cost>0</ns2:cost>
                <ns2:power>87.0</ns2:power>
                <ns2:maxWeight>0</ns2:maxWeight>
                <ns2:unladenWeight>0</ns2:unladenWeight>
                <ns2:passengers>0</ns2:passengers>
                <ns2:usedSince>2018-01-01+03:00</ns2:usedSince>
                <ns2:mileage>0</ns2:mileage>
                <ns2:engineNumber>0</ns2:engineNumber>
                <ns2:keysCount>0</ns2:keysCount>
                <ns2:yearIssue>2018</ns2:yearIssue>
            </ns2:object>
            <ns2:orderNo>1</ns2:orderNo>
            <ns2:period1BeginDate>2020-04-18+03:00</ns2:period1BeginDate>
            <ns2:period1EndDate>2021-04-17+03:00</ns2:period1EndDate>
        </ns2:policyObjects>
        <ns2:policyObjects>
            <ns2:object xsi:type="ns2:IndividualsXT" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <ns2:objectType>
                    <ns2:code>CONTRACTOR</ns2:code>
                </ns2:objectType>
                <ns2:name>СОКОЛОВ АЛЕКСАНДР СЕРГЕЕВИЧ</ns2:name>
                <ns2:docs>
                    <ns2:objectDocType>
                        <ns2:objectDocTypeCode>DRIVER_LICENSE</ns2:objectDocTypeCode>
                    </ns2:objectDocType>
                    <ns2:series>59ОМ</ns2:series>
                    <ns2:number>726212</ns2:number>
                    <ns2:dateIssue>2010-07-17+04:00</ns2:dateIssue>
                </ns2:docs>
                <ns2:contractorType>
                    <ns2:contractorTypeCode>INDIVIDUAL</ns2:contractorTypeCode>
                </ns2:contractorType>
                <ns2:firstName>АЛЕКСАНДР</ns2:firstName>
                <ns2:surname>СОКОЛОВ</ns2:surname>
                <ns2:secondName>СЕРГЕЕВИЧ</ns2:secondName>
                <ns2:birthDate>1963-11-17+03:00</ns2:birthDate>
                <ns2:gender>1</ns2:gender>
                <ns2:fullName>СОКОЛОВ АЛЕКСАНДР СЕРГЕЕВИЧ</ns2:fullName>
                <ns2:driveExperience>1982-07-17+04:00</ns2:driveExperience>
            </ns2:object>
            <ns2:orderNo>2</ns2:orderNo>
        </ns2:policyObjects>
        <ns2:participant>
            <ns2:contractor xsi:type="ns2:IndividualsXT" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <ns2:objectType>
                    <ns2:code>CONTRACTOR</ns2:code>
                </ns2:objectType>
                <ns2:name>СОКОЛОВ АЛЕКСАНДР СЕРГЕЕВИЧ</ns2:name>
                <ns2:docs>
                    <ns2:objectDocType>
                        <ns2:objectDocTypeCode>PASSPORT_RF</ns2:objectDocTypeCode>
                    </ns2:objectDocType>
                    <ns2:series>5709</ns2:series>
                    <ns2:number>594029</ns2:number>
                    <ns2:dateIssue>2010-06-26+04:00</ns2:dateIssue>
                    <ns2:whom>КРАЙ ПЕРМСКИЙ, Р-Н КУНГУРСКИЙ, Д ПОПОВКА, УЛ ГЕОЛОГОВ, ДОМ 2</ns2:whom>
                </ns2:docs>
                <ns2:contractorType>
                    <ns2:contractorTypeCode>INDIVIDUAL</ns2:contractorTypeCode>
                </ns2:contractorType>
                <ns2:isResident>true</ns2:isResident>
                <ns2:contacts>
                    <ns2:contactType>
                        <ns2:contactTypeCode>PHONE</ns2:contactTypeCode>
                    </ns2:contactType>
                    <ns2:contact>79519339777</ns2:contact>
                    <ns2:contactPerson>СОКОЛОВ АЛЕКСАНДР СЕРГЕЕВИЧ</ns2:contactPerson>
                </ns2:contacts>
                <ns2:contacts>
                    <ns2:contactType>
                        <ns2:contactTypeCode>EMAIL</ns2:contactTypeCode>
                    </ns2:contactType>
                    <ns2:contact>osago4cb646f9c72a@elmail.ru</ns2:contact>
                    <ns2:contactPerson>СОКОЛОВ АЛЕКСАНДР СЕРГЕЕВИЧ</ns2:contactPerson>
                </ns2:contacts>
                <ns2:addresses>
                    <ns2:country>
                        <ns2:countryCode>Россия</ns2:countryCode>
                    </ns2:country>
                    <ns2:addressType>
                        <ns2:addressTypeCode>REGISTRATION</ns2:addressTypeCode>
                    </ns2:addressType>
                    <ns2:region>Пермский</ns2:region>
                    <ns2:district>Кунгурский р-н</ns2:district>
                    <ns2:city></ns2:city>
                    <ns2:locality>Поповка</ns2:locality>
                    <ns2:street>Геологов</ns2:street>
                    <ns2:house>2</ns2:house>
                    <ns2:flat></ns2:flat>
                    <ns2:kladr>59012000150</ns2:kladr>
                </ns2:addresses>
                <ns2:firstName>АЛЕКСАНДР</ns2:firstName>
                <ns2:surname>СОКОЛОВ</ns2:surname>
                <ns2:secondName>СЕРГЕЕВИЧ</ns2:secondName>
                <ns2:birthDate>1963-11-17+03:00</ns2:birthDate>
                <ns2:gender>1</ns2:gender>
                <ns2:fullName>СОКОЛОВ АЛЕКСАНДР СЕРГЕЕВИЧ</ns2:fullName>
            </ns2:contractor>
            <ns2:participantType>
                <ns2:participantTypeCode>INSURANT</ns2:participantTypeCode>
            </ns2:participantType>
        </ns2:participant>
    </ns3:policy>
</ns3:calculatePolicyRequest>
XML;

        return $xml;
    }

    public function savePolicyXMLTest($calc , $vsk_api_online)
    {

        $xml = <<<XML
<policy:savePolicyRequest
        xmlns:common="http://www.vsk.ru/schema/partners/common"
        xmlns:model="http://www.vsk.ru/schema/partners/model"
        xmlns:policy="http://www.vsk.ru/schema/partners/policy"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <common:messageId>{$vsk_api_online->id_message}</common:messageId>
    <common:bpId>{$vsk_api_online->id_bp}</common:bpId>
    <common:sessionId>{$vsk_api_online->id_session}</common:sessionId>
    <policy:policy>
        <model:product>
            <model:productCode>OSAGO</model:productCode>
        </model:product>
        <model:dateCreate>2020-03-13T10:49:28+03:00</model:dateCreate> 
        <model:dateStart>2020-03-17T00:00:00+03:00</model:dateStart> 
        <model:dateEnd>2021-03-16T23:59:59+03:00</model:dateEnd>     
        <model:policyObjects>
            <model:object xsi:type="model:VehicleXT">
               <model:objectType>
                    <model:objectTypeId>4</model:objectTypeId>
                    <model:code>VEHICLE</model:code>
                </model:objectType>
                <model:docs>
                    <model:objectDocType>
                        <model:objectDocTypeCode>VEHICLE_PASSPORT</model:objectDocTypeCode>
                    </model:objectDocType>
                    <model:series>77УМ</model:series>
                    <model:number>213279</model:number>
                    <model:dateIssue>2010-01-11</model:dateIssue>
                    <model:whom/>
                    <model:subdivisionCode/>
                    <model:isMain>false</model:isMain>
                </model:docs>
                <model:model>
                    <model:vehicleModelCode>1121</model:vehicleModelCode>
                    <model:mark>
                        <model:vehicleMarkCode>252</model:vehicleMarkCode>
                    </model:mark>
                    <model:type>
                        <model:vehicleTypeCode>B</model:vehicleTypeCode>
                    </model:type>
                </model:model>
                <model:vin>KNMCSHLAS9P756535</model:vin>
                <model:bodyNumber/>
                <model:chassisNumber/>
                <model:licensePlate>С736АХ190</model:licensePlate>
                <model:purpose>
                    <model:vehiclePurposeCode>PERSONAL</model:vehiclePurposeCode>
                </model:purpose>
                <model:country>
                    <model:countryCode>Россия</model:countryCode>
                </model:country>
                <model:owner>
                    <model:objectType>
                        <model:code>CONTRACTOR</model:code>
                    </model:objectType>
                    <model:name>Попов Александр Сергеевич</model:name>
                    <model:descr/>
                    <model:docs>
                        <model:objectDocType>
                            <model:objectDocTypeCode>PASSPORT_RF</model:objectDocTypeCode>
                        </model:objectDocType>
                        <model:series>2001</model:series>
                        <model:number>884043</model:number>
                        <model:dateIssue>2002-10-15+04:00</model:dateIssue>
                        <model:whom/>
                        <model:subdivisionCode/>
                        <model:isMain>false</model:isMain>
                    </model:docs>
                    <model:contractorType>
                        <model:contractorTypeCode>INDIVIDUAL</model:contractorTypeCode>
                    </model:contractorType>
                    <model:isResident>true</model:isResident>
                    <model:contacts>
                        <model:contactType>
                            <model:contactTypeCode>EMAIL</model:contactTypeCode>
                        </model:contactType>
                        <model:contact>denis_danilov_97@mail.ru</model:contact>
                        <model:contactPerson>Попов Александр Сергеевич</model:contactPerson>
                    </model:contacts>
                    <model:addresses>
                        <model:country>
                            <model:countryCode>Россия</model:countryCode>
                        </model:country>
                        <model:addressType>
                            <model:addressTypeCode>REGISTRATION</model:addressTypeCode>
                        </model:addressType>
                        <model:region/>
                        <model:district/>
                        <model:city>Москва</model:city>
                        <model:locality/>
                        <model:street>Островная Улица</model:street>
                        <model:house>16</model:house>
                        <model:building>1</model:building>
                        <model:flat>119</model:flat>
                        <model:okato/>
                        <model:addressStr/>
                        <model:kladr>77000000000</model:kladr>
                    </model:addresses>
                    <model:firstName>Александр</model:firstName>
                    <model:surname>Попов</model:surname>
                    <model:secondName>Сергеевич</model:secondName>
                    <model:birthDate>1977-04-19+03:00</model:birthDate>
                    <model:gender>1</model:gender>
                    <model:fullName>Попов Александр Сергеевич</model:fullName>
                    <model:latName/>
                </model:owner>
                <model:ownershipType>
                    <model:ownershipTypeCode>INDIVIDUAL-OWNER</model:ownershipTypeCode>
                </model:ownershipType>
                <model:trailer>false</model:trailer>
                <model:transit>false</model:transit>
                <model:lease>false</model:lease>
                <model:cost>0.00</model:cost>
                <model:power>107</model:power>
                <model:maxWeight>0</model:maxWeight>
                <model:unladenWeight>0</model:unladenWeight>
                <model:passengers>0</model:passengers>
                <model:usedSince>2009-01-01</model:usedSince>
                <model:mileage>0</model:mileage>
                <model:engineNumber/>
                <model:keysCount>0</model:keysCount>
                <model:yearIssue>2009</model:yearIssue>
                <model:bodyType>
                    <model:bodyTypeCode>SEDAN</model:bodyTypeCode>
                </model:bodyType>
                <model:transmissionType>
                    <model:transmissionCode>M</model:transmissionCode>
                </model:transmissionType>
                <model:engineType>
                    <model:engineTypeCode>B</model:engineTypeCode>
                </model:engineType>
            </model:object>
            <model:objectType>
                <model:code>VEHICLE</model:code>
            </model:objectType>
            <model:product>
                <model:productCode>OSAGO</model:productCode>
            </model:product>
            <model:orderNo>1</model:orderNo>
            <model:period1BeginDate>2020-03-17</model:period1BeginDate>
            <model:period1EndDate>2021-03-16</model:period1EndDate>
        </model:policyObjects>
        <model:policyObjects>
            <model:object xsi:type="model:IndividualsXT">
                <model:objectType>
                    <model:code>CONTRACTOR</model:code>
                </model:objectType>
                <model:name>Филиппов Максим Львович</model:name>
                <model:docs>
                    <model:objectDocType>
                        <model:objectDocTypeCode>DRIVER_LICENSE</model:objectDocTypeCode>
                    </model:objectDocType>
                    <model:series>50ОУ</model:series>
                    <model:number>202172</model:number>
                    <model:dateIssue>2010-01-11</model:dateIssue>
                </model:docs>
                <model:contractorType>
                    <model:contractorTypeCode>INDIVIDUAL</model:contractorTypeCode>
                </model:contractorType>
                <model:firstName>Александр</model:firstName>
                <model:surname>Попов</model:surname>
                <model:secondName>Сергеевич</model:secondName>
                <model:birthDate>1977-04-19+03:00</model:birthDate>
                <model:gender>1</model:gender>
                <model:fullName>Попов Александр Сергеевич</model:fullName>
                <model:driveExperience>1995-12-31+00:00</model:driveExperience>
                <model:snils/>
            </model:object>
            <model:orderNo>2</model:orderNo>
        </model:policyObjects>
        <model:participant>
            <model:contractor xsi:type="model:IndividualsXT">
                <model:objectType>
                    <model:code>CONTRACTOR</model:code>
                </model:objectType>
                <model:docs>
                    <model:objectDocType>
                        <model:objectDocTypeCode>PASSPORT_RF</model:objectDocTypeCode>
                    </model:objectDocType>
                    <model:series>2001</model:series>
                    <model:number>884043</model:number>
                    <model:dateIssue>2002-10-15+04:00</model:dateIssue>
                    <model:whom/>
                    <model:subdivisionCode/>
                    <model:isMain>false</model:isMain>
                </model:docs>
                <model:contractorType>
                    <model:contractorTypeCode>INDIVIDUAL</model:contractorTypeCode>
                </model:contractorType>
                <model:contacts>
                    <model:contactType>
                        <model:contactTypeCode>EMAIL</model:contactTypeCode>
                    </model:contactType>
                    <model:contact>denis_danilov_97@mail.ru</model:contact>
                    <model:contactPerson/>
                </model:contacts>
                <model:contacts>
                    <model:contactType>
                        <model:contactTypeCode>PHONE</model:contactTypeCode>
                    </model:contactType>
                    <model:contact>79200287493</model:contact>
                    <model:contactPerson/>
                </model:contacts>
                <model:addresses>
                    <model:country>
                        <model:countryCode>Россия</model:countryCode>
                    </model:country>
                    <model:addressType>
                        <model:addressTypeCode>REGISTRATION</model:addressTypeCode>
                    </model:addressType>
                    <model:region/>
                    <model:district/>
                    <model:city>Москва</model:city>
                    <model:locality/>
                    <model:street>Островная Улица</model:street>
                    <model:house>16</model:house>
                    <model:building>1</model:building>
                    <model:flat>119</model:flat>
                    <model:okato/>
                    <model:addressStr/>
                    <model:kladr>77000000000</model:kladr>
                </model:addresses>
                <model:firstName>Александр</model:firstName>
                <model:surname>Попов</model:surname>
                <model:secondName>Сергеевич</model:secondName>
                <model:birthDate>1977-04-19+03:00</model:birthDate>
                <model:gender>1</model:gender>
                <model:fullName>Попов Александр Сергеевич</model:fullName>
                <model:snils/>
            </model:contractor>
            <model:participantType>
                <model:participantTypeCode>INSURANT</model:participantTypeCode>
            </model:participantType>
        </model:participant>
        <model:classifiers>
            <model:classifierType>
                <model:classifierTypeCode>DEAL_REGION</model:classifierTypeCode>
            </model:classifierType>
            <model:classifierValue>77</model:classifierValue>
        </model:classifiers>
    </policy:policy>
</policy:savePolicyRequest>
XML;

    return $xml;

    }

    private function get_drivers_data_calc_xml(ContractsCalculation $calc)
    {

        $contract = $calc->contract;


        $xml_result = '';

        if($contract->drivers_type_id == 0){
            foreach ($contract->drivers as $driver){

                $driver_fio = explode(' ', $driver->fio);
                $birthdate = $driver->birth_date.'+03:00';
                $date_issue = $driver->doc_date.'+03:00';
                $driver_exp = $driver->exp_date.'+03:00';

                if ($driver->sex == 0){
                    $driver_sex = 1;
                }else if($driver->sex == 1){
                    $driver_sex = 2;
                }

                $xml = <<<XML
                <ns2:policyObjects>
                    <ns2:object xsi:type="ns2:IndividualsXT" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                        <ns2:objectType>
                            <ns2:code>DRIVER</ns2:code>
                        </ns2:objectType>
                        <ns2:docs>
                            <ns2:objectDocType>
                                <ns2:objectDocTypeCode>DRIVER_LICENSE</ns2:objectDocTypeCode>
                            </ns2:objectDocType>
                            <ns2:series>$driver->doc_serie</ns2:series>
                            <ns2:number>$driver->doc_num</ns2:number>
                            <ns2:dateIssue>$date_issue</ns2:dateIssue><!--2019-02-16+03:00-->
                        </ns2:docs>
                        <ns2:contractorType>
                            <ns2:contractorTypeCode>INDIVIDUAL</ns2:contractorTypeCode>
                        </ns2:contractorType>
                        <ns2:firstName>$driver_fio[1]</ns2:firstName>
                        <ns2:surname>$driver_fio[0]</ns2:surname>
                        <ns2:secondName>$driver_fio[2]</ns2:secondName>
                        <ns2:birthDate>$birthdate</ns2:birthDate><!--1997-08-07+03:00-->
                        <ns2:gender>$driver_sex</ns2:gender>
                        <ns2:fullName>$driver->fio</ns2:fullName>
                        <ns2:driveExperience>$driver_exp</ns2:driveExperience><!--2020-02-16+03:00-->
                    </ns2:object>
                </ns2:policyObjects>
XML;
                $xml_result .= $xml;

            }
        }



        return $xml_result;
    }

    private function get_drivers_data_xml(ContractsCalculation $calc)
    {

        $contract = $calc->contract;


        $xml_result = '';

        if($contract->drivers_type_id == 0){

            foreach ($contract->drivers as $driver){

                $driver_fio = explode(' ', $driver->fio);
                $birthdate = $driver->birth_date.'+03:00';
                $date_issue = $driver->doc_date.'+03:00';
                $driver_exp = $driver->exp_date.'+03:00';

                if ($driver->sex == 0){
                    $driver_sex = 1;
                }else if($driver->sex == 1){
                    $driver_sex = 2;
                }

                $xml = <<<XML
                <policyObjects>
                    <object xsi:type="IndividualsXT" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                        <objectType>
                            <code>DRIVER</code>
                        </objectType>
                        <docs>
                            <objectDocType>
                                <objectDocTypeCode>DRIVER_LICENSE</objectDocTypeCode>
                            </objectDocType>
                            <series>$driver->doc_serie</series>
                            <number>$driver->doc_num</number>
                            <dateIssue>$date_issue</dateIssue><!--2019-02-16+03:00-->
                        </docs>
                        <contractorType>
                            <contractorTypeCode>INDIVIDUAL</contractorTypeCode>
                        </contractorType>
                        <firstName>$driver_fio[1]</firstName>
                        <surname>$driver_fio[0]</surname>
                        <secondName>$driver_fio[2]</secondName>
                        <birthDate>$birthdate</birthDate><!--1997-08-07+03:00-->
                        <gender>$driver_sex</gender>
                        <fullName>$driver->fio</fullName>
                        <driveExperience>$driver_exp</driveExperience><!--2020-02-16+03:00-->
                    </object>
                </policyObjects>
XML;
                $xml_result .= $xml;

            }
        }



        return $xml_result;
    }

    private function get_insurer_data_xml(ContractsCalculation $calc)
    {

        $contract = $calc->contract;
        $car_data = $contract->object_insurer_auto;
        $insurer = $contract->insurer->get_info();

        $region = $insurer->address_register_region;
        $city = $insurer->address_register_city;
        $kladr = mb_strimwidth($insurer->address_register_kladr, 0, 11);
        $street = $insurer->address_register_street;
        $house = $insurer->address_register_house;
        $flat = $insurer->address_register_flat;
        $birthdate = $insurer->birthdate.'+03:00';


        $fio = $insurer->fio;
        $fio_info = explode(' ', $fio);

        if ($insurer->sex == 0){
            $sex = 1;
        }elseif($insurer->sex == 1){
            $sex = 2;
        }


        $insurer__ = $contract->insurer;

        $phone = $insurer__->phone;
        $phone = str_replace('(', '', $phone);
        $phone = str_replace(')', '', $phone);
        $phone = str_replace('-', '', $phone);
        $phone = str_replace('+', '', $phone);
        $phone = str_replace(' ', '', $phone);








        $xml = <<<XML
            <contractor xsi:type="IndividualsXT">
                <objectType>
                    <code>CONTRACTOR</code>
                </objectType>
                <docs>
                    <objectDocType>
                        <objectDocTypeCode>PASSPORT_RF</objectDocTypeCode>
                    </objectDocType>
                    <series>$insurer->doc_serie</series>
                    <number>$insurer->doc_number</number>
                    <dateIssue>$insurer->doc_date</dateIssue>
                    <whom>$insurer->doc_info</whom>
                    <subdivisionCode>$insurer->doc_office</subdivisionCode>
                </docs>
                <contractorType>
                    <contractorTypeCode>INDIVIDUAL</contractorTypeCode>
                </contractorType>
                <contacts>
                    <contactType>
                        <contactTypeCode>EMAIL</contactTypeCode>
                    </contactType>
                    <contact>$insurer__->email</contact>
                    <contactPerson />
                </contacts>
                <contacts>
                    <contactType>
                        <contactTypeCode>PHONE</contactTypeCode>
                    </contactType>
                    <contact>$phone</contact>
                    <contactPerson />
                </contacts>
                <addresses>
                    <country>
                        <countryCode>Россия</countryCode>
                    </country>
                    <addressType>
                        <addressTypeCode>REGISTRATION</addressTypeCode>
                    </addressType>
                    <region>$insurer->address_register_region</region>
                    <district />
                    <city>$insurer->address_register_city</city>
                    <locality />
                    <street>$insurer->address_register_street</street>
                    <house>$insurer->address_register_house</house>
                    <building />
                    <flat>$insurer->address_register_flat</flat>
                    <okato/>
                    <addressStr/>
                    <kladr>$kladr</kladr>
                </addresses>
                <firstName>$fio_info[1]</firstName>
                <surname>$fio_info[0]</surname>
                <secondName>$fio_info[2]</secondName>
                <birthDate>$birthdate</birthDate>
                <gender>$sex</gender><!-- 1: мужчина, 2: женщина -->
                <fullName>$fio</fullName>
                <snils />
            </contractor>
            <participantType>
                <participantTypeCode>INSURANT</participantTypeCode>
            </participantType>
XML;
        return $xml;
    }

    private function get_car_data_xml(ContractsCalculation $calc)
    {
        $contract = $calc->contract;
        $car_data = $calc->object_insurer_auto;



        $reg_number = mb_strtoupper($car_data->reg_number);

        $owner = $calc->contract->owner;

        $owner_info = $owner->get_info();
        $owner_doc_date = date('Y-m-d', strtotime($owner_info->doc_date)).'+03:00';

        $owner_phone = $owner->phone;
        $owner_phone = str_replace('(', '', $owner_phone);
        $owner_phone = str_replace(')', '', $owner_phone);
        $owner_phone = str_replace('-', '', $owner_phone);
        $owner_phone = str_replace('+', '', $owner_phone);
        $owner_phone = str_replace(' ', '', $owner_phone);

        /*   */


        if($car_data->doc_type == 0){
            $object_doc_type_code = "VEHICLE_PASSPORT";
        }else if ($car_data->doc_type == 1){
            $object_doc_type_code = "VEHICLE_REGISTRATION_CERTIFICATE";
        }


        if (!isset($calc->sk_model->sk_title)){
            $this->errors[] = 'Не синхронизирован справочник: Марки';
        }

        $sk_car_mark = $calc->sk_mark;
        $sk_car_model = $calc->sk_model;


        $period_start_1 = date('Y-m-d', strtotime($contract->begin_date));
        $period_end_1 = date('Y-m-d', strtotime($contract->end_date));

        $vehicle_models_sk_id = '';
        $vehicle_marks_sk_id = '';

        if (isset($sk_car_model->vehicle_models_sk_id) and isset($sk_car_model->vehicle_marks_sk_id)){
            $vehicle_models_sk_id = $sk_car_model->vehicle_models_sk_id;
            $vehicle_marks_sk_id = $sk_car_mark->vehicle_marks_sk_id;
        }


        //dd($owner_info);

        $region = $owner_info->address_register_region;
        $city = $owner_info->address_register_city;
        $kladr = mb_strimwidth($owner_info->address_register_kladr, 0, 11);

        $street = $owner_info->address_register_street;
        $house = $owner_info->address_register_house;
        $flat = $owner_info->address_register_flat;


        $fio = $owner_info->fio;
        $fio_info = explode(' ', $fio);

        if ($owner_info->sex == 0){
            $sex = 1;
        }elseif($owner_info->sex == 1){
            $sex = 2;
        }



        if ($car_data->is_trailer == 0){
            $is_trailer = 'false';
        }else{
            $is_trailer = 'true';
        }

        //dd((int)$car_data->power);

        $object_insurer_auto = $contract->object_insurer_auto;
        $diagnosticCardDateIssue = $object_insurer_auto->dk_date_from.'+03:00';
        $diagnosticCardValidThru = $object_insurer_auto->dk_date.'+03:00';


        $power = $car_data->power;


        $xml = <<<XML
                <object xsi:type="VehicleXT">
                <objectType>
                    <objectTypeId>4</objectTypeId>
                    <code>VEHICLE</code>
                </objectType>
                <docs>
                    <objectDocType>
                        <objectDocTypeCode>$object_doc_type_code</objectDocTypeCode>
                    </objectDocType>
                    <series>$car_data->docserie</series>
                    <number>$car_data->docnumber</number>
                    <dateIssue>$car_data->docdate</dateIssue><!--2016-04-29+03:00-->
                </docs>
                <model>
                    <vehicleModelCode>$vehicle_models_sk_id</vehicleModelCode>
                    <mark>
                        <vehicleMarkCode>$vehicle_marks_sk_id</vehicleMarkCode>
                    </mark>
                    <type>
                        <vehicleTypeCode>B</vehicleTypeCode>
                    </type>
                    <!--<name>TOYOTA RAV 4, B</name>-->
                </model>
                <vin>$car_data->vin</vin>
                <bodyNumber />
                <chassisNumber />
                <licensePlate>$reg_number</licensePlate>
                <purpose>
                    <vehiclePurposeCode>PERSONAL</vehiclePurposeCode>
                </purpose>
                <country>
                    <countryCode>Россия</countryCode>
                </country>
                <owner>
                    <objectType>
                        <code>CONTRACTOR</code>
                    </objectType>
                    <name>$owner->title</name>
                    <docs>
                        <objectDocType>
                            <objectDocTypeCode>PASSPORT_RF</objectDocTypeCode>
                        </objectDocType>
                        <series>$owner->doc_serie</series>
                        <number>$owner->doc_number</number>
                        <dateIssue>$owner_doc_date</dateIssue><!--2002-10-15-->
                    </docs>
                    <contractorType>
                        <contractorTypeCode>INDIVIDUAL</contractorTypeCode>
                    </contractorType>
                    <isResident>true</isResident>
                    <contacts>
                        <contactType>
                            <contactTypeCode>PHONE</contactTypeCode>
                        </contactType>
                        <contact>$owner_phone</contact>
                        <contactPerson>$owner->title</contactPerson>
                    </contacts>
                    <addresses>
                        <country>
                            <countryCode>Россия</countryCode>
                        </country>
                        <addressType>
                            <addressTypeCode>REGISTRATION</addressTypeCode>
                        </addressType>
                        <region>$region</region>
                        <district />
                        <city>$city</city>
                        <locality />
                        <street>$street</street>
                        <house>$house</house>
                        <building />
                        <flat>$flat</flat>
                        <okato />
                        <addressStr />
                        <kladr>$kladr</kladr>
                    </addresses>
                    <firstName>$fio_info[1]</firstName>
                    <surname>$fio_info[0]</surname>
                    <secondName>$fio_info[2]</secondName>
                    <birthDate>$owner_info->birthdate</birthDate>
                    <gender>$sex</gender>
                    <fullName>$owner->title</fullName>
                </owner>
                <ownershipType>
                    <ownershipTypeCode>INDIVIDUAL-OWNER</ownershipTypeCode>
                </ownershipType>
                <trailer>$is_trailer</trailer>
                <transit>false</transit>
                <lease>false</lease>
                <cost>0.00</cost>
                <power>$power</power>
                <maxWeight>0</maxWeight>
                <unladenWeight>0</unladenWeight>
                <passengers>0</passengers>
                <usedSince>$car_data->docdate</usedSince>
                <mileage>0</mileage>
                <engineNumber />
                <keysCount>0</keysCount>
                <yearIssue>$car_data->car_year</yearIssue>
                <bodyType>
                    <bodyTypeCode>ANY</bodyTypeCode>
                </bodyType>
            </object>
            <objectType>
                <code>VEHICLE</code>
            </objectType>
            <product>
                <productCode>OSAGO</productCode>
            </product>
            <orderNo>1</orderNo>
            <period1BeginDate>$period_start_1</period1BeginDate><!-- Период начала совпадает с датой начала страхования-->
            <period1EndDate>$period_end_1</period1EndDate><!-- Период окончания совпадает с датой окончания страхования-->
XML;
        return $xml;
    }
}