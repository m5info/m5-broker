<?php

namespace App\Services\SK\Vsk;

use App\Services\SK\BaseServiceController;
use App\Services\SK\Vsk\Services\Dictionary\DictionaryApi;
use App\Services\SK\Vsk\Services\Dictionary\DictionaryService;
use App\Services\SK\Vsk\Services\Osago\EOsagoApi;
use App\Services\SK\Vsk\Services\Osago\OsagoApiFactory;
use App\Services\SK\Vsk\Services\Osago\OsagoService;
use App\Services\SK\Vsk\Services\Kasko\KaskoApi;
use App\Services\SK\Vsk\Services\Kasko\KaskoService;
use App\Services\SK\Vsk\Services\Kasko\Calculator\KaskoCalculatorApi;
use App\Services\SK\Vsk\Services\Kasko\Calculator\KaskoCalculatorService;

class ServiceController extends BaseServiceController
{
    const SERVICES = [
        'dictionary' => [
          'service' => DictionaryService::class,
          'api' => DictionaryApi::class
        ],
        'kasko' => [
            'service' => KaskoService::class,
            'api' => KaskoApi::class,
        ],
        'kasko_calculator' => [
            'service' => KaskoCalculatorService::class,
            'api' => KaskoCalculatorApi::class,
        ],
        'osago' => [
            'service' => OsagoService::class,
            'api' => OsagoApiFactory::class,
        ],

    ];


    const PROGRAMS = [
        'kasko' => [
            ['id' => 1, 'name' => 'Lesdf', 'template' => 'avto_casco', 'title' => 'КАСКО'],
            ['id' => 2, 'name' => 'AgrEconom', 'template' => 'default', 'title' => 'TEST'],
        ],

    ];

}
