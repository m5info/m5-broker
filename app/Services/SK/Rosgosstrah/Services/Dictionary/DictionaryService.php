<?php

namespace App\Services\SK\Rosgosstrah\Services\Dictionary;

use App\Interfaces\Services\SK\DictionaryServiceInterface;


class DictionaryService implements DictionaryServiceInterface {


    public $api;

    public function __construct(DictionaryApi $api){
        $this->api = $api;
    }

    public function get_api_purpose(){

        return $this->api->get_purpose();

    }

    public function get_api_categories(){

        return $this->api->get_categories();

    }

    public function get_marks_models(){

        $marks = $this->api->get_marks();
        $models = $this->api->get_models();
        $categories = $this->api->get_categories();

        $result = [
            'categories' => $categories,
            'marks' => $marks,
            'models' => $models,
        ];

        return $result;

    }
}