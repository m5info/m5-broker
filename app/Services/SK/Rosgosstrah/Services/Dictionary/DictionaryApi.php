<?php

namespace App\Services\SK\Rosgosstrah\Services\Dictionary;

use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Facades\Excel;

class DictionaryApi{


    public $url;
    public $login;
    public $password;

    public $cache_json = __DIR__."/lists/json.json";
    public $current_list = __DIR__ . "/lists/list-2018-08-07.xlsx";
    public $sheet_name = "Справочник";

    public $cols = [

        'category_id' => 'J',
        'category_name' => 'H',
        'code' => 'A',
        'mark_name' => 'B',
        'model_name' => 'C',
        'model_mark_code' => 'K',

    ];


    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

    }


    public function get_purpose(){
        //Дергаем справочник

        //Если у СК это не реализовано возвращаем константу

        $purposes = [
            ['id'=>'1',  'title'=>'Личная'],
            ['id'=>'2',  'title'=>'Служебная'],
            ['id'=>'3',  'title'=>'Регулярные перевозки пассажиров и багажа, вкл. такси '],
            ['id'=>'4',  'title'=>'Перевозка пассажиров и багажа по заказам (фрахт)'],
            ['id'=>'5',  'title'=>'Перевозка не взрывоопасных грузов (за исключением эвакуации ТС)'],
            ['id'=>'6',  'title'=>'Перевозка взрывоопасных грузов'],
            ['id'=>'7',  'title'=>'Прокат'],
            ['id'=>'8',  'title'=>'Обучение вождению'],
            ['id'=>'9',  'title'=>'Скорая медицинская помощь'],
            ['id'=>'10', 'title'=>'Пожарная охрана'],
            ['id'=>'11', 'title'=>'Инкассация'],
            ['id'=>'12', 'title'=>'Участие в соревнованиях'],
            ['id'=>'13', 'title'=>'Участие в испытаниях'],
            ['id'=>'14', 'title'=>'Выставочный экспонат'],
            ['id'=>'15', 'title'=>'Строительные работы'],
            ['id'=>'16', 'title'=>'Эвакуация ТС'],
            ['id'=>'17', 'title'=>'Коммунальные работы'],
            ['id'=>'18', 'title'=>'Тест-драйв'],
            ['id'=>'19', 'title'=>'Подменное ТС'],
            ['id'=>'20', 'title'=>'Сельскохозяйственные работы'],
            ['id'=>'21', 'title'=>'Служба доставки, в том числе почтовой'],
            ['id'=>'22', 'title'=>'УВО/ЧОП (выезды на объекты под охраной)'],
            ['id'=>'23', 'title'=>'Обслуживание инфраструктуры аэропорта'],
            ['id'=>'24', 'title'=>'Лесозаготовки'],
            ['id'=>'25', 'title'=>'Уборка мусора'],
            ['id'=>'26', 'title'=>'Карьерные работы'],
            ['id'=>'27', 'title'=>'Аренда (за исключением проката)'],
            ['id'=>'28', 'title'=>'Каршеринг'],
            ['id'=>'29', 'title'=>'Перевозка заключенных (ФСИН)'],
            ['id'=>'30', 'title'=>'Правоохранительная деятельность (МВД)'],
            ['id'=>'31', 'title'=>'Служба спасения (МЧС)'],
        ];

        return $purposes;
    }

    public function get_categories(){

        $categories = [];

        foreach($this->get_list() as $row){

            $row_category_name = $row[$this->cols['category_name']];
            $row_category_id = $row[$this->cols['category_id']];

            $checks = [
//                !isset($categories[$category_id]),
                !empty(trim($row_category_name)),
                !empty(trim($row_category_id)),
            ];

            if(!in_array(false, $checks,true)){

                $categories[$row_category_id] = [
                    'id' => $row_category_id,
                    'title' => $row_category_name,
                ];

            }
        }

        sort($categories);

        return $categories;

    }

    public function get_marks(){

        $marks = [];

        foreach($this->get_list() as $row){

            $mark_name = $row[$this->cols['mark_name']];
            $model_name = $row[$this->cols['model_name']];
            $mark_id = $row[$this->cols['code']];

            $checks = [
                !isset($marks[$mark_id]),
                empty($model_name),
                !empty(trim($mark_name)),
                !empty(trim($mark_id)),
            ];

            if(!in_array(false, $checks,true)){

                $marks[$mark_id] = [
                    'id' => $mark_id,
                    'title' => $mark_name,
                    'vehicle_categorie_sk_id' => '',
                ];

            }

        }

        sort($marks);

        return $marks;
    }


    public function get_models(){

        $models = [];

        foreach($this->get_list() as $row){

            $category_id = $row[$this->cols['category_id']];
            $mark_id = $row[$this->cols['model_mark_code']];
            $model_name = $row[$this->cols['model_name']];
            $model_id = $row[$this->cols['code']];



            $checks = [
                !isset($models[$model_id]),
                !empty($model_name),
                !empty(trim($model_name)),
                !empty(trim($model_id)),
            ];

            if(!in_array(false, $checks,true)){

                $models[$model_id] = [
                    'id' => $model_id,
                    'title' => $model_name,
                    'vehicle_mark_sk_id' => $mark_id,
                    'vehicle_categorie_sk_id' => $category_id,
                ];

            }

        }

        sort($models);

        return $models;

    }



    public function get_list(){

        $list = [];

        if(!is_file($this->cache_json)){

            Excel::load($this->current_list, function($reader) use (&$list) {
                $reader->sheet($this->sheet_name, function ($sheet) use (&$list) {
                    $list = self::get_array_cells($sheet);
                    array_shift($list);
                });
            });

            file_put_contents($this->cache_json, \GuzzleHttp\json_encode($list));

        }else{
            return \GuzzleHttp\json_decode(file_get_contents($this->cache_json),true);
        }

        return $list;
    }

    private static function get_array_cells(LaravelExcelWorksheet $sheet){
        $sheet_cells = [];
        foreach ($sheet->getCellCollection() as $cell_coord) {
            $coord = self::get_row_and_col($cell_coord);
            $sheet_cells[$coord['row']][$coord['col']] = $sheet->getCellCacheController()->getCacheData($cell_coord)->getValue();
        }
        return $sheet_cells;
    }

    private static function get_row_and_col($cell_coord){

        preg_match('/[0-9]{1,}/', $cell_coord, $row_match);
        preg_match('/[A-Z]{1,2}/', $cell_coord, $col_match);

        return [
            'row' => $row_match[0],
            'col' => $col_match[0],
        ];
    }

}