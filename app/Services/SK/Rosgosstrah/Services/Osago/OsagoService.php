<?php

namespace App\Services\SK\Rosgosstrah\Services\Osago;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\File;
use App\Repositories\FilesRepository;
use stdClass;

class OsagoService implements ProductServiceInterface{

    public $api;

    public function __construct(OsagoApi $api){
        $this->api = $api;
    }

    public function calc(ContractsCalculation $calc){

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';


        $calc = $this->api->calc_osago($calc);

        if($calc->state == true){
            $koeff = $calc->answer->K;

            $response->state = true;
            $response->error = '';
            $response->sk_key_id = $calc->answer->CalculationId;
            $response->statys_id = 1;
            $response->payment_total = $calc->answer->Premium;
            $response->msg = "<span style='color:red'>СК временно не имеет возможности выпуска.</span><br> ТБ: {$koeff->Tb}; КТ: {$koeff->Kt}; КБМ: {$koeff->Kbm}; КВС: {$koeff->Kbc}; КС: {$koeff->Kc}; КП: {$koeff->Kp}; КМ: {$koeff->Km}; КПР: {$koeff->Kpr}; КН: {$koeff->Kn};";

        }else{
            $response->error = $calc->answer;
        }

        return $response;
    }

    public function temp_calc(ContractsCalculation $calc){
        // TODO: Implement temp_calc() method.
    }

    public function release(ContractsCalculation $calc){

        $contract = $calc->contract;

        $response = new stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';

        $result = $this->api->release_osago($calc);

        if($result->state){

            $answer = $result->answer;

            $response->state = true;
            $response->statys_id = 4;  //Выпущен
            $response->answer = $answer;

            $this->save_files($calc, $answer->Data);

        }else{

            $response->error = $result->error;
        }

        $response->bso_id = $contract->bso->id;

        return $response;
    }

    public function check_status(ContractsCalculation $calc){
        $result = $this->api->check_status($calc);

        return $result;
    }

    public function sign(ContractsCalculation $calc, $request){

        if(isset($request->sms_code) && $request->sms_code){
            $result = $this->api->confirm_sms($calc, $request->sms_code);
        }else {
            $result = $this->api->send_sms($calc);
        }

        return $result;
    }

    public function get_files(ContractsCalculation $calc){

        return true;
    }

    public function save_files(ContractsCalculation $calc, $documents){

        $contract = $calc->contract;

        if(isset($documents) && $documents != ''){

            $contract->masks()->detach();

            foreach($documents as $document){

                $File_n = 'pdf';
                $file_contents = base64_decode($document->Document);
                $path = storage_path('app/' . Contracts::FILES_DOC . "/{$contract->id}/");

                if(!is_dir(($path))){
                    mkdir(($path), 0777, true);
                }

                $file_name = uniqid();

                file_put_contents($path . $file_name . '.' . $File_n, $file_contents);

                $file = File::create([
                    'original_name' => $document->Name,
                    'ext' => $File_n,
                    'folder' => Contracts::FILES_DOC . "/{$contract->id}/",
                    'name' => $file_name,
                    'user_id' => auth()->id()
                ]);

                $contract->masks()->save($file);
            }

        }

        return true;
    }
}
