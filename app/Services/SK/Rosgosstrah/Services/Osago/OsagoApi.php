<?php

namespace App\Services\SK\Rosgosstrah\Services\Osago;

use App\Models\Contracts\ContractsCalculation;
use App\Models\Log\DebugLogging;
use App\Services\SK\Rosgosstrah\Clients\DigestSoapClient;
use App\Services\SK\Rosgosstrah\Clients\WSSoapClient;
use Carbon\Carbon;
use Doctrine\DBAL\Driver\OCI8\Driver;
use Mockery\Exception;
use SoapClient;
use SoapHeader;
use SoapVar;

class OsagoApi{

    public $url;
    public $login;
    public $password;
    public $errors = [];

    public function __construct($url, $login, $password){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

    }

    /**
     * @param $method
     * @return $auth
     * Метод возвращает Correlation_id для дальнейших действий с методами РГС
     */
    public function auth($method){

        $auth_params = [
            'login' => $this->login,
            'password' => $this->password
        ];

        $curl = curl_init();

        $headers = [];
        $headers[] = "Content-Type: application/json";
        $headers[] = "Accept: application/json";
        $headers[] = "Auth-UserName: {$this->login}";
        $headers[] = "Auth-Password: {$this->password}";

        curl_setopt_array($curl, array(
            CURLOPT_URL => "{$this->url}{$method}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => \GuzzleHttp\json_encode($auth_params),
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return \GuzzleHttp\json_decode($response, true);
    }


    // калькуляция осаго
    public function calc_osago(ContractsCalculation $calc){

        //        $this->check_status($calc);

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $method = '/rest/Osago2/Calculate';

        $auth = $this->auth($method);

        $params = $this->get_calc_params($calc, $auth); // параметры из формы '30d2c035-f2c2-474b-be85-f3179547843b'

        $calc = $this->send($method, $params, $auth);

        if(isset($calc->IsSuccess) && $calc->IsSuccess == true){
            $result->state = true;
            $result->answer = $calc->Data;
        }else{
            $result->answer = $calc->ErrorMessage;
        }

        if(!empty($this->errors)){ // ошибки со справочниками
            $result->answer = implode(';', $this->errors);
        }

        return $result;
    }


    public function release_osago(ContractsCalculation $calc){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $method = '/rest/osago2/Print';

        $auth = $this->auth($method);

        $params = $this->get_release_params($calc, $auth); // передаем ск кей

        $release = $this->send($method, $params, $auth);

        if($release){
            $result->answer = $release;
            $result->state = true;
        }else{
            $result->error = 'Не удалось создать полис';
        }

        return $result;
    }


    public function check_status(ContractsCalculation $calc){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $method = '/rest/osago2/CheckContractStatus';

        $auth = $this->auth($method);

        $params = [
            'series' => "ККК",
            'number' => "6000000206",
            //            'creationDate' => "2019-09-19T09:58:08.8538832+03:00",
        ];

        $release = $this->send($method, \GuzzleHttp\json_encode($params), $auth);

        if($release){
            $result->answer = $release;
            $result->state = true;
        }else{
            $result->error = 'Не удалось создать полис';
        }

        return $result;
    }

    public function send_sms(ContractsCalculation $calc){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $method = '/rest/osago2/SendVerificationCode';

        $auth = $this->auth($method);

        $params = [
            'ContractId' => $calc->sk_key_id,
            'Mobile' => $calc->contract->insurer->phone
        ];

        $answer = $this->send($method, \GuzzleHttp\json_encode($params), $auth);

        if($answer){
            $result->answer = $answer;
            $result->state = true;
        }else{
            $result->error = 'Не удалось отправить смс';
        }

        return $result;
    }


    public function confirm_sms(ContractsCalculation $calc, $code){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $method = '/rest/osago2/ConfirmVerificationCode';

        $auth = $this->auth($method);

        $params = [
            'ContractId' => $calc->sk_key_id,
            'VerificationCode' => $code
        ];

        $answer = $this->send($method, \GuzzleHttp\json_encode($params), $auth);

        if((isset($answer->Data) && $answer->Data) || (isset($answer->ErrorMessage) && $answer->ErrorMessage == 'Номер телефона уже подтвержден')){
            $calc->sms_verify = 1;
            $calc->save();

            $result->answer = $answer;
            $result->state = true;
        }else{
            $result->error = 'Не удалось отправить смс';
        }

        return $result;
    }


    public function send($method, $params, $auth){

        $curl = curl_init();
        $headers = [];

        $headers[] = 'Content-Type: application/json';
        $headers[] = "Auth-UserName: {$this->login}";
        $headers[] = "Auth-Password: {$this->password}";

        curl_setopt_array($curl, array(
            CURLOPT_URL => "{$this->url}{$method}?correlationId={$auth['CorrelationId']}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return \GuzzleHttp\json_decode($response);
    }


    public function get_calc_params(ContractsCalculation $calc, $auth, $calculation_id = null){

        $is_pre_calculation = ($calculation_id) ? false : true;
        $contract = $calc->contract;
        $agent_info = ($contract->getAgentSettings($calc)) ? $contract->getAgentSettings($calc) : null;

        $result = [];

        if($agent_info){
            $result["SaleChannelType2008Id"] = $agent_info->сhannel_sale_id; // ID канала продаж 
            $result["BranchCode"] = $agent_info->agent_id;
        }else{
            $this->errors[] = 'Нет информации по агентскому договору';
        }
        $result["CalculationId"] = $calculation_id; // 'c9efa1d8-54b0-4dd2-bd7a-b549f247b97f'
        $result["CorellationId"] = $auth['CorrelationId'];
        $result["CorrelationId"] = $auth['CorrelationId'];
        $result["IsPreCalculation"] = false;
        $result["CalculationDate"] = date('Y-m-d\TH:i:s', strtotime(Carbon::now()));
        $result["StartDate"] = date('Y-m-d\T00:00:00', strtotime($contract->begin_date));
        $result["EndDate"] = date('Y-m-d\T00:00:00', strtotime($contract->end_date));
        $result["PrimaryUseKladr"] = $contract->insurer->data_fl->address_register_city_kladr_id;
        $result['Agent'] = $this->get_agent($calc);
        $result["IsAnyone"] = ($contract->drivers_type_id == 1) ? true : false; // ограничения допуска к авто
        $result['Auto'] = $this->get_auto($calc);
        $result['Insurant'] = $this->get_subject($calc, 'insurer');
        $result['Owner'] = $this->get_subject($calc, 'owner');
        $result["Periods"] = $this->get_periods_of_use($calc);
        $result["OwnerLicense"] = ($contract->owner->id == $contract->insurer->id) ? true : false; // ВУ собственника
        $result["IsUpdate"] = false;
        $result["UpdateRequest"] = false;
        $result["RegistrationPlace"] = 1;
        $result["NeedKbmRequest"] = true; // всегда true
        $result['IsRsaChecked'] = true;
        $result['IsEOsago'] = true;

        //        $result['UpdateRequest'] = [
        //            'StartDate' => date('Y-m-d\T00:00:00', strtotime($contract->begin_date)),
        //            'EndDate' => date('Y-m-d\T00:00:00', strtotime($contract->end_date)),
        //            'PrimaryUseKladr' => $contract->insurer->data_fl->address_register_city_kladr_id,
        //            'RegistrationPlace' => 1,
        //            'ModifyReasonIds' => [
        //                '15'
        //            ]
        //        ];
        //        $result['UpdateRequest']['Auto'] = $this->get_auto($calc);
        //        $result['UpdateRequest']['Coefficients'] = [
        //            "Tb" => 4118.0,
        //            "Kt" => 2.0,
        //            "Kbc" => 0.96,
        //            "Ko" => 1.0,
        //            "Km" => 1.4,
        //            "Kn" => 1.0,
        //            "Kc" => 1.0,
        //            "Kp" => 1.0,
        //            "Kbm" => 0.5,
        //            "Kpr" => 1.0
        //        ];
        //        $result['UpdateRequest']['Premium'] = 11111.11;
        //        $result['UpdateRequest']['ReturnsSum'] = 0.0;

        return \GuzzleHttp\json_encode($result, true);
    }


    public function get_release_params(ContractsCalculation $calc, $auth){

        $contract = $calc->contract;
        $autoInfo = $calc->object_insurer_auto;
        $insurer = $contract->insurer;
        $insurer_data = ($insurer->type == 0) ? $insurer->data_fl : $insurer->data_ul;
        $owner = $contract->owner;
        $owner_data = ($owner->type == 0) ? $owner->data_fl : $owner->data_ul;
        $fio = explode(' ', $insurer_data->fio);

        $bso_number = $contract->is_epolicy ? null : $contract->bso->bso_number;
        $bso_serie = $contract->is_epolicy ? null : $contract->bso->type->serie->first()->bso_serie;

        $result = [
            "calculationId" => $calc->sk_key_id,
            "draft" => true,
            "printOnlyDocumentType" => 0, // 0 - печать Заявления и Образец
            "number" =>  $bso_number,
            "series" =>  $bso_serie,
            "contractDate" =>  date('Y-m-d\TH:i:s', strtotime(Carbon::now())),
            "firstPremium" => '',
            "specialNotes" => "Спец. заметки",
            //            "inspectionDocument" => [
            //                "typeCode" => 11,
            //                "series" => 2233,
            //                "number" => 445566,
            //                "issueDate" => "2010-01-01T00:00:00",
            //            ],
            "insurantContacts" => [
                "homePhone" => "",
                "mobilePhone" => ($insurer->phone) ? $insurer->phone : "",
                "workPhone" => "",
                "email" => ($insurer->email) ? $insurer->email : ""
            ],
            "ownerAddress" => [
                "countryCode" => "643",
                "kladrCode" => $owner->address_register_city_kladr_id,
                "region" => $owner->address_register_region,
                "area"=>"",
                "city"=> $owner->address_register_city,
                "place"=> "",
                "street" => $owner->address_register_street,
                "house" => $owner->address_register_house,
                "building" => $owner->address_register_block,
                "flat" => $owner->address_register_flat,
                "postCode"=> "",
                "fullAddress"=> $owner->address_register
            ],
            "insurantAddress" => [
                "countryCode" => "643",
                "kladrCode" => $insurer_data->address_register_kladr,
                "region" => $owner->address_register_region,
                "area" => "",
                "city" => $insurer_data->address_register_city,
                "place" => "",
                "street" => $insurer_data->address_register_street,
                "house" => $insurer_data->address_register_house,
                "building" => $insurer_data->address_register_block,
                "flat" => $insurer_data->address_register_flat,
                "postCode" => "",
                "fullAddress" => $insurer_data->address_register
            ],
            "unladenMass" => ($autoInfo->weight) ? $autoInfo->weight : '',
        ];

        if($insurer->type == 0){// если физик
            $result['insurer'] = [
                "firstName" => $fio[0],
                "lastName" => $fio[2],
                "secondName" => $fio[1]
            ];
        }else{
            $subject = $contract->insurer;

            $result['insurer'] = [
                "KPP" => $subject->kpp,
                "OGRN" => $subject->ogrn,
                "Inn" => $subject->inn,
                "OrganizationName" => $subject->title,
            ];
        }

        return \GuzzleHttp\json_encode($result, true);
    }


    public function get_subject(ContractsCalculation $calc, $subj_type){

        $contract = $calc->contract;

        $subject = [];

        if($contract_subject = $contract->{$subj_type}){

            $subjectType = $contract->{$subj_type}->type == 0 ? '1' : '2';

            if($contract_subject->type == 0){// если физик

                $data_fl = $contract_subject->data_fl;

                $phone = ($contract_subject->phone) ? substr(parsePhoneNumber($contract_subject->phone), 1, 11) : null;

                $fio = $data_fl->get_fio();

                $subject = [
                    'BirthDate' => $data_fl->birthdate,
                    'IsResident' => true,
                    'SubjectTypeId' => $subjectType,
                    'Contacts' => [
                        'MobilePhone' => $phone,
                        'Email' => $contract_subject->email,
                    ],
                    'Document' => [
                        'TypeCode' => '012',
                        'Series' => $data_fl->doc_serie,
                        'Number' => $data_fl->doc_number,
                    ],
                    'Name' => [
                        'FirstName' => $fio['first_name'],
                        'LastName' => $fio['last_name'],
                        'SecondName' => $fio['middle_name'],
                    ],
                    'Address' => [
                        'CountryCode' => '643',
                        'City' => $data_fl->address_register_city,
                        //                        'Place' => $data_fl->address_register_city,
                        'Street' => $data_fl->address_register_street,
                        'KladrCode' => substr($data_fl->address_register_kladr, 0, 17),
                        'Region' => $data_fl->address_register_city,
                        'postCode' => $data_fl->address_register_zip,
                    ],
                ];

            }else{// юрик

                $contract_subject_info = $contract_subject->get_info();
                $phone = ($contract_subject->phone) ? setPhoneNumberFormat(parsePhoneNumber($contract_subject->phone), '+7dddddddddd') : null;

                $subject = [
                    "KPP" => $contract_subject_info->kpp,
                    "OGRN" => $contract_subject_info->ogrn,
                    "Contacts" => [
                        "WorkPhone" => $phone,
                        "Email" => $contract_subject->email,
                    ],
                    "Document" => [
                        "TypeCode" => "025",
                        "Series" => $contract_subject->doc_serie,
                        "Number" => $contract_subject->doc_number,
                    ],
                    "Inn" => $contract_subject->inn,
                    "IsResident" => true,
                    "OrganizationName" => $contract_subject->title,
                    "SubjectTypeId" => $subjectType,
                    "Address" => [
                        "CountryCode" => "643",
                        "KladrCode" => $contract_subject_info->address_register_kladr,
                        "Region" => $contract_subject_info->address_register_region,
                        "Area" => "",
                        "City" => $contract_subject_info->address_register_city,
                        "Place" => "",
                        "Street" => $contract_subject_info->address_register_street,
                        "House" => $contract_subject_info->address_register_house,
                        "Building" => $contract_subject_info->address_register_block,
                        "Flat" => $contract_subject_info->address_register_flat,
                        "PostCode" => "",
                        "FullAddress" => $contract_subject_info->address_register,
                    ],
                ];

            }

        }else{
            $this->errors[] = 'Нет данных по Страхователю';
        }

        return $subject;
    }


    public function get_auto(ContractsCalculation $calc){

        $contract = $calc->contract;
        $autoInfo = $calc->object_insurer_auto;
        $auto = [];

        if($autoInfo){

            if(isset($autoInfo->doc_type) && $autoInfo->doc_type == 1){ // STS
                $doc_type = 'СТС';
            }else{ // PTS
                $doc_type = 'ПТС';
            }

            $auto = [
                "HasTrailer" => ($autoInfo->is_trailer == 1) ? true : false,
                "LicensePlate" => $autoInfo->reg_number,
                "Power" => $autoInfo->power,
                "Vin" => $autoInfo->vin,
                "ClassifierVehicleModelCode" => $autoInfo->vin,
                "PurposeUseCode" => $autoInfo->vin,
                "Drivers" => ($contract->drivers_type_id == 1) ? null : $this->get_drivers($calc),
                "ManufactureYear" => $autoInfo->car_year,
                "Document" => [
                    'TypeCode' => $doc_type,
                    'Series' => $autoInfo->docserie,
                    'Number' => $autoInfo->docnumber,
                    'IssueDate' => date('Y-m-d\T00:00:00', strtotime($autoInfo->docdate))
                ]
            ];

            if($purpose = $calc->sk_purpose){
                $auto['PurposeUseCode'] = $purpose->vehicle_purpose_sk_id;
            }else{
                $this->errors[] = 'Цель использования не найдена';
            }

            if($calc->sk_model){
                $auto['ClassifierVehicleModelCode'] = $calc->sk_model->vehicle_models_sk_id;
                $auto['PtsModel'] = "{$calc->sk_mark->sk_title} {$calc->sk_model->sk_title}";
            }else{
                $this->errors[] = 'Не синхронизирован справочник: Марка';
            }

        }else{
            $this->errors[] = 'Авто не найдено';
        }

        return $auto;
    }


    public function get_drivers(ContractsCalculation $calc){

        $drivers = [];

        if($calc->drivers){

            $id = 0;

            foreach ($calc->drivers as $driver){

                $fio = $driver->get_fio();

                $last_name = $fio['last_name'];
                $first_name = $fio['first_name'];
                $middle_name = $fio['middle_name'];

                $driver_data = [
                    'Name' => [
                        'FirstName' => $first_name,
                        'LastName' => $last_name,
                        'SecondName' => $middle_name,
                    ],
                    'DriverId' => $driver->id,
                    'License' => [
                        'TypeCode' => (int)$driver->foreign_docs ? "024" : "017",
                        'Series' => $driver->doc_serie,
                        'Number' => $driver->doc_num,
                    ],
                    'BirthDate' =>  date('Y-m-d\TH:i:s', strtotime($driver->birth_date)),
                    'DrivingStartExperienceDate' =>  date('Y-m-d\TH:i:s', strtotime($driver->exp_date)),
                    'KbmRequestId' =>  $driver->kbm_rsa_request_id,
                    'CurrentDriverOsagoKbm' =>  $driver->kbm,
                    'CurrentDriverOsagoKbmClass' =>  (int)$driver->class_kbm,
                    'IsDriverAdded' =>  false,
                    'IsDriverRemoved' =>  false,
                    'DriverAddedDate' =>  date('Y-m-d\TH:i:s'),
                ];

                if($old_license_data = $driver->old_license){

                    $previous_driver_data = [
                        "PrevLicense" => [
                            "series" => $old_license_data->doc_series,
                            "number" => $old_license_data->doc_number,
                        ]
                    ];

                    $drivers[] = array_merge($driver_data, $previous_driver_data);

                }else{
                    $drivers[] = $driver_data;

                }
            }

        }else{
            $this->errors[] = 'Не указан водитель';
        }

        return $drivers;
    }

    public function get_purpose(ContractsCalculation $calc){

        $purpose_id = '';

        if($purpose = $calc->sk_purpose){
            $purpose_id = $purpose->vehicle_purpose_sk_id;
        }else{
            $this->errors[] = 'Цель использования не найдена';
        }

        return $purpose_id;
    }

    public function get_agent(ContractsCalculation $calc){

        $agent = [];
        $contract = $calc->contract;

        if($contract->user->organization->inn){

            $agent = [
                "INN" => $contract->user->organization->inn, // 7710353606 из примера
                "SubjectTypeId" => "2",
            ];

        }else{
            $this->errors[] = 'Агент не найден';
        }

        return $agent;
    }


    public function get_periods_of_use(ContractsCalculation $calc){

        $periods_of_use = [];

        $contract = $calc->contract;

        $periods_of_use[] = [
            "Id" => 1,
            "StartDate" => date('Y-m-d\TH:i:s', strtotime($contract->begin_date)),
            "EndDate" => date('Y-m-d\TH:i:s', strtotime($contract->end_date)),
            "IsUsagePeriodAdded" => false,
            "IsUsagePeriodRemoved" => false
        ];

        return $periods_of_use;

    }

}