<?php


namespace App\Services\SK\Rosgosstrah\Services\Kasko;


use App\Models\Contracts\ContractsCalculation;
use Carbon\Carbon;

class KaskoApi{

    public $url;
    public $login;
    public $password;
    public $errors = [];


    public function __construct($url, $login, $password){
        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
    }

    /**
     * @param $method
     * @return $auth
     * Метод возвращает Correlation_id для дальнейших действий с методами РГС
     */
    public function auth($method = '/rest/auth/Login'){

        $auth_params = [
            'login' => $this->login,
            'password' => $this->password
        ];

        $curl = curl_init();

        $headers = [];
        $headers[] = "Content-Type: application/json";
        $headers[] = "Accept: application/json";
        $headers[] = "Auth-UserName: {$this->login}";
        $headers[] = "Auth-Password: {$this->password}";

        curl_setopt_array($curl, array(
            CURLOPT_URL => "{$this->url}{$method}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => \GuzzleHttp\json_encode($auth_params),
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return \GuzzleHttp\json_decode($response, true);
    }

    public function calc_kasko(ContractsCalculation $calc){

        $result = (object)['state' => false, 'answer' => [], 'error'=>''];

        $auth = $this->auth();

        $method = '/rest/kasko/Calculate';

        $params = $this->get_calc_param($calc, $auth);

        $calc = $this->send($method, $params,  $auth);
        dd($calc);

        return $result;
    }


    public function send($method, $params, $auth){

        $curl = curl_init();
        $headers = [];

        $headers[] = 'Content-Type: application/json';
        $headers[] = "Auth-UserName: {$this->login}";
        $headers[] = "Auth-Password: {$this->password}";

        curl_setopt_array($curl, array(
            CURLOPT_URL => "{$this->url}{$method}?correlationId={$auth['CorrelationId']}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return \GuzzleHttp\json_decode($response);
    }


    public function get_calc_param(ContractsCalculation $calc, $auth){

        $contract = $calc->contract;

//        $result = [];
//        $result['isPreCalculation'] = false;
//        $result['calculationDate'] = date('Y-m-d\TH:i:s', strtotime(Carbon::now()));
//        $result['policyStartDate'] = date('Y-m-d\T00:00:00', strtotime($contract->begin_date));
//        $result['policyEndDate'] = date('Y-m-d\T00:00:00', strtotime($contract->end_date));
//        $result['kaskoTerritoryId'] = (int)substr($contract->owner->data_fl->address_fact_kladr, 0, 2);
//        $result['productId'] = 316;
//        $result['policyPaymentsCount'] = 1; // Количество платежей по договору
//        $result['saleChannelType2008Id'] = 310; // ID канала продаж
//        $result['vehicles'][] = $this->get_auto($calc);
//        $result['insurant'] = $this->get_subject($calc, 'insurant');
//        $result['owner'] = $this->get_subject($calc, 'owner');
//        $result['branchId'] = 12;
//        $result['bankId'] = 03333;
//        $result['autoDealerId'] = 4444;
//        $result['leasingCompanyId'] = 100;
//        $result['clientAccept'] = true;
//        $result['planVzd'] = '19.22';
//        $result['agents'] = $this->get_agent($calc);
//        $result['lessee'] = null

        $json = '{
    "CorellationId": "'.$auth['CorrelationId'].'",
    "CorrelationId": "'.$auth['CorrelationId'].'",
    "Beneficiary": null,
    "ContractId": null,
    "IsPreCalculation": false,
    "IsFinalCalculation": true,
    "IsNonTypicalDocuments": false,
    "CalculationDate": "2019-11-06T00:00:00.0000000+03:00",
    "PolicyStartDate": "2019-11-21T00:00:00.0000000+03:00",
    "PolicyEndDate": "2020-11-20T00:00:00.0000000+03:00",
    "KaskoTerritoryId": 77,
    "ProductId": 316,
    "PolicyPaymentsCount": 1,
    "SaleChannelType2008Id": "310",
    "Vehicles": [
        {
            "IsFirstRisk": false,
            "DsagoLiability": 0,
            "DmsLiability": 0,
            "AccidentLiability": 0,
            "Cost": 2000000.0,
            "KaskoRiskId": 2,
            "RsaModelId": "181022228",
            "TransdekraId": "",
            "ManufactureYear": 2018,
            "Liability": 2000000.0,
            "UsagePurposeId": "1",
            "AdmittedLimitId": 1,
            "HasTrailer": false,
            "Franchise": {
                "TypeId": "3",
                "UnitId": "2",
                "Value": 7.0
            },
            "Drivers": [
                {
                    "FullName": {
                        "FirstName": "ВЛАДИМИР",
                        "LastName": "ПАВЛОВ",
                        "SecondName": "ЛЕОНИДОВИЧ"
                    },
                    "BirthDate": "1967-04-09T00:00:00",
                    "GenderCode": "1",
                    "DriverLicense": {
                        "Type": "017",
                        "Series": "9912",
                        "Number": "692590"
                    },
                    "DrivingStartDate": "1985-12-31T00:00:00",
                    "Id": "9912518589432"
                },
                {
                    "FullName": {
                        "FirstName": "ТАТЬЯНА",
                        "LastName": "ПАВЛОВА",
                        "SecondName": "АЛЕКСАНДРОВНА"
                    },
                    "BirthDate": "1967-09-14T00:00:00",
                    "GenderCode": "2",
                    "DriverLicense": {
                        "Type": "017",
                        "Series": "7735",
                        "Number": "810724"
                    },
                    "DrivingStartDate": "1996-12-31T00:00:00",
                    "Id": "9912518591715"
                }
            ],
            "Kc": 1.0,
            "Ka": 1.0,
            "LicensePlate": "Е563МТ799",
            "AlarmIds": [],
            "PaymentMethodId": "13",
            "VehicleEngineHP": 200.0,
            "VehiclePTSDate": "2018-10-26T00:00:00+03:00",
            "IsLiabilityNonAggregate": true,
            "AddEquipments": [],
            "VehMileage": 14938.0,
            "VehNumber": "1",
            "ReservationIds": [],
            "VIN": "XWEPH81BDK0014910",
            "ChassisNumber": "",
            "FrameNumber": ""
        }
    ],
    "Insurant": {
        "SubjectTypeId": 1,
        "KladrCode": "77",
        "Name": {
            "FirstName": "Владимир",
            "LastName": "Павлов",
            "SecondName": "Леонидович"
        },
        "BirthDate": "1967-04-09T00:00:00",
        "Document": {
            "TypeId": "012",
            "Series": "4511",
            "Number": "460579"
        },
        "DocumentIssueDate": "2012-04-21T00:00:00",
        "DocumentIssuePlace": "ОТДЕЛЕНИЕМ УФМС РОССИИ ПО Г.МОСКВЕ ПО РАЙОНУ МЕЩАНСКИЙ",
        "FullAddress": "Город Москва Переулок Последний д. 12 кв. 3 107045",
        "InsurantContactPhone": "9166235086"
    },
    "BranchId": "17714990",
    "ProjectId": "",
    "ProgramId": "",
    "BankId": "01792",
    "AutoDealerId": null,
    "LeasingCompanyId": null,
    "OtherPartnerId": null,
    "Owner": {
        "SubjectTypeId": 1,
        "KladrCode": "77",
        "Name": {
            "FirstName": "Владимир",
            "LastName": "Павлов",
            "SecondName": "Леонидович"
        },
        "BirthDate": "1967-04-09T00:00:00",
        "Document": {
            "TypeId": "012",
            "Series": "4511",
            "Number": "460579"
        }
    },
    "Agents": {
        "AgentINN": "7723477447",
        "AgentLNR": "00772347",
        "AgentSubjectTypeId": "2",
        "AgentLogin": "WEB_OYMARTYNENKO"
    },
    "ClientAccept": true,
    "Lessee": null,
    "OnCredit": true,
    "ActivePoliciesVehiclesCount": 1,
    "PlanVzd": null,
    "LesseeSubjectTypeId": null,
    "ModifyReasonIds": null,
    "PolicyStartDateInternal": "2019-11-21T00:00:00+03:00",
    "PolicyEndDateInternal": "2020-11-20T00:00:00+03:00",
    "CalculationDateInternal": "2019-11-06T00:00:00+03:00",
    "PhoneContractsCount": null
}';

        return $json;
    }

    public function get_auto(ContractsCalculation $calc){

        $auto = [];

        if($ts = $calc->object_insurer_auto){

            $auto['cost'] = $ts->price;
            $auto['kaskoRiskId'] = 2;
            $auto['isFirstRisk'] = false;
            $auto['dsagoLiability'] = 10000;
            $auto['dmsLiability'] = 9500.5;
            $auto['rsaModelId'] = 366021558;
            $auto['transdekraId'] = 400460001;
            $auto['manufactureYear'] = 2012;
            $auto['liability'] = 550000.05;
            $auto['usagePurposeId'] = 2;
            $auto['admittedLimitId'] = 2;
            $auto['accidentLiability'] = 30000;
            $auto['hasTrailer'] = false;
            $auto['franchise'] = [
                'typeId' => 5,
                'unitId' => 7,
                'value' => 5000,
            ];
            $auto['seatCount'] = 2;
            $auto['allowWeight'] = 1000;
            $auto['drivers'] = $this->get_drivers($calc);
            $auto['kc'] = 0.99999;
            $auto['ka'] = 1.01;
            $auto['licensePlate'] = "а777аа 77";
            $auto['alarmIds'] = ['12', '13'];
            $auto['paymentMethodId'] = 3;
            $auto['isGroupDisabled'] = false;
            $auto['isLiabilityNonAggregate'] = true;
            $auto['addEquipments'] = [
                [
                    'id' => 1,
                    'liability' => 2200,
                ],
                [
                    'id' => 2,
                    'liability' => 3300,
                ]
            ];
            $auto['isNeedAddAgreement5'] = false;
            $auto['vehMileage'] = 64523;
            $auto['registrationDate'] = "2011-11-11T00:00:00";
            $auto['vehNumber'] = 1;
            $auto['reservationIds'] = [ "1", "13", "24", "37", "4" ];
            $auto['vin'] = "ABC45678901234567";
            $auto['chassisNumber'] = "12345678901234567";
            $auto['frameNumber'] = "76543210098765432";

        }

        return $auto;
    }

    public function get_drivers(ContractsCalculation $calc){

        $drivers = [];

        if($calc->drivers){

            $id = 0;

            foreach ($calc->drivers as $driver){

                $fio = $driver->get_fio();

                $last_name = $fio['last_name'];
                $first_name = $fio['first_name'];
                $middle_name = $fio['middle_name'];

                $drivers[] = [
                    'fullName' => [
                        'firstName' => $first_name,
                        'lastName' => $last_name,
                        'secondName' => $middle_name
                    ],
                    'birthDate' => date('Y-m-d\TH:i:s', strtotime($driver->birth_date)),
                    'genderCode' => 2,
                    'driverLicense' => [
                        'type' => 024,
                        'series' => $driver->doc_serie,
                        'number' => $driver->doc_num,
                    ],
                    'drivingStartDate' => date('Y-m-d\TH:i:s', strtotime($driver->exp_date)),
                    'id' => $id,
                ];

                $id++;
            }
        }else{
            $this->errors[] = 'Не указан водитель';
        }

        return $drivers;

    }

    public function get_subject(ContractsCalculation $calc, $subj_type){

        $contract = $calc->contract;

        $subject = [];

        if($contract_subject = $contract->{$subj_type}){

            $subjectType = $contract->{$subj_type}->type == 0 ? '1' : '2';

            if($contract_subject->type == 0){// если физик

                $data_fl = $contract_subject->data_fl;
                $phone = ($contract_subject->phone) ? setPhoneNumberFormat(parsePhoneNumber($contract_subject->phone), '+7dddddddddd') : null;
                $fio = explode(' ', $data_fl->fio);

                $subject['isKeyClient'] = false;
                $subject['subjectTypeId'] = $subjectType;
                $subject['name'] = [
                    'firstName' => $fio[1],
                    'lastName' => $fio[0],
                    'secondName' => $fio[2],
                ];
                $subject['birthDate'] = date('Y-m-d\T00:00:00', strtotime($data_fl->birthdate));
                $subject['document'] = [
                    'typeId' => 012,
                    'series' => $data_fl->doc_serie,
                    'number' => $data_fl->doc_number,
                ];

            }else{// юрик

                $subject_info = $contract_subject->get_info();

                $subject['inn'] = 500100732259;
                $subject['isKeyClient'] = true;
                $subject['subjectTypeId'] = 5;
                $subject['kladrCode'] = 35;
                $subject['name'] = [
                    'firstName' => "033",
                    'lastName' => "ABCDE",
                    'secondName' => "54321"
                ];
                $subject['organization'] = [
                    'name' => "Василёк",
                    'orgForm' => "ООО",
                    'orgFormFull' => "Общество с ограниченной ответственностью"
                ];
                $subject['birthDate'] = "1991-01-03T00:00:00";
                $subject['document'] = [
                    'typeId' => "033",
                    'series' => "ABCDE",
                    'number' => "54321"
                ];
                $subject['ogrnip'] = "310662720800034";

            }

        }else{
            $this->errors[] = 'Нет данных по Страхователю';
        }

        return $subject;
    }

    public function get_agent(ContractsCalculation $calc){

        $agent = [];

        $agent['agentBKCardId'] = 341234;
        $agent['agentINN'] = 123709870987;
        $agent['agentKPP'] = 452345;
        $agent['agentLNR'] = 34534;
        $agent['agentSubjectTypeId'] = 1;

        return $agent;
    }

}