<?php


namespace App\Services\SK\Rosgosstrah\Services\Kasko;


use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\ContractsCalculation;
use App\Services\SK\Rosgosstrah\Services\Kasko\KaskoApi;

class KaskoService implements ProductServiceInterface{

    public $api;

    public function __construct(KaskoApi $api){
        $this->api = $api;
    }

    public function temp_calc(ContractsCalculation $calc){

    }

    public function calc(ContractsCalculation $calc)
    {
        $response = new \stdClass();
        $response->state = false;
        $response->error = '';
        $response->sk_key_id = '';
        $response->statys_id = '';
        $response->payment_total = '';
        $response->msg = '';

        $result = $this->api->calc_kasko($calc);

        return $result;
    }

    public function release(ContractsCalculation $calc){

    }


    public function check_status(ContractsCalculation $calc){

    }


    public function get_files(ContractsCalculation $calc){

    }
}