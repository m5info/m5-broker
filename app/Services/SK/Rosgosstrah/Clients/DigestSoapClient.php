<?php

namespace App\Services\SK\Rosgosstrah\Clients;

use SoapClient;
use SoapHeader;
use SoapVar;

class DigestSoapClient extends SoapClient{

    public $login;
    public $password;


    public function __construct($params){

        $this->login = $params['login'];
        $this->password = $params['password'];

        $wsdl = "{$params['url']}/{$params['service_name']}?singlewsdl";
        parent::__construct($wsdl, ['trace' => true, 'exception' => false, 'encoding'=>'utf8']);

    }

    public function __call($function_name, $arguments){

        $auth           = <<<XML
<o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:s="s">
  <o:UsernameToken>
  <o:Username>{$this->login}</o:Username>
  <o:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd#PasswordString">{$this->password}</o:Password>
  </o:UsernameToken>
</o:Security>
XML;
        $authvalues      = new SoapVar($auth, XSD_ANYXML);
//        dd($authvalues);
        $security_header = new SoapHeader("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", $authvalues, true);

        $this->__setSoapHeaders([$security_header]);
        return parent::__call($function_name, $arguments);
    }

}