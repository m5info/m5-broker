<?php

namespace App\Services\SK\Rosgosstrah;

use App\Services\SK\BaseServiceController;
use App\Services\SK\Rosgosstrah\Services\Dictionary\DictionaryApi;
use App\Services\SK\Rosgosstrah\Services\Dictionary\DictionaryService;
use App\Services\SK\Rosgosstrah\Services\Osago\OsagoApi;
use App\Services\SK\Rosgosstrah\Services\Osago\OsagoService;
use App\Services\SK\Rosgosstrah\Services\Kasko\KaskoApi;
use App\Services\SK\Rosgosstrah\Services\Kasko\KaskoService;

class ServiceController extends BaseServiceController {


    const SERVICES = [

        'dictionary' => [
            'service' => DictionaryService::class,  /// Можно проставить для удобства конечно им префиксы типа "AlphaDictionaryService::class"
            'api' => DictionaryApi::class,          /// но наверное необязательно
        ],
        'osago' => [
            'service' => OsagoService::class,
            'api' => OsagoApi::class,
        ],
        'kasko' => [
            'service' => KaskoService::class,
            'api' => KaskoApi::class,
        ],

    ];

    const PROGRAMS = [
        'kasko' => [
            ['id' => 1, 'name' => '301', 'template' => 'default', 'title' => '301'],
        ]

    ];

}