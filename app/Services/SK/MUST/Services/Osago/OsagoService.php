<?php

namespace App\Services\SK\MUST\Services\Osago;

use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\ContractsCalculation;

class OsagoService implements ProductServiceInterface{

    public $available_types = [
        0 => 'Бумажный',
        1 => 'Электронный',
    ];

    public $api;

    public function __construct(OsagoApi $api){
        $this->api = $api;
    }


    public function temp_calc(ContractsCalculation $calc)
    {

    }


    public function calc(ContractsCalculation $calc)
    {
        dd($this->api->calc_osago($calc));
    }


    public function release(ContractsCalculation $calc)
    {

    }


    public function check_status(ContractsCalculation $calc)
    {

    }


    public function get_files(ContractsCalculation $calc)
    {

    }

}