<?php

namespace App\Services\SK\MUST\Services\Osago;

use App\Services\SK\MUST\Services\SendAPI\Send;

class OsagoApi{


    public $url;
    public $login;
    public $password;
    public $sender = null;
    public $errors = [];
    private $jsonBody = [];

    const PATHS = [
        'get_token' => '/api/scoring/auth'
    ];

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->sender = new Send($url, $login, $password, $api_setting);
        $this->jsonBody = [];

    }


    public function calc_osago($calc)
    {
        $this->jsonBody['login'] = 'login';
        $this->jsonBody['password'] = 'password';

        $result = $this->sender->send($this->jsonBody, OsagoApi::PATHS['get_token']);

        return $result;
    }
}
