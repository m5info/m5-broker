<?php

namespace App\Services\SK\MUST\Services\SendAPI;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class Send{

    public $url;
    public $login;
    public $password;

    public $SessionId;
    public $UserId;

    public $api_setting = null;

    public function __construct($url, $login, $password, $api_setting){

        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->api_setting = $api_setting;

        return $this;
    }

    public function send($data_arr, $method)
    {
        $client = new Client(['base_uri' => $this->url]);
        try{
            $result = $client->request('POST', $method, ['json' => $data_arr]);
        }catch (ClientException $e){
            dd($e->getMessage());
        }

        return $result;
    }

}


