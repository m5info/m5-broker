<?php

namespace App\Services\SK\MUST;

use App\Services\SK\BaseServiceController;
use App\Services\SK\MUST\Services\Dictionary\DictionaryApi;
use App\Services\SK\MUST\Services\Dictionary\DictionaryService;
use App\Services\SK\MUST\Services\Osago\OsagoApi;
use App\Services\SK\MUST\Services\Osago\OsagoService;

class ServiceController extends BaseServiceController {


    const SERVICES = [

        'dictionary' => [
            'service' => DictionaryService::class,
            'api' => DictionaryApi::class,
        ],
        'osago' => [
            'service' => OsagoService::class,
            'api' => OsagoApi::class,
        ],

    ];



}