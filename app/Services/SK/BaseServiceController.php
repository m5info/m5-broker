<?php

namespace App\Services\SK;

use App\Models\Directories\BsoSuppliers;

class BaseServiceController{

    public $bso_supplier;

    const SERVICES = [];

    public function __construct(BsoSuppliers $bso_supplier){

        $this->bso_supplier = $bso_supplier;
        //Сделать вспомогательную колонку и до кокого она действует api_settings

    }

    public function get_service($service){

        //если есть актуальные настройки апи
        if($api_setting = $this->bso_supplier->actual_api_setting){

            //если сконфигурирован сервис
            if(isset($this::SERVICES[$service])){

                $api_class = $this::SERVICES[$service]['api'];
                $service_class = $this::SERVICES[$service]['service'];

                $api_work_type = $api_setting->work_type == 0 ? "test" : "battle";

                $url = $api_setting->{"{$api_work_type}_server_url"};
                $login = $api_setting->{"{$api_work_type}_server_login"};
                $password = $api_setting->{"{$api_work_type}_server_password"};

                //создаём апи
                $api_obj = new $api_class($url, $login, $password, $api_setting);

                //создаём сервис
                $service_obj = new $service_class($api_obj);

                return $service_obj;

            }

        }

        return false;

    }

}