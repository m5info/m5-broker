<?php

namespace App\Services\DData;

use Exception;
use GuzzleHttp\Client;

class DDataGetAdress
{

    protected $url = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/';
    protected $url_address = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address';
    protected $url_party = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/party';
    protected $API_KEY = '5f6f4a246bd9d39b1a60c5058e4aaa359bed9c04'; //2db5a8f85ea762d911f7bf65b7371a076a7aebae

    public function send($metod, $arr){
        $curl = curl_init();

        $headers = [];
        $headers[] = 'Authorization: Token '. $this->API_KEY ;
        $headers[] = 'Content-type: application/json; charset=utf-8';
        $headers[] = 'Accept: application/json';

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url.$metod,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => \GuzzleHttp\json_encode($arr),
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return null;
        }


        return json_decode($response);

    }

    public function getAddress($address, $limit = 1)
    {
        if (!$address) {
            return 'Некорректный адрес';
        }




        $arr = [];
        $arr['query'] = $address;
        $arr['count'] = $limit;

        return $this->send('address', $arr);
    }

    public function getParty($party, $limit = 10)
    {
        if (!$party) {
            return 'Некорректный адрес';
        }

        $arr = [];
        $arr['query'] = $party;
        $arr['count'] = $limit;

        return $this->send('party', $arr);
    }


    public function getGetResponseAddressXml($json){

        $xml = '';

        foreach ($json->suggestions as $suggestions){

            $data = $suggestions->data;

            $xml .="
            <Address>
                <Value>$suggestions->value</Value>
                <UnrestrictedValue>$suggestions->unrestricted_value</UnrestrictedValue>
                <Data>
                    <PostalCode>{$data->postal_code}</PostalCode>
                    <Country>{$data->country}</Country>
                    <RegionFiasId>{$data->region_fias_id}</RegionFiasId >
                    <RegionKladrId>{$data->region_kladr_id}</RegionKladrId>
                    <RegionWithType>{$data->region_with_type}</RegionWithType>
                    <RegionTypeFull>{$data->region_type_full}</RegionTypeFull>
                    <Region>{$data->region}</Region>
                    <AreaFiasId>{$data->area_fias_id}</AreaFiasId>
                    <AreaKladrId>{$data->area_kladr_id}</AreaKladrId>
                    <AreaWithType>{$data->area_with_type}</AreaWithType>
                    <AreaTypeFull>{$data->area_type_full}</AreaTypeFull>
                    <Area>{$data->area}</Area>
                    <CityFiasId>{$data->city_fias_id}</CityFiasId>
                    <CityKladrId>{$data->city_kladr_id}</CityKladrId>
                    <CityWithType>{$data->city_with_type}</CityWithType>
                    <CityTypeFull>{$data->city_type_full}</CityTypeFull>
                    <City>{$data->city}</City>
                    <CityArea>{$data->city_area}</CityArea>
                    <CityDistrictFiasId>{$data->city_district_fias_id}</CityDistrictFiasId>
                    <CityDistrictKladrId>{$data->city_district_kladr_id}</CityDistrictKladrId>
                    <CityDistrictWithType>{$data->city_district_with_type}</CityDistrictWithType>
                    <CityDistrictTypeFull>{$data->city_district_type_full}</CityDistrictTypeFull>
                    <CityDistrict>{$data->city_district}</CityDistrict>
                    <SettlementFiasId>{$data->settlement_fias_id}</SettlementFiasId>
                    <SettlementKladrId>{$data->settlement_kladr_id}</SettlementKladrId>
                    <SettlementWithType>{$data->settlement_with_type}</SettlementWithType>
                    <SettlementTypeFull>{$data->settlement_type_full}</SettlementTypeFull>
                    <Settlement>{$data->settlement}</Settlement>
                    <StreetFiasId>{$data->street_fias_id}</StreetFiasId>
                    <StreetKladrId>{$data->street_kladr_id}</StreetKladrId>
                    <StreetWithType>{$data->street_with_type}</StreetWithType>
                    <StreetTypeFull>{$data->street_type_full}</StreetTypeFull>
                    <Street>{$data->street}</Street>
                    <HouseFiasId>{$data->house_fias_id}</HouseFiasId>
                    <HouseKladrId>{$data->house_kladr_id}</HouseKladrId>
                    <HouseTypeFull>{$data->house_type_full}</HouseTypeFull>
                    <House>{$data->house}</House>
                    <BlockTypeFull>{$data->block_type_full}</BlockTypeFull>
                    <Block>{$data->block}</Block>
                    <FlatTypeFull>{$data->flat_type_full}</FlatTypeFull>
                    <Flat>{$data->flat}</Flat>
                    <FlatArea>{$data->flat_area}</FlatArea>
                    <SquareMeterPrice>{$data->square_meter_price}</SquareMeterPrice>
                    <FlatPrice>{$data->flat_price}</FlatPrice>
                    <PostalBox>{$data->postal_box}</PostalBox>
                    <FiasId>{$data->fias_id}</FiasId>
                    <FiasCode>{$data->fias_code}</FiasCode>
                    <FiasLevel>{$data->fias_level}</FiasLevel>
                    <FiasActualityState>{$data->fias_actuality_state}</FiasActualityState>
                    <KladrId>{$data->kladr_id}</KladrId>
                    <CapitalMarker>{$data->capital_marker}</CapitalMarker>
                    <Okato>{$data->okato}</Okato>
                    <Oktmo>{$data->oktmo}</Oktmo>
                    <TaxOffice>{$data->tax_office}</TaxOffice>
                    <TaxOfficeLegal>{$data->tax_office_legal}</TaxOfficeLegal>
                    <GeoLat>{$data->geo_lat}</GeoLat>
                    <GeoLon>{$data->geo_lon}</GeoLon>
                </Data>
            </Address>
            ";

        }

        return $xml;
    }




    public function getGetResponsePartyXml($json){

        $xml = '';

        foreach ($json->suggestions as $suggestions){

            $data = $suggestions->data;

            $address = $this->getGetResponseAddressXml($this->getAddress($data->address->value, 1));
            if(!$address) $address = "<Address>{$data->address->value}</Address>";


            $xml .="
            <Address>
                <Value>$suggestions->value</Value>
                <UnrestrictedValue>$suggestions->unrestricted_value</UnrestrictedValue>
                <Data>
                    <NameFull>{$data->name->full}</NameFull>
                    <NameShort>{$data->name->short}</NameShort>
                    <Hid>{$data->hid}</Hid>
                    <OpfCode>{$data->opf->code}</OpfCode>
                    <OpfFull>{$data->opf->full}</OpfFull>
                    <Inn>{$data->inn}</Inn>
                    <Kpp>{$data->kpp}</Kpp>
                    <Ogrn>{$data->ogrn}</Ogrn>
                    <Okpo>{$data->okpo}</Okpo>
                    <Okved>{$data->okved}</Okved>
                    {$address}
                    <Phones>{$data->phones}</Phones>
                    <Emails>{$data->emails}</Emails>
                    <OkvedType>{$data->okved_type}</OkvedType>
                </Data>
            </Address>
            ";

        }

        return $xml;
    }

}