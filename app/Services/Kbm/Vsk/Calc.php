<?php


namespace App\Services\Kbm\Vsk;


use App\Models\Contracts\KbmCalculation;
use App\Services\SK\Vsk\Services\Clients\VskSoapClient;
use Illuminate\Support\Carbon;

class Calc{
    protected $calc;
    protected $url = 'https://b2b.alfastrah.ru';
    protected $login = 'EVROGARANTS';
    protected $password = '85gZ65p7';

    public function handle(KbmCalculation $calc)
    {
        $this->calc = $calc;

        $service_client = new VskSoapClient([
            'url' => 'http://autoservice.vsk.ru',
            'service_name' => 'Companies/vsk/services/osagointegration.asmx',
            'login' => 'int_sst_ipbaraboshin_20200930',
            'password' => '0yIDD419',
        ]);

        $now = date('Y-m-d', strtotime(Carbon::now()));

        $birthDate = date("Y-m-d", strtotime($this->calc->birth_date));

        $param = <<<XML
<ns1:GetKBM_RSA>
  <ns1:VskCalcRequest>
    <ns1:UserLogin>int_sst_ipbaraboshin_20200930</ns1:UserLogin>
    <ns1:UserPass>0yIDD419</ns1:UserPass>
    <ns1:Type>Person</ns1:Type>
    <ns1:CalcKBMRequest>
      <ns1:DriversRestriction>true</ns1:DriversRestriction>
      <ns1:DateCreate>$now</ns1:DateCreate>
      <ns1:PhysicalPersons>
        <ns1:PhysicalPerson>
          <ns1:Country>643</ns1:Country>
          <ns1:PersonalDriverDataInfo>
            <ns1:DocPerson>20</ns1:DocPerson>
            <ns1:Serial>{$this->calc->document_series}</ns1:Serial>
            <ns1:Number>{$this->calc->document_number}</ns1:Number>
            <ns1:Surname>{$this->calc->last_name}</ns1:Surname>
            <ns1:Name>{$this->calc->first_name}</ns1:Name>
            <ns1:Patronymic>{$this->calc->middle_name}</ns1:Patronymic>
            <ns1:BirthDate>{$birthDate}</ns1:BirthDate>
          </ns1:PersonalDriverDataInfo>
        </ns1:PhysicalPerson>
      </ns1:PhysicalPersons>
    </ns1:CalcKBMRequest>
  </ns1:VskCalcRequest>
</ns1:GetKBM_RSA>
XML;


        $param = new \SoapVar($param, XSD_ANYXML);

        try{
            $answer = $service_client->GetKBM_RSA($param);

        }catch(\SoapFault $e){

        }

        $this->saveResult($answer);
    }

    private function saveResult($response)
    {
        if(isset($response->VskCalcResponse->ErrorList->ErrorInfo) && $response->VskCalcResponse->ErrorList->ErrorInfo->Code == 3){
            $calcResponse = $response->VskCalcResponse->CalcResponceValue;

            $this->calc->update([
                'kbm' => $calcResponse->CalculatedKBMValue ?: 1,
                'class_kbm' => $this->getClassByKbm($calcResponse->CalculatedKBMValue),
                'rsa_request_id' => $calcResponse->CalcKBMResponses->CalcKBMResponse->PartyRequestId ?? '',
            ]);
        } else{
            $this->calc->update([
                'kbm' => 1,
                'class_kbm' => $this->getClassByKbm(1),
                'rsa_request_id' => '',
            ]);
        }
    }

    public function getClassByKbm($kbm){
        $class_kbm = 3;

        switch($kbm){
            case '2,3':
                $class_kbm = 0;
                break;
            case '1,55':
                $class_kbm = 1;
                break;
            case '1,4':
                $class_kbm = 2;
                break;
            case '1':
                $class_kbm = 3;
                break;
            case '0,95':
                $class_kbm = 4;
                break;
            case '0,9':
                $class_kbm = 5;
                break;
            case '0,85':
                $class_kbm = 6;
                break;
            case '0,8':
                $class_kbm = 7;
                break;
            case '0,75':
                $class_kbm = 8;
                break;
            case '0,7':
                $class_kbm = 9;
                break;
            case '0,65':
                $class_kbm = 10;
                break;
            case '0,6':
                $class_kbm = 11;
                break;
            case '0,55':
                $class_kbm = 12;
                break;
            case '0,5':
                $class_kbm = 13;
                break;
        }

        return $class_kbm;
    }

    public function auth($method, $json, $type = "POST")
    {

        $curl = curl_init();
        $headers = [];

        $headers[] = 'Accept: */*';
        $headers[] = 'Content-Type: application/json';

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url . $method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_HEADER => 1,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_POSTFIELDS => $json ? \GuzzleHttp\json_encode($json) : null,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        $headers = explode("\n", $response);
        $nonce_www_auth = null;

        foreach ($headers as $header){
            $exploded_header = explode(':', $header);
            if (isset($exploded_header[0]) && $exploded_header[0] == 'WWW-Authenticate'){
                $nonce_www_auth = $header;
            }
        }

        if (!$nonce_www_auth){
            $nonce_www_auth = $headers[8];
        }


        // WWW-Authenticate
        $nonce = stristr($nonce_www_auth, 'nonce="');
        $nonce = str_replace('nonce="', '', $nonce);
        $nonce = trim(str_replace('"', '', $nonce));
        $qop = 'auth';

        $ha1 = md5("{$this->login}:alfastrah.ru:{$this->password}");
        $ha2 = md5("$type:$method");
        $cnonce = 'nU4eIL3w';
        $final_hash = md5("$ha1:$nonce:00000001:$cnonce:$qop:$ha2");
        curl_close($curl);

        $hashs_data = [
            'method' => $method,
            'json' => $json,
            'type' => $type,
            'final_hash' => $final_hash,
            'nonce' => $nonce,
            'cnonce' => $cnonce,
        ];

        return $hashs_data;
    }

    public function send($method, $params, $type = 'POST')
    {
        $auth = $this->auth($method, $params, $type);

        $curl = curl_init();
        $headers = [];
        $headers[] = "Authorization: Digest username=\"{$this->login}\", realm=\"alfastrah.ru\", nonce=\"{$auth['nonce']}\", uri=\"{$auth['method']}\", cnonce=\"{$auth['cnonce']}\", nc=\"00000001\", qop=\"auth\", response=\"{$auth['final_hash']}\"";
        $headers[] = "Accept: */*";
        $headers[] = "Content-type: application/json";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url . $auth['method'],
            CURLOPT_RETURNTRANSFER => 10,
            //            CURLOPT_HEADER => 1,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $auth['type'],
            CURLOPT_POSTFIELDS => $auth['json'] ? \GuzzleHttp\json_encode($auth['json']) : null,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return \GuzzleHttp\json_decode($response);

    }
}