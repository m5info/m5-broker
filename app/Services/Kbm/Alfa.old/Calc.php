<?php

namespace App\Services\Kbm\Alfa;

use App\Models\Contracts\KbmCalculation;
use Carbon\Carbon;

class Calc
{
    const WSDL = 'https://b2b.alfastrah.ru/cxf/CalcOsago?wsdl';

    protected $calc;

    public function handle(KbmCalculation $calc)
    {

        $this->calc = $calc;

        $soapClient = (new Soap(self::WSDL))->getClient();

        $soapClient->calcOsago($this->formatParams());

        $response = $soapClient->__getLastResponse();

        $this->saveResult($response);

    }

    public function formatParams()
    {
        $params = [
            'ContractType'         => 'Первоначальный',
            'InsuranceMonth'       => '12',
            'RegionOfUse'          => 'Москва',
            'CityOfUse'            => 'Москва',
            'DriversRestriction'   => 'false',
            'FollowToRegistration' => false,
            'FlagrantViolation'    => false,
            'Transport'            => array(
                'Category'           => 'B - легковые',
                'Type'               => 'Легковой а/м',
                'RegistrationRussia' => true,
                'Power'              => '80',
                'IsTaxi'             => false,
                'VIN'                => 'JTMB431V40504C498',
                'BodyNumber'         => '',
                'ChassisNumber'      => '',
            ),
            'TransportOwner'       => [
                'LastName'       => 'Примеров1',
                'FirstName'      => 'Пример1',
                'MiddleName'     => 'Примерович1',
                'BirthDate'      => '1960-05-13',
                'ExperienceDate' => '2000-10-01',
                'PersonDocument' => [
                    'Number' => '4647453',
                    'Seria'  => 'АК34',
                ]
            ],
            'PolicyBeginDate'      => Carbon::tomorrow()->format('Y-m-d\TH:i:s'),
            'PeriodList'           => [
                'Period' => [
                    'StartDate' => Carbon::tomorrow()->format('Y-m-d'),
                    'EndDate'   => Carbon::tomorrow()->addYear(1)->addDay(-1)->format('Y-m-d')
                ]
            ],
            'Drivers'              => [
                [
                    'LastName'       => $this->calc->last_name,
                    'FirstName'      => $this->calc->first_name,
                    'MiddleName'     => $this->calc->middle_name,
                    'BirthDate'      => date("Y-m-d", strtotime($this->calc->birth_date)),
                    'ExperienceDate' => '2000-12-12',
                    'DriverDocument' => array(
                        'Seria'  => $this->calc->document_series,
                        'Number' => $this->calc->document_number,
                    ),
                    'IsResident'     => 1,
                ]
            ]

        ];

        return $params;
    }


    private function saveResult($response)
    {

        $xml = simplexml_load_string($response)->xpath('//soap:Body')[0]->CalcOsagoResponse;


        $coefficients = get_object_vars($xml->Coefficients);
        $policyKbm = get_object_vars($xml->PolicyKBM);

        $this->calc->update([
            'kbm' => $coefficients['Kbm'],
            'class_kbm' => $policyKbm[0],
        ]);

    }

}