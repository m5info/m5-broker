<?php

namespace App\Services\AuthAD;


use App\Models\Settings\SettingsSystem;
use App\Services\Front\api\KansaltingRIT_V2_1;
use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Mockery\Exception;

class LdapInfo
{
    protected
        $ldapConnected = false,
        $ldapServer = 'abook.msk-garant.ru',
        $ldapDn = "DC=msk-garant,DC=ru",
        $ldapUserDomain = 'msk-garant\\',
        // системный пользователь - для получения информации о любом пользователе без пароля
        $SYS_LOGIN = 'ldap_inet',
        $SYS_PWD = 'Gjhnfk_2019&';


    public function searchUser($login)
    {
        return $this->run($login, '', 'info');
    }

    public function getUser($login, $pwd)
    {
        $auth = $this->run($login, $pwd, 'auth');
        if($auth['result'] == true){
            return $this->run($login, $pwd, 'info');
        }
        return null;
    }

    public function run($login, $pwd, $action = 'auth') {

        $this->LOGIN = trim($login);
        $this->PWD = trim($pwd);
        $this->ACTION = $action;


        try {
            $this->getRequest();

            switch($this->ACTION) {
                // Проверка авторизации (логин-пароль) getCurrency
                case 'auth':
                    if (empty($this->LOGIN) || empty($this->PWD)) throw new Exception('EMPTY CREDS', 1);
                    $this->ldapConnect();


                    if($this->ldapBind($this->LOGIN, $this->PWD)){
                        $resp = array('result' => true);
                    }else{
                        $resp = array('result' => false);
                    }

                    break;
                // Информация о пользователе по логину
                case 'info':
                    if (empty($this->LOGIN)) throw new Exception('EMPTY LOGIN', 1);
                    $this->ldapConnect();
                    $this->ldapBind($this->SYS_LOGIN, $this->SYS_PWD);
                    $this->ldapSearch();
                    $it = $this->ldapEntries[0];

                    if($it){
                        $resp = array(
                            'result' => true,
                            'displayname' => $it['displayname'][0],
                            'title' => $it['title'][0],
                            'mail' => $it['mail'][0],
                            'department' => $it['department'][0],
                            'description' => $it['description'][0],
                        );
                    }else{
                        $resp = array('result' => false);
                    }

                    break;
                default:
                    $this->ACTION = 'Wrong action';
                    throw new Exception('Wrong action', 1);
            }
            if($this->ldapConnected) { $this->ldapClose(); }
            syslog(LOG_INFO, "[AD][{$this->ACTION}][ok][remote_addr={$this->REMOTE_ADDR} login={$this->LOGIN}]");
            // header('HTTP/1.1 200 OK', true); // ХЗ почему, но если включить, file_get_contents($uri) будет тормозить, препарация курлом curl --verbose не выявила различий
            return $resp;
        } catch (Exception $e) {
            if($this->ldapConnected) { $this->ldapClose(); }
            syslog(LOG_ERR, "[AD][{$this->ACTION}][error][remote_addr={$this->REMOTE_ADDR} login={$this->LOGIN}]: " . $e->getMessage());
            header('HTTP/1.1 403 Forbidden', true);
            die($e->getMessage());
        }
    }

    protected function getRequest() {

        $this->REMOTE_ADDR = trim($_SERVER['REMOTE_ADDR']);

    }

    // соединение с сервером
    protected function ldapConnect() {
        $this->ldapconn = ldap_connect($this->ldapServer);
        if($this->ldapconn === false) {
            throw new Exception('LDAP FAILED');
        }
        ldap_set_option($this->ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->ldapconn, LDAP_OPT_REFERRALS, 0);
        $this->ldapConnected = true;
    }

    protected function ldapBind($LOGIN, $PWD) {
        $this->ldapbind = ldap_bind($this->ldapconn, $this->ldapUserDomain . $LOGIN, $PWD);
        return $this->ldapbind;
    }

    protected function ldapSearch() {
        $filter = '(&(objectCategory=user)(samaccountname=' . $this->LOGIN.'))';
        $sr = ldap_search($this->ldapconn, $this->ldapDn, $filter);
        $this->ldapEntries = ldap_get_entries($this->ldapconn, $sr);
        if($this->ldapEntries['count'] != 1) $this->ldapEntries = null;//throw new Exception('TOO MANY RESULTS: ' . $this->ldapEntries['count'], 3);
    }


    protected function ldapClose() {
        ldap_close($this->ldapconn);
    }
}