<?php

namespace App\Console\Commands;

use Artisan;
use Illuminate\Console\Command;

class Cleaner extends Command{

    protected $signature = 'app:refresh';
    protected $description = 'All cached data refresh (Всё почистить и закешировать снова)';


    public function handle(){

        Artisan::call('cache:clear');
        Artisan::call('event:clear');
        Artisan::call('route:clear');
        Artisan::call('config:clear');
        Artisan::call('view:clear');
        Artisan::call('optimize:clear');


//        Artisan::call('route:cache');
//        Artisan::call('event:cache');
//        Artisan::call('config:cache');
//        Artisan::call('view:cache');
        Artisan::call('optimize');

    }
}
