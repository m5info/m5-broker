<?php

namespace App\Console\Commands;

use \Illuminate\Console\Command;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

use App\Classes\Socket\Pusher;
use React\EventLoop\Factory as ReactLoop;
use React\ZMQ\Context as ReactContext;
use React\Socket\Server as ReactServer;
use Ratchet\Wamp\WampServer;

class IntegrationServer extends Command
{

    protected $signature = 'integration_server:serve';
    protected $description = 'Pusher for Socket-server';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $loop = ReactLoop::create();

        $pusher = new Pusher;

        $context = new ReactContext($loop);

        $pull = $context->getSocket(\ZMQ::SOCKET_PULL);
        $pull->bind('tcp://'.config('app.pusher_integration_uri_inner').':'.config('app.pusher_integration_port'));
        $pull->on('message', [$pusher, 'broadcast']);

        $webSock = new ReactServer(config('app.integration_uri').':'.config('app.integration_port'), $loop);
        /*$webSock->listen(8081, '0.0.0.0');*/
        $webServer = new IoServer(
            new HttpServer(
                new WsServer(
                    new WampServer(
                        $pusher
                    )
                )
            ), $webSock
        );
        $this->info('Run handle');
        $loop->run();
    }
}