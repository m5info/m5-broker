<?php

namespace App\Models\BSO;

use App\Classes\Export\TagModels\BSO\TagBsoActs;
use App\Helpers\Visible;
use App\Models\Contracts\Payments;
use App\Models\Organizations\Organization;
use App\Models\Settings\PointsSale;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Settings\Organization
 *
 * @property integer $id
 * @property string $title
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereTitle($value)
 * @mixin \Eloquent
 * @property integer $next_act
 * @property string $default_purpose_payment
 * @property string $inn
 * @property float $limit_year
 * @property float $spent_limit_year
 * @property integer $is_actual
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereNextAct($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereDefaultPurposePayment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereInn($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereLimitYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereSpentLimitYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereIsActual($value)
 */
class BsoActs extends Model
{
    protected $table = 'bso_acts';

    protected $guarded = ['id'];

    public $timestamps = false;

    const TAG_MODEL = TagBsoActs::class;

    public function setItemsCarts($bso_cart_id)
    {
        $bso_items = BsoItem::where('bso_cart_id', $bso_cart_id)->get();

        $org_id = null;

        foreach ($bso_items as $bso)
        {
            BsoActsItems::create([
                'bso_act_id' => $this->id,
                'bso_id' => $bso->id,
                'bso_title' => $bso->bso_title,
            ]);
            if(!$org_id) $org_id = $bso->org_id;
        }

        if($org_id){
            $this->act_org_id = $org_id;
            $this->save();
        }

    }

    public function act_org(){
        return $this->hasOne(Organization::class, 'id', 'act_org_id');
    }

    public function user_from(){
        return $this->hasOne(User::class, 'id', 'user_id_from');
    }

    public function user_to(){
        return $this->hasOne(User::class, 'id', 'user_id_to');
    }

    public function bso_manager(){
        return $this->hasOne(User::class, 'id', 'bso_manager_id');
    }

    public function under_accepted(){
        return $this->hasOne(User::class, 'id', 'under_accepted_id');
    }


    public function bso_state(){
        return $this->hasOne(BsoState::class, 'id', 'bso_state_id');
    }

    public function logs(){
        return $this->hasMany(BsoLogs::class, 'bso_act_id', 'id')->groupby('bso_id')->with('bso','bso_state','bso_location');
    }

    public function point_sale(){
        return $this->hasOne(PointsSale::class, 'id', 'tp_id');
    }

    public function type(){
        return $this->hasOne(BsoCartType::class, 'id', 'type_id');
    }

    public function payments(){
        return $this->hasMany(Payments::class, 'realized_act_id', 'id')->where('payments.is_deleted', 0)->where('payments.type_id', 0);
    }

    public function bso_items(){
        return $this->hasMany(BsoItem::class, 'realized_act_id', 'id');
    }

    public function get_bsos(){
        return BsoItem::query()->whereIn('id', $this->logs->pluck('bso_id'));
    }

    public function courier(){
        return $this->hasOne(User::class, 'id', 'courier_id');
    }

    public function get_act_bso($field){

        $fields = [
            'acts_to_underwriting_id',
            'realized_act_id',
            'acts_sk_id',
            'acts_implemented_id',
            'acts_reserve_or_realized_id',
        ];

        $field = in_array($field, $fields) ? $field : current($fields);

        return $this->hasMany(BsoItem::class, $field, 'id');
    }


    public function get_link()
    {
        $link = url("/bso_acts/show_bso_act/{$this->id}/");
        if($this->type_id == 7){
            $link = url("/bso_acts/acts_implemented/details/{$this->id}/");
        }

        return $link;
    }

    public static function getActs(){
        $act = Visible::apply(BsoActs::query(), 'bso_acts', ['user_id_from', 'user_id_to', 'bso_manager_id']);
        return $act;

    }

    public static function getActId($id){
        $act = BsoActs::getActs()->with('logs');
        return $act->where('id', $id)->get()->first();
    }


    public static function getRealizedActs($user_id, $type_id = -1){

        $bso_act = BsoActs::whereIn('type_id', [4,7])
            ->where('user_id_from', $user_id)
            ->where('realized_state_id', 0);

        // 1 договор; 2 - БСО (Испорченые)
        if($type_id != -1){
            if($type_id == 1){
                $bso_state_id = 2;
            }elseif($type_id == 2){
                $bso_state_id = 3;
            }
            $bso_act->where('bso_state_id', $bso_state_id);
        }

        return $bso_act;

    }


    public static function createRealizedActs($user_id, $type_id){

        // 1 договор; 0 - БСО (Испорченые)
        $act_name = '';

        if($type_id == 1){
            $bso_state_id = 2;
            $act_name = 'Акт на проданные договора';
            $type_id = 4;
        }else{
            $bso_state_id = 3;
            $act_name = 'Акт на испорченые БСО';
            $type_id = 7;
        }

        $act_number = BsoActs::whereIn('type_id', [4,7])->count('id')+1;
        $act_title = 'P'.str_pad( $act_number, 6, '0', STR_PAD_LEFT );

        $time_create = date( 'Y-m-d H:i:s' );
        $target_date = date( 'Y-m-d H:i:s', strtotime( '+2 weekdays' ) );

        $bso_act = BsoActs::create([
            'type_id' => $type_id,
            'time_create' => $time_create,
            'target_date' => $target_date,
            'act_number' => $act_title,
            'act_number_int' => $act_number,
            'user_id_from' => $user_id,
            'bso_state_id' => $bso_state_id,
            'realized_state_id' => 0,
            'act_name' => $act_name,
            'bso_manager_id' => auth()->id(),

        ]);

        return $bso_act;
    }



    public static function createToUnderwritingAct($user_id){

        $last_act = BsoActs::where('type_id', 10)->get()->last();
        $act_name = 'Акт передачи БСО в Андеррайтинг';
        $act_number = $last_act ? $last_act->act_number_int + 1 : 1;
        $act_title = 'U' . str_pad($act_number, 6, '0', STR_PAD_LEFT);

        $time_create = date( 'Y-m-d H:i:s' );
        $target_date = date( 'Y-m-d H:i:s', strtotime( '+2 weekdays' ) );

        $bso_act = BsoActs::create([
            'type_id' => 10,
            'time_create' => $time_create,
            'target_date' => $target_date,
            'act_number' => $act_title,
            'act_number_int' => $act_number,
            'user_id_from' => auth()->id(),
            'bso_state_id' => 2,
            'realized_state_id' => 0,
            'act_name' => $act_name . " {$act_number}",
            'courier_id' => $user_id,
            'courier_target_date' => date('Y-m-d H:i:s', time())
        ]);

        return $bso_act;
    }


    public function setBsoRealizedActs($bso_arr, $bso_user_from){

        $bso_act = $this;

        foreach ($bso_arr as  $bso_id){
            $bso_log = BsoLogs::setLogs($bso_id, $bso_act->bso_state_id, 9, $bso_act->id, auth()->id());
        }


        $bsos = implode( ',', $bso_arr );
        \DB::update("update bso_items set realized_act_id='{$bso_act->id}' where id in ($bsos) && realized_act_id=0");

        $bso_act->actualsOrganizations();

        return $bso_act;

    }


    public function acceptRealizedActs()
    {
        $bso_act = $this;
        $items = [];
        if($bso_act->bso_state_id == 2){ //проданные договора
            $payments = $bso_act->payments;
            foreach ($payments as $payment){
                $items[] = $payment->bso_id;
                if($payment->bso_receipt_id > 0 ) $items[] = $payment->bso_receipt_id;
            }
        }

        if($bso_act->bso_state_id == 3){ //испорченые БСО
            $bsos = $bso_act->bso_items;
            foreach ($bsos as $bso){
                $items[] = $bso->id;
            }
        }

        $bso_act->acceptBSORealizedActs($items);

        return true;
    }


    public function deleteRealizedActs()
    {
        $bso_act = $this;

        if($bso_act->bso_state_id == 2){ //проданные договора
            $payments = $bso_act->payments;

            $items = [];
            foreach ($payments as $payment){
                $items[] = $payment->id;
            }

            if($items) $bso_act->deletePaymentRealizedActs($items);
        }

        if($bso_act->bso_state_id == 3){ //испорченые БСО
            $bsos = $bso_act->bso_items;

            $items = [];
            foreach ($bsos as $bso){
                $items[] = $bso->id;
            }

            if($items) $bso_act->deleteBSORealizedActs($items);
        }

        $bso_act->delete();

        return true;
    }

    public function deleteItemsRealizedActs($items)
    {
        $bso_act = $this;

        if($bso_act->bso_state_id == 2){ //проданные договора
            $bso_act->deletePaymentRealizedActs($items);
        }

        if($bso_act->bso_state_id == 3){ //испорченые БСО
            $bso_act->deleteBSORealizedActs($items);
        }


        return true;
    }

    public function deleteItemsToUnderwritingActs($item_array){

        BsoActsItems::query()->where('bso_act_id', $this->id)->whereIn('bso_id', $item_array)->delete();

        $payments = Payments::where('acts_to_underwriting_id', $this->id)->whereIn('id', $item_array)->get();
        foreach ($payments as $payment){
            BsoLogs::setLogs($payment->bso_id, $this->bso_state_id, 9, $this->id, auth()->id());
            $payment->update(['acts_to_underwriting_id' => 0]);
        }

        return true;
    }

    public function deleteToUnderwritingActs(){

        $act_items = BsoActsItems::query()->where('bso_act_id', $this->id);

        $payments = Payments::where('acts_to_underwriting_id', $this->id)->get();
        foreach ($payments as $payment){
            BsoLogs::setLogs($payment->bso_id, $this->bso_state_id, 9, $this->id, auth()->id());
            $payment->update(['acts_to_underwriting_id' => 0]);
        }

        $act_items->delete();
        $this->delete();
        return true;
    }


    public function deletePaymentRealizedActs($payments_arr){
        $bso_act = $this;

        $payments = implode( ',', $payments_arr );
        $payments_sql = \DB::select("select id, bso_receipt_id from payments where id in ($payments)");

        $bso = [];
        foreach ($payments_sql as $payment){
            $bso[] = $payment->id;
            if((int)$payment->bso_receipt_id > 0) $bso[] = $payment->bso_receipt_id;
        }

        $bso_act->deleteBSORealizedActs($bso);

        \DB::update("update payments set realized_act_id=0 where id in ($payments) and realized_act_id={$bso_act->id}");
        return true;
    }

    public function deleteBSORealizedActs($bso_arr){
        $bso_act = $this;

        foreach ($bso_arr as  $bso_id){
            $bso_log = BsoLogs::setLogs($bso_id, $bso_act->bso_state_id, 7, $bso_act->id, auth()->id());
        }

        $bsos = implode( ',', $bso_arr );
        \DB::update("update bso_items set realized_act_id='0' where id in ($bsos) && realized_act_id={$bso_act->id}");

        return true;
    }


    public function acceptBSORealizedActs($bso_arr){

        $bso_act = $this;
        $user_id = auth()->id();
        $user_org_id = (int)auth()->user()->organization_id;

        if(is_array($bso_arr) && count($bso_arr) > 0){

            foreach ($bso_arr as  $bso_id){
                $bso_log = BsoLogs::setLogs($bso_id, $bso_act->bso_state_id, 4, $bso_act->id, auth()->id());
            }

            $bsos = implode( ',', $bso_arr );
            $sql = "update bso_items set transfer_to_org_time=now(), location_id = 4, user_id = {$user_id}, user_org_id={$user_org_id} where id in ({$bsos}) ";

            \DB::update($sql);
        }

        $bso_act->user_id_to = $user_id;
        $bso_act->realized_state_id = 1;
        $bso_act->save();
        return true;

    }

    public function getDateRuAttribute(){
        return setDateTimeFormatRu($this->time_create);
    }


    public function actualsOrganizations()
    {
        $org_id = null;

        if($this->logs){
            foreach ($this->logs as $log){
                $org_id = $log->bso->org_id;
                break;
            }
        }

        if($org_id){
            $this->act_org_id = $org_id;
            $this->save();
            return true;
        }

        return false;
    }


}
