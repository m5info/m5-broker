<?php

namespace App\Models\BSO;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;

class BsoCheckDocuments extends Model
{

    const FILES_DOC = 'bso/bso_statuses_check';

    protected $table = 'bso_check_documents';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function file()
    {
        return $this->hasOne(File::class, 'id', 'file_id');
    }
}
