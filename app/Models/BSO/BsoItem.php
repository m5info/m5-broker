<?php

namespace App\Models\BSO;

use App\Classes\Export\TagModels\BSO\TagBsoItem;
use App\Models\Acts\ReportAct;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\Products;
use App\Models\Directories\TypeBso;
use App\Models\File;
use App\Models\Organizations\Organization;
use App\Models\Settings\PointsSale;
use App\Models\User;
use App\Traits\Models\ActiveConstTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BsoItem extends Model{

    use ActiveConstTrait;

    const FILES_DOC = 'bso/';
    const TAG_MODEL = TagBsoItem::class;

    protected $table = 'bso_items';

    protected $guarded = ['id'];

    public $timestamps = false;


    const ACT_STATE = [
        -2 => 'Реестр будущий',
        -1 => 'Реестр текущий',
         0 => 'Реестр корзина'
    ];


    public function transfer(){
        return $this->hasOne(BsoTransfer::class, 'id', 'transfer_id');
    }

    public function supplier(){
        return $this->hasOne(BsoSuppliers::class, 'id', 'bso_supplier_id');
    }

    public function supplier_org(){
        return $this->hasOne(Organization::class, 'id', 'org_id');
    }

    public function type(){
        return $this->hasOne(BsoType::class, 'id', 'type_bso_id');
    }

    public function point_sale(){
        return $this->hasOne(PointsSale::class, 'id', 'point_sale_id');
    }

    public function bso_locations(){
        return $this->hasOne(BsoLocations::class, 'id', 'location_id');
    }

    public function bso_states(){
        return $this->hasOne(BsoState::class, 'id', 'state_id');
    }

    public function product(){
        return $this->hasOne(Products::class, 'id', 'product_id');
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id')->with('curator');
    }

    public function agent(){
        return $this->hasOne(User::class, 'id', 'agent_id');
    }

    public function acts(){
        return $this->hasMany(BsoActsItems::class, 'bso_id', 'id');
    }

    public function comments(){
        return $this->hasMany(BsoComments::class, 'bso_id', 'id');
    }
    public function act_sk(){
        return $this->hasOne(ReportAct::class, 'id', 'acts_sk_id');
    }

    public function logs($direction = 'asc'){
        return $this->hasMany(BsoLogs::class, 'bso_id', 'id')
            ->groupby('log_time')
            ->groupby('bso_act_id')
            ->orderBy("log_time", $direction);
    }

    public function cars(){
        return $this->hasOne(BsoCarts::class, 'id', 'bso_cart_id');
    }

    public function contract_receipt(){
        return $this->hasOne(Contracts::class, 'id', 'contract_id');
    }

    public function contract(){
        return $this->hasOne(Contracts::class, 'bso_id', 'id');
    }

    public function payments(){
        return $this->hasMany(Payments::class, 'bso_id', 'id')->where('statys_id', '>=', 0);
    }

    public function paymentsReceipt(){
        return $this->hasMany(Payments::class, 'bso_receipt_id', 'id')->where('statys_id', '>=', 0);
    }

    public function all_payments(){
        return $this->hasMany(Payments::class, 'bso_id', 'id');
    }

    public function scan(){
        return $this->hasOne(File::class, 'id', 'file_id');
    }

    public function location(){
        return $this->hasOne(BsoLocations::class, 'id', 'location_id');
    }

    public function state(){
        return $this->hasOne(BsoState::class, 'id', 'state_id');
    }

    public function insurance(){
        return $this->hasOne(InsuranceCompanies::class, 'id', 'insurance_companies_id');
    }


    public function bso_serie(){
        return $this->hasOne(BsoSerie::class, 'id', 'bso_serie_id');
    }

    public function time_on_agent(){
        return ceil((time() - strtotime($this->transfer_to_agent_time))/60/60/24);
    }

    public function time_on_stock(){
        return ceil((time() - strtotime($this->time_create))/60/60/24);
    }


    public static function update_to_inital($ids){

        $update_data = [
            'transfer_id' => 0,
            'location_id' => 0,
            'user_org_id' => 0,
            'user_id' => 0,
            'org_id' => 0,
        ];

        self::query()->whereIn('id', $ids)->update($update_data);

        return true;

    }


    public static function getElectronicBso(User $agent, BsoSuppliers $bso_supplier, TypeBso $bso_type, BsoSerie $bso_serie, $bso_number_from){

        //Проверяем есть ли такой договор
        $bso = BsoItem::where('bso_supplier_id', $bso_supplier->id)
            ->where('insurance_companies_id', $bso_supplier->insurance_companies_id)
            ->where('bso_class_id', $bso_serie->bso_class_id)
            ->where('product_id', $bso_serie->product_id)
            ->where('bso_serie_id', $bso_serie->id)
            ->where('bso_number', $bso_number_from)->get()->first();


        if($bso)
        {
            //Если есть проверяем статус если чистый на этом же агенте то отдаем
            if($bso->agent_id == $agent->id && (int)$bso->state_id == 0 && ((int)$bso->location_id == 0 || (int)$bso->location_id == 1)) return $bso;
            else return null;
        }else{

            //Создаем БСО и вешаем на Агента

            $bso = BsoItem::create([
                'bso_supplier_id' => $bso_supplier->id,
                'org_id' => $bso_supplier->purpose_org_id,
                'insurance_companies_id' => $bso_supplier->insurance_companies_id,
                'bso_class_id' => $bso_serie->bso_class_id,
                'type_bso_id' => $bso_type->id,
                'product_id' => $bso_serie->product_id,
                'bso_serie_id' => $bso_serie->id,
                'bso_number' => $bso_number_from,
                'bso_title' => $bso_serie->bso_serie.' '.$bso_number_from,
                'location_id' => 1,
                'state_id' => 0,
                'agent_id' => $agent->id,
                'user_id' => $agent->id,
                'user_org_id' => $agent->organization->id,
                'time_create' => date('Y-m-d H:i:s'),
                'time_target' => date('Y-m-d H:i:s'),
                'last_operation_time' => date('Y-m-d H:i:s'),
                'transfer_to_agent_time' => date('Y-m-d H:i:s'),
                'bso_manager_id' => (int)auth()->id(),
                'point_sale_id' => (int)$agent->point_sale_id,
            ]);

            $bso->setBsoLog(1, 0, 0);


        }

        return $bso;

    }


    public static function getBso($user_type = 'agent_id'){
        $bsos = BsoItem::query();

        //Проверка на доступы
        $user = auth()->user();
        $visibility_obj = $user->role->rolesVisibility(4)->visibility;


        if($visibility_obj == 0){ //Все

        }elseif ($visibility_obj == 1){//Все в рамках организации

            $bsos->where("bso_items.user_org_id", $user->organization_id);

        }elseif ($visibility_obj == 2){//Только свои
            $bsos->where("bso_items.{$user_type}", $user->id);

        }elseif ($visibility_obj == 3){//Только свои и своих подчененных
            $bsos->whereIn("bso_items.{$user_type}", $user->getAllSubUsersIds());

            /*$bsos->whereIn("bso_items.{$user_type}", function($query) use ($user)
            {
                $query->select(\DB::raw('users.id'))
                    ->from('users')
                    ->where('parent_id', '=', auth()->id())
                    ->orWhere('curator_id', '=', auth()->id())
                    ->orWhere('id', '=', auth()->id())
                    ->orWhere('path_parent', 'like', "%:".auth()->id().":%");

            });*/

        }

        return $bsos;
    }


    public static function getBsoSold(){
        $bsos = BsoItem::query()->leftJoin('contracts', 'contracts.bso_id', '=', 'bso_items.id');

        //Проверка на доступы
        $user = auth()->user();
        $visibility_obj = $user->role->rolesVisibility(4)->visibility;


        if($visibility_obj == 0){ //Все

        }elseif ($visibility_obj == 1){//Все в рамках организации

            $bsos->where("bso_items.user_org_id", $user->organization_id);

        }elseif ($visibility_obj == 2){//Только свои

            $bsos->where(function($query) use ($user)
            {
                $query->where('contracts.agent_id', 'like', "%{$user->id}%")
                    ->orWhere('contracts.manager_id', 'like', "%{$user->id}%");
            });


        }elseif ($visibility_obj == 3){//Только свои и своих подчененных

            $bsos->where(function($query) use ($user)
            {
                $query->where('contracts.agent_id', 'like', "%{$user->id}%")
                    ->orWhere('contracts.manager_id', 'like', "%{$user->id}%")
                    ->orWhere('contracts.parent_agent_id', 'like', "%{$user->id}%");
            });

        }

        $bsos->select(['bso_items.*']);

        return $bsos;
    }


    public static function getBsoId($id){
        $bsos = BsoItem::getBso();

        $bsos->where('id', $id);
        return $bsos->get()->first();
    }


    public function setBsoLog($location_id, $postpone = 0, $state_id = -1)
    {
        if($state_id == -1) $state_id = $this->state_id;

        BsoLogs::setLogs(
            $this->id,
            $state_id,
            $location_id,
            0,
            auth()->id(),
            0,
            0,
            0,
            $postpone
        );

        switch($location_id){
            case 2:
                $this->update(['transfer_to_sk_time' => date('Y-m-d H:i:s')]);
                break;
        }
    }

    public function getTableTdColoredAttribute(){

        if($this->location_id != 2 && $this->location_id != 1){
            $days = $this->type->day_sk;
            $time = strtotime($this->time_create);

         }

        else if($this->location_id == 1){

            $days = $this->type->day_agent;
            $time = strtotime($this->transfer_to_agent_time);
        }

        $leftTime =  (time() - $time)/ (3600 * 24);
        $leftTime = ceil($leftTime);
        if($leftTime > $days)
            return 'true';

    }

    public function getColoredColAttribute (){
        if(count($this->type_bso_id) > $this->type->min_yellow)
            return 'yellow';
        elseif(count($this->type_bso_id) > $this->type->min_red)
            return 'red';
        return true;
    }


    public function saveDataBeforeRemoving()
    {
        return RemovedBsoItems::create([
            'bso_item_id' =>  $this->id,
            'removed_user_id' =>  auth()->id(),
            'old_data' =>  \GuzzleHttp\json_encode($this->getAttributes())
        ]);
    }

    public static function getSupplierBsoTypeList($supplier_id, $withLostAndCorrupted = false)
    {
        $query = self::select('type_bso.title', DB::raw('count(bso_items.id) as count'))
                    ->leftJoin('type_bso', 'bso_items.type_bso_id', 'type_bso.id')
                    ->where('bso_items.bso_supplier_id', $supplier_id);

        if(!$withLostAndCorrupted) {
            $query->whereNotIn('bso_items.state_id', [4, 3]);
        }
            $query->groupBy('type_bso.title');

        return $query;
    }

}
