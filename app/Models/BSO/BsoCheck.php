<?php

namespace App\Models\BSO;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;

class BsoCheck extends Model
{
    const FILES_DOC = 'bso/bso_statuses_check';

    protected $table = 'bso_check';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function documents()
    {
        return $this->belongsToMany(File::class, 'bso_check_documents', 'bso_check_id', 'file_id');
    }
}
