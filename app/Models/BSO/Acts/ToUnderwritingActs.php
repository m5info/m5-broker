<?php
namespace  App\Models\BSO\Acts;

use App\Classes\Export\TagModels\BSO\Acts\TagToUnderwritingActs;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Scopes\BSO\Acts\ToUnderwritingActsScope;
use App\Models\BSO\BsoActs;

class ToUnderwritingActs extends BsoActs{

    public $table = 'bso_acts';
    const TAG_MODEL = TagToUnderwritingActs::class;

    protected static function boot() {
        parent::boot();
        static::addGlobalScope(new ToUnderwritingActsScope());
    }


    public function payments(){
        return Payments::query()->where('acts_to_underwriting_id', $this->id);
    }

}