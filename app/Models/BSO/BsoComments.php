<?php

namespace App\Models\BSO;

use Illuminate\Database\Eloquent\Model;

class BsoComments extends Model
{

    protected $table = 'bso_comments';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function bso()
    {
        return $this->hasOne(BsoItem::class, 'id', 'bso_id');
    }
}
