<?php

namespace App\Models\BSO;

use App\Models\Contracts\Contracts;
use App\Models\Contracts\RemovedContracts;
use App\Models\Contracts\RemovedPayments;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class RemovedBsoItems extends Model
{
    protected $table = 'removed_bso_items';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function removed_user(){
        return $this->hasOne(User::class, 'id', 'removed_user_id');
    }

    public function removed_contract(){
        return $this->hasOne(RemovedContracts::class, 'removed_bso_item_id', 'id');
    }

    public function removed_payments(){
        return $this->hasMany(RemovedPayments::class, 'removed_bso_item_id', 'id');
    }

    public function bso_item(){
        return $this->hasOne(BsoItem::class, 'id', 'bso_item_id');
    }


    /**
     * Восстановление всех параметров бсошки исходя из сохраненных данных
     */
    public function restoreBsoItem()
    {
        $removedBsoItem = $this;
        $bsoItem = $removedBsoItem->bso_item;

        if($bsoOldData = $removedBsoItem->old_data){
            $bsoOldData = \GuzzleHttp\json_decode($bsoOldData);

            foreach($bsoOldData as $column => $value){
                $bsoItem->{$column} = $value;
            }

            $bsoItem->save();
        }
    }

    /**
     * Чистка записей об удаленных данных
     */
    public function destroyAllRemoved()
    {
        $removedBsoItem = $this;
        $removedContract = $removedBsoItem->removed_contract;
        $removedPayments = $removedBsoItem->removed_payments;

        if($removedContract){
            $removedContract->delete();
        }

        if($removedPayments){
            foreach($removedPayments as $removedPayment){
                $removedPayment->delete();
            }
        }

        $this->delete();
    }
}
