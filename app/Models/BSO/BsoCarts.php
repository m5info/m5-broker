<?php

namespace App\Models\BSO;

use App\Classes\Export\TagModels\BSO\TagBsoCarts;
use App\Models\Settings\PointsSale;
use App\Models\User;
use App\Traits\Models\ActiveConstTrait;
use Illuminate\Database\Eloquent\Model;

class BsoCarts extends Model
{

    use ActiveConstTrait;

    public $table = 'bso_carts';

    protected $guarded = ['id'];

    public $timestamps = false;

    const TAG_MODEL = TagBsoCarts::class;


    const STATE_CAR = [
        0 => 'В работе',
        1 => 'Передан агенту',
    ];


    public function bso_items(){
        return $this->hasMany(BsoItem::class, 'bso_cart_id', 'id');
    }


    public function user_to(){
        return $this->hasOne(User::class, 'id', 'user_id_to');
    }

    public function user_from(){
        return $this->hasOne(User::class, 'id', 'user_id_from');
    }


    public function bso_manager(){
        return $this->hasOne(User::class, 'id', 'bso_manager_id');
    }

    public function bso_act(){
        return $this->hasOne(BsoActs::class, 'bso_cart_id', 'id');
    }

    public function tp_bso_manager(){
        return $this->hasOne(User::class, 'id', 'tp_bso_manager_id');
    }

    public function point_sale(){
        return $this->hasOne(PointsSale::class, 'id', 'tp_id');
    }

    public function curr_point_sale(){
        return $this->hasOne(PointsSale::class, 'id', 'tp_id');
    }

    public function courier(){
        return $this->hasOne(User::class, 'id', 'courier_id');
    }

    public function type(){
        return $this->hasOne(BsoCartType::class, 'id', 'bso_cart_type');
    }

    public static function getCars(){
        $act = BsoCarts::query()->with('user_to','user_from','bso_manager','point_sale','type','courier');

        //Проверка доступа

        //Проверка доступа
        if(auth()->user()->role->rolesVisibility(5)->visibility == 0){

        }elseif(auth()->user()->role->rolesVisibility(5)->visibility == 3) {

            //$act->whereIn('user_id_from', auth()->user()->hasEmployee()->select('users.id')->get()->toArray());

            $act->where(function ($query) {
                $query->whereIn('user_id_from', auth()->user()->hasEmployee()->select('users.id')->get()->toArray())
                    ->orWhereIn('user_id_to',  auth()->user()->hasEmployee()->select('users.id')->get()->toArray())
                    ->orWhereIn('bso_manager_id', auth()->user()->hasEmployee()->select('users.id')->get()->toArray());
            });


        }else{

            //Видимость на всю организацию и свои СДЕЛАТЬ

            //Видимость только на свои

            $act->where(function ($query) {
                $query->where('user_id_from', auth()->id())
                    ->orWhere('user_id_to', auth()->id())
                    ->orWhere('bso_manager_id', auth()->id());
            });

        }

        return $act;

    }

    public static function getCarsId($id){
        $act = BsoCarts::getCars();
        return $act->where('id', $id)->get()->first();
    }

}
