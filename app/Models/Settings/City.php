<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'citys';

    protected $guarded = ['id'];

    public $timestamps = false;


}
