<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;


class DeparturesCourierPrice extends Model
{
    protected $table = 'departures_courier_price';

    protected $guarded = ['id'];

    public $timestamps = false;


    public function city() {
        return $this->hasOne(City::class, 'id', 'city_id');
    }


    public static function getDeparturesCourierPrice($city_id)
    {
        $result = DeparturesCourierPrice::where('is_actual', 1)->whereIn('city_id', [0, $city_id]);
        return $result->get();
    }


}
