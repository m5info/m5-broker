<?php

namespace App\Models\Settings;

use App\Models\BSO\BsoLogs;
use App\Models\Contracts\ContractsLogs;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    public $table = 'notifications';
    public $guarded = ['id'];
    public $timestamps = false;


    public function bso_log(){
        return $this->hasOne(BsoLogs::class, 'id', 'bso_log_id');
    }

    public function contract_log(){
        return $this->hasOne(ContractsLogs::class, 'id', 'contract_log_id');
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function notifier(){


        $config = [
            [
                'notifier' => BsoLogs::NOTIFIER,
                'related' => $this->bso_log
            ],
            [
                'notifier' => ContractsLogs::NOTIFIER,
                'related' => $this->contract_log
            ],
        ];


        $notifier_config = $config[array_search(true, array_column($config, 'related'))];

        return new $notifier_config['notifier']($this);
    }

    public static function sendNotificationsMsg($user_id, $msg)
    {
        Notification::create([
            'user_id' => $user_id,
            'is_read' => 0,
            'msg' => $msg,
        ]);
        return true;
    }

}