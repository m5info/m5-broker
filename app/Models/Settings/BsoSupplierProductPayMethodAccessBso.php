<?php

namespace  App\Models\Settings;

use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\Products;
use App\Models\Finance\PayMethod;
use Illuminate\Database\Eloquent\Model;

class BsoSupplierProductPayMethodAccessBso extends Model{


    public $table = 'bso_suppliers_products_pay_methods_access_bso';
    public $timestamps = false;
    public $guarded = [];

}