<?php

namespace App\Models\Settings;

use App\Models\Directories\ProductsPrograms;
use Illuminate\Database\Eloquent\Model;

class ProgramsOnlineList extends Model
{
    protected $table = 'programs_online_list';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function program(){
        return $this->hasOne(ProductsPrograms::class, 'id', 'program_id');
    }

}
