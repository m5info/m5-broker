<?php

namespace App\Models\Settings;

use App\Models\BSO\BsoItem;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Settings\Organization
 *
 * @property integer $id
 * @property string $title
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereTitle($value)
 * @mixin \Eloquent
 * @property integer $is_actual
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Bank whereIsActual($value)
 */
class PointsSale extends Model
{
    protected $table = 'points_sale';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function city() {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function bso_items(){
        return $this->hasMany(BsoItem::class, 'point_sale_id', 'id')->where('transfer_id', '=', '0');
    }
}
