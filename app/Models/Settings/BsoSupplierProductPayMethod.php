<?php

namespace  App\Models\Settings;

use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\Products;
use App\Models\Finance\PayMethod;
use Illuminate\Database\Eloquent\Model;

class BsoSupplierProductPayMethod extends Model{


    public $table = 'bso_suppliers_products_pay_methods';
    public $timestamps = false;
    public $guarded = [];


    public function bso_supplier(){
        return $this->hasOne(BsoSuppliers::class, 'id', 'bso_supplier_id');
    }

    public function product(){
        return $this->hasOne(Products::class, 'id', 'product_id');
    }

    public function pay_method(){
        return $this->hasOne(PayMethod::class, 'id', 'pay_method_id');
    }


}