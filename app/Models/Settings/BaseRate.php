<?php

namespace App\Models\Settings;

use App\Models\Directories\InsuranceCompanies;
use Illuminate\Database\Eloquent\Model;

class BaseRate extends Model
{
    protected $table = 'base_rate';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function company()
    {
        return $this->hasOne(InsuranceCompanies::class, 'id', 'company_id');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public static function getBaseRateTitle($city_id, $company_id)
    {
        $title = 'Не указано';
        $base_rate = self::getBaseRate($city_id, $company_id);
        if($base_rate) $title = titleFloatFormat($base_rate->rate);

        return $title;
    }

    public static function getBaseRate($city_id, $company_id)
    {
        $base_rate = BaseRate::where('city_id', $city_id)->where('company_id', $company_id)->get()->first();
        return $base_rate;
    }


}
