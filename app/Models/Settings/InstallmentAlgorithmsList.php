<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class InstallmentAlgorithmsList extends Model
{

    protected $table = 'installment_algorithms_list';

    protected $guarded = ['id'];

    public $timestamps = false;
}
