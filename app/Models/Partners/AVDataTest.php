<?php

namespace App\Models\Partners;

use Illuminate\Database\Eloquent\Model;
use App\Models\Partners\AVData;

class AVDataTest extends Model
{
    public function test()
    {
        $av = new AVData();
        $result = [];
        $cases = $this->getCases();

        array_walk($cases, function($value) use (&$result, &$av) {
            $result[] = $av->filterData($value) ? $av->filterData($value) : $value;
            //$result[] = $value["result"]  ?? false;
        });
        return $result;
    }

    public function getCases()
    {
        $data = AVData::all()->pluck('data')->toArray();
        $result = [];
        array_walk($data, function($value) use (&$result) {
            $result[] = json_decode($value, true);
        });
        return $result;
    }

    public function test2()
    {
        $av = new AVData();
        return $av->getTranslation();
    }
}
