<?php

namespace App\Models\Partners;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class IntegrationPartnersTokens extends Model
{
    protected $table = 'integration_partners_tokens';

    protected $fillable = ['partner_id', 'token'];

    private static $current_token;


    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public static function make($partner_id, $token, $privateKey)
    {
        $token = self::create([
            'partner_id' => $partner_id,
            'token' => $token
        ]);

        $token->hash = Hash::make($privateKey);

        return $token->save();
    }

    public function validate($token, $privateKey)
    {
        self::$current_token = $this;
        return (($token === $this->token) && Hash::check($privateKey, $this->hash));
    }

    public static function current()
    {
        return self::$current_token;
    }
}
