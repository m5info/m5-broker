<?php

namespace App\Models\Partners;

use App\Models\Partners\Dadata;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;
use Illuminate\Database\Eloquent\Model;

class VehicleMapper extends Model
{
    protected $table = "vehicle_mapper";

    public static function getCarByGosnum($gos_number) : array
    {
        $car = Cars::where('gosnum', $gos_number)->first();

        $local_brand = self::getLocalBrand($car->brand);

        $local_model = false;

        $result = [];

        if($local_brand){
            $local_model = self::getLocalModel($car->model, $local_brand->id);
            $result['brand'] = $local_brand->id;
        }

        if($local_model){
        $result['model'] = $local_model->id;
        }


        return $result;
    }


    public static function getCarByGosnum2($car) : array
    {

        $local_brand = self::getLocalBrand($car->mark);

        $local_model = false;

        $result = [];

        if($local_brand){
            $local_model = self::getLocalModel($car->model, $local_brand->id);
            $result['brand'] = $local_brand->id;
        }

        if($local_model){
            $result['model'] = $local_model->id;
        }


        return $result;
    }

    public static function getLocalBrand($partner_brand)
    {
        $local_brand = VehicleMarks::where('title', $partner_brand)->first();
        if(!$local_brand) {
            $local_brand_name = self::where('partner_name', $partner_brand)->first();
            if(!$local_brand_name) {
                $local_name = self::dadataBrand($partner_brand);
            } else {
                $local_name = $local_brand_name->local_name;
            }
            $local_brand = VehicleMarks::where('title', $local_name)->first();
        }

        return $local_brand;
    }

    public static function getLocalModel($partner_model, $mark_id)
    {
        if(!$local_model = VehicleModels::where('title', $partner_model)->where('mark_id', $mark_id)->first()) {

            $local_model_name = self::where('partner_name', $partner_model)->first();
            if(!$local_model_name)
                return false;
            $local_model = VehicleModels::where('title', $local_model_name->local_name)->where('mark_id', $mark_id)->first();
        }

        return $local_model;
    }

    public static function dadataBrand($str)
    {
        $dd = new Dadata();
        return $dd->check_simple($str);
    }

    public static function getBrandById($id){
        return VehicleMarks::where('id', $id)->first();
    }

    public static function getModelById($id){
        return VehicleModels::where('id', $id)->first();
    }
}
