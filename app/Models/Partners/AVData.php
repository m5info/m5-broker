<?php

namespace App\Models\Partners;

use App\Models\Contracts\ContractsCalculation;
use App\Models\Partners\Dadata;
use App\Models\Partners\Notifications;
use Illuminate\Database\Eloquent\Model;
use App\Services\AVDataInfo\IntegrationAVDataInfo;
use App\Models\Vehicle\VehicleMarks;
use PhpParser\Builder\Class_;
use App\Models\Partners\Leads;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AVData extends Model
{
    protected $table = "partners_av_data";
    private $gosnum_param = "";

    public function getByGosnum($val)
    {
        $this->gosnum_param = $val;
        return $this->getFromDB() ? $this->getFromDB() : $this->getFromIntegration();
    }

    private function getFromDB()
    {
        return self::where('gosnum', $this->gosnum_param)->where('created_at', '>', date('Y-m-d', strtotime("-1 day")))->first();
    }

    private function getFromIntegration()
    {
        $source = new IntegrationAVDataInfo();
        $data = $source->getApiGosNomerReporst($this->gosnum_param);
        //Ничего не сохраняем и выходим если ошибка
        if($data->error == true && $data->error_msg == 'Некорректный госномер')
            return 'Некорректный госномер';
        //Ничего не сохраняем и выходим если ошибка
        if($data->error == true){
            Notifications::error($data->error_msg);
            return false;
        }

        $item = new AVData();
        $item->gosnum = $this->gosnum_param;
        $item->data = json_encode($data);
        $item->save();

        return $item;
    }

    public function filterData($object)
    {
        //базовая проверка на ошибки
        if(!$object || $object['error'] || !isset($object['result']))
            return false;

        $Marka = $this->getAvBrand($object);

        //Пробуем достать через dadata и русский перевод
        if(!$Marka)
            $Marka = $this->getAvCar($object);

        //Если марки нет - значит информация некорректна
        if(!$Marka)
            return false;

        $Model = $this->getAvModel($object);

        /** ВЛАДЕЛЕЦ */

        $Family = '';       //  Фамилия
        $Name = '';     //  Имя
        $NameTwo = '';      //  Отчество
        $Phone = '';        //  Телефон

        /** АВТО */

        $VIN = '';      //  VIN
        $GRZ = '';      // Номер авто
        // подставить гос номер из запроса и дальше сравнить (может понадобится проверка на валидность)
        $Year = '';     //  Год выпуска
        $PTS = '';      //  ПТС не всегда правильная
        $PTS_region = '';       //  ПТС с регионом (правильная, не всегда есть)
        $PTS_date = '';     //  Дата выдвчи ПТС
        $STS = '';      //  СТС
        $STS_date = '';     //  Дата выдачи СТС
        $Power = '';        //  Мощность л.с.
        $Volume = '';       // Объем двигателя
        $NumDiag = '';      // Номер диагностической карты
        $DiagDate = '';     //  Когда делали диагностику
        $DiagDateExpire = '';     //  Когда заканчивается диагностика


        $objGIBDD = $object['result']['gibdd'];
        $objEAISTO = $object['result']['eaisto'];
        if($objGIBDD != NULL) {

            foreach ($objGIBDD as $key => $val) {

                if (strcmp($val['Family'], '') != 0 && strlen($Family) == 0) {
                    $Family = $val['Family'];
                }
                if (strcmp($val['Name'], '') != 0 && strlen($Name) == 0) {
                    $Name = $val['Name'];
                }
                if (strcmp($val['Patronim'], '') != 0 && strlen($NameTwo) == 0) {
                    $NameTwo = $val['Patronim'];
                }
                if (strcmp($val['phone'], '') != 0 && strlen($Phone) == 0) {
                    $Phone = $val['phone'];
                }
                if (strcmp($val['vin'], '') != 0 && strlen($VIN) == 0) {
                    $VIN = $val['vin'];
                }
                if (strcmp($val['carYear'], '') != 0 && strlen($Year) == 0) {
                    $Year = $val['carYear'];
                }
                if (strcmp($val['Pts'], '') != 0 && strlen($PTS) == 0) {
                    $PTS = $val['Pts'];
                }
                if (strcmp($val['Sts'], '') != 0 && strlen($STS) == 0) {
                    $STS = $val['Sts'];
                }

                if ($val['vininfo'] != NULL) {

                    if (strcmp($val['vininfo']['reghistory']['year'], '') != 0 && ($val['vininfo']['reghistory']['year'] != -1)  && strlen($Year) == 0) {
                        $Year = $val['vininfo']['reghistory']['year'];
                    }
                    if (strcmp($val['vininfo']['reghistory']['enginePower'], '') != 0 && strlen($Power) == 0) {
                        $Power = $val['vininfo']['reghistory']['enginePower'];
                    }
                    if (strcmp($val['vininfo']['reghistory']['engineVolume'], '') != 0 && strlen($Volume) == 0) {
                        $Volume = $val['vininfo']['reghistory']['engineVolume'];
                    }
                    if (strcmp($val['vininfo']['reghistory']['pts'], '') != 0 && $val['vininfo']['reghistory']['pts'] != NULL && strlen($PTS_region) == 0) {
                        $PTS_region = $val['vininfo']['reghistory']['pts'];
                    }
                }
            }       //  Бежим по инфо из ГИБДД 1-2 цикла
            //УТОЧНЕНИЕ: нет, это плохая идея, т.к. владельцы представлены в порядке от текущего к более ранним
            //соотв. берем инфу от первого

            // видимо не совсем плохая;) проверяем, если уже есть инфа, то не пишем
        }

        if($objEAISTO != NULL) {
            foreach ($objEAISTO as $key => $val) {

                if (strcmp($val['vin'], '') != 0 && strlen($VIN) == 0)
                    $VIN = $val['vin'];

                if (strcmp($val['year'], '') != 0 && ($val['year'] != -1) && strlen($Year) == 0)
                    $Year = $val['year'];

                if(strlen($NumDiag) == 0)
                    $NumDiag = $val['num'];

                if(strlen($DiagDate) == 0)
                    $DiagDate = date('d.m.Y', $val['date']);

                if(strlen($DiagDateExpire) == 0)
                    $DiagDateExpire = date('d.m.Y', $val['dateexpire']);

                $GRZ = $val['gosnumber'];  // тут проверить
            }       //  Бежим по инфо из бюро диагностики
        }

        /** ЗАПОЛНЕНИЕ ДАННЫХ */

            $Model = strtoupper($Model);        //  Поднимаем буквы в модели авто
            if(strlen($PTS) > 12){
                $PTS_date = substr($PTS, -10);      // получаем дату выдачи птс
            }
            $PTS_Serial = substr($PTS_region, 0, 6);        // получаем серию ПТС
            $PTS_Number = substr($PTS_region, 6, 6);        //  Получаем номер ПТС
            $PTS_Full = $PTS_Serial.' '.$PTS_Number;        //  Клеим Серию и номер
            //$PTS_Full = substr_replace($PTS_region, ' ', 6, 0);       //  Если не нужно по отельности серия и номер раскоментировать. три верхнии закоментировать.


            $INFO = new \stdClass();

            /**  СОБСТВЕННИК */

            $INFO->family = $Family;
            $INFO->name = $Name;
            $INFO->nametwo = $NameTwo;
            $INFO->phone = $Phone;

            /**  АВТО */

            $INFO->vin = $VIN;
            $INFO->grz = $GRZ;
            $INFO->mark = $Marka;
            $INFO->model = $Model;
            $INFO->year = $Year;
            $INFO->pts_serial = $PTS_Serial;
            $INFO->pts_number = $PTS_Number;
            $INFO->docserie = $PTS_Full;
            $INFO->pts_date = $PTS_date;
            $INFO->sts = $STS;
            $INFO->sts_date = $STS_date;
            $INFO->power = $Power == 0 ? '' : $Power;
            $INFO->volume = $Volume;
            $INFO->dk_number = $NumDiag;
            $INFO->dk_date_from = $DiagDate;
            $INFO->dk_date = $DiagDateExpire;

            return $INFO;  // вернуть объект

    }

    public function getDataFromJson()
    {
        return json_decode($this->data, true);
    }

    public function carData()
    {
        return $this->getDataFromJson()->gibdd[0];
    }

    public function eaisto()
    {
        return $this->getDataFromJson()->eaisto[0];
    }

    public function getYearAttribute()
    {
        return $this->carData()->carYear;
    }

    public function getPowerAttribute()
    {
        return $this->carData()->vininfo->reghistory->enginePower;
    }

    public function getVinAttribute()
    {
        return $this->carData()->vin;
    }

    public function getDocserieAttribute()
    {
        $pts = $this->carData()->vininfo->reghistory->pts;
        return substr_replace($pts, ' ', 6, 0);
    }

    public function getPtsDateAttribute()
    {
        $pts = $this->carData()->Pts;
        return substr($pts, -10);
    }

    public function getDkNumberAttribute()
    {
        return $this->eaisto()->num;
    }

    public function getDkDateFromAttribute()
    {
        //
    }

    public function getDkDateAttribute()
    {
        //
    }

    /**
     * Бренд и модель могут быть в разных местах и на русском
     */
    public function getAvBrand($object)
    {
        //где может быть?
        $result = false;

        foreach ($object['result']['eaisto'] as $key => $val) {
            if(isset($val['marka']) && $this->isEnglishName($val['marka'])) {
                $result = $val['marka'];//не всегда на верную тачку
                break;
            }
        }

        foreach ($object['result']['gibdd'] as $key => $val) {
            if (isset($val['vininfo']['reghistory']['marka']) && $this->isEnglishName($val['vininfo']['reghistory']['marka'])) {
                $result = $val['vininfo']['reghistory']['marka'];
                break;
            }
        }

        foreach ($object['result']['gibdd'] as $key => $val) {
            if ($val['vininfo']['dtp']['hasDtp']) {
                $dtp = $val['vininfo']['dtp']['accident'];
                foreach ($dtp as $key2 => $val2) {
                    if (isset($val2['marka']) && $this->isEnglishName($val2['marka'])) {
                        $result = $val2['marka'];
                        break;
                    }
                }
            }
        }

        foreach ($object['result']['offer'] as $key => $val) {
            if (isset($val['marka']) && $this->isEnglishName($val['marka'])) {
                $result = $val['marka'];
                break;
            }
        }

        foreach ($object['result']['gibdd'] as $key => $val) {
            if (isset($val['marka']) && $this->isEnglishName($val['marka'])) {
                $result = $val['marka'];//самый надежный результат
                break;
            }
        }

        return $result;

    }

    public function getAvModel($object)
    {
        $result = false;

        foreach ($object['result']['offer'] as $key => $val) {
            if (isset($val['model']) && $this->isEnglishName($val['model'])) {
                $result = $val['model'];
                break;
            }
        }

        foreach ($object['result']['gibdd'] as $key => $val) {
            if ($val['vininfo']['dtp']['hasDtp']) {
                $dtp = $val['vininfo']['dtp']['accident'];
                foreach ($dtp as $key2 => $val2) {
                    if (isset($val2['model']) && $this->isEnglishName($val2['model'])) {
                        $result = $val2['model'];
                        break;
                    }
                }
            }
        }

        foreach ($object['result']['gibdd'] as $key => $val) {
            if (isset($val['vininfo']['decoding']['Модель']) && $this->isEnglishName($val['vininfo']['decoding']['Модель'])) {
                $result = $val['vininfo']['decoding']['Модель'];
                break;
            }
        }

        return $result;
    }

    /**
     * Если не нашли бренд - пробуем достать полное русское название и перевести его через dadata
     */
    public function getAvCar($object)
    {
        $result = false;

        foreach ($object['result']['gibdd'] as $key => $val) {
            if (isset($val['car'])) {
                $result = $val['car'];
                break;
            }
        }

        $dd = new Dadata();
        return $dd->check($result);
    }

    public function isEnglishName($name)
    {
        return ($name && $name != '' && (preg_match( '/[а-яё]/iu',  $name) == 0) || VehicleMarks::where('title', $name)->first()) ? true : false;
    }

    public function getTranslation()
    {
        $dd = new Dadata();
        return $dd->check('431234а хендай');
    }

    public function getPreCalculation($gosnum)
    {
        if($this->pre_calculation)
            return $this->pre_calculation;
        $calc = self::where('gosnum', $gosnum)->where('pre_calculation', '<>', null)->first();
        if(!$calc)
            $this->pre_calculation = rand(200000, 1000000)/100;
        else
            $this->pre_calculation = $calc->pre_calculation;
        if(isset($this->data))
            $this->save();
        return $this->pre_calculation;
    }

    public function getStats()
    {
        $source = new IntegrationAVDataInfo();
        return $source->getApiStats();
    }

}
