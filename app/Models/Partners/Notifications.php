<?php

namespace App\Models\Partners;

use App\Mail\Partners\Release;
use Illuminate\Database\Eloquent\Model;
use App\Mail\Partners\Lead;
use App\Mail\Partners\Error;
use App\Mail\Partners\Contract;

use Mail;

class Notifications extends Model
{
    public static $mail_support = 'dd@sst.cat';

    public static function error(string $msg)
    {
        $mail = Mail::to(self::$mail_support)->send(new Error('Обнаружена ошибка интеграции ОСАГО для формы партнеров!', $msg));
    }

    public static function lead(?Leads $lead)
    {
        if(!$lead)
            return;
        $mail = new Lead($lead);
        Mail::to(IntegrationPartnersTokens::current()->user->email)->send($mail->subject('Форма ОСАГО: поступил лид ' . $lead->id));
    }

    public static function contract(?Leads $lead)
    {
        if(!$lead)
            return;
        $mail = new Contract($lead);
        Mail::to(IntegrationPartnersTokens::current()->user->email)->send($mail->subject('Форма ОСАГО: лид ' . $lead->id . ' заполнил данные'));
    }

    public static function release(?Leads $lead)
    {
        if(!$lead)
            return;
        $mail = new Release($lead);
        Mail::to(IntegrationPartnersTokens::current()->user->email)->send($mail->subject('Форма ОСАГО: лид ' . $lead->id . ' выбрал страховую'));
    }

}
