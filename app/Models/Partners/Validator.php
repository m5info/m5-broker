<?php

namespace App\Models\Partners;

use App\Domain\Validators\BaseValidator;

class Validator extends BaseValidator
{
    public function rules() : array {

        $rules = [

            'contract.begin_date' => 'required|date|after:+4 days',
            'contract.insurer.first_name' => 'required|regex:/(^([а-яА-Я- ёЁ]+)?$)/u',
            'contract.insurer.second_name' => 'required|regex:/(^([а-яА-Я- ёЁ]+)?$)/u',
            'contract.insurer.last_name' => 'required|regex:/(^([а-яА-Я- ёЁ]+)?$)/u',

        ];

        if(isset(request()['contract']['object_needs_dk']) && !request()['contract']['object_needs_dk']) {
            $rules['contract.object.dk_date'] = 'required|date|after:now';
        }

        return $rules;

    }


    public function messages() : array {

        $messages = [

            'contract.begin_date.after' => 'В поле Начало страх. периода должна быть дата больше текущей + 4 дня',
            'contract.object.dk_date.after' => 'Проверьте срок действия диагностической карты',
            'contract.insurer.first_name.regex' => 'Поле имени должно содержать русские буквы',
            'contract.insurer.second_name.regex' => 'Поле отчества должно содержать русские буквы',
            'contract.insurer.last_name.regex' => 'Поле фамилии должно содержать русские буквы',
/*            'contract.driver.*.fio.required' => 'Укажите ФИО водителя',
            'contract.driver.*.birth_date.required' => 'Укажите дату рождения водителя',
            'contract.driver.*.doc_date.required' => 'Укажите дату выдачи водительского удостоверения',
            'contract.driver.*.exp_date.required' => 'Укажите водительский стаж',
            'contract.driver.*.doc_num.required' => 'Укажите номер водительского удостоверения',
            'contract.driver.*.doc_serie.required' => 'Укажите серию водительского удостоверения',*/

        ];


        return $messages;
    }
}
