<?php

namespace App\Models\Partners;

use App\Domain\Processes\Operations\Contracts\Online\ContractCalculationOperations;
use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Domain\Processes\Scenaries\Contracts\Online\OnlineContractSave;
use App\Mail\PaymentLink;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\Contracts\DriverPreviousLicense;
use App\Models\Contracts\KbmCalculation;
use App\Models\Contracts\Payments;
use App\Models\Directories\Products;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Contracts\Online\OnlineController;
use App\Http\Controllers\Contracts\Online\ActionController;
use App\Services\Kbm\Alfa\Calc;
use App\Models\Contracts\Driver;
use Mockery\Exception;

class Integration extends Model
{

    private $drivers;

    public function make_lead($request)
    {
        $parsed_user_data = Leads::parseReferer(request()['referer']);
        $brand = request()['contract']['object']['mark_id'] > 0 ? VehicleMapper::getBrandById(request()['contract']['object']['mark_id']): '';
        $model = request()['contract']['object']['model_id'] > 0 ? VehicleMapper::getModelById(request()['contract']['object']['model_id']): '';
        $data = [
            'referer_data' => request()['referer'],
            'object_data' => json_encode(request()['contract']['object']),
            'car_brand' => $brand ? $brand->title : '',
            'car_model' => $model ? $model->title : '',
            'car_region' => request()['contract']['object']['city'],
            'object_gosnum' => request()['contract']['object']['reg_number'],
            'user_ip' => request()['referer']['REMOTE_ADDR'],
            'user_source' => request()['referer']['source'],
            'user_os' => $parsed_user_data['os'],
            'user_browser' => $parsed_user_data['browser'],
            'device_type' => $parsed_user_data['device'],
            'device_brand' => $parsed_user_data['brand'],
            'device_model' => $parsed_user_data['model'], //'clientInfo', 'osInfo', 'os', 'browser', 'device', 'brand', 'model'
            'token_id' => IntegrationPartnersTokens::current()->id,
        ];

        //новый лид создается если номер лида из сессии, либо номер телефона не совпадают

        $lead = $this->check_lead();
        if($lead) {
            $lead->update($data);
            $lead->save();
        } else {
            $lead = Leads::create($data);
        }


        return $lead;
    }

    public function fill_lead()
    {
        $data = [
            'need_diagnostics_card' => request()['contract']['object_needs_dk'],
            'name' => request()['contract']['insurer']['fio'],
            'phone' => request()['contract']['insurer']['phone'],
        ];
        $lead = $this->check_lead();
        if($lead) {
            $lead->update($data);
            $lead->save();
        }

    }

    public function check_lead()
    {
        $lead_id = request()['contract']['lead']['id'] ?? -1;
        $lead = Leads::where('id', $lead_id)->where('object_gosnum', request()['contract']['object']['reg_number'])->first();
        if(!$lead)
            $lead = Leads::where('object_gosnum', request()['contract']['object']['reg_number'])
                ->where('created_at', '>', date('Y-m-d', strtotime("-1 day")))
                ->latest('id')->first();
        return $lead;
    }

    public function get_payment_link($calc)
    {
        $this->login();

        $calculation = ContractsCalculation::find($calc);
        if(!$calculation)
            return false;
        $contract = Contracts::find($calculation->contract_id);
        $contract->send_email = $contract->insurer->email;
        $contract->is_epolicy = 1;
        //производим выбор конкретной страховой
        ContractCalculationOperations::select_calc($contract, $calc);

        $lead = Leads::where('contract_id', $contract->id)->first();
        //забираем апи, чтобы релизнуть полис
        $service = $calculation->get_product_service();

        $link = false;

        //return ['calculation' => $calculation, 'contract' => $contract, 'lead' => $lead, 'link' => $link];
        try {
        //релизим, забираем из него ссылку на оплату
        $api_response = $service->release($calculation);

        //#ToDo проработать сценарий, если нет диагностической карты
        if($api_response->state){
            if($api_response->payment_link)
                $link = $api_response->payment_link;
            else
                $link = $api_response->answer;
            //делаем всякие смежные операции

            $this->payment_confirm_helper($contract, $calculation);
        }


        } catch (\Throwable $exception) {
        }

        if($link)
            Ask::confirmLink();
        else
            Ask::confirmFailLink();

        $this->logout();
        return ['calculation' => $calculation, 'contract' => $contract, 'link' => $link, 'lead' => $lead];
    }

    /**
     * Функция заполнения/обновления контракта
     * Вызывается на step3 после создания контракта
     * @param $id
     * @return array
     */
    public function fill_contract($id)
    {
        $this->login();
        $contract = Contracts::getContractId($id);
        $data = request()->all();
        $data = json_decode(json_encode($data));

        if(isset(request()['contract']['lead']['id'])) {
            $lead = Leads::where('id', request()['contract']['lead']['id'])->first();
            if($lead) {
                $lead->contract_id = $id;
                $lead->save();
            }
        }

        $data->contract->object->vin = cyrillicDeobfuscation($data->contract->object->vin);
        $data->contract->driver = (array) $data->contract->driver;

        $this->drivers = $this->getDrivers($id);

        if($this->hasRemovedDrivers(array_keys($data->contract->driver))){
            foreach ($this->drivers as $key => $driver){
                if($key == 0)
                    continue;
                $driver->delete();
            }
        }

        foreach ($data->contract->driver as $key => $val){
            $data->contract->driver[$key]->id = $this->getDriverId($key);
        }

        $result = OnlineContractSave::handle($contract, $data);

        $this->drivers = $this->getDrivers($id);

        if(isset($data->contract->driver[1]->old_doc_serie)){
            $this->driver_old_license_save($data->contract->driver[1], $this->drivers[0]);
        }

        $this->get_kbm($id);

        $this->logout();
        return $result;
    }

    /**
     * Функция создания контракта, вызывается на step3
     * @param Request $request
     * @return bool|mixed
     */
    public function make_contract(Request $request)
    {

        $this->login();
        $product_id = 1; //Осаго
        $product = Products::where('is_actual', '1')->where('id', '=', $product_id)->first();

        if (!$product) {
            return false;
        }

        $program_id = null;
        if (isset($request->program) && $request->program > 0) {
            $program_id = $request->program;
        }


        $contract = new Contracts;
        $contract->product_id = $product->id;
        $contract->program_id = $program_id;
        $contract->agent_id = auth()->id();
        $contract->user_id = auth()->id();
        $contract->statys_id = 0;
        $contract->is_online = 1;
        $contract->save();


        if ($contract->program) {
            $program = $contract->program;

            if ($program->slug != 'kasko_calculator') {
                ContractsCalculation::createCalc($contract, $contract->product, $contract->program, true);
            }
        }

        $this->logout();
        return $contract->id;

    }

    /**
     * Функция перерасчета КБМ, вызывается при заполнении контракта
     */
    public function get_kbm()
    {
        $drivers = $this->drivers;
        foreach ($drivers as $driver){

            try {
                $kbmCalculation = KbmCalculation::create([
                    'first_name'      => $driver->first_name,
                    'last_name'       => $driver->last_name,
                    'middle_name'     => $driver->second_name,
                    'birth_date'      => date("Y-m-d 00:00:00", strtotime($driver->birth_date)),
                    'document_series' => $driver->doc_serie,
                    'document_number' => $driver->doc_num,
                    'foreign_docs'    => $driver->foreign_docs,
                ]);


                $calc = new Calc();

                $calc->handle($kbmCalculation);

                $driver->kbm = $kbmCalculation->kbm;
                $driver->class_kbm = $kbmCalculation->class_kbm;
                $driver->kbm_rsa_request_id = $kbmCalculation->rsa_request_id;
                $driver->save();

            } catch (\Throwable $exception) {

            }
        }

    }

    /**
     * Функция получения списка СК (обертка над get_calculate_list)
     * Вызывается на step3
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_calc_list($id)
    {

        $this->login();
        $controller = new OnlineController();

        $contract = Contracts::find($id);

        $result = $controller->get_calculate_list($id);
        $result = json_decode($result->getContent());
        foreach ($result as $key => $item){

            $calc = ContractsCalculation::find($item);

            $data = $calc->bso_supplier
                ->hold_kv_product($contract->product_id)
                ->pay_methods_group->pluck('id')->toArray();

            //если нет оплаты по ссылке - грохаем калькуляцию
            if(!in_array(9, $data)){
                unset($result[$key]);
            }
        }
        $this->logout();
        return $result;
    }

    /**
     * Функция получения расчета конкретной СК (обертка над get_calculate_sk, calculate_sk
     * Вызывается на step4
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_calc_sk($id)
    {
        $this->login();
        try {
            $controller = new OnlineController();
            $result = $controller->get_calculate_sk($id);
            $result = $controller->calculate_sk($id);
            $result = ContractsCalculation::where('id', $id)->first();
        } catch (\Throwable $exception) {
            $result = ContractsCalculation::where('id', $id)->first();
        }
        $this->logout();
        return $result;
    }

    /**
     * Рекурсивная функция создания плоского массива для заполнения форм
     * @param $array
     * @param string $prefix
     * @return array
     */
    public function flatten($array, $prefix = '') {
        $result = array();
        foreach($array as $key=>$value) {
            if(is_array($value)) {
                if($prefix == '')
                    $result = $result + $this->flatten($value, $prefix . $key );
                else
                    $result = $result + $this->flatten($value, $prefix .'['. $key . ']');
            }
            else {
                if($prefix == '')
                    $final_key = $prefix . $key;
                else
                    $final_key = $prefix .'['. $key . ']';
                $result[$final_key] = $value;
            }
        }
        return $result;
    }

    public function login()
    {
        $user = IntegrationPartnersTokens::current()->user;

        Auth::login($user, true);
    }

    public function logout()
    {
        Auth::logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();
    }

    public function get_contract($id)
    {
        $this->login();
        $contract = Contracts::getContractId($id);
        $this->logout();
        return $contract;
    }

    public function getDriver($id)
    {
        $driver = Driver::where('contract_id', $id)->orderBy('id', 'ASC')
            ->first();
        return $driver ? $driver->id : -1;
    }

    public function getDrivers($id)
    {
        $drivers = Driver::where('contract_id', $id)->orderBy('id', 'ASC')->get()
            ->all();
        return $drivers ? $drivers : -1;
    }

    public function getDriverId($key)
    {
        $drivers = $this->drivers;
        if(isset($drivers[$key-1]))
            return $drivers[$key-1]->id;
        return -1;
    }

    public function driver_old_license_save($data, $driver)
    {

        $update_or_create_array = [
            'driver_id' => $driver->id,
            'first_name' => $data->old_first_name,
            'middle_name' => $data->old_second_name,
            'last_name' => $data->old_last_name,
            'doc_series' => $data->old_doc_serie,
            //'doc_number' => $data->old_doc_number,
            'doc_date_issue' => date('Y-m-d', strtotime($data->old_doc_start_date)),
            'doc_date_end' => date('Y-m-d', strtotime($data->old_doc_end_date)),
        ];

        if($old_license_data = $driver->old_license){
            $old_license_data->update($update_or_create_array);
        }else {
            DriverPreviousLicense::create($update_or_create_array);
        }

        return ['state' => 1];
    }

    public function hasRemovedDrivers($indexes)
    {
        //if indexes are 1 2 3 4 => false, none removed

        //if indexes are 1 3 5 => true, 2 4 removed
        $current = $indexes[0];

        foreach ($indexes as $number => $index) {
            if($number == 0)
                continue;
            if($indexes[$number - 1] != $index - 1)
                return true;
        }

        return false;
    }

    public function payment_confirm_helper(Contracts $contract, ContractsCalculation $calculation){

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';

        $contract->send_email = $contract->insurer->email;

            if (!$payment = $contract->get_payment_first()) {
                $payment = Payments::create(["payment_number" => 1, "statys_id" => 0, "contract_id" => $contract->id]);
            }

            $payment->update([
                "statys_id" => 0,
                'accept_user_id' => auth()->id(),
                'accept_date' => date("Y-m-d H:i:s"),
                "type_id" => 0,
                "payment_total" => $calculation->sum,
                'official_discount' => 0,
                'bank_kv' => 0,
                "payment_data" => date('Y-m-d', time()),
                "informal_discount" => 0,
                'parent_agent_id' => $contract->parent_agent_id,
                'agent_id' => $contract->agent_id,
                'financial_policy_id' => $calculation->financial_policy_id,
                'pay_method_id' => 9,//у нас только оплата по ссылке!!!
            ]);

            //ПЕРЕСЧЫВАЕМ КВ
            $payment = PaymentFinancialPolicy::actualize($payment);
            $payment = PaymentDiscounts::recount($payment);


            $payment_method = $payment->pay_mehtod;
            $payment->update([
                "payment_type" => $payment_method->payment_type,
                "payment_flow" => $payment_method->payment_flow,
            ]);
            //все остальное сделается после обновления контракта живым агентом
        //статус 4 [Выпущен] будет присвоен тогда же
        return true;
    }
}
