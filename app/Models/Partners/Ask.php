<?php

namespace App\Models\Partners;

use App\Models\Dictionaries\DictionarySk;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;

class Ask
{
    public static $lead;

    public static function getLead()
    {
        if(self::$lead)
            return self::$lead;
        if(isset(request()->lead)){
            self::$lead = Leads::find(request()->lead);
            return self::$lead;
        }
        return;
    }

    public static function data()
    {
        self::getLead();
        switch (request()->ask){

            case 'brands':
                $response = VehicleMarks::get(['id', 'title'])->toArray();
                return response()->json($response);

            case 'models':
                $response = VehicleModels::where('mark_id', request()->param)->get(['id', 'title'])->toArray();
                return response()->json($response);

            case 'brand':
                $response = VehicleMarks::where('id', request()->param)->get(['id', 'title'])->first();
                if($response)
                    $response = $response->toArray();
                return response()->json($response);

            case 'model':
                $response = VehicleModels::where('id', request()->param)->get(['id', 'title'])->first();
                if($response)
                    $response = $response->toArray();
                return response()->json($response);

            case 'prolongation':
                $response = DictionarySk::get(['title', 'id'])->toArray();
                return response()->json($response);

            case 'confirm_lead':
                return self::confirmLead();

            case 'confirm_fail_results':
                return self::confirmFailResults();

            case 'validation':
                $response = self::validate();
                return response()->json($response);

            case 'update_lead':
                return self::update_lead();

        }

    }

    public static function confirmLead()
    {
        if(self::$lead->statys_id == 0 || self::$lead->statys_id > 4){
            Notifications::lead(self::$lead);
            self::$lead->statys_id = 1;
            self::$lead->save();
        }
        return 0;
    }

    public static function confirmContract()
    {
        self::getLead();
        if(self::$lead->statys_id == 1 || self::$lead->statys_id > 4){
            Notifications::contract(self::$lead);
            self::$lead->statys_id = 2;
            self::$lead->save();
        }
        return 0;
    }

    public static function confirmChoice()
    {
        self::getLead();
        if(self::$lead->statys_id == 2 || self::$lead->statys_id > 4){
            self::$lead->statys_id = 3;
            self::$lead->save();
        }
        return 0;
    }

    public static function confirmLink()
    {
        self::getLead();
        if(self::$lead->statys_id == 3 || self::$lead->statys_id > 4){
            self::$lead->statys_id = 4;
            Notifications::release(self::$lead);
            self::$lead->save();
        }
        return 0;
    }

    public static function confirmFailResults()
    {
        self::$lead->statys_id = 5;
        self::$lead->save();
        return view("contracts.contract_templates.online_contracts.partner.osago.calculation.error")->render();
    }

    public static function confirmFailLink()
    {
        self::getLead();
        self::$lead->statys_id = 6;
        Notifications::release(self::$lead);
        self::$lead->save();
    }

    public static function validate()
    {
        $validator = new Validator(request()->toArray());
        if($validator->validate())
            return [];
        return array_merge(...array_values($validator->get_errors()));
    }

    public static function update_lead()
    {
        self::$lead->phone = request()->param['phone'];
        self::$lead->name = request()->param['name'];
        self::$lead->save();
    }
}
