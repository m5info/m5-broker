<?php

namespace App\Models\Partners;

use App\Models\Settings\SettingsSystem;
use Dadata\DadataClient;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\Eloquent\Model;

class Dadata extends Model
{
    private $client;
    private $suggestion;

    public function check($str)
    {
        if(!$str || trim($str) == '')
            return false;

        $token = SettingsSystem::getDataParam('dadata', 'token');
        $this->client = new DadataClient($token, null);
        $vars = $this->generateVariants($str);

        foreach ($vars as $var) {
            $this->get_suggestion($var);
            if($this->suggestion && $this->valid_suggestion())
                return $this->get_suggestion_value();
        }

        return false;
    }

    public function check_simple($str)
    {
        if(!$str || trim($str) == '')
            return false;

        $token = SettingsSystem::getDataParam('dadata', 'token');
        $this->client = new DadataClient($token, null);

        $this->get_suggestion($str);
        if($this->suggestion && $this->valid_suggestion())
            return $this->get_suggestion_value();

    }

    private function generateVariants($str)
    {
        $variants = [];
        $variants[] = $str;
        $exploded = explode(' ', $str);
        $variants = array_merge($variants, $exploded);
        $half_of_brandname = mb_substr($exploded[0], 0, ceil(mb_strlen($exploded[0]) / 2));
        $variants[] = $half_of_brandname;

        foreach ($variants as $key => $variant) {
            if(!$this->valid($variant))
                unset($variants[$key]);
        }

        return $variants;
    }

    private function valid($str)
    {
        if(strlen($str) < 3)
            return false;
        if(preg_match( '/[а-яёa-z]/iu',  $str) == 0)
            return false;
        return true;
    }

    private function valid_suggestion()
    {
        return count($this->suggestion) == 1; //есть 1 конкретный бренд
    }

    private function get_suggestion($str)
    {
        try {
            $this->suggestion = $this->client->suggest("car_brand", $str);
        } catch (ClientException $e) {
            file_put_contents('logs.txt', $e->getMessage() , FILE_APPEND | LOCK_EX);
            $this->suggestion = false;
        }

    }

    private function get_suggestion_value()
    {
        return $this->suggestion[0]['value'];
    }
}
