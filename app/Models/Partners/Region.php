<?php

namespace App\Models\Partners;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public static function getRegion($gosnum)
    {

        $reg_code = mb_strlen($gosnum) == 9 ? mb_substr($gosnum, 6, 3) : mb_substr($gosnum, 6, 2);

        $regions = [
            'Москва' => ['77','97','99','177','197','199','777'],
            'Санкт-Петербург' => ['78','98','178'],
            'Московская обл.' => ['50','90','150','190','750']
        ];

        foreach ($regions as $key => $codes) {
            if(in_array($reg_code, $codes))
                return $key;
        }

        return false;
    }
}
