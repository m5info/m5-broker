<?php

namespace App\Models\Partners;

use App\Models\Contracts\Contracts;
use Illuminate\Database\Eloquent\Model;
use DeviceDetector\DeviceDetector;

class Leads extends Model
{

    const STATYS = [
        0 => 'Создан',
        1 => 'Лид подтвержден',
        2 => 'Контракт заполнен, страховка в процессе расчета',
        3 => 'Выбрал страховую',
        4 => 'Получил ссылку',
        5 => 'Ошибка - нет предложений от страховых',
        6 => 'Ошибка на этапе выпуска',
        7 => 'Отменен',
    ];

    //
    protected $table = "partners_leads";
    protected $fillable = [
        'name',
        'phone',
        'need_diagnostics_card',
        'car_brand',
        'car_model',
        'car_region',
        'user_ip',
        'user_source',
        'user_os',
        'user_browser',
        'device_type',
        'device_brand',
        'device_model',
        'object_data',
        'object_gosnum',
        'token_id'
    ];

    public function contract(){
        return $this->hasOne(Contracts::class, 'id', 'contract_id');
    }

    public function token(){
        return $this->hasOne(IntegrationPartnersTokens::class, 'id', 'token_id');
    }

    public function getStatusAttribute(){
        return self::STATYS[$this->statys_id];
    }

    public function getObjectAttribute(){
        return json_decode($this->object_data);
    }

    public function getAv100Attribute(){
        $data = new AVData();
        $res = $data->getByGosnum($this->object_gosnum);
        if(!$res)
            return;
        $res = json_decode($res->data);
        $res = json_encode($res->result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        return $res;
    }

    public static function parseReferer($server_data = false)
    {
        if(!$server_data)
            $server_data = $_SERVER;
        $parser = new DeviceDetector($server_data['HTTP_USER_AGENT']);
        $parser->parse();

        $clientInfo = $parser->getClient();
        $osInfo = $parser->getOs();

        $browser = isset($clientInfo['name']) ? $clientInfo['name'] : '';
        $os = isset($osInfo['name']) ? $osInfo['name'] : '';

        $clientInfo = json_encode($clientInfo);
        $osInfo = json_encode($osInfo);

        $device = $parser->getDeviceName();
        $brand = $parser->getBrandName();
        $model = $parser->getModel();

        return compact('clientInfo', 'osInfo', 'os', 'browser', 'device', 'brand', 'model');
    }

}
