<?php

namespace App\Models;

class Menu {

    public static function all() {
        $menuItems = [
            'bso' => [
                'ico' => 'ico-costs',
                'form_title' => 'bso',
                'form_button_name' => '',
                'form_button_class' => '',
                'form_button_link' => '',
                'links' => [
                    ['link' => 'add_bso_warehouse', 'ico' => 'ico-etc-parts', 'name' => 'add_bso_warehouse'],
                    ['link' => 'transfer', 'ico' => 'clients_dark', 'name' => 'bso_receive_transmit'],
//                    ['link' => 'formation_acts_sk', 'ico' => 'ico-insurance', 'name' => 'formation_acts_sk'],
                    ['link' => 'inventory_bso', 'ico' => 'ico-calendar', 'name' => 'inventory_bso'],
                    ['link' => 'inventory_agents', 'ico' => 'ico-calendar', 'name' => 'inventory_agents'],
                    ['link' => 'bso_statuses_check', 'ico' => 'ico-etc-parts', 'name' => 'bso_statuses_check'],
                    ['link' => 'ruin_bso', 'ico' => 'ico-orders', 'name' => 'ruin_bso'],
                ],
            ],
            'bso_acts' => [
                'ico' => 'ico-orders',
                'form_title' => 'bso_acts',
                'form_button_name' => '',
                'form_button_class' => '',
                'form_button_link' => '',
                'links' => [
                    ['link' => 'acts_reserve', 'ico' => 'ico-orders', 'name' => 'acts_reserve'],
                    ['link' => 'acts_transfer', 'ico' => 'clients_dark', 'name' => 'acts_transfer'],
                    ['link' => 'acts_implemented', 'ico' => 'ico-calendar', 'name' => 'acts_implemented'],
                    ['link' => 'acts_sk', 'ico' => 'ico-insurance', 'name' => 'acts_sk'],
                ],
            ],
            'contracts' => [
                'ico' => 'ico-orders',
                'form_title' => 'contracts',
                'form_button_name' => '',
                'form_button_class' => '',
                'form_button_link' => '',
                'links' => [
                    ['link' => 'fast_accept', 'ico' => 'ico-fin-politics', 'name' => 'fast_accept'],
                    ['link' => 'primary', 'ico' => 'ico-fin-politics', 'name' => 'primary'],
                    ['link' => 'primary-agent', 'ico' => 'ico-fin-politics', 'name' => 'primary-agent'],
                    ['link' => 'verification', 'ico' => 'clients_dark', 'name' => 'verification'],
                    //['link' => 'loading_contracts', 'ico' => 'ico-calendar', 'name' => 'loading_contracts'],
                    ['link' => 'temp_contracts', 'ico' => 'ico-orders', 'name' => 'temp_contracts'],
                    ['link' => 'partner-leads', 'ico' => 'ico-orders', 'name' => 'partner-leads'],
                    ['link' => 'prolongation', 'ico' => 'ico-notice', 'name' => 'prolongation'],
                    ['link' => 'online', 'ico' => 'ico-insurance', 'name' => 'online'],
                    ['link' => 'contracts_techunder', 'ico' => 'ico-fin-politics', 'name' => 'contracts_techunder'],
                    ['link' => 'reception_contracts', 'ico' => 'ico-fin-politics', 'name' => 'reception_contracts'],
                    ['link' => 'referral_for_pay', 'ico' => 'ico-fin-politics', 'name' => 'referral_for_pay'],
                    ['link' => 'underfillers', 'ico' => 'ico-orders', 'name' => 'underfillers'],

                    ['link' => 'downloading_contracts', 'ico' => 'ico-orders', 'name' => 'downloading_contracts'],
                ],
            ],
            'enrichment' => [
                'ico' => 'ico-calendar',
                'form_title' => 'enrichment',
                'form_button_name' => '',
                'form_button_class' => '',
                'form_button_link' => '',
                'links' => [
                    ['link' => 'av100', 'ico' => 'ico-insurance', 'name' => 'av100'],
                    ['link' => 'm5_back_old', 'ico' => 'ico-orders', 'name' => 'm5_back_old'],
                ],
            ],
            'orders' => [
                'ico' => 'ico-orders',
                'form_title' => 'orders',
                'form_button_name' => '',
                'form_button_class' => '',
                'form_button_link' => '',
                'links' => [
                    ['link' => '', 'ico' => 'ico-etc-parts', 'name' => 'inspections'],
                    ['link' => 'processing', 'ico' => 'clients_dark', 'name' => 'contract_orders'],
                    ['link' => 'acts_of_completion', 'ico' => 'ico-orders', 'name' => 'acts_of_completion'],
                    ['link' => 'departures_courier_acts', 'ico' => 'ico-orders', 'name' => 'departures_courier_acts']
                ],
            ],
            'finance' => [
                'ico' => 'ico-cashbox',
                'form_title' => 'finance',
                'form_button_name' => '',
                'form_button_class' => '',
                'form_button_link' => '',
                'links' => [
                    ['link' => 'invoice', 'ico' => 'ico-credit-cards', 'name' => 'invoice'],
                    ['link' => 'debts', 'ico' => 'ico-cash', 'name' => 'debts'],
                    ['link' => 'expected_payments', 'ico' => 'ico-salary', 'name' => 'expected_payments'],


                ],
            ],
            'cashbox' => [
                'ico' => 'ico-cashbox',
                'form_title' => 'cashbox',
                'form_button_name' => '',
                'form_button_class' => '',
                'form_button_link' => '',
                'links' => [
                    ['link' => 'invoice', 'ico' => 'ico-credit-cards', 'name' => 'invoice'],
                    ['link' => 'user_balance', 'ico' => 'ico-cash', 'name' => 'user_balance'],
                    ['link' => 'collection', 'ico' => 'ico-salary', 'name' => 'collection'],
                    ['link' => 'incomes_expenses', 'ico' => 'ico-fin-politics', 'name' => 'incomes_expenses'],
                    ['link' => 'acts_to_underwriting', 'ico' => 'ico-fin-politics', 'name' => 'acts_to_underwriting'],
                    ['link' => 'operation_list', 'ico' => 'ico-fin-politics', 'name' => 'operation_list'],
                    ['link' => 'ofd_cashbox', 'ico' => 'ico-cashbox', 'name' => 'ofd_cashbox'],
                    ['link' => 'foundation_payments', 'ico' => 'ico-salary', 'name' => 'foundation_payments'],
                ],
            ],
            'reports' => [
                'ico' => 'ico-orders',
                'form_title' => 'reports',
                'form_button_name' => '',
                'form_button_class' => '',
                'form_button_link' => '',
                'links' => [
                    ['link' => 'reports_sk', 'ico' => 'ico-credit-cards', 'name' => 'reports_sk'],
                    ['link' => 'reports_sk_summary', 'ico' => 'ico-fin-politics', 'name' => 'reports_sk_summary'],
                    ['link' => 'promotion', 'ico' => 'ico-salary', 'name' => 'reports_advertising'],
                ],
            ],
            'analitics' => [
                'ico' => 'ico-analitics',
                'form_title' => 'analitics',
                'form_button_name' => '',
                'form_button_class' => '',
                'form_button_link' => '',
                'links' => [
                    ['link' => 'analitics_common', 'ico' => 'ico-fin-politics', 'name' => 'analitics_common'],
                    ['link' => 'analitic_point_sale', 'ico' => 'ico-fin-politics', 'name' => 'analitic_point_sale'],

                    ['link' => 'analitics_total', 'ico' => 'ico-fin-politics', 'name' => 'analitics_total'],
                    ['link' => 'analytics_techunder', 'ico' => 'ico-fin-politics', 'name' => 'analytics_techunder'],
                    ['link' => 'analytics_underfillers', 'ico' => 'ico-fin-politics', 'name' => 'analytics_underfillers'],
                    ['link' => 'accepted', 'ico' => 'ico-fin-politics', 'name' => 'accepted'],
                    ['link' => 'clients', 'ico' => 'clients_dark', 'name' => 'clients'],
                    ['link' => 'kv_collections', 'ico' => 'ico-etc-parts', 'name' => 'kv_collections'],
                    ['link' => 'contracts_terminations', 'ico' => 'ico-etc-parts', 'name' => 'contracts_terminations'],
                    ['link' => 'basket_items', 'ico' => 'ico-etc-parts', 'name' => 'basket_items'],
                    ['link' => 'k_manager', 'ico' => 'ico-employers', 'name' => 'k_manager'],
                    ['link' => 'delivery', 'ico' => 'ico-etc-parts', 'name' => 'delivery_analitics'],
                ],
            ],
            'informing' => [
                'ico' => 'ico-calendar',
                'form_title' => 'informing',
                'form_button_name' => '',
                'form_button_class' => '',
                'form_button_link' => '',
                'links' => [
                    ['link' => 'news', 'ico' => 'ico-calendar', 'name' => 'news'],
                    ['link' => 'edit-news', 'ico' => 'ico-etc-parts', 'name' => 'edit_news'],
                    ['link' => 'segments', 'ico' => 'ico-calendar', 'name' => 'segments'],
                    ['link' => 'edit-segments', 'ico' => 'ico-etc-parts', 'name' => 'edit_segments'],
                    ['link' => 'kv', 'ico' => 'ico-calendar', 'name' => 'kv'],
                    ['link' => 'edit-kv', 'ico' => 'ico-etc-parts', 'name' => 'edit_kv'],
                    ['link' => 'guide', 'ico' => 'ico-etc-parts', 'name' => 'edit_guides'],
                ],
            ],

            'directories' => [
                'ico' => 'ico-costs',
                'form_title' => 'directories',
                'form_button_name' => '',
                'form_button_class' => '',
                'form_button_link' => '',
                'links' => [
                    ['link' => 'organizations/organizations', 'ico' => 'organisations_dark', 'name' => 'organizations'],
                    ['link' => 'insurance_companies', 'ico' => 'ico-insurance', 'name' => 'insurance_companies'],
                    ['link' => 'products', 'ico' => 'ico-etc-parts', 'name' => 'products'],
                    ['link' => 'purpose_of_use', 'ico' => 'ico-etc-parts', 'name' => 'purpose_of_use'],
                    ['link' => 'categories', 'ico' => 'ico-etc-parts', 'name' => 'categories'],
                    ['link' => 'marks_and_models', 'ico' => 'ico-etc-parts', 'name' => 'marks_and_models'],
                    ['link' => 'anti_theft_devices', 'ico' => 'ico-etc-parts', 'name' => 'anti-theft devices'],
                    ['link' => 'vehicle_colors', 'ico' => 'ico-etc-parts', 'name' => 'vehicle_colors'],
                    ['link' => 'delivery', 'ico' => 'ico-etc-parts', 'name' => 'edit_delivery'],
                    ['link' => 'avinfobot', 'ico' => 'ico-etc-parts', 'name' => 'avinfobot'],
                ],
            ],
            'users' => [
                'ico' => 'ico-users',
                'form_title' => 'users',
                'form_button_name' => 'form.buttons.create',
                'form_button_class' => '',
                'form_button_link' => '/users/users/create',
                'links' => [
                    ['link' => 'users', 'ico' => 'ico-employers', 'name' => 'users'],
                    ['link' => 'roles', 'ico' => 'ico-user-roles', 'name' => 'roles'],
                ],
            ],
            'settings' => [
                'ico' => 'ico-settings',
                'form_title' => 'settings',
                'form_button_name' => '',
                'form_button_class' => '',
                'form_button_link' => '',
                'links' => [
                    ['link' => 'citys', 'ico' => 'organisations_dark', 'name' => 'citys'],
                    ['link' => 'points_sale', 'ico' => 'ico-orders-map', 'name' => 'points_sale'],
                    ['link' => 'departments', 'ico' => 'ico-units', 'name' => 'departments'],
                    ['link' => 'financial_policy', 'ico' => 'ico-fin-politics', 'name' => 'financial_policy'],
                    ['link' => 'banks', 'ico' => 'ico-banks', 'name' => 'banks'],
                    ['link' => 'installment_algorithms_list', 'ico' => 'ico-banks', 'name' => 'installment_algorithms_list'],
                    ['link' => 'pay_methods', 'ico' => 'ico-credit-cards', 'name' => 'pay_methods'],
                    ['link' => 'type_org', 'ico' => 'ico-etc-parts', 'name' => 'type_org'],
                    ['link' => 'templates', 'ico' => 'ico-etc-parts', 'name' => 'templates'],
                    ['link' => 'system', 'ico' => 'ico-etc-parts', 'name' => 'system'],
                    ['link' => 'user_balance', 'ico' => 'ico-credit-cards', 'name' => 'user_balance'],
                    ['link' => 'incomes_expenses_categories', 'ico' => 'ico-salary', 'name' => 'incomes_expenses_categories'],
                    ['link' => 'system_project_features', 'ico' => 'ico-settings', 'name' => 'system_project_features'],
                    ['link' => 'departures_courier_price', 'ico' => 'ico-user-roles', 'name' => 'departures_courier_price']
                ],
            ],
            'logging' => [
                'ico' => 'ico-orders',
                'form_title' => 'logging',
                'form_button_name' => '',
                'form_button_class' => '',
                'form_button_link' => '',
                'links' => [
                    ['link' => 'actions', 'ico' => 'ico-orders', 'name' => 'cud_logs'],
                ],
            ],
        ];

        return collect($menuItems);
    }

    public static function forSelect(){

         $arr = [];
        foreach (Menu::all() as $menu => $m){
           $key = trans('menu.'.$menu);
             $arr[$key] = [];

            foreach ($m['links'] as $sum_m=> $sm){
               $arr[$key][$sm['link']]=trans('menu.'.$sm['name']);
            }
        }

        return  $arr;
    }
}
