<?php

namespace App\Models\Orders;

use App\Models\Contracts\Contracts;
use App\Models\Organizations\Organization;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class InspectionOrdersLogs extends Model
{
    protected $table = 'inspection_orders_logs';

    protected $guarded = ['id'];

    public $timestamps = true;

    const LOGS_STATUSES = [
        0 => 'Создана',
        1 => 'Отправлено в распределение',
        2 => 'Назначен исполнитель',
        3 => 'Отказ',
        4 => 'Подтверждение',
        5 => 'Приехал',
        6 => 'Закончил',
        7 => 'Акцепт',
    ];

    public function contract(){
        return $this->hasOne(Contracts::class, 'id', 'inspection_order_id');
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public static function setLogs($order_id, $status_title, $selected_executor = null)
    {

        $bso_log = InspectionOrdersLogs::create([
            'inspection_order_id' => $order_id,
            'user_id' => auth()->id(),
            'executor_user_id' => $selected_executor,
            'status_title' => $status_title,
            'geo_lon' => auth()->user()->geo_lon,
            'geo_lat' => auth()->user()->geo_lat,
        ]);

        return true;
    }
}
