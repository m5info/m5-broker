<?php

namespace App\Models\Orders;

use App\Domain\Processes\Operations\Inspection\Temple\InspectionCrashData;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsDocuments;
use App\Models\Contracts\Orders;
use App\Models\File;
use App\Models\Settings\City;
use App\Models\User;
use Dompdf\Dompdf;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Style;


class Inspection extends Model
{
    public static $pdf;

    protected $table = 'inspection_orders';

    protected $guarded = ['id'];

    public function order(){
        return $this->hasOne(InspectionOrders::class, 'inspection_id', 'id');
    }

    public function contract(){
        return $this->hasOne(Contracts::class, 'inspection_id', 'id');
    }

    public function accept_user() {
        return $this->hasOne(User::class, 'id', 'accept_user_id');
    }

    public function processing_user() {
        return $this->hasOne(User::class, 'id', 'processing_user_id');
    }

    public function city(){
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function export_act(){

        $order = \App\Domain\Entities\Inspection\Inspection::get_first($this->order->id);

        if($order && isset($order->inspection->temple_act) && !empty($order->inspection->temple_act)){

            $templ_data = InspectionCrashData::get_data($this->order->id);

            $uniqid = uniqid();

//            dd($uniqid);
//           return view('orders.order.partials.template_crash', ["data" => $templ_data]);

            Storage::put($uniqid . '.html',
                view('orders.order.partials.template_crash')
                    ->with(["data" => $templ_data])
                    ->render()
            );

            $contents = Storage::get($uniqid . '.html');

//            dd(base64_encode($contents));
            // с первого раза не срабатывает иногда
            while(true){
                self::html_to_pdf(base64_encode($contents));
                $result = self::$pdf;
                if($result){
                    Storage::delete($uniqid . '.html');
                    break;
                }
            }

            $url = url('/');
            $str=strpos($url, ":");
            $url=substr($url,  $str+3);

            $url = ($url == '127.0.0.1:8000') ? '' : $url.'/';

            $result = \GuzzleHttp\json_decode($result);

            $filename = $uniqid.".pdf";

            $path = $url . '/' . Contracts::FILES_DOC . '/' . $this->order->id . '/'. $uniqid.".pdf";

            $data = base64_decode($result->Files[0]->FileData);

            Storage::disk()->put($path, $data);

            $file_bd = self::createFileInDB(Contracts::FILES_DOC . '/' . $this->order->id, $filename, $uniqid);

            ContractsDocuments::create([
                'contract_id' => $this->order->id,
                'file_id' => $file_bd->id,
            ]);

            $pathToFile = storage_path('app' . $path);

//            return url('files/'. $uniqid);
//            return response()->download($pathToFile);
            return redirect(url('files/'. $uniqid));
        }

        return response()->json(['temple_act not found']);
    }

    public static function html_to_pdf($contents){

        $url = "https://v2.convertapi.com/convert/html/to/pdf?Secret=ALzo8iJ7D6egiC4a";
        $params = [
            'Parameters' => [
                0 => [
                    'Name' => "File",
                    'FileValue' => [
                        'Name' => 'test.html',
                        'Data' => $contents
                    ]
                ]
            ]
        ];

        $curl = curl_init();

        $headers = [];
        $headers[] = "Content-Type: application/json";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => \GuzzleHttp\json_encode($params),
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        self::$pdf = $response;

        return $response;
    }

    private static function createFileInDB($folder, $filename, $uniqid) {
        return File::create([
            'original_name' => $filename,
            'ext' => 'pdf',
            'folder' => $folder,
            'name' => $uniqid,
            'user_id' => auth()->check() ? auth()->id() : 1,
            'host' => request()->getHost()
        ]);
    }
}
