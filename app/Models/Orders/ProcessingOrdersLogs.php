<?php

namespace App\Models\Orders;

use App\Models\Contracts\Contracts;
use App\Models\Organizations\Organization;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class ProcessingOrdersLogs extends Model
{
    protected $table = 'orders_log_states';

    protected $guarded = ['id'];

    public $timestamps = false;


    public function contract(){
        return $this->hasOne(Contracts::class, 'id', 'contract_id');
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public static function setLogs($contract_id, $status_title)
    {

        ProcessingOrdersLogs::create([
            'contract_id' => $contract_id,
            'user_id' => auth()->id(),
            'text' => $status_title,
            'date_sent' => getDateTime(),
        ]);

        return true;
    }
}
