<?php

namespace App\Models\Orders;

use App\Models\Contracts\Contracts;
use App\Models\Contracts\Orders;
use App\Models\Organizations\Organization;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class DeparturesCourierActs extends Model
{
    protected $table = 'departures_courier_act';

    protected $guarded = ['id'];

    public $timestamps = true;

    const STATUS = [
        0 =>'Создан',
        1 =>'Согласование',
        2 =>'Акцептован',
    ];


    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function orders(){
        return $this->hasMany(Orders::class, 'departures_courier_act_id', 'id');
    }


    public static function getActs()
    {
        return DeparturesCourierActs::query();
    }

    public static function getActId($id)
    {

        $act = DeparturesCourierActs::getActs();
        return $act->where('id', $id)->get()->first();
    }


    public function setOrders($orders)
    {
        $order = Orders::whereIn('id', $orders)->update([
            'departures_courier_act_id' => $this->id,
        ]);

        return $this->refreshPriceTotal();
    }


    public function refreshPriceTotal()
    {
        $this->price_total = Orders::where('departures_courier_act_id', $this->id)->sum('departures_courier_price_total');
        $this->save();
        return true;
    }


}
