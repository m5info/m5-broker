<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;

class InspectionOrdersReportsDocuments extends Model
{

    protected $table = 'inspection_orders_reports_documents';

    public $timestamps = false;

}