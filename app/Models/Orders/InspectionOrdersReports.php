<?php

namespace App\Models\Orders;

use App\Models\File;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class InspectionOrdersReports extends Model
{

    const STATE = [
        0 => 'Создан',
        1 => 'На согласовании',
        2 => 'Акцептован',
        3 => 'Согласован',
        4 => 'На оплате',
        5 => 'Частично оплачен',
        6 => 'Оплачен',
    ];

    protected $table = 'inspection_orders_reports';

    public $timestamps = false;

    public function create_user(){
        return $this->hasOne(User::class, 'id', 'create_user_id');
    }

    public function files()
    {
        return $this->belongsToMany(File::class, 'inspection_orders_reports_documents', 'report_id', 'file_id');
    }

}