<?php

namespace App\Models\Orders;

use App\Models\Contracts\Contracts;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class InspectionComments extends Model
{
    protected $table = 'inspection_comments';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function contract(){
        return $this->hasOne(Contracts::class, 'id', 'contract_id');
    }
}
