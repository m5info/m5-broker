<?php

namespace App\Models\Orders;

use App\Domain\Samplers\Contracts\TabsVisibility;
use App\Helpers\Visible;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Orders;
use Illuminate\Database\Eloquent\Model;

class InspectionOrders extends Model
{

    protected $table = 'contracts';

    const STATUSES = [
        0 => 'Временные',
        1 => 'На распределении',
        2 => 'В работе',
        3 => 'В проверке',
        4 => 'На согласовании',
        5 => 'Архив',
    ];

    const COLOR = [
        0 => '#ffffff',
        1 => '#ffffff',
        2 => '#d6f5d6',
        3 => '#e6f0ff',
        4 => '#e6f0ff',
        5 => '#ffffcc',
    ];



    const POSITION_TYPE = [
        0 => 'Выезд',
        1 => 'Точка'
    ];

    const CLIENT_EVENT_TYPE = [
        0 => 'Страхователь',
        1 => 'Потерпевший',
        2 => 'Виновник',
    ];

    public function comments(){
        return $this->hasMany(InspectionComments::class, 'contract_id', 'id');
    }

    public function inspection(){
        return $this->hasOne(Inspection::class, 'id', 'inspection_id');
    }

    public static function getOrdersCountArr() {

        $result = [];

        $tabs_visibility = TabsVisibility::get_inspection_orders_tab_visibility();

        if(isset($tabs_visibility[0]['view']) && $tabs_visibility[0]['view'] == 1){
            $result[0] = ['title' => 'Временные', 'count' => InspectionOrders::getOrdersQuery()->whereIn('status_order_id', [0])->count()];
        }

        if(isset($tabs_visibility[1]['view']) && $tabs_visibility[1]['view'] == 1){
            $result[1] = ['title' => 'На распределении', 'count' => InspectionOrders::getOrdersQuery()->whereIn('status_order_id', [1])->count()];
        }

        if(isset($tabs_visibility[2]['view']) && $tabs_visibility[2]['view'] == 1){
            $result[2] = ['title' => 'В работе', 'count' => InspectionOrders::getOrdersQuery()->whereIn('status_order_id', [2])->count()];
        }

        if(isset($tabs_visibility[3]['view']) && $tabs_visibility[3]['view'] == 1){
            $result[3] = ['title' => 'В проверке', 'count' => InspectionOrders::getOrdersQuery()->whereIn('status_order_id', [3])->count()];
        }

        if(isset($tabs_visibility[4]['view']) && $tabs_visibility[4]['view'] == 1){
            $result[4] = ['title' => 'На согласовании', 'count' => InspectionOrders::getOrdersQuery()->whereIn('status_order_id', [4])->count()];
        }

        if(isset($tabs_visibility[5]['view']) && $tabs_visibility[5]['view'] == 1){
            $result[5] = ['title' => 'Архив', 'count' => InspectionOrders::getOrdersQuery()->whereIn('status_order_id', [5])->count()];
        }


        return $result;
    }

    public static function getOrdersQuery() {
        $builder = Visible::apply(Contracts::query()->whereNotNull('status_order_id'), 'contracts', ['agent_id', 'manager_id']);

        $user = auth()->user();
        if($pd = $user->point_department_id){
            $builder
                ->leftJoin('inspection_orders', 'inspection_orders.id', '=', 'contracts.inspection_id')
                ->where('inspection_orders.org_point_id', '=', $pd);
        }

        return $builder;
    }

    public function getTokenAct()
    {

        $id = $this->id;
        $token = md5((getDateRu()."{$id}pdf"));
        $inspection = $this->inspection;
        $inspection->token_act = $token;
        $inspection->save();
        return $token;
    }

    public static function getOrderId($id)
    {
        return InspectionOrders::getOrdersQuery()->where('id', $id)->get()->last();
    }
}
