<?php

namespace App\Models\Basket;

use App\Models\BSO\BsoItem;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class BasketItems extends Model
{
    protected $table = 'basket_items';

    protected $guarded = ['id'];

    public $timestamps = true;

    const TYPES = [
        1 => 'bso_items',
        2 => 'payments',
    ];

    const TYPES_RU = [
        1 => 'БСО',
        2 => 'Платежи',
    ];

    public function removed_user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function bso(){
        return $this->hasOne(BsoItem::class, 'id', 'removed_item_id');
    }

    public function payment(){
        return $this->hasOne(Payments::class, 'id', 'removed_item_id');
    }
}
