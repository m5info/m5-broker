<?php

namespace App\Models\Vehicle;

use App\Models\File;
use App\Models\Security\Security;
use App\Models\Settings\FinancialGroup;
use App\Models\Settings\TypeOrg;
use App\Models\User;
use App\Traits\Models\ManyOperationTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Settings\Organization
 *
 * @property integer $id
 * @property string $title
 * @mixin \Eloquent
 */
class VehicleModels extends Model
{

    use ManyOperationTrait;

    protected $table = 'vehicle_models';

    protected $guarded = ['id'];

    public $timestamps = false;



}
