<?php

namespace App\Models\Vehicle;

use Illuminate\Database\Eloquent\Model;

class VehicleAntiTheftSystem extends Model
{

    protected $table = 'vehicle_anti_theft_system';

    protected $guarded = ['id'];

    public $timestamps = false;



}
