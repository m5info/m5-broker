<?php

namespace App\Models\Vehicle;

use Illuminate\Database\Eloquent\Model;

class VehicleColor extends Model
{

    protected $table = 'vehicle_color';

    protected $guarded = ['id'];

    public $timestamps = false;



}
