<?php

namespace App\Models\Vehicle\APISettings;

use App\Models\File;
use App\Models\Security\Security;
use App\Models\Settings\FinancialGroup;
use App\Models\Settings\TypeOrg;
use App\Models\User;
use App\Traits\Models\ManyOperationTrait;
use Illuminate\Database\Eloquent\Model;


class VehicleMarksSK extends Model{

    use ManyOperationTrait;

    public $table = 'vehicle_marks_sk';

    public $guarded = ['id'];

    public $timestamps = false;


    public function model() {

        return $this->hasMany(VehicleModelsSK::class, 'vehicle_marks_sk_id', 'vehicle_marks_sk_id')
            ->where('insurance_companies_id', $this->insurance_companies_id)
            ->where('bso_supplier_id', $this->bso_supplier_id);
    }



}
