<?php

namespace App\Models\Vehicle\APISettings;

use App\Traits\Models\ManyOperationTrait;
use Illuminate\Database\Eloquent\Model;


class VehicleModelsSK extends Model{

    use ManyOperationTrait;

    public $table = 'vehicle_models_sk';

    public $guarded = ['id'];

    public $timestamps = false;

    public static function getQuery($insurance_companies_id, $bso_supplier_id, $mark_id)
    {   // шаблон запроса для поиска модели в справочнике ск
        return self::where('insurance_companies_id', $insurance_companies_id)
            ->where('bso_supplier_id', $bso_supplier_id)
            ->where('vehicle_marks_id', $mark_id);
    }

    public static function handleDuplicates($previous_models, $query = null)
    {
        // если нет запроса, создаем новый
        $previous_model = $previous_models->first();
        $query = $query ? clone $query : self::getQuery(
            $previous_model->insurance_companies_id,
            $previous_model->bso_supplier_id,
            $previous_model->vehicle_marks_id
        );

        // во всех записях очищаем локальную модель
        $previous_models->each(function($model) {
            $model->vehicle_models_id = 0;
            $model->save();
        });

        // ищем все модели по айди модели ск, считаем сколько всех и сколько чистых
        $model_sk_id = $previous_models->first()->vehicle_models_sk_id;
        $all_models = $query->where('vehicle_models_sk_id', $model_sk_id)->get();

        $clean_models = $all_models->filter(function($model) {
            return $model->vehicle_models_id == 0;
        });

        $all_models_count = $all_models->count();
        $clean_models_count = $clean_models->count();

        // если всех больше, чем чистых, удаляем все чистые, в противном случае удаляем все чистые кроме первой
        if ($all_models_count > $clean_models_count) {
            $clean_models->each->delete();
        } else if ($all_models_count === $clean_models_count) {
            $clean_models->shift();
            $clean_models->each->delete();
        }
    }

    public static function handleNewModel($models_sk, $modelDict)
    {
        $model_sk = $models_sk->last();

        if ($model_sk->vehicle_models_id == 0) {
            // если в последней записи привязанная локальная модель пустая, записываем в нее нужный айдишник
            $model_sk->vehicle_category_id = $modelDict->category_id;
            $model_sk->vehicle_models_id = $modelDict->id;
            $model_sk->save();
        } else {
            // в противном случае ищем, существует ли запись с нужной локальной моделью
            $model_exists = false;

            foreach($models_sk as $modelSk) {
                if ($modelSk->vehicle_models_id == $modelDict->id) {
                    $model_exists = true;
                    break;
                }
            }

            // если записи с такой моделью нет, клонируем последнюю запись и меняем в ней локальную модель на нужную
            if (!$model_exists) {
                $newModelSk = $model_sk->replicate();
                $newModelSk->vehicle_category_id = $modelDict->category_id;
                $newModelSk->vehicle_models_id = $modelDict->id;
                $newModelSk->save();
            }
        }
    }
}
