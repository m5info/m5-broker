<?php

namespace App\Models\Vehicle\APISettings;

use App\Models\File;
use App\Models\Security\Security;
use App\Models\Settings\FinancialGroup;
use App\Models\Settings\TypeOrg;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;


class VehicleColorSK extends Model
{
    protected $table = 'vehicle_color_sk';

    protected $guarded = ['id'];

    public $timestamps = false;



}
