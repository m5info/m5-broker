<?php

namespace App\Models\Vehicle;

use App\Models\File;
use App\Models\Security\Security;
use App\Models\Settings\FinancialGroup;
use App\Models\Settings\TypeOrg;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Settings\Organization
 *
 * @property integer $id
 * @property string $title
 * @mixin \Eloquent
 */
class VehicleMarks extends Model
{
    protected $table = 'vehicle_marks';

    protected $guarded = ['id'];

    public $timestamps = false;



}
