<?php

namespace App\Models\Vehicle;

use App\Services\ConnectApiEnergogarant\AutoCod;
use Illuminate\Database\Eloquent\Model;


class AutoCodInfo extends Model
{

    protected $table = 'vehicle_auto_cod_info';

    protected $guarded = ['id'];

    public $timestamps = false;


    //type = тип по сечу ищем vin или regNumber
    //value = значение
    public static function search($type, $value)
    {
        $search = 'vin';
        if($type == 'vin') $search = 'vin';
        if($type == 'regNumber') $search = 'reg_num';

        $actual_date = setDateTimePluss(getDateTime(), '- 2 month');
        $info = AutoCodInfo::query();
        $info->where($type, $value);
        $result = $info->where('actual_date', '>=', $actual_date)->get();

        if(count($result)>0){
            return $result->toArray();
        }

        return AutoCodInfo::getAutoCod($type, $value);

    }


    public static function getAutoCod($type, $value)
    {

        $cod = new AutoCod();
        $auto = $cod->getInfo($type, $value);


        if($auto){
            $info = new AutoCodInfo();
            $info->actual_date = getDateTime();
            $info->user_id = auth()->id();

            $info->vin = $auto->identifiers->vehicle->vin;
            $info->reg_num = $auto->identifiers->vehicle->reg_num;
            $info->year = $auto->tech_data->year;
            $info->title = $auto->tech_data->brand->name->original.'/'.$auto->tech_data->model->name->original.' '.$auto->tech_data->year;
            $info->info = \GuzzleHttp\json_encode($auto);
            $info->save();

            return [$info->toArray()];
        }


        return null;
    }



}
