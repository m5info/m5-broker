<?php

namespace App\Models\Vehicle;

use Illuminate\Database\Eloquent\Model;


class VehicleCategoriesOKP extends Model
{

    protected $table = 'vehicle_categories_okp';

    protected $guarded = ['id'];

    public $timestamps = false;



}
