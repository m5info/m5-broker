<?php

namespace App\Models\Vehicle;

use Illuminate\Database\Eloquent\Model;


class VehicleCategories extends Model
{

    const __CODES = [
        'A' => 'A',
        'B' => 'B',
        'C' => 'C',
        'D' => 'D',
        'H' => 'H',
        'G' => 'G',
    ];

    protected $table = 'vehicle_categories';

    protected $guarded = ['id'];

    public $timestamps = false;



}
