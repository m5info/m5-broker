<?php

namespace App\Models\Vehicle;

use Illuminate\Database\Eloquent\Model;

class VehiclePurpose extends Model{

    protected $table = 'vehicle_purpose';

    protected $guarded = ['id'];

    public $timestamps = false;


}
