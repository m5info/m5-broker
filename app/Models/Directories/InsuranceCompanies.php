<?php

namespace App\Models\Directories;

use App\Models\Contracts\Payments;
use App\Models\File;
use App\Models\Reports\ReportOrders;
use App\Models\Security\Security;
use App\Models\Settings\BaseRate;
use App\Models\Settings\InstallmentAlgorithmsList;
use App\Models\Settings\TypeOrg;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;


class InsuranceCompanies extends Model
{
    protected $table = 'insurance_companies';

    protected $guarded = ['id'];

    public $timestamps = false;

    const FILES_FOLDER = 'insurance_companies/logo';
    const TEMPLATES_FOLDER = 'insurance_companies/template';

    public function logo()
    {
        return $this->hasOne(File::class, 'id', 'logo_id');
    }

    public function algorithms()
    {
        return $this->hasMany(InstallmentAlgorithms::class, 'insurance_companies_id');
    }

    public function type_bso()
    {
        return $this->hasMany(TypeBso::class, 'insurance_companies_id');
    }


    public function bso_suppliers()
    {
        return $this->hasMany(BsoSuppliers::class, 'insurance_companies_id');
    }

    public function base_rates()
    {
        return $this->hasMany(BaseRate::class, 'company_id');
    }

    public function programs()
    {
        return $this->belongsToMany(ProductsPrograms::class, 'insurance_companies_programs', 'insurance_company_id', 'program_id');
    }


    /*
     * REPORTS
     *
     */


    public function getPaymentsTotal(){
        $payment_total = 0;

        foreach($this->bso_suppliers as $supplier){
            $payment_total += $supplier->getPaymentsTotal();
        }

        return $payment_total;
    }

     public function getDebtBrokerToSk(){

         $result = [
             'to_transfer' => 0,
             'to_return' => 0,
             'to_transfer_fact' => 0,
             'to_return_fact' => 0,
             'to_transfer_total' => 0,
             'to_return_total' => 0,
         ];

         foreach($this->bso_suppliers as $supplier){
             $supplier_payments = $supplier->getDebtBrokerToSk();
             $result['to_transfer'] += $supplier_payments['to_transfer'];
             $result['to_return'] += $supplier_payments['to_return'];
             $result['to_transfer_fact'] += $supplier_payments['to_transfer_fact'];
             $result['to_return_fact'] += $supplier_payments['to_return_fact'];
             $result['to_transfer_total'] += $supplier_payments['to_transfer_total'];
             $result['to_return_total'] += $supplier_payments['to_return_total'];
         }

         return $result;


    }



    public function getPaymentsTotalKV($report_type){
        $sum_kv = 0;
        foreach($this->bso_suppliers as $supplier){
            $sum_kv += $supplier->getPaymentsTotalKV($report_type);
        }
        return $sum_kv;
    }


    public function getPaymentsCount($report_type){
        $count = 0;
        foreach($this->bso_suppliers as $supplier){
            $count += $supplier->getPaymentsCount($report_type);
        }
        return $count;
    }


    public function getAlgorithmsArray()
    {
        $result = [];
        foreach ($this->algorithms as $algorithm){
            $alg = InstallmentAlgorithmsList::where('id', $algorithm->algorithm_id)->first();
            $result[$alg->id] = $alg->title;
        }

        return $result;
    }

    public function getAlgorithmsProductArray($product_id)
    {
        $result = [];
        $algorithms = $this->algorithms()->whereIn('product_id', [0, $product_id]);

        foreach ($algorithms->get() as $algorithm){
            $alg = InstallmentAlgorithmsList::where('id', $algorithm->algorithm_id)->first();
            $result[$alg->id] = $alg->title;
        }

        return $result;
    }


    public function getReportsSummary($report_year, $report_month)
    {
        $id = $this->id;

        $reports = ReportOrders::where('reports_orders.report_year', $report_year)->where('reports_orders.report_month', $report_month);
        $reports
            ->where('reports_orders.type_id', 0)
            ->where('reports_orders.is_deleted', '!=', 1)
            ->whereIn('reports_orders.bso_supplier_id', function ($query2) use ($id) {
            $query2->select(\DB::raw('bso_suppliers.id'))
                ->from('bso_suppliers')->where('bso_suppliers.insurance_companies_id', $id);
            })->orderBy('reports_orders.bso_supplier_id');

        return $reports->get();

    }

}
