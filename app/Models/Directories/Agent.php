<?php

namespace App\Models\Directories;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    const FILES_DOC = 'agent/docs';

    protected $table = 'agent';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function documents()
    {
        return $this->belongsToMany(File::class, 'agent_documents', 'agent_id', 'file_id');
    }

}
