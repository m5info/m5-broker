<?php

namespace App\Models\Directories;

use App\Models\Acts\ReportAct;
use App\Models\Api\SK\ApiSetting;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoType;
use App\Models\Contracts\Payments;
use App\Models\File;
use App\Models\Finance\Invoice;
use App\Models\Organizations\Organization;
use App\Models\Reports\AdverstisingReports;
use App\Models\Reports\ReportOrders;
use App\Models\Security\Security;
use App\Models\Settings\TypeOrg;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;


class BsoSuppliers extends Model{

    protected $table = 'bso_suppliers';

    const DOCUMENT_PATH = 'bso_acts/acts_sk';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function api_setting(){
        return $this->hasOne(ApiSetting::class, 'bso_supplier_id');
    }

    public function agent(){
        return $this->hasOne(Agent::class, 'bso_supplier_id');
    }

    public function actual_api_setting(){
        return $this->hasOne(ApiSetting::class, 'bso_supplier_id')->where('is_actual', 1);
    }

    public function hold_kv(){
        return $this->hasMany(HoldKv::class, 'bso_supplier_id');
    }

    public function hold_kv_product($product_id){
        return $this->hasMany(HoldKv::class, 'bso_supplier_id')->where('product_id', $product_id)->get()->first();
    }

    public function adverstisings(){
        return $this->hasMany(AdverstisingReports::class, 'supplier_id');
    }

    public function financial_policy(){
        return $this->hasMany(FinancialPolicy::class, 'bso_supplier_id');
    }

    public function type_bso(){
        return $this->hasMany(BsoType::class, 'insurance_companies_id')
            ->where('is_actual', '=', 1)
            ->where('title', '!=', '');
    }

    public function bso_items(){
        return $this->hasMany(BsoItem::class, 'bso_supplier_id');
    }

    public function bso_items_to_sk_acts(){

        return $this->bso_items()->whereIn('state_id', [0,1,3,4,5])->whereIn('location_id', [0,4]);

    }

    public function bso_items_search_filter(){

        $request = \request();
        $items = $this->bso_items();

        $items = $items->whereIn('state_id', [0,1,3,4,5])->whereIn('location_id', [0,4]);

        if($request['bso_title'])
            $items = $items->where('bso_title','like','%'.$request['bso_title'].'%');

        return $items;

    }

    public function invoices(){
        return $this->hasMany(Invoice::class, 'supplier_id');
    }

    public function purpose_org(){
        return $this->hasOne(Organization::class, 'id', 'purpose_org_id');
    }

    public function insurance(){
        return $this->hasOne(InsuranceCompanies::class, 'id', 'insurance_companies_id');
    }


    public function reports_acts(){
        return $this->hasMany(ReportAct::class, 'bso_supplier_id', 'id');
    }

    public function reports_border(){
        return $this->reports()->where('type_id', '=', 0);
    }

    public function reports_dvoy(){
        return $this->reports()->where('type_id', '=', 1);
    }

    public function reports(){
        return $this->hasMany(ReportOrders::class, 'bso_supplier_id', 'id')->orderBy('id', 'desc')->with('accept_user','create_user');
    }

    public function reports_actual()
    {
        return $this->hasMany(ReportOrders::class, 'bso_supplier_id', 'id')->where('is_deleted', '=', 0)->orderBy('id', 'desc')->with('accept_user','create_user');
    }

    public function getPaymentsTotal(){
        $payments = $this->getPayments()->get();
        return $payments->sum("payment_total");
    }


    public function getPaymentsTotalKV($report_type){
        //$report_type - 0 Бордеро 1 ДВОУ
        $payments = $this->getPayments($report_type)->get();
        $sum_kv = 0;
        if((int)$report_type == 0) $sum_kv = $payments->sum("financial_policy_kv_bordereau_total");
        if((int)$report_type == 1) $sum_kv = $payments->sum("financial_policy_kv_dvoy_total");
        return $sum_kv;
    }

    public function getPaymentsCount($report_type){
        //$report_type - 0 Бордеро 1 ДВОУ
        $payments = $this->getPayments($report_type)->get();
        $count = 0;
        if((int)$report_type == 0){
            $count = $payments->count();
        }
        if((int)$report_type == 1){
            $count = $payments->count();
        }
        return $count;
    }


    public function getActsBasePayments(){

        $bso_supplier_id = $this->id;
        $bso_title =  \request('bso_title');
        $act_sk_id = \request('act_sk_id');
        $reports_dvou_id = \request('reports_dvou_id');
        $reports_order_id = \request('reports_order_id');
        $payments_flow =  \request('payments_flow');
        $insurance_companies_id = $this->insurance_companies_id;

        $payments = Payments::where('payments.is_deleted', "0");

        if($payments_flow !='-1' && $payments_flow !='')
            $payments = $payments->where('payment_flow', $payments_flow);

        if(isset($act_sk_id) && $act_sk_id !='')
            $payments = $payments->where('payments.acts_sk_id','=', $act_sk_id);

        if(isset($reports_dvou_id) && $reports_dvou_id !='')
            $payments = $payments->where('payments.reports_dvou_id','=', (int)$reports_dvou_id);
        if(isset( $reports_order_id) &&  $reports_order_id !='')
            $payments = $payments->where('payments.reports_order_id','=', (int) $reports_order_id);

        $payments->whereIn('payments.bso_id', function($query) use ($bso_supplier_id,$bso_title, $insurance_companies_id)
        {
            $query->select(\DB::raw('bso_items.id'))
                ->from('bso_items')
                ->where('bso_items.bso_supplier_id', $bso_supplier_id)
                ->where('bso_items.location_id', '!=', 2)
                ->where('bso_items.bso_title','like','%'. $bso_title.'%')
                ->where('bso_items.insurance_companies_id', $insurance_companies_id);
        });



        return $payments;
    }

    public function getBasePayments($report_type = 0){

        $bso_supplier_id = $this->id;
        $bso_title =  \request('bso_title');
        $act_sk_id = \request('act_sk_id');
        $reports_dvou_id = \request('reports_dvou_id');
        $reports_order_id = \request('reports_order_id');
        $payments_flow =  \request('payments_flow');
        $insurance_companies_id = $this->insurance_companies_id;

        $payments = Payments::where('payments.is_deleted', "0");
        $payments->where('payments.type_id', 0);

        if($payments_flow !='-1' && $payments_flow !='')
            $payments = $payments->where('payment_flow', $payments_flow);

        if(isset($act_sk_id) && $act_sk_id !='')
            $payments = $payments->where('payments.acts_sk_id','=', $act_sk_id);

        if(isset($reports_dvou_id) && $reports_dvou_id !='')
            $payments = $payments->where('payments.reports_dvou_id','=', (int)$reports_dvou_id);
        if(isset( $reports_order_id) &&  $reports_order_id !='')
            $payments = $payments->where('payments.reports_order_id','=', (int) $reports_order_id);

        $payments->whereIn('payments.bso_id', function($query) use ($bso_supplier_id,$bso_title, $insurance_companies_id, $report_type)
        {
            $query->select(\DB::raw('bso_items.id'))
                ->from('bso_items')
                ->where('bso_items.bso_supplier_id', $bso_supplier_id)
                ->where('bso_items.bso_title','like','%'. $bso_title.'%')
                ->where('bso_items.insurance_companies_id', $insurance_companies_id);
                if ($report_type == 1){
                    $query->whereIn('bso_items.product_id', function($query_){
                        $query_->select('products.id')
                            ->from('products')
                            ->where('products.is_dvou', '=', 1);
                    });
                }
        });



        return $payments;
    }

    public function getPayments($report_type = -1){
        //$report_type - 0 Бордеро 1 ДВОУ - 1 ВСЕ

        //if($report_type = -1) $report_type = 0;

        if ($report_type == 1){
            $payments = $this->getBasePayments(1);
        }else{
            $payments = $this->getBasePayments();
        }
        //$payments->leftJoin('bso_items', 'bso_items.id', '=', 'payments.bso_id');
        //$payments->leftJoin('contracts', 'contracts.id', '=', 'payments.contract_id');
        //$payments->leftJoin('products', 'bso_items.product_id', 'products.id');

        if ($report_type == 1){
            //$payments->where('products.is_dvou', 1);
        }

        $payments->where(function($query){
            $query->where('payments.reports_order_id', '<=', "0")
                ->orWhere('payments.reports_dvou_id', '<=', "0");
        });



        if((int)$report_type == 0) $payments->where('payments.reports_order_id', '<=', "0");
        if((int)$report_type == 1) $payments->where('payments.reports_dvou_id', '<=', "0");
        $payments->whereIn('payments.statys_id', [0, 1]);


        return $payments;

    }

    public function getDebtBrokerToSk()
    {
        $reports = ReportOrders::where('reports_orders.bso_supplier_id', $this->id);
        $reports->where('reports_orders.is_deleted', '=', 0);
        $reports->where('reports_orders.accept_status','!=','6');


        $to_transfer = $reports->sum('reports_orders.to_transfer_total');
        $to_return = $reports->sum('reports_orders.to_return_total');

        $to_transfer_total = 0;
        $to_return_total = 0;
        foreach ($reports->get() as $report){
            $to_transfer_total += $report->report_payment_sums->where('type_id', 0)->sum('amount');
            $to_return_total += $report->report_payment_sums->where('type_id', 1)->sum('amount');
        }

        return [
            'to_transfer' => $to_transfer,
            'to_return' => $to_return,
            'to_transfer_fact' => $to_transfer_total,
            'to_return_fact' => $to_return_total,
            'to_transfer_total' => $to_transfer-$to_transfer_total,
            'to_return_total' => $to_return-$to_return_total,
        ];
    }


    public function get_service_controller(){

        if($api_settings = $this->actual_api_setting){

            $class = "App\\Services\\SK\\{$api_settings->dir_name}\\ServiceController";

            if(class_exists($class)){

                return new $class($this);

            }

        }

        return false;
    }


    public static function getBsoSuppliersIsEPolicy(){
        $bso_suppliers = BsoSuppliers::query();

        $bso_suppliers->where('bso_suppliers.is_actual', '=', 1);
        $bso_suppliers->whereIn('bso_suppliers.id', function($query)
        {
            $query->select(\DB::raw('hold_kv.bso_supplier_id'))
                ->from('hold_kv')
                ->where('hold_kv.is_epolicy', 1);
        });

        return $bso_suppliers;
    }

}
