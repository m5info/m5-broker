<?php

namespace App\Models\Directories;

use Illuminate\Database\Eloquent\Model;


class ManufacturingCalendar extends Model
{



    protected $table = 'manufacturing_calendar';

    protected $guarded = ['id'];

    public $timestamps = false;

    public static function getCountWorkedDayMounth($date_from){

        $mounth = (int)setDateMyFormat($date_from, 'm');
        $year = (int)setDateMyFormat($date_from, 'Y');

        $search_count_worked_day = self::where('mounth', $mounth)->where('year', $year)->get()->first();
        if($search_count_worked_day){
            return $search_count_worked_day->worked_day;
        }else{
            return self::freshManufacturingCalendar($mounth, $year);
        }

    }

    public static function freshManufacturingCalendar($mounth, $year){

        $mounth = str_pad($mounth, 2, '0', STR_PAD_LEFT);
        $countDay = cal_days_in_month ( CAL_GREGORIAN , $mounth , $year) ;
        $job_day = 0;
        for($i = 1; $i < $countDay; $i++){
            $j = str_pad($i, 2, '0', STR_PAD_LEFT);
            $page = file_get_contents("https://isdayoff.ru/{$year}{$mounth}{$j}");
            if($page == 0){     //будни 0, выходные 1, ошибка 100
                $job_day++;
            }
        }
        $search_count_worked_day = self::where('mounth', $mounth)->where('year', $year)->get()->first();
        if($search_count_worked_day){
            $search_count_worked_day->update([
                'worked_day' => $job_day
            ]);
        }else{
            self::create([
                'mounth' => $mounth,
                'year' => $year,
                'worked_day' => $job_day,
            ]);
        }

        return $job_day;
    }
}
