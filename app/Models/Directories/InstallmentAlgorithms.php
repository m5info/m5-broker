<?php

namespace App\Models\Directories;

use App\Models\File;
use App\Models\Security\Security;
use App\Models\Settings\InstallmentAlgorithmsList;
use App\Models\Settings\TypeOrg;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;


class InstallmentAlgorithms extends Model
{
    protected $table = 'installment_algorithms';

    protected $guarded = ['id'];

    public $timestamps = false;

//    const ALG_TYPE = [0=>'100%', 1=>'50% - 50%', 2=>'25% - 25% - 25% - 25%'];

    public function algorithm()
    {
        return $this->hasOne(InstallmentAlgorithmsList::class,  'id','algorithm_id');
    }

    public function product()
    {
        return $this->hasOne(Products::class,  'id','product_id');
    }

}
