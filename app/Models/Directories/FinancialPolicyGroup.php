<?php

namespace App\Models\Directories;

use App\Models\File;
use App\Models\Security\Security;
use App\Models\Settings\TypeOrg;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;


class FinancialPolicyGroup extends Model
{
    protected $table = 'financial_policies_groups_kv';

    protected $guarded = ['id'];

    public $timestamps = false;



}
