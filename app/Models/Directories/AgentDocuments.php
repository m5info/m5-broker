<?php

namespace App\Models\Directories;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;

class AgentDocuments extends Model
{

    const FILES_DOC = 'agent/docs';

    protected $table = 'agent_documents';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function file()
    {
        return $this->hasOne(File::class, 'id', 'file_id');
    }
}
