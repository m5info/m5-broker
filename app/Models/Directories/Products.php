<?php

namespace App\Models\Directories;

use App\Models\File;
use App\Models\Settings\ProgramsOnlineList;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use function GuzzleHttp\Promise\all;


class Products extends Model{

    protected $table = 'products';

    protected $guarded = ['id'];

    public $timestamps = false;

    const TEMPLATES_FOLDER = "products/template";


    const FIN_TYPE = [0=>'Процентная', 1=>'Фиксированная'];
    const INSPECTION_TEMPLE_ACT = [ '' =>'Нет', 'crash' =>'Извещение о ДТП'];
    const SLUG = [
        ''=>'нет',
        'osago'=>'ОСАГО',
        'ns'=>'НС',
        'kasko'=>'КАСКО',
        'diagnostic_card'=>'Диагностическая карта',
        'vtbOnline'=>'ВТБ онлайн',
        'flats'=>'Квартиры',
    ];

    public function category() {
        return $this->hasOne(ProductsCategory::class, 'id', 'category_id');
    }

    public function programs()
    {
        return $this->hasMany(ProductsPrograms::class,  'product_id');
    }

    public function optional_programs()
    {
        $optional_programs_list = ProgramsOnlineList::where('user_id', auth()->id())->get();

        if(count($optional_programs_list) && $this->id == 11){
            $query = ProductsPrograms::query();
            $query->leftJoin('programs_online_list', 'programs_online_list.program_id', '=', 'product_programs.id');
            $query->where('programs_online_list.user_id', '=', auth()->id());
            $query->orderBy('programs_online_list.sort');

            return $query->get();

        }else{
            return $this->hasMany(ProductsPrograms::class,  'product_id')->get();
        }
    }

    public function get_online_validator($data){

        $class_name = Str::ucfirst($this->slug);

        if($validator_class = "App\\Domain\\Validators\\Contracts\\Online\\Validate{$class_name}"){
            if(class_exists($validator_class)){
                return new $validator_class($data);
            }
        }

        return false;

    }

    public function template(){
        return $this->hasOne(File::class, 'id', "template_id");
    }


    public function get_products_info($type)
    {
        return $this->hasMany(ProductsInfo::class,  'product_id')->where('type_id', $type)->orderBy('sort')->get();
    }



}
