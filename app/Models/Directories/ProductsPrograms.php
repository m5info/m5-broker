<?php

namespace App\Models\Directories;

use App\Models\Settings\ProgramsOnlineList;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Str;


class ProductsPrograms extends Model{

    protected $table = 'product_programs';

    protected $guarded = ['id'];

    public $timestamps = false;

    const SLUG = [''=>'нет', 'kasko_calculator'=>'КАСКО: Калькулятор', 'kasko'=>'КАСКО', 'boxes'=>'Коробки', 'flats'=>'Квартиры'];

    public function insurance_companies_programs()
    {
        return $this->hasOne(InsuranceCompaniesPrograms::class, 'program_id','id');
    }


    public static function getActivPrograms($contract, $product_slug)
    {
        $programs = ProductsPrograms::query();
        $programs->where('slug', $product_slug);
        $programs->where('is_actual', 1);
        $programs->whereNotIn('id', function($query) use ($contract) {

            $query->select(\DB::raw('online_calculations.program_id'))
                ->from('online_calculations')
                ->where('online_calculations.contract_id', $contract->id);

        });

        return $programs->get();
    }

    public static function optional_programs_workarea(){

        $array = [];
        $array2 = [];
        $programs = ProductsPrograms::query()->where('is_actual', 1)->get();
        foreach($programs as $key => $program){
            $programs_list = ProgramsOnlineList::where('user_id', auth()->id())->where('program_id', $program->id)->first();
            $sort = $programs_list ? $programs_list->sort : null;
            if(!is_null($sort)){
                $program->sort = $sort;
                $array[$key] = $program;
            }else{
                $array2[] = $program;
            }
        }

        $sorted_programs = collect($array)->sortByDesc('sort')->reverse();

        foreach($array2 as $elem){
            $sorted_programs->push($elem);
        }

        return $sorted_programs;
    }


}
