<?php

namespace App\Models\Directories;

use App\Models\Security\Security;
use App\Models\Settings\TypeOrg;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;


class HoldKvDocuments extends Model
{
    protected $table = 'hold_kv_documents';

    protected $guarded = ['id'];

    public $timestamps = false;




}
