<?php

namespace App\Models\Directories;


use App\Models\Finance\PayMethod;
use App\Models\Settings\BsoSupplierProductPayMethodAccessBso;
use App\Traits\Models\CustomRelationTrait;
use Illuminate\Database\Eloquent\Model;


class HoldKv extends Model
{

    use CustomRelationTrait;

    protected $table = 'hold_kv';

    protected $guarded = ['id'];

    public $timestamps = false;


    const HOLD_TYPE = [0 => 'Без удержания', 1 => 'С удержанием', 2 => 'Частичное удержание'];

    public function product()
    {
        return $this->hasOne(Products::class, 'id', 'product_id');
    }


    public function documents()
    {
        return $this->hasMany(HoldKvDocuments::class, 'hold_kv_id', 'id');
    }


    public function pay_methods()
    {
        $hold_kv = $this;
        return $this->custom(PayMethod::class, function ($relation) use ($hold_kv) {
            $relation->join('bso_suppliers_products_pay_methods', 'bso_suppliers_products_pay_methods.pay_method_id', '=', 'pay_methods.id');
            $relation->where('bso_suppliers_products_pay_methods.bso_supplier_id', $hold_kv->bso_supplier_id);
            $relation->where('bso_suppliers_products_pay_methods.product_id', $hold_kv->product_id);
        });
    }

    public function pay_methods_with_group(){
        $hold_kv = $this;
        return $this->custom(PayMethod::class, function ($relation) use ($hold_kv) {
            $relation->join('bso_suppliers_products_pay_methods', 'bso_suppliers_products_pay_methods.pay_method_id', '=', 'pay_methods.id');
            $relation->where('bso_suppliers_products_pay_methods.bso_supplier_id', $hold_kv->bso_supplier_id);
            $relation->where('bso_suppliers_products_pay_methods.product_id', $hold_kv->product_id);
            $relation->where('bso_suppliers_products_pay_methods.group_id', request('group_id'));
        });
    }

    public function pay_methods_access_bso(){

        $hold_kv = $this;
        return $this->custom_one(BsoSupplierProductPayMethodAccessBso::class, function ($relation) use ($hold_kv) {
            $relation->where('bso_suppliers_products_pay_methods_access_bso.bso_supplier_id', $hold_kv->bso_supplier_id);
            $relation->where('bso_suppliers_products_pay_methods_access_bso.product_id', $hold_kv->product_id);
            $relation->where('bso_suppliers_products_pay_methods_access_bso.group_id', request('group_id'));
        });
    }

    public function pay_methods_group()
    {

        //СДЕЛАТЬ ПРВЯЗКУ К ГРУППЕ АГЕНТА

        return $this->pay_methods();
    }

}
