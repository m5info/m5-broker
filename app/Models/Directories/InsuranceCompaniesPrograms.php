<?php

namespace App\Models\Directories;

use Illuminate\Database\Eloquent\Model;


class InsuranceCompaniesPrograms extends Model
{
    protected $table = 'insurance_companies_programs';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function program()
    {
        return $this->hasOne(ProductsPrograms::class, 'id', 'product_id');
    }

    public function insurance_company()
    {
        return $this->hasOne(InsuranceCompanies::class, 'id', 'insurance_company_id');
    }

}
