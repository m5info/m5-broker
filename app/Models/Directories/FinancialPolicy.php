<?php

namespace App\Models\Directories;

use App\Helpers\Visible;
use App\Models\Settings\FinancialGroup;
use App\Models\Settings\TypeOrg;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class FinancialPolicy extends Model
{
    protected $table = 'financial_policies';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function groups()
    {
        return $this->belongsToMany(FinancialGroup::class, 'financial_policies_groups_kv', 'financial_policy_id', 'financial_policies_group_id')
            ->withPivot( 'kv_agent', 'kv_parent', 'is_actual');
    }

    public function availableGroups()
    {
        return FinancialGroup::all()->map(function ($group) {

            $kvAgent               = 0;
            $kvParent               = 0;
            $isActual               = 0;

            $relation = $this->groups()->where('financial_policies_group_id', $group->id)->first();

            if ($relation) {
                $kvAgent               = $relation->pivot->kv_agent;
                $kvParent               = $relation->pivot->kv_parent;
                $isActual               = $relation->pivot->is_actual;
            }

            $group->kv_agent                 = $kvAgent;
            $group->kv_parent                = $kvParent;
            $group->is_actual                = $isActual;

            return $group;

        });

    }


    public function product()
    {
        return $this->hasOne(Products::class, 'id', 'product_id');
    }

    public function segments()
    {
        return $this->hasMany(FinancialPolicySegment::class,  'financial_policy_id');
    }


    public function getGroupKV($payment)
    {

        $user_id = $payment->agent_id;



        if(isset($payment->contract) && isset($payment->contract->sales_condition) && $payment->contract->sales_condition != 0){
            $user_id = $payment->manager_id;
        }

        $user_financial_group_id = null;
        if($user_id){
            $user = User::find($user_id);
            $user_financial_group_id = $user->get_agent_financial_policies($payment->contract->sign_date);
        }


        $res = new \stdClass();
        $res->kv_agent = $this->kv_agent;
        $res->kv_parent = $this->kv_parent;

        $group = null;

        if(isset($user_financial_group_id) && (int)$user_financial_group_id > 0){

            $group = $this->groups()
                ->where('id', $user_financial_group_id)
                ->where('financial_group.is_actual', 1)
                ->where('financial_policies_groups_kv.is_actual', 1)
                ->first();

        }



        if($group){
            $res->kv_agent = $group->original['pivot_kv_agent'];
            $res->kv_parent = $group->original['pivot_kv_parent'];
        }


        return $res;

    }





    public static function getFinancialPolicyQuery($id, $bso_supplier_id) {
        return Visible::apply(FinancialPolicy::query()->where('insurance_companies_id', '=', $id)->where('bso_supplier_id', '=', $bso_supplier_id), 'directories', ['agent_id', 'manager_id']);
    }

    public function getUserKV($user_id)
    {

        $user = User::find($user_id);
        $res = new \stdClass();
        $res->kv_agent = $this->kv_agent;
        $res->kv_parent = $this->kv_parent;

        $group = $this->groups()
            ->where('id', $user->financial_group_id)
            ->where('financial_group.is_actual', 1)
            ->first();

        if($group){
            $res->kv_agent = $group->original['pivot_kv_agent'];
            $res->kv_parent = $group->original['pivot_kv_parent'];
        }


        return $res;

    }
}
