<?php

namespace App\Models\Directories;

use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Contracts\Contracts;
use App\Models\Settings\City;
use App\Models\Vehicle\VehicleCategories;
use App\Traits\Models\CustomRelationTrait;
use Illuminate\Database\Eloquent\Model;


class FinancialPolicySegment extends Model{

    use CustomRelationTrait;

    protected $table = 'financial_policies_segments';

    protected $guarded = ['id'];

    public $timestamps = false;

    public $casts = ['data' => 'json'];

    const SEGMENTS_JSON = [
        'osago' => [
            'insurer_type_any' => 0,
            'insurer_type_id' => 0,
            'insurer_location_any' => 0,
            'location_id' => 0,
            'insurer_kt_any' => 0,
            'insurer_kt' => '',
            'contract_type_any' => 0,
            'contract_type_id' => 0,
            'period_any' => 0,
            'period' => 12,
            'kbm_any' => 0,
            'kbm' => '',
            'vehicle_category_id' => 0,
            'vehicle_country_any' => 0,
            'vehicle_country_id' => 1,
            'vehicle_power_any' => 0,
            'vehicle_power_from' => '',
            'vehicle_power_to' => '',
            'has_trailer_any' => 0,
            'has_trailer' => 1,
            'vehicle_age_any' => '',
            'vehicle_age' => '',
            'owner_age_any' => 0,
            'is_multi_drive_any' => 0,
            'owner_age' => '',
            'drivers_age_any' => 0,
            'drivers_min_age' => '',
            'drivers_exp_any' => 0,
            'drivers_min_exp' => '',
        ],
        'kasko' => [],
    ];


    public function bso_supplier(){
        return $this->hasOne(BsoSuppliers::class, 'id', 'bso_supplier_id');
    }

    public function product(){
        return $this->hasOne(Products::class, 'id', 'product_id');
    }

    public function financial_policy(){
        return $this->hasOne(FinancialPolicy::class, 'id', 'financial_policy_id');
    }


    public function insurance_company(){
        return $this->hasOne(InsuranceCompanies::class, 'id', 'insurance_companies_id');
    }

    public function location(){
        $data = (array)\GuzzleHttp\json_decode($this->data);
        return $this->custom_one(City::class, function($relation) use ($data){
            $relation->where('id', $data['location_id']);
        });
    }

    public function categoryTS(){
        $data = (array)\GuzzleHttp\json_decode($this->data);
        return $this->custom_one(VehicleCategories::class, function($relation) use ($data){
            $relation->where('id', $data['vehicle_category_id']);
        });
    }



    public function getDataArray($slug, $data = null){

        $default = [];


        if(isset(self::SEGMENTS_JSON[$slug])){

            $default = (array)self::SEGMENTS_JSON[$slug];
            foreach ($default as $key => $value){
                $default[$key] = (isset($data[$key]))?$data[$key]:$value;
            }

        }

        return $default;


        /*
         $kbm = (isset($data->kbm))?$data->kbm:null;
        if($kbm){
            $kbm = str_replace(' ', "", $kbm);
            $kbm = str_replace(',', ".", $kbm);
            $data->kbm = $kbm;
        }

        $insurer_kt = (isset($data->insurer_kt))?$data->insurer_kt:null;
        if($insurer_kt){
            $insurer_kt = str_replace(' ', "", $insurer_kt);
            $insurer_kt = str_replace(',', ".", $insurer_kt);
            $data->insurer_kt = $insurer_kt;
        }

         */

    }




    public function getTitleAttribute(){

        $data = (array)\GuzzleHttp\json_decode($this->data);
        $title = '';


        if(isset($data['insurer_location_any'])){
            $title .= 'Город ';
            if(isset($data['insurer_kt_any']) && (int)$data['insurer_location_any'] == 1){
                $title .= 'любой - ';
            }else{
                if(isset($data['location_id'])){
                    $title .= (($this->location) ? $this->location->title : "" ). " - ";
                }
            }
        }

        if(isset($data['vehicle_country_any'])){
            $title .= 'Категоря ТС ';
            if(isset($data['vehicle_country_any']) && (int)$data['vehicle_country_any'] == 1){
                $title .= 'любая - ';
            }else{
                if(isset($data['vehicle_country_id'])){
                    $title .= (($this->categoryTS) ? $this->categoryTS->title : "" ). " - ";
                }
            }
        }



        if(isset($data['vehicle_power_any'])){
            $title .= 'Мощность ТС ';
            if(isset($data['vehicle_power_any']) && (int)$data['vehicle_power_any'] == 1){
                $title .= 'Любая - ';
            }else{
                if(isset($data['vehicle_power_from']) && isset($data['vehicle_power_to'])){
                    $title .= "от {$data['vehicle_power_from']} до {$data['vehicle_power_to']} - ";
                }
            }

        }

        if(isset($data['insurer_kt_any'])){
            $title .= 'Коэффициент территории ';
            if(isset($data['insurer_kt_any']) && (int)$data['insurer_kt_any'] == 1){
                $title .= 'любой - ';
            }else{
                if(isset($data['insurer_kt'])){
                    $title .= " = {$data['insurer_kt']} - ";
                }
            }
        }

        if(isset($data['kbm_any'])){
            $title .= 'КБМ ';
            if((int)$data['kbm_any'] == 1){
                $title .= 'любой - ';
            }else{
                if(isset($data['kbm'])){
                    $title .= " <= {$data['kbm']} - ";
                }
            }
        }


        if(isset($data['drivers_age_any'])){
            $title .= 'Водители ';
            if((int)$data['drivers_age_any'] == 1){
                $title .= 'неважно';
            }else{

                $title .= '- Мультидрайв ';
                if(isset($data['is_multi_drive_any']) && (int)$data['is_multi_drive_any'] == 1){
                    $title .= 'Да ';

                    if(isset($data['owner_age_any']) && (int)$data['owner_age_any'] != 1){
                        if(isset($data['owner_age'])){
                            $title .= "- Минимальный возраст собственника {$data['owner_age']} ";
                        }
                    }

                }else {
                    $title .= 'Нет ';

                    if (isset($data['owner_age_any']) && (int)$data['owner_age_any'] != 1) {
                        if (isset($data['drivers_age_any']) && (int)$data['drivers_age_any'] != 1) {
                            if(isset($data['drivers_min_age'])){
                                $title .= "- Минимальный возраст водителей {$data['drivers_min_age']} ";
                            }
                        }
                        if (isset($data['drivers_exp_any']) && (int)$data['drivers_exp_any'] != 1) {
                            if(isset($data['drivers_min_exp'])){
                                $title .= "- Минимальный стаж водителей {$data['drivers_min_exp']} ";
                            }
                        }

                    }
                }
            }
        }


        return $title;
    }



    public function get_product_service($contract){

        $product = $contract->product;
        $program = $contract->getProductOrProgram();
        $bso_supplier = $this->bso_supplier;


        if($product && $bso_supplier){

            //существует сервис контроллер для ск
            if($service_controller = $bso_supplier->get_service_controller()) {

                //у ск есть сервис для этого продукта
                if ($service = $service_controller->get_service($program->slug)) {
                    //сервис реализован как сервис и можем дёргать методы
                    if ($service instanceof ProductServiceInterface) {

                        return $service;

                    }

                }

            }

        }

        return false;
    }


    public function checkSegmentContract($contract_id)
    {
        $contract = Contracts::getContractId($contract_id);

        //ПРОВЕРЯЕМ СЕГМЕНТАЦИЮ



        return true;
    }

}
