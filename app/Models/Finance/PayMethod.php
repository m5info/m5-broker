<?php

namespace App\Models\Finance;


use Illuminate\Database\Eloquent\Model;

class PayMethod extends Model
{

    public $table = 'pay_methods';
    public $timestamps = false;
    public $guarded = [];

    const ACCESS_BSO = [
        0 => 'Все',
        1 => 'Бумажные',
        2 => 'Электронные'
    ];

    const KEY_TYPE = [
        0 => 'Квитанция БСО',
        1 => 'ОФД',
        2 => 'Без подтверждения',
        3 => 'Счет', /* ? */
        4 => 'Платежная страница'
    ];


    public static function searchMethod($payment_type, $payment_flow)
    {
        $method = PayMethod::where('is_actual', 1);
        $method->where('payment_type', $payment_type);
        $method->where('payment_flow', $payment_flow);

        return $method->get()->last();

    }

}