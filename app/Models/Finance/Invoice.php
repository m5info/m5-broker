<?php

namespace App\Models\Finance;

use App\Classes\Export\TagModels\Finance\TagInvoice;
use App\Domain\Samplers\Contracts\TabsVisibility;
use App\Helpers\Visible;
use App\Models\Contracts\Orders;
use App\Models\Contracts\Payments;
use App\Models\Organizations\Organization;
use App\Models\Settings\UserBalanceSettings;
use App\Models\User;
use App\Models\File;
use App\Models\Users\UsersBalance;
use App\Traits\Models\ActiveConstTrait;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

    use ActiveConstTrait;
    protected $table = 'invoices';
    protected $guarded = ['id'];

    const FILES_DOC = 'invoice/';

    const TAG_MODEL = TagInvoice::class;


    const STATUSES = [
        1 => 'Не оплачен',
        2 => 'Оплачен',
        3 => 'Ожидание',
    ];

    const TYPES = [
        'cash' => 'Наличный',
        'cashless' => 'Безналичный Брокер',
        'card' => 'Безналичный Карта',
        'sk' => 'Страховая',
        'cashless_sk' => 'Карта Брокер'
    ];

    const CREATE_TYPES = [
        1 =>'Автоматическое создание счёта',
        2 =>'Создание счёта на одно юр. лицо',
        3 =>'Создание счёта на несколько юр.лиц'
    ];

    const TYPE_INVOICE_PAYMENT = [
        1 => 'Наличный',
        2 => 'Баланс',
        3 => 'Безнал Брокер',
        4 => 'Безнал СК',
        5 => 'Карта Брокер СК',
        6 => 'Баланс руководителя',
    ];

    const TYPE_INVOICE_VAL = [
        1 => 'cash',
        2 => 'cash',
        3 => 'cashless',
        4 => 'sk',
        5 => 'cashless_sk',
        6 => 'cash',
    ];



    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function agent(){
        return $this->hasOne(User::class, 'id', 'agent_id');
    }

    public function invoice_payment_user(){
        return $this->hasOne(User::class, 'id', 'invoice_payment_user_id');
    }

    public function payments(){
        return $this->hasMany(Payments::class, 'invoice_id', 'id')->with('bso','contract');
    }

    public function org(){
        return $this->hasOne(Organization::class, 'id', 'org_id');
    }

    public function doc(){
        return $this->hasOne(File::class, 'id', 'file_id');
    }


    public function getInvoceDefaultPaymentType(){
        if($this->type == 'sk') return 4;
        if($this->type == 'cashless' || $this->type == 'card') return 3;
        return 1;
    }

    public static function getInvoiceId($id){
        //ПРОВЕРКА ДОСТУПА
        $invoice = Invoice::where("id", $id);

        return $invoice->get()->first();
    }

    public function getInfoSum()
    {
        $result = new \stdClass();
        $result->total = 0;
        $result->total_kv_agent = 0;
        $result->total_sum = 0;

        foreach($this->payments as $key => $payment)
        {
            $result->total += $payment->payment_total;
            $result->total_kv_agent += $payment->financial_policy_kv_agent_total;
            $result->total_sum += $payment->getPaymentAgentSum();
        }

        return $result;
    }

    //НАЛИЧНЫЕ
    public function setPaymentChash($invoice_payment_total, $invoice_id = 0)
    {
        $result = new \stdClass();
        $result->state = 0;
        $result->error = '';


        $invoice_info = $this->getInfoSum();

        $type_invoice_payment = Invoice::TYPE_INVOICE_PAYMENT[$this->type_invoice_payment_id].'';




        if(getFloatFormat($invoice_payment_total)>=getFloatFormat($invoice_info->total_sum)){

            //ПРИХОД В КАССУ
            $cashbox = auth()->user()->cashbox;
            $link = "#{$this->id}";



            $cashbox->setTransaction(0, 0, $invoice_info->total_sum, date("Y-m-d H:i:s"), "Оплата счета {$link} на сумму ".titleFloatFormat($invoice_info->total_sum).", ".$type_invoice_payment,auth()->id(), $this->agent_id ? $this->agent_id : null, 0, $invoice_id);

            if(getFloatFormat($invoice_payment_total)>getFloatFormat($invoice_info->total_sum)){


                $delta = $invoice_payment_total-$invoice_info->total_sum;
                if($delta > 0){//ЗАЧИСЛЯЕМ ОСТАТОК НА БАЛАНС

                    $_b = UserBalanceSettings::where('isset_for_kv', '=', 1)->where('is_actual', 1)->get()->first();
                    $agent_balance = $this->agent->getBalance($_b->id);
                    $link = "#{$this->id}";
                    $purpose_payment = "Оплата счета {$link} на сумму ".titleFloatFormat($invoice_info->total_sum).", приход в кассу ".titleFloatFormat($invoice_payment_total)." , сдача ".titleFloatFormat($delta).', '.$type_invoice_payment;

                    $agent_balance->setTransactions(1, 0, getFloatFormat($delta), date('Y-m-d H:i:s'), $purpose_payment, 0 ,0 ,0 ,$invoice_id);
                }
            }



            $result->state = $this->setPaymentInvoice($invoice_payment_total);

            //Зачисляем КВ НА БАЛАНС и Руководителю
            $this->sendKvBalance(false);

        }else{
            $result->error = 'Недостаточно средств!';
        }

        return $result;

    }

    //БАЛАНС
    public function setPaymentBalance($invoice_payment_balance, $invoice_id = 0)
    {
        $result = new \stdClass();
        $result->state = 0;
        $result->error = '';

        $invoice_info = $this->getInfoSum();
        $_b = UsersBalance::find($invoice_payment_balance);
        $agent_balance = $this->agent->getBalance($_b->balance_id);

        $type_invoice_payment = $agent_balance->type_balanse->title;

        $payment_total = getFloatFormat($invoice_info->total_sum);
        $agent_balance->balance = getFloatFormat($agent_balance->balance);

        if($agent_balance->balance >= $payment_total){

            $link = "#{$this->id}";

            $purpose_payment = "Оплата счета {$link} на сумму ".titleFloatFormat($payment_total).', '.$type_invoice_payment;
            $agent_balance->setTransactions(0, 1, $payment_total, date('Y-m-d H:i:s'), $purpose_payment, 0, 0, 0, $invoice_id);
            $result->state = $this->setPaymentInvoice($payment_total, $_b->balance_id);

        }else{
            $result->error = 'Недостаточно средств!';
        }

        return $result;
    }

    //БАЛАНС Руководителя
    public function setPaymentBalanceParent($invoice_payment_balance, $invoice_id = 0)
    {
        $result = new \stdClass();
        $result->state = 0;
        $result->error = '';

        $invoice_info = $this->getInfoSum();
        $_b = UsersBalance::find($invoice_payment_balance);
        $agent_balance = $this->agent->parent->getBalance($_b->balance_id);
        $type_invoice_payment = $agent_balance->type_balanse->title;


        $payment_total = getFloatFormat($invoice_info->total_sum);
        $agent_balance->balance = getFloatFormat($agent_balance->balance);

        if($agent_balance->balance >= $payment_total){

            $link = "#{$this->id}";

            $purpose_payment = "Оплата счета {$link} на сумму ".titleFloatFormat($payment_total).', '.$type_invoice_payment.' - '.$this->agent->name;
            $agent_balance->setTransactions(0, 1, $payment_total, date('Y-m-d H:i:s'), $purpose_payment, 0, 0, 0, $invoice_id);

            $result->state = $this->setPaymentInvoice($payment_total, $_b->balance_id);

        }else{
            $result->error = 'Недостаточно средств!';
        }

        return $result;
    }

    //БЕЗНАЛ БРОКЕР
    public function setPaymentBrokerNonChash($invoice_payment_total, $invoice_id = 0)
    {

        //dd("БЕЗНАЛ БРОКЕР");

        $invoice_info = $this->getInfoSum();

        //Если прямые продажи и сумма меньше тот минусуем

        //Если боольше то на баланс

        //КВ не зачистять

        $total = $invoice_info->total_sum;


        if($invoice_payment_total>=$total) { // ЕСЛИ ПРИШЛО СТОЛЬКО ИЛИ БОЛЬШЕ СКОЛЬКО ПО СЧЕТУ

            $delta = $invoice_payment_total - $total;
            if ($delta > 0) {//ЗАЧИСЛЯЕМ ОСТАТОК НА БАЛАНС

                $_b = UserBalanceSettings::where('isset_for_kv', '=', 1)->where('is_actual', 1)->get()->first();
                $agent_balance = $this->agent->getBalance($_b->id);
                $link = "#{$this->id}";
                $purpose_payment = "Оплата счета #{$link} на сумму " . titleFloatFormat($total) . ", приход безнал " . titleFloatFormat($invoice_payment_total) . " , сдача " . titleFloatFormat($delta);
                $agent_balance->setTransactions(0, 0, getFloatFormat($delta), date('Y-m-d H:i:s'), $purpose_payment, 0, 0, 0, $invoice_id);

            }



        }elseif ($invoice_payment_total < $total){ // ЕСЛИ ПРИШЛО МЕНЬШЕ СКОЛЬКО ПО СЧЕТУ

            $_b = UserBalanceSettings::where('isset_for_kv', '=', 1)->where('is_actual', 1)->get()->first();
            $agent_balance = $this->agent->getBalance($_b->id);
            $link = "#{$this->id}";
            $purpose_payment = "Зачисление на баланс. Оплата счета {$link} на сумму ".titleFloatFormat($total).", приход безнал ".titleFloatFormat($invoice_payment_total)." . Недостаточно средств:  ".titleFloatFormat($total-$invoice_payment_total);

            $_set_data =  date('Y-m-d H:i:s');


            if($agent_balance->setTransactions(0, 0, getFloatFormat($invoice_payment_total), $_set_data, $purpose_payment, 0, 0, 0, $invoice_id)){

                $link = "#{$this->id}";
                $purpose_payment = "Оплата счета {$link} на сумму ".titleFloatFormat($total)." приход безнал";
                $agent_balance->setTransactions(0, 1, $total, $_set_data, $purpose_payment, 0, 0, 0, $invoice_id);

            }

        }


        $result = new \stdClass();
        $result->state = 0;
        $result->error = '';

        //$invoice_payment_total
        if($this->setPaymentInvoice($invoice_payment_total)){
            //Зачисляем КВ НА БАЛАНС и Руководителю
            $this->sendKvBalance(true);
            $result->state = 1;
        }

        $this->sendInformalDiscountBalance();

        return $result;

    }



    //БЕЗНАЛ
    public function setPaymentNonChash($invoice_payment_total = 0, $setBalance = true)
    {
        $result = new \stdClass();
        $result->state = 0;
        $result->error = '';

        $invoice_info = $this->getInfoSum();
        $invoice_payment_date = date("Y-m-d H:i:s");


        $this->invoice_payment_total = ($invoice_payment_total>0)?$invoice_payment_total:$invoice_info->total;
        $this->invoice_payment_user_id = auth()->id();
        $this->invoice_payment_date = $invoice_payment_date;

        if($setBalance == true){
            //Зачисляем КВ НА БАЛАНС и Руководителю
            $this->sendKvBalance($setBalance);
        }else{
            foreach ($this->payments as $payment)
            {
                if($this->type == 'sk'){//Безналичный || Страховая
                    $payment->invoice_payment_total = $payment->payment_total;
                    $payment->invoice_payment_date = $invoice_payment_date;
                    $payment->statys_id = 1;
                    $payment->save();
                }
            }
        }




        $this->status_id = 2;
        $this->save();
        $result->state = 1;

        $this->sendInformalDiscountBalance();

        return $result;
    }



    public function setPaymentInvoice($invoice_payment_total, $invoice_payment_balance = null)
    {
        $invoice_payment_date = date("Y-m-d H:i:s");
        foreach ($this->payments as $payment)
        {
            $payment->invoice_payment_total = $payment->getPaymentAgentSum();
            $payment->invoice_payment_date = $invoice_payment_date;
            $payment->statys_id = 1;
            $payment->save();
        }


        $this->invoice_payment_total = $invoice_payment_total;
        $this->invoice_payment_balance_id = $invoice_payment_balance;
        $this->invoice_payment_user_id = auth()->id();
        $this->invoice_payment_date = $invoice_payment_date;

        $this->status_id = 2;
        $this->save();

        return 1;
    }

    public function refreshInvoice()
    {
        $invoice_payment_total = 0;

        foreach ($this->payments as $payment)
        {
            if ($this->status_id != 2){
                $payment->invoice_payment_total = $payment->getPaymentAgentSum();
                $payment->save();
            }

            $invoice_payment_total += $payment->invoice_payment_total;

        }

        $this->invoice_payment_total = $invoice_payment_total;
        $this->save();
        return 1;
    }



    public function sendKvBalance($setAgentKv = true)
    {


        $link = "#{$this->id}";

        $invoice = $this;

        if ($invoice->type_invoice_payment_id == 2){
            $type_invoice_payment = UserBalanceSettings::query()->where('id', $invoice->invoice_payment_balance_id)->first()->title.',';
        }else{

            $type_invoice_payment = Invoice::TYPE_INVOICE_PAYMENT[$invoice->type_invoice_payment_id].',';
        }


        $purpose_payment = "Оплата счета {$link}, $type_invoice_payment комиссия ";

        $_b = UserBalanceSettings::where('isset_for_kv', '=', 1)->where('is_actual', 1)->get()->first();
        $invoice_payment_date = $this->invoice_payment_date;



        foreach ($this->payments as $payment)
        {

            $agent_balance = $this->agent->getBalance($_b->id);

            $parent_balance = null;
            $curator_balance = null;


            if($this->type == 'cash'){//Наличный
                if($payment->contract->sales_condition != 0 && $payment->manager) { // Агента через организацию
                    $setAgentKv = true;
                    $agent_balance = $payment->manager->getBalance($_b->id);
                }
            }

            if($this->type == 'cashless' || $this->type == 'card' || $this->type == 'sk'){//Безналичный || Страховая
                if($payment->contract->sales_condition != 0 && $payment->manager) { // НЕ Агента
                    $setAgentKv = true;
                    $agent_balance = $payment->manager->getBalance($_b->id);
                }
            }


            if($payment->contract->sales_condition != 1 && $setAgentKv == true)
            {

                if($agent_balance && $payment->financial_policy_kv_agent_total > 0){

                    $agent_balance->setTransactions(0, 0, getFloatFormat($payment->financial_policy_kv_agent_total), $invoice_payment_date, $purpose_payment.titleFloatFormat($payment->financial_policy_kv_agent_total), 0, 0, 0, $this->id);

                }

            }




            if($payment->parent_agent){
                $parent_balance = $payment->parent_agent->getBalance($_b->id);
            }

            if($parent_balance && $payment->financial_policy_kv_parent_total > 0){
                $parent_balance->setTransactions(0, 0, getFloatFormat($payment->financial_policy_kv_parent_total), $invoice_payment_date, $purpose_payment.titleFloatFormat($payment->financial_policy_kv_parent_total), 0, 0, 0, $this->id);
            }


            if($agent_balance && $curator = $agent_balance->agent->curator){
                $curator_balance = $curator->getBalance($_b->id);
                if($curator_balance && $payment->financial_policy && getFloatFormat($payment->financial_policy->partnership_reward_1) > 0){

                    $curator_balance->setTransactions(0, 0, getFloatFormat($payment->financial_policy->partnership_reward_1), $invoice_payment_date, $purpose_payment.titleFloatFormat($payment->financial_policy_kv_parent_total), 0, 0, 0, $this->id);

                }
            }

            if($this->type == 'sk'){//Безналичный || Страховая
                $payment->invoice_payment_total = $payment->payment_total;
                $payment->invoice_payment_date = $invoice_payment_date;
                $payment->statys_id = 1;
                $payment->save();
            }


        }

        return true;
    }

    public static function getInvoicesCountArr(){

        $result = [];

        $tabs_visibility = TabsVisibility::get_invoices_tab_visibility();

        if(isset($tabs_visibility[0]['view']) && $tabs_visibility[0]['view'] == 1){
            $result[0] = ['title' => 'Все', 'type' => 'all', 'count' => Invoice::getInvoicesQuery()->where('status_id', 1)->whereIn('type', ['cash', 'cashless', 'card', 'sk'])->count()];
        }

        if(isset($tabs_visibility[1]['view']) && $tabs_visibility[1]['view'] == 1){
            $result[1] = ['title' => 'Наличные', 'type' => 'cash', 'count' => Invoice::getInvoicesQuery()->where('status_id', 1)->whereIn('type', ['cash'])->count()];
        }

        if(isset($tabs_visibility[2]['view']) && $tabs_visibility[2]['view'] == 1){
            $result[2] = ['title' => 'Безналичные Брокер', 'type' => 'cashless', 'count' => Invoice::getInvoicesQuery()->where('status_id', 1)->whereIn('type', ['cashless'])->count()];
        }

        if(isset($tabs_visibility[2]['view']) && $tabs_visibility[2]['view'] == 1){
            $result[2] = ['title' => 'Безналичные Карта', 'type' => 'card', 'count' => Invoice::getInvoicesQuery()->where('status_id', 1)->whereIn('type', ['card'])->count()];
        }

        if(isset($tabs_visibility[3]['view']) && $tabs_visibility[3]['view'] == 1){
            $result[3] = ['title' => 'СК', 'type' => 'sk', 'count' => Invoice::getInvoicesQuery()->where('status_id', 1)->whereIn('type', ['sk'])->count()];
        }

        return $result;
    }

    public static function getInvoicesQuery() {
        return Visible::apply(Invoice::query(), 'cashbox', ['agent_id', 'manager_id']);
    }



    public function sendInformalDiscountBalance()
    {
        foreach ($this->payments as $payment)
        {
            $contract = $payment->contract;
            if((int)$contract->sales_condition == 1){
                if($contract->get_payments_type(2)->sum('payment_total') == 0){
                    $informal_discount_total = $payment->informal_discount_total;
                    if($payment->manager_id > 0 && $informal_discount_total > 0){
                        $paymentPay = new Payments();
                        $paymentPay->type_id = 2;
                        $paymentPay->bso_id = $payment->bso->id;
                        $paymentPay->agent_id = $payment->manager_id;
                        $paymentPay->manager_id = $payment->manager_id;
                        $paymentPay->parent_agent_id = $payment->parent_agent_id;
                        $paymentPay->contract_id = $contract->id;
                        $paymentPay->set_balance = 1;
                        $paymentPay->invoice_id = null;
                        $paymentPay->org_id =  $payment->org_id;
                        $paymentPay->payment_total =  $informal_discount_total;
                        $paymentPay->comments = "Автоматическая выплата неофицальной скидки {$payment->bso->bso_title}";
                        $paymentPay->save();
                        $paymentPay->savePaymentData($paymentPay);


                    }
                }
            }
        }

        return true;
    }

}
