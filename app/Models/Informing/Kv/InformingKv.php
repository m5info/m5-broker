<?php

namespace App\Models\Informing\Kv;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;

class InformingKv extends Model
{
    protected $table = 'informing_kv';

    const FILES_DOC = 'informing/kv';

    public $timestamps = true;

    public function file()
    {
        return $this->hasOne(File::class, 'id', 'file_id');
    }

}