<?php

namespace App\Models\Informing\News;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public $table = 'news';

    public $guarded = ['id'];

    const FILES_DOC = 'news/docs';

    const VISIBILITY = [
      1 => 'Все',
      2 => 'Менеджеры',
      3 => 'Агенты'
    ];

    public function docs() {
        return $this->belongsToMany(File::class, 'news_documents', 'news_id', 'file_id');
    }

    public function document($document_id) {
        return $this->hasOne(NewsDocuments::class, 'news_id', 'id')
            ->where("news_id", $document_id)
            ->get()->first();
    }

    public static function getIndexNews(){
        $news = News::query();

        if (auth()->user()->is('manager') && auth()->user()->is('agent')){
            $news->whereIn('visibility', [1,2,3]);
        }else if (auth()->user()->is('manager') && !auth()->user()->is('agent')){
            $news->whereIn('visibility', [1,2]);
        }else if (auth()->user()->is('agent') && !auth()->user()->is('manager')){
            $news->whereIn('visibility', [1,3]);
        }else{
            $news->where('visibility', 1);
        }

        $news->orderBy('updated_at', 'desc')->limit(20);

        return $news;
    }
}