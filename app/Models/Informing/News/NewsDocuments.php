<?php

namespace App\Models\Informing\News;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;


class NewsDocuments extends Model
{

    const FILES_DOC = 'news/docs';

    protected $table = 'news_documents';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function file()
    {
        return $this->hasOne(File::class, 'id', 'file_id');
    }

}
