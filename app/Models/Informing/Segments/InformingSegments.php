<?php

namespace App\Models\Informing\Segments;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;

class InformingSegments extends Model
{
    protected $table = 'informing_segments';

    const FILES_DOC = 'informing/segments';

    public $timestamps = true;

    public function file()
    {
        return $this->hasOne(File::class, 'id', 'file_id');
    }

}