<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model {

    const URL = '/files';
    const MOB = '/mobile-file';
    const PREVIEW_URL = '/thumbs';

    protected $table = 'files';
    protected $guarded = ['id'];

    public function getPathAttribute() {
        return $this->folder . "/" . $this->name . "." . $this->ext;
    }

    public function getUrlAttribute() {
        return self::URL . "/" . $this->name;
    }

    public function getPreviewAttribute() {
        return self::PREVIEW_URL . "/" . $this->name;
    }

    public function getFolderWithHostAttribute() {
        return $this->host . '/' . $this->folder;
    }

    public function getPathWithHostAttribute() {
        return $this->folder_with_host . "/" . $this->name . "." . $this->ext;
    }

    public function getPrefix() {
        return [
            substr($this->name, 4, 2),
            substr($this->name, 6, 2),
        ];
    }

    public function getUrlMobileAttribute() {
        return self::MOB . "/" . $this->name . '.' . $this->ext;
    }

}
