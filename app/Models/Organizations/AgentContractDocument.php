<?php

namespace App\Models\Organizations;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;

class AgentContractDocument extends Model
{
    const FILES_DOC = 'organizations/agent_contract/docs';

    protected $table = 'agent_contract_docs';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function file() {
        return $this->hasOne(File::class, 'id', 'file_id');
    }
}
