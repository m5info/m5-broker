<?php

namespace App\Models\Organizations;

use App\Classes\Export\TagModels\BSO\TagBsoItem;
use App\Classes\Export\TagModels\Directories\Organizations\TagAgentContract;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\FNS;
use App\Models\Contracts\Payments;
use App\Models\Directories\BsoSuppliers;
use App\Models\File;
use App\Models\Orders\Inspection;
use App\Models\Orders\InspectionOrders;
use App\Models\Security\Security;
use App\Models\Settings\TypeOrg;
use App\Models\User;
use App\Traits\Models\ActiveConstTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Settings\Organization
 *
 * @property integer $id
 * @property string $title
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereTitle($value)
 * @mixin \Eloquent
 * @property integer $next_act
 * @property string $default_purpose_payment
 * @property string $inn
 * @property float $limit_year
 * @property float $spent_limit_year
 * @property integer $is_actual
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereNextAct($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereDefaultPurposePayment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereInn($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereLimitYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereSpentLimitYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereIsActual($value)
 */
class Organization extends Model
{

    use ActiveConstTrait;

    const TAG_MODEL = TagAgentContract::class;

    const FILES_DOC = 'organizations/docs';

    const ASSOCIATION_TYPE = [
        0 => 'Нет',
        1 => 'Ассоциация',
        2 => 'Член Ассоциации',
        3 => 'Партнер ассоциации',
    ];

//    const REQUEST_TYPE = [
//        0 => 'Income', //получение денежных средств от покупателя;
//        1 => 'IncomeReturn', //возврат денежных средств, полученных от покупателя
//        2 => 'IncomePrepayment', //авансовый платеж от покупателя
//        3 => 'IncomeReturnPrepayment', //возврат аванса
//        4 => 'IncomeCorrection', //чек коррекции/приход
//        5 => 'BuyCorrection', //чек коррекции/расход
//        6 => 'Expense', //выдача денежных средств покупателю
//        7 => 'ExpenseReturn', //возврат денежных средств, выданных покупателю
//    ];
//
//    const NDS_TYPE = [
//        0 => 'Vat10', //налог на добавленную стоимость (НДС) 10%;
//        1 => 'Vat18', //НДС 18%
//        2 => 'Vat20', //НДС 20%
//        3 => 'Vat0', //НДС 0%
//        4 => 'VatNo', //НДС не облагается
//        5 => 'CalculatedVat10110', //вычисленный НДС 10% от 110% суммы
//        6 => 'CalculatedVat18118', //вычисленный НДС 18% от 118% суммы
//        7 => 'CalculatedVat20120', //вычисленный НДС 20% от 120% суммы
//    ];
//
//    const TAXATION_SYSTEM_TYPE = [
//        0 => 'Common', //общая система налогообложения
//        1 => 'SimpleIn', //упрощенная система налогообложения (доход)
//        2 => 'SimpleInOut', //упрощенная  система  налогообложения  (доход  минус расход)
//        3 => 'Unified', //единый налог на вмененный доход
//        4 => 'UnifiedAgricultural', //единый сельскохозяйственный налог
//        5 => 'Patent', //патентная система налогообложения
//    ];

    const REQUEST_TYPE = [
        1 => 'получение денежных средств от покупателя', //Income
        2 => 'возврат денежных средств, полученных от покупателя', //IncomeReturn
        3 => 'авансовый платеж от покупателя', //IncomePrepayment
        4 => 'возврат аванса', //IncomeReturnPrepayment
        5 => 'чек коррекции/приход', //IncomeCorrection
        6 => 'чек коррекции/расход', //BuyCorrection
        7 => 'выдача денежных средств покупателю', //Expense
        8 => 'возврат денежных средств, выданных покупателю', //ExpenseReturn
    ];

    const NDS_TYPE = [
        1 => 'налог на добавленную стоимость (НДС) 10%', //Vat10
        2 => 'НДС 18%', //Vat18
        3 => 'НДС 20%', //Vat20
        4 => 'НДС 0%', //Vat0
        5 => 'НДС не облагается', //VatNo
        6 => 'вычисленный НДС 10% от 110% суммы', //CalculatedVat10110
        7 => 'вычисленный НДС 18% от 118% суммы', //CalculatedVat18118
        8 => 'вычисленный НДС 20% от 120% суммы', //CalculatedVat20120
    ];

    const TAXATION_SYSTEM_TYPE = [
        1 => 'общая система налогообложения', //Common
        2 => 'упрощенная система налогообложения (доход)', //SimpleIn
        3 => 'упрощенная  система  налогообложения  (доход  минус расход)', //SimpleInOut
        4 => 'единый налог на вмененный доход', //Unified
        5 => 'единый сельскохозяйственный налог', //UnifiedAgricultural
        6 => 'патентная система налогообложения', //Patent
    ];


    const CASHBOX_API_TYPE = [
        1 => 'ОФД Ферма'
    ];

    protected $table = 'organizations';

    protected $guarded = ['id'];

    public $timestamps = false;


    public function bso_supplier(){
        return $this->hasMany(BsoSuppliers::class, 'purpose_org_id', 'id');
    }

    public function scans()
    {
        return $this->belongsToMany(File::class, 'org_scans', 'org_id', 'file_id');
    }

    public function bank_account()
    {
        return $this->hasMany(OrgBankAccount::class, 'org_id');
    }

    public function org_type() {
        return $this->hasOne(TypeOrg::class, 'id', 'org_type_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'organization_id');
    }

    public function points_departments()
    {
        return $this->hasMany(PointsDepartments::class, 'organization_id', 'id');
    }

    public function agent_contracts()
    {
        return $this->belongsToMany(File::class, 'agent_contract_docs', 'org_id', 'file_id');
    }

    public function agent_contracts_docs()
    {
        return $this->hasMany(AgentContractDocument::class, 'org_id');
    }

    public function ofd_cashbox()
    {
        return $this->hasMany(FNS::class, 'org_id')->orderBy('create_date', 'desc');
    }

    public function security_service() {
        return $this->hasOne(Security::class, 'object_id')->where('type_inquiry' , '=', Security::TYPE_INQUIRY_ORG)->orderBy('created_at', 'desc');
    }

    public function parent_user() {
        return $this->hasOne(User::class, 'id','parent_user_id');
    }

    public function getOrgParentUser($user_id){
        return $this::where('parent_user_id', $user_id);
    }

    public function getListOrg($type = 0){
        return $this::getOrganizationPermissions($type)->get()->pluck('title', 'id')->prepend('Нет', 0);
    }

    public function getOrganizationPermissions($type = 0){
        $org = Organization::where('is_actual', 1);//getALLOrg();
        $org->where('is_delete', 0);
        if($type>0 && $type != 100) return $org->where('org_type_id', $type);
        elseif($type==100) return Organization::where('org_type_id', 0);
        else  return $org;

    }

    public function getInspectionOrders(){
        return $this->hasMany(Inspection::class, 'processing_org_id');
    }

    public function getInspectionOrdersAcceptAll(){
        return $this::getInspectionOrders()->where('status_id', 7);
    }

    public function getInspectionOrdersAcceptNotReport(){
        return $this::getInspectionOrdersAcceptAll()->where('status_id', 7)->whereNull('inspection_orders_report_id');
    }

    public function getInspectionOrdersAcceptNotReportPayments(){

        $org_id = $this->id;

        $payment = Payments::query();
        $payment->whereIn('payments.contract_id', function($query2) use ($org_id)
        {
            $query2->select(\DB::raw('contracts.id'))
                ->from('contracts')
                ->whereIn('contracts.inspection_id', function($query) use ($org_id)
                {
                    $query->select(\DB::raw('inspection_orders.id'))
                        ->from('inspection_orders')
                        ->where('inspection_orders.status_id', 7)
                        ->where('inspection_orders.processing_org_id', $org_id)
                        ->whereNull('inspection_orders.inspection_orders_report_id');
                });

        });

        return $payment;
    }


    public function types()
    {
        return $this->hasOne(TypeOrg::class, 'id', "org_type_id");
    }

    public static function getOrgProvider(){
        $org = Organization::getALLOrg();
        $org->where('is_actual', '=', '1')
            ->whereIn('org_type_id', function($query)
        {
            $query->select(\DB::raw('type_org.id'))
                ->from('type_org')
                ->where('type_org.is_provider', 1);

        });
        return $org;
    }


    public static function getIdOrg($id){
        $org = Organization::getALLOrg();
        $org->where('id', $id);
        return $org->get()->last();
    }

    public static function getALLOrg($org_type_id = 0){

        $organization = Organization::query();
        $organization->where('is_delete', 0);

        $user = auth()->user();
        $visibility_obj = $user->role->rolesVisibility(2)->visibility;
        if($visibility_obj == 0){
            //$organization->where('id', '!=', '0');
        }elseif ($visibility_obj == 1){
            //
            //$organization->where('id', $user->organization->id);
            //$organization->where('visibility_obj', 'like', "%:".auth()->id().":%");

        }elseif ($visibility_obj == 2){
            $organization->where('parent_user_id', auth()->id());
        }elseif ($visibility_obj == 3){



            $organization->whereIn('parent_user_id', function($query)
            {

                $query->select(\DB::raw('users.id'))
                    ->from('users')
                    ->where('parent_id', '=', auth()->id())
                    ->orWhere('curator_id', '=', auth()->id());

            });





        }

        if($org_type_id > 0){
            $organization->where('org_type_id', $org_type_id);
        }

        return $organization;
    }


    public function saveData($request)
    {
        $organization = $this;


        if(auth()->user()->hasPermission('directories', 'organizations_edit') || auth()->user()->id == $organization->parent_user_id){
            $organization->fact_address             = request('fact_address', $organization->fact_address);
            $organization->fact_address_lng         = request('fact_address_lng', $organization->fact_address_lng);
            $organization->fact_address_wth         = request('fact_address_wth', $organization->fact_address_wth);
            $organization->phone                    = request('phone', $organization->phone);
            $organization->email                    = request('email', $organization->email);
            $organization->comment                  = request('comment', $organization->comment);
            $organization->user_contact_title       = request('user_contact_title', $organization->user_contact_title);
        }

        if(auth()->user()->hasPermission('directories', 'organizations_edit')){

            $organization->is_actual                = (int)request('is_actual', $organization->is_actual);
            $organization->do_inspection            = (int)request('do_inspection');
            $organization->city_id                  = (int)request('city_id');
            $organization->is_agent_contract        = (int)request('is_agent_contract', 0);


            $organization->org_type_id              = (int)request('org_type_id', $organization->org_type_id);
            $organization->title                    = request('title', $organization->title);

            $organization->inn                      = request('inn', $organization->inn);
            $organization->kpp                      = request('kpp', $organization->kpp);
            $organization->okud                      = request('okud', $organization->okud);
            $organization->okpo                      = request('okpo', $organization->okpo);
            $organization->title_doc                = request('title_doc', $organization->title_doc);
            $organization->general_manager          = request('general_manager', $organization->general_manager);
            $organization->general_accountant          = request('general_accountant', $organization->general_accountant);
            $organization->address                  = request('address', $organization->address);


            $organization->parent_org_id            = (int)request('parent_org_id', $organization->parent_org_id);
            $organization->parent_user_id           = (int)request('parent_user_id', $organization->parent_user_id);


            $organization->nds_type_id              = (int)request('nds_type_id', $organization->nds_type_id);
            $organization->request_type_id          = (int)request('request_type_id', $organization->request_type_id);
            $organization->taxation_system_type_id  = (int)request('taxation_system_type_id', $organization->taxation_system_type_id);



            $organization->cashbox_api_type_id      = (int)request('cashbox_api_type_id', $organization->cashbox_api_type_id);
            $organization->cashbox_api_login        = request('cashbox_api_login', $organization->cashbox_api_login);
            $organization->cashbox_api_pass         = request('cashbox_api_pass', $organization->cashbox_api_pass);
            $organization->cashbox_api_token        = request('cashbox_api_token', $organization->cashbox_api_token);

            $organization->save();


        }

        $organization->save();

        return $organization->id;
    }

}
