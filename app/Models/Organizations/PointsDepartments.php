<?php

namespace App\Models\Organizations;

use Illuminate\Database\Eloquent\Model;

class PointsDepartments extends Model
{
    protected $table = 'points_departments';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }
}
