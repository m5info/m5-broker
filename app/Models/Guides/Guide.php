<?php

namespace App\Models\Guides;

use Illuminate\Database\Eloquent\Model;

class Guide extends Model
{

    protected $table = 'guides_blocks';
    protected $guarded = ['id'];

    public static function getInGroups(){
        $in =[];
        foreach (Guide::all() as  $item )

            if(!isset($in[$item->group_id]))
                $in[$item->group_id] = 1;
            else
                $in[$item->group_id] +=1;

        return \GuzzleHttp\json_encode($in);
    }

    public static function getGuidesListByGroup(){

        $group = Guide::getGroup();
        $guides = false;
        if($group)
            $guides = Guide::where('group_id',$group)->count();
     ;
        if($guides)
            return  [
                'guides' =>'Руководство ( '.$guides.' ) ',
                'group' => $group
            ];
        else
            return false;
    }

    public static function getGroup(){
        $group = explode('/',request()->getPathInfo());
        return $group[2] ?? false;
    }
}
