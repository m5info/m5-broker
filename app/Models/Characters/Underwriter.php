<?php

namespace App\Models\Characters;


use App\Models\User;
use App\Scopes\UnderwriterScope;

class Underwriter extends User{

    public $table = 'users';

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new UnderwriterScope);
    }


}