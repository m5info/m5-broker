<?php

namespace App\Models\Characters;


use App\Models\Contracts\Contracts;
use App\Models\User;
use App\Scopes\UnderfillerScope;

class Underfiller extends User{

    public $table = 'users';

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new UnderfillerScope());
    }


    public function complete_contracts(){
        return $this->hasMany(Contracts::class, 'underfiller_id')->where('underfiller_check', 1);
    }

}