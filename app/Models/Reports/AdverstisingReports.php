<?php

namespace App\Models\Reports;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class AdverstisingReports extends Model
{
    protected $table = 'reports_advertising';
    public $guarded = ['id'];

    public function reports() {
        return $this->hasMany(ReportOrders::class, 'advertising_id');
    }

    public function create_user() {
        return $this->hasOne(User::class, 'id', 'create_user_id');
    }

    public static function recount_payments($adverstising_id){

        $adverstising = AdverstisingReports::find($adverstising_id);

        $report_pay_income = 0;
        $report_pay_outcome = 0;

        foreach($adverstising->reports as $report){
            $report_pay_income += $report->report_payment_sums->where('type_id', 1)->sum('amount'); // приход
            $report_pay_outcome += $report->report_payment_sums->where('type_id', 0)->sum('amount'); // расход
        }

        $payment_total = $adverstising->reports->sum('payment_total'); // СП
        $bordereau_total = $adverstising->reports->sum('bordereau_total'); // Бордеро
        $dvoy_total = $adverstising->reports->sum('dvoy_total'); // ДВОУ
        $amount_total = $adverstising->reports->sum('amount_total'); // Общая
        $to_transfer_total = ($adverstising->reports->sum('to_transfer_total'));  // К перечислению
        $to_return_total = ($adverstising->reports->sum('to_return_total'));  // К возврату
        $report_pay_outcome = ($report_pay_outcome);  // Оплачено
        $report_pay_need = ($adverstising->reports->sum('to_transfer_total') - $report_pay_outcome);  // К оплате
        $report_pay_income = ($report_pay_income);  // Получено
        $debt_sk = ($adverstising->reports->sum('to_return_total') - $report_pay_income);  // Долг СК

        $adverstising->update([
            'payment_total' => $payment_total,
            'bordereau_total' => $bordereau_total,
            'dvoy_total' => $dvoy_total,
            'amount_total' => $amount_total,
            'to_transfer_total' => $to_transfer_total,
            'to_return_total' => $to_return_total,
            'report_pay_outcome' => $report_pay_outcome,
            'report_pay_need' => $report_pay_need,
            'report_pay_income' => $report_pay_income,
            'debt_sk' => $debt_sk,
        ]);

    }

}
