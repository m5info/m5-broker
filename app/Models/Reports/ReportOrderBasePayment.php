<?php

namespace App\Models\Reports;

use App\Classes\Export\TagModels\Report\TagReportOrder;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Payments;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\File;
use App\Models\User;
use App\Traits\Models\ActiveConstTrait;
use Illuminate\Database\Eloquent\Model;

class ReportOrderBasePayment extends Model {


    protected $table = 'reports_orders_base_payments';
    public $guarded = ['id'];


    public function payment(){
        return $this->hasOne(Payments::class, 'id', 'payment_id');
    }

    public function editDataBasePayment()
    {
        $base = $this->toArray();
        $payment = $this->payment->toArray();
        $contract = $this->payment->contract;

        $percents = 'financial_policy_kv_agent';
        $percents_total = 'financial_policy_kv_agent_total';

        if((int)$contract->sales_condition == 1){
            $percents = 'informal_discount';
            $percents_total = 'informal_discount_total';
        }

        //долги и выплаты выставляем только если платеж оплачен
        if($payment['statys_id'] != 1) {
            return true;
        }

        //Если не в брокер и не Карта брокер в СК то ни долгов ни выплат не делается
        if(((int)$payment['payment_flow'] != 0) && ((int)$payment['payment_type'] != 3)){
            return true;
        }

        $financial_policy_kv_agent_total = getFloatFormat($payment[$percents_total]);

        $payment_payment_total = getFloatFormat($payment['payment_total']);
        $base_payment_total = getFloatFormat($base['payment_total']);
        $payment_financial_policy_kv_agent = getFloatFormat($payment[$percents]);
        $payment_financial_policy_kv_agent_total = getTotalSumToPrice($payment_payment_total, $payment_financial_policy_kv_agent);

        //ЕСЛИ СУММЫ РАЗНЫЕ НО КВ ОДИНАКОВОЕ
        if($base['payment_total'] != $payment['payment_total'] && $base[$percents] == $payment[$percents]){


            if($base_payment_total < $payment_payment_total){

                $delta = $payment_payment_total - $base_payment_total;
                $sum = $delta-getTotalSumToPrice($delta, $payment_financial_policy_kv_agent);
                return $this->setDebt($sum);
            }elseif($base_payment_total > $payment_payment_total){
                //Если меньше то выплата
                $delta = $base_payment_total - $payment_payment_total;

                //Если в брокер или Карта брокер в СК то делаем выплату за миинумом КВ
                if(((int)$payment['payment_flow'] == 0) || ((int)$payment['payment_type'] == 3)) {
                    $sum = $delta - getTotalSumToPrice($delta, $payment_financial_policy_kv_agent);
                    return $this->setPremium($sum);
                }
            }


        }

        //ЕСЛИ СУММЫ ОДИНАКОВОЕ НО КВ РАЗНЫЕ
        if($base['payment_total'] == $payment['payment_total'] && $base[$percents] != $payment[$percents]){

            //1 Проверка изменения кв агента платежа
            $financial_policy_kv_agent_total = getTotalSumToPrice($payment_payment_total, $payment[$percents]);

            $payment_payment_total = getFloatFormat($payment['payment_total']);
            $payment_financial_policy_kv_agent = getFloatFormat($base[$percents]);
            $payment_financial_policy_kv_agent_total = getTotalSumToPrice($payment_payment_total, $payment_financial_policy_kv_agent);

           if($financial_policy_kv_agent_total < $payment_financial_policy_kv_agent_total)
            {
                //Если больше то выстовить долг
                return $this->setDebt(($payment_financial_policy_kv_agent_total-$financial_policy_kv_agent_total));
            }elseif($financial_policy_kv_agent_total > $payment_financial_policy_kv_agent_total)
            {
                //Если меньше то выплата
                return $this->setPremium(($financial_policy_kv_agent_total-$payment_financial_policy_kv_agent_total));
            }

        }


        //ЕСЛИ СУММЫ РАЗНЫЕ И КВ РАЗНЫЕ
        if($base['payment_total'] != $payment['payment_total'] && $base[$percents] != $payment[$percents]){

            $base_payment_total = $base['payment_total'];
            $base_agent = $base[$percents];
            $payment_total = $payment['payment_total'];
            $kv_agent = $payment[$percents];

            $summary = $this->segSummaruTotal($base_payment_total, $base_agent, $payment_total, $kv_agent);


            if($summary > 0){
                //Если больше выплата
                return $this->setPremium($summary);

            }elseif($summary < 0){
                //Если меньше долг
                return $this->setDebt(($summary*-1));
            }

        }








        return true;

    }

    //суммы платежа
    public function paymentTotalBasePayment($base, $payment){

        //ЕСЛИ БОЛЬШЕ
        if($payment['payment_total'] > $base['payment_total']){
            //ТО ВЫЩТЫВАЕМ ДЕЛЬТУ КВ АГЕНТА И ДЕЛАЕМ ВЫПЛАТУ
            $delta = getFloatFormat($payment['payment_total']) - getFloatFormat($base['payment_total']);
            $payment_financial_policy_kv_agent_total = getTotalSumToPrice($delta, getFloatFormat($payment['financial_policy_kv_agent']));

            //выплата
            return $this->setPremium($payment_financial_policy_kv_agent_total);


        }else{
            // ВЫЩТЫВАЕМ КВ АГЕНТА И ДЕЛАЕМ ДОЛГ
            $delta = getFloatFormat($base['payment_total']) - getFloatFormat($payment['payment_total']);
            $payment_financial_policy_kv_agent_total = getTotalSumToPrice($delta, getFloatFormat($payment['financial_policy_kv_agent']));
            //Долг
            return $this->setDebt($payment_financial_policy_kv_agent_total);

        }

        return true;
    }


    public function setDebt($payment_total)
    {

        $payment = new Payments();
        $payment->type_id = 1;
        return $this->createPayment($payment, $payment_total);

    }

    public function setPremium($payment_total)
    {
        $payment = new Payments();
        $payment->type_id = 2;
        return $this->createPayment($payment, $payment_total);
    }

    public function createPayment(Payments $pay, $payment_total)
    {
        $payment = $this->payment;

        $agent_id = $payment->agent_id;
        if($payment->contract->sales_condition != 0){
            $agent_id = $payment->manager_id;
        }

        $data = new \stdClass();
        $data->agent_id = $agent_id;
        $data->comments = 'Автоматическое выставление';
        $data->payment_total = $payment_total;

        $pay->statys_id = 0;
        $pay->bso_id = $payment->bso_id;
        $pay->contract_id = $payment->contract_id;
        $pay->org_id = $payment->org_id;


        return $pay->savePaymentData($data);
    }




    public static function segSummaruTotal($base_payment_total, $base_agent, $payment_total, $kv_agent)
    {
        $summary = 0;

        $base_agent_total = getTotalSumToPrice($base_payment_total, $base_agent);
        $agent_total = getTotalSumToPrice($payment_total, $kv_agent);

        $delta_kv = $agent_total-$base_agent_total;

        $summary = ($base_payment_total-$payment_total)+$delta_kv;

        return $summary;
    }



}