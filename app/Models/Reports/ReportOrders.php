<?php

namespace App\Models\Reports;

use App\Classes\Export\TagModels\Report\TagReportOrder;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Payments;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\File;
use App\Models\User;
use App\Traits\Models\ActiveConstTrait;
use Illuminate\Database\Eloquent\Model;

class ReportOrders extends Model {

    use ActiveConstTrait;

    protected $table = 'reports_orders';

    const DOCUMENTS_PATH = 'reports/order';

    protected $guarded = ['id'];

    const TAG_MODEL = TagReportOrder::class;

    // type_id
    const TYPE = [
        0 => 'Бордеро',
        1 => 'ДВОУ'
    ];
    const TEMPLATE_CATEGORY = [
        0 => 'report_borderau_to_sk',
        1 => 'report_dvou_to_sk'
    ];

    const PAYMENT_FLOW_TYPE = [
        0 => "Брокер",
        1 => "СК"
    ];

    const STATE = [
//        0 => 'Создан',
//        1 => 'На согласовании',
//        2 => 'Акцептован',
//        3 => 'Согласован'

        0 => 'Создан',
        1 => 'На согласовании',
        2 => 'Акцептован',
        3 => 'Согласован',
        4 => 'На оплате',
        5 => 'Частично оплачен',
        6 => 'Оплачен',
    ];


    const MARKER_COLORS = [
        0 => ['color'=>'', 'title' => 'Нет'],
        1 => ['color'=>'#f2f2f2', 'title' => 'Серый'],
        2 => ['color'=>'#fffae6', 'title' => 'Желтый'],
        3 => ['color'=>'#ccd9ff', 'title' => 'Синий'],
        4 => ['color'=>'#e6ffe6', 'title' => 'Зеленый'],
        5 => ['color'=>'#ffcccc', 'title' => 'Красный'],
    ];


    public function report_payment_sums(){
        return $this->hasMany(ReportPaymentSum::class, 'report_id');
    }

    public function bso_supplier(){
        return $this->hasOne(BsoSuppliers::class, 'id', 'bso_supplier_id');
    }

    public function bso_items(){
        return $this->hasMany(BsoItem::class, 'acts_sk_id');
    }

    public function create_user(){
        return $this->hasOne(User::class, 'id', 'create_user_id');
    }

    public function reports_advertising(){
        return $this->hasOne(AdverstisingReports::class, 'id', 'advertising_id');
    }

    public function accept_user(){
        return $this->hasOne(User::class, 'id', 'accept_user_id');
    }

    public function files()
    {
        return $this->belongsToMany(File::class, 'report_documents', 'report_id', 'file_id');
    }

    public function report_base_payments(){
        $report_order = $this;
        $report_base_payments = ReportOrderBasePayment::query()->whereIn('payment_id', $report_order->getPayments()->get()->pluck('id')->toArray())->get();
        return $report_base_payments;
      //  return $this->hasMany(ReportOrderBasePayment::class, 'report_order_id');
    }



    public function getPayments()
    {
        $payments = Payments::where('payments.is_deleted', "0");

        if((int)$this->type_id == 0) $payments->where('payments.reports_order_id', '=', $this->id);
        if((int)$this->type_id == 1) $payments->where('payments.reports_dvou_id', '=', $this->id);

        return $payments;
    }

    public function checkDvou()
    {
        $payments = Payments::where('payments.is_deleted', "0");

        if((int)$this->type_id == 0) $payments->where('payments.reports_order_id', '=', $this->id);
        if((int)$this->type_id == 1) $payments->where('payments.reports_dvou_id', '=', $this->id);

        $payment = $payments->first();

        return $payment && $payment->reports_dvou_id > 0 ? true : false;
    }

    public function getTemplateCategory(){
        return $this->template_category_ru('type_id');
    }

    public function refreshSumOrder($rec = 0)
    {
        $report = $this;
        $flows_in_payments = [];
        $payments = $report->getPayments();

        $agent_payment = clone $payments;

        $agent_payment->leftJoin('contracts', 'contracts.id', '=', 'payments.contract_id');
        $agent_payment->where('contracts.sales_condition', '!=', 1);


        $to_transfer_total = 0;
        $to_return_total = 0;

        $kv_recommender_total = 0;

        $report->payment_total = $payments->sum('payment_total');//-$payments->sum('official_discount_total');
        $report->bordereau_total = $payments->sum('financial_policy_kv_bordereau_total');
        $report->dvoy_total = $payments->sum('financial_policy_kv_dvoy_total');

        $report->kv_agent_total = $agent_payment->sum('financial_policy_kv_agent_total');
        $report->marjing_total = $payments->sum('financial_policy_marjing_total');

        $report->amount_total = $report->bordereau_total+$report->dvoy_total;

        foreach ($payments->get() as $payment)
        {

            if($rec == 0){
                if((int)$this->type_id == 0 && $payment->reports_dvoy){
                    $payment->reports_dvoy->refreshSumOrder(1);
                }

                if((int)$this->type_id == 1 && $payment->reports_border){
                    $payment->reports_border->refreshSumOrder(1);
                }
            }



            $flows_in_payments[] = $payment->payment_flow;
            $res_sum = $this->getSumToTransferAndReturn($payment, $this->type_id);

            $to_transfer_total += $res_sum->to_transfer_total;
            $to_return_total += $res_sum->to_return_total;

            if($payment->financial_policy_id > 0){
                $fp = FinancialPolicy::where('id', $payment->financial_policy_id)->get()->last();

                if($fp && isset($fp->partnership_reward_1)){
                    if($fp->partnership_reward_1 > 0){
                        if((int)$fp->fix_price){
                            $kv_recommender_total +=  getFloatFormat($fp->partnership_reward_1);
                        }else{
                            $kv_recommender_total +=  getTotalSumToPrice($payment->payment_total, $fp->partnership_reward_1);
                        }
                    }
                    if($fp->partnership_reward_2 > 0){
                        if((int)$fp->fix_price){
                            $kv_recommender_total +=  getFloatFormat($fp->partnership_reward_2);
                        }else{
                            $kv_recommender_total +=  getTotalSumToPrice($payment->payment_total, $fp->partnership_reward_2);
                        }
                    }
                    if($fp->partnership_reward_3 > 0){
                        if((int)$fp->fix_price){
                            $kv_recommender_total +=  getFloatFormat($fp->partnership_reward_3);
                        }else{
                            $kv_recommender_total +=  getTotalSumToPrice($payment->payment_total, $fp->partnership_reward_3);
                        }
                    }
                }


            }


        }

        $flows_in_payments = collect($flows_in_payments)->unique();

        $report->payment_flow_type = ($flows_in_payments->count() == 2) ? 2 : $flows_in_payments->first();
        $report->to_transfer_total = $to_transfer_total;
        $report->to_return_total = $to_return_total;
        $report->kv_recommender_total = $kv_recommender_total;

        $report->save();

        $report->refreshAdvancePayment();

        // если есть рекламник, то пересчитать его
        if($report->advertising_id){
            AdverstisingReports::recount_payments($report->advertising_id);
        }

        return true;
    }

    public function refreshAdvancePayment()
    {
        $report = $this;

        $advance_payment = 0;


        $advance_payment =  $report->to_transfer_total - $report->dvoy_total; // предватирельный платеж
        if(sizeof($report->report_payment_sums)){
            $total_sum = $report->report_payment_sums->where('type_id', 0)->sum('amount');
            $advance_payment = $report->to_transfer_total - $total_sum;

        }


        $report->advance_payment =  $advance_payment;
        $report->save();
    }



    public function getSumToTransferAndReturn($payment, $type_id)
    {
        $res_sum = new \stdClass();
        $res_sum->to_transfer_total = 0;
        $res_sum->to_return_total = 0;

        $payment_total = $payment->payment_total;// - $payment->official_discount_total;
        $bordereau_total = $payment->financial_policy_kv_bordereau_total;
        $dvoy_total = $payment->financial_policy_kv_dvoy_total;

        $payment_type = $payment->payment_type; // тип оплаты
        $payment_flow = $payment->payment_flow; // поток оплаты

        $hold_type_id = 2; //0 - Без удержания 1 - С удержанием 2 - Частичное удержание

        $hold_kv = $payment->bso->supplier->hold_kv_product($payment->bso->product_id);
        if($hold_kv) $hold_type_id = $hold_kv->hold_type_id;


        if((int)$this->type_id == 0){ // БОРДЕРО

            //оплата в БРОКЕР
            if($payment_flow == 0){

                $res_sum->to_transfer_total = $payment_total;
                switch ($hold_type_id) {
                    case 0:
                        $res_sum->to_return_total += $bordereau_total;//+$dvoy_total;
                        break;
                    case 1:
                        $res_sum->to_transfer_total -= $bordereau_total+$dvoy_total;
                        $res_sum->to_return_total += 0;
                        break;
                    case 2:
                        $res_sum->to_transfer_total -= $bordereau_total;
                        //$res_sum->to_return_total += $dvoy_total;
                        break;
                }

            }

            //оплата в СК
            if($payment_flow == 1){

                if($hold_type_id == 1){
                    $res_sum->to_return_total += $bordereau_total+$dvoy_total;
                }else{
                    $res_sum->to_return_total += $bordereau_total;
                }


            }

        }

        if((int)$this->type_id == 1){ // ДВОУ

            //оплата в БРОКЕР
            if($payment_flow == 0)
            {
                if($hold_type_id == 1){
                    $res_sum->to_return_total += 0;
                }else{
                    $res_sum->to_return_total += $dvoy_total;
                }
            }

            //оплата в СК
            if($payment_flow == 1)
            {
                if($hold_type_id == 1){
                    $res_sum->to_return_total += 0;
                }else{
                    $res_sum->to_return_total += $dvoy_total;
                }
            }





        }

        return $res_sum;
    }



    public function getProductsTitleList()
    {
        $product_title = '';

        $productArr = [];
        $p = $this->getPayments()->with('bso_for_reports_list')->get();
        $count_all = $p->count();

       foreach ( $p as $payment){

            if(!isset($productArr[$payment->bso_for_reports_list->product->id])){
                $productArr[$payment->bso_for_reports_list->product->id]['title'] = '';
                $productArr[$payment->bso_for_reports_list->product->id]['count'] = 0;
            }

            $productArr[$payment->bso_for_reports_list->product->id]['title'] = $payment->bso_for_reports_list->product->title;
            $productArr[$payment->bso_for_reports_list->product->id]['count'] += 1;

        }

        foreach ($productArr as $product){
            $product_title .= $product['title']." (".$product['count']."/$count_all) ";
        }


        return $product_title;
    }

    //Сохраняем базовые данные по платежу
    public static function saveBaseDataPayment($payment, $report_id = null)
    {
        $query = ReportOrderBasePayment::query();

        /*if($report_id){
            $query->where('report_order_id', $report_id);
        }*/

        $base = $query->where('payment_id', $payment->id)->get()->first();

        if(!$base){
            ReportOrderBasePayment::create([
                'report_order_id' => $report_id,
                'payment_id' => $payment->id,
                'payment_total' => $payment->payment_total,
                'official_discount' => $payment->official_discount,
                'informal_discount' => $payment->informal_discount,
                'bank_kv' => $payment->bank_kv,
                'financial_policy_id' => $payment->financial_policy_id,
                'financial_policy_manually_set' => $payment->financial_policy_manually_set,
                'financial_policy_kv_bordereau' => $payment->financial_policy_kv_bordereau,
                'financial_policy_kv_dvoy' => $payment->financial_policy_kv_dvoy,
                'financial_policy_kv_agent' => $payment->financial_policy_kv_agent,
                'financial_policy_kv_parent' => $payment->financial_policy_kv_parent,
            ]);
        } else {
            $base->report_order_id = $report_id;
            if($payment->type_id == 0 ){
                $base->payment_total = $payment->payment_total;
                $base->official_discount = $payment->official_discount;
                $base->informal_discount = $payment->informal_discount;
                $base->bank_kv = $payment->bank_kv;
                $base->financial_policy_id = $payment->financial_policy_id;
                $base->financial_policy_manually_set = $payment->financial_policy_manually_set;
                $base->financial_policy_kv_bordereau = $payment->financial_policy_kv_bordereau;
                $base->financial_policy_kv_dvoy = $payment->financial_policy_kv_dvoy;
                $base->financial_policy_kv_agent = $payment->financial_policy_kv_agent;
                $base->financial_policy_kv_parent = $payment->financial_policy_kv_parent;
            }
            $base->save();
        }


        return true;
    }


}