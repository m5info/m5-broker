<?php

namespace App\Models\Delivery;

use App\Models\File;
use App\Traits\Models\ActiveConstTrait;
use Illuminate\Database\Eloquent\Model;

class DeliveryFiles extends Model
{
    use ActiveConstTrait;

    protected $table = 'delivery_files';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function file()
    {
        return $this->hasOne(File::class, 'id', 'file_id');
    }
}
