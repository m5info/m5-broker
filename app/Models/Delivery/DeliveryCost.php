<?php

namespace App\Models\Delivery;

use App\Models\Directories\Products;
use App\Traits\Models\ActiveConstTrait;
use Illuminate\Database\Eloquent\Model;

class DeliveryCost extends Model
{
    use ActiveConstTrait;

    protected $table = 'delivery_cost';

    protected $guarded = ['id'];

    public $timestamps = false;


    /**
     * В какой день выезд
     */

    const WEEKEND = [
        0 => 'Будни',
        1 => 'Выходной'
    ];

    public function product() {
        return $this->hasOne(Products::class, 'id', 'product_id');
    }
}
