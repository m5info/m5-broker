<?php

namespace App\Models\Delivery;

use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{

    protected $table = 'delivery';

    protected $guarded = ['id'];

    public $timestamps = false;

    /**
     * В какой день выезд
     */

    const WEEKEND = [
        0 => 'Будни',
        1 => 'Выходной'
    ];

    const STATUS = [
        1 => 'Оформлен',
        2 => 'Аннулирован',
    ];

    public function payment() {
        return $this->hasOne(Payments::class,'id', 'payment_id');
    }

    public function contract() {
        return $this->hasOne(Contracts::class,'id', 'contract_id');
    }

    public function product() {
        return $this->hasOne(DeliveryCost::class,'id', 'product_id');
    }

    public function files() {
        return $this->hasMany(DeliveryFiles::class, 'delivery_id', 'id')
            ->leftJoin('files', 'delivery_files.file_id', '=', 'files.id');
    }
}
