<?php

namespace App\Models\Api\SK;


use App\Models\Directories\BsoSuppliers;
use Illuminate\Database\Eloquent\Model;

class ApiInsuranceAuxiliaryParameters extends Model{

    public $table = 'api_insurance_auxiliary_parameters';
    public $timestamps = false;
    public $guarded = [];


    const TYPE = [
        0 => 'Контрагент',
        1 => 'Водитель',
        2 => 'Застрахованный',
    ];


}