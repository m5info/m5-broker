<?php

namespace App\Models\Api\SK;


use App\Models\Directories\BsoSuppliers;
use Illuminate\Database\Eloquent\Model;

class ApiSetting extends Model{

    public $table = 'api_settings';
    public $timestamps = false;
    public $guarded = [];

    public function bso_supplier(){
        return $this->hasOne(BsoSuppliers::class, 'id', 'bso_supplier_id');
    }

}