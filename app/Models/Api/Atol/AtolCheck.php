<?php

namespace App\Models\Api\Atol;

use App\Models\Contracts\Payments;
use Illuminate\Database\Eloquent\Model;

class AtolCheck extends Model{

    public $guarded = ['id'];
    public $table = 'atol_check';

    const WHEN_TO_SEND = [
        0 => 'Сейчас',
        1 => 'Потом',
    ];

    public function payment(){
        return $this->belongsTo(Payments::class, 'atol_check_id');
    }




}