<?php

namespace App\Models\IntegrationOnline;

use Illuminate\Database\Eloquent\Model;

class VskApiOnline extends Model
{
    protected $table = 'vsk_api_online';

    public $timestamps = false;
}
