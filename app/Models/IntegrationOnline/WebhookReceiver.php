<?php

namespace App\Models\IntegrationOnline;

use Illuminate\Database\Eloquent\Model;

class WebhookReceiver extends Model
{
    protected $table = 'webhook_receivers';
    public $timestamps = false;
}