<?php


namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;

class TableColumn extends Model
{
    public $table = 'table_columns';

    protected $guarded = [];

    public $timestamps = false;
}