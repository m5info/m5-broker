<?php

namespace App\Models\Log;

use Illuminate\Database\Eloquent\Model;

class DebugLogging extends Model
{
    protected $table = 'debug_logging';
    public $timestamps = false;
}