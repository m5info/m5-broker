<?php

namespace App\Models\Log;

use App\Classes\Common\CUDLogsColumns;
use App\Classes\Common\CUDLogsColumnsValue;
use App\Models\User;
use App\Models\Users\Role;
use Illuminate\Database\Eloquent\Model;

class CUDLogs extends Model
{
    protected $table = 'cud_logs';

    public $timestamps = false;

    const EVENT_NAME = [
        '1' => 'Cоздан',
        '2' => 'Изменен',
        '3' => 'Удален',
        '4' => 'Сохранен'
    ];

    const EVENT_COLOR = [
        1 => '#e7ffe7',
        2 => '#ffffc0',
        3 => '#ffe8e8',
        4 => '#acecb1'
    ];

    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id','user_id');
    }

    public static function rus_column($table, $column)
    {
        $method = "{$table}Columns";

        return CUDLogsColumns::$method($column);
    }

    public static function rus_value($table, $column, $value)
    {
        $method = "{$table}Values";

        return CUDLogsColumnsValue::$method($column, $value);
    }
}