<?php

namespace App\Models\Dictionaries;

use Illuminate\Database\Eloquent\Model;

class DictionarySkSk extends Model
{
    protected $table = 'dictionary_sk_sk';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function sk()
    {
        return $this->hasOne(DictionarySk::class, 'id', 'dictionary_sk_id');
    }
}
