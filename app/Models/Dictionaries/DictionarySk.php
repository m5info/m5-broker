<?php

namespace App\Models\Dictionaries;

use Illuminate\Database\Eloquent\Model;

class DictionarySk extends Model
{
    protected $table = 'dictionary_sk';

    protected $guarded = ['id'];

    public $timestamps = false;
}
