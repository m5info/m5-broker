<?php

namespace App\Models\Actions;

use App\Models\Contracts\Contracts;
use Illuminate\Database\Eloquent\Model;

class ExpectedPayments extends Model
{
    public $table = 'expected_payments';

    public $timestamps = false;

    public $guarded = ['id'];

    public function contract(){
        return $this->hasOne(Contracts::class, 'id', 'contract_id');
    }
}
