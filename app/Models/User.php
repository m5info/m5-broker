<?php

namespace App\Models;

use App\Classes\Export\TagModels\Characters\TagUser;
use App\Helpers\Visible;
use App\Models\Account\TableColumn;
use App\Models\BSO\BsoItem;
use App\Models\Cashbox\Cashbox;
use App\Models\Cashbox\CashboxTransactions;
use App\Models\Characters\Agent;
use App\Models\Contracts\Debts;
use App\Models\Contracts\Payments;
use App\Models\Finance\Invoice;
use App\Models\Organizations\Organization;
use App\Models\Security\Security;
use App\Models\Settings\Department;
use App\Models\Settings\Notification;
use App\Models\Settings\PointsSale;
use App\Models\Settings\UserBalanceSettings;
use App\Models\Subject\Juridical;
use App\Models\Subject\Physical;
use App\Models\Users\AgentContracts;
use App\Models\Users\AgentFinancialPolicies;
use App\Models\Users\ApiTokens;
use App\Models\Users\Permission;
use App\Models\Users\Role;
use App\Models\Users\SalaryType;
use App\Models\Users\UsersBalance;
use App\Models\Users\UsersLimitBSO;
use App\Models\Users\UserThemes;
use App\Traits\Models\ActiveConstTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;


class User extends Authenticatable
{
    use Notifiable;
    use ActiveConstTrait;

    const FILES_FOLDER = 'users/photos';
    const FILES_DOC = 'users/docs';

    const TAG_MODEL = TagUser::class;

    const STATUS_USER = [
        0 => 'Работает',
        1 => 'Уволен',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'settings' => 'json',
    ];

    public static $usersEmploye = [];

    /*
     * Relations
     */

    public function notifications()
    {
        return $this->hasMany(Notification::class, 'user_id');
    }

    public function columns()
    {
        return $this->belongsToMany(TableColumn::class, 'users2columns', 'user_id', 'column_id')->withPivot('orders');
    }

    public function visible_columns($key)
    {
        /* Используется для "Отображении таблицы" */
        $subpermissions = collect($this->role->subpermissions)->pluck('title');
        $need_arr = [];

        foreach ($subpermissions as $sub){
            $need_arr[] = (int)str_replace($key.'_table_columns_key_', '', $sub);
        }

        $visible_columns = TableColumn::query()
            ->select('column_name', 'id')
            ->whereIn('id', $need_arr)
            ->get();

        return $visible_columns = collect($visible_columns)->keyBy('column_name')->keys()->toArray();
    }

    public function agent_contracts()
    {
        return $this->hasMany(AgentContracts::class, 'user_id');
    }

    public function agent_financial_policies()
    {
        return $this->hasMany(AgentFinancialPolicies::class, 'user_id');
    }

    public function point_sale()
    {
        return $this->hasOne(PointsSale::class, 'id', 'point_sale_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function scans()
    {
        return $this->belongsToMany(File::class, 'users_scans', 'user_id', 'file_id');
    }


    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function image()
    {
        return $this->belongsTo(File::class);
    }

    public function theme (){
        return $this->hasOne(UserThemes::class,'user_id','id');
    }

    public function smallImage()
    {
        return $this->belongsTo(File::class);
    }

    public function security_service()
    {
        return $this->hasOne(Security::class, 'object_id')->where('type_inquiry', '=', Security::TYPE_INQUIRY_USER)->orderBy('created_at', 'desc');
    }

    public function bso_items()
    {
        return $this->hasMany(BsoItem::class, 'user_id', 'id');
    }

    public function info()
    {
        $subjectClass = $this->subject_type_id == 1 ? Physical::class : Juridical::class;
        return $this->hasOne($subjectClass, 'id', 'subject_id');
    }

    public function perent()
    {
        return $this->hasOne(User::class, 'id', 'parent_id');
    }

    public function parent()
    {
        return $this->hasOne(User::class, 'id', 'parent_id');
    }

    public function hasEmployee()
    {
        return $this->hasOne(User::class, 'parent_id', 'id');
    }

    public function api_token()
    {
        return $this->hasOne(ApiTokens::class, 'user_id', 'id');
    }

    public function curator()
    {
        return $this->hasOne(User::class, 'id', 'curator_id');
    }

    public function transactions()
    {
        return $this->hasMany(CashboxTransactions::class, 'user_id', 'id');
    }


    public function cashbox()
    {
        return $this->hasOne(Cashbox::class, 'user_id', 'id')->where('is_actual', '=', 1)->limit(1);
    }

    /**
     * Берем всех юзеров по пермишену ( которые являются менеджерами, например )
     */
    public static function getUsersHavePermissionLike($permission_title)
    {
        $roles = Role::query()
            ->select('roles.id')
            ->leftJoin('roles_permissions', 'roles_permissions.role_id', '=', 'roles.id')
            ->leftJoin('permissions', 'roles_permissions.permission_id', '=', 'permissions.id')
            ->whereIn('roles_permissions.permission_id', function($sub) use ($permission_title){
                $sub->select('permissions.id')
                    ->from('permissions')
                    ->where('permissions.title', '=', $permission_title);
            });

        $users = User::query()
            ->whereIn('users.role_id', $roles);

        return $users;

    }

    public function getNotifications()
    {

        $notifications = $this->notifications()
            ->where('is_read', 0);

        return $notifications;

    }

    public static function getALLUserWhere()
    {
        $user = auth()->user();

        $user_list = User::orderBy('name', 'asc');
        $user_list->select('users.*', \DB::raw("CONCAT(users.name, ' ', users.email) as name"), 'users.name as _name');

        $rolesVisibility = $user->role->rolesVisibility(1);

        if ($rolesVisibility) {

            $visibility = $rolesVisibility->visibility;


            if ($visibility == 0) {//Все

            } elseif ($visibility == 1) {//Все в рамках организации

                $user_list->where(function ($query) {
                    $query->where('parent_id', auth()->id())
                        ->orWhere('id', auth()->id());
                });



            } elseif ($visibility == 2) {//Только свои

                $user_list->where('id', auth()->id());
            } elseif ($visibility == 3) {//Только свои и своих подчиненных



                $user_list->where(function ($query) {
                    $query->where('parent_id', auth()->id())
                        ->orWhere('id', auth()->id())
                        ->orWhere('path_parent', 'like', "%:".auth()->id().":%");
                });



            }
        } else {
            $user_list->where('id', auth()->id())->orderBy('name', 'asc');
        }

        return $user_list;

    }

    public static function getUsersByRole(array $perms) : Builder // возвращаем билдер запроса
    {

        //Видимость                 //роли->права, отношение моделей
        $users = User::getALLUserWhere()->whereHas('role.permissions',function (Builder $query) use ($perms) {
            // где у отношения прав есть название как из группы ключей
            $query->whereIn('permissions.title',   $perms[0]);
        }) ;

        return $users;
    }
    // Массивом
    public static function getUsersByRoleList(array $perms) : array // возвращаем билдер запроса
    {

        $users = User::getUsersByRole($perms)->with('role','role.permissions')->get(); //гет
        $user_arr = [];
        foreach ($users as $user){
            foreach ($perms[0] as  $key)
                if($user->role->permissions->where('title',$key)->first()){
                    if(!isset($user_arr[$key]))
                        $user_arr[$key] = [];
                    $user_arr[$key][] = $user->name;
                }

        }

        return $user_arr;
    }

    public static function getAgentForSearchFilterResults()
    {

        $agents = User::getALLUserWhere()->get();
        if ($agents)
            for ($i = 0; $i < $agents->count(); $i++) {
                $agents[$i]['name_login'] = $agents[$i]['name'] . ' : ' . $agents[$i]['email'];
            }

        return $agents;
    }

    /* Берем всех, кто является агентом (is_agent) текущего пользователя */
    public static function getAllImParentAgents_($get = true)
    {
        $agents = User::query()
            ->select('users.*')
            ->from('users')
            ->leftJoin('roles_permissions', 'users.role_id', '=', 'roles_permissions.role_id')
            ->where('users.parent_id', \Auth::id())
            ->where('roles_permissions.permission_id', 69);

        if ($get){
            $agents = $agents->get();
        }

        return $agents;
    }


    public static function getALLAgents($permission_id = 24, $get = true)
    {
        $user = User::getALLUserWhere();
        //$user->where();
        //$user->leftJoin('type_obj', 'type_obj.id', '=', 'objects.type_id')

        $user->whereIn('users.role_id', function ($query) use ($permission_id) {
            $query->select(\DB::raw('roles_permissions.role_id'))
                ->from('roles_permissions')
                ->where('roles_permissions.permission_id', $permission_id);

        });

        if ($get) {
            return $user->get();
        } else {
            return $user;
        }
    }

    public static function getALLAgentsToBSO()
    {
        $user = User::getALLUserWhere();
        $user->whereIn('ban_level', [0, 1]);
        //$user->leftJoin('type_obj', 'type_obj.id', '=', 'objects.type_id')


        return $user->get();
    }

    public static function getALLUser()
    {
        return User::getALLUserWhere()->get();
    }

    public static function getALLParent()
    {
        return User::getALLUserWhere()->where('is_parent', 1)->get();
    }

    /*
     * Accessors
     */

    public function getTimezoneAttribute()
    {
        return $this->attributes['timezone'] ?: config('app.timezone');
    }

    public function getUserLimitBSOToProduct($product_id, $type = 0, $day = null)
    {
        $count_bso = 0;

        if ($type == 0) { //вернуть лимит по продукту

            $my_limit = UsersLimitBSO::where('user_id', $this->id)->where('product_id', $product_id)->get()->first();
            if ($my_limit) $count_bso = $my_limit->max_limit;

        } elseif ($type == 1) { //узнать количество на руках по продукту
            $my_limit = BsoItem::where('user_id', $this->id)->where('location_id', 1);
            if ($product_id > 0) $my_limit->where('product_id', $product_id);

            if ($day && (int)$day > 0) {

                $myData = date('Y-m-d 00:00:00', strtotime("- $day day"));
                $my_limit->where('transfer_to_agent_time', '<=', $myData);

            }


            $my_bso = $my_limit->get();
            if ($my_bso) $count_bso = $my_bso->count();
        }


        return (int)$count_bso;
    }

    public function getChildsLimitBSOToProduct($product_id, $type = 0, $day = null)
    {
        $count_bso = 0;

        if ($type == 0) { //вернуть лимит по продукту

            $my_limit = UsersLimitBSO::where('user_id', $this->id)->where('product_id', $product_id)->get()->first();
            $id = $this->id;
            $parent_limit = Debts::query()->whereIn('user_id', function($q) use ($id){
                $q->select('id');
                $q->from('users');
                $q->where('agent_id', $id);
                $q->orWhere('parent_id', $id);
            })->get();

            if ($my_limit) $count_bso = $parent_limit->sum('max_limit');

        } elseif ($type == 1) { //узнать количество на руках по продукту
            $id = $this->id;

            $parent_limit = BsoItem::whereIn('user_id', function($q) use ($id){
                $q->select('id');
                $q->from('users');
                $q->where('agent_id', $id);
                $q->orWhere('parent_id', $id);
            })
                ->where('location_id', 1);

            if ($product_id > 0) $parent_limit->where('product_id', $product_id);

            if ($day && (int)$day > 0) {

                $myData = date('Y-m-d 00:00:00', strtotime("- $day day"));
                $parent_limit->where('transfer_to_agent_time', '<=', $myData);

            }


            $my_bso = $parent_limit->get();
            if ($my_bso) $count_bso = $my_bso->count();
        }


        return (int)$count_bso;
    }

    /*
     * Scopes
     */


    private function getByUserType($query, $type)
    {
        return $query->whereHas('department', function ($q) use ($type) {
            return $q->where('user_type_id', $type);
        });
    }

    /*
     * Functions
     */


    public function getParentUserList($prepend_name = 'Нет')
    {
        //Проверяем доступность пользователей
        return $this::getALLUser()->pluck('name', 'id')->prepend($prepend_name, 0);

    }



    public function hasPermission($permissionGroupName, $permissionName)
    {
        $permission = Permission::where('title', $permissionName)->whereHas('group', function ($query) use ($permissionGroupName) {

            $query->where('title', $permissionGroupName);

        })->first();

        if (!$permission || !$this->role) {

            return false;

        }

        return $this->role->hasPermission($permission->id);
    }

    public function hasGroupPermission($permissionGroupName)
    {
        $permissions = Permission::whereHas('group', function ($query) use ($permissionGroupName) {

            $query->where('title', $permissionGroupName);

        })->get();

        if (!sizeof($permissions) || !$this->role) {

            return false;

        }

        return $this->role->permissions()->whereIn('id', $permissions->pluck('id'))->exists();
    }


    public function getAllPermissionArray()
    {
        $userRolePermissions = [];
        foreach ($this->role->permissions_menu() as $permissions){
            $userRolePermissions[$permissions->groups][$permissions->permissions] = 1;
        }

        return $userRolePermissions;
    }







    public function ifSalaryTypeOfficial()
    {
        return $this->salary_type === SalaryType::OFFICIAL;
    }

    public function getScheduleForDay(Carbon $date)
    {
        return $this->schedules()->where('date', $date->format('Y-m-d'))->first();
    }


    public function getAllOrgUsers()
    {
        return User::query()->where('organization_id', '=', $this->organization_id)->get()->keyBy('id');
    }

    public function getSubUsers()
    {
        $res = [];
        if ($this->is_parent) {
            $res = User::query()->where('parent_id', '=', $this->id)->get()->keyBy('id');
        }
        return $res;
    }


    public function getAllSubUsersIds()
    {
        self::getDescendants($this->id);

        return self::$usersEmploye;
    }


    public static function getDescendants($id){

        $mainUser = User::find($id);
        $users = User::orderBy('name')->get();

        $usersByParent = [];
        foreach($users as $user){
            $usersByParent[$user['parent_id']][] = $user;
        }

        self::$usersEmploye[] = $mainUser['id'];

        $users = [
            $mainUser->id => [
                'id' => $mainUser->id,
                'parent_id' => $mainUser->parent_id,
                'childs' => self::getUserRecusive($usersByParent, $mainUser['id']),
            ]
        ];

        return collect(json_decode(json_encode($users)));
    }

    public static function getUserRecusive($usersByParent, $parent_id)
    {
        if (is_array($usersByParent) && isset($usersByParent[$parent_id]) && count($usersByParent[$parent_id])) {

            $out = [];
            foreach ($usersByParent[$parent_id] as $user) {

                self::$usersEmploye[] = $user->id;

                $out[$user->id]['id'] = $user->id;
                $out[$user->id]['parent_id'] = $user->parent_id;
                $out[$user->id]['childs'] = self::getUserRecusive($usersByParent, $user['id']);
            }

            return $out;
        }
    }

    public function visibleAgents($permission_group_name)
    {

        $agents = Agent::query();
        $agents = Visible::apply($agents, $permission_group_name, 'id');
        return $agents;
    }


    public function visibleUsers($permission_group_name)
    {

        $users = User::query();
        $users = Visible::apply($users, $permission_group_name, 'id');
        return $users;
    }


    public function visibleInvoices(Builder $builder = null)
    {
        $invoices = $builder ? $builder : Invoice::query();
        $invoices = Visible::apply($invoices, 'finance');
        return $invoices;
    }


    public function visibleDebts(Builder $builder = null)
    {
        $debts = $builder ? $builder : Debts::query();
        $debts = Visible::apply($debts, 'finance', ['agent_id', 'manager_id']);
        return $debts;
    }

    public function visiblePayments(Builder $builder = null)
    {
        $payments = $builder ? $builder : Payments::query();
        $payments = Visible::apply($payments, 'finance', ['agent_id', 'manager_id']);
        return $payments;
    }


    public function getBalance($balance_id)
    {
        $_balanse = UsersBalance::where('user_id', $this->id)->where('balance_id', $balance_id)->get()->first();
        if (!$_balanse) {
            $_balanse = UsersBalance::create([
                "user_id" => $this->id,
                "balance_id" => $balance_id,
                "balance" => 0,
            ]);
        }

        return $_balanse;
    }

    public function getBalanceId($balance_id)
    {
        $_balanse = UsersBalance::where('id', $balance_id)->get()->first();

        return $_balanse;
    }


    public function getBalanceList()
    {

        $user_balances = UserBalanceSettings::where('is_actual', 1)->get();
        $result = [];

        foreach ($user_balances as $balances) {
            $_balanse = $this->getBalance($balances->id);
            $balenc = new \stdClass();
            $balenc->id = $_balanse->id;
            $balenc->balance = $_balanse->balance;
            $balenc->title = $balances->title . " " . titleFloatFormat($_balanse->balance);
            $result[] = $balenc;
        }

        return $result;

    }

    public function getBalanceListHome()
    {

        $user_balances = UserBalanceSettings::where('is_actual', 1)->where('type_id', '!=', 3)->get();
        $result = [];

        foreach ($user_balances as $balances) {
            $_balanse = $this->getBalance($balances->id);
            $balenc = new \stdClass();
            $balenc->id = $_balanse->id;
            $balenc->balance = $_balanse->balance;
            $balenc->name = $balances->title;
            $balenc->title = $balances->title . " " . titleFloatFormat($_balanse->balance);
            $result[] = $balenc;
        }

        return $result;

    }


    public function getBalanceProfit()
    {
        $user_balances = UserBalanceSettings::where('is_actual', 1)->where('type_id', 0)->get()->first();
        return $this->getBalance($user_balances->id);
    }

    public function getBalanceCancelPayment()
    {
        $user_balances = UserBalanceSettings::where('is_actual', 1)->where('type_id', 3)->get()->first();
        return $this->getBalance($user_balances->id);
    }


    public function is($role_name)
    {
        $roles = [
            'under' => 'is_underwriter',
            'admin' => 'is_admin',
            'cashier' => 'is_cashier',
            'agent' => 'is_agent',
            'courier' => 'is_courier',
            'underfiller' => 'is_underfiller',
            'manager' => 'is_manager',
            'formalizator' => 'is_formalizator',
            'partner' => 'is_partner',
            'operator_bso' => 'is_operator_bso',
            'referencer' => 'is_referencer',
            'dtp_ride' => 'is_dtp_ride',
            'pso_ride' => 'is_pso_ride',
        ];

        if (isset($roles[$role_name])) {
            return $this->hasPermission('role_owns', $roles[$role_name]);
        }
        return false;

    }


    public function getDebts($type_id = 'all')
    {
        $agent_id = $this->id;
        $debts = Debts::whereOverdue(request()->get('overdue'))->whereIn('agent_id', [$agent_id]);
        $debts = $debts->get()->groupBy('agent_id');

        $agent_summaries = [];

        foreach ($debts as $agent_id => $debt_group) {

            foreach ($debt_group as $debt) {

                $type = $debt->type();
                $ptotal = $debt->payment_total;

                $agent_summaries[$type] = isset($agent_summaries[$type]) ? ($agent_summaries[$type] + $ptotal) : $ptotal;
                $agent_summaries['all'] = isset($agent_summaries['all']) ? ($agent_summaries['all'] + $ptotal) : $ptotal;
            }

        }


        return isset($agent_summaries[$type_id]) ? $agent_summaries[$type_id] : 0;
    }


    public function getAgentContracts($org_id)
    {
        return AgentContracts::where('user_id', $this->id)->where('org_id', $org_id)->get()->last();
    }

    public function getTableColumns($table_key)
    {
        $user = \Auth::user();
        $user_columns = $user->columns()->get()
            ->where('table_key', $table_key)
            ->sortBy('pivot.orders')
            ->toArray();
        if(count($user_columns)==0){
            $user_columns = TableColumn::all()->where('table_key', $table_key)->toArray();
        }
        foreach($user_columns as &$v){
            $v['_key'] = ($v['is_as']==1) ? $v['as_key'] : $v['column_key'];
        }

        return $user_columns;
    }

    public function getPathParent()
    {
        $path = '';
        if($this->parent){
            if($this->parent_id != $this->id){
                $path = $this->parent->getPathParent();
            }

        }

        return "{$path}:{$this->id}:";
    }


    public function get_agent_financial_policies($sign_date)
    {
        $sign_date = getDateFormatEn($sign_date);
        $user_financial_group_id = null;


        if(isset($this->financial_group_id) && (int)$this->financial_group_id > 0  && $this->financial_group_date <= $sign_date){
            $user_financial_group_id = $this->financial_group_id;
        }

        $agent_fp = $this->agent_financial_policies()
            ->where('begin_date', '<=', $sign_date)
            ->where('end_date', '>=', $sign_date)
            ->get()
            ->first();

        if($agent_fp){
            $user_financial_group_id = $agent_fp->financial_group_id;
        }

        return $user_financial_group_id;
    }

}
