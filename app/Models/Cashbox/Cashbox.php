<?php

namespace App\Models\Cashbox;

use App\Models\Organizations\Organization;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Cashbox extends Model {

    protected $table = 'cashbox';

    protected $guarded = ['id'];

    public $timestamps = false;


    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function organization(){
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function setTransaction($type_id, $event_type_id, $total_sum, $event_date, $purpose_payment, $user_id, $invoice_payment_user_id = 0, $view_type = 0, $invoice_id = 0)
    {
        if((int)$event_type_id == 1){
            $total_sum = $total_sum *-1;
        }

        if($type_id == 0){
            $this->balance += $total_sum;
            $this->save();
        }

        $residue = $this->balance;

        $transaction = CashboxTransactions::create([
            'user_id' => $user_id,
            'event_date' => $event_date,
            'cashbox_id' => $this->id,
            'type_id' => $type_id,
            'event_type_id' => $event_type_id,
            'total_sum' => $total_sum,
            'residue' => $residue,
            'purpose_payment' => $purpose_payment,
            'invoice_payment_user_id' => $invoice_payment_user_id ? $invoice_payment_user_id : 0,
            'view_type_id' => $view_type ? $view_type : 0,
            'invoice_id' => $invoice_id ? $invoice_id : 0
        ]);




        return true;
    }


    public function getEarlyTotalSummary($start_date = null)
    {

        $summary = [
            'balance' => 0,
            'all' => 0,
            'coming' => 0,
            'consumption' => 0,
        ];

        $transactions = CashboxTransactions::where('cashbox_id', $this->id);
        $transactions->where('type_id', 0);

        $getdata = clone $transactions;

        if ($start_date){
            $lastdata = $getdata->where('event_date', '<', date("Y-m-d 00:00:00", strtotime($start_date)))->get()->last();
        }else{
            $lastdata = $getdata->where('event_date', '<', date("Y-m-d 00:00:00"))->get()->last();
        }

        if($lastdata)
        {
            $transactions->where('event_date', '>=', setDateTimeFormat(getDateFormatEn($lastdata->event_date).' 00:00:00'));
            $transactions->where('event_date', '<=', setDateTimeFormat(getDateFormatEn($lastdata->event_date).' 23:59:59'));

            $last_data_with_inc = clone $transactions;
            $transactions_consumption = clone $transactions;

            $summary = [
                'balance' => $lastdata->residue,
                'all' => $last_data_with_inc->sum('total_sum'),
                'coming' => $last_data_with_inc->where('event_type_id', 0)->sum('total_sum'), //
                'consumption' => $transactions_consumption->where('event_type_id', 1)->sum('total_sum'),
            ];

        }

        return $summary;

    }

    public function getCountTransaction()
    {
        $transactions = CashboxTransactions::where('cashbox_id', $this->id);
        return $transactions->count();
    }

}
