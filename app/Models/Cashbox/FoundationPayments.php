<?php

namespace App\Models\Cashbox;

use App\Models\Organizations\Organization;
use App\Models\Reports\ReportOrders;
use App\Models\User;
use App\Models\Users\UsersBalance;
use App\Models\Users\UsersBalanceTransactions;
use Illuminate\Database\Eloquent\Model;

class FoundationPayments extends Model {

    protected $table = 'foundation_payments';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function incomes_expenses(){
        return $this->hasMany(IncomeExpense::class, 'foundation_payments_id', 'id');
    }

    public function foundation_payments_balance(){
        return $this->belongsToMany(UsersBalanceTransactions::class, 'foundation_payments_balance','foundation_id', 'balance_transactions_id');
    }

    public static function getIdReportData($id)
    {
        return FoundationPayments::where('id', (int)$id)->get()->first();;
    }

    public static function getInfoReportData($month, $year)
    {
        $month = (int)$month;
        $month_title = ((int)$month<=9)?'0'.$month:$month;

        $result = new \stdClass();
        //Текущие
        $result->report_total = 0; // Маржа по отчетам
        $result->incomes_total = 0; // Доходы
        $result->expenses_total = 0; // Расходы
        $result->profit_total = 0; // прибыль

        //Фактические
        $fact = FoundationPayments::where('month', (int)$month)->where('year', (int)$year)->get()->first();
        if(!$fact){
            $fact = FoundationPayments::create([
                'month' => (int)$month,
                'year' => (int)$year,
                'user_id' => 0,
                'status_id' => 0,
                'fact_report_total' => 0,
                'fact_incomes_total' => 0,
                'fact_expenses_total' => 0,
                'fact_profit_total' => 0,
            ]);
        }


        $result->fact = $fact;
        $result->fact_id = (int)$fact->id;
        $result->status_id = $fact->status_id;

        if($fact->status_id == 1){

            $result->fact_date = $fact->accept_date;
            $result->user = $fact->user;

            $result->fact_report_total = $fact->fact_report_total; // Маржа по отчетам
            $result->fact_incomes_total = $fact->fact_incomes_total; // Доходы
            $result->fact_expenses_total = $fact->fact_expenses_total; // Расходы
            $result->fact_profit_total = $fact->fact_profit_total; // прибыль

            $result->payment_report_total = $fact->payment_report_total; // Маржа по отчетам
            $result->payment_incomes_total = $fact->payment_incomes_total; // Доходы
            $result->payment_expenses_total = $fact->payment_expenses_total; // Расходы
            $result->payment_profit_total = $fact->payment_profit_total; // прибыль

            $my_incomes_expenses = IncomeExpense::where('incomes_expenses.foundation_payments_id', '>=',$fact->id);
            $my_incomes_expenses->leftJoin('incomes_expenses_categories', 'incomes_expenses_categories.id', '=', 'incomes_expenses.category_id');

            $my_incomes = clone $my_incomes_expenses;
            $my_incomes->where('incomes_expenses_categories.type', 1);
            $my_expenses = clone $my_incomes_expenses;
            $my_expenses->where('incomes_expenses_categories.type', 2);


            $result->fact_delta_total = $my_incomes->sum('incomes_expenses.sum')-$my_expenses->sum('incomes_expenses.sum'); // Выставленные расходы или доходы

        }else{
            $result->fact_date = '';
            $result->user = null;
            $result->fact_report_total = 0; // Маржа по отчетам
            $result->fact_incomes_total = 0; // Доходы
            $result->fact_expenses_total = 0; // Расходы
            $result->fact_profit_total = 0; // прибыль

            $result->payment_report_total = null; // Маржа по отчетам
            $result->payment_incomes_total = null; // Доходы
            $result->payment_expenses_total = null; // Расходы
            $result->payment_profit_total = null; // прибыль

            $result->fact_delta_total = 0; // Выставленные расходы или доходы

        }


        $start_date = "$year-$month_title-01 00:00:00";
        $countDay = $number = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $end_date = "$year-$month_title-$countDay 23:59:59";

        //ОТЧЕТЫ
        $reports = ReportOrders::where('report_year', $year)->where('report_month', $month)->where('is_deleted', 0)->where('type_id', 0);
        $result->report_total = $reports->sum("marjing_total");//$reports->sum('amount_total')-($reports->sum('kv_agent_total')+$reports->sum('kv_recommender_total'));

        //Доходы
        $incomes_expenses = IncomeExpense::query()->where('status_id', 2);
        $incomes_expenses->leftJoin('incomes_expenses_categories', 'incomes_expenses_categories.id', '=', 'incomes_expenses.category_id');

        if($result->fact_id > 0){
            $fact_id = $result->fact_id;


            $incomes_expenses->where(function ($query) use ($start_date, $end_date, $fact_id) {
                $query->where(function ($query2) use ($start_date, $end_date){
                    $query2->where('incomes_expenses.date', '>=',$start_date)

                        ->where('incomes_expenses.date', '<=',$end_date)
                        ->where('incomes_expenses_categories.counting_in_foundation_payments', 1)
                        ->whereNull('incomes_expenses.foundation_payments_id');

                })->orWhere('incomes_expenses.foundation_payments_id', $fact_id);
            });




        }else{
            $incomes_expenses->where('incomes_expenses.date', '>=',$start_date)->where('incomes_expenses.date', '<=',$end_date)->whereNull('incomes_expenses.foundation_payments_id');
        }

        //dd(getLaravelSql($incomes_expenses));


        $incomes = clone $incomes_expenses;
        $incomes->where('incomes_expenses_categories.type', 1);
        $expenses = clone $incomes_expenses;
        $expenses->where('incomes_expenses_categories.type', 2);

        $result->incomes_total = $incomes->sum('incomes_expenses.sum'); // Доходы
        $result->expenses_total = $expenses->sum('incomes_expenses.sum'); // Расходы
        $result->profit_total = ($result->report_total+$result->incomes_total)-$result->expenses_total;


        return $result;
    }



}
