<?php

namespace App\Models\Cashbox;

use App\Classes\Export\TagModels\Cashbox\TagIncomeExpense;
use App\Models\Settings\IncomeExpenseCategory;
use App\Models\User;
use App\Traits\Models\ActiveConstTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\File;

class IncomeExpense extends Model {

    use ActiveConstTrait;

    protected $table = 'incomes_expenses';
    protected $guarded = ['id'];
    public $timestamps = false;

    const TAG_MODEL = TagIncomeExpense::class;

    const STATUS = [
        1 => 'Создан',
        2 => 'Оплачен'
    ];
    const PAYMENT_TYPE = [
        0 => 'Наличные',
        1 => 'Безналичные',
    ];

    public function category() {
        return $this->hasOne(IncomeExpenseCategory::class, 'id', 'category_id');
    }

    public function created_user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function documents() {
        return $this->belongsToMany(File::class, 'incomes_expenses_documents', 'income_id', 'file_id');
    }

    public function payment_user(){
        return $this->hasOne(User::class, 'id', 'payment_user_id');
    }


    public static function createIncomeExpense($category_id, $user_id, $payment_type, $date, $sum, $commission_total = 0, $fact_id = null, $comment = '')
    {
        $income_expense = new IncomeExpense();
        $income_expense->category_id = $category_id;
        $income_expense->user_id = $user_id;
        $income_expense->payment_type = $payment_type;
        $income_expense->status_id = 1;
        $income_expense->date = setDateTimeFormat($date);
        $income_expense->sum = getFloatFormat($sum);
        $income_expense->commission_total = getTotalSumToPrice($income_expense->sum, $commission_total);
        $income_expense->total = $income_expense->sum+$income_expense->commission_total;
        $income_expense->foundation_payments_id = $fact_id;
        $income_expense->comment = $comment;
        $income_expense->save();

        return $income_expense;
    }


}
