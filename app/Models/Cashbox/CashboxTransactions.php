<?php

namespace App\Models\Cashbox;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class CashboxTransactions extends Model
{

    /* Для понимания параметра `type_id` */
    const TYPE_ID = [
        0 => 'Нал',
        1 => 'Безнал',
        2 => 'Безнал СК',
    ];

    /* Для понимания параметра `view_type_id` */
    const VIEW_TYPE_ID = [
        0 => 'Транзакции',
        1 => 'Инкасации',
    ];

    /* Для понимания параметра `event_type_id` */
    const EVENT_TYPE_ID = [
        0 => 'Приход',
        1 => 'Списание',
    ];



    protected $table = 'cashbox_transactions';

    protected $guarded = ['id'];

    public $timestamps = false;


    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function invoice_payment_user()
    {
        return $this->hasOne(User::class, 'id', 'invoice_payment_user_id');
    }


}
