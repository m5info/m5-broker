<?php

namespace App\Models\Users;

use App\Classes\Export\TagModels\Cashbox\TagUserBalanceTransactions;
use App\Models\Settings\UserBalanceSettings;
use App\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Users\Type
 *
 * @property integer $id
 * @property string $title
 * @method static Builder|Type whereId($value)
 * @method static Builder|Type whereTitle($value)
 * @mixin Eloquent
 */
class UsersBalanceTransactions extends Model
{
    const TAG_MODEL = TagUserBalanceTransactions::class;

    protected $table = 'users_balance_transactions';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function balanse(){
        return $this->hasOne(UsersBalance::class, 'id', 'balance_id');
    }

}
