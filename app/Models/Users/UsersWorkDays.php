<?php

namespace App\Models\Users;

use App\Models\Directories\ManufacturingCalendar;
use App\Models\Settings\UserBalanceSettings;
use App\Models\User;
use App\Services\Front\api\KansaltingRIT_V2_1;
use Illuminate\Database\Eloquent\Model;


class UsersWorkDays extends Model
{



    protected $table = 'user_worked_day';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public static function getWorkDayUser($user_id, $mounth_from, $year_from){

        $days = self::query();
        $days->where('user_id', $user_id);

        $days->where('mounth', (int)$mounth_from);
        $days->where('year', (int)$year_from);
        if($day = $days->get()->first()){
            return (int)$day->worked_day;
        }
        return 0;

    }

    public static function updateWorkDayUser($mounth_from, $year_from){

        $mounth_from = (int)$mounth_from;
        $year_from = (int)$year_from;

        $freshManufacturingCalendar = ManufacturingCalendar::freshManufacturingCalendar($mounth_from, $year_from);
        $freshWorkedDay = KansaltingRIT_V2_1::getWorkedDayToRIT($mounth_from, $year_from);

        foreach ($freshWorkedDay as $fresh){
            $user = User::where('front_user_id', $fresh->user_rit_id)->get()->first();
            if($user){
                $fresh_day = self::query();
                $fresh_day->where('user_id', $user->id);
                $fresh_day->where('mounth', (int)$fresh->date_month);
                $fresh_day->where('year', (int)$fresh->date_year);
                $search = $fresh_day->get()->first();
                if($search){
                    $search->update([
                        'worked_day' => $fresh->worked_day
                    ]);
                }else{
                    self::create([
                        'user_id' => $user->id,
                        'mounth' => (int)$fresh->date_month,
                        'year' => (int)$fresh->date_year,
                        'worked_day' => $fresh->worked_day,
                    ]);
                }
            }
        }

        return true;
    }
}
