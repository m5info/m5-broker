<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;


class RoleSubpermissions extends Model
{
    protected $table = 'roles_subpermissions';

    protected $guarded = ['id'];

    public $timestamps = false;


    public function role(){
        return $this->hasOne(Role::class, 'id', 'role_id');
    }



}