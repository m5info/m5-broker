<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserThemes extends Model
{

    protected $table = 'user_themes';

    protected $guarded = ['id'];

    public $timestamps = false;

    const themes = [
        0 => ['name'=>'Стандарт','file'=>'standard', 'c1'=>'#df4321','c2'=>'#5a5a5a','c3'=>'#eeeeed' ,'fs'=> 14],
        1 => ['name'=>'Контраст','file'=>'contrast', 'c1'=>'#df4321','c2'=>'#000','c3'=>'#eeeeed','fs'=> 16],
        2 => ['name'=>'1с','file'=>'1c', 'c1'=>'#fff8b2','c2'=>'#fbf6c3','c3'=>'#f9e7a7','fs'=> 14],
        3 => ['name'=>'black','file'=>'black', 'c1'=>'#000','c2'=>'#797979','c3'=>'#1eaad8','fs'=> 14],
    ];

    public function setUserTheme (){

    }

    public function getUserTheme(){

    }
    static function setUserThemeStandard (){

        $theme = new UserThemes();

        $theme->user_id = auth()->id();
        $theme->css_file = 'standard';
        $theme->custom_styles = '14';
        $theme->save();

        return $theme->css_file;
    }
}
