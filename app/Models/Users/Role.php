<?php

namespace App\Models\Users;

use App\Models\User;
use App\Traits\Models\CustomRelationTrait;
use Illuminate\Database\Eloquent\Model;


class Role extends Model{

    use CustomRelationTrait;

    protected $table = 'roles';

    protected $guarded = ['id'];

    public $timestamps = false;


    public function permissions(){
        return $this->belongsToMany(Permission::class, 'roles_permissions');
    }

    public function users(){
        return $this->hasMany(User::class, 'role_id', 'id');
    }

    public function subpermissions(){
        $role = $this;
        return $this->custom(Subpermission::class, function ($relation) use ($role) {
            $relation->join('roles_subpermissions', 'roles_subpermissions.subpermission_id', '=', 'subpermissions.id');
            $relation->where('roles_subpermissions.role_id', $role->id);
        });
    }

    public function role_subpermissions(){
        return $this->hasMany(RoleSubpermissions::class, 'role_id');
    }

    public function permissions_menu(){
        $permissions = Permission::query();
        $permissions->leftJoin('roles_permissions', 'permissions.id', '=', 'roles_permissions.permission_id');
        $permissions->leftJoin('permissions_groups', 'permissions_groups.id', '=', 'permissions.group_id');
        $permissions->where('roles_permissions.role_id', $this->id);
        $permissions->select('permissions.title as permissions', 'permissions_groups.title as groups');
        return $permissions->get();
    }

    public function hasPermission($permissionId){
        return $this->permissions()->where('permission_id', $permissionId)->exists();
    }

    public function rolesVisibility($permissionId){
        return RolesVisibility::where('role_id', $this->id)->where('permission_id', $permissionId)->get()->last();
    }

    public function visibility($permission_group_name){
        if($permission_group = PermissionGroup::query()->where('title', $permission_group_name)->first()){
            return $this->rolesVisibility($permission_group->id)->visibility;
        }
        return false;
    }


}
