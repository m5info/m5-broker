<?php

namespace App\Models\Users;

use App\Models\Organizations\Organization;
use Illuminate\Database\Eloquent\Model;


class AgentContracts extends Model
{
    protected $table = 'agent_contracts';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function organization()
    {
        return $this->hasOne(Organization::class, 'id', 'org_id');
    }


}