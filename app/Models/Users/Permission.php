<?php

namespace App\Models\Users;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;


class Permission extends Model
{
    protected $table = 'permissions';

    protected $guarded = ['id'];

    public $timestamps = false;

    const TEMPL_TYPE = [
        0 => 'Вкладки',
        1 => 'Настраиваемый',
    ];

    public function group(){
        return $this->belongsTo(PermissionGroup::class);
    }

    public function subpermissions(){
        return $this->hasMany(Subpermission::class, 'permission_id');
    }

    public function roles(){
        return $this->belongsToMany(Role::class, 'roles_permissions');
    }

    public function users()
    {
        $permission_title = $this->title;
        $roles = Role::query()
            ->select('roles.id')
            ->leftJoin('roles_permissions', 'roles_permissions.role_id', '=', 'roles.id')
            ->leftJoin('permissions', 'roles_permissions.permission_id', '=', 'permissions.id')
            ->whereIn('roles_permissions.permission_id', function($sub) use ($permission_title){
                $sub->select('permissions.id')
                    ->from('permissions')
                    ->where('permissions.title', '=', $permission_title);
            });

        $users = User::query()
            ->whereIn('users.role_id', $roles);

        return $users;
    }


}