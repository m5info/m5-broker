<?php

namespace App\Models\Users;

use App\Models\Settings\FinancialGroup;
use Illuminate\Database\Eloquent\Model;


class AgentFinancialPolicies extends Model
{
    protected $table = 'agent_financial_policies';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function financial_policies_group()
    {
        return $this->hasOne(FinancialGroup::class, 'id', 'financial_group_id');
    }


}