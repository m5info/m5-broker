<?php

namespace App\Models\Users;

use App\Models\Settings\UserBalanceSettings;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;


class UsersBalance extends Model
{

    const TYPE_ID = [
        0 => 'Безнал',
        1 => 'Нал',
    ];

    const EVENT_TYPE_ID = [
        0 => 'Приход',
        1 => 'Списание',
    ];

    protected $table = 'users_balance';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function agent(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function type_balanse(){
        return $this->hasOne(UserBalanceSettings::class, 'id', 'balance_id');
    }

    public function getTransactions()
    {
        $transactions = UsersBalanceTransactions::where('balance_id', $this->id);
        return $transactions;
    }

    public function setTransactions($type_id, $event_type_id, $total_sum, $event_date, $purpose_payment, $user = null)
    {
        $cashbox_purpose = 'Зачисление';
        $total_sum_original = $total_sum;

        if((int)$event_type_id == 1){
            $total_sum = $total_sum *-1;
            $cashbox_purpose = 'Списание';
        }

        if(!$user){
            $user = auth()->user();
        }

        $transaction = UsersBalanceTransactions::create([
            'user_id' => $user->id,
            'create_date' => date("Y-m-d H:i:s"),
            'event_date' => $event_date,
            'balance_id' => $this->id,
            'type_id' => $type_id,
            'event_type_id' => $event_type_id,
            'total_sum' => $total_sum,
            'residue' => ($this->balance+$total_sum),
            'purpose_payment' => $purpose_payment,
        ]);

        $this->balance += $total_sum;
        $this->save();

        $cashbox_type = 1;

        if($type_id == 1){
            //ЕСЛИ НАЛ ТО КАССА
            $cashbox_type = 0;
        }

        $cashbox = $user->cashbox;
        if($cashbox){
            $cashbox->setTransaction(
                $cashbox_type,
                $event_type_id,
                $total_sum_original,
                date("Y-m-d H:i:s"),
                "$cashbox_purpose на баланс {$this->type_balanse->title} {$this->agent->name}:".$purpose_payment,
                auth()->id());
        }


        return $transaction;
    }


    public function setTransactionsMobile($type_id, $event_type_id, $total_sum, $event_date, $purpose_payment, $user = null)
    {
        $cashbox_purpose = 'Зачисление';
        $total_sum_original = $total_sum;

        if ((int)$event_type_id == 1) {
            $total_sum = $total_sum * -1;
            $cashbox_purpose = 'Списание';
        }

        if (!$user) {
            $user = auth()->user();
        }

        $transaction = UsersBalanceTransactions::create([
            'user_id' => $user->id,
            'create_date' => date("Y-m-d H:i:s"),
            'event_date' => $event_date,
            'balance_id' => $this->id,
            'type_id' => $type_id,
            'event_type_id' => $event_type_id,
            'total_sum' => $total_sum,
            'residue' => ($this->balance + $total_sum),
            'purpose_payment' => $purpose_payment,
        ]);

        $this->balance += $total_sum;
        $this->save();

        return $transaction;
    }



}
