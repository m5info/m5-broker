<?php

namespace App\Models\Contracts;

use Illuminate\Database\Eloquent\Model;


class ContractsTempPayments extends Model
{


    protected $table = 'contracts_temp_payments';

    protected $guarded = ['id'];

    public $timestamps = false;

    public static function saveDataPayment($data, $contract = null)
    {
        if(isset($data->id)){
            $payment = ContractsTempPayments::find($data->id);
        }else{

            $payment = ContractsTempPayments::create([
                'contract_id'=>$contract->id,
                'payment_number'=>(count($contract->temp_payments)+1),
            ]);

        }


        $payment->payment_number = (int)$data->payment_number;
        $payment->type_id = (int)$data->type_id;
        $payment->payment_total = getFloatFormat($data->payment_total);
        $payment->payment_data = setDateTimeFormat($data->payment_data);
        $payment->official_discount = getFloatFormat(isset($data->official_discount) ? $data->official_discount : 0);
        $payment->informal_discount = getFloatFormat(isset($data->informal_discount) ? $data->informal_discount : 0);
        $payment->bank_kv = getFloatFormat(isset($data->bank_kv) ? $data->bank_kv : 0);


        $payment->payment_type = (int)$data->payment_type;
        $payment->payment_flow = (int)$data->payment_flow;
        $payment->bso_not_receipt = (isset($data->bso_not_receipt))?(int)$data->bso_not_receipt:0;
        $payment->bso_receipt = (isset($data->bso_receipt))?$data->bso_receipt:'';
        $payment->bso_receipt_id = (int)$data->bso_receipt_id;

        $payment->save();

        //dd($data);



        return $payment->id;
    }

}
