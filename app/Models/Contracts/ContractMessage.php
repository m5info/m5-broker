<?php

namespace App\Models\Contracts;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class ContractMessage extends Model {

    protected $table = 'contract_messages';
    protected $guarded = ['id'];
    public $timestamps = true;


    const TYPE = [
        0 => 'Комментарий',
        1 => 'Сообщение об ошибке',
        2 => 'Сообщение о правках',
    ];

    const MESSAGE_CLASS = [
        0 => 'comment',
        1 => 'error',
        2 => 'edits',
    ];

    protected static function boot(){

        static::created(function ($contract_log) {

            $contract = Contracts::where('id', '=', $contract_log->contract_id)->first();
            if ($contract){
                $username = auth()->user()->name;

                if ($contract_log->type_id == 0){
                    $desc = 'Пользователь '.$username.' оставил комментарий: "'.$contract_log->message.'"';
                }elseif ($contract_log->type_id == 1){
                    $desc = 'Пользователь '.$username.' указал на ошибку: "'.$contract_log->message.'"';
                }elseif ($contract_log->type_id == 2){
                    $desc = 'Пользователь '.$username.' отправил на проверку, после исправлений: "'.$contract_log->message.'"';
                }

                ContractsLogs::create([
                    'contract_id' => $contract_log->contract_id,
                    'status_id' => $contract->statys_id,
                    'user_id' => auth()->id(),
                    'description' => $desc
                ]);
            }


        });

        parent::boot();
    }

    public function contract(){
        return $this->hasOne(Contracts::class, 'id', 'contract_id');
    }


    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }





}