<?php

namespace App\Models\Contracts;

use Illuminate\Database\Eloquent\Model;

class OrderComments extends Model
{
    protected $table = 'order_comments';

    protected $guarded = ['id'];

    public function order() {
        return $this->hasOne(Orders::class, 'id', 'order_id');
    }
}
