<?php

namespace App\Models\Contracts;

use App\Classes\Export\TagModels\Contracts\TagOrderDelivery;
use App\Classes\Export\TagModels\Finance\TagInvoice;
use App\Domain\Samplers\Contracts\TabsVisibility;
use App\Helpers\Visible;
use App\Models\BSO\BsoCarts;
use App\Models\File;
use App\Models\Orders\DeparturesCourierActs;
use App\Models\Settings\City;
use App\Models\Settings\DeparturesCourierPrice;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    const TAG_MODEL = TagOrderDelivery::class;
    const FILES_DOC = 'orders/docs';

    protected $table = 'orders';

    protected $guarded = ['id'];

    public $timestamps = false;

    const STATUSES = [
        0 => 'Временные',
        1 => 'Оформление',
        2 => 'На печати',
        3 => 'Доставка',
        4 => 'Реализованный',
        5 => 'Архив'
    ];

    public function agent() {
        return $this->hasOne(User::class, 'id', 'agent_id');
    }

    public function manager() {
        return $this->hasOne(User::class, 'id', 'manager_id');
    }

    public function insurer() {
        return $this->hasOne(Subjects::class, 'id', 'insurer_id');
    }

    public function courier() {
        return $this->hasOne(User::class, 'id', 'delivery_user_id');
    }

    public function departures_courier_price() {
        return $this->hasOne(DeparturesCourierPrice::class, 'id', 'departures_courier_price_id');
    }

    public function departures_courier_act() {
        return $this->hasOne(DeparturesCourierActs::class, 'id', 'departures_courier_act_id');
    }

    public function departures_courier_debt() {
        return $this->hasOne(Payments::class, 'id', 'departures_courier_debt_id');
    }

    public function contracts(){
        return $this->hasMany(Contracts::class, 'order_form_id', 'id');
    }

    public function bso_cart(){
        return $this->hasOne(BsoCarts::class, 'id', 'bso_cart_id');
    }

    public function main_contract(){
        return $this->hasOne(Contracts::class, 'id', 'main_contract_id');
    }

    public function object_insurer(){
        return $this->hasOne(ObjectInsurer::class, 'id', 'object_insurer_auto_id');
    }

    public function delivery_city(){
        return $this->hasOne(City::class, 'id', 'delivery_city_id');
    }

    public function documents()
    {
        return $this->belongsToMany(File::class, 'order_documents', 'order_id', 'file_id');
    }

    public function comments()
    {
        return $this->hasMany(OrderComments::class, 'order_id', 'id');
    }


    public static function getOrdersCountArr() {

        $result = [];

        $tabs_visibility = TabsVisibility::get_orders_tab_visibility();

        if(isset($tabs_visibility[0]['view']) && $tabs_visibility[0]['view'] == 1){
            $result[0] = ['title' => 'Временные', 'count' => Orders::getOrdersQuery()->whereIn('status_id', [0])->count()];
        }

        if(isset($tabs_visibility[1]['view']) && $tabs_visibility[1]['view'] == 1){
            $result[1] = ['title' => 'Оформление', 'count' => Orders::getOrdersQuery()->whereIn('status_id', [1])->count()];
        }

        if(isset($tabs_visibility[2]['view']) && $tabs_visibility[2]['view'] == 1){
            $result[2] = ['title' => 'На печати', 'count' => Orders::getOrdersQuery()->whereIn('status_id', [2])->count()];
        }

        if(isset($tabs_visibility[3]['view']) && $tabs_visibility[3]['view'] == 1){
            $result[3] = ['title' => 'Доставка', 'count' => Orders::getOrdersQuery()->whereIn('status_id', [3])->count()];
        }

        if(isset($tabs_visibility[4]['view']) && $tabs_visibility[4]['view'] == 1){
            $result[4] = ['title' => 'Реализованные', 'count' => Orders::getOrdersQuery()->whereIn('status_id', [4])->count()];
        }

        if(isset($tabs_visibility[5]['view']) && $tabs_visibility[5]['view'] == 1){
            $result[5] = ['title' => 'Архив', 'count' => 0];
        }


        return $result;
    }

    public static function getOrdersQuery() {
        return Visible::apply(Orders::query()->where('status_id', '>', -1), 'orders', ['agent_id', 'manager_id']);
    }

    public static function getOrdersId($id) {
        return Orders::getOrdersQuery()->where('id', $id)->get()->first();
    }

    public function setDeparturesCourierPrice()
    {
        $departures_courier_price = $this->departures_courier_price;

        $main_contract = $this->main_contract;


        if($main_contract->bso_id > 0){

            //Если есть долг
            if($this->departures_courier_debt)
            {

                if($this->departures_courier_debt->statys_id == 0){
                    $this->departures_courier_debt->payment_total = $departures_courier_price->debt_total;
                    $this->departures_courier_debt->save();
                }

            }else{
                //Если надо выстовить долг
                if($departures_courier_price->is_debt == 1){

                    $payment = new Payments();
                    $payment->type_id = 1;
                    $payment->statys_id = 0;
                    $payment->bso_id = $main_contract->bso->id;
                    $payment->contract_id = $main_contract->id;
                    $payment->invoice_id = null;
                    $payment->org_id = $this->courier->organization_id;
                    $payment->payment_total = $departures_courier_price->debt_total;
                    $payment->payment_data = getDateTime();
                    $payment->agent_id = $main_contract->manager_id;
                    $payment->comments = "Долг за доставку # {$this->id}";
                    $payment->save();

                    $this->departures_courier_debt_id = $payment->id;

                }
            }
        }


        //Если акт то пересчет зп
        if($this->departures_courier_act && $this->departures_courier_act->status_id != 2){
            $this->departures_courier_price_total = $departures_courier_price->price_total;

        }else{
            $this->departures_courier_price_total = $departures_courier_price->price_total;
        }


        $this->save();


        if($this->departures_courier_act)
        {
            $this->departures_courier_act->refreshPriceTotal();
        }


        return true;

    }


}
