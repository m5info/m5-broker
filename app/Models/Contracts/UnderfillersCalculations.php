<?php

namespace App\Models\Contracts;

use Illuminate\Database\Eloquent\Model;

class UnderfillersCalculations extends Model
{

    const KASKO_PRODUCTS = [
        1 => 'Каско (Ущерб + Угон)',
        2 => 'Ущерб',
        3 => 'Угон',
        4 => 'ДО',
        5 => 'ДАГО',
        6 => 'НС',
        7 => 'Дополнительный сервис',
        8 => 'Иной риск',
    ];

    protected $table = 'underfillers_calculations';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function contract(){
        return $this->hasOne(Contracts::class, 'id', 'contract_id');
    }

}
