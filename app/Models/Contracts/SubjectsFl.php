<?php

namespace App\Models\Contracts;

use App\Models\BSO\BsoItem;
use App\Models\Directories\InsuranceCompanies;
use App\Models\File;
use App\Models\Security\Security;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Settings\Organization
 *
 * @property integer $id
 * @property string $title
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereTitle($value)
 * @mixin \Eloquent
 * @property integer $next_act
 * @property string $default_purpose_payment
 * @property string $inn
 * @property float $limit_year
 * @property float $spent_limit_year
 * @property integer $is_actual
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereNextAct($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereDefaultPurposePayment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereInn($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereLimitYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereSpentLimitYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereIsActual($value)
 */
class SubjectsFl extends Model
{
    protected $table = 'subjects_fl';

    protected $guarded = ['id'];

    const KBM_CLASS = [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13
    ];

    public function driver() {
        return $this->hasOne(Driver::class, 'subject_id', 'subject_id');
    }


    public function subject() {
        return $this->hasOne(Subjects::class, 'id', 'subject_id');
    }

    public function get_fio()
    {
        $fio = explode(' ', $this->fio);

        return [
            'last_name' => $fio[0],
            'first_name' => isset($fio[1]) ? $fio[1] : $fio[0],
            'middle_name' => isset($fio[2]) ? $fio[2] : $fio[0],
        ];
    }

    public function updateInfo($data, $subject_id)
    {
        $info = $this;

        $info->subject_id = $subject_id;
        $info->fio = $data->fio;
        $info->first_name = $data->first_name;
        $info->second_name = $data->second_name;
        $info->last_name = $data->last_name;
        $info->sex = $data->sex;

        $info->birthdate = $data->birthdate !== '' ? getDateFormatEn($data->birthdate) : '';
        $info->address_born = $data->address_born;
        $info->address_born_kladr = $data->address_born_kladr;

        $info->address_register = $data->address_register;
        $info->address_register_kladr = $data->address_register_kladr;
        $info->address_register_zip = $data->address_register_zip;
        $info->address_register_okato = $data->address_register_okato;

        $info->address_register_city_fias_id = $data->address_register_city_fias_id;

        $info->address_register_region = $data->address_register_region;
        $info->address_register_region_full = $data->address_register_region_full ?? '';
        $info->address_register_city = $data->address_register_city;
        $info->address_register_city_kladr_id = $data->address_register_city_kladr_id;
        $info->address_register_settlement = isset($data->address_register_settlement) ? $data->address_register_settlement : '';
        $info->address_register_settlement_kladr_id = isset($data->address_register_settlement_kladr_id) ? $data->address_register_settlement_kladr_id : '';
        $info->address_register_street = $data->address_register_street;
        $info->address_register_house = $data->address_register_house;
        $info->address_register_block = $data->address_register_block;
        $info->address_register_flat = $data->address_register_flat;


        $info->address_fact = $data->address_fact;
        $info->address_fact_kladr = $data->address_fact_kladr;
        $info->address_fact_zip = $data->address_fact_zip;
        $info->address_fact_okato = $data->address_fact_okato;

        $info->address_fact_city_fias_id = $data->address_fact_city_fias_id;

        $info->address_fact_region = $data->address_fact_region;
        $info->address_fact_region_full = $data->address_fact_region_full ?? '';
        $info->address_fact_city = $data->address_fact_city;
        $info->address_fact_city_kladr_id = $data->address_fact_city_kladr_id;
        $info->address_fact_settlement = isset($data->address_fact_settlement) ? $data->address_fact_settlement : '';
        $info->address_fact_settlement_kladr_id = isset($data->address_fact_settlement_kladr_id) ? $data->address_fact_settlement_kladr_id : '';
        $info->address_fact_street = $data->address_fact_street;
        $info->address_fact_house = $data->address_fact_house;
        $info->address_fact_block = $data->address_fact_block;
        $info->address_fact_flat = $data->address_fact_flat;

        $info->doc_type = $data->doc_type;
        $info->doc_serie = $data->doc_serie;
        $info->doc_number = $data->doc_number;
        $info->doc_date = $data->doc_date !== '' ? getDateFormatEn($data->doc_date) : '';
        $info->doc_office = $data->doc_office;
        $info->doc_info = $data->doc_info;
        $info->class_kmb = isset($data->class_kmb) ? $data->class_kmb : '';
        $info->count_insurance_cases = isset($data->count_insurance_cases) ? $data->count_insurance_cases : '';
        $info->license_serie = isset($data->license_serie) ? $data->license_serie : '';
        $info->license_number = isset($data->license_number) ? $data->license_number : '';
        $info->license_date = isset($data->license_date) ? $data->license_date : '';
        $info->exp_date = isset($data->exp_date) ? $data->exp_date : '';


        $info->save();

        return $info;
    }

}
