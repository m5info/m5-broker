<?php

namespace App\Models\Contracts;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @mixin \Eloquent
 */
class OptionalEquipment extends Model{

    protected $table = 'optional_equipment';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function contract(){
        return $this->hasOne(Contracts::class, 'id', 'contract_id');
    }

}