<?php

namespace App\Models\Contracts;

use App\Models\BSO\BsoItem;
use App\Models\Directories\InsuranceCompanies;
use App\Models\File;
use App\Models\Security\Security;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Settings\Organization
 *
 * @property integer $id
 * @property string $title
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereTitle($value)
 * @mixin \Eloquent
 * @property integer $next_act
 * @property string $default_purpose_payment
 * @property string $inn
 * @property float $limit_year
 * @property float $spent_limit_year
 * @property integer $is_actual
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereNextAct($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereDefaultPurposePayment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereInn($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereLimitYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereSpentLimitYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereIsActual($value)
 */
class SubjectsUl extends Model
{
    protected $table = 'subjects_ul';

    protected $guarded = ['id'];

    public function updateInfo($data, $subject_id)
    {
        $info = $this;

        $info->subject_id = $subject_id;
        $info->fio = $data->fio;
        $info->first_name = $data->first_name;
        $info->second_name = $data->second_name;
        $info->last_name = $data->last_name;
        $info->position = $data->position;
        $info->birthdate = $data->birthdate !== '' ? getDateFormatEn($data->birthdate) : '';

        $info->address_register = $data->address_register;
        $info->address_register_kladr = $data->address_register_kladr;
        $info->address_register_zip = $data->address_register_zip;
        $info->address_register_okato = $data->address_register_okato;

        $info->address_register_city_fias_id = $data->address_register_city_fias_id;

        $info->address_register_region = $data->address_register_region;
        $info->address_register_region_full = $data->address_register_region_full ?? '';
        $info->address_register_city = $data->address_register_city;
        $info->address_register_city_kladr_id = $data->address_register_city_kladr_id;
        $info->address_register_settlement = isset($data->address_register_settlement) ? $data->address_register_settlement : '';
        $info->address_register_settlement_kladr_id = isset($data->address_register_settlement_kladr_id) ? $data->address_register_settlement_kladr_id : '';
        $info->address_register_street = $data->address_register_street;
        $info->address_register_house = $data->address_register_house;
        $info->address_register_block = $data->address_register_block;
        $info->address_register_flat = $data->address_register_flat;


        $info->address_fact = $data->address_fact;
        $info->address_fact_kladr = $data->address_fact_kladr;
        $info->address_fact_zip = isset($data->address_fact_zip) ? $data->address_fact_zip : '';
        $info->address_fact_okato = isset($data->address_fact_okato) ? $data->address_fact_okato : '';

        $info->address_fact_city_fias_id = isset($data->address_fact_city_fias_id) ? $data->address_fact_city_fias_id : '';

        $info->address_fact_region = $data->address_fact_region;
        $info->address_fact_region_full = $data->address_fact_region_full ?? '';
        $info->address_fact_city = $data->address_fact_city;
        $info->address_fact_city_kladr_id = $data->address_fact_city_kladr_id;
        $info->address_fact_settlement = isset($data->address_fact_settlement) ? $data->address_fact_settlement : '';
        $info->address_fact_settlement_kladr_id = isset($data->address_fact_settlement_kladr_id) ? $data->address_fact_settlement_kladr_id : '';
        $info->address_fact_street = $data->address_fact_street;
        $info->address_fact_house = $data->address_fact_house;
        $info->address_fact_block = $data->address_fact_block;
        $info->address_fact_flat = $data->address_fact_flat;

        $info->title = $data->title;
        $info->inn = $data->inn;
        $info->kpp = $data->kpp;
        $info->bik = $data->bik;
        $info->general_manager = $data->general_manager;
        $info->ogrn = $data->ogrn;
        $info->doc_serie = $data->doc_serie;
        $info->doc_number = $data->doc_number;


        $info->save();

        return $info;
    }

}
