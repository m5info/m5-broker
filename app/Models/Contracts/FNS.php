<?php

namespace App\Models\Contracts;


use App\Models\Organizations\Organization;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @mixin \Eloquent
 */
class FNS extends Model{

    protected $table = 'fns_ferma';

    protected $guarded = ['id'];

    public $timestamps = false;


    public function user() {
        return $this->hasOne(User::class, 'id','user_id');
    }

    public function create_user() {
        return $this->hasOne(User::class, 'id','create_user_id');
    }

    public function organization() {
        return $this->hasOne(Organization::class, 'id','org_id');
    }

}