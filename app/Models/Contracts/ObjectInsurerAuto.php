<?php

namespace App\Models\Contracts;

use App\Models\BSO\BsoItem;
use App\Models\Directories\InsuranceCompanies;
use App\Models\File;
use App\Models\Security\Security;
use App\Models\User;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;
use App\Models\Vehicle\VehicleCategories;
use App\Models\Vehicle\VehiclePurpose;
use App\Traits\Models\GetRelatedTrait;
use Illuminate\Database\Eloquent\Model;


class ObjectInsurerAuto extends Model
{

    use GetRelatedTrait;

    protected $table = 'object_insurer_auto';

    protected $guarded = ['id'];

    public $timestamps = false;

    const TYPE_ENGINE = [
        0 => 'Бензин',
        1 => 'Дизель',
        2 => 'Электро',
        3 => 'Гибрид'
    ];

    const BRAKE_SYSTEM = [
        0 => 'Механический',
        1 => 'Гидравлический',
        2 => 'Пневматический',
        3 => 'Комбинированный',
        4 => 'Без тормозной системы',
    ];

    const DOC_KIND_PTS = [
        0 => 'ПТС',
        1 => 'ЭПТС',
    ];

    const TYPE_KPP = [
        0 => 'Автоматическая',
        1 => 'Механическая',
    ];

    public function mark()
    {
        return $this->hasOne(VehicleMarks::class, 'id', 'mark_id');
    }

    public function model()
    {
        return $this->hasOne(VehicleModels::class, 'id', 'model_id');
    }

    public function category_auto()
    {
        return $this->hasOne(VehicleCategories::class, 'id', 'ts_category');
    }

    public function target_use()
    {
        return $this->hasOne(VehiclePurpose::class, 'id', 'purpose_id');
    }


}
