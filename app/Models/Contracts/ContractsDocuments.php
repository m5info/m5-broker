<?php

namespace App\Models\Contracts;

use App\Models\BSO\BsoItem;
use App\Models\Directories\InsuranceCompanies;
use App\Models\File;
use App\Models\Security\Security;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;


class ContractsDocuments extends Model
{

    const FILES_DOC = 'contracts/docs';

    protected $table = 'contracts_documents';

    protected $guarded = ['id'];

    public $timestamps = false;


    public function file()
    {
        return $this->hasOne(File::class, 'id', 'file_id');
    }

}
