<?php

namespace App\Models\Contracts;

use App\Domain\Processes\Operations\Contracts\Online\ContractCalculationOperations;
use App\Domain\Samplers\Segments\SegmentsOnRequest;
use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Api\SK\ApiSetting;
use App\Models\Dictionaries\DictionarySkSk;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\Products;
use App\Models\Directories\ProductsPrograms;
use App\Models\Vehicle\APISettings\VehicleAntiTheftSystemSK;
use App\Models\Vehicle\APISettings\VehicleCategoriesSK;
use App\Models\Vehicle\APISettings\VehicleColorSK;
use App\Models\Vehicle\APISettings\VehicleMarksSK;
use App\Models\Vehicle\APISettings\VehicleModelsSK;
use App\Models\Vehicle\APISettings\VehiclePurposeSK;
use App\Models\Vehicle\VehicleColor;
use App\Traits\Models\ActiveConstTrait;
use App\Traits\Models\CustomRelationTrait;
use App\Traits\Models\GetRelatedTrait;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class ContractsCalculation extends Model {

    use ActiveConstTrait, CustomRelationTrait, GetRelatedTrait;

    protected $casts = [
        'json' => 'json',
        'messages' => 'json',
    ];

    protected $table = 'online_calculations';
    protected $guarded = ['id'];
    public $timestamps = true;

    const STATUS = [
        -1=> "Не сегмент/Данные изменены",
        0 => "Создан/Расчёт не выполнен",
        1 => "Расчёт выполнен",
        2 => "На подтверждении",
        3 => "На согласовании",
        4 => "Выпущен",
    ];

    /*
     * RELATIONS
     */

    public function bso_supplier() {
        return $this->hasOne(BsoSuppliers::class, 'id', 'bso_supplier_id');
    }

    public function insurance_companies() {
        return $this->hasOne(InsuranceCompanies::class, 'id', 'insurance_companies_id');
    }

    public function contract(){
        return $this->hasOne(Contracts::class, 'id','contract_id');
    }

    public function product() {
        return $this->hasOne(Products::class, 'id', 'product_id');
    }

    public function program() {
        return $this->hasOne(ProductsPrograms::class, 'id', 'program_id');
    }



    /*
     * HELP RELATIONS
     */

    public function getProductOrProgram()
    {
        return ($this->program)?$this->program:$this->product;
    }


    public function object_insurer_auto(){
        try{ return $this->contract->object_insurer_auto(); } catch(Exception $e){ return null; }
    }

    public function drivers(){
        try{ return $this->contract->drivers(); } catch(Exception $e){ return null; }
    }

    /*
    * END RELATIONS
    */

    public static function createCalc($contract, $product, $program, $is_set = false)
    {
        //СОЗДАНИЕ КАЛЬКУЛЯТОРА

        $calculation = new ContractsCalculation();
        $calculation->contract_id = $contract->id;

        if($program){
            //ПРИДУМАТЬ ПОИСК АКТВНОЙ АПИ
            $api = ApiSetting::where('is_actual', 1)->where('dir_name', $program->dir_name)->get()->first();
            if($api){
                $calculation->insurance_companies_id = $api->bso_supplier->insurance_companies_id;
                $calculation->bso_supplier_id = $api->bso_supplier->id;
                $calculation->program_id = $program->id;
            }else{
                return null;
            }

        }

        $calculation->statys_id = 0;
        $calculation->product_id = $product->id;
        $calculation->save();

        if($is_set == true) ContractCalculationOperations::select_calc($contract, $calculation);

        return $calculation;
    }





    /*
     * API
     */

    public function sk_purpose(){

        $calc = $this;

        return $this->custom_one(VehiclePurposeSK::class, function($query) use ($calc){

            $vehicle_purpose_id = $calc->contract &&
                $calc->contract->object_insurer_auto &&
                $calc->contract->object_insurer_auto->target_use ?
                $calc->contract->object_insurer_auto->target_use->id : -1;

            $query->where('insurance_companies_id', $calc->insurance_companies_id)
                ->where('bso_supplier_id', $calc->bso_supplier_id)
                ->where('vehicle_purpose_id', $vehicle_purpose_id);

        });

    }

    public function sk_category(){

        $calc = $this;

        return $this->custom_one(VehicleCategoriesSK::class, function($query) use ($calc){

            $vehicle_category_id = $calc->contract &&
                $calc->contract->object_insurer_auto &&
                $calc->contract->object_insurer_auto->category_auto ?
                $calc->contract->object_insurer_auto->category_auto->id : -1;

            $query->where('insurance_companies_id', $calc->insurance_companies_id)
                ->where('bso_supplier_id', $calc->bso_supplier_id)
                ->where('vehicle_categorie_id', $vehicle_category_id);

        });

    }

    public function sk_mark(){

        $calc = $this;

        return $this->custom_one(VehicleMarksSK::class, function($query) use ($calc){

            $mark_id = $calc->contract &&
                $calc->contract->object_insurer_auto &&
                $calc->contract->object_insurer_auto->mark ?
                $calc->contract->object_insurer_auto->mark->id : -1;

            $query->where('insurance_companies_id', $calc->insurance_companies_id)
                ->where('bso_supplier_id', $calc->bso_supplier_id)
                ->where('vehicle_marks_id', $mark_id);

        });

    }

    public function sk_model(){

        $calc = $this;

        return $this->custom_one(VehicleModelsSK::class, function($query) use ($calc){

            $model_id = $calc->contract &&
                $calc->contract->object_insurer_auto &&
                $calc->contract->object_insurer_auto->model ?
                $calc->contract->object_insurer_auto->model->id : -1;

            $query->where('insurance_companies_id', $calc->insurance_companies_id)
                ->where('bso_supplier_id', $calc->bso_supplier_id);
            if ($model_id == 0){
                    $query->where('vehicle_models_id', NULL);
            }else{
                $query->where('vehicle_models_id', $model_id);
            }

        });

    }


    public function sk_color(){

        $calc = $this;

        return $this->custom_one(VehicleColorSK::class, function($query) use ($calc){

            $color_id = $calc->contract->object_insurer_auto->color_id;

            $query->where('insurance_companies_id', $calc->insurance_companies_id)
                ->where('bso_supplier_id', $calc->bso_supplier_id)
                ->where('color_id', $color_id);

        });

    }

    public function sk_sk(){

        $calc = $this;

        return $this->custom_one(DictionarySkSk::class, function($query) use ($calc){

            $sk_id = $calc->contract->prev_sk_id;

            $query->where('insurance_companies_id', $calc->insurance_companies_id)
                ->where('bso_supplier_id', $calc->bso_supplier_id)
                ->where('dictionary_sk_id', $sk_id);

        });

    }

    public function sk_anti_theft_system(){

        $calc = $this;

        return $this->custom_one(VehicleAntiTheftSystemSK::class, function($query) use ($calc){

            $anti_theft_system_id = $calc->contract->object_insurer_auto->anti_theft_system_id;

            $query->where('insurance_companies_id', $calc->insurance_companies_id)
                ->where('bso_supplier_id', $calc->bso_supplier_id)
                ->where('anti_theft_system_id', $anti_theft_system_id);

        });

    }





    public function get_product_service(){

        if($contract = $this->contract){

            $product = $this->product;
            $program = $this->getProductOrProgram();
            $bso_supplier = $this->bso_supplier;


            if($product && $bso_supplier){


                //существует сервис контроллер для ск
                if($service_controller = $bso_supplier->get_service_controller()) {
                    //у ск есть сервис для этого продукта
                    if ($service = $service_controller->get_service($program->slug)) {

                        //сервис реализован как сервис и можем дёргать методы
                        if ($service instanceof ProductServiceInterface) {

                            return $service;

                        }

                    }

                }

            }

        }

        return false;
    }


    public function get_available_types(){

        $types = [
            0 => 'Бумажный',
            1 => 'Электронный',
        ];

        if($service = $this->get_product_service()){

            if(isset($service->available_types) && is_array($service->available_types)){

                $types = $service->available_types;

            }

        }

        return $types;


    }


    public function calculate()
    {
        $calculation = $this;
        $calculation->statys_id = 0;
        $calculation->sum = 0;

        $state = false;

        if($service = $calculation->get_product_service()){


            $calc = $service->calc($calculation);
            $state = $calc->state;
            if($calc->state == true){

                $calculation->sum = getFloatFormat($calc->payment_total);
                $calculation->statys_id = $calc->statys_id;
                $calculation->sk_key_id = $calc->sk_key_id;
                $calculation->tariff = Str::one_line($calc->msg);
                if(isset($calc->payments_sizes)){
                    $calculation->payments_sizes = $calc->payments_sizes;
                }

                $suitable_segments = SegmentsOnRequest::get_suitable_companies_segment($calculation);
                if($suitable_segments && sizeof($suitable_segments)){
                    if(isset($suitable_segments[0])){
                        $calculation->financial_policy_id = (int)$suitable_segments[0]['id'];
                        $calculation->save();
                    }
                }


                $fp = FinancialPolicy::find($calculation->financial_policy_id);

                $calculation->agent_kv = $fp->getUserKV($this->contract->agent_id)->kv_agent;

            }else{
                $calculation->messages = $calc->error;
                if(isset($calc->payment_total)){
                    $calculation->sum = getFloatFormat($calc->payment_total);
                }

            }

        }
        $calculation->save();

        return $state;

    }

    public function accept_save_policy_sk()
    {
        $calculation = $this;
        $calculation->sum = 0;

        $state = false;

        if($service = $calculation->get_product_service()){


            $calc = $service->savePolicy($calculation);
            $state = $calc->state;
            if($calc->state == true){

                $calculation->sum = getFloatFormat($calc->payment_total);
                $calculation->tariff = Str::one_line($calc->msg);
                if(isset($calc->payments_sizes)){
                    $calculation->payments_sizes = $calc->payments_sizes;
                }

                $fp = FinancialPolicy::find($calculation->financial_policy_id);
                $calculation->agent_kv = $fp->getUserKV($this->contract->agent_id)->kv_agent;

            }else{
                $calculation->messages = $calc->error;
            }

        }
        $calculation->save();

        return $state;

    }



    public function temp_calc()
    {
        $calculation = $this;
        $calculation->statys_id = 0;
        $calculation->sum = 0;



        if($service = $calculation->get_product_service()){


            $calc = $service->temp_calc($calculation);

            if($calc->state == true){

                $calculation->sum = getFloatFormat($calc->payment_total);
                $calculation->statys_id = $calc->statys_id;
                $calculation->sk_key_id = $calc->sk_key_id;
                $calculation->tariff = Str::one_line($calc->msg);

            }else{

                $calculation->messages = $calc->error;
            }



        }
        $calculation->save();

        return true;
    }


}
