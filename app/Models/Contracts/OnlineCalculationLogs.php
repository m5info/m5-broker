<?php

namespace App\Models\Contracts;

use Illuminate\Database\Eloquent\Model;

class OnlineCalculationLogs extends Model{

    protected $table = 'online_calculations_logs';
    protected $guarded = ['id'];
    public $timestamps = true;
}