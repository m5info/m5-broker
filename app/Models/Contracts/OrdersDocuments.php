<?php

namespace App\Models\Contracts;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;

class OrdersDocuments extends Model
{
    const FILES_DOC = 'orders/docs';

    protected $table = 'order_documents';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function file()
    {
        return $this->hasOne(File::class, 'id', 'file_id');
    }
}
