<?php

namespace App\Models\Contracts;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class ContractTerms extends Model {

    protected $table = 'contracts_terms';
    protected $guarded = ['id'];
    public $timestamps = false;



    public function contract(){
        return $this->hasOne(Contracts::class, 'id', 'contract_id');
    }


}