<?php

namespace App\Models\Contracts;

use Illuminate\Database\Eloquent\Model;

class RemovedPayments extends Model
{
    protected $table = 'removed_payments';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function payment(){
        return $this->hasOne(Payments::class, 'id', 'payment_id');
    }

    /**
     * Восстановление всех параметров платежа исходя из сохраненных данных
     */
    public function restorePayment()
    {
        $removedPayment = $this;
        $payment = $removedPayment->payment;

        if($paymentOldData = $removedPayment->old_data){
            $paymentOldData = \GuzzleHttp\json_decode($paymentOldData);

            foreach($paymentOldData as $column => $value){
                $payment->{$column} = $value;
            }

            $payment->save();
        }
    }

}
