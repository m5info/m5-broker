<?php

namespace App\Models\Contracts;

use App\Classes\Notification\ContractLogNotifier;
use App\Classes\Notification\NotificationManager;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class ContractsLogs extends Model {

    protected $table = 'contracts_logs';
    protected $guarded = ['id'];

    const NOTIFIER = ContractLogNotifier::class;

    protected static function boot(){

        static::created(function($contract_log){
           NotificationManager::handle($contract_log);
        });

        parent::boot();
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }


}
