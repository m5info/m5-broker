<?php

namespace App\Models\Contracts;

use App\Scopes\DebtScope;

class Debts extends Payments{

    public $table = 'payments';

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new DebtScope());
    }


}