<?php

namespace App\Models\Contracts;

use App\Classes\Export\TagModels\Contracts\TagContracts;
use App\Domain\Entities\Payments\EPayment;
use App\Domain\Samplers\Contracts\TabsVisibility;
use App\Helpers\Visible;
use App\Models\Actions\ExpectedPayments;
use App\Models\Actions\PaymentAccept;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\InstallmentAlgorithms;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\ProductsPrograms;
use App\Models\Directories\TypeBso;
use App\Models\File;
use App\Models\Orders\Inspection;
use App\Models\Orders\InspectionComments;
use App\Models\Orders\InspectionOrdersLogs;
use App\Models\Orders\ProcessingOrdersLogs;
use App\Models\Settings\Bank;
use App\Models\Settings\InstallmentAlgorithmsList;
use App\Models\User;
use App\Services\Front\IntegrationFront;
use App\Traits\Models\ActiveConstTrait;
use App\Traits\Models\CustomRelationTrait;
use App\Traits\Models\GetRelatedTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Directories\Products;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class Contracts
 * @property ContractsCalculation $selected_calculation
 * @property ContractMessage $errors
 * @property ContractMessage $messages
 * @property Collection $drivers
 * @property BsoSuppliers $bso_supplier
 */
class Contracts extends Model {

    use ActiveConstTrait,
        GetRelatedTrait,
        CustomRelationTrait;

    const FILES_DOC = 'contracts/docs';

    const TAG_MODEL = TagContracts::class;

    protected $table = 'contracts';
    protected $guarded = ['id'];
    public $timestamps = true;

    const SALES_CONDITION = [
        0 => 'Агентская',
        1 => 'Продажа организации',
        2 => 'Агента через организацию',
    ];

    const STATYS = [
        -2=> 'Оформление',
        -1=> 'Удален',
        0 => 'Создан',
        1 => 'В проверке',
        2 => 'Коррекция',
        3 => 'Подтверждение оплаты',
        4 => 'Выпущен',
        5 => 'Сдан в кассу',
        6 => 'Ожидает подтверждения (АТОЛ)',
        7 => 'Закрытие коррекции',
    ];

    const KIND_ACCEPTANCE = [
        0 => 'Условный',
        1 => 'Безусловный',
        2 => 'По служебке',
        3 => 'Не проверен',
    ];

    const IS_PERSONAL_SALES = [
        0 => 'Нет',
        1 => 'Да'
    ];


    const DRIVERS_TYPE = [
        0=>"Ограниченный поименный список ЛДУ",
        1=>'Неограниченный, список ЛДУ (мультидрайв)'
    ];

    const TYPE = [
        1 => 'Первичный',
        2 => 'Пролонгация',
    ];

    
    protected static function boot(){

        static::created(function ($contract) {
            ContractsLogs::create([
                'contract_id' => $contract->id,
                'status_id' => 0,
                'user_id' => auth()->id(),
            ]);
        });


        static::updated(function ($contract) {

            $last_status = $contract->logs->last() ? $contract->logs->last()->status_id : -10;

            if ($last_status !== $contract->statys_id) {
                ContractsLogs::create([
                    'contract_id' => $contract->id,
                    'status_id' => $contract->statys_id,
                    'user_id' => auth()->id(),
                ]);
            }
        });

        parent::boot();
    }


    /*
     * RELATIONS
     */
    public function errors(){
        return $this->hasMany(ContractMessage::class, 'contract_id')->where('type_id', 1);
    }

    public static function releasedContracts(){
        $contracts = Contracts::query()->where('statys_id', '=', 4);
        return $contracts;
    }

    public function messages(){
        return $this->hasMany(ContractMessage::class, 'contract_id');
    }

    public function chats(){
        return $this->hasMany(ContractsChat::class, 'contract_id');
    }

    public function selected_calculation(){
        return $this->hasOne(ContractsCalculation::class, 'id', 'selected_calculation_id');
    }

    public function drivers(){
        return $this->hasMany(Driver::class, 'contract_id');
    }

    public function expected_payments(){
        return $this->hasMany(ExpectedPayments::class, 'contract_id', 'id');
    }

    public function bank() {
        return $this->hasOne(Bank::class, 'id', 'bank_id');
    }

    public function bso_supplier() {
        return $this->hasOne(BsoSuppliers::class, 'id', 'bso_supplier_id');
    }

    public function logs() {
        return $this->hasMany(ContractsLogs::class, 'contract_id');
    }

    public function user(){
        return $this->hasOne(User::class, 'id' ,'user_id');
    }

    public function referencer(){
        return $this->hasOne(User::class, 'id' ,'referencer_id');
    }

    public function bso() {
        return $this->hasOne(BsoItem::class, 'id', 'bso_id')->with('product','user','type','supplier');
    }

    public function inspection(){
        return $this->hasOne(Inspection::class, 'id', 'inspection_id');
    }

    public function underfillers_calculation(){
        return $this->hasOne(UnderfillersCalculations::class, 'contract_id', 'id');
    }

    public function insurance_companies() {
        return $this->hasOne(InsuranceCompanies::class, 'id', 'insurance_companies_id');
    }

    public function insurance_companies_algorithm() {
        return $this->hasOne(InstallmentAlgorithms::class, 'id', 'installment_algorithms_id');
    }

    public function installment_algorithm() {
        return $this->hasOne(InstallmentAlgorithmsList::class, 'id', 'installment_algorithms_id');
    }

    public function agent() {
        return $this->hasOne(User::class, 'id', 'agent_id');
    }

    public function manager() {
        return $this->hasOne(User::class, 'id', 'manager_id');
    }

    public function insurer() {
        return $this->hasOne(Subjects::class, 'id', 'insurer_id');
    }

    public function owner() {
        return $this->hasOne(Subjects::class, 'id', 'owner_id');
    }

    public function beneficiar() {
        return $this->hasOne(Subjects::class, 'id', 'beneficiar_id');
    }

    /* Андеррайтер */
    public function check_user() {
        return $this->hasOne(User::class, 'id', 'check_user_id');
    }

    public function calculation() {
        return $this->belongsTo(ContractsCalculation::class, 'id', 'contract_id')->where('id','=', $this->selected_calculation_id);
    }

    public function online_calculations() {
        return $this->hasMany(ContractsCalculation::class, 'contract_id', 'id')
            ->where('statys_id','!=', -1);
    }

    public function inspection_comments() {
        return $this->hasMany(InspectionComments::class, 'contract_id', 'id');
    }

    public function inspection_logs() {
        return $this->hasMany(InspectionOrdersLogs::class, 'inspection_order_id', 'id');
    }

    public function last_inspection_log() {
        $logs = InspectionOrdersLogs::where('inspection_order_id', '=', $this->id);
        return $logs->get()->last();
    }

    public function online_calculations_history() {
        return $this->hasMany(ContractsCalculation::class, 'contract_id', 'id')
            ->where('statys_id','!=', -1)->where('id','!=', $this->selected_calculation_id);
    }

    public function product() {
        return $this->hasOne(Products::class, 'id', 'product_id');
    }

    public function program() {
        return $this->hasOne(ProductsPrograms::class, 'id', 'program_id');
    }

    public function object_insurer() {
        return $this->hasOne(ObjectInsurer::class, 'id', 'object_insurer_id');
    }

    public function object_insurer_auto(){
        return $this->hasOne(ObjectInsurerAuto::class, 'object_insurer_id', 'object_insurer_id');
    }

    public function object_insurer_flats(){
        return $this->hasOne(ObjectInsurerFlats::class, 'object_insurer_id', 'object_insurer_id');
    }


    public function scans() {
        return $this->belongsToMany(File::class, 'contracts_scans', 'contract_id', 'file_id');
    }

    public function masks() {
        return $this->belongsToMany(File::class, 'contracts_masks', 'contract_id', 'file_id');
    }

    public function document($document_id) {
        return $this->hasOne(ContractsDocuments::class, 'contract_id', 'id')
            ->where("document_id", $document_id)
            ->get()->first();
    }

    public function temp_payments() {
        return $this->hasMany(Payments::class,'contract_id')
            ->where('statys_id', -1);
    }

    public function order(){
        return $this->hasOne(Orders::class, 'id', 'order_form_id');
    }

    public function order_log_states(){
        return $this->hasMany(ProcessingOrdersLogs::class, 'contract_id');
    }

    public function all_payments() {
        return $this->hasMany(Payments::class,'contract_id');
    }

    public function payments() {
        return $this->hasMany(Payments::class, 'contract_id')
            ->where('statys_id', '>', -1)->where('is_deleted', '=', 0);
    }

    /**
     * Платежи договора, учитывая только взносы (для алгоритмов рассрочки)
     */
    public function paymentsOnlyInstallment(){
        return $this->hasMany(Payments::class, 'contract_id')
            ->where('statys_id', '>', -1)
            ->where('is_deleted', '=', 0)
            ->where('type_id', 0);
    }

    public function financial_policies() {
        return $this->hasOne(FinancialPolicy::class, 'id', 'financial_policy_id');
    }

    public function payment_accepts(){
        return $this->hasMany(PaymentAccept::class, 'contract_id');
    }

    public function payment_accepts_last(){
        return $this->hasOne(PaymentAccept::class, 'contract_id')->get()->last();
    }

    public function get_payment_first() {
        return $this->all_payments()->first();
    }

    /**
     * Получаем все неоплаченные ожидаемые платежи
     */
    public function get_expected_payments()
    {
        return $this->hasMany(Payments::class,'contract_id')
            ->where('statys_id', -2)
            ->where('is_deleted', 0);
    }

    public function get_payments_type($type_id)
    {
        return $this->payments()->where('type_id', '=', $type_id);
    }

    public function terms(){
        return $this->hasOne(ContractTerms::class, 'contract_id');
    }

    public function parent(){

        $parent = null;
        if((int)$this->sales_condition != 0){
            if(isset($this->manager)){
                if($this->manager->perent){
                    $parent = $this->manager->perent;
                }
            }
        }else{
            if($this->agent){
                $parent = $this->agent->perent;
            }

        }



        return $parent;
    }

    public function financial_policy()
    {
        return $this->hasOne(FinancialPolicy::class, 'id', 'financial_policy_id');
    }

    /*
     * END RELATIONS
     */

    public static function getContractsQuery() {

        $contracts = Contracts::query();
        $contracts->where('contracts.statys_id', '!=', -1);


        //Проверка на доступы
        $user = auth()->user();
        $visibility_obj = $user->role->rolesVisibility(6)->visibility;

        if($visibility_obj == 0){ //Все

        }elseif ($visibility_obj == 1){//Все в рамках организации


        }elseif ($visibility_obj == 2){//Только свои

            $contracts->where(function ($query) use ($user) {
                return $query->where('contracts.agent_id', $user->id)
                    ->orWhere('contracts.manager_id', $user->id);
            });


        }elseif ($visibility_obj == 3){//Только свои и своих подчененных

            $contracts->where(function ($query) use ($user) {
                return $query->whereIn('contracts.agent_id', $user->getAllSubUsersIds())
                    ->orWhereIn('contracts.manager_id', $user->getAllSubUsersIds());
            });

            /*$contracts->where(function ($query) use ($user) {
                return $query->whereIn('contracts.agent_id', function($query)
                {

                    $query->select(\DB::raw('users.id'))
                        ->from('users')
                        ->where('parent_id', '=', auth()->id())
                        ->orWhere('curator_id', '=', auth()->id())
                        ->orWhere('id', '=', auth()->id())
                        ->orWhere('path_parent', 'like', "%:".auth()->id().":%");

                })
                    ->orWhereIn('contracts.manager_id', function($query)
                    {

                        $query->select(\DB::raw('users.id'))
                            ->from('users')
                            ->where('parent_id', '=', auth()->id())
                            ->orWhere('curator_id', '=', auth()->id())
                            ->orWhere('id', '=', auth()->id())
                            ->orWhere('path_parent', 'like', "%:".auth()->id().":%");

                    });
            });*/

        }

        return $contracts;

        //$contracts



        //return Visible::apply($contracts, 'contracts', ['contracts.agent_id', 'contracts.manager_id']);
    }

    public static function getContracts($statys) {
        $contracts = Contracts::getContractsQuery();
        $contracts->whereIn('statys_id', $statys);

        return $contracts->get();
    }

    /**
     * @return Contracts $contract
     */
    public static function getContractId($id) {

        $contract = Contracts::getContractsQuery()->with('bso');
        $contract->where('id', $id);
        return $contract->get()->first();
    }

    public static function createContracts($data) {

        $contract = Contracts::where('bso_id', $data->bso_id)->get()->first();

        if (!$contract) {
            $agent = User::find($data->agent_id);

            $contract = Contracts::create([
                'user_id' => auth()->id(),
                'bso_id' => $data->bso_id,
                'bso_title' => $data->bso_title,
                'insurance_companies_id' => $data->insurance_companies_id,
                'bso_supplier_id' => $data->bso_supplier_id,
                'product_id' => $data->product_id,
                'agent_id' => $data->agent_id,
                'parent_agent_id' => $agent->parent_id,
                'is_online' => isset($data->is_online) ? $data->is_online : "",
            ]);




            EPayment::create_temp($contract);
        }

        if ($contract->statys_id != 4) {
            $contract->statys_id = 0;
            $contract->agent_id = $data->agent_id;
            $contract->save();
        }



        return $contract;
    }

    public function saveDataContracts($data) {
        $contract = $this;
        $contract::update([
            'order_title' => $data->order_title,
            'order_id' => $data->order_id,
            'sales_condition' => $data->sales_condition,
            'sign_date' => setDateTimeFormat($data->sign_date),
            'begin_date' => setDateTimeFormat($data->begin_date),
            'end_date' => setDateTimeFormat($data->end_date),
            'payment_total' => getFloatFormat($data->payment_total),
            'installment_algorithms_id' => $data->installment_algorithms_id ?? 0,
            'type_id' => isset($data->type_id) ? $data->type_id : 1,
            'insurance_amount' => isset($data->insurance_amount) ? $data->insurance_amount : 0,
        ]);

        $parent_agent_id = $contract->agent->parent_id;

        if (!$data->sales_condition == 0) {
            $contract::update([
                'manager_id' => $data->manager_id,
            ]);
            $parent_agent_id = isset($contract->manager->parent_id) ? $contract->manager->parent_id : null;
        }


        $contract::update([
            'parent_agent_id' => $parent_agent_id,
        ]);

        if (isset($data->financial_policy_id)) {
            $contract::update([
                'financial_policy_manually_set' => (isset($data->financial_policy_manually_set)) ? (int) $data->financial_policy_manually_set : 0,
                'financial_policy_id' => (int) $data->financial_policy_id,
                'payment_next_date' => isset($data->payment_next_date) ? $data->payment_next_date : '',
                'payment_next_sum' => isset($data->payment_next_sum) ? getFloatFormat($data->payment_next_sum) : '',
            ]);

            if (isset($data->financial_policy_manually_set) && (int) $data->financial_policy_manually_set == 1) {
                $contract::update([
                    'financial_policy_kv_bordereau' => getFloatFormat($data->financial_policy_kv_bordereau),
                    'financial_policy_kv_dvoy' => getFloatFormat($data->financial_policy_kv_dvoy),
                    'financial_policy_kv_agent' => getFloatFormat($data->financial_policy_kv_agent),
                    'financial_policy_kv_parent' => getFloatFormat($data->financial_policy_kv_parent),
                ]);
            }
        }

        if (isset($data->insurer)) {
            if ((int) $contract->insurer_id > 0) {
                $contract->insurer->saveOrCreateSubject((object) $data->insurer);
            } else {
                $contract->insurer_id = Subjects::saveOrCreateSubject((object) $data->insurer);
                $contract->save();
            }
        }


        if (isset($data->object_insurer)) {
            if ((int) $contract->object_insurer_id > 0) {
                $contract->object_insurer->saveOrCreateObject((object) $data->object_insurer);
            } else {
                $contract->object_insurer_id = ObjectInsurer::saveOrCreateObject((object) $data->object_insurer);
                $contract->save();
            }
        }

        if (isset($data->payment)) {

            foreach ($data->payment as $pay) {

                ContractsTempPayments::saveDataPayment((object) $pay, $contract);
            }
        }

        if (isset($data->error_accept)) {
            $contract->error_accept = $data->error_accept;
            $contract->save();
        }


        return true;
    }



    public static function getTempContractsCountArr() {

        $result = [];

        $tabs_visibility = TabsVisibility::get_temp_contracts_tab_visibility();


        if(isset($tabs_visibility[0]['view']) && $tabs_visibility[0]['view'] == 1){
            $result[0] = ['title' => 'Временные', 'count' => Contracts::getContractsQuery()->whereIn('statys_id', [0])->where('kind_acceptance', '!=', 1)->count()];
        }

        if(isset($tabs_visibility[5]['view']) && $tabs_visibility[5]['view'] == 1) {
            $result[5] = ['title' => 'Сдан в кассу', 'count' => Contracts::getContractsQuery()->whereIn('statys_id', [5])->where('kind_acceptance', '!=', 1)->count()];
        }

        if(isset($tabs_visibility[1]['view']) && $tabs_visibility[1]['view'] == 1) {
            $result[1] = ['title' => 'В проверке', 'count' => Contracts::getContractsQuery()->whereIn('statys_id', [1])->where('kind_acceptance', '!=', 1)->count()];
        }

        if(isset($tabs_visibility[2]['view']) && $tabs_visibility[2]['view'] == 1) {
            $result[2] = ['title' => 'Коррекция', 'count' => Contracts::getContractsQuery()->whereIn('statys_id', [2])->where('kind_acceptance', '!=', 1)->count()];
        }

        if(isset($tabs_visibility[3]['view']) && $tabs_visibility[3]['view'] == 1) {
            $result[3] = [
                'title' => 'На оплате',
                'count' => Contracts::getContractsQuery()
                    ->where('kind_acceptance', '!=', 1)
                    ->whereIn('id', function ($q) {
                    $q->select('contract_id')->from('payments')->where('statys_id', 0);
                })->count()
            ];
        }

        if(isset($tabs_visibility[6]['view']) && $tabs_visibility[6]['view'] == 1) {
            $result[6] = ['title' => 'Ожидает подтверждения', 'count' => Contracts::getContractsQuery()->whereIn('statys_id', [6])->count()];
        }

        if(isset($tabs_visibility[7]['view']) && $tabs_visibility[7]['view'] == 1) {
            $result[7] = ['title' => 'Закрытие коррекции', 'count' => Contracts::getContractsQuery()->whereIn('statys_id', [7])->where('kind_acceptance', '!=', 1)->count()];
        }


        return $result;
    }


    public static function getVerificationCountArr() {

        $result = [];

        $tabs_visibility = TabsVisibility::get_verification_tab_visibility();

        if(isset($tabs_visibility[0]['view']) && $tabs_visibility[0]['view'] == 1){
            $result[0] = ['title' => 'Временные', 'count' => Contracts::getContractsQuery()->whereIn('statys_id', [0])->where('kind_acceptance', '!=', 1)->count()];
        }

        if(isset($tabs_visibility[5]['view']) && $tabs_visibility[5]['view'] == 1) {
            $result[5] = ['title' => 'Сдан в кассу', 'count' => Contracts::getContractsQuery()->whereIn('statys_id', [5])->where('kind_acceptance', '!=', 1)->count()];
        }

        if(isset($tabs_visibility[1]['view']) && $tabs_visibility[1]['view'] == 1) {
            $result[1] = ['title' => 'В проверке', 'count' => Contracts::getContractsQuery()->whereIn('statys_id', [1])->where('kind_acceptance', '!=', 1)->count()];
        }

        if(isset($tabs_visibility[2]['view']) && $tabs_visibility[2]['view'] == 1) {
            $result[2] = ['title' => 'Коррекция', 'count' => Contracts::getContractsQuery()->whereIn('statys_id', [2])->where('kind_acceptance', '!=', 1)->count()];
        }

        if(isset($tabs_visibility[3]['view']) && $tabs_visibility[3]['view'] == 1) {
            $result[3] = [
                'title' => 'На оплате',
                'count' => Contracts::getContractsQuery()->whereIn('id', function ($q) {
                    $q->select('contract_id')->from('payments')->where('statys_id', 0);
                })->count()
            ];
        }

        if(isset($tabs_visibility[6]['view']) && $tabs_visibility[6]['view'] == 1) {
            $result[6] = ['title' => 'Ожидает подтверждения', 'count' => Contracts::getContractsQuery()->whereIn('statys_id', [6])->where('kind_acceptance', '!=', 1)->count()];
        }

        if(isset($tabs_visibility[7]['view']) && $tabs_visibility[7]['view'] == 1) {
            $result[7] = ['title' => 'Закрытие коррекции', 'count' => Contracts::getContractsQuery()->whereIn('statys_id', [7])->where('kind_acceptance', '!=', 1)->count()];
        }

        return $result;
    }

    public function acceptContract($data, $kind_acceptance = 3) {
        $contract = $this;

        $res = new \stdClass();
        $res->state = false;
        $res->msg = '';


        if ($contract->statys_id != 4 || $contract->kind_acceptance != 1) {

            $payments = $data['payment'];

            foreach ($payments as $payment) {
                $pay = Payments::acceptPaymentContract((object) $data, (object) $payment, $contract);
                if ($pay->state == false) {
                    if ($contract->kind_acceptance != 0) /// ??
                        return $pay;                     /// ??
                }else {
                    $res = $pay;
                }

                PaymentAccept::create([
                    'contract_id' => isset($contract->id) ? $contract->id : 0,
                    'payment_id' => isset($payment->id) ? $payment->id : 0,
                    'accept_user_id' => auth()->id(),
                    'parent_user_id' => auth()->user()->parent_id,
                    'accept_date' => date('Y-m-d H:i:s'),
                    'kind_acceptance' => $kind_acceptance,
                ]);
            }


            //Меняем статус БСО
            $bso = $contract->bso;
            $bso->state_id = 2;
            if (isset($data['withdraw_documents']) && (int) $data['withdraw_documents'] == 1) {
                $bso->location_id = 4;
                $bso->user_id = auth()->id();
            }
            $bso->contract_id = $contract->id;
            $bso->save();

            //Логируем акцепт
            BsoLogs::setLogs($contract->bso->id, $bso->state_id, $bso->location_id);
            $contract->kind_acceptance = $kind_acceptance;

            if ($kind_acceptance == 0){
                $contract->statys_id = 2;
            }elseif($kind_acceptance == 1) {
                $contract->statys_id = 3; //// ?? 4
                $contract->bso->setBsoLog(13);
            }

            $contract->save();
        } else {
            $res->msg = 'Данный договор уже акцептован!';
        }

        return $res;
    }


    public function courier() {
        return $this->agent ? $this->agent->name : "";
    }




    public function getInsuranceAmount(){

        $insurance_amount = 0;

        if($this->product && $this->product->insurance_amount_default > 0){
            $insurance_amount = $this->product->insurance_amount_default;
        }

        if($this->insurance_amount > 0){
            $insurance_amount = $this->insurance_amount;
        }

        return $insurance_amount;
    }


    public function getViewStateOnline(){

        if($this->selected_calculation && $this->selected_calculation->statys_id >= 2){
            return 'view';
        }
        return 'edit';

    }


    public function saveOnlineContract($data)
    {
        $class_name = Str::ucfirst($this->product->slug);

        if($contract_class = "App\\Domain\\Online\\Contracts\\Products\\{$class_name}"){
            $controlClass = new $contract_class($data);
            return $controlClass->save($this, $data);
        }

        return ['state'=>'false', 'msg'=>'Класс продукта не найден!'];
    }



    public function getElectronBSO($bso_serie, $bso_number)
    {
        //$bso_serie - ФОРМАТ "XXX"
        //$bso_number - ФОРМАТ "000000000"


        $agent = $this->agent;
        $bso_supplier = $this->bso_supplier;


        $bso_serie = BsoSerie::where('bso_serie', $bso_serie)
                    ->where('insurance_companies_id', $this->insurance_companies_id)
                    ->where('bso_class_id', 1)->get()->first();

        $bso = null;

        if($bso_serie){
            $bso_type = $bso_serie->type_bso;
            $bso = BsoItem::getElectronicBso($agent, $bso_supplier, $bso_type, $bso_serie, $bso_number);
        }

        return $bso;
    }


    public function getProductOrProgram()
    {
        return ($this->program)?$this->program:$this->product;
    }

    public function getAgentSettings(ContractsCalculation $calc){
        return ($calc->bso_supplier->agent) ? $calc->bso_supplier->agent : null;
    }

    public function getInsurer()
    {
        return ($this->insurer)?$this->insurer->title:'';
    }

    public function getTokenAct()
    {

        $id = $this->id;
        $token = md5((getDateRu()."{$id}pdf"));
        $inspection = $this->inspection;
        $inspection->token_act = $token;
        $inspection->save();
        return $token;
    }

    public function sendDataToFront()
    {
        if($this->order_id > 0){
            $front = new IntegrationFront();
            return $front->set_order_data($this);
        }
        return false;
    }

    public function destroyDataToFront()
    {
        if($this->order_id > 0){
            $front = new IntegrationFront();
            return $front->destroy_order_data($this);
        }
        return false;
    }

    public function saveDataBeforeRemoving($removed_bso_id)
    {
        RemovedContracts::create([
            'contract_id' =>  $this->id,
            'removed_bso_item_id' =>  (int)$removed_bso_id,
            'old_data' =>  \GuzzleHttp\json_encode($this->getAttributes())
        ]);
    }

    public function isOverdueConditionalAcceptance()
    {
        if ($this->delay_date) {
            $days_from_accept = date_diff(now(), date_create($this->delay_date))->days;

            return ($this->kind_acceptance == 0 && $days_from_accept > 5 ) ? true : false;
        }

        return false;
    }

    public function daysFromDelay()
    {
        if ($this->delay_date) {
            return $days_from_accept = date_diff(now(), date_create($this->delay_date))->days;
        }
        return 0;
    }
}
