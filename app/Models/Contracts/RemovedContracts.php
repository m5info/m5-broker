<?php

namespace App\Models\Contracts;

use Illuminate\Database\Eloquent\Model;

class RemovedContracts extends Model
{
    protected $table = 'removed_contracts';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function contract(){
        return $this->hasOne(Contracts::class, 'id', 'contract_id');
    }

    /**
     * Восстановление всех параметров договора исходя из сохраненных данных
     */
    public function restoreContract()
    {
        $removedContract = $this;
        $contract = $removedContract->contract;

        if($contractOldData = $removedContract->old_data){
            $contractOldData = \GuzzleHttp\json_decode($contractOldData);

            foreach($contractOldData as $column => $value){
                $contract->{$column} = $value;
            }

            $contract->save();
        }

        $contract->sendDataToFront();
    }
}
