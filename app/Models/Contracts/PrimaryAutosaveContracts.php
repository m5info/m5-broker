<?php

namespace App\Models\Contracts;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class PrimaryAutosaveContracts extends Model
{
    public $table = 'primary_autosave';

    protected $guarded = ['id'];

    public $timestamps = true;

    const TYPES = [
        1 => 'Первичка',
        2 => 'Быстрый акцепт',
        3 => 'Первичка быстрая',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id' ,'user_id');
    }

    /**
     * @param $type_id
     */
    public static function unsetUserData($type_id)
    {
        PrimaryAutosaveContracts::query()
            ->where('user_id', '=', auth()->id())
            ->where('type_id', '=', $type_id)
            ->delete();
    }
}
