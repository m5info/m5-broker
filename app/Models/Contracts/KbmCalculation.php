<?php

namespace App\Models\Contracts;

use App\Classes\Export\TagModels\Contracts\TagContracts;
use App\Domain\Samplers\Contracts\TabsVisibility;
use App\Helpers\Visible;
use App\Models\Actions\PaymentAccept;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\InsuranceCompanies;
use App\Models\File;
use App\Models\User;
use App\Traits\Models\ActiveConstTrait;
use App\Traits\Models\CustomRelationTrait;
use App\Traits\Models\GetRelatedTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Directories\Products;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class Contracts
 * @property ContractsCalculation $selected_calculation
 * @property ContractMessage $errors
 * @property ContractMessage $messages
 * @property Collection $drivers
 * @property BsoSuppliers $bso_supplier
 */
class KbmCalculation extends Model {

    protected $table = 'kbm_calculations';

    protected $guarded = ['id'];

}
