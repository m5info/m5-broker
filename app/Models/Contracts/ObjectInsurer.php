<?php

namespace App\Models\Contracts;

use App\Models\BSO\BsoItem;
use App\Models\Directories\InsuranceCompanies;
use App\Models\File;
use App\Models\Security\Security;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Settings\Organization
 *
 * @mixin \Eloquent
 */
class ObjectInsurer extends Model
{
    protected $table = 'object_insurer';

    protected $guarded = ['id'];

    public $timestamps = false;


    public function auto(){
        return $this->hasOne(ObjectInsurerAuto::class, 'object_insurer_id', 'id');
    }

    public function object_insurer_auto_equipment()
    {
        return $this->hasMany(ObjectInsurerAutoEquipment::class, 'object_insurer_id', 'id');
    }


    public function data(){

        $data = null;


        if($this->type == 1){
            if($this->auto){
                $data = $this->auto;
            }else{
                $data = new ObjectInsurerAuto();
                $data->ts_category = 2;
                $data->mark_id = 0;
                $data->model_id = 0;
            }
        }




        return $data;
    }

    public static function saveOrCreateObject($data)
    {
        $object = ObjectInsurer::query();

        if(isset($data->id) && (int)$data->id>0){
            $object = ObjectInsurer::find($data->id);
        }else{
            $object = ObjectInsurer::create(['title'=> $data->title]);
        }

        $object->saveDataObject($data);

        return $object->id;
    }



    public function saveDataObject($data)
    {
        $object = $this;

        $object::update([
            'type' => $data->type,
            'title' => $data->title,
        ]);

        if((int)$data->type == 1)
        {
            ObjectInsurerAuto::where('object_insurer_id', $object->id)->delete();


            ObjectInsurerAuto::create([
                'object_insurer_id' => $object->id,
                'ts_category' => isset($data->ts_category) ? $data->ts_category : "",
                'power' => isset($data->power) ? getFloatFormat($data->power) : "",
                'reg_number' => isset($data->reg_number) ? $data->reg_number : "",
                'mark_id' => isset($data->mark_id) ? $data->mark_id : "",
                'model_id' => isset($data->model_id) ? $data->model_id : "",
                'vin' => isset($data->vin) ? $data->vin : "",
            ]);

        }


        return $object;
    }


    public static function createDefaultProduct($product)
    {
        $object_insurer = new ObjectInsurer();
        $object_insurer->type = 1;
        $object_insurer->save();
        if ($product->category_id == 1)
        {
            $auto = new ObjectInsurerAuto();
            $auto->object_insurer_id = $object_insurer->id;
            $auto->save();
        }

        return $object_insurer;
    }

}
