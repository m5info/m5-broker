<?php

namespace App\Models\Contracts;

use Illuminate\Database\Eloquent\Model;


class ObjectInsurerAutoEquipment extends Model
{

    protected $table = 'object_insurer_auto_equipment';

    protected $guarded = ['id'];

    public $timestamps = false;

}
