<?php


namespace App\Models\Contracts;


use Illuminate\Database\Eloquent\Model;

class DriverPreviousLicense extends Model{

    protected $table = 'previous_drivers_licences';

    protected $guarded = ['id'];

    public function driver(){
        return $this->hasOne(Driver::class, 'id', 'driver_id');
    }
}