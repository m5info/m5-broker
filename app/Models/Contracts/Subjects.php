<?php

namespace App\Models\Contracts;

use App\Models\BSO\BsoItem;
use App\Models\Directories\InsuranceCompanies;
use App\Models\File;
use App\Models\Security\Security;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Settings\Organization
 *
 * @property integer $id
 * @property string $title
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereTitle($value)
 * @mixin \Eloquent
 * @property integer $next_act
 * @property string $default_purpose_payment
 * @property string $inn
 * @property float $limit_year
 * @property float $spent_limit_year
 * @property integer $is_actual
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereNextAct($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereDefaultPurposePayment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereInn($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereLimitYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereSpentLimitYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settings\Organization whereIsActual($value)
 */
class Subjects extends Model
{
    protected $table = 'subjects';

    protected $guarded = ['id'];

    const DOC_TYPE = [
        0 => 'Паспорт гражданина РФ',
        1 => 'Иностранный паспорт',
        2 => 'Вид на жительство',
        99 => 'Иное',
    ];

    /*
    const DOC_TYPE = [
        0 => 'Паспорт гражданина РФ',
        1 => 'Водительское удостоверение РФ',
        2 => 'Водительское удостоверение ГТН',
        3 => 'Водительское удостоверение иностранного государства',
        4 => 'Военный билет солдата (матроса, сержанта, старшины) РФ',
        5 => 'Временное удостоверение личности гражданина РФ',
        6 => 'Удостоверение личности офицера РФ',
        7 => 'Иностранный паспорт',
        8 => 'Загранпаспорт гражданина РФ',
        9 =>  'Свидетельство о государственной регистрации (запись в ЕГРЮЛ)'
    ];*/



    public function data_fl() {
        return $this->hasOne(SubjectsFl::class, 'subject_id', 'id');
    }

    public function data_ul() {
        return $this->hasOne(SubjectsUl::class, 'subject_id', 'id');
    }

    public function get_info()
    {
        $subject = $this;
        $info = null;

        if($subject->type == 0){
            if($subject->data_fl) $info = $subject->data_fl;
            else $info = new SubjectsFl();
        }

        if($subject->type == 1){
            if($subject->data_ul) $info = $subject->data_ul;
            else $info = new SubjectsUl();
        }

        $info->subject_id = $this->id;

        return $info;

    }


    public static function saveOrCreateSubject($data)
    {
        //$find_subject = Subjects::query();
        //Поиск страхователя НАДО ПРОДУМАТЬ
        /*
        if(isset($data->id) && (int)$data->id>0){

            $find_subject->where('id', $data->id);

        }else{

            $find_subject->where('type', $data->type);

            if((int)$data->type == 0)
            {
                $find_subject->where('doc_serie', $data->doc_serie);
                $find_subject->where('doc_number', $data->doc_number);
            }else{
                $find_subject->where('inn', $data->inn);
                $find_subject->where('kpp', $data->kpp);
            }
        }
        $subject = $find_subject->get()->first();
        */

        $subject = null;

        if(!$subject)
        {
            $subject = Subjects::create(['type' => $data->type, 'phone' => $data->phone, 'email' => $data->email]);
        }

        $subject->saveDataSubject($data);

        return $subject->id;
    }

    public function saveDataSubject($data)
    {
        $subject = $this;

        $subject::update([
            'type' => $data->type,
            'email' => $data->email,
            'phone' => $data->phone,
        ]);

        if((int)$data->type == 0)
        {
            $subject::update([
                'title' => $data->fio,
                'doc_serie' => $data->doc_serie,
                'doc_number' => $data->doc_number,
            ]);

        }else{
            $subject::update([
                'title' => $data->title,
                'inn' => $data->inn,
                'kpp' => $data->kpp,
            ]);
        }




        return $subject;
    }







    public static function saveOrCreateOnlineSubject($data, $subject_id)
    {

        //Поиск страхователя НАДО ПРОДУМАТЬ
        if(!$subject = Subjects::where('id', $subject_id)->get()->first())
        {
            $subject = Subjects::create(['type' => $data->type, 'phone' => $data->phone, 'email' => $data->email]);
        }

        return $subject->saveDataSubject($data);
    }

}
