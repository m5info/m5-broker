<?php

namespace App\Models\Contracts;

use App\Classes\Export\TagModels\Contracts\TagPayments;
use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentMethodPay;
use App\Domain\Processes\Operations\Payments\PaymentReceipt;
use App\Domain\Processes\Scenaries\Contracts\Salaries\SalariesController;
use App\Models\Actions\PaymentAccept;
use App\Models\Acts\ReportAct;
use App\Models\Api\Atol\AtolCheck;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\Directories\FinancialPolicy;
use App\Models\Finance\Invoice;
use App\Models\Finance\PayMethod;
use App\Models\Organizations\Organization;
use App\Models\Reports\ReportOrderBasePayment;
use App\Models\Reports\ReportOrders;
use App\Models\User;
use App\Services\Front\IntegrationFront;
use App\Traits\Models\ActiveConstTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;


class Payments extends Model
{
    use ActiveConstTrait;

    const TAG_MODEL = TagPayments::class;

    protected $table = 'payments';

    protected $guarded = ['id'];

    public $timestamps = false;

    protected $attributes = [
        'reports_order_id' => 0,
        'reports_dvou_id' => 0,
    ];

    const STATUS = [
        -2 => 'Ожидаемый',
        -1 => 'Временный',
        0 => 'Не оплачен',
        1 => 'Оплачен',
        2 => 'Развязан'
    ];

    /**
     * Вид оплаты
     */
    const PAYMENT_TYPE = [
        0 => 'Наличные',
        1 => 'Безналичные',
        2 => 'Карта',
        3 => 'Карта БРОКЕР',
    ];

    /**
     * Поток оплаты
     */
    const PAYMENT_FLOW = [
        0 => 'Брокер',
        1 => 'СК',
    ];


    const TRANSACTION_TYPE = [
        0 => 'Взнос',
        1 => 'Долг',
        2 => 'Выплата',
        3 => 'Доплата',
    ];

    const OVERDUE = [
        1 => ['key' => 1, 'title' => 'претензий нет', 'color' => '#FFF'],
        2 => ['key' => 2, 'title' => 'более 3-х дней', 'color' => '#DDF'],
        3 => ['key' => 3, 'title' => 'более 15-ти дней', 'color' => '#FDD'],
    ];

    const SEND_FRONT = [
        0 => 'Не передан',
        1 => 'Создана заявка',
        2 => 'Выполнена и передан платеж',
    ];

    const TYPE_RU = ['sk' => 'СК', 'cash' => 'Наличные', 'cashless' => 'Безналичные', 'cashless_sk' => 'Безналичные СК'];

    public function atol_check()
    {
        return $this->hasOne(AtolCheck::class, 'id', 'atol_check_id');
    }

    public function contract()
    {
        return $this->hasOne(Contracts::class, 'id', 'contract_id')->with('insurer','owner');
    }

    public function bso()
    {
        return $this->hasOne(BsoItem::class, 'id', 'bso_id')->with('product','user','type','supplier','insurance','supplier_org');
    }

    public function bso_for_reports_list()
    {
        return $this->hasOne(BsoItem::class, 'id', 'bso_id')->with('product');
    }

    public function agent()
    {
        return $this->hasOne(User::class, 'id', 'agent_id');
    }

    public function manager()
    {
        return $this->hasOne(User::class, 'id', 'manager_id');
    }

    public function check_user()
    {
        return $this->hasOne(User::class, 'id', 'check_user_id');
    }

    public function payment_accepts()
    {
        return $this->hasMany(PaymentAccept::class, 'payment_id', 'id');
    }

    public function referencer()
    {
        return $this->hasOne(User::class, 'id', 'referencer_id');
    }

    public function cashbox_user()
    {
        return $this->hasOne(User::class, 'id', 'cashbox_user_id');
    }

    public function parent_agent()
    {
        return $this->hasOne(User::class, 'id', 'parent_agent_id');
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class, 'id', 'invoice_id');
    }

    public function org()
    {
        return $this->hasOne(Organization::class, 'id', 'org_id');
    }

    public function reports_border()
    {
        return $this->hasOne(ReportOrders::class, 'id', 'reports_order_id');
    }

    public function base_payment()
    {
        return $this->hasOne(ReportOrderBasePayment::class, 'payment_id', 'id');
    }

    public function reports_dvoy()
    {
        return $this->hasOne(ReportOrders::class, 'id', 'reports_dvou_id');
    }

    public function act_sk()
    {
        return $this->hasOne(ReportAct::class, 'id', 'acts_sk_id');
    }

    public function receipt()
    {
        return $this->hasOne(BsoItem::class, 'id', 'bso_receipt_id');
    }

    public function pay_mehtod()
    {
        return $this->hasOne(PayMethod::class, 'id', 'pay_method_id');
    }

    public function financial_policy()
    {
        return $this->hasOne(FinancialPolicy::class, 'id', 'financial_policy_id');
    }




    public static function getPaymentsQuery(){
        $payments = self::query();

        //Проверка на доступы
        $user = auth()->user();
        //$payments->where("payments.agent_id", $user->id)->orWhere('payments.manager_id', '=', $user->id)->orWhere('payments.parent_agent_id', '=', $user->id);

        return $payments;

    }

    public static function getPayments()
    {
        $payment = self::query()->with('bso','org','contract','agent','manager');

        return $payment;
    }

    public static function getPaymentId($id)
    {
        return self::getPayments()->where('id', $id)->get()->first();
    }


    public static function acceptPaymentContract($data, $data_pay, $contract)
    {

        $res = new \stdClass();
        $res->state = false;
        $res->msg = '';

        $payment = Payments::where("bso_id", $data->bso_id)->where("payment_number", $data_pay->payment_number)->first();

        if (!$payment) {
            $payment = Payments::create([
                'statys_id' => 0,
                'bso_id' => $data->bso_id,
                'payment_number' => $data_pay->payment_number,
            ]);

            $payment->contract_id = $contract->id;
            $payment->agent_id = $contract->agent_id;
            $payment->manager_id = $contract->manager_id;
            $payment->parent_agent_id = $contract->parent_agent_id;
            $payment->org_id = $contract->bso->org_id;
            $payment->accept_user_id = auth()->id();
            $payment->accept_date = date("Y-m-d H:i:s");

            $payment->order_id = $contract->order_id;
            $payment->order_title = $contract->order_title;
            $payment->order_sort_id = $contract->order_sort_id ? $contract->order_sort_id : "";
            $payment->save();
        }

        if ($contract->kind_acceptance == 1) {

            if ($payment->statys_id == 1) {
                $res->msg = 'Данный платеж уже оплачен!';
                return $res;
            }

            if ((int)$payment->cashbox_id > 0) {
                $res->msg = "Данный платеж находится в счете № {$payment->cashbox_id}!";
                return $res;
            }
        }


        $data_pay->payment_data = $data_pay->payment_data;
        $data_pay->payment_total = $data_pay->payment_total;
        $data_pay->point_sale_id = $contract->bso->point_sale_id;

        $data_pay->financial_policy_manually_set = 0;
        $data_pay->financial_policy_id = null;
        $data_pay->financial_policy_kv_bordereau = 0;
        $data_pay->financial_policy_kv_dvoy = 0;
        $data_pay->financial_policy_kv_agent = 0;
        $data_pay->financial_policy_kv_parent = 0;


        if (isset($data->financial_policy_manually_set) && $data->financial_policy_manually_set == 1) {

            $data_pay->financial_policy_manually_set = isset($data->financial_policy_manually_set) ?: 0;
            $data_pay->financial_policy_kv_bordereau = $data->financial_policy_kv_bordereau;
            $data_pay->financial_policy_kv_dvoy = $data->financial_policy_kv_dvoy;
            $data_pay->financial_policy_kv_agent = $data->financial_policy_kv_agent;
            $data_pay->financial_policy_kv_parent = $data->financial_policy_kv_parent;

        } elseif (isset($data->financial_policy_id) && $data->financial_policy_id > 0) {

            $data_pay->financial_policy_id = $data->financial_policy_id;
        }


        $res = $payment->savePaymentContract($data_pay, $data);
        if ($res->state == true) $payment->automaticChargeKV();

        return $res;

    }


    public function savePaymentContract($data, $data_contract = null)
    {
        $payment = $this;

        $res = new \stdClass();
        $res->state = false;
        $res->msg = '';


        $data->official_discount = isset($data->official_discount) ? $data->official_discount : 0;
        $data->bank_kv = isset($data->bank_kv) ? $data->bank_kv : 0;


        $payment->point_sale_id = $data->point_sale_id;
        $payment->payment_number = (int)$data->payment_number;

        $payment->payment_data = getDateFormatEn($data->payment_data);
        $payment->payment_total = getFloatFormat($data->payment_total);

        $payment->payment_type = (int)$data->payment_type;
        $payment->payment_flow = (int)$data->payment_flow;

        $payment->official_discount = getFloatFormat($data->official_discount);
        $payment->official_discount_total = getTotalSumToPrice($payment->payment_total, $payment->official_discount);

        $payment->informal_discount = getFloatFormat($data->informal_discount);
        $payment->informal_discount_total = getTotalSumToPrice($payment->payment_total, $payment->informal_discount);

        $payment->bank_kv = getFloatFormat($data->bank_kv);
        $payment->bank_kv_total = getTotalSumToPrice($payment->payment_total, $payment->bank_kv);
        $payment->save();

        //ФП ВЫБРАНА В РУЧНУЮ
        if (isset($data->financial_policy_manually_set) && $data->financial_policy_manually_set == 1) {
            //$payment->financial_policy_id = null;
            $payment->financial_policy_manually_set = 1;

            // режем дробь до сотых, иначе считает потом по одному пути, а сохраняется по-другому и из-за этого выставляется долг
            $payment->financial_policy_kv_bordereau = getFloatFormatHundredthFraction($data->financial_policy_kv_bordereau);
            $payment->financial_policy_kv_dvoy = getFloatFormatHundredthFraction($data->financial_policy_kv_dvoy);
            $payment->financial_policy_kv_agent = getFloatFormatHundredthFraction($data->financial_policy_kv_agent);
            $payment->financial_policy_kv_parent = getFloatFormatHundredthFraction($data->financial_policy_kv_parent);


        } elseif ($data->financial_policy_id != null) {

            $payment->financial_policy_id = $data->financial_policy_id;
            $payment->financial_policy_manually_set = 0;

            $fp = FinancialPolicy::findOrFail($data->financial_policy_id);
            $group_kv = $fp->getGroupKV($payment);

            $payment->financial_policy_kv_bordereau = getFloatFormat($fp->kv_bordereau);
            $payment->financial_policy_kv_dvoy = getFloatFormat($fp->kv_dvou);

            //ЕСЛИ ПЛАТЕЖ ОПЛАЧЕН И АГЕНТСКАЯ ПРОДАЖА
            if ($payment->statys_id == 1 && $payment->contract->sales_condition != 1) {


            } else {
                $payment->financial_policy_kv_agent = getFloatFormat($fp->kv_agent);
                $payment->financial_policy_kv_parent = getFloatFormat($fp->kv_parent);

                if ($group_kv) {
                    $payment->financial_policy_kv_agent = getFloatFormat($group_kv->kv_agent);
                    $payment->financial_policy_kv_parent = getFloatFormat($group_kv->kv_parent);
                }

                $payment->financial_policy_kv_agent_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_agent);
                $payment->financial_policy_kv_parent_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_parent);

                $payment->save();

                //$payment = PaymentDiscounts::recount($payment);
            }


        }

        $payment->financial_policy_kv_bordereau_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_bordereau);
        $payment->financial_policy_kv_dvoy_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_dvoy);

        if ($payment->statys_id != 1 || $payment->contract->sales_condition == 1) {
            $payment->financial_policy_kv_agent_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_agent);
            $payment->financial_policy_kv_parent_total = getTotalSumToPrice($payment->payment_total, $payment->financial_policy_kv_parent);

            $payment->save();

            $payment = PaymentDiscounts::recount($payment);
        }

        //устанавливаем метод оплаты
        if (isset($data->pay_method_id)) {
            $payment = PaymentMethodPay::set($payment, $data->pay_method_id);
        }

        PaymentReceipt::attach($payment, $data);

        $payment->save();

        //$payment = PaymentDiscounts::recount($payment);


        $res->state = true;


        return $res;
    }


    public function automaticChargeKV()
    {
        $payment = $this;
        //Проверка платежа если Е-полис и оплата в СК


        return true;
    }


    public function type()
    {

        /*if (in_array($this->payment_type, [0, 3])) {  // Нал, карта брокер
            if ($this->payment_flow == 1 && $this->payment_type == 3){ // СК
                return 'cash'; // Карта брокер + СК
            }elseif($this->payment_flow == 1 && $this->payment_type !== 3){
                return 'sk';
            }else{ // Брокер
                return 'cash'; //
            }
        } elseif (in_array($this->payment_type, [1, 2])) { // Безнал, Карта
            if ($this->payment_flow == 1){ // СК
                return 'sk';
            }else{ // Брокер
                return 'cashless';
            }
        }*/

        if ($this->payment_flow == 1) { // СК

            if ($this->payment_type == 3){ // Карта брокер

                return 'cashless_sk';

            } else {

                return 'sk';

            }

        } elseif ($this->payment_flow == 0) { // Брокер

            if (in_array($this->payment_type, [0, 3])) { // Нал, карта брокер

                return 'cash';

            } elseif ($this->payment_type == 1) { // Безнал

                return 'cashless';

            } elseif ($this->payment_type == 2) { // Карта

                return 'card';

            }
        }

        return 0;
    }

    public function type_ru()
    {

        $ru = self::TYPE_RU;
        $type = $this->type();
        return isset($ru[$type]) ? $ru[$type] : false;

    }

    public function type_to_invoice()
    {

        $pt = $this->payment_type;
        $pf = $this->payment_flow;

        $type_invoice_payment = [
            1 => $pt == 0 && $pf == 0,
            3 => in_array($pt, [1, 2]) && $pf == 0,
            4 => in_array($pt, [1, 2]) && $pf == 1,
            5 => $pt == 3 && $pf == 1,
        ];

        return array_search(true, $type_invoice_payment);

    }

    public function overdue()
    {

        $payment_date = strtotime($this->payment_data);
        if ($payment_date > time() - 60 * 60 * 24 * 3) {
            return self::OVERDUE[1];
        }
        if ($payment_date < time() - 60 * 60 * 24 * 3 && $payment_date > time() - 60 * 60 * 24 * 15) {
            return self::OVERDUE[2];
        }
        if ($payment_date < time() - 60 * 60 * 24 * 15) {
            return self::OVERDUE[3];
        }
        return false;
    }


    public static function whereType($type = false, Builder $builder = null)
    {

        $payments = $builder ? $builder : self::query();

        if ($type == 'cash') {
            $payments->where('payment_flow', 0);
            $payments->whereIn('payment_type', [0, 3]);
        } elseif ($type == 'cashless') {
            $payments->where('payment_flow', 0);
            $payments->where('payment_type', 1);
        } elseif ($type == 'card') {
            $payments->where('payment_flow', 0);
            $payments->where('payment_type', 2);
        } elseif ($type == 'sk') {
            $payments->where('payment_flow', '=', 1);
        }


        return $payments;
    }

    public static function whereOverdue($overdue = 0, Builder $builder = null)
    {

        $debts = $builder ? $builder : self::query();

        if ($overdue == 1) {
            $debts->whereDate('payment_data', '>', date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 3));
        }

        if ($overdue == 2) {
            $debts->whereDate('payment_data', '<', date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 3));
            $debts->whereDate('payment_data', '>', date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 15));
        }

        if ($overdue == 3) {
            $debts->whereDate('payment_data', '<', date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 15));
        }

        $debts->where('is_deleted', '=', 0);


        return $debts;
    }

    public function getPaymentAgentSum()
    {
        $payment_sum = $this->payment_total;

        if ($this->statys_id == 1) {
            if ($this->payment_flow == 1) {
                return 0;
            }


            return $this->invoice_payment_total;
        }

        if ($this->type_id == 1) {
            return $this->payment_total;
        }

        if ($this->type_id == 2) {
            //return $this->payment_total*-1;
        }


        //оплата в БРОКЕР
        if ($this->payment_flow == 0) {

            //оплата НАЛИЧНЫМИ или КАРТОЙ БРОКЕР
            if ($this->payment_type == 0 || $this->payment_type == 3) {

                //$payment_sum =$payment_sum-$this->official_discount_total; //МИНУСУЕМ ОФИЦАЛЬНУЮ СКИДКУ


                if ($this->contract) {
                    if ($this->contract->sales_condition == 0) {//Агентская
                        $payment_sum = $payment_sum - $this->financial_policy_kv_agent_total;
                    }


                    if ($this->contract->sales_condition == 1) {//Продажа организации
                        //$payment_sum =$payment_sum-$this->financial_policy_kv_agent_total;
                        $payment_sum = $payment_sum - $this->informal_discount_total; //МИНУСУЕМ НЕ ОФИЦАЛЬНУЮ СКИДКУ
                        //$payment_sum =$payment_sum-$this->official_discount_total; //МИНУСУЕМ ОФИЦАЛЬНУЮ СКИДКУ
                    }


                    if ($this->contract->sales_condition == 2) {//Агента через организацию
                        //$payment_sum =$payment_sum-$this->financial_policy_kv_agent_total;
                        $payment_sum = $payment_sum - $this->informal_discount_total; //МИНУСУЕМ НЕ ОФИЦАЛЬНУЮ СКИДКУ
                        //$payment_sum =$payment_sum-$this->official_discount_total; //МИНУСУЕМ ОФИЦАЛЬНУЮ СКИДКУ
                    }
                }


            }

            //оплата БЕЗНАЛИЧНЫМИ
            if ($this->payment_type == 1) {
                //$payment_sum = 0;

                if ($this->contract->sales_condition == 1) {//Продажа организации
                    //$payment_sum =$payment_sum-$this->financial_policy_kv_agent_total;
                    //$payment_sum = $payment_sum - $this->informal_discount_total; //МИНУСУЕМ НЕ ОФИЦАЛЬНУЮ СКИДКУ
                    //$payment_sum =$payment_sum-$this->official_discount_total; //МИНУСУЕМ ОФИЦАЛЬНУЮ СКИДКУ
                }


                if ($this->contract->sales_condition == 2) {//Агента через организацию
                    //$payment_sum =$payment_sum-$this->financial_policy_kv_agent_total;
                    //$payment_sum = $payment_sum - $this->informal_discount_total; //МИНУСУЕМ НЕ ОФИЦАЛЬНУЮ СКИДКУ
                    //$payment_sum =$payment_sum-$this->official_discount_total; //МИНУСУЕМ ОФИЦАЛЬНУЮ СКИДКУ
                }


            }

            //оплата КАРТОЙ
            if ($this->payment_type == 2) {


            }

            //оплата КАРТОЙ БРОКЕР
            if ($this->payment_type == 3) {


            }

        }


        //оплата в СК
        if ($this->payment_flow == 1) {

            //оплата БЕЗНАЛИЧНЫМИ
            if ($this->payment_type == 1) {
                $payment_sum = 0;
            }

            //оплата КАРТОЙ
            if ($this->payment_type == 2) {
                $payment_sum = 0;
            }

            //оплата КАРТОЙ БРОКЕР
            if ($this->payment_type == 3) {

                if ($this->contract) {
                    if ($this->contract->sales_condition == 0) {//Агентская
                        $payment_sum = $payment_sum- $this->financial_policy_kv_agent_total;
                    }


                    if ($this->contract->sales_condition == 1) {//Продажа организации
                        //$payment_sum =$payment_sum-$this->financial_policy_kv_agent_total;
                        $payment_sum = $payment_sum - $this->informal_discount_total; //МИНУСУЕМ НЕ ОФИЦАЛЬНУЮ СКИДКУ
                        //$payment_sum =$payment_sum-$this->official_discount_total; //МИНУСУЕМ ОФИЦАЛЬНУЮ СКИДКУ
                    }


                    if ($this->contract->sales_condition == 2) {//Агента через организацию
                        //$payment_sum =$payment_sum-$this->financial_policy_kv_agent_total;
                        $payment_sum = $payment_sum - $this->informal_discount_total; //МИНУСУЕМ НЕ ОФИЦАЛЬНУЮ СКИДКУ
                        //$payment_sum =$payment_sum-$this->official_discount_total; //МИНУСУЕМ ОФИЦАЛЬНУЮ СКИДКУ
                    }
                }

            }


        }


        return $payment_sum;
    }

    public function getInsurer()
    {
        $insurer = '';

        if ($this->contract) {
            $insurer = ($this->contract->insurer) ? $this->contract->insurer->title : '';
        }

        return $insurer;
    }

    public function getInsurerAttribute()
    {
        $insurer = '';

        if ($this->contract) {
            $insurer = ($this->contract->insurer) ? $this->contract->insurer->title : '';
        }

        return $insurer;
    }


    public function getPaymentsColor()
    {
        $color = "#FFF";

        if ($this->statys_id == 1) {
            $color = "#d9ffcc";
        }

        if ($this->is_deleted == 1) {
            $color = "#ffcccc";
        }

        return $color;
    }

    public function getPaymentsColorTitle()
    {
        $title = "";

        if ($this->statys_id == 1) {
            $title = "Платеж оплачен";
        }

        if ($this->is_deleted == 1) {
            $title = "Платеж удален";
        }

        return $title;
    }


    public function savePaymentData($data)
    {
        $payment = $this;

        $data->payment_total = getFloatFormat($data->payment_total);

        if ($payment->type_id == 0) {//payment

            $need_log = in_array(true, [
                $data->financial_policy_id != $this->financial_policy_id,
                $data->financial_policy_kv_bordereau != $this->financial_policy_kv_bordereau,
                $data->financial_policy_kv_dvoy != $this->financial_policy_kv_dvoy,
                $data->financial_policy_kv_agent != $this->financial_policy_kv_agent,
                $data->financial_policy_kv_parent != $this->financial_policy_kv_parent,
            ]);


            if ($need_log) {
                $this->setBsoLogToPayment(11);
            }

            $payment->agent_id = $data->agent_id;
            $payment->manager_id = $data->manager_id;
            $payment->parent_agent_id = $data->parent_agent_id;

            $payment->order_id = $data->order_id;
            $payment->order_title = $data->order_title;
            $payment->order_sort_id = $data->order_sort_id ? $data->order_sort_id : "";

            $payment->comments = $data->comments;

            if(isset($data->referencer_id) && $data->referencer_id) {
                $payment->referencer_id = $data->referencer_id;
                $ref_kv = $data->referencer_kv != '' ? (float) $data->referencer_kv : User::find($data->referencer_id)->referencer_kv;
                $payment->referencer_kv = $ref_kv;
                } else {
                $payment->referencer_id = '';
                $payment->referencer_kv = '';
            }
            return $payment->savePaymentContract($data)->state;
        }

        if ($payment->statys_id == 0 && $payment->type_id == 1 || $payment->type_id == 2) {
            $payment->agent_id = $data->agent_id;
            $payment->comments = $data->comments;
            $payment->payment_total = getFloatFormat($data->payment_total);
            $payment->payment_data = date("Y-m-d H:i:s");
            $payment->payment_type = 0;
            $payment->payment_flow = 0;

            $agent = User::find($data->agent_id);
            $payment->parent_agent_id = $agent->parent_id;
            $payment->user_id = auth()->id();

            if($payment->type_id == 1){
                $payment->financial_policy_marjing = 100;
                $payment->financial_policy_marjing_total = $data->payment_total;
            }

            if($payment->type_id == 2){
                $payment->financial_policy_marjing = -100;
                $payment->financial_policy_marjing_total = $data->payment_total * -1;
            }

            //&& isset($data->set_balance) && (int)$data->set_balance == 1 ВСЕГДА НА БАЛАНС
            if ($payment->type_id == 2) {//premium
                //Зачисляем на баланс
                $balance = $agent->getBalanceProfit();
                $balance->setTransactions(0, 0, $payment->payment_total, $payment->payment_data, $payment->comments);
                if ($payment->type_id == 2) {
                    $payment->payment_total = $payment->payment_total * -1;
                }

                $payment->invoice_payment_date = $payment->payment_data;
                $payment->invoice_payment_total = $payment->payment_total;
                $payment->invoice_id = null;

                $payment->set_balance = 1;
                $payment->payment_type = 1;
                $payment->statys_id = 1;
            }

            if ($payment->type_id == 1) {
                $payment->invoice_id = null;
            }
        }


        $payment->save();

        return true;

    }


    public function deletePayment()
    {
        $res = '';

        $payment = $this;

        if ($payment->type_id != 0) { // НЕ ВЗНОС
            if ($payment->statys_id == 1) {
                $res = 'Данный платеж уже оплачен!';
            } elseif ($payment->statys_id == 2) {
                $res = 'Данный платеж уже удален!';
            } else {
                $payment->is_deleted = 1;
                $payment->statys_id = 2;
                $payment->save();
                $res = 1;
            }
        }

        if ($payment->type_id == 0) { // ВЗНОС


            if ($payment->statys_id == 0) { // НЕ ОПЛАЧЕН
                if ((int)$payment->invoice_id > 0) {
                    $invoice = Invoice::find($payment->invoice_id);
                    $payment->invoice_id = null;
                    $invoice->refreshInvoice();
                }
            }

            if ($payment->statys_id == 1) { // ОПЛАЧЕН
                //$payment->invoice_payment_total // Сумма зашедшая в кассу
                $agent = User::find($payment->agent_id);

                $balance = $agent->getBalanceCancelPayment();
                $balance->setTransactions(0, 0, $payment->invoice_payment_total, date("Y-m-d H:i:s"), "Развязка договора # {$payment->bso->bso_title}");

                if($payment->contract->sales_condition != 1){
                    if($payment->payment_type == 1 || $payment->payment_type == 2)
                    {
                        $payment_debt = new Payments();
                        $payment_debt->type_id = 1;
                        $payment_debt->bso_id = $payment->bso_id;
                        $payment_debt->contract_id = $payment->contract_id;
                        $payment_debt->org_id = $payment->org_id;

                        $data = new \stdClass();
                        $data->agent_id = $payment->agent_id;
                        $data->comments = "Развязка договора # {$payment->bso->bso_title} долг КВ ".titleFloatFormat($payment->financial_policy_kv_agent_total);
                        $data->payment_total = $payment->financial_policy_kv_agent_total;

                        $payment_debt->savePaymentData($data);
                    }
                }


            }

            $front = new IntegrationFront();
            $front->destroy_order_payment($payment->contract, $payment);

            $payment->is_deleted = 1;
            $payment->statys_id = 2;

            $payment->save();
            $res = 1;
        }

        if($res == 1){
            SalariesController::destroySalaries($payment);
        }

        return $res;

    }


    public function setBsoLogToPayment($location_id = 10, $postpone = 0)
    {

        $payment = $this;
        $payment->bso->setBsoLog($location_id, $postpone);

        if ($payment->bso_receipt_id > 0) {
            BsoLogs::setLogs(
                $payment->bso_receipt_id,
                $payment->bso->state_id,
                $location_id,
                0,
                auth()->id(),
                0,
                0,
                0,
                $postpone
            );
        }


        return true;

    }


    public function detachReceipt()
    {

        if ($this->receipt) {

            $bso_item = $this->receipt;
            $bso_item->setBsoLog(14);
            $bso_item->update([
                'location_id' => 1,
                'state_id' => 0,
                'contract_id' => 0,
            ]);

        }

        $this->update([
            'bso_not_receipt' => 1,
            'bso_receipt_id' => null,
            'bso_receipt' => null,

        ]);

    }


    public function getMargin()
    {
        return
            ($this->financial_policy_kv_bordereau_total + $this->financial_policy_kv_dvoy_total) -
            ($this->financial_policy_kv_agent_total + $this->financial_policy_kv_parent_total);
    }


    public function getInsuranceAmount()
    {

        return $this->contract ? $this->contract->getInsuranceAmount() : 0;
    }


    public function getMarginAmountCommonAnalitycs($return_type = 0)
    {
        $payment = $this;
        $margin = 0;
        $result = 0;

        if ($payment->type_id == 0) {//Взнос
            $margin = $payment->financial_policy_kv_bordereau + $payment->financial_policy_kv_dvoy;
            if ($payment->contract->sales_condition != 1){
                $margin = $margin - ($payment->financial_policy_kv_agent + $payment->financial_policy_kv_parent);
            }

            $margin = $margin - ( ($payment->f_p_partnership_reward_1 !== null ? $payment->f_p_partnership_reward_1 : 0.00 ) +
                    ( $payment->f_p_partnership_reward_2 !== null ? $payment->f_p_partnership_reward_2 : 0.00 ) +
                    ( $payment->f_p_partnership_reward_3 !== null ? $payment->f_p_partnership_reward_3 : 0.00 ) );
        }

        if ($payment->type_id == 1) {//Долг
            $margin = 100;
        }

        if ($payment->type_id == 2) {//Выплата
            $margin = -100;
        }

        if ($return_type == 0) {
            if ($payment->contract->sales_condition == 1){
                $result = $margin - ( getFloatFormat($payment->informal_discount_total_procent) + getFloatFormat($payment->acquire_percent) );
            }else{
                $result = $margin;
            }
        }

        if ($return_type == 1) {
            if ($payment->type_id !== 2) {
                $result = ($payment->payment_total * $margin) / 100;
            } else {
                $result = $payment->payment_total;
            }
        }

        return $result;

    }


    public function getMarginAmount($return_type = 0)
    {
        $payment = $this;
        $margin = 0;
        $result = 0;

        if ($payment->type_id == 0) {//Взнос
            $margin = $payment->financial_policy_kv_bordereau + $payment->financial_policy_kv_dvoy;
            $margin = $margin - ($payment->financial_policy_kv_agent + $payment->financial_policy_kv_parent);
        }

        if ($payment->type_id == 1) {//Долг
            $margin = 100;
        }

        if ($payment->type_id == 2) {//Выплата
            $margin = -100;
        }

        if ($return_type == 0) {
            $result = $margin;
        }

        if ($return_type == 1) {
            if ($payment->type_id !== 2) {
                $result = ($payment->payment_total * $margin) / 100;
            } else {
                $result = $payment->payment_total;
            }
        }

        return $result;

    }


    public function addPaymentDebt($original_payment_total)
    {
        $payment = $this;
        $payment_total = $payment->payment_total;

        $data = new \stdClass();
        $data->type_id = $payment->type_id;
        $data->bso_id = $payment->bso_id;
        $data->contract_id = $payment->contract_id;
        $data->org_id = $payment->org_id;
        $data->agent_id = $payment->agent_id;
        $data->comments = '';
        $data->payment_total = 0;

        if ($payment_total > $original_payment_total) {
            //Выстовляем долг
            $data->type_id = 1;
            $sum_debt = $payment_total - $original_payment_total;


            //ДЕЛАЕМ ПЕРЕСЧЕТ КВ в процентах $payment->financial_policy_kv_agent

            $kv_original_agent = ($original_payment_total * $payment->financial_policy_kv_agent) / 100;
            $kv_agent = ($payment_total * $payment->financial_policy_kv_agent) / 100;
            $kv_debt = $kv_agent - $kv_original_agent;

            if ($kv_debt > 0) {

                $payment->financial_policy_kv_agent = getFloatFormat(100 / ($payment_total / $payment->financial_policy_kv_agent_total));
                $payment->save();
                $sum_debt = $sum_debt - $kv_debt;
            }

            $data->payment_total = $sum_debt;
            $data->comments = 'Долг изменение СП';
        }

        if ($payment_total < $original_payment_total) {
            //Делаем выплату
            $data->type_id = 2;
            $data->payment_total = $original_payment_total - $payment_total;
            $data->comments = 'Выплата изменение СП';
        }

        $payment_debt = new Payments();
        $payment_debt->type_id = $data->type_id;
        $payment_debt->bso_id = $data->bso_id;
        $payment_debt->contract_id = $data->contract_id;
        $payment_debt->org_id = $data->org_id;
        $payment_debt->savePaymentData($data);

        return true;

    }


    public function get_financial_policy()
    {

        $insurance_companies_id = $this->bso->insurance_companies_id;
        $bso_supplier_id = $this->bso->bso_supplier_id;
        $product_id = $this->bso->product_id;

        $row_text = 'title as title';

        if (auth()->user()->is('under')) {
            $row_text = 'CONCAT((kv_bordereau+kv_dvou), "% ", title) as title';
        }

        $fp = FinancialPolicy::where('insurance_companies_id', $insurance_companies_id)
            ->where('bso_supplier_id', $bso_supplier_id)
            ->where('product_id', $product_id)
            ->where('is_actual', 1)
            ->orderBy('title')
            ->select([
                '*', \DB::raw($row_text)
            ])->get(['id', 'title']);


        return $fp;

    }


    public function getBasePayment($report_id){
        return ReportOrderBasePayment::/*where('report_order_id', $report_id)->*/where('payment_id', $this->id)->get()->first();
    }


    public function checkBaseData($base, $data)
    {
        $payment = $this;
        if($base){

            $arr_base = $base->toArray();
            $arr_payment = $payment->toArray();

            if(isset($arr_base[$data]) && isset($arr_payment[$data])){
                if($arr_base[$data] != $arr_payment[$data]){
                    return false;
                }
            }
        }

        return true;
    }


    public static function getDebtPaymentsColor($payment_data)
    {
        $color = "#FFF";
        $day = (int)countDayToDates($payment_data, date("Y-m-d H:i:s"));

        if($day >= 3) $color = "#DDF"; // фиолетовый
        if($day >= 15) $color = "#ffcccc"; // красный


        return $color;
    }


    public function saveDataBeforeRemoving($removed_bso_id)
    {
        RemovedPayments::create([
            'payment_id' =>  $this->id,
            'removed_bso_item_id' =>  (int)$removed_bso_id,
            'old_data' =>  \GuzzleHttp\json_encode($this->getAttributes())
        ]);
    }

}
