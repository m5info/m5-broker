<?php

namespace App\Models\Contracts;


use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @mixin \Eloquent
 */
class Driver extends Model{

    protected $table = 'drivers';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function contract(){
        return $this->hasOne(Contracts::class, 'id', 'contract_id');
    }

    public function old_license(){
        return $this->hasOne(DriverPreviousLicense::class, 'driver_id', 'id');
    }

    public function get_fio(){

        $fio = explode(' ', $this->fio);
        return [
            'last_name' => $fio[0],
            'first_name' => isset($fio[1]) ? $fio[1] : $fio[0],
            'middle_name' => isset($fio[2]) ? $fio[2] : $fio[0],
        ];
    }




}