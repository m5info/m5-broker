<?php

namespace App\Repositories;

use App\Models\Contracts\Contracts;
use App\Models\Actions\PaymentAccept;
use Carbon\Carbon;

class ContractsTechunderRepository
{
    public function getContractsDischargeForTable($request){

        $model = Contracts::getContractsQuery()->whereIn('statys_id', [4])
            ->with('manager','errors','logs','insurance_companies','bso','agent','insurer');

        $accepts = PaymentAccept::where('accept_user_id', '>', '0');
        if (isset($request->underwriter_id) && $request->underwriter_id > 0) {
            if (is_array($request->underwriter_id)) {
                $accepts->where('accept_user_id', 'in', $request->underwriter_id);
            } else {
                $accepts->where('accept_user_id', '=', $request->underwriter_id);
            }
        }
        if (isset($request->product_id) && $request->product_id > 0) {
            $accepts->whereHas('contract', function($query) use ($request) {
                $query->where('product_id', '=', $request->product_id);
            });
        }
        if (isset($request->from)) {
//            $accepts->where('accept_date', '>=', $from->format('Y-m-d'));
            $accepts->whereHas('contract', function($query) use ($request) {
                $from = new Carbon($request->from);
                $query->where('check_date', '>=', $from->format('Y-m-d'));
            });
        }
        if (isset($request->to)) {
//            $accepts->where('accept_date', '<=', $to->format('Y-m-d'));
            $accepts->whereHas('contract', function($query) use ($request) {
                $to = new Carbon($request->to);
                $query->where('check_date', '<=', $to->format('Y-m-d'));
            });
        }

        $model->whereIn('id', $accepts->get()->pluck('contract_id')->toArray());

        $agents = PaymentAccept::select('accept_user_id')->with('accept_user')->distinct()->get();
        foreach ($agents as $i => $item) {
            $agents[$i]['name'] = $item->accept_user ? $item->accept_user->name : '';
        }
        /* filters */
        if (isset($request->underwriter_id) && $request->underwriter_id > 0) {
            $model->whereIn('id', PaymentAccept::where('accept_user_id', '=', $request->underwriter_id)->pluck('contract_id')->toArray());
        }

        return $model;
    }

}