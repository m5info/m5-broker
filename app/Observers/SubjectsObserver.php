<?php

namespace App\Observers;

use App\Models\Contracts\Subjects;
use App\Models\Log\CUDLogs;
use App\Observers\Base\AbstractObserver;

class SubjectsObserver extends AbstractObserver
{
    private $log;

    protected function get_model_table()
    {
        return 'subjects';
    }

    public function __construct()
    {
        $this->log = new CUDLogs();
        $this->log->table = $this->get_model_table();
        $this->log->user_id = \Auth::id() ? \Auth::id() : null;
        $this->log->role_id = (\Auth::user() && \Auth::user()->role_id) ? \Auth::user()->role_id : null;
        $this->log->from_url = '/'.request()->path();
    }

    /**
     * Handle the contracts "created" event.
     *
     * @param  \App\Models\Contracts\Subjects  $subjects
     * @return void
     */
    public function created(Subjects $subjects)
    {
        $this->log->context_id = $subjects->id;
        $this->log->event = 1;
        $this->log->new_data = json_encode($subjects->getAttributes());
        $this->log->save();
    }

    /**
     * Handle the contracts "saved" event.
     *
     * @param  \App\Models\Contracts\Subjects  $subjects
     * @return void
     */
    public function saved(Subjects $subjects)
    {
        $this->log->context_id = $subjects->id;
        $this->log->event = 4;
        $this->log->old_data = json_encode($this->getOriginals($subjects));
        $this->log->new_data = json_encode($subjects->getChanges());
        $this->log->save();
    }

    /**
     * Handle the contracts "updated" event.
     *
     * @param  \App\Models\Contracts\Subjects  $subjects
     * @return void
     */
    public function updated(Subjects $subjects)
    {
        $this->log->context_id = $subjects->id;
        $this->log->event = 2;
        $this->log->old_data = json_encode($this->getOriginals($subjects));
        $this->log->new_data = json_encode($subjects->getChanges());
        $this->log->save();
    }

    /**
     * Handle the contracts "deleted" event.
     *
     * @param  \App\Models\Contracts\Subjects  $subjects
     * @return void
     */
    public function deleted(Subjects $subjects)
    {
        $this->log->context_id = $subjects->id;
        $this->log->event = 3;
        $this->log->old_data = json_encode($subjects->getAttributes());
        $this->log->save();
    }
}
