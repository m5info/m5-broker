<?php

namespace App\Observers;

use App\Models\BSO\BsoItem;
use App\Models\Log\CUDLogs;
use App\Observers\Base\AbstractObserver;

class BsoItemsObserver extends AbstractObserver
{
    private $log;

    protected function get_model_table()
    {
        return 'bso_items';
    }

    public function __construct()
    {
        $this->log = new CUDLogs();
        $this->log->table = $this->get_model_table();
        $this->log->user_id = \Auth::id() ? \Auth::id() : null;
        $this->log->role_id = (\Auth::user() && \Auth::user()->role_id) ? \Auth::user()->role_id : null;
        $this->log->from_url = '/'.request()->path();
    }

    /**
     * Handle the contracts "created" event.
     *
     * @param  \App\Models\BSO\BsoItem  $bso_items
     * @return void
     */
    public function created(BsoItem $bso_items)
    {
        $this->log->context_id = $bso_items->id;
        $this->log->event = 1;
        $this->log->new_data = json_encode($bso_items->getAttributes());
        $this->log->save();
    }

    /**
     * Handle the contracts "saved" event.
     *
     * @param  \App\Models\BSO\BsoItem  $bso_items
     * @return void
     */
    public function saved(BsoItem $bso_items)
    {
        $this->log->context_id = $bso_items->id;
        $this->log->event = 4;
        $this->log->old_data = json_encode($this->getOriginals($bso_items));
        $this->log->new_data = json_encode($bso_items->getChanges());
        $this->log->save();
    }

    /**
     * Handle the contracts "updated" event.
     *
     * @param  \App\Models\BSO\BsoItem  $bso_items
     * @return void
     */
    public function updated(BsoItem $bso_items)
    {
        $this->log->context_id = $bso_items->id;
        $this->log->event = 2;
        $this->log->old_data = json_encode($this->getOriginals($bso_items));
        $this->log->new_data = json_encode($bso_items->getChanges());
        $this->log->save();
    }

    /**
     * Handle the contracts "deleted" event.
     *
     * @param  \App\Models\BSO\BsoItem  $bso_items
     * @return void
     */
    public function deleted(BsoItem $bso_items)
    {
        $this->log->context_id = $bso_items->id;
        $this->log->event = 3;
        $this->log->old_data = json_encode($bso_items->getAttributes());
        $this->log->save();
    }
}
