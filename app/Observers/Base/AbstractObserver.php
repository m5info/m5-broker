<?php

namespace App\Observers\Base;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractObserver
{
    abstract protected function get_model_table();

    protected function getOriginals(Model $changed_model)
    {
        $originals = $changed_model->getAttributes();
        foreach ($changed_model->getDirty() as $key => $value)
        {
            $originals[$key] = $changed_model->getOriginal($key);
        }
        return $originals;
    }
}