<?php

namespace App\Observers;

use App\Models\Finance\Invoice;
use App\Models\Log\CUDLogs;
use App\Observers\Base\AbstractObserver;

class InvoicesObserver extends AbstractObserver
{
    private $log;

    protected function get_model_table()
    {
        return 'invoices';
    }

    public function __construct()
    {
        $this->log = new CUDLogs();
        $this->log->table = $this->get_model_table();
        $this->log->user_id = \Auth::id() ? \Auth::id() : null;
        $this->log->role_id = (\Auth::user() && \Auth::user()->role_id) ? \Auth::user()->role_id : null;
        $this->log->from_url = '/'.request()->path();
    }

    /**
     * Handle the contracts "created" event.
     *
     * @param  \App\Models\Finance\Invoice  $invoices
     * @return void
     */
    public function created(Invoice $invoices)
    {
        $this->log->context_id = $invoices->id;
        $this->log->event = 1;
        $this->log->new_data = json_encode($invoices->getAttributes());
        $this->log->save();
    }

    /**
     * Handle the contracts "saved" event.
     *
     * @param  \App\Models\Finance\Invoice  $invoices
     * @return void
     */
    public function saved(Invoice $invoices)
    {
        $this->log->context_id = $invoices->id;
        $this->log->event = 4;
        $this->log->old_data = json_encode($this->getOriginals($invoices));
        $this->log->new_data = json_encode($invoices->getChanges());
        $this->log->save();
    }

    /**
     * Handle the contracts "updated" event.
     *
     * @param  \App\Models\Finance\Invoice  $invoices
     * @return void
     */
    public function updated(Invoice $invoices)
    {
        $this->log->context_id = $invoices->id;
        $this->log->event = 2;
        $this->log->old_data = json_encode($this->getOriginals($invoices));
        $this->log->new_data = json_encode($invoices->getChanges());
        $this->log->save();
    }

    /**
     * Handle the contracts "deleted" event.
     *
     * @param  \App\Models\Finance\Invoice  $invoices
     * @return void
     */
    public function deleted(Invoice $payments)
    {
        $this->log->context_id = $payments->id;
        $this->log->event = 3;
        $this->log->old_data = json_encode($payments->getAttributes());
        $this->log->save();
    }
}
