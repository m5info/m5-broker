<?php

namespace App\Observers;

use App\Models\Cashbox\IncomeExpense;
use App\Models\Log\CUDLogs;
use App\Observers\Base\AbstractObserver;

class IncomesExpensesObserver extends AbstractObserver
{
    private $log;

    protected function get_model_table()
    {
        return 'incomes_expenses';
    }

    public function __construct()
    {
        $this->log = new CUDLogs();
        $this->log->table = $this->get_model_table();
        $this->log->user_id = \Auth::id() ? \Auth::id() : null;
        $this->log->role_id = (\Auth::user() && \Auth::user()->role_id) ? \Auth::user()->role_id : null;
        $this->log->from_url = '/'.request()->path();
    }

    /**
     * Handle the contracts "created" event.
     *
     * @param  \App\Models\Cashbox\IncomeExpense $income_expense
     * @return void
     */
    public function created(IncomeExpense $income_expense)
    {
        $this->log->context_id = $income_expense->id;
        $this->log->event = 1;
        $this->log->new_data = json_encode($income_expense->getAttributes());
        $this->log->save();
    }

    /**
     * Handle the contracts "saved" event.
     *
     * @param  \App\Models\Cashbox\IncomeExpense  $income_expense
     * @return void
     */
    public function saved(IncomeExpense $income_expense)
    {
        $this->log->context_id = $income_expense->id;
        $this->log->event = 4;
        $this->log->old_data = json_encode($this->getOriginals($income_expense));
        $this->log->new_data = json_encode($income_expense->getChanges());
        $this->log->save();
    }

    /**
     * Handle the contracts "updated" event.
     *
     * @param  \App\Models\Cashbox\IncomeExpense $income_expense
     * @return void
     */
    public function updated(IncomeExpense $income_expense)
    {
        $this->log->context_id = $income_expense->id;
        $this->log->event = 2;
        $this->log->old_data = json_encode($this->getOriginals($income_expense));
        $this->log->new_data = json_encode($income_expense->getChanges());
        $this->log->save();
    }

    /**
     * Handle the contracts "deleted" event.
     *
     * @param  \App\Models\Cashbox\IncomeExpense $income_expense
     * @return void
     */
    public function deleted(IncomeExpense $income_expense)
    {
        $this->log->context_id = $income_expense->id;
        $this->log->event = 3;
        $this->log->old_data = json_encode($income_expense->getAttributes());
        $this->log->save();
    }
}
