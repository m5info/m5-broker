<?php


namespace App\Http\QueryHandlers\Logs;

use App\Classes\BuilderDecorators\Period\Period;
use App\Classes\BuilderDecorators\Period\PeriodFieldFilter;
use App\Http\QueryHandlers\QueryHandler;

class CUDLogsQueryHandler extends QueryHandler
{

    public function table($value)
    {
        $this->builder->where('table','=', $value);
    }


    public function event($value)
    {
        $this->builder->where('event', $value);
    }

    public function for_administrator($value)
    {
        if($value){
            $this->builder->whereIn('event', [1, 2, 3]);
        }
    }

    public function user($value)
    {
        $this->builder->where('cud_logs.user_id', $value);
    }

    public function role($value)
    {
        $this->builder->where('cud_logs.role_id', $value);
    }

    public function context_id($value)
    {

        $this->builder->where('context_id', '=', $value);
    }

    public function date_from($value){
        Period::field($this->builder, 'created', function (PeriodFieldFilter $period) use ($value) {
            $period->from_date($value);
        });
    }

    public function date_to($value){
        Period::field($this->builder, 'created', function (PeriodFieldFilter $period) use ($value) {
            $period->to_date($value);
        });
    }
}
