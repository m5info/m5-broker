<?php

namespace App\Http\QueryHandlers\Analitics\Techunder;

use App\Classes\BuilderDecorators\Period\Period;
use App\Classes\BuilderDecorators\Period\PeriodFieldFilter;
use App\Http\QueryHandlers\QueryHandler;
use App\Models\Contracts\Subjects;
use Carbon\Carbon;

class TechunderContractsQueryHandler extends QueryHandler{

    public function underwriter_id($value){
//        $this->builder->whereHas('payment_accepts', function($query) use ($value){
//            $query->where('accept_user_id', (int)$value);
//        });
        $this->builder->where('check_user_id', (int)$value);
    }

    public function product_ids($value){
        if($value){
            $this->builder->whereHas('contract', function($query) use ($value){
                if(is_array($value)){
                    $query->whereIn('product_id', $value);
                }else{
                    $query->where('product_id', $value);
                }
            });
        }
    }

    public function from($value){
        $from = new Carbon($value);
        $this->builder->where('check_date', '>=', $from->format('Y-m-d'));

//        $this->builder->whereHas('payment_accepts', function($query) use ($value){
//            Period::field($query, 'accept_date', function(PeriodFieldFilter $filter) use ($value){
//                $filter->from_date($value);
//            });
//        });
    }

    public function to($value){
        $to = new Carbon($value);
        $this->builder->where('check_date', '<=', $to->format('Y-m-d'));

//        $this->builder->whereHas('payment_accepts', function($query) use ($value){
//            Period::field($query, 'accept_date', function(PeriodFieldFilter $filter) use ($value){
//                $filter->to_date($value);
//            });
//        });
    }

    public function sk($value){
        if($value){
            $this->builder->whereHas('contract', function($query) use ($value){
                $query->where('insurance_companies_id', $value);
            });
        }
    }

    public function insurer($value){
        if(isset($value)){
            $this->builder->whereIn('insurer_id', Subjects::where('title', 'LIKE', "%$value%")->get()->pluck('id'));
        }
    }

    public function agent_id($value){
        if($value > 0){
            $this->builder->where('agent_id', $value);
        }
    }

}