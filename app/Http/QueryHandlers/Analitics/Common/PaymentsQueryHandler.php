<?php

namespace App\Http\QueryHandlers\Analitics\Common;

use App\Http\QueryHandlers\QueryHandler;

class PaymentsQueryHandler extends QueryHandler
{


    public function agent_id($value)
    {
        if ((int)$value !== 0) {
            $this->builder->where('payments.agent_id', '=', $value);
        }
    }

    public function nop_id($value)
    {
        if ((int)$value !== 0) {
            $this->builder->where('payments.parent_agent_id', '=', $value);
        }
    }

    public function manager_id($value)
    {
        if ((int)$value !== 0) {
            $this->builder->where('payments.manager_id', '=', $value);
        }
    }

    public function manager_ids($value)
    {
        if (is_array($value) && count($value) > 0) {
            $this->builder->whereIn('manager.id', function ($query) use ($value) {
                $query->select('id')->from('users')->whereIn('id', $value);
            });
        }
    }

    public function insurance_ids($value)
    {
        if (is_array($value) && count($value)) {
            $this->builder->whereIn('payments.bso_id', function ($query) use ($value) {
                $query->select('id')->from('bso_items')->whereIn('insurance_companies_id', $value);
            });
        }else{
            if($value){
                $this->builder->whereIn('payments.bso_id', function ($query) use ($value) {
                    $query->select('id')->from('bso_items')->where('insurance_companies_id', '=',$value);
                });
            }
        }
    }

    public function payment_status_id($value)
    {
        if ((int)$value > -1) {
            $this->builder->where('payments.statys_id', '=', $value);
        }
    }

    public function type_id_($value)
    {
        if ($value > -1) {
            $this->builder->where('payments.type_id', '=', $value);
        }
    }

    public function personal_selling($value)
    {

        if ((int)$value > -1) {
            $this->builder->whereHas('contract', function ($query) use ($value) {
                $query->where('is_personal_sales', '=', $value);
            });
        }
    }

    public function product_id($value)
    {
        if (is_array($value) && count($value)) {
             $this->builder->whereIn('payments.bso_id', function ($query) use ($value) {
                $query->select('id')->from('bso_items')->whereIn('product_id', $value);
            });
        }else{
            if($value){
                $this->builder->whereIn('payments.bso_id', function ($query) use ($value) {
                    $query->select('id')->from('bso_items')->where('product_id', '=',$value);
                });
            }
        }
    }

    public function org_ids($value)
    {
        if ((int)$value > 0) {
            $this->builder->whereIn('payments.bso_id', function ($query) use ($value) {
                $query->select('id')->from('bso_items')->whereIn('org_id', $value);
            });
        }
    }


    public function kind_acceptance($value)
    {
        if ((int)$value > -1) {
            $this->builder->whereHas('contract', function ($query) use ($value) {
                $query->where('kind_acceptance', '=', $value);
            });
        }
    }

    public function insurer_title($value)
    {
        if (strlen($value) > 3) {
            $this->builder->whereIn('insurer.id', function ($query) use ($value) {
                $query->select('id')->from('subjects')->where('title', 'like', "%$value%");
            });
        }
    }

    public function department_ids($value)
    {
        if (is_array($value) && count($value) > 0) {
            $this->builder->whereIn('departments.id', function ($query) use ($value) {
                $query->select('id')->from('departments')->whereIn('id', $value);
            });
        }
    }

    public function payment_user_id($value)
    {
        if ($value > 0) {
            $this->builder->whereHas('invoice', function ($query) use ($value) {
                $query->where('invoice_payment_user_id', $value);
            });
        }
    }

    public function submit_receiver($value)
    {
        if ((int)$value > -1) {
            $this->builder->where('payment_flow', '=', $value);
        }
    }

    public function terms_sale($value)
    {
        if ($value > -1) {
            $this->builder->whereHas('contract', function ($query) use ($value) {
                $query->where('sales_condition', '=', $value);
            });
        }
    }

    public function is_deleted($value)
    {
        if ((int)$value > -1) {
            $this->builder->where('payments.is_deleted', '=', $value);
        }
    }

    public function prolongation_or_not($value)
    {
        if ($value > 0) {
            $this->builder->whereHas('contract', function ($query) use ($value) {
                $query->where('type_id', '=', $value);
            });
        }
    }

    public function payment_type($value)
    {
        if (!empty($value)) {
            if ($value == 'cash') {
                $this->builder->where('payment_flow', 0);
                $this->builder->where('payment_type', 0);
            } elseif ($value == 'cashless') {
                $this->builder->where('payment_flow', 0);
                $this->builder->where('payment_type', 1);
            } elseif ($value == 'card') {
                $this->builder->where('payment_flow', 0);
                $this->builder->where('payment_type', 2);
            } elseif ($value == 'sk') {
                $this->builder->where('payment_flow', '=', 1);
            }
        }
    }

    public function point_sale($value)
    {
        if (!empty($value)) {

            $this->builder->where('payments.point_sale_id', $value);
            /*
            $this->builder->whereHas('bso', function ($query) use ($value) {
                $query->whereHas('point_sale', function ($_query) use ($value) {
                    $_query->where('id', $value);
                });
            });
            */
        }
    }

    public function number_reports($value)
    {
        if (!empty($value)) {
            $this->builder->whereHas('reports_border', function ($query) use ($value) {
                $query->whereIn('id', $value);
            });
        }
    }

    public function payment_date_type_id($value)
    {
        if (request('date_from') || request('date_to')) {
            switch ($value) {
                case 1:
                    if (!empty(request('date_from'))) {
                        $this->builder->whereDate('payments.payment_data', '>=', date('Y-m-d H:i:s', strtotime(request('date_from'))));
                    }
                    if (!empty(request('date_to'))) {
                        $this->builder->whereDate('payments.payment_data', '<=', date('Y-m-d H:i:s', strtotime(request('date_to'))));
                    }
                    $this->builder->orderBy('payments.payment_data', 'desc');
                    break;
                case 2:
                    if (!empty(request('date_from'))) {
                        $this->builder->whereHas('contract', function ($query) {
                            $query->whereDate('begin_date', '>=', date('Y-m-d H:i:s', strtotime(request('date_from'))));
                        });
                    }
                    if (!empty(request('date_to'))) {
                        $this->builder->whereHas('contract', function ($query) {
                            $query->whereDate('begin_date', '<=', date('Y-m-d H:i:s', strtotime(request('date_to'))));
                        });
                    }
                    $this->builder->orderBy('contracts.begin_date', 'desc');
                    break;
                case 3:
                    if (!empty(request('date_from'))) {
                        $this->builder->whereDate('payments.invoice_payment_date', '>=', date('Y-m-d H:i:s', strtotime(request('date_from'))));
                    }
                    if (!empty(request('date_to'))) {
                        $this->builder->whereDate('payments.invoice_payment_date', '<=', date('Y-m-d H:i:s', strtotime(request('date_to'))));
                    }
                    $this->builder->orderBy('payments.invoice_payment_date', 'desc');
                    break;
                case 4:
                    if (!empty(request('date_from'))) {
                        $this->builder->whereHas('contract', function ($query) {
                            $query->whereDate('underfiller_date', '>=', date('Y-m-d H:i:s', strtotime(request('date_from'))));
                        });
                    }
                    if (!empty(request('date_to'))) {
                        $this->builder->whereHas('contract', function ($query) {
                            $query->whereDate('underfiller_date', '<=', date('Y-m-d H:i:s', strtotime(request('date_to'))));
                        });
                    }
                    $this->builder->orderBy('contracts.underfiller_date', 'desc');
                    break;
                case 5:
                    if (!empty(request('date_from'))) {
                        $this->builder->whereHas('contract', function ($payments) {
                            $payments->whereDate('check_date', '>=', date('Y-m-d H:i:s', strtotime(request('date_from'))));
                        });
                    }
                    if (!empty(request('date_to'))) {
                        $this->builder->whereHas('bso', function ($payments) {
                            $payments->whereDate('check_date', '<=', date('Y-m-d H:i:s', strtotime(request('date_to'))));
                        });
                    }


                    break;
            }
        }
    }


}