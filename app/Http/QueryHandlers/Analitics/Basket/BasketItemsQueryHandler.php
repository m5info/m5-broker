<?php


namespace App\Http\QueryHandlers\Analitics\Basket;


use App\Http\QueryHandlers\QueryHandler;

class BasketItemsQueryHandler extends QueryHandler{

    public function type_id($value){
        if($value > 0){
            $this->builder->where('type_id', $value);
        }
    }
}