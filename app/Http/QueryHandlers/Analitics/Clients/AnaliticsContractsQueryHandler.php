<?php

namespace App\Http\QueryHandlers\Analitics\Clients;

use App\Classes\BuilderDecorators\Period\Period;
use App\Classes\BuilderDecorators\Period\PeriodFieldFilter;
use App\Http\QueryHandlers\QueryHandler;

class AnaliticsContractsQueryHandler extends QueryHandler {

    public function period($value){
       if (!empty($value)){
           if ($value == 0){
//               Period::field($this->builder, 'sign_date', function(PeriodFieldFilter $pff) use ($value){
//                   $pff->month($value);
//               });
           }elseif($value == 1){
//               Period::field($this->builder, 'sign_date', function(PeriodFieldFilter $pff) use ($value){
//                   $pff->year($value);
//               });
           }elseif($value == 2){

           }

           /*Period::field($this->builder, 'sign_date', function(PeriodFieldFilter $pff) use ($value){
               $pff->from_date($value);
           });*/
       }
    }

    public function year($value){
        if (!empty($value)){
            if(request('period') == 1 || request('period') == 2){
                Period::field($this->builder, 'sign_date', function(PeriodFieldFilter $p) use ($value){
                    $p->year($value);
                });
            }
        }
    }

    public function month($value){
        if (!empty($value)){
            if(request('period') == 0 || request('period') == 2){
                Period::field($this->builder, 'sign_date', function(PeriodFieldFilter $p) use ($value){
                    $p->month($value);
                });
            }
        }
    }

    public function from($value){
        if(!empty($value)){
            if(request('period') == 3 || request('period') == 2){
                Period::field($this->builder, 'sign_date', function(PeriodFieldFilter $pff) use ($value){
                    $pff->from_date($value);
                });
            }
        }
    }

    public function to($value){
        if(!empty($value)) {
            if(request('period') == 3 || request('period') == 2){
                Period::field($this->builder, 'sign_date', function(PeriodFieldFilter $pff) use ($value){
                    $pff->to_date($value);
                });
            }
        }
    }
}
