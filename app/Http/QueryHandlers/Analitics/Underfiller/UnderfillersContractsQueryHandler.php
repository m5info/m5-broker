<?php

namespace App\Http\QueryHandlers\Analitics\Underfiller;

use App\Classes\BuilderDecorators\Period\Period;
use App\Classes\BuilderDecorators\Period\PeriodFieldFilter;
use App\Http\QueryHandlers\QueryHandler;

class UnderfillersContractsQueryHandler extends QueryHandler{

    public function underfiller_id($value){
        $this->builder->where('underfiller_id', (int)$value);
    }

    public function product_id($value){
        $this->builder->where('product_id', (int)$value);
    }

    public function from($value){

        Period::field($this->builder, 'underfiller_date', function(PeriodFieldFilter $filter) use ($value){
            $filter->from_date($value);
        });

    }

    public function to($value){

        Period::field($this->builder, 'underfiller_date', function(PeriodFieldFilter $filter) use ($value){
            $filter->to_date($value);
        });

    }

}