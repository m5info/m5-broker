<?php

namespace App\Http\QueryHandlers\Cashbox\ActsToUnderwriting;


use App\Classes\BuilderDecorators\Period\Period;
use App\Classes\BuilderDecorators\Period\PeriodFieldFilter;
use App\Http\QueryHandlers\QueryHandler;

class ContractListQueryHandler extends QueryHandler
{


    public function agent_id($value)
    {
        if ($value > 0) {
            $this->builder->where('contracts.agent_id', $value);
        }
    }

    public function kind_acceptance($value)
    {
        if ($value > 0) {
            $this->builder->where('contracts.kind_acceptance', $value);
        }
    }

    public function date_from($value){
        if(!empty($value)){
            Period::field($this->builder, 'contracts.created_at', function(PeriodFieldFilter $pff) use ($value){
                $pff->from_date($value);
            });
        }
    }

    public function date_to($value){
        if(!empty($value)) {
            Period::field($this->builder, 'contracts.created_at', function (PeriodFieldFilter $pff) use ($value) {
                $pff->to_date($value);
            });
        }
    }

    public function sk($value)
    {
        if ($value > 0) {
            $this->builder->where('contracts.insurance_companies_id', $value);
        }
    }

    public function product($value)
    {
        if ($value > 0) {
            $this->builder->where('contracts.product_id', $value);
        }
    }


}
