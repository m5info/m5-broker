<?php

namespace App\Http\QueryHandlers\Cashbox\ActsToUnderwriting;

use App\Classes\BuilderDecorators\Period\Period;
use App\Classes\BuilderDecorators\Period\PeriodFieldFilter;
use App\Http\QueryHandlers\QueryHandler;

class ActsListQueryHandler extends QueryHandler{

    public function number($value){
        $this->builder->where('act_number', 'LIKE', "%{$value}%");
    }

    public function agent_id($value){
        if($value>0){
            $this->builder->where('courier_id', $value);
        }
    }


    public function date_from($value){
        if(!empty($value)){
            Period::field($this->builder, 'time_create', function(PeriodFieldFilter $pff) use ($value){
                $pff->from_date($value);
            });
        }
    }

    public function date_to($value){
        if(!empty($value)) {
            Period::field($this->builder, 'time_create', function (PeriodFieldFilter $pff) use ($value) {
                $pff->to_date($value);
            });
        }
    }


}