<?php

namespace App\Http\QueryHandlers\Cashbox\IncomesExpenses;

use App\Http\QueryHandlers\QueryHandler;
use Carbon\Carbon;

class IncomeExpenseQueryHandler extends QueryHandler{

    public function category_id($value){

        if(is_array($value)){
            $this->builder->whereIn('category_id', $value);
        }
        /*
        if($value > 0){
            $this->builder->where('category_id', $value);
        }
        */
    }



    public function is_foundation($value){
        if($value == 1){
            $this->builder->whereHas('category', function($query) use ($value){
                return $query->where('counting_in_foundation_payments', '=', $value);
            });
        }
    }

    public function type_id($value){
        if($value > 0 && request('divide') == 0){
            $this->builder->whereHas('category', function($query) use ($value){
                return $query->where('type', '=', $value);
            });
        }
    }

    public function status_id($value){
        if($value > 0){
            $this->builder->where('status_id', $value);
        }
    }

    public function payment_user($value){
        if($value > 0){
            $this->builder->where('payment_user_id', $value);
        }
    }

    public function payment_id($value){
        if($value >= 0){
            $this->builder->where('payment_type', $value);
        }
    }

    public function date_from($value){
        if($value > 0){
            $this->builder->where('payment_date', '>=', Carbon::parse($value)->startOfDay());
        }
    }

    public function date_to($value){
        if($value > 0){
            $this->builder->where('payment_date', '<=', Carbon::parse($value)->endOfDay());
        }
    }


}