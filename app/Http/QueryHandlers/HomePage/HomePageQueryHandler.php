<?php

namespace App\Http\QueryHandlers\HomePage;

use App\Classes\BuilderDecorators\Period\Period;
use App\Classes\BuilderDecorators\Period\PeriodFieldFilter;
use App\Http\QueryHandlers\QueryHandler;

class HomePageQueryHandler extends QueryHandler
{
    public function type_id($value)
    {
        if (intval($value) > -1){
            $this->builder->where('type_id', '=', $value);
        }
    }

    public function event_type_id($value)
    {
        if (intval($value) > -1){
            $this->builder->where('event_type_id', '=', $value);
        }
    }

    public function date_from($value){

        if(!empty($value)){

            $this->builder->where('event_date', '>=', setDateTimeFormat(getDateFormatRu($value).' 00:00:00'));
            /*
            Period::field($this->builder, 'event_date', function(PeriodFieldFilter $pff) use ($value){
                $pff->from_date($value);
            });
            */
        }
    }

    public function date_to($value){
        if(!empty($value)) {

            $this->builder->where('event_date', '<=', setDateTimeFormat(getDateFormatRu($value).' 23:59:59'));
            /*
            Period::field($this->builder, 'event_date', function (PeriodFieldFilter $pff) use ($value) {
                $pff->to_date($value);
            });
            */
        }
    }

    public function balance($value){
        if(!empty($value)) {
            //$this->builder->where('balance_id', '=', $value);
        }
    }
}