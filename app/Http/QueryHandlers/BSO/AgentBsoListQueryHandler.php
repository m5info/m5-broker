<?php

namespace App\Http\QueryHandlers\BSO;

use App\Http\QueryHandlers\QueryHandler;

class AgentBsoListQueryHandler extends QueryHandler
{


    public function agent_id($value)
    {
        if ($value > 0) {
            //$this->builder->where('agent_id', $value);
            $this->builder->where('user_id', $value);
        }
    }

    public function point_sale_id($value)
    {
        if ($value > 0) {
            $this->builder->where('point_sale_id', $value);
        }
    }

    public function organization_id($value)
    {
        if ($value > 0) {
            $this->builder->where('org_id', $value);
        }
    }


    public function type_bso_id($value)
    {
        if ($value > 0) {
            $this->builder->where('type_bso_id', $value);
        }

    }


    public function bso_class_id($value)
    {
        if($value == -1){

        }elseif($value == 100){
            $this->builder->where('bso_class_id', $value);
        }else{
            $this->builder->whereIn('bso_class_id', [0,1]);
        }
    }



    public function product_id($value)
    {
        if ($value > 0) {
            $this->builder->where('product_id', $value);
        }

    }


    public function insurance_companies_id($value)
    {
        if ($value > 0) {
            $this->builder->where('insurance_companies_id', $value);
        }

    }

    public function nop_id($value)
    {
        if ($value > 0) {
            $this->builder->whereIn('user_id', function ($query) use ($value) {
                $query->select('id')->from('users')->where('parent_id', $value);
            });
        }
    }

    public function types($value)
    {
        switch ($value) {
            case 'bso_in_30':
                $this->builder->where('time_create', '<=', date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 30));
                break;

            case 'bso_in_90':
                $this->builder->where('time_create', '<=', date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 90));
                break;
        }
    }

    public function state_id($value)
    {
        if ($value >= 0) {
            $this->builder->where('state_id', $value);
        }
    }

    public function from($value)
    {
        if (strtotime($value) > 0) {
            $this->builder->whereDate('time_create', '>=', date('Y-m-d H:i:s', strtotime($value)));
        }
    }

    public function to($value)
    {
        if (strtotime($value) > 0) {
            $this->builder->whereDate('time_create', '<=', date('Y-m-d H:i:s', strtotime($value)));
        }
    }

    public function overdue($value)
    {
        if (isset($value) && $value) {
            $myData = date('Y-m-d 00:00:00', strtotime("- $value day"));

            $this->builder->where('transfer_to_agent_time', '<=', $myData);
        }
    }

}