<?php


namespace App\Http\QueryHandlers\Contracts\Underfillers;

use App\Classes\BuilderDecorators\Period\Period;
use App\Classes\BuilderDecorators\Period\PeriodFieldFilter;
use App\Http\QueryHandlers\QueryHandler;

class UnderfillersQueryHandler extends QueryHandler
{

    public function bso_title($value)
    {
        if ($value != "") {
            $this->builder->where('bso_title', 'like', "%{$value}%");
        }
    }

    public function date_from($value)
    {
        if ($value != '') {
            Period::field($this->builder, 'sign_date', function (PeriodFieldFilter $period) use ($value) {
                $period->from_date($value);
            });
        }
    }

    public function date_to($value)
    {
        if ($value != '') {
            Period::field($this->builder, 'sign_date', function (PeriodFieldFilter $period) use ($value) {
                $period->to_date($value);
            });
        }
    }

}
