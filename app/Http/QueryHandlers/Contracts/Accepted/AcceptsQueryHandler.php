<?php

namespace App\Http\QueryHandlers\Contracts\Accepted;

use App\Classes\BuilderDecorators\Period\Period;
use App\Classes\BuilderDecorators\Period\PeriodFieldFilter;
use App\Http\QueryHandlers\QueryHandler;
use Carbon\Carbon;
use Illuminate\Support\Str;

class AcceptsQueryHandler extends QueryHandler {

    public function bso_title($value){
        if(!empty($value)){
            $this->builder->whereIn('contract_id', function($q) use ($value){
                $q->select('id')->from('contracts')->where('bso_title', 'like', "%{$value}%");
            });
        }
    }

    public function org_id($value){
        if((int)$value>0){
            $this->builder->whereIn('contract_id', function($q) use ($value){
                $q->select('id')->from('contracts')->whereIn('bso_supplier_id', function($_q) use ($value){
                    $_q->select('id')->from('bso_suppliers')->where('purpose_org_id', (int)$value);
                });
            });

        }
    }

    public function sk($value){
        if((int)$value>0){
            $this->builder->whereIn('contract_id', function($q) use ($value){
                $q->select('id')->from('contracts')->where('insurance_companies_id', (int)$value);
            });
        }
    }

    public function bso_supplier_id($value){
        if((int)$value>0){
            $this->builder->whereIn('contract_id', function($q) use ($value){
                $q->select('id')->from('contracts')->where('bso_supplier_id', (int)$value);
            });
        }
    }

    public function type_id($value){
        if((int)$value >= 0){
            $this->builder->where('contracts.kind_acceptance', (int)$value);
        }
    }

    public function manager_id($value){
        if((int)$value>0){
            $this->builder->whereIn('contract_id', function($q) use ($value){
                $q->select('id')->from('contracts')->where('manager_id', (int)$value);
            });
        }
    }

    public function agent_id($value){
        if((int)$value>0){
            $this->builder->whereIn('contract_id', function($q) use ($value){
                $q->select('id')->from('contracts')->where('agent_id', (int)$value);
            });
        }
    }

    public function department_ids($value)
    {
        if (is_array($value) && count($value) > 0) {
            $this->builder->whereIn('departments.id', function ($query) use ($value) {
                $query->select('id')->from('departments')->whereIn('id', $value);
            });
        }
    }

    public function date_from($value){
        if(!empty($value)){
            $this->builder->whereHas('contract', function($query) use ($value) {
                $from = new Carbon($value);
                $query->where('check_date', '>=', $from->format('Y-m-d'));
            });
        }
    }

    public function date_to($value){
        if(!empty($value)){
            $this->builder->whereHas('contract', function($query) use ($value) {
                $to = new Carbon($value);
                $query->where('check_date', '<=', $to->format('Y-m-d'));
            });
        }
    }
}
