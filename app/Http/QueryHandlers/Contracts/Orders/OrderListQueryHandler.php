<?php


namespace App\Http\QueryHandlers\Contracts\Orders;

use App\Classes\BuilderDecorators\Period\Period;
use App\Classes\BuilderDecorators\Period\PeriodFieldFilter;
use App\Http\QueryHandlers\QueryHandler;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Subjects;
use App\Models\User;

class OrderListQueryHandler extends QueryHandler
{

    public function status($value)
    {
        if($value == 3){
            $this->builder->whereIn('status_id', [1,2,3]);
        }else{
            $this->builder->whereIn('status_id', [$value]);
        }



    }

    public function bso_title($value)
    {
        if ($value != "") {
            $this->builder->where('bso_title', 'like', "%{$value}%");
        }
    }

    public function product($value)
    {
        if ($value > 0) {
            $this->builder->where('product_id', $value);
        }
    }

    public function city($value)
    {
        if ($value > 0) {
            $this->builder->where('delivery_city_id', $value);
        }
    }




    public function insurer($value)
    {
        if ($value) {
            // TODO: лучше наверно сохранять страхователя в order для оптимизации
            $this->builder->whereIn('id', Contracts::whereIn('insurer_id', Subjects::where('title', 'LIKE', "%$value%")->get()->pluck('id'))->get()->pluck('order_form_id'));
        }
    }

    public function agent($value)
    {
        if ($value > 0) {

            $this->builder->where(function ($query) use ($value){
                $query->where('manager_id', '=', $value)
                    ->orWhere('delivery_user_id', '=', $value)
                    ->orWhere('agent_id', '=', $value);
            });
        }
    }


    public function department_ids($value)
    {
        $this->builder->where(function ($query) use ($value){
            $query->whereIn('manager_id', function($query) use ($value){
                $query->select('id')
                    ->from(with(new User)->getTable())
                    ->whereIn('department_id', $value);
            });
        });
    }


    public function conclusion_date_from($value)
    {
        if ($value != '') {
            Period::field($this->builder, 'delivery_date', function (PeriodFieldFilter $period) use ($value) {
                $period->from_date($value);
            });
        }
    }

    public function conclusion_date_to($value)
    {
        if ($value != '') {
            Period::field($this->builder, 'delivery_date', function (PeriodFieldFilter $period) use ($value) {
                $period->to_date($value);
            });
        }
    }


}
