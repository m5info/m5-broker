<?php


namespace App\Http\QueryHandlers\Contracts\TempContracts;

use App\Classes\BuilderDecorators\Period\Period;
use App\Classes\BuilderDecorators\Period\PeriodFieldFilter;
use App\Http\QueryHandlers\QueryHandler;

class VerificationContractListQueryHandler extends QueryHandler
{

    public function statys($value)
    {

        switch ((int)$value) {
            case 3:
                $this->builder->whereIn('contracts.id', function ($q) {
                    $q->select('payments.contract_id')->from('payments')->where('payments.statys_id', 0);
                    $q->whereIn('payments.invoice_id', function ($q) {
                        $q->select('invoices.id')->from('invoices')->where('invoices.status_id', 1);
                    });
                });
                break;
            default:
                $this->builder->whereIn('contracts.statys_id', [$value]);

        }
    }

    public function sk($value)
    {
        if (is_array($value)) {
            $value = array_filter($value);
            if ($value) {
                $this->builder->whereIn('insurance_companies.id', $value);
            }
        } else {
            if ($value > 0) {
                $this->builder->where('insurance_companies.id', $value);
            }
        }
    }

    public function bso_title($value)
    {
        if ($value != "") {
            $this->builder->where('bso_items.bso_title', 'like', "%{$value}%");
        }
    }

    public function product($value)
    {
        if ($value > 0) {
            $this->builder->where('products.id', $value);
        }
    }

    public function insurer($value)
    {
        if (strlen($value) > 3) {
            $this->builder->where('insurer.title', 'like', "%{$value}%");
        }
    }

    public function agent($value)
    {
        if ($value > 0) {
            $this->builder
                ->whereRaw("(agent.id = $value or manager.id = $value)");
        }
    }

    public function conclusion_date_from($value)
    {
        if ($value != '') {
            Period::field($this->builder, 'contracts.sign_date', function (PeriodFieldFilter $period) use ($value) {
                $period->from_date($value);
            });
        }
    }

    public function conclusion_date_to($value)
    {
        if ($value != '') {
            Period::field($this->builder, 'contracts.sign_date', function (PeriodFieldFilter $period) use ($value) {
                $period->to_date($value);
            });
        }
    }

    public function contract_type($value)
    {
        if ($value > 0) {
            $this->builder->where('contracts.kind_acceptance', $value);
        }
    }

}
