<?php

namespace App\Http\QueryHandlers\Contracts\Prolongation;

use App\Classes\BuilderDecorators\Period\Period;
use App\Classes\BuilderDecorators\Period\PeriodFieldFilter;
use App\Http\QueryHandlers\QueryHandler;
use Illuminate\Support\Facades\DB;


class ProlongationQueryHandler extends QueryHandler
{
    public function status($value)
    {
        if ($value > -2) {
            $this->builder->where('statys_id', '=', $value);
        }
    }

    public function sk($value)
    {
        if ($value != 0) {
            $this->builder->where('insurance_companies_id', '=', $value);
        }
    }

    public function bso_title($value)
    {
        if (strlen($value) > 3) {
            $this->builder->where('bso_title', 'LIKE', '%' . $value . '%');
        }
    }

    public function product($value)
    {
        if (is_array($value)) {
            $this->builder->whereIn('product_id', $value);
        }
    }

    public function date_type($value){

        if($value == 0){ // Диапазон дат

            $begin_date = request()->get('begin_date');
            $end_date = request()->get('end_date');

            if ($begin_date != 0) {
                Period::field($this->builder, 'begin_date', function (PeriodFieldFilter $filter) use ($begin_date) {
                    $filter->from_date($begin_date);
                });
            }
            if ($end_date != 0) {
                Period::field($this->builder, 'begin_date', function (PeriodFieldFilter $filter) use ($end_date) {
                    $filter->to_date($end_date);
                });
            }

        }elseif($value == 1){ // По месяцам

            $years = request()->get('years');
            $month = request()->get('month');

            $this->builder->whereMonth('begin_date', $month);

            if(is_array($years)){
                $years = implode(', ', $years);

                $this->builder->where(function($query) use ($years){
                    return $query->whereRaw(DB::raw("year(`begin_date`) in ({$years})"));
                });
            }

        }

    }

/*    public function begin_date($value)
    {

        if ($value != 0) {
            Period::field($this->builder, 'begin_date', function (PeriodFieldFilter $filter) use ($value) {
                $filter->from_date($value);
            });
        }
    }*/

/*    public function end_date($value)
    {
        if ($value != 0) {
            Period::field($this->builder, 'begin_date', function (PeriodFieldFilter $filter) use ($value) {
                $filter->to_date($value);
            });
        }
    }*/

    public function insurer($value)
    {
        if (strlen($value) > 3) {

            $this->builder->leftJoin('subjects', 'subjects.id', '=', 'contracts.insurer_id');
            $this->builder->where('subjects.title', 'like', "%{$value}%");

        }
    }

    public function manager($value)
    {
        if ($value > 0) {

            //$this->builder->where('manager_id', '=', $value);

            $this->builder->where(function ($query) use ($value) {
                $query->where('manager_id', '=', $value)
                    ->orWhere('agent_id', '=', $value);
            });

        }else{

            $this->builder->where('manager_id', '>=', -1);

        }
    }

    public function courier($value)
    {
        if ($value > -1 && $value != '') {

            $this->builder->where('agent_id', '=', $value);

        }
    }


    public function underwriter($value)
    {
        if ($value > -1 && $value != '') {

            $this->builder->whereHas('payment_accepts', function ($query) use ($value) {
                return $query->where('accept_user_id', '=', $value);
            });

        }
    }

}