<?php

namespace App\Http\QueryHandlers\Contracts\ReceptionContracts;

use App\Http\QueryHandlers\QueryHandler;

class FromCashboxActsQueryHandler extends QueryHandler {

    public function user_from($value){
        if(isset($value) && (int)$value > 0){
            $this->builder->where('user_id_from', (int)$value);
        }
    }

    public function number($value){
        if(isset($value) && !empty($value)){
            $this->builder->where('act_number', 'like',"%{$value}%");
        }
    }

    public function status($value){
        if((int)$value==0){
            $this->builder->where('under_accepted_id',0);
        }else{
            $this->builder->where('under_accepted_id', '!=', 0);
        }
    }

    public function invoice_id($value){
        if (isset($value) && $value>0){
            $this->builder->where('invoices.id','=', $value);
        }
    }

}
