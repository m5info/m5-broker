<?php


namespace App\Http\QueryHandlers\Orders;

use App\Classes\BuilderDecorators\Period\Period;
use App\Classes\BuilderDecorators\Period\PeriodFieldFilter;
use App\Http\QueryHandlers\QueryHandler;
use App\Models\Contracts\Subjects;

class InspectionOrderListQueryHandler extends QueryHandler
{

    public function status($value)
    {
        $this->builder->whereIn('status_order_id', [$value]);
    }


    public function product($value)
    {
        if ($value > 0) {
            $this->builder->where('product_id', $value);
        }
    }

    public function insurer($value)
    {
        if ($value) {
            $this->builder->whereIn('insurer_id', Subjects::where('title', 'LIKE', "%$value%")->get()->pluck('id'));
        }
    }

    public function conclusion_date_from($value)
    {
        if ($value != '') {
            Period::field($this->builder, 'delivery_date', function (PeriodFieldFilter $period) use ($value) {
                $period->from_date($value);
            });
        }
    }

    public function conclusion_date_to($value)
    {
        if ($value != '') {
            Period::field($this->builder, 'delivery_date', function (PeriodFieldFilter $period) use ($value) {
                $period->to_date($value);
            });
        }
    }


}
