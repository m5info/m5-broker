<?php

namespace App\Http\QueryHandlers\Directories\FinancialPolicy;

use App\Http\QueryHandlers\QueryHandler;

class FinancialPolicyQueryHandler extends QueryHandler{


    public function is_actual_fp($value){
        if($value != -1){
            $this->builder->where('is_actual', '=', $value);
        }
    }

    public function product($value){
        if($value != 0){
            $this->builder->where('product_id', '=', $value);
        }
    }

    public function fix_price($value){
        if ($value > -1){
            $this->builder->where('fix_price', $value);
        }
    }

}