<?php

namespace App\Http\QueryHandlers\Finance\Invoice;

use App\Http\QueryHandlers\QueryHandler;
use App\Models\Contracts\Payments;

class PaymentsQueryHandler extends QueryHandler
{


    public function agent_id($value)
    {
        if($value != 0){
            $this->builder->where('agent_id', '=', $value);
        }
    }

    public function type($value)
    {

        Payments::whereType($value, $this->builder);
    }

    public function insurance_companies_id($value)
    {
        if (is_array($value) && count($value) > 0) {
            $this->builder->whereIn('bso_id', function ($query) use ($value) {
                $query->select('id')->from('bso_items')->whereIn('insurance_companies_id', $value);
            });
        }else{
            if($value != -1){
                $this->builder->whereIn('bso_id', function ($query) use ($value) {
                    $query->select('id')->from('bso_items')->where('insurance_companies_id', $value);
                });
            }
        }
    }

    public function kind_acceptance($value)
    {
        if ($value > -1) {
            $this->builder->whereHas('contract', function ($query) use ($value) {
                $query->where('kind_acceptance', $value);
            });
        }
    }


}