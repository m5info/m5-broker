<?php

namespace App\Http\Requests\Api;

use App\Models\Users\ApiTokens;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class OsagoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $token = request()->header('Authorization');

        if($api_token = ApiTokens::where('api_token', '=', $token)->first()){

            $user = $api_token->user;

            Auth::login($user, true);

            return true;
        }else{
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contractId' => 'contract',
            'product' => [
                'required',
                'string',
                'regex:/^osago$|^kasko$/',
            ],
            'contract' => 'required',
            'contract.begin_time' => 'required|date_format:H:i',
            'contract.begin_date' => 'required|date_format:d.m.Y',
            'contract.end_date' => 'required|date_format:d.m.Y',
            'contract.insurer' => 'required',
            'contract.insurer.type' => 'required|boolean',
            'contract.insurer.fio' => 'required|string',
            'contract.insurer.sex' => 'required|boolean',
            'contract.insurer.birthdate' => 'required|date_format:d.m.Y',
            'contract.insurer.phone' => 'required',
            'contract.insurer.email' => 'required|email',
            'contract.insurer.address_born' => 'required',
            'contract.insurer.address_born_kladr' => 'required',
            'contract.insurer.address_register' => 'required',
            'contract.insurer.address_register_kladr' => 'required',
            'contract.insurer.address_register_region' => 'required',
            'contract.insurer.address_register_city' => 'required',
            'contract.insurer.address_register_city_kladr_id' => 'required',
            'contract.insurer.address_register_street' => 'required',
            'contract.insurer.address_register_house' => 'required',
            'contract.insurer.address_fact' => 'required',
            'contract.insurer.address_fact_kladr' => 'required',
            'contract.insurer.address_fact_region' => 'required',
            'contract.insurer.address_fact_city' => 'required',
            'contract.insurer.address_fact_city_kladr_id' => 'required',
            'contract.insurer.address_fact_street' => 'required',
            'contract.insurer.address_fact_house' => 'required',
            'contract.insurer.doc_type' => 'required|integer',
            'contract.insurer.doc_serie' => 'required|integer',
            'contract.insurer.doc_number' => 'required|integer',
            'contract.insurer.doc_date' => 'required|date_format:d.m.Y',
            'contract.insurer.doc_office' => 'string',
            'contract.insurer.doc_info' => 'string',
            'contract.owner.is_insurer' => 'required|boolean',
            'contract.owner.type' => 'required|boolean',
            'contract.drivers_type_id' => 'required|boolean',
            'contract.drivers_count' => 'required|integer',
            'contract.driver' => 'required',
            'contract.driver.*.doc_serie' => 'required',
            'contract.driver.*.doc_num' => 'required',
            'contract.driver.*.doc_date' => 'required|date_format:d.m.Y',
            'contract.driver.*.exp_date' => 'required|date_format:d.m.Y',
            'contract.object' => 'required',
            'contract.object.car_year' => 'required|date_format:Y',
            'contract.object.ts_category' => 'required|ts_category',
            'contract.object.mark_id' => 'required|mark',
            'contract.object.model_id' => 'required|model',
            'contract.object.purpose_id' => 'required|purpose',
            'contract.object.power' => 'required|float',
            'contract.object.vin' => 'required|string',
            'contract.object.reg_number' => 'required',
            'contract.object.doc_type' => 'required|boolean',
            'contract.object.docnumber' => 'required',
            'contract.object.docdate' => 'required|date_format:d.m.Y',
            'contract.object.dk_number' => 'integer',
            'contract.object.dk_date_from' => 'date_format:d.m.Y',
            'contract.object.dk_date' => 'date_format:d.m.Y',
        ];
    }


    public function messages()
    {
        return [
            'product.regex' => 'Поле должно содержать данные из документации: osago, kasko.',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * Чтоб запрос был как json - это для ответов
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
