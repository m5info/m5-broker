<?php

namespace App\Http\Controllers\Reports\ReportsSKSummary;

use App\Http\QueryHandlers\BsoActs\ActTransfer\ActListQueryHandler;
use App\Http\Controllers\Controller;
use App\Helpers\PaginationHelper;
use App\Models\BSO\BsoActs;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Organizations\Organization;
use App\Models\Reports\ReportOrders;
use Illuminate\Http\Request;

class ReportsSKSummaryController extends Controller {

    public function __construct() {
        $this->middleware('permissions:reports,reports_sk_summary');
        $this->breadcrumbs[] = [
            'label' => 'Финансовые потоки',
            'url' => 'reports/reports_sk_summary'
        ];
    }

    public function index()
    {
        $insurances_select = InsuranceCompanies::where('is_actual', 1);
        $report_year = ReportOrders::select('report_year')->latest()->distinct()->get();


        return view('reports.reports_sk_summary.index', [
            'insurances_select' => $insurances_select,
            'report_year' => $report_year,
        ]);
    }

    public function get_filters() {
        return view('reports.reports_sk_summary.filters', $this->get_suppliers());
    }

    public function get_table() {

        $data = $this->get_suppliers();
        $data['html'] = view("reports.reports_sk_summary.table", $data)->render();
        return $data;

    }

    public function get_suppliers() {

        $this->validate(request(), [
            'org_id' => 'integer',
            'insurance_id' => 'integer',
            'supplier_id' => 'integer',
            'report_month' => 'integer',
            'report_year' => 'integer',
        ]);



        $insurances = InsuranceCompanies::query();


        if (request()->has('insurance_id') && request('insurance_id') > 0) {
            $insurances->where('id', request('insurance_id'));
        }


        $insurances = $insurances->get()->keyBy('id');

        $report_year = ReportOrders::select('report_year')->latest()->distinct()->get();


        return [
            'insurances' => $insurances,
            'report_year' => $report_year,
        ];
    }


}
