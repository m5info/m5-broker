<?php

namespace App\Http\Controllers\Reports\ReportsSK;

use App\Http\QueryHandlers\BsoActs\ActTransfer\ActListQueryHandler;
use App\Http\Controllers\Controller;
use App\Helpers\PaginationHelper;
use App\Models\BSO\BsoActs;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Organizations\Organization;
use Illuminate\Http\Request;

class ReportsSKController extends Controller {

    public function __construct() {
        $this->middleware('permissions:reports,reports_sk');
        $this->breadcrumbs[] = [
            'label' => 'Отчеты СК',
            'url' => 'reports/reports_sk'
        ];
    }

    public function index() {
        return view('reports.reports_sk.index');
    }

    public function get_filters() {
        return view('reports.reports_sk.filters', $this->get_suppliers());
    }

    public function get_table() {

        $data = $this->get_suppliers();
        $data['html'] = view("reports.reports_sk.table", $data)->render();
        return $data;
    }

    public function get_suppliers() {

        $this->validate(request(), [
            'org_id' => 'integer',
            'insurance_id' => 'integer',
            'supplier_id' => 'integer',
            'group_type' => 'string',
        ]);

        if(request('group_type')){
            session(['report_sk.group_type' => request('group_type')]);
        }

        if(request('insurance_id')){
            session(['report_sk.insurance_id' => request('insurance_id')]);
        }

        if(request('org_id')){
            session(['report_sk.org_id' => request('org_id')]);
        }

        if(request('supplier_id')){
            session(['report_sk.supplier_id' => request('supplier_id')]);
        }



        $organizations = Organization::query()->whereIn('org_type_id', [1, 2]);
        $suppliers = BsoSuppliers::query()->with('insurance','reports')
        ->where('is_actual', '=', 1);
        $insurances = InsuranceCompanies::query();

        if (request()->has('org_id') && request('org_id') > 0) {
            $suppliers = $suppliers->where('purpose_org_id', request('org_id'));
        }

        if (request()->has('insurance_id') && request('insurance_id') > 0) {
            $suppliers->where('insurance_companies_id', request('insurance_id'));
        }

        if (request()->has('supplier_id') && request('supplier_id') > 0) {
            $suppliers->where('id', request('supplier_id'));
        }

        $suppliers = $suppliers->orderBy('title')->get()->keyBy('id');

        $organizations->whereIn('id', $suppliers->pluck('purpose_org_id')->toArray());
        //$insurances->whereIn('id', $suppliers->pluck('insurance_companies_id')->toArray());

        $organizations = $organizations->orderBy('title')->get()->keyBy('id');
        $insurances = $insurances->orderBy('title')->get()->keyBy('id');

        return [
            'organizations' => $organizations,
            'insurances' => $insurances,
            'suppliers' => $suppliers,
        ];
    }

    public function reports($supplier_id) {
        $supplier = BsoSuppliers::findOrFail($supplier_id);

        $this->breadcrumbs[] = [
            'label' => $supplier->title,
        ];


        return view('reports.list.index', [
            'supplier' => $supplier,
            'motion' => request()->has('motion') && request('motion') > 0 && request('motion') < 3 ? request('motion') : 0
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function reports_table($supplier_id, Request $request) {

        $result = [
            'perpage' => 10,
            'page' => 1,
        ];

        $supplier = BsoSuppliers::findOrFail($supplier_id);
        $reports = $supplier->reports();
        $reports->where('reports_orders.is_deleted', '=', 0);

        if($request->id && $request->id > 0){
            $reports->where('reports_orders.id', '=', $request->id);
        }


        if($request->payment_flow_type && $request->payment_flow_type > -1){
            $temp = array_flip($request->payment_flow_type);
            if (!collect($temp)->has(-1)){
                $reports->whereIn('reports_orders.payment_flow_type', $request->payment_flow_type);
            }
        }

        if ($request->type_id) {
            $reports->whereIn('reports_orders.type_id', $request->type_id);
        }
        if ($request->accept_status) {
            $reports->whereIn('reports_orders.accept_status', $request->accept_status);
        }
        if ($request->report_month) {
            $reports->whereIn('reports_orders.report_month', $request->report_month);
        }

        if ($request->like_title && strlen($request->like_title) >= 3) {
            $reports->where('reports_orders.title', 'like', "%{$request->like_title}%");
        }

        if ($request->has_adverstising) {
            if($request->has_adverstising == 1){
                $reports->where('reports_orders.advertising_id', '>', 0);
            }elseif($request->has_adverstising == 2){

                $reports->where(function ($query) {
                    $query->whereNull('reports_orders.advertising_id')
                        ->orWhere('reports_orders.advertising_id', '=', 0);
                });
            }
        }

        if ($request->year) {
            $reports->where('reports_orders.report_year', '=', $request->year);
        }

        if($request->use_by_button == 1 && $request->reports_in_list){
            $reports->whereIn('reports_orders.id',  $request->reports_in_list);
            $reports_in_list = $request->reports_in_list;
        }else{
            $reports_in_list = [];
        }

        if ($request->motion && $request->motion > 0 && $request->motion < 3){
            if ($request->motion == 1){
                $reports->where('reports_orders.to_transfer_total', '>', 0);
            }elseif($request->motion == 2){
                $reports->where('reports_orders.to_return_total', '>', 0);
            }
        }

        $result['count'] = $reports->count();

        if ($request->page_count > 0) {
            $result['perpage'] = $request->page_count;
        }

        if($request->use_by_button == 1 && $request->use_by_button != $request->used_by_button){
            $result['page'] = 1;
        }elseif ($request->page > 0) {
            $result['page'] = $request->page;
        }

        $reports->offset($result['perpage'] * ($result['page'] - 1));
        $reports->limit($result['perpage']);

        $result['used_by_button'] = $request->use_by_button == 1 ? 1 : 0;

        $result['html'] = view('reports.list.table', [
            'reports' => $reports->with('report_payment_sums')->get(),
            'reports_in_list' => $reports_in_list,
        ])->render();


        return $result;
    }

}
