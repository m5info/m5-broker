<?php

namespace App\Http\Controllers\Reports\ReportsSK;

use App\Http\QueryHandlers\BsoActs\ActTransfer\ActListQueryHandler;
use App\Http\Controllers\Controller;
use App\Helpers\PaginationHelper;
use App\Models\Account\Users2Columns;
use App\Models\BSO\BsoActs;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Organizations\Organization;
use App\Models\Reports\ReportOrders;
use Auth;
use DB;
use Illuminate\Http\Request;


class ReportsSKFormationController extends Controller
{

    const DEFAULT_NOT_SELECTED_COLUMNS_OFF = [


    ];

    const DEFAULT_NOT_SELECTED_COLUMNS_ON = [
        '№' => '№',
        'Акт в СК' => 'Акт в СК',
        '№ договора' => '№ договора',
        'Продукт' => 'Продукт',
        'Страхователь' => 'Страхователь',
        'Агент/Менеджер' => 'Агент/Менеджер',
        'Андеррайтер' => 'Андеррайтер',
        'Руководитель' => 'Руководитель',
        'Тип договора' => 'Тип договора',
        'Тип платежа' => 'Тип платежа',
        'Поток оплаты' => 'Поток оплаты',
        'Квитанция' => 'Квитанция',
        'Сумма' => 'Сумма',
        'Оф. скидка' => 'Оф. скидка',
        'Оф. скидка (%)' => 'Оф. скидка (%)',
        'Неоф. скидка' => 'Неоф. скидка',
        'Неоф. скидка (%)' => 'Неоф. скидка (%)',
        'Дата оплаты' => 'Дата оплаты',
        'Статус' => 'Статус',
        'Финансовая политика' => 'Финансовая политика',
        'КВ Агента' => 'КВ Агента',
        'Бордеро КВ (%)' => 'Бордеро КВ (%)',
        'Бордеро КВ' => 'Бордеро КВ',
        'Бордеро Отчет' => 'Бордеро Отчет',
        'ДВОУ КВ (%)' => 'ДВОУ КВ (%)',
        'ДВОУ КВ' => 'ДВОУ КВ',
        'ДВОУ Отчет' => 'ДВОУ Отчет',
        'Дата заключения договора' => 'Дата заключения договора',
        'Дата начала договора' => 'Дата начала договора',
        'Дата окончания договора' => 'Дата окончания договора',
        'Маркер' => 'Маркер',
        'Тип акцепта' => 'Тип акцепта',
        'СП - Бордеро' => 'СП - Бордеро',
        'Условие продажи' => 'Условие продажи'
    ];

    const TABLE_KEY_VISIBILITY = [
        '№' => 'all',
        'Акт в СК' => 'all',
        '№ договора' => 'all',
        'Продукт' => 'all',
        'Страхователь' => 'all',
        'Агент/Менеджер' => 'all',
        'Руководитель' => 'all',
        'Тип договора' => 'all',
        'Тип платежа' => 'all',
        'Поток оплаты' => 'all',
        'Квитанция' => 'all',
        'Сумма' => 'all',
        'Оф. скидка' => 'all',
        'Оф. скидка (%)' => 'all',
        'Неоф. скидка' => 'all',
        'Неоф. скидка (%)' => 'all',
        'Дата оплаты' => 'all',
        'Статус' => 'all',
        'Финансовая политика' => 'all',
        'КВ Агента' => 'all',
        'Бордеро КВ (%)' => 'all',
        'Бордеро КВ' => 'all',
        'Бордеро Отчет' => 'all',
        'ДВОУ КВ (%)' => 'all',
        'ДВОУ КВ' => 'all',
        'ДВОУ Отчет' => 'all',
        'Дата заключения договора' => 'all',
        'Дата начала договора' => 'all',
        'Дата окончания договора' => 'all',
        'Маркер' => 'all',
        'Тип акцепта' => 'all',
        'Андеррайтер' => 'all',
        'СП - Бордеро' => 'all',
        'Условие продажи' => 'all'

    ];

    private $table_key = 'reports_sk';

    protected $report_type = 0; //$report_type - 0 Бордеро 1 ДВОУ

    public function __construct(Request $request)
    {

        $Prefix = $request->route() ? $request->route()->getPrefix() : "";
        if (strpos($Prefix, 'bordereau') !== false) $this->report_type = 0;
        if (strpos($Prefix, 'dvoy') !== false) $this->report_type = 1;

        if ($this->report_type == 0) {

            $this->breadcrumbs[] = [
                'label' => 'Отчеты СК',
                'url' => 'reports/reports_sk'
            ];

            $bso_supplier_title = (BsoSuppliers::find($request->route()->supplier_id)) ? (BsoSuppliers::find($request->route()->supplier_id))->title : '';

            $this->breadcrumbs[] = [
                'label' => 'Формирования отчета Бордеро - '. $bso_supplier_title,
            ];

        } elseif ($this->report_type == 1) {

            $this->breadcrumbs[] = [
                'label' => 'Отчеты СК',
                'url' => 'reports/reports_sk'
            ];

            $bso_supplier_title = (BsoSuppliers::find($request->route()->supplier_id)) ? (BsoSuppliers::find($request->route()->supplier_id))->title : '';

            $this->breadcrumbs[] = [
                'label' => 'Формирования отчета ДВОУ - '. $bso_supplier_title,
            ];
        }
    }


    public function index($supplier_id)
    {
        $supplier = BsoSuppliers::findOrFail($supplier_id);

        return view('reports.formation.index', [
            'supplier' => $supplier,
            'table_key' => $this->table_key,
            'report_type' => $this->report_type,
            'report_name' => $this->getReportName(),
            'report_prefix' => $this->getReportPrefix(),

        ])->with('breadcrumbs', $this->breadcrumbs);
    }


    public function getReportName()
    {
        return ($this->report_type == 0) ? 'Бордеро' : 'ДВОУ';
    }

    public function getReportPrefix()
    {
        return ($this->report_type == 0) ? 'bordereau' : 'dvoy';
    }

    public function getReportSql()
    {
        return ($this->report_type == 0) ? 'reports_order_id' : 'reports_dvou_id';
    }


    public function get_table($supplier_id, Request $request)
    {

        $date_type = $request->date_type;
        $date_from = $request->date_from;
        $date_to = $request->date_to;


        $location_id = $request->location_id;
        $product_id = $request->product_id;

        $type_id = $request->type_id;
        $statys_id = $request->statys_id;

        $payment_type = $request->payment_type;
        $payment_flow = $request->payment_flow;
        $kind_acceptance = $request->kind_acceptance;

        $report_id = $request->report_id;


        $supplier = BsoSuppliers::findOrFail($supplier_id);
        $payments = $supplier->getPayments($this->report_type)
            ->with('bso', 'contract', 'agent', 'manager', 'parent_agent', 'financial_policy');

        if (isset($request->page_count) && $request->page_count > 0)
            $payments->limit($request->page_count);

        $payments->where($this->getReportSql(), $report_id);
        $payments->where('payments.type_id', 0);
        $payments->whereIn('payments.statys_id', [0, 1]);



        $payments->leftJoin('bso_items', 'bso_items.id', '=', 'payments.bso_id');
        $payments->leftJoin('bso_items as bso_items_bso_receipt', 'bso_items_bso_receipt.id', '=', 'payments.bso_receipt_id');
        $payments->leftJoin('contracts', 'contracts.id', '=', 'payments.contract_id');
        $payments->leftJoin('reports_act', 'payments.acts_sk_id', '=', 'reports_act.id');
        $payments->leftJoin('products', 'bso_items.product_id', 'products.id');
        $payments->leftJoin('subjects as insurer', 'contracts.insurer_id', 'insurer.id');
        $payments->leftJoin('users as parent_agent', 'payments.parent_agent_id', '=', 'parent_agent.id');
        $payments->leftJoin('financial_policies', 'payments.financial_policy_id', '=', 'financial_policies.id');
        $payments->leftJoin('reports_orders as reports_bordero', 'payments.reports_order_id', 'reports_bordero.id');
        $payments->leftJoin('reports_orders as reports_dvoy', 'payments.reports_dvou_id', 'reports_dvoy.id');
        $payments->leftJoin('users as agent', 'payments.agent_id', 'agent.id');
        $payments->leftJoin('users as manager', 'payments.manager_id', 'manager.id');
        $payments->leftJoin('users as underwriter', 'contracts.check_user_id', 'underwriter.id');

        if ($this->report_type == 1){
            //Для ДВОУ только ОСАГО, так сказала Пахорукова
            $payments->where('products.is_dvou', 1);
        }



        if ($product_id) $payments->whereIn('bso_items.product_id', $product_id);
        if ($location_id) $payments->whereIn('bso_items.location_id', $location_id);
        $payments->where('bso_items.state_id','!=',3); //БСО не испорчена

        if ($type_id) $payments->whereIn('contracts.type_id', $type_id);
        if ($statys_id) $payments->whereIn('payments.statys_id', $statys_id);

        if ($payment_type) $payments->whereIn('payments.payment_type', $payment_type);
        if ($payment_flow) $payments->whereIn('payments.payment_flow', $payment_flow);

        if ($kind_acceptance) $payments->whereIn('contracts.kind_acceptance', $kind_acceptance);



        switch ($date_type):
            case(1):
                if (!empty($date_from)) {
                    $payments->whereDate('payments.payment_data', '>=', date('Y-m-d H:i:s', strtotime($date_from)));
                }
                if (!empty($date_to)) {
                    $payments->whereDate('payments.payment_data', '<=', date('Y-m-d H:i:s', strtotime($date_to)));
                }
                $payments->orderBy('payments.payment_data', 'asc');
                break;
            case(2):
                if (!empty($date_from)) {
                    $payments->whereDate('contracts.sign_date', '>=', date('Y-m-d H:i:s', strtotime($date_from)));
                }
                if (!empty($date_to)) {
                    $payments->whereDate('contracts.sign_date', '<=', date('Y-m-d H:i:s', strtotime($date_to)));
                }
                $payments->orderBy('contracts.sign_date', 'asc');
                break;
            case(3):
                if (!empty($date_from)) {
                    $payments->whereDate('contracts.begin_date', '>=', date('Y-m-d H:i:s', strtotime($date_from)));
                }
                if (!empty($date_to)) {
                    $payments->whereDate('contracts.begin_date', '<=', date('Y-m-d H:i:s', strtotime($date_to)));
                }
                $payments->orderBy('contracts.begin_date', 'asc');
                break;
            case(4):
                if (!empty($date_from)) {
                    $payments->whereDate('contracts.end_date', '>=', date('Y-m-d H:i:s', strtotime($date_from)));
                }
                if (!empty($date_to)) {
                    $payments->whereDate('contracts.end_date', '<=', date('Y-m-d H:i:s', strtotime($date_to)));
                }
                $payments->orderBy('contracts.end_date', 'asc');
                break;
        endswitch;


        $payments->select("payments.*");

       // dd(getLaravelSql($payments));

        $payments_sql = clone $payments;

        $user = Auth::user();

        /* Проверка на настройку таблицы */
        $check_builder = Users2Columns::query();
        $check_builder->leftJoin('table_columns', 'table_columns.id', '=', 'users2columns.column_id');
        $check_builder->where('users2columns.user_id', '=', Auth::id())->where('table_columns.table_key', '=', $this->table_key);
        $check_result = $check_builder->select('users2columns.id as users2columns_id')->get();

        $visibility_ = new \stdClass();
        $visibility_->title = 'all';

        $visibility = [
            'all' => $visibility_
        ];

        $user_columns = $user->getTableColumns($this->table_key);

        $query = [
            'fields' => ['payments.*'],
            'where' => [],
            'order' => [],
        ];

        foreach ($user_columns as $column) {
            if ($column['is_as'] == 1) {
                $query['fields'][] = "{$column['column_key']} as {$column['as_key']}";
            }
        }

        foreach ($query['fields'] as $key => &$v) {
            $v = DB::raw($v);
        }

        $payments->select($query['fields']);

        $payments = $payments->get();

        foreach ($payments as $payment) {
            if ($payment->payment_flow == 1 && !$payment->marker_text) { // если СК и цвет маркера не настроен
                $payment->marker_color = '#fffae6';
            }
        }

        return [
            'table' => view('reports.formation.table', [
                'visibility' => $visibility,
                'check_result' => $check_result->isNotEmpty(),
                'supplier' => $supplier,
                'user_columns' => $user_columns,
                'report_type' => $this->report_type,
                'report_name' => $this->getReportName(),
                'report_prefix' => $this->getReportPrefix(),
                'payments_sql' => $payments_sql,
                'payments' => $payments,
            ])->render(),
            'statistic' => view('reports.formation.statistic', [
                'payments_sql' => $payments_sql,
            ])->render()

        ];


    }


    public function get_action_table($supplier_id, Request $request)
    {
        $supplier = BsoSuppliers::findOrFail($supplier_id);

        $reports = ($this->report_type == 0) ? $supplier->reports_border : $supplier->reports_dvoy;
        $reports = $reports->where('is_deleted', 0)->sortByDesc('created_at');

        return view('reports.formation.action_table', [
            'supplier' => $supplier,
            'report_prefix' => $this->getReportPrefix(),
            'report_id' => $request->report_id,
            'reports' => $reports,
        ]);


    }


    public function execute($supplier_id, Request $request){


        $status = 1;
        $report_dvou = false;

        $supplier = BsoSuppliers::findOrFail($supplier_id);
        $payment_ids = array_map('intval', request('payment_ids'));

        $payments = $supplier->getPayments($this->report_type);
        $payments->whereIn("payments.id", $payment_ids);


        //Создаем Отчет
        if((int)$request->event_id == 1){

            $report_data = [
                'type_id' => $this->report_type,
                'title' => request()->get('report_name'),
                'bso_supplier_id' => request()->route('supplier_id'),
                'report_year' => request()->get('report_year'),
                'report_month' => request()->get('report_month'),
                'create_user_id' => auth()->id(),
                'report_date_start' => date('Y-m-d', strtotime(request()->get('report_date_start'))),
                'report_date_end' => date('Y-m-d', strtotime(request()->get('report_date_end'))),
            ];


            $report = ReportOrders::create($report_data);

            if($request->and_dvou_report == 1){
                $report_data_dvou = $report_data;
                $report_data_dvou['type_id'] = 1;
                $report_data_dvou['title'] .= ' (Создан автоматически)';
                $report_dvou = ReportOrders::create($report_data_dvou);

            }


        }

        //Добавляем в Отчет
        if((int)$request->event_id == 2){
            $report = ReportOrders::find((int)$request->get('to_report_id'));
        }

        $needRefreshReport = false;
        $needRefreshReportDvou = false;
        $reportSql = $this->getReportSql();

        if((int)$request->event_id <= 0){
            //Движение ДОГОВОРА
            $payments->update([$reportSql => (int)$request->event_id]);

        } else {

            foreach($payments->get() as $payment){

                if($report){

                    //Добавляем ЛОГ
                    $payment->setBsoLogToPayment(10);

                    $needRefreshReport = true;

                    if($request->and_dvou_report == 1 && $report_dvou){
                        if((int)$payment->contract->product->is_dvou == 1){

                            $payment->update(['reports_dvou_id' => (int)$report_dvou->id]);
                            $needRefreshReportDvou = true;
                        }
                    }

                }else{
                    $status = 0;
                }
            }
        }

        // обновляем 1 раз после проверки, а то долго отрабатывает
        if ($needRefreshReport){
            $payments->update([$reportSql => (int)$report->id]);
            //Обновляем цифры
            $report->refreshSumOrder(1);
        }
        if ($needRefreshReportDvou){
            //Обновляем цифры
            $report_dvou->refreshSumOrder(1);
        }

        return response()->json(['status' => $status]);
    }

    public function filter_last_reports($supplier_id, Request $request){

        $supplier = BsoSuppliers::findOrFail($supplier_id);

        $reports = ($this->report_type == 0) ? $supplier->reports_border : $supplier->reports_dvoy;

        $reports = $reports->where('is_deleted', 0)->sortByDesc('created_at');

        /*
        if($request->year && $request->month){
            $reports = $reports
                ->where('report_year', '=', $request->year)
                ->where('report_month', '=', $request->month);
        }
        */

        return [
            'html' => view('reports.formation.last_reports', ['reports' => $reports])->render()
        ];
    }


}
