<?php

namespace App\Http\Controllers\Reports\ReportsSK;

use App\Classes\Export\ExportManager;
use App\Http\Controllers\Controller;
use App\Models\Contracts\Payments;
use App\Models\Organizations\Organization;
use App\Models\Reports\ReportOrderBasePayment;
use App\Models\Reports\ReportOrders;
use App\Models\Settings\TemplateCategory;
use App\Repositories\CustomFilesRepository;
use Illuminate\Http\Request;


class ReportsSKOrderController extends Controller
{

    protected $filesRepository;

    public function __construct(CustomFilesRepository $filesRepository)
    {
        $this->filesRepository = $filesRepository;
        $this->middleware('permissions:reports,reports_sk');
        $this->breadcrumbs[] = [
            'label' => 'Отчеты СК',
            'url' => 'reports/reports_sk'
        ];
    }


    public function index($report_id)
    {

        $orgs = Organization::find(1);

        $report = ReportOrders::find($report_id);

        $this->breadcrumbs[] = [
            'label' => $report->bso_supplier->title,
            'url' => "{$report->bso_supplier->id}/reports"
        ];

        $this->breadcrumbs[] = [
            'label' => "Отчет ".ReportOrders::TYPE[$report->type_id]. " ID $report_id",
        ];

        $payments = $report->getPayments();
        $payments_sql = clone $payments;

        return view('reports.order.index', [
            'report' => $report,
            'payments' => $payments->get(),
            'payments_sql' => $payments_sql,
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function get_table(Request $request)
    {
        $data = $this->get_suppliers();

        $data = view("payments.reports.payments", ['payments' => $data, 'report_id' => $request->report_id])->render();
        return $data;
    }

    public function upload_file(Request $request, $id)
    {
        ReportOrders::findOrFail($id)->files()->save($this->filesRepository->makeFile($request->file, ReportOrders::DOCUMENTS_PATH. "/$id/"));
        return response(200);
    }

    public function get_suppliers()
    {
        $orders = ReportOrders::find(request('report_id'));

        $payments = $orders->getPayments();

        $payments->select("payments.*");

        $payments->leftJoin('contracts', 'contracts.id', '=', 'payments.contract_id');
        $payments->leftJoin('bso_items', 'bso_items.id', '=', 'payments.bso_id');


        if (request('payment_flow')){
            $payments->whereIn('payments.payment_flow', request('payment_flow'));
        }

        if (request('type_id')){
            $payments->whereIn('payments.type_id', request('type_id'));
        }

        if (request('statys_id')){
            $payments->whereIn('payments.statys_id', request('statys_id'));
        }
        if (request('product_id')){
            $payments->whereIn('bso_items.product_id', request('product_id'));
        }
        if (request('location_id')){
            $payments->whereIn('bso_items.location_id', request('location_id'));
        }

        if (request('payment_type')){
            $payments->whereIn('payments.payment_type', request('payment_type'));
        }

        if (request('kind_acceptance')){
            $payments->whereIn('contracts.kind_acceptance', request('kind_acceptance'));
        }

        $payments->where('payments.type_id','=',0); //только взносы

        return $payments->get();

    }


    public function save($report_id, Request $request)
    {

        $report = ReportOrders::find($report_id);

        $report->title = $request->title;

        $report->report_month = $request->report_month;
        $report->report_year = $request->report_year;


        $report->report_date_start = setDateTimeFormat($request->report_date_start);
        $report->report_date_end = setDateTimeFormat($request->report_date_end);

        $report->signatory_org = $request->signatory_org;
        $report->signatory_sk_bso_supplier = $request->signatory_sk_bso_supplier;
        $report->comments = $request->comments;



        if ((int)$request->accept_status == 2 && $report->accept_status != 2) {
            $report->accept_user_id = auth()->id();
            $report->accepted_at = getDateTime();
        }

        $report->accept_status = $request->accept_status;
        $report->save();

        return redirect("/reports/order/$report_id")->with('success', trans('form.success_update'));
    }

    public function delete_payments($report_id, Request $request)
    {
        $status = 1;

        $report = ReportOrders::find($report_id);
        $payment_ids = array_map('intval', request('payment_ids'));

        $payments = $report->getPayments();
        $payments->whereIn("payments.id", $payment_ids);


        $report_type = ($report->type_id == 0) ? 'reports_order_id' : 'reports_dvou_id';

        foreach ($payments->get() as $payment) {

            /*$base = ReportOrderBasePayment::where('report_order_id', $report_id)->where('payment_id', $payment->id)->get()->first();
            if($base){
                $base->delete();
            }*/


            $payment->update([$report_type => -1]);
            $payment->setBsoLogToPayment(8);

        }

        //Обновляем цыфры
        $report->refreshSumOrder();


        return response()->json(['status' => $status]);

    }


    public function delete_report_with_payments($report_id, Request $request)
    {
        $status = 1;

        $report = ReportOrders::find($report_id);

        $payments = $report->getPayments();

        $report_type = ($report->type_id == 0) ? 'reports_order_id' : 'reports_dvou_id';

        foreach ($payments->get() as $payment) {

           /* $base = ReportOrderBasePayment::where('report_order_id', $report_id)->where('payment_id', $payment->id)->get()->first();
            if($base = $payment->base_payment){
                $base->delete();
            }*/


            $payment->update([$report_type => 0]);
            $payment->setBsoLogToPayment(8);

        }

        $report->is_deleted = 1;
        $report->save();

        //Обновляем цифры
        $report->refreshSumOrder();


        return response()->json(['status' => $status]);

    }


    public function recalc_kv($report_id)
    {

        $this->validate(request(), [
            'payment_ids' => 'array',
            'payment_ids.*' => 'integer',
            'kv_borderau' => 'integer',
            'kv_dvou' => 'integer',
            'marker_color' => 'integer',
            'marker_text' => 'string',
        ]);


        $result = ['status' => 'ok'];

        if(request('kv_borderau') || request('kv_dvou')){


            $report = ReportOrders::find($report_id);
            $payment_ids = array_map('intval', request('payment_ids'));

            $payments = $report->getPayments();
            $payments->whereIn("payments.id", $payment_ids);
            foreach ($payments->get() as $payment) {

                if(ReportOrders::saveBaseDataPayment($payment, $report->id) == true){
                    $new_borderau = request('kv_borderau', $payment->financial_policy_kv_bordereau);
                    $new_dvou = request('kv_dvou', $payment->financial_policy_kv_dvoy);
                    $new_borderau_total = $new_borderau / 100 * $payment->payment_total;
                    $new_dvou_total = $new_dvou / 100 * $payment->payment_total;



                    $updated_data = [
                        'financial_policy_kv_bordereau' => $new_borderau,
                        'financial_policy_kv_bordereau_total' => $new_borderau_total,
                        'financial_policy_kv_dvoy' => $new_dvou,
                        'financial_policy_kv_dvoy_total' => $new_dvou_total,
                        'financial_policy_manually_set' => 1
                    ];

                    $payment->update($updated_data);
                    $payment->setBsoLogToPayment(11);
                }
            }
            //Обновляем цифры
            $report->refreshSumOrder();

        }


        if (!empty(request('marker_text')) || (int)request('marker_color') > 0) {
            $this->marker_payments($report_id);
        }

        return response()->json($result);

    }

    public function marker_payments($report_id)
    {

        $request = request();
        $status = 1;

        $report = ReportOrders::find($report_id);
        $payment_ids = array_map('intval', request('payment_ids'));

        $payments = $report->getPayments();
        $payments->whereIn("payments.id", $payment_ids);

        foreach ($payments->get() as $payment) {

            $color = ReportOrders::MARKER_COLORS[$request->marker_color];

            $payment->update([
                'marker_color' => $color['color'],
                'marker_text' => $request->marker_text,
            ]);

        }

        //Обновляем цифры
        $report->refreshSumOrder();

        return response()->json(['status' => $status]);

    }


    public function form_report($report_id)
    {
        $this->validate(request(), [
            'template_id' => 'integer'
        ]);

        $builder = ReportOrders::query()->where('id', $report_id);
        $report = $builder->firstOrFail();

        $category = TemplateCategory::get($report->getTemplateCategory());
        $supplier_id = $report->bso_supplier_id;

        return (new ExportManager($category, $builder, $supplier_id))->handle();
    }


    public function set_edit_base_payments($report_id, Request $request)
    {
        $report = ReportOrders::find($report_id);
        foreach ($report->report_base_payments() as $base_payments){

            if($base_payments->editDataBasePayment() == true){
                $base_payments->delete();
            }

        }

        $report->refreshSumOrder();

        return response('', 200);
    }

    public function generate_dvou_report($report_id)
    {

        try{

            $report_bordereau = ReportOrders::query()->where('id', $report_id)->first();

            /* Точную копию отчета БОРДЕРО  */
            $report_dvou = new ReportOrders();
            $report_dvou->type_id = 1;
            $report_dvou->title = $report_bordereau->title.' (Создан автоматически)';
            $report_dvou->bso_supplier_id = $report_bordereau->bso_supplier_id;
            $report_dvou->report_year = $report_bordereau->report_year;
            $report_dvou->report_month = $report_bordereau->report_month;
            $report_dvou->create_user_id = $report_bordereau->create_user_id;
            $report_dvou->report_date_start = $report_bordereau->report_date_start;
            $report_dvou->report_date_end = $report_bordereau->report_date_end;
            $report_dvou->payment_flow_type = $report_bordereau->payment_flow_type;
            $report_dvou->save();

            Payments::query()->where('reports_order_id', $report_id)->update(['reports_dvou_id' => $report_dvou->id]);

            $report_dvou->refreshSumOrder();

            return response()->json([
                'status' => true,
                'message' => 'Отчет ДВОУ успешно создан'
            ]);
        }catch(\Exception $e){
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
