<?php

namespace App\Http\Controllers\Reports\ReportsAdverstising;

use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Organizations\Organization;
use App\Models\Reports\AdverstisingReports;
use App\Models\Reports\ReportOrders;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportsAdverstisingController extends Controller
{
    public function __construct(){

        $this->middleware('permissions:reports,reports_advertising');
        $this->breadcrumbs[] = [
            'label' => 'Рекламник',
            'url' => 'reports/promotion'
        ];
    }
    public function adverstisings($supplier_id){

        $supplier = BsoSuppliers::findOrFail($supplier_id);

        $this->breadcrumbs[] = [
            'label' => 'Список',
        ];

        return view('reports.advertising.adv_list.index', [
            'supplier' => $supplier,
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function adverstisings_table($supplier_id, Request $request) {

        $data = $this->get_adverstising_suppliers($supplier_id, $request);
        $data['html'] = view("reports.advertising.adv_list.table", $data)->render();
        return $data;
    }

    public function get_adverstising_suppliers($supplier_id, Request $request) {

        $this->validate(request(), [
            'supplier_id' => 'integer',
        ]);

        $adverstising = AdverstisingReports::query()
        ->where('supplier_id', '=', $supplier_id);

        if($request->accept_status){
            $adverstising->whereIn('accept_status', $request->accept_status);
        }

        if($request->like_title){
            $adverstising->where('title', 'like', "%$request->like_title%");
        }

        return [
            'supplier_id' => $supplier_id,
            'adverstisings' => $adverstising->get(),
        ];
    }

    public function update_adverstising(Request $request) {

        if($request->event == 1){ // создание

            $adv = AdverstisingReports::create([
                'title' => $request->title,
                'supplier_id' => $request->supplier_id,
                'accept_status' => 0,
                'create_user_id' => auth()->id()
            ]);

            foreach($request->reports_list as $report_id){
                $report = ReportOrders::find($report_id);

                if($report->type_id == 0){ // ДВОУ в создании рекламника не учитывать
                    $report->advertising_id = $adv->id;
                    $report->save();
                }
            }

            AdverstisingReports::recount_payments($adv->id);

        }elseif($request->event == 2){ // добавление

            if($request->adverstising_id){
                $adv = AdverstisingReports::find($request->adverstising_id);

                foreach($request->reports_list as $report_id){
                    $report = ReportOrders::find($report_id);

                    if($report->type_id == 0){ // ДВОУ в создании рекламника не учитывать
                        $report->advertising_id = $adv->id;
                        $report->save();
                    }
                }

                AdverstisingReports::recount_payments($request->adverstising_id);
            }

        }elseif($request->event == 3){ // обновление

            $adv = AdverstisingReports::find($request->adverstising_id);

            $adv->comment = $request->comment;
            $adv->accept_status = $request->accept_status;
            $adv->save();

            foreach($adv->reports as $report){
                $report->accept_status = $request->accept_status;
                $report->save();
            }

            AdverstisingReports::recount_payments($adv->id);

        }elseif($request->event == 4){ // удаление

            $report = ReportOrders::find($request->report_id);
            $report->advertising_id = '';

            $report->save();

            AdverstisingReports::recount_payments($request->adverstising_id);

        }

        return redirect()->back();
    }


    public function index(){
        return view('reports.advertising.index');
    }

    public function get_filters() {
        return view('reports.advertising.filters', $this->get_suppliers());
    }

    public function get_table() {

        $data = $this->get_suppliers();
        $data['html'] = view("reports.advertising.table", $data)->render();
        return $data;
    }

    public function get_suppliers() {

        $this->validate(request(), [
            'org_id' => 'integer',
            'insurance_id' => 'integer',
            'supplier_id' => 'integer',
            'group_type' => 'string',
        ]);

        if(request('group_type')){
            session(['report_sk.group_type' => request('group_type')]);
        }

        $organizations = Organization::query()->whereIn('org_type_id', [1, 2]);
        $suppliers = BsoSuppliers::query()->orderBy('purpose_org_id')->with('insurance','reports');
        $insurances = InsuranceCompanies::query();

        if (request()->has('org_id') && request('org_id') > 0) {
            $suppliers = $suppliers->where('purpose_org_id', request('org_id'));
        }

        if (request()->has('insurance_id') && request('insurance_id') > 0) {
            $suppliers->where('insurance_companies_id', request('insurance_id'));
        }

        if (request()->has('supplier_id') && request('supplier_id') > 0) {
            $suppliers->where('id', request('supplier_id'));
        }

        $suppliers = $suppliers->get()->keyBy('id');

        $organizations->whereIn('id', $suppliers->pluck('purpose_org_id')->toArray());

        $organizations = $organizations->get()->keyBy('id');
        $insurances = $insurances->get()->keyBy('id');

        $adverstidings = AdverstisingReports::query();

        return [
            'organizations' => $organizations,
            'insurances' => $insurances,
            'suppliers' => $suppliers,
            'adverstidings' => $adverstidings,
        ];
    }

    public function reports($supplier_id, $adverstising_id) {

        $supplier = BsoSuppliers::findOrFail($supplier_id);
        $adv = AdverstisingReports::find($adverstising_id);

        $this->breadcrumbs[] = [
            'label' => 'Список',
            'url' => "$supplier_id/adverstisings"
        ];
        $this->breadcrumbs[] = [
            'label' => $adv->title,
        ];

        $report_pay_income = 0;
        $report_pay_outcome = 0;

        foreach($adv->reports as $report){
            $report_pay_income += $report->report_payment_sums->where('type_id', 1)->sum('amount'); // приход
            $report_pay_outcome += $report->report_payment_sums->where('type_id', 0)->sum('amount'); // расход
        }

        return view('reports.advertising.list.index', [
            'supplier' => $supplier,
            'report_pay_income' => $report_pay_income,
            'report_pay_outcome' => $report_pay_outcome,
            'adverstising_id' => $adverstising_id,
            'adverstising' => $adv,
            'motion' => request()->has('motion') && request('motion') > 0 && request('motion') < 3 ? request('motion') : 0
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function reports_table($supplier_id, $adverstising_id, Request $request) {

        $result = [
            'perpage' => 10,
            'page' => 1,
        ];

        $supplier = BsoSuppliers::findOrFail($supplier_id);
        $reports = $supplier->reports();
        $reports->where('reports_orders.is_deleted', '=', 0);
        $reports->where('reports_orders.advertising_id', '=', $adverstising_id);

        if($request->id && $request->id > 0){
            $reports->where('reports_orders.id', '=', $request->id);
        }


        if($request->payment_flow_type && $request->payment_flow_type > -1){
            $temp = array_flip($request->payment_flow_type);
            if (collect($temp)->has(-1)){
                $reports->whereIn('reports_orders.payment_flow_type', [0, 1]);
            }else{
                $reports->whereIn('reports_orders.payment_flow_type', $request->payment_flow_type);
            }
        }

        if ($request->type_id) {
            $reports->whereIn('reports_orders.type_id', $request->type_id);
        }
        if ($request->accept_status) {
            $reports->whereIn('reports_orders.accept_status', $request->accept_status);
        }
        if ($request->report_month) {
            $reports->whereIn('reports_orders.report_month', $request->report_month);
        }

        if ($request->like_title && strlen($request->like_title) >= 3) {
            $reports->where('reports_orders.title', 'like', "%{$request->like_title}%");
        }

        if ($request->year) {
            $reports->where('reports_orders.report_year', '=', $request->year);
        }

        if($request->use_by_button == 1 && $request->reports_in_list){
            $reports->whereIn('reports_orders.id',  $request->reports_in_list);
            $reports_in_list = $request->reports_in_list;
        }else{
            $reports_in_list = [];
        }

        if ($request->motion && $request->motion > 0 && $request->motion < 3){
            if ($request->motion == 1){
                $reports->where('reports_orders.to_transfer_total', '>', 0);
            }elseif($request->motion == 2){
                $reports->where('reports_orders.to_return_total', '>', 0);
            }
        }

        $result['count'] = $reports->count();

        if ($request->page_count > 0) {
            $result['perpage'] = $request->page_count;
        }

        if($request->use_by_button == 1 && $request->use_by_button != $request->used_by_button){
            $result['page'] = 1;
        }elseif ($request->page > 0) {
            $result['page'] = $request->page;
        }

        $reports->offset($result['perpage'] * ($result['page'] - 1));
        $reports->limit($result['perpage']);

        $result['used_by_button'] = $request->use_by_button == 1 ? 1 : 0;

        $result['html'] = view('reports.advertising.list.reports_list_table', [
            'reports' => $reports->with('report_payment_sums')->get(),
            'reports_in_list' => $reports_in_list,
        ])->render();


        return $result;
    }

    public function recount_adverstising(Request $request){
        AdverstisingReports::recount_payments($request->adverstising_id);

        return reload();
    }
}
