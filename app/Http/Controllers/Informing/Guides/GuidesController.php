<?php

namespace App\Http\Controllers\Informing\Guides;

use App\Models\Guides\Guide;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GuidesController extends Controller
{


    public function index()
    {

        $guides = Guide::all();
        return view('informing.guides.editor.index', ['guides' => $guides]);
    }

    public function edit(Request $request ,$id =null)
    {

        $guides = Guide::all();
        if ($id)
            $guides = $guides->where('id', $id);
        if ($request->group)
            $guides = $guides->where('group_id', $request->group);


        return view('informing.guides.editor.edit', ['in_groups'=>Guide::getInGroups(),'guides' => $guides]);
    }

    public function guide($id = null)
    {
        $guides = Guide::all();
        if ($id)
            $guides = $guides->where('id', $id);
        return view('informing.guides.show.index', ['guides' => $guides]);
    }

    public function frame($id = null)
    {
        $guides = Guide::all();


        if ($id)
            $guides = $guides->where('id', $id);


        if(\request()->group)
            $guides = $guides->where('group_id', \request()->group);

        return view('informing.guides.show.frame', ['guides' => $guides]);
    }

    public function save(Request $request ,$id = null)
    {

        $guide = Guide::find($id);
        $guide->block_html = $request->html;
        $guide->block_title = $request->title;
        $guide->group_id = $request->group;
        $guide->save();

        return 'block_saved';
    }

    public function delete(Request $request){

        $guide = Guide::find($request->id);

        $guide->delete();

        return 'true';
    }
    public function create()
    {
        $guide = new Guide();
        $guide->save();
        return \GuzzleHttp\json_encode([
            'html'=> view('informing.guides.partials.guide_block', ['edit'=>'true','guides' =>  [$guide] ])->render(),
            'id'=> $guide->id
        ]);
    }
}
