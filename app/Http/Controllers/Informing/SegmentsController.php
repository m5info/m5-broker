<?php

namespace App\Http\Controllers\Informing;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Informing\Segments\InformingSegments;
use App\Repositories\FilesRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SegmentsController extends Controller
{

    protected $filesRepository;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $role = Auth::user()->role_id;

        $segments = InformingSegments::all();

        return view('informing.segments.index', [
            'role' => $role,
            'segments' => $segments
        ]);
    }

    /* Редактирование сегментов */

    public function edit_segments()
    {
        $segments = InformingSegments::all();

        return view('informing.segments.edit_segments.index', [
            'segments' => $segments
        ]);
    }

    public function edit_segment_form($segment_id)
    {
        $this->breadcrumbs[] = [
            'label' => 'Cегменты',
            'url' => 'informing/edit-segments'
        ];


        $segment = InformingSegments::query()->where('id', $segment_id)->first();

        return view('informing.segments.edit_segments.edit', [
            'segment' => $segment
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function edit_segment()
    {
        return view('informing.segments.edit_segments.edit');
    }

    public function update_file($segment_id, Request $request)
    {

        $segment = InformingSegments::query()->where('id', $segment_id)->first();

        $files_rep = new FilesRepository();

        $file = $files_rep->makeFile($request->file, InformingSegments::FILES_DOC . "/$segment_id/");

        $segment->file_id = $file->id;
        $segment->save();

    }

    public function delete_file($segment_id, $file_id)
    {

        $file = File::query()->where('id', $file_id)->first();

        \Storage::deleteDirectory(InformingSegments::FILES_DOC . '/' . $segment_id . '/');

        $segment = InformingSegments::query()->where('id', $segment_id)->first();
        $segment->file_id = 0;

        $segment->is_actual = 0;
        $segment->save();

        $file->delete();

    }

    public function save_segment()
    {
        $segment = new InformingSegments();
        $segment->is_actual = 0;
        $segment->save();

        return response(200);
    }

    public function update_segment($segment_id, Request $request)
    {

        if (isset($request->is_actual)) {

            $prev_actual_segment = InformingSegments::query()->where('is_actual', 1)->first();
            if ($prev_actual_segment) {
                $prev_actual_segment->is_actual = 0;
                $prev_actual_segment->save();
            }
            $segment = InformingSegments::query()->where('id', $segment_id)->first();
            if ($segment->file) {
                $segment->is_actual = 1;
                $segment->actual_from = date('Y-m-d H:i:s');
            }else{
                return redirect(url("/informing/edit-segments/".$segment_id))->with('error', 'Нельзя сделать актуальным, пока не добавлен файл.');
            }
        } else {
            $segment = InformingSegments::query()->where('id', $segment_id)->first();
            $segment->is_actual = 0;
        }

        $segment->save();

        return redirect(url("/informing/edit-segments/".$segment_id))->with('success', trans('form.success_update'));

    }

    public function delete_segment($segment_id)
    {
        $segment = InformingSegments::query()->where('id', $segment_id)->first();

        $file = File::query()->where('id', $segment->file_id)->first();

        if ($file) {

            \Storage::deleteDirectory(InformingSegments::FILES_DOC . '/' . $segment_id . '/');

            $file->delete();
        }

        $segment->delete();

        return response(200);

    }

}