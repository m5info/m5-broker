<?php

namespace App\Http\Controllers\Informing;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Informing\News\News;
use App\Models\Informing\News\NewsDocuments;
use App\Models\Informing\News\NewsRead;
use App\Models\User;
use App\Repositories\NewsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    protected $filesRepository;

    public function __construct(NewsRepository $filesRepository)
    {
        $this->filesRepository = $filesRepository;
        $this->middleware('auth');
    }

    public function index()
    {
        $role = Auth::user()->role_id;

        if (Auth::user()->is('manager') && Auth::user()->is('agent')){
            $news = News::query()->whereIn('visibility', [1,2,3])->get();
        }else if (Auth::user()->is('manager') && !Auth::user()->is('agent')){
            $news = News::query()->whereIn('visibility', [1,2])->get();
        }else if (Auth::user()->is('agent') && !Auth::user()->is('manager')){
            $news = News::query()->whereIn('visibility', [1,3])->get();
        }else{
            $news = News::query()->where('visibility', 1)->get();
        }

        return view('informing.news.index', [
            'role' => $role,
            'news' => $news
        ]);
    }

    public function show($id)
    {

        if (Auth::user()->is('manager') && Auth::user()->is('agent')){
            $new = News::query()
                ->whereIn('visibility', [1,2,3])
                ->where('id', $id)
                ->first();
        }else if (Auth::user()->is('manager') && !Auth::user()->is('agent')){
            $new = News::query()
                ->whereIn('visibility', [1,2])
                ->where('id', $id)
                ->first();
        }else if (Auth::user()->is('agent') && !Auth::user()->is('manager')){
            $new = News::query()
                ->whereIn('visibility', [1,3])
                ->where('id', $id)
                ->first();
        }else{
            $new = News::query()
                ->where('visibility', 1)
                ->where('id', $id)
                ->first();
        }

        if ($new == null){
            return redirect('informing');
        }

        $prev_url = \URL::previous();
        if (strripos($prev_url, 'informing')){
            $this->breadcrumbs[] = [
                'label' => 'Новости',
                'url' => 'informing/news/',
            ];
        }else{
            $this->breadcrumbs[] = [
                'label' => 'Главная',
                'url' => '',
            ];
        }



        $this->breadcrumbs[] = [
            'label' => $new->header,
        ];

        return view('informing.news.new', [
            'new' => $new
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function edit()
    {
        $news = News::query()->get();

        return view('informing.news.edit', [
            'news' => $news
        ]);
    }

    public function edit_new($id)
    {
        $this->breadcrumbs[] = [
            'label' => 'Редактирование новостей',
            'url' => 'informing/edit-news',
        ];

        $new = News::query()->where('id', $id)->first();

        $this->breadcrumbs[] = [
            'label' => $new->header,
        ];

        return view('informing.news.form', [
            'new' => $new
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function delete_file($new_id, $file_name)
    {
        $file = File::query()->where('name', $file_name)->first();

        NewsDocuments::query()->where('file_id', $file->id)->delete();

        $news_documents = NewsDocuments::query()->where('news_id', $new_id)->get();

        \Storage::delete(News::FILES_DOC . '/' . $new_id . '/'.$file->name.'.'.$file->ext);

        if ($news_documents->isEmpty()){
            \Storage::deleteDirectory(News::FILES_DOC . '/' . $new_id . '/');
        }

        $file->delete();

        return response('200');
    }

    public function update_file($new_id, Request $request)
    {

        $new = News::findOrFail($new_id);

        $file = $request->file("file");

        $file_id = $this->filesRepository->makeFile($file, NewsDocuments::FILES_DOC . "/$new->id/")->id;

        NewsDocuments::create([
            'news_id' => $new->id,
            'file_id' => $file_id,
        ]);

    }


    public function update($id)
    {
        $new = News::where('id', $id)->first();
        $new->header = request('header');
        $new->short_desc = request('short_desc');
        $new->visibility = request('visibility');
        $new->content = request('content');
        $new->save();
        if (\request('notify')){
            $this->notifyUsers($new);
        }
    }

    public function delete($id)
    {
        $new = News::query()->where('id', $id)->delete();
        $documents = NewsDocuments::query()->where('news_id', $id)->delete();
        \Storage::deleteDirectory(News::FILES_DOC . '/' . $id . '/');

        return response(200);
    }

    public function form_create()
    {
        $this->breadcrumbs[] = [
            'label' => 'Редактирование новостей',
            'url' => 'settings/news',
        ];

        $this->breadcrumbs[] = [
            'label' => 'Создание новой',
        ];
        return view('informing.news.create')->with('breadcrumbs', $this->breadcrumbs);
    }

    public function create()
    {
        $new = new News();
        $new->header = request('header');
        $new->short_desc = request('short_desc');
        $new->visibility = request('visibility');
        $new->content = request('content');
        $new->save();
        if (\request('notify')){
            $this->notifyUsers($new);
        }
    }

    /**
     *  Пользователь прочитал - уведомление не показывать
     */
    public function confirmRead()
    {
        User::findOrFail(\auth()->id())->update(['notification_news' => 1]);
    }

    /**
     * Включить оповещения для пользователей, разделяя по пермишенам
     */
    public function notifyUsers(News $news)
    {
        if ($news->visibility == 1){
            $users = User::query();
        }elseif ($news->visibility == 2){
            $users = User::getUsersHavePermissionLike('is_manager');
        }else{
            $users = User::getUsersHavePermissionLike('is_agent');
        }
        $users->where('id', '!=', \auth()->id());
        $users->update(['notification_news' => 0]);
    }

}