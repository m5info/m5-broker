<?php

namespace App\Http\Controllers\Informing;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Informing\Kv\InformingKv;
use App\Repositories\FilesRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KvController extends Controller
{

    protected $filesRepository;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $role = Auth::user()->role_id;

        $kvs = InformingKv::all();

        return view('informing.kv.index', [
            'role' => $role,
            'kvs' => $kvs
        ]);
    }

    /* Редактирование КВ */

    public function edit_kvs()
    {
        $kvs = InformingKv::all();

        return view('informing.kv.edit_kv.index', [
            'kvs' => $kvs
        ]);
    }

    public function edit_kv_form($kv_id)
    {
        $this->breadcrumbs[] = [
            'label' => 'Cегменты',
            'url' => 'informing/edit-kv'
        ];


        $kv = InformingKv::query()->where('id', $kv_id)->first();

        return view('informing.kv.edit_kv.edit', [
            'kv' => $kv
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function edit_kv()
    {
        return view('informing.kv.edit_kv.edit');
    }

    public function update_file($kv_id, Request $request)
    {

        $kv = InformingKv::query()->where('id', $kv_id)->first();

        $files_rep = new FilesRepository();

        $file = $files_rep->makeFile($request->file, InformingKv::FILES_DOC . "/$kv_id/");

        $kv->file_id = $file->id;
        $kv->save();

    }

    public function delete_file($kv_id, $file_id)
    {

        $file = File::query()->where('id', $file_id)->first();

        \Storage::deleteDirectory(InformingKv::FILES_DOC . '/' . $kv_id . '/');

        $kv = InformingKv::query()->where('id', $kv_id)->first();
        $kv->file_id = 0;

        $kv->is_actual = 0;
        $kv->save();

        $file->delete();

    }

    public function save_kv()
    {
        $kv = new InformingKv();
        $kv->is_actual = 0;
        $kv->save();

        return response(200);
    }

    public function update_kv($kv_id, Request $request)
    {

        if (isset($request->is_actual)) {

            $prev_actual_kv = InformingKv::query()->where('is_actual', 1)->first();
            if ($prev_actual_kv) {
                $prev_actual_kv->is_actual = 0;
                $prev_actual_kv->save();
            }

            $kv = InformingKv::query()->where('id', $kv_id)->first();
            if ($kv->file) {
                echo 'file_exist';
                $kv->is_actual = 1;
                $kv->actual_from = date('Y-m-d H:i:s');
            }else{
                echo 'file_not_exist';
                $kv->is_actual = 0;
                return redirect(url("/informing/edit-kv/".$kv_id))->with('error', 'Нельзя сделать актуальным, пока не добавлен файл.');
            }
        } else {
            $kv = InformingKv::query()->where('id', $kv_id)->first();
            echo 'is_actual_not_set';
            $kv->is_actual = 0;
        }

        $kv->save();

        return redirect(url("/informing/edit-kv/".$kv_id))->with('success', trans('form.success_update'));

    }

    public function delete_kv($kv_id)
    {
        $kv = InformingKv::query()->where('id', $kv_id)->first();

        $file = File::query()->where('id', $kv->file_id)->first();

        if ($file) {

            \Storage::deleteDirectory(InformingKv::FILES_DOC . '/' . $kv_id . '/');

            $file->delete();
        }

        $kv->delete();

        return response(200);

    }

}