<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Models\Settings\Notification;
use App\Models\User;
use App\Repositories\FilesRepository;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class ActionsController extends Controller {


	public function __construct() {


	}

	public function sendNotifications(Request $request)
    {

        Notification::sendNotificationsMsg($request->user_id, $request->msg);
        return response('', 200);
	}



}
