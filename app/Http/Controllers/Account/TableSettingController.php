<?php

namespace App\Http\Controllers\Account;

use App\Domain\Samplers\Contracts\TabsVisibility;
use App\Http\Controllers\Controller;
use App\Models\Account\TableColumn;
use Auth;
use DB;
use Illuminate\Http\Request;

class TableSettingController extends Controller
{

    private $table_key;
    private $user_columns;
    private $user_columns_all;
    private $other_columns_keys;
    private $table_columns;
    private $visibility;
    private $controller;

    public function edit($table_key)
    {
        $visibility = null;
        $controller = null;
        $this->table_key = $table_key;
        $this->controller = app(request('controller'));

        $this->user_columns = Auth::user()->columns()->get()
            ->where('table_key', $this->table_key)
            ->sortBy('pivot.orders');

        $this->user_columns_all = TableColumn::query()
            ->where('table_key', $this->table_key)
            ->get();

        $this->other_columns_keys = TableColumn::query()
            ->whereNotIn('column_key', $this->user_columns->pluck('column_key'))
            ->pluck('column_key');

        if (request('controller')) {
            if (request('controller') == 'App\Http\Controllers\Analitics\Common\CommonAnalyticsController') {
                $this->visibility = TabsVisibility::get_analytics_visibility();
            } elseif (\request('controller') == 'App\Http\Controllers\Reports\ReportsSK\ReportsSKFormationController') {
                $visibility_ = new \stdClass();
                $visibility_->title = 'all';
                $this->visibility = [
                    'all' => $visibility_
                ];
            }

            if (\request('in_detail')) {
                return $this->subpermissions_edit();
            } else {
                return $this->constants_edit();
            }

        }
    }

    private function constants_edit()
    {


        $this->table_columns = TableColumn::all()
            ->where('table_key', $this->table_key)
            ->whereIn('column_key', $this->other_columns_keys);

        $default_columns_off = $this->controller::DEFAULT_NOT_SELECTED_COLUMNS_OFF;
        $default_columns_on = $this->controller::DEFAULT_NOT_SELECTED_COLUMNS_ON;

        if ($this->user_columns->isEmpty()) {

            $default_columns_ = [];

            $i = 0;

            foreach ($default_columns_off as $def_key => $def_column) {

                foreach ($this->table_columns as $col) {
                    if ($def_column == $col['column_name']) {

                        $default_columns_[$i]['id'] = $col['id'];
                        $default_columns_[$i]['column_name'] = $col['column_name'];
                        $default_columns_[$i]['table_key'] = $col['table_key'];
                        $default_columns_[$i]['is_as'] = $col['is_as'];
                        $default_columns_[$i]['as_key'] = $col['as_key'];
                        $default_columns_[$i]['sorting'] = $col['sorting'];

                        break;
                    }
                }

                $i++;
            }

            $this->table_columns = $default_columns_;
            $default_columns_ = [];

            $i = 0;

            foreach ($default_columns_on as $def_key => $def_column) {

                foreach ($this->user_columns_all as $col) {

                    if ($def_column == $col['column_name']) {

                        $default_columns_[$i]['id'] = $col['id'];
                        $default_columns_[$i]['column_name'] = $col['column_name'];
                        $default_columns_[$i]['table_key'] = $col['table_key'];
                        $default_columns_[$i]['is_as'] = $col['is_as'];
                        $default_columns_[$i]['as_key'] = $col['as_key'];
                        $default_columns_[$i]['sorting'] = $col['sorting'];

                        break;
                    }
                }

                $i++;
            }

            $this->user_columns = $default_columns_;

        }

        return view('account.table_edit', [
            'controller' => $this->controller,
            'visibility' => $this->visibility,
            'user_columns' => $this->user_columns,
            'table_columns' => $this->table_columns,
            'table_key' => $this->table_key,
        ]);


    }

    private function subpermissions_edit()
    {
        $subpermissions = collect(Auth::user()->role->subpermissions)->pluck('title');
        $need_arr = [];

        foreach ($subpermissions as $sub){
            $need_arr[] = (int)str_replace($this->table_key.'_table_columns_key_', '', $sub);
        }

        $visible_columns = TableColumn::query()
            ->select('column_name', 'id')
            ->whereIn('id', $need_arr)
            ->get();

        $this->table_columns = TableColumn::all()
            ->where('table_key', $this->table_key)
            ->whereIn('column_key', $this->other_columns_keys)
            ->whereIn('id', collect($visible_columns)->keyBy('id')->keys());

        $visible_columns = collect($visible_columns)->keyBy('column_name')->keys()->toArray();

        $this->user_columns = Auth::user()->columns()->get()
            ->where('table_key', $this->table_key)
            ->whereNotIn('id', collect($visible_columns)->keyBy('id')->keys())
            ->sortBy('pivot.orders');

        return view('account.table_edit_in_detail', [
            'controller' => $this->controller,
            'table_columns' => $this->table_columns,
            'table_key' => $this->table_key,
            'visible_columns' => $visible_columns,
            'user_columns' => $this->user_columns
        ]);
    }

    public function save($table_key, Request $request)
    {
        $result = ['status' => 'error'];


        $user = Auth::user();
        $data = $request->all();

        $i = 0;

        foreach ($data as $column_id => $val) {

            $data[$column_id] = $i;
            $i++;

        }

        DB::query()->from('users2columns')
            ->join('table_columns', 'table_columns.id', 'users2columns.column_id')
            ->where('users2columns.user_id', $user->id)
            ->whereIn('table_columns.table_key', [$table_key, '_' . $table_key])->delete();

        $table_columns = TableColumn::all()
            ->whereIn('table_key', [$table_key, '_' . $table_key])
            ->whereIn('id', array_keys($data));

        foreach ($table_columns as $table_column) {
            $user->columns()->attach([
                $table_column->id => [
                    'orders' => $data[$table_column['id']],
                ]
            ]);
        }

        $result['status'] = 'ok';

        return response()->json($result);
    }


}