<?php

namespace App\Http\Controllers\BsoActs;

use App\Classes\Export\ExportManager;
use App\Classes\Export\Replacers\ExcelReplacer;
use App\Http\Controllers\Controller;
use App\Models\BSO\BsoActs;
use App\Models\Organizations\Organization;
use App\Models\Settings\ExportItem;
use App\Models\Settings\Template;
use function GuzzleHttp\Promise\queue;
use Illuminate\Http\Request;


class ShowBsoActController extends Controller
{


    public function __construct()
    {
        ///$this->middleware('permissions:bso_acts,inventory_agents');
    }


    public function index($id)
    {
        $act = BsoActs::getActId($id);

        if(!$act){
            return view('errors.403', ['exception'=>1]);
        }

        $sorted_logs = array_values($act->logs->groupBy("bso.org_id")->toArray());
        $counter_array = [];

        foreach($sorted_logs as $sorted_log){
            $counter_array[] = count($sorted_log);
        }

        // максималное кол-во организаций для отображения в селекте
        $org_id = 1;

        if(!empty($counter_array)){
            $max = array_keys($counter_array, max($counter_array))[0];
            $org_id = $sorted_logs[$max][0]['bso']['org_id'];
        }

        $main_org = Organization::find($org_id)->only('id', 'title');

        return view('bso_acts.show_bso_act.index', [
            "act" => $act,
            "main_org" => $main_org,
            "bso_table" => $this->get_bso_table($id)['html'],
        ]);

    }

    public function get_bso_table($id){
        $act = BsoActs::getActId($id);
        $data['html'] = view('bso_acts.show_bso_act.table.view', [
            "act" => $act,
        ])->render();
        return $data;
    }



    public function export($id){

        $builder = BsoActs::query()->where('id', $id);
        $act = $builder->firstOrFail();

        $category = $act->type->template_category();
        return (new ExportManager($category, $builder, null, true))->handle();
    }


    public function update($id){

        $this->validate(request(), ['act_org_id' => 'integer']);

        $result = ['status'=>'error'];

        if(BsoActs::query()->findOrFail($id)->update(['act_org_id' => (int)request('act_org_id')])){
            $result['status'] = 'ok';
        }

        return response()->json($result);


    }



}
