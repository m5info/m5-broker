<?php

namespace App\Http\Controllers\Directories\Delivery;

use App\Models\Delivery\DeliveryCost;
use App\Models\Directories\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeliveryController extends Controller
{
    public function index()
    {
        $arr_cost = DeliveryCost::all();
        return view('directories.delivery.index', ['costs' => $arr_cost]);
    }

    public function save(Request $request)
    {
        DeliveryCost::where('weekend', 0)->update(['delivery_cost' => (int)$request->work_day]);
        DeliveryCost::where('weekend', 1)->update(['delivery_cost' => (int)$request->weekend_day]);
        return json_encode(['status'=> 'ok']);
    }

    public function create(Request $request)
    {
        $products = Products::all();
        return view('directories.delivery.create', ['products' => $products]);
    }

    public function create_new(Request $request)
    {
        if(!isset($request->cost_new_product)){
            $new_delivery = new DeliveryCost();
            $new_delivery->product_id = $request->product_id;
            $new_delivery->delivery_price = getFloatFormat($request->delivery_price);
            $new_delivery->weekend_price = getFloatFormat($request->weekend_price);
            $new_delivery->is_actual = ( isset($request->is_actual) ) ? 1 : 0;
            $new_delivery->save();
        }else{
            $new_delivery = new DeliveryCost();
            $new_delivery->product_id = 0;
            $new_delivery->delivery_price = getFloatFormat($request->delivery_price);
            $new_delivery->weekend_price = getFloatFormat($request->weekend_price);
            $new_delivery->product_title = $request->product_title;
            $new_delivery->is_actual = ( isset($request->is_actual) ) ? 1 : 0;
            $new_delivery->save();
        }

        return parentReload();

    }

    public function edit($id)
    {
        $products = Products::all();
        return view('directories.delivery.edit', [
            'cost' => $city = DeliveryCost::findOrFail($id),
            'products' => $products
        ]);
    }
    public function delete($id)
    {
        DeliveryCost::findOrFail($id)->delete();

        return response('', 200);
    }

    public function update($id, Request $request)
    {

        $delivery = DeliveryCost::findOrFail($id);

        if(!isset($request->product_title)){
            $delivery->product_id = $request->product_id;
            $delivery->delivery_price = getFloatFormat($request->delivery_price);
            $delivery->weekend_price = getFloatFormat($request->weekend_price);
            $delivery->is_actual = ( isset($request->is_actual) ) ? 1 : 0;
            $delivery->save();
        }else{
            $delivery->product_id = 0;
            $delivery->delivery_price = getFloatFormat($request->delivery_price);
            $delivery->weekend_price = getFloatFormat($request->weekend_price);
            $delivery->product_title = $request->product_title;
            $delivery->is_actual = ( isset($request->is_actual) ) ? 1 : 0;
            $delivery->save();
        }

        return parentReload();
    }
}
