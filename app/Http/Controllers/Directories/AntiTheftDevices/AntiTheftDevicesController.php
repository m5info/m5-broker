<?php

namespace App\Http\Controllers\Directories\AntiTheftDevices;

use App\Http\Controllers\Controller;
use App\Models\Log\LogEvents;
use App\Models\Vehicle\VehicleAntiTheftSystem;
use Illuminate\Http\Request;

class AntiTheftDevicesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permissions:directories,anti-theft devices');
    }

    public function index()
    {
        return view('directories.anti_theft_devices.index', [
            'devices' => VehicleAntiTheftSystem::orderBy('title')->get()
        ]);
    }

    public function create()
    {
        return view('directories.anti_theft_devices.create');
    }

    public function edit($id)
    {
        return view('directories.anti_theft_devices.edit', [
            'device' => VehicleAntiTheftSystem::findOrFail($id)
        ]);
    }

    public function store(Request $request)
    {
        $category = new VehicleAntiTheftSystem;
        $category->save();
        LogEvents::event($category->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_CREATE], 6, 0,0, $request->all());

        return $this->save($category, $request);
    }

    public function update($id, Request $request)
    {
        $category = VehicleAntiTheftSystem::findOrFail($id);
        LogEvents::event($category->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_UPDATE], 6, 0,0, $request->all());
        return $this->save($category, $request);
    }

    private function save(VehicleAntiTheftSystem $category, Request $request)
    {
        $category->title = $request->title;
        $category->row = $request->row;
        $category->save();

        return parentReload();
        return redirect("/directories/anti_theft_devices/");
    }

    public function destroy($id)
    {
        LogEvents::event($id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_DELETE], 6);

        VehicleAntiTheftSystem::findOrFail($id)->delete();

        return response('', 200);
    }

}
