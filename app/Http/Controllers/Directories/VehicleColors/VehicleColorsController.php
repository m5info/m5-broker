<?php

namespace App\Http\Controllers\Directories\VehicleColors;

use App\Http\Controllers\Controller;
use App\Models\Log\LogEvents;
use App\Models\Vehicle\VehicleColor;
use Illuminate\Http\Request;

class VehicleColorsController extends Controller
{

    public function __construct()
    {
        $this->middleware('permissions:directories,vehicle_colors');
    }

    public function index()
    {
        return view('directories.vehicle_colors.index', [
            'colors' => VehicleColor::orderBy('title')->get()
        ]);
    }

    public function create()
    {
        return view('directories.vehicle_colors.create');
    }

    public function edit($id)
    {
        return view('directories.vehicle_colors.edit', [
            'color' => VehicleColor::findOrFail($id)
        ]);
    }

    public function store(Request $request)
    {
        $color = new VehicleColor;
        $color->save();
        LogEvents::event($color->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_CREATE], 6, 0,0, $request->all());

        return $this->save($color, $request);
    }

    public function update($id, Request $request)
    {
        $color = VehicleColor::findOrFail($id);
        LogEvents::event($color->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_UPDATE], 6, 0,0, $request->all());
        return $this->save($color, $request);
    }

    private function save(VehicleColor $color, Request $request)
    {
        $color->title = $request->title;
        $color->is_actual = isset($request->is_actual) ? 1 : 0;
        $color->save();

        return parentReload();
        return redirect("/directories/vehicle_colors/");
    }

    public function destroy($id)
    {
        LogEvents::event($id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_DELETE], 6);

        VehicleColor::findOrFail($id)->delete();

        return response('', 200);
    }

}
