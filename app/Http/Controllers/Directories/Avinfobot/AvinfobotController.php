<?php

namespace App\Http\Controllers\Directories\Avinfobot;

use App\Models\Delivery\DeliveryCost;
use App\Models\Directories\Products;
use App\Models\Vehicle\VehicleMarks;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AvinfobotController extends Controller
{
    public function index()
    {

        $remote = "https://data.av100.ru/profile.ashx?key=c39ff186-b98d-4e5c-b395-942228274136";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $remote, // Полный адрес метода
            CURLOPT_RETURNTRANSFER => true, // Возвращать ответ
            CURLOPT_POST => false, // Метод GET
            CURLOPT_USERAGENT => 'MyAgent',
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false
        ));
        $server_output = curl_exec($curl);
        curl_close ($curl);

        $data = json_decode($server_output);
        $result = [];
        if(!$data->error){
            $result = $data->result;
        }

        return view('directories.avinfobot.index', ['result' => $result]);
    }

    public function get_info(Request $request)
    {
        return json_encode($this->avinfo($request->all()));
    }

    public function avinfo($data)
    {
        $data = (object)$data;

        $type = $data->type;
        $request = $data->request;
/*
        $test = true;      //  для теста
        if($test){
            if($type == 'gosnomer'){
                // ГРЗ
                $server_output = '{"error":false,"result":{"gibdd":[{"car":"ХЕНДЭ ЭЛАНТРА","marka":"HYUNDAI","regdate":"15.10.2016","carYear":2016,"phone":"79057667566","vin":"XWEDB41CBH0000404","Family":"СЕЛИВАНОВА","Name":"НАТАЛЬЯ","Patronim":"ВАЛЕРЬЕВНА","Pts":"ОВ 261089 ОТ 26.06.2016","Sts":"5043 946385","bd":731376000,"vininfo":{"reghistory":{"ownerShipPeriod":[{"from":"15.10.2016","to":"01.01.0001","ownerType":"Физическое лицо","lastOperation":"регистрация новых, произведенных в России или везенных, а также ввезенных в Россию бывших в эксплуатации, в том числе временно на срок более 6 месяцев, испытательной техники."}],"requestDate":"29.11.2020 6:56:24","marka":"HYUNDAI","markaModel":"ХЕНДЭ ЭЛАНТРА","year":"2016","bodyNumber":"ХWЕDВ41СВН0000404","color":"СЕРЫЙ ТЕМНЫЙ","engineVolume":"1591.0","enginePower":"127.0","engineNumber":"GU153572","type":"Легковые автомобили прочие","pts":null},"dtp":{"accident":[{"type":"Наезд на стоящее ТС","date":"08.10.2017 18:10","region":"Московская область","marka":"HYUNDAI","model":"Elantra (Lantra, Avante)","schemeUrl":null},{"type":"Наезд на стоящее ТС","date":"26.10.2016 14:30","region":"Московская область","marka":"HYUNDAI","model":"Elantra (Lantra, Avante)","schemeUrl":"http://data.av100.ru/img.ashx?mode=sr\u0026id=388"}],"requestDate":"01.09.2020 6:14:23","inspectDt":1598940863,"hasDtp":true},"restrict":{"records":[],"requestDate":"30.08.2018 14:19:51","hasRestrict":false},"search":null,"decoding":{},"zalogreestrDate":"10.11.2019","zalogreestr":"Информация из реестра залогов по VIN \u003ccode\u003eXWEDB41CBH0000404\u003c/code\u003e:\r\nДата регистрации: 13.10.2016\r\nРегистрационный номер: 2016-000-513382-586(Прекращение залога 10.10.2019)\r\n","osago":""}}],"autonumber":[],"offer":[],"eaisto":[]}}';
            }else if($type == 'vin'){
                // VIN
                $server_output = '{"error":false,"result":{"vininfo":{"reghistory":{"ownerShipPeriod":[{"from":"15.10.2016","to":"01.01.0001","ownerType":"Физическое лицо","lastOperation":"регистрация новых, произведенных в России или везенных, а также ввезенных в Россию бывших в эксплуатации, в том числе временно на срок более 6 месяцев, испытательной техники."}],"requestDate":"29.11.2020 6:56:24","marka":"HYUNDAI","markaModel":"ХЕНДЭ ЭЛАНТРА","year":"2016","bodyNumber":"ХWЕDВ41СВН0000404","color":"СЕРЫЙ ТЕМНЫЙ","engineVolume":"1591.0","enginePower":"127.0","engineNumber":"GU153572","type":"Легковые автомобили прочие","pts":null},"dtp":{"accident":[{"type":"Наезд на стоящее ТС","date":"08.10.2017 18:10","region":"Московская область","marka":"HYUNDAI","model":"Elantra (Lantra, Avante)","schemeUrl":null},{"type":"Наезд на стоящее ТС","date":"26.10.2016 14:30","region":"Московская область","marka":"HYUNDAI","model":"Elantra (Lantra, Avante)","schemeUrl":"http://data.av100.ru/img.ashx?mode=sr\u0026id=388"}],"requestDate":"01.09.2020 6:14:23","inspectDt":1598940863,"hasDtp":true},"restrict":{"records":[],"requestDate":"30.08.2018 14:19:51","hasRestrict":false},"search":null,"decoding":{},"zalogreestrDate":"10.11.2019","zalogreestr":"Информация из реестра залогов по VIN \u003ccode\u003eXWEDB41CBH0000404\u003c/code\u003e:\r\nДата регистрации: 13.10.2016\r\nРегистрационный номер: 2016-000-513382-586(Прекращение залога 10.10.2019)\r\n","osago":""},"gibdd":[{"car":"ХЕНДЭ ЭЛАНТРА","regdate":"15.10.2016","carYear":2016,"phone":"79057667566","vin":"XWEDB41CBH0000404","Family":"СЕЛИВАНОВА","Name":"НАТАЛЬЯ","Patronim":"ВАЛЕРЬЕВНА","Pts":"ОВ 261089 ОТ 26.06.2016","Sts":"5043 946385","bd":731376000,"marka":"HYUNDAI"},{"car":"","regdate":"01.01.0001","carYear":0,"phone":"","vin":"XWEDB41CBH0000404","Family":"Селиванова","Name":"Наталья","Patronim":"Валерьевна","Pts":"","Sts":"","bd":731376000,"marka":"HYUNDAI"}],"autonumber":[],"offer":[],"eaisto":[{"vin":"XWEDB41CBH0000404","num":"079690021914122","gosnumber":"Х125МУ750","rama":"","date":1571011200,"dateexpire":1634256000,"kuzov":"XWEDB41CBH0000404","docname":"Диагностическая карта","markamodel":"HYUNDAI ELANTRA","year":-1,"marka":"HYUNDAI"}]},"times":{"full":"0,2 s","mark":"0,0 s","gibdd":"0,0 s","an":"0,0 s","offer":"0,0 s","eaisto":"0,0 s","generateResult":"0,1 s","vinGenerator":{"autoins":"0,0 s","autoinsPolice":"0,0 s","full":"0,1 s","gibddSr":"0,0 s","savevin":"0,0 s","decode":"0,1 s","zalog":"0,0 s"}}}';
            }else if($type == 'phone'){
                // Phone
                $server_output = '{"error":false,"result":{"request":{"phone":"79167042556","auto":[],"realty":[{"credate":"16.02.2016","title":"Дача 100 м на участке 12 сот.","contactface":"Илья","price":1300000,"city":"Сергиев Посад","source":"avito.ru"},{"credate":"17.01.2016","title":"Дача 100 м на участке 12 сот.","contactface":"Илья","price":1300000,"city":"Сергиев Посад","source":"avito.ru"},{"credate":"16.01.2016","title":"Дача 100 м на участке 12 сот.","contactface":"Илья","price":1300000,"city":"Сергиев Посад","source":"avito.ru"},{"credate":"05.12.2015","title":"Дача 100 м? на участке 12 сот.","contactface":"Илья","price":1300000,"city":"Сергиев Посад","source":"avito.ru"}],"other":[{"credate":"10.02.2020","title":"Музыкальные качели Capella TY-002B, цвет: бежевый","category":"Личные вещи,Товары для детей и игрушки","contactface":"Илья Федоров","price":3000,"city":"Москва","source":"avito.ru"},{"credate":"07.01.2020","title":"Видеокарта asus Mining P106-6G 6GB (P106-100)","category":"Бытовая электроника,Товары для компьютера","contactface":"Илья Федоров","price":10000,"city":"Москва","source":"avito.ru"},{"credate":"27.07.2020","title":"Детская коляска Teutonia Mistral P 12","category":"Личные вещи,Товары для детей и игрушки","contactface":"Илья Федоров","price":15000,"city":"Котельники","source":"avito.ru"}],"gibdd":[{"gosnumber":"С863КУ77","car":"ВОЛЬВО ХС70","marka":"","regdate":"29.10.2013","carYear":2006,"phone":"79167042556","vin":"","family":"ФЕДОРОВ","name":"ИЛЬЯ","patronim":"ДМИТРИЕВИЧ","pts":"","sts":"","bd":0},{"gosnumber":"С863КУ77","car":"ВОЛЬВО ХС70","marka":"VOLVO","regdate":"29.10.2013","carYear":2006,"phone":"79167042556","vin":"YV1SZ595761239235","family":"ФЕДОРОВ","name":"ИЛЬЯ","patronim":"ДМИТРИЕВИЧ","pts":"78 ТТ 013932 ОТ 05.05.2006","sts":"7713 325035","bd":558403200},{"gosnumber":"","car":"BMW 528","marka":"","regdate":"22.11.2010","carYear":1999,"phone":"79167042556","vin":"","family":"ФЕДОРОВ","name":"ИЛЬЯ","patronim":"ДМИТРИЕВИЧ","pts":"","sts":"","bd":0}]},"autorulinked":[],"avitorulinked":[],"dromrulinked":[],"phonenumberto":"","leaks":[{"phone":"79167042556","email":"","lastname":"Федоров","firstname":"Илья","middlename":"Дмитриевич","facebookid":"","instagramname":"","vkid":"","vkusername":""}]}}';
                $server_output = '{"error":false,"result":{"request":{"phone":"ХХХХХХХХХХ","auto":[{"credate":"31.12.2001","source":"auto.ru","marka":"FORD","model":"FOCUS","year":2011,"city":"Москва","distance":222001,"price":234000,"vin":"ХХХХХХХХХХХХХХХХХ","contactface":"Игорь","gosnomer":"А001АА11","images":"ссылка 1 на изображение на сайте-источнике, ссылка 2 на изображение на сайте-источнике","isfakephone":1}],"realty":[{"credate":"29.11.2016","title":"Дом 269 м на участке 11 сот.","contactface":"Александр","price":22234345,"city":"Москва","source":"avito.ru"}],"other":[{"credate":"31.01.2017","title":"Продам что-то","category":"Одеждаи обувь, Обувь","contactface":"Иван","price":234,"city":"Москва","source":"avito.ru"}],"gibdd":[{"gosnumber":"А001АА11","car":"ФОРД ФОКУС","regdate":"31.12.2001","carYear":2011,"phone":"ХХХХХХХХХХ","vin":"ХХХХХХХХХХХХХХХХХ","family":"Иванов","name":"Иван","patronim":"Иванович","pts":"ХХ 000000 ОТ 31.12.2001","sts":"0000 000000","bd":1538115732}]},"autorulinked":[{"phone":"ХХХХХХХХХХ","auto":[{"credate":"31.12.2001","source":"auto.ru","marka":"FORD","model":"FOCUS","year":2011,"city":"Москва","distance":222001,"price":234000,"vin":"ХХХХХХХХХХХХХХХХХ","contactface":"Игорь","gosnomer":"А001АА11","images":"ссылка 1 на изображение на сайте-источнике, ссылка 2 на изображение на сайте-источнике","isfakephone":1}],"realty":[{"credate":"29.11.2016","title":"Дом 269 м на участке 11 сот.","contactface":"Александр","price":22234345,"city":"Москва","source":"avito.ru"}],"other":[{"credate":"31.01.2017","title":"Продам что-то","category":"Одеждаи обувь, Обувь","contactface":"Иван","price":234,"city":"Москва","source":"avito.ru"}],"gibdd":[{"gosnumber":"А001АА11","car":"ФОРД ФОКУС","marka":"FORD","regdate":"31.12.2001","carYear":2011,"phone":"ХХХХХХХХХХ","vin":"ХХХХХХХХХХХХХХХХХ","family":"Иванов","name":"Иван","patronim":"Иванович","pts":"ХХ 000000 ОТ 31.12.2001","sts":"0000 000000","bd":1538115732}]}],"avitorulinked":[{"phone":"ХХХХХХХХХХ","auto":[{"credate":"31.12.2001","source":"auto.ru","marka":"FORD","model":"FOCUS","year":2011,"city":"Москва","distance":222001,"price":234000,"vin":"ХХХХХХХХХХХХХХХХХ","contactface":"Игорь","gosnomer":"А001АА11","images":"ссылка 1 на изображение на сайте-источнике, ссылка 2 на изображение на сайте-источнике","isfakephone":1}],"realty":[{"credate":"29.11.2016","title":"Дом 269 м на участке 11 сот.","contactface":"Александр","price":22234345,"city":"Москва","source":"avito.ru"}],"other":[{"credate":"31.01.2017","title":"Продам что-то","category":"Одеждаи обувь, Обувь","contactface":"Иван","price":234,"city":"Москва","source":"avito.ru"}],"gibdd":[{"gosnumber":"А001АА11","car":"ФОРД ФОКУС","marka":"FORD","regdate":"31.12.2001","carYear":2011,"phone":"ХХХХХХХХХХ","vin":"ХХХХХХХХХХХХХХХХХ","family":"Иванов","name":"Иван","patronim":"Иванович","pts":"ХХ 000000 ОТ 31.12.2001","sts":"0000 000000","bd":1538115732}]}],"dromrulinked":[{"phone":"ХХХХХХХХХХ","auto":[{"credate":"31.12.2001","source":"auto.ru","marka":"FORD","model":"FOCUS","year":2011,"city":"Москва","distance":222001,"price":234000,"vin":"ХХХХХХХХХХХХХХХХХ","contactface":"Игорь","gosnomer":"А001АА11","images":"ссылка 1 на изображение на сайте-источнике, ссылка 2 на изображение на сайте-источнике","isfakephone":1}],"realty":[{"credate":"29.11.2016","title":"Дом 269 м на участке 11 сот.","contactface":"Александр","price":22234345,"city":"Москва","source":"avito.ru"}],"other":[{"credate":"31.01.2017","title":"Продам что-то","category":"Одеждаи обувь, Обувь","contactface":"Иван","price":234,"city":"Москва","source":"avito.ru"}],"gibdd":[{"gosnumber":"А001АА11","car":"ФОРД ФОКУС","marka":"FORD","regdate":"31.12.2001","carYear":2011,"phone":"ХХХХХХХХХХ","vin":"ХХХХХХХХХХХХХХХХХ","family":"Иванов","name":"Иван","patronim":"Иванович","pts":"ХХ 000000 ОТ 31.12.2001","sts":"0000 000000","bd":1538115732}]}],"phonenumberto":"ФИО: ИВАНОВ ИВАН; Номер телефона: +ХХХХХХХХХ; Адрес: г. Москва и Московская область, Россия; Тип телефона: Сотовый; Оператор: ПАО \"МегаФон\"; "}}';
            }

            if($type == 'phone'){
                $result = $this->filterDataForPhone(json_decode($server_output, true));
                return $result;
            }

            $result = $this->filterData(json_decode($server_output, true), $type);
            return $result;
        }
*/
        $remote = "https://data.av100.ru/api.ashx?key=c39ff186-b98d-4e5c-b395-942228274136&".$type."=".$request;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $remote, // Полный адрес метода
            CURLOPT_RETURNTRANSFER => true, // Возвращать ответ
            CURLOPT_POST => false, // Метод GET
            CURLOPT_USERAGENT => 'MyAgent',
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false
        ));
        $server_output = curl_exec($curl);
        curl_close ($curl);

        if($type == 'phone'){
            $result = $this->filterDataForPhone(json_decode($server_output, true));
            return $result;
        }

        $result = $this->filterData(json_decode($server_output, true), $type);
        return $result;

    }

    public function filterData($object, $type)
    {
        //базовая проверка на ошибки
        if(!$object || $object['error'] || !isset($object['result']))
            return false;

        $Marka = $this->getAvBrand($object, $type);

        $Model = $this->getAvModel($object, $type);

        /** ВЛАДЕЛЕЦ */

        $Family = '';       //  Фамилия
        $Name = '';     //  Имя
        $NameTwo = '';      //  Отчество
        $Phone = '';        //  Телефон

        /** АВТО */

        $Car = '';      //  Русское название
        $VIN = '';      //  VIN
        $GRZ = '';      // Номер авто
        // подставить гос номер из запроса и дальше сравнить (может понадобится проверка на валидность)
        $Year = '';     //  Год выпуска
        $PTS = '';      //  ПТС не всегда правильная
        $PTS_region = '';       //  ПТС с регионом (правильная, не всегда есть)
        $PTS_date = '';     //  Дата выдвчи ПТС
        $STS = '';      //  СТС
        $STS_date = '';     //  Дата выдачи СТС
        $Power = '';        //  Мощность л.с.
        $Volume = '';       // Объем двигателя
        $NumDiag = '';      // Номер диагностической карты
        $DiagDate = '';     //  Когда делали диагностику
        $DiagDateExpire = '';     //  Когда заканчивается диагностика

        $PeriodVladeniya = [];      // Регистрационные действия
        $DTP = $this->getDTP($object, $type);;      // ДТП


        $objGIBDD = $object['result']['gibdd'];
        $objEAISTO = $object['result']['eaisto'];
        if($objGIBDD != NULL) {

            foreach ($objGIBDD as $key => $val) {

                if (strcmp($val['car'], '') != 0 && strlen($Car) == 0) {
                    $Car = $val['car'];
                }
                if (strcmp($val['Family'], '') != 0 && strlen($Family) == 0) {
                    $Family = $val['Family'];
                }
                if (strcmp($val['Name'], '') != 0 && strlen($Name) == 0) {
                    $Name = $val['Name'];
                }
                if (strcmp($val['Patronim'], '') != 0 && strlen($NameTwo) == 0) {
                    $NameTwo = $val['Patronim'];
                }
                if (strcmp($val['phone'], '') != 0 && strlen($Phone) == 0) {
                    $Phone = $val['phone'];
                }
                if (strcmp($val['vin'], '') != 0 && strlen($VIN) == 0) {
                    $VIN = $val['vin'];
                }
                if (strcmp($val['carYear'], '') != 0 && strlen($Year) == 0) {
                    $Year = $val['carYear'];
                }
                if (strcmp($val['Pts'], '') != 0 && strlen($PTS) == 0) {
                    $PTS = $val['Pts'];
                }
                if (strcmp($val['Sts'], '') != 0 && strlen($STS) == 0) {
                    $STS = $val['Sts'];
                }

                if($type == 'gosnomer'){
                    if ($val['vininfo'] != NULL) {

                        if (strcmp($val['vininfo']['reghistory']['year'], '') != 0 && ($val['vininfo']['reghistory']['year'] != -1)  && strlen($Year) == 0) {
                            $Year = $val['vininfo']['reghistory']['year'];
                        }
                        if (strcmp($val['vininfo']['reghistory']['enginePower'], '') != 0 && strlen($Power) == 0) {
                            $Power = $val['vininfo']['reghistory']['enginePower'];
                        }
                        if (strcmp($val['vininfo']['reghistory']['engineVolume'], '') != 0 && strlen($Volume) == 0) {
                            $Volume = $val['vininfo']['reghistory']['engineVolume'];
                        }
                        if (strcmp($val['vininfo']['reghistory']['pts'], '') != 0 && $val['vininfo']['reghistory']['pts'] != NULL && strlen($PTS_region) == 0) {
                            $PTS_region = $val['vininfo']['reghistory']['pts'];
                        }

                        foreach ($val['vininfo']['reghistory']['ownerShipPeriod'] as $ownerShipPeriod){
                            $PeriodVladeniya[] = [
                                'DateStartOwnerShip' => $ownerShipPeriod['from'],
                                'DateEndOwnerShip' => $ownerShipPeriod['to'],
                                'OwnerType' => $ownerShipPeriod['ownerType'],
                                'Oparetion' => $ownerShipPeriod['lastOperation']
                            ];
                        }
                    }
                }
            }

            if($type == 'vin'){     // запрос по vin
                $val = $object['result'];
                if ($val['vininfo'] != NULL) {

                    if (strcmp($val['vininfo']['reghistory']['year'], '') != 0 && ($val['vininfo']['reghistory']['year'] != -1)  && strlen($Year) == 0) {
                        $Year = $val['vininfo']['reghistory']['year'];
                    }
                    if (strcmp($val['vininfo']['reghistory']['enginePower'], '') != 0 && strlen($Power) == 0) {
                        $Power = $val['vininfo']['reghistory']['enginePower'];
                    }
                    if (strcmp($val['vininfo']['reghistory']['engineVolume'], '') != 0 && strlen($Volume) == 0) {
                        $Volume = $val['vininfo']['reghistory']['engineVolume'];
                    }
                    if (strcmp($val['vininfo']['reghistory']['pts'], '') != 0 && $val['vininfo']['reghistory']['pts'] != NULL && strlen($PTS_region) == 0) {
                        $PTS_region = $val['vininfo']['reghistory']['pts'];
                    }

                    foreach ($val['vininfo']['reghistory']['ownerShipPeriod'] as $ownerShipPeriod){
                        $PeriodVladeniya[] = [
                            'DateStartOwnerShip' => $ownerShipPeriod['from'],
                            'DateEndOwnerShip' => $ownerShipPeriod['to'],
                            'OwnerType' => $ownerShipPeriod['ownerType'],
                            'Oparetion' => $ownerShipPeriod['lastOperation']
                        ];
                    }
                }
            }
        }

        if($objEAISTO != NULL) {
            foreach ($objEAISTO as $key => $val) {

                if (strcmp($val['vin'], '') != 0 && strlen($VIN) == 0)
                    $VIN = $val['vin'];

                if (strcmp($val['year'], '') != 0 && ($val['year'] != -1) && strlen($Year) == 0)
                    $Year = $val['year'];

                if(strlen($NumDiag) == 0)
                    $NumDiag = $val['num'];

                if(strlen($DiagDate) == 0)
                    $DiagDate = date('d.m.Y', $val['date']);

                if(strlen($DiagDateExpire) == 0)
                    $DiagDateExpire = date('d.m.Y', $val['dateexpire']);

                $GRZ = $val['gosnumber'];  // тут проверить
            }
        }

        /** ЗАПОЛНЕНИЕ ДАННЫХ */

        $Model = strtoupper($Model);        //  Поднимаем буквы в модели авто
        if(strlen($PTS) > 12){
            $PTS_date = substr($PTS, -10);      // получаем дату выдачи птс
        }
        $PTS_Serial = substr($PTS_region, 0, 6);        // получаем серию ПТС
        $PTS_Number = substr($PTS_region, 6, 6);        //  Получаем номер ПТС
        $PTS_Full = $PTS_Serial.' '.$PTS_Number;        //  Клеим Серию и номер
        //$PTS_Full = substr_replace($PTS_region, ' ', 6, 0);       //  Если не нужно по отельности серия и номер раскоментировать. три верхнии закоментировать.


        $INFO = new \stdClass();

        /**  СОБСТВЕННИК */

        $INFO->family = $Family;
        $INFO->name = $Name;
        $INFO->nametwo = $NameTwo;
        $INFO->phone = $Phone;

        /**  АВТО */

        $INFO->car = $Car;
        $INFO->vin = $VIN;
        $INFO->grz = $GRZ;
        $INFO->mark = $Marka;
        $INFO->model = $Model;
        $INFO->year = $Year;
        $INFO->pts_serial = $PTS_Serial;
        $INFO->pts_number = $PTS_Number;
        $INFO->docserie = $PTS_Full;
        $INFO->pts_date = $PTS_date;
        $INFO->sts = $STS;
        $INFO->sts_date = $STS_date;
        $INFO->power = $Power == 0 ? '' : $Power;
        $INFO->volume = $Volume;
        $INFO->dk_number = $NumDiag;
        $INFO->dk_date_from = $DiagDate;
        $INFO->dk_date = $DiagDateExpire;
        $INFO->period_vladeniya = $PeriodVladeniya;
        $INFO->dtp = $DTP;

        return $INFO;  // вернуть объект

    }

    public function filterDataForPhone($object)
    {
        //базовая проверка на ошибки
        if(!$object || $object['error'] || !isset($object['result']))
            return 'No info';

        $AllInfo = $this->getGeneralInfo($object);

        return $AllInfo;

    }

    public function getDTP($object, $type){
        $DTP_1 = [];

        if($type == 'gosnomer'){
            foreach ($object['result']['gibdd'] as $key => $val) {
                if ($val['vininfo']['dtp']['hasDtp']) {
                    $dtp = $val['vininfo']['dtp']['accident'];
                    foreach ($dtp as $key2 => $val2) {
                        $DTP_1[] = [
                            'type' =>   $val2['type'],
                            'date' =>   $val2['date'],
                            'region' =>   $val2['region'],
                            'schemeUrl' =>   ( $val2['schemeUrl'] != null ) ? $val2['schemeUrl'] : '',
                        ];
                    }
                }
            }
        }else if($type == 'vin'){
            if ($object['result']['vininfo']['dtp']['hasDtp']) {
                $dtp = $object['result']['vininfo']['dtp']['accident'];
                foreach ($dtp as $key2 => $val2) {
                    $DTP_1[] = [
                        'type' =>   $val2['type'],
                        'date' =>   $val2['date'],
                        'region' =>   $val2['region'],
                        'schemeUrl' =>   ( $val2['schemeUrl'] != null ) ? $val2['schemeUrl'] : '',
                    ];
                }
            }
        }

        return $DTP_1;
    }

    public function getAvBrand($object, $type)
    {
        //где может быть?
        $result = false;

        foreach ($object['result']['eaisto'] as $key => $val) {
            if(isset($val['marka']) && $this->isEnglishName($val['marka'])) {
                $result = $val['marka'];//не всегда на верную тачку
                break;
            }
        }

        if($type == 'gosnomer'){
            foreach ($object['result']['gibdd'] as $key => $val) {
                if (isset($val['vininfo']['reghistory']['marka']) && $this->isEnglishName($val['vininfo']['reghistory']['marka'])) {
                    $result = $val['vininfo']['reghistory']['marka'];
                    break;
                }
            }

            foreach ($object['result']['gibdd'] as $key => $val) {
                if ($val['vininfo']['dtp']['hasDtp']) {
                    $dtp = $val['vininfo']['dtp']['accident'];
                    foreach ($dtp as $key2 => $val2) {
                        if (isset($val2['marka']) && $this->isEnglishName($val2['marka'])) {
                            $result = $val2['marka'];
                            break;
                        }
                    }
                }
            }
        }elseif($type == 'vin'){
            if (isset($object['result']['vininfo']['reghistory']['marka']) && $this->isEnglishName($object['result']['vininfo']['reghistory']['marka'])) {
                $result = $object['result']['vininfo']['reghistory']['marka'];
            }

            if ($object['result']['vininfo']['dtp']['hasDtp']) {
                $dtp = $object['result']['vininfo']['dtp']['accident'];
                foreach ($dtp as $key2 => $val2) {
                    if (isset($val2['marka']) && $this->isEnglishName($val2['marka'])) {
                        $result = $val2['marka'];
                    }
                }
            }
        }

        foreach ($object['result']['offer'] as $key => $val) {
            if (isset($val['marka']) && $this->isEnglishName($val['marka'])) {
                $result = $val['marka'];
                break;
            }
        }

        foreach ($object['result']['gibdd'] as $key => $val) {
            if (isset($val['marka']) && $this->isEnglishName($val['marka'])) {
                $result = $val['marka'];//самый надежный результат
                break;
            }
        }

        return $result;

    }

    public function getAvModel($object, $type)
    {
        $result = false;

        foreach ($object['result']['offer'] as $key => $val) {
            if (isset($val['model']) && $this->isEnglishName($val['model'])) {
                $result = $val['model'];
                break;
            }
        }

        if($type == 'gosnomer'){
            foreach ($object['result']['gibdd'] as $key => $val) {
                if ($val['vininfo']['dtp']['hasDtp']) {
                    $dtp = $val['vininfo']['dtp']['accident'];
                    foreach ($dtp as $key2 => $val2) {
                        if (isset($val2['model']) && $this->isEnglishName($val2['model'])) {
                            $result = $val2['model'];
                            break;
                        }
                    }
                }
            }

            foreach ($object['result']['gibdd'] as $key => $val) {
                if ($val['vininfo']['dtp']['hasDtp']) {
                    $dtp = $val['vininfo']['dtp']['accident'];
                    foreach ($dtp as $key2 => $val2) {
                        if (isset($val2['model']) && $this->isEnglishName($val2['model'])) {
                            $result = $val2['model'];
                            break;
                        }
                    }
                }
            }

            foreach ($object['result']['gibdd'] as $key => $val) {
                if (isset($val['vininfo']['decoding']['Модель']) && $this->isEnglishName($val['vininfo']['decoding']['Модель'])) {
                    $result = $val['vininfo']['decoding']['Модель'];
                    break;
                }
            }

        }elseif($type == 'vin'){
            if ($object['result']['vininfo']['dtp']['hasDtp']) {
                $dtp = $object['result']['vininfo']['dtp']['accident'];
                foreach ($dtp as $key2 => $val2) {
                    if (isset($val2['model']) && $this->isEnglishName($val2['model'])) {
                        $result = $val2['model'];
                    }
                }
            }

            if ($object['result']['vininfo']['dtp']['hasDtp']) {
                $dtp = $object['result']['vininfo']['dtp']['accident'];
                foreach ($dtp as $key2 => $val2) {
                    if (isset($val2['model']) && $this->isEnglishName($val2['model'])) {
                        $result = $val2['model'];
                    }
                }
            }

            if (isset($object['result']['vininfo']['decoding']['Модель']) && $this->isEnglishName($object['result']['vininfo']['decoding']['Модель'])) {
                $result = $object['result']['vininfo']['decoding']['Модель'];
            }
        }

        return $result;
    }

    public function getGeneralInfo($object)
    {
        $object = $object['result'];

        foreach ($object as $key => $item) {
            if(is_array($item) && is_string($key) && $key === 'request'){
                foreach ($item as $key_2 => $item_2) {
                    if(isset($key_2) && is_string($key_2) && $key_2 === 'phone'){
                        echo 'Телефон: '.$item_2.'<br>';
                    }else if(isset($key_2) && is_string($key_2) && $key_2 === 'auto'){
                        echo '<br><br>АВТО<br><br>';
                        if(is_array($item_2) && sizeof($item_2)){
                            foreach ($item_2 as $key_3 => $item_3){
                                if(is_array($item_3)){
                                    foreach ($item_3 as $key_4 => $item_4) {
                                        if(isset($key_4) && $key_4 == 'credate'){
                                            echo 'Дата создания: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'source'){
                                            echo 'Источник: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'marka'){
                                            echo 'Марка: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'model'){
                                            echo 'Модель: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'year'){
                                            echo 'Год: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'city'){
                                            echo 'Город: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'distance'){
                                            echo 'Пробег: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'price'){
                                            echo 'Цена: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'vin'){
                                            echo 'VIN: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'contactface'){
                                            echo 'Контактное лицо: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'gosnomer'){
                                            echo 'Госномер: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'images'){
                                            echo 'Ссылка на картинку: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'isfakephone'){
                                            echo 'Фейк номер: '.$item_4.'<br>';
                                        }
                                    }
                                }
                            }
                        }else{
                            echo 'Нет информации!<br>';
                        }
                    }else if(isset($key_2) && is_string($key_2) && $key_2 === 'realty'){
                        echo '<br><br>НЕДВИЖИМОСТЬ<br><br>';
                        if(is_array($item_2) && sizeof($item_2)){
                            foreach ($item_2 as $key_3 => $item_3){
                                if(is_array($item_3)){
                                    foreach ($item_3 as $key_4 => $item_4) {
                                        if(isset($key_4) && $key_4 == 'credate'){
                                            echo 'Дата создания: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'source'){
                                            echo 'Источник: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'title'){
                                            echo 'Объект: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'city'){
                                            echo 'Город: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'price'){
                                            echo 'Цена: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'contactface'){
                                            echo 'Контактное лицо: '.$item_4.'<br>';
                                        }
                                    }
                                }
                            }
                        }else{
                            echo 'Нет информации!<br>';
                        }
                    }else if(isset($key_2) && is_string($key_2) && $key_2 === 'other'){
                        echo '<br><br>ИНОЕ<br><br>';
                        if(is_array($item_2) && sizeof($item_2)){
                            foreach ($item_2 as $key_3 => $item_3){
                                if(is_array($item_3)){
                                    foreach ($item_3 as $key_4 => $item_4) {
                                        if(isset($key_4) && $key_4 == 'credate'){
                                            echo 'Дата создания: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'source'){
                                            echo 'Источник: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'title'){
                                            echo 'Объект: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'city'){
                                            echo 'Город: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'price'){
                                            echo 'Цена: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'contactface'){
                                            echo 'Контактное лицо: '.$item_4.'<br>';
                                        }
                                    }
                                }
                            }
                        }else{
                            echo 'Нет информации!<br>';
                        }
                    }else if(isset($key_2) && is_string($key_2) && $key_2 === 'gibdd'){
                        echo '<br><br>ГИБДД<br><br>';
                        if(is_array($item_2) && sizeof($item_2)){
                            foreach ($item_2 as $key_3 => $item_3){
                                if(is_array($item_3)){
                                    foreach ($item_3 as $key_4 => $item_4) {
                                        if(isset($key_4) && $key_4 == 'gosnumber'){
                                            echo 'Госномер: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'car'){
                                            echo 'Авто: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'regdate'){
                                            echo 'Дата регистрации: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'carYear'){
                                            echo 'Год: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'phone'){
                                            echo 'Телефон: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'vin'){
                                            echo 'VIN: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'family'){
                                            echo 'Фамилия: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'name'){
                                            echo 'Имя: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'patronim'){
                                            echo 'Отчетсво: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'pts'){
                                            echo 'ПТС: '.$item_4.'<br>';
                                        }
                                        if(isset($key_4) && $key_4 == 'sts'){
                                            echo 'СТС: '.$item_4.'<br>';
                                        }
                                    }
                                }
                            }
                        }else{
                            echo 'Нет информации!<br>';
                        }
                    }
                }
            }else if (is_array($item) && is_string($key) && $key === 'autorulinked'){
                echo '<br><br>ИНФО ИЗ AUTO.RU<br><br>';
                if(sizeof($item)){
                    foreach ($item as $key_1 => $item_1) {
                        foreach ($item_1 as $key_2 => $item_2) {
                            if(isset($key_2) && is_string($key_2) && $key_2 === 'phone'){
                                echo 'Телефон: '.$item_2.'<br>';
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'auto'){
                                echo '<br><br>АВТО<br><br>';
                                if(is_array($item_2)){
                                    foreach ($item_2 as $key_3 => $item_3){
                                        if(is_array($item_3)){
                                            foreach ($item_3 as $key_4 => $item_4) {
                                                if(isset($key_4) && $key_4 == 'credate'){
                                                    echo 'Дата создания: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'source'){
                                                    echo 'Источник: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'marka'){
                                                    echo 'Марка: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'model'){
                                                    echo 'Модель: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'year'){
                                                    echo 'Год: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'city'){
                                                    echo 'Город: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'distance'){
                                                    echo 'Пробег: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'price'){
                                                    echo 'Цена: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'vin'){
                                                    echo 'VIN: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'contactface'){
                                                    echo 'Контактное лицо: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'gosnomer'){
                                                    echo 'Госномер: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'images'){
                                                    echo 'Ссылка на картинку: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'isfakephone'){
                                                    echo 'Фейк номер: '.$item_4.'<br>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'realty'){
                                echo '<br><br>НЕДВИЖИМОСТЬ<br><br>';
                                if(is_array($item_2)){
                                    foreach ($item_2 as $key_3 => $item_3){
                                        if(is_array($item_3)){
                                            foreach ($item_3 as $key_4 => $item_4) {
                                                if(isset($key_4) && $key_4 == 'credate'){
                                                    echo 'Дата создания: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'source'){
                                                    echo 'Источник: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'title'){
                                                    echo 'Объект: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'city'){
                                                    echo 'Город: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'price'){
                                                    echo 'Цена: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'contactface'){
                                                    echo 'Контактное лицо: '.$item_4.'<br>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'other'){
                                echo '<br><br>ИНОЕ<br><br>';
                                if(is_array($item_2)){
                                    foreach ($item_2 as $key_3 => $item_3){
                                        if(is_array($item_3)){
                                            foreach ($item_3 as $key_4 => $item_4) {
                                                if(isset($key_4) && $key_4 == 'credate'){
                                                    echo 'Дата создания: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'source'){
                                                    echo 'Источник: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'title'){
                                                    echo 'Объект: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'city'){
                                                    echo 'Город: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'price'){
                                                    echo 'Цена: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'contactface'){
                                                    echo 'Контактное лицо: '.$item_4.'<br>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'gibdd'){
                                echo '<br><br>ГИБДД<br><br>';
                                if(is_array($item_2)){
                                    foreach ($item_2 as $key_3 => $item_3){
                                        if(is_array($item_3)){
                                            foreach ($item_3 as $key_4 => $item_4) {
                                                if(isset($key_4) && $key_4 == 'gosnumber'){
                                                    echo 'Госномер: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'car'){
                                                    echo 'Авто: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'regdate'){
                                                    echo 'Дата регистрации: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'carYear'){
                                                    echo 'Год: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'phone'){
                                                    echo 'Телефон: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'vin'){
                                                    echo 'VIN: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'family'){
                                                    echo 'Фамилия: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'name'){
                                                    echo 'Имя: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'patronim'){
                                                    echo 'Отчетсво: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'pts'){
                                                    echo 'ПТС: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'sts'){
                                                    echo 'СТС: '.$item_4.'<br>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    echo 'Нет информации!<br>';
                }

            }else if (is_array($item) && is_string($key) && $key === 'avitorulinked'){
                echo '<br><br>ИНФО ИЗ AVITO.RU<br><br>';
                if(sizeof($item)){
                    foreach ($item as $key_1 => $item_1) {
                        foreach ($item_1 as $key_2 => $item_2) {
                            if(isset($key_2) && is_string($key_2) && $key_2 === 'phone'){
                                echo 'Телефон: '.$item_2.'<br>';
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'auto'){
                                echo '<br><br>АВТО<br><br>';
                                if(is_array($item_2)){
                                    foreach ($item_2 as $key_3 => $item_3){
                                        if(is_array($item_3)){
                                            foreach ($item_3 as $key_4 => $item_4) {
                                                if(isset($key_4) && $key_4 == 'credate'){
                                                    echo 'Дата создания: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'source'){
                                                    echo 'Источник: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'marka'){
                                                    echo 'Марка: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'model'){
                                                    echo 'Модель: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'year'){
                                                    echo 'Год: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'city'){
                                                    echo 'Город: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'distance'){
                                                    echo 'Пробег: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'price'){
                                                    echo 'Цена: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'vin'){
                                                    echo 'VIN: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'contactface'){
                                                    echo 'Контактное лицо: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'gosnomer'){
                                                    echo 'Госномер: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'images'){
                                                    echo 'Ссылка на картинку: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'isfakephone'){
                                                    echo 'Фейк номер: '.$item_4.'<br>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'realty'){
                                echo '<br><br>НЕДВИЖИМОСТЬ<br><br>';
                                if(is_array($item_2)){
                                    foreach ($item_2 as $key_3 => $item_3){
                                        if(is_array($item_3)){
                                            foreach ($item_3 as $key_4 => $item_4) {
                                                if(isset($key_4) && $key_4 == 'credate'){
                                                    echo 'Дата создания: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'source'){
                                                    echo 'Источник: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'title'){
                                                    echo 'Объект: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'city'){
                                                    echo 'Город: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'price'){
                                                    echo 'Цена: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'contactface'){
                                                    echo 'Контактное лицо: '.$item_4.'<br>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'other'){
                                echo '<br><br>ИНОЕ<br><br>';
                                if(is_array($item_2)){
                                    foreach ($item_2 as $key_3 => $item_3){
                                        if(is_array($item_3)){
                                            foreach ($item_3 as $key_4 => $item_4) {
                                                if(isset($key_4) && $key_4 == 'credate'){
                                                    echo 'Дата создания: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'source'){
                                                    echo 'Источник: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'title'){
                                                    echo 'Объект: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'city'){
                                                    echo 'Город: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'price'){
                                                    echo 'Цена: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'contactface'){
                                                    echo 'Контактное лицо: '.$item_4.'<br>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'gibdd'){
                                echo '<br><br>ГИБДД<br><br>';
                                if(is_array($item_2)){
                                    foreach ($item_2 as $key_3 => $item_3){
                                        if(is_array($item_3)){
                                            foreach ($item_3 as $key_4 => $item_4) {
                                                if(isset($key_4) && $key_4 == 'gosnumber'){
                                                    echo 'Госномер: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'car'){
                                                    echo 'Авто: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'regdate'){
                                                    echo 'Дата регистрации: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'carYear'){
                                                    echo 'Год: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'phone'){
                                                    echo 'Телефон: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'vin'){
                                                    echo 'VIN: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'family'){
                                                    echo 'Фамилия: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'name'){
                                                    echo 'Имя: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'patronim'){
                                                    echo 'Отчетсво: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'pts'){
                                                    echo 'ПТС: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'sts'){
                                                    echo 'СТС: '.$item_4.'<br>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    echo 'Нет информации!<br>';
                }

            }else if (is_array($item) && is_string($key) && $key === 'dromrulinked'){
                echo '<br><br>ИНФО ИЗ AVITO.RU<br><br>';
                if(sizeof($item)){
                    foreach ($item as $key_1 => $item_1) {
                        foreach ($item_1 as $key_2 => $item_2) {
                            if(isset($key_2) && is_string($key_2) && $key_2 === 'phone'){
                                echo 'Телефон: '.$item_2.'<br>';
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'auto'){
                                echo '<br><br>АВТО<br><br>';
                                if(is_array($item_2)){
                                    foreach ($item_2 as $key_3 => $item_3){
                                        if(is_array($item_3)){
                                            foreach ($item_3 as $key_4 => $item_4) {
                                                if(isset($key_4) && $key_4 == 'credate'){
                                                    echo 'Дата создания: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'source'){
                                                    echo 'Источник: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'marka'){
                                                    echo 'Марка: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'model'){
                                                    echo 'Модель: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'year'){
                                                    echo 'Год: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'city'){
                                                    echo 'Город: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'distance'){
                                                    echo 'Пробег: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'price'){
                                                    echo 'Цена: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'vin'){
                                                    echo 'VIN: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'contactface'){
                                                    echo 'Контактное лицо: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'gosnomer'){
                                                    echo 'Госномер: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'images'){
                                                    echo 'Ссылка на картинку: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'isfakephone'){
                                                    echo 'Фейк номер: '.$item_4.'<br>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'realty'){
                                echo '<br><br>НЕДВИЖИМОСТЬ<br><br>';
                                if(is_array($item_2)){
                                    foreach ($item_2 as $key_3 => $item_3){
                                        if(is_array($item_3)){
                                            foreach ($item_3 as $key_4 => $item_4) {
                                                if(isset($key_4) && $key_4 == 'credate'){
                                                    echo 'Дата создания: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'source'){
                                                    echo 'Источник: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'title'){
                                                    echo 'Объект: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'city'){
                                                    echo 'Город: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'price'){
                                                    echo 'Цена: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'contactface'){
                                                    echo 'Контактное лицо: '.$item_4.'<br>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'other'){
                                echo '<br><br>ИНОЕ<br><br>';
                                if(is_array($item_2)){
                                    foreach ($item_2 as $key_3 => $item_3){
                                        if(is_array($item_3)){
                                            foreach ($item_3 as $key_4 => $item_4) {
                                                if(isset($key_4) && $key_4 == 'credate'){
                                                    echo 'Дата создания: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'source'){
                                                    echo 'Источник: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'title'){
                                                    echo 'Объект: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'city'){
                                                    echo 'Город: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'price'){
                                                    echo 'Цена: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'contactface'){
                                                    echo 'Контактное лицо: '.$item_4.'<br>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'gibdd'){
                                echo '<br><br>ГИБДД<br><br>';
                                if(is_array($item_2)){
                                    foreach ($item_2 as $key_3 => $item_3){
                                        if(is_array($item_3)){
                                            foreach ($item_3 as $key_4 => $item_4) {
                                                if(isset($key_4) && $key_4 == 'gosnumber'){
                                                    echo 'Госномер: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'car'){
                                                    echo 'Авто: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'regdate'){
                                                    echo 'Дата регистрации: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'carYear'){
                                                    echo 'Год: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'phone'){
                                                    echo 'Телефон: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'vin'){
                                                    echo 'VIN: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'family'){
                                                    echo 'Фамилия: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'name'){
                                                    echo 'Имя: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'patronim'){
                                                    echo 'Отчетсво: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'pts'){
                                                    echo 'ПТС: '.$item_4.'<br>';
                                                }
                                                if(isset($key_4) && $key_4 == 'sts'){
                                                    echo 'СТС: '.$item_4.'<br>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    echo 'Нет информации!<br>';
                }

            }else if (is_string($key) && $key === 'phonenumberto'){
                echo '<br><br>ИНФО ПО ТЕЛЕФОНУ<br><br>';
                if(strlen($item) > 0){
                    echo 'Информация по номеру: '.$item.'<br>';
                }else{
                    echo 'Нет информации!<br>';
                }

            }else if (is_array($item) && is_string($key) && $key === 'leaks'){
                echo '<br><br>УТЕЧКА<br><br>';
                if(sizeof($item)){
                    foreach ($item as $key_1 => $item_1) {
                        foreach ($item_1 as $key_2 => $item_2) {
                            if(isset($key_2) && is_string($key_2) && $key_2 === 'phone'){
                                echo 'Телефон: '.$item_2.'<br>';
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'email'){
                                echo 'Email: '.$item_2.'<br>';
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'lastname'){
                                echo 'Фамилия: '.$item_2.'<br>';
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'firstname'){
                                echo 'Имя: '.$item_2.'<br>';
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'middlename'){
                                echo 'Отчетсво: '.$item_2.'<br>';
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'facebookid'){
                                echo 'Facebook ID: '.$item_2.'<br>';
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'instagramname'){
                                echo 'INSTAGRAMM: '.$item_2.'<br>';
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'vkid'){
                                echo 'VK ID: '.$item_2.'<br>';
                            }else if(isset($key_2) && is_string($key_2) && $key_2 === 'vkusername'){
                                echo 'VK NAME: '.$item_2.'<br>';
                            }
                        }
                    }
                }else{
                    echo 'Нет информации!<br>';
                }
            }
        }

        return  'Info LOAD';

    }

    public function isEnglishName($name)
    {
        return ($name && $name != '' && (preg_match( '/[а-яё]/iu',  $name) == 0) || VehicleMarks::where('title', $name)->first()) ? true : false;
    }
}
