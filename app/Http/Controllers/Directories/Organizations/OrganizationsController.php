<?php

namespace App\Http\Controllers\Directories\Organizations;

use App\Classes\Export\ExportManager;
use App\Http\Controllers\Controller;

use App\Models\Log\LogEvents;
use App\Models\Organizations\AgentContractDocument;
use App\Models\Organizations\Organization;
use App\Models\Organizations\OrgBankAccount;
use App\Models\Settings\Department;
use App\Models\Settings\Template;
use App\Models\Settings\TemplateCategory;
use App\Models\Settings\TypeOrg;
use App\Models\User;
use App\Repositories\FilesRepository;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;

class OrganizationsController extends Controller
{
    protected $filesRepository;

    public function __construct(FilesRepository $filesRepository)
    {
        $this->middleware('permissions:directories,organizations');

        $this->filesRepository = $filesRepository;

        $this->breadcrumbs[] = [
            'label' => 'Справочники',
        ];
        
        $this->breadcrumbs[] = [
            'label' => 'Организации',
            'url' => 'directories/organizations/organizations',
        ];

    }

    public function index()
    {
        $control_url = '/directories/organizations';

        return view('organizations.organizations.index', [
            'organizations' => Organization::getALLOrg()->get(),
            'control_url' => $control_url
        ]);
    }

    public function create()
    {
        $organization = new Organization();
        return view('organizations.edit',
            ['organization'     => $organization]
        )->with('breadcrumbs', $this->breadcrumbs);
    }

    public function edit($id)
    {
        
        $user = Auth::user();
        $organization = Organization::getIdOrg($id);

        if(isset($organization->id)){

            $this->breadcrumbs[] = [
            'label' => $organization->title,
            ];
            
            return view('organizations.edit', [
                'organization'      => $organization,
                'org_bank_account'  => OrgBankAccount::where('org_id', $id)->get(),
                'permission_delete' => $user->hasPermission('directories', 'organizations_edit')
            ])->with('breadcrumbs', $this->breadcrumbs);
        }else{
            return view('errors.403', ['exception'=>1]);
        }

    }


    public function store(Request $request)
    {
        return $this->save(new Organization, $request);
    }

    public function update($id, Request $request)
    {
        if((int)$id > 0 ) $org = Organization::findOrFail($id);
        else $org = new Organization;

        return $this->save($org, $request);
    }


    private function save(Organization $organization, Request $request)
    {
        $organization->saveData($request);
        return redirect("/directories/organizations/organizations/{$organization->id}/edit/")->with('success', trans('form.success_update'));
    }

    public function delete($id)
    {
        $user = Auth::user();

        if($user->hasPermission('directories', 'organizations_edit')){
            $org = Organization::findOrFail($id);
            $org->update([
                'is_delete' => 1
            ]);

            LogEvents::event($id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_DELETE], 9);

            User::where('organization_id', $id)->update([
                'status_user_id' => 1
            ]);

        }else{
            return view('errors.403', ['exception'=>1]);
        }

        return response('', 200);
    }



    public function get_html_block($id, Request $request)
    {
        $user = Auth::user();

        if($id == 0)$organization = new Organization();
        else $organization = Organization::getIdOrg($id);

        $send_urls = "/directories/organizations/organizations/".(int)$organization->id;
        return view($request->view, [
            'organization' => $organization,
            'send_urls' => $send_urls,
            'users' => new User(),
            'permission_delete' => $user->hasPermission('directories', 'organizations_edit')
        ]);

    }



    public function get_users_list($id){

        $organization  = Organization::query()->findOrFail($id);
        $users = $organization->users();

        if(request()->has('department_id') && request()->get('department_id') > 0){
            $users->where('department_id', (int)request()->get('department_id'));
        }

        return [
            'organization' => $organization,
            'users' => $users->get(),
        ];
    }

    public function get_users_table($id){
        $data = $this->get_users_list($id);
        $data['html'] = view("organizations.users_table", $data)->render();
        return $data;
    }

    public function document_template($id,  Request $request){

        if($tmp = Template::where('org_id', $id)->first()){
            return view('settings.templates.edit', [
                'template' => $tmp,
            ]);
        }

        return view('settings.templates.create');
    }

    public function addDocument($id,  Request $request){

        $file = $request->file('agent_contract');

        Organization::findOrFail($id)->agent_contracts()->save($this->filesRepository->makeFile($file, AgentContractDocument::FILES_DOC . "/$id/"));

        return parentReload();
    }

    public function documentation($id, Request $request){

        $builder = Organization::query()->where('id', $id);
        $builder->firstOrFail();

        $category = TemplateCategory::get('contract_agent');
        return (new ExportManager($category, $builder))->handle();

    }

}
