<?php


namespace App\Http\Controllers\Directories\Organizations;


use App\Http\Controllers\Controller;
use App\Models\Organizations\Organization;
use App\Models\Organizations\PointsDepartments;
use Illuminate\Http\Request;

class PointsDepartmentsController extends Controller
{
    public function __construct()
    {

    }


    public function edit($id, $point_department_id)
    {

        $org = Organization::find($id);

        if((int)$point_department_id > 0){
            $point_department = PointsDepartments::where('organization_id', $id)->where('id', $point_department_id)->get()->first();
        }else{
            $point_department = new PointsDepartments();
        }

        return view('organizations.organizations.partials.points_departments.edit', [
            'organization' => $org,
            'point_department' => $point_department
        ]);

    }


    public function save($id, $point_department_id, Request $request)
    {
        $org = Organization::find($id);

        if((int)$point_department_id > 0){
            $point_department = PointsDepartments::where('organization_id', $id)->where('id', $point_department_id)->get()->first();

        }else{
            $point_department = new PointsDepartments();
            $point_department->organization_id = $id;
        }

        $point_department->title = $request->title;
        $point_department->address = $request->address;
        $point_department->geo_lat = $request->geo_lat;
        $point_department->geo_lng = $request->geo_lon;
        $point_department->save();

        return parentReload();
    }

    public function delete($id, $point_department_id, Request $request){
        $point_department = PointsDepartments::where('organization_id', $id)->where('id', $point_department_id)->get()->first()->delete();

        return parentReload();
    }
}