<?php

namespace App\Http\Controllers\Directories\Categories;

use App\Http\Controllers\Controller;
use App\Models\Log\LogEvents;
use App\Models\Vehicle\VehicleCategories;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permissions:directories,categories');
    }

    public function index()
    {
        return view('directories.categories.index', [
            'categories' => VehicleCategories::orderBy('code')->get()
        ]);
    }

    public function create()
    {
        return view('directories.categories.create');
    }

    public function edit($id)
    {
        return view('directories.categories.edit', [
            'category' => VehicleCategories::findOrFail($id)
        ]);
    }

    public function store(Request $request)
    {
        $category = new VehicleCategories;
        $category->save();
        LogEvents::event($category->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_CREATE], 6, 0,0, $request->all());

        return $this->save($category, $request);
    }

    public function update($id, Request $request)
    {
        $category = VehicleCategories::findOrFail($id);
        LogEvents::event($category->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_UPDATE], 6, 0,0, $request->all());
        return $this->save($category, $request);
    }

    private function save(VehicleCategories $category, Request $request)
    {
        $category->title = $request->title;
        $category->code = $request->code;
        $category->save();

        return parentReload();
        return redirect("/directories/categories/");
    }

    public function destroy($id)
    {
        LogEvents::event($id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_DELETE], 6);

        VehicleCategories::findOrFail($id)->delete();

        return response('', 200);
    }

}
