<?php

namespace App\Http\Controllers\Directories\InsuranceCompanies\BsoSuppliers\Agent;

use App\Models\Directories\Agent;
use App\Models\Directories\AgentDocuments;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Repositories\FilesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgentController extends Controller
{
    protected $filesRepository;

    public function __construct(FilesRepository $filesRepository)
    {
        $this->filesRepository = $filesRepository;

        $this->breadcrumbs[] = [
            'label' => 'Справочники',
        ];

        $this->breadcrumbs[] = [
            'label' => 'Страховые компании',
            'url' => 'directories/insurance_companies',
        ];
    }

    public function agent_edit($id, $bso_supplier_id){

        $sk = InsuranceCompanies::where('id', $id)->get()->first();

        $this->breadcrumbs[] = [
            'label' => $sk->title,
            'url' => $id,
        ];

        $bso_supplier = BsoSuppliers::where('insurance_companies_id', $id)->where('id', $bso_supplier_id)->get()->first();
        $this->breadcrumbs[] = [
            'label' => 'Поставщик БСО - ' . $bso_supplier->title,
            'url' => 'bso_suppliers/' . $bso_supplier_id  . '/'
        ];

        $this->breadcrumbs[] = [
            'label' => 'Настройка Агента',
        ];


        if(!$agent = $bso_supplier->agent){
            $agent =  new Agent();
        }

        return view('directories.insurance_companies.bso_suppliers.agent.agent_settings', [
            'id' => $id,
            'bso_supplier' => $bso_supplier,
            'agent' => $agent

        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function agent_save($id, $bso_supplier_id){

        $this->validate(request(), [
            "agent_id" => "string",
            "department_id" => "string",
            "manager_id" => "string",
            "signer_id" => "string",
            "сhannel_sale_id" => "string",
            "agent_name" => "string",
            "contract_name" => "string",
        ]);

        $agent = Agent::where('bso_supplier_id', $bso_supplier_id);

        $params = [
            'bso_supplier_id' => $bso_supplier_id,
            'agent_id' => request('agent_id'),
            'agent_name' => request('agent_name'),
            'contract_name' => request('contract_name'),
            'department_id' => request('department_id'),
            'manager_id' => request('manager_id'),
            'signer_id' => request('signer_id'),
            'сhannel_sale_id' => request('сhannel_sale_id'),
        ];

        if(!isset($agent->get()[0])){
            $agent = Agent::create($params);
        }else{
            $agent->update($params);
        }

        if($agent){
            return back()->with('success', 'Успешно сохранено');
        }else{
            return back()->with('error', 'Произошла ошибка');
        }

    }

    public function addDocument($id, $bso_supplier_id,  Request $request){
        $agent = Agent::where('bso_supplier_id', $bso_supplier_id)->first();

        Agent::findOrFail($agent->id)->documents()->save($this->filesRepository->makeFile($request->file, Agent::FILES_DOC . "/$agent->id/"));

        return response('', 200);
    }

    public function deleteDocument($id, $bso_supplier_id) {

        $agent = Agent::findOrFail($bso_supplier_id);

        if (isset($agent) && sizeof($agent->documents)) {
            AgentDocuments::where("bso_supplier_id", $bso_supplier_id)->delete();
            $fileDelete = app()->make('\App\Http\Controllers\FilesController')
                ->callAction('destroy', [$bso_supplier_id]);
        }
    }
}
