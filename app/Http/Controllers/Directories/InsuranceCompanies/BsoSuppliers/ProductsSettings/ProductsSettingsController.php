<?php

namespace App\Http\Controllers\Directories\InsuranceCompanies\BsoSuppliers\ProductsSettings;

use App\Http\Controllers\Controller;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\Products;
use App\Models\Finance\PayMethod;
use App\Models\Settings\BsoSupplierProductPayMethod;
use App\Models\Settings\BsoSupplierProductPayMethodAccessBso;

class ProductsSettingsController extends Controller
{

    public function __construct()
    {

    }

    public function save($insurance_companies_id, $bso_supplier_id, $product_id)
    {

        $this->validate(request(), [
            'method.*' => 'integer'
        ]);

        $insurance_company = InsuranceCompanies::query()->findOrFail($insurance_companies_id);
        $bso_supplier = BsoSuppliers::query()->findOrFail($bso_supplier_id);
        $product = Products::query()->findOrFail($product_id);

        $access_bso = BsoSupplierProductPayMethodAccessBso::query()
            ->where('bso_supplier_id', $bso_supplier_id)
            ->where('product_id', $product_id)
            ->where('group_id', request('group_id'))
            ->first();

        if ($access_bso){
            $access_bso->access_bso = request('access_bso');
            $access_bso->save();
        }else{
            BsoSupplierProductPayMethodAccessBso::create([
                'group_id' => request('group_id'),
                'access_bso' => request('access_bso'),
                'bso_supplier_id' => $bso_supplier_id,
                'product_id' => $product_id
            ]);
        }

        $all_methods = PayMethod::all()->pluck('id')->toArray();

        foreach($all_methods as $method_id){

            BsoSupplierProductPayMethod::where('bso_supplier_id', '=', $bso_supplier_id)
                ->where('pay_method_id', '=', $method_id)
                ->where('product_id', '=', $product_id)
                ->where('group_id', '=', request('group_id'))->delete();
        }


        foreach (request('method', []) as $method_id) {

            BsoSupplierProductPayMethod::create([
                'bso_supplier_id' => $bso_supplier_id,
                'pay_method_id' => $method_id,
                'product_id' => $product_id,
                'group_id' => request('group_id'),
            ]);

        }



        return back()->with('group_id', request('group_id'));
    }


}