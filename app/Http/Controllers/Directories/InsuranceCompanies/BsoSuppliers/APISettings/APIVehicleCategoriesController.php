<?php

namespace App\Http\Controllers\Directories\InsuranceCompanies\BsoSuppliers\APISettings;

use App\Http\Controllers\Controller;
use App\Models\Api\SK\ApiSetting;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Log\LogEvents;
use App\Models\Vehicle\APISettings\VehicleCategoriesSK;
use App\Models\Vehicle\VehicleCategories;
use Illuminate\Http\Request;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;


class APIVehicleCategoriesController extends Controller
{

    public function __construct()
    {
        $this->breadcrumbs[] = [
            'label' => 'Справочники',
        ];

        $this->breadcrumbs[] = [
            'label' => 'Страховые компании',
            'url' => 'directories/insurance_companies',
        ];
    }




    public function edit($id, $bso_supplier_id){

        $sk = InsuranceCompanies::where('id', $id)->get()->first();
        $bso_supplier = BsoSuppliers::where('insurance_companies_id', $id)->where('id', $bso_supplier_id)->get()->first();

        $this->breadcrumbs[] = [
            'label' => $sk->title,
            'url' => $id,
        ];


        $this->breadcrumbs[] = [
            'label' => 'Поставщик БСО - ' . $bso_supplier->title,
            'url' => 'bso_suppliers/' . $bso_supplier_id
        ];

        $this->breadcrumbs[] = [
            'label' => 'Настройка API',
            'url' => 'api/'
        ];

        $this->breadcrumbs[] = [
            'label' => 'Категории авто',
        ];

        $all_vehicle_categories = VehicleCategories::orderBy('code', 'asc')->get();
        $vehicle_categories_sk = VehicleCategoriesSK::where('insurance_companies_id', $id)->where('bso_supplier_id', $bso_supplier_id)->get();


        return view('directories.insurance_companies.bso_suppliers.api.vehicle_categories', [
            'all_vehicle_categories' => $all_vehicle_categories,
            'vehicle_categories_sk' => $vehicle_categories_sk,
            'id' => $id,
            'bso_supplier' => $bso_supplier
        ])->with('breadcrumbs', $this->breadcrumbs);

    }

    public function save($id, $bso_supplier_id, Request $request)
    {

        $categories = $request->categories_sk;
        foreach ($categories as $key => $categorie){

            if(strlen($categorie)>0){
                $categorieSk = VehicleCategoriesSK::where('insurance_companies_id', $id)
                    ->where('bso_supplier_id', $bso_supplier_id)
                    ->where('vehicle_categorie_sk_id', $categorie)
                    ->get()
                    ->first();

                if($categorieSk){
                    $categorieSk->vehicle_categorie_id = $key;
                    $categorieSk->save();
                }
            }
        }

        return redirect(url("/directories/insurance_companies/$id/bso_suppliers/$bso_supplier_id/api/vehicle_categories_sk/"))
            ->with('success', trans('form.success_update'));

    }


    public function updata($id, $bso_supplier_id){

        $sk = InsuranceCompanies::where('id', $id)->get()->first();
        $bso_supplier = BsoSuppliers::where('insurance_companies_id', $id)->where('id', $bso_supplier_id)->get()->first();

        $result = new \stdClass();
        $result->state = false;
        $result->msg = 'Ошибка настройки API';


        if($api = $bso_supplier->get_service_controller()){

            if($dictionary = $api->get_service('dictionary')){

                if($categories = $dictionary->get_api_categories()){

                    VehicleCategoriesSK::where('insurance_companies_id', $id)
                        ->where('bso_supplier_id', $bso_supplier_id)
                        ->delete();


                    //СОХРАНЯЕМ В БАЗУ

                    foreach ($categories as $categorie){

                        VehicleCategoriesSK::create([
                            'insurance_companies_id' => $id,
                            'bso_supplier_id' => $bso_supplier_id,
                            'vehicle_categorie_sk_id' => $categorie['id'],
                            'sk_title' => $categorie['title'],
                        ]);
                    }

                    $result->state = true;
                    $result->response = null;
                }
            }
        }

        return response()->json($result);

    }

}
