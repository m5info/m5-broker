<?php

namespace App\Http\Controllers\Directories\InsuranceCompanies\BsoSuppliers\APISettings;

use App\Http\Controllers\Controller;
use App\Models\Api\SK\ApiSetting;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\Products;
use App\Models\Log\LogEvents;
use Illuminate\Http\Request;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;


class APISettingsController extends Controller
{

    public function __construct()
    {
        $this->breadcrumbs[] = [
            'label' => 'Справочники',
        ];

        $this->breadcrumbs[] = [
            'label' => 'Страховые компании',
            'url' => 'directories/insurance_companies',
        ];
    }



    public function api_edit($id, $bso_supplier_id){

        $sk = InsuranceCompanies::where('id', $id)->get()->first();

        $this->breadcrumbs[] = [
            'label' => $sk->title,
            'url' => $id,
        ];

        $bso_supplier = BsoSuppliers::where('insurance_companies_id', $id)->where('id', $bso_supplier_id)->get()->first();

        $this->breadcrumbs[] = [
            'label' => 'Поставщик БСО - ' . $bso_supplier->title,
            'url' => 'bso_suppliers/' . $bso_supplier_id  . '/'
        ];


        $this->breadcrumbs[] = [
            'label' => 'Настройка API',
        ];


        $sk_services_path = app()->basePath() . '/app/Services/SK';

        $dir_names = array_filter(scandir($sk_services_path), function($v){
            return !stristr($v, '.');
        });

        $dir_names = collect($dir_names)->keyBy(function($item){
            return $item;
        });

        if(!$api_setting = $bso_supplier->api_setting){
            $api_setting =  new ApiSetting();
        }

        $all_marks = VehicleMarks::get();

        $online_products = Products::query()->where('is_online', 1)->get();

        return view('directories.insurance_companies.bso_suppliers.api.api_settings', [
            'all_marks' => $all_marks,
            'id' => $id,
            'bso_supplier' => $bso_supplier,
            'dir_names' => $dir_names,
            'api_setting' => $api_setting,
            'online_products' => $online_products

        ])->with('breadcrumbs', $this->breadcrumbs);

    }


    public function api_save($id, $bso_supplier_id){

        $this->validate(request(), [
            "is_actual" => "integer",
            "custom_realization" => "integer",
            "work_type" => "integer",
            "dir_name" => "string",
            "battle_server_url" => "string",
            "battle_server_login" => "string",
            "battle_server_password" => "string",
            "test_server_url" => "string",
            "test_server_login" => "string",
            "test_server_password" => "string",
            "sms_confirmation" => "integer"
        ]);

        ApiSetting::where('bso_supplier_id', $bso_supplier_id)->delete();
        $setting = ApiSetting::create([
            "is_actual" => request('is_actual', 0),
            "custom_realization" => request('custom_realization', 0),
            "work_type" => request('work_type'),
            "dir_name" => request('dir_name'),
            "battle_server_url" => request('battle_server_url'),
            "battle_server_login" => request('battle_server_login'),
            "battle_server_password" => request('battle_server_password'),
            "test_server_url" => request('test_server_url'),
            "test_server_login" => request('test_server_login'),
            "test_server_password" => request('test_server_password'),
            "bso_supplier_id" => $bso_supplier_id,
            "sms_confirmation" => request('sms_confirmation'),
            "auxiliary_date" => null,
            "auxiliary_parameters" => '',
        ]);

        if($setting){
            return back()->with('success', 'Успешно сохранено');
        }else{
            return back()->with('error', 'Произошла ошибка');
        }



    }



}
