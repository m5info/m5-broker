<?php

namespace App\Http\Controllers\Directories\InsuranceCompanies\BsoSuppliers\APISettings;

use App\Helpers\EmptyObject;
use App\Http\Controllers\Controller;
use App\Models\Api\SK\ApiSetting;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Log\LogEvents;
use App\Models\Vehicle\APISettings\VehicleCategoriesSK;
use App\Models\Vehicle\APISettings\VehicleMarksSK;
use App\Models\Vehicle\APISettings\VehicleModelsSK;
use App\Models\Vehicle\VehicleAntiTheftSystem;
use DB;
use Illuminate\Http\Request;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;
use Illuminate\Support\Str;


class APIAntiTheftDevicesController extends Controller
{

    public function __construct()
    {
        $this->breadcrumbs[] = [
            'label' => 'Справочники',
        ];

        $this->breadcrumbs[] = [
            'label' => 'Страховые компании',
            'url' => 'directories/insurance_companies',
        ];
    }

    public function anti_theft_devices($id, $bso_supplier_id){

        $sk = InsuranceCompanies::where('id', $id)->get()->first();

        $this->breadcrumbs[] = [
            'label' => $sk->title,
            'url' => $id,
        ];

        $bso_supplier = BsoSuppliers::where('insurance_companies_id', $id)->where('id', $bso_supplier_id)->get()->first();

        $this->breadcrumbs[] = [
            'label' => 'Поставщик БСО - ' . $bso_supplier->title,
            'url' => 'bso_suppliers/' . $bso_supplier_id
        ];

        $this->breadcrumbs[] = [
            'label' => 'Настройка API',
            'url' => 'api/'
        ];


        $this->breadcrumbs[] = [
            'label' => 'Противоугонные устройства',
        ];

        $devices = VehicleAntiTheftSystem::all();

        return view('directories.insurance_companies.bso_suppliers.api.anti_theft_devices', [
            'devices' => $devices,
            'id' => $id,
            'bso_supplier' => $bso_supplier
        ])->with('breadcrumbs', $this->breadcrumbs);

    }

    /*public function updata_mark($id, $bso_supplier_id){

        $sk = InsuranceCompanies::where('id', $id)->get()->first();
        $bso_supplier = BsoSuppliers::where('insurance_companies_id', $id)->where('id', $bso_supplier_id)->get()->first();

        $result = new \stdClass();
        $result->state = false;
        $result->msg = 'Ошибка настройки API';

        //если есть сервис контроллер для ск
        if($api = $bso_supplier->get_service_controller()){

            //если есть сервис справочников
            if($dictionary = $api->get_service('dictionary')){

                //всё что было трём нафиг
                VehicleMarksSK::where('insurance_companies_id', $id)->where('bso_supplier_id', $bso_supplier_id)->delete();
                VehicleModelsSK::where('insurance_companies_id', $id)->where('bso_supplier_id', $bso_supplier_id)->delete();

                //наши марки по тайтлу
                $vehicle_marks = VehicleMarks::query()
                    ->selectRaw('*, LOWER(title) as title')
                    ->get()->keyBy('title');


                //наши модели по "{id нашей марки}x{тайтл нашей модели}"
                $vehicle_models = VehicleModels::query()
                    ->selectRaw("*, CONCAT(mark_id, 'x', LOWER(title)) as markid_title")
                    ->get()->keyBy('markid_title');


                $sk_marks_2_marks = [];


                $vehicle_marks_sk = [];
                $vehicle_models_sk = [];

                //запрашиваем данные из апишки
                $api_marks_models = $dictionary->get_marks_models();


                //идём по маркам
                if(isset($api_marks_models['marks']) && is_array($api_marks_models['marks'])){

                    foreach($api_marks_models['marks'] as $mark){

                        $vehicle_mark = new EmptyObject();

                        //если у нас есть марка с таким же названием
                        if(isset($vehicle_marks[Str::lower($mark['title'])])){
                            $vehicle_mark = $vehicle_marks[Str::lower($mark['title'])];
                        }

                        $vehicle_mark_sk = [
                            'insurance_companies_id' => (int)$id,
                            'bso_supplier_id' => (int)$bso_supplier_id,
                            'vehicle_category_id' => 0, // TODO у нас пока все марки с категорией 0
                            'vehicle_marks_id' => (int)$vehicle_mark->id,
                            'vehicle_marks_sk_id' => $mark['id'],
                            'sk_title' => $mark['title'],
                        ];

                        $vehicle_marks_sk[] = $vehicle_mark_sk;

                        $sk_marks_2_marks[$mark['id']] = $vehicle_mark->id;

                    }

                }


                //идём по моделям
                if(isset($api_marks_models['models']) && is_array($api_marks_models['models'])){

                    foreach($api_marks_models['models'] as $model){

                        $vehicle_mark_id = 0;
                        $vehicle_model = new EmptyObject();

                        //если у нас нашлась марка как у этой модели(СК)
                        if(isset($sk_marks_2_marks[$model['vehicle_mark_sk_id']]) && !empty($sk_marks_2_marks[$model['vehicle_mark_sk_id']])){
                            $vehicle_mark_id = $sk_marks_2_marks[$model['vehicle_mark_sk_id']];

                            //если у нашей марки нашлась такая модель(СК)
                            $lower_model_title = Str::lower($model['title']);
                            if(isset($vehicle_models["{$vehicle_mark_id}x{$lower_model_title}"])){
                                $vehicle_model = $vehicle_models["{$vehicle_mark_id}x{$lower_model_title}"];
                            }

                        }


                        $vehicle_model_sk = [
                            'insurance_companies_id' => (int)$id,
                            'bso_supplier_id' => (int)$bso_supplier_id,

                            'vehicle_category_id' => (int)$vehicle_model->vehicle_categorie_id,
                            'vehicle_marks_id' => (int)$vehicle_mark_id,
                            'vehicle_models_id' => (int)$vehicle_model->id,

                            'vehicle_category_sk_id' => $model['vehicle_categorie_sk_id'],
                            'vehicle_marks_sk_id' => $model['vehicle_mark_sk_id'],
                            'vehicle_models_sk_id' => $model['id'],

                            'sk_title' => $model['title'],
                        ];

                        $vehicle_models_sk[] = $vehicle_model_sk;

                    }

                }



                VehicleMarksSK::create_many($vehicle_marks_sk);
                VehicleModelsSK::create_many($vehicle_models_sk);

                $result->state = true;
                $result->msg = 'Данные успешно обновлены!';
                $result->response = null;

            }
        }

        return response()->json($result);

    }*/


    public function save_devices($id, $bso_supplier_id, $mark_id, Request $request)
    {
        /*$models = $request->model_sk;

        foreach ($models as $key => $model){
            if(strlen($model)>0){
                $modelSk = VehicleModelsSK::where('insurance_companies_id', $id)
                    ->where('bso_supplier_id', $bso_supplier_id)
                    ->where('vehicle_models_sk_id', $model)
                    ->get()
                    ->first();

                if($modelSk){
                    $modelDict = VehicleModels::where('id', $key)->get()->first();
                    $modelSk->vehicle_category_id = $modelDict->category_id;
                    $modelSk->vehicle_models_id = $modelDict->id;
                    $modelSk->save();
                }
            }
        }

        return redirect(url("/directories/insurance_companies/$id/bso_suppliers/$bso_supplier_id/api/mark/$mark_id"))->with('success', trans('form.success_update'));*/

    }

}
