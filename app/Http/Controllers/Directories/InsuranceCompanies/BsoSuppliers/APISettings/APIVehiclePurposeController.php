<?php

namespace App\Http\Controllers\Directories\InsuranceCompanies\BsoSuppliers\APISettings;

use App\Http\Controllers\Controller;
use App\Models\Api\SK\ApiSetting;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Log\LogEvents;
use App\Models\Vehicle\APISettings\VehicleCategoriesSK;
use App\Models\Vehicle\APISettings\VehiclePurposeSK;
use App\Models\Vehicle\VehicleCategories;
use App\Models\Vehicle\VehiclePurpose;
use Illuminate\Http\Request;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;


class APIVehiclePurposeController extends Controller
{

    public function __construct()
    {
        $this->breadcrumbs[] = [
            'label' => 'Справочники',
        ];

        $this->breadcrumbs[] = [
            'label' => 'Страховые компании',
            'url' => 'directories/insurance_companies',
        ];
    }




    public function edit($id, $bso_supplier_id){

        $sk = InsuranceCompanies::where('id', $id)->get()->first();
        $bso_supplier = BsoSuppliers::where('insurance_companies_id', $id)->where('id', $bso_supplier_id)->get()->first();

        $this->breadcrumbs[] = [
            'label' => $sk->title,
            'url' => $id,
        ];


        $this->breadcrumbs[] = [
            'label' => 'Поставщик БСО - ' . $bso_supplier->title,
            'url' => 'bso_suppliers/' . $bso_supplier_id
        ];

        $this->breadcrumbs[] = [
            'label' => 'Настройка API',
            'url' => 'api/'
        ];

        $this->breadcrumbs[] = [
            'label' => 'Цель использования',
        ];

        $all_vehicle_purpose = VehiclePurpose::get();
        $vehicle_purpose_sk = VehiclePurposeSK::where('insurance_companies_id', $id)->where('bso_supplier_id', $bso_supplier_id)->get();


        return view('directories.insurance_companies.bso_suppliers.api.vehicle_purpose', [
            'all_vehicle_purpose' => $all_vehicle_purpose,
            'vehicle_purpose_sk' => $vehicle_purpose_sk,
            'id' => $id,
            'bso_supplier' => $bso_supplier
        ])->with('breadcrumbs', $this->breadcrumbs);

    }

    public function save($id, $bso_supplier_id, Request $request)
    {

        $purposes = $request->purpose_sk;
        foreach ($purposes as $key => $purpose){

            if(strlen($purpose)>0){
                $purposeSk = VehiclePurposeSK::where('insurance_companies_id', $id)
                    ->where('bso_supplier_id', $bso_supplier_id)
                    ->where('vehicle_purpose_sk_id', $purpose)
                    ->get()
                    ->first();

                if($purposeSk){
                    $purposeSk->vehicle_purpose_id = $key;
                    $purposeSk->save();
                }
            }
        }

        return redirect(url("/directories/insurance_companies/$id/bso_suppliers/$bso_supplier_id/api/vehicle_purpose_sk/"))->with('success', trans('form.success_update'));

    }


    public function updata($id, $bso_supplier_id){

        $sk = InsuranceCompanies::where('id', $id)->get()->first();

        $bso_supplier = BsoSuppliers::where('insurance_companies_id', $id)
            ->where('id', $bso_supplier_id)
            ->get()->first();

        $result = new \stdClass();
        $result->state = false;
        $result->msg = 'Ошибка настройки API';


        if($api = $bso_supplier->get_service_controller()){

            if($dictionary = $api->get_service('dictionary')){

                if($purposes = $dictionary->get_api_purpose()){


                    //СОХРАНЯЕМ В БАЗУ
                    VehiclePurposeSK::where('insurance_companies_id', $id)->where('bso_supplier_id', $bso_supplier_id)->delete();
                    foreach ($purposes as $purpose){

                        VehiclePurposeSK::create([
                            'insurance_companies_id' => $id,
                            'bso_supplier_id' => $bso_supplier_id,
                            'vehicle_purpose_sk_id' => $purpose['id'],
                            'sk_title' => $purpose['title'],
                        ]);
                    }

                    $result->state = true;
                    $result->response = null;
                }

            }
        }

        return response()->json($result);

    }

}
