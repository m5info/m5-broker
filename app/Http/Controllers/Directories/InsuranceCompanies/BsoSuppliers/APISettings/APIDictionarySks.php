<?php

namespace App\Http\Controllers\Directories\InsuranceCompanies\BsoSuppliers\APISettings;

use App\Helpers\EmptyObject;
use App\Models\Dictionaries\DictionarySk;
use App\Models\Dictionaries\DictionarySkSk;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Vehicle\VehicleMarks;
use Dadata\DadataClient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class APIDictionarySks extends Controller
{
    public function __construct()
    {
        $this->breadcrumbs[] = [
            'label' => 'Справочники',
        ];

        $this->breadcrumbs[] = [
            'label' => 'Страховые компании',
            'url' => 'directories/insurance_companies',
        ];
    }



    public function edit($id, $bso_supplier_id){

        $sk = InsuranceCompanies::where('id', $id)->get()->first();
        $bso_supplier = BsoSuppliers::where('insurance_companies_id', $id)->where('id', $bso_supplier_id)->get()->first();

        $this->breadcrumbs[] = [
            'label' => $sk->title,
            'url' => $id,
        ];


        $this->breadcrumbs[] = [
            'label' => 'Поставщик БСО - ' . $bso_supplier->title,
            'url' => 'bso_suppliers/' . $bso_supplier_id
        ];

        $this->breadcrumbs[] = [
            'label' => 'Настройка API',
            'url' => 'api/'
        ];

        $this->breadcrumbs[] = [
            'label' => 'Список страховых (пролонгация)',
        ];

        $all_sks = DictionarySk::get();
        $all_sks_sk = DictionarySkSk::where('insurance_companies_id', $id)->where('bso_supplier_id', $bso_supplier_id)->get();


        return view('directories.insurance_companies.bso_suppliers.api.dictionary_sks', [
            'all_sks' => $all_sks,
            'all_sks_sk' => $all_sks_sk,
            'id' => $id,
            'bso_supplier' => $bso_supplier
        ])->with('breadcrumbs', $this->breadcrumbs);

    }


    public function save($id, $bso_supplier_id, Request $request)
    {

        $dictionary_sks_sk = $request->dictionary_sks_sk;
        foreach ($dictionary_sks_sk as $key => $dictionary_sk_sk){

            if(strlen($dictionary_sk_sk)>0){
                $dictionarySkSk = DictionarySkSk::where('insurance_companies_id', $id)
                    ->where('bso_supplier_id', $bso_supplier_id)
                    ->where('dictionary_sk_sk_id', $dictionary_sk_sk)
                    ->get()
                    ->first();

                if($dictionarySkSk){
                    $dictionarySkSk->dictionary_sk_id = $key;
                    $dictionarySkSk->save();
                }
            }
        }

        return redirect(url("/directories/insurance_companies/$id/bso_suppliers/$bso_supplier_id/api/dictionary_sks/"))->with('success', trans('form.success_update'));

    }


    public function updata($id, $bso_supplier_id){

        $sk = InsuranceCompanies::where('id', $id)->get()->first();

        $bso_supplier = BsoSuppliers::where('insurance_companies_id', $id)
            ->where('id', $bso_supplier_id)
            ->get()->first();

        $result = new \stdClass();
        $result->state = false;
        $result->msg = 'Ошибка настройки API';


        if($api = $bso_supplier->get_service_controller()){

            if($dictionary = $api->get_service('dictionary')){

                if($sks = $dictionary->get_api_sks()){

                    //наши ск по тайтлу
                    $our_sks = DictionarySk::query()
                        ->selectRaw('*, LOWER(title) as title')
                        ->get()->keyBy('title');

                    //СОХРАНЯЕМ В БАЗУ
                    DictionarySkSk::where('insurance_companies_id', $id)->where('bso_supplier_id', $bso_supplier_id)->delete();

                    foreach ($sks as $sk){

                        $sk_title = Str::lower($sk['title']);
                        $_sk = new EmptyObject();

                        //если у нас есть ск с похожим названием
                        foreach($our_sks as $our_title => $our_sk){

                            if(mb_strstr($sk_title, $our_title)){
                                $_sk = $our_sk;

                                $kpp = $this->getKppByInn($our_sk->inn);
                                $org_title = $this->getTitleByInn($our_sk->inn);
                            }
                        }

                        DictionarySkSk::create([
                            'insurance_companies_id' => $id,
                            'bso_supplier_id' => $bso_supplier_id,
                            'dictionary_sk_id' => (int)$_sk->id,
                            'dictionary_sk_sk_id' => $sk['id'],
                            'sk_title' => $sk['title'],
                            'kpp' => $kpp,
                            'official_title' => $org_title,
                        ]);
                    }

                    $result->state = true;
                    $result->response = null;
                }

            }
        }

        return response()->json($result);

    }

    public function getKppByInn($inn)
    {
        if($org = $this->getOrgByInn($inn)){

            return $org['kpp'];
        }

        return null;
    }

    public function getTitleByInn($inn)
    {
        if($org = $this->getOrgByInn($inn)){

            return $org['name']['full_with_opf'];
        }

        return null;
    }

    public function getOrgByInn($inn)
    {
        if($inn){
            $token = "5f6f4a246bd9d39b1a60c5058e4aaa359bed9c04";
            $dadata = new DadataClient($token, null);
            $result = $dadata->findById("party", $inn, 1);

            if(!$result){
                return false;
            }

            return $result[0]['data'];
        }

        return null;
    }
}
