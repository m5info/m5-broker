<?php

namespace App\Http\Controllers\Directories\InsuranceCompanies\BsoSuppliers\FinancialPolicy;

use App\Http\Controllers\Controller;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\FinancialPolicySegment;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Log\LogEvents;
use Illuminate\Http\Request;

class SegmentsController extends Controller
{

    public function __construct()
    {

    }



    public function edit($id, $bso_supplier_id, $financial_policy_id, $segment_id)
    {

        $sk = InsuranceCompanies::where('id', $id)->get()->first();
        $bso_supplier = BsoSuppliers::where('insurance_companies_id', $id)->where('id', $bso_supplier_id)->get()->first();
        $financial_policy = FinancialPolicy::where('id', $financial_policy_id)->get()->first();

        if((int)$segment_id > 0){
            $segment = FinancialPolicySegment::where('id', $segment_id)->get()->first();
        }else{
            $segment = new FinancialPolicySegment();
        }

        //dd($segment->data);

        $data = (strlen($segment->data)>3)?(array)\GuzzleHttp\json_decode($segment->data):null;

        return view('directories.insurance_companies.bso_suppliers.financial_policy.segments.edit', [
            'insurance_companies' => $sk,
            'bso_supplier' => $bso_supplier,
            'financial_policy' => $financial_policy,
            'segment' => $segment,
            'data' => $segment->getDataArray($financial_policy->product->slug, $data)
        ]);

    }


    public function save($id, $bso_supplier_id, $financial_policy_id, $segment_id){

        $sk = InsuranceCompanies::where('id', $id)->get()->first();
        $bso_supplier = BsoSuppliers::where('insurance_companies_id', $id)->where('id', $bso_supplier_id)->get()->first();
        $financial_policy = FinancialPolicy::where('id', $financial_policy_id)->get()->first();


        if($segment_id>0){
            $segment = FinancialPolicySegment::findOrFail($segment_id);
            LogEvents::event($segment->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_UPDATE].' cегмента в ФП:'.$financial_policy->title, 19, $id, $bso_supplier_id, request('data'));
        }else{
            $segment = new FinancialPolicySegment([
                'insurance_companies_id' => $id,
                'bso_supplier_id' => $bso_supplier_id,
                'financial_policy_id' => $financial_policy_id,
            ]);
            LogEvents::event($segment->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_CREATE].' cегмента в ФП:'.$financial_policy->title, 19, $id, $bso_supplier_id, request('data'));
        }


        $segment->product_id = $financial_policy->product_id;
        $segment->data = \GuzzleHttp\json_encode($segment->getDataArray($financial_policy->product->slug, (array)request('data')));
        $segment->title = $segment->getTitleAttribute();
        $segment->save();

        return parentReload();

    }






}
