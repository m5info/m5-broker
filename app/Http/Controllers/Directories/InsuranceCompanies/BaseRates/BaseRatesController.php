<?php

namespace App\Http\Controllers\Directories\InsuranceCompanies\BaseRates;

use App\Http\Controllers\Controller;
use App\Models\Settings\BaseRate;

class BaseRatesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permissions:directories,insurance_companies');
    }

    public function edit($insurance_company_id ,$base_rate)
    {
        $_base_rate = BaseRate::query()->where('id', $base_rate)->first();
        return view('directories.insurance_companies.base_rates.frame', [
            'base_rate' => $_base_rate,
            'insurance_company_id' => $insurance_company_id
        ]);
    }

    public function delete($insurance_company_id, $base_rate)
    {
        $rate = BaseRate::query()->findOrFail($base_rate);
        $rate->delete();
        return response(200);
    }

    public function add_form($insurance_company_id)
    {
        return view('directories.insurance_companies.base_rates.add_form', [
            'insurance_company_id' => $insurance_company_id
        ]);
    }

    public function add($insurance_company_id)
    {
        $rate = new BaseRate();
        $rate->company_id = $insurance_company_id;
        $rate->city_id = request('city_id');
        $rate->rate = getFloatFormat(request('rate'));
        $rate->save();
        return response(200);
    }

    public function update($insurance_company_id, $base_rate)
    {
        $rate = BaseRate::query()->findOrFail($base_rate);
        $rate->city_id = request('city_id');
        $rate->rate = getFloatFormat(request('rate'));
        $rate->save();
        return response(200);
    }

}
