<?php

namespace App\Http\Controllers\Directories\PurposeOfUse;

use App\Http\Controllers\Controller;
use App\Models\Log\LogEvents;
use App\Models\Vehicle\VehicleCategories;
use App\Models\Vehicle\VehiclePurpose;
use Illuminate\Http\Request;

class PurposeOfUseController extends Controller
{

    public function __construct()
    {
        $this->middleware('permissions:directories,purpose_of_use');
    }

    public function index()
    {
        return view('directories.purpose_of_use.index', [
            'purposes' => VehiclePurpose::orderBy('title')->get()
        ]);
    }

    public function create()
    {
        return view('directories.purpose_of_use.create');
    }

    public function edit($id)
    {
        return view('directories.purpose_of_use.edit', [
            'purpose' => VehiclePurpose::findOrFail($id)
        ]);
    }

    public function store(Request $request)
    {
        $purpose = new VehiclePurpose;
        $purpose->save();
        LogEvents::event($purpose->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_CREATE], 6, 0,0, $request->all());

        return $this->save($purpose, $request);
    }

    public function update($id, Request $request)
    {
        $purpose = VehiclePurpose::findOrFail($id);
        LogEvents::event($purpose->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_UPDATE], 6, 0,0, $request->all());
        return $this->save($purpose, $request);
    }

    private function save(VehiclePurpose $category, Request $request)
    {
        $category->title = $request->title;
        $category->save();

        return parentReload();
        return redirect("/directories/purpose_of_use/");
    }

    public function destroy($id)
    {
        LogEvents::event($id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_DELETE], 6);

        VehiclePurpose::findOrFail($id)->delete();

        return response('', 200);
    }

}
