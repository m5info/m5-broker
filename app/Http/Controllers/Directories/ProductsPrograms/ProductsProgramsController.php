<?php

namespace App\Http\Controllers\Directories\ProductsPrograms;

use App\Http\Controllers\Controller;
use App\Models\Directories\InsuranceCompaniesPrograms;
use App\Models\Directories\Products;
use App\Models\Directories\ProductsPrograms;
use App\Models\Log\LogEvents;
use Illuminate\Http\Request;
use Auth;

class ProductsProgramsController extends Controller
{

    public function __construct()
    {
        $this->middleware('permissions:directories,products');
    }

    public function index($id, $program)
    {
        if ((int)$program > 0) {
            $programs = ProductsPrograms::find($program);
        } else {
            $programs = new ProductsPrograms();
            $programs->product_id = $id;
        }

        $sk_services_path = app()->basePath() . '/app/Services/SK';
        $dir_names = array_filter(scandir($sk_services_path), function ($v) {
            return !stristr($v, '.');
        });

        $dir_names = collect($dir_names)->keyBy(function ($item) {
            return $item;
        });


        return view('directories.products.programs.index', [
            'programs' => $programs,
            'product' => $id,
            'dir_names' => $dir_names
        ]);
    }


    public function save($id, $program, Request $request)
    {

        if ((int)$program > 0) {
            $programs = ProductsPrograms::find($program);
        } else {
            $programs = new ProductsPrograms();
            $programs->product_id = $id;
        }

        $programs->title = $request->title;
        $programs->is_actual = (int)$request->is_actual;
        $programs->slug = $request->slug;
        $programs->description = $request->description;
        $programs->dir_name = $request->dir_name;
        $programs->program_id = (int)$request->program_id;

        if ((int)$request->program_id > 0) {
            $class = "App\\Services\\SK\\{$programs->dir_name}\\ServiceController";

            if (class_exists($class)) {
                if ($class::PROGRAMS && isset($class::PROGRAMS[$programs->slug])) {
                    foreach ($class::PROGRAMS[$programs->slug] as $program) {
                        $program = (object)$program;
                        if ($program->id == $request->program_id) {
                            $programs->api_name = $program->name;
                            $programs->template = $program->template;
                        }
                    }

                }
            }
        }


        $programs->save();

        $icp = InsuranceCompaniesPrograms::query()->where('program_id', $programs->id)->first();

        if ((int)$request->is_actual == 1) {
            if ($icp){
                $icp->insurance_company_id = (int)$request->insurance_company;
                $icp->program_id = (int)$programs->id;
                $icp->default = 0;
                $icp->save();
            }else{
                $icp = new InsuranceCompaniesPrograms();
                $icp->insurance_company_id = (int)$request->insurance_company;
                $icp->program_id = (int)$programs->id;
                $icp->default = 0;
                $icp->save();
            }
        }else{
            InsuranceCompaniesPrograms::query()->where('program_id', $programs->id)->delete();
        }

        return parentReload();

    }

    public function destroy($id, $program)
    {
        ProductsPrograms::findOrFail($program)->delete();

        InsuranceCompaniesPrograms::query()->where('program_id', $id)->delete();

        return response('', 200);
    }


    public function get_api_programs($id, $program, Request $request)
    {
        $slug = $request->slug;
        $dir_name = $request->dir_name;

        $result = [];

        $class = "App\\Services\\SK\\{$dir_name}\\ServiceController";

        if (class_exists($class)) {
            if ($class::PROGRAMS && isset($class::PROGRAMS[$slug])) $result = $class::PROGRAMS[$slug];
        }

        return response()->json($result);

    }

}
