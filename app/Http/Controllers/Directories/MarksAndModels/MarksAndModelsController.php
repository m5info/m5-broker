<?php

namespace App\Http\Controllers\Directories\MarksAndModels;

use App\Http\Controllers\Controller;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;
use Illuminate\Http\Request;

class MarksAndModelsController extends Controller
{
    public function marks_and_models()
    {
        $all_marks = VehicleMarks::orderBy('title', 'asc')->get();

        return view('directories.marks_and_models.marks_and_models', [
            'all_marks' => $all_marks,
        ]);

    }

    public function show_models($mark_id)
    {
        $this->breadcrumbs[] = [
            'label' => 'Марки и модели',
            'url' => 'directories\marks_and_models'
        ];

        $mark = VehicleMarks::where('id', $mark_id)->first();

        $this->breadcrumbs[] = [
            'label' => 'Модели ' . $mark->title,
        ];

        $models = VehicleModels::where('mark_id', $mark_id)->orderBy('title')->get();

        return view('directories.marks_and_models.show_models', [
            'models' => $models,
            'mark' => $mark,
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function edit_mark($mark_id, Request $request)
    {
        $mark = VehicleMarks::where('id', $mark_id)->first();

        return view('directories.marks_and_models.edit_mark', [
            'mark' => $mark,
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function edit_model($mark_id, $model_id, Request $request)
    {
        $mark = VehicleMarks::where('id', $mark_id)->first();
        $model = VehicleModels::where('id', $model_id)->first();

        return view('directories.marks_and_models.edit_model', [
            'mark' => $mark,
            'model' => $model
        ]);
    }

    public function update_mark($mark_id, Request $request)
    {
        $mark = VehicleMarks::where('id', $mark_id)->first();
        $mark->title = $request->title;

        $mark->is_rus = isset($request->is_rus) ? 1 : 0;
        $mark->save();

        return parentReload();
    }

    public function update_model($mark_id, $model_id, Request $request)
    {
        $model = VehicleModels::where('id', $model_id)->first();
        $model->title = $request->title;
        $model->category_id = $request->category_id;
        $model->save();

        return parentReload();
    }

    public function mark_create()
    {
        return view('directories.marks_and_models.mark_create');
    }

    public function mark_save(Request $request)
    {
        $mark = new VehicleMarks;
        $mark->id = $request->id;
        $mark->title = $request->title;
        $mark->category_id = $request->category_id;
        $mark->is_rus = $request->is_rus;
        $mark->save();

        return parentReload();
    }

    public function model_create($mark_id)
    {
        $mark = VehicleMarks::findOrFail($mark_id);

        return view('directories.marks_and_models.model_create', [
            'mark' => $mark
        ]);
    }

    public function model_save($mark_id, Request $request)
    {

        $mark = VehicleMarks::findOrFail($mark_id);
        $models_count = VehicleModels::where('mark_id', '=', $mark_id)->count();

        $model = new VehicleModels;
        $model->mark_id = $mark->id;
        $model->id = $mark->id * 1000 + ($models_count + 1);
        $model->title = $request->title;
        $model->category_id = $request->category_id;

        $model->save();

        return parentReload();
    }

    public function mark_delete()
    {
        $mark = VehicleMarks::findOrFail(request('id'));
        $mark->delete();
    }

    public function model_delete()
    {
        $mark = VehicleModels::findOrFail(request('id'));
        $mark->delete();
    }


}