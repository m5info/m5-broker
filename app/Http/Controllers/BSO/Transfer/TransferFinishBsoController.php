<?php

namespace App\Http\Controllers\BSO\Transfer;

use App\Domain\Processes\Operations\Bso\TransferBso;
use App\Http\Controllers\Controller;

use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoCarts;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\User;
use Illuminate\Http\Request;

class TransferFinishBsoController extends Controller
{


    public function transfer_bso_act_agent(Request $request)
    {
        $act = BsoActs::findOrFail($request->bso_act_id);
        $bsos = BsoLogs::where('bso_act_id', $act->id)->groupby('bso_id')->distinct()->get();


        $bso_cart = new BsoCarts();
        $bso_cart->bso_cart_type = 1;
        $bso_cart->time_create = $act->time_create;
        $bso_cart->user_id_from = $act->user_id_from;
        $bso_cart->bso_manager_id = $act->bso_manager_id;
        $bso_cart->curr_tp_id = $act->curr_tp_id;
        $bso_cart->tp_id = $act->tp_id;
        $bso_cart->user_id_to = $request->agent_id;
        $bso_cart->save();

        foreach ($bsos as $tempBSO){
            $bso_items = $tempBSO->bso;
            $bso_items->is_reserved = 1;
            $bso_items->bso_cart_id = $bso_cart->id;
            $bso_items->save();
        }

        return redirect("/bso/transfer/transfer_bso/?bso_cart_id={$bso_cart->id}");
    }


    public function transfer_bso(Request $request)
    {
        $bso_cart_id = (int)$request->bso_cart_id;

        return TransferBso::handle($bso_cart_id);

    }

}
