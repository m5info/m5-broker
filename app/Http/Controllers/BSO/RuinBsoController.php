<?php

namespace App\Http\Controllers\BSO;

use App\Classes\Export\ExportManager;
use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoActsItems;
use App\Models\BSO\BsoItem;
use App\Models\Settings\TemplateCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RuinBsoController extends Controller
{
    public function index(Request $request)
    {
        return view('bso.ruin_bso.index');
    }

    public function ruin(Request $request)
    {
        $data = $request->getContent();
        parse_str($data, $bsos);

        if (isset($bsos['bso_for_ruin'])){
            $bsos = $bsos['bso_for_ruin'];

            foreach ($bsos as $bsoId){
                $bso = BsoItem::find($bsoId);

                $bso->update([
                    'state_id' => 3, // испорчен
                    'location_id' => 4 // принят от агента
                ]);
            }

            return $this->getAgentBsos($bsos);
        }

        return frameError('Выберите БСО!');
    }


    public function getAgentBsos($bsoIds)
    {
        $bsos = BsoItem::whereIn('id', $bsoIds)->get();

        $bsosByAgent = [];

        foreach($bsos as $bso){
            $bsosByAgent[$bso->agent_id][] = $bso->id;
        }

        return $bsosByAgent;
    }

    public function export($agentId, Request $request)
    {
        $bsos = explode(',', $request->bsos);

        $bso_acts = BsoActs::select((\DB::raw('max(act_number_int) as max_act_number')))->get()->first();
        $log_time = date( 'Y-m-d H:i:s' );
        $bso_state_id = 0;
        $bso_manager_id = $user_id_to = $agentId;
        $ip_address     = $_SERVER['REMOTE_ADDR'];

        $act = BsoActs::create([
            'time_create' => $log_time,
            'type_id' => '7',
            'tp_id' => 0,
            'user_id_from' => $bso_manager_id,
            'user_id_to' => auth()->id(),
            'bso_manager_id' => auth()->id(),
            'location_from' => 1,
            'location_to' => 0,
            'bso_state_id' => $bso_state_id,
            'act_number' => $bso_acts->max_act_number ? (int)$bso_acts->max_act_number + 1 : 1,
            'act_number_int' => $bso_acts->max_act_number ? (int)$bso_acts->max_act_number + 1 : 1
        ]);


        $bso_act_id = $act->id;

        $sql = "insert into bso_logs (log_time, bso_id, bso_act_id, bso_state_id, bso_location_id, bso_user_from, bso_user_to, user_id, ip_address)
	SELECT '$log_time', id, '$bso_act_id', '$bso_state_id', '1', '$bso_manager_id', '$user_id_to', '$bso_manager_id', '$ip_address' from bso_items where id in ({$request->bsos})";
        \DB::insert($sql);

        foreach (BsoItem::whereIn('id', $bsos)->get() as $bso)
        {
            BsoActsItems::create([
                'bso_act_id' => $act->id,
                'bso_id' => $bso->id,
                'bso_title' => $bso->bso_title,
            ]);
        }

        return (new ExportManager(TemplateCategory::get('damaged_bso_from_agent'), BsoActs::where('id', $act->id)))->handle();
    }

    public function show_error(Request $request)
    {
        return frameError($request->getContent());
    }

    public function get_table(Request $request)
    {
        parse_str($request->found_bsos, $bsos);

        if (isset($bsos['bso_for_ruin'])){
            $bsos = $bsos['bso_for_ruin'];
        }
        $bso = BsoItem::getBso();
        $bso->where('state_id', '=', 0); // чистые
        $bso->where('location_id', '=', 1); // передан агенту

        if($request->agent_id != -1){ // Агент
            $bso->where('agent_id', '=', $request->agent_id);
        }

        if($request->bso_title != 0){
            $find = $request->bso_title;
            $bso->where(function ($query) use ($find) {
                return $query->where('bso_title', 'like', "%{$find}%")
                    ->orWhere('bso_blank_title', 'like', "%{$find}%");
            });
        }

        /*//для пагинации
        $result = [
            'perpage' => 10,
            'page' => 1,
        ];

        if ($request->page_count > 0) {
            $result['perpage'] = $request->page_count;
        }

        if ($request->page > 0) {
            $result['page'] = $request->page;
        }*/

        /*$bso->offset($result['perpage'] * ($result['page'] - 1));
        $bso->limit($result['perpage']);*/
        $result['count_result'] = $bso->count();

        $bso->orWhereIn('id',$bsos);

        $result['count'] = $bso->count();
        $result['html'] = view('bso.ruin_bso.table', ['bsos' => $bso->get()])->render();

        return $result;
    }
}
