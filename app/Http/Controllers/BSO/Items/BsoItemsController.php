<?php

namespace App\Http\Controllers\BSO\Items;

use App\Domain\Processes\Operations\Payments\TransactionReferencerOnAccept;
use App\Http\Controllers\Controller;
use App\Models\BSO\BsoComments;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Contracts;
use App\Models\Directories\BsoDopSerie;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\TypeBso;
use App\Models\User;
use App\Models\Users\Permission;
use Illuminate\Http\Request;

class BsoItemsController extends Controller
{

    public function __construct()
    {
        $this->middleware('permissions:bso,items');
        $this->breadcrumbs[] = [
            'label' => 'Бсо',
        ];
        
        $this->breadcrumbs[] = [
            'label' => 'Инвентаризация БСО',
            'url' => 'bso/inventory_bso',
        ];
    }

    public function index($id)
    {

        $bso = BsoItem::find($id);

        if(!$bso){
            abort(404);
        }

        $agents = User::getALLUser(24)->pluck('name', 'id');

        $roles_ids = array_column(Permission::where('title', '=', 'is_referencer')->first()->roles->toArray(),'id');

        $referencers = User::query()->whereIn('role_id',$roles_ids)->get();

        $this->breadcrumbs[] = [
            'label' => $bso->bso_title,
        ];
        
        return view('bso.items.index', [
            'bso' => $bso,
            'agents' => $agents,
            'referencers' => $referencers,
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function supplier_org($id)
    {
        $bso = BsoItem::find($id);
        $supplier = BsoSuppliers::where('is_actual', 1)->where('insurance_companies_id', $bso->insurance_companies_id);

        return view('bso.items.edit_supplier_org', [
            'bso' => $bso,
            'supplier' => $supplier->get()
        ]);
    }


    public function edit_supplier_org($id, Request $request)
    {
        $bso = BsoItem::find($id);
        $supplier = BsoSuppliers::find($request->bso_suppliers_id);
        $bso->bso_supplier_id = $supplier->id;
        $bso->org_id = $supplier->purpose_org_id;
        $bso->save();

        return parentReload();
    }


    public function accept_status($id)
    {
        $bso = BsoItem::find($id);

        return view('bso.items.edit_contract_status', [
            'bso' => $bso,
        ]);
    }


    public function edit_accept_status($id, Request $request)
    {
        $bso = BsoItem::find($id);

        if(isset($request->kind_acceptance) && $contract = $bso->contract){

            if($contract->kind_acceptance == 1 && $request->kind_acceptance != 1 && $contract->sales_condition == 1){
                //если при продаже организации договор меняет акцепт с безусловного на любой другой, возвращаем кв рекомендодателя по последнему платежу, если ранее было начисление
                TransactionReferencerOnAccept::store_transaction($contract);
            }

            $contract->kind_acceptance = $request->kind_acceptance;

            if($request->kind_acceptance == 0 || $request->kind_acceptance == 3){
                $contract->statys_id = 1;
            }else if($request->kind_acceptance == 1 || $request->kind_acceptance == 2){
                $contract->statys_id = 4;
            }
            $contract->save();
        }

        return parentReload();
    }

    public function save_status($id, Request $request){

        $bso = BsoItem::find($id);

        $bso->state_id = $request->state_id;
        $bso->location_id = $request->location_id;
        if(\request()->has('agent_id') && $contract = $bso->contract){
            $bso->agent_id = (int)$request->agent_id;
            $contract->agent_id = (int)$request->agent_id;
        }

        if($request->comment){
            BsoComments::create([
                'bso_id' => $id,
                'comment' => $request->comment,
            ]);
        }

        $bso->push();

        return parentReload();
    }


    public function bso_title($id)
    {
        $bso = BsoItem::find($id);
        $bso_type = TypeBso::where('type_bso.is_actual', 1)
            ->where('type_bso.insurance_companies_id', $bso->insurance_companies_id)
            ->orderBy('title', 'asc')
            ->get();

        $bso_serie = BsoSerie::where('insurance_companies_id', $bso->insurance_companies_id)
            ->where('type_bso_id', $bso->type_bso_id)->get();

        $bso_dop_serie = BsoDopSerie::where('bso_serie_id', $bso->bso_serie_id)->get();


        return view('bso.items.bso_title', [
            'bso' => $bso,
            'bso_type' => $bso_type->pluck('title', 'id'),
            'bso_serie' => $bso_serie->pluck('bso_serie', 'id'),
            'bso_dop_serie' => $bso_dop_serie->pluck('bso_dop_serie', 'id'),
        ]);
    }


    public function edit_bso_title($id, Request $request)
    {
        //Изменять номер БСО
        $bso = BsoItem::find($id);

        $bso_serie = BsoSerie::find($request->bso_serie_id);
        $bso_dop_serie = BsoDopSerie::find($request->bso_dop_serie_id);


        $bso->type_bso_id = $request->bso_type;
        $bso->bso_class_id = $request->bso_class_id;

        $bso->bso_serie_id = $bso_serie->id;
        $bso->bso_number = $request->bso_number;
        $bso->bso_dop_serie_id = ($bso_dop_serie)?$bso_dop_serie->id:0;

        $bso_title = $bso_serie->bso_serie.' '.$bso->bso_number;

        if(isset($request->apply_class_to_all) && $request->apply_class_to_all == 1) {
            BsoItem::where('bso_supplier_id', $bso->bso_supplier_id)
                ->where('insurance_companies_id', $bso->insurance_companies_id)
                ->where('org_id', $bso->org_id)
                ->where('bso_serie_id', $bso->bso_serie_id)
                ->update(['type_bso_id' => $request->bso_type, 'bso_class_id' => $bso->bso_class_id]);
        }

        if($bso_dop_serie){
            $bso_title .= $bso_dop_serie->bso_dop_serie;
        }

        $bso->bso_title = $bso_title;
        if (isset($bso->type) && isset($bso->type->product_id)) {
            $productId = $bso->type->product_id;
            $bso->product_id = $productId;
            if ($productId == 0) { // если тип бсо квитанция
                if ($bso->all_payments && $bso->all_payments->count()) {
                    foreach ($bso->all_payments as $payment) {
                        $payment->bso_receipt = $bso_title;
                    }
                }
            } else {
                if ($contract = $bso->contract) {
                    $contract->product_id = $productId;
                    $contract->bso_title = $bso_title;
                    $contract->save();
                }
            }
        }

        $bso_blank_dop_serie = BsoDopSerie::find($request->bso_blank_dop_serie_id);

        if($request->bso_blank_serie_id > 0){

            $bso_blank_serie = BsoSerie::find($request->bso_blank_serie_id);

            $bso_blank_title = $bso_blank_serie->bso_serie.' '.$request->bso_blank_number;

            if($bso_blank_dop_serie){
                $bso_blank_title .= $bso_blank_dop_serie->bso_dop_serie;
                $bso->bso_blank_dop_serie_id = $bso_blank_dop_serie->id;
            }

            $bso->bso_blank_serie_id = $bso_blank_serie->id;
            $bso->bso_blank_title = $bso_blank_title;

            $bso->bso_blank_number = $request->bso_blank_number;


        }else{
            $bso->bso_blank_serie_id = null;
            $bso->bso_blank_title = null;
            $bso->bso_blank_number = null;
            $bso->bso_blank_dop_serie_id = null;
        }




        $bso->save();


        return parentReload();

    }


    public function sendContractFront($id, Request $request)
    {
        $contract = Contracts::getContractId($request->contract_id);
        $contract->sendDataToFront();

        return response(200, 200);
    }

    public function destroyContractFront($id, Request $request)
    {
        $contract = Contracts::getContractId($request->contract_id);
        $contract->destroyDataToFront();

        return response(200, 200);
    }


    public function clearAllContract($id, Request $request)
    {
        $bso = BsoItem::find($id);

        if($bso->contract && $bso->contract->statys_id > 0 && auth()->user()->hasPermission('reports', 'payment_delete')){

            $removed_bso = $bso->saveDataBeforeRemoving();

            $contract = $bso->contract;
            $contract->saveDataBeforeRemoving($removed_bso->id);

            foreach ($contract->payments as $payment){
                $payment->saveDataBeforeRemoving($removed_bso->id);
                $payment->deletePayment();
            }

            $contract->destroyDataToFront();
            $contract->statys_id = -1;
            $contract->save();

            $bso->state_id = 0;
            $bso->save();

        }

        return redirect("/bso/items/{$id}/");
    }




}
