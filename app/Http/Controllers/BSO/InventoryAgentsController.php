<?php

namespace App\Http\Controllers\BSO;

use App\Helpers\PaginationHelper;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\BSO\AgentBsoListQueryHandler;
use App\Models\BSO\BsoItem;
use App\Models\Characters\Agent;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class InventoryAgentsController extends Controller {

    public function __construct() {
        $this->middleware('permissions:bso,inventory_agents');
        $this->breadcrumbs[] = [
            'label' => 'Инвентаризация по агентам',
            'url' => 'bso/inventory_agents'
        ];
        $this->limits = [ // количество дней хранения полисов у агентов
            1 => Agent::DAYS_LIMITS[1],
            2 => Agent::DAYS_LIMITS[2]
        ];
    }

    public function index() {

        if(auth()->user()->role->visibility('bso') == 2){
            return $this->details();
        }
        return view('bso.inventory.agents.index');
    }

    public function agents_list(){

        $request = request()->all();

        $bso_items = BsoItem::getBso('user_id');
        $bso_items->where('location_id', 1);
        $agents = Agent::getAgents()->orderBy('name');//Agent::query()->with('perent');


        if (isset($request['agent_id']) && $request['agent_id'] >= 0) {
            $agents->where('id', '=', $request['agent_id']);
        }
        if (isset($request['nop_id']) && $request['nop_id'] >= 0) {
            $agents->where('users.parent_id', '=', $request['nop_id']);
        }

        $agents = $agents->get()->keyBy('id');

        $bso_items = $bso_items->whereIn('user_id', $agents->pluck('id')->toArray());



        if (isset($request['point_sale_id']) && $request['point_sale_id'] >= 0) {
            $bso_items->where('point_sale_id', '=', $request['point_sale_id']);
        }

       /* if (isset($request['type_bso_id']) && $request['type_bso_id'] >= 0) {
            $bso_items->where('type_bso_id', '=', $request['type_bso_id']);
        }*/

        if (isset($request['product_id']) && $request['product_id'] >= 0) {
            $bso_items->where('product_id', '=', $request['product_id']);
        }

        if (isset($request['insurance_companies_id']) && $request['insurance_companies_id'] >= 0) {
            $bso_items->where('insurance_companies_id', '=', $request['insurance_companies_id']);
        }

        $bso_items = $bso_items->get()->groupBy('user_id');

        foreach($agents as $k => $agent){
            if(!$bso_items->has($k)){
                $agents->forget($k);
            }
        }

        return [
            'agents' => $agents,
            'bso_items' => $bso_items,
            'limits' => $this->limits
        ];
    }


    public function get_agents_table(){
        $data = $this->agents_list();
        $data['html'] = view('bso.inventory.agents.agents_table', $data)->render();
        return $data;
    }

    public function get_agents_full_table(){
        $data = $this->agents_list_with_bso();
        $data['html'] = view('bso.inventory.agents.agents_table_full', $data)->render();
        return $data;
    }

    public function agents_list_with_bso(){

        $request = request()->all();
        $request = $request['param'];
        $bso_items = BsoItem::getBso('user_id');

        $bso_items->where('location_id', 1);
        $agents = Agent::getAgents()->orderBy('name');//Agent::query()->with('perent');

        if (isset($request['agent_id']) && $request['agent_id'] >= 0) {
            $agents->where('id', '=', $request['agent_id']);
        }
        if (isset($request['nop_id']) && $request['nop_id'] >= 0) {
            $agents->where('users.parent_id', '=', $request['nop_id']);
        }

        $agents = $agents->get()->keyBy('id');

        $bso_items = $bso_items->whereIn('user_id', $agents->pluck('id')->toArray());



        if (isset($request['point_sale_id']) && $request['point_sale_id'] >= 0) {
            $bso_items->where('point_sale_id', '=', $request['point_sale_id']);
        }

        /* if (isset($request['type_bso_id']) && $request['type_bso_id'] >= 0) {
             $bso_items->where('type_bso_id', '=', $request['type_bso_id']);
         }*/

        if (isset($request['product_id']) && $request['product_id'] >= 0) {
            $bso_items->where('product_id', '=', $request['product_id']);
        }

        if (isset($request['insurance_companies_id']) && $request['insurance_companies_id'] >= 0) {
            $bso_items->where('insurance_companies_id', '=', $request['insurance_companies_id']);
        }

        $bso_items = $bso_items->get()->groupBy('user_id');

        foreach($agents as $k => $agent){
            if(!$bso_items->has($k)){
                $agents->forget($k);
            }
        }

        return [
            'agents' => $agents,
            'bso_items' => $bso_items,
            'limits' => $this->limits
        ];
    }

    public function reports_table_export () {
        $data = $this->agents_list_with_bso();
        Excel::create('Инфо по агентам'.date('Y-m-d H:i:s'), function($excel) use ($data){

            $excel->sheet('Лист', function($sheet) use ($data) {
                $sheet->loadView('bso.inventory.agents.agents_table_full', $data);
                $sheet->setWidth(array('A' => 17, 'B' => 26, 'C' =>  70, 'D' => 16, 'E' => 10, 'F' => 20, 'G' => 10, 'H' => 15, 'I' => 10, 'J' => 35, 'K' => 35, 'L' => 35));
                $sheet->getStyle('1')->getAlignment()->setWrapText(true);
                $sheet->getStyle('A:I')->getAlignment()->setWrapText(true);
                $sheet->freezeFirstRow();
                $sheet->setHeight(1, 50);
                $sheet->setAllBorders('thin');
            });

        })->export('xlsx');
    }

    public function details() {

        $type = request()->has('types') ? request()->get('types') : 'bso_in';
        switch ($type) {
            case 'bso_in';
                $title = 'Всего';
                break;
            case 'bso_in_limit1';
                $title = 'Всего старых (более ' . $this->limits[1]  . ' дней)';
                break;
            case 'bso_in_limit2';
                $title = 'Всего старых (более ' . $this->limits[2]  . ' дней)';
                break;
            default;
                $title = '';
        }

        $this->breadcrumbs[] = [
            'label' => $title,
        ];


       // $data = $this->details_list();


        return view('bso.inventory.agents.details', [
            'breadcrumbs' => $this->breadcrumbs,
            //'data' => $data,
            'title' => $title,
            'limits' => $this->limits
        ]);
    }


    public function details_list(){


        $this->validate(request(), [
            "agent_id"      => "integer",
            "point_sale_id" => "integer",
            "type_bso_id"   => "integer",
            "product_id"   => "integer",
            "insurance_companies_id"   => "integer",
            "state_id"      => "integer",
            "types"         => "string",
        ]);



        $bso_items = (new AgentBsoListQueryHandler(BsoItem::getBso('user_id')))->allowEmpty()->apply();
        $bso_items->where('location_id', 1)
            ->with('supplier_org','supplier','product','point_sale','user','bso_states','bso_locations','act_sk','type');
        $page = request('PAGE') > 0 ? (int)request('PAGE') : 1;
        $page_count = request('page_count') > 0 ? (int)request('page_count') : 10;
        $page_count = request('page_count') == -1 ? 9999 : $page_count;

        $result = PaginationHelper::paginate($bso_items, $page, $page_count);

        $acts = $result['builder']->orderBy('time_create', 'desc')->get();

        return [
            'acts' => $acts,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];

    }


    public function get_details_table() {


        $data = $this->details_list();
        $data['html'] = view('bso.inventory.agents.details_table', $data)->render();
        return $data;
    }


    public function refresh_agent_date(Request $request)
    {
        $bsoIds = \request('bso_ids', []);

        if(count($bsoIds)){
            foreach($bsoIds as $bsoId){
                $bso = BsoItem::find($bsoId);

                $bso->transfer_to_agent_time = date('Y-m-d H:i:s');
                $bso->save();
            }
        }

        return response('OK', 200);
    }
}
