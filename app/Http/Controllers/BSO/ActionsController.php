<?php

namespace App\Http\Controllers\BSO;

use App\Http\Controllers\Controller;

use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ObjectInsurer;
use App\Models\Contracts\Orders;
use App\Models\Contracts\Payments;
use App\Models\Contracts\PrimaryAutosaveContracts;
use App\Models\Directories\BsoDopSerie;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\HoldKv;
use App\Models\Directories\InstallmentAlgorithms;
use App\Models\Directories\Products;
use App\Models\Directories\TypeBso;
use App\Models\Settings\InstallmentAlgorithmsList;
use App\Services\Front\Integration;
use App\Services\Front\IntegrationFront;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ActionsController extends Controller
{



    public function get_bso_type(Request $request){

        $bso_supplier = BsoSuppliers::findOrFail($request->bso_supplier_id);

        $bso_type = TypeBso::where('type_bso.is_actual', 1)
            ->whereIn('type_bso.id', function($query) use ($bso_supplier) {
                $query->select(\DB::raw('bso_serie.type_bso_id'))
                    ->from('bso_serie')
                    ->where('bso_serie.bso_class_id', 1)
                    ->where('bso_serie.insurance_companies_id', $bso_supplier->insurance_companies_id);

            })
            ->orderBy('title', 'asc')
            ->get();

        return response()->json($bso_type);

    }

    public function get_bso_product(Request $request){

        $bso_supplier = BsoSuppliers::findOrFail($request->bso_supplier_id);

        $bso_type = TypeBso::where('type_bso.is_actual', 1)
            ->whereIn('type_bso.id', function($query) use ($bso_supplier) {
                $query->select(\DB::raw('bso_serie.type_bso_id'))
                    ->from('bso_serie')
                    ->where('bso_serie.bso_class_id', '!=', 100)
                    ->where('bso_serie.insurance_companies_id', $bso_supplier->insurance_companies_id);

            })
            ->orderBy('title', 'asc')
            ->get();

        return response()->json($bso_type);

    }


    public function get_supplier_product(Request $request){

        $bso_supplier = BsoSuppliers::findOrFail($request->bso_supplier_id);

        $hold_kv = HoldKv::query()
        ->where('bso_supplier_id', $request->bso_supplier_id);

        if($request->e_product){
            $hold_kv->where('is_epolicy', 1);
        }
        $productIds = $hold_kv->groupBy('product_id')->get()->pluck('product_id');

        $products = TypeBso::where('type_bso.is_actual', 1)
            ->whereIn('type_bso.id', function($query) use ($bso_supplier) {
                $query->select(\DB::raw('bso_serie.type_bso_id'))
                    ->from('bso_serie')
                    ->where('bso_serie.bso_class_id', '!=', 100)
                    ->where('bso_serie.insurance_companies_id', $bso_supplier->insurance_companies_id);
            })

            ->whereIn('type_bso.product_id', $productIds)
            ->orderBy('type_bso.title', 'asc');

        return response()->json($products->get());

    }

    public function get_series(Request $request){
        $bso_supplier = BsoSuppliers::findOrFail($request->bso_supplier_id);
        $bso_serie = BsoSerie::where('insurance_companies_id', $bso_supplier->insurance_companies_id)
            ->where('type_bso_id', $request->bso_type_id);

        if(isset($request->is_electronic)){
            $bso_serie->where('bso_class_id', 1);
        }

        return response()->json($bso_serie->get(['bso_serie', 'id'])->toArray());

    }

    public function get_dop_series(Request $request)
    {
        $bso_dop_serie = BsoDopSerie::where('bso_serie_id', $request->series_id)->get(['bso_dop_serie', 'id'])->toArray();
        return response()->json($bso_dop_serie);

    }

    public function bso_number_to(Request $request)
    {
        $number_to = '';
        $number_from = isset( $request->bso_num ) ? $request->bso_num : '';
        $bso_qty     = isset( $request->bso_qty ) ? (int) str_replace( ' ', '', $request->bso_qty ) : 0;
        if ( $bso_qty == 0 || $number_from == '' ) {

        }else{
            if ( strlen( $number_from ) > 15 ) {// длинные номера бсо больше max int, поэтому обрезаем

                $number_tmp = substr( $number_from, - ( (int) ( strlen( $number_from ) / 2 ) ) );

                $number_tmp += $bso_qty - 1;

                $number_to = substr( $number_from, 0, ( (int) ( strlen( $number_from ) / 2 ) ) +1) . $number_tmp;


            } else {
                $number_to = (int) $number_from + $bso_qty - 1;
                $number_to = str_pad( $number_to, strlen( $number_from ), '0', STR_PAD_LEFT );

            }
        }

        return response($number_to);

    }


    public function create_transfer_act(Request $request)
    {

        $act_number = $request->act_number;
        $bso_supplier_id = $request->bso_supplier_id;
        $point_sale_id = $request->point_sale_id;
        $bso_manager_id = auth()->id();

        $time_create = date('Y-m-d H:i:s');
        $user_id_from = $bso_manager_id;
        $user_id_to = 0;
        $location_from = $point_sale_id; // Склад БСО
        $location_to = 1; // Агент
        $bso_state_id = 0;


        $bso_acts = BsoActs::select((\DB::raw('max(act_number_int) as max_act_number')))->get()->first();
        if($bso_acts->max_act_number){
            $act_number_int = (int)$bso_acts->max_act_number + 1;
        }else{
            $act_number_int = 1;
        }

        if(strlen($act_number) == 0){
            $act_number = str_pad($act_number_int, 6, '0', STR_PAD_LEFT);
        }

        $acts = BsoActs::create([
            'time_create' => $time_create,
            'type_id' => '0',
            'tp_id' => $point_sale_id,
            'user_id_from' => $user_id_from,
            'user_id_to' => $user_id_to,
            'bso_manager_id' => $bso_manager_id,
            'location_from' => $location_from,
            'location_to' => $location_to,
            'bso_state_id' => $bso_state_id,
            'act_number' => $act_number,
            'act_number_int' => $act_number_int,
        ]);



        return response($acts->id);

    }

    public function get_bso_sold(Request $request)
    {
        $json = \GuzzleHttp\json_decode($request->getContent());
        $count = $json->count;
        $like_query = $json->query;
        $type_bso = (int)$json->type_bso;// 1 БСО 2 квитанция
        $state = $json->state;
        $checkAlgo = $json->checkAlgo;
        $bso_supplier_id = isset($json->bso_supplier_id) ? (int)$json->bso_supplier_id : 0;
        $bso_agent_id = isset($json->bso_agent_id) ? (int)$json->bso_agent_id : 0;
        $bso_used = $json->bso_used;

        $res = [];
        $res["suggestions"] = [];

        $bso_items = BsoItem::getBsoSold();

        $bso_items->whereNotIn('bso_items.bso_title', $bso_used);
        //$bso_items->where('location_id', '=',1);

        if(is_array($state)){
            $bso_items->whereIn('bso_items.state_id', $state);
        }else {
            $bso_items->where('bso_items.state_id', '=', $state);
        }

        if($type_bso == 1) $bso_items->where('bso_items.bso_class_id', '!=', 100);
        if($type_bso == 2){
            $bso_items->where('bso_items.bso_class_id', '=',100);
            if($bso_supplier_id > 0) $bso_items->where('bso_items.bso_supplier_id', $bso_supplier_id);
            if($bso_agent_id > 0) $bso_items->where('bso_items.agent_id', $bso_agent_id);
        }

        if(isset($request['bso_items.product_id'])){
            $bso_items->where('bso_items.product_id', $request['bso_items.product_id']);
        }


        $bso_items->where(function($query) use ($like_query)
        {
            $query->where('bso_items.bso_number', 'like', "%{$like_query}%")
                ->orWhere('bso_items.bso_title', 'like', "%{$like_query}%");
        });

        $bso_items->orderBy('bso_items.state_id');
        $bso_items->limit($count);

        foreach ($bso_items->get() as $bso)
        {
            $data = [];
            $data["value"] = $bso->bso_title;
            $data["unrestricted_value"] = $bso->bso_title;
            $data["data"] = [];
            $data["data"]['bso_type'] = $bso->type->title.' ('.$bso->bso_states->title.')';
            $data["data"]['bso_title'] = $bso->bso_title;
            $data["data"]['bso_sk'] = $bso->supplier->title;
            $data["data"]['agent_name'] = ($bso->user)?$bso->user->name:'';

            $data["data"]['bso_id'] = $bso->id;
            $data["data"]['bso_supplier_id'] = $bso->bso_supplier_id;
            $data["data"]['insurance_companies_id'] = $bso->insurance_companies_id;
            $data["data"]['product_id'] = $bso->product_id;
            $data["data"]['agent_id'] = $bso->user_id;

            if($contract = $bso->contract){

                $data["data"]['agent_id'] = auth()->user()->is('under') ? $contract->agent_id : auth()->id();

                $data["data"]['contract']['id'] = $contract->id;
                $data["data"]['contract']['statys_id'] = $contract->statys_id;
                $data["data"]['contract']['sign_date'] = setDateTimeFormatRu($contract->sign_date, 1);
                $data["data"]['contract']['begin_date'] = setDateTimeFormatRu($contract->begin_date, 1);
                $data["data"]['contract']['end_date'] = setDateTimeFormatRu($contract->end_date, 1);
                $data["data"]['contract']['manager_id'] = $contract->manager_id;
                $data["data"]['contract']['financial_policy_id'] = $contract->financial_policy_id;
                $data["data"]['contract']['payment_total'] = titleFloatFormat($contract->payment_total);
                $data["data"]['contract']['payment_number'] = (int)$contract->paymentsOnlyInstallment()->count()+1;

                if($last_payment = $contract->payments->where('type_id', 0)->where('payment_number', 1)->last()){
                    $data["data"]['contract']['informal_discount'] = titleFloatFormat($last_payment->informal_discount);
                    $data["data"]['contract']['official_discount'] = titleFloatFormat($last_payment->official_discount);
                    $data["data"]['contract']['bank_kv'] = titleFloatFormat($last_payment->bank_kv);
                }

                if($insurer = $contract->insurer){
                    $data["data"]['contract']['insurer_type'] = $insurer->type;
                    $data["data"]['contract']['insurer_title'] = $insurer->title;
                }

                if($object_insurer_auto = $contract->object_insurer_auto){
                    $data["data"]['contract']['object_insurer_ts_category'] = $object_insurer_auto->ts_category;
                    $data["data"]['contract']['object_insurer_ts_power'] = $object_insurer_auto->power;
                    $data["data"]['contract']['object_insurer_ts_reg_number'] = $object_insurer_auto->reg_number;
                    $data["data"]['contract']['object_insurer_ts_mark_id'] = $object_insurer_auto->mark_id;
                    $data["data"]['contract']['object_insurer_ts_model_id'] = $object_insurer_auto->model_id;
                    $data["data"]['contract']['object_insurer_ts_vin'] = $object_insurer_auto->vin;
                }
            }

            $available = true;

            if($checkAlgo){ // если нужно проверить алгоритмы рассрочки
                $available = false;

                if($contract = $bso->contract){
                    if($insurance_companies_algorithm = $contract->insurance_companies_algorithm){
                        if($algorithm = $insurance_companies_algorithm->algorithm){
                            $count = $algorithm->quantity; // кол-во сколько должно быть по алгоритму
                            $count_already = $contract->paymentsOnlyInstallment()->count(); // кол-во сколько уже есть
                            if($count_already < $count){
                                $available = true;
                            }
                        }
                    }
                }
            }

            if($available){
                $res["suggestions"][] = $data;
            }
        }

        return response()->json($res);
    }

    public function get_clear_bso(Request $request)
    {
        $json = \GuzzleHttp\json_decode($request->getContent());
        $count = $json->count;
        $like_query = $json->query;

        $type_selector = isset($json->type_selector) ? $json->type_selector : 0;// Тип БСО
        $series_selector = isset($json->series_selector) ? $json->series_selector : 0; //СЕРИИЯ
        $bso_cart_type = isset($json->bso_cart_type) ? $json->bso_cart_type : 0; //Тип корзины
        $bso_supplier_id = (int)$json->bso_supplier_id;

        $bso_items = BsoItem::getBso();

        $bso_items->where('state_id', '=',0);
        $bso_items->where('bso_supplier_id', $bso_supplier_id);

        if($type_selector > 0){
            $bso_items->where('type_bso_id', $type_selector);
        }

        if($series_selector > 0){
            $bso_items->where('bso_serie_id', $series_selector);
        }

        if($bso_cart_type > 0){
            if($bso_cart_type == 2){ // Передача от агента-агенту
                $bso_items->where('location_id', '=', 1); // Передан агенту
            }
        }else {
            $bso_items->where('location_id', '=',0);
        }

        $bso_items->where(function($query) use ($like_query)
        {
            $query->where('bso_number', 'like', "%{$like_query}%")
                ->orWhere('bso_title', 'like', "%{$like_query}%");
        });


        $bso_items->limit($count);

        $res = [];
        $res["suggestions"] = [];

        foreach ($bso_items->get() as $bso)
        {
            $data = [];

            $bso_title = $bso->bso_title;
            $data["value"] = $bso->bso_title;
            $data["unrestricted_value"] = $bso->bso_title;
            $data["data"] = [];
            $data["data"]['bso_type'] = $bso->type->title.' ('.$bso->bso_states->title.')';
            $data["data"]['bso_title'] = $bso->bso_title;
            $data["data"]['bso_sk'] = $bso->supplier->title;
            $data["data"]['agent_name'] = ($bso->user)?$bso->user->name:'';

            $data["data"]['bso_id'] = $bso->id;
            $data["data"]['bso_supplier_id'] = $bso->bso_supplier_id;
            $data["data"]['insurance_companies_id'] = $bso->insurance_companies_id;
            $data["data"]['product_id'] = $bso->product_id;
            $data["data"]['agent_id'] = $bso->user_id;

            $data["data"]['type_bso_id'] = $bso->type_bso_id;
            $data["data"]['bso_serie_id'] = $bso->bso_serie_id;
            $data["data"]['bso_number'] = $bso->bso_number;


            $bso_serie = BsoSerie::where('insurance_companies_id', $bso->insurance_companies_id)
                ->where('type_bso_id', $bso->type_bso_id);

            $data["data"]['select_bso_serie'] = $bso_serie->get(['bso_serie', 'id'])->toArray();

            $res["suggestions"][] = $data;
        }

        return response()->json($res);
    }

    public function get_bso_order(Request $request)
    {
        $json = \GuzzleHttp\json_decode($request->getContent());
        $count = $json->count;
        $like_query = $json->query;

        $series_selector = isset($json->series_selector) ? $json->series_selector : '';

        $type_bso = (int)$json->type_bso;// 1 БСО 2 квитанция
        $bso_supplier_id = (int)$json->bso_supplier_id;
        $bso_agent_id = (int)$json->bso_agent_id;
        $order = Orders::find((int)$json->order_id);



        $res = [];
        $res["suggestions"] = [];

        $bso_items = BsoItem::getBso();


        $bso_items->where('state_id', '=',0);

        if($type_bso == 1){
            $bso_items->where('bso_class_id', '!=', 100);
        }
        if($type_bso == 2){
            $bso_items->where('bso_class_id', '=',100);

            if($bso_supplier_id > 0 && !empty($bso_supplier_id)) $bso_items->where('bso_supplier_id', $bso_supplier_id);




        }

        /*
        if(isset($request['product_id']) && (int)$request['product_id'] > 0){
            $bso_items->where('product_id', $request['product_id']);
        }
        */

        if($order->bso_cart_id > 0){

            $bso_cart_id = $order->bso_cart_id;

            $bso_items->where(function ($query) use($bso_agent_id, $bso_cart_id) {
                $query->where('agent_id', '=', $bso_agent_id)
                    ->orWhere('bso_cart_id', '=', $bso_cart_id);
            });

        }else{
            if($bso_agent_id > 0 && !empty($bso_agent_id)) $bso_items->where('agent_id', $bso_agent_id);
        }






        $bso_items->where(function($query) use ($like_query)
        {
            $query->where('bso_number', 'like', "%{$like_query}%")
                ->orWhere('bso_title', 'like', "%{$like_query}%");
        });

        //dd(getLaravelSql($bso_items));


        $bso_items->orderBy('state_id');
        $bso_items->limit($count);



        if (!empty($series_selector)){
            foreach ($bso_items->get() as $bso)
            {
                $data = [];
                $temp___ = stristr($bso->bso_title, ' ');
                $temp___ = str_replace(' ', '', $temp___);

                $bso_title = $temp___;
                $data["value"] = $temp___;
                $data["unrestricted_value"] = $temp___;
                $data["data"] = [];
                $data["data"]['bso_type'] = $bso->type->title.' ('.$bso->bso_states->title.')';
                $data["data"]['bso_title'] = $bso->bso_title;
                $data["data"]['bso_sk'] = $bso->supplier->title;
                $data["data"]['agent_name'] = ($bso->user)?$bso->user->name:'';

                $data["data"]['bso_id'] = $bso->id;
                $data["data"]['bso_supplier_id'] = $bso->bso_supplier_id;
                $data["data"]['insurance_companies_id'] = $bso->insurance_companies_id;
                $data["data"]['product_id'] = $bso->product_id;
                $data["data"]['agent_id'] = $bso->user_id;

                $res["suggestions"][] = $data;
            }
        }else{
            foreach ($bso_items->get() as $bso)
            {
                $data = [];

                $bso_title = $bso->bso_title;
                $data["value"] = $bso->bso_title;
                $data["unrestricted_value"] = $bso->bso_title;
                $data["data"] = [];
                $data["data"]['bso_type'] = $bso->type->title.' ('.$bso->bso_states->title.')';
                $data["data"]['bso_title'] = $bso->bso_title;
                $data["data"]['bso_sk'] = $bso->supplier->title;
                $data["data"]['agent_name'] = ($bso->user)?$bso->user->name:'';

                $data["data"]['bso_id'] = $bso->id;
                $data["data"]['bso_supplier_id'] = $bso->bso_supplier_id;
                $data["data"]['insurance_companies_id'] = $bso->insurance_companies_id;
                $data["data"]['product_id'] = $bso->product_id;
                $data["data"]['agent_id'] = $bso->user_id;
                $data["data"]['isset_hold_kv'] = $bso->product ? $bso->supplier->hold_kv_product($bso->product->id) ? 1 : 0 : 1;

                $res["suggestions"][] = $data;
            }
        }




        return response()->json($res);

    }

    public function get_bso(Request $request)
    {
        $json = \GuzzleHttp\json_decode($request->getContent());
        $count = $json->count;
        $like_query = $json->query;

        $series_selector = isset($json->series_selector) ? $json->series_selector : '';

        $type_bso = (int)$json->type_bso;// 1 БСО 2 квитанция
        $bso_supplier_id = isset($json->bso_supplier_id) ? (int)$json->bso_supplier_id : 0;
        $bso_agent_id = isset($json->bso_agent_id) ? (int)$json->bso_agent_id : 0;
        $bso_used = [];
        $temp_bso_used = [];

        if(isset($json->bso_used)){
            foreach ($json->bso_used as $k => $b){
                if (!empty($b)){
                    if (!empty($series_selector)){
                        $temp_bso_used[] = $series_selector." ".$b;
                    }else{
                        $temp_bso_used[] = $b;
                    }

                }
            }
        }

        $bso_used = $temp_bso_used;

        $res = [];
        $res["suggestions"] = [];

        $bso_items = BsoItem::getBso();

        $bso_items->whereNotIn('bso_title', $bso_used);




        $bso_items->where('state_id', '=',0);

        if($type_bso == 1){
            $bso_items->where('bso_class_id', '!=', 100);
            if (empty($series_selector)){
                $bso_items->where('location_id', '=',1);
            }else{
                $bso_items->where('location_id', '=',0);
            }
        }
        if($type_bso == 2){
            $bso_items->where('bso_class_id', '=',100);

            if($bso_supplier_id > 0 && !empty($bso_supplier_id)) $bso_items->where('bso_supplier_id', $bso_supplier_id);
            if($bso_agent_id > 0 && !empty($bso_agent_id)) $bso_items->where('agent_id', $bso_agent_id);
        }

        if(isset($request['product_id']) && (int)$request['product_id'] > 0){
            $bso_items->where('product_id', $request['product_id']);
        }


        $bso_items->where(function($query) use ($like_query)
        {
            $query->where('bso_number', 'like', "%{$like_query}%")
                ->orWhere('bso_title', 'like', "%{$like_query}%")
                ->orWhere('bso_blank_title', 'like', "%{$like_query}%");
        });

        // Если пришло из первички - быстрой, то учитывать доступ продукта в первичке - быстрой
        if (isset($request->from_url) && $request->from_url == '/contracts/primary-agent'){
            $bso_items
                ->leftJoin('bso_suppliers', 'bso_suppliers.id', '=', 'bso_items.bso_supplier_id')
                ->leftJoin('hold_kv', 'hold_kv.bso_supplier_id', '=', 'bso_suppliers.id')
                ->leftJoin('products', 'hold_kv.product_id', '=', 'products.id')
                ->where('products.available_in_primary_fast', '=', 1);
        }

        /*
        if (!empty($series_selector))
        {
            $bso_items->where('bso_title', 'like', "%{$series_selector}%");
        }
        */


        //dd(getLaravelSql($bso_items));

        $bso_items->orderBy('state_id');
        $bso_items->limit($count);



        if (!empty($series_selector)){
            foreach ($bso_items->get() as $bso)
            {
                $data = [];
                $temp___ = stristr($bso->bso_title, ' ');
                $temp___ = str_replace(' ', '', $temp___);

                $bso_title = $temp___;
                $data["value"] = $temp___;
                $data["unrestricted_value"] = $temp___;
                $data["data"] = [];
                $data["data"]['bso_type'] = $bso->type->title.' ('.$bso->bso_states->title.')';
                $data["data"]['bso_title'] = $bso->bso_title;
                $data["data"]['bso_sk'] = $bso->supplier->title;
                $data["data"]['agent_name'] = ($bso->user)?$bso->user->name:'';

                $data["data"]['bso_id'] = $bso->id;
                $data["data"]['bso_supplier_id'] = $bso->bso_supplier_id;
                $data["data"]['insurance_companies_id'] = $bso->insurance_companies_id;
                $data["data"]['product_id'] = $bso->product_id;
                $data["data"]['agent_id'] = $bso->user_id;

                $res["suggestions"][] = $data;
            }
        }else{
            foreach ($bso_items->get() as $bso)
            {
                $data = [];

                $isset_hold_kv = 1;
                if($bso->bso_class_id != 100){
                    $isset_hold_kv = $bso->product ? $bso->supplier->hold_kv_product($bso->product->id) ? 1 : 0 : 1;
                }

                $bso_title = $bso->bso_title;
                $data["value"] = $bso->bso_title;
                $data["unrestricted_value"] = $bso->bso_title;
                $data["data"] = [];
                $data["data"]['bso_type'] = $bso->type->title.' ('.$bso->bso_states->title.')';
                $data["data"]['bso_title'] = $bso->bso_title;
                $data["data"]['bso_sk'] = $bso->supplier->title;
                $data["data"]['agent_name'] = ($bso->user)?$bso->user->name:'';

                $data["data"]['bso_id'] = $bso->id;
                $data["data"]['bso_supplier_id'] = $bso->bso_supplier_id;
                $data["data"]['insurance_companies_id'] = $bso->insurance_companies_id;
                $data["data"]['product_id'] = $bso->product_id;
                $data["data"]['agent_id'] = $bso->user_id;
                $data["data"]['isset_hold_kv'] = $isset_hold_kv;

                $res["suggestions"][] = $data;
            }
        }




        return response()->json($res);

    }



    public function get_installment_algorithms(Request $request)
    {

        $insurance_companies_id = $request->insurance_companies_id;

        $result = [];

        $algorithms = InstallmentAlgorithms::where('insurance_companies_id', $insurance_companies_id)->get();
        foreach ($algorithms as $algo){
            $result[] = collect(['id'=>$algo->id, 'title'=>InstallmentAlgorithmsList::where('id', '=', $algo->algorithm_id)->get()->first()->title]);
        }

        return response()->json($result);

    }

    public function get_inputs_installment_algorithms(Request $request)
    {
        $algorithm_id = $request->algorithm_id;

/*        if($contract_id = $request->contract_id){
            $contract = Contracts::find($contract_id);

            if($contract && $contract->expected_payments){
                foreach($contract->expected_payments as $expected_payment){
                    $expected_payment->delete();
                }
            }
        }*/

        $algorithms = InstallmentAlgorithms::where('id', '=', $algorithm_id)->first();

        $algos = $algorithms->algorithm;

        $template = view('contracts.contract_templates.default.algorithms.inputs_installment_algorithms', [ 'algos' => $algos, 'key' => $request->key])->render();

        return $template;
    }


    public function get_financial_policy(Request $request)
    {

        $insurance_companies_id = $request->insurance_companies_id;
        $bso_supplier_id = $request->bso_supplier_id;
        $product_id = $request->product_id;

        $row_text = 'title as title';

        if(auth()->user()->is('under')){
            $row_text =  'CONCAT(if(fix_price = 0, kv_agent, fix_price_sum), if(fix_price = 0, "% ", " "), title) as title';
        }

        $temp_sql_fp = FinancialPolicy::where('insurance_companies_id', $insurance_companies_id)
            ->where('bso_supplier_id', $bso_supplier_id)
            ->where('product_id', $product_id)
            ->where('is_actual', 1);

        if (isset($request->sign_date) && $request->sign_date && $request->sign_date != 0){
//            $temp_sql_fp->whereDate('date_active', '<=', date('Y-m-d', strtotime($request->sign_date)));
            $temp_sql_fp->where(function($q) use ($request) {
                $q->whereDate('date_end', '>=', date('Y-m-d', strtotime($request->sign_date)));
                $q->orWhere('date_end', '0000-00-00');
                $q->orWhereNull('date_end');
            });
        }

        $fp = $temp_sql_fp
            ->orderBy('title')
            ->select([
                '*', DB::raw($row_text)
            ])->get(['id', 'title']);


        $result = $fp->toArray();

//        foreach($result as $key => $financialPolicy){
//            $fp_title_arr = mb_str_split($financialPolicy['title'], 80);
//            $result[$key]['title'] = is_array($fp_title_arr) ? implode(PHP_EOL, $fp_title_arr) : $fp_title_arr;
//        }

        return response()->json($result);

    }

    public function get_accesses_products(Request $request){

        $product = Products::find($request->product_id);

        return $product;
    }

    public function get_html_mini_contract_object_insurer(Request $request)
    {
        $product_id = $request->product_id;

        $product = Products::find($product_id);
        $key = str_replace('_', '', $request->key);
        $object = new ObjectInsurer();

        if($product->category->template == 'auto'){
            $object->type = 1;
        }

        return view("contracts.contract_templates.default.insurance_object.partials.edit.{$product->category->template}", [
            'key' => $key,
            'object' => $object,
        ]);

    }


    public function get_html_mini_contract_object_insurer_with_data(Request $request)
    {
        $product_id = $request->product_id;

        $product = Products::find($product_id);
        $key = str_replace('_', '', $request->key);
        $object = new ObjectInsurer();

        if($product->category->template == 'auto'){
            $object->type = 1;
        }

        $usersAutosavePrimary = PrimaryAutosaveContracts::query()
            ->where('user_id', '=', auth()->id())
            ->where('type_id', '=', $request->type_id)->first();

        if($usersAutosavePrimary){
            if(is_array(\GuzzleHttp\json_decode($usersAutosavePrimary->data)->contract)){
                if(isset(\GuzzleHttp\json_decode($usersAutosavePrimary->data)->contract[$key]) && isset(\GuzzleHttp\json_decode($usersAutosavePrimary->data)->contract[$key]->object_insurer)){
                    $object =  \GuzzleHttp\json_decode($usersAutosavePrimary->data)->contract[$key]->object_insurer;
                }
            }else{
                if(isset(\GuzzleHttp\json_decode($usersAutosavePrimary->data)->contract->object_insurer)){
                    $object = \GuzzleHttp\json_decode($usersAutosavePrimary->data)->contract->object_insurer;
                }
            }
        }


        return view("contracts.contract_templates.default.insurance_object.partials.edit_autosaved.{$product->category->template}", [
            'key' => $key,
            'object' => $object
        ]);

    }


    public function get_orders_front(Request $request)
    {
        $json = \GuzzleHttp\json_decode($request->getContent());
        $count = $json->count;
        $like_query = $json->query;

        $front = new IntegrationFront();

        $res = [];

        $res["suggestions"] = collect($front->search_orders($like_query, $count))->filter(function($value, $key){
            $contract = Contracts::query()->where('order_id', $value['data']['id'])->first();
            return empty($contract);
        });

        return response()->json($res);
    }

    public function get_order_id_front(Request $request)
    {

        $order_id = $request->order_id;
        $front = new IntegrationFront();
        $res = $front->get_order_data($order_id);

//dd($res);
        return response()->json($res);


    }

    /**
     * Скрипт для обновления данных из РИТ
     */
    public function update_source_from_front(){
        //обновление Источника из Рит в платежах
        /*if(auth()->user()->is('admin')){
            $payments = Payments::query()
                ->where('order_id','>',0)
                ->whereNull('order_source')
                ->limit(3000)
                ->get();
            $front = new IntegrationFront();
            foreach ($payments as $pay){
                if($pay->order_id > 0) {
                    $res = $front->get_order_data($pay->order_id);
                    $pay->order_source = $res['source'];
                    $pay->save();
                }
            }
        }
        return 'Обновлено записей: ' . $payments->count();*/
        return 0;
    }
    /**
     * Скрипт для обновления данных из РИТ
     */
    public function update_subjects_from_front($from, $to){
        /*$from = (int) $from;
        $to = (int) $to;
        if(empty($from) || empty($to)){
            return 0;
        }
        //обновление года выпуска, доп телефона адреса доставки и метро
        if(auth()->user()->is('admin')){
            $contracts = Contracts::query()
                ->where('order_id','>',0)
                ->whereBetween('id',[$from,$to])
                ->get();
            $front = new IntegrationFront();
            dd($front->get_order_data(23076214));
            foreach($contracts as $contract){
                $res = $front->get_order_data($contract->order_id);

                if(empty($contract->insurer->add_phone)) {
                    $contract->insurer->add_phone = $res['insurer_add_phone'];
                }
                if(empty($contract->insurer->delivery_address)) {
                    $contract->insurer->delivery_address = $res['insurer_delivery_address'];
                }
                if(empty($contract->insurer->underground)) {
                    $contract->insurer->underground = $res['insurer_underground'];
                }
                $contract->insurer->save();

                if($contract->insurer->type == 0 && $contract->insurer->data_fl){
                    $contract->insurer->data_fl->add_phone = $contract->insurer->add_phone;
                    $contract->insurer->data_fl->delivery_address = $contract->insurer->delivery_address;
                    $contract->insurer->data_fl->underground = $contract->insurer->underground;
                    $contract->insurer->data_fl->save();
                } elseif ($contract->insurer->type == 1 && $contract->insurer->data_ul) {
                    $contract->insurer->data_ul->add_phone = $contract->insurer->add_phone;
                    $contract->insurer->data_ul->delivery_address = $contract->insurer->delivery_address;
                    $contract->insurer->data_ul->underground = $contract->insurer->underground;
                    $contract->insurer->data_ul->save();
                }

                if($contract->object_insurer_auto && empty($contract->object_insurer_auto->car_year)) {
                    $contract->object_insurer_auto->car_year = $res['object_insurer_auto']['car_year'];
                    $contract->object_insurer_auto->save();
                }
            }
            return 'Обновлено записей: ' . $contracts->count();
        }*/
        return 0;
    }

    public function status_party(Request $request)
    {
        return response('',200);
    }

    /**
     * Получаем номер ближайшийего ожидаемого платежа, который нужно оплатить
     */
    public function get_payments_accepts(Request $request)
    {
        $contract = Contracts::query()->where('order_id', $request->order_id)->first();
        $payments = $contract->get_expected_payments;
        $min_payment_number = collect($payments)->min('payment_number');
        return $min_payment_number;
    }


    /**
     * просто проверка на существование бсо
     */
    public function check_exsistence_bso(Request $request)
    {
        $bso_title = $request->bso_title;
        $bso_serie_id = $request->bso_serie_id;

        $bso_item = BsoItem::where('bso_number', '=', $bso_title)->where('bso_serie_id', '=', $bso_serie_id)->count();

        if($bso_item){
            return ['status' => 0];
        }

        return ['status' => 1];
    }



}
