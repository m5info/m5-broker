<?php

namespace App\Http\Controllers\BSO;

use App\Http\Controllers\Controller;

use App\Models\Basket\BasketItems;
use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoActsItems;
use App\Models\BSO\BsoComments;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use Illuminate\Http\Request;

class DeleteBsoWarehouseController extends Controller
{


    public function __construct()
    {
        $this->middleware('permissions:bso,delete_bso_warehouse');
    }

    public function delete_bso($id, Request $request)
    {

        $return = new \stdClass();
        $return->error_state = 0;

        $bso = BsoItem::find($id);
        if ($bso->state_id !=0){
            $return->error_state = 1;
            /*$return->error_attr  = ;*/
            $return->error_title = 'БСО должен быть в статусе "Чистый"';
            return response()->json($return);
        }

        $bso->in_basket = 1;
        $bso->save();

        BasketItems::create([
            'removed_item_id' => $id,
            'type_id' => 1,
            'user_id' => auth()->id(),
        ]);

        return response()->json($return);

    }
}