<?php

namespace App\Http\Controllers\BSO;

use App\Models\BSO\BsoCheckDocuments;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Payments;
use App\Models\File;
use App\Repositories\FilesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class BsoStatusesController extends Controller
{

    public $checked_file;
    protected $filesRepository;

    public function __construct(FilesRepository $filesRepository)
    {
        $this->filesRepository = $filesRepository;
        $this->middleware('permissions:bso,bso_statuses_check');
        $this->breadcrumbs[] = [
            'label' => 'Инвентаризация БСО',
            'url' => 'bso/inventory_bso'
        ];
    }

    public function index(Request $request) {

        $documents = BsoCheckDocuments::query();
        $documents->leftJoin('files', 'files.id', '=', 'bso_check_documents.file_id');

        return view('bso.statuses_check.index', ['documents' => $documents->get()]);
    }

    public function check(Request $request){

        // для больших выгрузок
        ini_set('max_execution_time', 12000);
        ini_set('memory_limit', '10000M');


        if(isset($request->check_method) && $request->check_method == 1){ // сверка по файлу

            $this->check_with_document($request);

        }else{ // по данным из textarea

            $this->check_with_data($request);

        }



        Session::flash('checked_file', $this->checked_file);

        return redirect('/files/'.$this->checked_file);

        //return redirect('bso/bso_statuses_check')->withInput(Input::except('check_method'));
    }

    public function check_with_data(Request $request){

        $this->validate($request, [
            'bso_data'  => 'required'
        ]);

        $bso_data = explode("\n", $request->bso_data);

        $array = [];

        foreach($bso_data as $key => $bso){
            $array[$key][] = $bso;
        }

        $excel = $this->excel_generate($array, $request);

        $this->create_document($excel, 'Провренный файл.xlsx');

        return true;
    }

    public function check_with_document(Request $request){

        $this->validate($request, [
            'file'  => 'required'
        ]);

        $this->delete_files();

        Excel::load(Input::file('file'), function ($data) use ($request) {

            $array = $data->toArray();

            foreach ($array as $key => $value) {
                $array[$key] = array_values($value);
            }


            $excel = $this->excel_generate($array, $request);

            $this->create_document($excel, Input::file('file')->getClientOriginalName());

        });

        return true;
    }

    public function excel_generate($array, Request $request){

        $excel = [];


        $i = 0;
        foreach($array as $key => $value)
        {
            $bso_from_file = trim((string)$value[0]);

            if($bso_from_file != '' && $bso_info = $this->get_bso_info($bso_from_file, $request)){ // если находит БСО

                $excel[$i]['Номер БСО'] = $bso_from_file;
                $excel[$i]['Вид страхования'] = ($bso_info->product) ? $bso_info->product->title : '' ;
                $excel[$i]['Примечание по сверке'] = ($bso_info->comments->last()) ? $bso_info->comments->last()->comment : '';
                if($bso_info->location){
                    if(in_array($bso_info->location->id, [0,1,2,4])){
                        $excel[$i]['Событие'] = $bso_info->location->title;
                    }else{
                        $excel[$i]['Событие'] = $bso_info->location->title;
                    }
                }else{
                    $excel[$i]['Событие'] = '';
                }
                $excel[$i]['Статус'] = ($bso_info->state_id) ? $bso_info->state->title : '';
                $excel[$i]['Документ СК'] = ($bso_info->act_sk) ? $bso_info->act_sk->title : '';
                $excel[$i]['Дата события'] = ($bso_info->logs->last()) ? $bso_info->logs->last()->log_time : '';
                if($act = $bso_info->acts->last()){
                    $excel[$i]['Документ Агент'] = ($act = $act->act) ? $act->act_number : '';
                }else{
                    $excel[$i]['Документ Агент'] = '';
                }
                $excel[$i]['Агент'] = ($bso_info->agent) ? $bso_info->agent->name : '';

                $excel[$i]['Менеджер'] = ($bso_info->contract && $bso_info->contract->manager) ? $bso_info->contract->manager->name : '';
                $excel[$i]['Страхователь'] = ($bso_info->contract && $bso_info->contract->insurer) ? $bso_info->contract->insurer->title : '';

                $payment = $bso_info->bso_class_id == 100 ? $bso_info->paymentsReceipt->first() : $bso_info->payments->first();

                if($payment){
                    $excel[$i]['Отчет в страховую'] = ($payment->reports_border) ? $payment->reports_border->title : '';
                }else{
                    $excel[$i]['Отчет в страховую'] = '';
                }
                if($payment){
                    $excel[$i]['Примечание к акту'] = ($payment->reports_border) ? $payment->reports_border->comments : '';
                }else{
                    $excel[$i]['Примечание к акту'] = '';
                }
                $excel[$i]['Id-БСО'] = $bso_info->id;


                if($bso_info->bso_class_id == 100 && $bso_info->contract_receipt) {
                    $excel[$i]['Номер договора/Квитанция'] = $bso_info->contract_receipt->bso_title;

                    if (sizeof($bso_info->contract_receipt->payments)) {
                        $excel[$i]['Страховая премия'] = $bso_info->contract_receipt->payments->last()->payment_total;
                        $excel[$i]['Статус платежа'] = Payments::STATUS[$bso_info->contract_receipt->payments->last()->statys_id];
                    } else {
                        $excel[$i]['Страховая премия'] = '';
                        $excel[$i]['Статус платежа'] = '';
                    }

                } else if ($bso_info->contract && sizeof($bso_info->contract->payments)) {
                    $excel[$i]['Номер договора/Квитанция'] = $bso_info->contract->payments->last()->bso_receipt;
                    $excel[$i]['Страховая премия'] = $bso_info->contract->payments->last()->payment_total;
                    $excel[$i]['Статус платежа'] = Payments::STATUS[$bso_info->contract->payments->last()->statys_id];
                } else {
                    $excel[$i]['Номер договора/Квитанция'] = '';
                    $excel[$i]['Страховая премия'] = '';
                    $excel[$i]['Статус платежа'] = '';
                }

            }else{

                $excel[$i]['Номер БСО'] = $bso_from_file;
                $excel[$i]['Вид страхования'] = '';
                $excel[$i]['Примечание по сверке'] = '';
                $excel[$i]['Событие'] = '';
                $excel[$i]['Статус'] = '';
                $excel[$i]['Документ СК'] = '';
                $excel[$i]['Дата события'] = '';
                $excel[$i]['Документ Агент'] = '';
                $excel[$i]['Агент'] = '';
                $excel[$i]['Менеджер'] = '';
                $excel[$i]['Страхователь'] = '';
                $excel[$i]['Отчет в страховую'] = '';
                $excel[$i]['Примечание к акту'] = '';
                $excel[$i]['Id-БСО'] = '';
                $excel[$i]['Номер договора'] = '';
                $excel[$i]['Страховая премия'] = '';
                $excel[$i]['Статус платежа'] = '';
            }
            $i++;
        }

        return $excel;
    }

    public function create_document($data, $filename){
        $uniqid = uniqid();

        $url = url('/');
        $str=strpos($url, ":");
        $url=substr($url,  $str+3);

        $url = ($url == '127.0.0.1:8000') ? '' : $url.'/';
        $excel_file = Excel::create($uniqid, function($excel) use($data) {


            $excel->sheet('Sheetname', function($sheet) use($data) {
                /*
                                $set_row = 0;

                                for ($i=1;$i<=count($data); $i++){
                                    if($i == 1){
                                        $sheet->row($set_row, array_keys ( $data[$i-1] ));
                                        $set_row += 1;
                                    }

                                    if(isset($data[$i-1])){
                                        $_set_val = [];
                                        foreach ($data[$i-1] as $_val){
                                            $_set_val[] = $_val;
                                        }
                                        dump($set_row);
                                        $sheet->row($set_row, $_set_val);
                                        $set_row += 1;
                                    }

                                }

                                dd($data);

                */
                $sheet->fromArray($data);
            });




        })->store('xlsx', storage_path('app/' . $url . BsoCheckDocuments::FILES_DOC));


        $file_bd = $this->createFileInDB(BsoCheckDocuments::FILES_DOC, $filename, $uniqid);


        BsoCheckDocuments::create([
            'bso_check_id' => 1,
            'file_id' => $file_bd->id,
        ]);

        $this->checked_file = $uniqid;

        return true;
    }

    private function createFileInDB($folder, $filename, $uniqid) {
        return File::create([
            'original_name' => $filename,
            'ext' => 'xlsx',
            'folder' => $folder,
            'name' => $uniqid,
            'user_id' => auth()->check() ? auth()->id() : null,
            'host' => request()->getHost()
        ]);
    }


    public function delete_files(){

        $old_files = BsoCheckDocuments::all();

        if($old_files){
            /*
             * TODO: На бою удаление не работает. Проблеша с пермишном к папке (если все норм, можно нижний код раскоментить)
             */
//            $FolderToDelete = storage_path('app/' . $_SERVER['SERVER_NAME']. '/bso/bso_statuses_check');
//            $fs = new \Illuminate\Filesystem\Filesystem;
//            $fs->cleanDirectory($FolderToDelete);
//
//            if(rmdir($FolderToDelete)){
//                mkdir($FolderToDelete, 0777);
//            }

            foreach($old_files as $old_file){
                $old = File::find($old_file->file_id);

                File::destroy($old_file->file_id);
                BsoCheckDocuments::destroy($old_file->id);
            }
        }
    }


    public function get_bso_info($bso, Request $request){

        $bso_search = trim($bso);


        if($request->search_type == 1){ // Частичное совпадение

            $bso_search = (int) filter_var($bso, FILTER_SANITIZE_NUMBER_INT);

            $bso_item = BsoItem::query()
                ->where('insurance_companies_id', $request->sk)
                ->where('bso_number', 'like', "%$bso_search%")
                ->orWhere('bso_blank_number', 'like', "%$bso_search%")
                ->where('insurance_companies_id', $request->sk);

        }else{ // Полное совпадение


            $bso_item = BsoItem::query()
                ->where('insurance_companies_id', $request->sk)
                ->where('bso_title', 'like', "%$bso_search%")
                ->orWhere('bso_blank_title', 'like', "%$bso_search%")
                ->where('insurance_companies_id', $request->sk);

        }

        if($request->bso_type == 2){ // БСО

            $bso_item->where('bso_class_id', '!=', '100');

        }elseif($request->bso_type == 3){ // Квитанция

            $bso_item->where('bso_class_id', '=', '100');

        }


        $bso_item = $bso_item->first();

        return ($bso_item) ? $bso_item : false;
    }
}
