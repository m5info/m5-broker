<?php

namespace App\Http\Controllers\Users;

use App\Classes\Export\ExportManager;
use App\Helpers\Visible;
use App\Http\Controllers\Controller;
use App\Models\Directories\BsoSuppliers;
use App\Models\Log\LogEvents;
use App\Models\Organizations\Organization;
use App\Models\Organizations\PointsDepartments;
use App\Models\Settings\City;
use App\Models\Settings\TemplateCategory;
use App\Models\Subject\Juridical;
use App\Models\Subject\Physical;
use App\Models\Subject\Type as SubjectType;
use App\Models\User;
use App\Models\Users\Role;
use App\Models\Users\SalaryType;
use App\Repositories\FilesRepository;
use Illuminate\Http\Request;
use Validator;
use Mail;
use App\Mail\UserAdd;

class UsersController extends Controller
{

    protected $filesRepository;

    public function __construct(FilesRepository $filesRepository)
    {

        $this->middleware('permissions:users,users');

        $this->filesRepository = $filesRepository;


        $this->breadcrumbs[] = [
            'label' => 'Пользователи',
            'url' => 'users/users',
        ];
    }

    public function index(Request $request)
    {
        if (isset($request->fio)) {
            $users = User::where('name', 'like', "%" . $request->fio . "%")->get();
        } else {
            $users = User::all();
        }

        return view('users.users.index', [
            'users' => $users,
        ]);
    }

    public function get_table(Request $request)
    {

        $users = User::getALLUserWhere();

        if (isset($request->fio) && strlen($request->fio)>=3) {
            $users->where('name', 'like', "%" . $request->fio . "%");
        }

        if (isset($request->supervisor) && strlen($request->supervisor)>=3) {

            $supervisor = $request->supervisor;
            $users->whereHas('perent', function ($query) use ($supervisor) {
                $query->where('name', 'like',  '%' .$supervisor. '%');
            });
        }

        $page = (int)$request->page;
        if ($page == 0) {
            $page = 1;
        }
        $page_count = isset($request->page_count) && (int)$request->page_count > 0 ? (int)$request->page_count : 25;


        $max_row = (int)$users->count();
        $page_max = ceil($max_row/$page_count);
        $view_row = ($page_count*($page));

        $users->skip(($page_count*($page-1)))->take(($page_count));
        $res = $users->get();

        if($res->count() < $page_count){
            $view_row = ($max_row);
        }



        return response()->json([
            'result' => view('users.users.get_table', [
                'users'=>$res
            ])->render(),
            'page_max' => $page_max,
            'page_sel' => $page,
            'max_row' => $max_row,
            'view_row' => $view_row,
        ]);


    }

    public function edit($id)
    {

        //LogEvents::event($id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_VIEW], 1);
        $user = User::find($id);

        $this->breadcrumbs[] = [
            'label' => $user->name,
        ];



        return view('users.users.edit', [
            'user' => $user,
            'roles' => Role::all()->pluck('title', 'id')->prepend(trans('form.select.not_selected'), ''),
            'organizations' => Organization::query()->where('is_delete', 0)->get()->pluck('title', 'id')->prepend(trans('form.select.not_selected'), ''),
            'suppliers' => BsoSuppliers::query()->where('is_actual', 1)->get()->pluck('title', 'id')->prepend(trans('form.select.not_selected'), ''),
            'cities' => City::query()->where('is_actual', 1)->get()->pluck('title', 'id'),
            'userInfoFields' => $this->getUserInfoFields(),
            'points_departments' => PointsDepartments::where('organization_id', $user->organization_id)->get()->pluck('title', 'id'),
            'salaryTypes' => [
                SalaryType::OFFICIAL => trans('users/users.edit.salary_types.official'),
                SalaryType::UNOFFICIAL => trans('users/users.edit.salary_types.unofficial'),
            ]
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function create()
    {

        $this->breadcrumbs[] = [
            'label' => 'Новый пользователь',
        ];

        return view("users.users.create", [
            'roles' => Role::all()->pluck('title', 'id')->prepend(trans('form.select.not_selected'), ''),
            'organizations' => Organization::query()->where('is_delete', 0)->get()->pluck('title', 'id')->prepend(trans('form.select.not_selected'), ''),
            'suppliers' => BsoSuppliers::query()->where('is_actual', 1)->get()->pluck('title', 'id')->prepend(trans('form.select.not_selected'), ''),
            'userInfoFields' => $this->getUserInfoFields(),
            'points_departments' => collect([]),
            'salaryTypes' => [
                SalaryType::OFFICIAL => trans('users/users.edit.salary_types.official'),
                SalaryType::UNOFFICIAL => trans('users/users.edit.salary_types.unofficial'),
            ]
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users|max:255',
        ]);

        if ($validator->fails() && $user->email !== $request->email) {
            return redirect(url("/users/users/$user->id/edit/"))->withInput()->with('error', "Ошибка валидации email");
        }

        LogEvents::event($user->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_UPDATE], 1, 0, 0, $request->all());

        return $this->save($user, $request);
    }

    public function store(Request $request)
    {
        $user = new User;

        //$user->save();

        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users|max:255',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(url("/users/users/create"))->withInput()->with('error', implode(', ', $validator->errors()->all()));
        }


        LogEvents::event($user->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_CREATE], 1, 0, 0, $request->all());

        return $this->save($user, $request);
    }

    private function save(User $user, Request $request)
    {

        $this->validate($request, [
            'name' => '',
            'image' => 'mimes:jpeg,png,bmp',
        ]);


        $user->name = $request->name;
        $user->email = mb_strtolower(trim($request->email));
        $user->role_id = $request->role_id;
        $user->subject_type_id = $request->subject_type_id;
        $user->organization_id = $request->organization_id;
        $user->mobile_phone = $request->mobile_phone;
        $user->work_phone = $request->work_phone;
        $user->status_user_id = $request->status_user_id;
        $user->is_parent = (int)$request->is_parent;
        $user->financial_group_id = (int)$request->financial_group_id;
        $user->department_id = (int)$request->department_id;
        $user->region = $request->region;
        $user->city_id = (int)$request->city_id;
        $user->bso_supplier_id = (int)$request->bso_supplier_id;
        $user->point_department_id = (int)$request->point_department_id;


        $user->parent_id = (int)$request->parent_id;
        $user->curator_id = (int)$request->curator_id;


        $user->point_sale_id = (int)$request->point_sale_id;

        $user->zp = getFloatFormat($request->zp);
        $user->financial_expenses = getFloatFormat($request->financial_expenses);

        if ($request->has('password') && strlen($request->get('password')) > 3) {
            $user->password = bcrypt(trim($request->password));
        }

        if(isset($request->agent_contract_title)){
            $user->agent_contract_title = $request->agent_contract_title;
            $user->agent_contract_begin_date = setDateTimeFormat($request->agent_contract_begin_date);
            $user->agent_contract_end_date = setDateTimeFormat($request->agent_contract_end_date);

            $user->ban_level = (int)$request->ban_level;
            $user->ban_reason = $request->ban_reason;
            $user->signer_fio = ($request->signer_fio)?$request->signer_fio:'';

            $user->front_user_id = ($request->front_user_id)?$request->front_user_id:'';
            $user->front_user_title = ($request->front_user_title)?$request->front_user_title:'';

            $user->referencer_kv = ($request->referencer_kv)?(int)$request->referencer_kv:'';
        }

        if(isset($request->financial_group_date) && strlen($request->financial_group_date) > 5){
            $user->financial_group_date = setDateTimeFormat($request->financial_group_date);
        }else{
            $user->financial_group_date = date('Y-m-d');
        }



        //if(isset($request->status_security_service)) $user->status_security_service = $request->status_security_service;

        if ($request->hasFile('image')) {
            $user->image()->associate($this->filesRepository->makeFile($request->image, User::FILES_FOLDER . "/{$user->id}/"));
            $user->smallImage()->associate($this->filesRepository->makeResizedImage($request->image, User::FILES_FOLDER));
        }

        $user->info()->delete();

        $subjectClass = $user->subject_type_id == SubjectType::PHYSICAL ? Physical::class : Juridical::class;

        $subject = $subjectClass::create($request->only($subjectClass::getFields()));

        $user->subject_id = $subject->id;

        //$user->settings = (string)'{"menu":"","menu_section":""}';

        if ($user->save()) {

            $user->path_parent = $user->getPathParent();
            $user->save();


            if (\App::environment() != 'local') {
                Mail::to($user->email)->send(new UserAdd($request));
            }

        }

        return redirect(url("/users/users/$user->id/edit/"))->with('success', trans('form.success_update'));
    }

    public function destroy($id)
    {
        $user = User::find($id);

        $user->delete();

        return response('', 200);
    }

    public function getUserInfoFields()
    {
        return [
            'physical' => Physical::getFields(),
            'juridical' => Juridical::getFields(),
        ];
    }

    public function generate_contract($id)
    {
        $agent = Visible::apply(User::query(), 'users', 'id')->where('id', $id);
        return (new ExportManager(TemplateCategory::get('contract_agent'), $agent))->handle();
    }

    public function get_points_departments(Request $request){

        $user_id = (int)$request->user_id;
        if($user_id == 0){
            $user = new User;
        }else{
            $user = User::findOrFail($user_id);
        }
        $user->organization_id = (int)$request->org_id;
        $user->save();

        return view('users.frame.points_departments_select', [
            'user' => $user,
            'points_departments' => PointsDepartments::where('organization_id', $user->organization_id)->get()->pluck('title', 'id'),
        ]);
    }

}
