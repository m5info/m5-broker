<?php

namespace App\Http\Controllers\Users\Users\AgentFinancialPolicies;

use App\Classes\Export\ExportManager;
use App\Http\Controllers\Controller;
use App\Models\Cashbox\IncomeExpense;
use App\Models\Organizations\Organization;
use App\Models\Settings\FinancialGroup;
use App\Models\Settings\TemplateCategory;
use App\Models\User;
use App\Models\Users\AgentContracts;
use App\Models\Users\AgentFinancialPolicies;
use Illuminate\Http\Request;


class AgentFinancialPoliciesController extends Controller{



    public function edit($user_id, $id, Request $request)
    {

        if((int)$id > 0){
            $agent_financial_policies = AgentFinancialPolicies::find($id);
        }else{
            $agent_financial_policies = new AgentFinancialPolicies();
        }

        return view('users.agent_financial_policies.edit', [
            'agent_financial_policies' => $agent_financial_policies,
            'user_id' => $user_id,
            'id' => $id,
            'financial_policies' => FinancialGroup::where('is_actual', '=', '1')->get(),
        ]);
    }


    public function save($user_id, $id, Request $request)
    {
        if((int)$id > 0){
            $agent_financial_policies = AgentFinancialPolicies::find($id);
        }else{
            $agent_financial_policies = new AgentFinancialPolicies();
            $agent_financial_policies->user_id = $user_id;
        }


        $agent_financial_policies->financial_group_id = $request->financial_group_id;
        $agent_financial_policies->begin_date = getDateFormatEn($request->begin_date);
        $agent_financial_policies->end_date = getDateFormatEn($request->end_date);
        $agent_financial_policies->save();

        return parentReload();
    }



    public function delete($user_id, $id, Request $request){

        AgentFinancialPolicies::find($id)->delete();

        return response(200);
    }



}