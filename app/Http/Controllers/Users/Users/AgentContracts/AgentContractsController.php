<?php

namespace App\Http\Controllers\Users\Users\AgentContracts;

use App\Classes\Export\ExportManager;
use App\Http\Controllers\Controller;
use App\Models\Cashbox\IncomeExpense;
use App\Models\Organizations\Organization;
use App\Models\Settings\TemplateCategory;
use App\Models\User;
use App\Models\Users\AgentContracts;
use Illuminate\Http\Request;


class AgentContractsController extends Controller{



    public function edit($user_id, $id, Request $request)
    {
        $user = User::find($user_id);

        $finish = (isset($request->finish))?$request->finish:'';

        if((int)$id > 0){
            $agent_contract = AgentContracts::find($id);
        }else{
            $agent_contract = new AgentContracts();
        }

        return view('users.agent_contracts.edit', [
            'user' => $user,
            'agent_contract' => $agent_contract,
            'finish' => $finish,
            'organizations' => Organization::where('is_actual', 1)->where('is_agent_contract', 1)->get(),
        ]);
    }


    public function save($user_id, $id, Request $request)
    {
        if((int)$id > 0){
            $agent_contract = AgentContracts::find($id);
        }else{
            $agent_contract = new AgentContracts();
            $agent_contract->user_id = $user_id;
        }

        $agent_contract->org_id = $request->org_id;
        $agent_contract->agent_contract_title = $request->agent_contract_title ? $request->agent_contract_title : 0;
        $agent_contract->begin_date = getDateFormatEn($request->begin_date);
        $agent_contract->end_date = getDateFormatEn($request->end_date);
        $agent_contract->save();

        if(isset($request->finish) && $request->finish == 'info'){

            return parentGetAgetnInfo($user_id);

        }else{
            return parentReload();
        }
    }

    public function get_agent_contract($user_id, $id, Request $request){

        if((int)$id > 0){
            $agent_contract = AgentContracts::find($id);
        }else{
            $agent_contract = new AgentContracts();
            $agent_contract->user_id = $user_id;
        }

        $agent_contract->org_id = $request->org_id;

        $agent_contract->begin_date = getDateFormatEn($request->begin_date);
        $agent_contract->end_date = getDateFormatEn($request->end_date);
        $agent_contract->agent_contract_title = $request->agent_contract_title != '' ? $request->agent_contract_title : $this->getAgentContractNumb($agent_contract->org_id);
        $agent_contract->save();

        return $agent_contract;
    }

    public function download($user_id, Request $request){
        $org = Organization::find($request->org_id);
        $contracts = $org->agent_contracts->first();

        return $contracts;
    }

    public function getAgentContractNumb($org_id){
        return AgentContracts::where('org_id', $org_id)->get()->count() + 1;
    }

}