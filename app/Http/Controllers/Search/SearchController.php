<?php

namespace App\Http\Controllers\Search;

use App\Http\Controllers\Controller;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Subjects;
use App\Models\Finance\Invoice;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SearchController extends Controller
{

    public function __construct()
    {

    }

    public function index(Request $request)
    {
        $bsos = null;
        $errors = null;

        $find = trim($request->find);

        session(['search.find'=> $find]);
        session(['search.find_name_sk'=> $request->global_search_sk]);
        session(['search.find_search_type'=> $request->search_type]);

           switch($request->search_type){
                case 1; // БСО

                    if(strlen($find) >= 3){

                        $user = auth()->user();
                        $visibility_obj = $user->role->rolesVisibility(4)->visibility;

                        $bso = BsoItem::query();

                        if ($visibility_obj != 0) {
                            $users_sub = User::where('path_parent', 'like', "%:{$user->id}:%")->select('id');

                            $bso = BsoItem::query()
                                ->where(function ($query) use ($users_sub) {
                                    return $query->whereIn('bso_items.agent_id', $users_sub)
                                        ->orWhereIn('id', function($query) use ($users_sub) {
                                            return $query->select('bso_id')
                                                ->from(with(new Contracts)->getTable())
                                                ->whereIn('manager_id', $users_sub);
                                        });
                                });
                        }

                        $int = (int) filter_var($find, FILTER_SANITIZE_NUMBER_INT);

                        if($int > 0){


                            //$bso->where('bso_items.bso_title', 'like', "%{$find}%");

                            $bso->where(function ($query) use ($find) {
                                return $query->where('bso_items.bso_title', 'like', "%{$find}%")
                                    ->orWhere('bso_items.bso_blank_title', 'like', "%{$find}%");
                            });

                            if($request->global_search_sk != 0){ // СК

                                $bso->where('insurance_companies_id', '=', $request->global_search_sk);

                            }

                        }else{
                            $bso->where('bso_items.id', '=', 0);
                            $errors = "Введите Номер";
                        }

                    }else{
                        $bso = BsoItem::getBso();
                        $bso->where('bso_items.id', '=', 0);
                        $errors = "Введите более 3 символов!";
                    }

                    break;
                case 2; // Страхователь

                    $bso = BsoItem::query();

                    if(strlen($find) >= 3){

                        $contrsct = Contracts::getContractsQuery()->where('contracts.statys_id', '>', 0);

                        $contrsct->where('contracts.bso_id', '>', 0);
                        $contrsct->leftJoin('subjects', 'subjects.id', '=', 'contracts.insurer_id');
                        $contrsct->where('subjects.title', 'like', "%{$find}%");

                        if($request->global_search_sk != 0){ // СК
                            $bso->where('insurance_companies_id', '=', $request->global_search_sk);
                        }

                        $contrsct->select("contracts.bso_id");
                        $bso->whereRaw('`bso_items`.`id` IN (' . getLaravelSql($contrsct) . ')');

                    }else{
                        $errors = "Введите более 3 символов!";
                    }

                    break;
                case 3; // Счет
                    $invoice = Invoice::query();
                    //$invoice->where('bso_items.bso_title', 'like', "%{$find}%");
                    $invoice->where('invoices.id', '=', $find);
                    $invoice->leftJoin('payments', 'invoices.id','payments.invoice_id');
                    $invoice->leftJoin('bso_items', 'payments.bso_id', 'bso_items.id');
                    $invoice->leftJoin('organizations', 'invoices.org_id', 'organizations.id');
                    $invoice->leftJoin('users as agent', 'invoices.agent_id', 'agent.id');

                    $invoice->select([
                        'invoices.id as invoices_id',
                        'payments.id as payments_id' ,
                        'bso_items.id as bso_items_id',
                        'organizations.title as org_title',
                        'invoices.type as invoice_type',
                        'agent.name as agent_name',
                        'invoices.created_at as invoices_created_at',
                        'invoices.status_id as invoices_status_id',
                        \DB::raw('(select COUNT(*) from payments where payments.invoice_id = invoices_id) as count_payments'),
                        \DB::raw('(select SUM(payment_total) from payments where payments.invoice_id = invoices_id) as total_payments_sum'),
                    ]);

                    $invoice->groupBy('invoices.id');

                    if($request->global_search_sk != 0){ // СК
                        $invoice->where('bso_items.insurance_companies_id', '=', $request->global_search_sk);
                    }

                    break;
               case 4; // Направление(из фронт офиса)

                   $bso = BsoItem::getBso();

                   if(strlen($find) >= 3){

                       $contrsct = Contracts::where('contracts.statys_id', '>', 0);
                       $contrsct->where('contracts.order_id', '>', 0);
                       $contrsct->where('contracts.bso_id', '>', 0);
                       $contrsct->where('contracts.order_title','like', "%{$find}%");

                       if($request->global_search_sk != 0){ // СК
                           $bso->where('insurance_companies_id', '=', $request->global_search_sk);
                       }

                       $contrsct->select("contracts.bso_id");
                       $bso->whereRaw('`bso_items`.`id` IN (' . getLaravelSql($contrsct) . ')');

                   }else{
                       $errors = "Введите более 3 символов!";
                   }

                   break;
            }


        if($request->search_type == 3){

            $invoices = $invoice->get();

            if(isset($invoices)){
                if($invoices->isEmpty()){
                    $errors = "Нет счетов по заданным параметрам";
                }
            }

            return view('search.invoices', [
                'invoices' => $invoices,
                'errors' => $errors
            ]);

        }else{

            $bsos = $bso->get();

            if(isset($bsos)){
                if($bsos->isEmpty()){
                    $errors = "Нет записей";
                }
            }

            return view('search.bsos', [
                'bsos' => $bsos,
                'errors' => $errors
            ]);
        }

    }




    public function phpinfo()
    {
        if(auth()->id() == 1) {

            phpinfo();

            die();
        }

        return redirect('/');
    }




}
