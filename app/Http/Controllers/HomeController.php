<?php

namespace App\Http\Controllers;

use App\Http\QueryHandlers\HomePage\HomePageQueryHandler;
use App\Models\BSO\BsoItem;
use App\Models\Cashbox\IncomeExpense;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\FinancialPolicyGroup;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Finance\Invoice;
use App\Models\Informing\Kv\InformingKv;
use App\Models\Informing\News\News;
use App\Models\Informing\Segments\InformingSegments;
use App\Models\Settings\BaseRate;
use App\Models\Settings\City;
use App\Models\Users\UsersBalanceTransactions;
use App\Models\Users\UserThemes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*public function index(){

        if (Auth::check()){
            return $this->index_user();
        }

        return view('landing.index');
    }*/

    public function test()
    {

        /*$bso = new BsoItem([
            'bso_supplier_id' => 2,
            'insurance_companies_id' => 3,
            'point_sale_id' => 4,
            'bso_class_id' => 5,
            'product_id' => 6,
            'type_bso_id' => 7,
            'state_id' => 8,
            'location_id' => 9,
            'user_id' => 9,
            'user_org_id' => 9,
            'bso_serie_id' => 9,
            'bso_number' => 9,
            'bso_dop_serie_id' => 9,
            'bso_title' => 9,
            'bso_blank_serie_id' => 9,
            'bso_blank_number' => 9,
            'bso_blank_dop_serie_id' => 9,
            'bso_blank_title' => 9,
            'acts_reserve_or_realized_id' => 9,
            'acts_implemented_id' => 9,
            'transfer_id' => 9,
            'last_transfer_id' => 9,
            'bso_manager_id' => 9,
            'is_reserved' => 9,
            'bso_cart_id' => 9,
            'agent_id' => 9,
            'contract_id' => 9,
            'acts_to_underwriting_id' => 9,
        ]);
        $bso->save();
        $bso->agent_id = 7;
        $bso->save();
        $bso->delete();*/

        /*$payment = new Payments([
            'acts_sk_id' => 1,
            'accept_user_id' => 1,
            'accept_date' => '2020-06-03',
            'atol_check_id' => 1,
            'pay_method_id' => 5
        ]);
        $payment->save();
        $payment->acts_sk_id = 6;
        $payment->save();
        $payment->delete();*/

        /*$invoice = new Invoice([
            'user_id' => 1,
            'created_at' => '2020-06-03',
            'updated_at' => '2020-06-03',
            'status_id' => 1,
            'type' => 1,
            'create_type' => 1,
            'org_id' => 1
        ]);
        $invoice->save();
        $invoice->type = 3;
        $invoice->save();
        $invoice->delete();*/

        /*$contract = new Contracts([
           'order_form_id' => 1,
            'type_id' => 1,
            'insurance_amount' => 19881.84,
            'bank_id' => 1,
            'underfiller_check' => 1,
            'owner_id' => 1,
            'selected_calculation_id' => 1,
            'underfiller_id' => 1,

        ]);

        $contract->save();
        $contract->order_form_id = 5;
        $contract->save();
        $contract->delete();*/

        /*$income_expenses = new IncomeExpense();
        $income_expenses->category_id = 0;
        $income_expenses->status_id = 1;
        $income_expenses->user_id = 1;
        $income_expenses->comment = 1;
        $income_expenses->payment_type = 1;
        $income_expenses->save();

        $income_expenses->category_id = 2;
        $income_expenses->save();

        $income_expenses->delete();*/


    }

    public function getCurrentUserData()
    {
        return auth()->user();
    }

    public function index()
    {
        $user = Auth::user();
        $role = $user->role_id;
        $balances = $user->getBalanceListHome();
        $debt = $user->getDebts();

        $transactions = $user->transactions;

        $base_rates = BaseRate::all();
        $cities = City::all();

        $insurance_companies = InsuranceCompanies::all();

        $actual_segment = InformingSegments::query()->where('is_actual', 1)->first();
        $actual_kv = InformingKv::query()->where('is_actual', 1)->first();

        $news = News::getIndexNews()->get();

        /*$str = '<sch:loginResponse xmlns:sch=\"http:\/\/www.vsk.ru\/schema\/partners\/auth\"\n                           xmlns:sch1=\"http:\/\/www.vsk.ru\/schema\/partners\/common\">\n    <sch1:systemId><\/sch1:systemId>\n    <sch1:messageId>5e2973a73558d<\/sch1:messageId>\n    <sch1:correlationId><\/sch1:correlationId>\n    <sch1:bpId>543440f4-5487-11ea-b41d-005056910a7e<\/sch1:bpId>\n\n    \n\n    <sch1:sessionId>2066253893890585829<\/sch1:sessionId>\n<\/sch:loginResponse>';
        $str = simplexml_load_string(stripcslashes($str));*/

        /*dump($str->asXML());
        dump($str->getNamespaces(true));
        dump((string)$str->children('sch1', true)->messageId);*/

        /*$arr = [];

        foreach ($str->children('sch1', true) as $children){
            $arr[$children->getName()] = (string)$children;
        }

        dump($arr);*/


        return view('home', [
            'transactions' => $transactions,
            'cities' => $cities,
            'debt' => $debt,
            'insurance_companies' => $insurance_companies,
            'base_rates' => $base_rates,
            'role' => $role,
            'user' => $user,
            'balances' => $balances,
            'news' => $news,
            'actual_segment' => $actual_segment,
            'actual_kv' => $actual_kv,
        ]);
    }

    public function get_transaction_table()
    {


        $builder = UsersBalanceTransactions::where('balance_id', request('balance'));//Auth()->user()->getBalance(request('balance'))->getTransactions();

        $builder = (new HomePageQueryHandler($builder))->allowEmpty()->apply();

        $builder->orderBy('event_date', 'desc')->orderBy('id', 'desc');
        /*
        if (!empty(request('balances'))) {
            $array = array_values(request('balances'));
            $builder->whereIn('balance_id', $array);
        }
        */
        $transactions = $builder->get();

        return view('partials.transactions_table', [
            'transactions' => $transactions
        ]);
    }

    public function change_password(Request $request)
    {
        $user = \Auth::user();

        if (empty($request->new_password)) {
            return response()->json('{"error": 4, "message": "Пароль не может быть пустым."}');
        }

        if (iconv_strlen($request->new_password) < 6) {
            return response()->json('{"error": 5, "message": "Пароль не может быть меньше 6 символов"}');
        }

        if ($request->new_password != $request->new_password_repeat) {
            return response()->json('{"error": 1, "message": "Вы ввели неправильно повторный пароль."}');
        }

        if (Hash::check($request->old_password, $user->password)) {
            $new_password = bcrypt(trim($request->new_password));
            $user->password = $new_password;
            $user->save();
            return response()->json('{"error": 0, "message": "Пароль успешно изменен."}');
        } else {
            return response()->json('{"error": 2, "message": "Текущий пароль введен неверно."}');
        }

    }


    /*Фрейм с выбором темы*/
    public function getThemes(){

        if( !\auth()->id())
            die();

        $themes = [];


        return view('users.frame.theme',['token'=> csrf_token(),'u_theme'=>\auth()->user()->theme]);

    }

    public function themeChange(Request $request){
        if( !\auth()->id())
            die('no_user');


        $request = $request->post();
        //dd($request);
        $themes = UserThemes::themes;

        $userTheme = UserThemes::where('user_id',\auth()->id())->first();
        if($userTheme){
            $userTheme->css_file = $themes[$request['theme']]['file'];
            $userTheme->custom_styles = $request['font'];

            $userTheme->save();
        }
        else {
            UserThemes::setUserThemeStandard();
        }


       // dd($request['font']);
        return $themes[$request['theme']]['file'];
    }

    public function setUserTheme(){

    }

}
