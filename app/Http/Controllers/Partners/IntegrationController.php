<?php

namespace App\Http\Controllers\Partners;

use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\Directories\Products;
use App\Models\Partners\IntegrationPartnersTokens;
use App\Models\Partners\Notifications;
use App\Models\Partners\VehicleMapper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Partners\Integration;
use Illuminate\Support\Collection;
use App\Models\Partners\Cars;
use App\Models\Partners\Ask;
use App\Models\Partners\AVData;
use App\Models\Partners\AVDataTest;
use App\Models\Partners\Region;
use App\Models\Partners\Validator;

class IntegrationController extends Controller
{
    public function index()
    {
        $token = IntegrationPartnersTokens::where('token', request()->token)->first();

        if(!$token || !$token->validate(request()->token, request()->private_key)){
            echo json_encode(['status' => 'error']);
            return;
        } else {
            if(isset(request()->ask) && request()->ask)
                return Ask::data();
            else
                return $this->step(request()->step);
        }
    }

    public function step($step_num)
    {
        switch ($step_num){
            case 1:
                //$car = Cars::where('gosnum', request()->contract)->first();
                $av_data = new AVData();
                $car = $av_data->getByGosnum(request()->contract);

                //ситуация, когда avinfo дает ошибку
                if(!$car)
                    return response()->json(['error' => 'no_car_found']);

                //ситуация, когда avinfo дает ошибку
                if(is_string($car))
                    return response()->json(['error' => 'special_error', 'msg' => $car]);

                #ToDO: filterData должен делать корректный std-объект из нашего раздекоденного json-объекта - потестить можно через addToken
                $car = $av_data->filterData($car->getDataFromJson());

                //ситуация, когда парсинг данных не дает результатов
                if(!$car)
                    return response()->json(['error' => 'no_car_found']);

                #$car->mark, $car->model - данные тачки на английском
                $translated_car = VehicleMapper::getCarByGosnum2($car);

                $response = [
                    'contract[object][car_year]' => $car->year,
                    'contract[object][power]' => $car->power,
                    'contract[object][vin]' => $car->vin,
                    'contract[object][docserie]' => trim($car->docserie),
                    'contract[object][docdate]' => $car->pts_date,
                    'contract[object][dk_number]' => $car->dk_number,
                    'contract[object][dk_date_from]' => $car->dk_date_from,
                    'contract[object][dk_date]' => $car->dk_date,
                    'contract[insurer][last_name]' => $car->family,
                    'contract[insurer][first_name]' => $car->name,
                    'contract[insurer][second_name]' => $car->nametwo,
                    'contract[insurer][phone]' => $car->phone ? oldSetPhoneNumberFormat($car->phone, '+d (ddd) ddd-dd-dd') : '',
                    'contract[lead][first_name]' => $car->name,
                    'contract[lead][phone]' => $car->phone ? oldSetPhoneNumberFormat($car->phone, '+d (ddd) ddd-dd-dd') : '',
                ];

//                dd($response);
                $region = Region::getRegion(request()->contract);
                if($region)
                    $response['contract[object][city]'] = $region;

                if(!isset($translated_car['model']))
                    $response['error'] = 'no_model';
                if(!isset($translated_car['brand']))
                    $response['error'] = 'no_brand';

                if(isset($translated_car['brand']))
                    $response['contract[object][mark_id]'] = $translated_car['brand'];
                else
                    $response['contract[object][mark_id]'] = -1;
                if(isset($translated_car['model']))
                    $response['contract[object][model_id]'] = $translated_car['model'];
                else
                    $response['contract[object][model_id]'] = -1;
                return response()->json($response);
                break;

            case 2:
                $integration = new Integration();
                $lead = $integration->make_lead(request());

                $regnum = request()['contract']['object']['reg_number'];
                $av_data = new AVData();
                $car = $av_data->getByGosnum($regnum);
                $car = $car ? $car : $av_data;

                return response()->json([
                    'calc' => view("contracts.contract_templates.online_contracts.partner.osago.calculation.precalc", [
                        'data' => round($car->getPreCalculation($regnum)  * 0.5, 2)
                    ])->render(),
                    'lead_id' => isset($lead) ? $lead->id : ''
                ]);
                break;

            case 3:
                $integration = new Integration();
                $id = (isset(request()->contract_id) && request()->contract_id) ? request()->contract_id : $integration->make_contract(request());
                $integration->fill_contract($id);
                $integration->fill_lead();
                $response = $integration->get_calc_list($id);
                //file_put_contents('logs.txt', serialize($_POST)."\n\n" , FILE_APPEND | LOCK_EX);
                Ask::confirmContract();
                return response()->json(['list' => $response, 'contract' => $id]);
                break;

            case 4:
                //return 'kek';
                $integration = new Integration();
                $calculation = $integration->get_calc_sk(request()->calc);
                $contract = $integration->get_contract($calculation->contract_id);
                if(!$contract)
                    $template_name = 'osago';
                else
                    $template_name = $contract->getProductOrProgram()->slug;

            return view("contracts.contract_templates.online_contracts.partner.osago.calculation.tariff", [
                        'calc' => $calculation
                    ])->render();

            case 5:
                $integration = new Integration();
                Ask::confirmChoice();
                $data = $integration->get_payment_link(request()->calc);
                return view("contracts.contract_templates.online_contracts.partner.osago.final", [
                    'data' => $data
                ])->render();
        }
    }

    public function addToken()
    {

        #toDo: сначала тестируем функцию. Вставляем ее в AVData->filter() и смотрим результат по адресу /sk-partner-integration/add-token
        //dd(config('mail'));
        //\Mail::raw('bonjour', function($message) {
        //    $message->subject('Email de test')
        //        ->to('test@example.org');
        //});
        //$test = new AVDataTest();
        //$cases = $test->test();


        //var_dump($cases); //тут должны быть объекты тачек


            //$test = new AVData();
        //var_dump($test->tst());
        //file_put_contents('logs.txt', json_encode($test->tst())."\n\n" , FILE_APPEND | LOCK_EX);
        //$car = $test->getByGosnum('ЯЯЯЯЯЯЯЯЯ');
//
        //var_dump($car->getCalculation());

        //var_dump(Region::getRegion('Т587МВ150'));
        //$car = new \stdClass();
        //$car->mark = 'Ford';
        //$car->model = 'Focusaaa';
        //$translated_car = VehicleMapper::getCarByGosnum2($car);
        //var_dump($translated_car);


        /*$partner_id = 1;
        $token = 'weq87e6qwe87qwe8q76e9qe';
        $private_key = '123456';

        $partner_id2 = 2;
        $token2 = 'guh34g53h45gb345';
        $private_key2 = '654321';

        $partner_id3 = 3;
        $token3 = '438080db9fbd0b9ofbvdf0b';
        $private_key3 = '098765';

        $tokens = new Collection();

        $tokens->push(IntegrationPartnersTokens::make($partner_id, $token, $private_key));
        $tokens->push(IntegrationPartnersTokens::make($partner_id2, $token2, $private_key2));
        $tokens->push(IntegrationPartnersTokens::make($partner_id3, $token3, $private_key3));

        return dd($tokens);*/

    }

    public function avdataStats()
    {
        echo 'Расшифровка: <a href="https://data.av100.ru/default.aspx?viewMode=18">https://data.av100.ru/default.aspx?viewMode=18</a> <br><br>';
        $av_data = new AVData();
        dd($av_data->getStats());
    }

}
