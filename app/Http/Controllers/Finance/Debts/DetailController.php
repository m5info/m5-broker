<?php

namespace App\Http\Controllers\Finance\Debts;

use App\Helpers\PaginationHelper;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Finance\Debts\DetailQueryHandler;
use App\Models\Characters\Agent;
use App\Models\Contracts\Debts;
use Illuminate\Http\Request;

class DetailController extends Controller {

    public function index($agent_id) {
        $agent = Agent::query()->findOrFail($agent_id);
        return view('finance.debts.detail.index', [
            'agent' => $agent,
        ]);
    }

    public function get_detail_list($agent_id) {

        $this->validate(request(), [
            'overdue' => 'int|min:0|max:3',
            'type' => 'in:sk,cash,cashless,',
        ]);

        $agent = Agent::query()->findOrFail($agent_id);
        $debts = Debts::query()->where(function($q) use ($agent_id){
            $q->where('agent_id', $agent_id);
            $q->orWhere('manager_id', $agent_id);
        })->with('bso');

        $debts = (new DetailQueryHandler($debts))->apply();


        /*$page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
        $result = PaginationHelper::paginate($debts, $page, $page_count);*/
//dd(getLaravelSql($debts));

        $d = $debts->get();

        $summary = ['all' => 0, 'cash' => 0, 'cashless' => 0, 'card' => 0, 'sk' => 0];
        foreach ($d as $type => &$debt) {
            $summary['all'] += $debt->payment_total;
        }

        $result = [
            'summary' => $summary,
            'agent' => $agent,
            'debts' => $d,
            /*'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],*/
        ];

        return $result;
    }

    public function get_detail_table($agent_id, Request $request) {
        $data = $this->get_detail_list($agent_id, $request);
        $data['html'] = view('finance.debts.detail.table', $data)->render();
        return $data;
    }

}
