<?php

namespace App\Http\Controllers\Finance\Debts;

use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Finance\Debts\AgentQueryHandler;
use App\Models\Characters\Agent;
use App\Models\Contracts\Debts;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Settings\UserBalanceSettings;

class AgentController extends Controller {

    public function index() {

        $user = auth()->user();

        if($user->role->rolesVisibility(7)->visibility == 2){
            return view('finance.debts.detail.index', [
                'agent' => Agent::query()->findOrFail($user->id),
            ]);
        }

        return view('finance.debts.agents.index');

    }

    public function get_agent_list() {

        $this->validate(request(), [
            'agent_id' => 'int',
            'parent_agent_id' => 'int',
            'overdue' => 'int|min:0|max:3',
        ]);

        $user_balances = UserBalanceSettings::where('is_actual', 1)->get();

        $agents = Agent::getAgents();//auth()->user()->visibleAgents('finance');
        $agents = (new AgentQueryHandler($agents))->apply();



        $debts = Debts::whereOverdue(request()->get('overdue'));

        $agents_deb = clone $agents;
        $agents_deb->select("id");
        $debts->whereRaw('`payments`.`agent_id` IN (' . $agents_deb->toSql() . ')', $agents_deb->getBindings());
        $debts->leftJoin('users', 'users.id', '=', 'payments.agent_id');
        $debts->groupBy('payments.agent_id');
        $debts->orderBy('users.name');

        $debts->select(\DB::raw(
            "users.name, payments.agent_id,
            sum(payments.payment_total) as debt_all,
            sum(if((payments.payment_type = 0), payments.payment_total, 0)) as debt_nal,
            sum(if((payments.payment_type = 1 && payments.payment_flow = 0), payments.payment_total, 0)) as debt_bn,
            sum(if((payments.payment_type = 1 && payments.payment_flow = 1), payments.payment_total, 0)) as debt_bn_sk,
            min(payment_data) as payment_data"
        ));


        return ['debts' => $debts->get(), 'user_balances' => $user_balances];
    }

    public function get_agent_table(Request $request) {
        $data['html'] = view('finance.debts.agents.table', $this->get_agent_list($request))->render();
        return $data;
    }

}
