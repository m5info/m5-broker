<?php

namespace App\Http\Controllers\Finance\ExpectedPayments;

use App\Classes\Export\ExportManager;
use App\Command\Finance\Invoice\CreateInvoiceAutomatically;
use App\Command\Finance\Invoice\CreateInvoiceToOneJure;
use App\Command\Finance\Invoice\CreateInvoiceToSomeJure;
use App\Helpers\PaginationHelper;
use App\Helpers\Visible;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Finance\Invoice\InvoicesQueryHandler;
use App\Models\Characters\Agent;
use App\Models\Contracts\Payments;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\Products;
use App\Models\Finance\Invoice;
use App\Models\Organizations\Organization;
use App\Models\Settings\TemplateCategory;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class ExpectedPaymentsController extends Controller{


    public function index()
    {

        $insurances = InsuranceCompanies::all();

        $organizations = Organization::all();

        $products = Products::all();

        $agents = Agent::all();

        $users = User::all();

        return view('finance.expected_payments.index', [
            'organizations' => $organizations,
            'insurances' => $insurances,
            'products' => $products,
            'agents' => $agents,
            'users' => $users,
        ]);


    }


    public function get_payments_table(Request $request)
    {

        $payments = Payments::getPayments();


        $payments->where('payments.statys_id', -2);
        $payments->leftJoin('contracts', 'contracts.id', '=', 'payments.contract_id');


        if (!empty(request('date_from'))) {
            $payments->whereDate('payments.payment_data', '>=', date('Y-m-d H:i:s', strtotime(request('date_from'))));
        }
        if (!empty(request('date_to'))) {
            $payments->whereDate('payments.payment_data', '<=', date('Y-m-d H:i:s', strtotime(request('date_to'))));
        }

        if(isset($request->agent_id) && $request->agent_id > 0){
            $payments->where('payments.agent_id', $request->agent_id);
        }

        if(isset($request->manager_id) && $request->manager_id > 0){
            $payments->where('payments.manager_id', $request->manager_id);
        }

        if(isset($request->nop_id) && $request->nop_id > 0){
            $payments->where('payments.parent_agent_id', $request->nop_id);
        }


        if(isset($request->insurance_ids) && is_array($request->insurance_ids)){
            $payments->whereIn('contracts.insurance_companies_id', $request->insurance_ids);
        }

        if(isset($request->org_ids) && is_array($request->org_ids)){

            $orgObsId = $request->org_ids;

            $payments->whereIn('contracts.bso_supplier_id', function($query) use ($orgObsId)
            {
                $query->select(\DB::raw('bso_suppliers.id'))
                    ->from('bso_suppliers')
                    ->whereIn('bso_suppliers.purpose_org_id', $orgObsId);

            });

            $payments->whereIn('contracts.bso_supplier_id', $request->org_ids);
        }

        if(isset($request->product_id) && is_array($request->product_id)){
            $payments->whereIn('contracts.product_id', $request->product_id);
        }


        $payments->select('payments.*');

        return view('finance.expected_payments.table', [
            'payments' => $payments->get()
        ]);


    }



}