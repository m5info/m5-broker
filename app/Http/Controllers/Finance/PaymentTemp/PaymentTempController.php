<?php

namespace App\Http\Controllers\Finance\PaymentTemp;

use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Helpers\EmptyObject;
use App\Http\Controllers\Controller;
use App\Models\Contracts\Payments;
use App\Models\User;
use Illuminate\Http\Request;

class PaymentTempController extends Controller{


    public function index($id){

        $payment = Payments::getPaymentId($id);

        if($payment && $payment->type_id == 0 && $payment->statys_id == 0 && $payment->contract->sales_condition == 1)
        {
            return view('finance.payment_temp.edit', [
                'payment' => $payment,
                'contract' => $payment->contract,
                'agents' => User::getALLUserWhere()->get()->pluck('name', 'id'),
            ]);
        }

        return abort(403);
    }


    public function save($id, Request $request)
    {

        $payment = Payments::getPaymentId($id);

        if($payment && $payment->type_id == 0 && $payment->statys_id == 0 && $payment->contract->sales_condition == 1)
        {
            $contract = $payment->contract;
            $old_contract = $contract ? $contract : new EmptyObject();

            $contract_data = (object)$request->contract;
            $payment_data = (object)$request->payment;

            $update_contract_data = [
                'user_id' => auth()->id(),

                'order_title' => isset($payment_data->order_title) ? $payment_data->order_title : $old_contract->order_title,
                'order_id' => isset($payment_data->order_id) ? $payment_data->order_id : $old_contract->order_id,

                'sign_date' => setDateTimeFormat($contract_data->sign_date),
                'begin_date' => setDateTimeFormat($contract_data->begin_date),
                'end_date' => setDateTimeFormat($contract_data->end_date),

                'payment_total' => getFloatFormat($payment_data->payment_total),
            ];

            $contract->update($update_contract_data);


            $update_payment_data = [

                'order_title' => isset($payment_data->order_title) ? $payment_data->order_title : $old_contract->order_title,
                'order_id' => isset($payment_data->order_id) ? $payment_data->order_id : $old_contract->order_id,

                'payment_data' => setDateTimeFormat($payment_data->payment_data),
                'payment_total' => getFloatFormat($payment_data->payment_total),

                'official_discount' => getFloatFormat($payment_data->official_discount),
                'informal_discount' => getFloatFormat($payment_data->informal_discount),
                'bank_kv' => getFloatFormat($payment_data->bank_kv),

            ];

            $payment->update($update_payment_data);

            //пересчитываем скидки для платежа
            $payment = PaymentDiscounts::recount($payment);

            $payment->invoice->refreshInvoice();
            return parentReload();

        }

        return abort(403);
    }


    public function delete_payment($id){
        $payment = Payments::getPaymentId($id);
        $result = (object)['status'=>0, "msg"=>"Доступ запрещен!"];

        if($payment && $payment->type_id == 0 && $payment->statys_id == 0 && $payment->contract->sales_condition == 1)
        {
            $contract = $payment->contract;
            $contract->destroyDataToFront();

            foreach ($contract->payments as $payment){
                $payment->deletePayment();
            }

            $contract->statys_id = -1;
            $contract->save();

            $contract->bso->state_id = 0;
            $contract->bso->save();


            $result = (object)['status'=>1, "msg"=>""];
        }

        return response()->json($result);
    }




}