<?php

namespace App\Http\Controllers\Finance\Invoice;


use App\Http\Controllers\Controller;
use App\Models\Finance\Invoice;

class MainController extends Controller{


    public function index(){

        return view('finance.invoice.index');
    }


    public function view($id)
    {
        $invoice = Invoice::getInvoiceId($id);
        $invoice_info = $invoice->getInfoSum();

        return view('finance.invoice.view', [
            'invoice' => $invoice,
            'invoice_info' => $invoice_info,
        ]);
    }




}