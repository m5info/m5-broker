<?php

namespace App\Http\Controllers\Finance\Invoice;

use App\Helpers\PaginationHelper;
use App\Helpers\Visible;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Finance\Invoice\PaymentsQueryHandler;
use App\Models\Contracts\Debts;
use App\Models\Contracts\Payments;
use App\Models\Finance\Invoice;
use Faker\Provider\Payment;
use Illuminate\Http\Request;

class PaymentsController extends Controller {

    public function index() {
        return view('finance.invoice.payments.index');
    }

    public function get_payments_table() {


        session(['finance.agent_id' => request('agent_id', auth()->id())]);

        $data = $this->get_payments_list();
        $data['html'] = view('finance.invoice.payments.table', $data)->render();
        return $data;
    }

    public function get_payments_list() {

        $this->validate(request(), [
            'agent_id' => 'required|int|min:0',
            'type' => 'in:sk,cash,cashless,',
        ]);


        //$payments = auth()->user()->visibleDebts()->whereRaw("(invoice_id IS NULL OR invoice_id = 0)")->where('is_deleted', 0);
        $payments = Payments::query()->whereRaw("(invoice_id IS NULL OR invoice_id = 0)")
            ->where('is_deleted', 0)->where('statys_id', 0)
            ->with('bso','contract');

        $payments = (new PaymentsQueryHandler($payments))->allowEmpty()->apply();


        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
        $result = PaginationHelper::paginate($payments, $page, $page_count);

        $result['builder']->orderBy('payment_data', 'desc');
        $payments = $result['builder']->get();

        return [
            'payments' => $payments,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row'  => $result['max_row'],
            'view_row' => $result['view_row'],
        ];
    }



    public function update_invoice(Request $request) {
        $messages = [];
        $invoice_id = $request->get('invoice_id');
        $payment_ids = $request->get('payments');
        $user = auth()->user();

        if ($invoice_id && $payment_ids) {
            $this->validate($request, [
                'invoice_id' => 'exists:invoices,id',
                'payments' => 'exists:payments,id'
            ]);

            $invoice = $user->visibleInvoices()->where('id', $request->get('invoice_id'))->get()->keyBy('id')->first();

            $payments = $user->visibleDebts()->whereIn('id', $request->get('payments'))
                ->whereRaw("(invoice_id IS NULL OR invoice_id = 0)")
                ->where('payment_type', $invoice->payments->first()->payment_type)
                ->get()->keyBy('id');


            if ($payments->count() > 0 && $invoice) {
                foreach ($payments as $payment) {
                    $payment->invoice_id = $invoice->id;
                    if ($payment->save()) {
                        $messages[] = ['type' => 'success', 'name' => 'В счет добавлен платеж. Платеж: ' . $payment->invoice_id];
                    } else {
                        $messages[] = ['type' => 'error', 'name' => 'В счет не удалось добавить платеж. Платеж: ' . $payment->invoice_id];
                    }
                }
            } else {
                $messages[] = ['type' => 'error', 'name' => 'Нельзя сформировать счет'];
            }
        }

        $organizations = collect([]);
        $invoices = $user->visibleInvoices()->get();
        $payments = $user->visibleDebts()->whereIn('id', $request->get('payments'))->get()->keyBy('id');

        $payments->map(function($payment) use($organizations) {
            $payment->type = $payment->type();
            $organizations->push($payment->bso->supplier_org);
        });
        $types_count = $payments->pluck('type')->flip()->count();
        if ($types_count > 1) {
            if ($request->ajax()) {
                return response()->json(['error' => 'Нельзя сформировать счёт с разными типами платежей.']);
            } else {
                return parentReload();
            }
        } else {
            if ($request->ajax()) {
                return response()->json(['status' => 'ok']);
            }
        }

        $select = [];
        foreach ($invoices as $item) {
            $select[$item->id] = $item->id . ' ( ' . $organizations->where('id', '=', $item->org_id)->pluck('title')->first() . ', ' . Invoice::TYPES[$item->type] . ' )';
        }

        return view('finance.invoice.payments.update', [
            'invoices' => $invoices,
            'payments' => $payments,
            'select' => $select,
            'messages' => $messages,
        ]);
    }

}
