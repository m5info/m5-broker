<?php

namespace App\Http\Controllers\Finance\Invoice;

use App;
use Maatwebsite\Excel\Facades\Excel;
use App\Classes\Export\ExportManager;
use App\Command\Finance\Invoice\CreateInvoiceAutomatically;
use App\Command\Finance\Invoice\CreateInvoiceToOneJure;
use App\Command\Finance\Invoice\CreateInvoiceToSomeJure;
use App\Helpers\PaginationHelper;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Finance\Invoice\InvoicesQueryHandler;
use App\Models\Contracts\Payments;
use App\Models\Finance\Invoice;
use App\Models\Settings\TemplateCategory;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class InvoicesController extends Controller{


    public function index(){
        return view('finance.invoice.invoices.index');
    }

    public function get_invoices_table(Request $request){


        $data = $this->get_invoices_list($request);
        $data['html'] = view('finance.invoice.invoices.table', $data)->render();
        return $data;
    }


    public function get_invoices_list(Request $request){

        $this->validate($request, [
            'type' => 'in:sk,cash,cashless',
            'status' => 'integer',
            'invoice_number' => 'integer'
        ]);


        $invoices = (new InvoicesQueryHandler(auth()->user()->visibleInvoices()))->apply()
            ->with('agent','payments','org');

        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
        $result = PaginationHelper::paginate($invoices, $page, $page_count);
        if(request('invoice_number')){
            $result['builder']->where('id', (int)request('invoice_number'));
        }
        $result['builder']->orderBy('id', 'desc')->get();

        return [
            'invoices' => $invoices->get(),
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];

    }


    public function edit($invoice_id){
        $invoice = Invoice::query()->findOrFail($invoice_id);
        $invoice_info = $invoice->getInfoSum();

        $this->breadcrumbs[] = [
            'label' => 'Счета',
            'url' => 'finance/invoice',
        ];

        $this->breadcrumbs[] = [
            'label' => 'Редактирование',
        ];

        return view('finance.invoice.invoices.edit',[
            'invoice' => $invoice,
            'invoice_info' => $invoice_info,
        ])->with('breadcrumbs', $this->breadcrumbs);
    }


    public function save($invoice_id, Request $request){
        $this->validate($request,[
            'type' => 'in:sk,cash,cashless',
            'org_id' => 'exists:organizations,id'
        ]);

        $invoice = Invoice::query()->findOrFail($invoice_id);
        $invoice->type = $request->get('type');
        $invoice->org_id = $request->get('org_id');
        $invoice->save();
        return back()->with('success', trans('form.success_update'));
    }

    public function create(Request $request){

        $user = auth()->user();
        $organizations = collect([]);
        $free_invoice_group = $user->hasPermission('finance', 'free_invoice_group');


        //dd($request->get('payments'));

        //dd(getLaravelSql($user->visibleDebts()->whereIn('id', $request->get('payments'))));
        //$payments = $user->visibleDebts()->whereIn('id', $request->get('payments'))->get()->keyBy('id');
        //$payments = Payments::where('agent_id', $request->get('agent_id'))->whereIn('id', $request->get('payments'))->get()->keyBy('id');

        $payments = Payments::query()->where('statys_id', 0)->whereIn('id', $request->get('payments'))->get()->keyBy('id');



        foreach ($payments as $payment){
            $pt = $payment->type();
            $pt = ($pt == 'sk' && $free_invoice_group) ? 'cashless' : $pt;
            $payment->type = $pt;
            $organizations->push($payment->bso->supplier_org);
        }
        $types_count = $payments->pluck('type')->flip()->count();

        if($request->ajax()){
            return response()->json(['status' => 'ok']);
        }

        return view('finance.invoice.invoices.create',[
            'organizations' => $organizations,
            'payments' => $payments,
        ]);
    }


    public function store(Request $request){

        $this->validate($request, [
            'create_type' => 'int|min:1|max:3',
            'org_id' => 'int|required_if:create_type,2'
        ]);

        $user = auth()->user();
        $payments = Payments::query()->where('statys_id', 0)->whereIn('id', $request->get('payments'))->get()->keyBy('id');

        $payment_types = collect([]);
        foreach($payments as $payment){
            $payment_types->push($payment->type());
        }

        $payments_arrays = array();

        $payments_sk = []; // Разбиение платежей в СК (Безнал СК)
        $other_payments = []; // Разбиение платежей в СК (Все, кроме безнал СК)

        foreach($payments as $payment){

            if ($payment->bso && $payment->bso->supplier){

                $current_type = $payment->type();

                if ($current_type != 'sk'){ // Не Безнал СК

                    $other_payments[$current_type][] = $payment;

                }else{

                    if (in_array($payment->bso->supplier->id, $payments_sk)){
                        $payments_sk[$payment->bso->supplier->id][$payment->type()][] = $payment;
                    }else{
                        $payments_sk[$payment->bso->supplier->id][$payment->type()][] = $payment;
                    }
                }

            }

            $payments_arrays[$payment->type()][] = $payment;
        }

        $payments_sk[] = $other_payments;

        $payment_types = $payment_types->flip()->flip();

        if(request('create_type') == 1){ // автоматическое

            foreach($payments_sk as $payment_sk){

                foreach($payment_sk as $key => $payments_array){

                    $pipes = [
                        CreateInvoiceAutomatically::class,
                        CreateInvoiceToOneJure::class,
                        CreateInvoiceToSomeJure::class,
                    ];

                    $data = [
                        'payments' => collect($payments_array),
                        'payment_types' => collect($key),
                    ];

                    $invoice = app(Pipeline::class)->send($data)->through($pipes)->then(function ($data) {});


                    foreach ($payment_sk as $payment){
                        foreach($payment as $p){
                            if($contract = $p->contract){
                                if($contract->kind_acceptence == 0){ // если Условный
                                    if($contract->sales_condition == 1){ // если менеджеркая продажа
                                        $contract->update(['statys_id' => 3]);
                                    }else{
                                        $contract->update(['statys_id' => 2]);
                                    }
                                }else{// если не Условный
                                    $contract->update(['statys_id' => 5]);
                                }
                            }
                        }
                    }
                }

            }

        }else{

            foreach($payments_sk as $payment_sk){

                if (count($payment_sk) > 1){
                    return frameError('Нельзя сформировать счёт с разными типами платежей для этого типа операции.');
                }
            }

            foreach ($payments_sk as $type_payment_sk => $payment_sk){

                    foreach ($payment_sk as $p_s_key => $p_s_v){

                    $pipes = [
                        CreateInvoiceAutomatically::class,
                        CreateInvoiceToOneJure::class,
                        CreateInvoiceToSomeJure::class,
                    ];

                    $data = [
                        'payments' => $p_s_v,
                        'payment_types' => $p_s_key,
                    ];

                    $invoice = app(Pipeline::class)->send($data)->through($pipes)->then(function ($data) {});


                    foreach ($p_s_v as $payment){
                        if($contract = $payment->contract){
                            if($contract->kind_acceptence == 0){ // если Условный
                                $contract->update(['statys_id' => 2]);
                            }else{ // если не Условный
                                $contract->update(['statys_id' => 5]);
                            }
                        }
                    }
                }
            }

        }

        return closeFancyBoxTabsWithSetPage('#tt',1);
    }


    public function delete_payments($invoice_id){

        $this->validate(request(), [
           'payment' => 'array',
           'payment.*' => 'integer',
        ]);

        $result = ['status'=>'ok', 'type' => 'payment'];

        $invoice = Invoice::query()
            ->where('status_id', 1)
            ->findOrFail($invoice_id);

        $payments = $invoice->payments()->whereIn('id', request('payment'));

        $pall_count = $invoice->payments()->count();
        $p_count = $payments->count();

        $payments->update(['invoice_id' => 0]);

        if($p_count == $pall_count){
            $this->delete_invoice($invoice_id);
            $result['type'] = 'invoice';
        }

        return response()->json($result);

    }

    public function delete_invoice($invoice_id){

        $invoice = Invoice::query()
            ->where('status_id', 1)
            ->findOrFail($invoice_id);

        $invoice->payments->map(function($payment){
            $payment->invoice_id = 0;
            $payment->save();
        });

        $invoice->delete();
        return response()->json(['status'=>'ok']);

    }


    public function update(Request $request){
        $messages = [];
        $invoice_id = $request->get('invoice_id');
        $payment_ids = $request->get('payments');
        $user = auth()->user();
        $free_invoice_group = $user->hasPermission('finance', 'free_invoice_group');


        if($invoice_id && $payment_ids){

            $this->validate($request, [
                'invoice_id' => 'exists:invoices,id',
                'payments'   => 'exists:payments,id',
            ]);

            $invoice = Invoice::query()
                ->where('status_id', 1)
                ->findOrFail($invoice_id);
            $isPaymentValid = in_array($invoice_id, $user->visibleDebts()->pluck('id')->toArray());
            if(!$isPaymentValid){
                $messages[] = [
                    'type' => 'error',
                    'name' => 'Нельзя добавить этот платеж',
                ];
            }
            if($invoice->payments()->count() >= 1 && $isPaymentValid){
                foreach($payment_ids as $id){
                    $payment = Payments::query()->findOrFail($id);
                    if($invoice->payments->first()->payment_type !== $payment->payment_type){
                        $messages[] = [
                            'type' => 'error',
                            'name' => 'Нельзя сформировать счёт с разными типами платежей. Платеж: ' . $id,
                        ];
                    }elseif($payment->invoice_id){
                        $messages[] = [
                            'type' => 'error',
                            'name' => 'Нельзя добавить прикрепленный платеж',
                        ];
                    }else{
                        $payment->invoice_id = $invoice_id;
                        if($payment->save()){
                            $messages[] = [
                                'type' => 'success',
                                'name' => 'В счет добавлен платеж. Платеж: ' . $id,
                            ];
                        }else{
                            $messages[] = [
                                'type' => 'error',
                                'name' => 'В счет не удалось добавить платеж. Платеж: ' . $id,
                            ];
                        }
                    }
                }
            }
        }

        $organizations = collect([]);
        $invoices = $user
            ->visibleInvoices()
            ->where('status_id', 1)
            ->get();
        $payments = $user->visibleDebts()->whereIn('id', $request->get('payments'))->get()->keyBy('id');

        foreach ($payments as $payment){
            $pt = $payment->type();
            $pt = ($pt == 'sk' && $free_invoice_group) ? 'cashless' : $pt;
            $payment->type = $pt;
            $organizations->push($payment->bso->supplier_org);
        }

        $types_count = $payments->pluck('type')->flip()->count();
        if($types_count > 1){
            if($request->ajax()){
                return response()->json(['error' => 'Нельзя сформировать счёт с разными типами платежей.']);
            }else{
                return parentReload();
            }
        }else{
            if($request->ajax()){
                return response()->json(['status' => 'ok']);
            }
        }

        $select = [];
        foreach($invoices as $item){
            $select[$item->id] = $item->id . ' ( ' . $organizations->where('id', '=', $item->org_id)->pluck('title')->first() . ', ' . Invoice::TYPES[$item->type] . ' )';
        }

        return view('finance.invoice.invoices.update', [
            'invoices' => $invoices,
            'payments' => $payments,
            'select'   => $select,
            'messages' => $messages,
        ]);
    }

    public function export_many(Request $request){

        $this->validate($request, [
            'type' => 'in:sk,cash,cashless',
            'status_id' => 'integer',
            'agent_id' => 'integer'
        ]);

        $invoices = (new InvoicesQueryHandler(auth()->user()->visibleInvoices()))->apply()
            ->with('agent','payments','org')->get()->reverse();

        $sheets = [];

        $export = app('App\Http\Controllers\Exports\ExportsController');

        foreach($invoices as $invoice){

            $invoice = Invoice::query()->findOrFail($invoice->id);

            $sheet = view('finance.invoice.invoices.export.export_many', ['invoice' => $invoice])->render();

            preg_match('/<table(.*?)>(.*?)<\/table>/s', $sheet, $mtable);
            preg_match('/<thead(.*?)>(.*?)<\/thead>/s', $mtable[0], $mhead);
            preg_match('/<tbody(.*?)>(.*?)<\/tbody>/s', $mtable[0], $mbody);

            $sheets[] = [
                'invoice_id' => $invoice->id,
                'invoice_fio' => ['ФИО агента:',$invoice->agent_id ? App\Models\User::find($invoice->agent_id)->name : ''],
                'invoice_date' => ['Дата создания счета', $invoice->created_at],
                'head' => $export->getParseThToArr($mhead[0]),
                'body' => $export->getParseTdToArr($mbody[0])
            ];
        }

        Excel::create(date('Y-m-d H:i:s'), function($excel) use ($sheets) {

            foreach($sheets as $key => $sheet_data){

                $sheet_n = $sheet_data['invoice_id'];

                $excel->sheet("Счет № $sheet_n", function($sheet) use ($sheet_data) {

                    $arr_letter = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];

                    foreach($sheet_data['invoice_fio'] as $hk => $val){
                        $sheet->setCellValueByColumnAndRow($hk+1, 1, $val);
                        $sheet->row(1,function($row){
                            $row->setFontWeight('bold');
                        });
                    }

                    foreach($sheet_data['invoice_date'] as $hk => $val){
                        $sheet->setCellValueByColumnAndRow($hk+1, 2, $val);
                        $sheet->row(2,function($row){
                            $row->setFontWeight('bold');
                        });
                    }

                    foreach($sheet_data['head'] as $hk => $val){
                        $sheet->setCellValueByColumnAndRow($hk, 3, $val);
                        $sheet->row(3,function($row){
                            $row->setFontWeight('bold');
                        });
                        foreach($arr_letter as $letter){
                            $sheet->cell($letter."3", function($cell) use ($letter){
                                $cell->setBorder('medium','thin','medium','thin');
                            });
                        }
                    }
                    $count_elems = count($sheet_data['body']);
                    $i = 0;
                    foreach($sheet_data['body'] as $row_key => $row){
                        $i++;
                        foreach($row as $cell_key => $cell){

                            $cell = html_entity_decode($cell);
                            $sheet->setCellValueByColumnAndRow($cell_key, 4 + $row_key, $cell);

                            if($i != $count_elems){
                                $sheet->row(4 + $row_key,function($row){
                                    $row->setBorder('thin', 'thin', 'thin', 'thin');
                                });

                                foreach($arr_letter as $letter){
                                    $sheet->cell($letter.($row_key + 4), function($cell){
                                        $cell->setBorder('thin','thin','thin','thin');
                                    });
                                }
                            }else{
                                $sheet->row(4 + $row_key,function($row){
                                    $row->setBorder('medium', 'none', 'none', 'none');
                                });
                            }

                        }
                    }

                    $sheet->cell("K".($count_elems + 3), function($cell){
                        $cell->setBorder('medium','medium','medium','medium');
                    });
                    $sheet->cell("L".($count_elems + 3), function($cell){
                        $cell->setBorder('medium','medium','medium','medium');
                    });
                    $sheet->setAutoSize(true);

                    $sheet->setHeight(3, 50);
                    $sheet->setWidth(array(
                        'A'     =>  3,
                        'C'     =>  15,
                        //'E'     =>  6,
                        //'I'     =>  4,
                        //'J'     =>  7,
                    ));


                });

                $excel->setActiveSheetIndex(0);
            }

        })->export('xlsx');
    }


    public function direction($invoice_id){
        $invoice = Invoice::query()->findOrFail($invoice_id);

        return view('finance.invoice.invoices.export.direction',[
            'invoice' => $invoice,
        ]);
    }

    public function act_export($invoice_id){

        $builder = Invoice::query()->where('id', $invoice_id);

        $builder->firstOrFail();//просто чтоб фейлил левые запросы

        $category = TemplateCategory::get('act_checkout');

        return (new ExportManager($category, $builder, false, true, '777'))->handle();
    }

    public function act_export_under($invoice_id){

        $builder = Invoice::query()->where('id', $invoice_id);

        $builder->firstOrFail();//просто чтоб фейлил левые запросы

        $category = TemplateCategory::get('act_to_under');

        return (new ExportManager($category, $builder))->handle();
    }

    public function act_for_pay_cash($invoice_id){

        $builder = Invoice::query()->where('id', $invoice_id);

        $builder->firstOrFail();//просто чтоб фейлил левые запросы

        $category = TemplateCategory::get('act_for_pay_cash');

        return (new ExportManager($category, $builder))->handle();
    }

    public function act_for_pay_cashless($invoice_id){

        $builder = Invoice::query()->where('id', $invoice_id);

        $builder->firstOrFail();//просто чтоб фейлил левые запросы

        $category = TemplateCategory::get('act_for_pay_cashless');

        return (new ExportManager($category, $builder))->handle();
    }

    public function act_for_pay_cashless_card_broker($invoice_id){

        $builder = Invoice::query()->where('id', $invoice_id);

        $builder->firstOrFail();//просто чтоб фейлил левые запросы

        $category = TemplateCategory::get('act_for_pay_cashless_card_broker');

        return (new ExportManager($category, $builder))->handle();
    }




}