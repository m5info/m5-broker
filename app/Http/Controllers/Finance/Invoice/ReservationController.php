<?php

namespace App\Http\Controllers\Finance\Invoice;


use App\Classes\Export\ExportManager;
use App\Classes\Export\Replacers\ExcelReplacer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Finance\Invoice\ReservationStoreSaveRequest;
use App\Models\BSO\BsoItem;
use App\Models\Finance\Reservation;
use App\Models\Settings\ExportItem;
use App\Models\Settings\Template;
use App\Models\Settings\TemplateCategory;
use App\Models\User;

class ReservationController extends Controller{


    public function __construct()
    {
        $this->breadcrumbs[] = [
            'label' => 'Резервные счёта',
            'url' => "finance/invoice/",
        ];
    }

    public function index(){

        return view('finance.invoice.reservation.index');
    }


    public function get_reservation_table(){
        $data = $this->get_reservation_list();
        $data['html'] = view('finance.invoice.reservation.table', $data)->render();
        return $data;

    }

    public function get_reservation_list(){


        $this->validate(request(), [
            'agent_id' => 'required|int|min:0',
        ]);

        $reservations = Reservation::query();
        if(request()->get('agent_id')){
            $reservations->where('user_id', '=', request()->get('agent_id'));
        }
        $reservations->orderBy('created_at', 'desc');
        $reservations = $reservations->get();

        $result = [
            'reservations' => $reservations
        ];

        return $result;
    }




    public function create()
    {

        $this->breadcrumbs[] = ['label' => 'Создание'];

        return view('finance.invoice.reservation.create', [
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function store(ReservationStoreSaveRequest $request){



        $reservation = new Reservation();

        $bsos = collect(request()->get('data')['bso']);
        $sum = 0;
        foreach ($bsos as $bso){
            $sum += getFloatFormat($bso['bso_sum']);
        }

        $reservation->user_id = $request->user_id;

        $reservation->data = request()->get('data');

        $reservation->amount = $sum;

        if($reservation->save()){
            return response()->json(['status' => 'ok', 'id' => $reservation->id]);
        }else{
            return response()->json(['status' => 'error']);
        }

    }




    public function edit($reservation_id)
    {

        $this->breadcrumbs[] = ['label' => "Счёт № $reservation_id"];


        $reservation = Reservation::query()->findOrFail($reservation_id);
        return view('finance.invoice.reservation.edit',[
            'reservation' => $reservation,
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function save(ReservationStoreSaveRequest $request, $reservation_id){


        $bsos = collect(request()->get('data')['bso']);
        $sum = 0;
        foreach ($bsos as $bso){
            $sum += getFloatFormat($bso['bso_sum']);
        }


        $reservation = Reservation::query()->findOrFail($reservation_id);
        $reservation->user_id = $request->user_id;
        $reservation->data = request()->get('data');
        $reservation->my_org = request()->get('my_org', 0);
        $reservation->amount = $sum;

        if($reservation->save()){
            return redirect("/finance/invoice/reservation/{$reservation->id}/edit/")->with('success', 'Успешно');
        }else{
            return back()->with('error', 'Не удалось сохранить');
        }

    }


    public function delete($reservation_id){
        $result = ['status' => 'error'];
        $reservation = Reservation::query()->findOrFail($reservation_id);
        if($reservation->delete()){
            $result = ['status' => 'ok', 'msg' => 'Резервирование усешно удалено'];
        }
        return response()->json($result);
    }


    public function export($reservation_id){

        $builder = Reservation::query()->where('id', $reservation_id);
        $category = TemplateCategory::get('invoice_reservation');
        $builder->firstOrFail();
        return (new ExportManager($category, $builder))->handle();

    }


}