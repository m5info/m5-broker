<?php

namespace App\Http\Controllers\Contracts\Techunder;

use App\Helpers\PaginationHelper;
use App\Http\Controllers\Controller;
use App\Repositories\ContractsTechunderRepository;
use Illuminate\Http\Request;
use App\Models\Contracts\Contracts;
use App\Models\Actions\PaymentAccept;
use Carbon\Carbon;

class TechunderController extends Controller {

    public function __construct() {
        $this->middleware('permissions:contracts,contracts_techunder');
    }

    public function index(Request $request) {

        $model = Contracts::getContractsQuery()->whereIn('statys_id', [4]);

        $accepts = PaymentAccept::where('accept_user_id', '>', '0');
        if (isset($request->underwriter_id) && $request->underwriter_id > 0) {
            if (is_array($request->underwriter_id)) {
                $accepts->where('accept_user_id', 'in', $request->underwriter_id);
            } else {
                $accepts->where('accept_user_id', '=', $request->underwriter_id);
            }
        }
        if (isset($request->product_id) && $request->product_id > 0) {
            $accepts->whereHas('contract', function($query) use ($request) {
                $query->where('product_id', '=', $request->product_id);
            });
        }
        if (isset($request->from)) {
//            $accepts->where('accept_date', '>=', $from->format('Y-m-d'));
            $accepts->whereHas('contract', function($query) use ($request) {
                $from = new Carbon($request->from);
                $query->where('check_date', '>=', $from->format('Y-m-d'));
            });
        }
        if (isset($request->to)) {
//            $accepts->where('accept_date', '<=', $to->format('Y-m-d'));
            $accepts->whereHas('contract', function($query) use ($request) {
                $to = new Carbon($request->to);
                $query->where('check_date', '<=', $to->format('Y-m-d'));
            });
        }

        $model->whereIn('id', $accepts->get()->pluck('contract_id')->toArray());

        $agents = PaymentAccept::select('accept_user_id')->distinct()->get();
        foreach ($agents as $i => $item) {
            if(isset($item->accept_user)){ // проверка(был ексепшн)
                $agents[$i]['name'] = $item->accept_user->name;
            }
        }
        /* filters */
        if (isset($request->underwriter_id) && $request->underwriter_id > 0) {
            $model->whereIn('id', PaymentAccept::where('accept_user_id', '=', $request->underwriter_id)->pluck('contract_id')->toArray());
        }
        /* end */

        $count = $model->count();
        $result["limit"] = request()->get('limit') ?? 25;

        $from = 0;
        if ($request['page'] && (int) $request['page']) {
            $from = ((int) $request['page'] - 1) * $result["limit"];
        }
        $model->orderBy('id', 'DESC')->skip($from)->take($result["limit"]);
        $contracts = $model->get();
        $paginator = createPageNavigator($count, $request['page'], $result["limit"], $_SERVER['REQUEST_URI']);

        return view('contracts.techunder.index', [
            'contracts' => $contracts,
            'count' => $count,
            'paginator' => $paginator,
            'agents' => $agents,
        ]);
    }

    public function get_table(Request $request){


        $techunderRepository = new ContractsTechunderRepository();

        $model = $techunderRepository->getContractsDischargeForTable($request);

        $result["limit"] = request()->get('limit') ?? 25;

        $from = 0;
        if ($request['page'] && (int) $request['page']) {
            $from = ((int) $request['page'] - 1) * $result["limit"];
        }
        $model->orderBy('id', 'DESC')->skip($from)->take($result["limit"]);

        $paginator = PaginationHelper::paginate($model, $request['PAGE'],  $request['page_count']);
        $contracts = $paginator['builder']->get();

        return [
            'html' => view('contracts.partials.contracts_table', ['contracts'=> $contracts])->render(),
            'page_max' => $paginator['page_max'],
            'page_sel' => $paginator['page_sel'],
            'max_row' => $paginator['max_row'],
            'view_row' => $paginator['view_row'],
        ];



    }

    public function to_excel(Request $request) {
        $model = Contracts::getContractsQuery()->whereIn('statys_id', [4]);

        $accepts = PaymentAccept::where('accept_user_id', '>', '0');
        if (isset($request->underwriter_id) && $request->underwriter_id > 0) {
            if (is_array($request->underwriter_id)) {
                $accepts->where('accept_user_id', 'in', $request->underwriter_id);
            } else {
                $accepts->where('accept_user_id', '=', $request->underwriter_id);
            }
        }
        if (isset($request->product_id) && $request->product_id > 0) {
            $accepts->whereHas('contract', function($query) use ($request) {
                $query->where('product_id', '=', $request->product_id);
            });
        }
        if (isset($request->from)) {
//            $accepts->where('accept_date', '>=', $from->format('Y-m-d'));
            $accepts->whereHas('contract', function($query) use ($request) {
                $from = new Carbon($request->from);
                $query->where('check_date', '>=', $from->format('Y-m-d'));
            });
        }
        if (isset($request->to)) {
//            $accepts->where('accept_date', '<=', $to->format('Y-m-d'));
            $accepts->whereHas('contract', function($query) use ($request) {
                $to = new Carbon($request->to);
                $query->where('check_date', '<=', $to->format('Y-m-d'));
            });
        }

        $model->whereIn('id', $accepts->get()->pluck('contract_id')->toArray());

        $agents = PaymentAccept::select('accept_user_id')->distinct()->get();
        foreach ($agents as $i => $item) {
            if($item->accept_user){
                $agents[$i]['name'] = $item->accept_user->name;
            }
        }

        /* filters */
        if (isset($request->underwriter_id) && $request->underwriter_id > 0) {
            $model->whereIn('id', PaymentAccept::where('accept_user_id', '=', $request->underwriter_id)->pluck('contract_id')->toArray());
        }
        /* end */

        $contracts = $model->get();


        return ['html' => view('contracts.partials.contracts_table', [
                'contracts' => $contracts,
                'agents' => $agents,
            ])->render()];
    }

}
