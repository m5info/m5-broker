<?php
namespace App\Http\Controllers\Contracts\ReceptionContracts\Details;

use App\Http\Controllers\Controller;
use App\Models\BSO\BsoActs;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use Illuminate\Http\Request;

class ReceptionContractsDetailsController extends Controller{

    public function __construct() {
        $this->middleware('permissions:contracts,reception_contracts');
    }

    public function index($id){

        //where('under_accepted_id', 0)

        $act = BsoActs::query()
            ->where('type_id', 10)
            ->findOrFail((int)$id);


        $payments = Payments::where('acts_to_underwriting_id', $id);


        $this->breadcrumbs[] = [
            'label' => 'Приём договоров из кассы',
            'url' => 'contracts/reception_contracts',
        ];

        $this->breadcrumbs[] = [
            'label' => 'Редактирование ' . $act->act_number,
        ];

        return view('contracts.reception_contracts.details.index',[
            'payments' => $payments->get(),
            'act' => $act,
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function accept($id){

        $under_id = auth()->id();

        $act = BsoActs::query()->where('type_id', 10)
            ->where('under_accepted_id', 0)->findOrFail((int)$id);

        $act->update([
            'under_accepted_date' => date('Y-m-d H:i:s', time()),
            'under_accepted_id' => $under_id,
        ]);


        $payments = Payments::where('acts_to_underwriting_id', $act->id)->get();
        foreach ($payments as $payment){

            $payment->contract->update([
                'statys_id' => 1
            ]);

            $payment->bso->update([
                'user_id' => $under_id
            ]);

            if($payment->receipt){
                $payment->receipt->update([
                    'user_id' => $under_id
                ]);
            }

        }


        return redirect('/contracts/reception_contracts/')->with('success', 'Бсо принято успешно');
    }


    public function delete_items($act_id, Request $request){

        $item_array = \GuzzleHttp\json_decode($request->get('item_array'));
        $act = BsoActs::getActId($act_id);
        $act->deleteItemsToUnderwritingActs($item_array);
        return response(200);
    }

    public function delete_act($act_id){

        $act = BsoActs::getActId($act_id);
        $act->deleteToUnderwritingActs();
        return response(200);
    }

}