<?php

namespace App\Http\Controllers\Contracts\ReceptionContracts;

use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Contracts\ReceptionContracts\FromCashboxActsQueryHandler;
use App\Models\BSO\BsoActs;
use App\Models\Characters\Cashier;
use App\Models\Contracts\Contracts;

class ReceptionContractsController extends Controller{

    public function __construct() {
        $this->middleware('permissions:contracts,reception_contracts');
    }

    public function index(){
        return view('contracts.reception_contracts.index');
    }

    public function get_table() {
        $data = $this->get_list();
        $data['html'] = view('contracts.reception_contracts.table', $data)->render();
        return $data;
    }


    public function get_list(){

        $this->validate(request(), [
            'user_from' => 'integer',
            'status' => 'integer',
            'number' => 'string',
        ]);


        $acts = BsoActs::query()
            ->select([
                'bso_acts.id as bso_acts_id',
                'bso_acts.act_number',
                'bso_acts.time_create',
                'bso_acts.target_date',
                'users.name as creator',
                'bso_cart_types.short_title as short_title',
                'bso_items.id as bso_items_id',
                'payments.id as payments_id',
                'invoices.id as invoices_id'
            ])
            ->leftJoin('bso_cart_types', 'bso_cart_types.id', '=', 'bso_acts.type_id')
            ->leftJoin('users', 'users.id', '=', 'bso_acts.user_id_from')
            ->leftJoin('payments', 'payments.acts_to_underwriting_id', '=', 'bso_acts.id')
            ->leftJoin('bso_items', 'bso_items.id', '=', 'payments.bso_id')
            ->leftJoin('invoices', 'invoices.id', '=', 'payments.invoice_id')
            ->whereIn('user_id_from', Cashier::all()->pluck('id'))
            ->where('bso_acts.type_id', 10);


        $acts = (new FromCashboxActsQueryHandler($acts))->allowEmpty()->apply();


        //$acts->orderBy('bso_acts.time_create', 'desc');
        $acts->orderBy('invoices.id', 'desc');
        $acts->groupBy('act_number');

        return [
            'acts' => $acts->get()
        ];
    }


}