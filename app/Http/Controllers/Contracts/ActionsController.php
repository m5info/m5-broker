<?php

namespace App\Http\Controllers\Contracts;

use App\Http\Controllers\Controller;
use App\Models\Contracts\Contracts;
use App\Models\Settings\ProgramsOnlineList;
use App\Models\Vehicle\VehicleCategoriesOKP;
use App\Models\Vehicle\VehicleModels;
use Illuminate\Http\Request;

class ActionsController extends Controller
{


    public function __construct()
    {

    }


    public function setCheckUser($id, Request $request)
    {
        $contract = Contracts::getContractId($id);
        $contract->check_user_id = $request->user_id;
        $contract->save();

        return response('',200);
    }




    public function get_models(Request $request)
    {

        $result = [];

        $markId = $request->markId;
        $categoryId = $request->categoryId ? $request->categoryId : 2;

        if ((int)$markId>0) {
            $result = VehicleModels::select(\DB::raw("IF(title REGEXP '^[а-яА-Я0-9]', 0, 1) as sort, id, title"))->where('mark_id', $markId)->where('category_id', $categoryId)->orderBy('sort', 'desc')->orderBy('title', 'asc')->get(['id', 'title'])->toArray();
        }

        return response()->json($result);

    }

    public function classification(Request $request)
    {

        $result = [];

        $categoryId = $request->categoryId ? $request->categoryId : 2;

        //СДЕЛАТЬ СПРАВОЧНИК В БД
        if($categoryId == 2){
            $result = [
                ['id'=> '54', 'title' => 'Внедорожник'],
                ['id'=> '24', 'title' => 'Кабриолет'],
                ['id'=> '25', 'title' => 'Купе'],
                ['id'=> '43', 'title' => 'Лифтбек'],
                ['id'=> '27', 'title' => 'Минивэн'],
                ['id'=> '29', 'title' => 'Пикап'],
                ['id'=> '26', 'title' => 'Родстер'],
                ['id'=> '23', 'title' => 'Седан'],
                ['id'=> '22', 'title' => 'Универсал'],
                ['id'=> '44', 'title' => 'Фастбек'],
                ['id'=> '21', 'title' => 'Хэтчбек'],
            ];
        }




        return response()->json($result);

    }




    public function get_contract_info(Request $request)
    {
        $contract = Contracts::getContractsQuery()->where('bso_id', $request->bso_id)->get()->last();
        $payment = $contract->get_payment_first();

        $response = new \stdClass();
        $response->contract_id = $contract->id;
        $response->financial_policy_id = $contract->financial_policy_id;
        $response->payment_total = $payment->payment_total;
        $response->official_discount = $payment->official_discount;
        $response->informal_discount = $payment->informal_discount;
        $response->bank_kv = $payment->bank_kv;
        $response->pay_method_id = $payment->pay_method_id;

        return response()->json($response);
    }


    public function get_category_okp(Request $request)
    {
        $result = VehicleCategoriesOKP::where('category_id', $request->categoryId)->orderBy('title', 'asc')->get(['id', 'title'])->toArray();
        return response()->json($result);
    }


    public function set_programs_list(Request $request){

        $programs_list = ProgramsOnlineList::where('user_id', auth()->id());

        $programs_list->delete();

        if($request->programs){
            foreach($request->programs['sk'] as $sk_id => $programs){
                $sort = 0;
                foreach($programs['program'] as $program_id => $isset){
                    $sort++;
                    ProgramsOnlineList::create([
                        'user_id' => auth()->id(),
                        'sort' => $sort,
                        'program_id' => $program_id,
                    ]);
                }
            }
        }

        return response('200');
    }


}
