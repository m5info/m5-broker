<?php

namespace App\Http\Controllers\Contracts\PrimaryAgent;

use App\Domain\Processes\Scenaries\Contracts\FastAccept\FastAcceptProcessContract;
use App\Domain\Processes\Scenaries\Contracts\Primary\PrimaryAcceptContract;
use App\Domain\Processes\Scenaries\Contracts\Primary\PrimaryProcessContract;
use App\Domain\Processes\Scenaries\Contracts\Primary\PrimaryProcessPayments;
use App\Domain\Processes\Scenaries\Contracts\Primary\PrimaryProcessTypePay;
use App\Http\Controllers\Controller;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\PrimaryAutosaveContracts;
use App\Models\User;
use Illuminate\Http\Request;

class PrimaryAgentController extends Controller
{


    public function __construct()
    {
        $this->middleware('permissions:contracts,primary-agent');
    }


    public function index(Request $request)
    {
        $usersAutosavePrimary = PrimaryAutosaveContracts::query()
            ->where('user_id', '=', auth()->id())
            ->where('type_id', '=', 3)->first();

        return view('contracts.primary_agent.index', [
            'autosave' => $usersAutosavePrimary,
//            'autosave' => null,
        ]);

    }


    public function form_fast_acceptance(Request $request)
    {

        $im = \Auth::user();

        $agents = User::getALLUserWhere()->get();

        /*
        if ($im->role->title == 'Эксперт'){
            $agents = User::query()->select('users.*')
                ->leftJoin('roles', 'users.role_id', 'roles.id')
                ->where('roles.title', "Агент/Менеджер")
                ->get();
        }else{
            if ($im->is_parent){
                $agents = $im->getAllImParentAgents_();
            }else{
                $agents = collect([]);
            }
        }
        */
        return view('contracts.primary_agent.form_fast_acceptance', [
            'agents' => $agents,
            'im' => $im
        ]);

    }


    public function set_fast_accept(Request $request){


        $contracts = $request->get('contract')?:null;


        foreach ($contracts as $data){

            $data = (object) $data;

            //dd($data);

            $contract = Contracts::where('bso_id', $data->bso_id)->get()->first();

            $contract = PrimaryProcessContract::handle($contract, $data);

            $contract = PrimaryAcceptContract::handle($contract);

            $contract = PrimaryProcessPayments::handle($contract, $data);

            $contract = PrimaryProcessTypePay::handle($contract, $data);

        }

        PrimaryAutosaveContracts::unsetUserData(3);

        session()->flash('payment_tab', 0);

        return redirect("/finance/invoice/");
    }

    public function autosave(Request $request)
    {
        parse_str($request->data, $output);

        $usersAutosavePrimary = PrimaryAutosaveContracts::query()
            ->where('user_id', '=', auth()->id())
            ->where('type_id', '=', 3)->first();

        if(!$usersAutosavePrimary){
            $usersAutosavePrimary = PrimaryAutosaveContracts::create([
                'user_id' => auth()->id(),
                'type_id' => 3,
            ]);
        }

        $usersAutosavePrimary->update([
            'count' => $request->count,
            'data' => \GuzzleHttp\json_encode($output),
        ]);

        return response('success', 200);
    }

    public function get_autosave(Request $request)
    {
        $usersAutosavePrimary = PrimaryAutosaveContracts::query()
            ->where('user_id', '=', auth()->id())
            ->where('type_id', '=', 3)->first();

        $im = \Auth::user();

        $agents = User::getALLUserWhere()->get();


        return view('contracts.primary_agent.form_fast_acceptance_with_data', [
            'agents' => $agents,
            'im' => $im,
            'autosaved_data' => \GuzzleHttp\json_decode($usersAutosavePrimary->data)
        ]);

    }

}
