<?php

namespace App\Http\Controllers\Contracts\TempContracts;

use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentExpectedPayments;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Domain\Processes\Operations\Payments\PaymentInvoice;
use App\Domain\Processes\Operations\Payments\PaymentReceipt;
use App\Domain\Processes\Scenaries\Contracts\Salaries\SalariesController;
use App\Domain\Samplers\Contracts\TabsVisibility;
use App\Domain\Entities\Bso\EBsoItem;
use App\Domain\Entities\Payments\EPayment;
use App\Domain\Processes\Operations\Bso\BsoStateOperations;
use App\Domain\Processes\Operations\Contracts\ContractAccept;
use App\Domain\Processes\Operations\Contracts\ContractStatus;
use App\Domain\Processes\Operations\Payments\PaymentAcceptOperations;
use App\Domain\Processes\Operations\Payments\PaymentStatus;
use App\Domain\Processes\Scenaries\Contracts\TempContracts\UpdateTempContract;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Contracts\TempContracts\ContractListQueryHandler;
use App\Models\Actions\PaymentAccept;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\ContractMessage;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ObjectInsurer;
use App\Models\Contracts\ObjectInsurerAuto;
use App\Models\Contracts\Payments;
use App\Models\Contracts\Subjects;
use App\Models\Delivery\Delivery;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\Products;
use App\Models\Directories\TypeBso;
use App\Models\Finance\PayMethod;
use App\Models\User;
use App\Models\Users\Permission;
use Illuminate\Http\Request;

class TempContractsController extends Controller {

    public function __construct() {

        $this->middleware('permissions:contracts,temp_contracts');

        $this->breadcrumbs[] = [
            'label' => 'Временные договоры',
            'url' => 'contracts/temp_contracts'
        ];
    }

    public function index() {

        $count_arr = Contracts::getTempContractsCountArr();
//        $builder = Contracts::getContractsQuery();
        $sks = InsuranceCompanies::query()->where('is_actual', 1)->get();
        $products = Products::query()->where('is_actual', 1)->get();
//        $agents = User::getALLUser();

        return view('contracts.temp_contracts.index', [
            "count_arr" => $count_arr,
            'products' => $products,
//            'agents' => $agents,
            'sks' => $sks,
        ]);
    }


    public function add_electronic(Request $request) {

        $agents = User::getALLUser();
        $bso_suppliers = BsoSuppliers::getBsoSuppliersIsEPolicy()->get();

        return view('contracts.temp_contracts.add.electronic', [
            "agents" => $agents,
            "bso_suppliers" => $bso_suppliers,
        ]);
    }


    public function electronic_for_primary(Request $request) {

        $agents = User::getALLUser();
        $bso_suppliers = BsoSuppliers::getBsoSuppliersIsEPolicy();
        $is_fast_primary = false;

        if (isset($request->from_url)){
            $bso_suppliers->select([\DB::raw('DISTINCT(bso_suppliers.id)'), 'bso_suppliers.title']);
            $bso_suppliers
                ->leftjoin('hold_kv', 'bso_suppliers.id', '=', 'hold_kv.bso_supplier_id')
                ->leftjoin('products', 'hold_kv.product_id', '=', 'products.id');
                /* Если пришло с обычной первички быстрой */
            if (stripos($request->from_url,  'contracts/primary-agent') !== false){
                $bso_suppliers->where('products.available_in_primary_fast', 1);
                $is_fast_primary = true;
                /* Если пришло с обычной первички */
            }elseif (stripos($request->from_url,  'contracts/primary') !== false){
                $bso_suppliers->where('products.available_in_primary_fast', 0);
            }
        }


        return view('contracts.temp_contracts.add.electronic_for_primary', [
            "is_fast_primary" => $is_fast_primary,
            "agents" => $agents,
            "bso_suppliers" => $bso_suppliers->get(),
            "key" => $request->key,
            "from_url" => isset($request->from_url) ? $request->from_url : '',
        ]);
    }

    public function add_paper(Request $request) {

        return view('contracts.temp_contracts.add.contract', [
            "is_payment" => 0,
            "title" => 'Договор',
        ]);
    }

    public function add_payment(Request $request) {


        if(isset($request->payment_id) && $request->payment_id > 0){
            $payment = Payments::getPaymentId($request->payment_id);
        }else{
            $payment = new Payments();
            $payment->payment_data = date("Y-m-d");
        }

        return view('contracts.temp_contracts.add.payment', [
            "is_payment" => 1,
            "title" => 'Второй взнос',
            'payment' => $payment
        ]);
    }


    public function add_receipt(Request $request) {

        $this->validate(request(), [
            'bso_id' => 'integer',
            'key' => 'integer',
        ]);


        if($bso_item = BsoItem::find((int)request('bso_id'))){

            $bso_series = BsoSerie::query()
                ->where('insurance_companies_id', $bso_item->supplier->insurance_companies_id)
                ->where('bso_class_id', 100)
                ->get();


            return view('contracts.temp_contracts.add.add_receipt', [
                'bso_series' => $bso_series,
                'bso_item' => $bso_item,
                'key' => request('key')
            ]);

        }

        return frameError('Договор не найден');
    }


    public function create_receipt(){


        $this->validate(request(), [
            'receipt_number' => 'string',
            'series_id' => 'integer',
            'bso_id' => 'integer',
        ]);

        $bso_item = BsoItem::query()->findOrFail((int)request('bso_id'));
        $bso_serie = BsoSerie::query()->findOrFail((int)request('series_id'));
        $bso_supplier = $bso_item->supplier;

        $receipt_number = request('receipt_number');

        $bso_title = $bso_serie->bso_serie . ' ' . $receipt_number;

        if(empty($receipt_number) || mb_strlen($bso_title, 'utf-8') < 2){
            return back()->with('error', 'Номер квитанции  должен состоять минимум из двух цифр');
        }

        if(BsoItem::query()->where('bso_title', $bso_title)->first()){
            return back()->with('error', 'БСО с таким номером уже существует в этой серии');
        }

        $receipt = BsoItem::create([
            'location_id' => 1,
            'state_id' => 0,
            'bso_supplier_id' => $bso_supplier->id,
            'org_id' => $bso_item->org_id,
            'insurance_companies_id' => $bso_item->insurance_companies_id,
            'bso_class_id' => $bso_serie->bso_class_id,
            'type_bso_id' => $bso_item->type_bso_id,
            'product_id' => $bso_serie->product_id,
            'bso_serie_id' => $bso_serie->id,
            'bso_number' => request('receipt_number'),
            'bso_title' => $bso_title,
            'agent_id' => $bso_item->agent_id,
            'user_id' => $bso_item->user_id,
            'user_org_id' => $bso_item->user_org_id,
            'time_create' => date('Y-m-d H:i:s'),
            'time_target' => date('Y-m-d H:i:s'),
            'last_operation_time' => date('Y-m-d H:i:s'),
            'transfer_to_agent_time' => date('Y-m-d H:i:s'),
            'bso_manager_id' => $bso_item->bso_manager_id,
            'point_sale_id' => $bso_item->point_sale_id,
        ]);

        $receipt->setBsoLog(1, 0, 0);

        if($receipt){

            $array = [
                'add_params' => 1,
                'bso_receipt' => $receipt->bso_title,
                'bso_receipt_id' => $receipt->id,
                'bso_id' => \request('bso_id'),
                'bso_supplier_id' => \request('bso_supplier_id'),
                'insurance_companies_id' => \request('insurance_companies_id'),
                'agent_id' => \request('agent_id'),
                'contract_id' => \request('contract_id'),
                'bso_title' => \request('bso_title'),
                'payment_data' => \request('payment_data'),
                'payment_total' => \request('payment_total'),
                'financial_policy_id' => \request('financial_policy_id'),
                'official_discount' => \request('official_discount'),
                'informal_discount' => \request('informal_discount'),
                'bank_kv' => \request('bank_kv'),
            ];

            $query = http_build_query($array);

            echo "<script>
                window.parent.flashHeaderMessage('Квитанция успешно создана', 'success');
                window.parent.jQuery.fancybox.close();
                window.parent.openFancyBoxFrame('http://127.0.0.1:8000/contracts/temp_contracts/add/payment?{$query}');
            </script>";
        }else{
            echo "<script>
                window.parent.flashHeaderMessage('Произошла ошибка. Квитанция не была создана', 'danger');
                window.parent.jQuery.fancybox.close();
            </script>";
        }

        die();

    }

    public function create_bso_contract_for_primary(Request $request) {

        $agent = User::findOrFail((int) $request->agent_id);
        $bso_supplier = BsoSuppliers::findOrFail((int) $request->bso_supplier_id);
        $bso_type = TypeBso::findOrFail($request->bso_type_id);
        $bso_serie = BsoSerie::findOrFail($request->bso_serie_id);

        $bso = BsoItem::getElectronicBso($agent, $bso_supplier, $bso_type, $bso_serie, $request->bso_title);

        if (!$bso) {
            return redirect()->back()->withErrors([0 => "Ошибка БСО!",]);
        }

        $data = new \stdClass();

        $data->bso_id = $bso->id;
        $data->bso_title = $bso->bso_title;
        $data->insurance_companies_id = $bso->insurance_companies_id;
        $data->bso_supplier_id = $bso->bso_supplier_id;
        $data->product_id = $bso->product_id;
        $data->agent_id = $bso->agent_id;


        $contract = Contracts::createContracts($data);

        /* Если стоит галочка в продукте "Доступна в первичке - быстрой", то фин. политика задается */
        if ($bso->product->available_in_primary_fast){
            if($bso_supplier = $contract->bso_supplier){
                if($financial_policies = $bso_supplier->financial_policy){
                    if($product_fp = $financial_policies->where('product_id', '=', $data->product_id)->first()){
                        $contract->update(['financial_policy_id' => $product_fp->id]);
                    }
                }
            }
        }

        $object_insurer = new ObjectInsurer();
        $object_insurer->type = 1;
        $object_insurer->save();
        $auto = new ObjectInsurerAuto();
        $auto->object_insurer_id = $object_insurer->id;
        $auto->save();

        $insurer = new Subjects();
        $insurer->save();

        $contract->update([
            'insurer_id' => $insurer->id,
            'object_insurer_id' => $object_insurer->id,
        ]);

        return ['status' => 1, 'contract' => $contract, 'key' => $request->key];
    }

    public function create_bso_contract(Request $request) {

        $agent = User::findOrFail((int) $request->agent_id);
        $bso_supplier = BsoSuppliers::findOrFail((int) $request->bso_supplier_id);
        $bso_type = TypeBso::findOrFail($request->bso_type_id);
        $bso_serie = BsoSerie::findOrFail($request->bso_serie_id);

        $bso = BsoItem::getElectronicBso($agent, $bso_supplier, $bso_type, $bso_serie, $request->bso_title);


        if (!$bso) {
            return redirect()->back()->withErrors([0 => "Ошибка БСО!",]);
        }

        $data = new \stdClass();

        $data->bso_id = $bso->id;
        $data->bso_title = $bso->bso_title;
        $data->insurance_companies_id = $bso->insurance_companies_id;
        $data->bso_supplier_id = $bso->bso_supplier_id;
        $data->product_id = $bso->product_id;
        $data->agent_id = $bso->agent_id;


        $contract = Contracts::createContracts($data);

        $object_insurer = new ObjectInsurer();
        $object_insurer->type = 1;
        $object_insurer->save();
        $auto = new ObjectInsurerAuto();
        $auto->object_insurer_id = $object_insurer->id;
        $auto->save();

        $insurer = new Subjects();
        $insurer->save();

        $contract->update([
            'insurer_id' => $insurer->id,
            'object_insurer_id' => $object_insurer->id,
        ]);

        return parentRedirect(url("/contracts/temp_contracts/contract/{$contract->id}/edit"));
    }

    public function create_contract(Request $request) {
        $contract = Contracts::createContracts($request);

        $object_insurer = new ObjectInsurer();
        $object_insurer->type = 1;
        $object_insurer->save();
        $auto = new ObjectInsurerAuto();
        $auto->object_insurer_id = $object_insurer->id;
        $auto->save();

        $insurer = new Subjects();
        $insurer->save();

        $contract->update([
            'insurer_id' => $insurer->id,
            'object_insurer_id' => $object_insurer->id,
        ]);

        return parentRedirect(url("/contracts/temp_contracts/contract/{$contract->id}/edit"));
    }

    public function temp_contracts_edit($contract_id){

        $contract = Contracts::getContractId($contract_id);

        if (!$contract) {
            return view('errors.403', ['exception' => 1]);
        }

        $agents = User::getALLUser(24)->pluck('name', 'id');

        $user = \Auth::user();
        $permission = $user->hasPermission('role_owns', 'temp_contract_visibility_not_full');

        $object_insurer = $contract->object_insurer;
        if (!$object_insurer) {
            $object_insurer = new ObjectInsurer();
            if ($contract->bso && $contract->bso->product->category->template == 'auto') {
                $object_insurer->type = 1;
            }
        }

        $view_type = 'view';
        $tabs = TabsVisibility::get_temp_contracts_tab_visibility();

        if(isset($tabs[$contract->statys_id]) && $tabs[$contract->statys_id]['edit'] == 1){

            if(!$contract->is_online) {
                $view_type = 'edit';
            }

        }

        if($contract['statys_id'] == 4 || request('no_permission', 0)){
            $view_type = 'view';
        }

        $this->breadcrumbs[] = ['label' => 'Редактирование',];

        $template = $contract->is_online ?
            'contracts.contract_templates.online_contracts.main' :
            'contracts.contract_templates.temp_contracts.main';

        $referencers = User::getUsersHavePermissionLike('is_referencer')->get();

        return view($template, [
            'permission' => $permission,
            'object_insurer' => $object_insurer,
            "contract" => $contract,
            "product" => $contract->product,
            'agents' => $agents,
            'referencers' => $referencers,
            'view_type' => $view_type
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function temp_contracts_save($contract_id) {

        $result = new \stdClass();
        $result->sate = 1;
        $result->msg = 'Ошибка сохранения договора!';

        if(UpdateTempContract::handle($contract_id, request('contract.0', []))){
            $result->sate = 0;
            $result->msg = '';
        }

        return response()->json($result);
    }

    public function add_pay($contract_id, Request $request) {


        if($contract = Contracts::getContractId($contract_id)){
            EPayment::create_temp($contract);
        }

        return response(200);
    }

    public function del_pay($contract_id, Request $request) {

        $contract = Contracts::getContractId($contract_id);

        Payments::where('contract_id', $contract_id)
            ->where('id', $request->pay)
            ->delete();

        $payments = $contract->temp_payments;
        foreach ($payments as $i => $payment){
            $payment->payment_number = ($i+1);
            $payment->save();
        }

        return response(200);
    }

    private function validatePayment($payment, $i) {
        $errors = [];
        $msg = [];

        if (!isset($payment['type_id'])) {
            $msg[] = "Нужно заполнить тип платежа \n";
            $errors[] = '#type_id_' . $i;
        }

        if (!$payment['payment_number']) {
            $msg[] = "Нужно заполнить номер взноса \n";
            $errors[] = '#payment_number_' . $i;
        }

        if ($payment['payment_total'] <= 0) {
            $msg[] = "Не указана сумма платежа для платежа " . ($i + 1) . " \n";
            $errors[] = '#payment_total_' . $i;
        }

        if (!$payment['payment_data']) {
            $msg[] = "Не указана Дата оплаты для платежа " . ($i + 1) . " \n";
            $errors[] = '#payment_data_' . $i;
        }

        if (!isset($payment['bso_not_receipt']) && !$payment['bso_receipt']) {
            $msg[] = "Не указана Квитанция для платежа " . ($i + 1) . " \n";
            $errors[] = '#bso_receipt_' . $i;
        }

        return ['msg' => $msg, 'errors' => $errors];
    }

    private static function requiredField($fieldName) {
        return "Заполните поле: " . $fieldName . " \n";
    }

    private function validateInsurer($insurer) {
        $errors = [];
        $msg = [];

        if (!$insurer['title']) {
            /*$msg[] = "Не указаны данные страхователя \n";
            $errors[] = '#insurer_fio_0';
            $errors[] = '#insurer_title_0';*/
        }

        return ['msg' => $msg, 'errors' => $errors];
    }

    private function validateContract($contract) {
        $errors = [];
        $msg = [];

        if (!$contract['sign_date']) {
            $msg[] = self::requiredField("Дата заключения");
            $errors[] = '#sign_date_0';
        }
        if (!$contract['begin_date']) {
            $msg[] = self::requiredField("Период действия - дата начала");
            $errors[] = '#begin_date_0';
        }

        if (!$contract['end_date']) {
            $msg[] = self::requiredField("Период действия - дата окончания");
            $errors[] = '#end_date_0';
        }

        if ($contract['payment_total'] <= 0) {
            $msg[] = self::requiredField("Премия по договору");
            $errors[] = '#payment_total_0';
        }

        if (!$contract['financial_policy_id'] && (!isset($contract['financial_policy_manually_set']) || !$contract['financial_policy_manually_set'])) {
            $msg[] = "Не выбрана Финансовая политика \n";
            $errors[] = '#financial_policy_id_0';
        }

        if (isset($contract['financial_policy_manually_set']) && $contract['financial_policy_manually_set']) {

            if (!is_numeric($contract['financial_policy_kv_bordereau'])) {
                $msg[] = "Не указано бордеро \n";
                $errors[] = '#financial_policy_kv_bordereau_0';
            }

            if (!is_numeric($contract['financial_policy_kv_dvoy'])) {

                $msg[] = "Не указано ДВОУ \n";
                $errors[] = '#financial_policy_kv_dvoy_0';
            }

            if (!is_numeric($contract['financial_policy_kv_agent'])) {
                $msg[] = "Не указано % Агента\n";
                $errors[] = '#financial_policy_kv_agent_0';
            }

            if (!is_numeric($contract['financial_policy_kv_parent'])) {
                $msg[] = "Не указано % Руководителя \n";
                $errors[] = '#financial_policy_kv_parent_0';
            }
        }


        return ['msg' => $msg, 'errors' => $errors];
    }

    public function set_comment($contract_id){

        $this->validate(request(), [
            'message' => 'required|string|min:1'
        ]);


        if($contract = Contracts::getContractId($contract_id)){
            ContractMessage::create([
                'message' => request('message'),
                'type_id' => 0,
                'user_id' => auth()->id(),
                'contract_id' => $contract_id,
            ]);
        }


        return 'ok';
    }

    public function send_check($contract_id) {

        $contract = Contracts::getContractId($contract_id);
        $res = new \stdClass();
        $res->msg = [];
        $res->errors = [];
        $res->state = true;

        if ($contract->statys_id == 0) {
            $res->state = true;
            $statys_id = 1;
            $hold_kv_product = $contract->bso->supplier->hold_kv_product($contract->product_id);

            if (isset($hold_kv_product) && $hold_kv_product->is_check_policy == 0) {
                $statys_id = 3;
            }

            ContractStatus::set($contract, $statys_id);
        }

        return response()->json($res);
    }


    public function send_to_fixed($contract_id) {

        if($contract = Contracts::getContractId($contract_id)){

            if ($contract->statys_id == 2) {
                ContractStatus::set($contract, 7);
            }

            ContractMessage::create([
                'message' => request('message', 'Исправлено'),
                'type_id' => 2,
                'user_id' => auth()->id(),
                'contract_id' => $contract_id,
            ]);
        }


        return response()->json(['state' => true]);
    }



    public function error_contract($contract_id) {

        $contract = Contracts::getContractId($contract_id);

        $contract = ContractAccept::set_error($contract, request('message', 'Ошибка'));

        return response(200);
    }


    public function saveOnlyMessage($contract_id, Request $request)
    {
        ContractMessage::create([
            'message' => $request->message,
            'type_id' => 0,
            'user_id' => auth()->id(),
            'contract_id' => $contract_id,
        ]);
    }

    public function accept_contract($contract_id){

        if(!auth()->user()->hasPermission('contracts', 'verification')){
            return view('errors.403', ['exception' => 1]); 
        }

        $res = new \stdClass();
        $res->state = true;
        $res->msg = [];
        $res->errors = [];
        $contract = Contracts::getContractId($contract_id);

        //акцепт либо который выбрали либо "проверка"
        $kind_acceptance = request('kind_acceptance', 3);

        //акцептуем
        $contract = ContractAccept::set($contract, $kind_acceptance);

        //если акцепт не условный, то выпускаем договор
        if($kind_acceptance != 0){
            $contract = ContractStatus::set($contract, 4);
        }

        //продаём бсо
        $bso = BsoStateOperations::set($contract->bso, 2);


        //если есть платежи
        if(sizeof($contract->all_payments)){
            foreach ($contract->all_payments as $payment){

                //обновляем
                $payment = PaymentAcceptOperations::update_or_create($payment);

                //если платёж временный, делаем его нормальным, неоплаченым
                if($payment->statys_id == -1){

                    $parent_agent_id = 0;

                    if($contract->sales_condition != 0){
                        if($contract->manager){
                            $parent_agent_id = $contract->manager->parent_id;
                            $payment->manager_id = $contract->manager_id;
                        }
                        $payment->agent_id = $contract->agent_id;

                    }else{
                        if($contract->agent){
                            $payment->agent_id = $contract->agent_id;
                            $parent_agent_id = $contract->agent->parent_id;
                        }
                    }



                    $payment->parent_agent_id = $parent_agent_id;
                    $payment->point_sale_id = $contract->bso->point_sale_id;
                    $payment->save();


                    //Квитанция
                    if($payment->bso_receipt_id > 0){

                        PaymentReceipt::attach($payment, $payment);

                    }

                    $payment = PaymentStatus::set($payment, 0);

                }

                if($payment = PaymentFinancialPolicy::actualize($payment)){
                    $payment = PaymentDiscounts::recount($payment);
                }


                if($kind_acceptance == 1)
                {
                    $payment = PaymentExpectedPayments::expected($payment);

                    $hold_kv_product = $contract->bso->supplier->hold_kv_product($contract->product_id);
                    if($hold_kv_product->is_auto_bso == 1) {
                        PaymentInvoice::create_to_payment($payment);
                    }
                }
            }
        }

        return response()->json($res);
    }

    public function get_tab() {

        $builder = Contracts::getContractsQuery();
        $builder->where('kind_acceptance', '!=', 1);

        $sks = InsuranceCompanies::query()->whereIn('id', $builder->get()->pluck('insurance_companies_id'))->get();
        $insurers = Subjects::query()->whereIn('id', $builder->get()->pluck('insurer_id'))->get();
        $products = Products::query()->whereIn('id', $builder->get()->pluck('product_id'))->get();
        $agents = User::query()->whereIn('id', $builder->get()->pluck('agent_id'))->get();


        return view('contracts.partials.info_tab', [
            'insurers' => $insurers,
            'products' => $products,
            'agents' => $agents,
            'sks' => $sks,
        ]);
    }


    public function get_table(){

        $this->validate(request(), [
            'sk.*' => 'integer',
            'statys' => 'integer',
            'product' => 'integer',
            'insurer' => 'string',
            'agent' => 'integer',
            'bso_title' => 'string',
            'conclusion_date_from' => 'date',
            'conclusion_date_to' => 'date',
            'contract_type' => 'integer',
        ]);


        $builder = Contracts::getContractsQuery();
        $builder->where('kind_acceptance', '!=', 1)
            ->with('bso','insurer','manager','agent','insurance_companies','errors','logs','product');
        $builder->orderBy('updated_at', 'desc');
        $contracts = (new ContractListQueryHandler($builder))->allowEmpty()->apply();

        return view('contracts.partials.contracts_table', [
            "contracts" => $contracts->get(),
        ]);

    }

    public function create_receipt_frame(){


        return view('contracts.temp_contracts.frame.add_receipt');

    }

    public function create_to_payment(Request $request)
    {

        if((int)$request->payment_id > 0){
            $payment = Payments::getPaymentId($request->payment_id);
            $contract = Contracts::find($payment->contract_id);

        }else{
            $contract = Contracts::find($request->contract_id);

            $payment = $contract->payments()->where('type_id', 0)->get()->last()->replicate();
            $payment->payment_number = $payment->payment_number+1;
            $payment->invoice_id = null;
            $payment->invoice_payment_total = null;
            $payment->invoice_payment_date = null;
            $payment->reports_order_id = null;
            $payment->reports_dvou_id = null;
            $payment->marker_color = null;
            $payment->marker_text = null;
            $payment->acts_sk_id = 0;
            $payment->atol_check_id = 0;
            $payment->accept_date = '';
            $payment->accept_user_id = 0;
            $payment->realized_act_id = 0;
            $payment->statys_id = -1;

            $payment->bso_receipt = null;
            $payment->bso_receipt_id = null;


        }


        $payment->financial_policy_id = $request->financial_policy_id;
        $payment->payment_data = getDateFormatEn($request->payment_data);
        $payment->payment_total = getFloatFormat($request->payment_total);
        $payment->official_discount = getFloatFormat($request->official_discount);
        $payment->informal_discount = getFloatFormat($request->informal_discount);
        $payment->bank_kv = getFloatFormat($request->bank_kv);

        $method = PayMethod::find($request->pay_method_id);
        $payment->payment_type = $method->payment_type;
        $payment->payment_flow = $method->payment_flow;
        $payment->pay_method_id = $request->pay_method_id;
        $payment->save();


        if($method->key_type == 0){
            //Квитанция
            if((int)$request->bso_receipt_id > 0){
                $payment->bso_receipt_id = (int)$request->bso_receipt_id;
                $payment->bso_receipt = $request->bso_receipt;
            }
        }else{
            $payment->bso_not_receipt = 1;
        }


        $payment->save();


        $payment = PaymentFinancialPolicy::actualize($payment);


        //обновляем
        $payment = PaymentAcceptOperations::update_or_create($payment);

        //если платёж временный, делаем его нормальным, неоплаченым
        if($payment->statys_id == -1){

            $parent_agent_id = 0;

            if($contract->sales_condition != 0){
                if($contract->manager){
                    $parent_agent_id = $contract->manager->parent_id;
                    $payment->manager_id = $contract->manager_id;
                }
            }else{
                if($contract->agent){
                    $parent_agent_id = $contract->agent->parent_id;
                }
            }

            $payment->parent_agent_id = $parent_agent_id;
            $payment->save();


            //Квитанция
            if($payment->bso_receipt_id > 0){

                PaymentReceipt::attach($payment, $payment);

            }

        }

        $payment = PaymentDiscounts::recount($payment);
        $payment = PaymentStatus::set($payment, 0);


        return parentRedirect("/contracts/temp_contracts/contract/{$contract->id}/edit?no_permission=1");

    }

    public function delete_contract($contract_id)
    {
        $contract = Contracts::getContractId($contract_id);

        if($contract && $contract->statys_id <= 2){
            if($contract->all_payments){
                $contract->all_payments()->delete();
            }

            $contract->delete();
        }
        return response(200);
    }


}
