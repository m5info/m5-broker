<?php

namespace App\Http\Controllers\Contracts\FastAccept;

use App\Domain\Processes\Operations\Contracts\ContractStatus;
use App\Domain\Processes\Operations\Payments\ExpectedPayment;
use App\Domain\Processes\Operations\Payments\PaymentAcceptOperations;
use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentExpectedPayments;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Domain\Processes\Operations\Payments\PaymentReceipt;
use App\Domain\Processes\Operations\Payments\PaymentReceiptStatus;
use App\Domain\Processes\Operations\Payments\PaymentStatus;
use App\Domain\Processes\Scenaries\Contracts\FastAccept\FastAcceptContract;
use App\Domain\Processes\Scenaries\Contracts\FastAccept\FastAcceptProcessContract;
use App\Domain\Processes\Scenaries\Contracts\FastAccept\FastAcceptProcessPayments;
use App\Http\Controllers\Controller;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\ContractMessage;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\PrimaryAutosaveContracts;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\User;
use Illuminate\Http\Request;

class FastAcceptController extends Controller
{

    public function index(Request $request)
    {
        $this->middleware('permissions:contracts,fast_accept');

        $usersAutosaveFastAccept = PrimaryAutosaveContracts::query()
            ->where('user_id', '=', auth()->id())
            ->where('type_id', '=', 2)->first();

        return view('contracts.fast_accept.index', [
            'autosave' => $usersAutosaveFastAccept
        ]);

    }


    public function form_fast_acceptance(Request $request)
    {
        $this->middleware('permissions:contracts,fast_accept');

        $agents = User::getALLUser(24)->pluck('name', 'id');

        return view('contracts.fast_accept.form_fast_acceptance', [
            'agents' => $agents,
        ]);

    }


    public function set_fast_accept(Request $request)
    {
        $this->middleware('permissions:contracts,fast_accept');

        $contracts = $request->get('contract')?:null;

        $res = [];

        $contracts_count = count($contracts);

        foreach ($contracts as $data){

            $data = (object) $data;

            if(isset($data->bso_id)){

                $bso = BsoItem::query()->where('id', $data->bso_id)->first();

                if($bso){

                    $contract = Contracts::where('bso_id', $data->bso_id)->get()->first();

                    $data->bso_title = $bso->bso_title;

                    $contract = FastAcceptProcessContract::handle($contract, $data);

                    $contract = FastAcceptContract::handle($contract, $data);

                    $contract = ExpectedPayment::update_or_create($contract, $data);

                    $contract = FastAcceptProcessPayments::handle($contract, $data);

                    if($data->kind_acceptance != 0){

                        $contract = ContractStatus::set($contract, 4);

                    }else{
                        $contract = ContractStatus::set($contract, 2);
                    }

                    $contract = PaymentReceiptStatus::refresh($contract);


                    //если есть платежи
                    if(sizeof($contract->all_payments)){
                        foreach ($contract->all_payments as $payment){

                            //обновляем
                            $payment = PaymentAcceptOperations::update_or_create($payment);

                            if($data->kind_acceptance == 1){
                                $payment = PaymentExpectedPayments::expected($payment);
                            }

                            //если платёж временный, делаем его нормальным, неоплаченым
                            if($payment->statys_id == -1){

                                $parent_agent_id = 0;

                                if($contract->sales_condition != 0){
                                    if($contract->manager){
                                        $parent_agent_id = $contract->manager->parent_id;
                                        $payment->manager_id = $contract->manager_id;
                                    }
                                }else{
                                    if($contract->agent){
                                        $parent_agent_id = $contract->agent->parent_id;
                                    }
                                }

                                $payment->parent_agent_id = $parent_agent_id;

                                $payment->point_sale_id = $contract->bso->point_sale_id;

                                $payment->save();

                                //Квитанция
                                if($payment->bso_receipt_id > 0){

                                    PaymentReceipt::attach($payment, $payment);

                                }


                            }

                            $payment = PaymentFinancialPolicy::actualize($payment);
                            $payment = PaymentDiscounts::recount($payment);
                            $payment = PaymentStatus::set($payment, 0);

                            if(!(int)$payment->check_user_id){
                                $payment->update([
                                    'check_user_id' => auth()->check() ? auth()->id() : 0,
                                    'check_date' => date('Y-m-d')
                                ]);
                            }

                        }
                    }


                    $res[] = (["bso_title"=>$contract->bso_title, "bso_id"=>$contract->bso_id]);
                }
            }

        }

        PrimaryAutosaveContracts::unsetUserData(2);

        return view('contracts.fast_accept.result', [
            'res' => $res,
            'contracts_count' => $contracts_count,
        ]);

    }


    public function create_receipt_frame(Request $request){

        $this->validate(request(), [
            'bso_id' => 'integer',
            'key' => 'integer',
        ]);


        if($bso_item = BsoItem::find((int)request('bso_id'))){

            $bso_series = BsoSerie::query()
                ->where('insurance_companies_id', $bso_item->supplier->insurance_companies_id)
                ->where('bso_class_id', 100)
                ->get();


            return view('contracts.fast_accept.frame.add_receipt', [
                'bso_series' => $bso_series,
                'bso_item' => $bso_item,
                'key' => request('key')
            ]);

        }

        return frameError('Договор не найден');
    }



    public function create_receipt(){


        $this->validate(request(), [
            'receipt_number' => 'string',
            'series_id' => 'integer',
            'bso_id' => 'integer',
            'key' => 'integer'
        ]);

        $bso_item = BsoItem::query()->findOrFail((int)request('bso_id'));
        $bso_serie = BsoSerie::query()->findOrFail((int)request('series_id'));
        $bso_supplier = $bso_item->supplier;

        $key = (int)request('key');
        $receipt_number = request('receipt_number');

        $bso_title = $bso_serie->bso_serie . ' ' . $receipt_number;

        if(empty($receipt_number) || mb_strlen($bso_title, 'utf-8') < 2){
            return back()->with('error', 'Номер квитанции  должен состоять минимум из двух цифр');
        }

        if(BsoItem::query()->where('bso_title', $bso_title)->first()){
            return back()->with('error', 'БСО с таким номером уже существует в этой серии');
        }

        $receipt = BsoItem::create([
            'location_id' => 1,
            'state_id' => 0,
            'bso_supplier_id' => $bso_supplier->id,
            'org_id' => $bso_item->org_id,
            'insurance_companies_id' => $bso_item->insurance_companies_id,
            'bso_class_id' => $bso_serie->bso_class_id,
            'type_bso_id' => $bso_serie->type_bso_id,
            'product_id' => $bso_serie->product_id,
            'bso_serie_id' => $bso_serie->id,
            'bso_number' => request('receipt_number'),
            'bso_title' => $bso_title,
            'agent_id' => $bso_item->agent_id,
            'user_id' => $bso_item->user_id,
            'user_org_id' => $bso_item->user_org_id,
            'time_create' => date('Y-m-d H:i:s'),
            'time_target' => date('Y-m-d H:i:s'),
            'last_operation_time' => date('Y-m-d H:i:s'),
            'transfer_to_agent_time' => date('Y-m-d H:i:s'),
            'bso_manager_id' => $bso_item->bso_manager_id,
            'point_sale_id' => $bso_item->point_sale_id,
        ]);

        $receipt->setBsoLog(1, 0, 0);

        if($receipt){
            echo "<script>
                window.parent.flashHeaderMessage('Квитанция успешно создана', 'success');
                window.parent.jQuery('#bso_receipt_{$key}').val('{$receipt->bso_title}');
                window.parent.jQuery('#bso_receipt_id_{$key}').val('{$receipt->id}');
                window.parent.jQuery('#bso_not_receipt_{$key}').prop('checked', false).change();
                window.parent.jQuery.fancybox.close();
            </script>";
        }else{
            echo "<script>
                window.parent.flashHeaderMessage('Произошла ошибка. Квитанция не была создана', 'danger');
                window.parent.jQuery.fancybox.close();
            </script>";
        }

        die();

    }

    public function check_correct_bso(Request $request){

        if((int)$request->bso_id){
            $bso = BsoItem::where('bso_title', '=', $request->bso)->where('id', '=', $request->bso_id)->first();
        }else{
            $bso = BsoItem::where('bso_title', '=', $request->bso)->first();
        }

        if(!$bso){
            return 0;
        }

        return 1;
    }

    public function get_autosave(Request $request)
    {
        $usersAutosavePrimary = PrimaryAutosaveContracts::query()
            ->where('user_id', '=', auth()->id())
            ->where('type_id', '=', 2)->first();

        $im = \Auth::user();

        $agents = User::getALLUserWhere()->get();


        return view('contracts.fast_accept.form_fast_acceptance_with_data', [
            'agents' => $agents,
            'im' => $im,
            'autosaved_data' => \GuzzleHttp\json_decode($usersAutosavePrimary->data)
        ]);

    }

    public function autosave(Request $request)
    {
        parse_str($request->data, $output);

        $usersAutosavePrimary = PrimaryAutosaveContracts::query()
            ->where('user_id', '=', auth()->id())
            ->where('type_id', '=', 2)->first();

        if(!$usersAutosavePrimary){
            $usersAutosavePrimary = PrimaryAutosaveContracts::create([
                'user_id' => auth()->id(),
                'type_id' => 2
            ]);
        }

        $usersAutosavePrimary->update([
            'count' => $request->count,
            'data' => \GuzzleHttp\json_encode($output),
        ]);

        return response('success', 200);
    }
}
