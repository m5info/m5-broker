<?php

namespace App\Http\Controllers\Contracts\Partners;

use App\Helpers\PaginationHelper;
use App\Models\Partners\Leads;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeadController extends Controller
{
    public function __construct()
    {
        $this->breadcrumbs[] = [
            'label' => 'Лиды по онлайн-договорам',
            'url' => 'contracts/partner-leads/'
        ];
    }

    public function index()
    {
        return view('contracts.partner_leads.index');
    }

    public function leads_list()
    {

        $lead_items = Leads::where('id', '>', 0);
        $page = request('PAGE') > 0 ? (int)request('PAGE') : 1;
        $page_count = request('page_count') > 0 ? (int)request('page_count') : 10;
        $page_count = request('page_count') == -1 ? 9999 : $page_count;

        $result = PaginationHelper::paginate($lead_items, $page, $page_count);

        $lead_items = $result['builder']->orderBy('id', 'desc')->get();

        return [
            'html' => view("contracts.partner_leads.result", [
                'lead_items' => $lead_items
            ])->render(),
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];
    }

    public function view($id)
    {
        $lead = Leads::find($id);
        return view('contracts.partner_leads.edit', ['lead' => $lead])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function edit($id)
    {
        $lead = Leads::find($id);
        $lead->statys_id = request()->statys_id;
        $lead->save();
    }
}
