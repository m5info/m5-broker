<?php

namespace App\Http\Controllers\Contracts\Primary;

use App\Domain\Processes\Operations\Payments\PaymentReceiptStatus;
use App\Domain\Processes\Scenaries\Contracts\FastAccept\FastAcceptProcessContract;
use App\Domain\Processes\Scenaries\Contracts\Primary\PrimaryAcceptContract;
use App\Domain\Processes\Scenaries\Contracts\Primary\PrimaryProcessContract;
use App\Domain\Processes\Scenaries\Contracts\Primary\PrimaryProcessPayments;
use App\Domain\Processes\Scenaries\Contracts\Primary\PrimaryProcessTypePay;
use App\Http\Controllers\Controller;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\PrimaryAutosaveContracts;
use App\Models\User;
use App\Models\Users\Permission;
use App\Models\Users\Role;
use Illuminate\Http\Request;

class PrimaryController extends Controller
{


    public function __construct()
    {
        $this->middleware('permissions:contracts,primary');
    }


    public function index(Request $request)
    {
        $usersAutosavePrimary = PrimaryAutosaveContracts::query()
            ->where('user_id', '=', auth()->id())
            ->where('type_id', '=', 1)->first();

        return view('contracts.primary.index', [
            'autosave' => $usersAutosavePrimary
//            'autosave' => null
        ]);

    }


    public function form_fast_acceptance(Request $request)
    {

        $im = \Auth::user();

        $agents = User::getALLUserWhere()->get();

        $roles_ids = array_column(Permission::where('title', '=', 'is_referencer')->first()->roles->toArray(),'id');

        $referencers = User::query()->whereIn('role_id',$roles_ids)->get();
        /*
        if ($im->role->title == 'Эксперт'){
            $agents = User::query()->select('users.*')
                ->leftJoin('roles', 'users.role_id', 'roles.id')
                ->where('roles.title', "Агент/Менеджер")
                ->get();
        }else{
            if ($im->is_parent){
                $agents = $im->getAllImParentAgents_();
            }else{
                $agents = collect([]);
            }
        }
        */

        return view('contracts.primary.form_fast_acceptance', [
            'agents' => $agents,
            'referencers' => $referencers,
            'im' => $im
        ]);

    }

    public function autosave(Request $request)
    {
        parse_str($request->data, $output);

        $usersAutosavePrimary = PrimaryAutosaveContracts::query()
            ->where('user_id', '=', auth()->id())
            ->where('type_id', '=', 1)->first();

        if(!$usersAutosavePrimary){
            $usersAutosavePrimary = PrimaryAutosaveContracts::create([
                'user_id' => auth()->id(),
                'type_id' => 1,
            ]);
        }

        $usersAutosavePrimary->update([
            'count' => $request->count,
            'data' => \GuzzleHttp\json_encode($output),
        ]);

        return response('success', 200);
    }

    public function get_autosave(Request $request)
    {
        $usersAutosavePrimary = PrimaryAutosaveContracts::query()
            ->where('user_id', '=', auth()->id())
            ->where('type_id', '=', 1)->first();

        $im = \Auth::user();

        $agents = User::getALLUserWhere()->get();

        $roles_ids = array_column(Permission::where('title', '=', 'is_referencer')->first()->roles->toArray(),'id');

        $referencers = User::query()->whereIn('role_id',$roles_ids)->get();

        return view('contracts.primary.form_fast_acceptance_with_data', [
            'agents' => $agents,
            'referencers' => $referencers,
            'im' => $im,
            'autosaved_data' => \GuzzleHttp\json_decode($usersAutosavePrimary->data)
        ]);

    }

    public function set_fast_accept(Request $request){


        $contracts = $request->get('contract')?:null;

        foreach ($contracts as $data){

            $data = (object) $data;
            $contract = null;

            if ($contract = Contracts::where('bso_id', $data->bso_id)->get()->first()){
                if(isset($data->second_payment) && (int)$data->second_payment == 1){

                }else{
                    if ($contract->statys_id < 1){
                        $contract->statys_id = 0;
                    }
                }
            }

            $contract = PrimaryProcessContract::handle($contract, $data);

            $contract = PrimaryAcceptContract::handle($contract);

            $contract = PrimaryProcessPayments::handle($contract, $data);

            $contract = PrimaryProcessTypePay::handle($contract, $data);

            $contract = PaymentReceiptStatus::refresh($contract);

        }

        PrimaryAutosaveContracts::unsetUserData(1);

        session()->flash('payment_tab', 0);
        return redirect("/finance/invoice/");

    }





}
