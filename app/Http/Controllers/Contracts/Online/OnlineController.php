<?php

namespace App\Http\Controllers\Contracts\Online;

use App\Domain\Processes\Operations\Contracts\Online\ContractCalculationOperations;
use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Domain\Processes\Scenaries\Contracts\Online\OnlineContractSave;
use App\Domain\Samplers\Segments\SegmentsOnRequest;
use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Mail\PaymentLink;
use App\Models\Api\SK\ApiSetting;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Driver;
use App\Models\Contracts\DriverPreviousLicense;
use App\Models\Contracts\KbmCalculation;
use App\Models\Contracts\ObjectInsurerAutoEquipment;
use App\Models\Contracts\OptionalEquipment;
use App\Models\Contracts\Payments;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\InsuranceCompaniesPrograms;
use App\Models\Directories\ProductsPrograms;
use App\Models\Finance\PayMethod;
use App\Services\Kbm\Alfa\Calc;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Directories\Products;
use App\Models\Directories\TypeBso;
use App\Models\Directories\ProductsCategory;
use App\Models\Contracts\ContractsCalculation;
use App\Models\Contracts\Contracts;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Mockery\Exception;

class OnlineController extends Controller
{

    public function __construct()
    {
        $this->breadcrumbs[] = [
            'label' => 'Оформление онлайн',
            'url' => 'contracts/online'
        ];
    }

    //ВЫБОР ПРОДУКТА
    public function index(Request $request)
    {
        $this->middleware('permissions:contracts,online');

        $icp = InsuranceCompaniesPrograms::query();
        $icp->select([
            'insurance_companies.id as insurance_companies_id',
            'insurance_companies.title as insurance_companies_title',

            'product_programs.id as product_programs_id',
            'product_programs.slug as product_programs_slug',
            'product_programs.title as product_programs_title',

            'programs_online_list.sort as programs_online_list_sort',
            'insurance_companies.sort as insurance_companies_sort',

            'products.title as products_title',
            'products.id as products_id',
        ]);
        $icp->from('insurance_companies_programs');
        $icp->leftjoin('insurance_companies', 'insurance_companies.id', '=', 'insurance_companies_programs.insurance_company_id');
        $icp->leftjoin('product_programs', 'product_programs.id', '=', 'insurance_companies_programs.program_id');
        $icp->leftJoin('programs_online_list', 'programs_online_list.program_id', '=', 'insurance_companies_programs.program_id');
        $icp->join('products', 'product_programs.product_id', '=', 'products.id');
        $icp->orderBy('insurance_companies_sort');
        $icp->orderBy('programs_online_list_sort');
        $icp->where('products.is_actual', 1);
        $icp->where('product_programs.is_actual', 1);
        $icp = $icp->get();

        $products = Products::where('is_actual', '1')->where('is_online', '1')->orderBy('id');
        $categories = ProductsCategory::whereIn('id', $products->distinct('category_id')->pluck('category_id')->toArray())->orderBy('sort', 'asc')->get();

        return view('contracts.online.index', [
            'icp' => $icp,
            'products' => $products,
            'categories' => $categories,
        ]);
    }

    //СОЗДАЕМ ВРЕМЕННЫЙ ДОГОВОР
    public function create($product_id, Request $request)
    {
        $this->middleware('permissions:contracts,online');

        $product = Products::where('is_actual', '1')->where('id', '=', $product_id)->first();

        if (!$product) {
            return false;
        }

        $program_id = null;
        if (isset($request->program) && $request->program > 0) {
            $program_id = $request->program;
        }


        $contract = new Contracts;
        $contract->product_id = $product->id;
        $contract->program_id = $program_id;
        $contract->agent_id = auth()->id();
        $contract->user_id = auth()->id();
        $contract->statys_id = 0;
        $contract->is_online = 1;
        $contract->save();


        if ($contract->program) {
            $program = $contract->program;

            if ($program->slug != 'kasko_calculator') {
                ContractsCalculation::createCalc($contract, $contract->product, $contract->program, true);
            }
        }


        return redirect("/contracts/online/edit/{$contract->id}");

    }

    //ОТКРЫВАЕМ ИНТЕРФЕЙС ДОГОВОРА
    public function edit($id)
    {
        $this->middleware('permissions:contracts,online');

        $contract = Contracts::getContractId($id);

        $this->breadcrumbs[] = [
            'label' => $contract->getProductOrProgram()->title,
        ];


        return view('contracts.contract_templates.online_contracts.main', [
            'product' => $contract->getProductOrProgram(),
            'contract' => $contract,
            'view_type' => $contract->getViewStateOnline(),
        ])->with('breadcrumbs', $this->breadcrumbs);

    }


    //Сохраняем договор
    public function save($id)
    {
        $this->checkPermission();

        $contract = Contracts::getContractId($id);

        $data = request()->all();

        return OnlineContractSave::handle($contract, $data);
    }

    public function underfillersSave($id)
    {
        $this->checkPermission();

        $contract = Contracts::find($id);

        $data = request()->all();

        return OnlineContractSave::handle($contract, $data);
    }

    //Выбераем Расчёт
    public function set_sk_calc()
    {
        $this->middleware('permissions:contracts,online');

        $this->validate(request(), [
            'calc_id' => 'integer'
        ]);

        if (!$calculation = ContractsCalculation::query()->find(request('calc_id'))) {
            return back()->with('error', 'Не удалось найти расчёт');
        }

        if ($contract = $calculation->contract) {
            if (!Contracts::getContractId($contract->id)) {
                return back()->with('error', 'Не удалось найти договор');
            }
        }

        if (ContractCalculationOperations::select_calc($contract, $calculation)) {
            return back()->with('success', 'Расчёт выбран');
        }


    }

    public function release_calculate()
    {
        $this->middleware('permissions:contracts,online');

        $contract = Contracts::getContractId(request('id'));

        if (!$contract) {
            return back()->with('error', 'Не удалось найти договор');
        }

        if ($calculation = $contract->calculation) {
            $calculation->statys_id = 2;
            $calculation->save();
            return back()->with('success', 'Расчёт выбран');

        }

        return back()->with('error', 'Не удалось установить расчёт');
    }

    public function return_calculation($contract_id)
    {
        $this->middleware('permissions:contracts,online');

        $contract = Contracts::getContractId($contract_id);

        $contract->update([
            'selected_calculation_id' => 0,
        ]);

        if ($calculations = $contract->online_calculations) {

            foreach ($calculations as $calculation) {
                $calculation->statys_id = 1;
                $calculation->save();
            }
        }

        return back()->with('success', 'Расчёт возвращён успешно');
    }

    //Устанавливаем платеж
    public function payment_confirm($contract_id)
    {
        $this->middleware('permissions:contracts,online');

        $response = new \stdClass();
        $response->state = false;
        $response->error = '';

        $contract = Contracts::getContractId($contract_id);

        $contract->is_epolicy = (int)request('bso_is_epolicy');
        $contract->save();

        if (request('send_email')) {
            $contract->send_email = request('send_email');
            $contract->save();
        }

        $response->error = 'В демонстрационном режиме выпуск договора запрещен';
//        return response()->json($response);


        if ($calculation = $contract->selected_calculation) {


            if (!$payment = $contract->get_payment_first()) {
                $payment = Payments::create(["payment_number" => 1, "statys_id" => 0, "contract_id" => $contract->id]);
            }

            $payment->update([
                "statys_id" => 0,
                'accept_user_id' => auth()->id(),
                'accept_date' => date("Y-m-d H:i:s"),
                "type_id" => 0,
                "payment_total" => $calculation->sum,
                'official_discount' => 0,
                'bank_kv' => 0,
                "payment_data" => date('Y-m-d', time()),
                "informal_discount" => 0,
                'parent_agent_id' => $contract->parent_agent_id,
                'agent_id' => $contract->agent_id,
                'financial_policy_id' => $calculation->financial_policy_id,
                'pay_method_id' => request('payment_method'),
            ]);

            //ПЕРЕСЧЫВАЕМ КВ
            $payment = PaymentFinancialPolicy::actualize($payment);
            $payment = PaymentDiscounts::recount($payment);


            $payment_method = $payment->pay_mehtod;
            $payment->update([
                "payment_type" => $payment_method->payment_type,
                "payment_flow" => $payment_method->payment_flow,
            ]);


            //Квитанция БСО
            if ($payment_method->key_type == 0) {
                $receipt = BsoItem::query()->find(request('bso_receipt_id'));
                $payment->update([
                    "bso_receipt_id" => $receipt->id,
                    "bso_receipt" => $receipt->bso_title,
                ]);
            }

            //Email ОФД
            if ($payment_method->key_type == 1 || $payment_method->key_type == 2) {

            }

            // dd($contract);
            if ((int)$contract->is_epolicy == 0) {

                $bso = BsoItem::query()->find(request('bso_id'));

                $contract->update([
                    'bso_id' => $bso->id,
                    'bso_title' => $bso->bso_title,
                ]);

            }


            if ($service = $calculation->get_product_service()) {
                /*Выпуск сертификата -- функция релиза*/
                $release = $service->release($calculation);

                if ($release->state == true) {

                    if ($payment_method->key_type == 4) { // Платежная страница
                        $mail = Mail::to($contract->send_email)->send(new PaymentLink($release->payment_link));

                        if(!(int)$release->bso_id){ // чтобы сообщить что перед получением бсо - нужно сначала оплатить
                            $release->error = 'Договор ожидает оплаты. Ссылка для оплаты на почте '. $contract->send_email;
                            return response()->json($release);
                        }
                    }

                    $bso = BsoItem::query()->find($release->bso_id);
                    $contract->update([
                        'bso_id' => $bso->id,
                        'bso_title' => $bso->bso_title,
                        'statys_id' => $release->statys_id,
                    ]);

                    $payment->update([
                        "bso_id" => $bso->id,
                        'org_id' => $bso->org_id,

                    ]);

                    $bso->update([
                        'state_id' => 2,
                        'location_id' => 1,
                    ]);
                    $bso->setBsoLog(1);

                    //Квитанция БСО
                    if ($payment_method->key_type == 0 && $payment->receipt) {
                        $payment->receipt->update([
                            'state_id' => 2,
                            'location_id' => 1,
                        ]);
                    }


                    $calculation->update([
                        'statys_id' => $release->statys_id,
                    ]);

                    //ПОЛУЧАЕМ ДОКУМЕНТТЫ
                    if ($release->statys_id == 4) {
                        $service->get_files($calculation);
                    }

                }


                //ЕСЛИ ДОГОВОР НАДО ПРОВЕРЯТЬ то $contract->statys_id = 1


                $response = $release;

            }
        }

        return response()->json($response);

    }

    public function check_status($id, Request $request)
    {
        $this->middleware('permissions:contracts,online');

        $contract = Contracts::getContractId($id);
        if ($calculation = $contract->selected_calculation) {
            if ($service = $calculation->get_product_service()) {

                $response = $service->check_status($calculation, $request);

                return response()->json($response);
            }
        }

    }


    public function confirm_sms($id, Request $request)
    {
        $this->middleware('permissions:contracts,online');

        $contract = Contracts::getContractId($id);
        if ($calculation = $contract->selected_calculation) {
            if ($service = $calculation->get_product_service()) {

                $response = $service->sign($calculation, $request);

                return response()->json($response);
            }
        }

    }


    public function temp_calculate($id)
    {
        $this->middleware('permissions:contracts,online');

        $contract = Contracts::getContractId($id);
        $product = $contract->product;
        $supplier_offers = [];

        $data = request()->all();

        OnlineContractSave::handle($contract, $data);

        //если есть валидатор - валидируем
        if ($validator = $product->get_online_validator($data)) {

            //если сфейлили
            if (!$valid = $validator->add_to_process()->validate()) {

                return response()->json([
                    'errors' => $validator->get_errors()
                ]);

            }

        }

        //СОЗДАЕМ ВСЕ АКТИВНЫЕ ПРОГРАММЫ
        $programs = ProductsPrograms::getActivPrograms($contract, $contract->product->slug);

        //dd($programs);

        foreach ($programs as $prog) {
            if ($prog->dir_name) {
                ContractsCalculation::createCalc($contract, $contract->product, $prog, false);
            }
        }

        //ПРОЗВОДМ РАСЧЕТ
        foreach ($contract->online_calculations as $calculation) {

            $calculation->temp_calc();
            /*
            if($service = $calculation->get_product_service($calculation)) {
                $calcs = $service->temp_calc($calculation);

            }
            */
        }


        return [
            'html' => view('contracts.contract_templates.online_contracts.tariff.insurance_price', [
                'online_calculations' => $contract->online_calculations
            ])->render(),
            'errors' => []
        ];

    }


    public function get_calculate_list($contract_id)
    {
        $this->middleware('permissions:contracts,online');

        $contract = Contracts::getContractId($contract_id);
        $product = $contract->product;
        ContractsCalculation::where('contract_id', $contract->id)->update(['statys_id' => -1]);

        $result = [];
        $suitable_segments = SegmentsOnRequest::get_suitable_product_segment($contract_id, $product->id);
        foreach ($suitable_segments as $segments) {

            $segments = (object)$segments;

            $calculation = ContractsCalculation::query()
                ->where('contract_id', $contract->id)
                ->where('insurance_companies_id', $segments->insurance_companies_id)
                ->where('bso_supplier_id', $segments->bso_supplier_id)
                ->where('product_id', $contract->product_id)
                ->get()
                ->first();

            if (!$calculation) {
                $calculation = new ContractsCalculation();
                $calculation->contract_id = $contract->id;
                $calculation->insurance_companies_id = $segments->insurance_companies_id;
                $calculation->bso_supplier_id = $segments->bso_supplier_id;
                $calculation->product_id = $contract->product_id;
                $calculation->save();
            }


            $calculation->financial_policy_id = $segments->id;
            $calculation->statys_id = 0;
            $calculation->sum = '';
            $calculation->messages = null;
            $calculation->save();

            $result[] = $calculation->id;
        }

        return response()->json($result);
    }

    public function get_calculate_sk($calculation_id)
    {
        $this->middleware('permissions:contracts,online');

        $calculation = ContractsCalculation::find($calculation_id);

        $bso_supplier_id = $calculation->bso_supplier_id;
        $bso_supplier = BsoSuppliers::find($bso_supplier_id);

        $api_setting = $bso_supplier->api_setting;

        $calculation->product->slug;



        return view('contracts.contract_templates.online_contracts.tariff.start_calculate_sk', [
            'calculation_id' => $calculation_id,
            'calculation' => ContractsCalculation::find($calculation_id),
            'api_setting' => $api_setting
        ]);

    }

    public function calculate_sk($calculation_id)
    {
        $this->middleware('permissions:contracts,online');

        $calculation = ContractsCalculation::find($calculation_id);
        $calculation->calculate();

        return view('contracts.contract_templates.online_contracts.tariff.default.price', [
            'calculation_id' => $calculation_id,
            'calc' => ContractsCalculation::find($calculation_id)
        ]);
    }

    public function accept_save_policy_sk($calculation_id)
    {
        $this->middleware('permissions:contracts,online');

        $calculation = ContractsCalculation::find($calculation_id);
        return response()->json($calculation->accept_save_policy_sk());
    }

    public function calculate($id)
    {
        $this->middleware('permissions:contracts,online');

        $contract = Contracts::getContractId($id);
        $product = $contract->product;

        $supplier_offers = [];

        $data = request()->all();

        OnlineContractSave::handle($contract, $data);


        /*
        //если есть валидатор - валидируем
        if($validator = $product->get_online_validator($data)){

            //если сфейлили
            if(!$valid = $validator->add_to_process()->validate()){

                return response()->json([
                    'errors' => $validator->get_errors()
                ]);
            }
        }
        */

        if ($contract->calculation) {
            //ОПРЕДЕЛЯЕМ СЕГМЕНТ
            $calculation = $contract->calculation;

            $is_set_calt = $calculation->calculate(null);


            return [
                'html' => view("contracts.contract_templates.online_contracts.tariff.{$contract->getProductOrProgram()->slug}.price", [
                    'calc' => $calculation, 'contract' => $contract, 'is_set_calt' => $is_set_calt
                ])->render(),
                'errors' => []
            ];

        } else {

            $suitable_segments = SegmentsOnRequest::get_suitable_product_segment($contract->id, $product->id);
            ContractsCalculation::where('contract_id', $contract->id)->update(['statys_id' => -1]);

            foreach ($suitable_segments as $segment) {

                $bso_supplier = $segment->bso_supplier;

                $calculation = ContractsCalculation::query()
                    ->where('contract_id', $contract->id)
                    ->where('insurance_companies_id', $bso_supplier->insurance_companies_id)
                    ->where('bso_supplier_id', $bso_supplier->id)
                    ->where('product_id', $contract->product_id)
                    ->first();

                if (!$calculation) {
                    $calculation = new ContractsCalculation();
                    $calculation->contract_id = $contract->id;
                    $calculation->insurance_companies_id = $bso_supplier->insurance_companies_id;
                    $calculation->bso_supplier_id = $bso_supplier->id;
                    $calculation->product_id = $contract->product_id;
                    $calculation->save();
                }


                $calculation->statys_id = 0;
                $calculation->sum = '';
                $calculation->messages = null;


                $calculation->calculate($segment);

                $supplier_offers[] = $calculation;


            }

            return [
                'html' => view('contracts.contract_templates.online_contracts.tariff.insurance_price', [
                    'online_calculations' => $contract->online_calculations
                ])->render(),
                'errors' => []
            ];

        }


    }

    public function driver_form_calc($id)
    {
        $this->checkPermission();

        $view_type = request('view_type', 'view');
        return view("contracts.contract_templates.online_contracts.drivers.partials.calc.{$view_type}", [
            'driver' => new Driver(),
            'i' => $id,
        ]);
    }

    public function driver_form($id)
    {
        $this->checkPermission();

        $view_type = request('view_type', 'view');
        return view("contracts.contract_templates.online_contracts.drivers.partials.driver.{$view_type}", [
            'driver' => Driver::create(),
            'i' => $id,
        ]);
    }


    public function driver_old_license_form($id)
    {
        $this->checkPermission();

        $driver = Driver::find($id);

        return view("contracts.contract_templates.online_contracts.drivers.partials.driver_old_license.edit", [
            'driver' => $driver,
            'driver_old_data' => $driver->old_license,
        ]);
    }


    public function driver_old_license_save($id, Request $request)
    {
        $this->checkPermission();

        $driver = Driver::find($id);

        $update_or_create_array = [
            'driver_id' => $id,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'doc_series' => $request->doc_series,
            'doc_number' => $request->doc_number,
            'doc_date_issue' => date('Y-m-d', strtotime($request->doc_date_issue)),
            'doc_date_end' => date('Y-m-d', strtotime($request->doc_date_end)),
        ];

        if($old_license_data = $driver->old_license){
            $old_license_data->update($update_or_create_array);
        }else {
            DriverPreviousLicense::create($update_or_create_array);
        }

        return ['state' => 1];
    }


    public function driver_old_license_delete($id, Request $request)
    {
        $this->checkPermission();

        $driver = Driver::find($id);

        if($old_license_data = $driver->old_license){
            $old_license_data->delete();
        }

        return ['state' => 1];
    }

    public function optional_equipment($object_insurer_id = null)
    {
        $count = request('i');
        return view("contracts.contract_templates.online_contracts.vehicles.optional_equipment", [
            'i' => $count
        ]);
    }

    public function optional_equipment_all()
    {
        $this->checkPermission();

        $equipments = ObjectInsurerAutoEquipment::query()->where('object_insurer_id', request('object_insurer_id'))->get();
        return view("contracts.contract_templates.online_contracts.vehicles.optional_equipment_get_all", [
            'equipments' => $equipments
        ]);
    }

    public function optional_equipment_detele($object_insurer_auto_id)
    {
        $this->checkPermission();

        ObjectInsurerAutoEquipment::findOrFail($object_insurer_auto_id)->delete();
    }

    public function get_drivers($id)
    {
        $this->checkPermission();

        $contract = Contracts::getContractId($id);
        $drivers_ids = $contract->drivers->pluck('id');

        return $drivers_ids;
    }

    /**
     * Редактирование расчета
     * @param type $id
     */
    public function edit_calculation($id)
    {

    }

    public function issue_form($product_id, $insurance_id)
    {
        $this->checkPermission();

        return [
            'html' => view('contracts.online.issue_form', ['product_id' => $product_id, 'insurance_id' => $insurance_id])->render(),
        ];
    }

    public function release_boxes($contract_id)
    {
        $this->middleware('permissions:contracts,online');

        $result = new \stdClass();
        $result->state = false;
        $result->msg = '';

        $contract = Contracts::getContractId($contract_id);
        $contract->is_epolicy = 1;
        $contract->save();

        $calculation = $contract->calculation;
        if ($service = $calculation->get_product_service()) {
            $release = $service->release($calculation);

            if ($release->state == true) {
                $calculation->statys_id = 2;
                $calculation->sum = $release->payment_total;
                $calculation->sk_key_id = $release->sk_key_id;
                $calculation->json = \GuzzleHttp\json_encode($release);

                //Находим акттуальную ФП
                $fp = FinancialPolicy::query()
                    ->where('is_actual', 1)
                    ->where('bso_supplier_id', $calculation->bso_supplier_id)
                    ->where('insurance_companies_id', $calculation->insurance_companies_id)
                    ->where('product_id', $calculation->product_id)
                    ->where('date_active', '<=', getDateTime())->get()->last();

                if ($fp) {
                    $calculation->financial_policy_id = $fp->id;
                }
                $calculation->save();

                //Получаем документы
                $service->get_files($calculation);

                //Создаем платеж

                $payment_method = PayMethod::query()
                    ->where('is_actual', 1)
                    ->where('payment_type', 0)
                    ->where('payment_flow', 0)
                    ->where('key_type', 2)->get()->last();

                if ($payment_method) {
                    if (!$payment = $contract->get_payment_first()) {
                        $payment = Payments::create(["payment_number" => 1, "statys_id" => 0, "contract_id" => $contract->id]);
                    }

                    $payment->update([
                        "statys_id" => 0,
                        'accept_user_id' => auth()->id(),
                        'accept_date' => date("Y-m-d H:i:s"),
                        "type_id" => 0,
                        "payment_total" => $calculation->sum,
                        'official_discount' => 0,
                        'bank_kv' => 0,
                        "payment_data" => date('Y-m-d', time()),
                        "informal_discount" => 0,
                        'parent_agent_id' => $contract->parent_agent_id,
                        'agent_id' => $contract->agent_id,
                        'financial_policy_id' => $calculation->financial_policy_id,
                        'pay_method_id' => $payment_method->id,
                        "payment_type" => $payment_method->payment_type,
                        "payment_flow" => $payment_method->payment_flow,
                    ]);

                    //ПЕРЕСЧЫВАЕМ КВ
                    $payment = PaymentFinancialPolicy::actualize($payment);
                    $payment = PaymentDiscounts::recount($payment);

                }


                // dd($release);

            } else {
                $result->msg = $release->error;
            }

        }


        return response()->json($result);
    }

    public function checkPermission(){
        if (auth()->user()->hasPermission('contracts', 'online') || auth()->user()->is('underfiller')){
            return true;
        }

        abort(403);
    }

}
