<?php

namespace App\Http\Controllers\Contracts\Online;

use App\Domain\Process;
use App\Domain\Processes\Operations\Contracts\Online\ContractCalculationOperations;
use App\Domain\Samplers\Segments\SegmentsOnRequest;
use App\Interfaces\Services\SK\ProductServiceInterface;
use App\Models\Api\SK\ApiSetting;
use App\Models\Contracts\Driver;
use App\Models\Contracts\KbmCalculation;
use App\Models\Contracts\Subjects;
use App\Models\Directories\ProductsPrograms;
use App\Services\Kbm\Alfa\Calc;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Directories\Products;
use App\Models\Directories\TypeBso;
use App\Models\Directories\ProductsCategory;
use App\Models\Contracts\ContractsCalculation;
use App\Models\Contracts\Contracts;
use Illuminate\Support\Str;
use Mockery\Exception;

class ActionController extends Controller {

    public function __construct() {
        $this->breadcrumbs[] = [
            'label' => 'Оформление онлайн',
            'url' => 'contracts/online'
        ];
    }


    public function subject($id, Request $request) {
        $this->checkPermission();
        $subject = new Subjects();
        $subject_name = $request->name;
        $forUnderfill = request()->has('forUnderfill') && (int)$request->forUnderfill ? true : false;

        if($id > 0){
            $contract = Contracts::findOrFail($id);
            if($subject_name == 'insurer'){
                if($contract->insurer) $subject = $contract->insurer;
            }

            if($subject_name == 'owner'){
                if($contract->owner) $subject = $contract->owner;
            }

        }else{
            $contract = new Contracts();
        }

        $subject->type = (int)$request->type;

        $view_type = $contract->getViewStateOnline();

        return view("contracts.contract_templates.online_contracts.subject.partials.{$view_type}.{$subject->type}", [
            'subject' => $subject,
            'subject_name' => $subject_name,
            'contract' => $contract,
            'forUnderfill' => $forUnderfill
        ]);

    }


    //Пролучения КБМ
    public function get_kbm(Request $request) {
        $this->checkPermission();

        $result = new \stdClass();
        $result->kbm = '1';
        $result->class_kbm = '1';

        $data = (object)$request->data;


        $name = explode(" ", $data->fio);

        if(isset($name[2]) && strlen($data->birth_date)>5 && strlen($data->doc_serie)>1 && strlen($data->doc_num)>1){

            try {
                $kbmCalculation = KbmCalculation::create([
                    'first_name'      => $name[1],
                    'last_name'       => $name[0],
                    'middle_name'     => $name[2],
                    'birth_date'      => date("Y-m-d 00:00:00", strtotime($data->birth_date)),
                    'document_series' => $data->doc_serie,
                    'document_number' => $data->doc_num,
                    'foreign_docs'    => isset($data->foreign_docs) && $data->foreign_docs ? 1 : 0,
                ]);


                $calc = new Calc();

                $calc->handle($kbmCalculation);

                $result->kbm = $kbmCalculation->kbm;
                $result->class_kbm = $kbmCalculation->class_kbm;
                $result->rsa_request_id = $kbmCalculation->rsa_request_id;

            } catch (Exception $exception) {

            }
        }

        return response()->json($result);
    }

    //Удаляем водителя
    public function delete_driver(Request $request) {
        $this->checkPermission();

        $driver =  Driver::find($request->driver_id);
        $driver->delete();

        return response(200);

    }


    public function creare_new_calc($id)
    {
        $this->checkPermission();


        $contract = Contracts::findOrFail($id);
        $programs = ProductsPrograms::getActivPrograms($contract, $contract->product->slug);


        return view("contracts.contract_templates.online_contracts.programs.select", [
            'programs' => $programs,
            'contract' => $contract,
        ]);

    }

    public function set_new_calc($id, Request $request)
    {
        $this->checkPermission();

        $result = new \stdClass();
        $result->state = true;
        $result->msg = '';

        $contract = Contracts::findOrFail($id);
        $contract->program_id = $request->program_id;
        $contract->save();

        ContractsCalculation::createCalc($contract, $contract->product, $contract->program, true);

        return response()->json($result);
    }

    public function get_prolongation($id, Request $request)
    {
        $this->checkPermission();

        if ($request->is_prolongation){
            $contract = Contracts::findOrFail($id);
            return view("contracts.contract_templates.online_contracts.prolongation.partials.inner", [
                'prev_policy_serie' => $contract->prev_policy_serie,
                'prev_policy_number' => $contract->prev_policy_number,
                'prev_sk_id' => $contract->prev_sk_id,
            ]);
        }else{
            return null;
        }

    }


    public function checkPermission(){
        if (auth()->user()->hasPermission('contracts', 'online') || auth()->user()->hasPermission('contracts', 'underfillers')){
            return true;
        }

        abort(403);
    }

}
