<?php

namespace App\Http\Controllers\Contracts\Online\Products;


abstract class BaseProduct implements BaseProductInterface {

    protected $errors = [];
    protected $form_errors = [];
    protected $messages = [];
    protected $sum = 1000;
    protected $formValues = [];

    public function __construct($formValues) {
        $this->formValues = $formValues;
    }

    public function getResult() {
        $this->beforeValidate();
        if (!$this->validate()) {
            return [
                'errors' => $this->errors,
                'form_errors' => $this->form_errors
            ];
        }
        $this->afterValidate();
        $this->calculate();
        $this->afterCalculate();
        return [
            'sum' => $this->sum,
            'messages' => $this->messages,
        ];
    }

    /**
     * Валидация формы
     */
    protected function validate() {
        if ($this->errors || $this->form_errors) {
            return false;
        }
        return true;
    }

    protected function beforeValidate() {
        return false;
    }

    protected function afterValidate() {
        return false;
    }

    protected function afterCalculate() {
        return false;
    }

    /**
     * Возвращает стоимость продукта
     * @return float
     */
    protected function calculate() {
        return false;
    }

}
