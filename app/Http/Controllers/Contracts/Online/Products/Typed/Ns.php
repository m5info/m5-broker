<?php

namespace App\Http\Controllers\Contracts\Online\Products\Typed;

use App\Http\Controllers\Contracts\Online\Products\BaseProduct;

class Ns extends BaseProduct {

    protected function validate() {

        foreach ($this->formValues['contract'] as $i => $contract) {
            
            $insurer = $contract['insurer'];
            
            if (!isset($insurer['fio']) || !$insurer['fio']) {
                $this->form_errors['#insurer_fio_0'][] = 'Нужно заполнить фио страхователя';
            }

            if (!isset($insurer['date']) || !$insurer['date']) {
                $this->form_errors['#insurer_birthdate_0'][] = 'Нужно заполнить дату рождения страхователя';
            }
        }


        //$this->errors[] = 'Указанные данные заполнены неверно';
        return parent::validate();
    }

    protected function calculate() {
        $this->sum = 4000;
    }

}
