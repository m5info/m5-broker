<?php

namespace App\Http\Controllers\Contracts;

use App\Http\Controllers\Controller;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsDocuments;
use App\Models\File;
use App\Repositories\FilesRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ActionsMasksController extends Controller {

    protected $filesRepository;

    public function __construct(FilesRepository $filesRepository) {

        $this->filesRepository = $filesRepository;
    }

    public function store($contract_id, Request $request) {

        Contracts::findOrFail($contract_id)->masks()->save($this->filesRepository->makeFile($request->file, Contracts::FILES_DOC . "/$contract_id/"));

        return response('', 200);
    }


    public function download_all_scans($id){

        $contract = Contracts::findOrFail($id);
        $files = [];

        foreach($contract->scans as $scan){
            $files[] = url('files/'. $scan->name);
        }

        return $files;
    }

}
