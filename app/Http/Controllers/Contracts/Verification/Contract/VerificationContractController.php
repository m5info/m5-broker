<?php

namespace App\Http\Controllers\Contracts\Verification\Contract;

use App\Domain\Samplers\Contracts\TabsVisibility;
use App\Http\Controllers\Controller;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ObjectInsurer;
use App\Models\User;
use App\Models\Users\Permission;
use Illuminate\Http\Request;

class VerificationContractController extends Controller {

    public function __construct(){
        $this->middleware('permissions:contracts,verification');
    }

    public function edit($id){
        $contract = Contracts::getContractId($id);

        if (!$contract || $contract['statys_id'] == 4) {
            return view('errors.403', ['exception' => 1]);
        }

        $agents = User::getALLUser(24)->pluck('name', 'id');

        $object_insurer = $contract->object_insurer;
        if (!$object_insurer) {
            $object_insurer = new ObjectInsurer();
            if ($contract->bso->product->category->template == 'auto') {
                $object_insurer->type = 1;
            }
        }

        $view_type = 'view';
        if($contract->kind_acceptance == 0 && $contract->statys_id == 2 && auth()->user()->hasPermission('role_owns', 'is_manager')){
            $view_type = 'view_edit';
        }
        $tabs = TabsVisibility::get_verification_tab_visibility();
        if(isset($tabs[$contract->statys_id]) && $tabs[$contract->statys_id]['edit'] == 1){
            if(!$contract->is_online) {
                $view_type = 'edit';
            }
        }

        $this->breadcrumbs[] = [
            'label' => 'Проверка/Подтверждение',
            'url' => 'contracts/verification',
        ];

        $this->breadcrumbs[] = [
            'label' => 'Редактирование',
        ];

        $template = $contract->is_online ?
            'contracts.contract_templates.online_contracts.main' :
            'contracts.contract_templates.temp_contracts.main';

        $permission = auth()->user()->hasPermission('role_owns','temp_contract_visibility_not_full');

        $roles_ids = array_column(Permission::where('title', '=', 'is_referencer')->first()->roles->toArray(),'id');

        $referencers = User::query()->whereIn('role_id',$roles_ids)->get();

        return view($template, [
            'permission' => $permission,
            'object_insurer' => $object_insurer,
            "contract" => $contract,
            "product" => $contract->product,
            'agents' => $agents,
            'referencers' => $referencers,
            'view_type' => $view_type
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function return_to_check($id, Request $request){

        $contract = Contracts::getContractId($id);

        $contract->update([
            'statys_id' => 1,
            'kind_acceptance' => 0,
        ]);

        return redirect("/contracts/verification/contract/$id/edit");
    }

}