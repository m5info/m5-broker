<?php

namespace App\Http\Controllers\Contracts\Verification;

use App\Classes\Export\ExportManager;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Contracts\TempContracts\VerificationContractListQueryHandler;
use App\Models\Contracts\Contracts;
use App\Models\Directories\Products;
use App\Models\Settings\TemplateCategory;
use Illuminate\Http\Request;

class VerificationController extends Controller
{


    public function __construct()
    {
        $this->middleware('permissions:contracts,verification');
    }




    public function index(Request $request)
    {



        $count_arr = Contracts::getVerificationCountArr();
        $products = Products::query()->where('is_actual', 1)->get();//->whereIn('id', $builder->get()->pluck('product_id'))->get();


        return view('contracts.verification.index', [
            "count_arr" => $count_arr,
            'products' => $products,
            'set_tab' => $request->set_tab ? $request->set_tab : -1
        ]);


    }



    public function get_tab(){

        //dd("OK");

        $builder = Contracts::getContractsQuery();
        $builder->where('kind_acceptance', '!=', 1);
        //$sks = InsuranceCompanies::query()->whereIn('id', $builder->get()->pluck('insurance_companies_id'))->get();
        //$insurers = Subjects::query()->whereIn('id', $builder->get()->pluck('insurer_id'))->get();
        $products = Products::query()->where('is_actual', 1)->get();//->whereIn('id', $builder->get()->pluck('product_id'))->get();
        //$agents = User::getALLUserWhere()->get();//query()->whereIn('id', $builder->get()->pluck('agent_id'))->get();

        //dd($agents);


        return view('contracts.verification.partials.info_tab', [
            'products' => $products,
        ]);
    }



    public function get_list(){

        $this->validate(request(), [
            'statys' => 'integer',
            'product' => 'integer',
            'insurer' => 'string',
            'agent' => 'integer',
            'bso_title' => 'string',
            'conclusion_date_from' => 'date',
            'conclusion_date_to' => 'date',
        ]);

        $builder = Contracts::getContractsQuery();
        $builder->leftJoin('subjects as insurer', 'insurer.id', '=', 'contracts.insurer_id');
        $builder->leftJoin('users as agent', 'contracts.agent_id', '=', 'agent.id');
        $builder->leftJoin('users as manager', 'contracts.manager_id', '=', 'manager.id');
        $builder->leftJoin('users as check_user', 'contracts.check_user_id', '=', 'check_user.id');
        $builder->leftJoin('bso_items', 'bso_items.id', '=', 'contracts.bso_id');
        $builder->leftJoin('products', 'products.id', '=', 'bso_items.product_id');
        $builder->leftJoin('insurance_companies', 'contracts.insurance_companies_id', '=', 'insurance_companies.id');

        $builder->select([
            'contracts.id as contract_id',
            'contracts.sign_date as contract_sign_date',
            'contracts.begin_date as contract_begin_date',
            'contracts.end_date as contract_end_date',
            'contracts.statys_id as contract_statys_id',
            'contracts.check_date as contract_check_date',
            'insurance_companies.title as insurance_companies_title',
            'products.title as product_title',
            'insurer.title as insurer_title',
            'agent.name as agent_name',
            'manager.name as manager_name',
            'check_user.name as check_user_name',
            'bso_items.bso_title as bso_items_bso_title',
            'contracts.kind_acceptance as contract_kind_acceptance',
            'contracts.delay_date as delay_date'
        ]);

        // на всех вкладках безусловные не вводятся, кроме "на оплате"
        if(request('statys') != 3){
            $builder->where('contracts.kind_acceptance', '!=', 1);
        }

        $builder->orderBy('contracts.updated_at', 'desc');

        $contracts = (new VerificationContractListQueryHandler($builder))->allowEmpty()->apply();

        return $contracts->get();
    }

    public function get_table()
    {
        return view('contracts.verification.table', [
            "contracts" => $this->get_list(),
        ]);
    }

    public function get_export_table(){

        $view = request('view');
        $views = ['in_check'];


        if(in_array($view, $views)){
            return [
                'html' => view("contracts.verification.export.{$view}", [
                    "contracts" => $this->get_list(),
                ])->render()
            ];
        }

        return false;

    }

    public function get_export_act_of_disagreement(Request $request)
    {
        $p = Contracts::query();
        $p->whereIn('id', array_keys($request->contract));
        $category = TemplateCategory::get('contracts_act_of_disagreement');
        return (new ExportManager($category, $p, false, true))->handle();
    }

}

