<?php

namespace App\Http\Controllers\Contracts;

use App\Http\Controllers\Controller;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsChat;
use App\Models\Settings\ProgramsOnlineList;
use App\Models\User;
use App\Models\Vehicle\VehicleCategoriesOKP;
use App\Models\Vehicle\VehicleModels;
use App\Services\FireBase\FireBaseAndroid;
use App\Services\Pushers\PusherRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ActionsChatController extends Controller
{


    protected $pusherRepository;

    public function __construct(PusherRepository $pusherRepository)
    {
        $this->pusherRepository = $pusherRepository;
    }

    public function setPush($contract_id, Request $request)
    {


        $contract = Contracts::findOrFail($contract_id);

        $message = new ContractsChat([
            'sender_id' => auth()->id(),
            'text' => $request->text,
            'date_sent' => Carbon::now(),
            'status' => ContractsChat::STATUS_SENT,
            'is_player' => ContractsChat::EMPLOYEE,
        ]);


        $inspection = $contract->inspection;
        if ($inspection && $inspection->processing_user_id > 0) {
            $user = User::find($inspection->processing_user_id);
            if ($user->firebase && $user->is_work == 1) {
                $fcm = new FireBaseAndroid();
                $fcm->push("Сообщение", $request->text, $contract->id, 2, $user->firebase);
            }
        }


        $contract->chats()->save($message);

        $this->pusherRepository->triggerContractChat($message);

        return response()->json([
            'sender' => $message->sender->name,
            'date' => $message->date_sent->format('d.m.Y H:i'),
            'text' => $message->text,
            'status' => $message->status,
            'is_player' => $message->is_player,
        ]);

    }


    public function read($contract_id, Request $request)
    {
        ContractsChat::unread()->where('contract_id', $contract_id)->where('sender_id', '!=', auth()->id())->update(['status' => ContractsChat::STATUS_RECEIPT, 'date_receipt' => Carbon::now()]);
    }


    public function documents($contract_id, Request $request)
    {
        $contract = Contracts::findOrFail($contract_id);
        $file = $contract->scans->where('id', $request->id)->last();

        return view('contracts.chat.documents', [
            "file" => $file,
        ]);

    }


}
