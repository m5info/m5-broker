<?php

namespace App\Http\Controllers\Contracts\Underfillers;

use App\Domain\Processes\Scenaries\Contracts\Online\Products\Osago;
use App\Helpers\PaginationHelper;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Contracts\Underfillers\UnderfillersQueryHandler;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ObjectInsurer;
use App\Models\Contracts\ObjectInsurerAuto;
use App\Models\Contracts\Subjects;
use App\Models\Contracts\UnderfillersCalculations;
use App\Models\Vehicle\VehicleModels;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UnderfillersController extends Controller
{
    public function __construct()
    {
        $this->middleware('permissions:contracts,underfillers');
    }

    public function index()
    {
        return view('contracts.underfillers.index');
    }

    public function complete_polices()
    {
        return view('contracts.underfillers.index');
    }

    public function edit($id)
    {

        $contract = Contracts::where('id', '=', $id)->first();

        $this->breadcrumbs[] = [
            'label' => 'Дозаполнение полисов',
            'url' => 'contracts/underfillers'
        ];

        $this->breadcrumbs[] = [
            'label' => "Договор $contract->id (БСО: $contract->bso_title)"
        ];

        if ($contract == null || ($contract->kind_acceptance != 1)) {
            return redirect('/contracts/underfillers/');
        }

        return view('contracts.underfillers.edit', [
            'contract' => $contract,
            'breadcrumbs' => $this->breadcrumbs,
        ]);
    }

    public function save_contract(Request $request)
    {
        $errors = $this->validation($request);

        if (count($errors) > 0) {
            return back()->with([
                'error' => current($errors),
            ])->withInput();
        }


        $contract = Contracts::where('id', '=', $request->id)->first();

        if ($contract == null || ($contract->kind_acceptance != 1 /*|| $contract->all_payments->first()->statys_id != 1*/)) {
            return redirect('/contracts/underfillers/');
        }

        if ($insurerData = $request->contract['insurer']){
            $contract->insurer_id = Osago::create_update_online_subject((object)$insurerData, 'insurer', $contract->insurer_id);
        }

        $object_insurer = ObjectInsurer::where('id', $contract->object_insurer_id)->first();

        $object_insurer->title = trim($request->title);

        if (request('underfiller_check')) {
            $contract->underfiller_check = request('underfiller_check');
            $contract->underfiller_id = \Auth::user()->id;
        }

        $object_insurer->save();
        $contract->save();

        return back()->with('success', 'Данные успешно обновлены!');
    }

    public function get_models()
    {
        if (request('mark')) {
            return VehicleModels::where('mark_id', '=', request('mark'))->get();
        }
    }

    public function get_complete_polices_table()
    {
        $data = $this->get_complete_polices_list();
        $data['html'] = view('contracts.underfillers.table', $data)->render();
        return $data;
    }

    public function get_complete_polices_list()
    {
        $contracts = (new UnderfillersQueryHandler(Contracts::query()->where('kind_acceptance', '=', 1)->where('underfiller_check', '=', 1)))->allowEmpty()->apply();
        $contracts->where('underfiller_check', '=', '1');
//        $contracts->whereHas('all_payments', function ($query) {
//            $query->where('statys_id', '=', 1);
//            $query->orWhere('statys_id', '=', 0);
//        });

        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
        $result = PaginationHelper::paginate($contracts, $page, $page_count);

        $contracts = $result['builder']->get();

        return [
            'contracts' => $contracts,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];
    }

    public function get_table()
    {
        $data = $this->get_list();
        $data['html'] = view('contracts.underfillers.table', $data)->render();
        return $data;
    }

    public function get_list()
    {

        $contracts = (new UnderfillersQueryHandler(Contracts::query()
            ->with('product','insurer','object_insurer','check_user')
            ->where(function ($q){
                $q->where('kind_acceptance', '=', 1);
                $q->orWhere('kind_acceptance', '=', 0);
            })
            ->where('contracts.statys_id', '!=', -1)
            ->where('underfiller_check', '=', 0)))
            ->allowEmpty()
            ->apply();
//        закомментил чтоб показывались просто безусловные
//        $contracts->whereHas('all_payments', function ($query) {
//            $query->where('statys_id', '=', 1);
//        });

        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
        $result = PaginationHelper::paginate($contracts, $page, $page_count);

        $contracts = $result['builder']->get();

        return [
            'contracts' => $contracts,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];

    }


    public function close_contract($id)
    {
        $contract = Contracts::getContractId($id);
        $contract->underfiller_check = 1;
        $contract->underfiller_id = auth()->id();
        $contract->underfiller_date = Carbon::now()->format('Y-m-d H:i:s');
        $contract->save();

        return response()->json(['state'=>true]);
    }

    private function validation(Request $request)
    {
        $errors = [];

        if (isset($request->serial_passport) && $request->serial_passport != ''){
            if (!preg_match('/^[0-9А-Яа-яA-Za-z ]+$/', $request->serial_passport))
            {
                $errors['serial_passport'] = 'Серия паспорта не может быть со спецсимволами';
            }
        }

        if (isset($request->number_passport) && $request->number_passport != ''){
            if (!preg_match('/^[0-9А-Яа-яA-Za-z ]+$/', $request->number_passport))
            {
                $errors['number_passport'] = 'Номер паспорта не может быть со спецсимволами';
            }
        }

        if (isset($request->inn) && $request->inn != ''){
            if (!preg_match('/^[0-9]+$/', trim($request->inn)))
            {
                $errors['inn'] = 'ИНН должен состоять из цифр';
            }
        }

        if (isset($request->kpp) && $request->kpp != ''){
            if (!preg_match('/^[0-9]+$/', $request->kpp))
            {
                $errors['kpp'] = 'КПП должен состоять из цифр';
            }
        }

        if (isset($request->reg_number) && $request->reg_number != ''){
            if (!preg_match('/^[0-9А-Яа-яA-Za-z ]+$/', (string)$request->reg_number))
            {
                $errors['reg_number'] = 'Рег. номер может состоять только из цифр и букв';
            }
        }

        if (isset($request->year_of_car) && $request->year_of_car != '' && $request->year_of_car != 0){
            if (!preg_match('/^[0-9]{4}+$/', (int)$request->year_of_car))
            {
                $errors['year_of_car'] = 'Год выпуска должен состоять из 4-х цифр, например "1997"';
            }
        }

        return $errors;

    }


    public function saveCalc($contract_id, Request $request)
    {
        $data = $request->all();
        $contract = Contracts::find($contract_id);

        if($underfillers_calculation = $contract->underfillers_calculation){
            $underfillers_calculation->update([
                'contract_id' => $contract_id,
                'product_id' => $contract->product_id,
                'json_calculate' => json_encode($data),
            ]);
        }else{
            UnderfillersCalculations::create([
                'contract_id' => $contract_id,
                'product_id' => $contract->product_id,
                'json_calculate' => json_encode($data),
            ]);
        }

        return response()->json(['status' => 1]);
    }

}