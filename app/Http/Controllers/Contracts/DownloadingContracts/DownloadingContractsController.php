<?php

namespace App\Http\Controllers\Contracts\DownloadingContracts;

use App\Domain\Processes\Operations\Contracts\ContractAcceptDownloading;
use App\Http\Controllers\Controller;
use App\Models\Contracts\Contracts;
use App\Models\Directories\BsoSuppliers;

use App\Repositories\FilesRepository;
use Illuminate\Http\Request;

class DownloadingContractsController extends Controller
{


    public function __construct()
    {
        $this->middleware('permissions:contracts,downloading_contracts');
    }


    public function index(Request $request)
    {
        $bso_suppliers = BsoSuppliers::where('is_actual', 1)->get();


        return view('contracts.downloading_contracts.index', [
            'bso_suppliers' => $bso_suppliers,
        ]);

    }


    public function loadData(Request $request)
    {
        $path = $request->file;
        $bso_supplier_id = $request->bso_suppliers;
        $product_id = $request->product_id;
        $par = $request->par;
        $columns = $request->excel_columns;

        $xls = \PHPExcel_IOFactory::load($path);
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();

        $columnsIndex = array();

        for ($i = 0; $i <= 1; $i++) {
            $nColumn = \PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());

            for ($c = 0; $c <= $nColumn; $c++) {
                $val = $sheet->getCellByColumnAndRow($c, $i)->getValue();
                if(strlen($val)>0) array_push($columnsIndex, $val);
            }
        }


        $columns_contract = [];
        $keys = array_keys($columns);


        for ($i = 1; $i <= $sheet->getHighestRow(); $i++) {
            $_temp_columns_contract = [];
            for ($c = 0; $c <= count($columnsIndex); $c++) {

                $Val = $sheet->getCellByColumnAndRow($c, $i)->getValue();

                if(isset($columns['payment_column'])){
                    if(isset($columnsIndex[$c]) && $columns['payment_column'] == $columnsIndex[$c] && $columns['payment_text'] != $Val){
                        $_temp_columns_contract = [];
                        continue;
                    }
                }

                for ($s = 0; $s <= count($keys); $s++) {
                    if(isset($keys[$s]) && isset($columnsIndex[$c]) && $columns[$keys[$s]] == $columnsIndex[$c]){
                        if(strlen($Val)>0){
                            $_temp_columns_contract[$keys[$s]] = $Val;
                        }
                    }
                }
            }

            if(count($_temp_columns_contract)>0) array_push($columns_contract, $_temp_columns_contract);
        }

        //Удаляем файл с сервака
        unlink($path);


        //Создаем договор и рисуем

        $list = [];
        $all_count = count($columns_contract);
        $save_count = 0;
        for ($i = 0; $i < count($columns_contract); $i++) {
            if((int)$this->updateContract($columns_contract[$i], $bso_supplier_id, $product_id, $par) == true){
                $save_count++;
                $list[] = $columns_contract[$i];

            }
        }

        return view('contracts.downloading_contracts.result', [
            'all_count' => $all_count,
            'save_count' => $save_count,
            'list' => $list,
        ]);

    }

    public function uploadFile(Request $request)
    {

        $res = [];
        if ($request->hasFile('file')) {
            $ext = $request->file('file')->getClientOriginalExtension();
            if(in_array($ext, ['xls', 'xlsx', 'docx'])){
                $file = (new FilesRepository)->makeFile($request->file, "temp/downloading_contracts");

                /* Хук для локальной версии */
                $path = storage_path() . '/app/' . ltrim(str_replace(['//', '127.0.0.1'], ['/', ''], $file->path_with_host), '/');

                //$path = storage_path() . '/app/' . $file->getPathAttribute();

                $res['file'] = $path;
                $res['columns'] = $this->readExelFile($path);
            }
        }
        return response()->json($res);
    }

    public function readExelFile($path)
    {

        $xls = \PHPExcel_IOFactory::load($path);

// Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);
// Получаем активный лист
        $sheet = $xls->getActiveSheet();

        $myArr = array();


        for ($i = 0; $i <= 1; $i++) {
            $nColumn = \PHPExcel_Cell::columnIndexFromString(
                $sheet->getHighestColumn());

            for ($c = 0; $c <= $nColumn; $c++) {
                $val = $sheet->getCellByColumnAndRow($c, $i)->getValue();
                if(strlen($val)>0) array_push($myArr, $val);
            }


        }

        return $myArr; //возвращаем массив

    }


    public function updateContract($arr, $bso_supplier_id, $product_id, $par)
    {
        $keys = array_keys($arr);

        $columns_contract_object = (object)$arr;
        if(isset($columns_contract_object->bso_title)){

            if($par == ""){
                $bso_number = preg_replace('/[^0-9]/', '', $columns_contract_object->bso_title);
            }else{
                $bso = explode($par, $columns_contract_object->bso_title);
                $bso_number = $bso[1];
            }


            $contract = Contracts::query()
                ->where('contracts.bso_title', 'like', "%{$bso_number}%")
                ->where('contracts.bso_supplier_id', $bso_supplier_id)
                ->where('contracts.product_id', $product_id)
                ->where(function ($query) {
                $query->where('contracts.statys_id', '=', 1)
                    ->orWhere('contracts.statys_id', '=', 3);
            })->get()->first();

            //dd($keys);

            if($contract && $contract->kind_acceptance != 1){

                $contract->update([
                    'begin_date' => setDateTimeFormat($columns_contract_object->contract_begin),
                    'end_date' => setDateTimeFormat($columns_contract_object->contract_end),
                    'sign_date' => setDateTimeFormat($columns_contract_object->contract_date),
                    'payment_total' => getFloatFormat($columns_contract_object->payment_total),
                ]);

                $insurer = $contract->insurer;
                $insurer->title = $columns_contract_object->insurer_title;
                $insurer->save();

                //$setAccept = 0;
                //dd($columns_contract_object->payment_column);


                return ContractAcceptDownloading::accept($contract, setDateTimeFormat($columns_contract_object->payment_date), getFloatFormat($columns_contract_object->payment_total));


            }

        }


        return false;
    }


}
