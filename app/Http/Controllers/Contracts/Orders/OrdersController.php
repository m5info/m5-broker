<?php

namespace App\Http\Controllers\Contracts\Orders;

use App\Classes\Export\ExportManager;
use App\Classes\Export\TagModels\Finance\TagInvoice;
use App\Domain\Entities\Payments\EPayment;
use App\Domain\Processes\Operations\Bso\TransferBso;
use App\Domain\Processes\Operations\Contracts\ContractAccept;
use App\Domain\Processes\Scenaries\Contracts\Orders\BsoToCourier;
use App\Domain\Processes\Scenaries\Contracts\TempContracts\UpdateTempContract;
use App\Domain\Samplers\Contracts\TabsVisibility;
use App\Http\QueryHandlers\Contracts\Orders\OrderListQueryHandler;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ObjectInsurer;
use App\Models\Contracts\ObjectInsurerAuto;
use App\Models\Contracts\OrderComments;
use App\Models\Contracts\Orders;
use App\Models\Contracts\Subjects;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\Products;
use App\Models\Settings\TemplateCategory;
use App\Models\User;
use App\Repositories\FilesRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    protected $filesRepository;

    public function __construct(FilesRepository $filesRepository)
    {
        $this->filesRepository = $filesRepository;

        $this->middleware('permissions:orders,contract_orders');

        $this->breadcrumbs[] = [
            'label' => 'Заявки на оформление',
            'url' => 'orders/contracts/orders'
        ];
    }

    public function index() {

        $count_arr = Orders::getOrdersCountArr();

        return view('contracts.orders.index', [
            "count_arr" => $count_arr,
        ]);
    }


    public function create() {

        $order = new Orders;

        $order->status_id = 0;
        $order->create_user_id = auth()->id();
        $order->save();

        return redirect("/contracts/orders/edit/{$order->id}");
    }

    public function edit($id, Request $request) {

        $this->breadcrumbs[] = [
            'label' => 'Редактирование',
        ];
        $auth_user = auth()->user();

        $order = Orders::find($id);
        $object = $order->object_insurer;

        $tabs_visibility = TabsVisibility::get_orders_tab_visibility();
        $select_visability = TabsVisibility::get_orders_select_visibility();

        if($order->status_id > 4){
            return redirect(url('orders/contracts/orders'));
        }

        if($tabs_visibility[$order->status_id]['edit']){
            $temp = 'edit';
        }elseif($tabs_visibility[$order->status_id]['view']){
            $temp = 'view';
        }else{
            return redirect(url('orders/contracts/orders'));
        }

        $available_products = Products::where('is_actual', '1')->orderBy('id')->get();
        $products = ($order->products) ? \GuzzleHttp\json_decode($order->products) : null;
        $products = ($products) ? $products->products : $products;

        $products_view = view("contracts.orders.order.partials.products.{$temp}", [
            'available_products' => $available_products,
            'products' => $products,
        ])->render();

        return view("contracts.orders.order.{$temp}", [
            "auth_user" => $auth_user,
            "order" => $order,
            "object" => $object,
            "products_view" => $products_view,
            "select_visability" => $select_visability,
            "breadcrumbs" => $this->breadcrumbs,
        ]);
    }

    public function save($id, Request $request){

        $order = Orders::find($id);
        $data = $request->order[0];

        $order->create_user_id = (isset($data['manager_id'])) ? $data['manager_id'] : $order->create_user_id;
        $order->manager_id = (isset($data['manager_id'])) ? $data['manager_id'] : '';
        $order->urgency = (isset($data['urgency'])) ? 1 : 0;
        $order->without_forming = (isset($data['without_forming'])) ? 1 : 0;
        $order->main_contract_id = (isset($data['main_contract_id'])) ? $data['main_contract_id'] : '';
        $order->is_delivery = (isset($data['delivery']['is_delivery'])) ? 1 : 0;
        $order->delivery_city_id = $data['delivery']['city_id'];
        $order->delivery_metro = $data['delivery']['metro'];
        $order->address = $data['delivery']['address'];
        $order->address_width = $data['delivery']['address_width'];
        $order->address_longitude = $data['delivery']['address_longitude'];
        $order->address_kladr = $data['delivery']['address_kladr'];
        $order->delivery_date = date('Y-m-d', strtotime($data['delivery']['date']));
        $order->delivery_time = date('H:i', strtotime($data['delivery']['time']));
        $order->delivery_comment = $data['delivery']['comment'];
        if(isset($data['formalizator_id'])){
            $order->formalizator_id = $data['formalizator_id'];
        }
        $order->delivery_user_id = (isset($data['delivery']['courier'])) ? $data['delivery']['courier'] : '';
        $order->save();


        if(isset($data['delivery']['courier'])){
            $this->change_agents($id, $data);
        }

        if(isset($data['bso_cart_id']) && $data['bso_cart_id']){
            $order->bso_cart_id = $data['bso_cart_id'];

            // после того как происходит передача, точка продаж у бсо меняется на ту которая закреплена за manager_id
            if($order->bso_cart && $order->bso_cart->bso_act){
                foreach($order->bso_cart->bso_act->get_bsos()->get() as $bso_item){
                    $bso_item->point_sale_id = $bso_item->agent ? $bso_item->agent->point_sale_id : $bso_item->point_sale_id;
                    $bso_item->save();
                }
            }
        }

        if($data['workarea_comment']){ // созраняем сообщения

            OrderComments::create([
                'order_id' => $id,
                'comment' => $data['workarea_comment'],
                'proccesing_user_id' => auth()->id()
            ]);
        }

        $this->change_status($id, $request);

        $order->save();

        if(isset($data['release_order'])){ // договоры в условные акцепты

            foreach($order->contracts as $contract){

                if($contract->bso){
                    $contract->bso->update([
                        'state_id' => 2,
                        'location_id' => 1,
                    ]);
                    BsoLogs::setLogs($contract->bso_id, 2, 1);
                }


                $contract->update([
                    'bso_title' => $contract->bso ? $contract->bso->bso_title : '',
                    'bso_supplier_id' => $contract->bso ? $contract->bso->bso_supplier_id : '',
                    'insurance_companies_id' => $contract->bso ? $contract->bso->insurance_companies_id : '',
                    'statys_id' => 1,
                ]);

//                foreach($contract->all_payments as $payment_data){
//                    $payment_data->update([
//                        'agent_id' => $contract->agent_id,
//                        'bso_id' => $contract->bso_id,
//                        'statys_id' => 0
//                    ]);
//                }

            }

            $order->status_id = 5;
            $order->save();
        }

        return $order;
    }


    public function create_contracts_template($id, Request $request){
        $available_products = Products::where('is_actual', '1')->orderBy('id')->get();

        return view('contracts.orders.order.create_contracts', ['order' => Orders::find($id), 'available_products' => $available_products]);
    }

    public function create_contracts($id, Request $request){

        $order = Orders::find($id);

        if($request->products){

            $products = $request->products;

            $contracts = [];

            foreach($products as $key => $product_id){

                $object_insurer = new ObjectInsurer();
                $object_insurer->type = 1;
                $object_insurer->save();
                $auto = new ObjectInsurerAuto();
                $auto->object_insurer_id = $object_insurer->id;
                $auto->save();

                $insurer = new Subjects();
                $insurer->save();

                $contract = Contracts::create([
                    'created_at' => date('Y-m-d', strtotime(Carbon::now())),
                    'statys_id' => -2,
                    'product_id' => $product_id,
                    'user_id' => $order->proccesing_user_id,
                    'agent_id' => $order->delivery_user_id && $order->delivery_user_id != -1 ? $order->delivery_user_id : $order->manager_id,
                    'manager_id' => $order->manager_id,
                    'sales_condition' => 0,
                    'insurer_id' => $insurer->id,
                    'object_insurer_id' => $object_insurer->id,
                    'order_form_id' => $order->id,
                ]);

                if(count($order->contracts) == 1){ // первый договор - основной
                    $order->main_contract_id = $contract->id;
                    $order->save();
                }

//                EPayment::create_temp($contract);

                $contracts[] = $contract;
            }
        }else{
            $contracts = null;
        }

        return parentReload();
    }


    public function created_contracts_edit($id){

        $contract = Contracts::find($id);

        $order = $contract->order;

        if (!$contract || $contract['statys_id'] == 4) {
            return view('errors.403', ['exception' => 1]);
        }

        $agents = User::getALLUser()->pluck('name', 'id');

        $object_insurer = $contract->object_insurer;

        $this->breadcrumbs[] = [
            'label' => 'Договор №' . $contract->id,
        ];

        $template = 'contracts.orders.order.created_contract.edit';

        $permission = auth()->user()->hasPermission('role_owns','temp_contract_visibility_not_full');

        return view($template, [
            'permission' => $permission,
            'object_insurer' => $object_insurer,
            "contract" => $contract,
            "order" => $order,
            "product" => $contract->product,
            'agents' => $agents,
        ])->with('breadcrumbs', $this->breadcrumbs);
    }


    public function order_contracts_save($contract_id) {

        $result = new \stdClass();
        $result->sate = 1;
        $result->msg = 'Ошибка сохранения договора!';

        if(UpdateTempContract::handle($contract_id, request('contract.0', []))){
            $result->sate = 0;
            $result->msg = '';
        }

        return response()->json($result);
    }


    public function add_pay($contract_id, Request $request) {


        if($contract = Contracts::find($contract_id)){
            EPayment::create_temp($contract);
        }

        return response(200);
    }


    public function change_status($id, Request $request){

        $order = Orders::find($id);
        $data = $request->order[0];

        if($data['status_to_change'] > -1){
            $order->status_id = $data['status_to_change'];
            $order->save();
        }

        return $order;
    }

    public function document($id,  Request $request){

        Orders::findOrFail($id)->documents()->save($this->filesRepository->makeFile($request->file, Orders::FILES_DOC . "/$id/"));

        return response('', 200);
    }


    public function get_tab() {

        $builder = Orders::getOrdersQuery();
        $sks = InsuranceCompanies::query()->whereIn('id', $builder->get()->pluck('insurance_companies_id'))->get();
        $insurers = Subjects::query()->whereIn('id', $builder->get()->pluck('insurer_id'))->get();
        $products = Products::query()->whereIn('id', $builder->get()->pluck('product_id'))->get();
        $agents = User::query()->whereIn('id', $builder->get()->pluck('agent_id'))->get();


        return view('contracts.orders.partials.info_tab', [
            'insurers' => $insurers,
            'products' => $products,
            'agents' => $agents,
            'sks' => $sks,
        ]);
    }

    public function delete($id){
        Orders::destroy($id);
        return back();
    }

    public function get_table(){

        $this->validate(request(), [
            'statys' => 'integer',
            'product' => 'integer',
            'insurer' => 'string',
            'agent' => 'integer',
            'bso_title' => 'string',
            'conclusion_date_from' => 'date',
            'conclusion_date_to' => 'date',
        ]);

        $builder = Orders::getOrdersQuery();
        $orders = (new OrderListQueryHandler($builder))->allowEmpty()->apply();
        $orders->orderBy('urgency', 'desc');
        $orders->orderBy('id', 'desc');
        $tabs_visibility = TabsVisibility::get_orders_tab_visibility();

        return view('contracts.orders.partials.contracts_table', [
            "orders" => $orders->get(),
            "tabs_visibility" => $tabs_visibility,
        ]);

    }


    // При добавлении курьера менять агента в контракте на курьера во всех контрактах
    public function change_agents($id, $data){

        $order = Orders::find($id);

        if($order->contracts->count() > 0){

            BsoToCourier::handle($id, $order->delivery_user_id);


            if(isset($data['courier_going'])){ // при отправке курьера

                $acts_id = TransferBso::handle($order->bso_cart_id);

                foreach($order->contracts as $contract){
                    if($contract->sales_condition == 0){
                        $contract->sales_condition = 2;

                    }

                    $contract->manager_id = $order->manager_id;
                    $contract->save();

                }
            }
        }

        return true;
    }

    // поиск по корзине
    public function get_bso_cart(Request $request)
    {
        $json = \GuzzleHttp\json_decode($request->getContent());
        $count = $json->count;
        $like_query = $json->query;
        $type_bso = $json->type_bso;

        $order = Orders::find($json->order_id);
        $bso_cart = $order->bso_cart;
        $bso_items = $bso_cart->bso_items;

        if(isset($request['product_id']) && (int)$request['product_id'] > 0){
            $bso_items->where('product_id', $request['product_id']);
        }


        if(isset($json->type_bso)){
            if($type_bso == 1){
                $bso_items = collect($bso_items)->filter(function ($item) use ($like_query) {
                    return false !== ($item->bso_class_id != 100);
                });
            }elseif($type_bso == 2){
                $bso_items = collect($bso_items)->filter(function ($item) use ($like_query) {
                    return false !== ($item->bso_class_id == 100);
                });
            }
        }

        $bso_items = collect($bso_items)->filter(function ($item) use ($like_query) {

            return false !== stristr($item->bso_title, $like_query);
        });

        $bso_items->sortBy('state_id');
        $bso_items->slice(0, $count);

        $res = [];
        $res["suggestions"] = [];

        foreach ($bso_items as $bso)
        {
            $data = [];

            $data["value"] = $bso->bso_title;
            $data["unrestricted_value"] = $bso->bso_title;
            $data["data"] = [];
            $data["data"]['bso_type'] = $bso->type->title.' ('.$bso->bso_states->title.')';
            $data["data"]['bso_title'] = $bso->bso_title;
            $data["data"]['bso_sk'] = $bso->supplier->title;
            $data["data"]['agent_name'] = ($bso->user)?$bso->user->name:'';

            $data["data"]['bso_id'] = $bso->id;
            $data["data"]['bso_supplier_id'] = $bso->bso_supplier_id;
            $data["data"]['insurance_companies_id'] = $bso->insurance_companies_id;
            $data["data"]['product_id'] = $bso->product_id;
            $data["data"]['agent_id'] = $bso->user_id;
            $data["data"]['isset_hold_kv'] = $bso->product ? $bso->supplier->hold_kv_product($bso->product->id) ? 1 : 0 : 1;

            $res["suggestions"][] = $data;
        }

        return response()->json($res);
    }

    public function act_export_delivery($id){

        $builder = Orders::query()->where('id', $id);

        $builder->firstOrFail();//просто чтоб фейлил левые запросы

        $category = TemplateCategory::get('delivery_act');

        return (new ExportManager($category, $builder))->handle();
    }
}
