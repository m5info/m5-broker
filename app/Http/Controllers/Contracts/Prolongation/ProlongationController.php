<?php

namespace App\Http\Controllers\Contracts\Prolongation;


use App\Helpers\PaginationHelper;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Contracts\Prolongation\ProlongationQueryHandler;
use App\Models\Contracts\ObjectInsurer;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Contracts\Contracts;

class ProlongationController extends Controller {

    public function __construct() {
        $this->middleware('permissions:contracts,prolongation');
    }

    public function index(Request $request) {

        $model = Contracts::getContractsQuery()->whereIn('statys_id', [4]);
        $count = $model->count();

        $result["limit"] = request()->get('limit') ?? 25;

        $from = 0;
        if ($request['page'] && (int) $request['page']) {
            $from = ((int) $request['page'] - 1) * $result["limit"];
        }
        $model->orderBy('id', 'DESC')->skip($from)->take($result["limit"]);
        $contracts = $model->get();
        $paginator = createPageNavigator($count, $request['page'], $result["limit"], $_SERVER['REQUEST_URI']);

        return view('contracts.prolongation.index', [
            'contracts' => $contracts,
            'count' => $count,
            'paginator' => $paginator,
        ]);
    }

    public function index_custom(Request $request) {

        $model = Contracts::getContractsQuery()->whereIn('statys_id', [4]);
        $count = $model->count();

        $result["limit"] = request()->get('limit') ?? 25;

        $from = 0;
        if ($request['page'] && (int) $request['page']) {
            $from = ((int) $request['page'] - 1) * $result["limit"];
        }
        $model->orderBy('id', 'DESC')->skip($from)->take($result["limit"]);
        $contracts = $model->get();
        $paginator = createPageNavigator($count, $request['page'], $result["limit"], $_SERVER['REQUEST_URI']);

        return view('contracts.prolongation.index_custom', [
            'contracts' => $contracts,
            'count' => $count,
            'paginator' => $paginator,
        ]);
    }



    /* Версия для Гужевой */
    public function get_table_custom(){

        $builder = Contracts::getContractsQuery()->whereIn('statys_id', [4]);

        $contracts = (new ProlongationQueryHandler($builder))->allowEmpty()->apply();

        $contracts = $contracts->get();

        return [
            'html' => view('contracts.partials.prolongation_custom_xls_2',[
                'contracts' => $contracts,

            ])->render(),
        ];
    }

    /* Не актуально (разногласия между Гужевой и Андреем) */
    public function get_table_custom_old(){

        $builder = Contracts::getContractsQuery()->whereIn('statys_id', [4]);

        $contracts = (new ProlongationQueryHandler($builder))->allowEmpty()->apply();

        $contracts = $contracts->get();

        return [
            'html' => view('contracts.partials.prolongation_custom_xls',[
                'contracts' => $contracts,

            ])->render(),
        ];
    }


    public function get_table() {

        $statys_id = request()->get('status') ? (int)request()->get('status') : 4;



        $builder = Contracts::getContractsQuery()
            ->with('manager','errors','logs','insurance_companies','bso','agent','insurer');

        if($statys_id > -1){
            $builder->where('statys_id', $statys_id);
        }

        $contracts = (new ProlongationQueryHandler($builder))->allowEmpty()->apply();

        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;

        $result = PaginationHelper::paginate($contracts, $page, $page_count);

        $contracts->orderBy('begin_date');

        $contracts = $contracts->get();

        return [
            'html' => view('contracts.partials.contracts_table',[
                'contracts' => $contracts,

            ])->render(),
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row']
        ];

    }

    public function contract($contract_id) {
        $contract = Contracts::getContractId($contract_id);
        if (!$contract || $contract->statys_id !== 4) {
            return view('errors.403', ['exception' => 1]);
        }
        $agents = User::getALLUser(24)->pluck('name', 'id');
        $object_insurer = $contract->object_insurer;
        if (!$object_insurer) {
            $object_insurer = new ObjectInsurer();
            if ($contract->bso->product->category->template == 'auto') {
                $object_insurer->type = 1;
            }
        }

        return view('contracts.contract.contract_view', [
            "contract" => $contract,
            'agents' => $agents,
            'object_insurer' => $object_insurer,
        ]);
    }

}
