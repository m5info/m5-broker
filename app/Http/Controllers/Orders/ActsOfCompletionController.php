<?php

namespace App\Http\Controllers\Orders;

use App\Models\File;
use App\Models\Orders\Inspection;
use App\Models\Orders\InspectionOrdersReports;
use App\Models\Orders\InspectionOrdersReportsDocuments;
use App\Models\Organizations\Organization;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use App\Repositories\CustomFilesRepository;

class ActsOfCompletionController extends Controller
{

    protected $filesRepository;

    const DOCUMENTS_PATH = 'acts_of_completion';

    public function __construct(CustomFilesRepository $filesRepository)
    {

        $this->filesRepository = $filesRepository;
        $this->middleware('permissions:orders,acts_of_completion');

        $this->breadcrumbs[] = [
            'label' => 'Акты выполненных работ',
            'url' => 'orders/acts_of_completion'
        ];
    }

    public function index()
    {

        $orgs = Organization::getAllOrg()->where('do_inspection','=','1')->orderBy('title')->get();

        return view('orders.acts_of_completion.index', [
            'orgs' => $orgs,
        ]);
    }

    public function index_table(Request $request)
    {
        $orgs = Organization::getAllOrg()->where('do_inspection','=','1')->orderBy('title');

        $org_id = $request->organization_id;

        if ($org_id && $org_id > -1) {
            $orgs->where('organizations.id', $request->organization_id);
        }

        return view('orders.acts_of_completion.table', [
            'orgs' => $orgs->get(),
        ]);
    }

    public function created_reports($org_id)
    {

        $org = Organization::find($org_id);

        $this->breadcrumbs[] = [
            'label' => 'Созданные отчеты',
        ];

        $this->breadcrumbs[] = [
            'label' => $org->title,
        ];


        return view('orders.acts_of_completion.created_reports.index', [
            'org' => $org,
            'breadcrumbs' => $this->breadcrumbs
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function created_reports_get_table(Request $request, $org_id)
    {

        $inspection_orders = \DB::raw('(SELECT SUM(`financial_policy_kv_agent_total`) FROM `inspection_orders` WHERE `inspection_orders_report_id` = `inspection_orders_reports`.`id`) as `total_sum`');

        $inspection_orders_reports = InspectionOrdersReports::query()
            ->select('inspection_orders_reports.*')
            ->addSelect($inspection_orders)
            ->where('org_id', $org_id)
            ->leftJoin('inspection_orders', 'inspection_orders.id', '=', 'inspection_orders.inspection_orders_report_id');


        $count_inspection_orders_reports = clone $inspection_orders_reports;

        if ($request->report_month){
            $inspection_orders_reports->whereIn('inspection_orders_reports.report_month', $request->report_month);
        }

        if ($request->report_year){
            $inspection_orders_reports->where('inspection_orders_reports.report_year', 'like', $request->report_year);
        }

        if ($request->like_title){
            $inspection_orders_reports->where('inspection_orders_reports.title', 'like', '%'.$request->like_title.'%');
        }

        if ($request->id){
            $inspection_orders_reports->where('inspection_orders_reports.id', $request->id);
        }

        if ($request->accept_status > -1){
            $inspection_orders_reports->whereIn('inspection_orders_reports.accept_status', $request->accept_status);
        }

        $result = [
            'perpage' => 10,
            'page' => 1,
        ];


        $result['count'] = $count_inspection_orders_reports->count();

        if ($request->page_count > 0) {
            $result['perpage'] = $request->page_count;
        }

        if ($request->use_by_button == 1 && $request->use_by_button != $request->used_by_button) {
            $result['page'] = 1;
        } elseif ($request->page > 0) {
            $result['page'] = $request->page;
        }

        $inspection_orders_reports->offset($result['perpage'] * ($result['page'] - 1));
        $inspection_orders_reports->limit($result['perpage']);

        $result['used_by_button'] = $request->use_by_button == 1 ? 1 : 0;

        $result['html'] = view('orders.acts_of_completion.created_reports.table', [
            'inspection_orders_reports' => $inspection_orders_reports->get(),
            'org_id' => $org_id,
            'count_inspection_orders_reports' => $count_inspection_orders_reports->count(),
        ])->render();

        return $result;
    }

    public function reports_create($org_id)
    {

        $org = Organization::findOrFail($org_id);

        $this->breadcrumbs[] = [
            'label' => 'Формирование отчетов',
        ];

        $this->breadcrumbs[] = [
            'label' => $org->title,
        ];

        return view('orders.acts_of_completion.reports_create.index', [
            'org' => $org,

        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function reports_create_get_table(Request $request, $org_id)
    {


        $orgs = Organization::getALLOrg()->where('id', $org_id)
            ->first();

        $inspections = $orgs->getInspectionOrdersAcceptNotReport()
            ->select('inspection_orders.*', 'contracts.sign_date as contracts_sign_date', 'contracts.begin_date as contracts_begin_date')
            ->leftJoin('contracts', 'contracts.inspection_id','=', 'inspection_orders.id')
            ->leftJoin('products', 'products.id', '=', 'contracts.product_id')
            ->leftJoin('users as processing_users', 'inspection_orders.processing_user_id', '=', 'processing_users.id')
            ->leftJoin('users as accept_users', 'inspection_orders.accept_user_id', '=', 'accept_users.id')
            ->leftJoin('bso_suppliers', 'bso_suppliers.id', '=', 'contracts.bso_supplier_id')
            ->leftJoin('organizations', 'organizations.id', '=', 'bso_suppliers.purpose_org_id');



        $count_inspections = $inspections->count();

        if ($request->date_type){

            $date_from = $request->date_from;
            $date_to = $request->date_to;

            switch ($request->date_type):
                case 1:
                    if ($date_from){
                        $inspections->whereDate('contracts.begin_date', '>=', date('Y-m-d H:i:s', strtotime($date_from)));
                    }
                    if ($date_to){
                        if (strlen($date_to) < 11){
                            $date_to .= ' 23:59:59';
                            $inspections = $inspections->whereDate('contracts.begin_date', '<=', date('Y-m-d H:i:s', strtotime($date_to)));
                        }else{
                            $inspections = $inspections->whereDate('contracts.begin_date', '<=', date('Y-m-d H:i:s', strtotime($date_to)));
                        }
                    }
                break;
                case 2:
                    if ($date_from){
                        $inspections->whereDate('contracts.sign_date', '>=', date('Y-m-d H:i:s', strtotime($date_from)));
                    }
                    if ($request->date_to){
                        if (strlen($date_to) < 11){
                            $date_to .= ' 23:59:59';
                            $inspections = $inspections->whereDate('contracts.sign_date', '<=', date('Y-m-d H:i:s', strtotime($date_to)));
                        }else{
                            $inspections = $inspections->whereDate('contracts.sign_date', '<=', date('Y-m-d H:i:s', strtotime($date_to)));
                        }
                    }
                break;
            endswitch;
        }

        if ($request->products){
            $inspections->whereIn('products.id', $request->products);
        }

        if ($request->executor_manager > -1){
            $inspections->where('processing_users.id', '=', $request->executor_manager);
            $inspections->orWhere('accept_users.id', '=', $request->executor_manager);
            $inspections->where('organizations.id', '=', $org_id);
        }

        $result = [
            'perpage' => 10,
            'page' => 1,
        ];

        $result['count'] = $count_inspections;

        if ($request->page_count > 0) {
            $result['perpage'] =$request->page_count;
        }

        if ($request->page > 1){
            $result['page'] = $request->page;
        }

        $inspections->offset($result['perpage'] * ($result['page'] - 1));
        $inspections->limit($result['perpage']);

        $result['used_by_button'] = $request->use_by_button == 1 ? 1 : 0;

        $result['table'] = view('orders.acts_of_completion.reports_create.table', [
            'inspections' => $inspections->get()
        ])->render();

        return $result;
    }

    public function reports_create_get_actions(Request $request, $bso_supplier)
    {
        return view('orders.acts_of_completion.reports_create.actions');
    }

    public function reports_create_execute(Request $request, $bso_supplier)
    {

        try {
            $this->validate($request, [
                'inspection_ids' => 'required|array',
                'report_name' => 'required|string',
                'report_year' => 'required|numeric',
                'report_month' => 'required|numeric',
                'report_date_start' => 'required',
                'report_date_end' => 'required'
            ], [
                'report_name.required' => 'Поле `Название` не может быть пустым',
                'report_date_start.required' => 'Поле `Дата заключения договора с` не может быть пустым',
                'report_date_end.required' => 'Поле `Дата заключения договора по` не может быть пустым',
            ]);
        } catch (ValidationException $e) {
            return ['errors' => $e->errors()];
        }

        $report = new InspectionOrdersReports();
        $report->org_id = $request->org_id;
        $report->create_user_id = Auth::id();
        $report->title = $request->report_name;
        $report->report_year = $request->report_year;
        $report->report_month = $request->report_month;
        $report->accept_status = 0;
        $report->is_deleted = 0;
        $report->report_date_start = $request->report_date_start;
        $report->report_date_end = $request->report_date_end;
        $report->created_at = date('Y-m-d H:i:s');
        $report->save();

        Inspection::query()
            ->whereIn('id', $request->inspection_ids)
            ->update([
                'inspection_orders_report_id' => $report->id
            ]);

        return [
          'id' => $report->id
        ];
    }

    public function inspections_created_report(Request $request, $org_id, $inspection_id)
    {

        $org = Organization::query()->where('id', $org_id)->first();
        $inspection_order = InspectionOrdersReports::query()->where('id', $inspection_id)->first();


        $this->breadcrumbs[] = [
            'label' => $org->title,
            'url' => "{$org_id}/created_reports"
        ];

        $inspections = Inspection::query()->where('inspection_orders_report_id', $inspection_id)->select('inspection_orders.*', 'citys.title as city_title', 'users.name as user_name');

        $inspections
            ->leftJoin('citys', 'inspection_orders.city_id', '=', 'citys.id')
            ->leftJoin('users', 'inspection_orders.accept_user_id', 'users.id')
            ->join('contracts', 'inspection_orders.id', '=', 'contracts.inspection_id')
            ->leftJoin('products', 'contracts.product_id', '=', 'products.id')
            ->leftJoin('bso_suppliers', 'contracts.bso_supplier_id', '=', 'bso_suppliers.id');

        $total_sum = $inspections->sum('financial_policy_kv_agent_total');

        $this->breadcrumbs[] = [
            'label' => '#'.$inspection_order->id.' Название: "'.$inspection_order->title.'", Итого: '.$total_sum,
        ];

        return view('orders.acts_of_completion.created_reports.inspections.index', [
            'org' => $org,
            'inspection_id' => $inspection_id,
            'inspection_order' => $inspection_order,
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function inspections_created_report_get_table(Request $request, $org_id, $inspection_id)
    {

        $inspections = Inspection::query()->where('inspection_orders_report_id', $inspection_id)->select('inspection_orders.*', 'citys.title as city_title', 'users.name as user_name');

        $inspections
            ->leftJoin('citys', 'inspection_orders.city_id', '=', 'citys.id')
            ->leftJoin('users', 'inspection_orders.accept_user_id', 'users.id')
            ->join('contracts', 'inspection_orders.id', '=', 'contracts.inspection_id')
            ->leftJoin('products', 'contracts.product_id', '=', 'products.id')
            ->leftJoin('bso_suppliers', 'contracts.bso_supplier_id', '=', 'bso_suppliers.id');

        $count_inspections = clone $inspections;

        $result = [
            'perpage' => 10,
            'page' => 1,
        ];

        $result['count'] = $count_inspections->count();

        if ($request->page_count > 0) {
            $result['perpage'] = $request->page_count;
        }

        if ($request->page > 1){
            $result['page'] = $request->page;
        }

        $inspections->offset($result['perpage'] * ($result['page'] - 1));
        $inspections->limit($result['perpage']);

        $result['used_by_button'] = $request->use_by_button == 1 ? 1 : 0;


        $result['html'] = view('orders.acts_of_completion.created_reports.inspections.table',[
            'inspections' => $inspections->get(),
        ])->render();

        return $result;
    }

    public function inspections_created_report_update(Request $request, $org_id, $inspection_id)
    {

        $ins = InspectionOrdersReports::query()->where('id', $inspection_id)->first();

        if ($request->title){
            $ins->title = $request->title;
        }

        if ($request->report_month){
            $ins->report_month = $request->report_month;
        }

        if ($request->report_year){
            $ins->report_year = $request->report_year;
        }

        if ($request->report_date_start){
            $ins->report_date_start = date('Y-m-d H:i:s', strtotime($request->report_date_start));
        }

        if ($request->report_date_end){
            $ins->report_date_end = date('Y-m-d H:i:s', strtotime($request->report_date_end));
        }

        if ($request->comment){
            $ins->comment = $request->comment;
        }

        if ($request->accept_status > -1){
            if ($request->accept_status == 2){
                $ins->accept_user_id = Auth::id();
            }
            $ins->accept_status = $request->accept_status;
            $ins->accepted_at = date('Y-m-d H:i:s');
        }

        if ($request->signatory_org){
            $ins->signatory_org = $request->signatory_org;
        }

        if ($request->signatory_sk_bso_supplier){
            $ins->signatory_sk_bso_supplier = $request->signatory_sk_bso_supplier;
        }

        $ins->save();



        return redirect("/orders/acts_of_completion/$org_id/created_reports/$inspection_id");
    }

    public function inspections_created_report_upload_file(Request $request, $org_id, $inspection_id)
    {
        InspectionOrdersReports::findOrFail($inspection_id)->files()->save($this->filesRepository->makeFile($request->file, ActsOfCompletionController::DOCUMENTS_PATH. "/$inspection_id/"));
        return response(200);
    }

    public function inspections_created_report_delete_file(Request $request, $org_id, $inspection_id)
    {

        $contract = InspectionOrdersReports::findOrFail($request->inspection_id);

        if ($contract){

                $doc = InspectionOrdersReportsDocuments::where("report_id", $request->inspection_id)->where("file_id", $request->file_id)->first();
                $file = File::query()->where('id', $doc->file_id)->first();
                $doc->delete();
                app()->make('\App\Http\Controllers\FilesController')
                    ->callAction('destroy', [$file->name]);
        }
    }




}
