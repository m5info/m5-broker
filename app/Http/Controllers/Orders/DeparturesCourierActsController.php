<?php

namespace App\Http\Controllers\Orders;

use App\Domain\Entities\Payments\EPayment;
use App\Domain\Processes\Scenaries\Contracts\TempContracts\UpdateTempContract;
use App\Domain\Processes\Scenaries\Processing\ProcessingEdit;
use App\Domain\Samplers\Contracts\TabsVisibility;
use App\Http\QueryHandlers\Contracts\Orders\OrderListQueryHandler;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Orders;
use App\Models\Contracts\Subjects;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\Products;
use App\Models\Orders\DeparturesCourierActs;
use App\Models\Orders\InspectionOrders;
use App\Models\Orders\ProcessingOrdersLogs;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeparturesCourierActsController extends Controller
{

    public function __construct()
    {
        $this->middleware('permissions:orders,departures_courier_acts');

        $this->breadcrumbs[] = [
            'label' => 'Курьерские акты',
            'url' => 'orders/departures_courier_acts'
        ];
    }

    public function index(Request $request){

        return view('orders.departures_courier_acts.index', [

        ]);
    }


    public function orders(Request $request){

        return view('orders.departures_courier_acts.orders.index', [

        ]);
    }


    public function acts(Request $request){

        return view('orders.departures_courier_acts.acts.index', [

        ]);
    }


    public function set_acts(Request $request){

        if($request->act_id == 0){
            $act = new DeparturesCourierActs();
            $act->title = $request->act_name;
            $act->user_id = auth()->id();
            $act->save();
        }else{
            $act = DeparturesCourierActs::getActId($request->act_id);
        }

        $act->setOrders($request->orders);

        return response('',200);
    }



    public function get_acts(Request $request){

        $acts = DeparturesCourierActs::getActs();
        if(isset($request->status) && is_array($request->status)){
            $acts->whereIn('status_id', $request->status);
        }

        if(isset($request->user) && $request->user > 0){
            $acts->where('user_id', $request->user);
        }
        $page = (int)$request->page;
        if ($page == 0) {
            $page = 1;
        }
        $page_count = isset($request->page_count) && (int)$request->page_count > 0 ? (int)$request->page_count : 25;

        $max_row = (int)$acts->count();
        $page_max = ceil($max_row/$page_count);
        $view_row = ($page_count*($page));
        if($view_row > $max_row) $view_row = $max_row;
        $acts->skip(($page_count*($page-1)))->take(($page_count));

        return response()->json([
            'result' => view("orders.departures_courier_acts.acts.table", [
                "acts" => $acts->get(),
            ])->render(),
            'page_max' => $page_max,
            'page_sel' => $page,
            'max_row' => $max_row,
            'view_row' => $view_row,
        ]);

    }


    public function view_act($id, Request $request)
    {
        $act = DeparturesCourierActs::getActId($id);

        $act_users = \DB::select("
select orders.delivery_user_id,
users.name,
count(orders.id) as count_orders,
sum(orders.departures_courier_price_total) as price_orders
from 
orders 
left join users on users.id = orders.delivery_user_id
where orders.departures_courier_act_id = {$id} group by orders.delivery_user_id");


        return view('orders.departures_courier_acts.acts.act', [
            'act' => $act,
            'act_users' => $act_users
        ]);


    }

    public function save_acts($id, Request $request)
    {
        $act = DeparturesCourierActs::getActId($id);

        $act->title = $request->title;
        $act->comments = $request->comments;
        $act->status_id = (int)$request->status_id;
        $act->save();

        return redirect("/orders/departures_courier_acts/acts/$id");
    }



    public function get_table(Request $request){

        $this->validate(request(), [
            'statys' => 'integer',
            'product' => 'integer',
            'insurer' => 'string',
            'agent' => 'integer',
            'city' => 'integer',
            'bso_title' => 'string',
            'conclusion_date_from' => 'date',
            'conclusion_date_to' => 'date',
        ]);

        $builder = Orders::getOrdersQuery();
        $orders = (new OrderListQueryHandler($builder))->allowEmpty()->apply();
        $orders->orderBy('urgency', 'desc');
        $orders->orderBy('id', 'desc');
        $orders->whereNull('departures_courier_act_id');


        $page = (int)$request->page;
        if ($page == 0) {
            $page = 1;
        }
        $page_count = isset($request->page_count) && (int)$request->page_count > 0 ? (int)$request->page_count : 25;

        $max_row = (int)$orders->count();
        $page_max = ceil($max_row/$page_count);
        $view_row = ($page_count*($page));
        if($view_row > $max_row) $view_row = $max_row;
        $orders->skip(($page_count*($page-1)))->take(($page_count));


        $tabs_visibility = TabsVisibility::get_orders_tab_visibility();

        $temp = 'view';

        if($tabs_visibility[$request->status]['edit']){
            $temp = 'edit';
        }


        return response()->json([
            'result' => view("orders.departures_courier_acts.orders.table", [
                "orders" => $orders->get(),
                "tabs_visibility" => $tabs_visibility,
                "status" => $request->status,
                "city_id" => $request->city,
                'temp' => $temp,
            ])->render(),
            'page_max' => $page_max,
            'page_sel' => $page,
            'max_row' => $max_row,
            'view_row' => $view_row,
        ]);



    }


    public function delete_orders_act($id, Request $request)
    {
        $act = DeparturesCourierActs::getActId($id);
        $order = Orders::whereIn('id', $request->orders)->update([
            'departures_courier_act_id' => null,
        ]);
        $act->refreshPriceTotal();

        return response('', 200);

    }

    public function delete_act($id, Request $request)
    {
        $act = DeparturesCourierActs::getActId($id);
        $order = Orders::where('departures_courier_act_id', $id)->update([
            'departures_courier_act_id' => null,
        ]);
        $act->delete();

        return response('', 200);

    }

}
