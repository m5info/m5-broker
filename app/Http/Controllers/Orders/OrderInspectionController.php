<?php

namespace App\Http\Controllers\Orders;

use App\Domain\Entities\Inspection\Inspection;
use App\Domain\Processes\Operations\Inspection\InspectionChangeStatus;
use App\Domain\Processes\Operations\Inspection\InspectionCreate;
use App\Domain\Processes\Operations\Inspection\InspectionSetExecutor;
use App\Domain\Processes\Operations\Inspection\Temple\InspectionCrashData;
use App\Domain\Processes\Scenaries\Inspection\InspectionAccept;
use \App\Domain\Processes\Scenaries\Inspection\InspectionSave;
use App\Domain\Samplers\Contracts\TabsVisibility;
use App\Http\QueryHandlers\Orders\InspectionOrderListQueryHandler;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsDocuments;
use App\Models\Contracts\Subjects;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\Products;
use App\Models\File;
use App\Models\Orders\InspectionOrders;
use App\Models\Orders\InspectionOrdersLogs;
use App\Models\Organizations\Organization;
use App\Models\Organizations\PointsDepartments;
use App\Models\Settings\City;
use App\Models\User;
use App\Models\Users\Permission;
use App\Models\Vehicle\VehicleMarks;
use App\Repositories\FilesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use mikehaertl\wkhtmlto\Pdf;

class OrderInspectionController extends Controller
{
    protected $filesRepository;

    public function __construct(FilesRepository $filesRepository)
    {
        $this->filesRepository = $filesRepository;

        $this->breadcrumbs[] = [
            'label' => 'Заявки на осмотр',
            'url' => 'orders'
        ];
    }

    public function index(Request $request){
        $this->middleware('permissions:orders,inspections');

        $count_arr = InspectionOrders::getOrdersCountArr();
        $tabs_visibility = TabsVisibility::get_inspection_orders_tab_visibility();

        return view('orders.index', [
            "count_arr" => $count_arr,
            "tabs_visibility" => $tabs_visibility,
        ]);
    }

    public function create() {
        $this->middleware('permissions:orders,inspections');

        $order = InspectionCreate::handle();

        return redirect("/orders/edit/{$order->id}");
    }


    public function edit($id, Request $request) {
        $this->middleware('permissions:orders,inspections');

        $this->breadcrumbs[] = [
            'label' => 'Редактирование',
        ];

        $order = Contracts::find($id);
        $object = $order->object_insurer;

        $tabs_visibility = TabsVisibility::get_inspection_orders_tab_visibility();
        $hold_kv_product = $order->bso_supplier->hold_kv_product($order->product_id);

        if($tabs_visibility[$order->status_order_id]['edit']){
            $temp = 'edit';
        }elseif($tabs_visibility[$order->status_order_id]['view']){
            $temp = 'view';
        }

        $products = \App\Models\Directories\Products::where('is_actual', '=', '1')->where('category_id', '=', '6')->get()->pluck('title', 'id');

        $sks = \App\Models\Directories\InsuranceCompanies::query()
            ->select(['insurance_companies.*'])
            ->leftJoin('bso_suppliers', 'bso_suppliers.insurance_companies_id', '=', 'insurance_companies.id')
            ->leftJoin('hold_kv', 'hold_kv.bso_supplier_id', '=', 'bso_suppliers.id')
            ->leftJoin('products', 'products.id', '=', 'hold_kv.product_id')
            ->where('products.category_id', 6)
            ->where('insurance_companies.is_actual', 1)->get()->pluck('title', 'id');

        $bso_suppliers = \App\Models\Directories\BsoSuppliers::query()
            ->select(['bso_suppliers.*'])
            ->leftJoin('hold_kv', 'hold_kv.bso_supplier_id', '=', 'bso_suppliers.id')
            ->leftJoin('products', 'products.id', '=', 'hold_kv.product_id')
            ->where('bso_suppliers.is_actual', 1)
            ->where('products.category_id', 6)
            ->get()->pluck('title', 'id');


        return view("orders.order.edit", [
            "temp" => $temp,
            "sks" => $sks,
            "bso_suppliers" => $bso_suppliers,
            "tabs_visibility" => $tabs_visibility,
            "order" => $order,
            "hold_kv_product" => $hold_kv_product,
            "object" => $object,
            "products" => $products,
            "breadcrumbs" => $this->breadcrumbs,
        ]);
    }


    public function save($id, Request $request){
        $this->middleware('permissions:orders,inspections');

        $data = json_decode(json_encode($request->order[0]), FALSE);
        $order = InspectionSave::handle($id, (object) $data);

        return $order;
    }

    public function accept($id, Request $request){
        $this->middleware('permissions:orders,inspections');
        return response()->json(InspectionAccept::handle($id));
    }

    public function change_event($id, Request $request){

        InspectionChangeStatus::change_event($id, $request->status_id);

        return response(200);
    }

    public function change_status($id, Request $request){

        InspectionChangeStatus::change_status($id, $request->status_id);

        return response(200);
    }

    public function set_executor($id, Request $request){

        $order = Inspection::get_first($id);

        if(isset($request->order[0]['set']) && (int)$request->order[0]['set']){
            if((int)$request->order[0]['inspection']['position_type_id']){
                InspectionSetExecutor::set_org($id, $request->order[0]['inspection']['processing_org_id']);
            }else{
                InspectionSetExecutor::set_user($id, $request->order[0]['inspection']['processing_user_id']);
            }
        }

        return response(200);
    }

    public function decline_user($id, Request $request){

        $order = Inspection::get_first($id);

        if((int)$order->inspection->position_type_id){
            InspectionChangeStatus::change_event($id, 3, auth()->id());
            InspectionChangeStatus::change_status($id, 5, auth()->id());
        }else{
            if(isset($request->order[0]['decline']) && (int)$request->order[0]['decline']){
                InspectionSetExecutor::decline_order($id, auth()->id());
            }
        }


        return response(200);
    }


    public function processing_user($id, Request $request){

        InspectionSetExecutor::processing_user($id, auth()->id());

        return response(200);
    }


    public function arrived_order($id, Request $request){

        InspectionSetExecutor::arrived_order($id, auth()->id());

        return response(200);
    }

    public function finish_order($id, Request $request){

        InspectionSetExecutor::finish_order($id, auth()->id());

        return response(200);
    }

    public function delete($id, Request $request){
        $this->middleware('permissions:orders,inspections');

        $order = Contracts::find($id);
        $order->delete();

        return response(200);
    }

    public function get_autowashes($id, Request $request){

        if($request->city_id){
            $city = City::find($request->city_id);

            $raw = \DB::raw("( 6371 * 2 * ASIN(SQRT(
                          POWER(SIN((`points_departments`.`geo_lat` - ABS($city->geo_lat)) * PI()/180 / 2), 2) +
                          COS(`points_departments`.`geo_lat` * PI()/180) *
                          COS(ABS($city->geo_lat) * PI()/180)  *
                          POWER(SIN((`points_departments`.`geo_lng` - $city->geo_lon) * PI()/180 / 2), 2)
                        ))
                    ) as distance");

            $distance = 100;

            $orgs = PointsDepartments::query()
                ->select(['points_departments.*', $raw])
                ->addSelect($raw)
                ->leftJoin('organizations', 'points_departments.organization_id', '=', 'organizations.id')
//                ->where('organizations.city_id', '=', $request->city_id)
                ->where('organizations.do_inspection', '=', 1)
                ->orderBy('distance', 'ASC')
                ->having('distance', '<=', $distance)
                ->get();

            $choosen_org = Contracts::find($id);
            $choosen_org = $choosen_org->inspection->org_point_id;

            return ['city' => $city, 'orgs' => $orgs, 'html' => view('orders.order.partials.autowashes', ['choosen_org' => $choosen_org, 'orgs' => $orgs])->render()];
        }

    }

    public function get_people($id, Request $request){

        $order = Inspection::get_first($id);
        $inspection = $order->inspection;

        if($request->city_id){

            // пермишны
            $role_is_dtp = Permission::where('title', '=', 'is_dtp_ride')->first();
            $role_is_pso = Permission::where('title', '=', 'is_pso_ride')->first();

            $raw = \DB::raw("( 6371 * 2 * ASIN(SQRT(
                          POWER(SIN((`users`.`geo_lat` - ABS($inspection->geo_lat)) * PI()/180 / 2), 2) +
                          COS(`users`.`geo_lat` * PI()/180) *
                          COS(ABS($inspection->geo_lat) * PI()/180)  *
                          POWER(SIN((`users`.`geo_lon` - $inspection->geo_lon) * PI()/180 / 2), 2)
                        ))
                    ) as distance");

            $distance = 70;

            $orgs_users = User::query()
                ->select([
                    'users.name as name',
                    'users.id as id' ,
                    'organizations.title as title',
                    'organizations.address as address',
                    'users.geo_lat as fact_address_wth',
                    'users.geo_lon as fact_address_lng',
                    $raw
                ])
                ->addSelect($raw)
                ->leftJoin('organizations', 'users.organization_id', '=', 'organizations.id')
                ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
                ->leftJoin('roles_permissions', 'roles_permissions.role_id', '=', 'roles.id')
                ->whereIn('roles_permissions.permission_id', function ($query){
                    $query->select('id')
                        ->from('permissions')
                        ->whereIn('title', ['is_dtp_ride', 'is_pso_ride']);
                })
                ->where('users.is_work', '=', 1)
                ->groupBy('id')->distinct() // чет без него дубляжи записей
                ->orderBy('distance', 'ASC')
                ->having('distance', '<=', $distance)
                ->get();


            $choosen_user = Contracts::find($id);
            $choosen_user = $choosen_user->inspection->processing_user_id;

            return ['orgs' => $orgs_users, 'mission_point' => $inspection, 'html' => view('orders.order.partials.people', ['choosen_user' => $choosen_user, 'users' => $orgs_users])->render()];
        }

    }


    public function get_moving_logs($id, Request $request){

        $order = Inspection::get_first($id);
        $inspection = $order->inspection;
        $last_log = $order->last_inspection_log();

        $moving_logs = InspectionOrdersLogs::query();
        $moving_logs = $moving_logs
            ->select([
                'inspection_orders_logs.geo_lat as geo_lat',
                'inspection_orders_logs.geo_lon as geo_lon',
                'inspection_orders_logs.new_status as status',
                'inspection_orders_logs.id as id',
                'users.name as name',
            ])
            ->leftJoin('users', 'users.id', '=', 'inspection_orders_logs.executor_user_id')
            ->where('inspection_orders_logs.inspection_order_id', '=', $id)
            ->where('inspection_orders_logs.geo_lon', '>', 0)
            ->where('inspection_orders_logs.executor_user_id', '=', $last_log->executor_user_id)
            ->get();

        return ['moving_logs' => $moving_logs, 'mission_point' => $inspection];
    }


    public function get_executor_detail($id, Request $request){

        $executor = null;

        if($request->position_type){
            $executor = PointsDepartments::query()
                ->select([
                    'points_departments.title as pd_title',
                    'organizations.title as org_title',
                    'organizations.inn as inn',
                    'organizations.kpp as kpp',
                    'organizations.fact_address as fact_address',
                    'organizations.address as address',
                    'organizations.general_manager as general_manager',
                    'organizations.user_contact_title as user_contact_title',
                    'organizations.phone as phone',
                    'organizations.email as email',
                ])
                ->leftJoin('organizations', 'points_departments.organization_id', '=', 'organizations.id')
                ->where('points_departments.id', '=', $request->executor_id)->first();
        }else{
            $executor = User::find($request->executor_id);
        }

        return ['html' => view('orders.order.partials.executor_detail_info', ['executor' => $executor, 'position_type' => $request->position_type])->render()];
    }

    public function get_tab() {

        $builder = InspectionOrders::getOrdersQuery();
        $sks = InsuranceCompanies::query()->whereIn('id', $builder->get()->pluck('insurance_companies_id'))->get();
        $insurers = Subjects::query()->whereIn('id', $builder->get()->pluck('insurer_id'))->get();
        $products = Products::query()->whereIn('id', $builder->get()->pluck('product_id'))->get();
        $agents = User::query()->whereIn('id', $builder->get()->pluck('agent_id'))->get();


        return view('orders.partials.info_tab', [
            'insurers' => $insurers,
            'products' => $products,
            'agents' => $agents,
            'sks' => $sks,
        ]);
    }

    public function get_mobile_act($token_id, Request $request){

        $inspection = \App\Models\Orders\Inspection::where('token_act', '=', $token_id)->first();

        return $inspection->export_act();
    }





//    public function export_act($id, Request $request){
//
//        $templ_data = InspectionCrashData::get_data($id);
//
//        //        return view('orders.order.partials.template_crash', ["data" => $templ_data]);
//
//
//        Storage::put('test.html',
//            view('orders.order.partials.template_crash')
//                ->with(["data" => $templ_data])
//                ->render()
//        );
//
//        $uniqid = uniqid();
//
//        $path = '/' . Contracts::FILES_DOC . '/' . $id . '/' . $uniqid.".pdf";
//
//        $tmpHtmlFile = storage_path('app/test.html' );
//        $tmpPdfFile = storage_path('app'.$path);
//
//
//        $options = array(
//            //            'no-outline',
//            'margin-top'    => 0,
//            'margin-right'  => 0,
//            'margin-bottom' => 0,
//            'margin-left'   => 0,
//            //            'disable-smart-shrinking',
//            'background',
//            //            'no-collate',
//            'images',
//            //            'enable-smart-shrinking',
//            //            'title "Fechamento de proposta"',
//            'page-size' => 'A4',
//            'page-height' => '267mm',
//            'page-width' => '200mm',
//            'margin-bottom' => '5mm',
//            'margin-left' => '5mm',
//            'margin-right' => '5mm',
//            'margin-top' => '30mm',
//            'header-line',
//            'footer-font-size' => '8',
//            'footer-spacing' => '-5',
//            'footer-line',
//            'print-media-type',
//        );
//
//          mikehaertl\wkhtmlto\Pdf
//        $pdf = new Pdf($options);
//
//        $pdf->addPage($tmpHtmlFile);
//
//        if (!$pdf->saveAs($tmpPdfFile)) {
//            dd($pdf->getError());
//        }
//
//        $comand = "/usr/local/bin/wkhtmltopdf.sh --margin-top 0mm --margin-bottom 0mm --margin-left 300mm --margin-right 0mm  ".$tmpHtmlFile." $tmpPdfFile";
//
//        exec($comand);
//
//        $pathToFile = storage_path('app' . $path);
//
//        return response()->download($pathToFile);
//    }




    public function get_table(){

        $this->validate(request(), [
            'statys' => 'integer',
            'product' => 'integer',
            'insurer' => 'string',
            'agent' => 'integer',
            'bso_title' => 'string',
            'conclusion_date_from' => 'date',
            'conclusion_date_to' => 'date',
        ]);

        $builder = InspectionOrders::getOrdersQuery();
        $orders = (new InspectionOrderListQueryHandler($builder))->allowEmpty()->apply();
        $tabs_visibility = TabsVisibility::get_inspection_orders_tab_visibility();

        $orders->orderBy('begin_date', 'desc');

        return view('orders.partials.contracts_table', [
            "orders" => $orders->get(),
            "tabs_visibility" => $tabs_visibility,
        ]);

    }
}
