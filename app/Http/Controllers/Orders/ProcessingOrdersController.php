<?php

namespace App\Http\Controllers\Orders;

use App\Domain\Entities\Payments\EPayment;
use App\Domain\Processes\Scenaries\Contracts\TempContracts\UpdateTempContract;
use App\Domain\Processes\Scenaries\Processing\ProcessingEdit;
use App\Domain\Samplers\Contracts\TabsVisibility;
use App\Http\QueryHandlers\Contracts\Orders\OrderListQueryHandler;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Orders;
use App\Models\Contracts\Subjects;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\Products;
use App\Models\Orders\InspectionOrders;
use App\Models\Orders\ProcessingOrdersLogs;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProcessingOrdersController extends Controller
{

    public function __construct()
    {
        $this->breadcrumbs[] = [
            'label' => 'Заявки оформление',
            'url' => 'orders/processing'
        ];
    }

    public function index(Request $request){
        $this->middleware('permissions:orders,contract_orders');


        $count_arr = Orders::getOrdersCountArr();
        $tabs_visibility = TabsVisibility::get_orders_tab_visibility();

        return view('orders.processing.index', [
            "count_arr" => $count_arr,
            "tabs_visibility" => $tabs_visibility,
        ]);
    }

    public function bsoSupplierProduct($id, Request $request)
    {
        if($id == 0) $contract = new Contracts();
        else $contract = Contracts::getContractId($id);


        return view('orders.processing.bso_supplier_product', [
            "contract" => $contract,
            'id' => $id,
            'bso_suppliers' => BsoSuppliers::query()->get()
        ]);
    }



    public function create(Request $request)
    {
        if((int)$request->id == 0){
            $order = ProcessingEdit::create();
            $request->id = $order->main_contract_id;
            ProcessingOrdersLogs::setLogs($order->main_contract_id, 'Создана');
        }

        $order = ProcessingEdit::updateContractBsoSupplierProduct($request->id, $request);
        return parentRedirect("/orders/processing/contract/{$order->main_contract_id}");
    }

    public function contract($id, Request $request)
    {

        $this->middleware('permissions:orders,contract_orders');

        $contract = Contracts::getContractId($id);
        $tabs_visibility = TabsVisibility::get_orders_tab_visibility();


        $count_arr = Orders::getOrdersCountArr();

        if(!isset($count_arr[$contract->order->status_id])){
            return redirect("/orders/processing/");
        }



        if($tabs_visibility[$contract->order->status_id]['edit']){
            $temp = 'edit';
        }elseif($tabs_visibility[$contract->order->status_id]['view']){
            $temp = 'view';
        }





        if($contract->order->status_id >= 3){

            $this->breadcrumbs[] = [
                'label' => 'Заявка №' . $contract->order->id,
                'url' => "order/{$contract->order->id}"
            ];

            $this->breadcrumbs[] = [
                'label' => Orders::STATUSES[$contract->order->status_id],
            ];
        }else{
            $this->breadcrumbs[] = [
                'label' => 'Заявка №' . $contract->order->id. ' - '.Orders::STATUSES[$contract->order->status_id],
            ];
        }


        return view('orders.processing.contract', [
            "temp" => $temp,
            'id' => $id,
            "contract" => $contract,
            'product' => $contract->product,
            "permission" => auth()->user()->hasPermission('role_owns', 'temp_contract_visibility_not_full'),
            "breadcrumbs" => $this->breadcrumbs,
        ]);
    }

    public function save($id, Request $request)
    {
        $result = new \stdClass();


        $result->state = 0;
        $result->msg = '';


        if(isset($request->contract) && isset($request->contract[0])){
            if(!UpdateTempContract::handle($id, request('contract.0', []))){
                $result->state = 1;
                $result->msg = 'Ошибка сохранения договора!';
            }
        }

        if(isset($request->order) && isset($request->order[0])){
            $contract = Contracts::getContractId($id);
            ProcessingEdit::updateOrders($contract->order_form_id, $request->order[0]);
        }



        return response()->json($result);
    }


    public function status_to_change($id, Request $request)
    {
        $result = new \stdClass();
        $result->state = 1;
        $result->msg = [];

        $contract = Contracts::getContractId($id);

        ProcessingEdit::statusChangeOrders($contract->order_form_id, $request->status);

        if($contract->order){

            if(!isset($contract->sign_date)){
                $result->msg[] = 'Заполните дату заключения';
            }

            if(!isset($contract->begin_date)){
                $result->msg[] = 'Заполните период действия договора от';
            }

            if(!isset($contract->end_date)){
                $result->msg[] = 'Заполните период действия договора до';
            }

            if(!isset($contract->payment_total) || (int)$contract->payment_total == 0){
                $result->msg[] = 'Заполните премию по договору';
            }

            if($contract->insurer){
                if(!(isset($contract->insurer->title) && strlen($contract->insurer->title) > 0)){
                    $result->msg[] = 'Заполните ФИО страхователя';
                }
                //if(!(isset($contract->insurer->phone) && strlen($contract->insurer->phone) > 0)){
                //    $result->msg[] = 'Заполните телефон страхователя';
               //}
            }

            if(isset($contract->order->is_delivery) && $contract->order->is_delivery == 1){

                if(!isset($contract->order->delivery_date) || ($contract->order->delivery_date == "" || $contract->order->delivery_date == "1970-01-01")){
                    $result->msg[] = 'Заполните дату доставки';
                }

                if(!isset($contract->order->delivery_time) || $contract->order->delivery_time == ""){
                    $result->msg[] = 'Заполните время доставки';
                }

                if(!isset($contract->order->address) || $contract->order->address == ""){
                    $result->msg[] = 'Заполните адрес доставки';
                }
            }


            if(count($result->msg) > 0){
                $result->state = 0;
            }
        }

        return response()->json($result);
    }

    public function delete_order($id, Request $request)
    {
        $result = new \stdClass();
        $result->state = 1;
        $result->msg = 'Ошибка';

        $contract = Contracts::getContractId($id);
        if(ProcessingEdit::deleteOrders($contract->order_form_id)){
            $result->state = 0;
            $result->msg = '';
        }

        return response()->json($result);
    }





    public function saveBsoCart($order_id, Request $request)
    {
        $order = Orders::find($order_id);

        $order->bso_cart_id = $request->bso_cart_id;
        $order->save();
        // после того как происходит передача, точка продаж у бсо меняется на ту которая закреплена за manager_id
        if($order->bso_cart && $order->bso_cart->bso_act){
            foreach($order->bso_cart->bso_act->get_bsos()->get() as $bso_item){
                $bso_item->point_sale_id = $bso_item->agent ? $bso_item->agent->point_sale_id : $bso_item->point_sale_id;
                $bso_item->save();
            }
        }

        ProcessingOrdersLogs::setLogs($order->main_contract_id, 'Резерв БСО');

        return response('', 200);

    }


    public function get_html_block($id, Request $request)
    {
        $contract = Contracts::getContractId($id);
        $tabs_visibility = TabsVisibility::get_orders_tab_visibility();

        if($tabs_visibility[$contract->order->status_id]['edit']){
            $temp = 'edit';
        }elseif($tabs_visibility[$contract->order->status_id]['view']){
            $temp = 'view';
        }


        return view("orders.processing.contract.{$temp}.{$request->view}", [
            "contract" => $contract,
            "permission" => auth()->user()->hasPermission('role_owns', 'temp_contract_visibility_not_full'),
        ]);
    }


    public function get_table(Request $request){

        $this->validate(request(), [
            'statys' => 'integer',
            'product' => 'integer',
            'insurer' => 'string',
            'agent' => 'integer',
            'city' => 'integer',
            'bso_title' => 'string',
            'conclusion_date_from' => 'date',
            'conclusion_date_to' => 'date',
        ]);

        $builder = Orders::getOrdersQuery();
        $orders = (new OrderListQueryHandler($builder))->allowEmpty()->apply();
        $orders->orderBy('urgency', 'desc');
        $orders->orderBy('id', 'desc');

        $page = (int)$request->page;
        if ($page == 0) {
            $page = 1;
        }
        $page_count = isset($request->page_count) && (int)$request->page_count > 0 ? (int)$request->page_count : 25;

        $max_row = (int)$orders->count();
        $page_max = ceil($max_row/$page_count);
        $view_row = ($page_count*($page));
        if($view_row > $max_row) $view_row = $max_row;
        $orders->skip(($page_count*($page-1)))->take(($page_count));


        $tabs_visibility = TabsVisibility::get_orders_tab_visibility();

        $temp = 'view';

        if($tabs_visibility[$request->status]['edit']){
            $temp = 'edit';
        }


        return response()->json([
            'result' => view("orders.processing.orders_view", [
                "orders" => $orders->get(),
                "tabs_visibility" => $tabs_visibility,
                "status" => $request->status,
                "city_id" => $request->city,
                'temp' => $temp,
            ])->render(),
            'page_max' => $page_max,
            'page_sel' => $page,
            'max_row' => $max_row,
            'view_row' => $view_row,
        ]);



    }


    public function combine(Request $request){
        $orders_id = \GuzzleHttp\json_decode($request->orders);
        $orders = Orders::getOrdersQuery()->whereIn('id', $orders_id)->get();

        return view('orders.processing.partials.combine', [
            "orders" => $orders,
            "orders_arr" => $request->orders,
        ]);
    }

    public function set_combine(Request $request){
        $orders_id = \GuzzleHttp\json_decode($request->orders_arr);
        $orders = Orders::getOrdersQuery()->whereIn('id', $orders_id)->get();
        $orders_active = Orders::getOrdersId($request->active);
        ProcessingEdit::combineOrders($orders_active, $orders);

        return parentReloadTab();
    }


    public function assign(Request $request){
        $orders_id = \GuzzleHttp\json_decode($request->orders);
        $orders = Orders::getOrdersQuery()->whereIn('id', $orders_id)->get();

        return view('orders.processing.partials.assign', [
            "orders" => $orders,
            "orders_arr" => $request->orders,
        ]);
    }


    public function set_assign(Request $request){
        $orders_id = \GuzzleHttp\json_decode($request->orders_arr);
        $orders = Orders::getOrdersQuery()->whereIn('id', $orders_id)->get();

        ProcessingEdit::assignOrders($orders, $request->courier, $request->departures_courier_price_id);

        return parentReloadTab();
    }

    public function order($id, Request $request)
    {
        $this->middleware('permissions:orders,contract_orders');
        $order = Orders::find($id);
        $tabs_visibility = TabsVisibility::get_orders_tab_visibility();

        if($tabs_visibility[$order->status_id]['edit']){
            $temp = 'edit';
        }elseif($tabs_visibility[$order->status_id]['view']){
            $temp = 'view';
        }

        $this->breadcrumbs[] = [
            'label' => 'Заявка №' . $order->id. ' - '.Orders::STATUSES[$order->status_id],
        ];

        return view('orders.processing.order', [
            "temp" => $temp,
            'id' => $id,
            'order' => $order,
            "permission" => auth()->user()->hasPermission('role_owns', 'temp_contract_visibility_not_full'),
            "breadcrumbs" => $this->breadcrumbs,
        ]);


    }

    public function save_order($id, Request $request)
    {
        $result = new \stdClass();
        $result->state = 1;
        $result->msg = 'Ошибка';

        if(isset($request->order) && isset($request->order[0])){
            ProcessingEdit::updateDeliveryOrders($id, $request->order[0]);
            $result->state = 0;
            $result->msg = '';
        }



        return response()->json($result);

    }


    public function close_order($id, Request $request)
    {
        $result = new \stdClass();
        $result->state = 2;
        $result->msg = 'Ошибка';

        if(isset($request->order) && isset($request->order[0])){
            ProcessingEdit::updateDeliveryOrders($id, $request->order[0]);

        }

        $close = 0;
        $order = Orders::find($id);

        if($request->contract_id > 0){

            $contract = Contracts::getContractId($id);

            if(!$contract->bso)
            {
                $result->state = 2;
                $result->msg = 'Нельзя отправить на проверку не указав БСО!';
            }else{
                ProcessingEdit::sendContractToCheck($request->contract_id);
                $result->state = 1;
                $result->msg = '';

            }



        }
        if($request->contract_id == 0){

            foreach($order->contracts as $contract){
                if(!$contract->bso)
                {
                    $result->state = 2;
                    $result->msg = 'Нельзя отправить на проверку не указав БСО!';
                }else{
                    ProcessingEdit::sendContractToCheck($contract->id);
                }
            }

        }

        if($order->contracts()->where('statys_id', -2)->count() == 0){
            $close = 1;
        }

        if($request->contract_id == -1){
            $close = 1;
        }

        if($close == 1){
            ProcessingEdit::statusChangeOrders($id, 5);
            $result->state = 0;
            $result->msg = '';
        }


        return response()->json($result);
    }


    public function clone_contract($id, Request $request)
    {
        $result = new \stdClass();
        $result->state = 2;
        $result->msg = 'Ошибка';
        $defaultContract = Contracts::getContractId($id);

        $order = $defaultContract->order->replicate();
        $order->status_id = 0;
        $order->create_user_id = auth()->id();
        $order->agent_id = auth()->id();
        $order->manager_id = auth()->id();
        $order->delivery_user_id = 0;
        $order->delivery_city_id = auth()->user()->city_id;
        $order->bso_cart_id = null;
        $order->save();


        $contract = $defaultContract->replicate();
        $contract->order_form_id = $order->id;
        $contract->save();
        EPayment::create_temp($contract);

        $contract->scans()->attach($defaultContract->scans);


        $order->main_contract_id = $contract->id;
        $order->save();

        $result->state = 0;
        $result->msg = url("/orders/processing/contract/{$contract->id}");


        return response()->json($result);
    }







}
