<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest', ['except' => 'logout']);
    }

	protected function sendLoginResponse(Request $request)
	{

        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

//        if(!$this->authenticated($request, $this->guard()->user()) && auth()->user()->status_user_id == 0){
//            return redirect()->intended($this->redirectPath())->with('login-success', true);
//        }

        if($this->guard()->user()){
            return redirect()->intended($this->redirectPath())->with('login-success', true);
        }

        auth()->logout();

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                $this->username() => "Ошибка авторизации",
            ]);
	}
	public function logout(Request $request)
    {
        auth()->logout();
        return redirect('/');
    }
}
