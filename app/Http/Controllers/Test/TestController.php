<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use SoapClient;
use SoapHeader;
use SoapVar;

class TestController extends Controller
{

    public function test($id){
        if(method_exists($this, "test_{$id}")){
            return $this->{"test_{$id}"}();
        }
        dd('теста нет');
    }

    public function test_1(){





        $service_partners_interaction = $this->get_service_client('PartnersInteraction');
        $get_upid_response = $service_partners_interaction->getUPID(['callerCode' => "1"]);



        $load_vehicle_params = [
            "TSRequestValue" => [
                "TSInfoRequest" => [
                    "CountryCar" => "1",
                    "CarIdent" => [
                        "LicensePlate" => "Т639АА96",
                        "VIN" => "TMBKK61ZXB2172238"
                    ],
                    "Mark" => "SKODA",
                    "Model" => "OCTAVIA",
                    "YearIssue" => "2011",
                    "DocumentCar" => "31",
                    "DocCarSerial" => "66ХВ",
                    "DocCarNumber" => "168754",
                    "DocumentCarDate" => "2011-07-15",
                    "EngCap" => "152"
                ],
                "DateRequest" => "2016-12-19T18:00:00"
            ],
        ];


        $service_eosago_relaunch = $this->get_service_client('EOsagoRelaunch');
        $load_vehicle_response = $service_eosago_relaunch->loadVehicle($load_vehicle_params);
        /*
            {#1037 ▼
              +"TSResponseValue": {#1038 ▼
                +"ISTSFound": true
                +"ISTSChecked": true
              }
              +"IDCheckTS": 2441253131
              +"ErrorList": {#1039 ▼
                +"ErrorInfo": {#1040 ▼
                  +"Code": 3
                  +"Message": "Обработан успешно"
                }
              }
            }
        */


        $load_insurer_owner_params = [
            "InsurerOwnerRequestValue" => [
                "CheckedPerson" => "Owner",
                "PhysicalPersonInfoRequest" => [
                    "Country" => "643",
                    "Zip" => "309201",
                    "State" => "Свердловская обл",
                    "City" => "Екатеринбург",
                    "Surname" => "Манаков",
                    "Name" => "Денис",
                    "Patronymic" => "Иванович",
                    "BirthDate" => "1979-10-04",
                    "PersonDocument" => [
                        "DocPerson" => "12",
                        "Serial" => "6502",
                        "Number" => "294960"
                    ]
                ],
                "DateRequest" => "2016-09-13T10:31:42"
            ],
        ];

        $service_eosago_relaunch = $this->get_service_client('EOsagoRelaunch');
        $load_insurer_owner_response = $service_eosago_relaunch->loadInsurerOwner($load_insurer_owner_params);
        /*
            {#1041 ▼
              +"InsurerOwnerResponseValue": {#1042 ▼
                +"ISInsurerOwnerFound": true
                +"ISInsurerOwnerChecked": true
              }
              +"IDCheckInsurerOwner": 2441227933
              +"ErrorList": {#1043 ▼
                +"ErrorInfo": {#1044 ▼
                  +"Code": 3
                  +"Message": "Обработан успешно"
                }
              }
            }
         */


        $load_driver_params = [
            "DriverRequestValue" => [
                "DriverInfoRequest" => [
                    "Surname" => "Мохнатов",
                    "Name" => "Пётр",
                    "Patronymic" => "Иванович",
                    "BirthDate" => "1980-01-01",
                    "DriverDocument" => [
                        "Serial" => "77ММ",
                        "Number" => "123456"
                    ],
                    "DriverDocDate" => "2000-01-01"
                ],
                "DateRequest" => "2017-01-13T18:00:00"
            ],
        ];

        $service_eosago_relaunch = $this->get_service_client('EOsagoRelaunch');
        $load_driver_response = $service_eosago_relaunch->loadDriver($load_driver_params);
        /*
            {#1037 ▼
              +"DriverResponseValue": {#1038 ▼
                +"ISDriverFound": false
                +"ISDriverChecked": false
              }
              +"IDCheckDriver": 2441270265
              +"ErrorList": {#1039 ▼
                +"ErrorInfo": {#1040 ▼
                  +"Code": 3
                  +"Message": "Обработан успешно"
                }
              }
            }
        */


        $calc_eosago_param = [
            "PartnerInfo" => [
                "UPID" => $get_upid_response->UPID//"f7c28e39-6121-48c5-9115-3c5008456ddf"
            ],
            "ContractType" => "Первоначальный",
            "PolicyBeginDate" => "2020-01-29T00:00:00",
            "InsuranceMonth" => "12",
            "RegionOfUse" => "Ямало-Ненецкий",
            "CityOfUse" => "Салехард",
            "DriversRestriction" => true,
            "FollowToRegistration" => false,
            "Transport" => [
                "Mark" => "KIA",
                "Model" => "RIO",
                "Category" => "B - легковые",
                "Type" => "Легковой а/м",
                "RegistrationRussia" => true,
                "Power" => "123",
                "Purpose" => "Личные",
                "LicensePlate" => "И434АВ161",
                "VIN" => "Z94CC41BBGR298840",
                "WithUseTrailer" => false,
                "YearIssue" => "2012"
            ],
            "TransportOwner" => [
                "LastName" => "Смирнов",
                "FirstName" => "Алексей",
                "MiddleName" => "Викторович",
                "BirthDate" => "1986-01-22",
                "PersonDocument" => [
                    "Number" => "089199",
                    "Seria" => "4509"
                ]
            ],
            "Drivers" => [
                "Driver" => [
                    [
                        "LastName" => "Смирнов",
                        "FirstName" => "Алексей",
                        "MiddleName" => "Викторович",
                        "BirthDate" => "1986-01-22",
                        "ExperienceDate" => "2006-01-01",
                        "DriverDocument" => [
                            "Number" => "023887",
                            "Seria" => "77УЕ"
                        ]
                    ],
                    [
                        "LastName" => "Денисенко",
                        "FirstName" => "Ирина",
                        "MiddleName" => "Михайловна",
                        "BirthDate" => "1986-12-29",
                        "ExperienceDate" => "2006-01-01",
                        "DriverDocument" => [
                            "Number" => "120493",
                            "Seria" => "77ОС"
                        ]
                    ]
                ]
            ],
            "UnwantedClient" => false
        ];





//        dd($service_eosago_relaunch->__getFunctions());

        $service_eosago_calc_relaunch = $this->get_service_client('OsagoCalcRelaunch');
        $calc_response = $service_eosago_calc_relaunch->CalcEOsago($calc_eosago_param);
        /*
            {#1037 ▼
              +"RefCalcId": "46620D57712862098666E8C8CA7B075C91E84752"
              +"IdRequestCalc": 7491173848
              +"commentList": "Произошел сбой при расчете премии по заданным параметрам, обратитесь к андеррайтерам по ОСАГО"
              +"operationPeriodList": {#1038 ▼
                +"endDate": "2021-01-28T00:00:00"
                +"startDate": "2020-01-29T00:00:00"
              }
              +"contractDate": "2020-01-29T00:00:00"
              +"Drivers": {#1039 ▼
                +"Driver": array:2 [▼
                  0 => {#1040 ▼
                    +"PersonName": "Смирнов Алексей Викторович"
                    +"LastName": "Смирнов"
                    +"FirstName": "Алексей"
                    +"MiddleName": "Викторович"
                    +"BirthDate": "1986-01-22"
                    +"DriverClass": "8"
                    +"KMBValue": 0.75
                  }
                  1 => {#1041 ▼
                    +"PersonName": "Денисенко Ирина Михайловна"
                    +"LastName": "Денисенко"
                    +"FirstName": "Ирина"
                    +"MiddleName": "Михайловна"
                    +"BirthDate": "1986-12-29"
                    +"DriverClass": "10"
                    +"KMBValue": 0.65
                  }
                ]
              }
              +"PolicyKBM": "8"
              +"InsuranceBonus": 0.0
              +"Coefficients": {#1042 ▼
                +"Tb": 4242.0
                +"Kt": 1.1
                +"Kbm": 0.75
                +"Kvs": 0.96
                +"Ks": 1.0
                +"Kp": 1.0
                +"Km": 1.4
                +"Kn": 1.0
                +"Ko": 1.0
                +"Kpr": 1.0
              }
              +"RsaPersonMessageList": {#1043 ▼
                +"RsaPersonMessage": {#1044 ▼
                  +"Message": "Обработан успешно"
                  +"PersonName": "Денисенко Ирина Михайловна"
                }
              }
              +"RsaCarMessageList": {#1045 ▼
                +"RsaCarMessage": {#1046 ▼
                  +"Message": "Запрос в ЕАИС ТО временно отключен из-за проблем на стороне МВД"
                }
              }
            }
        */




        $load_contract_eosago_params = [
            "PartnerInfo" => [
                "UPID" => $get_upid_response->UPID
            ],
            "LoadContractRequest" => [
                "CalcRef" => $calc_response->RefCalcId,
                "DateRevision" => "2020-09-13T18:20:00",
                "DateActionBeg" => "2020-09-11T00:00:00",
                "DateActionEnd" => "2021-09-10T00:00:00",
                "DriversRestriction" => true,
                "IsTransCar" => false,
                "IsTrailerAllowed" => "0",
                "Car" => [
                    "IDCheckTS" => $load_vehicle_response->IDCheckTS,
                    "CountryCar" => "1",
                    "CarIdent" => [
                        "VIN" => "XWKFB227270057729"
                    ],
                    "LicensePlate" => "О750СН86",
                    "Mark" => "KIA",
                    "Model" => "SPECTRA",
                    "YearIssue" => "2007",
                    "DocumentCar" => "30",
                    "DocCarSerial" => "8627",
                    "DocCarNumber" => "139071",
                    "DocumentCarDate" => "2014-12-18",
                    "TicketCar" => "53",
                    "TicketCarNumber" => "019849991501584",
                    "TicketCarYear" => "2017",
                    "TicketCarMonth" => "1",
                    "EngCap" => "101",
                    "Purpose" => "Личные"
                ],
                "Insurer" => [
                    "PhysicalPerson" => [
                        "IDCheckInsurerOwner" => $load_insurer_owner_response->IDCheckInsurerOwner,
                        "Surname" => "Тастанов",
                        "Name" => "Азимхан",
                        "Patronymic" => "Серикович",
                        "BirthDate" => "1991-11-24",
                        "Phone" => "+7 912 417-48-01",
                        "Email" => "tastan-15.45@mail.ru",
                        "PersonDocument" => [
                            "DocPerson" => "12",
                            "Serial" => "3711",
                            "Number" => "456809"
                        ],
                        "CountryCode" => "643",
                        "CountryName" => "Россия",
                        "Zip" => "628461",
                        "State" => "Ханты-Мансийский Автономный округ - Югра",
                        "City" => "Радужный"
                    ]
                ],
                "Owner" => [
                    "PhysicalPerson" => [
                        "IDCheckInsurerOwner" => $load_insurer_owner_response->IDCheckInsurerOwner,
                        "Surname" => "Тастанов",
                        "Name" => "Азимхан",
                        "Patronymic" => "Серикович",
                        "BirthDate" => "1991-11-24",
                        "Phone" => "+7 912 417-48-01",
                        "Email" => "tastan-15.45@mail.ru",
                        "PersonDocument" => [
                            "DocPerson" => "12",
                            "Serial" => "3711",
                            "Number" => "456809"
                        ],
                        "CountryCode" => "643",
                        "CountryName" => "Россия",
                        "Zip" => "628461",
                        "State" => "Ханты-Мансийский Автономный округ - Югра",
                        "City" => "Радужный"
                    ]
                ],
                "Drivers" => [
                    "Driver" => [
                        "IDCheckDriver" => $load_driver_response->IDCheckDriver,
                        "CountryCode" => "643",
                        "CountryName" => "Россия",
                        "DriverDocument" => [
                            "Serial" => "8613",
                            "Number" => "696070",
                            "DateIssue" => "2014-01-01"
                        ],
                        "CategoriesDriverLicense" => [
                            "CatDriverLicense" => "2531"
                        ],
                        "DriverDocDate" => "2014-01-01",
                        "Surname" => "Тастанов",
                        "Name" => "Азимхан",
                        "Patronymic" => "Серикович",
                        "BirthDate" => "1991-11-24"
                    ]
                ]
            ],
        ];


        $service_eosago_relaunch = $this->get_service_client('EOsagoRelaunch');
        $load_contract_eosago_response = $service_eosago_relaunch->loadContractEosago($load_contract_eosago_params);
        dd($load_contract_eosago_response);







    }



    public function get_service_client($service_name){

        $service_client            = new SoapClient( "https://b2b.alfastrah.ru/cxf/{$service_name}?wsdl", ['trace' => true, 'exception' => false]);
        $timestamp                 = gmdate('Y-m-d\TH:i:s\Z', time());
        $nonce                     = mt_rand();
        $nonceBase64               = base64_encode(pack('H*', $nonce));
        $passDigest                = base64_encode(pack('H*', sha1(pack('H*', $nonce) . pack('a*', $timestamp) . pack('a*', '85gZ65p7'))));
        $auth                      = <<<XML
        <wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
            <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="UsernameToken-3">
                <wsse:Username>EVROGARANTS</wsse:Username>
                <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">{$passDigest}</wsse:Password>
                <wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">{$nonceBase64}</wsse:Nonce>
                <wsu:Created xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">{$timestamp}</wsu:Created>
            </wsse:UsernameToken>
        </wsse:Security>
XML;
        $authvalues      = new SoapVar($auth, XSD_ANYXML);
        $security_header = new SoapHeader("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", $authvalues, true);

        $service_client->__setSoapHeaders(array($security_header));

        return $service_client;
    }





    public function test_2(){
        echo Str::json_export(
'{
"LoadContractEosago": {
	"PartnerInfo": {
		"UPID": "f7c28e39-6121-48c5-9115-3c5008456ddf"
	},
	"LoadContractRequest": {
		"CalcRef": "F4B5CDD57DBAACCDF813F665C8154CE14B89DA9E",
		"DateRevision": "2017-09-13T18:20:00",
		"DateActionBeg": "2017-09-11T00:00:00",
		"DateActionEnd": "2018-09-10T00:00:00",
		"DriversRestriction": "true",
		"IsTransCar": "false",
		"IsTrailerAllowed": "0",
		"Car": {
			"IDCheckTS": "158132070",
			"CountryCar": "1",
			"CarIdent": {
				"VIN": "XWKFB227270057729"
			},
			"LicensePlate": "О750СН86",
			"Mark": "KIA",
			"Model": "SPECTRA",
			"YearIssue": "2007",
			"DocumentCar": "30",
			"DocCarSerial": "8627",
			"DocCarNumber": "139071",
			"DocumentCarDate": "2014-12-18",
			"TicketCar": "53",
			"TicketCarNumber": "019849991501584",
			"TicketCarYear": "2017",
			"TicketCarMonth": "1",
			"EngCap": "101",
			"Purpose": "Личные"
		},
		"Insurer": {
			"PhysicalPerson": {
				"IDCheckInsurerOwner": "158130040",
				"Surname": "Тастанов",
				"Name": "Азимхан",
				"Patronymic": "Серикович",
				"BirthDate": "1991-11-24",
				"Phone": "+7 912 417-48-01",
				"Email": "tastan-15.45@mail.ru",
				"PersonDocument": {
					"DocPerson": "12",
					"Serial": "3711",
					"Number": "456809"
				},
				"CountryCode": "643",
				"CountryName": "Россия",
				"Zip": "628461",
				"State": "Ханты-Мансийский Автономный округ - Югра",
				"City": "Радужный"
			}
		},
		"Owner": {
			"PhysicalPerson": {
				"IDCheckInsurerOwner": "158129565",
				"Surname": "Тастанов",
				"Name": "Азимхан",
				"Patronymic": "Серикович",
				"BirthDate": "1991-11-24",
				"Phone": "+7 912 417-48-01",
				"Email": "tastan-15.45@mail.ru",
				"PersonDocument": {
					"DocPerson": "12",
					"Serial": "3711",
					"Number": "456809"
				},
				"CountryCode": "643",
				"CountryName": "Россия",
				"Zip": "628461",
				"State": "Ханты-Мансийский Автономный округ - Югра",
				"City": "Радужный"
			}
		},
		"Drivers": {
			"Driver": {
				"IDCheckDriver": "158126438",
				"CountryCode": "643",
				"CountryName": "Россия",
				"DriverDocument": {
					"Serial": "8613",
					"Number": "696070",
					"DateIssue": "2014-01-01"
				},
				"CategoriesDriverLicense": {
					"CatDriverLicense": "2531"
				},
				"DriverDocDate": "2014-01-01",
				"Surname": "Тастанов",
				"Name": "Азимхан",
				"Patronymic": "Серикович",
				"BirthDate": "1991-11-24"
			}
		}
	},
},



};'
        );  die();
    }

    public function test_vsk() {
        //$api = new \App\Services\SK\Vsk\Services\Kasko\KaskoApi();
        //$api->calcV2();
        $api = new \App\Services\SK\Vsk\Services\DictionaryEOsago\DictionaryApi();
        $res = $api->getClassifierItemB2B('535C2CB6-D075-4880-BBF0-9090197D2168');
        print '<pre>';
        print_r($res);
        print '</pre>';
    }







}
