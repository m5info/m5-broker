<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Models\Api\Atol\AtolCheck;


class AtolCheckController extends Controller
{

    public function __construct()
    {

    }


    public function print_check($atol_id)
    {
        $atol = AtolCheck::find($atol_id);

        $data_info = \GuzzleHttp\json_decode($atol->data_info);

        $qr_txt = "t=".date("YmdHis", strtotime($data_info->payload->receipt_datetime))."&s={$data_info->payload->total}&fn={$data_info->payload->fn_number}&i={$data_info->payload->fiscal_document_number}&fp={$data_info->payload->fiscal_document_attribute}&n=1";

        return view("payments.atol.check", [
            'atol' => $atol,
            'data_info' => $data_info,
            'qr_txt' => $qr_txt,
        ]);

    }



}
