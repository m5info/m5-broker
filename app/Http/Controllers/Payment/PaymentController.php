<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Models\Basket\BasketItems;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Models\Finance\PayMethod;
use App\Models\Reports\ReportOrderBasePayment;
use App\Models\Reports\ReportOrders;
use App\Models\User;
use App\Models\Users\Permission;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

    public function __construct()
    {
        //$this->middleware('permissions:security, security_inquiry');
    }


    public function add_payment($bso_id, Request $request)
    {
        $bso = BsoItem::getBsoId($bso_id);

        $contract = Contracts::where('bso_id', $bso_id)->get()->first();

        $payment = new Payments();

        if($request->type_id == 0){ // если взнос
            $payment->payment_number = 1;
            if ($lastPayment = $contract->payments->where('type_id', 0)->last()){
                $payment = $lastPayment->replicate();
                $payment->invoice_id = null;
                $payment->bso_receipt = '';
                $payment->bso_receipt_id = null;

                $payment->payment_number += 1;
            }

        }

        $payment->type_id = $request->type_id_id;
        $payment->bso_id = $bso->id;
        $payment->agent_id = $contract->sales_condition && $contract->sales_condition == 1 ? $contract->manager_id : $bso->agent_id;
        $payment->contract_id = $contract->id;
        $payment->statys_id = 0;

        if($payment->type_id == 2){
            $payment->set_balance = 1;
            $payment->invoice_id = null;//$request->invoice_id;
            $payment->payment_total = '';
        }

        return $this->view($payment, $request);
    }

    public function index($id, Request $request)
    {
        $payment = Payments::find($id);
        return $this->view($payment, $request);
    }

    public function view(Payments $payment, Request $request)
    {
        $view = 'payment';
        if($payment->type_id == 0) $view = 'payment';
        if($payment->type_id == 1) $view = 'debt';
        if($payment->type_id == 2) $view = 'premium';

        $agents = User::getALLUser(24)->pluck('name', 'id');

        $referencers = User::getUsersHavePermissionLike('is_referencer')->get();

        if($payment->type_id == 0 ){
            if(isset($request->report_id) && $request->report_id > 0) {
                ReportOrders::saveBaseDataPayment($payment, $request->report_id);
            } else {
                ReportOrders::saveBaseDataPayment($payment);
            }
        }

        if((int)$payment->in_basket){
            $view_path = "payments.payment.in_basket";
        }else {
            $view_path = "payments.payment.$view";
        }

        return view($view_path, [
            'payment' => $payment,
            'agents' => $agents,
            'referencers' => $referencers,
        ]);
    }


    public function save($id, Request $request)
    {
        $data = (object)$request->get('payment')?:[];
        if($data->type_id == 2 && isset($data->set_balance) && $data->set_balance == 1){ // выплата и Зачислить на баланс
            //return parentReload();
        }
        if((int)$id == 0){
            $payment = new Payments();
            $payment->type_id = $data->type_id;
            $payment->bso_id = $data->bso_id;
            $payment->contract_id = $data->contract_id;
            $payment->invoice_id = $data->invoice_id;



            $bso = BsoItem::getBsoId($data->bso_id);
            $payment->org_id = $bso->supplier_org->id;

            $payment->save();

        }else{
            $payment = Payments::find($id);
        }

        $original_payment_total = getFloatFormat($payment->payment_total);
        $payment_total = getFloatFormat($data->payment_total);

        if($data->type_id == 2){
            //$payment->payment_total = $payment->payment_total*-1;
            //$payment->save();
        }



        $payment->savePaymentData($data);
        if(isset($data->need_recount_kv) && $data->need_recount_kv){
            $this->recalc_kv($payment);
        }

        if($payment->statys_id == 1 && $original_payment_total != $payment_total){
            //$payment->addPaymentDebt($original_payment_total);
        }

        return parentReload();
    }

    public function saveComment($id, Request $request)
    {
        $payment = Payments::find($id);
        $payment->comments = $request->payment['comments'];
        $payment->save();

    return parentReload();
    }

    public function delete($id)
    {
        $payment = Payments::find($id);

        $payment->in_basket = 1;
        $payment->save();

        BasketItems::create([
            'removed_item_id' => $id,
            'type_id' => 2,
            'user_id' => auth()->id(),
        ]);

        return response(1, 200);
    }

    public function detach_receipt($id){
        $payment = Payments::findOrFail($id);
        $payment->detachReceipt();
        return response()->json(['status' => 'ok']);
    }

    public function get_pay_method_key_type($id, Request $request)
    {
        $pay_method = PayMethod::findOrFail($request->payment_method);
        $answer = $pay_method->key_type == 0 ? true : false;
        $response = new \stdClass();
        $response->receipt = $answer;
        return response()->json($response);
    }

    public function recalc_kv($payment)
    {
        if($base_payment = $payment->base_payment){
            if ($base_payment->editDataBasePayment()){
                if(isset($payment->reports_order_id)) {
                    $payment_data['report_order_id'] = $payment->reports_order_id;
                }
                $payment_data['payment_total'] = $payment->payment_total;
                $payment_data['official_discount'] = $payment->official_discount;
                $payment_data['informal_discount'] = $payment->informal_discount;
                $payment_data['bank_kv'] = $payment->bank_kv;
                $payment_data['financial_policy_id'] = $payment->financial_policy_id;
                $payment_data['financial_policy_manually_set'] = $payment->financial_policy_manually_set;
                $payment_data['financial_policy_kv_bordereau'] = $payment->financial_policy_kv_bordereau;
                $payment_data['financial_policy_kv_dvoy'] = $payment->financial_policy_kv_dvoy;
                $payment_data['financial_policy_kv_agent'] = $payment->financial_policy_kv_agent;
                $payment_data['financial_policy_kv_parent'] = $payment->financial_policy_kv_parent;

                $base_payment->update($payment_data);

            }
        }
    }

}
