<?php

namespace App\Http\Controllers\Payment;

use App\Domain\Entities\Payments\EPayment;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ObjectInsurer;
use App\Models\Contracts\Payments;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SecondPayment extends Controller
{
    public function __construct() {

        $this->middleware('permissions:contracts,temp_contracts');

    }

    public function add_second_payment(Request $request) {

        if($request->bso_id == ''){
            return parentReload();
        }

        $bso = BsoItem::find($request->bso_id);

        $contract = $bso->contract_receipt;

        return parentRedirect(url("/second_payment/{$contract->id}"))->with();
    }

    public function second_payment($contract_id){

        $contract = Contracts::find($contract_id);

        $agents = User::getALLUser(24)->pluck('name', 'id');

        $object_insurer = $contract->object_insurer;
        if (!$object_insurer) {
            $object_insurer = new ObjectInsurer();
            if ($contract->bso->product->category->template == 'auto') {
                $object_insurer->type = 1;
            }
        }

        $this->breadcrumbs[] = ['label' => 'Второй взнос'];

        return view('contracts.contract_templates.temp_contracts.second_payment', [
            'object_insurer' => $object_insurer,
            "contract" => $contract,
            "product" => $contract->product,
            'agents' => $agents,
            'view_type' => 'second_payment',
            'key' => count($contract->all_payments)
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

}
