<?php

namespace App\Http\Controllers\Log;

use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Logs\CUDLogsQueryHandler;
use App\Models\Log\CUDLogs;
use Illuminate\Http\Request;

class CudLogsController extends Controller
{
    public function actions()
    {
        $_tables = CUDLogs::query()
            ->select('table')
            ->distinct()
            ->get();

        $tables = [];
        foreach ($_tables as $table){

            $rus_name = $table->table;

            switch($rus_name){
                case 'contracts':
                    $rus_name = 'Договоры';
                    break;
                case 'bso_items':
                    $rus_name = 'БСО';
                    break;
                case 'payments':
                    $rus_name = 'Платежи';
                    break;
                case 'invoices':
                    $rus_name = 'Счета';
                    break;
                case 'incomes_expenses':
                    $rus_name = 'Расходы/доходы';
                    break;
                case 'subjects':
                    $rus_name = 'Страхователь';
                    break;
            }
            $tables[$table->table] = $rus_name;
        }
        return view('log.cud_logs.index', [
            'tables' => $tables
        ]);
    }

    public function get_table()
    {
        $logs = CUDLogs::query()
        ->select([
            'cud_logs.id as id',
            'cud_logs.table as table',
            'cud_logs.context_id as context_id',
            'users.name as user_name',
            'roles.title as role_name',
            'cud_logs.event as event',
            'cud_logs.old_data as old_data',
            'cud_logs.new_data as new_data',
            'cud_logs.created as created',
            'cud_logs.from_url as from_url'
        ])
        ->leftJoin('users', 'cud_logs.user_id', '=', 'users.id')
        ->leftJoin('roles', 'cud_logs.role_id', '=', 'roles.id')
        ->limit(request('limit'));

        $logs = (new CUDLogsQueryHandler($logs))->apply();
        $logs->orderBy('created','asc');

        return view('log.cud_logs.table', [
            'logs' => $logs->get(),
            'for_administrator' => \request('for_administrator')
        ]);
    }

    public function get_context_id(Request $request)
    {
        return CUDLogs::query()
            ->select('context_id')
            ->distinct()
            ->where('table', '=', $request->table)
            ->orderBy('context_id')
            ->get();
    }


}