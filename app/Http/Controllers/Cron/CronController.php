<?php

namespace App\Http\Controllers\Cron;

use App\Domain\Integrations\Atol\Atol;
use App\Domain\Processes\Operations\Bso\BsoStateOperations;
use App\Domain\Processes\Operations\Contracts\ContractAccept;
use App\Domain\Processes\Operations\Contracts\ContractStatus;
use App\Domain\Processes\Operations\Payments\PaymentAcceptOperations;
use App\Domain\Processes\Operations\Payments\PaymentDiscounts;
use App\Domain\Processes\Operations\Payments\PaymentExpectedPayments;
use App\Domain\Processes\Operations\Payments\PaymentFinancialPolicy;
use App\Domain\Processes\Operations\Payments\PaymentInvoice;
use App\Domain\Processes\Operations\Payments\PaymentReceipt;
use App\Domain\Processes\Operations\Payments\PaymentStatus;
use App\Domain\Processes\Scenaries\Contracts\Salaries\SalariesController;
use App\Http\Controllers\Controller;
use App\Models\Acts\ReportAct;
use App\Models\Api\Atol\AtolCheck;
use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoActsItems;
use App\Models\BSO\BsoCarts;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\Cashbox\Cashbox;
use App\Models\Cashbox\CashboxTransactions;
use App\Models\Cashbox\FoundationPayments;
use App\Models\Characters\Agent;
use App\Models\Contracts\ContractMessage;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsChat;
use App\Models\Contracts\ContractsDocuments;
use App\Models\Contracts\ContractsLogs;
use App\Models\Contracts\FNS;
use App\Models\Contracts\OnlineCalculationLogs;
use App\Models\Contracts\Orders;
use App\Models\Contracts\Payments;
use App\Models\Delivery\Delivery;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Finance\Invoice;
use App\Models\Orders\DeparturesCourierActs;
use App\Models\Orders\InspectionOrders;
use App\Models\Organizations\Organization;
use App\Models\Reports\ReportOrderBasePayment;
use App\Models\Reports\ReportOrders;
use App\Models\Settings\UserBalanceSettings;
use App\Models\User;
use App\Models\Users\AgentContracts;
use App\Models\Vehicle\AutoCodInfo;
use App\Services\AuthAD\LdapInfo;
use App\Services\AVDataInfo\IntegrationAVDataInfo;
use App\Services\BankiRu\IntegrationBankiRu;
use App\Services\BankiRu\IntegrationCubeGroup;
use App\Services\ConnectApiEnergogarant\AutoCod;
use App\Services\FireBase\FireBaseAndroid;
use App\Services\FNS\FNSFerma;
use App\Services\Front\api\KansaltingRITSql;
use App\Services\Front\IntegrationFront;
use App\Services\ResponseAPI\TestMobileConnect;
use App\Services\SK\Rosgosstrah\Services\Dictionary\UUID;
use App\Services\TecDoc\TecDocService;
use App\Services\Transfer\M5_v1\TransferM5Controller;
use App\Services\Transfer\test\TransferTestController;
use App\Services\Yandex\Maps\GeoCoder;
use Faker\Provider\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CronController extends Controller
{

    public function __construct()
    {

    }



    public function updataUser()
    {
        $users = User::whereNull('path_parent')->limit(10)->get();


        foreach ($users as $user){

            $user->path_parent = $user->getPathParent();
            $user->save();

            return $this->updataUser();
        }


        return true;
    }

    public function clearSystems(){

        \DB::table('incomes_expenses')->truncate();
        \DB::table('incomes_expenses_documents')->truncate();
        \DB::table('inspection_comments')->truncate();

        \DB::table('subjects')->truncate();
        \DB::table('subjects_fl')->truncate();
        \DB::table('subjects_ul')->truncate();

        //БСО
        BsoLogs::query()->truncate();
        BsoCarts::query()->truncate();
        BsoActs::query()->truncate();
        BsoActsItems::query()->truncate();
        ReportOrders::query()->truncate();
        ReportAct::query()->truncate();
        BsoItem::query()->truncate();

        \DB::table('report_documents')->truncate();
        \DB::table('report_payment_sum')->truncate();
        \DB::table('reports_act_documents')->truncate();
        \DB::table('reports_advertising')->truncate();
        \DB::table('reports_orders_base_payments')->truncate();
        \DB::table('reservations')->truncate();
        \DB::table('notifications')->truncate();


        //Договора
        Invoice::query()->truncate();
        Cashbox::query()->truncate();
        CashboxTransactions::query()->truncate();
        ContractMessage::query()->truncate();
        Contracts::query()->truncate();
        ContractsChat::query()->truncate();
        ContractsDocuments::query()->truncate();
        ContractsLogs::query()->truncate();

        \DB::table('contracts_masks')->truncate();
        \DB::table('contracts_scans')->truncate();
        \DB::table('contracts_terms')->truncate();
        \DB::table('drivers')->truncate();
        \DB::table('expected_payments')->truncate();
        \DB::table('foundation_payments')->truncate();


        \DB::table('inspection_orders')->truncate();
        \DB::table('inspection_orders_logs')->truncate();
        \DB::table('inspection_orders_reports')->truncate();
        \DB::table('inspection_orders_reports_documents')->truncate();


        \DB::table('kbm_calculations')->truncate();
        \DB::table('log_events')->truncate();

        \DB::table('notifications')->truncate();

        \DB::table('object_insurer')->truncate();
        \DB::table('object_insurer_auto')->truncate();
        \DB::table('object_insurer_auto_equipment')->truncate();
        \DB::table('online_calculations')->truncate();
        \DB::table('optional_equipment')->truncate();
        \DB::table('order_comments')->truncate();
        \DB::table('order_documents')->truncate();

        \DB::table('orders')->truncate();
        \DB::table('orders_log_states')->truncate();
        \DB::table('org_scans')->truncate();

        \DB::table('files')->where('is_migrate', 1)->delete();

        \DB::table('payment_accepts')->truncate();
        \DB::table('payments')->truncate();

        \DB::table('users_balance')->truncate();
        \DB::table('users_balance_transactions')->truncate();

        return true;
    }





    public function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public function test(Request $request)
    {


        dd('add all delivery');

        $payments = Payments::where('payment_data', '>=','2021-02-01')
            //->where('order_id', '>', 0)
            ->where('statys_id', '=', 1)
            ->whereNotNull('delivery_date')
            ->get();

        $RIT = new IntegrationFront();


        foreach ($payments as $payment){
            $date = (object)$RIT->get_order_data($payment->order_id);
            $payment->delivery_date = getDateFormatEn($date->delivery_date);
            $payment->save();

            //SalariesController::setSalaries($payment);
            Delivery::where('payment_id', $payment->id)->update(['delivery_date' => getDateFormatEn($date->delivery_date)]);
        }

       

        dd('IS LOAD');


        $res = MSSQLQueryResult("SELECT TOP 10 [id_node]
      ,[type]
      ,[GuidCode]
      ,[HashCode]
      ,[@РодительскийРаздел]
      ,[@РодительскийРаздел.НазваниеОбъекта]
      ,[Примечание]
      ,[ПримечаниеКСрочности]
      ,[ДатаПерезвона]
      ,[ХешВидовСтрахования]
      ,[ХешОбъектовСтрахования]
      ,[СсылкаНаКлиента]
      ,[СсылкаНаКлиента.НазваниеОбъекта]
      ,[ПорядковыйНомер]
      ,[СтатусКонсультации]
      ,[ВиртуальныйКаталог]
      ,[СсылкаНаКлиента/ТелефонныйНомер]
      ,[Classitem]
      ,[ХешИсточников]
      ,[СсылкаНаПользователя]
      ,[СсылкаНаПользователя.НазваниеОбъекта]
      ,[ДатаСтрахования]
      ,[@Новый]
      ,[СсылкаНаКлиента/КатегорияТелефонов]
      ,[ФлагСрочноПерезвонитьКлиенту]
      ,[СсылкаНаКлиента/List<ТелефонныйНомер>]
  FROM [еврогарант].[dbo].[cache_Консультирование]");


        dd($res);

//        $file = '/home/sem/Рабочий стол/renisans/Тест 7';
//
//        $current = '';
//        $logs = OnlineCalculationLogs::where('contract_id', '=', 7)->get();
//
//        foreach($logs as $log){
//
//            $data_send = $this->isJson($log->data_send) ? \GuzzleHttp\json_encode(\GuzzleHttp\json_decode($log->data_send), JSON_UNESCAPED_UNICODE) : $log->data_send;
//            $data_response = $this->isJson($log->data_response) ? \GuzzleHttp\json_encode(\GuzzleHttp\json_decode($log->data_response), JSON_UNESCAPED_UNICODE) : $log->data_response;
//
//            $current .= "REQUEST:\n";
//            $current .= "{$log->action}\n";
//            $current .= "{$data_send}\n";
//            $current .= "\n";
//            $current .= "RESPONSE:\n";
//            $current .= "{$data_response}\n";
//            $current .= "\n";
//            $current .= "\n";
//            $current .= "\n";
//            $current .= "\n";
//            $current .= "\n";
//        }
//
//        file_put_contents($file, $current);
dd(1);
        $front = new IntegrationFront();
        $payment = Payments::getPaymentId($request->id);

        dd($front->create_second_payment($payment));


        //$tec = new TecDocService();

        //dd($tec->realNumberArticles(123));

        //dd($tec->adaptabilityModifications('febi', '01089', 'audi', 75));



        //

       dd("OK");


        /*
       $payment = Payments::find($request->id);



       $_b = UserBalanceSettings::where('isset_for_kv', '=', 1)->where('is_actual', 1)->get()->first();
       $agent_balance = $payment->agent->getBalance($_b->id);


       if($agent_balance && $curator = $agent_balance->agent->curator){
           $curator_balance = $curator->getBalance($_b->id);
           if($curator_balance && $payment->financial_policy && getFloatFormat($payment->financial_policy->partnership_reward_1) > 0){
               dd($payment->financial_policy->partnership_reward_1);
           }
       }

       dd($curator);


       $payment = PaymentFinancialPolicy::actualize($payment);
       $payment = PaymentDiscounts::recount($payment);


       dd($payment);
       */
        dd("OK");


        if(isset($request->myclear) && $request->myclear == 2345){
            //dd($this->clearSystems());
        }


        $contract = Contracts::getContractId($request->id);

        foreach ($contract->online_calculations as $calculation){



            //$calculation->temp_calc();
            //dd($calculation->insurance_companies_id);

            if($calculation->insurance_companies_id == $request->sk){


                //if ($service = $calculation->get_product_service()) {
                    /*Выпуск сертификата -- функция релиза*/
                    //$release = $service->release($calculation);

                    //dd($release);
                //}

                $res = $calculation->calculate(null);

                dd($res);

                //$calculation->temp_calc();



            }



        }


        dd(333);





        /*
        $calculation = $contract->selected_calculation;
        $service = $calculation->get_product_service();

        $release = $service->get_files($calculation);
        */

        //dd($release);


        return view('test.index', [

        ]);

    }

    public function site(Request $request)
    {
        if($_POST){

            $params = $_POST;

            $url = "http://192.168.2.18/k/Web/default.aspx";
            $ch = curl_init();                              // инициализируем сессию curl

            curl_setopt($ch, CURLOPT_URL,$url);             // указываем URL, куда отправлять POST-запрос
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);    // разрешаем перенаправление
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);     // указываем, что результат запроса следует передать в переменную, а не вывести на экран
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);           // таймаут соединения
            curl_setopt($ch, CURLOPT_POST, 1);              // указываем, что данные надо передать именно методом POST
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_USERAGENT, 'MyAgent');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);  // добавляем данные POST-запроса
            $numOrder = curl_exec($ch);                     // выполняем запрос
            $error = curl_error($ch);                     // выполняем запрос
            curl_close($ch);

            if ($error){
                dd($error);
                return response($error, 200);
            };




            return response($numOrder, 200);
        }

        return response('GET', 200);

    }





}
