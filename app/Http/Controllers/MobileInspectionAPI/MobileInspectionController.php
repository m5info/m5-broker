<?php

namespace App\Http\Controllers\MobileInspectionAPI;

use App\Domain\Processes\Operations\Inspection\InspectionSetExecutor;
use App\Domain\Processes\Operations\Inspection\Temple\InspectionCrash;
use App\Http\Controllers\Controller;
use App\Models\Cashbox\IncomeExpense;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsChat;
use App\Models\Contracts\ContractsDocuments;
use App\Models\File;
use App\Models\Orders\InspectionOrders;
use App\Models\Settings\IncomeExpenseCategory;
use App\Models\User;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;
use App\Repositories\FilesRepository;
use App\Services\FireBase\FireBaseAndroid;
use App\Services\Pushers\PusherRepository;
use App\Services\ResponseAPI\ResponseMobileJSON;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Mockery\Exception;


class MobileInspectionController extends Controller
{
    use AuthenticatesUsers;

    public function __construct(Request $request)
    {

        $token = $request->header('token');
        if(isset($token) && strlen($token)>10)
        {
            $user = User::where('api_token', $token)->where('status_user_id', '!=', 1)->get()->last();
            if($user){
                $this->guard('auth')->loginUsingId($user->id);
            }else{
                echo \GuzzleHttp\json_encode(ResponseMobileJSON::responseErrorJSON('Ошибка авторизации'));
                die(1);
            }
        }
    }


    public function index(Request $request)
    {
        $user = User::find(1);

        //$user = Auth::user();
        $balances = $user->getBalanceListHome();
        $info = [];
        foreach ($balances as $balance){
            $info[] = [
                'id' => $balance->id,
                'title' => $balance->name,
                'balance' => $balance->balance,
            ];
        }
        return response()->json(ResponseMobileJSON::responseJSON($info));

        //dd(auth()->user());

        //$fcm = new FireBaseAndroid();
        //$res = $fcm->push($request->title, $request->message, $request->id, $request->event, $user->push);

        //dd($res);

        $res = ResponseMobileJSON::responseErrorJSON('test');
        return response()->json($res);
    }



    //ПОЛЬЗОВАТЕЛЬ

    public function auth(Request $request)
    {
        //\Log::info($request->getContent());

        $res = ResponseMobileJSON::responseErrorJSON("Ошибка авторизации");

        $auth = auth()->attempt(['email' => $request->login, 'password' => $request->password]);
        if($auth == true){
            $user = auth()->user();
            if($user->status_user_id != 1){
                $userToken = $this->create_token(auth()->user()->id);
                $user->api_token = $userToken;
                $user->save();
                $res = ResponseMobileJSON::responseJSON(['token'=> $userToken]);
            }
        }

        return response()->json($res);
    }


    public function create_token($key){
        $txt = md5(date("Ymd").$key.date("His"));
        if(strlen($txt)>=16)  $txt = substr($txt, 0, 16);
        else  $txt = $this->create_token($key);
        if(strlen($txt) == 16){
            return $res = strtoupper($txt);
        }
    }


    public function balance(Request $request)
    {
        $user = auth()->user();
        $balances = $user->getBalanceListHome();
        $info = [];
        foreach ($balances as $balance){
            $info[] = [
                'id' => $balance->id,
                'title' => $balance->name,
                'balance' => $balance->balance,
            ];
        }

        return response()->json(ResponseMobileJSON::responseJSON($info));
    }



    public function userIsWork(Request $request)
    {
        $user = auth()->user();
        $user->is_work = ($request->is_work == 'true')?1:0;
        $user->save();
        return response()->json(ResponseMobileJSON::responseJSON([]));
    }

    public function userGeo(Request $request)
    {
        $user = auth()->user();
        $user->geo_lat = $request->lat;
        $user->geo_lon = $request->lon;
        $user->save();
        return response()->json(ResponseMobileJSON::responseJSON([]));
    }



    public function firebase(Request $request)
    {
        $user = auth()->user();
        $user->firebase = $request->token;
        $user->save();

        $fcm = new FireBaseAndroid();
        $fcm->register($user->firebase);

        return response()->json(ResponseMobileJSON::responseJSON([]));
    }






    //РАСХОДЫ

    public function costs(Request $request)
    {

        $page_count = 25;
        $user = auth()->user();
        $incomes_expenses = IncomeExpense::query();

        $incomes_expenses->where('payment_user_id', $user->id);
        $incomes_expenses->orderBy('payment_date', 'desc');
        $incomes_expenses->skip((int)$request->offset)->take(($page_count));


        $res = $incomes_expenses->get();

        $info = [];
        foreach ($res as $cost){

            $info[] = [
                'category' => $cost->category->title,
                'cost' => $cost->total,
                'comment' => $cost->comment,
            ];


        }
        return response()->json(ResponseMobileJSON::responseJSON($info));
    }

    public function costsCategory(Request $request)
    {
        $categorys = IncomeExpenseCategory::query();
        $categorys->where('type', 2);

        //ДОБАВИТЬ УСЛОВИЕ НА ТО ЧТО ЭТО РАСХОД НА ВЫЕЗДЫ

        $info = [];
        foreach ($categorys->get() as $category){
            $info[] = [
                'id' => $category->id,
                'title' => $category->title,
            ];
        }

        return response()->json(ResponseMobileJSON::responseJSON($info));
    }

    public function costsAdd(Request $request)
    {
        $user = auth()->user();

        $cost = getFloatFormat($request->cost);
        $comment = $request->comment;
        $balance = $user->getBalanceId((int)$request->balanceId);

        $category = IncomeExpenseCategory::where('id', (int)$request->categoryId)->get()->last();

        if($balance->balance >= $cost){

            $income_expense = new IncomeExpense();
            $income_expense->category_id = (int)$request->categoryId;
            $income_expense->user_id = auth()->id();
            $income_expense->payment_type = 0;
            $income_expense->status_id = 2;
            $income_expense->payment_user_id = auth()->id();
            $income_expense->date = date('Y-m-d H:i:s');
            $income_expense->sum = $cost;
            $income_expense->commission = 0;
            $income_expense->comment = $comment;
            $income_expense->commission_total = 0;
            $income_expense->total = $cost;
            $income_expense->save();

            $balance->setTransactionsMobile(1, 1, $cost, date('Y-m-d H:i:s'), "{$category->title} $comment");

            return response()->json(ResponseMobileJSON::responseJSON([]));

        }

        return ResponseMobileJSON::responseErrorJSON("Недостаточно средств!");

    }




    //ЗАЯВКИ
    public function orders(Request $request)
    {

        $contracts = InspectionOrders::getOrdersQuery()->whereIn('contracts.status_order_id', [1, 2, 3, 4, 5]);
        $contracts->leftJoin('inspection_orders', 'inspection_orders.id', '=', 'contracts.inspection_id');
        $contracts->where('inspection_orders.processing_user_id', auth()->id());
        $contracts->select("contracts.*");
        $contracts->orderBy('contracts.status_order_id', 'asc');
        $contracts->orderBy('contracts.begin_date', 'desc');

        $page_count = 25;

        $contracts->skip((int)$request->offset)->take(($page_count));

        $res = $contracts->get();
        $orders = [];

        foreach ($res as $order){


            $row = [];

            $address = ($order->inspection)?$order->inspection->address:'';
            $object = $order->object_insurer;

            $mark = $object->data()->mark_id ? \App\Models\Vehicle\VehicleMarks::find($object->data()->mark_id)->title : '';
            $model = $object->data()->model_id ? \App\Models\Vehicle\VehicleModels::find($object->data()->model_id)->title : '';

            $row[] = ['title'=>'Дата', 'value'=>setDateTimeFormatRu($order->begin_date)];
            $row[] = ['title'=>'Адрес', 'value'=>$address];
            $row[] = ['title'=>'Марка/Модель', 'value'=>"$mark $model"];
            $row[] = ['title'=>'Рег. номер', 'value'=>$object->data()->reg_number];

            //$order->status_order_id = 0;

            $orders[] = [
                'id' => $order->id,
                'title' => $order->product->title." ".$order->id." - ".InspectionOrders::STATUSES[$order->status_order_id],
                'color' => InspectionOrders::COLOR[$order->status_order_id],
                'row' => $row,
            ];



        }




        return response()->json(ResponseMobileJSON::responseJSON($orders));


    }



    public function order(Request $request)
    {
        $order = InspectionOrders::getOrderId($request->id);
        $inspection = $order->inspection;


        $address = ($inspection)?$inspection->address:'';
        $object = $order->object_insurer;

        $mark = $object->data()->mark_id ? \App\Models\Vehicle\VehicleMarks::find($object->data()->mark_id)->title : '';
        $model = $object->data()->model_id ? \App\Models\Vehicle\VehicleModels::find($object->data()->model_id)->title : '';


        $row = [];
        $row[] = ['title'=>'Тип', 'value'=>InspectionOrders::POSITION_TYPE[$inspection->position_type_id ? $inspection->position_type_id : 0]];

        $row[] = ['title'=>'Дата', 'value'=>setDateTimeFormatRu($order->begin_date)];
        $row[] = ['title'=>'Адрес', 'value'=>$address];
        $row[] = ['title'=>'Марка/Модель', 'value'=>"$mark $model"];
        $row[] = ['title'=>'Рег. номер', 'value'=>$object->data()->reg_number];

        $row[] = ['title'=>'ФИО/Название', 'value'=>$order->insurer->title];
        $row[] = ['title'=>'Телефон', 'value'=>$order->insurer->phone];


        $status = -1;


        if($order->status_order_id == 1 ) $status = 0; //Назначение  - Отказался или Взял в работу
        if($order->status_order_id == 2 )
        {
            if($order->inspection->processing_user_id == auth()->id()){
                if($order->inspection->status_id == 4) $status = 1; //В работе - Я приехал
                if($order->inspection->status_id == 5) $status = 2; //Завершить заявку
            }

        }

        $info = [
            'id' => $order->id,
            'status' => $status,
            'temples' => 'crash',
            'url' => url("get_mobile_act/".$order->getTokenAct()),
            'row' => $row,
        ];

        return response()->json(ResponseMobileJSON::responseJSON($info));
    }


    public function orderEvent(Request $request)
    {
        $order = InspectionOrders::getOrderId($request->id);
        $event = (int)$request->event;


        if($event == 0) // Отказался
        {
            InspectionSetExecutor::decline_order($order->id, auth()->id());
        }

        if($event == 1) // Взял
        {
            InspectionSetExecutor::processing_user($order->id, auth()->id());
        }

        if($event == 2) // приехал
        {
            InspectionSetExecutor::arrived_order($order->id, auth()->id());
        }

        if($event == 3) // Завершить
        {
            InspectionSetExecutor::finish_order($order->id, auth()->id());
        }

        $Pusher = new PusherRepository();
        $Pusher->triggerContractContent($request->id, ['event' => 'reload']);

        return response()->json(ResponseMobileJSON::responseJSON([]));
    }




    public function getOrderMedia(Request $request)
    {
        //$request->lat;
        //$request->lon;

        $address = " ";
        $order = InspectionOrders::getOrderId($request->id);

        $row = [];
        foreach($order->scans as $file)
        {
            //type  0 фото 1 видео 2 иное
            //image - маленькая картинка для списка
            //source - ссылка
            $type = 2;
            $image = "/images/extensions/{$file->ext}.png";
            $source = $file->getUrlMobileAttribute();

            if (in_array($file->ext, ['jpg', 'jpeg', 'png', 'gif'])){
                $type = 0;
                $image = $source;
            }

            if (in_array($file->ext, ['mp4', 'mkv', 'avi ','mov','avi','mpeg4','flv','3gpp'])){
                $type = 1;
                $image = "/images/extensions/video.png";
            }

            $row[] = ['id'=>$file->id, 'type'=>$type, 'image'=>url($image), 'source'=>url($source)];

        }

        $info = [
            'address' => $address,
            'row' => $row,
        ];

        return response()->json(ResponseMobileJSON::responseJSON($info));
    }


    public function orderFile(Request $request)
    {

        $filesRepository = new FilesRepository();
        $file = $filesRepository->makeFile($request->file, Contracts::FILES_DOC . "/{$request->id}/");
        Contracts::findOrFail($request->id)->scans()->save($file);

        $Pusher = new PusherRepository();
        $Pusher->triggerContractContent($request->id, ['event' => 'add-file', 'id' => $file->id]);

        return response()->json(ResponseMobileJSON::responseJSON([]));
    }

    public function orderFileDelete(Request $request)
    {
        $contract = Contracts::findOrFail($request->id);
        $file = File::query()->where('id', $request->document_id)->first();
        $Pusher = new PusherRepository();
        $Pusher->triggerContractContent($request->id, ['event' => 'delete-file', 'id' => $file->id]);

        app()->make('\App\Http\Controllers\FilesController')->callAction('destroy', [$file->name]);


        return response()->json(ResponseMobileJSON::responseJSON([]));
    }


    public function temple(Request $request)
    {
        $order = InspectionOrders::getOrderId($request->id);
        $info = [];

        if($request->temples == 'crash')
        {
            $info = InspectionCrash::getTempleCategory($order, $request->category);
        }

        return response()->json(ResponseMobileJSON::responseJSON($info));
    }

    public function saveTemple(Request $request)
    {
        $order = InspectionOrders::getOrderId($request->id);
        if($request->temples == 'crash')
        {
            InspectionCrash::saveTempleOrder($order, $request->category, $request->data);
        }

        return response()->json(ResponseMobileJSON::responseJSON([]));
    }


    public function directory(Request $request)
    {
        $info = [];
        if($request->category == 'mark')
        {
            $info[] = ['id'=>0, 'title'=>'Не выбрано'];
            foreach (VehicleMarks::orderBy('title')->get() as $mark){
                $info[] = ['id'=>$mark->id, 'title'=>$mark->title];
            }
        }

        if($request->category == 'model')
        {
            $models = VehicleModels::select(\DB::raw("IF(title REGEXP '^[а-яА-Я0-9]', 0, 1) as sort, id, title"))->where('mark_id', $request->mark)->orderBy('sort', 'desc')->orderBy('title', 'asc');

            foreach ($models->get() as $model){
                $info[] = ['id'=>$model->id, 'title'=>$model->title];
            }

        }

        if($request->category == 'search')
        {
            //ПОИСК В АВТОКОДЕ
            //type = тип по сечу ищем vin или regNumber
            //value = значение

            //AutoCodInfo::search('vin','X894923H7C1FC0010')

            $info = [
                ['id'=>0, 'title'=>'АвтоКод не подключен'],
                //['id'=>1, 'title'=>'Машина 1'],
                //['id'=>2, 'title'=>'Машина 2'],
            ];

        }


        return response()->json(ResponseMobileJSON::responseJSON($info));
    }

    public function saveAutoData(Request $request)
    {
        $order = InspectionOrders::getOrderId($request->id);
        //Находим данные сохраненные в автоКоде search = тут id закешированных данных
        //category = в какой шаблон подставлять

        return response()->json(ResponseMobileJSON::responseJSON([]));
    }


    public function orderChatList(Request $request)
    {
        $this->markAsRead($request);

        $contract = Contracts::findOrFail($request->id);
        $messages = $contract->chats()->get()->map(function ($msg) use ($request) {
            return [
                'sender' => $msg->sender->name,
                'date'   => $msg->date_sent->format('Y-m-d H:i'),
                'text'   => $msg->text,
                'status'   => $msg->status,
                'is_player' => $msg->is_player
            ];
        });

        return response()->json(ResponseMobileJSON::responseJSON($messages));
    }


    public function orderChatSend(Request $request) {
        $msg = ContractsChat::saveMsg($request->id, $request->text, 1);

        $info = [
            'sender' => $msg->sender->name,
            'date'   => $msg->date_sent->format('Y-m-d H:i'),
            'text'   => $msg->text,
            'status'   => $msg->status,
            'is_player' => $msg->is_player
        ];




        return response()->json(ResponseMobileJSON::responseJSON($info));
    }


    private function markAsRead(Request $request) {
        try {
            ContractsChat::unread()->where('contract_id', $request->id)->where('is_player', ContractsChat::EMPLOYEE)->update(['status' => ContractsChat::STATUS_RECEIPT, 'date_receipt' => Carbon::now()]);
            return response()->json(ResponseMobileJSON::responseJSON([]));
        } catch (Exception $e) {
            return ResponseMobileJSON::responseErrorJSON("Something went wrong");
        }
    }

}
