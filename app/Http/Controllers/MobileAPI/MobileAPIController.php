<?php

namespace App\Http\Controllers\MobileAPI;

use App\Http\Controllers\Controller;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Payments;
use App\Models\Finance\Invoice;
use App\Models\Security\Security;
use App\Models\User;
use App\Services\ResponseAPI\ResponseMobileJSON;
use Illuminate\Http\Request;

class MobileAPIController extends Controller
{

    public function __construct(Request $request)
    {

        $token = $request->header('token');
        if(isset($token) && strlen($token)>10)
        {
            $user = User::where('api_token', $token)->get()->last();
            if($user){
                $this->guard('auth')->loginUsingId($user->id);
            }else{
                echo \GuzzleHttp\json_encode(ResponseMobileJSON::responseErrorJSON('Ошибка авторизации'));
                die(1);
            }
        }

    }


    public function test(Request $request)
    {

        $res = ResponseMobileJSON::responseErrorJSON('test');
        return response()->json($res);
    }



}
