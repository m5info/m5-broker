<?php

namespace App\Http\Controllers\FrontFrame;

use App\Http\Controllers\Controller;
use App\Models\Contracts\Payments;
use App\Models\User;
use App\Services\Front\IntegrationFront;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class FrontFrameController extends Controller
{

    public function __construct(Request $request)
    {


        if(!auth()->check() && (int)$request->user_id > 1)
        {
            $user = User::where('front_user_id', (int)$request->user_id)->where('status_user_id', 0)->get()->first();
            if($user){
                Auth::login($user, true);
            }else{
                abort(404);
            }
        }

    }


    public function expected_payments(Request $request)
    {

        //dd(auth()->user());

        return view('front.frame.expected_payments.index', []);

    }

    public function get_payments_table(Request $request)
    {

        $payments = Payments::getPaymentsQuery();
        $payments->leftJoin('contracts', 'contracts.id', '=', 'payments.contract_id');

        $payments->where('payments.type_id', 0);
        $payments->where('payments.payment_number', '>', 1);
        $payments->where('payments.statys_id', '<=', 0);
        $payments->where('contracts.order_id','>', 0);

        $payments->where('payments.is_deleted', 0);

        $payments->where('payments.send_front', 0);

        if(isset($request->date_from) && strlen($request->date_from) > 6){
            $payments->where('payments.payment_data', '>=', getDateFormatEn($request->date_from));
        }

        if(isset($request->date_to) && strlen($request->date_to) > 6){
            $payments->where('payments.payment_data', '<=', getDateFormatEn($request->date_to));
        }

        $payments->orderBy("payments.payment_data",'asc');

        $payments->select('payments.*');

        return view('front.frame.expected_payments.table', [
            'payments' => $payments->get()
        ]);


    }


    public function sendPayments(Request $request)
    {
        $agent_id = $request->agent_id;
        $front = new IntegrationFront();

        $payments_ids = $request->payment_ids;
        foreach ($payments_ids as $id){
            $payment = Payments::getPaymentId($id);
            $payment->agent_id = $agent_id;
            $payment->manager_id = $agent_id;
            $payment->save();
            $front->create_second_payment($payment);
        }


        return response('', 200);
    }


}
