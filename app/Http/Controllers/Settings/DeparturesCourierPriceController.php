<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Log\LogEvents;
use App\Models\Settings\DeparturesCourierPrice;
use Illuminate\Http\Request;

class DeparturesCourierPriceController extends Controller
{

    public function __construct()
    {
        $this->middleware('permissions:settings,departures_courier_price');
    }

    public function index()
    {
        return view('settings.departures_courier_price.index', [
            'departures' => DeparturesCourierPrice::orderBy('title')->get()
        ]);
    }

    public function create()
    {
        return view('settings.departures_courier_price.create');
    }

    public function edit($id)
    {
        return view('settings.departures_courier_price.edit', [
            'departure' => DeparturesCourierPrice::findOrFail($id)
        ]);
    }

    public function store(Request $request)
    {
        $departure = new DeparturesCourierPrice;
        $departure->save();
        LogEvents::event($departure->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_CREATE], 6, 0,0, $request->all());

        return $this->save($departure, $request);
    }

    public function update($id, Request $request)
    {
        $departure = DeparturesCourierPrice::findOrFail($id);
        LogEvents::event($departure->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_UPDATE], 6, 0,0, $request->all());

        return $this->save($departure, $request);
    }

    private function save(DeparturesCourierPrice $departure, Request $request)
    {

        $departure->title = $request->title;
        $departure->is_actual = (int)$request->is_actual;
        $departure->city_id = (int)$request->city_id;
        $departure->is_debt = (int)$request->is_debt;
        $departure->price_total = getFloatFormat($request->price_total);
        $departure->debt_total = getFloatFormat($request->debt_total);

        $departure->save();
        return parentReload();
    }

    public function destroy($id)
    {
        LogEvents::event($id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_DELETE], 6);

        DeparturesCourierPrice::findOrFail($id)->delete();

        return response('', 200);
    }

}
