<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SystemProjectFeaturesController extends Controller
{

    const ACTIONS = [
        'uc' => 'uc.sh',
        'ud' => 'ud.sh',
        'program' => 'program.sh'
    ];


    public function index()
    {

        if (stristr($_SERVER['HTTP_HOST'], '127.0.0.1')){
            $localhost = true;
        }else{
            $localhost = false;
        }

        return view('settings.system_project_features.index', [
            'localhost' => $localhost
        ]);
    }


    public function any_login(Request $request)
    {

        if (auth()->user()->hasPermission('role_owns', 'login_any_user') || session()->has('any_login')){
            $mainUserId = auth()->id();

            if ($user = User::findOrFail($request->login_user_id)){

                Auth::login($user, true);
                Session::put([
                    'any_login' => true,
                    'user_id' => $mainUserId
                ]);
            }

        }
    }

    public function action()
    {

        try{
            $ssh = ssh2_connect(config('app.system_features_host'), 22);

            if ($result = $this->decrypt(config('app.system_features_password'), request('password'))){

                if (ssh2_auth_password($ssh, config('app.system_features_user'), $result)) {

                    $action = request('action');
                    $dir = str_replace('public', '', getcwd());
                    $command = 'cd '.$dir.' && sh '.$this::ACTIONS[$action].' '.$dir;

                    $stream = ssh2_exec($ssh, $command);
                    stream_set_blocking($stream, true);
                    $stream_out = ssh2_fetch_stream($stream, SSH2_STREAM_STDIO);

                    $temp_log = stream_get_contents($stream_out);
                    $log =  $this->answer($temp_log, false);


                    if (stristr($temp_log, 'Конфликт слияния') || stristr($temp_log, 'Merge conflict')){
                        $log = $this->answer('Конфликт слияния.'.PHP_EOL.'Зайдите на сервер и решите конфликт', true);
                    }

                } else {
                    $log = $this->answer('Не удалось авторизоваться.'.PHP_EOL.'Возможно доступы к серверу были изменены, смотрите свою локальную конфигурацию', true);
                }

            }else{
                $log = $this->answer('Не удалось авторизоваться'.PHP_EOL.'Неверный пароль, попробуйте снова..', true);
            }
        }catch(\Exception $e){
            $log = $this->answer('Локальные настройки для доступов не указаны', true);
        }

        return $log;
    }

    private function decrypt($word, $password)
    {

        $c = base64_decode($word);
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $plaintext = openssl_decrypt($ciphertext_raw, $cipher, $password, $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $password, $as_binary=true);
        if (hash_equals($hmac, $calcmac))
        {
            return $plaintext;
        }else{
            return false;
        }

    }

    private function answer($log, $error)
    {
        return response()->json([
            'time' => date('H:i:s'),
            'log' => PHP_EOL.PHP_EOL.$log,
            'error' => $error,
        ]);
    }

    /*                          Генерация пароля                         */
    /* https://sst2015.atlassian.net/wiki/spaces/techdoc/pages/752746497 */

    /*private function example_encrypt($word, $salt)
    {
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($word, $cipher, $salt, $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $salt, $as_binary=true);
        $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
        return $ciphertext;
    }*/

}