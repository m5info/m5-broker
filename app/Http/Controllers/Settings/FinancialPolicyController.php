<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Directories\FinancialPolicyGroup;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Log\LogEvents;
use App\Models\Settings\FinancialGroup;
use Illuminate\Http\Request;

class FinancialPolicyController extends Controller
{

    public function __construct()
    {
        $this->middleware('permissions:settings,financial_policy');
    }

    public function index()
    {
        return view('settings.financial_group.index', [
            'financial_groups' => FinancialGroup::orderBy('title')->get()
        ]);
    }

    public function create()
    {
        return view('settings.financial_group.create');
    }

    public function edit($id)
    {
        return view('settings.financial_group.edit', [
            'financial_group' => FinancialGroup::findOrFail($id)
        ]);
    }

    public function store(Request $request)
    {
        $financial_group = new FinancialGroup;
        $financial_group->save();
        LogEvents::event($financial_group->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_CREATE], 5, 0,0, $request->all());

        return $this->save($financial_group, $request);
    }

    public function update($id, Request $request)
    {
        $financial_group = FinancialGroup::findOrFail($id);
        LogEvents::event($financial_group->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_UPDATE], 5, 0,0, $request->all());

        return $this->save($financial_group, $request);
    }

    private function save(FinancialGroup $financial_group, Request $request)
    {

        $financial_group->title = $request->title;
        $financial_group->is_actual = (int)$request->is_actual;

        $financial_group->save();
        return parentReload();

    }

    public function destroy($id)
    {
        LogEvents::event($id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_DELETE], 5);
        FinancialGroup::findOrFail($id)->delete();

        return response('', 200);
    }

    public function copy_template()
    {
        $fp = FinancialGroup::all();
        $fp = $fp->pluck('title', 'id')->prepend('Не выбрано', 0);

        return view('settings.financial_group.copy', [
            'fp' => $fp,
        ]);
    }

    public function copy(Request $request)
    {

        foreach (InsuranceCompanies::all() as $insurance_company)
        {
            foreach ($insurance_company->bso_suppliers as $bso_supplier)
            {
                foreach ($bso_supplier->financial_policy as $financial_policy )
                {

                    foreach ($financial_policy->groups as $group){

                        if ($group->id==$request->from){
                            $category_1 = $group;
                        }

                        if ($group->id==$request->to){
                            $category_2 = $group;
                        }
                    }

                    $fp_1 = FinancialPolicyGroup::query()
                        ->where('financial_policies_group_id', '=', $category_1->getOriginal('pivot_financial_policies_group_id'))
                        ->where('financial_policy_id', '=', $category_1->getOriginal('pivot_financial_policy_id'))
                        ->first();

                    $q = "update financial_policies_groups_kv set";

                    $fpgi = $category_2->getOriginal('pivot_financial_policies_group_id');
                    $fpi = $category_2->getOriginal('pivot_financial_policy_id');

                    \DB::statement("$q is_actual = $fp_1->is_actual, kv_agent = $fp_1->kv_agent, kv_parent = $fp_1->kv_parent where financial_policies_group_id = $fpgi AND financial_policy_id = $fpi;");

                }
            }
        }

        return frameSuccess('Выполнено успешно!');
    }

}
