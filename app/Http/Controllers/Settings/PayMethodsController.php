<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Finance\PayMethod;
use App\Models\Log\LogEvents;
use App\Models\Settings\Bank;
use Illuminate\Http\Request;

class PayMethodsController extends Controller
{

    public function __construct()
    {
        $this->middleware('permissions:settings,pay_methods');
    }

    public function index()
    {
        return view('settings.pay_methods.index', [
            'pay_methods' => PayMethod::orderBy('title')->get()
        ]);
    }

    public function create()
    {
        return view('settings.pay_methods.create');
    }

    public function edit($id)
    {
        return view('settings.pay_methods.edit', [
            'pay_method' => PayMethod::findOrFail($id)
        ]);
    }

    public function store(Request $request)
    {
        $pay_method = new PayMethod;
        $pay_method->save();
        LogEvents::event($pay_method->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_CREATE], 6, 0,0, $request->all());

        return $this->save($pay_method, $request);
    }

    public function update($id, Request $request)
    {
        $pay_method = PayMethod::findOrFail($id);
        LogEvents::event($pay_method->id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_UPDATE], 6, 0,0, $request->all());

        return $this->save($pay_method, $request);
    }

    private function save(PayMethod $pay_method, Request $request)
    {

        $pay_method->title = $request->title;
        $pay_method->is_actual = (int)$request->is_actual;
        $pay_method->show_in_primary = (int)$request->show_in_primary;

        $pay_method->payment_type = (int)$request->payment_type;
        $pay_method->payment_flow = (int)$request->payment_flow;
        $pay_method->key_type = (int)$request->key_type;
        $pay_method->acquiring = getFloatFormat($request->acquiring);

        $pay_method->save();
        return parentReload();
    }

    public function destroy($id)
    {
        LogEvents::event($id, LogEvents::EVENT_DEFAULT_NAME[LogEvents::LOG_DELETE], 6);

        PayMethod::findOrFail($id)->delete();

        return response('', 200);
    }

}
