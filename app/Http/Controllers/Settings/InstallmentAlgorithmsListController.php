<?php

namespace App\Http\Controllers\Settings;

use App\Models\Directories\InstallmentAlgorithms;
use App\Models\Log\LogEvents;
use App\Models\Settings\InstallmentAlgorithmsList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InstallmentAlgorithmsListController extends Controller
{
    public function __construct()
    {
        $this->middleware('permissions:settings,installment_algorithms_list');
    }

    public function index()
    {
        return view('settings.installment_algorithms_list.index', [
            'algoritms' => InstallmentAlgorithmsList::orderBy('title')->get()
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('settings.installment_algorithms_list.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $algoritm = new InstallmentAlgorithmsList;
        $algoritm->save();

        return $this->save($algoritm, $request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('settings.installment_algorithms_list.edit', [
            'algoritm' => InstallmentAlgorithmsList::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $algoritm = InstallmentAlgorithmsList::findOrFail($id);

        return $this->save($algoritm, $request);
    }

    private function save(InstallmentAlgorithmsList $algoritm, Request $request)
    {
        $algoritm->title = $request->title;
        $algoritm->quantity = $request->quantity;
        $algoritm->save();

        return parentReload();
    }

    public function destroy($id)
    {
        InstallmentAlgorithmsList::findOrFail($id)->delete();

        return response('', 200);
    }

    public function get_quantity(Request $request){

        $algoritm_id = InstallmentAlgorithms::findOrFail($request->algorithm_id)->algorithm_id;
        $algoritm = InstallmentAlgorithmsList::findOrFail($algoritm_id);

        return $algoritm->quantity;
    }
}
