<?php

namespace App\Http\Controllers\Cashbox\IncomesExpenses;

use App\Helpers\Visible;
use App\Http\Controllers\Controller;
use App\Models\Cashbox\IncomeExpense;
use Illuminate\Http\Request;
use App\Repositories\FilesRepository;
use App\Models\Cashbox\IncomeDocuments;


class IncomesExpensesFileController extends Controller {

    public function __construct(FilesRepository $filesRepository) {
        $this->filesRepository = $filesRepository;
        $this->middleware('permissions:cashbox,incomes_expenses');
    }

    public function addFiles($id, Request $request) {
        $income_expense = IncomeExpense::query();
        $income_expense = Visible::apply($income_expense, 'cashbox')->findOrFail($id);

        if (!$income_expense) {
            abort(404);
        }
        $file = $request->file("file");

        $contract_doc = IncomeDocuments::create([
                    'income_id' => $id,
                    'file_id' => $this->filesRepository->makeFile($file, IncomeDocuments::FILES_DOC . "/$id/")->id,
        ]);

        return $contract_doc->id;
    }

    public function deleteFiles($id, $file_id) {
        $income_expense = IncomeExpense::query();
        $income_expense = Visible::apply($income_expense, 'cashbox')->findOrFail($id);
        if ($income_expense) {

            $documents = IncomeDocuments::where("income_id", $id)->whereHas('file', function ($query) use ($file_id) {
                return $query->where('name', '=', $file_id);
            });


            $fileDelete = app()->make('\App\Http\Controllers\FilesController')
                    ->callAction('destroy', [$file_id]);


            if ($documents->delete() && $fileDelete) {
                return 1;
            }
        }
    }

}
