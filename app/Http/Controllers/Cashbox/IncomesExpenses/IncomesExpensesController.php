<?php

namespace App\Http\Controllers\Cashbox\IncomesExpenses;

use App\Classes\Export\ExportManager;
use App\Helpers\PaginationHelper;
use App\Helpers\Visible;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Cashbox\IncomesExpenses\IncomeExpenseQueryHandler;
use App\Http\Requests\Cashbox\IncomesExpenses\IncomesExpensesListRequest;
use App\Models\Cashbox\IncomeExpense;
use App\Models\Settings\IncomeExpenseCategory;
use App\Models\Settings\TemplateCategory;
use Illuminate\Http\Request;

class IncomesExpensesController extends Controller
{

    public function __construct()
    {

        $this->middleware('permissions:cashbox,incomes_expenses');

        $this->breadcrumbs[] = [
            'label' => 'Расходы/Доп доходы',
            'url' => 'cashbox/incomes_expenses'
        ];
    }

    public function index()
    {

        if (!auth()->user()->cashbox)
            return view('errors.not_cashbox');

        return view('cashbox.incomes_expenses.index');
    }

    public function get_incomes_expenses_list(IncomesExpensesListRequest $request)
    {

        $incomes_expenses = IncomeExpense::query();
        $incomes_expenses = Visible::apply($incomes_expenses, 'cashbox');
        $incomes_expenses = (new IncomeExpenseQueryHandler($incomes_expenses))->allowEmpty()->apply();

        $incomes_expenses_spending = [];
        $incomes_expenses_getting = [];

        if (request('divide') == 1) {

            $incomes_expenses_spending = clone $incomes_expenses;
            $incomes_expenses_getting = clone $incomes_expenses;

            $incomes_expenses_spending->whereHas('category', function ($query) {
                return $query->where('type', '=', 2);
            });
            $incomes_expenses_getting->whereHas('category', function ($query) {
                return $query->where('type', '=', 1);
            });

            $incomes_expenses_spending = $incomes_expenses_spending->get();
            $incomes_expenses_getting = $incomes_expenses_getting->get();
        }


        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
        $result = PaginationHelper::paginate($incomes_expenses, $page, $page_count);

        $incomes_expenses = $result['builder']->get();


        return [
            'incomes_expenses' => $incomes_expenses,
            'incomes_expenses_spending' => $incomes_expenses_spending,
            'incomes_expenses_getting' => $incomes_expenses_getting,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];
    }

    public function get_incomes_expenses_table(IncomesExpensesListRequest $request)
    {

        $data = $this->get_incomes_expenses_list($request);
        if ($request->divide) {
            $data['html'] = view('cashbox.incomes_expenses.divide_tables', $data)->render();
        } else {
            $data['html'] = view('cashbox.incomes_expenses.table', $data)->render();
        }


        return $data;
    }

    public function create()
    {

        return view('cashbox.incomes_expenses.create');
    }

    public function store()
    {

        $this->validate(request(), [
            'category_id' => 'required|integer|min:1',
            'payment_user' => 'integer',
            'date' => 'date',
        ]);

        if ((int)getFloatFormat(request()->get('sum')) <= 0) {
            return redirect()->back()->with('error', 'Ошибка суммы');
        }

        $income_expense = new IncomeExpense();
        $income_expense->category_id = request('category_id');
        $income_expense->user_id = auth()->id();
        $income_expense->payment_type = request('payment_type');
        $income_expense->status_id = 1;
        $income_expense->payment_user_id = request('payment_user');
        $income_expense->date = setDateTimeFormat(request('date'));
        $income_expense->sum = getFloatFormat(request('sum'));
        $income_expense->commission = getFloatFormat(request('commission'));
        $income_expense->comment = request('comment');
        $income_expense->commission_total = getTotalSumToPrice($income_expense->sum, $income_expense->commission);
        $income_expense->total = $income_expense->sum + $income_expense->commission_total;
        $income_expense->save();

        return redirect("/cashbox/incomes_expenses/{$income_expense->id}/edit");
    }

    public function get_categories()
    {
        $this->validate(request(), ['category_type' => 'integer']);
        $categories = IncomeExpenseCategory::query()->where('type', request('category_type'))->get();
        return response()->json($categories);
    }

    public function edit($id)
    {

        $income_expense = IncomeExpense::query();
        $income_expense = Visible::apply($income_expense, 'cashbox')->findOrFail($id);

        $this->breadcrumbs[] = [
            'label' => 'Редактировать',
        ];

        return view('cashbox.incomes_expenses.edit', [
            'income_expense' => $income_expense
        ])->with('breadcrumbs', $this->breadcrumbs);
    }

    public function save($id)
    {

        $this->validate(request(), [
            'category_id' => 'required|integer|min:1',
            'payment_user' => 'integer',
            'date' => 'date',
        ]);

        if ((int)getFloatFormat(request()->get('sum')) <= 0) {
            return redirect()->back()->with('error', 'Ошибка суммы');
        }

        $income_expense = IncomeExpense::find($id);
        $income_expense->category_id = request('category_id');
        $income_expense->payment_type = request('payment_type');
        $income_expense->status_id = $income_expense->status_id > 1 ? $income_expense->status_id : 1;
        $income_expense->payment_user_id = request('payment_user');
        $income_expense->date = setDateTimeFormat(request('date'));
        $income_expense->sum = getFloatFormat(request('sum'));
        $income_expense->commission = getFloatFormat(request('commission'));
        $income_expense->comment = request('comment');
        $income_expense->commission_total = getTotalSumToPrice($income_expense->sum, $income_expense->commission);
        $income_expense->total = $income_expense->sum + $income_expense->commission_total;

        $income_expense->save();


        return redirect()->back()->with('success', 'Успешно сохранено')->withInput();
    }

    public function accept($id, Request $request)
    {
        $income_expense = IncomeExpense::find($id);


        $cashbox = auth()->user()->cashbox;
        $msg = '';
        $state = $income_expense->status_id;

        if ($income_expense->payment_type == 0 && $income_expense->category->type == 2) {
            if ($cashbox->balance < $income_expense->total) {
                $msg = 'В кассе недостаточно средств!';
                $state = 0;
            }
        }



        if ($state == 1) {
            $event_date = date("Y-m-d H:i:s");
            $income_expense->status_id = 2;
            $income_expense->payment_date = $event_date;
            $income_expense->user_id = auth()->id();

            $purpose_user_name = $income_expense->payment_user ? $income_expense->payment_user->name : "";
            $purpose_default = "#{$income_expense->id} {$income_expense->category->title} {$purpose_user_name}: {$income_expense->comment}";


            if ($income_expense->category->type == 2) {
                $purpose_payment = "Оплаты расходов $purpose_default";
                $event_type_id = 1;
            } else {
                $purpose_payment = "Приход доходов $purpose_default";
                $event_type_id = 0;
            }

            $cashbox->setTransaction($income_expense->payment_type, $event_type_id, $income_expense->total, $event_date, $purpose_payment, auth()->id());


            $income_expense->save();

        }


        return response()->json(['msg' => $msg, "state" => $state]);
    }


    public function export($id, Request $request)
    {

        $type = $request->type ? $request->type : 1;
        if ($type == 1) {
            $category = TemplateCategory::get('income_expense_comming');
        } elseif ($type == 2) {
            $category = TemplateCategory::get('income_expense_spending');
        }

        $builder = IncomeExpense::query()->where('id', $id);
        $builder->firstOrFail();

        return (new ExportManager($category, $builder))->handle();

    }

}
