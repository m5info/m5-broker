<?php

namespace App\Http\Controllers\Cashbox\ActsToUnderwriting;

use App\Helpers\PaginationHelper;
use App\Http\Controllers\Controller;

use App\Http\QueryHandlers\Cashbox\ActsToUnderwriting\ActsListQueryHandler;
use App\Http\QueryHandlers\Cashbox\ActsToUnderwriting\ContractListQueryHandler;
use App\Models\BSO\Acts\ToUnderwritingActs;
use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoActsItems;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Models\Finance\Invoice;
use App\Repositories\FilesRepository;
use Illuminate\Http\Request;

class ActsToUnderwritingController extends Controller
{

    protected $filesRepository;

    public function __construct(FilesRepository $filesRepository)
    {
        $this->middleware('permissions:cashbox,acts_to_underwriting');
        $this->filesRepository = $filesRepository;
    }

    public function index()
    {
        return view('cashbox.acts_to_underwriting.index');
    }

    public function get_view(Request $request)
    {
        return view('cashbox.acts_to_underwriting.info.' . $request->load);
    }

    public function acts_list()
    {

        $this->validate(request(), [
            'page_count' => 'integer',
            'agent_id' => 'integer',
            'date_from' => 'date',
            'number' => 'string',
            'date_to' => 'date',
            'PAGE' => 'integer',
        ]);

        $bso_act = ToUnderwritingActs::query()
            ->with('user_from');

        $bso_act = (new ActsListQueryHandler($bso_act))->allowEmpty()->apply();
        $bso_act->orderBy('id', 'desc');


        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
        $result = PaginationHelper::paginate($bso_act, $page, $page_count);

        $bso_act = $result['builder']->get();

        $html = view('cashbox.acts_to_underwriting.info.acts.list', [
            'bso_act' => $bso_act,
        ])->render();

        return [
            'html' => $html,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];
    }


    public function contract_list()
    {
        $this->validate(request(), [
            'kind_acceptance' => 'integer',
            'agent_id' => 'integer',
            'product' => 'integer',
            'date_from' => 'date',
            'date_to' => 'date',
            'sk' => 'integer',
        ]);

        $builder = Payments::query()
            ->leftJoin('invoices', 'invoices.id', '=', 'payments.invoice_id')
            ->leftJoin('contracts', 'contracts.id', '=', 'payments.contract_id')
            ->whereIn('invoices.status_id', [2, 3])
            ->where('contracts.statys_id',  5)
            ->where('payments.acts_to_underwriting_id', 0);

        if(auth()->user()->role->rolesVisibility(8)->visibility == 2){//Только свои
            $builder->where('payments.cashbox_user_id', auth()->id());
        }

        $builder->select(["payments.*"]);
        $payments = (new ContractListQueryHandler($builder))->allowEmpty()->apply();
        $payments->orderBy('payments.id', 'desc');



        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
        $result = PaginationHelper::paginate($payments, $page, $page_count);


        $payments = $result['builder']->get();

        $html = view('cashbox.acts_to_underwriting.info.contract.list', [
            'payments' => $payments,
        ])->render();

        return [
            'html' => $html,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];
    }


    public function submitted_list()
    {
        $this->validate(request(), [
            'kind_acceptance' => 'integer',
            'agent_id' => 'integer',
            'product' => 'integer',
            'date_from' => 'date',
            'date_to' => 'date',
            'sk' => 'integer',
        ]);


        $builder = Payments::query()
            ->leftJoin('invoices', 'invoices.id', '=', 'payments.invoice_id')
            ->leftJoin('contracts', 'contracts.id', '=', 'payments.contract_id')
            ->whereIn('invoices.status_id', [2, 3])
            ->whereIn('contracts.statys_id',  [1, 2, 4, 7])
            ->where('payments.acts_to_underwriting_id','>', 0);

        if(auth()->user()->role->rolesVisibility(8)->visibility == 2){//Только свои
            $builder->where('payments.cashbox_user_id', auth()->id());
        }

        $builder->select(["payments.*"]);
        $payments = (new ContractListQueryHandler($builder))->allowEmpty()->apply();
        $payments->orderBy('payments.id', 'desc');

        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
        $result = PaginationHelper::paginate($payments, $page, $page_count);


        $payments = $result['builder']->get();

        $html = view('cashbox.acts_to_underwriting.info.submitted.list', [
            'payments' => $payments,
        ])->render();


        return [
            'html' => $html,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];
    }


    public function create_get_acts()
    {

        $this->validate(request(), [
            'agent_id' => 'integer',
            'payment_array.*' => 'integer',
        ]);

        $payment_array = array_map('intval', json_decode(request('payment_array', []), true));
        $bso_act = BsoActs::createToUnderwritingAct(request('agent_id'));
        $payments = Payments::query()->whereIn('id', $payment_array);

        $payments->update(['acts_to_underwriting_id' => $bso_act->id]);

        foreach ($payments->get() as $payment) {

            BsoLogs::setLogs($payment->bso->id, $bso_act->bso_state_id, 9, $bso_act->id, auth()->id());

            BsoActsItems::create([
                'bso_act_id' => $bso_act->id,
                'bso_id' => $payment->bso->id,
                'bso_title' => $payment->bso->bso_title,
            ]);
        }

        return response(200);

    }


}
