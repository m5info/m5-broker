<?php

namespace App\Http\Controllers\Cashbox\ActsToUnderwriting\Details;

use App\Classes\Export\ExportManager;
use App\Http\Controllers\Controller;
use App\Models\BSO\Acts\ToUnderwritingActs;
use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoActsItems;
use App\Models\BSO\BsoItem;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Models\Settings\TemplateCategory;
use Illuminate\Http\Request;

class ActsToUnderwritingDetailsController extends Controller
{


    public function __construct()
    {
        $this->middleware('permissions:cashbox,acts_to_underwriting');
    }


    public function index($act_id)
    {
        $bso_act = BsoActs::getActId($act_id);

        $payments = Payments::where('acts_to_underwriting_id', $act_id);

        $this->breadcrumbs[] = [
            'label' => 'Акты передачи БСО в Андеррайтинг',
            'url' => 'cashbox/acts_to_underwriting',
        ];

        $this->breadcrumbs[] = [
            'label' => 'Редактирование',
        ];

        return view('cashbox.acts_to_underwriting.details', [
            'act' => $bso_act,
            'payments' => $payments->get(),
        ])->with('breadcrumbs', $this->breadcrumbs);

    }


    public function export($act_id){

        $builder = ToUnderwritingActs::query()->where('id', $act_id);
        $category = TemplateCategory::get('bso_to_underwriting');
        $builder->firstOrFail();

//        return (new ExportManager($category, $builder))->handle();
        return (new ExportManager($category, $builder, null, true))->handle();

    }


    public function delete_items($act_id, Request $request)
    {
        $item_array = \GuzzleHttp\json_decode($request->get('item_array'));
        $act = BsoActs::getActId($act_id);
        $act->deleteItemsToUnderwritingActs($item_array);
        return response(200);
    }

    public function delete_act($act_id){
        $act = BsoActs::getActId($act_id);
        $act->deleteToUnderwritingActs();
        return response(200);
    }

}
