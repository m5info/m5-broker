<?php

namespace App\Http\Controllers\Cashbox\OFDCashbox;


use App\Http\Controllers\Controller;
use App\Models\Contracts\FNS;
use App\Models\Organizations\Organization;
use App\Models\User;
use App\Services\FNS\FNSFerma;
use Illuminate\Http\Request;

class OFDCashboxController extends Controller
{


    public function __construct()
    {
        $this->middleware('permissions:cashbox,ofd_cashbox');
    }


    public function index()
    {


        return view('cashbox.ofd_cashbox.index', [
            'organizations' => Organization::where('cashbox_api_type_id','>', 0)->get(),

        ]);

    }

    public function view($org_id)
    {

        $organization = Organization::findOrFail($org_id);

        return view('cashbox.ofd_cashbox.view', [
            'organization' => $organization,
            'ofd_cashbox' => $organization->ofd_cashbox,
        ]);
    }

    public function edit($org_id, $id)
    {
        $organization = Organization::findOrFail($org_id);

        if((int)$id>0){
            $ofd = FNS::findOrFail($id);
        }else{
            $ofd = new FNS();
        }

        $agents = User::getALLUser(24)->pluck('name', 'id');

        $response_json = (isset($ofd->response_json) && strlen($ofd->response_json)>10)?\GuzzleHttp\json_decode($ofd->response_json):null;

        return view('cashbox.ofd_cashbox.ofd.edit', [
            'organization' => $organization,
            'ofd' => $ofd,
            'agents' => $agents,
            'response_json' => $response_json
        ]);

    }

    public function show($org_id, $id)
    {
        $organization = Organization::findOrFail($org_id);

        $ofd = FNS::findOrFail($id);

        $response_json = (isset($ofd->response_json) && strlen($ofd->response_json)>10)?\GuzzleHttp\json_decode($ofd->response_json):null;

        return view('cashbox.ofd_cashbox.ofd.show', [
            'organization' => $organization,
            'ofd' => $ofd,
            'response_json' => $response_json
        ]);

    }




    public function save($org_id, $id, Request $request)
    {

        if((int)$id>0){
            $ofd = FNS::findOrFail($id);
        }else{
            $ofd = new FNS();
            $ofd->create_user_id = auth()->id();
            $ofd->save();
        }

        $ofd->org_id = $org_id;
        $ofd->client_type_id = (int)$request->client_type_id;
        $ofd->client_title = ((int)$request->client_type_id == 0) ? $request->client_fio : $request->client_title;
        $ofd->client_doc_serie = $request->client_doc_serie;
        $ofd->client_doc_number = $request->client_doc_number;
        $ofd->client_inn = $request->client_inn;
        $ofd->client_phone = $request->client_phone;
        $ofd->client_email = $request->client_email;


        $ofd->create_date = setDateTimeFormat($request->create_date.' '.$request->create_time);
        $ofd->user_id = (int)$request->user_id;
        $ofd->location_create_date = getDateTime();

        $ofd->type_id = (int)$request->type_id;
        $ofd->payment_method_id = (int)$request->payment_method_id;
        $ofd->payment_type = (int)$request->payment_type;

        $ofd->payment_total = getFloatFormat($request->payment_total);
        $ofd->product_title = $request->product_title;

        $ofd->save();

        if((int)$request->status_cashbox == 0){
            return redirect("/cashbox/ofd_cashbox/{$org_id}/edit/{$ofd->id}")->with('success', trans('form.success_update'));
        }else {


            $ferms = new FNSFerma($ofd->organization);
            $ferms->sendDataKKT($ofd);

            if ($ofd->status == 'Success'){
                return redirect("/cashbox/ofd_cashbox/{$org_id}/show/{$ofd->id}");
            }

            return redirect("/cashbox/ofd_cashbox/{$org_id}/edit/{$ofd->id}");
            //return parentReload();
        }

    }


}
