<?php

namespace App\Http\Controllers\Cashbox\FoundationPayments;

use App\Helpers\Visible;
use App\Http\Controllers\Cashbox\IncomesExpenses\IncomesExpensesController;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Cashbox\IncomesExpenses\IncomeExpenseQueryHandler;
use App\Http\Requests\Cashbox\IncomesExpenses\IncomesExpensesListRequest;
use App\Models\Cashbox\Cashbox;
use App\Models\Cashbox\CashboxTransactions;
use App\Models\Cashbox\FoundationPayments;
use App\Models\Cashbox\IncomeExpense;
use App\Models\Reports\ReportOrders;
use App\Models\Users\UsersBalance;
use Illuminate\Http\Request;

class FoundationPaymentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('permissions:cashbox,foundation_payments');
        $this->breadcrumbs[] = [
            'label' => 'Учредительские выплаты',
            'url' => 'cashbox/foundation_payments',
        ];

    }

    public function index(Request $request)
    {
        $year = request('year') ? request()->query('year') : date("Y");

        return view('cashbox.foundation_payments.index', [
            'year' => $year
        ]);

    }


    public function info($month, $year)
    {
        $this->breadcrumbs[] = [
            'label' => $year,
        ];

        $this->breadcrumbs[] = [
            'label' => getOneMonth($month),
        ];

        $result = FoundationPayments::getInfoReportData($month, $year);



        return view('cashbox.foundation_payments.info.info', [
            'result' => $result,
            'month' => $month,
            'year' => $year
        ])->with('breadcrumbs', $this->breadcrumbs);


    }

    public function detail($month, $year, $type, IncomesExpensesListRequest $request){

        $temp_type = $type == 'report' ? $type : 'incomes_expenses';
        $a = FoundationPayments::query()->where('month', $month)->where('year', $year)->first();
        $accept_date = $a ? $a->accept_date : null;

        return view("cashbox.foundation_payments.info.detail.$temp_type", [
            $temp_type => $this->get_list_for_detail($month, $year, $type, $request),
            'accept_date' => new \DateTime($accept_date),
            'month' => $month,
            'year' => $year,
            'type' => $type,
        ]);
    }

    public function get_list_for_detail($month, $year, $type, IncomesExpensesListRequest $request) {

        $result = new \stdClass();

        $month = (int)$month;
        $month_title = ((int)$month<=9)?'0'.$month:$month;

        $start_date = "$year-$month_title-01 00:00:00";
        $countDay = $number = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $end_date = "$year-$month_title-$countDay 23:59:59";

        if($type == 'report'){ // тип fancybox

            $result = ReportOrders::where('report_year', $year)->where('report_month', $month)->where('is_deleted', 0)->where('type_id', 0); // ОТЧЕТЫ

        }else{

            $incomes_expenses = IncomeExpense::where('incomes_expenses.date', '>=',$start_date)->where('incomes_expenses.date', '<=',$end_date)->where('status_id', 2)->orderBy('payment_date', 'asc');
            $incomes_expenses->leftJoin('incomes_expenses_categories', 'incomes_expenses_categories.id', '=', 'incomes_expenses.category_id');
            $incomes_expenses->where('incomes_expenses_categories.counting_in_foundation_payments', 1);

            $result = $incomes_expenses->where('incomes_expenses_categories.type', $type == 'incomes' ? 1 : 2); // Расходы/Доходы
        }

        return $result->get();
    }

    public function accept_foundation_payments($month, $year, Request $request)
    {

        $info = FoundationPayments::getInfoReportData($month, $year);

        // получаем отчеты проставляем что они уже были зафиксированны
        $reports = ReportOrders::where('report_year', $year)->where('report_month', $month)->where('is_deleted', 0)->where('type_id', 0)->update(['is_recorded_to_foundations' => 1]);

        $dates = getDateFormat($request->dates);
        //FoundationPayments::where('month', (int)$month)->where('year', (int)$year)->delete();

        if ($info->status_id == 0){
            $data = [
                'month' => (int)$month,
                'year' => (int)$year,
                'accept_date' => $dates,
                'user_id' => auth()->id(),
                'status_id' => 1,
                'fact_report_total' => $info->report_total,
                'fact_incomes_total' => $info->incomes_total,
                'fact_expenses_total' => $info->expenses_total,
                'fact_profit_total' => $info->profit_total,
            ];
        }else{
            $data = [
                'payment_date' => $dates,
                'payment_report_total' => $info->report_total,
                'payment_incomes_total' => $info->incomes_total,
                'payment_expenses_total' => $info->expenses_total,
                'payment_profit_total' => $info->profit_total,
            ];
        }



        //FoundationPayments::create([
        FoundationPayments::where('month', (int)$month)->where('year', (int)$year)->update($data);

        return response(200);

    }


    public function incomes_expenses($month, $year, Request $request)
    {
        $total = getFloatFormat($request->total);
        $fact_id = FoundationPayments::getInfoReportData($month, $year)->fact_id;


        return view('cashbox.foundation_payments.info.incomes_expenses', [
            'fact_id' => $fact_id,
            'total' => $total,
        ]);

    }

    public function save_incomes_expenses(Request $request)
    {

        $income_expense = new IncomeExpense();
        $income_expense->category_id = request('category_id');
        $income_expense->user_id = auth()->id();
        $income_expense->payment_type = request('payment_type');
        $income_expense->status_id = 1;
        $income_expense->date = setDateTimeFormat(request('date'));
        $income_expense->sum = getFloatFormat(request('sum'));
        $income_expense->commission_total = getTotalSumToPrice($income_expense->sum, 0);
        $income_expense->total = $income_expense->sum+$income_expense->commission_total;
        $income_expense->foundation_payments_id = request('fact_id');
        $income_expense->save();

        return parentReload();


    }



    public function foundation($month, $year, Request $request)
    {
        $total = getFloatFormat($request->total);
        $fact_id = FoundationPayments::getInfoReportData($month, $year)->fact_id;


        return view('cashbox.foundation_payments.info.foundation.index', [
            'fact_id' => $fact_id,
            'total' => $total,
        ]);

    }


    public function save_foundation(Request $request)
    {
        $info = FoundationPayments::getInfoReportData($request->month, $request->year);


        //Списание
        $expense = IncomeExpense::createIncomeExpense($request->category_id, auth()->id(), 1, setDateTimeFormat($request->date), getFloatFormat($request->sum), 0, $request->fact_id, $request->comment);
        $expense->status_id = 2;
        $expense->save();

        //Начисление
        $income = IncomeExpense::createIncomeExpense($request->income_category_id, auth()->id(), 1, setDateTimeFormat($request->date), getFloatFormat($request->sum), 0, $info->fact_id, $request->comment);
        $income->status_id = 2;
        $income->save();

        return parentReload();

    }



    public function balance_foundation_payments($month, $year, Request $request)
    {

        $fact = FoundationPayments::getInfoReportData($month, $year);


        return view('cashbox.foundation_payments.info.foundation.balance', [
            'fact' => $fact,
        ]);

    }


    public function set_balance_foundation_payments(Request $request)
    {

        $fact = FoundationPayments::getIdReportData($request->fact_id);

        $agent_id = $request->agent_id;
        $sum = getFloatFormat($request->sum);
        $comments = $request->comments;
        $balance_transfer = UsersBalance::find($request->balance_id);

        $month = getMonthById($fact->month);
        $comment = "Учредительские выплаты $month - {$fact->year} на {$balance_transfer->type_balanse->title} $comments";
        $transaction = $balance_transfer->setTransactions(0, 0, $sum, date('Y-m-d H:i:s'), $comment);


        $fact->foundation_payments_balance()->attach($transaction->id);

        return parentReload();


    }





}
