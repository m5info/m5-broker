<?php

namespace App\Http\Controllers\Cashbox\OperationList;


use App\Http\Controllers\Controller;
use App\Models\Cashbox\Cashbox;

class OperationListController extends Controller
{


    public function __construct()
    {
        $this->middleware('permissions:cashbox,operation_list');
    }


    public function index()
    {
        if(!auth()->user()->hasPermission('cashbox', 'collection')){

            if(auth()->user()->cashbox){
                $cashbox_id = auth()->user()->cashbox->id;
                return redirect("/cashbox/operation_list/{$cashbox_id}/show");
            }else{
                return view('errors.403', ['exception' => 1]);
            }

        }
        return view('cashbox.collection.index', [
            'cashbox' => Cashbox::all(),

        ]);

    }

    public function operation_list($id)
    {

        if ((int)$id > 0) {

            $cashbox = Cashbox::find($id);

        } else {

            $cashbox = new Cashbox();

        }


        $this->breadcrumbs[] = [
            'label' => 'Список операций',
            'url' => 'cashbox/operation_list',
        ];

        $this->breadcrumbs[] = [
            'label' => 'Редактирование',
        ];

        return view('cashbox.operation_list.show', [
            'cashbox' => $cashbox,
        ])->with('breadcrumbs', $this->breadcrumbs);

    }


}
