<?php

namespace App\Http\Controllers\Cashbox\Invoice;

use App\Domain\Processes\Operations\Payments\PaymentStatus;
use App\Domain\Processes\Scenaries\Contracts\Salaries\SalariesController;
use App\Helpers\PaginationHelper;
use App\Http\Controllers\Controller;


use App\Http\QueryHandlers\Cashbox\Invoice\InvoicesQueryHandler;
use App\Models\Cashbox\CashboxTransactions;
use App\Models\Finance\Invoice;
use App\Models\Reports\ReportOrders;
use App\Models\User;
use App\Repositories\FilesRepository;
use Illuminate\Http\Request;

class CashboxInvoiceController extends Controller
{

    protected $filesRepository;

    public function __construct(FilesRepository $filesRepository)
    {
        $this->filesRepository = $filesRepository;
        $this->middleware('permissions:cashbox,invoice');
    }



    public function index()
    {

        if(!auth()->user()->cashbox) return view('errors.not_cashbox');

        $agent_id = session()->get('invoice.agent_id')?:auth()->id();

        if(auth()->user()->hasPermission('cashbox', 'invoices_type_view')){

            $count_arr = Invoice::getInvoicesCountArr();

            return view('cashbox.invoice.index_with_tabs', [
                'agent' => User::find($agent_id),
                "count_arr" => $count_arr,
            ]);
        }


        return view('cashbox.invoice.index', [
            'agent' => User::find($agent_id),
        ]);
    }

    public function get_invoices_tab(){
        return view('cashbox.invoice.info_tab');
    }

    public function get_invoices_table(){


        $this->validate(request(),[
            'agent_id' => 'integer',
            'type' => 'string',
            'status' => 'integer',
            'date_from' => 'date',
            'date_to' => 'date',
            'page_count' => 'integer',
            'PAGE' => 'integer',
            'invoice_number' => 'integer',
        ]);


        $invoices = (new InvoicesQueryHandler(Invoice::query()))->allowEmpty()->apply();

        session(['finance.agent_id' => request('agent_id', auth()->id())]);

        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
        $result = PaginationHelper::paginate($invoices, $page, $page_count);
        if(request('invoice_number')){
            $invoice_number = (int)request('invoice_number');
            $result['builder']->where('id', 'like', "%{$invoice_number}%");
        }
        $result['builder']->orderBy('created_at', 'desc')->with('payments','agent','org');

        $invoices = $result['builder']->get();
        $html = view('cashbox.invoice.info', [
            'invoices' => $invoices,
        ])->render();

        return [
            'html' => $html,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];

    }

    public function edit($id)
    {
        $invoice = Invoice::getInvoiceId($id);

        $invoice_info = $invoice->getInfoSum();

        $need_withdraw = 1;
        $bsos_withdrawed = 0;
        $bsos_count = $invoice->payments->count();

        foreach($invoice->payments as $payment){
            $bsos_withdrawed += ($payment->bso && $payment->bso->location_id == 4) ? 1 : 0;
        }

        if($bsos_withdrawed > $bsos_count/2){
            $need_withdraw = 0;
        }

        $this->breadcrumbs[] = [
            'label' => 'Счета',
            'url' => 'cashbox/invoice',
        ];

        $this->breadcrumbs[] = [
            'label' => 'Редактирование',
        ];

        return view('cashbox.invoice.edit', [
            'invoice' => $invoice,
            'invoice_info' => $invoice_info,
            'need_withdraw' => $need_withdraw,
        ])->with('breadcrumbs', $this->breadcrumbs);
    }


    public function data_invoice_payment($id, Request $request){

        $view_payment = 'payment_cash';
        if((int)$request->type_invoice_payment == 1 || $request->type_invoice_payment == 5) $view_payment = 'payment_cash';
        elseif((int)$request->type_invoice_payment == 2) $view_payment = 'payment_balance';
        elseif((int)$request->type_invoice_payment == 3) $view_payment = 'payment_broker_cashless';
        elseif((int)$request->type_invoice_payment == 6) $view_payment = 'payment_balance_parent';
        else $view_payment = 'payment_cashless';

        $invoice = Invoice::getInvoiceId($id);
        $invoice_info = $invoice->getInfoSum();

        return view("payments.invoice.partials.$view_payment", [
            'invoice' => $invoice,
            'invoice_info' => $invoice_info,
        ]);

    }

    public function in_waiting($id, Request $request){

        $invoice = Invoice::getInvoiceId($id);

        if((int)$request->withdraw_documents){

            //События БСО 4 Принят от агента BsoLog
            foreach ($invoice->payments as $payment){

                $payment->setBsoLogToPayment(4, 10);

                $payment->bso->update([
                    'location_id' => 4,
                    'user_id' => auth()->id()
                ]);

                if($payment->receipt){
                    $payment->receipt->update(['location_id' => 4]);
                }

                if($contract = $payment->contract){
                    $contract->update([
                        'statys_id' => 5,
                        'to_cashbox_date' => date('Y-m-d H:i:s', time())
                    ]);
                }

                SalariesController::setSalaries($payment);

            }

        }

        $invoice->status_id = 3;
        $invoice->save();

        return 1;
    }


    public function save($id, Request $request)
    {



        $invoice = Invoice::getInvoiceId($id);
        $invoice_info = $invoice->getInfoSum();

        $temp_invoice_status = $invoice->status_id;

        $result = new \stdClass();
        $result->state = 0;
        $result->error = '';



        if($invoice->status_id != 2){
            $type_invoice_payment = (int)$request->type_invoice_payment;
            $invoice->type_invoice_payment_id = $type_invoice_payment;
            $invoice->type = Invoice::TYPE_INVOICE_VAL[($type_invoice_payment == 2) ? 3 : $type_invoice_payment];
            $invoice->invoice_payment_com = $request->invoice_payment_comm ? $request->invoice_payment_comm : '';
            $invoice->save();

            session(['invoice.type_invoice_payment' => $type_invoice_payment]);

            if($type_invoice_payment == 1 || $type_invoice_payment == 5){//НАЛИЧНЫЕ
                $result = $invoice->setPaymentChash(getFloatFormat($request->invoice_payment_total), $invoice->id);

            }else{
                $cashbox_type = 1;

                if($type_invoice_payment == 2) {//БАЛАНС
                    session(['invoice.invoice_payment_balance' => $request->invoice_payment_balance]);
                    $result = $invoice->setPaymentBalance($request->invoice_payment_balance, $invoice->id);

                }elseif($type_invoice_payment == 6) {//БАЛАНС
                    session(['invoice.invoice_payment_balance' => $request->invoice_payment_balance]);
                    $result = $invoice->setPaymentBalanceParent($request->invoice_payment_balance, $invoice->id);

                }else{

                    if($request->file){
                        $file = $this->filesRepository->makeFile($request->file, Invoice::FILES_DOC.$invoice->id.'/');
                        $invoice->file_id = $file->id;
                    }
                    $invoice->save();


                    if($type_invoice_payment == 3) {//Безналичный Брокер
                        $cashbox_type = 1;
                        $result = $invoice->setPaymentBrokerNonChash(getFloatFormat($request->invoice_payment_total), $invoice->id);
                    }elseif($type_invoice_payment == 4) {//Безналичный СК
                        $cashbox_type = 2;
                        $result = $invoice->setPaymentNonChash();
                    }
                }

                $cashbox = auth()->user()->cashbox;

                $link = "#{$invoice->id}";

                $cashbox->setTransaction(
                    $cashbox_type,
                    0,
                    $invoice_info->total_sum,
                    date("Y-m-d H:i:s"),
                    "Оплата счета $link на сумму ".titleFloatFormat($invoice_info->total_sum),
                    auth()->id(),
                    $invoice->agent_id,
                    0,
                    $invoice->id);

            }


            //События БСО 12 Оплачен BsoLog
            foreach ($invoice->payments as $payment){

                if($payment->type_id == 0){
                    ReportOrders::saveBaseDataPayment($payment);
                }

                PaymentStatus::set($payment, 1);
                $payment->setBsoLogToPayment(12);
            }


            if($request->get('withdraw_documents') == 1 && $temp_invoice_status != 3){
                //События БСО 4 Принят от агента BsoLog
                foreach ($invoice->payments as $payment){


                    if($payment->bso->location_id == 1){
                        $payment->bso->update([
                            'location_id' => 4,
                            'user_id' => auth()->id()
                        ]);
                    }

                    $contract = $payment->contract;
                    //if($contract->statys_id != 4){
                        $contract->update([
                            'statys_id' => 5,
                            'to_cashbox_date' => date('Y-m-d H:i:s', time())
                        ]);
                    //}

                    $payment->setBsoLogToPayment(4, 10);

                    if($payment->receipt){
                        $payment->receipt->update(['location_id' => 4]);
                    }


                    //Указываем у кого сейчас платеж и обнуляем акт кассира
                    $payment->update([
                        'acts_to_underwriting_id' => 0,
                        'cashbox_user_id' => auth()->id()
                    ]);

                }
            }



        }else{
            $result->error = 'Ошибка счет оплачен!';
        }


        if($result->state == 0){
            return redirect(url("/cashbox/invoice/{$invoice->id}/edit/"))->with('error', $result->error);
        }

         return redirect(url("/finance/invoice/{$invoice->id}/view/"))->with('success', "Счет оплачен!");
//        return redirect(url("/cashbox/invoice/{$invoice->id}/edit/"))->with('success', "Счет оплачен!");
    }



    public function saveInvoiceTypePayment($id, Request $request)
    {
        $invoice = Invoice::getInvoiceId($id);
        $type_invoice_payment = (int)$request->type_invoice_payment;
        $invoice->type_invoice_payment_id = $type_invoice_payment;
        $invoice->invoice_payment_com = $request->invoice_payment_com ? $request->invoice_payment_com : '';

        $invoice->type = Invoice::TYPE_INVOICE_VAL[$type_invoice_payment];

        $invoice->save();

        $invoice->refreshInvoice();

        return response(200);
    }


}
