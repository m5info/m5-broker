<?php

namespace App\Http\Controllers\Cashbox\Collection;

use App\Http\Controllers\Controller;
use App\Models\Cashbox\Cashbox;
use App\Models\Cashbox\CashboxTransactions;
use Illuminate\Http\Request;

class CollectionController extends Controller
{

    public function __construct()
    {
        $this->breadcrumbs[] = [
            'label' => 'Инкассация',
            'url' => 'cashbox/collection',
        ];

    }

    public function index(Request $request)
    {
        if (!auth()->user()->hasPermission('cashbox', 'collection')) {
            return view('errors.403', ['exception' => 1]);
        }
        return view('cashbox.collection.index', [
            'cashbox' => Cashbox::all(),
        ]);
    }


    public function edit($id, Request $request)
    {

        $this->breadcrumbs[] = [
            'label' => 'Редактирование',
        ];

        if (!auth()->user()->hasPermission('cashbox', 'collection')) {
            return view('errors.403', ['exception' => 1]);
        }
        if ((int)$id > 0) {
            $cashbox = Cashbox::find($id);
        } else {
            $cashbox = new Cashbox();
        }

        return view('cashbox.collection.edit', [
            'cashbox' => $cashbox,
            'breadcrumbs' => $this->breadcrumbs,
        ]);

    }


    public function save($id, Request $request)
    {
        if ((int)$id > 0) {
            $cashbox = Cashbox::find($id);
        } else {
            $cashbox = new Cashbox();
        }

        $cashbox->title = $request->title;
        $cashbox->is_actual = (isset($request->is_actual)) ? (int)$request->is_actual : 0;
        $cashbox->organization_id = $request->organization_id;

        $cashbox->user_id = $request->user_id;
        $cashbox->max_balance = getFloatFormat($request->max_balance);
        $cashbox->save();

        return redirect("/cashbox/collection/{$cashbox->id}/edit")->with('success', trans('form.success_update'));

    }


    public function get_details($id, Request $request)
    {
        $type_id = $request->type_id;
        //$type_id = 0 - Транзакции  1 - Инкассации 2 - Приход/Расход (налчные)

        $cashbox = Cashbox::find($id);

        $transactions = CashboxTransactions::where('cashbox_id', $id);
        if($type_id == 1){
            $transactions->where('view_type_id', 1);
        }else{
            $transactions->where('view_type_id', 0);
        }

        if($type_id == 2){
            $transactions->where('type_id', 0);
        }

        //$transactions_last_day = clone $transactions;

        if (isset($request->begin_date) && strlen($request->begin_date) > 3) {
            $transactions->where('event_date', '>=', setDateTimeFormat("{$request->begin_date} 00:00:00"));
        }

        if (isset($request->type_payment_id) && $request->type_payment_id != '-1'){
            $transactions->where('type_id', $request->type_payment_id);
        }

        if (isset($request->end_date) && strlen($request->end_date) > 3) {
            $transactions->where('event_date', '<=', setDateTimeFormat("{$request->end_date} 23:59:59"));
        }

        if (isset($request->event_type_id) && $request->event_type_id > -1){
            $transactions->where('event_type_id', $request->event_type_id);
        }

        $transactions->orderBy('event_date', 'desc');



        if($type_id == 1){ //Инкассации
            return view('cashbox.collection.details.collection', [
                'transactions' => $transactions->get(),
            ]);
        }


        $transactions_all = clone $transactions;
        $transactions_coming = clone $transactions;
        $transactions_consumption = clone $transactions;

        $summary = [
            'all' => $transactions_all->sum('total_sum'),
            'coming' => $transactions_coming->where('event_type_id', 0)->sum('total_sum'),
            'consumption' => $transactions_consumption->where('event_type_id', 1)->sum('total_sum'),
        ];

        if (isset($request->divide) && $request->divide == 0) {
            //ТАБЛЦА




            return view('cashbox.collection.details.table', [
                'transactions' => $transactions->get(),
                'cashbox' => $cashbox,
                'summary' => $summary,
                'start_date' => $request->begin_date
            ]);

        }else{
            //КОЛОНК
            return view('cashbox.collection.details.half_details', [
                'transactions' => $transactions->get(),
                'cashbox' => $cashbox,
                'summary' => $summary,
                'start_date' => $request->begin_date
            ]);
        }




        //dd(getLaravelSql($transactions_last_day));




        if ($type_id == 2) {
            $transactions->where('user_id', auth()->id());
            $transactions_last_day->where('user_id', auth()->id());
        } else {
            if ($type_id != '-1') {
                $transactions->where('type_id', $type_id);
                $transactions_last_day->where('type_id', $type_id);
            }
        }




        $date = new \DateTime(); // For today/now, don't pass an arg.
        $date->modify("-1 day");

        $transactions_last_day->where('event_date', '>=', $date->format("Y-m-d")." 00:00:00");
        $transactions_last_day->where('event_date', '<=', $date->format("Y-m-d")." 23:59:59");

        $last_day_sum = 0;
        $last_day_sum_coming = 0;
        $last_day_sum_spend = 0;

        foreach ($transactions_last_day->get() as $trn) {
            $last_day_sum += $trn->total_sum;
            if ($trn->event_type_id == 0){
                $last_day_sum_coming += $trn->total_sum;
            }
            if ($trn->event_type_id == 1){
                $last_day_sum_spend += $trn->total_sum;
            }
        }



        $current_day_sum = 0;
        $current_day_sum_coming = 0;
        $current_day_sum_spend = 0;

        /* Итог по выборке */
        foreach ($transactions->get() as $trn) {
            $current_day_sum += $trn->total_sum;
            if ($trn->event_type_id == 0){
                $current_day_sum_coming += $trn->total_sum;
            }
            if ($trn->event_type_id == 1){
                $current_day_sum_spend += $trn->total_sum;
            }

        }


        if (isset($request->divide) && $request->divide == 0) {

            return view('cashbox.collection.details', [
                'type_id' => $type_id,
                'current_day_sum' => $current_day_sum,
                'current_day_sum_comming' => $current_day_sum_coming,
                'current_day_sum_spend' => $current_day_sum_spend,
                'last_day_sum' => $last_day_sum,
                'last_day_sum_comming' => $last_day_sum_coming,
                'last_day_sum_spend' => $last_day_sum_spend,
                'transactions' => $transactions->get(),
            ]);
        } else {
            return view('cashbox.collection.half_details', [
                'type_id' => $type_id,
                'current_day_sum' => $current_day_sum,
                'current_day_sum_comming' => $current_day_sum_coming,
                'current_day_sum_spend' => $current_day_sum_spend,
                'last_day_sum' => $last_day_sum,
                'last_day_sum_comming' => $last_day_sum_coming,
                'last_day_sum_spend' => $last_day_sum_spend,
                'transactions' => $transactions->get(),
            ]);
        }


    }

    public function view_balance($id)
    {
        $cashbox = Cashbox::find($id);
        return view('cashbox.collection.balance', [
            'cashbox' => $cashbox,
        ]);

    }


    public function set_balance($id, Request $request)
    {

        $sum  = $request->total_sum;

        $cashbox = Cashbox::find($id);

        if($cashbox->balance <= 0 || $cashbox->balance < $sum)
            return view('cashbox.collection.balance',[
                'cashbox' => $cashbox,
                'error_balance' => 'true'
            ]);

        $cashbox->setTransaction(
            $request->type_id,
            $request->event_type_id,
            getFloatFormat($request->total_sum),
            date("Y-m-d H:i:s"),
            $request->purpose_payment,
            auth()->id());

        return parentReload();


    }


    public function view_collection($id)
    {
        if(auth()->user()->cashbox){
            $cashbox = Cashbox::find($id);
            return view('cashbox.collection.collection', [
                'cashbox' => $cashbox,
            ]);
        }else{
            return view('errors.view_not_cashbox');
        }

    }


    public function set_collection($id, Request $request)
    {
        $user_collection = auth()->user();


        $cashbox = Cashbox::find($id);
        $cashbox->setTransaction(
            0,
            1,
            getFloatFormat($request->total_sum),
            date("Y-m-d H:i:s"),
            $request->purpose_payment,
            auth()->id(), 0, 1);

        if ($user_collection->cashbox) {
            $user_collection->cashbox->setTransaction(
                0,
                0,
                getFloatFormat($request->total_sum),
                date("Y-m-d H:i:s"),
                "Инкассация: " . $request->purpose_payment,
                auth()->id());
        }


        return parentReload();


    }


}
