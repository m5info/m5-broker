<?php

namespace App\Http\Controllers\Cashbox\Balance;

use App\Classes\Export\ExportManager;
use App\Http\Controllers\Controller;
use App\Models\Settings\TemplateCategory;
use App\Models\Settings\UserBalanceSettings;
use App\Models\User;
use App\Models\Users\UsersBalance;
use App\Models\Users\UsersBalanceTransactions;
use Illuminate\Http\Request;

class BalanceController extends Controller
{

    public function __construct()
    {
        $this->middleware('permissions:cashbox,user_balance');



    }

    public function index(Request $request)
    {

        if(!auth()->user()->cashbox) return view('errors.not_cashbox');
        $agent_id = session()->get('user_balance.agent_id')?:auth()->id();
        $balance_id = session()->get('user_balance.balance_id')?:1;


        return view('cashbox.balance.index', [
            'agent' => User::find($agent_id),
            'balance_id' => $balance_id
        ]);
    }

    public function get_balance(Request $request)
    {
        $agent_id = $request->agent_id;
        session(['user_balance.agent_id' => $agent_id]);
        $agent = User::find($agent_id);
        $balance_id = session()->get('user_balance.balance_id')?:1;
        $user_balances = $agent->getBalanceList();

        $result = new \stdClass();
        $result->select_balance = view('cashbox.balance.select_balance', [
            'agent' => $agent,
            'user_balances' => $user_balances,
            'balance_id' => $balance_id
        ])->render();

        $result->debts = view('cashbox.balance.agent_debts', [
            'agent' => $agent,
        ])->render();



        return response()->json($result);


    }

    public function export_balance($id)
    {
        $type = \request('transaction_type');

        if ($type == 0){
            $category = TemplateCategory::get('user_balance_comming');
        }elseif($type == 1){
            $category = TemplateCategory::get('user_balance_spending');
        }

        $builder = UsersBalanceTransactions::query()->where('id', $id);

        $builder->firstOrFail();
        return (new ExportManager($category, $builder))->handle();



    }

    public function get_pko()
    {

    }

    public function get_rko()
    {

    }


    public function get_data_balance(Request $request)
    {
        $agent_id = $request->agent_id;
        $balance_id = $request->balance_id;
        session(['user_balance.agent_id' => $agent_id]);
        session(['user_balance.balance_id' => $balance_id]);

        $agent = User::find($agent_id);

        $balance = $agent->getBalanceId($balance_id);

        $transactions = $balance->getTransactions();

        if(isset($request->type_id) && $request->type_id > -1){
            $transactions->where('type_id', $request->type_id);
        }

        if(isset($request->event_type_id) && $request->event_type_id > -1){
            $transactions->where('event_type_id', $request->event_type_id);
        }

        if(isset($request->begin_date) && strlen($request->begin_date) > 3){
            $transactions->where('event_date', '>=', setDateTimeFormat("{$request->begin_date} 00:00:00"));
        }

        if(isset($request->end_date) && strlen($request->end_date) > 3){
            $transactions->where('event_date', '<=', setDateTimeFormat("{$request->end_date} 23:59:59"));
        }

        $transactions->orderBy('event_date', 'desc')->orderBy('id', 'desc');

        return view('cashbox.balance.info', [
            'transactions' => $transactions->get(),
            'agent_id' => $agent_id
        ]);

    }

    public function edit($id)
    {
        //dd(\request()->balance_type);
        return view('cashbox.balance.edit', [
            'balance' => UsersBalance::find($id),
        ]);
    }


    public function save($id, Request $request)
    {
        if(getFloatFormat($request->total_sum) >0){
            $balance = UsersBalance::find($id);
            $balance->setTransactions(
                $request->type_id,
                $request->event_type_id,
                getFloatFormat($request->total_sum),
                date('Y-m-d H:i:s', strtotime($request->create_date.' '.$request->create_date_time.':'.date("s"))),
                $request->purpose_payment
            );

        }

        return parentReload();
    }


    public function transfer_view($id)
    {

        $balance = UsersBalance::find($id);
        $agent = $balance->agent;

        return view('cashbox.balance.transfer_view', [
            'balance' => $balance,
            'agent' => $agent
        ]);
    }

    public function transfer_save($id, Request $request)
    {
        $balance = UsersBalance::find($id);
        $balance_transfer = UsersBalance::find($request->balance_id);
        $agent = $balance->agent;

        $agent_transfer = $balance_transfer->agent;

        $total_sum = getFloatFormat($request->total_sum);
        $purpose_payment = $request->total_sum;

        if($balance->balance >= $total_sum){

            $balance->setTransactions(0, 1, $total_sum, date('Y-m-d H:i:s'), "Перевод на {$agent_transfer->name} {$balance_transfer->type_balanse->title}".$purpose_payment);
            $balance_transfer->setTransactions(0, 0, $total_sum, date('Y-m-d H:i:s'), "Перевод от {$agent->name} {$balance->type_balanse->title}".$purpose_payment);
            return parentReload();
        }

        return back()->with('error', 'Недостаточно средств!');

    }





}
