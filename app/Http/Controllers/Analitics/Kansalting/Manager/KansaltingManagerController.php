<?php

namespace App\Http\Controllers\Analitics\Kansalting\Manager;


use App\Http\Controllers\Controller;
use App\Models\User;
use DB;
use Illuminate\Http\Request;


class KansaltingManagerController extends Controller{


    public function __construct(){
        $this->middleware('permissions:analitics,k_manager');
    }

    public function index(){

        $source = null;
        $department = null;



        if(session('analitics.source')){
            $source = session('analitics.source');
            $department = session('analitics.department');
        }else{

            // Забираем из бд
            $source = $this->getOptions('SELECT distinct [ХешИсточников] as val FROM [еврогарант].[dbo].[cache_Консультирование] order by [ХешИсточников]');
            $department = $this->getOptions('SELECT distinct [СсылкаНаГруппуПользователей.НазваниеОбъекта] as val FROM [еврогарант].[dbo].[cache_Пользователь]  order by [СсылкаНаГруппуПользователей.НазваниеОбъекта]');

            session(['analitics.source' => $source]);
            session(['analitics.department' => $department]);
        }



        return view('analitics.kansalting.manager.index', [
            'source' => $source,
            'department' => $department
        ]);
    }

    public function getOptions($sql)
    {
        $res = [];
        $res[-1] = "Все";

        $result = MSSQLQueryResult($sql);

        foreach ($result as $key => $data){
            $res[$data->val] = $data->val;
        }


        return $res;
    }

    public function getWhere(Request $request)
    {
        $date_from = getDateFormatEn($request->date_from);
        $date_to = getDateFormatEn($request->date_to);
        $products = $request->products;

        $where = "(cast([cache_РезультатКонсультированияПоЗаявке].[ДатаСозданияОбъекта] as date) >= '{$date_from} 00:00:00' or
  cast([cache_РезультатКонсультированияПоЗаявке].[ДатаДостиженияРезультата] as date) >= '{$date_from} 00:00:00') and
  (cast([cache_РезультатКонсультированияПоЗаявке].[ДатаСозданияОбъекта] as date) <= '{$date_to} 23:59:59' or
  cast([cache_РезультатКонсультированияПоЗаявке].[ДатаДостиженияРезультата] as date) <= '{$date_to} 23:59:59')";

        if($products != -1){

        }

        if(isset($request->department) && is_array($request->department) && count($request->department) > 0){

            $where .= " and manager.[СсылкаНаГруппуПользователей.НазваниеОбъекта] in (".$this->getArryToIn($request->department).")";
        }

        if(isset($request->source) && is_array($request->source) && count($request->source) > 0){
            $where .= " and [СсылкаНаЗаявку/ИсточникЗаявления] in (".$this->getArryToIn($request->source).")";
        }

        $user = auth()->user();
        $visibility_obj = $user->role->rolesVisibility(4)->visibility;

        if($visibility_obj != 0){ //Не все
            $users_sub = User::where('path_parent', 'like', "%:{$user->id}:%")->where('front_user_id', '>', 0)->get()->pluck('front_user_id');
            $where .= " and [@РодительскийРаздел/СсылкаНаПользователя] in (".$this->getArryToIn($users_sub).")";
        }

        return $where;
    }


    public function getArryToIn($arr)
    {
        $res = '';
        foreach ($arr as $key => $a){
            $res .= " '$a'";
            if(($key+1) < count($arr)){
                $res .= ",";
            }
        }

        return $res;
    }



    public function getData(Request $request)
    {
        $where = $this->getWhere($request);


        $sql = "
        SELECT
[@РодительскийРаздел/СсылкаНаПользователя] as user_id,
manager.[НазваниеОбъекта] as magager_title,
manager.[СсылкаНаГруппуПользователей.НазваниеОбъекта] as manager_dep,



COUNT(*) as AllOrder,

sum(CASE
    WHEN [cache_РезультатКонсультированияПоЗаявке].[Результат] = 'В работе'
       THEN 1
       ELSE 0
END) as Work,


sum(CASE
    WHEN [cache_РезультатКонсультированияПоЗаявке].[Результат] = 'Отказался'
       THEN 1
       ELSE 0
END) as NotWork,
sum(CASE
    WHEN [cache_РезультатКонсультированияПоЗаявке].[Результат] = 'Направлено в отдел продаж' or
    [cache_РезультатКонсультированияПоЗаявке].[Результат] = 'Передано на контроль' or
    [cache_РезультатКонсультированияПоЗаявке].[Результат] = 'Направлено в отдел реализации'
       THEN 1
       ELSE 0
END) as FinishWork

  FROM [cache_РезультатКонсультированияПоЗаявке]
  left join [еврогарант].[dbo].[cache_Пользователь] as manager on manager.[id_node] = [@РодительскийРаздел/СсылкаНаПользователя]
  where
  {$where}
group by
  [@РодительскийРаздел/СсылкаНаПользователя], manager.[НазваниеОбъекта], manager.[СсылкаНаГруппуПользователей.НазваниеОбъекта]
  order by  manager.[СсылкаНаГруппуПользователей.НазваниеОбъекта], manager.[НазваниеОбъекта]
        ";

        return view('analitics.kansalting.manager.table', [
            'results' => MSSQLQueryResult($sql),
        ]);


    }



    public function getDetails($id, $status, Request $request)
    {
        $where = $this->getWhere($request);



        switch ($status) {
            case 'AllOrder':

                break;
            case 'Work':
                $where .= " and [cache_РезультатКонсультированияПоЗаявке].[Результат] = 'В работе'";
                break;
            case 'NotWork':
                $where .= " and [cache_РезультатКонсультированияПоЗаявке].[Результат] = 'Отказался'";
                break;
            case 'FinishWork':
                $where .= " and ([cache_РезультатКонсультированияПоЗаявке].[Результат] = 'Направлено в отдел продаж' or
    [cache_РезультатКонсультированияПоЗаявке].[Результат] = 'Передано на контроль' or
    [cache_РезультатКонсультированияПоЗаявке].[Результат] = 'Направлено в отдел реализации')";
                break;

        }


        $sql = "
SELECT
    [cache_РезультатКонсультированияПоЗаявке].[id_node] as id,
    [cache_РезультатКонсультированияПоЗаявке].[GuidCode] as guid,
    [@РодительскийРаздел/ПорядковыйНомер] as num,
    [@РодительскийРаздел/ДатаПерезвонаПредпоследняя] as date_last_phone,
    [@РодительскийРаздел/ДатаПерезвона] as date_phone,
    [ДатаДостиженияРезультата] as date_res,
    [СсылкаНаНаправление/ДатаВыезда] as date_deliveri,
    [@РодительскийРаздел/СсылкаНаПользователя.НазваниеОбъекта] as manager,
    [@РодительскийРаздел/СсылкаНаКлиента.НазваниеОбъекта] as client,
    [СсылкаНаЗаявку/ВидСтрахования] as product,
    [СсылкаНаЗаявку/ОбъектСтрахования] as obj_insh,
    [СсылкаНаЗаявку/ИсточникЗаявления] as source,
    [Результат] as status,
    [ИнаяПричинаОтказа] as not_work,
    [СсылкаНаНаправление/СтатусАкцепцииПолиса] as contract_status,
    [СсылкаНаНаправление/ПредочитаемаяСтраховаяКомпания] as sk_name,
    [СсылкаНаНаправление/СуммаВзносаПоПолису] as payment_total,
    [СсылкаНаНаправление/ПрибыльОператораПоЗавке] as payment_manager
FROM [cache_РезультатКонсультированияПоЗаявке]
left join [еврогарант].[dbo].[cache_Пользователь] as manager on manager.[id_node] = [@РодительскийРаздел/СсылкаНаПользователя]
where
  $where
and [@РодительскийРаздел/СсылкаНаПользователя] = {$id}";



        return view('analitics.kansalting.manager.details', [
            'results' => MSSQLQueryResult($sql),
        ]);

    }



    /*


     */



}