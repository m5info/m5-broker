<?php

namespace App\Http\Controllers\Analitics\Underfillers;

use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Analitics\Underfiller\UnderfillersContractsQueryHandler;
use App\Models\Characters\Underfiller;
use App\Models\Contracts\Contracts;
use App\Models\Directories\Products;

class UnderfillersAnalyticsController extends Controller {

    public function __construct() {
        $this->middleware('permissions:analitics,analytics_underfillers');
        $this->breadcrumbs[] = [
            'label' => 'Аналитика - Дозаполнение договоров',
            'url' => 'analitics/analytics_underfillers'
        ];
    }

    public function index() {
        return view('analitics.underfillers.index');
    }

    public function get_filters() {

        $underfillers = Underfiller::where('status_user_id', 0)->get();

        return view('analitics.underfillers.filters', [
            'underfillers' => $underfillers,
        ]);
    }

    public function get_charts() {

        $contracts = Contracts::getContractsQuery()
            ->where('underfiller_check',1)
            ->where('underfiller_id','>', 0);

        $contracts = (new UnderfillersContractsQueryHandler($contracts))->apply();


        $underfillers = Underfiller::query()
            ->whereIn('id', $contracts->pluck('underfiller_id')->unique())
            ->get();

        $result = [];

        $datasets = ['data' => [], 'backgroundColor' => [], 'label' => [],];


        $products = Products::all()->keyBy('id');
        $_products = $products->pluck('title', 'id');

        $all_count = 0;

        foreach ($underfillers as $underfiller){

            $color = dynamicColors();

            $contracts = (new UnderfillersContractsQueryHandler($underfiller->complete_contracts()->getQuery()))->apply()->get();

            $datasets['data'][] = $contracts->count();
            $datasets['backgroundColor'][] = $color;
            $datasets['label'][] = $underfiller->name;

            $productsStats = [];

            foreach ($_products as $productId => $productName) {

                $count = $contracts->filter(function ($item) use ($productId) {
                    return $item->product_id == $productId;
                })->count();

                $productsStats[$productId] = $count;
                $all_count += $count;

            }
            $result[] = [
                'underfiller_id' => $underfiller->id,
                'title' => $underfiller->name,
                'color' => $color,
                'total' => $contracts->count(),
                'products' => $productsStats,
            ];
        }

        $charts = [
            'agentspie' => [
                'type' => 'pie',
                'data' => [
                    'labels' => $datasets['label'],
                    'datasets' => [
                        [
                            'data' => $datasets['data'],
                            'backgroundColor' => $datasets['backgroundColor'],
                            'label' => 'Техандеррайтинг по агентам'
                        ]
                    ],
                ],
            ],
            'total' => [
                'type' => 'doughnut',
                'data' => [
                    'labels' => 'test',
                    'datasets' => [
                    ]
                ],
                'options' => [
                    'elements' => [
                        'center' => [
                            'text' => $all_count,
                            'color' => '#36A2EB',
                            'fontStyle' => 'Helvetica',
                            'sidePadding' => 15
                        ],
                    ],
                ],
            ],
        ];


        $res = [
            'result' => $result,
            'charts' => $charts,
            'products' => $_products,
        ];

        return response()->json($res);
    }

    public function details(){

        $this->validate(request(), [
            'underfiller_id' => 'integer',
            'product_id' => 'integer',
            'from' => 'date',
            'to' => 'date',
        ]);

        $contracts = Contracts::query()
            ->where('underfiller_check',1)
            ->where('underfiller_id','>', 0);


        $contracts = (new UnderfillersContractsQueryHandler($contracts))->apply();

        return view('analitics.underfillers.details.index', [
            'contracts' => $contracts->get()
        ]);

    }
}
