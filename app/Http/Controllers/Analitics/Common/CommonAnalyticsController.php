<?php

namespace App\Http\Controllers\Analitics\Common;

use App\Domain\Samplers\Contracts\TabsVisibility;
use App\Helpers\PaginationHelper;
use App\Helpers\Visible;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Analitics\Common\PaymentsQueryHandler;
use App\Http\Requests\Analitics\Common\PaymentListRequest;
use App\Models\Account\Users2Columns;
use App\Models\Characters\Agent;
use App\Models\Contracts\Payments;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\Products;
use App\Models\Organizations\Organization;
use App\Models\Settings\Department;
use App\Models\Settings\PointsSale;
use App\Models\User;
use Auth;
use DB;

class CommonAnalyticsController extends Controller
{
    // Для выгрузки Excel через POST
    const TEMP_EXPORT_STORAGE_FOLDER = 'analitics_common';



    public function index()
    {

        $insurances = InsuranceCompanies::orderBy("title")->get();

        $organizations = Organization::orderBy("title")->get();

        $departments = Department::all();

        $products = Products::orderBy("title")->get();

        $agents = Agent::getALLUser();

        $users = User::getALLUser();

        $points_sale = PointsSale::all();

        $table_key = 'analitics_common';

        $result['count_pagination'] = [
            -1 => 'Все', 25 => '25', 50 => '50', 100 => '100', 150 => '150'
        ];

        Auth::user()->hasPermission('analitics', 'limited_functionality_in_common_analytics') ? $limited_functionality_in_common_analytics = true : $limited_functionality_in_common_analytics = false;

        return view('analitics.common.index', [
            'organizations' => $organizations,
            'points_sale' => $points_sale,
            'departments' => $departments,
            'insurances' => $insurances,
            'products' => $products,
            'agents' => $agents,
            'users' => $users,
            'table_key' => $table_key,
            'count_pagination' => $result['count_pagination'],
            'limited_functionality_in_common_analytics' => $limited_functionality_in_common_analytics
        ]);

    }


    public function get_payments_list(PaymentListRequest $request)
    {
        $visibility = TabsVisibility::get_analytics_visibility();

        $payments = Payments::getPayments();/*->where('statys_id', 1)*/
        $payments->leftJoin('contracts', 'contracts.id', '=', 'payments.contract_id')
            ->leftJoin('bso_items', 'bso_items.id', '=', 'payments.bso_id')
            ->leftJoin('users as agent', 'agent.id', '=', 'payments.agent_id')
            ->leftJoin('users as manager', 'manager.id', '=', 'payments.manager_id')
            ->leftJoin('points_sale', 'points_sale.id', '=', DB::raw('IF (contracts.sales_condition = 0, agent.point_sale_id, manager.point_sale_id)'))
            ->leftJoin('departments', 'departments.id', '=', DB::raw('IF (contracts.sales_condition = 0, agent.department_id, manager.department_id)'))
            ->leftJoin('products', 'products.id', '=', 'bso_items.product_id')
            ->leftJoin('insurance_companies', 'insurance_companies.id', '=', 'bso_items.insurance_companies_id')
            ->leftJoin('citys', 'citys.id', '=', DB::raw('IF (contracts.sales_condition = 0, agent.city_id, manager.city_id)'))
            ->leftJoin('invoices', 'invoices.id', '=', 'payments.invoice_id')
            ->leftJoin('subjects as insurer', 'insurer.id', '=', 'contracts.insurer_id')
            ->leftJoin('users as invoice_payment_user', 'invoice_payment_user.id', '=', 'invoices.invoice_payment_user_id')
            ->leftJoin('organizations', 'organizations.id', '=', 'payments.org_id')
            ->leftJoin('financial_policies', 'financial_policies.id', '=', 'payments.financial_policy_id')
            ->leftJoin('installment_algorithms_list', 'installment_algorithms_list.id', '=', 'contracts.installment_algorithms_id')
            ->leftJoin('reports_orders', 'reports_orders.id', '=', 'payments.reports_order_id')
            ->leftJoin('object_insurer_auto', 'object_insurer_auto.object_insurer_id', '=', 'contracts.object_insurer_id');
        $payments->where('contracts.statys_id', '!=', -1); // не удаленные
            //->where('payments.is_deleted', 0);
        // ->whereRaw('IF (contracts.sales_condition = 0, agent.id, manager.id)');


        $payments = Visible::apply($payments, 'analitics', ['payments.agent_id', 'payments.manager_id']);
        $payments = (new PaymentsQueryHandler($payments))->allowEmpty()->apply();

        $payments_sql = clone $payments;

        $payments__ = clone $payments;

        $payments__->where('payments.is_deleted', '=', 0);

        $_nal = clone $payments;
        $_beznal = clone $payments;
        $_sk = clone $payments;

        $_nal->where('payments.is_deleted', '=', 0);
        $_beznal->where('payments.is_deleted', '=', 0);
        $_sk->where('payments.is_deleted', '=', 0);


        $sym_nalichnie = $_nal->where('payments.payment_type', 0)->where('payments.payment_flow', 0)->sum('payments.payment_total');
        $sym_beznalichnie = $_beznal->whereIn('payments.payment_type', [1, 2])->where('payments.payment_flow', 0)->sum('payments.payment_total');
        $sym_beznalichnie_sk = $_sk->where('payments.payment_flow', 1)->sum('payments.payment_total');

        $statistic = new \stdClass();
        $statistic->sum_payments_total = $payments__->sum('payments.payment_total');
        $statistic->sum_official_discount_total = $payments__->sum('payments.official_discount_total');
        $statistic->sum_informal_discount_total = $payments__->sum('payments.informal_discount_total');
        $statistic->sum_financial_policy_kv_parent_total = $payments__->sum('payments.financial_policy_kv_parent_total');
        $statistic->sum_all_kv_total = $payments__->sum('payments.financial_policy_kv_bordereau_total') + $payments__->sum('payments.financial_policy_kv_dvoy_total');


        $temp_kv_agent_total = $payments__
            ->select(
                DB::raw('SUM(IF (contracts.sales_condition = 1, 0, payments.financial_policy_kv_agent_total)) as sum_financial_policy_kv_agent_total'),
                DB::raw('SUM((payments.payment_total/100)*(financial_policies.partnership_reward_1+financial_policies.partnership_reward_2+financial_policies.partnership_reward_3)
                ) as sum_financial_policy_kv_partnership_reward_total')
            )->first();

        $statistic->sum_financial_policy_kv_agent_total = $temp_kv_agent_total->sum_financial_policy_kv_agent_total;


        // $margin = $statistic->sum_all_kv_total-($statistic->sum_financial_policy_kv_agent_total+$temp_kv_agent_total->sum_financial_policy_kv_partnership_reward_total);

        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : -1;

        $result = PaginationHelper::paginate($payments, $page, $page_count);


        $table_key = 'analitics_common';
        $user = Auth::user();


        /* Проверка на настройку таблицы */
        $check_builder = Users2Columns::query();
        $check_builder->leftJoin('table_columns', 'table_columns.id', '=', 'users2columns.column_id');
        $check_builder->where('users2columns.user_id', '=', Auth::id())->where('table_columns.table_key', '=', $table_key);
        $check_result = $check_builder->select('users2columns.id as users2columns_id')->get();

        $user_columns = $user->getTableColumns($table_key);

        $query = [
            'fields' => ['payments.*'],
            'where' => [],
            'order' => [],
        ];

        foreach ($user_columns as $column) {
            if ($column['is_as'] == 1) {
                $query['fields'][] = "{$column['column_key']} as {$column['as_key']}";
            }
        }


        $result['builder'];

        foreach ($query['fields'] as $key => &$v) {
            $v = DB::raw($v);
        }

        $result['builder']->select($query['fields']);

        $result['count_pagination'] = [
            [-1 => 'Все', 25 => '25', 50 => '50', 100 => '100', 150 => '150']
        ];

        $result_ = $result['builder']->get();

        return [
            'statistic' => $statistic,
            'sym_nalichnie' => $sym_nalichnie,
            'sym_beznalichnie' => $sym_beznalichnie,
            'sym_beznalichnie_sk' => $sym_beznalichnie_sk,
            //'margin' => $margin,

            'visibility' => $visibility,
            'check_result' => $check_result->isNotEmpty(),
            'payments' => $result_,
            'payments_sql' => $payments_sql,
            'user_columns' => $user_columns,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
            'visible_columns' => Auth::user()->visible_columns($table_key),
            'count_pagination' => $result['count_pagination']
        ];

    }

    public function get_payments_list_to_excel(PaymentListRequest $request)
    {
        $visibility = TabsVisibility::get_analytics_visibility();

        $payments = Payments::getPayments();
        $payments->leftJoin('contracts', 'contracts.id', '=', 'payments.contract_id')
            ->leftJoin('bso_items', 'bso_items.id', '=', 'payments.bso_id')
            ->leftJoin('users as agent', 'agent.id', '=', 'payments.agent_id')
            ->leftJoin('users as manager', 'manager.id', '=', 'payments.manager_id')
            ->leftJoin('points_sale', 'points_sale.id', '=', DB::raw('IF (contracts.sales_condition = 0, agent.point_sale_id, manager.point_sale_id)'))
            ->leftJoin('departments', 'departments.id', '=', DB::raw('IF (contracts.sales_condition = 0, agent.department_id, manager.department_id)'))
            ->leftJoin('products', 'products.id', '=', 'bso_items.product_id')
            ->leftJoin('insurance_companies', 'insurance_companies.id', '=', 'bso_items.insurance_companies_id')
            ->leftJoin('citys', 'citys.id', '=', DB::raw('IF (contracts.sales_condition = 0, agent.city_id, manager.city_id)'))
            ->leftJoin('invoices', 'invoices.id', '=', 'payments.invoice_id')
            ->leftJoin('subjects as insurer', 'insurer.id', '=', 'contracts.insurer_id')
            ->leftJoin('users as invoice_payment_user', 'invoice_payment_user.id', '=', 'invoices.invoice_payment_user_id')
            ->leftJoin('organizations', 'organizations.id', '=', 'payments.org_id')
            ->leftJoin('financial_policies', 'financial_policies.id', '=', 'payments.financial_policy_id')
            ->leftJoin('installment_algorithms_list', 'installment_algorithms_list.id', '=', 'contracts.installment_algorithms_id')
            ->leftJoin('reports_orders', 'reports_orders.id', '=', 'payments.reports_order_id')
            ->leftJoin('object_insurer_auto', 'object_insurer_auto.object_insurer_id', '=', 'contracts.object_insurer_id')
            //->where('payments.is_deleted', 0)
            ->whereRaw('IF (contracts.sales_condition = 0, agent.id, manager.id) IS NOT NULL');

        $payments = Visible::apply($payments, 'finance', ['payments.agent_id', 'payments.manager_id']);
        $payments = (new PaymentsQueryHandler($payments))->allowEmpty()->apply();

        $result = $payments;

        $payments_sql = clone $payments;
        $table_key = 'analitics_common';
        $user = Auth::user();


        /* Проверка на настройку таблицы */
        $check_builder = Users2Columns::query();
        $check_builder->leftJoin('table_columns', 'table_columns.id', '=', 'users2columns.column_id');
        $check_builder->where('users2columns.user_id', '=', Auth::id())->where('table_columns.table_key', '=', $table_key);
        $check_result = $check_builder->select('users2columns.id as users2columns_id')->get();

        $user_columns = $user->getTableColumns($table_key);

        $query = [
            'fields' => ['payments.*'],
            'where' => [],
            'order' => [],
        ];

        foreach ($user_columns as $column) {
            if ($column['is_as'] == 1) {
                $query['fields'][] = "{$column['column_key']} as {$column['as_key']}";
            }
        }


        foreach ($query['fields'] as $key => &$v) {
            $v = DB::raw($v);
        }

        $result->select($query['fields']);

        $result_ = $result->get();

        return [
            'visibility' => $visibility,
            'check_result' => $check_result->isNotEmpty(),
            'payments' => $result_,
            'payments_sql' => $payments_sql,
            'user_columns' => $user_columns,
            'visible_columns' => Auth::user()->visible_columns($table_key),
        ];

    }

    public function get_payments_table(PaymentListRequest $request)
    {

        $result = $this->get_payments_list($request);
        $result['html']['table'] = view('analitics.common.table', $result)->render();
        $result['html']['statistic'] = view('analitics.common.statistic', $result)->render();

        return $result;
    }

    public function get_payments_table_to_excel(PaymentListRequest $request)
    {
        $result = $this->get_payments_list_to_excel($request);
        $result['html'] = view('analitics.common.table', $result)->render();
        return $result;
    }

}