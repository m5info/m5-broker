<?php

namespace App\Http\Controllers\Analitics\RemovedBsos;

use App\Helpers\PaginationHelper;
use App\Http\QueryHandlers\Analitics\Clients\AnaliticsContractsQueryHandler;
use App\Models\BSO\BsoItem;
use App\Models\BSO\RemovedBsoItems;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\RemovedPayments;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RemovedBsosController extends Controller
{
    public function __construct() {
        $this->breadcrumbs[] = [
            'label' => 'Аналитика - Развязанные договоры',
            'url' => 'analitics/contracts_terminations'
        ];
    }

    public function index() {
        $this->middleware('permissions:analitics,contracts_terminations');

        return view('analitics.removed_bsos.index')->with('breadcrumbs', $this->breadcrumbs);
    }


    public function get_table(Request $request) {

        $data = $this->getItems($request);

        $table = view('analitics.removed_bsos.table',  ['items' => $data['items']])->render();

        return [
            'html' => $table,
            'page_max' => $data['page_max'],
            'page_sel' => $data['page_sel'],
            'max_row' => $data['max_row'],
            'view_row' => $data['view_row'],
        ];
    }

    public function getItems(Request $request)
    {
        $builder = RemovedBsoItems::query()
            ->select([
                'removed_bso_items.id as removed_bso_item_id',
                'removed_bso_items.bso_item_id as bso_item_id',
                'removed_bso_items.removed_user_id as removed_user_id',
                'removed_bso_items.created_at as created_at',
                'removed_bso_items.old_data as bso_item_old_data',
                'removed_contracts.contract_id as contract_id',
                'removed_contracts.old_data as contract_old_data',
            ])
            ->leftJoin('removed_contracts', 'removed_contracts.removed_bso_item_id', '=', 'removed_bso_items.id');

        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
        $result = PaginationHelper::paginate($builder, $page, $page_count);

        $items = $result['builder']->get();

        $itemsArr = [];
        foreach($items as $key => $item){

            $removed_payments = RemovedPayments::where('removed_bso_item_id', '=', $item->removed_bso_item_id)->get();

            $itemsArr[$key] = [
                'removed_bso_item_id' => $item->removed_bso_item_id,
                'bso_item_id' => $item->bso_item_id,
                'contract_id' => $item->contract_id,
                'removed_user_id' => $item->removed_user_id,
                'created_at' => $item->created_at,
                'bso_item_old_data' => $item->bso_item_old_data ? \GuzzleHttp\json_decode($item->bso_item_old_data) : new \stdClass(),
                'contract_old_data' => $item->contract_old_data ? \GuzzleHttp\json_decode($item->contract_old_data) : new \stdClass(),
                'payment_ids' => $removed_payments ? $removed_payments->pluck('payment_id')->toArray() : []
            ];
        }

        return [
            'items' => $itemsArr,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];

    }

    public function restoreItem($id)
    {
        $removedBsoItem = RemovedBsoItems::findOrFail($id);

        $removedBsoItem->restoreBsoItem();

        if($removedContract = $removedBsoItem->removed_contract){
            $removedContract->restoreContract();
        }

        if($removedPayments = $removedBsoItem->removed_payments){
            foreach($removedPayments as $removedPayment){
                $removedPayment->restorePayment();
            }
        }

        $removedBsoItem->destroyAllRemoved();
    }
}
