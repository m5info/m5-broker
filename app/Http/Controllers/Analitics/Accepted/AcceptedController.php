<?php

namespace App\Http\Controllers\Analitics\Accepted;

use App\Helpers\PaginationHelper;
use App\Helpers\Visible;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Contracts\Accepted\AcceptsQueryHandler;
use App\Models\Actions\PaymentAccept;
use DB;

class AcceptedController extends Controller{


    public function __construct(){
        $this->middleware('permissions:analitics,accepted');
    }


    public function index(){
        return view('contracts.accepted.index');
    }



    public function get_table(){

        $data = $this->get_list();
        $data['html'] = view('contracts.accepted.table', $data)->render();
        return $data;

    }


    public function get_list(){
        $this->validate(request(), [
            'bso_supplier_id' => 'integer',
            'manager_id' => 'integer',
            'bso_title' => 'string',
            'type_id' => 'integer',
            'org_id' => 'integer',
            'date_from' => 'date',
            'date_to' => 'date',
            'sk' => 'integer',
        ]);

        $accepts = (new AcceptsQueryHandler(PaymentAccept::query()))->allowEmpty()->apply();
        Visible::apply($accepts, 'analitics', ['contracts.agent_id', 'contracts.manager_id']);

        $accepts->leftJoin('contracts', 'contracts.id', '=', 'payment_accepts.contract_id')
            ->leftJoin('users as agent', 'agent.id', '=', 'contracts.agent_id')
            ->leftJoin('users as manager', 'manager.id', '=', 'contracts.manager_id')
            ->leftJoin('departments', 'departments.id', '=', DB::raw('IF (contracts.sales_condition = 0, agent.department_id, manager.department_id)'))
            ->whereRaw('IF (contracts.sales_condition = 0, agent.id, manager.id) IS NOT NULL');


        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
        $result = PaginationHelper::paginate($accepts, $page, $page_count);

        $result['builder']->orderBy('accept_date', 'desc');

        $accepts = $result['builder']->get();

        return [
            'accepts' => $accepts,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];

    }
}