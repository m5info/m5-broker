<?php

namespace App\Http\Controllers\Analitics\Delivery;

use App\Helpers\PaginationHelper;
use App\Http\Requests\Analitics\Common\PaymentListRequest;
use App\Models\Delivery\Delivery;
use App\Models\Delivery\DeliveryCost;
use App\Models\Delivery\DeliveryFiles;
use App\Models\Directories\ManufacturingCalendar;
use App\Models\File;
use App\Models\User;
use App\Models\Users\UsersWorkDays;
use App\Repositories\FilesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class DeliveryAnaliticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    const TEMP_EXPORT_STORAGE_FOLDER = 'analitics_delivery';

    public function index()
    {
        return view('analitics.delivery.index');
    }

    public function get_table(Request $request)
    {
        $date_from = getDateFormatEn("{$request->year}-{$request->mounth}-01");
        $number = cal_days_in_month(CAL_GREGORIAN, $request->mounth, $request->year);
        $date_to = getDateFormatEn("{$request->year}-{$request->mounth}-{$number}");

        $delivery = Delivery::query();
        $delivery->where('delivery.delivery_date', '>=', getDateFormatEn($date_from).' 00:00:00');
        $delivery->where('delivery.delivery_date', '<=', getDateFormatEn($date_to).' 23:59:59');


        if((int)$request->agent_id > 0){
            $delivery->where('delivery.user_id', (int)$request->agent_id);
        }
        if((int)$request->parent_id > 0){
            $delivery->where('users.parent_id', (int)$request->parent_id);
        }

        $delivery->select(['delivery.user_id', 'users.name',
                                \DB::raw('count(delivery.id) as c_deliveri'),
                                \DB::raw('sum(delivery.delivery_cost) as payments_summ'), 'users.zp', 'users.financial_expenses',
                                \DB::raw('sum(payments.financial_policy_kv_agent_total) as financial_policy_kv_agent_total'),
                                \DB::raw('products.title as product_title')
                        ]);

        $delivery->leftJoin('users', 'users.id', '=', 'delivery.user_id');
        $delivery->leftJoin('payments', 'payments.id', '=', 'delivery.payment_id');
        $delivery->leftJoin('contracts', 'contracts.id', '=', 'delivery.contract_id');
        $delivery->leftJoin('products', 'products.id', '=', 'contracts.product_id');

        $delivery->groupBy('delivery.user_id');

//        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
//        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
//        $result = PaginationHelper::paginate($delivery, $page, $page_count);
        $count_worked_day_mounth = ManufacturingCalendar::getCountWorkedDayMounth($date_from);

//        $result['count_pagination'] = [
//            -1 => 'Все', 25 => '25', 50 => '50', 100 => '100', 150 => '150'
//        ];

        return [
//            'page_max' => $result['page_max'],
//            'page_sel' => $result['page_sel'],
//            'max_row' => $result['max_row'],
//            'view_row' => $result['view_row'],
//            'count_pagination' => $result['count_pagination'],
//            'html' => view('analitics.delivery.table', ['deliverys' => $result['builder']->get(), 'request' => $request, 'count_worked_day_mounth' => $count_worked_day_mounth])->render(),
            'html' => view('analitics.delivery.table', ['deliverys' => $delivery->get(), 'request' => $request, 'count_worked_day_mounth' => $count_worked_day_mounth])->render(),
        ];
    }

    public function details(Request $request){

        $date_from = getDateFormatEn("{$request->year}-{$request->mounth}-01");
        $number = cal_days_in_month(CAL_GREGORIAN, $request->mounth, $request->year);
        $date_to = getDateFormatEn("{$request->year}-{$request->mounth}-{$number}");
        $delivery = Delivery::query();
        $delivery->select([
            'delivery.*',
            'users.name as agent_name'
        ]);
        $delivery->where('delivery_date', '>=', $date_from);
        $delivery->where('delivery_date', '<=', $date_to);
        $delivery->where('delivery.user_id', (int)$request->user);
        $delivery->leftJoin('users', 'users.id', '=', 'delivery.agent_id');

        $user = User::find((int)$request->user);

        $dataExel = new \stdClass();
        $dataExel->user = (int)$request->user;
        $dataExel->mounth = (int)$request->mounth;
        $dataExel->year = (int)$request->year;

        if($request->exel){
            return [
                'html' => view('analitics.delivery.details', ['deliverys' => $delivery->get(), 'user' => $user, 'dataExel' => $dataExel ])->render(),
            ];
        }


        return view('analitics.delivery.details', ['deliverys' => $delivery->get(), 'user' => $user, 'dataExel' => $dataExel ]);
    }

    public function getFreshWorkedDay(Request $request){

        $update_work_day_user = UsersWorkDays::updateWorkDayUser($request->mounth, $request->year);

        if($update_work_day_user){
            return [ 'result' => true ];
        }

        return [ 'result' => false ];
    }

    public function setZeroCostDelivery(Request $request){

        $delivery = Delivery::where('id', (int)$request->id)->update(['delivery_cost' => 0]);

        if($delivery){
            return ['result' => true];
        }
        return ['result' => false];
    }

    public function deleteDelivery(Request $request){

        $delivery = Delivery::find((int)$request->id);

        $delivery->delete();

        if($delivery){
            return ['result' => true];
        }
        return ['result' => false];
    }

    public function setCostDelivery(Request $request){

        $delivery = Delivery::where('id', (int)$request->id)->update(['delivery_cost' => (int)$request->cost]);

        if($delivery){
            return ['result' => true];
        }
        return ['result' => false];
    }

    public function newDelivery(){

        $product = DeliveryCost::query()
            ->leftJoin('products', 'products.id', '=', 'delivery_cost.product_id')
            ->select('delivery_cost.*', 'products.title')
            ->where('delivery_cost.product_id', 0)
            ->get()->pluck('product_title', 'id')/*->prepend('Продукт', -1)*/;

        $expert = User::where('role_id', '11')->get()->pluck('name', 'id')/*->prepend('Эксперт', -1)*/;
        $users = User::all()->pluck('name', 'id')/*->prepend('Эксперт', -1)*/;

        return view('analitics.delivery.new_delivery', ['product' => $product, 'users' => $users, 'expert' => $expert]);
    }

    public function AddNewDelivery(Request $request){

        $arrData = [];
        mb_parse_str($request->data_form, $arrData);
        $arrData = (object)$arrData;

        $day = (date('N', strtotime($arrData->date)) >= 6) ? 1 : 0 ; // Узнаём выходной или нет ?

        $price = DeliveryCost::where('id', $arrData->product_id)->first();
        $expert = User::where('id', $arrData->expert_id)->first();
        $agent = User::where('id', $arrData->agent_id)->first();

        $delivery = Delivery::create([
            'user_id'=>$expert->id,
            'parent_id'=>$expert->parent_id,
            'delivery_date'=>getDateFormatEn($arrData->date),
            'contract_id'=>0,
            'payment_id'=>0,
            'delivery_cost'=>($day == 0) ? $price->delivery_price : $price->weekend_price,
            'weekend'=>$day,
            'product_id'=>$arrData->product_id,
            'agent_id'=>$agent->id,
            'insurer_title'=>$arrData->insurer_title,
        ]);

        if(isset($request->arrDoc) && sizeof($request->arrDoc)){
            foreach ($request->arrDoc as $doc) {
                Storage::move($doc['host'].'/delivery_document_temp/'.$doc['name'].'.'.$doc['ext'], $doc['host'].'/delivery_document/'.$delivery->id.'/'.$doc['name'].'.'.$doc['ext']);    // перемещаем из временной папки

                File::where('id', $doc['id'])->update(['folder' => '/delivery_document/'.$delivery->id]);   // обновляем запись в бд

                DeliveryFiles::create([
                    'file_id' => $doc['id'],
                    'delivery_id' => $delivery->id,
                ]);
            }
        }

        return ['result' => true];
    }

    public function uploadFileDelivery(Request $request){       // загружаем файлы во временную папку

        if ($request->hasFile('file')) {

            if($request->delivery_id){
                $file = (new FilesRepository)->makeFile($request->file, '/delivery_document/'.$request->delivery_id);
                DeliveryFiles::create([
                    'file_id' => $file->id,
                    'delivery_id' => $request->delivery_id,
                ]);
            }else{
                $file = (new FilesRepository)->makeFile($request->file, "delivery_document_temp");
            }

            return ['file' => $file];
        }

        return false;
    }

    public function deleteDeliveryDoc(Request $request){
        $file = File::where('id', $request->file_id)->first();
        DeliveryFiles::where('file_id', $request->file_id)->first()->delete();
        $delete_file = Storage::disk('local')->delete($file->host.$file->folder.'/'.$file->name.'.'.$file->ext);
        $file->delete();
    }

    public function getAllDocDelivery(Request $request){


        $delivery = Delivery::where('id', $request->delivery_id)->first();

        $files = $delivery->files;

        return view('analitics.delivery.doc_delivery', ['files' => $files, 'delivery_id' => $request->delivery_id]);
    }

    public function get_delivery_table_to_excel(PaymentListRequest $request)
    {
        $result = $this->get_table($request);

        return $result;
    }

    public function get_delivery_detail_table_to_excel(PaymentListRequest $request)
    {
        $result = $this->details($request);

        return $result;
    }
}
