<?php

namespace App\Http\Controllers\Analitics\Total;


use App\Http\Controllers\Controller;
use DB;



class TotalAnalyticsController extends Controller{

    protected $havings;

    public function index(){
        return view('analitics.total.index');
    }

    public function get_filters(){
        return view('analitics.total.filters');
    }

    public function get_charts(){

        $this->validate(request(), [
            "period" => "integer",
            "year" => "integer",
            "month" => "integer",
            "from" => "string",
            "to" => "string",
        ]);

        $this->havings = $this->get_havings();

        $incomes_by_uts = collect($this->get_incomes_data_by_uts());
        $expenses_by_uts = collect($this->get_expenses_data_by_uts());
        $payments = collect($this->get_payments_data());

        $all = collect([])->merge($incomes_by_uts)->merge($expenses_by_uts)->merge($payments)->sortBy('uts');

        $from_time = $all->first() ? $all->first()->uts : time();
        $to_time = $all->last() ? $all->last()->uts : time();

        $incomes_by_category = collect($this->get_incomes_data_by_category())->keyBy('category_title');
        $expenses_by_category = collect($this->get_expenses_data_by_category())->keyBy('category_title');

        $colors = ['#30d0d8', 'orange', '#6699FF', '#307d30','#305afb',  'purple',  '#6633CC',  '#FFEB3B',  'red',  '#35d832',       '#339999', '#99FF66', '#999933','#FF9999',];

        $datasets = [
            'payment_total' => ['fill'=>false, 'backgroundColor' => '', 'borderColor' => $colors[2], 'label' => 'Оборот', 'data' => []],
            'kv_sk' => ['fill'=>false, 'backgroundColor' => $colors[2], 'borderColor' => $colors[2], 'label' => 'КВ СК', 'data' => []],
            'kv_parent' => ['fill'=>false, 'backgroundColor' => $colors[0], 'borderColor' => $colors[0], 'label' => 'КВ Руководителей', 'data' => []],
            'kv_agent' => ['fill'=>false, 'backgroundColor' => $colors[1], 'borderColor' => $colors[1], 'label' => 'КВ Агентов', 'data' => []],
            'income_kv' => ['fill'=>false, 'backgroundColor' => $colors[3], 'borderColor' => $colors[3], 'label' => 'Доход по КВ', 'data' => []],
            'expense_kv' => ['fill'=>false, 'backgroundColor' => $colors[4], 'borderColor' => $colors[4], 'label' => 'Расход по КВ', 'data' => []],
            'income' => ['fill'=>false, 'backgroundColor' => $colors[5], 'borderColor' => $colors[5], 'label' => 'Доп доходы', 'data' => []],
            'expense' => ['fill'=>false, 'backgroundColor' => $colors[6], 'borderColor' => $colors[6], 'label' => 'Расходы', 'data' => []],
            'common_income' => ['fill'=>false, 'backgroundColor' => $colors[7], 'borderColor' => $colors[7], 'label' => 'Общий доход', 'data' => []],
            'common_expense' => ['fill'=>false, 'backgroundColor' => $colors[8], 'borderColor' => $colors[8], 'label' => 'Общий расход', 'data' => []],
            'profit' => ['fill'=>false, 'backgroundColor' => $colors[9], 'borderColor' => $colors[9], 'label' => 'Прибыль', 'data' => []],
        ];

        $checkpoints = $this->get_checkpoints($from_time, $to_time);
        $line_labels = $this->get_labels($checkpoints);

        foreach($checkpoints as $k => $time){

            if($k == 0){
                $datasets['payment_total']['data'][] = 0;
                $datasets['kv_sk']['data'][] = 0;
                $datasets['kv_parent']['data'][] = 0;
                $datasets['kv_agent']['data'][] = 0;
                $datasets['income_kv']['data'][] = 0;
                $datasets['expense_kv']['data'][] = 0;
                $datasets['income']['data'][] = 0;
                $datasets['expense']['data'][] = 0;
                $datasets['common_income']['data'][] = 0;
                $datasets['common_expense']['data'][] = 0;
                $datasets['profit'][] = 0;
                continue;
            }

            $prev_time = $checkpoints[$k-1];

            $period_incomes = $incomes_by_uts->filter(function($item) use ($time, $prev_time){
                return $item->uts > $prev_time && $item->uts <= $time;
            });

            $period_expenses = $expenses_by_uts->filter(function($item) use ($time, $prev_time){
                return $item->uts > $prev_time && $item->uts <= $time;
            });

            $period_payments = $payments->filter(function($item) use ($time, $prev_time){
                return $item->uts > $prev_time && $item->uts <= $time;
            });

            $period_kv_parent = $period_payments->first() ? $period_payments->first()->kv_parent : 0;
            $period_kv_agent = $period_payments->first() ? $period_payments->first()->kv_agent : 0;
            $period_payment_total = $period_payments->first() ? $period_payments->first()->payment_total : 0;
            $period_kv_sk = $period_payments->first() ? $period_payments->first()->kv_sk : 0;
            $period_income_kv = $period_payments->first() ? $period_payments->first()->income_kv : 0;
            $period_expense_kv = $period_payments->first() ? $period_payments->first()->expense_kv : 0;
            $period_income = $period_incomes->first() ? $period_incomes->first()->income : 0;
            $period_expense = $period_expenses->first() ? $period_expenses->first()->expense : 0;
            $period_common_income = $period_income + $period_income_kv;
            $period_common_expense = $period_expense + $period_expense_kv;
            $period_profit = $period_common_income - $period_expense;

            $datasets['payment_total']['data'][] = $period_payment_total;
            $datasets['kv_sk']['data'][] = $period_kv_sk;
            $datasets['kv_parent']['data'][] = $period_kv_parent;
            $datasets['kv_agent']['data'][] = $period_kv_agent;
            $datasets['income_kv']['data'][] = $period_income_kv;
            $datasets['expense_kv']['data'][] = $period_expense_kv;
            $datasets['income']['data'][] = $period_income;
            $datasets['expense']['data'][] = $period_expense;
            $datasets['common_income']['data'][] = $period_common_income;
            $datasets['common_expense']['data'][] = $period_common_expense;
            $datasets['profit']['data'][] = $period_profit;


        }

        $dataset_income_category = [
            'data' => $incomes_by_category->pluck('income'),
            'backgroundColor' => array_slice($colors, 0, $incomes_by_category->count())
        ];

        $dataset_expense_category = [
            'data' => $expenses_by_category->pluck('expense'),
            'backgroundColor' => array_slice($colors, 0, $expenses_by_category->count())
        ];

        $charts = [
            'income' => [
                'type'    => 'doughnut',
                'data'    => [
                    'labels' => $incomes_by_category->keys(),
                    'datasets' => [
                        $dataset_income_category
                    ],
                ],
                'options' => [
                    'elements' => [
                        'center' => [
                            'text'        => getPriceFormat($incomes_by_category->sum('income')),
                            'color'       => '#36A2EB',
                            'fontStyle'   => 'Helvetica',
                            'sidePadding' => 15
                        ],
                    ],
                ],
            ],
            'expense' => [
                'type'    => 'doughnut',
                'data'    => [
                    'labels' => $expenses_by_category->keys(),
                    'datasets' => [
                        $dataset_expense_category
                    ]
                ],
                'options' => [
                    'elements' => [
                        'center' => [
                            'text'        => getPriceFormat($expenses_by_category->sum('expense')),
                            'color'       => '#36A2EB',
                            'fontStyle'   => 'Helvetica',
                            'sidePadding' => 15
                        ],
                    ],
                ],
            ],
            'turn' => [
                'type' => 'line',
                'data' => [
                    'labels' => $line_labels,
                    'datasets' => [
                        $datasets['kv_parent'],
                        $datasets['kv_agent'],
                        $datasets['kv_sk'],
                        $datasets['income_kv'],
                        $datasets['expense_kv'],

                    ]
                ]
            ],
            'profit' => [
                'type' => 'line',
                'data' => [
                    'labels' => $line_labels,
                    'datasets' => [
                        $datasets['income'],
                        $datasets['expense'],
                        $datasets['common_income'],
                        $datasets['common_expense']
                    ]
                ]
            ],

        ];

        $result = [];
        foreach($datasets as $dataset_name => $dataset){
            $result[] = [
                'title' => $dataset['label'],
                'color' => $dataset['backgroundColor'],
                'total' => array_sum($dataset['data'])
            ];
        }


        return response()->json([
            'charts' => $charts,
            'result' => $result
        ]);
    }





    protected function get_incomes_data_by_category(){


        $sql = "select sum(ie.sum) as income, iec.title as category_title, UNIX_TIMESTAMP(date) as uts
        from incomes_expenses ie
        join incomes_expenses_categories iec on (iec.id = ie.category_id)
        where iec.type=1 and ie.status_id=2
        group by category_title
        having {$this->havings}";

        return DB::select($sql);
    }

    protected function get_expenses_data_by_category(){

        $sql = "select sum(ie.sum) as expense, iec.title as category_title, UNIX_TIMESTAMP(date) as uts 
        from incomes_expenses ie
        join incomes_expenses_categories iec on (iec.id = ie.category_id)
        where iec.type=2 and ie.status_id=2
        group by category_title
        having {$this->havings}";

        return DB::select($sql);
    }


    protected function get_incomes_data_by_uts(){


        $sql = "select sum(ie.sum) as income, UNIX_TIMESTAMP(date) as uts
        from incomes_expenses ie
        join incomes_expenses_categories iec on (iec.id = ie.category_id)
        where iec.type=1 and ie.status_id=2
        group by uts
        having {$this->havings}
        order by uts";

        return DB::select($sql);
    }

    protected function get_expenses_data_by_uts(){

        $sql = "select sum(ie.sum) as expense,  UNIX_TIMESTAMP(date) as uts
        from incomes_expenses ie
        join incomes_expenses_categories iec on (iec.id = ie.category_id)
        where iec.type=2 and ie.status_id=2
        group by uts
        having {$this->havings}
        order by uts";

        return DB::select($sql);
    }


    protected function get_payments_data(){

        $sql = "select UNIX_TIMESTAMP(payment_data) as uts,
        sum(payment_total) as payment_total,
        sum(financial_policy_kv_parent_total) as kv_parent,
        sum(financial_policy_kv_agent_total) as kv_agent,
        sum(financial_policy_kv_bordereau_total + financial_policy_kv_dvoy_total) as kv_sk,
        sum(financial_policy_kv_bordereau_total + financial_policy_kv_dvoy_total - (financial_policy_kv_parent_total + financial_policy_kv_agent_total)) as income_kv,
        sum(financial_policy_kv_agent_total + financial_policy_kv_parent_total) as expense_kv
        from payments
        group by uts
        having {$this->havings}
        order by uts";

        return DB::select($sql);
    }



    protected function get_checkpoints($from_time=false, $to_time=false, $count = 27){

        $month = str_pad((int)request()->get('month'), 2, '0', STR_PAD_LEFT);
        $year = (int)request()->get('year');

        $from = request()->get('from');
        $to = request()->get('to');

        $time_checkpoints = [];

        if(request('period') == 3){
            $from_time = strtotime($from);
            $to_time = strtotime($to);
        }

        switch(request('period')){

            case 0: //month

                $first_day_month_date = "{$year}-{$month}-01";
                $last_day_in_month = date('t', strtotime($first_day_month_date));
                $month_day_arr = range(1, $last_day_in_month);
                $time_checkpoints = array_map(function($_day) use ($year, $month){
                    return strtotime("{$year}-{$month}-{$_day} 00:00:00");
                }, $month_day_arr);

                $time_checkpoints[] = strtotime("{$year}-{$month}-{$last_day_in_month} 23:59:59");
                break;

            case 1: //year

                $first_year_date = "{$year}-01-01";
                $last_day_in_month = date('t', strtotime($first_year_date));
                $year_month_arr = range(1,12);
                $time_checkpoints = array_map(function($_month) use ($year){
                    $_month = str_pad($_month, 2, '0', STR_PAD_LEFT);
                    return strtotime("{$year}-{$_month}-01 00:00:00");
                }, $year_month_arr);
                $time_checkpoints[] = strtotime("{$year}-12-{$last_day_in_month} 23:59:59");

                break;

            case 2: //all
            case 3: //period

                $one_day_sec = 60*60*24;
                $diff = $to_time - $from_time;
                $diff = $diff > 0 ? $diff : $one_day_sec;

                $diff_days = $diff / $one_day_sec;
                $diff_days = $diff_days > 0 ? $diff_days : 1;

                $count++;
                $count = $diff_days < $count ? $diff_days : $count;

                $time_checkpoints = array_map('ceil', range($from_time, $to_time, $diff / $count));

                break;

        }

        $time_checkpoints = array_map('intval', $time_checkpoints);

        return $time_checkpoints;
    }


    protected function get_havings(){

        $month = str_pad((int)request()->get('month'), 2, '0', STR_PAD_LEFT);
        $year = (int)request()->get('year');
        $from = request()->get('from');
        $to = request()->get('to');

        $havings = [];

        switch(request('period')){

            case 0: //month
                $first_time = strtotime("{$year}-{$month}-01 00:00:00");
                $last_day_in_month = date('t', $first_time);
                $last_time = strtotime("{$year}-{$month}-{$last_day_in_month} 23:59:59");

                $first_time--;
                $last_time++;

                $havings[] = "uts > {$first_time}";
                $havings[] = "uts < {$last_time}";

                break;

            case 1: //year
                $first_time = strtotime("{$year}-01-01 00:00:00");
                $last_day_in_month = date('t', $first_time);
                $last_time = strtotime("{$year}-12-{$last_day_in_month} 23:59:59");

                $first_time--;
                $last_time++;

                $havings[] = "uts > {$first_time}";
                $havings[] = "uts < {$last_time}";
                break;

            case 2:
                $havings[] = "1=1";
                break;
            case 3: //period
                $first_time = strtotime($from);
                $last_time = strtotime($to);

                $first_time--;
                $last_time++;

                $havings[] = "uts > {$first_time}";
                $havings[] = "uts < {$last_time}";
                break;
        }

        return implode(" AND ", $havings);
    }

    private function get_labels($checkpoints){

        $labels = [];

        switch(request('period')){

            case 0: // month
            case 2: // all
            case 3: // period

                $labels = array_unique(array_map(function($item){
                    return date('d.m.Y', $item);
                }, $checkpoints));

                break;

            case 1: //year

                $labels = array_values(getRuMonthes());

                break;

        }

        return $labels;
    }

}