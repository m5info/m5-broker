<?php

namespace App\Http\Controllers\Analitics\Basket;

use App\Helpers\PaginationHelper;
use App\Http\QueryHandlers\Analitics\Basket\BasketItemsQueryHandler;
use App\Http\QueryHandlers\Analitics\Techunder\TechunderContractsQueryHandler;
use App\Models\Basket\BasketItems;
use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoActsItems;
use App\Models\BSO\BsoComments;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLogs;
use App\Models\BSO\RemovedBsoItems;
use App\Models\Contracts\Payments;
use App\Models\Contracts\RemovedPayments;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BasketItemsController extends Controller
{
    public function __construct() {
        $this->breadcrumbs[] = [
            'label' => 'Аналитика - Корзина',
            'url' => 'analitics/basket_items'
        ];
    }

    public function index() {
        $this->middleware('permissions:analitics,basket_items');

        return view('analitics.basket.index')->with('breadcrumbs', $this->breadcrumbs);
    }


    public function get_table(Request $request) {

        $data = $this->getItems($request);

        $table = view('analitics.basket.table',  ['items' => $data['items']])->render();

        return [
            'html' => $table,
            'page_max' => $data['page_max'],
            'page_sel' => $data['page_sel'],
            'max_row' => $data['max_row'],
            'view_row' => $data['view_row'],
        ];
    }

    public function getItems(Request $request)
    {
        $builder = BasketItems::query();

        $builder = (new BasketItemsQueryHandler($builder))->apply();

        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
        $result = PaginationHelper::paginate($builder, $page, $page_count);

        $items = $result['builder']->get();

        return [
            'items' => $items,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];

    }

    public function restore_item($id)
    {
        $basketItem = BasketItems::find($id);

        if($basketItem){
            if($basketItem->type_id == 1){ // БСО
                $bso = $basketItem->bso;
                $bso->in_basket = 0;
                $bso->save();
                $basketItem->delete();
            }elseif($basketItem->type_id == 2){ // платеж
                $payment = $basketItem->payment;
                $payment->in_basket = 0;
                $payment->save();
                $basketItem->delete();
            }
        }

        return response(200, 200);
    }


    public function remove_item($id)
    {
        $basketItem = BasketItems::find($id);

        if($basketItem){
            if($basketItem->type_id == 1){ // БСО
                $response = $this->delete_bso($basketItem->removed_item_id);
                $basketItem->delete();
            }elseif($basketItem->type_id == 2){ // платеж
                $response = $this->delete_payment($basketItem->removed_item_id);
                $basketItem->delete();
            }
        }

        return $response;
    }

    public function delete_payment($id){
        $payment = Payments::getPaymentId($id);

        $payment->in_basket = 0;
        $payment->save();
        $payment->deletePayment();

        $return = new \stdClass();
        $return->error_state = 0;
        $return->error_title = '';

        return response()->json($return);
    }

    public function delete_bso($id)
    {

        $return = new \stdClass();
        $return->error_state = 0;

        $bso = BsoItem::find($id);

        if ($bso->state_id !=0){
            $return->error_state = 1;
            /*$return->error_attr  = ;*/
            $return->error_title = 'БСО должен быть в статусе "Чистый"';
            return response()->json($return);
        }

        $acts = BsoActsItems::query()->select('bso_act_id')->where('bso_id','=', $bso->id)->get()->toArray();


        // логируем удаление чистого БСО со склада
        $bsolog = new BsoLogs();
        $bsolog->log_time = date('Y-m-d H:i:s');
        $bsolog->bso_id = $bso->id;
        $bsolog->bso_act_id = (int)$bso->bso_act_id;
        $bsolog->bso_state_id = $bso->state_id;
        $bsolog->bso_location_id = $bso->location_id;
        $bsolog->bso_user_from = $bso->bso_supplier_id;
        $bsolog->bso_user_to = $bso->user_id;
        $bsolog->user_id = (int)auth()->id();
        $bsolog->ip_address = $_SERVER['REMOTE_ADDR'];
        $bsolog->is_deleted = 1;
        $bsolog->save();

        foreach ($acts as $key => $act) {

            $act_info = BsoActs::find($act['bso_act_id']);

            $bsolog = new BsoLogs();
            $bsolog->log_time = date('Y-m-d H:i:s');
            $bsolog->bso_id = $bso->id;
            $bsolog->bso_act_id = $act['bso_act_id'];
            $bsolog->bso_state_id = $act_info->bso_state_id;
            $bsolog->bso_location_id = $act_info->location_to;
            $bsolog->bso_user_from = $act_info->user_id_from;
            $bsolog->bso_user_to = $act_info->user_id_to;
            $bsolog->user_id = (int)auth()->id();
            $bsolog->ip_address = $_SERVER['REMOTE_ADDR'];
            $bsolog->is_deleted = 1;
            $bsolog->save();

        }

        $acts[]=[
            'bso_act_id'=> $bso->bso_act_id
        ];
        $act_bd = array_column($acts,'bso_act_id');

        BsoComments::query()->where('bso_id','=', $bso->id)->delete();
        BsoActsItems::query()->where('bso_id','=', $bso->id)->delete();

        $acts=BsoActsItems::query()->select('bso_act_id')->where('bso_id','=', $bso->id)->get()->toArray();


        // логируем удаление чистого БСО со склада
        $bsolog = new BsoLogs();
        $bsolog->log_time = date('Y-m-d H:i:s');
        $bsolog->bso_id = $bso->id;
        $bsolog->bso_act_id = (int)$bso->bso_act_id;
        $bsolog->bso_state_id = $bso->state_id;
        $bsolog->bso_location_id = $bso->location_id;
        $bsolog->bso_user_from = $bso->bso_supplier_id;
        $bsolog->bso_user_to = $bso->user_id;
        $bsolog->user_id = (int)auth()->id();
        $bsolog->ip_address = $_SERVER['REMOTE_ADDR'];
        $bsolog->is_deleted = 1;
        $bsolog->save();

        foreach ($acts as $key=>$act) {

            $act_info = BsoActs::find($act['bso_act_id']);

            $bsolog = new BsoLogs();
            $bsolog->log_time = date('Y-m-d H:i:s');
            $bsolog->bso_id = $bso->id;
            $bsolog->bso_act_id = $act['bso_act_id'];
            $bsolog->bso_state_id = $act_info->bso_state_id;
            $bsolog->bso_location_id = $act_info->location_to;
            $bsolog->bso_user_from = $act_info->user_id_from;
            $bsolog->bso_user_to = $act_info->user_id_to;
            $bsolog->user_id = (int)auth()->id();
            $bsolog->ip_address = $_SERVER['REMOTE_ADDR'];
            $bsolog->is_deleted = 1;
            $bsolog->save();

        }

        $acts[]=[
            'bso_act_id'=> $bso->bso_act_id
        ];
        $act_bd = array_column($acts,'bso_act_id');

        BsoComments::query()->where('bso_id','=', $bso->id)->delete();
        BsoActsItems::query()->where('bso_id','=', $bso->id)->delete();

        $bso->delete();

        //проверка актов БСО на связь с другими БСО и удаление актов, связанных только с удаляемым БСО
        $bso1 = BsoItem::query()->select('bso_act_id')->whereIn('bso_act_id', $act_bd)->get()->toArray();
        $bso2 = BsoActsItems::query()->select('bso_act_id')->whereIn('bso_act_id', $act_bd)->get()->toArray();
        $bso_acts = array_unique(array_column(array_merge($bso1, $bso2),'bso_act_id'));

        foreach ($acts as $key=>$act){
            if (in_array($act['bso_act_id'],$bso_acts)) {
                unset($acts[$key]);
            }
        }

        $act_bd = array_column($acts,'bso_act_id');

        BsoActs::query()->whereIn('id', $act_bd)->delete();

        return response()->json($return);
    }
}
