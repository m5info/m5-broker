<?php

namespace App\Http\Controllers\Analitics\Techunder;

use App\Helpers\PaginationHelper;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Analitics\Techunder\TechunderContractsQueryHandler;
use App\Http\QueryHandlers\Contracts\Accepted\AcceptsQueryHandler;
use App\Models\Actions\PaymentAccept;
use App\Models\Characters\Agent;
use App\Models\Characters\Underwriter;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Directories\Products;

class TechunderAnalyticsController extends Controller {

    public function __construct() {
        $this->middleware('permissions:analitics,analytics_techunder');
        $this->breadcrumbs[] = [
            'label' => 'Аналитика - Техандеррайтинг договоров',
            'url' => 'analitics/analytics_techunder'
        ];
    }

    public function index() {
        return view('analitics.techunder.index');
    }

    public function get_filters() {

        $underwriters = Underwriter::all();

        return view('analitics.techunder.filters', [
            'underwriters' => $underwriters,
        ]);
    }

    public function get_charts(Request $request) {

/*
        $accepts = Contracts::whereIn('check_user_id', Underwriter::all()->pluck('id'));
        $result = [];

        $datasets = ['data' => [], 'backgroundColor' => [], 'label' => [],];

        if (isset($request->underwriter_id) && $request->underwriter_id > 0) {
            if (is_array($request->underwriter_id)) {
                $accepts->where('check_user_id', 'in', $request->underwriter_id);
            } else {
                $accepts->where('check_user_id', '=', $request->underwriter_id);
            }
        }
        if (isset($request->product_ids) && count($request->product_ids)) {
            $accepts->whereIn('product_id', $request->product_ids);
        }
        if (isset($request->from)) {
            $from = new Carbon($request->from);
            $accepts->where('check_date', '>=', $from->format('Y-m-d'));
        }
        if (isset($request->to)) {
            $to = new Carbon($request->to);
            $accepts->where('check_date', '<=', $to->format('Y-m-d'));
        }

        if (isset($request->sk) && (int)$request->sk) {
            $accepts->where('insurance_companies_id', '=', (int)$request->sk);
        }


        $products = Products::all()->keyBy('id');
        $_products = $products->pluck('title', 'id');
        $accepts->distinct();
        $accepts = $accepts->with('payment_accepts')->get();
dd($accepts);
        $totalAccepts = $accepts->count();*/

        //        $accepts = $accepts->filter(function ($item){
//            return $item->payment && isset($item->payment->type_id) && $item->payment->type_id == 0;
//        });

        $accepts = Payments::query()
            ->where('type_id', '=', 0)
            ->whereIn('statys_id', [0, 1])
//            ->where('statys_id', '==', 1)
            ->whereIn('check_user_id', Underwriter::all()->pluck('id'));

        if (isset($request->underwriter_id) && $request->underwriter_id > 0) {
            if (is_array($request->underwriter_id)) {
                $accepts->where('check_user_id', 'in', $request->underwriter_id);
            } else {
                $accepts->where('check_user_id', '=', $request->underwriter_id);
            }
        }

        if (isset($request->product_ids)) {
            $accepts->whereHas('contract', function($query) use ($request) {
                $query->whereIn('product_id', $request->product_ids);
            });
        }
        if (isset($request->sk) && $request->sk) {
            $accepts->whereHas('contract', function($query) use ($request) {
                $query->where('insurance_companies_id', '=', $request->sk);
            });
        }
        if (isset($request->from)){
            $from = new Carbon($request->from);
            $accepts->where('check_date', '>=', $from->format('Y-m-d'));
        }
        if (isset($request->to)) {
            $to = new Carbon($request->to);
            $accepts->where('check_date', '<=', $to->format('Y-m-d'));
        }

        $totalAccepts = $accepts->count();
        $result = [];
        $datasets = ['data' => [], 'backgroundColor' => [], 'label' => [],];

        $products = Products::all()->keyBy('id');
        $_products = $products->pluck('title', 'id');

        $accepts->distinct();
        $accepts = $accepts->with('payment_accepts')->get();

        foreach ($accepts->groupBy('check_user_id') as $n => $group) {
            $color = dynamicColors();
            $datasets['data'][] = $group->count();
            $datasets['backgroundColor'][] = $color;
            $datasets['label'][] = $group->first()->check_user->name;

            $productsStats = [];

            foreach ($_products as $productId => $productName) {

                $productsStats[$productId] = $group->filter(function ($item) use ($productId) {
                    return $item->contract->product_id == $productId;
                })->count();
            }


            $under_rate_sum = 0;
            foreach ($productsStats as $productId => $count){
                $under_rate_sum += isset($products[$productId]) ? ($products[$productId]->underwriter_rate * $count) : 0;
            }

            $result[] = [
                'underwriter_id' => $group->first()->check_user_id,
                'title' => $group->first()->check_user->name,
                'color' => $color,
                'total' => $group->count(),
                'products' => $productsStats,
                'under_rate_sum' => $under_rate_sum
            ];
        }

        $charts = [
            'agentspie' => [
                'type' => 'pie',
                'data' => [
                    'labels' => $datasets['label'],
                    'datasets' => [
                        [
                            'data' => $datasets['data'],
                            'backgroundColor' => $datasets['backgroundColor'],
                            'label' => 'Техандеррайтинг по агентам'
                        ]
                    ],
                ],
            ],
            'total' => [
                'type' => 'doughnut',
                'data' => [
                    'labels' => 'test',
                    'datasets' => [
                    ]
                ],
                'options' => [
                    'elements' => [
                        'center' => [
                            'text' => $totalAccepts,
                            'color' => '#36A2EB',
                            'fontStyle' => 'Helvetica',
                            'sidePadding' => 15
                        ],
                    ],
                ],
            ],
        ];


        return response()->json([
            'result' => $result,
            'charts' => $charts,
            'products' => $_products,
        ]);
    }

    public function details(){

        $this->validate(request(), [
            'underwriter_id' => 'integer',
            'product_id' => 'integer',
            'from' => 'date',
            'to' => 'date',
            'insurer' => 'string',
            'agent_id' => 'integer',
        ]);


        $payments = Payments::whereIn('check_user_id', Underwriter::all()->pluck('id'));
        $payments = (new TechunderContractsQueryHandler($payments))->apply();
        $contract_ids = $payments->get()->pluck('contract_id');
        $contracts = Contracts::query()->whereIn('id', $contract_ids);

        return view('analitics.techunder.details.index', [
            'contracts' => $contracts->get()
        ]);

    }

    public function get_table(){

        $data = $this->get_list();
        $data['html'] = view('analitics.techunder.details.table', $data)->render();
        return $data;

    }


    public function get_list(){

        $this->validate(request(), [
            'underwriter_id' => 'integer',
            'product_id' => 'integer',
            'from' => 'date',
            'to' => 'date',
            'insurer' => 'string',
            'agent_id' => 'integer',
        ]);

        $payments = Payments::whereIn('check_user_id', Underwriter::all()->pluck('id'));
        $payments = (new TechunderContractsQueryHandler($payments))->apply();
        $contract_ids = $payments->get()->pluck('contract_id');
        $contracts = Contracts::query()->whereIn('id', $contract_ids);

        if ((int)request()->get('is_export')){
            $page = 1;
            $page_count = 999999;
        }else{
            $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
            $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 50;
        }
        $result = PaginationHelper::paginate($contracts, $page, $page_count);

        $contracts = $result['builder']->get();

        return [
            'contracts' => $contracts,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];

    }
}
