<?php

namespace App\Http\Controllers\Analitics\PointSale;


use App\Http\Controllers\Controller;
use DB;



class PointSaleAnalyticsController extends Controller{

    protected $havings;

    public function index(){
        return view('analitics.point_sale.index');
    }

    public function get_filters(){
        return view('analitics.point_sale.filters');
    }

    public function get_table(){

        $this->validate(request(), [
            "type_date" => "integer",
            "payment_type" => "integer",
            "period" => "integer",
            "year" => "integer",
            "month" => "integer",
            "from" => "string",
            "to" => "string",
        ]);

        $this->havings = $this->get_havings();

        $payments = $this->get_payments_data();

        return view('analitics.point_sale.table', [
            'payments' => $payments
        ]);

    }

    public function details($point_sale_id){

        $this->validate(request(), [
            "type_date" => "integer",
            "payment_type" => "integer",
            "period" => "integer",
            "year" => "integer",
            "month" => "integer",
            "from" => "string",
            "to" => "string",
        ]);

        $this->havings = $this->get_havings();
        $payments = $this->get_payments_agent($point_sale_id);

        return view('analitics.point_sale.details', [
            'payments' => $payments
        ]);

    }


    protected function get_payments_agent($point_sale_id){

        $sql = "
        select 
            points_sale.id as point_sale_id,
            points_sale.title as point_sale_title,
            agent.name as agent_title,
            sum(payments.payment_total) as payment_total,
            sum(payments.financial_policy_kv_bordereau_total + payments.financial_policy_kv_dvoy_total) as all_kv_total,
            sum(payments.financial_policy_kv_agent_total) as agent_kv_total,
            sum(payments.financial_policy_marjing_total) as marjing_total
            from payments
            left join contracts on contracts.id = payments.contract_id
            left join users as agent on agent.id = if(contracts.sales_condition = 0, payments.agent_id, payments.manager_id)
            left join points_sale  on points_sale.id = agent.point_sale_id 
            where
            payments.is_deleted=0 and payments.statys_id = 1 and agent.point_sale_id = {$point_sale_id}
            and {$this->havings}
            group by agent.id
            ORDER BY payment_total desc";


        return DB::select($sql);

    }




    protected function get_payments_data(){

        $sql = "
        select 
            points_sale.id as point_sale_id,
            points_sale.title as point_sale_title,
            sum(payments.payment_total) as payment_total,
            sum(payments.financial_policy_kv_bordereau_total + payments.financial_policy_kv_dvoy_total) as all_kv_total,
            sum(payments.financial_policy_kv_agent_total) as agent_kv_total,
            sum(payments.financial_policy_marjing_total) as marjing_total
            from payments
            left join contracts on contracts.id = payments.contract_id
            left join users as agent on agent.id = if(contracts.sales_condition = 0, payments.agent_id, payments.manager_id)
            left join points_sale on points_sale.id = agent.point_sale_id 
            where
            payments.is_deleted=0 and payments.statys_id = 1
            and {$this->havings}
            group by points_sale.id
            ORDER BY payment_total desc";



        return DB::select($sql);

    }





    protected function get_havings(){

        $month = str_pad((int)request()->get('month'), 2, '0', STR_PAD_LEFT);
        $year = (int)request()->get('year');
        $from = request()->get('from');
        $to = request()->get('to');
        $type_date = (int)request()->get('type_date');
        $payment_type = (int)request()->get('payment_type');


        $data_val = 'payments.payment_data';
        if($type_date == 1) $data_val = 'payments.payment_data';//Даты оплаты
        if($type_date == 2) $data_val = 'payments.invoice_payment_date';//Дата по кассе

        $havings = [];

        if($payment_type != -1){
            $havings[] = "payments.type_id = $payment_type";
        }


        switch(request('period')){

            case 0: //month
                $first_time = strtotime("{$year}-{$month}-01 00:00:00");
                $last_day_in_month = date('t', $first_time);
                $last_time = strtotime("{$year}-{$month}-{$last_day_in_month} 23:59:59");

                $first_time--;
                $last_time++;

                $havings[] = "UNIX_TIMESTAMP({$data_val}) > {$first_time}";
                $havings[] = "UNIX_TIMESTAMP({$data_val}) < {$last_time}";

                break;

            case 1: //year
                $first_time = strtotime("{$year}-01-01 00:00:00");
                $last_day_in_month = date('t', $first_time);
                $last_time = strtotime("{$year}-12-{$last_day_in_month} 23:59:59");

                $first_time--;
                $last_time++;

                $havings[] = "UNIX_TIMESTAMP({$data_val}) > {$first_time}";
                $havings[] = "UNIX_TIMESTAMP({$data_val}) < {$last_time}";
                break;

            case 2:
                $havings[] = "1=1";
                break;
            case 3: //period
                $first_time = strtotime($from);
                $last_time = strtotime($to);

                $first_time--;
                $last_time++;

                $havings[] = "UNIX_TIMESTAMP({$data_val}) > {$first_time}";
                $havings[] = "UNIX_TIMESTAMP({$data_val}) < {$last_time}";
                break;
        }

        return implode(" AND ", $havings);
    }

    private function get_labels($checkpoints){

        $labels = [];

        switch(request('period')){

            case 0: // month
            case 2: // all
            case 3: // period

                $labels = array_unique(array_map(function($item){
                    return date('d.m.Y', $item);
                }, $checkpoints));

                break;

            case 1: //year

                $labels = array_values(getRuMonthes());

                break;

        }

        return $labels;
    }

}