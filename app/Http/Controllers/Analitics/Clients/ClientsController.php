<?php

namespace App\Http\Controllers\Analitics\Clients;

use App\Helpers\PaginationHelper;
use App\Http\Controllers\Controller;
use App\Http\QueryHandlers\Analitics\Clients\AnaliticsContractsQueryHandler;
use App\Http\QueryHandlers\Contracts\Accepted\AcceptsQueryHandler;
use App\Models\Actions\PaymentAccept;
use App\Models\Contracts\Contracts;

class ClientsController extends Controller{


    public function __construct(){
        $this->middleware('permissions:analitics,clients');
    }


    public function index(){
        return view('analitics.clients.index');
    }



    public function get_table(){

        $data = $this->get_list();

        $data['html'] = view('analitics.clients.table', ['data' => $data])->render();
        return $data;

    }


    public function get_list(){

        $contractsAccepted = (new AnaliticsContractsQueryHandler(Contracts::releasedContracts()))->allowEmpty()->apply();
        $page = request()->get('PAGE') > 0 ? (int)request()->get('PAGE') : 1;
        $page_count = request()->get('page_count') > 0 ? (int)request()->get('page_count') : 10;
        $result = PaginationHelper::paginate($contractsAccepted, $page, $page_count);

        $contractsAccepted = $result['builder']->get();

        return [
            'contractsAccepted' => $contractsAccepted,
            'page_max' => $result['page_max'],
            'page_sel' => $result['page_sel'],
            'max_row' => $result['max_row'],
            'view_row' => $result['view_row'],
        ];

    }
}