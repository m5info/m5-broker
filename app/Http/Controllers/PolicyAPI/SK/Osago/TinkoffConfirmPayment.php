<?php


namespace App\Http\Controllers\PolicyAPI\SK\Osago;


use App\Http\Controllers\Controller;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use Illuminate\Http\Request;

class TinkoffConfirmPayment extends Controller{

    public function handle(Request $request)
    {
        $data = \GuzzleHttp\json_decode($request->getContent());

        if($sk_key_id = $data->setNumber){

            $calc = ContractsCalculation::query()
                ->where('sk_key_id', '=', $sk_key_id)
                ->first();

            if($calc){
                $contract = $calc->contract;
                if ($calculation = $contract->selected_calculation) {
                    if ($service = $calculation->get_product_service()) {

                        $calculation->payment_confirm = 1;
                        $calculation->save();

                        return $this->tinkoffResponse();
                    }
                }
            }
        }

        return false;
    }

    public function tinkoffResponse(){

        $arr = [
            'Header' => [
                'resultInfo' => [
                    'status' => 'OK',
                    'agreement_current_status' => 'issued'
                ]
            ]
        ];

        return \GuzzleHttp\json_encode($arr);
    }
}