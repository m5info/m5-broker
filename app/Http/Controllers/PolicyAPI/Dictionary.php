<?php

namespace App\Http\Controllers\PolicyAPI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Dictionary extends Controller
{

    public $errors = [];
    public $answer = [];

    const DICT_TYPE = [
        1 => 'Marks',
        2 => 'Models',
        3 => 'Categories',
        4 => 'Purpose',
    ];

    public function get_dictionary(Request $request){

        $data = \GuzzleHttp\json_decode($request->getContent());

        if(isset($data->code)){

            if(in_array($data->code, [1,2,3,4])){

                $className = 'App\Models\Vehicle\Vehicle'.self::DICT_TYPE[$data->code];

                foreach($className::all() as $dict){

                    $this->answer[] = [
                        'id' => $dict->id,
                        'title' => $dict->title,
                    ];
                }

            }else{
                $this->errors[] = 'Справочника с таким кодом не существует! Допустимые значения: 1, 2, 3, 4.';
            }

        }else{
            $this->errors[] = 'Некорректно составлено тело запроса!';
        }

        $response = count($this->errors) > 0 ? implode(';', $this->errors) : \GuzzleHttp\json_encode($this->answer);
        $status = count($this->errors) > 0 ? 500 : 200;

        return response()->json($response, $status)->header('Content-Type', 'application/json');
    }
}
