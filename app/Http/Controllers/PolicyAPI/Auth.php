<?php

namespace App\Http\Controllers\PolicyAPI;

use App\Models\User;
use App\Models\Users\ApiTokens;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Auth extends Controller
{
    /**
     Пример запроса:
        {
            "login":"login",
            "password": "123123"
        }
     */
    public function get_token(Request $request){

        $response = null;
        $data = \GuzzleHttp\json_decode($request->getContent());

        $user = User::where('email', '=', $data->login)->first();

        if($user->is('partner')){ // партнер

            $api_token = $user->api_token;

            if(!$api_token){
                $api_token = new ApiTokens;
                $api_token->user_id = $user->id;
                $api_token->save();
            }

            $response['token'] = base64_encode($user->email . date('H:i:s') . $user->password);

            $api_token->api_token = $response['token'];
            $api_token->ip_address = request()->ip();
            $api_token->save();

        }

        return \GuzzleHttp\json_encode($response);
    }
}
