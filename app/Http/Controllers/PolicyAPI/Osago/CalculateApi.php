<?php

namespace App\Http\Controllers\PolicyAPI\Osago;

use App\Domain\Processes\Scenaries\Contracts\Online\OnlineContractSave;
use App\Http\Controllers\Contracts\Online\OnlineController;
use App\Http\Controllers\PolicyAPI\Auth;
use App\Http\Requests\Api\OsagoRequest;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\Directories\Products;
use App\Models\Users\ApiTokens;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CalculateApi extends Controller
{
    public $errors = [];
    public $state = false;
    public $answer = [];

    public function calculate(OsagoRequest $request){

        $data = \GuzzleHttp\json_decode($request->getContent());

        // находим контракт или создаем новый
        $contract_id = $this->create_contract($data);

        $contract = Contracts::getContractId($contract_id);

        // сохраняем контракт с данными
        $save = OnlineContractSave::handle($contract, $data);

        // получаем список калькуляций
        $get_calc_list = $this->get_calc_list($save, $contract_id);

        // подготавливаем ответ с расчетами
        $this->prepare_answer($contract, $get_calc_list);

        if(count($this->errors) > 0 || $this->state == false){
            return response()->json(implode(';', $this->errors), 200)->header('Content-Type', 'application/json');
        }

        return response()->json($this->answer, 200)->header('Content-Type', 'application/json');
    }


    public function create_contract($data){

        $product_id = null;
        $program_id = null;

        if($data->product == 'osago'){
            $product_id = 10;
        }

        $product = Products::where('is_actual', '1')->where('id', '=', $product_id)->first();

        if (!$product) {
            return false;
        }

        if(isset($data->contractId) && $data->contractId != ''){

            // находим существующий
            if($contract = Contracts::find($data->contractId)){

                // проверяем, принадлежит ли контракт партнеру
                if($contract->user_id != auth()->id()){
                    $this->errors[] = "Пользователь ". auth()->user()->email ." не имеет доступ к договору №". $data->contractId;
                }

            }else{
                $this->errors[] = "Договор №". $data->contractId ." не найден";
            }

        }else{
            $contract = new Contracts;
            $contract->product_id = $product->id;
            $contract->program_id = $program_id;
            $contract->agent_id = auth()->id();
            $contract->user_id = auth()->id();
            $contract->statys_id = 0;
            $contract->is_online = 1;
            $contract->save();
        }

        return $contract->id;
    }

    public function get_calc_list($save, $contract_id){

        $result = (object)['state' => false, 'calculation_list' => []];

        if($save['state']){ // сохранили

            $calc_list = app('App\Http\Controllers\Contracts\Online\OnlineController')->get_calculate_list($contract_id);
            $calc_list = \GuzzleHttp\json_decode($calc_list->getContent());

            if(count($calc_list) == 0){
                $this->errors[] = "Не найдены СК для данного продукта!";
            }else{
                $result->state = true;
                $result->calculation_list = $calc_list;
            }

        }else{
            $this->errors[] = $save['msg'];
        }

        return $result;
    }


    public function prepare_answer($contract, $get_calc_list){

        $calc_answer = [];
        $drivers_answer = [];

        if($get_calc_list->state){

            $calc_list = $get_calc_list->calculation_list;

            foreach($calc_list as $key => $calc_id){

                $calculation = ContractsCalculation::find($calc_id);
                $calculation->calculate();

                $calc_answer[$key] = [
                    'calc_id' => $calculation->id,
                    'sk_key' => $calculation->sk_key_id,
                    'sk' => $calculation->insurance_companies->title,
                    'sum' => $calculation->sum,
                    'messages' => $calculation->messages,
                ];

            }

            $this->answer['contractId'] = $contract->id;
            $this->answer['product'] = $contract->product->title;
            $this->answer['calculations'] = $calc_answer;
            $this->answer['insurer_id'] = $contract->insurer_id;
            $this->answer['owner_id'] = $contract->owner_id;
            $this->answer['object_insurer_id'] = $contract->object_insurer_id;

            if(isset($contract->drivers_type_id) && $contract->drivers_type_id == 1){

                $this->answer['drivers_type_id'] = 1;

            }else{

                foreach($contract->drivers as $key => $driver){
                    $drivers_answer[$key]['id'] = $driver->id;
                    $drivers_answer[$key]['fio'] = $driver->fio;
                }

                $this->answer['drivers_type_id'] = 0;
                $this->answer['drivers'] = $drivers_answer;
            }

            $this->state = true;
        }

    }
}
