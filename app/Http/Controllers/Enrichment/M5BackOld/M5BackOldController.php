<?php

namespace App\Http\Controllers\Enrichment\M5BackOld;


use App\Domain\Processes\Operations\Enrichment\M5BackOld\M5BackOldClean;
use App\Domain\Processes\Scenaries\Enrichment\M5BackOld\M5BackOldMain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class M5BackOldController extends Controller{


    public function __construct()
    {
        $this->middleware('permissions:enrichment,m5_back_old');
    }


    public function index(Request $request)
    {
        return view('enrichment.m5_back_old.index', [

        ]);
    }

    public function startInfo(Request $request)
    {
        return response()->json(M5BackOldMain::getDataResult($request->type, $request->start, $request->counts, $request->count_all, $request, 'info'));
    }


    public function updataInfo(Request $request)
    {
        return response()->json(M5BackOldMain::getDataResult($request->type, $request->start, $request->counts, $request->count_all, $request, 'updata'));
    }


    public function clearSystem(Request $request)
    {

        $res = new \stdClass();
        $res->state = 1;
        $res->msg = "Error";

        $clean = 1;

        if($clean == 1){

            M5BackOldClean::clean();
            $res->state = 0;
            $res->msg = "Система очищина";

        }

        return response()->json($res);
    }



}
