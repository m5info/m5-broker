<?php

namespace App\Http\Controllers\Enrichment\AV100;


use App;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\FilesRepository;
use App\Http\Controllers\Controller;


class AV100Controller extends Controller{


    public function __construct()
    {
        $this->middleware('permissions:enrichment,av100');
    }


    public function index(Request $request)
    {
        return view('enrichment.av100.index', [

        ]);
    }


    public function uploadFile(Request $request)
    {

        $res = [];
        if ($request->hasFile('file')) {
            $ext = $request->file('file')->getClientOriginalExtension();
            if(in_array($ext, ['xls', 'xlsx', 'docx'])){
                $file = (new FilesRepository)->makeFile($request->file, "temp/av100");
                $path = storage_path() . '/app/' . $file->getPathAttribute();

                /* Хук для локальной версии */
                $path = storage_path() . '/app/' . ltrim(str_replace(['//', '127.0.0.1'], ['/', ''], $file->path_with_host), '/');

                //dd($url);


                $res['file'] = $path;
                $res['columns'] = getColonsExelFile($path);
            }
        }
        return response()->json($res);
    }


    public function loadData(Request $request)
    {
        $path = $request->file;
        $columns = $request->excel_columns;

        $xls = \PHPExcel_IOFactory::load($path);
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();

        $columnsIndex = array();

        for ($i = 0; $i <= 1; $i++) {
            $nColumn = \PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());

            for ($c = 0; $c <= $nColumn; $c++) {
                $val = $sheet->getCellByColumnAndRow($c, $i)->getValue();
                if (strlen($val) > 0) array_push($columnsIndex, $val);
            }
        }


        $columns_contract = [];
        $keys = array_keys($columns);

        for ($i = 1; $i <= $sheet->getHighestRow(); $i++) {
            $reg_number = '';
            $vin = '';
            $_temp_columns_contract = [];
            for ($c = 0; $c <= count($columnsIndex); $c++) {

                $Val = $sheet->getCellByColumnAndRow($c, $i)->getValue();
                $_temp_columns_contract[] = $Val;

                for ($s = 0; $s <= count($keys); $s++) {
                    if(isset($keys[$s]) && isset($columnsIndex[$c]) && $columns[$keys[$s]] == $columnsIndex[$c]){
                        if($keys[$s] == 'reg_number') $reg_number = $Val;
                        if($keys[$s] == 'vin') $vin = $Val;
                    }
                }
            }
            if($i == 1){
                $_temp_columns_contract[] = 'AV100';
                $_temp_columns_contract[] = 'AV100 parkon';
            }else{

                $_temp_columns_contract[] = App\Services\AVDataInfo\IntegrationAVDataInfo::getPhoneStr($vin, $reg_number);
                $_temp_columns_contract[] = App\Services\AVDataInfo\IntegrationAVDataInfo::getPhoneParkon($reg_number);
            }

            if(count($_temp_columns_contract)>0) array_push($columns_contract, $_temp_columns_contract);
        }


        //Удаляем файл с сервака
        unlink($path);

        Excel::create('AV100_'.date('d.m.Y H:i:s'), function($excel) use ($columns_contract) {

            $excel->sheet('Sheetname', function($sheet) use ($columns_contract) {

                $sheet->fromArray($columns_contract);

            });

        })->export('xlsx');



    }

}
