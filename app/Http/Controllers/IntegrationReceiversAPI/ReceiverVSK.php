<?php

namespace App\Http\Controllers\IntegrationReceiversAPI;

use App\Http\Controllers\Controller;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;
use App\Models\IntegrationOnline\VskApiOnline;
use Illuminate\Http\Request;

class ReceiverVSK extends Controller
{

    public function calc_response(Request $request)
    {
        try{

            $response = simplexml_load_string(stripcslashes($request->getContent()));

            $query = VskApiOnline::query()->where('id_correlation', $request->headers->get('X-VSK-CorrelationId'))->first();

            $xml_data = [];

            foreach ($response->children('com', true) as $children){
                $xml_data[$children->getName()] = (string)$children;
            }

            if (isset($xml_data['error'])){
                $query->error = 'Внешняя ошибка сервиса VSK. Попробуйте позже.';
            }

            if ($query->step == 'calc'){
                foreach ($response->children('pol', true) as $children){
                    $xml_data[$children->getName()] = (string)$children;
                }

                $current_calc = ContractsCalculation::query()->where('id', $request->headers->get('X-VSK-CorrelationId'))->first();
                $current_calc->sum = twoNumbersEndFormat($xml_data['amount']);
                $current_calc->save();

                $query->temp_amount = twoNumbersEndFormat($xml_data['amount']);
                $query->agent_kv = $current_calc->agent_kv;

                $xml_data['agent_kv'] = $current_calc->agent_kv;
                $xml_data['test'] = $current_calc->toArray();
            }


            if ($query->step == 'auth'){
                foreach ($response->children('sch1', true) as $children){
                    $xml_data[$children->getName()] = (string)$children;
                }
            }

            if ($query){

                $temp = '';

                if ($query->step == 'auth'){
                    $temp = 'calc';
                }else if($query->step == 'calc'){
                    $temp = 'end_point';
                }

                /*if ($query->step == 'auth'){
                    $temp = 'end_point';
                }else if($query->step == 'calc'){
                    $temp = 'end_point';
                }*/


                $query->step = $temp;

                $query->body = stripcslashes($request->getContent());
                $query->headers = stripslashes($request->headers);

                $query->id_correlation = $request->headers->get('X-VSK-CorrelationId');

                if (isset($xml_data['messageId'])){
                    $query->id_message = $xml_data['messageId'];
                }
                if (isset($xml_data['bpId'])){
                    $query->id_bp = $xml_data['bpId'];
                }
                if (isset($xml_data['sessionId']) && $xml_data['sessionId'] !== ''){
                    $query->id_session = $xml_data['sessionId'];
                }

                $query->save();

            }

            $data = [
                'topic_id' => 'Vsk_osago_'.$request->headers->get('X-VSK-CorrelationId'),
                'response' => $xml_data,
                'correlation_id' => $request->headers->get('X-VSK-CorrelationId'),
                'step' => $query->step
            ];

            \App\Classes\Socket\Pusher::sendDataToServer($data);

        }catch(\Exception $e){

            $data = [
                'topic_id' => 'Vsk_osago_'.$request->headers->get('X-VSK-CorrelationId'),
                'error' => $e->getMessage()
            ];

            \App\Classes\Socket\Pusher::sendDataToServer($data);
        }

        return 1;
    }

    public function save_policy_response(Request $request)
    {
        try{

            $response = simplexml_load_string(stripcslashes($request->getContent()));

            $query = VskApiOnline::query()->where('id_correlation', $request->headers->get('X-VSK-CorrelationId'))->first();

            $xml_data = [];

            foreach ($response->children('com', true) as $children){
                $xml_data[$children->getName()] = (string)$children;
            }

            if (isset($xml_data['error'])){
                $query->error = 'Внешняя ошибка сервиса VSK. Попробуйте позже.';
            }

            if ($query->step == 'auth'){
                foreach ($response->children('sch1', true) as $children){
                    $xml_data[$children->getName()] = (string)$children;
                }
            }

            if ($query){

                $temp = '';

                if ($query->step == 'auth'){
                    $temp = 'savePolicy';
                }elseif($query->step == 'savePolicy'){
                    $temp = 'end_point';
                }

                $query->step = $temp;

                $query->body = stripcslashes($request->getContent());
                $query->headers = stripslashes($request->headers);

                $query->id_correlation = $request->headers->get('X-VSK-CorrelationId');

                if (isset($xml_data['messageId'])){
                    $query->id_message = $xml_data['messageId'];
                }
                if (isset($xml_data['bpId'])){
                    $query->id_bp = $xml_data['bpId'];
                }
                if (isset($xml_data['sessionId'])){
                    $query->id_session = $xml_data['sessionId'];
                }

                $query->save();

            }

            $data = [
                'topic_id' => 'Vsk_osago_'.$request->headers->get('X-VSK-CorrelationId'),
                'response' => $xml_data,
                'correlation_id' => $request->headers->get('X-VSK-CorrelationId'),
                'step' => $query->step
            ];

            \App\Classes\Socket\Pusher::sendDataToServer($data);

        }catch(\Exception $e){

            $data = [
                'topic_id' => 'Vsk_osago_'.$request->headers->get('X-VSK-CorrelationId'),
                'error' => $e->getMessage(),
            ];

            \App\Classes\Socket\Pusher::sendDataToServer($data);
        }

        return 1;
    }

    public function sign_response(Request $request)
    {
        try{

            $response = simplexml_load_string(stripcslashes($request->getContent()));

            $query = VskApiOnline::query()->where('id_correlation', $request->headers->get('X-VSK-CorrelationId'))->first();

            $xml_data = [];

            foreach ($response->children('com', true) as $children){
                $xml_data[$children->getName()] = (string)$children;
            }

            if (isset($xml_data['error'])){
                $query->error = 'Внешняя ошибка сервиса VSK. Попробуйте позже.';
            }

            if ($query->step == 'auth'){
                foreach ($response->children('pol', true) as $children){
                    $xml_data[$children->getName()] = (string)$children;
                }

                $current_calc = ContractsCalculation::query()->where('id', $request->headers->get('X-VSK-CorrelationId'))->first();
                $current_calc->save();

                $query->agent_kv = $current_calc->agent_kv;

                if ($xml_data['policyNumber'] != ''){

                    $query->policy_number = $xml_data['policyNumber'];
                    $query->sms_verify = 1;

                    $contractObj = Contracts::findOrFail($query->id_correlation);
                    $contractObj->is_epolicy = 1;
                    $contractObj->bso_title = $xml_data['policyNumber'];

                    $current_calc->sms_verify = 1;
                    $current_calc->save();

                }


                $xml_data['agent_kv'] = $current_calc->agent_kv;
                $xml_data['test'] = $current_calc->toArray();
            }

            if ($query){

                $temp = '';

                if ($query->step == 'auth'){
                    $temp = 'sign';
                }elseif($query->step == 'sign'){
                    $temp = 'end_point';
                }

                $query->step = $temp;

                $query->body = stripcslashes($request->getContent());
                $query->headers = stripslashes($request->headers);

                $query->id_correlation = $request->headers->get('X-VSK-CorrelationId');

                if (isset($xml_data['messageId'])){
                    $query->id_message = $xml_data['messageId'];
                }
                if (isset($xml_data['bpId'])){
                    $query->id_bp = $xml_data['bpId'];
                }
                if (isset($xml_data['sessionId'])){
                    $query->id_session = $xml_data['sessionId'];
                }

                $query->save();

            }

            if ($xml_data['policyNumber'] == ''){

                $data = [
                    'topic_id' => 'Vsk_osago_'.$request->headers->get('X-VSK-CorrelationId'),
                    'error' => 'Код не подходит, введите верный код.',
                ];

            }else{

                $data = [
                    'topic_id' => 'Vsk_osago_'.$request->headers->get('X-VSK-CorrelationId'),
                    'response' => $xml_data,
                    'correlation_id' => $request->headers->get('X-VSK-CorrelationId'),
                    'step' => $query->step
                ];

            }

            \App\Classes\Socket\Pusher::sendDataToServer($data);

        }catch(\Exception $e){

            $data = [
                'topic_id' => 'Vsk_osago_'.$request->headers->get('X-VSK-CorrelationId'),
                'error' => $e->getMessage(),
            ];

            \App\Classes\Socket\Pusher::sendDataToServer($data);
        }

        return 1;
    }

    public function buy_policy_response(Request $request)
    {
        try{

            $response = simplexml_load_string(stripcslashes($request->getContent()));

            $query = VskApiOnline::query()->where('id_correlation', $request->headers->get('X-VSK-CorrelationId'))->first();

            $xml_data = [];

            foreach ($response->children('com', true) as $children){
                $xml_data[$children->getName()] = (string)$children;
            }

            if (isset($xml_data['error'])){
                $query->error = 'Внешняя ошибка сервиса VSK. Попробуйте позже.';
            }

            if ($query->step == 'auth'){
                foreach ($response->children('pol', true) as $children){
                    $xml_data[$children->getName()] = (string)$children;
                }

                $current_calc = ContractsCalculation::query()->where('id', $request->headers->get('X-VSK-CorrelationId'))->first();
                $current_calc->save();

                $query->agent_kv = $current_calc->agent_kv;

                $xml_data['agent_kv'] = $current_calc->agent_kv;
                $xml_data['test'] = $current_calc->toArray();
            }

            if ($query){

                $temp = '';

                if ($query->step == 'auth'){
                    $temp = 'buyPolicy';
                }elseif($query->step == 'buyPolicy'){
                    $temp = 'end_point';
                }

                $query->step = $temp;

                $query->body = stripcslashes($request->getContent());
                $query->headers = stripslashes($request->headers);

                $query->id_correlation = $request->headers->get('X-VSK-CorrelationId');

                if (isset($xml_data['messageId'])){
                    $query->id_message = $xml_data['messageId'];
                }
                if (isset($xml_data['bpId'])){
                    $query->id_bp = $xml_data['bpId'];
                }
                if (isset($xml_data['sessionId'])){
                    $query->id_session = $xml_data['sessionId'];
                }

                $query->save();

            }

            $data = [
                'topic_id' => 'Vsk_osago_'.$request->headers->get('X-VSK-CorrelationId'),
                'response' => $xml_data,
                'correlation_id' => $request->headers->get('X-VSK-CorrelationId'),
                'step' => $query->step
            ];

            \App\Classes\Socket\Pusher::sendDataToServer($data);

        }catch(\Exception $e){

            $data = [
                'topic_id' => 'Vsk_osago_'.$request->headers->get('X-VSK-CorrelationId'),
                'error' => $e->getMessage()
            ];

            \App\Classes\Socket\Pusher::sendDataToServer($data);
        }

        return 1;
    }

    public function show_responses()
    {

        return VskApiOnline::all()->toArray();
    }

    public function clear_responses()
    {
        try{
            $t = VskApiOnline::query();
            $t->delete();
            return 1;
        }catch(\Exception $e){
            return $e->getMessage();
        }

    }



}