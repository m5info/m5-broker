<?php

namespace App\Http\Controllers\IntegrationReceiversAPI;

use App\Http\Controllers\Controller;
use App\Http\Controllers\IntegrationReceiversAPI\Traits\ReceiverMustValidator;
use App\Models\IntegrationOnline\WebhookReceiver;
use Illuminate\Http\Request;

class ReceiverMUST extends Controller
{
    use ReceiverMustValidator;
    private $identifier = 'integration_must';
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->receiveRequest();
        $this->saveResponse();
    }

    /**
     *  Шаг 1.
     *  Авторизация в системе (получение токена)
     */
    public function getToken()
    {
        $this->getTokenValidator();
    }

    /**
     *  Шаг 2.
     *  Инициализация процесса
     */
    public function initProcess()
    {
        $this->initProcessValidator();
    }

    private function saveResponse()
    {
        $wh = new WebhookReceiver();
        $wh->identifier = $this->identifier;
        $wh->body = json_encode($this->body);
        $wh->headers = json_encode($this->request->headers->all());
        $wh->save();
    }
}