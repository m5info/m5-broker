<?php

namespace App\Http\Controllers\IntegrationReceiversAPI\Traits;

trait ReceiverMustValidator
{
    private $body;
    private $validResult;

    /**
     *  Шаг 1.
     *  Авторизация в системе (получение токена)
     */
    private function getTokenValidator()
    {
        $this->validResult['token'] = $this->body['token'];
    }

    /**
     *  Шаг 2.
     *  Инициализация процесса
     */
    private function initProcessValidator()
    {
        $this->validResult['scoringId'] = $this->body['scoringId'];
    }



    /**
     * Первоначальная обработка JSON body
     */
    private function receiveRequest()
    {
        $this->body = json_decode($this->request->getContent(), true);
    }
}