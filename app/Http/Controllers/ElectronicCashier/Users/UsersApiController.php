<?php

namespace App\Http\Controllers\ElectronicCashier\Users;


use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class UsersApiController extends Controller{

    public function __construct()
    {
        //$this->middleware('guest');
    }

    public function info($id)
    {
        $user = User::where('id', $id)->get()->first();
        return response()->json($user);
    }


    public function set_user_balance(Request $request)
    {


        $json = \GuzzleHttp\json_decode($request->json);


        $elka_id = (int) $json->elka_id;
        $agent_id = (int) $json->agent_id;
        $summ = (int) $json->summ;
        $state = (int) $json->state; //0 - balance 1 - cashbox

        if($elka_id<=0){
            return response('0');
        }


        $arr = array();
        $arr['agent_name'] = '';
        $arr['elka_name'] = '';
        $arr['elka_counter'] = '';
        $arr['cashbox_id'] = 0;
        $arr['total_payment_value'] = $summ;
        $arr['balance_agent'] = 0;
        $arr['balance_short_change'] = 0;

        $arr['total_payment_value'] = titleFloatFormat($summ);
        $arr['balance_agent'] = titleFloatFormat($arr['balance_agent']);
        $arr['balance_short_change'] = titleFloatFormat($arr['balance_short_change']);

        $agent = User::where('id', $agent_id)->get()->first();
        $elka = User::where('id', $elka_id)->get()->first();



        $balance = $agent->getBalanceProfit();
        $balance->setTransactions(
            1,
            0,
            getFloatFormat($summ),
            date('Y-m-d H:i:s'),
            'Приход в кассу, Электронный кассир',
            $elka
        );

        $arr['agent_name'] = $agent->name;
        $arr['elka_name'] = $elka->name;
        $arr['elka_counter'] = $elka->cashbox->getCountTransaction();


        return response()->json($arr);

    }


}
