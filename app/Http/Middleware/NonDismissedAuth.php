<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class NonDismissedAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->isMethod('post') && $request->path() == 'login'){

            $user = User::where('email', '=', $request->email)->first();

            if($user && $user->status_user_id == 1){ // если уволен
                return redirect()->back()->with(['error' => 'Данный пользователь заблокирован!'])->withInput();
            }else{
                return $next($request);
            }
        }
        return $next($request);
    }
}
