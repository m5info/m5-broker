<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Permissions
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param $permissionGroupName
     * @param $permissionName
     * @return mixed
     */
    public function handle($request, Closure $next, $permissionGroupName, $permissionName)
    {
        if (!auth()->check() || !auth()->user()->role) {

            return abort(403, 'Permission denied');

        }

        if (!auth()->user()->hasPermission($permissionGroupName, $permissionName)) {

            return abort(403, 'Permission denied');

        }

        return $next($request);
    }
}
