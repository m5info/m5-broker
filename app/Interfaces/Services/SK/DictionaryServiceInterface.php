<?php

namespace App\Interfaces\Services\SK;

interface DictionaryServiceInterface
{
//
//    public function get_api_marks($vehicle_categories_sk);
//    public function get_api_model($mark_id);
    /**
     * Формат данных для выхода
     *
     * $arr = [
     *      1 => array:2 [
     *          "id" => "1"
     *          "title" => "Личные"
     *      ],
     *      2 => array:2 [
     *          "id" => "2"
     *          "title" => "Такси"
     *      ],
     *      ...
     * ]
     * @return mixed
     */
    public function get_api_purpose();

    /**
     * Формат данных для выхода
     *
     * $arr = [
     *      1 => array:2 [
     *          "id" => "1"
     *          "title" => "Легковые"
     *      ],
     *      2 => array:2 [
     *          "id" => "2"
     *          "title" => "Грузовые"
     *      ],
     *      ...
     * ]
     * @return mixed
     */
    public function get_api_categories();

    /**
     * Формат данных для выхода
     *
     * $arr = [
     *   'categories' =>
     *      [
     *          [
     *              'id'=>'1',
     *              'title' => 'A'
     *          ],
     *          [
     *              'id'=>'2',
     *              'title' => 'B'
     *          ]
     *      ],
     *   'marks' =>
     *      [
     *          [
     *              'id' => '1',
     *              'title' => 'Название_марки',
     *              'vehicle_categorie_sk_id' => 'ИД_категории'
     *          ],
     *          [
     *              'id' => '2',
     *              'title' => 'Название_марки'
     *              'vehicle_categorie_sk_id' => 'ИД_категории'
     *          ]
     *      ],
     *    'models' =>
     *      [
     *          [
     *              'id' => '1',
     *              'title' => 'Название_модели',
     *              'vehicle_mark_sk_id' => 'ИД_марки',
     *              'vehicle_categorie_sk_id' => 'ИД_категории'
     *          ],
     *          [
     *              'id' => '2',
     *              'title' => 'Название_модели',
     *              'vehicle_mark_sk_id' => 'ИД_марки',
     *              'vehicle_categorie_sk_id' => 'ИД_категории'
     *          ],
     *      ]
     * ]
     * @return mixed
     */
    public function get_marks_models();

}