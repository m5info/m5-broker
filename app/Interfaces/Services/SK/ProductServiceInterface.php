<?php

namespace App\Interfaces\Services\SK;

use App\Models\Contracts\Contracts;
use App\Models\Contracts\ContractsCalculation;

interface ProductServiceInterface{

    public function temp_calc(ContractsCalculation $calc);

    public function calc(ContractsCalculation $calc);

    public function release(ContractsCalculation $calc);

    public function check_status(ContractsCalculation $calc);

    public function get_files(ContractsCalculation $calc);

    # Если включено СМС-подтверждение, реализовать метод
//  public function sign(ContractsCalculation $calc);

    # Если интеграция требует подтверждение покупки полиса, реализовать метод
//  public function savePolicy(ContractsCalculation $calc);

}