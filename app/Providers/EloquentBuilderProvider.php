<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;

class EloquentBuilderProvider extends ServiceProvider{

    public function boot(){

        Builder::macro("whereJson", function ($column, $key, $val) {
            $json_string =  '"'.$key.'":"'.$val.'"';
            return $this->whereRaw("{$column} like '%{$json_string}%'");
        });

    }

}
