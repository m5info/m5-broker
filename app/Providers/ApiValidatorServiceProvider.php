<?php


namespace App\Providers;


use App\Models\Contracts\Contracts;
use App\Models\Vehicle\VehicleCategories;
use App\Models\Vehicle\VehicleMarks;
use App\Models\Vehicle\VehicleModels;
use App\Models\Vehicle\VehiclePurpose;
use Illuminate\Support\ServiceProvider;

class ApiValidatorServiceProvider extends ServiceProvider{

    public function boot()
    {
        $this->app['validator']->extend('contract', function ($attribute, $value, $parameters)
        {

            $contract = Contracts::where('id', $value)->first();

            if ($contract && $contract->agent_id == auth()->id()) {
                return true;
            }

            return false;
        }, 'Данный договор не найден или не принадлежит партнеру!');

        $this->app['validator']->extend('float', function ($attribute, $value, $parameters)
        {

            if(gettype($value) == 'double'){
                return true;
            }

            return false;
        }, 'Параметр должен быть типа float (Пример: 1.00).');


        $this->app['validator']->extend('ts_category', function ($attribute, $value, $parameters)
        {

            if(VehicleCategories::find($value)){
                return true;
            }else{
                return false;
            }

        }, 'Категория авто не найдена! Синхронизируйте справочник категорий авто.');


        $this->app['validator']->extend('mark', function ($attribute, $value, $parameters)
        {

            if(VehicleMarks::find($value)){
                return true;
            }else{
                return false;
            }

        }, 'Марка авто не найдена! Синхронизируйте справочник марок.');


        $this->app['validator']->extend('model', function ($attribute, $value, $parameters)
        {

            if(VehicleModels::find($value)){
                return true;
            }else{
                return false;
            }

        }, 'Модель авто не найдена! Синхронизируйте справочник моделей.');


        $this->app['validator']->extend('purpose', function ($attribute, $value, $parameters)
        {

            if(VehiclePurpose::find($value)){
                return true;
            }else{
                return false;
            }

        }, 'Цель использования авто не найдена! Синхронизируйте справочник целей использования.');
    }

}