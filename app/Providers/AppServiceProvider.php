<?php

namespace App\Providers;


use App\Models\BSO\BsoItem;
use App\Models\Cashbox\IncomeExpense;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Models\Contracts\Subjects;
use App\Models\Contracts\SubjectsFl;
use App\Models\Contracts\SubjectsUl;
use App\Models\Finance\Invoice;
use App\Models\Menu;
use App\Observers\BsoItemsObserver;
use App\Observers\ContractsObserver;
use App\Observers\IncomesExpensesObserver;
use App\Observers\InvoicesObserver;
use App\Observers\PaymentsObserver;
use App\Observers\SubjectsFLObserver;
use App\Observers\SubjectsObserver;
use App\Observers\SubjectsULObserver;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Validators;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {

        Carbon::setLocale(config('app.locale'));

        view()->composer('partials.menu', function ($view) {
            $view->with('menuItems', Menu::all());
        });

        Validator::extend('inn', function ($attribute, $value, $parameters, $validator) {
            return Validators::validateInn($value);
        });

        Validator::extend('kpp', function ($attribute, $value, $parameters, $validator) {
            return Validators::validatekpp($value);
        });


        $this->boot_string_func();

        Contracts::observe(ContractsObserver::class);
        Invoice::observe(InvoicesObserver::class);
        Payments::observe(PaymentsObserver::class);
        BsoItem::observe(BsoItemsObserver::class);
        IncomeExpense::observe(IncomesExpensesObserver::class);
        Subjects::observe(SubjectsObserver::class);
        SubjectsFl::observe(SubjectsFLObserver::class);
        SubjectsUl::observe(SubjectsULObserver::class);
    }


    public function boot_string_func(){

        Str::macro('one_line', function($str){
            return str_replace(["\r\n", "\r", "\n", "\t", '  ', '    ', '    '], '', $str);
        });

        Str::macro('json_export', function($str){
            $str = str_replace('{', '[', $str);
            $str = str_replace('}', ']', $str);
            $str = str_replace('":', '" =>', $str);
            $str = str_replace('" :', '" =>', $str);
            $str = str_replace('"true"', 'true', $str);
            $str = str_replace('"false"', 'false', $str);

            return $str;
        });

    }

}
