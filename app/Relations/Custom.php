<?php
namespace App\Relations;

use Closure;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Custom extends Relation{

    protected $baseConstraints;
    protected $eagerConstraints;

    public function __construct(Builder $query, Model $parent, Closure $baseConstraints, Closure $eagerConstraints){
        $this->baseConstraints = $baseConstraints;
        $this->eagerConstraints = $eagerConstraints;
        parent::__construct($query, $parent);
    }

    public function addConstraints(){
        call_user_func($this->baseConstraints, $this);
    }

    public function addEagerConstraints(array $models){
        call_user_func($this->eagerConstraints, $this, $models);
    }

    public function initRelation(array $models, $relation){
        foreach ($models as $model) {
            $model->setRelation($relation, $this->related->newCollection());
        }
        return $models;
    }

    public function match(array $models, Collection $results, $relation){
        $dictionary = $this->buildDictionary($results);
        foreach ($models as $model) {
            if (isset($dictionary[$key = $model->getKey()])) {
                $collection = $this->related->newCollection($dictionary[$key]);
                $model->setRelation($relation, $collection);
            }
        }
        return $models;
    }

    public function getResults(){
        return $this->get();
    }

    public function get($columns = ['*']){

        $columns = $this->query->getQuery()->columns ? [] : $columns;
        if ($columns == ['*']) {
            $columns = [$this->related->getTable().'.*'];
        }

        $builder = $this->query->applyScopes();
        $models = $builder->addSelect($columns)->getModels();

        if (count($models) > 0) {
            $models = $builder->eagerLoadRelations($models);
        }
        return $this->related->newCollection($models);
    }

}