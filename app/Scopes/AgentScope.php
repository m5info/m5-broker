<?php
namespace App\Scopes;

use App\Models\User;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class AgentScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereIn('ban_level', [0, 1]);

        $agents = User::getALLUserWhere();
        $agents->select("id");
        $builder->whereRaw('`users`.`id` IN (' . $agents->toSql() . ')', $agents->getBindings());


    }

}