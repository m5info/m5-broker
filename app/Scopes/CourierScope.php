<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class CourierScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereIn('role_id', function($_query){
            $_query->select('role_id')->from('roles_permissions')->where('permission_id', function($__query){
                $__query->select('id')->from('permissions')->where('title', 'is_courier');
            });
        });
    }

}