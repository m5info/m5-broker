<?php

namespace App\Scopes\BSO\Acts;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class ToUnderwritingActsScope implements Scope{

    public function apply(Builder $builder, Model $model){
        $builder->where('type_id', 10);
    }

}