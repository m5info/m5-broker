<?php

namespace App\Classes\Socket;

use App\Classes\Socket\Base\BaseSocket;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Ratchet\ConnectionInterface;

class IntegrationSocket extends BaseSocket
{
    protected $clients = [];

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);

        $answer = json_encode($conn);

        echo "New connection! ($answer)\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {

        echo gettype($msg);

        $msg_obj = json_decode($msg);

        echo gettype($msg_obj);
        echo $msg_obj->message;



        /* $msg_obj = new Messages();
         $msg_obj->message = $msg->message;
         $msg_obj->user_id = $msg->id;
         $msg_obj->save();*/


        $numRecv = count($this->clients) - 1;

        foreach ($this->clients as $client){
            if ($from !== $client){
                $client->send(json_encode($msg_obj));
            }
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);

        $conn = json_encode($conn);

        echo "Connection {$conn} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occured: {$e->getMessage()}\n";

        $conn->close();
    }

}