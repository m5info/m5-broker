<?php

namespace App\Classes\Socket;

use App\Classes\Socket\Base\BasePusher;
use ZMQ;
use ZMQContext;

class Pusher extends BasePusher
{
    static function sendDataToServer(array $data)
    {
        $context = new ZMQContext();
        $socket = $context->getSocket(\ZMQ::SOCKET_PUSH, 'my pusher');
        $socket->connect('tcp://'.config('app.pusher_integration_uri_inner').':'.config('app.pusher_integration_port'));
        $data = json_encode($data);
        $socket->send($data);
    }

    public function broadcast($jsonDataToSend)
    {
        $aDataToSend = json_decode($jsonDataToSend, true);
        $subscribedTopics = $this->getSubscribeTopics();

        if (isset($subscribedTopics[$aDataToSend['topic_id']])){
            $topic = $subscribedTopics[$aDataToSend['topic_id']];
            $topic->broadcast($aDataToSend);
        }
    }
}