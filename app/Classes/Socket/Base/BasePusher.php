<?php

namespace App\Classes\Socket\Base;

use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;
use Ratchet\Wamp\WampServerInterface;

class BasePusher implements WampServerInterface
{
    protected $subscribedTopics = [];

    public function getSubscribeTopics()
    {
        return $this->subscribedTopics;
    }

    public function addSubscribeTopic($topic)
    {
        $this->subscribedTopics[$topic->getId()] = $topic;
    }

    function onSubscribe(ConnectionInterface $conn, $topic)
    {
        $this->addSubscribeTopic($topic);
    }

    function onUnSubscribe(ConnectionInterface $conn, $topic)
    {
        // TODO: Implement onUnSubscribe() method.
    }

    function onOpen(ConnectionInterface $conn)
    {
        echo "New Connection ! ({$conn->resourceId} \n (BasePusher)";
    }

    function onClose(ConnectionInterface $conn)
    {
        echo "Connection ! ({$conn->resourceId} \n has disconnected (BasePusher)";
    }

    function onCall(ConnectionInterface $conn, $id, $topic, array $params)
    {
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }

    function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
        $conn->close();
    }

    function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occured: {$e->getMessage()}\n";
        $conn->close();
    }








}