<?php

namespace App\Classes\Export\TagModels\Finance;


use App\Classes\Export\TagModels\BSO\TagBsoItem;
use App\Classes\Export\TagModels\TagModel;
use App\Models\BSO\BsoItem;
use App\Models\Organizations\Organization;
use App\Models\User;

class TagReservation extends TagModel{



    public function apply(){

        $replace_arr = [];

        if($reservation = $this->builder->first()){

            if(isset($reservation['my_org']) && $reservation['my_org'] != 0){ // если выбрали организацию у получателя
                $org = Organization::find($reservation['my_org']);
            }else{
                $org = Organization::where('kpp', '=', $reservation->data['recipient_kpp'])->where('inn', '=', $reservation->data['recipient_inn'])->first();
            }

            $org_bank_account = null;
            if($org){
                $org_bank_account = $org->bank_account->first();
            }


            $replace_arr['id'] = isset($reservation['id']) ? $reservation['id'] : "";
            $replace_arr['created_at'] = isset($reservation['created_at']) ? date('d.m.Y', strtotime($reservation['created_at'])) : "";
            $replace_arr['user_agent'] = isset($reservation['user_id']) ? User::find($reservation['user_id'])->name : "";
            $replace_arr['rs'] = $org_bank_account ? $org_bank_account->account_number : "";
            $replace_arr['bik'] = $org_bank_account ? $org_bank_account->bik : "";
            $replace_arr['kur'] = $org_bank_account ? $org_bank_account->kur : "";
            $replace_arr['bank_title'] = $org_bank_account ? $org_bank_account->bank_title : "";
            $replace_arr['comment'] = isset($reservation->data['comment']) ? $reservation->data['comment'] : "";
            $replace_arr['payer_inn'] = isset($reservation->data['payer_inn']) ? $reservation->data['payer_inn'] : "";
            $replace_arr['payer_title'] = isset($reservation->data['payer_title']) ? $reservation->data['payer_title'] : "";
            $replace_arr['recipient_title'] = isset($reservation->data['recipient_title']) ? $reservation->data['recipient_title'] : "";
            $replace_arr['payer_kpp'] = isset($reservation->data['payer_kpp']) ? $reservation->data['payer_kpp'] : "";
            $replace_arr['payer_name'] = isset($reservation->data['payer_name']) ? $reservation->data['payer_name'] : "";
            $replace_arr['payer_address'] = isset($reservation->data['payer_address']) ? $reservation->data['payer_address'] : "";
            $replace_arr['recipient_inn'] = isset($reservation->data['recipient_inn']) ? $reservation->data['recipient_inn'] : "";
            $replace_arr['recipient_kpp'] = isset($reservation->data['recipient_kpp']) ? $reservation->data['recipient_kpp'] : "";
            $replace_arr['recipient_name'] = isset($reservation->data['recipient_name']) ? $reservation->data['recipient_name'] : "";
            $replace_arr['recipient_address'] = isset($reservation->data['recipient_address']) ? $reservation->data['recipient_address'] : "";
            $replace_arr['total_amount'] = isset($reservation['amount']) ? $reservation['amount'] : "";
            $replace_arr['total_amount_text'] = isset($reservation['amount']) ? num2str($reservation['amount']) : "";

            $bso_items = BsoItem::query()->whereIn('bso_title', array_column($reservation->data['bso'], 'bso_number'));

            $bso_tags  = (new TagBsoItem($bso_items))->apply();

            foreach($bso_items->get() as $key => $bso_item){
                $bso_tags['bso_list'][$key]['supplier_title'] = $bso_item->supplier->title;
            }

            foreach($reservation->data['bso'] as $key => $bso){
                $bso_tags['bso_list'][$key]['bso_amount'] = $bso['bso_sum'];
            }

            $replace_arr = array_merge($replace_arr, $bso_tags);

        }

        return $replace_arr;

    }


    public static function doc(){

        $bso_doc = TagBsoItem::doc();
        $keys = array_keys($bso_doc);
        $bso_doc[$keys[1]]['bso_amount'] = "Сумма БСО";
        $bso_doc[$keys[1]]['supplier_title'] = "Наименование поставщика";

        $doc = [
            'Теги резерва' => [
                'id' => 'Номер счета',
                'created_at' => 'Дата создания',
                'comment' => 'Комментарий',
                'payer_inn' => 'Инн плательщика',
                'user_agent' => 'ФИО агента',
                'payer_title' => 'Название организации плательщика',
                'recipient_title' => 'Название организации получателя',
                'payer_kpp' => 'КПП плательщика',
                'payer_name' => 'ФИО плательщика',
                'payer_address' => 'Адрес плательщика',
                'recipient_inn' => 'Инн получателя',
                'recipient_kpp' => 'КПП получателя',
                'recipient_name' => 'ФИО получателя',
                'recipient_address' => 'Адрес получателя',
                'total_amount' => 'Общая сумма',
                'total_amount_text' => 'Общая сумма прописью',
                'bank_title' => 'Банк',
                'rs' => 'Расчетный счет',
                'bik' => 'БИК',
                'kur' => 'К/Р',
            ]
        ];

        foreach ($doc as $k => $v){
            asort($doc[$k]);
        }

        return array_merge($bso_doc, $doc);

    }


}