<?php
namespace App\Classes\Export\TagModels\BSO\Acts;


use App\Classes\Export\TagModels\Contracts\TagContracts;
use App\Classes\Export\TagModels\Contracts\TagPayments;
use App\Classes\Export\TagModels\TagModel;

class TagToUnderwritingActs extends TagModel {


    public function apply(){

        $act = $this->builder->first();

        $replace_arr = [
            'act_date' => getDateFormatRu($act->time_create),
            'act_number' => $act->act_number,
            'create_user_name' => $act->user_from ? $act->user_from->name : "",
            'current_date' => date('d.m.Y', time()),
        ];

        $payment_tags = (new TagPayments($act->payments()))->apply();

        $replace_arr = array_merge($replace_arr, $payment_tags);

        return $replace_arr;

    }


    public static function doc(){

        $doc = [
            'Данные акта передачи в андеррайтинг' => [
                'act_number' => 'Название акта',
                'act_date' => 'Дата создания',
                'create_user_name' => 'Создал',
                'current_date' => 'Текущая дата',
            ]
        ];

        $contracts_doc = TagPayments::doc();

        foreach ($doc as $k => $v){
            asort($doc[$k]);
        }

        $doc = array_merge($doc, $contracts_doc);

        return $doc;
    }

}