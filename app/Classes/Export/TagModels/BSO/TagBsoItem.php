<?php
namespace App\Classes\Export\TagModels\BSO;


use App\Classes\Export\TagModels\TagModel;
use App\Models\BSO\BsoItem;

class TagBsoItem extends TagModel {


    public function apply(){
        $replace_arr = [
            'bso_list' => [],
            'bso_types_list' => []
        ];

        // БСО и поставщик из акта
        $act_bso = $this->builder->get();
        $supplier_id = $act_bso->first()->supplier->id;

        // остаток БСО поставщика
        $bso_types = BsoItem::getSupplierBsoTypeList($supplier_id)->get();

        /** утраченные и испорченные БСО */
        $replace_arr['lost_bso_count'] = 0;
        $replace_arr['corrupted_bso_count'] = 0;
        $replace_arr['unused_bso_count'] = 0;

        foreach ($act_bso as $key => $bso){

            $row = [];

            $row['number'] = $key+1;
            $row['organization'] = $bso->supplier_org ? $bso->supplier_org->title : "";
            $row['insurance'] = $bso->supplier ? $bso->supplier->title : "";
            $row['bso_type'] = $bso->product ? $bso->product->title : "";
            $row['bso_title'] = $bso->bso_title;
            $row['bso_blank_title'] = $bso->bso_blank_title;
            $row['bso_blank_number'] = $bso->bso_number;
            $row['bso_blank_serie'] = str_replace($bso->bso_number, '', $bso->bso_title);
            $row['bso_blank_type'] = $bso->type ? $bso->type->title : "";
            $row['location'] = $bso->location ? $bso->location->title : "";
            if($bso->contract){
                $row['bso_insurer'] = $bso->contract->insurer ? $bso->contract->insurer->title : "";
            }else{
                $row['bso_insurer'] = "";
            }
            $row['state'] = $bso->state ? $bso->state->title : "";
            $row['bso_comment'] = $bso->bso_comment ? $bso->bso_comment : "";


            /** утраченные и испорченные БСО */
            $row['lost_bso_blank_number'] = "";
            $row['lost_bso_blank_quantity'] = "";
            $row['corrupted_bso_blank_number'] = "";
            $row['corrupted_bso_blank_quantity'] = "";
            $row['unused_bso_blank_number'] = "";
            $row['unused_bso_blank_quantity'] = "";

            if ($bso->state_id == 4) {
                $row['lost_bso_blank_number'] = $bso->bso_number;
                $row['lost_bso_blank_quantity'] = 1;
                $replace_arr['lost_bso_count']++;
            } else if ($bso->state_id == 3) {
                $row['corrupted_bso_blank_number'] = $bso->bso_number;
                $row['corrupted_bso_blank_quantity'] = 1;
                $replace_arr['corrupted_bso_count']++;
            }  else if ($bso->state_id == 0) {
                $row['unused_bso_blank_number'] = $bso->bso_number;
                $row['unused_bso_blank_quantity'] = 1;
                $replace_arr['unused_bso_count']++;
            }


            $replace_arr['bso_list'][] = $row;
        }


        /** остаток на конец отчетного периода - теги:
         * 'bso_type_number' - счетчик
         * 'bso_type_name' - тип бсо
         * 'bso_type_amount' - количество бсо каждого типа
         */
        foreach($bso_types as $key => $bso_type) {
            $row = [];

            $row['bso_type_number'] = $key + 1;
            $row['bso_type_name'] = $bso_type->title;
            $row['bso_type_amount'] = $bso_type->count;

            $replace_arr['bso_types_list'][] = $row;
        }

        if (empty($replace_arr['bso_types_list'])) {
            $row = [];

            $row['bso_type_number'] = "";
            $row['bso_type_name'] = "";
            $row['bso_type_amount'] = "";

            $replace_arr['bso_types_list'][] = $row;
        }

        $replace_arr['bso_type_count'] = count($replace_arr['bso_list']);
        $replace_arr['bso_count'] = count($replace_arr['bso_list']);

        return $replace_arr;
    }


    public static function doc(){

        $doc = [
            'Данные БСО<sup style="font-size: 75%;">(общие)</sup>' => [
                'bso_list' => '<b style="color: #333">Тег списка БСО</b>',
                'bso_types_list' => '<b style="color: #333">Тег списка видов БСО</b>',
                'bso_count' => 'Количество БСО',
                'lost_bso_count' => 'Количество утраченных БСО',
                'corrupted_bso_count' => 'Количество испорченных БСО',
                'unused_bso_count' => 'Количество неиспользованных БСО',
            ],

            'Данные БСО<sup style="font-size: 75%;">(список)</sup>' => [
                'number' => '№ п/п',
                'organization' => 'Организация',
                'insurance' => 'Страховая компания',
                'bso_type' => 'Вид страхования',
                'bso_title' => '№ полиса / квит. / сер.карт с',
                'bso_blank_title' => '№ бланка',
                'bso_blank_serie' => 'Серия бланка',
                'bso_blank_number' => 'Номер бланка (без серии)',
                'bso_blank_type' => 'Тип бланка',
                'location' => 'Событие',
                'bso_insurer' => 'Страхователь (если есть)',
                'bso_comment' => 'Комментарий БСО',
                'state' => 'Статус',
                'lost_bso_blank_number' => 'Номер утраченной БСО (если утрачена)',
                'corrupted_bso_blank_number' => 'Номер испорченной БСО (если испорчена)',
                'unused_bso_blank_number' => 'Номер неиспользованной БСО (если неиспользована)',
                'lost_bso_blank_quantity' => 'Количество утраченных БСО (1, если утрачена)',
                'corrupted_bso_blank_quantity' => 'Количество испорченных БСО (1, если испорчена)',
                'unused_bso_blank_quantity' => 'Номер неиспользованной БСО (если неиспользована)',
            ],

            'Данные видов БСО (остаток на конец отчётного периода)<sup style="font-size: 75%;">(список)</sup>' => [
            'bso_type_number' => '№ п/п',
            'bso_type_name' => 'Вид БСО',
            'bso_type_amount' => 'Количество БСО каждого вида',
            ]
        ];


        foreach($doc as $k => $v){
            asort($doc[$k]);
        }


        return $doc;
    }

}