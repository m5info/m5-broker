<?php
namespace App\Classes\Export\TagModels\Cashbox;


use App\Classes\Export\TagModels\TagModel;
use App\Models\User;
use Illuminate\Support\Carbon;

class TagIncomeExpense extends TagModel {


    public function apply(){

        $replace_arr = [];
        $income_expense = $this->builder->first();

        $replace_arr['document_id'] = $income_expense->id;
        $replace_arr['category_type'] = $income_expense->category ? $income_expense->category->type_ru('type') : "";
        $replace_arr['category_id'] = $income_expense->category ? $income_expense->category->title : "";
        $replace_arr['payment_type'] = $income_expense->payment_type_ru('payment_type');
        $replace_arr['sum'] = $income_expense->sum;
        $user_to = User::where('id', $income_expense->payment_user_id)->first();
        if ($user_to){
            $replace_arr['user_to'] = User::where('id', $income_expense->payment_user_id)->first()->name;
        }else{
            $replace_arr['user_to'] = auth()->user()->name;
        }

        $replace_arr['sum_string'] = num2str($income_expense->sum);
        $replace_arr['date'] = getDateFormatRu($income_expense->date);
        $replace_arr['commission'] = $income_expense->commission;
        $replace_arr['comment'] = $income_expense->comment;
        $replace_arr['current_date'] = Carbon::now()->format('d.m.Y');
        $replace_arr['current_day'] = Carbon::now()->format('d');
        $replace_arr['current_month'] = getRuMonthes(true)[Carbon::now()->format('m')];
        $replace_arr['current_year'] = Carbon::now()->format('Y');
        $replace_arr['cashier'] = auth()->user()->name;
        $replace_arr['organization_title'] = auth()->user()->cashbox->organization ? auth()->user()->cashbox->organization->title_doc : '';
        $replace_arr['general_manager'] = auth()->user()->cashbox && auth()->user()->cashbox->organization ? auth()->user()->cashbox->organization->general_manager : '';
        $replace_arr['general_accountant'] = auth()->user()->cashbox && auth()->user()->cashbox->organization ? auth()->user()->cashbox->organization->general_accountant : '';
        $replace_arr['okud'] = auth()->user()->cashbox && auth()->user()->cashbox->organization ? auth()->user()->cashbox->organization->okud : '';
        $replace_arr['okpo'] = auth()->user()->cashbox && auth()->user()->cashbox->organization ? auth()->user()->cashbox->organization->okpo : '';

        $month = getOneMonth(date_format(new \DateTime($income_expense->date), 'm'), true);
        $kvit_date = date_format(new \DateTime($income_expense->date), 'd').' '.$month.' '.date_format(new \DateTime($income_expense->date), 'Y').' г.';
        $replace_arr['kvit_date'] = $kvit_date;

        $temp_kvit_sum = explode('.',$income_expense->sum);
        if (substr($temp_kvit_sum[1], -1) == '0'){
            $temp_kvit_sum[1] = substr($temp_kvit_sum[1], 0, strlen($temp_kvit_sum[1]) - 1);
        }
        $kvit_sum = $temp_kvit_sum[0].'руб.'.$temp_kvit_sum[1].'коп.'.PHP_EOL.num2str($income_expense->sum).PHP_EOL.'В том числе без налога (НДС)';
        $replace_arr['kvit_sum'] = $kvit_sum;

        $current_date = New \DateTime();

        $day = $current_date->format('d');
        $month_string = getOneMonth($current_date->format('m'), true);
        $year = $current_date->format('Y');

        $replace_arr['data_current_day'] = $day;
        $replace_arr['data_current_string_month'] = $month_string;
        $replace_arr['data_current_year'] = $year;


        return $replace_arr;

    }


    public static function doc(){

        $doc = [
            'Теги расходов/доп. доходов' => [
                'category_type' => 'Тип',
                'category_id' => 'Категория',
                'payment_type' => 'Тип оплаты',
                'sum' => 'Сумма',
                'user_to' => 'Кому выдать',
                'sum_string' => 'Сумма прописью',
                'date' => 'Дата платежа',
                'commission' => 'Комиссия',
                'comment' => 'Комментарий',
                'current_date' => 'Текущая дата',
                'current_day' => 'Текущий день',
                'current_month' => 'Текущий месяц',
                'current_year' => 'Текущий год',
                'cashier' => 'Кассир',
                'organization_title' => 'Название организации',
                'general_manager' => 'Генеральный директор',
                'general_accountant' => 'Главный бухгалтер',
                'okud' => 'ОКУД',
                'okpo' => 'ОКПО',
            ]
        ];

        foreach ($doc as $k => $v){
            asort($doc[$k]);
        }

        return $doc;
    }

}