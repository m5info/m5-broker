<?php

namespace App\Classes\Export\TagModels\Cashbox;

use App\Classes\Export\TagModels\BSO\TagBsoItem;
use App\Classes\Export\TagModels\Contracts\TagPayments;
use App\Classes\Export\TagModels\TagModel;
use App\Models\Cashbox\Cashbox;
use App\Models\User;
use App\Models\Users\UsersBalance;
use function GuzzleHttp\Psr7\str;

class TagUserBalanceTransactions extends TagModel{

    public function apply()
    {
        $replace_arr = [];


        if($user_balance_trans = $this->builder->first()){

            $total_sum = getPriceFormat(str_replace('-', '', $user_balance_trans->total_sum));
            $total_sum_string = num2str(str_replace(' ', '', $total_sum));
            $kvit_sum = str_replace(' ', '', $total_sum);
            $kvit_sum = str_replace(',','руб.', $kvit_sum);
            $kvit_sum_length = strlen($kvit_sum);
            $kvit_sum = substr($kvit_sum, 0, (int)$kvit_sum_length - 1);
            $kvit_sum = $kvit_sum.'коп. '.PHP_EOL.$total_sum_string.PHP_EOL.'В том числе без налога (НДС)';
            $month = getOneMonth(date_format(new \DateTime($user_balance_trans->event_date), 'm'), true);
            $kvit_date = date_format(new \DateTime($user_balance_trans->event_date), 'd').' '.$month.' '.date_format(new \DateTime($user_balance_trans->event_date), 'Y').' г.';

            $organization = $user_balance_trans->user->cashbox->organization;

            $user_balance = UsersBalance::query()->where('id', $user_balance_trans->balance_id)->first();
            $agent_name = User::query()->where('id', $user_balance->user_id)->first()->name;



            if ($user_balance_trans->invoice_id !== 0){
                $comment = "Оплата счета #$user_balance_trans->invoice_id на сумму $total_sum";
            }else{
                $comment = $user_balance_trans->purpose_payment;
            }

            $replace_arr['total_sum'] = $total_sum;
            $replace_arr['event_date'] = getDateFormatRu($user_balance_trans->event_date);
            $replace_arr['cashier_name'] = $user_balance_trans->user->name;
            $replace_arr['total_sum_string'] = $total_sum_string;
            $replace_arr['comment'] = $comment;
            $replace_arr['kvit_sum'] = $kvit_sum;
            $replace_arr['kvit_date'] = $kvit_date;
            $replace_arr['document_id'] = $user_balance_trans->id;
            $replace_arr['okud'] = $organization ? $organization->okud : '';
            $replace_arr['okpo'] = $organization ? $organization->okpo : '';
            $replace_arr['agent_name'] = $agent_name;

            $current_date = New \DateTime();

            $day = $current_date->format('d');
            $month_string = getOneMonth($current_date->format('m'), true);
            $year = $current_date->format('Y');

            $replace_arr['data_current_day'] = $day;
            $replace_arr['data_current_string_month'] = $month_string;
            $replace_arr['data_current_year'] = $year;



        }


        return $replace_arr;
    }


    public static function doc(){

        $doc = [
            'Доступные теги транзакции<!--<sup style="font-size: 75%;">(общие)</sup>-->' => [
                'okud' => 'ОКУД Код',
                'okpo' => 'ОКПО Код',
                'document_id' => 'Номер документа',
                'event_date' => 'Дата составления',
                'total_sum' => 'Сумма руб. коп.',
                'agent_name' => 'Агент',
                'comment' => 'Основание',
                'total_sum_string' => 'Сумма прописью',
                'cashier_name' => 'Кассир',
            ],
            'Доступные теги квитанции<!--<sup style="font-size: 75%;">(общие)</sup>-->' => [
                'document_id' => 'Номер документа',
                'agent_name' => 'Агент',
                'comment' => 'Основание',
                'kvit_sum' => 'Сумма',
                'cashier_name' => 'Кассир',
            ]

        ];

        foreach ($doc as $k => $v){
            asort($doc[$k]);
        }

        return $doc;

    }


}