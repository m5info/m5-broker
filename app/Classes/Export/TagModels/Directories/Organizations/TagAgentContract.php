<?php


namespace App\Classes\Export\TagModels\Directories\Organizations;


use App\Classes\Export\TagModels\TagModel;
use App\Models\Organizations\Organization;
use App\Models\User;
use App\Models\Users\AgentContracts;

class TagAgentContract extends TagModel {

    public function apply(){
        $replace_arr = [];

        $org = $this->builder->get()->first();

        $row = [];

        // данные организации

        $replace_arr['title'] = $org->title;
        $replace_arr['next_act'] = $org->next_act;
        $replace_arr['default_purpose_payment'] = $org->default_purpose_payment;
        $replace_arr['inn'] = $org->inn;
        $replace_arr['limit_year'] = $org->limit_year;
        $replace_arr['spent_limit_year'] = $org->spent_limit_year;
        $replace_arr['is_actual'] = $org->is_actual == 1 ? 'Да' : 'Нет';
        $replace_arr['org_type_id'] = $org->org_type->title;
        $replace_arr['title_doc'] = $org->title_doc;
        $replace_arr['general_manager'] = $org->general_manager;
        $replace_arr['address'] = $org->address;
        $replace_arr['phone'] = $org->phone;
        $replace_arr['org_email'] = $org->email;
        $replace_arr['comment'] = $org->comment;
        $replace_arr['kpp'] = $org->kpp;
        $replace_arr['fact_address'] = $org->fact_address;
        $replace_arr['user_contact_title'] = $org->user_contact_title;
        $replace_arr['nds_type_id'] = $org->nds_type_id;
        $replace_arr['request_type_id'] = $org->request_type_id;
        $replace_arr['taxation_system_type_id'] = $org->taxation_system_type_id ? Organization::TAXATION_SYSTEM_TYPE[$org->taxation_system_type_id] : '';
        $replace_arr['cashbox_api_type_id'] = $org->cashbox_api_type_id ? Organization::CASHBOX_API_TYPE[$org->cashbox_api_type_id] : '';
        $replace_arr['cashbox_api_login'] = $org->cashbox_api_login;
        $replace_arr['cashbox_api_pass'] = $org->cashbox_api_pass;
        $replace_arr['cashbox_api_token'] = $org->cashbox_api_token;
        $replace_arr['is_agent_contract'] = $org->is_agent_contract;
        $replace_arr['general_accountant'] = $org->general_accountant;
        $replace_arr['okud'] = $org->okud;
        $replace_arr['okpo'] = $org->okpo;


        // пользовательские данные
        $user_id = request('user');//auth()->user();
        $user = User::find($user_id);

        $replace_arr['name'] = $user->name;
        $replace_arr['status_user'] = $user->status_user_ru('status_user_id');
        $replace_arr['subject_type'] = $user->subject_type_id == 1 ? "Физическое лицо" : "Юридическое лицо";
        $replace_arr['passport_series'] = $user->info && $user->subject_type_id == 1 ? $user->info->passport_series : "";
        $replace_arr['passport_number'] = $user->info && $user->subject_type_id == 1 ? $user->info->passport_number : "";
        $replace_arr['user_title'] = $user->info && $user->subject_type_id == 2 ? $user->info->title : "";
        $replace_arr['user_ogrn'] = $user->info && $user->subject_type_id == 2 ? $user->info->ogrn : "";
        $replace_arr['user_inn'] = $user->info && $user->subject_type_id == 2 ? $user->info->inn : "";
        $replace_arr['user_bik'] = $user->info && $user->subject_type_id == 2 ? $user->info->bik : "";
        $replace_arr['user_bank'] = $user->info && $user->subject_type_id == 2 ? $user->info->bank : "";
        $replace_arr['user_rs'] = $user->info && $user->subject_type_id == 2 ? $user->info->rs : "";
        $replace_arr['user_email'] = $user->email;
        $replace_arr['work_phone'] = $user->work_phone;
        $replace_arr['mobile_phone'] = $user->mobile_phone;
        $replace_arr['role'] = $user->role ? $user->role->title : "";
        $replace_arr['organization'] = $user->organization ? $user->organization->title_doc : "";
        $replace_arr['department'] = $user->department ? $user->department->title : "";
        $replace_arr['point_sale'] = $user->point_sale ? $user->point_sale->title : "";
        $replace_arr['is_parent'] = $user->is_parent ? "Да" : "Нет";
        $replace_arr['parent'] = $user->parent ? $user->parent->name : "";


        if($org->id == $user->organization_id){
            $replace_arr['agent_contract_title'] = $user->agent_contract_title;
            $replace_arr['agent_contract_begin_date'] = date('d.m.Y', strtotime($user->agent_contract_begin_date));
            $replace_arr['agent_contract_end_date'] = date('d.m.Y', strtotime($user->agent_contract_end_date));
        }else{
            $agent_contract = AgentContracts::where('user_id', $user->id)->where('org_id', $org->id)->get()->last();
            if($agent_contract){
                $replace_arr['agent_contract_title'] = $agent_contract->agent_contract_title;
                $replace_arr['agent_contract_begin_date'] = date('d.m.Y', strtotime($agent_contract->begin_date));
                $replace_arr['agent_contract_end_date'] = date('d.m.Y', strtotime($agent_contract->end_date));
            }
        }



        return $replace_arr;
    }


    public static function doc(){

        $doc = [
            'Данные Организации<sup style="font-size: 75%;">(общие)</sup>' => [
                'title' => 'Название',
                'next_act' => 'Акт выполненых работ',
                'default_purpose_payment' => 'Назначение платежа по умолчанию',
                'inn' => 'ИНН',
                'limit_year' => 'Лимит в год',
                'spent_limit_year' => 'Израсходавано',
                'is_actual' => 'Актуально',
                'org_type_id' => 'Тип организации',
                'title_doc' => 'Название для документов',
                'general_manager' => 'Генеральный директор',
                'address' => 'Адрес',
                'phone' => 'Телефон',
                'org_email' => 'Email',
                'comment' => 'Примечание',
                'kpp' => 'КПП',
                'fact_address' => 'Фактический адрес',
                'user_contact_title' => 'Контактное лицо',
                'nds_type_id' => 'Тип НДС',
                'taxation_system_type_id' => 'Тип налогообложения',
                'cashbox_api_type_id' => 'Тип кассы api',
                'cashbox_api_login' => 'Логин api',
                'cashbox_api_pass' => 'Пароль api',
                'cashbox_api_token' => 'Token api',
                'is_agent_contract' => 'Нужен ли агентские договор',
                'general_accountant' => 'Главный бухгалтер',
                'okud' => 'ОКУД',
                'okpo' => 'ОКПО',
            ],

            'Данные Пользователя<sup style="font-size: 75%;">(список)</sup>' => [
                'name' => 'ФИО',
                'status_user' => 'Статус',
                'subject_type' => 'Тип субъекта',
                'passport_series' => '(Физ)Серия паспорта',
                'passport_number' => '(Физ)Номер паспорта',
                'user_title' => '(Юр)Название',
                'user_ogrn' => '(Юр)ОГРН',
                'user_inn' => '(Юр)ИНН',
                'user_bik' => '(Юр)БИК',
                'user_bank' => '(Юр)Банк',
                'user_rs' => '(Юр)РС',
                'email' => 'Email',
                'work_phone' => 'Рабочий телефон',
                'mobile_phone' => 'Мобильный телефон',
                'role' => 'Роль',
                'organization' => 'Организация',
                'department' => 'Подразделение',
                'point_sale' => 'Точка продаж',
                'is_parent' => 'Является руководителем',
                'parent' => 'Руководитель',
                'agent_contract_title' => 'Номер агентского договора',
                'agent_contract_begin_date' => 'Агентский договор с',
                'agent_contract_end_date' => 'Агентский договор по',
            ]
        ];



        foreach($doc as $k => $v){
            asort($doc[$k]);
        }


        return $doc;
    }
}