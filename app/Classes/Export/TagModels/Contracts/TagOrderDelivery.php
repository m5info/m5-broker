<?php
namespace App\Classes\Export\TagModels\Contracts;

use App\Classes\Export\TagModels\TagModel;
use App\Models\Contracts\Payments;
use App\Models\User;


class TagOrderDelivery extends TagModel {


    public function apply(){

        $replace_arr = [
            'cash_amount_real_sum' => 0,
            'amount_sum' => 0,
            'has_osago' => '',
            'has_kasko' => '',
            'has_dsago' => '',
        ];



        $order = $this->builder->first();


        $replace_arr['order_number'] = $order->id;
        $replace_arr['order_manager'] = $order->manager->name;
        $replace_arr['order_parent'] = $order->manager->perent ? $order->manager->perent->name : '';
        $replace_arr['order_parent_phone'] = $order->manager->perent ? $order->manager->perent->phone : '';
        $replace_arr['order_metro'] = $order->delivery_metro;
        $replace_arr['order_address'] = $order->address;
        $replace_arr['delivery_comment'] = $order->delivery_comment;
        $replace_arr['order_deliver'] = $order->courier ? $order->courier->name : '';
        $replace_arr['order_delivery_phone'] = $order->courier ? $order->courier->phone : '';
        $replace_arr['insurer_name'] = $order->main_contract->insurer ? $order->main_contract->insurer->title : '';
        $replace_arr['delivery_date'] = setDateTimeFormatRu($order->delivery_date, 1);
        $replace_arr['delivery_time'] = $order->delivery_time;

        $replace_arr['discount'] = 0;
        $replace_arr['discount_sum'] = 0;
        $phones = [];

        foreach ($order->contracts as $k => $contract){

//            $cash_amount_real = 0;
//            foreach ($contract->payments()->get() as $payment){
//                $cash_amount_real += $this->getCashAmountReal($payment);
//            }


            if($contract->bso){
                if($contract->bso->product_id == 1){
                    $replace_arr['has_osago'] = 'X';
                }elseif($contract->bso->product_id == 2){
                    $replace_arr['has_kasko'] = 'X';
                }
            }


            $row = [];
            $row['insurer_fio'] = $contract->insurer ? $contract->insurer->title : "";
            if($contract->insurer){
                $phones[] = $contract->insurer->phone;
            }
            $row['sk_title'] = $contract->bso && $contract->bso->insurance ? $contract->bso->insurance->title : "";
            $row['receipt_title'] = $contract->payments->first() ? $contract->payments->first()->bso_receipt : "";
            $row['bso_title'] = $contract->bso ? $contract->bso->bso_title : "";
            $row['contract_date'] = getDateFormatRu($contract->sign_date);
//            $row['cash_amount_real'] = getPriceFormat($cash_amount_real);
            $row['amount'] = getPriceFormat($contract->payment_total);
            $row['number'] = $k+1;

            $payment  = $contract->temp_payments->first();

            if($payment){
                $replace_arr['discount'] += $payment->informal_discount;
                $replace_arr['discount_sum'] += $payment->informal_discount_total;
                $replace_arr['cash_amount_real_sum'] += $payment->payment_total-$payment->informal_discount_total;
            }



            $replace_arr['amount_sum'] += $contract->payment_total;

            $replace_arr['contracts_list'][] = $row;
        }

        $replace_arr['phones'] = implode(' ', $phones);
        $replace_arr['cash_amount_real_sum'] = getPriceFormat($replace_arr['cash_amount_real_sum']);
        $replace_arr['amount_sum'] = getPriceFormat($replace_arr['amount_sum']);

        return $replace_arr;
    }



    public static function doc(){

        $doc = [

            'Доступные теги договоров<sup style="font-size: 75%;">(общие)</sup>' => [
                'contracts_list' => '<b style="color: #333">Тег списка договоров</b>',
                'cash_amount_real_sum' => 'Сумма к сдаче в кассу итоговая',
                'amount_sum' => 'Сумма премии итоговая',
            ],

            'Данные договора<sup style="font-size: 75%; color: #000; font-weight: bold">(список)</sup>' => [
                'contract_date' => 'Дата заключения договора',
                'cash_amount_real' => 'Сумма к сдаче в кассу',
                'receipt_title' => 'Номер квитанции',
                'insurer_fio' => 'ФИО Страхователя',
                'number' => 'Порядковый номер',
                'bso_title' => 'Номер полиса',
                'sk_title' => 'Название СК',
                'amount' => 'Сумма премии',
                'discount' => 'Скидка',
            ],


            'Данные заявки' => [
                'order_number' => 'Заявка №',
                'order_manager' => 'Менеджер',
                'order_parent' => 'Нач. отдела продаж',
                'order_parent_phone' => 'Тел. нач. отдела продаж',
                'order_address' => 'Адрес доставки',
                'insurer_name' => 'Имя страхователя',
                'delivery_comment' => 'Комментарий к доставке',
                'order_deliver' => 'Курьер',
                'order_delivery_phone' => 'Курьер',
                'phones' => 'Список телефонов',
                'delivery_date' => 'Дата доставки',
                'delivery_time' => 'Время доставки',
            ],

        ];

        foreach ($doc as $k => $v){
            asort($doc[$k]);
        }
        return  $doc;
    }



    private function getCashAmountReal(Payments $payment){

        $cash_amount_real = $payment->getPaymentAgentSum();

        if($tag_invoice = $this->process()->get('TagInvoice')){
            if($invoice = $tag_invoice->builder->first()){
                if($invoice->type == "cashless"){
                    $cash_amount_real = 0;
                }
            }
        }

        return $cash_amount_real;
    }


}
