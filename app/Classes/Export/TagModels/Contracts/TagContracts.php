<?php
namespace App\Classes\Export\TagModels\Contracts;

use App\Classes\Export\TagModels\TagModel;
use App\Models\Contracts\Payments;


class TagContracts extends TagModel {


    public function apply(){

        $contracts = $this->builder->get();

        $first_contract = $contracts->first();
        $replace_arr = [
            'current_date' => date('d.m.Y'),
            'cash_amount_real_sum' => 0,
            'amount_sum' => 0,
            'underwriter_name' => $first_contract->check_user ? $first_contract->check_user->name : "",
            'agent_name' => $first_contract->agent ? $first_contract->agent->name : "",
            'current_user' => auth()->user() ? auth()->user()->name : ""
        ];

        foreach ($contracts as $k => $contract){

            $cash_amount_real = 0;
            foreach ($contract->payments()->get() as $payment){
                $cash_amount_real += $this->getCashAmountReal($payment);
            }

            $row = [];
            $row['insurer_fio'] = $contract->insurer ? $contract->insurer->title : "";
            $row['sk_title'] = $contract->bso && $contract->bso->insurance ? $contract->bso->insurance->title : "";
            $row['receipt_title'] = $contract->payments->first() ? $contract->payments->first()->bso_receipt : "";
            $row['bso_title'] = $contract->bso ? $contract->bso->bso_title : "";
            $row['bso_blank_title'] = $contract->bso ? $contract->bso->bso_blank_title : "";
            $row['contract_date'] = getDateFormatRu($contract->sign_date);
            $row['cash_amount_real'] = getPriceFormat($cash_amount_real);
            $row['amount'] = getPriceFormat($contract->payment_total);
            $row['number'] = $k+1;
            $row['discount'] = $contract->payments->first() ? $contract->payments->first()->informal_discount : 0;
            $row['product_title'] = $contract->product ? $contract->product->title : "";

            $row['errors'] = '';
            foreach ($contract->errors as $error){
                if (empty($row['errors'])){
                    $row['errors'] = $error->message.PHP_EOL;
                }else{
                    $row['errors'] = $row['errors'].$error->message.PHP_EOL;
                }
            }


            $replace_arr['cash_amount_real_sum'] += $cash_amount_real;
            $replace_arr['amount_sum'] += $contract->payment_total;

            $replace_arr['contracts_list'][] = $row;
        }

        $replace_arr['cash_amount_real_sum'] = getPriceFormat($replace_arr['cash_amount_real_sum']);
        $replace_arr['amount_sum'] = getPriceFormat($replace_arr['amount_sum']);

        return $replace_arr;
    }



    public static function doc(){

        $doc = [

            'Доступные теги договоров<sup style="font-size: 75%;">(общие)</sup>' => [
                'contracts_list' => '<b style="color: #333">Тег списка договоров</b>',
                'cash_amount_real_sum' => 'Сумма к сдаче в кассу итоговая',
                'amount_sum' => 'Сумма премии итоговая',
                'current_date' => 'Текущая дата',
                'underwriter_name' => 'Имя андеррайтера',
                'agent_name' => 'Имя андеррайтера',
                'current_user' => 'Имя текущего пользователя',
            ],

            'Данные договора<sup style="font-size: 75%; color: #000; font-weight: bold">(список)</sup>' => [
                'contract_date' => 'Дата заключения договора',
                'cash_amount_real' => 'Сумма к сдаче в кассу',
                'receipt_title' => 'Номер квитанции',
                'insurer_fio' => 'ФИО Страхователя',
                'number' => 'Порядковый номер',
                'bso_title' => 'Номер полиса',
                'sk_title' => 'Название СК',
                'amount' => 'Сумма премии',
                'discount' => 'Скидка',
                'bso_blank_title' => 'Номер бланка БСО',
                'product_title' => 'Название продукта',
                'errors' => 'Ошибки, замечания'
            ],

        ];

        foreach ($doc as $k => $v){
            asort($doc[$k]);
        }
        return  $doc;
    }



    private function getCashAmountReal(Payments $payment){

        $cash_amount_real = $payment->getPaymentAgentSum();

        if($tag_invoice = $this->process()->get('TagInvoice')){
            if($invoice = $tag_invoice->builder->first()){
                if($invoice->type == "cashless"){
                    $cash_amount_real = 0;
                }
            }
        }

        return $cash_amount_real;
    }


}
