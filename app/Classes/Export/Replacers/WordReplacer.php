<?php

namespace App\Classes\Export\Replacers;

use App\Models\File;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;
use Storage;

class WordReplacer {

    public static function replace($file_path, $replace_arr) {

        $templateProcessor = new TemplateProcessor($file_path);
        foreach ($replace_arr as $key => $value) {
            if(!is_array($value)){
                $templateProcessor->setValue($key, $value);
            }
        }
        $tempname = md5(microtime()) . '.docx';
        $temppath = '/app/public/' . $tempname;
        $templateProcessor->saveAs(storage_path() . $temppath);
        $reader = IOFactory::createReader();
        $word = $reader->load(storage_path() . $temppath);
        Storage::disk('public')->delete($tempname);
        return $word;
    }

    public static function output(PhpWord $word, $name = false) {
        $date = date('Ymd_His', time());
        $name = $name ? $name : "{$date}.docx";
        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="' . $name . '"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        $xmlWriter = IOFactory::createWriter($word, 'Word2007');
        $xmlWriter->save("php://output");
    }

}
