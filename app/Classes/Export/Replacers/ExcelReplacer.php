<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 12.12.18
 * Time: 12:14
 */

namespace App\Classes\Export\Replacers;

use App\Models\File;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Facades\Excel;


class ExcelReplacer
{
    public static $index_row = 0;
    public static $protected_array = [];

    public static function replace($file_path, $replace_arr, $need_correct, $need_protect){

        $replace_arr = self::replace_arr_wrap($replace_arr);
        $flat = self::get_flat($replace_arr);
        $lists = self::get_lists($replace_arr);

        $excel = Excel::load($file_path, function ($reader) use ($flat, $lists, $need_correct, $need_protect) {
            foreach ($reader->excel->getAllSheets() as $list) {
                $reader->sheet($list->getTitle(), function ($sheet) use ($flat, $lists, $need_correct, $need_protect) {

                    /*$sheet_flat_cells = self::get_array_cells($sheet, true);
                    $sheet_cells = self::get_array_cells($sheet);*/

                    foreach ($lists as $list_key => $vl){
                        if(self::$index_row){
                            self::$index_row = 0;
                        }

                        $sheet_flat_cells = self::get_array_cells($sheet, true);
                        $sheet_cells = self::get_array_cells($sheet);

                        foreach($sheet_flat_cells as $cell_coord => $cell_val){
                            if (stristr($cell_val, $list_key)) {

                                $coord = self::get_row_and_col($cell_coord);
                                $sheet->setHeight($coord['row'], 30);
                                $list_row = $sheet_cells[$coord['row']];
                                $list_row[$coord['col']] = null;

                                $new_rows = [];
                                if (is_array($lists[$cell_val]) && count($lists[$cell_val]) > 0) {
                                    foreach ($lists[$cell_val] as $row_number => $row_replace_arr) {
                                        $new_row = $list_row;
                                        foreach ($new_row as $col => $val) {
                                            $val = str_replace(array_keys($row_replace_arr), $row_replace_arr, $val);
                                            if(stringIsFloatFormar($val)){
                                                $val = titleFloatFormat(getFloatFormat($val), 1);
                                            }
                                            $new_row[$col] = $val;
                                        }
                                        $new_rows[] = $new_row;
                                    }
                                }

                                //dd($new_rows);

                                if (is_array($new_rows) && count($new_rows) > 0) {

                                    if(self::$index_row){
                                        $coord['row'] = $coord['row'] + count($new_rows) - 1;
                                    }
                                    self::$index_row += count($new_rows);

                                    foreach ($new_rows as $k => $row) {

                                        $row_num = $coord['row'] + $k;

                                        if($k > 0){
                                            $sheet->prependRow($row_num, function ($row) {});
                                        }

                                        // если нужна корретировка, то меняем высоту для строк
                                        if(isset($need_correct) && $need_correct == true){

                                            $max_val = 0;
                                            foreach ($row as $col => $val) {
                                                if ($need_protect){ // если нужна защита ячеек
                                                    self::$protected_array[] = "{$col}{$row_num}";
                                                }

                                                $sheet->setCellValue("{$col}{$row_num}", $val);
                                                // максимальная длина строки
                                                if(mb_strlen($val) > $max_val){
                                                    $max_val = mb_strlen($val);
                                                }

                                            }

                                            $sheet->setHeight([
                                                $row_num => $max_val + 10
                                            ]);

                                        }else{

                                            foreach ($row as $col => $val) {
                                                if ($need_protect){ // если нужна защита ячеек
                                                    self::$protected_array[] = "{$col}{$row_num}";
                                                }

                                                $sheet->setCellValue("{$col}{$row_num}", $val);
                                            }

                                        }

                                    }
                                }
                            }
                        }
                    }

                    foreach($sheet->getCellCollection() as $cell_coord){
                        $type = $sheet->getCellCacheController()->getCacheData($cell_coord)->getDataType();

                        if($type == 's' || $type == 'inlineStr'){
                            $old_val = $sheet->getCellCacheController()->getCacheData($cell_coord)->getValue();

                            if(gettype($old_val) == 'object'){
                                foreach($old_val->getRichTextElements() as $part){
                                    $part->setText(str_replace(array_keys($flat), $flat, $part->getText()));
                                }
                                $new_val = $old_val;
                            }else{
                                $new_val = str_replace(array_keys($flat), $flat, $old_val);
                            }
                            $sheet->setCellValue($cell_coord, $new_val);
                        }
                    }

                    self::protect($sheet, $need_protect);

                });
            }
        });

        return $excel;
    }


    private static function protect($sheet, $need_protect){

        if ($need_protect && count(self::$protected_array)){ // если нужна защита ячеек

            $sheet->protect($need_protect);

            // TODO: можно сделать чтоб заблокированны были только те, где переменные
//            $final_cells_array = array_keys(self::get_array_cells($sheet, true));
//            $unprotected_keys = array_diff($final_cells_array, self::$protected_array);
//            foreach ($unprotected_keys as $unprotected_elem){
//                $sheet->getStyle($unprotected_elem)->getProtection()->setLocked(\PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
//            }
        }

        return $sheet;
    }

    private static function get_flat($replace_arr){
        if(is_array($replace_arr) && count($replace_arr)>0){
            foreach ($replace_arr as $key => $val){
                if(is_array($val)){
                    unset($replace_arr[$key]);
                }
            }
        }
        return $replace_arr;
    }

    private static function get_lists($replace_arr){
        if(is_array($replace_arr) && count($replace_arr)>0){
            foreach ($replace_arr as $key => $val){
                if(!is_array($val)){
                    unset($replace_arr[$key]);
                }
            }
        }
        return $replace_arr;

    }


    private static function get_array_cells(LaravelExcelWorksheet $sheet, $one_level = false)
    {
        $sheet_cells = [];

        foreach ($sheet->getCellCollection() as $cell_coord) {

            $coord = self::get_row_and_col($cell_coord);
            if($one_level){
                $sheet_cells["{$coord['col']}{$coord['row']}"] = $sheet->getCellCacheController()->getCacheData($cell_coord)->getValue();
            }else{
                $sheet_cells[$coord['row']][$coord['col']] = $sheet->getCellCacheController()->getCacheData($cell_coord)->getValue();
            }

        }

        return $sheet_cells;
    }

    private static function get_row_and_col($cell_coord)
    {
        preg_match('/[0-9]{1,3}/', $cell_coord, $row_match);
        preg_match('/[A-Z]{1,2}/', $cell_coord, $col_match);

        $row = $row_match[0];
        $col = $col_match[0];

        return [
            'row' => $row,
            'col' => $col,
        ];
    }


    private static function replace_arr_wrap($replace_arr){
        $old_replace_arr = $replace_arr;
        $replace_arr = [];
        if(is_array($old_replace_arr) && count($old_replace_arr)>0){
            foreach($old_replace_arr as $k => $v){
                if(is_array($v) && count($v)>0){
                    $_arr = [];
                    foreach ($v as $_k => $_v){
                        if(is_array($_v) && count($_v)>0){
                            $_v_new = [];
                            foreach ($_v as $__k => $__v){
                                $_v_new['${'.$__k.'}'] = $__v;
                            }
                            $_arr[] = $_v_new;
                        }else{
                            $_arr[] = $_v;
                        }
                    }
                    $replace_arr['${'.$k.'}'] = $_arr;
                }else{
                    $replace_arr['${'.$k.'}'] = $v;
                }
            }
        }

        return $replace_arr;
    }


}