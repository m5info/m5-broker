<?php


namespace App\Classes\Common;

/**
 * Class CUDLogsColumns
 * Для понятных названий колонок в логах
 */
class CUDLogsColumns{

    /**
     * Для колонок contracts
     */
    public static function contractsColumns($column)
    {
        $columns = [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'statys_id' => 'Статус договора',
            'user_id' => 'Пользователь',
            'bso_id' => 'ID БСО',
            'bso_title' => 'Номер БСО',
            'insurance_companies_id' => 'СК',
            'bso_supplier_id' => 'Поставщик БСО',
            'product_id' => 'Продукт',
            'agent_id' => 'Агент',
            'is_personal_sales' => 'Личная продажа',
            'manager_id' => 'Менеджер',
            'referencer_id' => 'Рекомендодатель',
            'parent_agent_id' => 'Руководитель',
            'sales_condition' => 'Условие продажи',
            'sign_date' => 'Дата подписания',
            'begin_date' => 'Дата начала',
            'end_date' => 'Дата окончания',
            'check_date' => 'Дата акцепта',
            'underfiller_date' => 'Дата дозабивки',
            'insurer_id' => 'Страхователь',
            'object_insurer_id' => 'Объект страхования',
            'order_title' => 'Заявка',
            'order_id' => 'ID заявки',
            'order_form_id' => 'Сформированная заявка',
            'payment_total' => 'Премия по договору',
            'financial_policy_manually_set' => 'ФП вручную',
            'financial_policy_id' => 'ФП',
            'financial_policy_kv_bordereau' => 'Бордеро %',
            'financial_policy_kv_dvoy' => 'ДВОУ %',
            'financial_policy_kv_agent' => 'Агент %',
            'financial_policy_kv_parent' => 'Руководитель %',
            'installment_algorithms_id' => 'Алгоритм рассрочки',
            'error_accept' => 'Комментарий',
            'check_user_id' => 'Андер',
            'kind_acceptance' => 'Тип акцепта',
            'is_online' => 'Договор онлайн',
            'payment_next_date' => 'Следующая дата платежа',
            'payment_next_sum' => 'Следующая сумма платежа',
            'type_id' => 'Тип',
            'insurance_amount' => 'Страховая сумма',
            'to_cashbox_date' => 'Дата в кассу',
            'bank_id' => 'Банк',
            'underfiller_check' => 'Завершенная дозабивка',
            'owner_id' => 'Владелец объекта страхования',
            'drivers_type_id' => 'Мультидрайв',
            'selected_calculation_id' => 'Рассчет',
            'underfiller_id' => 'Дозабивщик',
            'is_epolicy' => 'Е-ПОЛИС',
            'send_email' => 'Почта',
            'program_id' => 'Программа',
            'is_prolongation' => 'Пролонгация',
            'beneficiar_id' => 'Бенефициар',
            'status_order_id' => 'Статус заявки',
            'inspection_id' => 'Осмотр',
            'prev_policy_number' => 'Предыдущий номер полиса',
        ];

        if(array_key_exists($column, $columns)){
            return $columns[$column];
        }

        return $column;
    }

    /**
     * Для колонок bso_items
     */
    public static function bso_itemsColumns($column)
    {
        $columns = [
            'id' => 'ID',
            'bso_supplier_id' => 'Поставщик БСО',
            'insurance_companies_id' => 'СК',
            'point_sale_id' => 'Точка продаж',
            'org_id' => 'Организация',
            'bso_class_id' => 'БСО/Квитанция',
            'product_id' => 'Продукт',
            'type_bso_id' => 'Тип БСО',
            'state_id' => 'Статус',
            'location_id' => 'Событие',
            'user_id' => 'Пользователь',
            'user_org_id' => 'Организация пользователя',
            'time_create' => 'Дата создания',
            'time_target' => 'Дата создания',
            'last_operation_time' => 'Дата последней операции',
            'transfer_to_agent_time' => 'Дата передачи агенту',
            'transfer_to_org_time' => 'Дата передачи в организацию',
            'transfer_to_sk_time' => 'Дата передачи в СК',
            'bso_serie_id' => 'Серия БСО',
            'bso_number' => 'Номер БСО',
            'bso_dop_serie_id' => 'Доп. серия БСО',
            'bso_title' => 'БСО',
            'bso_blank_serie_id' => 'Серия бланка БСО',
            'bso_blank_number' => 'Номер бланка БСО',
            'bso_blank_dop_serie_id' => 'Серия бланка БСО',
            'bso_blank_title' => 'Бланк БСО',
            'bso_comment' => 'Комментарий',
            'acts_reserve_or_realized_id' => 'Резервный/реализованный акт',
            'acts_implemented_id' => 'Реализованный акт',
            'acts_sk_id' => 'Акт в СК',
            'act_add_number' => 'Акт доп. номера',
            'bso_act_id' => 'Акт БСО',
            'transfer_id' => 'Передача',
            'last_transfer_id' => 'Последняя передача',
            'bso_manager_id' => 'Менеджер',
            'is_reserved' => 'Зарезервирован',
            'bso_cart_id' => 'ID корзины БСО',
            'agent_id' => 'Агент',
            'contract_id' => 'ID договора',
            'realized_act_id' => 'Акт реализованный',
            'file_id' => 'Файл',
            'acts_to_underwriting_id' => 'Акт в андеррайтинг',
            'in_basket' => 'В корзине',
        ];

        if(array_key_exists($column, $columns)){
            return $columns[$column];
        }

        return $column;
    }

    /**
     * Для колонок payments
     */
    public static function paymentsColumns($column)
    {
        $columns = [
            'id' => 'ID',
            'statys_id' => 'Статус',
            'type_id' => 'type_id',
            'is_deleted' => 'Удален',
            'invoice_id' => 'Счет',
            'bso_id' => 'БСО',
            'contract_id' => 'ID договора',
            'payment_number' => 'Номер платежа',
            'payment_data' => 'Дата оплаты',
            'payment_type' => 'Тип оплаты',
            'payment_flow' => 'Поток оплаты',
            'payment_total' => 'Сумма оплаты',
            'official_discount' => 'Оф. скидка %',
            'official_discount_total' => 'Оф. скидка',
            'informal_discount' => 'Неоф. скидка %',
            'informal_discount_total' => 'Неоф. скидка',
            'bank_kv' => 'Банк КВ %',
            'bank_kv_total' => 'Банк КВ',
            'financial_policy_id' => 'ФП',
            'financial_policy_manually_set' => 'ФП вручную',
            'financial_policy_kv_bordereau' => 'ФП бордеро %',
            'financial_policy_kv_bordereau_total' => 'ФП бордеро',
            'financial_policy_kv_dvoy' => 'ФП ДВОУ %',
            'financial_policy_kv_dvoy_total' => 'ФП ДВОУ',
            'financial_policy_kv_agent' => 'КВ агента %',
            'financial_policy_kv_agent_total' => 'КВ агента',
            'financial_policy_kv_parent' => 'КВ руководителя %',
            'financial_policy_kv_parent_total' => 'КВ руководителя',
            'referencer_kv' => 'Доля рекомендодателя %',
            'referencer_kv_real' => 'КВ рекомендодателя %',
            'referencer_kv_total' => 'КВ рекомендодателя',
            'transaction_referencer_id' => 'ID выплаты КВ реком. на баланс',
            'bso_not_receipt' => 'Без квитанции',
            'bso_receipt' => 'Квитанция',
            'bso_receipt_id' => 'ID квитанции',
            'agent_id' => 'Агент',
            'manager_id' => 'Менеджер',
            'parent_agent_id' => 'Руководитель',
            'referencer_id' => 'Рекомендодатель',
            'realized_act_id' => 'Акт. реализованный',
            'org_id' => 'Организация',
            'invoice_payment_total' => 'Сумма счета',
            'invoice_payment_date' => 'Дата счета',
            'comments' => 'Комментарий',
            'point_sale_id' => 'Точка продаж',
            'set_balance' => 'Зачислить на баланс',
            'user_id' => 'Пользователь',
            'order_id' => 'ID заявки',
            'order_title' => 'Заявка',
            'reports_order_id' => 'Отчет бордеро',
            'reports_dvou_id' => 'Отчет ДВОУ',
            'marker_color' => 'Цвет маркера',
            'marker_text' => 'Текст маркера',
            'acts_sk_id' => 'Акты в СК',
            'accept_user_id' => 'Андеррайтер',
            'accept_date' => 'Дата акцепта',
            'atol_check_id' => 'Чек онлайн',
            'pay_method_id' => 'Метод оплаты',
            'acquire_total' => 'Эквайринг',
            'acquire_percent' => 'Эквайринг %',
            'financial_policy_marjing' => 'ФП маржа %',
            'financial_policy_marjing_total' => 'ФП маржа',
            'in_basket' => 'В корзине',
            'check_date' => 'Дата акцепта',
            'check_user_id' => 'Андер',
        ];

        if(array_key_exists($column, $columns)){
            return $columns[$column];
        }

        return $column;
    }

    /**
     * Для колонок invoices
     */
    public static function invoicesColumns($column)
    {
        $columns = [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'status_id' => 'Статус',
            'type' => 'Тип',
            'create_type' => 'Как создан',
            'org_id' => 'Организация',
            'agent_id' => 'Агент',
            'type_invoice_payment_id' => 'Тип платежей',
            'invoice_payment_balance_id' => 'Баланс',
            'invoice_payment_total' => 'Сумма',
            'invoice_payment_com' => 'Комментарий',
            'invoice_payment_user_id' => 'Пользователь',
            'invoice_payment_date' => 'Дата счета',
            'file_id' => 'Файл',
        ];

        if(array_key_exists($column, $columns)){
            return $columns[$column];
        }

        return $column;
    }

    public static function subjectsColumns($column)
    {
        $columns = [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'type' => 'Тип страхователя',
            'title' => 'ФИО',
            'doc_serie' => 'Серия документа',
            'doc_number' => 'Номер документа',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'email' => 'Email',
            'phone' => 'Телефон',
            'add_phone' => 'Дополнительный телефон',
            'delivery_address' => 'Адрес доставки',
            'underground' => 'Метро','subject_id' => 'Гл. ID',
            'fio' => 'ФИО',
            'first_name' => 'Имя',
            'second_name' => 'Отчество',
            'last_name' => 'Фамилия',
            'sex' => 'Пол',
            'birthdate' => 'День рождения',
            'address_born' => 'Место рождения',
            'address_born_kladr' => 'Место рождения КЛАДР',
            'address_register' => 'Адрес регистрации',
            'address_register_kladr' => 'Адрес регистрации КЛАДР',
            'address_register_okato' => 'Адрес регистрации ОКАТО',
            'address_register_zip' => 'Адрес регистрации индекс',
            'address_fact' => 'Фактический адрес',
            'address_fact_kladr' => 'Фактический адрес КЛАДР',
            'address_fact_okato' => 'Фактический адрес ОКАТО',
            'address_fact_zip' => 'Фактический адрес индекс',
            'doc_type' => 'Тип документа',
            'doc_date' => 'Дата выдачи',
            'doc_office' => 'Код подразделения',
            'doc_info' => 'Кем выдан',
            'address_register_region' => 'Регион регистрации',
            'address_register_city' => 'Город регистрации',
            'address_register_city_kladr_id' => 'КЛАДР регистрации',
            'address_register_street' => 'Улица регистрации',
            'address_register_house' => 'Дом регистрации',
            'address_register_block' => 'Корпус регистрации',
            'address_register_flat' => 'Квартира  регистрации',
            'address_fact_region' => 'Регион факт',
            'address_fact_city' => 'Город факт',
            'address_fact_city_kladr_id' => 'КЛАДР факт',
            'address_fact_street' => 'Улица факт',
            'address_fact_house' => 'Дом факт',
            'address_fact_block' => 'Корпус факт',
            'address_fact_flat' => 'Квартира факт',
            'address_register_settlement' => 'Посёлок регистрации',
            'address_fact_settlement' => 'Посёлок факт',
            'address_register_settlement_kladr_id' => 'КЛАДР посёлок регистрации',
            'address_fact_settlement_kladr_id' => 'КЛАДР посёлок факт',
            'class_kmb' => 'КБМ',
            'count_insurance_cases' => 'Кол-во страховых случаев',
            'license_serie' => 'Серия В/У',
            'license_number' => 'Номер В/У',
            'license_date' => 'Дата выдачи В/У',
            'exp_date' => 'Стаж',
            'address_register_city_fias_id' => 'ФИАС регистрации',
            'address_fact_city_fias_id' => 'ФИАС факт',
            'address_fact_region_full' => 'Область факт',
            'address_register_region_full' => 'Область регистрации','position' => 'Позиция',
            'bik' => 'БИК',
            'general_manager' => 'Ответственный',
            'ogrn' => 'ОГРН',
        ];

        if(array_key_exists($column, $columns)){
            return $columns[$column];
        }

        return $column;
    }

    public static function __callStatic($name, $arguments){
        return $arguments[0];
    }
}