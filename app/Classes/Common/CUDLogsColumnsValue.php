<?php


namespace App\Classes\Common;

use App\Models\Acts\ReportAct;
use App\Models\BSO\BsoActs;
use App\Models\BSO\BsoActsItems;
use App\Models\BSO\BsoItem;
use App\Models\BSO\BsoLocations;
use App\Models\BSO\BsoState;
use App\Models\BSO\BsoType;
use App\Models\Contracts\Contracts;
use App\Models\Contracts\Payments;
use App\Models\Contracts\Subjects;
use App\Models\Directories\BsoDopSerie;
use App\Models\Directories\BsoSerie;
use App\Models\Directories\BsoSuppliers;
use App\Models\Directories\FinancialPolicy;
use App\Models\Directories\InstallmentAlgorithms;
use App\Models\Directories\InsuranceCompanies;
use App\Models\Directories\Products;
use App\Models\Finance\Invoice;
use App\Models\Finance\PayMethod;
use App\Models\Organizations\Organization;
use App\Models\Reports\ReportOrders;
use App\Models\Settings\Bank;
use App\Models\Settings\PointsSale;
use App\Models\Settings\UserBalanceSettings;
use App\Models\User;

/**
 * Class CUDLogsColumnsValue
 * Для понятных значений колонок в логах
 */
class CUDLogsColumnsValue{

    protected static $column;

    /**
     * Для значений contracts
     */
    public static function contractsValues($column, $value)
    {
        self::$column = $column;

        $value = self::check('statys_id') ? array_key_exists($value, Contracts::STATYS) ? Contracts::STATYS[$value] : '' : $value;
        $value = self::check('user_id') ? (int)$value ? User::find($value)->name : '' : $value;
        $value = self::check('insurance_companies_id') ? (int)$value ? InsuranceCompanies::find($value)->title : '' : $value;
        $value = self::check('bso_supplier_id') ? (int)$value ? BsoSuppliers::find($value)->title : '' : $value;
        $value = self::check('product_id') ? (int)$value ? Products::find($value)->title : '' : $value;
        $value = self::check('agent_id') ? (int)$value ? User::find($value)->name : '' : $value;
        $value = self::check('manager_id') ? (int)$value ? User::find($value)->name : '' : $value;
        $value = self::check('referencer_id') ? (int)$value ? User::find($value)->name : '' : $value;
        $value = self::check('parent_agent_id') ? (int)$value ? User::find($value)->name : '' : $value;
        $value = self::check('is_personal_sales') ? ((int)$value ? 'Да' : 'Нет') : $value;
        $value = self::check('drivers_type_id') ? ((int)$value ? 'Да' : 'Нет') : $value;
        $value = self::check('underfiller_check') ? ((int)$value ? 'Да' : 'Нет') : $value;
        $value = self::check('sales_condition') ? ((int)$value ? Contracts::SALES_CONDITION[$value] : '') : $value;
        $value = self::check('financial_policy_id') ? ((int)$value ? FinancialPolicy::find($value)->title : '') : $value;
        $value = self::check('kind_acceptance') ? ((int)$value ? Contracts::KIND_ACCEPTANCE[$value] : '') : $value;
        $value = self::check('type_id') ? ((int)$value ? Contracts::TYPE[$value] : '') : $value;
        $value = self::check('bank_id') ? ((int)$value ? Bank::find($value)->title : '') : $value;
        $value = self::check('owner_id') ? ((int)$value ? Subjects::find($value)->title : '') : $value;
        $value = self::check('insurer_id') ? ((int)$value ? Subjects::find($value)->title : '') : $value;
        $value = self::check('beneficiar_id') ? ((int)$value ? Subjects::find($value)->title : '') : $value;
        $value = self::check('check_user_id') ? ((int)$value ? User::find($value)->name : '') : $value;
        $value = self::check('underfiller_id') ? ((int)$value ? User::find($value)->name : '') : $value;
        $value = self::check('installment_algorithms_id') ? ((int)$value ? InstallmentAlgorithms::find($value)->algorithm->title : '') : $value;

        return $value;
    }


    /**
     * Для значений bso_items
     */
    public static function bso_itemsValues($column, $value)
    {
        self::$column = $column;

        $value = self::check('bso_supplier_id') ? (int)$value ? BsoSuppliers::find($value)->title : '' : $value;
        $value = self::check('insurance_companies_id') ? (int)$value ? InsuranceCompanies::find($value)->title : '' : $value;
        $value = self::check('point_sale_id') ? (int)$value ? PointsSale::find($value)->title : '' : $value;
        $value = self::check('org_id') ? (int)$value ? Organization::find($value)->title : '' : $value;
        $value = self::check('bso_class_id') ? (int)$value && $value == 100 ? 'Квитанция' : 'БСО' : $value;
        $value = self::check('product_id') ? (int)$value ? Products::find($value)->title : '' : $value;
        $value = self::check('type_bso_id') ? (int)$value ? BsoType::find($value)->title : '' : $value;
        $value = self::check('state_id') ? BsoState::find($value) ? BsoState::find($value)->title : '' : $value;
        $value = self::check('location_id') ? (int)$value ? BsoLocations::find($value)->title : '' : $value;
        $value = self::check('user_id') ? (int)$value ? User::find($value)->name : '' : $value;
        $value = self::check('user_org_id') ? (int)$value ? Organization::find($value)->title : '' : $value;
        $value = self::check('bso_serie_id') ? (int)$value ? BsoSerie::find($value)->bso_serie : '' : $value;
        $value = self::check('bso_dop_serie_id') ? (int)$value ? BsoDopSerie::find($value)->bso_dop_serie : '' : $value;
        $value = self::check('bso_blank_serie_id') ? (int)$value ? BsoSerie::find($value)->bso_serie : '' : $value;
        $value = self::check('bso_blank_dop_serie_id') ? (int)$value ? BsoDopSerie::find($value)->bso_dop_serie : '' : $value;
        $value = self::check('acts_sk_id') ? (int)$value ? ReportAct::find($value)->title : '' : $value;
        $value = self::check('bso_act_id') ? (int)$value ? BsoActsItems::find($value)->act->act_name : '' : $value;
        $value = self::check('bso_manager_id') ? ((int)$value ? User::find($value)->name : '') : $value;
        $value = self::check('agent_id') ? ((int)$value ? User::find($value)->name : '') : $value;
        $value = self::check('is_reserved') ? ((int)$value ? 'Да' : 'Нет') : $value;
        $value = self::check('in_basket') ? ((int)$value ? 'Да' : 'Нет') : $value;
        $value = self::check('realized_act_id') ? ((int)$value ? BsoActs::find($value)->act_name : '') : $value;

        return $value;
    }


    /**
     * Для значений payments
     */
    public static function paymentsValues($column, $value)
    {
        self::$column = $column;

        $value = self::check('statys_id') ? (int)$value ? Payments::STATUS[$value] : '' : $value;
        $value = self::check('type_id') ? (int)$value ? Payments::TRANSACTION_TYPE[$value] : '' : $value;
        $value = self::check('is_deleted') ? (int)$value ? 'Да' : 'Нет' : $value;
        $value = self::check('financial_policy_manually_set') ? (int)$value ? 'Да' : 'Нет' : $value;
        $value = self::check('bso_not_receipt') ? (int)$value ? 'Да' : 'Нет' : $value;
        $value = self::check('set_balance') ? (int)$value ? 'Да' : 'Нет' : $value;
        $value = self::check('in_basket') ? (int)$value ? 'Да' : 'Нет' : $value;
        $value = self::check('bso_id') ? (int)$value ? BsoItem::find($value)->bso_title : '' : $value;
        $value = self::check('payment_type') ? (int)$value ? Payments::PAYMENT_TYPE[$value] : '' : $value;
        $value = self::check('payment_flow') ? (int)$value ? Payments::PAYMENT_FLOW[$value] : '' : $value;
        $value = self::check('financial_policy_id') ? ((int)$value ? FinancialPolicy::find($value)->title : '') : $value;
        $value = self::check('user_id') ? ((int)$value ? User::find($value)->name : '') : $value;
        $value = self::check('agent_id') ? ((int)$value ? User::find($value)->name : '') : $value;
        $value = self::check('manager_id') ? (int)$value ? User::find($value)->name : '' : $value;
        $value = self::check('referencer_id') ? (int)$value ? User::find($value)->name : '' : $value;
        $value = self::check('check_user_id') ? (int)$value ? User::find($value)->name : '' : $value;
        $value = self::check('parent_agent_id') ? (int)$value ? User::find($value)->name : '' : $value;
        $value = self::check('accept_user_id') ? (int)$value ? User::find($value)->name : '' : $value;
        $value = self::check('realized_act_id') ? ((int)$value ? BsoActs::find($value)->act_name : '') : $value;
        $value = self::check('org_id') ? (int)$value ? Organization::find($value)->title : '' : $value;
        $value = self::check('point_sale_id') ? (int)$value ? PointsSale::find($value)->title : '' : $value;
        $value = self::check('reports_order_id') ? (int)$value ? ReportOrders::find($value)->title : '' : $value;
        $value = self::check('reports_dvou_id') ? (int)$value ? ReportOrders::find($value)->title : '' : $value;
        $value = self::check('acts_sk_id') ? (int)$value ? ReportAct::find($value)->title : '' : $value;
        $value = self::check('pay_method_id') ? (int)$value ? PayMethod::find($value)->title : '' : $value;

        return $value;
    }


    /**
     * Для значений invoices
     */
    public static function invoicesValues($column, $value)
    {
        self::$column = $column;

        $value = self::check('user_id') ? (int)$value ? User::find($value)->name : '' : $value;
        $value = self::check('status_id') ? (int)$value ? Invoice::STATUSES[$value] : '' : $value;
        $value = self::check('type') ? (int)$value ? Invoice::TYPES[$value] : '' : $value;
        $value = self::check('type_invoice_payment_id') ? (int)$value ? Invoice::TYPE_INVOICE_PAYMENT[$value] : '' : $value;
        $value = self::check('create_type') ? (int)$value ? Invoice::CREATE_TYPES[$value] : '' : $value;
        $value = self::check('org_id') ? (int)$value ? Organization::find($value)->title : '' : $value;
        $value = self::check('agent_id') ? ((int)$value ? User::find($value)->name : '') : $value;
        $value = self::check('invoice_payment_user_id') ? ((int)$value ? User::find($value)->name : '') : $value;
        $value = self::check('invoice_payment_balance_id') ? ((int)$value ? UserBalanceSettings::find($value)->title : '') : $value;

        return $value;
    }

    public static function check($column){
        return self::$column == $column ? true : false;
    }

    public static function __callStatic($name, $arguments){
        return $arguments[1];
    }
}