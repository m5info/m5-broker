<?php
namespace App\Classes\Notification\Orders;

use App\Models\Contracts\Contracts;

class OrderDeclinedSampler{

    public static function sample(Contracts $contract){

        //ищем последний лог отправки в коррекцию и смотрим кто отправил
        if($log = $contract->inspection_logs->where('new_status', 2)->last()){
            return collect([$log->user]);
        }
        return collect();
    }
}