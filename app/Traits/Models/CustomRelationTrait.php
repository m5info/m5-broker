<?php
namespace App\Traits\Models;

use App\Relations\Custom;
use App\Relations\CustomOne;
use Closure;

trait CustomRelationTrait{

    public function custom($related, Closure $baseConstraints){
        $instance = new $related;
        $query = $instance->newQuery();
        return new Custom($query, $this, $baseConstraints, $baseConstraints);
    }

    public function custom_one($related, Closure $baseConstraints){
        $instance = new $related;
        $query = $instance->newQuery();
        return new CustomOne($query, $this, $baseConstraints, $baseConstraints);
    }



}
