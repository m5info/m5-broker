<?php

namespace App\Traits\Models;


use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use ReflectionClass;

trait GetRelatedTrait{

    function get_relation_methods(){


        $reflection_class = false;
        $file_name = false;

        $relation_methods = [];

        try {

            $reflection_class = new ReflectionClass($this);

            $file_name = $reflection_class->getFileName();

        } catch (Exception $e) {}


        if ($reflection_class && $methods = $reflection_class->getMethods()) {

            foreach ($methods as $method) {

                $check = [
                    $method->isPublic(),
                    !$method->isStatic(),
                    count($method->getParameters()) == 0,
                    $method->getFileName() == $file_name
                ];


                if (!in_array(false, $check, true)) {

                    $method_name = $method->getName();

                    try {

                        if($method_result = $this->{$method_name}()){

                            if($method_result instanceof Relation){

                                $relation_methods[] = $method_name;

                            }

                        }

                    } catch (Exception $e) {}

                }

            }

        }

        return $relation_methods;

    }


    public function get_all_relations($without = []){

        $relations = [];

        $relation_methods = $this->get_relation_methods();

        try{

            foreach ($relation_methods as $method_name){

                if(!in_array($method_name, $without)){

                    $relations[$method_name] = $this->{$method_name};

                }

            }

        }catch (Exception $e){}

        return $relations;

    }



}