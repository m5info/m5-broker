<?php

namespace App\Traits\Models;

use DB;

trait ManyOperationTrait{

    public static function create_many($array){

        $properties = get_class_vars(self::class);

        if(isset($properties['table']) && !empty($properties['table'])){
            $table = $properties['table'];

            $fields = implode(', ', array_keys(current($array)));
            $rows = "";
            foreach ($array as $item){
                $item = array_map('addslashes', $item);
                $rows .= !empty($rows) ? ", \n" : "\n";
                $rows .= "(\"".implode("\", \"", $item)."\")";
            }

            $sql = "insert into {$table} ({$fields}) VALUES {$rows}";

            if(DB::insert($sql)){
                return true;
            }

        }

        return false;
    }



}